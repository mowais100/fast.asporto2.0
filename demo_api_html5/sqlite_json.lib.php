<?php

ini_set('display_errors', 'On');


error_reporting(0);

set_time_limit(0);

@$host = $_REQUEST['db_host'];
@$user = $_REQUEST['db_user'];
@$pass = $_REQUEST['db_pass'];

@$key = $_REQUEST['key'];

$db_name = $_REQUEST['db_name'];
$db_type = $_REQUEST['type'];

$prefix = '../';

if ($db_type === 'mysql') {
    $db = new mysqli($host, $user, $pass, $db_name);
} else {
    if ($db_name === "DATABASE_SETTAGGI.sqlite") {
        $encrypted_code = file_get_contents($prefix . 'DATABASE_SETTAGGI_ENCRYPTED.sqlite'); //Get the encrypted code.
        $decrypted_code = my_decrypt($encrypted_code, $key); //Decrypt the encrypted code.
        file_put_contents($prefix . 'DATABASE_SETTAGGI.sqlite', $decrypted_code); //Save the decrypted code somewhere.
    }
    $db = new SQLite3($prefix . $db_name);
}

//lcz corrisponderebbe a $_GET['lcz']
if (function_exists('filter_input')) {
    $id_licenza = filter_input(INPUT_POST, 'lcz', FILTER_SANITIZE_NUMBER_INT);
    $tabella = filter_input(INPUT_POST, 'tbx', FILTER_SANITIZE_STRING);
} else {
    $id_licenza = $_POST['lcz'];
    $tabella = $_POST['tbx'];
}

if ($db_type !== 'mysql') {

    $tablesquery = $db->query("PRAGMA table_info($tabella);");

    for ($i = 0; $table = $tablesquery->fetchArray(SQLITE3_ASSOC); $i++) {
        $colonna[$i] = $table['name'];
    }

    $prodotti[0] = $colonna;
}

if ($db_type === 'mysql') {
    $query = "SELECT * FROM fastcassa_$tabella WHERE id_licenza=$id_licenza ORDER BY id ASC;";
} else if ($tabella === "comanda") {
    //ESEMPIO DATA 19-05-16
    if (date("H") < 5) {
        $data = date("Y-m-d", strtotime("-1 days"));
    } else {
        $data = date("Y-m-d");
    }
    //BISOGNA AVERE ANCHE I NON ATTIVI PER SICUREZZA IN CASO DI RIALLINEAMENTO
    //$query = "SELECT * FROM comanda WHERE substr(id,5,8)='$data' ORDER BY id ASC;";
    $query = "SELECT * FROM comanda WHERE data_servizio='$data' ORDER BY id ASC;";
} else if ($tabella === "prodotti") {
    $query = "SELECT * FROM prodotti WHERE categoria!='XXX' and descrizione!='.NUOVO PRODOTTO' ORDER BY id ASC;";
} else {
    $query = "SELECT * FROM $tabella ORDER BY id ASC;";
}

$risultato = $db->query($query);

if ($db_type === 'mysql') {
    for ($i = 1; $riga = $risultato->fetch_assoc(); $i++) {
        $prodotti[$i] = $riga;
    }
} else {
    for ($i = 1; $riga = $risultato->fetchArray(SQLITE3_ASSOC); $i++) {
        $prodotti[$i] = $riga;
    }
}


$db->close();

if ($db_name === "DATABASE_SETTAGGI.sqlite") {
    $code = file_get_contents($prefix . 'DATABASE_SETTAGGI.sqlite'); //Get the code to be encypted.
    $encrypted_code = my_encrypt($code, $key); //Encrypt the code.    
    file_put_contents($prefix . 'DATABASE_SETTAGGI_ENCRYPTED.sqlite', $encrypted_code); //Save the ecnypted code somewhere.
    unlink($prefix . 'DATABASE_SETTAGGI.sqlite');
}

echo json_encode($prodotti);

function my_encrypt($data, $key) {
    $encryption_key = base64_decode($key);
    $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
    $encrypted = openssl_encrypt($data, 'aes-256-cbc', $encryption_key, 0, $iv);
    return base64_encode($encrypted . '::' . $iv);
}

function my_decrypt($data, $key) {
    $encryption_key = base64_decode($key);
    list($encrypted_data, $iv) = explode('::', base64_decode($data), 2);
    return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
}
