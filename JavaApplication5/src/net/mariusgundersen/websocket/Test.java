package net.mariusgundersen.websocket;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.mariusgundersen.websocket.WebSocketServer.Listener;

public class Test implements Listener {

    ArrayList<WebSocket> clients = new ArrayList<WebSocket>();

    @Override
    public void newConnection(WebSocket socket) {

        clients.add(socket);

        socket.addEventListener(new WebSocket.Listener() {

            @Override
            public void messageReceived(String message, WebSocket source) {
                System.out.println("String received from client: " + message);

                for (int i = 0; i < clients.size(); i++) {
                    try {
                        //messo il 27 agosto. non so se vada bene
                        if (source != clients.get(i)) {
                            clients.get(i).sendMessage(message);
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            }

            @Override
            public void connectionClosed(WebSocket socket) {
                System.out.println("Connection with client closed");
            }
        });

    }

    @Override
    public void error(String message) {
        System.out.println("Error handshaking with client");
    }

    public static void main(String[] args) {

        final Test test = new Test();
        WebSocketServer server = new WebSocketServer(7779);
        server.addEventListener(test);
        server.startListening();
    }
}
