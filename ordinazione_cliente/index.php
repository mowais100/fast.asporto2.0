﻿<!DOCTYPE html>
<!-- DOCUMENTAZIONE: https://bootswatch.com/darkly/ -->
<html>
    <head>
        <title>fast.intellinet</title>

        <!-- TAG BASE -->
        <meta charset="UTF-8">
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="mobile-web-app-capable" content="yes">

        <!--ICONA-->
        <link rel="icon" href="../img/fastintellinet.ico?random=<?php echo time(); ?>">

        <!-- CSS DI BOOTSTRAP -->
        <link href="../bootstrap/css/bootstrap.css?random=<?php echo time(); ?>" rel="stylesheet">
        <link href="../bootstrap/css/bootstrap.min.css?random=<?php echo time(); ?>" rel="stylesheet">
        <link href="../bootstrap/css/jquery-ui.min.css?random=<?php echo time(); ?>" rel="stylesheet">

        <!-- Temi e aggiunte Bootstrap -->
        <link href="../bootstrap/css/darkly.min.css?random=<?php echo time(); ?>" rel="stylesheet">
        <link href="../bootstrap/css/docs.min.css?random=<?php echo time(); ?>" rel="stylesheet">
        <link href="../bootstrap/css/sticky-footer-navbar.css?random=<?php echo time(); ?>" rel="stylesheet">
        <link href="../bootstrap/css/stile_fastintelligent.css?random=<?php echo time(); ?>" rel="stylesheet">

        <!-- Classe loading per il caricamento della webapp-->
        <link href="../bootstrap/css/loading.css?random=<?php echo time(); ?>" rel="stylesheet">

        <link href='http://fonts.googleapis.com/css?family=Italianno' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Tajawal' rel='stylesheet' type='text/css'>


        <script type="text/javascript" src="screenfull.js?random=<?php echo time(); ?>"></script>


        <!-- jQuery -->
        <script type="text/javascript" src="../jQuery/jquery-1.11.3.min.js?random=<?php echo time(); ?>"></script>
        <script type="text/javascript" src="../jQuery/jquery-2.1.4.min.js?random=<?php echo time(); ?>"></script>
        <script type="text/javascript" src="../jQuery/jquery-ui.js?random=<?php echo time(); ?>"></script>

        <!-- Plugins jQuery -->
        <script type="text/javascript" src="../jQuery/jquery.ui.touch-punch.min.js?random=<?php echo time(); ?>"></script>
        <script type="text/javascript" src="../jQuery/jquery.mobile.custom.min.js?random=<?php echo time(); ?>"></script>
        <script type="text/javascript" src="../jQuery/fabric.js?random=<?php echo time(); ?>"></script>

        <!-- PrintArea -->
        <script type="text/javascript" src="../classi_javascript/jquery.PrintArea.js?random=<?php echo time(); ?>" ></script>

        <!-- Bootstrap Js -->
        <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js?random=<?php echo time(); ?>" ></script>


        <script src="../classi_javascript/bootbox.min.js" type="text/javascript"></script>



        <!-- Classi per la stampa -->
        <script type="text/javascript" src="../classi_javascript/epos-print-4.1.0.js?random=<?php echo time(); ?>"></script>
        <script type="text/javascript" src="../classi_javascript/fiscalprint.js?random=<?php echo time(); ?>"></script>
        <script type="text/javascript" src="../classi_javascript/spooler.js?random=<?php echo time(); ?>"></script>

        <!-- Gestione del database locale -->
        <script type="text/javascript" src="gestione_db_cliente.js?random=<?php echo time(); ?>" ></script>
        <script type="text/javascript" src="../classi_javascript/funzioni_db_locale.js?random=<?php echo time(); ?>"></script>

        <!-- Riconnessione al websocket -->
        <script type="text/javascript" src="../classe_riconnessione_websocket/reconnecting-websocket.js?random=<?php echo time(); ?>"></script>

        <script type="text/javascript">
            var websocket = new Array();
        </script>

        <!-- 3 SOCKET IN CONTEMPORANEA -->
        <script type="text/javascript" src="../classi_javascript/socket.js?random=<?php echo time(); ?>"></script>

        <!-- Gestione multilanguage -->
        <script type="text/javascript" src="../classi_javascript/multilanguage.js?random=<?php echo time(); ?>"></script>

        <!-- Configurazione iniziale -->
        <script type="text/javascript" src="configuration_cliente.js?random=<?php echo time(); ?>"></script>



        <!--Raccoglitore informazioni-->
        <script type="text/javascript" src="../classi_javascript/raccoglitore_informazioni.js?random=<?php echo time(); ?>"></script>


        <!-- Script principali per la gestione locale -->
        <script type="text/javascript" src="../classi_javascript/cassa.js?random=<?php echo time(); ?>"></script>

        <script type="text/javascript" src="../classi_javascript/gestione_buoni.js?random=<?php echo time(); ?>"></script>


        <script type="text/javascript" src="../classi_javascript/listeners_tastiera.js?random=<?php echo time(); ?>"></script>


        <script type="text/javascript" src="../classi_javascript/gestione_settaggi.js?random=<?php echo time(); ?>"></script>

        <script src="../classi_javascript/split_conto.js?random=<?php echo time(); ?>" type="text/javascript"></script>

        <script type="text/javascript" src="bootstrap_cliente.js?random=<?php echo time(); ?>"></script>
        <script src="../classi_javascript/servizio.js?random=<?php echo time(); ?>" type="text/javascript"></script>
        <script src="../classi_javascript/stampa_comanda.js?random=<?php echo time(); ?>" type="text/javascript"></script>

        <!-- Gestione sicurezza MD5 e date -->
        <script type="text/javascript" src="../classi_javascript/md5.js?random=<?php echo time(); ?>"></script>
        <script type="text/javascript" src="../classi_javascript/dateformat.js?random=<?php echo time(); ?>"></script>

        <!-- Tastiera virtuale -->
        <link href="../tastiera_virtuale/css/keyboard.css?random=<?php echo time(); ?>" rel="stylesheet">
        <link href="../tastiera_virtuale/css/keyboard-previewkeyset.css?random=<?php echo time(); ?>" rel="stylesheet">
        <script src="../tastiera_virtuale/js/jquery.keyboard.js?random=<?php echo time(); ?>"></script>
        <script src="../tastiera_virtuale/js/jquery.keyboard.extension-all.js?random=<?php echo time(); ?>"></script>

        <!--PING-->
        <script src="../ping_js/ping.js?random=<?php echo time(); ?>" type="text/javascript"></script>

        <script src="../classi_javascript/incassi_giornalieri.js?random=<?php echo time(); ?>" type="text/javascript"></script>

        <script src="../MultiDatesPicker/jquery-ui.multidatespicker.js?random=<?php echo time(); ?>"></script>

        <script type="text/javascript" src="../classi_javascript/event_register.js?random=<?php echo time(); ?>"></script>
        <script type="text/javascript" src="../chart/dist/Chart.bundle.js?random=<?php echo time(); ?>"></script>

        <script type="text/javascript" src="../classi_javascript/ricerca_per_lettera.js?random=<?php echo time(); ?>"></script>

        <style>

            .elenco_categorie_list>.list-group>a{
                width: 100% !important;
                min-height: 50px !important;
                border-radius: 2px !important;
                font-size: 3vh;
            }

            body,h1,h2{
                font-family: "Century Gothic", CenturyGothic, AppleGothic, sans-serif;
            }

            .bs-glyphicons li{                
                min-width:100% !important;
                font-size: 3vh;
                height: 10vh;
                color:black;
                border-radius:2px;
            }

            .bs-glyphicons li:hover {
                color:initial;
            }

            .bs-glyphicons li:active {
                background-color:#3498db !important;
                color:black;
            }

            .btn{
                border-radius:10px;
            }

            .ultime_battiture tr>td{
                text-align:left;
                font-size:3.2vh;
                padding:2px !important;
            }

            #tab_sx > div{
                font-size:3vh !important;
                margin-bottom:0 !important;

            }

            #tab_sx > div >a{
                border-radius:10px;
            }

            #conto_grande th, #conto_grande td{
                font-size:3vh;
            }

            #tab_dx th, #tab_dx tr:not(.portata) td{
                font-size:3vh;
                text-align: left;
                padding: 5px;
                padding-top: 10px;
                padding-bottom:10px;
            }


            .btn-default,.btn-default:focus,btn-default:active,btn-default:hover,btn-default:visited{
                background-color:#464545;
                outline:0 !important;
                outline-color:#464545;
                border:5px solid #464545;
                border:0;
            }

            .bs-glyphicons li:active{
                background-color:#e74c3c !important;
            }

            .modal{
                z-index:2000;
            }

            .modal-body{
                overflow:auto;
            }

            h4.modal-title{
                font-size:4vh;
            }

            ::-webkit-scrollbar { 
                display: none; 
            }

            .table-striped>tbody>tr:nth-of-type(odd) {
                background-color: initial;
            }

            .table-hover>tbody>tr:hover {
                background-color: initial;
            }

            .table-hover>tbody>tr:focus {
                outline: initial;
            }



        </style>
        <script>
            comanda.tavolo = '<?php echo @$_GET['tavolo']; ?>';
            comanda.dispositivo = 'TAVOLO SELEZIONATO';

        </script>
    </head>

    <body style="width:99%;/*height:100vh;*/font-family:Tajawal;padding-top: 2vh;">


        <div class="mostra_resto col-xs-6 nopadding text-left" id="fullscreen" style="display:none;">
            <div style="padding-top:1vh;">
                <span style="margin:15px;margin-top:5px;margin-bottom:5px;height:3vh;font-size:3vh;color:white;">TAVOLO <?php echo @$_GET['tavolo']; ?></span>
            </div>
        </div>

        <div class="mostra_resto col-xs-6 nopadding" id="totale_conto"  style="display:none;">                  
            <div style="padding-top:0.5vh;float:right;">
                <button  class="btn btn-default" style="padding:3px;    padding-bottom: 0;border-radius:2px;" onclick="carrello_fastorder();">
                    <span class="glyphicon glyphicon-shopping-cart"></span>
                    <span style="z-index: 1100;clear: both;width:16%;top:2px;text-align:center;" id="quantita_carrello"></span>
                </button>
            </div>
            <div style="padding-top:1vh;float:right;">
                <span style="font-size:3vh;">&nbsp;</span>
            </div>
            <div style="padding-top:1vh;float:right;">
                <span style="font-size:3vh;">&euro; </span>
                <span style="font-size:3vh;" class="totale_scontrino"></span>
                <span style="font-size:3vh;">&nbsp;</span>
            </div>      
        </div>

        <div class="col-xs-12" id="fai_il_tuo_ordine" style="position:fixed;top:40vh;height:10vh;font-family:Italianno;font-size:8vh;text-align:center">Fai il tuo Ordine!</div>

        <script>

            function mostra_resto() {
                $(".mostra_resto").show();

            }

            $("#fai_il_tuo_ordine").fadeOut(4000, "linear", function () {
                mostra_resto();
            });
        </script>


        <div class="mostra_resto" style="display:none;clear: both;overflow: auto;padding-top: 1vh;">

            <div id="tab_dx" class="col-xs-12 text-center" style="overflow: auto;height:95vh;display:none;">

                <div style="overflow: auto;height:80vh;">

                    <table class="tab_conto table bg-info">
                        <thead class="bg-info" id="intestazioni_conto">
                        </thead>
                    </table>

                    <table class="tab_conto table table-striped table-hover">
                        <tbody id="conto" >
                        </tbody>
                    </table>
                </div>
                <div class="col-xs-2 text-center nopadding" style="border-right:0.5vh solid black;height:9vh;"><button onclick="cancella_conto_intero();" style="border-radius:2px;height:50px;font-size:3vh;padding:0;" class="form-control btn btn-danger">&#10008;</button></div>
                <div class="col-xs-10 text-center nopadding" style="height:9vh;"><button onclick="invia_ordinazione_da_pagare();" style="border-radius:2px;height:50px;font-size:3vh;padding:0;" class="form-control btn btn-success"><span>ORDINA</span></button></div>


            </div>

            <div id="tab_sx"  class="col-xs-12 text-center elenco_categorie_list" style="overflow: auto;padding:4px;">

            </div>

            <div id="elenco_prodotti" class="col-xs-12 text-center elenco_prodotti" style="overflow: auto;height:83vh;display:none;">

            </div>


            <div id="ordine_inviato" class="col-xs-12 text-center" style="overflow: auto;height:85vh;display:none;">
                <h2>ORDINE INVIATO.</h2>
                <h4>LA INVITIAMO A RECARSI IN CASSA<br/>
                    PER EFFETTUARE IL PAGAMENTO.</h4>
                <br/>
                <br/>
                <br/>
                <br/>
                <h4 id="totale_conto">Prezzo Totale: <span class="totale_scontrino"></span></h4>
                <h4>Numero di persone: <input type="number" id="numero_persone" value="1"  style="width: 10%;"></h4>
                <h4>Prezzo a testa: <span id="prezzo_a_testa"></span></h4>            
            </div>
        </div>


    </body>

</html>
