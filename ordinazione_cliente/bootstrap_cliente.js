function salva_utente()
{
    var scadenza = new Date();
    var adesso = new Date();
    var md5 = +new Date();
    md5 = CryptoJS.MD5(md5.toString()).toString();
    scadenza.setTime(adesso.getTime() + (24 * 60 * 60000));
    document.cookie = "UTENTE" + '=' + md5 + '; expires=' + scadenza.toGMTString() + '; path=/';

    return md5;
}


var fine_conto_attivo = function (callBACCO) {


    $('#intestazioni_conto_separato_grande').show();
    //console.log(indice, array_numero_righe, indice === array_numero_righe);
    $('#intestazioni_conto_separato_2_grande').show();
    //console.log("TOTALE REINSERITO");

    calcola_totale();

    /*if (comanda.mobile === true || comanda.pizzeria_asporto === true) {
        ultime_battiture();
    }*/

    if (typeof (callBACCO) === "function") {
        console.log("CALLBACK CONTO ATTIVO");
        callBACCO(true);
    }

};

function once(fn) {

    var run = true;

    var timeout = setTimeout(function () {
        if (run) {
            //fn(false);
            run = false;
            alert("ATTENZIONE! RTEngine bloccati a causa di Problemi di Rete. Non puoi continuare a lavorare in queste condizioni.\n\nAssicurati che la rete funzioni bene, e riavvia il programma");
            $('#popup_attesa_socket').modal('hide');
            $('#messaggi_caricamento').hide();
            $('#loading').hide();
            $('#game-over').modal('show');
            window.close();
        }
    }, 20000);

    return function () {
        if (run) {
            fn.apply(null, arguments);
            //resolve(true);
            clearTimeout(timeout);
            run = false;
        }
    };
}

function invia_ordinazione_da_pagare() {
    var testo_query = "update tavoli set occupato='0',colore='green' where numero='" + comanda.tavolo + "';";

    comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
    comanda.sincro.query(testo_query, function () {

        var testo_query = "update comanda set nome_comanda='"+comanda.parcheggio+"',operatore='INVIATO' where ntav_comanda='" + comanda.tavolo + "' and stato_record='ATTIVO' and operatore='" + comanda.operatore + "';";

        comanda.sock.send({tipo: "aggiornamento_tavoli", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
        comanda.sincro.query(testo_query, function () {

            $('#fullscreen').hide();
            $('#totale_conto').hide();
            $('#tab_sx').hide();
            $('#tab_dx').hide();
            $('#elenco_prodotti').hide();
            $('#ordine_inviato').show();

            $('#prezzo_a_testa').html('€ ' + (parseFloat(comanda.totale_fastorder) / $('#numero_persone').val()).toFixed(2));
        });
    });
}

function elenco_categorie_main() {
    $('#tab_sx > div> a').show();
    $('#tab_sx').css('height', '');
    $('#tab_sx').css('overflow', '');
    $('#elenco_prodotti').hide();
}

function avviso_varianti_fastorder() {
    console.log("evento", event);
    $(event.target).css('opacity', '0.1');
    $(event.target).parent().parent().find('.avviso_fastorder').html('<h4>Vuoi aggiungere qualcosa?</h4><h5 style="font-weight:bold" onclick="risposta_avviso_varianti_fastorder(true)">SI</h5><h5 style="font-weight:bold" onclick="risposta_avviso_varianti_fastorder(false)">NO</h5>');
}

function risposta_avviso_varianti_fastorder(risposta) {

    if (risposta === true) {
        mod_categoria(comanda.ultima_cat_variante, '+');
    }

    console.log("evento", event);
    
    $(event.target).parent().parent().find('.immagine_fastorder img').css('opacity', '');
    $(event.target).parent().parent().find('.avviso_fastorder').html('');

    event.preventDefault();
    event.stopPropagation();

}

function nome_comanda(){   
    comanda.parcheggio=$('#nome_utente_input').val();
    $('#nome_utente').hide();
   
}



function carrello_fastorder() {
    if (comanda.paginata !== "carrello") {
        comanda.paginata = "carrello";

        $('#tab_sx').hide();
        $('#tab_sx').css('height', 'auto');
        $('#tab_sx').css('overflow', 'hidden');

        $('#elenco_prodotti').hide();

        $('#tab_dx').show();
    } else
    {
        comanda.paginata = "categorie";
        $('#elenco_prodotti').hide();
        $('#tab_dx').hide();

        $('#tab_sx').show();
        $('#tab_sx > div> a').show();
        $('#tab_sx').css('height', '');
        $('#tab_sx').css('overflow', '');
    }
}

function categoria_cliccata() {
    if (comanda.paginata === "prodotti") {
        comanda.paginata = "categorie";
        elenco_categorie_main();
    } else {
        comanda.paginata = "prodotti";
        $('#tab_sx > div > a').not(event.target).hide();
        $('#tab_sx').css('height', 'auto');
        $('#tab_sx').css('overflow', 'hidden');
        $(event.target).css('background-color', 'rgb(70, 69, 69)');
        $('#elenco_prodotti').show();
    }
}



$(document).on('change', '#numero_persone', function () {
    $('#prezzo_a_testa').html('€ ' + (parseFloat(comanda.totale_fastorder) / $('#numero_persone').val()).toFixed(2));
});

$(document).ready(function () {

    var md5 = getCookie('UTENTE');

    if (md5 === null) {
        md5 = salva_utente();
    }
    comanda.operatore = md5;
    comanda.paginata = "categorie";
    comanda.terminale = "TAVOLO" + comanda.tavolo;
    comanda.ip_address = comanda.tavolo;

    lng(comanda.lingua);
    lngstm(comanda.lingua_stampa, function () {});

    /*comanda.sincro.query("select occupato,colore from tavoli where numero='" + comanda.tavolo + "';", function (colore) {

        if (colore[0].colore !== 'green')
        {*/
            socket_init(function () {
                var testo_query = "update tavoli set occupato='1' where numero='" + comanda.tavolo + "';";

                comanda.sincro.query(testo_query, function () {

                    comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});

                    comanda.funzionidb.elenco_categorie("list", "1", "0", "#tab_sx");

                    corrispondenze_stampanti(comanda.lingua_stampa);
                    corrispondenze_portate(comanda.lingua_stampa);

                    comanda.funzionidb.elenco_prodotti(function (cb_elenco_prodotti) {});

                    comanda.funzionidb.conto_attivo(function () {});

                });
            });
        /*} else {
            $('#tab_sx').html('<h1>Il tavolo è occupato<br/>o c\'è un ordine in sospeso.</h1>');
            $('#totale_conto').hide();
        }
    });
*/
});

comanda.sock.waitForSocketConnection = function (socket, callback) {

    if (websocket !== undefined && websocket[0] !== undefined && websocket[0].readyState === 1) {
        if (callback !== null) {
            callback(0);
        }
        return;
    } else if (websocket !== undefined && websocket[1] !== undefined && websocket[1].readyState === 1) {
        if (callback !== null) {
            callback(1);
        }
        return;
    } else if (websocket !== undefined && websocket[2] !== undefined && websocket[2].readyState === 1) {
        if (callback !== null) {
            callback(2);
        }
        return;
    } else
    {
        if (callback !== null) {
            callback(false);
        }
        return;
    }
};

//FA PARTE DELLA FUNZIONE SOTTO
var esperimento_output;
var inizio_elenco_prodotti = false;
comanda.funzionidb.elenco_prodotti = function (callBack) {

    if (inizio_elenco_prodotti === false) {
        inizio_elenco_prodotti = true;
        console.log("INIZIO CALLBACK PRODOTTI");
        esperimento_output = new Object();
        //QUERY SETTAGGIO COLORI TABLET
        var query_colori_tablet = "select Field116 from settaggi_profili limit 1;";
        comanda.sincro.query(query_colori_tablet, function (colori_tablet) {

            //Può essere "true" o "false"
            var colori_tablet_uguali_pc = colori_tablet[0].Field116;
            //QUERY DI SELEZIONE CATEGORIE
            var query_categorie = "select * from categorie;";
            var prodotti = new Array();
            //VARIABILE DI OUTPUT

            comanda.sincro.query(query_categorie, function (categoria) {

                var count_cat_number = categoria.length;
                var cat_n = 1;
                categoria.forEach(function (cat, i1) {

                    //RIEMPIO I DATI DELLE CATEGORIE
                    if (prodotti[cat.id] === undefined) {
                        prodotti[cat.id] = new Object();
                    }

                    //QUERY DI SELEZIONE PRODOTTI
                    //IN QUESTO CASO LA POSIZIONE HA SOLO UN NUMERO E NON DELLE COORDINATE
                    var query_prodotti = '';
                    query_prodotti = 'select ricetta_fastorder, immagine, id, ordinamento, cat_varianti,colore_tasto,descrizione,prezzo_1, categoria, pagina, posizione, perc_iva_base, perc_iva_takeaway  from prodotti WHERE descrizione!="" AND prezzo_1 !="" AND categoria="' + cat.id + '" ORDER BY cast(ordinamento as int) ASC, descrizione ASC;';
                    console.log("PROD QUERY", query_prodotti);
                    //FUNZIONE DELLA QUERY
                    comanda.sincro.query(query_prodotti, function (prodotto) {
                        var i = 0;
                        var pagina = 0;
                        //PER OGNI PRODOTTO VADO A CREARE UNA SERIE DI ARRAY CON VARI DATI
                        prodotto.forEach(function (prod, i2) {

                            //CALCOLA LA PAGINA IN BASE AL NUMERO DI PRODOTTI
                            //Al 50esimo articolo cambia pagina e non 49esimo!

                            //ATTENZIONE A QUESTA CONDIZIONE PUO' ESSERE PERICOLOSA!
                            var max_prodotti = 49;
                            if (cat.descrizione.substr(0, 2) === 'V.')
                            {
                                max_prodotti = 48;
                            }

                            if (i % 42 === 0 && comanda.pizzeria_asporto === true) {
                                pagina++;
                            } else
                            {
                                pagina = 1;
                            }

                            if (prodotti[cat.id][pagina] === undefined) {
                                prodotti[cat.id][pagina] = new Object();
                            }

                            if (prodotti[cat.id][pagina][i] === undefined) {
                                prodotti[cat.id][pagina][i] = new Object();
                            }

                            //DATI PRODOTTO
                            prodotti[cat.id][pagina][i].id = prod.id;
                            prodotti[cat.id][pagina][i].cat_varianti = prod.cat_varianti;
                            prodotti[cat.id][pagina][i].colore_tasto = prod.colore_tasto;
                            prodotti[cat.id][pagina][i].descrizione = prod.descrizione;
                            prodotti[cat.id][pagina][i].prezzo_1 = prod.prezzo_1;
                            prodotti[cat.id][pagina][i].categoria = prod.categoria;
                            prodotti[cat.id][pagina][i].pagina = prod.pagina;
                            prodotti[cat.id][pagina][i].posizione = prod.posizione;
                            prodotti[cat.id][pagina][i].perc_iva_base = prod.perc_iva_base;
                            prodotti[cat.id][pagina][i].perc_iva_takeaway = prod.perc_iva_takeaway;
                            prodotti[cat.id][pagina][i].immagine = prod.immagine;
                            prodotti[cat.id][pagina][i].ricetta_fastorder = prod.ricetta_fastorder;
                            i++;
                        });
                        //PRENDO LE PAGINE NEI PRODOTTI
                        for (var p in prodotti[cat.id]) {
                            if (esperimento_output[cat.id] === undefined)
                            {
                                esperimento_output[cat.id] = new Object();
                            }

                            if (esperimento_output[cat.id][p] === undefined)
                            {
                                esperimento_output[cat.id][p] = '';
                            }

                            esperimento_output[cat.id][p] += "<div class='pag_" + p + " cat_" + cat.id + "' >";
                            esperimento_output[cat.id][p] += "<div class='bs-docs-section btn_COMANDA'>";
                            esperimento_output[cat.id][p] += "<div class='bs-glyphicons'>";
                            esperimento_output[cat.id][p] += "<ul class='bs-glyphicons-list elenco_prodotti_draggable'>";
                            var i = 0;
                            if (cat.descrizione.substr(0, 2) === 'V.' && i === 0) {
                                i++;
                                esperimento_output[cat.id][p] += '<li style="height: 106px !important;background-color:lightblue; color:aliceblue; font-weight: bold; font-size:3em;" value="VARIANTELIBERA" class="btn_variante_libera btn_comanda prodotto_esistente"  onClick="popup_variante_libera();">+</li>';
                            }

                            //PRENDO I PRODOTTI
                            for (var art in prodotti[cat.id][p]) {

                                var articolo = prodotti[cat.id][p][art];

                                var colore = '';
                                console.log("COLORE TASTI" + articolo.colore_tasto);
                                console.log("COLORE TASTI", cat.descrizione.substr(0, 2) === 'V.', comanda.mobile === false && comanda.pizzeria_asporto === false && articolo.colore_tasto !== undefined && articolo.colore_tasto !== null && articolo.colore_tasto.length > 0, comanda.mobile === true && articolo.colore_tasto !== undefined && articolo.colore_tasto !== null && articolo.colore_tasto.length > 0, comanda.pizzeria_asporto === true);
                                //linen
                                var colore_tasto = "linen";
                                colore = ' style="background-color:' + colore_tasto + ';" ';
                                //DIFFERENZA TRA ARTICOLO CON VARIANTE AUTOMATICA E SENZA

                                console.log("ARTICOLO VARIANTE AUTO", articolo, articolo.cat_varianti !== undefined && articolo.cat_varianti !== "undefined" && articolo.cat_varianti !== "undefined/undefined" && articolo.cat_varianti !== null && articolo.cat_varianti.indexOf("/") !== -1 && articolo.cat_varianti.split('/')[1] !== "ND");
                                if (articolo.cat_varianti !== undefined && articolo.cat_varianti !== "undefined" && articolo.cat_varianti !== "undefined/undefined" && articolo.cat_varianti !== null && articolo.cat_varianti.indexOf("/") !== -1 && articolo.cat_varianti.split('/')[1] !== "ND") {
                                    esperimento_output[cat.id][p] += '<li ' + colore + ' value="' + articolo.id + '" class="btn_comanda prodotto_esistente"  onClick="$(\'body\').css(\'pointer-events\', \'none\');setTimeout(function(){aggiungi_articolo(\'' + articolo.id + '\',\'' + articolo.descrizione.replace('\'', "\\'") + '\',\'&euro;' + articolo.prezzo_1 + '\',null,$(\'#quantita_articolo\').val(),null,\'' + articolo.cat_varianti.match(/\w+/g)[0] + '\',null,null,\'' + articolo.perc_iva_base + '\',\'' + articolo.perc_iva_takeaway + '\');if(comanda.dispositivo=== \'TAVOLO SELEZIONATO\'){setTimeout(function(){comanda.ultima_cat_variante=\'' + articolo.cat_varianti + '\';mod_categoria(\'' + articolo.cat_varianti + '\', \'=\',\'\',true);},250);}},100);">';
                                } else
                                {
                                    //SE LA VARIANTE E' NORMALE
                                    esperimento_output[cat.id][p] += '<li ' + colore + ' value="' + articolo.id + '" class="btn_comanda prodotto_esistente"  onClick="$(\'body\').css(\'pointer-events\', \'none\');setTimeout(function(){aggiungi_articolo(\'' + articolo.id + '\',\'' + articolo.descrizione.replace('\'', "\\'") + '\',\'&euro;' + articolo.prezzo_1 + '\',null,\'1\',null,\'' + articolo.cat_varianti + '\',null,null,\'' + articolo.perc_iva_base + '\',\'' + articolo.perc_iva_takeaway + '\');},100);">';
                                }

                                var ricetta_fastorder = '';

                                if (articolo.ricetta_fastorder !== undefined && articolo.ricetta_fastorder !== null) {
                                    ricetta_fastorder = '<br/>' + articolo.ricetta_fastorder;
                                }

                                if (articolo.immagine !== undefined && articolo.immagine !== null && articolo.immagine !== 'undefined' && articolo.immagine !== '') {
                                    esperimento_output[cat.id][p] += '<div style="text-align:center;padding:5px;"><div class="avviso_fastorder" style="z-index:1000; position: absolute;text-align: center;padding:5px;width: 95%;"></div><div class="immagine_fastorder" onclick="avviso_varianti_fastorder();"><img style="width:95%;height:auto;" src="./immagini/' + articolo.immagine + '" /></div></strong></div>';
                                    esperimento_output[cat.id][p] += '<div class="glyphicon-class"><strong>' + articolo.descrizione + '</strong> € ' + articolo.prezzo_1 + ricetta_fastorder + '</div>';
                                } else if (articolo.ricetta_fastorder !== undefined && articolo.ricetta_fastorder !== null&& articolo.ricetta_fastorder !== 'undefined'&& articolo.ricetta_fastorder !=='') {
                                    esperimento_output[cat.id][p] += '<div class="glyphicon-class"><strong>' + articolo.descrizione + '</strong> € ' + articolo.prezzo_1 + ricetta_fastorder + '</div>';
                                } else
                                {
                                    esperimento_output[cat.id][p] += '<div class="glyphicon-class"><strong>' + articolo.descrizione + '</strong><br/>€ ' + articolo.prezzo_1 + '</div>';
                                }
                                esperimento_output[cat.id][p] += '</li>';


                                i++;
                                console.log("ELENCO PRODOTTI DATI: ", i, pagina);
                            }

                            esperimento_output[cat.id][p] += "</ul>";
                            esperimento_output[cat.id][p] += "</div>";
                            esperimento_output[cat.id][p] += "</div>";
                            esperimento_output[cat.id][p] += "</div>";
                            console.log("ELENCO PRODOTTI DATI " + p + " - NUMERO ARTICOLI: ", prodotti[cat.id][p]);
                        }

                        console.log(cat_n, count_cat_number);
                        if (cat_n === count_cat_number) {
                            console.log("OGGETTO_PRODOTTI", prodotti);
                            inizio_elenco_prodotti = false;
                            callBack(true);
                        }

                        cat_n++;
                    });
                });
            });
        });
    }
};