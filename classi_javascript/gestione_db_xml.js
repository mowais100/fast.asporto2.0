var Sincro = function () {


    var dbtype = 'sqlite';

    var dbname = 'DATABASE_CLIENTE.sqlite';

    //MANCA LA GESTIONE ERRORI
    //SI RISCHIA DI NON ALLINEARE BENE LE TABELLE SE CI SONO ERRORI DI QUERY

    var ar;
    //var db = openDatabase('DB', '1.0', 'Database client side', 2 * 2048 * 2048);
    //this.db_2 = db;
    var query = '';

    this.return_db = function ( ) {
        return db;
    };

    this.backup_db_locale = function (callback) {

        comanda.socket_ciclizzati = new Array();
        callback(true);
    };

    this.sincronizzazione_upload = function (tab, id_lic, callback) {
        var testo_query = new Array();

        switch (tab) {
            case "comanda":
                testo_query[0] = 'SELECT * FROM ' + tab + ' WHERE stato_record="CHIUSO" AND id LIKE "' + comanda.operatore.substr(0, 3) + '_%";';
                testo_query[1] = 'DELETE FROM comanda WHERE stato_record = "CHIUSO";';
                break;

            default:
                testo_query[0] = 'SELECT * FROM ' + tab + ';';
                testo_query[1] = 'select count(0);';
                break;

        }

        comanda.sincro.query(testo_query[0], function (righe) {
            ////console.log(righe);

            var cols = new Array();
            var rows = new Array();


            righe.forEach(function (riga) {
                ////console.log(riga);

                var key = new Array();
                var value = new Array();

                var i = 0;
                for (var chiave in riga) {
                    key[i] = chiave;
                    ////console.log(riga[chiave]);
                    value[i] = "'" + escape(riga[chiave]) + "'";

                    i++;
                }

                cols.push(key.join());
                rows.push(value.join());

            });


            $.ajax({
                type: "POST",
                dataType: "text",
                url: './demo_api_html5/sincro_upload.php',
                data: {tbx: tab, colonne: JSON.stringify(cols[0]), righe: JSON.stringify(rows), type: dbtype,
                    db_name: dbname},
                success: function (risposta) {
                    if (risposta === "")
                    {
                        comanda.sincro.query(testo_query[1], function () {
                        });
                    } else {
                    }
                }
            });

        });
    };

    this.sincronizzazione_download = function (tab, id_lic, callback) {

        //console.log(tab);

        $.ajax({
            beforeSend: function () {
            },
            complete: function () {
            },
            success: function (data) {
                ar = data;

                //console.log("JSON DATA", ar);

                if (ar && ar !== null && ar.length > 0 && ar[0] && ar[0] !== null) {

                    var testo_query;

                    if (tab === "comanda")
                    {
                        testo_query = 'select count(0);';
                    } else
                    {
                        testo_query = 'DROP TABLE IF EXISTS ' + tab + ';';
                    }
                    comanda.sincro.db_2.transaction(function (tx, results) {
                        tx.executeSql(testo_query);

                        console.log("QUERY INCRIMINATA", testo_query);

                        var colonne = ar[0].join(',');

                        if (tab === "lingue")
                        {
                            testo_query = 'CREATE TABLE IF NOT EXISTS ' + tab + ' (' + colonne + ');';
                        } else if (tab !== "comanda")
                        {
                            testo_query = 'CREATE TABLE ' + tab + ' (' + colonne + ');';
                        }

                        tx.executeSql(testo_query);

                        ar.shift();

                        ar.forEach(
                                function (element, index, array)
                                {
                                    var valori = Object.keys(element).map(function (key) {
                                        if (typeof (element[key]) === "string") {
                                            return element[key].replace(/\'/, '');
                                        } else
                                        {
                                            return element[key];
                                        }
                                    });

                                    valori = valori.join('","');
                                    valori = '"' + valori + '"';

                                    testo_query = 'INSERT INTO ' + tab + ' (' + colonne + ') VALUES (' + valori + ');';

                                    tx.executeSql(testo_query);

                                });
                        callback(true);

                    });
                } else
                {
                    callback(true);
                }
                $('.spia.status_server').css('background-color', 'green');
            },
            error: function ()
            {

                if (comanda.syncerror !== true) {
                    comanda.syncerror = true;
                    /*alert("I tempi di risposta del server sono troppo lunghi.\n\
                     Il server potrebbe essere occupato, o non corrispondente ai requisiti tecnici minimi.\n\
                     POTRESTI AVERE PROBLEMI CON I PROGRESSIVI NELLE STAMPANTI TEDESCHE, O CON LA SINCRONIA DEL DATABASE!\n\n\
                     Ticonsigliamo di RIAVVIARE il programma e contattarci subito!");*/
                    $('.spia.status_server').css('background-color', 'red');
                }

                callback(false);
            },
            type: "POST",
            dataType: "json",
            url: './demo_api_html5/sqlite_json.lib.php?timestamp=' + new Date().getTime(),
            //TIMEOUT ALTRIMENTI DA PALMARE NON PARTE MAI
            timeout: 15000,
            data: {lcz: id_lic, tbx: tab, type: dbtype,
                db_name: dbname}
        });
    };


    function comprimi_query(query_input) {

        if (typeof query_input === 'string') {

            var query_output = query_input.replace(/\n/gi, '').replace(/\s\s+/g, ' ');

        } else
        {
            query_output = query_input;
        }

        return query_output;
    }


    this.query_cassa = function (query, timeout, callback) {

        if ((comanda.sincro_query_cassa_attivo !== true && comanda.array_dbcentrale_mancanti !== undefined && comanda.array_dbcentrale_mancanti[0] !== undefined && comanda.array_dbcentrale_mancanti[0] !== null) || (Date.now() / 1000 - comanda.last_time_query_cassa >= 5) || query !== undefined) {

            comanda.sincro_query_cassa_attivo = true;


            var testo_query;

            if (query !== undefined) {
                testo_query = query;
            } else
            {
                testo_query = comanda.array_dbcentrale_mancanti[0];
            }

            testo_query = comprimi_query(testo_query);


            console.log("DB CENTRALE SENDED", testo_query);

            var to = 10000;
            if (timeout !== undefined)
            {
                to = timeout;
            }

            $.ajax({
                type: "POST",
                async: true,
                url: comanda.url_server + 'classi_php/db_realtime_cassa.lib.php',
                data: {
                    type: dbtype,
                    db_name: dbname,
                    query: testo_query
                },
                dataType: "json",
                timeout: to,
                beforeSend: function () {

                    console.log("QUERY CASSA BEFORESEND", testo_query);

                    comanda.last_time_query_cassa = Date.now() / 1000;
                    comanda.sincro_query_cassa_attivo = true;

                },
                success: function (dati) {

                    console.log("QUERY CASSA DEBUG", dati);

                    /*if (typeof (callback) === "function") {
                     console.log("QUERY CASSA CALLBACK TRUE");
                     
                     callback(dati);
                     }*/

                    //Rimuove l'indice 0 per una lunghezza di indice, senza sostituire niente
                    //CASISTICA IN CUI E' UNA QUERY CICLICIZZATA
                    if (query === undefined && dati === true) {

                        console.log("QUERY CASSA SUCCESS TRUE", testo_query, comanda.array_dbcentrale_mancanti[0], comanda.array_dbcentrale_mancanti);

                        comanda.array_dbcentrale_mancanti.shift();

                        console.log("QUERY CASSA DELETED", comanda.array_dbcentrale_mancanti);


                        comanda.last_time_query_cassa = Date.now() / 1000;

                        $('.spia.db_centrale').css('background-color', 'green');

                        comanda.sincro_query_cassa_attivo = false;

                        comanda.sincro.query_cassa();

                        if (typeof (callback) === "function") {
                            console.log("QUERY CASSA CALLBACK TRUE");

                            callback(dati);
                        }


                    }
                    //CASISTICA IN CUI E' UNA QUERY IN CASSA DIRETTA
                    else if (dati === true) {

                        console.log("QUERY CASSA (2) SUCCESS TRUE", testo_query, comanda.array_dbcentrale_mancanti[0], comanda.array_dbcentrale_mancanti);


                        comanda.last_time_query_cassa = Date.now() / 1000;

                        $('.spia.db_centrale').css('background-color', 'green');

                        comanda.sincro_query_cassa_attivo = false;

                        comanda.sincro.query_cassa();

                        if (typeof (callback) === "function") {
                            console.log("QUERY CASSA CALLBACK TRUE");

                            callback(dati);
                        }


                    } else if (isJson(dati) == true) {



                        comanda.last_time_query_cassa = Date.now() / 1000;

                        $('.spia.db_centrale').css('background-color', 'green');

                        console.trace();



                        comanda.sincro_query_cassa_attivo = false;

                        comanda.sincro.query_cassa();

                        if (typeof (callback) === "function") {
                            callback(dati);
                        }

                    } else {

                        if (typeof (callback) === "function") {
                            callback(dati);
                        }

                        console.log("QUERY CASSA SUCCESS ERR", query);

                        comanda.last_time_query_cassa = Date.now() / 1000;


                        $('.spia.db_centrale').css('background-color', 'red');

                        setTimeout(function () {
                            //facendo così la condizione resta bloccata a true fino al prossimo giro
                            //mettendo false fuori dal timeout ci sarebbero 3 secondi di gap con possibili overload
                            comanda.sincro_query_cassa_attivo = false;
                            comanda.sincro.query_cassa();
                        }, 3000);


                    }

                },
                error: function (xhr, status, error) {

                    if (typeof (callback) === "function") {
                        console.log("QUERY CASSA CALLBACK FALSE");

                        callback(false);
                    }

                    console.log("QUERY CASSA ERROR", testo_query, comanda.array_dbcentrale_mancanti[0], xhr.responseText);

                    comanda.last_time_query_cassa = Date.now() / 1000;


                    $('.spia.db_centrale').css('background-color', 'red');


                    setTimeout(function () {
                        //facendo così la condizione resta bloccata a true fino al prossimo giro
                        //mettendo false fuori dal timeout ci sarebbero 3 secondi di gap con possibili overload
                        comanda.sincro_query_cassa_attivo = false;
                        comanda.sincro.query_cassa();
                    }, 3000);
                }

            });
        }

    };

    this.query_sqlserver = function (string, callBack) {

        if (string !== undefined) {


            if (string.toUpperCase().search("COMANDA") !== -1)
            {
                if (string.search("(numero_conto='' OR numero_conto='undefined' OR numero_conto is null OR numero_conto='null' OR numero_conto='1' OR numero_conto=1)") !== -1 || string.search("select distinct numero_conto") !== -1)
                {
                } else {
                }
            }

            $.ajax({
                type: "POST",
                async: true,
                url: comanda.url_server + 'classi_php/query_sqlserver.lib.php',
                data: {
                    /* nomedb,username,password*/
                    SET88: comanda.SET88,
                    SET89: comanda.SET89,
                    SET90: comanda.SET90,
                    query: comprimi_query(string)
                },
                dataType: "json",
                beforeSend: function () {},
                success: function (risultato_json) {
                    var risultato = new Array();

                    for (var i = 0; i < risultato_json.length; i++) {
                        var row = risultato_json[i];
                        risultato[i] = row;
                    }


                    callBack(risultato);
                },
                error: function (errore) {
                    console.log("ERRORE QUERY LOCALE: ", errore, comprimi_query(string));
                    salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # DB ERROR # " + string.replace(/[\n\r]+/g, '') + " # " + new Error().stack.replace(/[\n\r]+/g, ''));
                }
            });

        }
    };

    this.query = function (string, callBack) {


        if (string !== undefined) {

            //salva_log(comanda.id_tablet + " # " +new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # QUERY # " + string.replace(/[\n\r]+/g, '') + " # " + new Error().stack.replace(/[\n\r]+/g, ''));

            if (true) {

                var risultato = alasql(string);

                callBack(risultato);

            } else {
                var risultato = new Array();
                db.transaction(function (tx, results) {

                    tx.executeSql(string, [], function (tx, rs)
                    {

                        for (var i = 0; i < rs.rows.length; i++) {

                            var row = rs.rows.item(i);
                            risultato[i] = row;
                        }

                        callBack(risultato);
                    }, errorHandler);

                    function errorHandler(transaction, error) {
                        console.log("DB ERROR : " + error.message + " -  " + string);
                        salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # DB ERROR # " + string.replace(/[\n\r]+/g, '') + " # " + new Error().stack.replace(/[\n\r]+/g, ''));

                    }

                });
            }
        }
    };


    this.sincronizzazione_alasql = function (tab, id_lic, callback) {




        $.ajax({
            beforeSend: function () {
            },
            complete: function () {
            },
            success: function (data) {

                ar = data;

                //console.log("JSON DATA", ar);

                if (ar && ar !== null && ar.length > 0 && ar[0] && ar[0] !== null) {

                    var testo_query;


                    testo_query = 'DROP TABLE IF EXISTS ' + tab + ';';

                    alasql(testo_query);

                    console.log("QUERY INCRIMINATA", testo_query);

                    var colonne_create_array = ar[0].map(function (e) {
                        return  e + " TEXT DEFAULT '' ";
                    });

                    var colonne_create_string = colonne_create_array.join(',');

                    var colonne = ar[0].join(',');

                    if (tab === "lingue")
                    {
                        testo_query = 'CREATE TABLE IF NOT EXISTS ' + tab + ' (' + colonne_create_string + ');';
                    } else
                    {
                        testo_query = 'CREATE TABLE ' + tab + ' (' + colonne_create_string + ');';
                    }

                    alasql(testo_query);

                    ar.shift();


                    /*ar.forEach(
                     function (element, index, array)
                     {
                     var valori = Object.keys(element).map(function (key) {
                     if (typeof (element[key]) === "string") {
                     return element[key].replace(/\'/, '');
                     } else
                     {
                     return element[key];
                     }
                     });
                     
                     valori = valori.join('","');
                     valori = '"' + valori + '"';
                     
                     testo_query = 'INSERT INTO ' + tab + ' (' + colonne + ') VALUES (' + valori + ');';
                     
                     alasql(testo_query);
                     
                     });*/

                    alasql.tables[tab].data = ar;






                    callback(true);




                } else
                {


                    callback(true);


                }
                $('.spia.status_server').css('background-color', 'green');
            },
            error: function ()
            {

            },
            type: "POST",
            dataType: "json",
            url: comanda.url_server + 'demo_api_html5/sqlite_json.lib.php',
            //TIMEOUT ALTRIMENTI DA PALMARE NON PARTE MAI
            timeout: 15000,
            data: {tbx: tab, type: 'sqlite',
                db_name: 'DATABASE_CLIENTE.sqlite'}
        });

    }
};

