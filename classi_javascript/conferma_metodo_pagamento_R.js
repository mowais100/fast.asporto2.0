async function conferma_metodo_pagamento(bypass, carta) {

    var testa = new Object();
    testa.totale = parseFloat($(comanda.totale_scontrino).html()).toFixed(2);
    testa.contanti = 0;
    testa.assegno = 0;
    testa.bonifico = 0;
    testa.pos = 0;
    testa.droppay = 0;
    testa.bancomat = 0;
    testa.cartadicredito = 0;
    testa.cartadicredito2 = 0;
    testa.cartadicredito3 = 0;
    testa.buoniacquisto = 0;
    testa.buonipasto = 0;
    testa.nonpagato = 0;
    testa.monouso = 0;

    if (comanda.compatibile_xml === true || bypass === true) {
        testa.contanti = testa.totale;
        stampa_scontrino(comanda.tipologia_ricevuta, testa, carta, true);
    } else if ($('#popup_metodo_pagamento_da_pagare:visible').html() !== '0.00' && $('#popup_metodo_pagamento_da_pagare:visible').html() !== null && $('#popup_metodo_pagamento_da_pagare:visible').html() !== undefined) {
        alert("Mancano ancora da pagare " + " € " + $('#popup_metodo_pagamento_da_pagare').html());
    } else
    {

        if ($('#scelta_buoni:visible').length === 1) {

            var buoni_spesa = $('#scelta_buoni [name="scelta_buoni__buono_spesa"]').val();
            if (parseFloat(buoni_spesa) > 0) {
                testa.buoniacquisto = parseFloat(buoni_spesa);
            }

            testa.totale_buoni_pasto = 0;
            testa.numero_buoni = 0;
            testa.nome_buono = '';
            testa.buoni_pasto = '0';
            testa.numero_buoni_pasto = '0';
            testa.nome_buono_2 = '';
            testa.buoni_pasto_2 = '0';
            testa.numero_buoni_pasto_2 = '0';
            testa.nome_buono_3 = '';
            testa.buoni_pasto_3 = '0';
            testa.numero_buoni_pasto_3 = '0';
            testa.nome_buono_4 = '';
            testa.buoni_pasto_4 = '0';
            testa.numero_buoni_pasto_4 = '0';
            testa.nome_buono_5 = '';
            testa.buoni_pasto_5 = '0';
            testa.numero_buoni_pasto_5 = '0';
            var i = 1;
            if (buoni_memorizzati.length >= 1) {
                buoni_memorizzati.forEach(function (val, index) {
                    switch (i) {
                        case 1:

                            testa.nome_buono = val.id_circuito;
                            testa.buoni_pasto = val.importo_taglio;
                            testa.numero_buoni_pasto = val.quantita;
                            //SOLO PER USO INTERNO ALLO SCONTRINO
                            testa.totale_buoni_pasto += parseFloat(val.importo_taglio) * parseInt(val.quantita);
                            testa.numero_buoni += parseInt(val.quantita);
                            break;
                        case 2:
                            testa.nome_buono_2 = val.id_circuito;
                            testa.buoni_pasto_2 = val.importo_taglio;
                            testa.numero_buoni_pasto_2 = val.quantita;
                            //SOLO PER USO INTERNO ALLO SCONTRINO
                            testa.totale_buoni_pasto += parseFloat(val.importo_taglio) * parseInt(val.quantita);
                            testa.numero_buoni += parseInt(val.quantita);
                            break;
                        case 3:
                            testa.nome_buono_3 = val.id_circuito;
                            testa.buoni_pasto_3 = val.importo_taglio;
                            testa.numero_buoni_pasto_3 = val.quantita;
                            //SOLO PER USO INTERNO ALLO SCONTRINO
                            testa.totale_buoni_pasto += parseFloat(val.importo_taglio) * parseInt(val.quantita);
                            testa.numero_buoni += parseInt(val.quantita);
                            break;
                        case 4:
                            testa.nome_buono_4 = val.id_circuito;
                            testa.buoni_pasto_4 = val.importo_taglio;
                            testa.numero_buoni_pasto_4 = val.quantita;
                            //SOLO PER USO INTERNO ALLO SCONTRINO
                            testa.totale_buoni_pasto += parseFloat(val.importo_taglio) * parseInt(val.quantita);
                            testa.numero_buoni += parseInt(val.quantita);
                            break;
                        case 5:
                            testa.nome_buono_5 = val.id_circuito;
                            testa.buoni_pasto_5 = val.importo_taglio;
                            testa.numero_buoni_pasto_5 = val.quantita;
                            //SOLO PER USO INTERNO ALLO SCONTRINO
                            testa.totale_buoni_pasto += parseFloat(val.importo_taglio) * parseInt(val.quantita);
                            testa.numero_buoni += parseInt(val.quantita);
                            break;
                    }
                    i++;
                });
            }

            var totale_conto = parseFloat($(comanda.totale_scontrino).html());
            var restante = parseFloat(totale_conto - testa.totale_buoni_pasto - testa.buoniacquisto).toFixed(2);
            if (restante > 0) {

                var tipo_pagamento__restante = $('.scelta_buoni__tipo_pagamento_restante option:selected').val();
                if (tipo_pagamento__restante !== null) {

                    switch (tipo_pagamento__restante) {
                        case "CONTANTI":
                            var resto = $('#popup_metodo_pagamento_resto').html();
                            if (resto === undefined) {
                                resto = "0";
                            }
                            testa.contanti = restante - parseFloat(resto);
                            break;
                        case "POS":
                            testa.pos = restante;
                            break;
                        case "ASSEGNO":
                            testa.assegno = restante;
                            break;
                        case "BONIFICO":
                            testa.bonifico = restante;
                            break;
                        case "DROPPAY":
                            testa.droppay = restante;
                            break;
                        case "BANCOMAT":
                            testa.bancomat = restante;
                            break;
                        case "CARTA DI CREDITO":
                            testa.cartadicredito = restante;
                            break;
                        case "CARTA DI CREDITO 2":
                            testa.cartadicredito2 = restante;
                            break;
                        case "CARTA DI CREDITO 3":
                            testa.cartadicredito3 = restante;
                            break;
                    }

                } else
                {
                    alert("Devi scegliere un metodo di pagamento per l'importo restante.");
                    return false;
                }
            }

            console.log("DATI TESTA TEMPORANEI", testa);
            $('#scelta_metodo_pagamento').modal('hide');
            $('#scelta_buoni').modal('hide');
            stampa_scontrino(comanda.tipologia_ricevuta, testa, undefined, true);
        } else if ($('#popup_nonpagato:visible').length === 1) {

            $("#popup_nonpagato input").each(function (i, e) {
                $(e).val($(e).val().replace(/\'/gi, " "));
            });

            /*var ragione_sociale = $('#popup_nonpagato [name="ragione_sociale"]').val();
             var indirizzo = $('#popup_nonpagato [name="indirizzo"]').val();
             var numero = $('#popup_nonpagato [name="numero"]').val();
             var cap = $('#popup_nonpagato [name="cap"]').val();
             var citta = $('#popup_nonpagato [name="citta"]').val();
             var provincia = $('#popup_nonpagato [name="provincia"]').val();
             var paese = $('#popup_nonpagato [name="paese"]').val();
             var cellulare = $('#popup_nonpagato [name="cellulare"]').val();
             var telefono = $('#popup_nonpagato [name="telefono"]').val();
             var fax = $('#popup_nonpagato [name="fax"]').val();
             var email = $('#popup_nonpagato [name="email"]').val();
             var codice_fiscale = $('#popup_nonpagato [name="codice_fiscale"]').val();*/
            var partita_iva = $('#popup_nonpagato [name="partita_iva"]').val();
            /*var partita_iva_estera = $('#popup_nonpagato [name="partita_iva_estera"]').val();
             var formato_trasmissione = $('#popup_nonpagato [name="formato_trasmissione"]').prop("checked");
             var codice_destinatario = $('#popup_nonpagato [name="codice_destinatario"]').val();
             var regime_fiscale = $('#popup_nonpagato [name="regime_fiscale"]').val();
             var pec = $('#popup_nonpagato [name="pec"]').val();
             var comune = $('#popup_nonpagato [name="comune"]').val();
             var nazione = $('#popup_nonpagato [name="nazione"]').val();
             //-----
             
             var tendina = $('#popup_nonpagato_elenco_clienti option:selected').val();*/


            if (await controlla_esistenza_piva_nonpagato() === true) {
                var query_id = "select id,fatt_fine_mese_sn from clienti where partita_iva='" + partita_iva + "';";
                var r = alasql(query_id);
                testa.id_cliente = r[0].id;
                if (r[0].fatt_fine_mese_sn === 'true') {
                    testa.nonpagato = $(comanda.totale_scontrino).html();
                    console.log("DATI TESTA TEMPORANEI", testa);
                    $('#scelta_metodo_pagamento').modal('hide');
                    stampa_scontrino(comanda.tipologia_ricevuta, testa, undefined, true);
                    $('#popup_nonpagato').modal('hide');
                } else {
                    bootbox.alert("Il cliente non &egrave; abilitato per la fatturazione a fine mese.");
                }
            } else {
                bootbox.alert("Avviso: i clienti nuovi con le fatture a fine mese devono essere abilitati dalla Gestione Clienti");
            }



        } else if ($('#scelta_metodo_pagamento:visible').length === 1) {

            var premessa = new Promise(function (resolve, reject) {

                if (comanda.tipologia_ricevuta === "FATTURA") {

                    var ragione_sociale = $('#fattura [name="ragione_sociale"]').val().toUpperCase();
                    var indirizzo = $('#fattura [name="indirizzo"]').val().toUpperCase();
                    var numero = $('#fattura [name="numero"]').val().toUpperCase();
                    var cap = $('#fattura [name="cap"]').val();
                    var citta = $('#fattura [name="citta"]').val();
                    var provincia = $('#fattura [name="provincia"]').val().toUpperCase();
                    var paese = $('#fattura [name="paese"]').val();
                    var cellulare = $('#fattura [name="cellulare"]').val();
                    var telefono = $('#fattura [name="telefono"]').val();
                    var fax = $('#fattura [name="fax"]').val();
                    var email = $('#fattura [name="email"]').val();
                    var codice_fiscale = $('#fattura [name="codice_fiscale"]').val().toUpperCase();
                    var partita_iva = $('#fattura [name="partita_iva"]').val();
                    var partita_iva_estera = $('#fattura [name="partita_iva_estera"]').val();
                    var formato_trasmissione = $('#fattura [name="formato_trasmissione"]').prop("checked");
                    var split_payment = $('#fattura [name="split_payment"]').prop("checked");
                    var codice_destinatario = $('#fattura [name="codice_destinatario"]').val().toUpperCase();
                    var regime_fiscale = $('#fattura [name="regime_fiscale"]').val();
                    var pec = $('#fattura [name="pec"]').val();
                    var comune = $('#fattura [name="comune"]').val().toUpperCase();
                    var nazione = $('#fattura [name="nazione"] option:selected').val();
                    var azienda_privato = $('[name="azienda_privato"]:checked').attr("id");
                    var cliente_selezionato = $("#fattura_elenco_clienti option:selected").val();

                    var query_controllo = "";
                    if (cliente_selezionato !== undefined && cliente_selezionato.length > 0) {
                        query_controllo = "SELECT id FROM clienti WHERE id=" + parseInt(cliente_selezionato) + " LIMIT 1;";
                    } else if (partita_iva.trim().length > 0) {
                        query_controllo = "SELECT id FROM clienti WHERE partita_iva='" + partita_iva + "' LIMIT 1;";
                    } else if (partita_iva_estera.trim().length > 0) {
                        query_controllo = "SELECT id FROM clienti WHERE partita_iva_estera='" + partita_iva_estera + "' LIMIT 1;";
                    } else if (codice_fiscale.trim().length > 0) {
                        query_controllo = "SELECT id FROM clienti WHERE codice_fiscale='" + codice_fiscale + "' LIMIT 1;";
                    } else {
                        query_controllo = "SELECT id FROM clienti WHERE  ragione_sociale='" + ragione_sociale + "' LIMIT 1;";
                    }


                    //-----

                    var tendina = $('#fattura_elenco_clienti option:selected').val();

                    if ((tendina === undefined || tendina.length < 1) && ragione_sociale.length > 2) {


                        comanda.sincro.query(query_controllo, function (result_clienti_corrispondenti) {

                            if (result_clienti_corrispondenti[0] !== undefined && result_clienti_corrispondenti[0].id !== undefined) {

                                resolve(result_clienti_corrispondenti[0].id);
                            } else {

                                var testo_query = "SELECT id FROM clienti ORDER BY cast(id as int) DESC LIMIT 1;";
                                comanda.sincro.query(testo_query, function (result) {
                                    var id = 0;
                                    if (result[0] !== undefined)
                                    {
                                        id = parseInt(result[0].id) + 1;
                                    }
                                    testo_query = "INSERT INTO clienti (tipo_cliente,split_payment,formato_trasmissione,codice_destinatario,regime_fiscale,pec,comune,id_nazione,nazione,id,ragione_sociale,indirizzo,numero,cap,citta,provincia,paese,cellulare,telefono,fax,email,codice_fiscale,partita_iva,iva_non_pagati,partita_iva_estera) VALUES ('" + azienda_privato + "','" + split_payment + "','" + formato_trasmissione + "','" + codice_destinatario + "','" + regime_fiscale + "','" + pec + "','" + comune + "','" + nazione + "','" + nazione + "'," + id + ",'" + ragione_sociale.toUpperCase() + "','" + indirizzo + "','" + numero + "','" + cap + "','" + citta + "','" + provincia + "','" + paese + "','" + cellulare + "','" + telefono + "','" + fax + "','" + email + "','" + codice_fiscale + "','" + partita_iva + "','10','" + partita_iva_estera + "')";
                                    comanda.sock.send({tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                                    comanda.sincro.query(testo_query, function () {
                                        resolve(id);
                                    });
                                });
                            }
                        });
                    } else if ((tendina === undefined || tendina.length < 1) && ragione_sociale.length < 2 /*&& ((comanda.partita_iva_estera === true && partita_iva_estera.length > 0) || (partita_iva.length === 11 || partita_iva.length === 16))*/) {



                        var testo_query = "SELECT * FROM clienti WHERE partita_iva='" + partita_iva + "' ORDER BY ragione_sociale DESC LIMIT 1;";
                        comanda.sincro.query(testo_query, function (result_clienti_corrispondenti) {

                            if (result_clienti_corrispondenti[0].id) {

                                resolve(result_clienti_corrispondenti[0].id);
                            } else {

                                testo_query = "SELECT id FROM clienti ORDER BY cast(id as int) DESC LIMIT 1;";
                                comanda.sincro.query(testo_query, function (result) {
                                    var id = 0;
                                    if (result[0] !== undefined)
                                    {
                                        id = parseInt(result[0].id) + 1;
                                    }
                                    testo_query = "INSERT INTO clienti (tipo_cliente,split_payment,formato_trasmissione,codice_destinatario,regime_fiscale,pec,comune,id_nazione,nazione,id,ragione_sociale,indirizzo,numero,cap,citta,provincia,paese,cellulare,telefono,fax,email,codice_fiscale,partita_iva,iva_non_pagati,partita_iva_estera) VALUES ('" + azienda_privato + "','" + split_payment + "','" + formato_trasmissione + "','" + codice_destinatario + "','" + regime_fiscale + "','" + pec + "','" + comune + "','" + id_nazione + "','" + nazione + "'," + id + ",'" + partita_iva + "','" + indirizzo + "','" + numero + "','" + cap + "','" + citta + "','" + provincia + "','" + paese + "','" + cellulare + "','" + telefono + "','" + fax + "','" + email + "','" + codice_fiscale + "','" + partita_iva + "','10','" + partita_iva_estera + "')";
                                    comanda.sock.send({tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                                    comanda.sincro.query(testo_query, function () {
                                        resolve(id);
                                    });
                                });
                            }
                        });
                    } else if (tendina !== "" && ragione_sociale.length > 2 /*&& ((comanda.partita_iva_estera === true && partita_iva_estera.length > 0) || (partita_iva.length === 11 || partita_iva.length === 16))*/) {
                        resolve(tendina);
                    } else
                    {
                        resolve('N');
                    }

                } else if (comanda.tipologia_ricevuta.indexOf("SCONTRINO") !== -1)
                {
                    resolve('N');
                }

            });
            premessa.then(function (id_cliente) {

                testa.id_cliente = id_cliente;
                var totale_progressivo = 0;
                var contanti = $('#popup_metodo_pagamento_importo_contanti').val();
                if (parseFloat(contanti) > 0 && totale_progressivo < testa.totale) {
                    var resto = $('#popup_metodo_pagamento_resto').html();
                    testa.contanti = parseFloat(contanti) - parseFloat(resto);
                    totale_progressivo += parseFloat(contanti);
                } else if (parseFloat(contanti) > 0)
                {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai gi&agrave;  raggiunto o superato il totale.");
                    return false;
                }

                var pos = $('#popup_metodo_pagamento_importo_pos').val();
                if (parseFloat(pos) > 0 && totale_progressivo < testa.totale) {
                    testa.pos = pos;
                    totale_progressivo += parseFloat(pos);
                } else if (parseFloat(pos) > 0)
                {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai giÃ Â  raggiunto o superato il totale.");
                    return false;
                }

                var assegno = $('#popup_metodo_pagamento_importo_assegno').val();
                if (parseFloat(assegno) > 0 && totale_progressivo < testa.totale) {
                    testa.assegno = parseFloat(assegno);
                    totale_progressivo += parseFloat(assegno);
                } else if (parseFloat(assegno) > 0)
                {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai gi&agrave;  raggiunto o superato il totale.");
                    return false;
                }

                var bonifico = $('#popup_metodo_pagamento_importo_bonifico').val();
                if (parseFloat(bonifico) > 0 && totale_progressivo < testa.totale) {
                    testa.bonifico = parseFloat(bonifico);
                    totale_progressivo += parseFloat(bonifico);
                } else if (parseFloat(bonifico) > 0)
                {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai gi&agrave;  raggiunto o superato il totale.");
                    return false;
                }

                var droppay = $('#popup_metodo_pagamento_importo_droppay').val();
                if (parseFloat(droppay) > 0 && totale_progressivo < testa.totale) {
                    testa.droppay = droppay;
                    totale_progressivo += parseFloat(droppay);
                } else if (parseFloat(droppay) > 0)
                {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai giÃ Â  raggiunto o superato il totale.");
                    return false;
                }

                var bancomat = $('#popup_metodo_pagamento_importo_bancomat').val();
                if (parseFloat(bancomat) > 0 && totale_progressivo < testa.totale) {
                    testa.bancomat = bancomat;
                    totale_progressivo += parseFloat(bancomat);
                } else if (parseFloat(bancomat) > 0)
                {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai giÃ Â  raggiunto o superato il totale.");
                    return false;
                }

                var cartadicredito = $('#popup_metodo_pagamento_importo_cc').val();
                if (parseFloat(cartadicredito) > 0 && totale_progressivo < testa.totale) {
                    testa.cartadicredito = cartadicredito;
                    totale_progressivo += parseFloat(cartadicredito);
                } else if (parseFloat(cartadicredito) > 0)
                {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai giÃ Â  raggiunto o superato il totale.");
                    return false;
                }

                var cartadicredito2 = $('#popup_metodo_pagamento_importo_cc2').val();
                if (parseFloat(cartadicredito2) > 0 && totale_progressivo < testa.totale) {
                    testa.cartadicredito2 = cartadicredito2;
                    totale_progressivo += parseFloat(cartadicredito2);
                } else if (parseFloat(cartadicredito2) > 0)
                {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai giÃ Â  raggiunto o superato il totale.");
                    return false;
                }

                var cartadicredito3 = $('#popup_metodo_pagamento_importo_cc3').val();
                if (parseFloat(cartadicredito3) > 0 && totale_progressivo < testa.totale) {
                    testa.cartadicredito3 = cartadicredito3;
                    totale_progressivo += parseFloat(cartadicredito3);
                } else if (parseFloat(cartadicredito3) > 0)
                {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai giÃ Â  raggiunto o superato il totale.");
                    return false;
                }

                var monouso = $('#popup_metodo_pagamento_importo_monouso').val();
                if (parseFloat(monouso) > 0 && totale_progressivo < testa.totale) {
                    testa.monouso = monouso;
                    totale_progressivo += parseFloat(monouso);
                } else if (parseFloat(monouso) > 0)
                {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai giÃ Â  raggiunto o superato il totale.");
                    return false;
                }

                var buoni_acquisto = $('#popup_metodo_pagamento_importo_buoni_acquisto').val();
                if (parseFloat(buoni_acquisto) > 0 && totale_progressivo < testa.totale) {
                    testa.buoniacquisto = buoni_acquisto;
                    totale_progressivo += parseFloat(buoni_acquisto);
                } else if (parseFloat(buoni_acquisto) > 0)
                {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai giÃ Â  raggiunto o superato il totale.");
                    return false;
                }

                console.log("DATI TESTA TEMPORANEI", testa);
                $('#scelta_metodo_pagamento').modal('hide');
                stampa_scontrino(comanda.tipologia_ricevuta, testa, undefined, true);
            });
        }
    }
}