    function elimina_tavolo(tavolo) {
        tavolo.remove();
    }

    function nuovo_tavolo() {
        var errori = false;
        var numero = $('#numero_tavolo').val();
        if (numero.length < 1) {
            alert("Devi assegnare obbligatoriamente un numero o un nome al tavolo.");
            errori = true;
        }

        $('div.tavolo.rett,div.tavolo.cerc').each(function () {

            if ($(this).find('h4').html() === numero) {
                alert("Errore. Il tavolo con quel numero già esiste");
                errori = true;
            }
        });
        var forma = $('.forma_selezionata').attr('class');
        forma = forma.replace('forma_selezionata', '');
        forma = forma.replace('-min', '');
        console.log(forma);
        //forma = forma.slice(0, -4);


        if (forma === undefined) {
            alert("Devi scegliere obbligatoriamente una forma per il tavolo.");
        }

        if (errori === false)
        {

            var top = ($('#contenitore_tavoli').offset().top) + 20;
            var left = ($('#contenitore_tavoli').offset().left) + 20;
            //console.log(top, left);
            $('#contenitore_tavoli').append('<div style="top:' + top + 'px;left:' + left + 'px;" class="' + forma + '" ><h4></h4><h6><b>' + numero + '</b><br/>hh:mm<br/>hh:mm</h6></div>');
            make_draggable();
            deletor();


            $('#numero_tavolo').val('');
        }

    }

    function make_draggable() {
        $('div.tavolo.rett,div.tavolo.cerc').draggable({containment: "parent"});
        $('div.tavolo.rett,div.tavolo.cerc').resizable();
    }

    function cancella_tutti_tavoli() {
        confirm("Sei sicuro di voler cancellare tutti i tavoli? \nNB La cancellazione non è irrimediabile finchè non premi 'Salva tavoli', e può essere revocata aggiornando la pagina.");
    }

    function disegna_tavoli_salvati() {
        $('.tavolo.rett,.tavolo.cerc').remove();

        comanda.sincro.query("select * from tavoli", function (result) {
            result.forEach(function (obj) {

                if (obj.ora_apertura_tavolo === 'null') {
                    obj.ora_apertura_tavolo = '-';
                }
                if (obj.ora_ultima_comanda === 'null') {
                    obj.ora_ultima_comanda = '-';
                }


                var colore;

                if (obj.ora_apertura_tavolo !== 0 && obj.ora_apertura_tavolo !== null && obj.ora_apertura_tavolo !== 'null' && obj.ora_apertura_tavolo !== '' && obj.ora_apertura_tavolo !== '-')
                {
                    colore = 'background-color:red;';
                }
                else
                {
                    colore = '';
                }
                $('#contenitore_tavoli').prepend('<div style="' + colore + 'width:' + obj.larghezza + 'px; height:' + obj.altezza + 'px;top:' + obj.pos_y + 'px;left:' + obj.pos_x + 'px;" class="' + obj.class + '" ><h4></h4><h6><b>' + obj.numero + '</b><br/>' + obj.ora_apertura_tavolo + '<br/>' + obj.ora_ultima_comanda + '</h6></div>');

                //console.log('<div style="width:' + obj.larghezza + 'px; height:' + obj.altezza + 'px;top:' + obj.pos_y + 'px;left:' + obj.pos_x + 'px;" class="' + obj.class + '" ><h4></h4><h6><b>' + obj.numero + '</b><br/>hh:mm<br/>hh:mm</h6></div>');


            });
            if (comanda.permesso !== 0) {
                make_draggable();
                deletor();
            }
            else
            {
                selettore_tavolo();
            }
        });
    }

    function deletor() {
        $('div.tavolo.rett,div.tavolo.cerc').mousedown(function () {
            var time = new Date().getTime();
            var left = $(this).offset().left;
            var top = $(this).offset().top;
            var width = $(this).width();
            var height = $(this).height();
            $(this).mouseup(function () {
                var time2 = new Date().getTime();
                var left2 = $(this).offset().left;
                var top2 = $(this).offset().top;
                var width2 = $(this).width();
                var height2 = $(this).height();

                if ((time2 - time) > 300 &&
                        Math.abs(left2 - left) <= 1 &&
                        Math.abs(top2 - top) <= 1 &&
                        Math.abs(width2 - width) <= 1 &&
                        Math.abs(height2 - height) <= 1)

                {

                    elimina_tavolo($(this));
                }


            });
        });
    }

    function posizione_tavoli() {

        comanda.sincro.query("delete from tavoli", function () {
        });


        $('#posizione_tavoli').html('');
        $('div.tavolo.rett,div.tavolo.cerc').each(function () {
            var numero = $(this).find('h6>b').html();
            var x = $(this).offset().left;
            var y = $(this).offset().top;
            var w = $(this).width();
            var h = $(this).height();
            var classe = $(this).attr('class');

            //$('#posizione_tavoli').append('<br/><strong>Numero tavolo: ' + numero + '<br/>Posizione:</strong><br>Da sinistra: ' + x + '<br/>Da sopra: ' + y + '<br/><strong>Grandezza:</strong><br>Larghezza: ' + w + '<br/>Altezza: ' + h + '<br/><br/>');

            //posiziona nel db

            comanda.sincro.query("insert into tavoli (class,numero,abilitato,altezza,larghezza,pos_x,pos_y,colore) VALUES ('" + classe + "','" + numero + "','1','" + h + "','" + w + "','" + x + "','" + y + "','grey');", function () {
            });

        });

        comanda.sincro.sincronizzazione_upload('tavoli');

    }

    $(document).keydown(function (e) {
        //console.log(e.keyCode);

        switch (e.keyCode) {
            case 13: // invio
                e.preventDefault();
                $('#nuovo_tavolo').trigger('click');
                break;
            case 120:// F9 rettangolo
                e.preventDefault();
                $('.tavolo.rett-min').trigger('click');
                break;
            case 121: // F10 cerchio
                e.preventDefault();
                $('.tavolo.cerc-min').trigger('click');
                break;
        }

        $('#numero_tavolo').focus();
    });

    function selettore_tavolo() {
        $('div.tavolo.rett,div.tavolo.cerc').click(function () {

            var testo_query;
            var date = new Date();
            var ora = addZero(date.getHours(), 2) + ':' + addZero(date.getMinutes(), 2);

            var tavolo = $(this).find('h6>b').html();
            console.log("Tavolo " + tavolo);

            testo_query = 'select occupato from tavoli where numero="' + tavolo + '"';
            comanda.sincro.query(testo_query, function (result) {

                if (result[0].occupato === "0")
                {
                    console.log(tavolo);
                    if (tavolo !== 0 && tavolo !== null && tavolo !== undefined)
                    {
                        testo_query = "update tavoli set occupato='0' where numero='" + comanda.tavolo + "';";

                        comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query,terminale:comanda.terminale,ip:comanda.ip_address});

                        comanda.sincro.query(testo_query, function () {
                        });
                    }

                    testo_query = "update tavoli set occupato='1' where numero='" + tavolo + "'";
                    console.log(testo_query);

                    comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query,terminale:comanda.terminale,ip:comanda.ip_address});
                    console.log("sock teoricamente inviato");


                    comanda.sincro.query(testo_query, function () {
                    });
                    comanda.tavolo = tavolo;

                    //riempie il conto con il tavolo giusto
                    comanda.funzionidb.conto_attivo();

                    selezione_operatore("COMANDA");
                    //var testo_query = "update tavoli set ora_apertura_tavolo='" + ora + "' where numero='" + tavolo + "' ";
                    //comanda.sincro.query(testo_query, function () {
                    //});
                }
                else
                {
                    alert("Il tavolo è già occupato!");
                }
            });
        });
    }


    $(document).ready(function () {

        disegna_tavoli_salvati();


        //Per prova
        if (comanda.permesso !== 0)
        {

            //per una maggiore compatibilità
            $('.ajax>[class^=col-md-]').each(function () {

                var classe = $(this).attr('class').substr(0, 11);
                console.log(classe);
                classe = classe.replace('md', 'xs');
                console.log(classe);
                $(this).addClass(classe);
            });
            $('div.tavolo.rett-min,div.tavolo.cerc-min').click(function () {

                $('div.tavolo.rett-min,div.tavolo.cerc-min').each(function () {
                    $(this).removeClass('forma_selezionata');
                });
                $(this).addClass('forma_selezionata');
            });
            $('#rileva_posizione_grandezza_tavoli').click(function () {
                posizione_tavoli();
            });
            $('#nuovo_tavolo').click(function () {
                nuovo_tavolo();
            });


            make_draggable();

            deletor();

            $('#amministrazione').show();
            $('#tasti_cameriere').hide();


            $('#numero_tavolo').focus();

        }
        else
        {
            $('#amministrazione').hide();
            $('#tasti_cameriere').show();
        }

    });