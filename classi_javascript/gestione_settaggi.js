/* global comanda, bootbox,  parseFloat, alasql */


function gestisci_pony(event, id) {

    if (event !== undefined && event.target !== undefined && event.target !== null) {
        $(event.target).parent().parent().find("a").removeClass('cat_accesa');
        $(event.target).addClass("cat_accesa");
    }

    $('#form_gestione_clienti').html('');
    var form_gestione_stampanti = '';
    comanda.sincro.query("SELECT * FROM pony_express WHERE id=" + id + " LIMIT 1;", function (result) {

        for (var key in result[0]) {
            console.log("RESULT0", key, result[0], result[0][key]);
            if (result[0][key] === null) {
                result[0][key] = '';
            }
        }

        var tipo = '';
        //Nome(stampante),Lingua(stanza),Ip,Piccola,Intelligent,Fiscale

        form_gestione_stampanti = '<div class="editor_operatori">\n\
                                   <div class="col-md-12 col-xs-12">\n\
                                        <div class="col-md-5 col-xs-5"><label>' + comanda.lang[99] + '</label><input maxlength="20" style="text-transform:uppercase;font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="nome"  value="' + result[0].nome + '"></div>\n\
                                    </div>';
        form_gestione_stampanti += "<div class='col-md-12 col-xs-12'>";
        form_gestione_stampanti += "<div class='col-md-6 col-xs-6'>";
        form_gestione_stampanti += "<div id='data_incassi_partenza'></div>";
        form_gestione_stampanti += "</div>";
        form_gestione_stampanti += "<div class='col-md-6 col-xs-6'>";
        form_gestione_stampanti += "<div id='data_incassi_arrivo'></div>";
        form_gestione_stampanti += "</div>";
        form_gestione_stampanti += "</div>";
        form_gestione_stampanti += '<div class="col-md-12 col-xs-12" style="padding-top:50px"><button class="col-md-4 col-xs-4 btn btn-warning" ' + comanda.evento + '="stampa_consegne_giornata_pony(\'' + id + '\');" style="text-transform:uppercase;">STAMPA CONSEGNE</button><button class="col-md-4 col-xs-4 btn btn-warning" ' + comanda.evento + '="vedi_consegne_giornata_pony(\'' + id + '\');" style="text-transform:uppercase;">VEDI CONSEGNE</button><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_pony(\'' + result[0].id + '\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button><button class="pull-right col-md-2 col-xs-2 btn btn-danger" ' + comanda.evento + '="elimina_pony(\'' + result[0].id + '\',\'' + tipo + '\');" style="text-transform:uppercase;">' + comanda.lang[179] + '</button></div>';
        let resultt = "SELECT * FROM settaggi_profili  where id=" + comanda.folder_number + "  LIMIT 1;"
        let test = alasql(resultt);
        if (test[0].Field508 === "true") {
            form_gestione_stampanti += '<div  class="col-md-12 col-xs-12"><button class="col-md-4 col-xs-4 btn btn-warning" ' + comanda.evento + '="scarica_qr_pony(\'' + id + '\');" style="text-transform:uppercase;">SCARICA QR</button></div>';
        }

        form_gestione_stampanti += '</div>';
        $('#form_gestione_pony').html(form_gestione_stampanti);
        $("#data_incassi_partenza:visible").multiDatesPicker({
            maxPicks: 1,
            dateFormat: 'yy-mm-dd'
        });
        $("#data_incassi_arrivo:visible").multiDatesPicker({
            maxPicks: 1,
            dateFormat: 'yy-mm-dd'
        });
    });
}

function gestisci_rtengine(event, id) {

    if (event !== undefined && event.target !== undefined && event.target !== null) {
        $(event.target).parent().parent().find("a").removeClass('cat_accesa');
        $(event.target).addClass("cat_accesa");
    }

    $('#form_gestione_clienti').html('');
    var form_gestione_rtengine = '';
    comanda.sincro.query("SELECT * FROM socket_listeners WHERE id='" + id + "' LIMIT 1;", function (result) {

        for (var key in result[0]) {
            console.log("RESULT0", key, result[0], result[0][key]);
            if (result[0][key] === null) {
                result[0][key] = '';
            }
        }

        //Nome(stampante),Lingua(stanza),Ip,Piccola,Intelligent,Fiscale

        form_gestione_rtengine = '<div class="editor_rtengine">\n\
                                   <div class="col-md-12 col-xs-12">\n\
                                        <div class="col-md-12 col-xs-12"><label>IP</label><input maxlength="20" style="text-transform:uppercase;font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="ip"  value="' + result[0].ip + '"></div>\n\
                                   </div>\n\
                                   <div class="col-md-12 col-xs-12"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_rtengine(\'' + result[0].id + '\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                   </div>';
        $('#form_gestione_rtengine').html(form_gestione_rtengine);
    });
}

function stampa_anagrafica_prodotti(solo_categoria_corrente) {
    var totale_generale = 0;
    var sett = alasql("SELECT Field330,Field331,Field332,Field333,Field334,Field335,Field336,Field337,Field338,Field339,Field340,Field341,Field342,Field343,Field344,Field345,Field346,Field347,Field348,Field349,Field350,Field351,Field352 FROM settaggi_profili;");
    sett = sett[0];
    var where = "";
    if (solo_categoria_corrente === true) {
        where += " WHERE categoria='" + comanda.categoria + "' ";
    }

    var result = alasql("SELECT * FROM prodotti " + where + " ORDER BY descrizione ASC;");
    //INTESTAZIONE AZIENDA - SOLO PRIMO FOGLIO

    var pagina = '<div style="padding:5px;border:1px solid grey; overflow: auto;">';
    pagina += '<div style="float:left;"><strong>' + comanda.profilo_aziendale.ragione_sociale + '</strong></div>';
    pagina += '<div style="float:right;"><strong>' + comanda.locale + '</strong></div>';
    var data = new Date().format('dd/mm/yyyy');
    var ora = new Date().format('HH:MM');
    pagina += '<br/>' + '<div style="float:left;">Stampa effettuata il: <strong>' + data + '</strong></div>';
    pagina += '<div style="float:right;">' + 'Alle ore: <strong>' + ora + '</strong></div>';
    pagina += '</div>';
    pagina += '<div class="text-center" style="padding-bottom:1vh;padding-top:1vh">';
    pagina += '<span class="text-center" style="font-size:1.8vh;text-decoration:underline;">STAMPA ANAGRAFICA PRODOTTI</span>';
    pagina += '</div>';
    //SUDDIVISIONE ARTICOLI PER CATEGORIA

    var categoria = new Object();
    var categoria_varianti = new Object();
    result.forEach(function (el) {
        var nome_categoria = el.categoria;
        if (el.categoria !== "XXX") {

            if (comanda.nome_categoria[el.categoria] !== undefined) {
                nome_categoria = comanda.nome_categoria[el.categoria];
            }

            if (nome_categoria.substr(0, 2) === "V.") {
                if (categoria_varianti[nome_categoria] === undefined) {
                    categoria_varianti[nome_categoria] = new Array();
                }
                categoria_varianti[nome_categoria].push(el);
            } else {
                if (categoria[nome_categoria] === undefined) {
                    categoria[nome_categoria] = new Array();
                }
                categoria[nome_categoria].push(el);
            }

        }

    });
    //RIPESCAGGIO DATI PER OGNI CATEGORIA CON CREAZIONE LAYOUT

    for (var desc of Object.keys(categoria).sort()) {

        pagina += '<div class="text-center">';
        pagina += '<h4 style="font-weight:strong;">' + desc + '</h4>';
        pagina += '</div>';
        pagina += '<div style="padding-bottom:0.8vh"><table class="stampa_tabella_anagrafica_prodotti" style="width:100%;">';
        //RIGA CON TITOLETTI
        pagina += '<tr  style="font-size:1vh;font-weight:strong;text-transform:uppercase;">';
        if (sett.Field330 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'Descrizione';
            pagina += '</th>';
        }

        if (sett.Field331 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'Spazio Forno';
            pagina += '</th>';
        }

        if (sett.Field332 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'Pr. Normale';
            pagina += '</th>';
        }

        if (sett.Field333 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'Pr. Maxi';
            pagina += '</th>';
        }

        if (sett.Field334 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'Pr. Bar';
            pagina += '</th>';
        }

        if (sett.Field335 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'Pr. Asporto';
            pagina += '</th>';
        }

        if (sett.Field336 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'Pr. Continuo';
            pagina += '</th>';
        }

        if (sett.Field337 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'Pr. 1/4 Metro';
            pagina += '</th>';
        }

        if (sett.Field338 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'Unit&agrave; Misura';
            pagina += '</th>';
        }

        if (sett.Field339 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'Pr. a Unit&agrave;';
            pagina += '</th>';
        }

        if (sett.Field340 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'Dest. Stampa';
            pagina += '</th>';
        }

        if (sett.Field341 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'Dest. Stampa 2';
            pagina += '</th>';
        }

        if (sett.Field342 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'Portata';
            pagina += '</th>';
        }

        if (sett.Field343 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'Cat. Varianti';
            pagina += '</th>';
        }

        if (sett.Field344 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'Var. Automatica';
            pagina += '</th>';
        }

        if (sett.Field345 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'IVA Base';
            pagina += '</th>';
        }

        if (sett.Field346 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'IVA T.A.';
            pagina += '</th>';
        }

        if (sett.Field347 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'Riepilogo';
            pagina += '</th>';
        }

        if (sett.Field348 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'Cod. Promo';
            pagina += '</th>';
        }


        pagina += '</tr>';
        categoria[desc].forEach(function (el) {
            pagina += '<tr style="font-size:1vh;">';
            if (sett.Field330 === "true") {
                pagina += '<td>';
                pagina += el.descrizione;
                pagina += '</td>';
            }

            if (sett.Field331 === "true") {
                pagina += '<td class="text-center" style="width:6.5vh;">';
                pagina += el.spazio_forno;
                pagina += '</td>';
            }

            if (sett.Field332 === "true") {
                pagina += '<td class="text-right" style="width:7.5vh;padding-right:5px">';
                var prezzo = "";
                if (el.prezzo_1.length > 0) {
                    prezzo = '&euro; ' + parseFloat(el.prezzo_1).toFixed(2);
                }
                pagina += prezzo;
                pagina += '</td>';
            }

            if (sett.Field333 === "true") {
                pagina += '<td class="text-right" style="width:7.5vh;padding-right:5px">';
                var prezzo = "";
                if (el.prezzo_maxi.length > 0) {
                    prezzo = '&euro; ' + parseFloat(el.prezzo_maxi).toFixed(2);
                }
                pagina += prezzo;
                pagina += '</td>';
            }

            if (sett.Field334 === "true") {
                pagina += '<td class="text-right" style="width:7.5vh;padding-right:5px">';
                var prezzo = "";
                if (el.listino_bar.length > 0) {
                    prezzo = '&euro; ' + parseFloat(el.listino_bar).toFixed(2);
                }
                pagina += prezzo;
                pagina += '</td>';
            }

            if (sett.Field335 === "true") {
                pagina += '<td class="text-right" style="width:7.5vh;padding-right:5px">';
                var prezzo = "";
                if (el.listino_asporto.length > 0) {
                    prezzo = '&euro; ' + parseFloat(el.listino_asporto).toFixed(2);
                }
                pagina += '</td>';
            }

            if (sett.Field336 === "true") {
                pagina += '<td class="text-right" style="width:7.5vh;padding-right:5px">';
                var prezzo = "";
                if (el.listino_continuo.length > 0) {
                    prezzo = '&euro; ' + parseFloat(el.listino_continuo).toFixed(2);
                }
                pagina += '</td>';
            }

            if (sett.Field337 === "true") {
                pagina += '<td class="text-right" style="width:7.5vh;padding-right:5px">';
                var prezzo = "";
                if (el.prezzo_2.length > 0) {
                    prezzo = '&euro; ' + parseFloat(el.prezzo_2).toFixed(2);
                }
                pagina += '</td>';
            }

            if (sett.Field338 === "true") {
                pagina += '<td  class="text-center" style="width:7.5vh;">';
                var peso_ums = "";
                if (el.peso_ums !== undefined && el.peso_ums !== "undefined" && el.prezzo_3 !== undefined && el.prezzo_3 !== "undefined" && el.prezzo_3.length > 0) {
                    peso_ums = el.peso_ums;
                }
                pagina += peso_ums;
                pagina += '</td>';
            }

            if (sett.Field339 === "true") {
                pagina += '<td class="text-right" style="width:7.5vh;padding-right:5px">';
                var prezzo = "";
                if (el.prezzo_3 !== undefined && el.prezzo_3 !== "undefined" && el.prezzo_3.length > 0) {
                    prezzo = '&euro; ' + parseFloat(el.prezzo_3).toFixed(2);
                }
                pagina += prezzo;
                pagina += '</td>';
            }

            if (sett.Field340 === "true") {

                var nome_stampante = "";
                if (comanda.nome_stampante[el.dest_st_1] !== undefined) {
                    nome_stampante = comanda.nome_stampante[el.dest_st_1].nome_umano;
                }

                pagina += '<td class="text-center" style="width:8vh;">';
                pagina += nome_stampante;
                pagina += '</td>';
            }

            if (sett.Field341 === "true") {
                var nome_stampante = "";
                if (comanda.nome_stampante[el.dest_st_2] !== undefined) {
                    nome_stampante = comanda.nome_stampante[el.dest_st_2].nome_umano;
                }

                pagina += '<td  class="text-center" style="width:8vh;">';
                pagina += nome_stampante;
                pagina += '</td>';
            }


            if (sett.Field342 === "true") {
                var nome_portata = "";
                if (comanda.nome_portata[el.portata] !== undefined) {
                    nome_portata = comanda.nome_portata[el.portata];
                }

                pagina += '<td  class="text-center" style="width:7.5vh;">';
                pagina += nome_portata;
                pagina += '</td>';
            }

            if (sett.Field343 === "true") {

                var nome_categoria = "";
                if (comanda.nome_categoria[el.cat_varianti.split("/")[0]] !== undefined) {
                    nome_categoria = comanda.nome_categoria[el.cat_varianti.split("/")[0]];
                }

                if (nome_categoria === "NESSUNA") {
                    nome_categoria = "";
                }
                pagina += '<td class="text-center" >';
                pagina += nome_categoria;
                pagina += '</td>';
            }

            if (sett.Field344 === "true") {

                var nome_categoria = "";
                if (el.cat_varianti.split("/")[1] !== undefined && comanda.nome_categoria[el.cat_varianti.split("/")[1]] !== undefined) {
                    nome_categoria = comanda.nome_categoria[el.cat_varianti.split("/")[1]];
                }
                if (nome_categoria === "NESSUNA") {
                    nome_categoria = "";
                }

                pagina += '<td class="text-center" >';
                pagina += nome_categoria;
                pagina += '</td>';
            }

            if (sett.Field345 === "true") {
                pagina += '<td  class="text-center" style="width:6.5vh;">';
                pagina += el.perc_iva_base;
                pagina += '</td>';
            }

            if (sett.Field346 === "true") {
                pagina += '<td  class="text-center" style="width:6.5vh;">';
                pagina += el.perc_iva_takeaway;
                pagina += '</td>';
            }

            if (sett.Field347 === "true") {

                var riepilogo = "";
                if (el.riepilogo === "S") {
                    riepilogo = "S";
                }
                pagina += '<td  class="text-center" style="width:7vh;">';
                pagina += riepilogo;
                pagina += '</td>';
            }

            if (sett.Field348 === "true") {
                pagina += '<td  class="text-center" style="width:6.5vh;">';
                var cod_promo = "";
                if (el.cod_promo !== undefined && el.cod_promo !== "undefined") {
                    cod_promo = el.cod_promo;
                }
                pagina += cod_promo;
                pagina += '</td>';
            }
            pagina += '</tr>';
        });
        pagina += '<tr>';
        pagina += '<td style="font-weight:bold;font-size:1vh">';
        pagina += 'TOTALE: ' + categoria[desc].length;;
        pagina += '</td>';
        pagina += '</tr>';
        pagina += '</table></div>';
        totale_generale += categoria[desc].length;
    }





    //VARIANTI

    //RIPESCAGGIO DATI PER OGNI CATEGORIA CON CREAZIONE LAYOUT

    for (var desc of Object.keys(categoria_varianti).sort()) {

        pagina += '<div class="text-center">';
        pagina += '<h4 style="font-weight:strong;">' + desc + '</h4>';
        pagina += '</div>';
        pagina += '<div style="padding-bottom:0.8vh"><table class="stampa_tabella_anagrafica_prodotti" style="width:100%;">';
        //RIGA CON TITOLETTI
        pagina += '<tr  style="font-size:1vh;font-weight:strong;text-transform:uppercase;">';
        if (sett.Field330 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'Descrizione';
            pagina += '</th>';
        }

        /*if (sett.Field331 === "true") {
         pagina += '<th class="text-center">';
         pagina += 'Spazio Forno';
         pagina += '</th>';
         }*/

        if (sett.Field332 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'Pr. Normale';
            pagina += '</th>';
        }

        if (sett.Field333 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'Pr. Maxi';
            pagina += '</th>';
        }

        if (sett.Field334 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'Pr. Bar';
            pagina += '</th>';
        }

        if (sett.Field335 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'Pr. Asporto';
            pagina += '</th>';
        }

        if (sett.Field336 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'Pr. Continuo';
            pagina += '</th>';
        }

        if (sett.Field349 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'Pr. Poco';
            pagina += '</th>';
        }

        if (sett.Field350 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'Pr. Variante Maxi';
            pagina += '</th>';
        }

        if (sett.Field351 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'Pr. Varianti Aggiuntive';
            pagina += '</th>';
        }

        if (sett.Field352 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'Pr. Maxi Aggiuntive';
            pagina += '</th>';
        }

        /*if (sett.Field338 === "true") {
         pagina += '<th class="text-center">';
         pagina += 'Unit&agrave; Misura';
         pagina += '</th>';
         }*/

        /*if (sett.Field339 === "true") {
         pagina += '<th class="text-center">';
         pagina += 'Pr. a Unit&agrave;';
         pagina += '</th>';
         }*/

        /*if (sett.Field340 === "true") {
         pagina += '<th class="text-center">';
         pagina += 'Dest. Stampa';
         pagina += '</th>';
         }*/

        /*if (sett.Field341 === "true") {
         pagina += '<th class="text-center">';
         pagina += 'Dest. Stampa 2';
         pagina += '</th>';
         }*/

        /*if (sett.Field342 === "true") {
         pagina += '<th class="text-center">';
         pagina += 'Portata';
         pagina += '</th>';
         }*/

        /*if (sett.Field343 === "true") {
         pagina += '<th class="text-center">';
         pagina += 'Cat. Varianti';
         pagina += '</th>';
         }*/

        /*if (sett.Field344 === "true") {
         pagina += '<th class="text-center">';
         pagina += 'Var. Automatica';
         pagina += '</th>';
         }*/

        if (sett.Field345 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'IVA Base';
            pagina += '</th>';
        }

        if (sett.Field346 === "true") {
            pagina += '<th class="text-center">';
            pagina += 'IVA T.A.';
            pagina += '</th>';
        }

        /*if (sett.Field347 === "true") {
         pagina += '<th class="text-center">';
         pagina += 'Riepilogo';
         pagina += '</th>';
         }*/

        /*if (sett.Field348 === "true") {
         pagina += '<th class="text-center">';
         pagina += 'Cod. Promo';
         pagina += '</th>';
         }*/


        pagina += '</tr>';
        categoria_varianti[desc].forEach(function (el) {
            pagina += '<tr style="font-size:1vh;">';
            if (sett.Field330 === "true") {
                pagina += '<td>';
                pagina += el.descrizione;
                pagina += '</td>';
            }

            /*if (sett.Field331 === "true") {
             pagina += '<td  class="text-center" style="width:6.5vh;">';
             pagina += el.spazio_forno;
             pagina += '</td>';
             }*/

            if (sett.Field332 === "true") {
                pagina += '<td class="text-right" style="width:7.5vh;padding-right:5px">';
                var prezzo = "";
                if (el.prezzo_1.length > 0) {
                    prezzo = '&euro; ' + parseFloat(el.prezzo_1).toFixed(2);
                }
                pagina += prezzo;
                pagina += '</td>';
            }

            if (sett.Field333 === "true") {
                pagina += '<td class="text-right" style="width:7.5vh;padding-right:5px">';
                var prezzo = "";
                if (el.prezzo_maxi.length > 0) {
                    prezzo = '&euro; ' + parseFloat(el.prezzo_maxi).toFixed(2);
                }
                pagina += prezzo;
                pagina += '</td>';
            }

            if (sett.Field334 === "true") {
                pagina += '<td class="text-right" style="width:7.5vh;padding-right:5px">';
                var prezzo = "";
                if (el.listino_bar.length > 0) {
                    prezzo = '&euro; ' + parseFloat(el.listino_bar).toFixed(2);
                }
                pagina += prezzo;
                pagina += '</td>';
            }

            if (sett.Field335 === "true") {
                pagina += '<td class="text-right" style="width:7.5vh;padding-right:5px">';
                var prezzo = "";
                if (el.listino_asporto.length > 0) {
                    prezzo = '&euro; ' + parseFloat(el.listino_asporto).toFixed(2);
                }
                pagina += prezzo;
                pagina += '</td>';
            }

            if (sett.Field336 === "true") {
                pagina += '<td class="text-right" style="width:7.5vh;padding-right:5px">';
                var prezzo = "";
                if (el.listino_continuo.length > 0) {
                    prezzo = '&euro; ' + parseFloat(el.listino_continuo).toFixed(2);
                }
                pagina += prezzo;
                pagina += '</td>';
            }

            if (sett.Field349 === "true") {
                pagina += '<td class="text-right" style="width:7.5vh;padding-right:5px">';
                var prezzo = "";
                if (el.prezzo_4.length > 0) {
                    prezzo = '&euro; ' + parseFloat(el.prezzo_4).toFixed(2);
                }
                pagina += prezzo;
                pagina += '</td>';
            }

            if (sett.Field350 === "true") {
                pagina += '<td class="text-right" style="width:7.5vh;padding-right:5px">';
                var prezzo = "";
                if (el.prezzo_maxi_prima.length > 0) {
                    prezzo = '&euro; ' + parseFloat(el.prezzo_maxi_prima).toFixed(2);
                }
                pagina += prezzo;
                pagina += '</td>';
            }

            if (sett.Field351 === "true") {
                pagina += '<td class="text-right" style="width:7.5vh;padding-right:5px">';
                var prezzo = "";
                if (el.prezzo_varianti_aggiuntive.length > 0) {
                    prezzo = '&euro; ' + parseFloat(el.prezzo_varianti_aggiuntive).toFixed(2);
                }
                pagina += prezzo;
                pagina += '</td>';
            }

            if (sett.Field352 === "true") {
                pagina += '<td class="text-right" style="width:7.5vh;padding-right:5px">';
                var prezzo = "";
                if (el.prezzo_varianti_maxi.length > 0) {
                    prezzo = '&euro; ' + parseFloat(el.prezzo_varianti_maxi).toFixed(2);
                }
                pagina += prezzo;
                pagina += '</td>';
            }

            /* if (sett.Field338 === "true") {
             pagina += '<td  class="text-center" style="width:7.5vh;">';
             var peso_ums="";
             if(el.peso_ums!==undefined&&el.peso_ums!=="undefined"&&el.prezzo_3!==undefined&&el.prezzo_3!=="undefined"&&el.prezzo_3.length>0){
             peso_ums=el.peso_ums;
             }
             pagina += peso_ums;
             pagina += '</td>';
             }*/

            /* if (sett.Field339 === "true") {
             pagina += '<td class="text-right" style="width:7.5vh;padding-right:5px">';
             var prezzo="";
             if(el.prezzo_3!==undefined&&el.prezzo_3!=="undefined"&&el.prezzo_3.length>0){
             prezzo='&euro; '+parseFloat(el.prezzo_3).toFixed(2);
             }
             pagina += prezzo;
             pagina += '</td>';
             }*/

            /*if (sett.Field340 === "true") {
             
             var nome_stampante = "";
             
             if (comanda.nome_stampante[el.dest_st_1] !== undefined) {
             nome_stampante = comanda.nome_stampante[el.dest_st_1].nome_umano;
             }
             
             pagina += '<td class="text-center"  style="width:8vh;">';
             pagina += nome_stampante;
             pagina += '</td>';
             }*/

            /*if (sett.Field341 === "true") {
             var nome_stampante = "";
             
             if (comanda.nome_stampante[el.dest_st_2] !== undefined) {
             nome_stampante = comanda.nome_stampante[el.dest_st_2].nome_umano;
             }
             
             pagina += '<td class="text-center"  style="width:8vh;">';
             pagina += nome_stampante;
             pagina += '</td>';
             }*/


            /* if (sett.Field342 === "true") {
             var nome_portata = "";
             
             if (comanda.nome_portata[el.portata] !== undefined) {
             nome_portata = comanda.nome_portata[el.portata];
             }
             
             pagina += '<td  class="text-center" style="width:7.5vh;"';
             pagina += nome_portata;
             pagina += '</td>';
             }*/

            /*if (sett.Field343 === "true") {
             
             var nome_categoria = "";
             
             if (comanda.nome_categoria[el.cat_varianti.split("/")[0]] !== undefined) {
             nome_categoria = comanda.nome_categoria[el.cat_varianti.split("/")[0]];
             }
             
             if(nome_categoria==="NESSUNA"){
             nome_categoria="";
             }
             pagina += '<td class="text-center" >';
             pagina += nome_categoria;
             pagina += '</td>';
             }*/

            /*if (sett.Field344 === "true") {
             
             var nome_categoria = "";
             
             if (el.cat_varianti.split("/")[1] !== undefined && comanda.nome_categoria[el.cat_varianti.split("/")[1]] !== undefined) {
             nome_categoria = comanda.nome_categoria[el.cat_varianti.split("/")[1]];
             }
             if(nome_categoria==="NESSUNA"){
             nome_categoria="";
             }
             
             pagina += '<td class="text-center" >';
             pagina += nome_categoria;
             pagina += '</td>';
             }*/

            if (sett.Field345 === "true") {
                pagina += '<td class="text-center"  style="width:6.5vh;">';
                pagina += el.perc_iva_base;
                pagina += '</td>';
            }

            if (sett.Field346 === "true") {
                pagina += '<td class="text-center"  style="width:6.5vh;">';
                pagina += el.perc_iva_takeaway;
                pagina += '</td>';
            }

            /*if (sett.Field347 === "true") {
             
             var riepilogo="";
             if(el.riepilogo==="S"){
             riepilogo="S";
             }
             pagina += '<td class="text-center"  style="width:7vh;">';
             pagina += riepilogo;
             pagina += '</td>';
             }*/

            /*if (sett.Field348 === "true") {
             pagina += '<td class="text-center"  style="width:6.5vh;">';
             var cod_promo="";
             if(el.cod_promo!==undefined&&el.cod_promo!=="undefined"){
             cod_promo=el.cod_promo;
             }
             pagina += cod_promo;
             pagina += '</td>';
             }*/
            pagina += '</tr>';
        });
        pagina += '<tr>';
        pagina += '<td style="font-weight:bold;font-size:1vh">';
        pagina += 'TOTALE: ' + categoria_varianti[desc].length;;
        pagina += '</td>';
        pagina += '</tr>';
        pagina += '</table></div>';
        totale_generale += categoria_varianti[desc].length;
    }

    //pagina += '<h5>Totale Generale: '+totale_generale+'</h5>';



    $('.print').html(pagina);
    window.print();
    //});
}

var RAM = new Object();
RAM.prg_art_promo_principale = 0;
RAM.prg_art_principale = 0;
RAM.tipo_categorie_layout_tasti = "Categorie";

function popup_aggiungi_promo_2(id_articolo) {

    RAM.id_articolo_cod_promo_2 = id_articolo;
    $('#popup_cod_promo_2').modal('show');
}

function aggiungi_sconto_cod_promo_2(event, id_articolo) {

    var quantita_soglia = $('#popup_cod_promo_2 input[name="quantita_soglia"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var tipo_sconto = $('#popup_cod_promo_2 input[name="tipo_sconto"]:visible').val().toUpperCase().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var valore_sconto = $('#popup_cod_promo_2 input[name="valore_sconto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '').replace(",", ".");
    var da_ora = $('#popup_cod_promo_2 input[name="da_ora"]:visible').val().replace(/'/gi, '').replace(/"/gi, '').replace(".", ":").replace(",", ":");
    var a_ora = $('#popup_cod_promo_2 input[name="a_ora"]:visible').val().replace(/'/gi, '').replace(/"/gi, '').replace(".", ":").replace(",", ":");
    var c = new Array();
    var cstring = "";
    $("#giorni_cod_promo_2 [type='checkbox']:checked").each(function (a, b) {
        c.push($(b).val());
    });
    cstring = c.join(";");
    if ((da_ora.length !== 0 && da_ora.length !== 5) || (a_ora.length !== 0 && a_ora.length !== 5)) {
        bootbox.alert("Gli orari devono essere o lasciati vuoti, o in formato di 5 caratteri e 24H (es. 22:00)");
    } else {

        var testo_query = "select id from cod_promo_2 order by cast(id as int) desc limit 1;";
        var risultato = alasql(testo_query);
        var id = 0;
        if (risultato[0] !== undefined && risultato[0].id !== undefined) {
            id = parseInt(risultato[0].id) + 1;
        }

        var query = "INSERT INTO cod_promo_2 (id,id_articolo,qta_soglia,tipo_sconto,valore_sconto,da_ora,a_ora,giorni_settimana) VALUES (" + id + ",'" + id_articolo + "','" + quantita_soglia + "','" + tipo_sconto + "','" + valore_sconto + "','" + da_ora + "','" + a_ora + "','" + cstring + "');";
        comanda.sock.send({
            tipo: "aggiornamento_posizione_prodotti",
            operatore: comanda.operatore,
            query: query,
            terminale: comanda.terminale,
            ip: comanda.ip_address
        });
        comanda.sincro.query(query, function () {

            gestisci_prodotto(event, id_articolo);
            RAM.id_articolo_cod_promo_2 = '';
            $('#popup_cod_promo_2 input[type="text"]').val('');
            tabella_sconti_cod_promo_2();
        });
        comanda.sincro.query_cassa();
    }
}


function cancella_cod_promo_2(event, id_tabella, id_prodotto) {
    var query = "DELETE FROM cod_promo_2 WHERE id=" + id_tabella + ";";
    comanda.sock.send({
        tipo: "aggiornamento_posizione_prodotti",
        operatore: comanda.operatore,
        query: query,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    comanda.sincro.query(query, function () {

        gestisci_prodotto(event, id_prodotto);
        tabella_sconti_cod_promo_2();
        comanda.sincro.query_cassa();
    });
}

function gestisci_menu(event) {

}

function gestisci_prodotto(event, id) {

    let tabella_reparti_IVA = REPARTO_controller.tabella_reparti_IVA();
    let tabella_IVA_semplice = IVA_controller.tabella_IVA_semplice();
    if (event !== undefined && event.target !== undefined && event.target !== null) {
        $(event.target).parent().find("a").removeClass('cat_accesa');
        $(event.target).addClass("cat_accesa");
    }

    comanda.sincro.query("SELECT fastorder,articolo_a_peso FROM impostazioni_fiscali  where id=" + comanda.folder_number + "  LIMIT 1;", function (fastorder) {
        var fastorder_abilitato = fastorder[0].fastorder;
        var articolo_a_peso = fastorder[0].articolo_a_peso;
        $('#form_gestione_prodotti').html('');
        var form_gestione_prodotti = '';
        comanda.sincro.query("SELECT * FROM prodotti WHERE categoria!='XXX' and id='" + id + "' ORDER BY id ASC LIMIT 1;", function (result) {
            comanda.sincro.query("SELECT * FROM categorie WHERE id='" + comanda.categoria + "' ORDER BY id ASC LIMIT 1;", function (resultcat) {
                for (var key in result[0]) {
                    console.log("RESULT0", key, result[0], result[0][key]);
                    if (result[0][key] === null) {
                        result[0][key] = '';
                    }
                }

                comanda.sincro.query('SELECT descrizione,cod_promo FROM prodotti WHERE cod_promo="1" and cat_varianti LIKE "%' + comanda.categoria + '";', function (r_cod_promo_1) {

                    comanda.sincro.query("SELECT * FROM cod_promo_2 WHERE id_articolo='" + id + "' ORDER BY id ASC;", function (r_cod_promo_2) {

                        if (resultcat[0].descrizione.substr(0, 2) === "V.") {
                            form_gestione_prodotti = '<div class="editor_clienti">\n\
                                   <div class="col-md-12 col-xs-12">';
                            if (r_cod_promo_1[0] !== undefined) {
                                form_gestione_prodotti += '<div class="col-md-1 col-xs-1"><label>Qta Fissa</label><input style="margin-bottom:10px;" class="form-control" type="text" name="qta_fissa"   value="' + result[0].qta_fissa + '"></div>';
                            }

                            var prezzo_varianti_aggiuntive = "";
                            if (!isNaN(parseFloat(result[0].prezzo_varianti_aggiuntive))) {
                                prezzo_varianti_aggiuntive = parseFloat(result[0].prezzo_varianti_aggiuntive).toFixed(2);
                            }

                            var prezzo_2 = "0.00";
                            if (!isNaN(parseFloat(result[0].prezzo_2))) {
                                prezzo_2 = parseFloat(result[0].prezzo_2).toFixed(2);
                            }

                            var prezzo_maxi_prima = "";
                            if (!isNaN(parseFloat(result[0].prezzo_maxi_prima))) {
                                prezzo_maxi_prima = parseFloat(result[0].prezzo_maxi_prima).toFixed(2);
                            }

                            var prezzo_varianti_maxi = "";
                            if (!isNaN(parseFloat(result[0].prezzo_varianti_maxi))) {
                                prezzo_varianti_maxi = parseFloat(result[0].prezzo_varianti_maxi).toFixed(2);
                            }

                            form_gestione_prodotti += '<div class="col-md-6 col-xs-6"><label>' + comanda.lang[96] + '</label><input maxlength="20" style="text-transform:uppercase;font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="descrizione" value="' + result[0].descrizione.toUpperCase() + '"></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>' + comanda.lang[51] + '</label><input style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_1"   value="' + parseFloat(result[0].prezzo_1).toFixed(2) + '" placeholder="1.00"></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>Costo</label><input style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="costo"   value="' + parseFloat(result[0].costo).toFixed(2) + '" placeholder="1.00"></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>Prezzo Variante Maxi</label><input style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_maxi_prima"   value="' + prezzo_maxi_prima + '" placeholder="1.00"></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>Prezzo Varianti Aggiuntive&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'PREZZO VARIANTI AGGIUNTIVE\', \'Inserendo il prezzo in questo campo, la prima variante agigunta alla pizza presenter&agrave; il prezzo regolare; dalla seconda in poi, presenter&agrave; il prezzo inserito in questa casella (prezzo varianti aggiuntive).\')">?</a></label><input style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_varianti_aggiuntive"   value="' + prezzo_varianti_aggiuntive + '" placeholder="0.50"></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>Prezzo Maxi Aggiuntive</label><input style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_varianti_maxi"   value="' + prezzo_varianti_maxi + '" placeholder="1.00"></div>';
                            form_gestione_prodotti += '<div class="col-md-8 col-xs-8">\n\
                                    <div class="col-xs-2"><label>Prezzo POCO</label><input style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_4" value="' + result[0].prezzo_4 + '"></div>\n\
                                    <div class="col-xs-2"><label>Prezzo Bar</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_bar" value="' + result[0].listino_bar + '"></div>\n\
                                    <div class="col-xs-2"><label>Prezzo Asporto</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_asporto" value="' + result[0].listino_asporto + '"></div>\n\
                                    <div class="col-xs-2"><label>Pr. Continuo</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_continuo" value="' + result[0].listino_continuo + '"></div>\n\
                                    <div class="col-xs-2"><label>Prezzo Var. -</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_variante_meno" value="' + result[0].prezzo_variante_meno + '"></div>\n\
                                   </div>';
                            form_gestione_prodotti += '<div class="col-md-12 col-xs-12">';
                            if (comanda.abilita_fattorino1 === "1") {
                                let nome_fattorino = "Fattorino 1";
                                if (comanda.nome_fattorino1 !== undefined && comanda.nome_fattorino1 !== null && comanda.nome_fattorino1.trim().length > 0) {
                                    nome_fattorino = comanda.nome_fattorino1.trim();
                                }
                                form_gestione_prodotti += '<div class="col-xs-2"><label>Prezzo ' + nome_fattorino + ' Base</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_fattorino1_norm" value="' + result[0].prezzo_fattorino1_norm + '"></div>';
                                form_gestione_prodotti += '<div class="col-xs-2"><label>Prezzo ' + nome_fattorino + ' Maxi</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_fattorino1_maxi" value="' + result[0].prezzo_fattorino1_maxi + '"></div>';
                            }

                            if (comanda.abilita_fattorino2 === "1") {
                                let nome_fattorino = "Fattorino 2";
                                if (comanda.nome_fattorino2 !== undefined && comanda.nome_fattorino2 !== null && comanda.nome_fattorino2.trim().length > 0) {
                                    nome_fattorino = comanda.nome_fattorino2.trim();
                                }
                                form_gestione_prodotti += '<div class="col-xs-2"><label>Prezzo ' + nome_fattorino + ' Base</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_fattorino2_norm" value="' + result[0].prezzo_fattorino2_norm + '"></div>';
                                form_gestione_prodotti += '<div class="col-xs-2"><label>Prezzo ' + nome_fattorino + ' Maxi</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_fattorino2_maxi" value="' + result[0].prezzo_fattorino2_maxi + '"></div>';
                            }

                            if (comanda.abilita_fattorino3 === "1") {
                                let nome_fattorino = "Fattorino 3";
                                if (comanda.nome_fattorino3 !== undefined && comanda.nome_fattorino3 !== null && comanda.nome_fattorino3.trim().length > 0) {
                                    nome_fattorino = comanda.nome_fattorino3.trim();
                                }
                                form_gestione_prodotti += '<div class="col-xs-2"><label>Prezzo ' + nome_fattorino + ' Base</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_fattorino3_norm" value="' + result[0].prezzo_fattorino3_norm + '"></div>';
                                form_gestione_prodotti += '<div class="col-xs-2"><label>Prezzo ' + nome_fattorino + ' Maxi</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_fattorino3_maxi" value="' + result[0].prezzo_fattorino3_maxi + '"></div>';
                            }
                            form_gestione_prodotti += '</div>';
                            form_gestione_prodotti += '<div class="col-md-4 col-xs-4"><label>' + comanda.lang[24] + '</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="categoria" ></select></div>\n\
                                   </div>\n\
                                   <div class="col-md-12 col-xs-12">\n\
                                   <div class="col-md-3 col-xs-3" style="display:none"><label>' + comanda.lang[100] + '</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="dest_stampa" ></select></div>\n\
                                   <div class="col-md-3 col-xs-3" style="display:none"><label>' + comanda.lang[100] + ' 2</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="dest_stampa_2" ></select></div>\n\
                                   <div class="col-md-3 col-xs-3" style="display:none"><label>' + comanda.lang[126] + '</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="portata" ></select></div>\n\
                                   <div class="col-md-3 col-xs-3" style="display:none"><label>Gruppo Statistico</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="gruppi_statistici" ></select></div>\n\
                                   <div class="col-md-2 col-xs-2" style="display:none;"><label>' + comanda.lang[171] + '</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="posizione" value="' + result[0].posizione + '"></div>\n\
                                   <div class="col-md-1 col-xs-1" style="display:none;"><label>' + comanda.lang[172] + '</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="pagina" value="' + result[0].pagina + '"></div>\n\
                                   </div>';
                            form_gestione_prodotti += '<div class="col-md-12 col-xs-12">\n\
                                   <div class="col-md-4 col-xs-3" style="display:none"><label>' + comanda.lang[173] + '</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="cat_var" ></select></div>\n\
                                   <div class="col-md-4 col-xs-3" style="display:none"><label>' + comanda.lang[174] + '</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="cat_var_auto" ></select></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>Iva Base / Servizi</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="perc_iva_base" >' + tabella_IVA_semplice + '</select></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>Iva TakeAway / Beni</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="perc_iva_takeaway" >' + tabella_IVA_semplice + '</select></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>Reparto Servizi</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="reparto_servizi" >' + tabella_reparti_IVA + '</select></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>Reparto Beni</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="reparto_beni" >' + tabella_reparti_IVA + '</select></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>' + comanda.lang[177] + '</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="ordinamento" value="' + result[0].ordinamento + '"></div>\n\
                                   <div class="col-md-12 col-xs-12" style="display:none"><label>' + comanda.lang[178] + '</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" maxlength="38" name="ricetta" value="' + result[0].ricetta + '"></div>';

                            if (fastorder_abilitato === '1') {
                                form_gestione_prodotti += '<div class="col-md-12 col-xs-12" style="display:none"><label>Nome Immagine</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="immagine" value="' + result[0].immagine + '"></div>';
                                form_gestione_prodotti += '<div class="col-md-12 col-xs-12" style="display:none"><label>Descrizione fast.order</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="descrizione_fastorder" value="' + result[0].descrizione_fastorder + '"></div>';
                                form_gestione_prodotti += '<div class="col-md-12 col-xs-12" style="display:none"><label>Ricetta fast.order</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="ricetta_fastorder" value="' + result[0].ricetta_fastorder + '"></div>';
                                form_gestione_prodotti += '<div class="col-md-2 col-xs-2"  style="display:none"><label>Codice Promozionale</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="cod_promo" onkeyup="salva_prodotto(event,\'' + result[0].id + '\');" maxlength="2" value=""></div>';
                            }

                            form_gestione_prodotti += '</div>\n\
                                   <div class="col-md-12 col-xs-12"><button class="btn_salva_prodotto pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_prodotto(event,\'' + result[0].id + '\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button><button class="pull-right col-md-2 col-xs-2 btn btn-danger" ' + comanda.evento + '="elimina_prodotto(\'' + result[0].id + '\');" style="text-transform:uppercase;">' + comanda.lang[179] + '</button></div>\n\
                                   </div>';
                            $('#form_gestione_prodotti').html(form_gestione_prodotti);
                            //RIMUOVO I VECCHI RISULTATI DELLE TENDINE
                            $('select[name="dest_stampa"] option').remove();
                            $('select[name="dest_stampa_2"] option').remove();
                            $('select[name="portata"] option').remove();
                            $('select[name="gruppi_statistici"] option').remove();
                            /* Selettore dell'IVA */
                            $('[name="perc_iva_base"] option[value="' + result[0].perc_iva_base + '"]').attr("selected", true);
                            $('[name="perc_iva_takeaway"] option[value="' + result[0].perc_iva_takeaway + '"]').attr("selected", true);
                            /* Selettore dei reparti */
                            $('[name="reparto_servizi"] option[value="' + result[0].reparto_servizi + '"]').attr("selected", true);
                            $('[name="reparto_beni"] option[value="' + result[0].reparto_beni + '"]').attr("selected", true);
                            var destinazione_stampa = '';
                            var destinazione_stampa_2 = '';
                            var opzione;
                            //RIEMPIO LA TENDINA DELLE CATEGORIE
                            comanda.sincro.query("SELECT id,ordinamento,descrizione FROM categorie ORDER BY ordinamento ASC;", function (results) {
                                results.forEach(function (row) {
                                    opzione = "<option value=\"" + row.id + "\">" + row.descrizione + "</option>";
                                    $('[name="categoria"]').append(opzione);
                                });
                                var categoria = '';
                                if (result[0].categoria !== undefined && result[0].categoria.length > 0) {
                                    categoria = result[0].categoria;
                                } else {
                                    categoria = resultcat[0].categoria;
                                }
                                $('[name="categoria"] option[value="' + categoria + '"]').attr("selected", true);
                            });
                        }
                        //SE NON E' UNA CATEGORIA DI VARIANTI
                        else {

                            var spazio_forno = '1';
                            if (!isNaN(parseInt(result[0].spazio_forno))) {
                                spazio_forno = result[0].spazio_forno;
                            }

                            var prezzo_un_quarto = "";
                            if (parseFloat(result[0].prezzo_2).toFixed(2) !== "NaN") {
                                prezzo_un_quarto = parseFloat(result[0].prezzo_2).toFixed(2);
                            }

                            var prezzo_maxi = "0.00";
                            if (!isNaN(parseFloat(result[0].prezzo_maxi))) {
                                prezzo_maxi = parseFloat(result[0].prezzo_maxi).toFixed(2);
                            }

                            form_gestione_prodotti = '<div class="editor_clienti">\n\
                                   <div class="col-md-12 col-xs-12">\n\
                                    <div class="col-xs-6"><label>Descrizione</label><input maxlength="20" style="text-transform:uppercase;font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="descrizione" value="' + result[0].descrizione.toUpperCase() + '"></div>\n\
                                    <div class="col-xs-2"><label>Spazio&nbsp;forno</label><input style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="spazio_forno"   value="' + spazio_forno + '"></div>\n\
                                    <div class="col-xs-2"><label>Prezzo Default</label><input style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_1"   value="' + parseFloat(result[0].prezzo_1).toFixed(2) + '"></div>\n\
                                    <div class="col-xs-2"><label>Costo</label><input style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="costo"   value="' + parseFloat(result[0].costo).toFixed(2) + '"></div>\n\
                                    <div class="col-xs-2"><label>Prezzo Maxi</label><input style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_maxi"   value="' + prezzo_maxi + '"></div>\n\
                                    <div class="col-xs-2"><label>Prezzo Bar</label><input style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_bar" value="' + result[0].listino_bar + '"></div>\n\
                                    <div class="col-xs-2"><label>Pr. Asporto</label><input style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_asporto" value="' + result[0].listino_asporto + '"></div>\n\
                                   </div>';
                            form_gestione_prodotti += '<div class="col-md-12 col-xs-12">';
                            if (comanda.abilita_fattorino1 === "1") {
                                let nome_fattorino = "Fattorino 1";
                                if (comanda.nome_fattorino1 !== undefined && comanda.nome_fattorino1 !== null && comanda.nome_fattorino1.trim().length > 0) {
                                    nome_fattorino = comanda.nome_fattorino1.trim();
                                }
                                form_gestione_prodotti += '<div class="col-xs-2"><label>Prezzo ' + nome_fattorino + ' Base</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_fattorino1_norm" value="' + result[0].prezzo_fattorino1_norm + '"></div>';
                                form_gestione_prodotti += '<div class="col-xs-2"><label>Prezzo ' + nome_fattorino + ' Maxi</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_fattorino1_maxi" value="' + result[0].prezzo_fattorino1_maxi + '"></div>';
                            }

                            if (comanda.abilita_fattorino2 === "1") {
                                let nome_fattorino = "Fattorino 2";
                                if (comanda.nome_fattorino2 !== undefined && comanda.nome_fattorino2 !== null && comanda.nome_fattorino2.trim().length > 0) {
                                    nome_fattorino = comanda.nome_fattorino2.trim();
                                }
                                form_gestione_prodotti += '<div class="col-xs-2"><label>Prezzo ' + nome_fattorino + ' Base</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_fattorino2_norm" value="' + result[0].prezzo_fattorino2_norm + '"></div>';
                                form_gestione_prodotti += '<div class="col-xs-2"><label>Prezzo ' + nome_fattorino + ' Maxi</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_fattorino2_maxi" value="' + result[0].prezzo_fattorino2_maxi + '"></div>';
                            }

                            if (comanda.abilita_fattorino3 === "1") {
                                let nome_fattorino = "Fattorino 3";
                                if (comanda.nome_fattorino3 !== undefined && comanda.nome_fattorino3 !== null && comanda.nome_fattorino3.trim().length > 0) {
                                    nome_fattorino = comanda.nome_fattorino3.trim();
                                }
                                form_gestione_prodotti += '<div class="col-xs-2"><label>Prezzo ' + nome_fattorino + ' Base</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_fattorino3_norm" value="' + result[0].prezzo_fattorino3_norm + '"></div>';
                                form_gestione_prodotti += '<div class="col-xs-2"><label>Prezzo ' + nome_fattorino + ' Maxi</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_fattorino3_maxi" value="' + result[0].prezzo_fattorino3_maxi + '"></div>';
                            }
                            form_gestione_prodotti += '</div>';
                            form_gestione_prodotti += '<div class="col-md-9 col-xs-9">\n\
                                    <div class="col-xs-2"><label>Prezzo t. Continuo</label><input style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_continuo" value="' + result[0].listino_continuo + '"></div>\n\
                                    <div class="col-xs-2"><label>Prezzo 1/4 di metro</label><input style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_2"   value="' + prezzo_un_quarto + '"></div>\n\
                                    <div class="col-xs-8"><label>Categoria</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="categoria" ></select></div>\n\
                            </div>';
                            if (articolo_a_peso === '1') {

                                form_gestione_prodotti += '<div class="col-md-3 col-xs-3">\n\
                                   <div class="col-xs-6"><label>Unita\' di Misura</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" name="scelta_unita_misura"><option value="Kg">Kg</option><option value="Hg">Hg</option><option value="u">Unita\'</option></select></div>\n\
                                   <div class="col-xs-6"><label>Prezzo x U.</label><input style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_x_um" value="' + result[0].prezzo_3 + '"></div>\n\
                                   </div>';
                            }



                            form_gestione_prodotti += '<div class="col-md-12 col-xs-12">\n\
                                   <div class="col-md-3 col-xs-3"><label>Destinazione di stampa</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="dest_stampa" ></select></div>\n\
                                   <div class="col-md-3 col-xs-3"><label>Destinazione di stampa 2</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="dest_stampa_2" ></select></div>\n\
                                   <div class="col-md-3 col-xs-3"><label>Portata</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="portata" ></select></div>\n\
                                   <div class="col-md-3 col-xs-3"><label>Gruppo Statistico</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="gruppi_statistici" ></select></div>\n\
                                   <div class="col-md-2 col-xs-2" style="display:none;"><label>' + comanda.lang[171] + '</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="posizione" value="' + result[0].posizione + '"></div>\n\
                                   <div class="col-md-1 col-xs-1" style="display:none;"><label>' + comanda.lang[172] + '</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="pagina" value="' + result[0].pagina + '"></div>\n\
                                   </div>\n\
                                   \n\
                                   <div class="col-md-12 col-xs-12">\n\
                                   <div class="col-md-4 col-xs-3"><label>' + comanda.lang[173] + '</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="cat_var" ></select></div>\n\
                                   <div class="col-md-4 col-xs-3"><label>' + comanda.lang[174] + '</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="cat_var_auto" ></select></div>\n\                                                                     \n\
                                   <div class="col-md-2 col-xs-2"><label>Iva Base / Servizi</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="perc_iva_base" >' + tabella_IVA_semplice + '</select></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>Iva TakeAway / Beni</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="perc_iva_takeaway" >' + tabella_IVA_semplice + '</select></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>Reparto Servizi</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="reparto_servizi" >' + tabella_reparti_IVA + '</select></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>Reparto Beni</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="reparto_beni" >' + tabella_reparti_IVA + '</select></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>' + comanda.lang[177] + '</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control parte_numerica" type="text" name="ordinamento" value="' + result[0].ordinamento + '"></div>';
                            var test1 = result[0].automodifica_prezzo == "S" ? "checked" : "";
                            form_gestione_prodotti += '<div class="col-md-2 col-xs-2"><label>Modifica Prezzo Auto.&nbsp;<a style="font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'MODIFICA PREZZO AUTOMATICA\', \'Spuntando questa casella, una volta battuto l articolo si aprir&agrave; in automatico la finestra del modifica prodotto, con il focus direttamente sulla modifica prezzo.\')">?</a></label><input class="form-control esclusione_css" type="checkbox" name="modifica_prezzo_auto"  ' + test1 + '></div>\n\
                                   <div class="col-md-12 col-xs-12"><label>' + comanda.lang[178] + '</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="ricetta" maxlength="100" value="' + result[0].ricetta + '"></div>';
                            var test1 = result[0].abilita_riepilogo == "S" ? "checked" : "";
                            form_gestione_prodotti += '<div class="col-md-1 col-xs-1"><label>Riepilogo</label><input class="form-control esclusione_css" type="checkbox" name="abilita_riepilogo"  ' + test1 + '></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>Codice Promozionale&nbsp;&nbsp;&nbsp;&nbsp;<a style="font-weight:bold;cursor:pointer;" ' + comanda.evento + '="$(\'#popup_spiegazione_cod_promo\').modal(\'show\')">?</a></label><input style="font-weight:bold;margin-bottom:10px;" class="form-control parte_numerica" type="text" name="cod_promo" placeholder="Es.: 2" onkeyup="salva_prodotto(event,\'' + result[0].id + '\');" maxlength="2" value="' + result[0].cod_promo + '"></div>';
                            if (result[0].cod_promo === '2') {
                                form_gestione_prodotti += '<div class="col-md-2 col-xs-2"><label>Gestione Scontistica</label><button name="btn_cod_promo_2" class="btn btn-info" style="margin-top: -2px;" ' + comanda.evento + '="popup_aggiungi_promo_2(\'' + result[0].id + '\')">+</button></div>';
                                form_gestione_prodotti += '<div class="col-md-12 col-xs-12" style="font-weight:bold;"><div class="col-xs-2">Soglia</div><div class="col-xs-2">T.Sconto</div><div class="col-xs-2">Sconto</div><div class="col-xs-1">Da ora</div><div class="col-xs-1">A ora</div><div class="col-xs-2">Giorni</div><div class="col-xs-2">Opzioni</div></div>';
                                r_cod_promo_2.forEach(function (v) {

                                    var array_giorni_settimana = new Array();
                                    if (v.giorni_settimana.match(/\d+/gi) !== null) {
                                        v.giorni_settimana.match(/\d+/gi).forEach(function (v) {

                                            switch (v) {
                                                case "1":
                                                    array_giorni_settimana.push("Lu");
                                                    break;
                                                case "2":
                                                    array_giorni_settimana.push("Ma");
                                                    break;
                                                case "3":
                                                    array_giorni_settimana.push("Me");
                                                    break;
                                                case "4":
                                                    array_giorni_settimana.push("Gi");
                                                    break;
                                                case "5":
                                                    array_giorni_settimana.push("Ve");
                                                    break;
                                                case "6":
                                                    array_giorni_settimana.push("Sa");
                                                    break;
                                                case "0":
                                                    array_giorni_settimana.push("Do");
                                                    break;
                                                default:
                                                    array_giorni_settimana.push("TUTTI");
                                            }


                                        });
                                    }

                                    form_gestione_prodotti += '<div class="col-md-12 col-xs-12"><div class="col-xs-2">' + v.qta_soglia + '</div><div class="col-xs-2">' + v.tipo_sconto + '</div><div class="col-xs-2">' + v.valore_sconto + '</div><div class="col-xs-1">' + v.da_ora + '</div><div class="col-xs-1">' + v.a_ora + '</div><div class="col-xs-2">' + array_giorni_settimana.join() + '</div><div class="col-xs-2" style="color:red;cursor:pointer;" ' + comanda.evento + '="cancella_cod_promo_2(event,\'' + v.id + '\',\'' + result[0].id + '\')">X</div></div>';
                                });
                            }

                            if (fastorder_abilitato === '1') {
                                form_gestione_prodotti += '<div class="col-md-12 col-xs-12"><label>Nome Immagine</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="immagine" value="' + result[0].immagine + '"></div>';
                                form_gestione_prodotti += '<div class="col-md-12 col-xs-12"><label>Descrizione fast.order</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="descrizione_fastorder" value="' + result[0].descrizione_fastorder + '"></div>';
                                form_gestione_prodotti += '<div class="col-md-12 col-xs-12"><label>Ricetta fast.order</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="ricetta_fastorder" value="' + result[0].ricetta_fastorder + '"></div>';

                                var test1 = result[0].esportazione_fastorder_abilitata == "true" ? "checked" : "";
                                form_gestione_prodotti += '<div class="col-md-1 col-xs-1"><label>Esportazione Abilitata</label><input class="form-control esclusione_css" type="checkbox" name="esportazione_fastorder_abilitata"  ' + test1 + '></div>';

                            }

                            form_gestione_prodotti += '</div>\n\
                                   <div class="col-md-12 col-xs-12" style="margin-top: 1vh;"><button class="btn_salva_prodotto pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_prodotto(event,\'' + result[0].id + '\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button><button class="pull-right col-md-2 col-xs-2 btn btn-danger" ' + comanda.evento + '="elimina_prodotto(\'' + result[0].id + '\');" style="text-transform:uppercase;">' + comanda.lang[179] + '</button></div>\n\
                                   </div>';
                            $('#form_gestione_prodotti').html(form_gestione_prodotti);
                            //RIMUOVO I VECCHI RISULTATI DELLE TENDINE
                            $('select[name="dest_stampa"] option').remove();
                            $('select[name="dest_stampa_2"] option').remove();
                            $('select[name="portata"] option').remove();
                            $('select[name="gruppi_statistici"] option').remove();
                            /* Selettore dell'IVA */
                            $('[name="perc_iva_base"] option[value="' + result[0].perc_iva_base + '"]').attr("selected", true);
                            $('[name="perc_iva_takeaway"] option[value="' + result[0].perc_iva_takeaway + '"]').attr("selected", true);
                            /* Selettore dei reparti */
                            $('[name="reparto_servizi"] option[value="' + result[0].reparto_servizi + '"]').attr("selected", true);
                            $('[name="reparto_beni"] option[value="' + result[0].reparto_beni + '"]').attr("selected", true);
                            $('[name="scelta_unita_misura"] option[value="' + result[0].peso_ums + '"]').attr("selected", true);
                            var destinazione_stampa = '';
                            var destinazione_stampa_2 = '';
                            var opzione;
                            //RIEMPIO LA TENDINA DELLE CATEGORIE
                            comanda.sincro.query("SELECT id,ordinamento,descrizione FROM categorie ORDER BY ordinamento ASC;", function (results) {
                                results.forEach(function (row) {
                                    opzione = "<option value=\"" + row.id + "\">" + row.descrizione + "</option>";
                                    $('[name="categoria"]').append(opzione);
                                });
                                var categoria = '';
                                if (result[0].categoria !== undefined && result[0].categoria.length > 0) {
                                    categoria = result[0].categoria;
                                } else {
                                    categoria = resultcat[0].categoria;
                                }
                                $('[name="categoria"] option[value="' + categoria + '"]').attr("selected", true);
                            });
                            //RIEMPIO LA TENDINA DELLE CATEGORIA VARIANTI
                            comanda.sincro.query("SELECT id,ordinamento,descrizione FROM categorie WHERE SUBSTR(descrizione,1,2)=\"V.\" ORDER BY ordinamento ASC;", function (results) {

                                opzione = "<option value=\"ND\">NESSUNA</option>";
                                $('[name="cat_var"]').append(opzione);
                                results.forEach(function (row) {


                                    opzione = "<option value=\"" + row.id + "\">" + row.descrizione + "</option>";
                                    $('[name="cat_var"]').append(opzione);
                                });
                                var categoria_variante = '';
                                if (result[0].cat_varianti !== undefined && result[0].cat_varianti.length > 0) {
                                    categoria_variante = result[0].cat_varianti.split('/')[0];
                                } else {
                                    categoria_variante = resultcat[0].cat_var;
                                }

                                $('[name="cat_var"] option[value="' + categoria_variante + '"]').attr("selected", true);
                            });
                            //RIEMPIO LA TENDINA DELLE CATEGORIA VARIANTI AUTOMATICHE
                            comanda.sincro.query("SELECT id,ordinamento,descrizione FROM categorie WHERE SUBSTR(descrizione,1,2)=\"V.\" ORDER BY ordinamento ASC;", function (results) {

                                opzione = "<option value=\"ND\">NESSUNA</option>";
                                $('[name="cat_var_auto"]').append(opzione);
                                results.forEach(function (row) {
                                    opzione = "<option value=\"" + row.id + "\">" + row.descrizione + "</option>";
                                    $('[name="cat_var_auto"]').append(opzione);
                                });
                                var categoria_variante = '';
                                if (result[0].cat_varianti !== undefined && result[0].cat_varianti.length > 0) {
                                    if (result[0].cat_varianti.indexOf('/') !== -1) {
                                        categoria_variante = result[0].cat_varianti.split('/')[1];
                                    }
                                }
                                $('[name="cat_var_auto"] option[value="' + categoria_variante + '"]').attr("selected", true);
                            });
                            //RIEMPIO LA TENDINA DELLE STAMPANTI
                            comanda.sincro.query("SELECT " + comanda.lingua_stampa + " as nome,numero FROM nomi_stampanti WHERE fiscale='n' and  cast(numero as int) >= 50;", function (results) {

                                $('[name="dest_stampa_2"]').append("<option></option>");
                                results.forEach(function (row) {
                                    opzione = "<option value=\"" + row.numero + "\">" + row.nome + "</option>";
                                    $('[name="dest_stampa"]').append(opzione);
                                    $('[name="dest_stampa_2"]').append(opzione);
                                });
                                $('[name="dest_stampa"]').append("<option value=\"T\">TUTTE</option>");
                                if (result[0].dest_st_1 !== undefined && result[0].dest_st_1.length > 0) {
                                    destinazione_stampa = result[0].dest_st_1;
                                } else {
                                    destinazione_stampa = resultcat[0].dest_stampa;
                                }

                                if (result[0].dest_st_2 !== undefined && result[0].dest_st_2.length > 0) {
                                    destinazione_stampa_2 = result[0].dest_st_2;
                                }

                                $('[name="dest_stampa"] option[value="' + destinazione_stampa + '"]').attr("selected", true);
                                $('[name="dest_stampa_2"] option[value="' + destinazione_stampa_2 + '"]').attr("selected", true);
                            });
                            //RIEMPIO LA TENDINA DELLA PORTATA
                            var portata = '';
                            comanda.sincro.query("SELECT " + comanda.lingua_stampa + ",numero FROM nomi_portate  order by numero asc;", function (results) {
                                results.forEach(function (row) {
                                    opzione = "<option value=\"" + row.numero + "\">" + row[comanda.lingua_stampa] + "</option>";
                                    $('[name="portata"]').append(opzione);
                                });
                                if (result[0].dest_st_1 !== undefined && result[0].dest_st_1.length > 0) {
                                    portata = result[0].portata;
                                } else {
                                    portata = resultcat[0].portata;
                                }
                                $('[name="portata"] option[value="' + portata + '"]').attr("selected", true);
                            });
                            //RIEMPIO LA TENDINA DEI GRUPPI STATISTICI
                            var gruppi_statistici = '';
                            comanda.sincro.query("SELECT nome,id FROM gruppi_statistici order by cast(id as int) asc;", function (results) {
                                results.forEach(function (row) {
                                    opzione = "<option value=\"" + row.id + "\">" + row.nome + "</option>";
                                    $('[name="gruppi_statistici"]').append(opzione);
                                });
                                if (result[0].gruppo !== undefined && result[0].gruppo.length > 0) {
                                    gruppi_statistici = result[0].gruppo;
                                } else {
                                    gruppi_statistici = "0";
                                }

                                $('[name="gruppi_statistici"] option[value="' + gruppi_statistici + '"]').attr("selected", true);
                            });
                        }
                    });
                });
            });
        });
    });
}

function gestisci_listino_clienti(event, id) {

    if (event !== undefined && event.target !== undefined && event.target !== null) {
        $(event.target).parent().find("a").removeClass('cat_accesa');
        $(event.target).addClass("cat_accesa");
    }

    $('#form_gestione_listino_clienti').html('');
    var form_gestione_prodotti = '';
    comanda.sincro.query("SELECT * FROM listino_clienti WHERE id='" + id + "' ORDER BY id ASC LIMIT 1;", function (result) {
        for (var key in result[0]) {
            console.log("RESULT0", key, result[0], result[0][key]);
            if (result[0][key] === null) {
                result[0][key] = '';
            }
        }

        form_gestione_prodotti = '<div class="editor_clienti">\n\
                                   <div class="col-md-12 col-xs-12">\n\
                                   <div class="col-md-6 col-xs-6"><label>' + comanda.lang[96] + '</label><input maxlength="20" style="text-transform:uppercase;font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="descrizione" value="' + result[0].descrizione.toUpperCase() + '"></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>' + comanda.lang[51] + '</label><input style="margin-bottom:10px;" class="form-control" type="text" name="prezzo_1"   value="' + parseFloat(result[0].prezzo_1).toFixed(2) + '"></div>\n\
                                   <div class="col-md-4 col-xs-4"><label>' + comanda.lang[24] + '</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="categoria" ></select></div>\n\
                                   </div>\n\
                                   \n\
                                   <div class="col-md-12 col-xs-12">\n\
                                   <div class="col-md-6 col-xs-6"><label>' + comanda.lang[100] + '</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="dest_stampa" ></select></div>\n\
                                   <div class="col-md-3 col-xs-3"><label>' + comanda.lang[126] + '</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="portata" ></select></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>' + comanda.lang[171] + '</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="posizione" value="' + result[0].posizione + '"></div>\n\
                                   <div class="col-md-1 col-xs-1"><label>' + comanda.lang[172] + '</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="pagina" value="' + result[0].pagina + '"></div>\n\
                                   </div>\n\
                                   \n\
                                   <div class="col-md-12 col-xs-12">\n\
                                   <div class="col-md-4 col-xs-3"><label>' + comanda.lang[176] + '</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="cat_var" ></select></div>\n\
                                   <div class="col-md-4 col-xs-3"><label>' + comanda.lang[174] + '</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="cat_var_auto" ></select></div>\n\                                                                     \n\
                                   <div class="col-md-2 col-xs-2"><label>Iva Base / Servizi</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="perc_iva_base" value="' + result[0].perc_iva_base + '"></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>Iva TakeAway / Beni</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="perc_iva_takeaway" value="' + result[0].perc_iva_takeaway + '"></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>' + comanda.lang[177] + '</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="ordinamento" value="' + result[0].ordinamento + '"></div>\n\
                                   <div class="col-md-10 col-xs-10"><label>' + comanda.lang[178] + '</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="ricetta" maxlength="38" value="' + result[0].ricetta + '"></div>\n\
                                    </div>\n\
                                   <div class="col-md-12 col-xs-12"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_prodotto(event,\'' + result[0].id + '\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button><button class="pull-right col-md-2 col-xs-2 btn btn-danger" ' + comanda.evento + '="elimina_prodotto(\'' + result[0].id + '\');" style="text-transform:uppercase;">' + comanda.lang[179] + '</button></div>\n\
                                   </div>';
        $('#form_gestione_listino_clienti').html(form_gestione_prodotti);
        //RIMUOVO I VECCHI RISULTATI DELLE TENDINE
        $('select[name="dest_stampa"] option').remove();
        $('select[name="portata"] option').remove();
        var destinazione_stampa = '';
        var opzione;
    });
}

function salva_categoria(id) {
    var descrizione = $('#form_gestione_categorie input[name="descrizione"]:visible').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var alias = $('#form_gestione_categorie input[name="alias"]:visible').val();
    if (alias !== undefined && typeof (alias) === 'string') {
        alias = alias.replace(/'/gi, '').replace(/"/gi, '');
    } else {
        alias = '';
    }

    var ordinamento = $('#form_gestione_categorie input[name="ordinamento"]:visible').val();
    var iva = $('#form_gestione_categorie select[name="perc_iva"]:visible').val();
    var iva_takeaway = $('#form_gestione_categorie select[name="perc_iva_takeaway"]:visible').val();
    var reparto_servizi = $('#form_gestione_categorie select[name="reparto_servizi"]:visible').val();
    var reparto_beni = $('#form_gestione_categorie select[name="reparto_beni"]:visible').val();
    var cat_var = $('#form_gestione_categorie select[name="cat_var"]:visible').val() /*+ '/ND'*/;
    var dest_stampa = $('#form_gestione_categorie select[name="dest_stampa"]:visible').val();
    var dest_stampa_2 = $('#form_gestione_categorie select[name="dest_stampa_2"]:visible').val();
    var portata = $('#form_gestione_categorie select[name="portata"]:visible').val();
    var abilita_asporto = $('#form_gestione_categorie input[name="abilita_asporto"]:visible').is(':checked');
    var var_aperte = $('#form_gestione_categorie input[name="var_aperte"]:visible').is(':checked') ? 'S' : 'N';
    var disabilita_pda = $('#form_gestione_categorie input[name="disabilita_pda"]:visible').is(':checked') ? 'S' : 'N';
    var disabilita_server = $('#form_gestione_categorie input[name="disabilita_server"]:visible').is(':checked') ? 'S' : 'N';
    var visu_tavoli = $('#form_gestione_categorie input[name="visu_tavoli"]:visible').is(':checked') ? 'true' : 'false';
    var visu_bar = $('#form_gestione_categorie input[name="visu_bar"]:visible').is(':checked') ? 'true' : 'false';
    var visu_asporto = $('#form_gestione_categorie input[name="visu_asporto"]:visible').is(':checked') ? 'true' : 'false';
    var visu_continuo = $('#form_gestione_categorie input[name="visu_continuo"]:visible').is(':checked') ? 'true' : 'false';
    var consegna_abilitata = $('#form_gestione_categorie input[name="consegna_abilitata"]:visible').is(':checked');
    var testo_query = "UPDATE categorie SET  consegna_abilitata='" + consegna_abilitata + "',visu_tavoli='" + visu_tavoli + "',visu_bar='" + visu_bar + "',visu_asporto='" + visu_asporto + "',visu_continuo='" + visu_continuo + "',var_aperte='" + var_aperte + "',disabilita_pda='" + disabilita_pda + "',disabilita_server='" + disabilita_server + "',alias='" + alias + "',abilita_asporto='" + abilita_asporto + "',descrizione='" + descrizione.toUpperCase() + "',ordinamento='" + ordinamento + "',perc_iva='" + iva + "',perc_iva_takeaway='" + iva_takeaway + "',reparto_servizi='" + reparto_servizi + "',reparto_beni='" + reparto_beni + "',cat_var='" + cat_var + "',dest_stampa='" + dest_stampa + "',dest_stampa_2='" + dest_stampa_2 + "',portata='" + portata + "' WHERE id='" + id + "';";
    console.log("SALVA CATEGORIA", testo_query);
    //comanda.array_dbcentrale_mancanti.push(testo_query);


    comanda.categorie_con_consegna_abilitata = new Array();
    var query_categorie_con_calcolo_consegna = 'SELECT id FROM categorie WHERE consegna_abilitata="true" ORDER BY id ASC;';
    comanda.sincro.query(query_categorie_con_calcolo_consegna, function (categorie_con_calcolo_consegna) {


        categorie_con_calcolo_consegna.forEach(function (categorie_con_calcolo_consegna) {

            comanda.categorie_con_consegna_abilitata.push(categorie_con_calcolo_consegna.id);
        });
    });
    comanda.sock.send({
        tipo: "aggiornamento_categorie",
        operatore: comanda.operatore,
        query: testo_query,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    comanda.sincro.query(testo_query, function () {

        var applica_tutti_articoli = $('#form_gestione_categorie input[name="applica_tutti_articoli"]:visible').is(':checked');
        $('#form_gestione_categorie input[name="applica_tutti_articoli"]:visible').prop('checked', false);
        $('.raggruppamento_applica_tutti_articoli').hide();
        if (applica_tutti_articoli === true) {

            //CATEGORIA VARIANTI

            var iva_2 = "";
            bootbox.confirm("Vuoi modificare l'IVA a tutti gli articoli gi&agrave; inseriti? OK per cambiare a tutti, Cancel per impostare la nuova aliquota gli articoli di prossimo inserimento.", function (risposta) {
                if (risposta === true) {
                    iva_2 = " perc_iva_base='" + iva + "',perc_iva_takeaway='" + iva_takeaway + "',reparto_servizi='" + reparto_servizi + "',reparto_beni='" + reparto_beni + "', ";
                }

                //alasql("SELECT cat_varianti FROM prodotti WHERE categoria='"+id+"' limit 1;")

                var testo_query = "UPDATE prodotti SET " + iva_2 + " cat_varianti='" + cat_var + "'||substr(cat_varianti ,instr(cat_varianti,'/')+1) ,dest_st_1='" + dest_stampa + "',dest_st_2='" + dest_stampa_2 + "',portata='" + portata + "' WHERE categoria='" + id + "' and cat_varianti LIKE '%/%';";
                comanda.sock.send({
                    tipo: "aggiornamento_prodotti",
                    operatore: comanda.operatore,
                    query: testo_query,
                    terminale: comanda.terminale,
                    ip: comanda.ip_address
                }, false);
                alasql(testo_query);
                var testo_query = "UPDATE prodotti SET " + iva_2 + " cat_varianti='" + cat_var + "' ,dest_st_1='" + dest_stampa + "',dest_st_2='" + dest_stampa_2 + "',portata='" + portata + "' WHERE categoria='" + id + "' and cat_varianti NOT LIKE '%/%';";
                comanda.sock.send({
                    tipo: "aggiornamento_prodotti",
                    operatore: comanda.operatore,
                    query: testo_query,
                    terminale: comanda.terminale,
                    ip: comanda.ip_address
                }, false);
                alasql(testo_query);
                var testo_query2 = "UPDATE prodotti SET " + iva_2 + " cat_varianti='" + cat_var + "'||substr(cat_varianti ,instr(cat_varianti,'/')) ,dest_st_1='" + dest_stampa + "',dest_st_2='" + dest_stampa_2 + "',portata='" + portata + "' WHERE categoria='" + id + "' and cat_varianti LIKE '%/%';UPDATE prodotti SET " + iva_2 + " cat_varianti='" + cat_var + "' ,dest_st_1='" + dest_stampa + "',dest_st_2='" + dest_stampa_2 + "',portata='" + portata + "' WHERE categoria='" + id + "' and cat_varianti NOT LIKE '%/%';";
                comanda.sincro.query_cassa(testo_query2, 3000, function () { });
                crea_griglia_prodotti(comanda.dispositivo, function () { });
            });
        }



        selezione_operatore("GESTIONE CATEGORIE");
        elenco_categorie("list", "1", "0", "#tab_sx");
    });

}

function salva_gruppo_statistico(id) {
    var nome = $('#form_gestione_gruppi_statistici input[name="nome"]:visible').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var testo_query = "UPDATE gruppi_statistici SET  nome='" + nome + "' WHERE id='" + id + "';";
    console.log("SALVA GRUPPO STATISTICO", testo_query);
    comanda.sock.send({
        tipo: "aggiornamento_gruppi_statistici",
        operatore: comanda.operatore,
        query: testo_query,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    comanda.sincro.query(testo_query, function () {

        selezione_operatore("gruppi_statistici");
    });

}

function salva_portata(id) {
    let tendini_concomitanzi = [];
    $(".tendina_concomitanza").each((i, v) => { tendini_concomitanzi.push(v.value) })
    let d = JSON.stringify(tendini_concomitanzi);
    console.log(d);
    var descrizione = $('#form_gestione_portate input[name="descrizione"]:visible').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var numero = $('#form_gestione_portate input[name="numero"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    if ($('#form_gestione_portate input[name="ordinamento_stampa"]:visible').val().length === 1) {
        let a = String($('#form_gestione_portate input[name="ordinamento_stampa"]:visible').val()).padStart(2, '0');
        $('#form_gestione_portate input[name="ordinamento_stampa"]:visible').val(a);

    }
    var ordinamento_stampa = $('#form_gestione_portate input[name="ordinamento_stampa"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var visu_articoli_concomitanze = $('#form_gestione_portate input[name="visu_articoli_concomitanze"]:visible').is(':checked') ? 'true' : 'false';

    var attributi_font_concomitanze = $('#form_gestione_portate select[name="attributi_font_concomitanze"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var testo_query = "UPDATE nomi_portate SET  visu_articoli_concomitanze='" + visu_articoli_concomitanze + "',attributi_font_concomitanze='" + attributi_font_concomitanze + "',ordinamento_stampa='" + ordinamento_stampa + "',numero='" + numero + "',concomitanze='" + d + "'," + comanda.lingua_stampa + "='" + descrizione + "' WHERE id='" + id + "';";



    if (d === "[]" && visu_articoli_concomitanze === "true") {
        bootbox.alert("attenzione non sono state aggiunte le Portate Concomitanti");
        visu_articoli_concomitanze = false;
        $('#form_gestione_portate input[name="visu_articoli_concomitanze"]:visible').attr('checked', false); //uncheck della checkbox se è check


    } else {

        console.log("SALVA PORTATA", testo_query);
        comanda.sock.send({
            tipo: "aggiornamento_portate",
            operatore: comanda.operatore,
            query: testo_query,
            terminale: comanda.terminale,
            ip: comanda.ip_address
        });
        comanda.sincro.query(testo_query, function () {
            selezione_operatore("PORTATE");
        });

    }





}

function salva_stampante(id) {
    var nome = $('#form_gestione_stampanti select[name="nome"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var stanza = $('#form_gestione_stampanti input[name="' + comanda.lingua_stampa + '"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var ip = $('#form_gestione_stampanti input[name="ip"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');

    var matricola = $('#form_gestione_stampanti input[name="matricola"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var doppia_comanda = $('#form_gestione_stampanti input[name="comandadoppia"]:visible').is(':checked') ? 'true' : 'false';
    var sds_controllo = $('#form_gestione_stampanti input[name="sds_controllo"]:visible').is(':checked') ? 'true' : 'false';
    if (sds_controllo === "true") {
        var sds_stampa_nome = $('#form_gestione_stampanti input[name="nome_stampa_sds"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    } else {
        var ip_alternativo = $('#form_gestione_stampanti input[name="ip_alternativo"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    }
    switch (nome) {
        case "CUBO":
            var piccola = "N";
            var intelligent = "S";
            var fiscale = "N";
            break;
        case "TMT88-Vi":
            var piccola = "N";
            var intelligent = "S";
            var fiscale = "N";
            break;
        case "P20":
            var piccola = "P";
            var intelligent = "S";
            var fiscale = "N";
            break;
        case "MISURATORE FISCALE":
            var piccola = "N";
            var intelligent = "S";
            var fiscale = "S";
            break;
    }
    if (sds_controllo !== "true") {

        var testo_query = "UPDATE nomi_stampanti SET matricola='" + matricola + "',nome='" + nome.toUpperCase() + "'," + comanda.lingua_stampa + "='" + stanza.toUpperCase() + "',ip='" + ip + "',ip_alternativo='" + ip_alternativo + "',dimensione='" + piccola.toLowerCase() + "',intelligent='" + intelligent.toLowerCase() + "',fiscale='" + fiscale.toLowerCase() + "',doppia='" + doppia_comanda + "',sds_controllo='" + sds_controllo + "',devid_nf='" + "" + "' WHERE id='" + id + "';";
    } else {
        var testo_query = "UPDATE nomi_stampanti SET matricola='" + matricola + "',nome='" + nome.toUpperCase() + "'," + comanda.lingua_stampa + "='" + stanza.toUpperCase() + "',ip='" + ip + "',ip_alternativo='" + ip_alternativo + "',dimensione='" + piccola.toLowerCase() + "',intelligent='" + "SDS" + "',fiscale='" + fiscale.toLowerCase() + "',doppia='" + doppia_comanda + "',sds_controllo='" + sds_controllo + "',devid_nf='" + sds_stampa_nome.toUpperCase() + "' WHERE id='" + id + "';";
    }

    console.log("SALVA STAMPANTE", testo_query);
    //comanda.array_dbcentrale_mancanti.push(testo_query);
    comanda.sock.send({
        tipo: "aggiornamento_stampanti",
        operatore: comanda.operatore,
        query: testo_query,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    comanda.sincro.query(testo_query, function () {

        corrispondenze_stampanti(comanda.lingua_stampa);
        selezione_operatore("GESTIONE STAMPANTI");
    });

}
$(document).on("change", "[name='scelta_diretta_stampante']", function () {
    if (this.checked) {
        document.getElementsByName("associa_tavoli_stampante")[0].checked = false;
        document.getElementsByName("associa_tavoli_stampante")[0].checked = false;
    }
});
$(document).on("change", "[name='associa_tavoli_stampante']", function () {
    if (this.checked) {
        document.getElementsByName("scelta_diretta_stampante")[0].checked = false;
        document.getElementsByName("scelta_diretta_stampante")[0].checked = false;
    }
});
$(document).on('change', '[name="varianti_su_tastiera"]', function () {
    if (this.checked) {
        document.getElementsByName("tastiera_scomparsa_auto")[0].checked = false;
        document.getElementsByName("tastiera_scomparsa_auto")[0].disabled = true;
    } else {
        document.getElementsByName("tastiera_scomparsa_auto")[0].disabled = false;
    }
});
$(document).on('change', '[name="tastiera_ricerca_filtro_plu"]', function () {
    if (this.checked) {
        document.getElementsByName("tastiera_ricerca_tasto_verde_batte_articolo")[0].disabled = false;
    } else {
        document.getElementsByName("tastiera_ricerca_tasto_verde_batte_articolo")[0].checked = false;
        document.getElementsByName("tastiera_ricerca_tasto_verde_batte_articolo")[0].disabled = true;
    }
});
$(document).on('change', '[name="avviso_coperti_comanda"]', function () {
    if (this.checked) {
        document.getElementsByName("avviso_mancati_coperti")[0].checked = true;
    } else {

    }
});
$(document).on('change', '[name="avviso_mancati_coperti"]', function () {
    if (!this.checked) {
        document.getElementsByName("avviso_coperti_comanda")[0].checked = false;
        document.getElementsByName("avviso_coperti_comanda")[0].disabled = true;
    } else {
        document.getElementsByName("avviso_coperti_comanda")[0].disabled = false;
    }
});
$(document).on('change', '[name="tasto_contosep"]', function () {
    if (this.checked) {
        document.getElementsByName("multiquantita")[0].checked = false;
        document.getElementsByName("multiquantita")[0].disabled = true;
    } else {
        document.getElementsByName("multiquantita")[0].disabled = false;
    }
});
$(document).on('change', '[name="multiquantita"]', function () {
    if (this.checked) {
        document.getElementsByName("tasto_contosep")[0].checked = false;
        document.getElementsByName("tasto_contosep")[0].disabled = true;
    } else {
        document.getElementsByName("tasto_contosep")[0].disabled = false;
    }
});
$(document).on('change', '[name="modifica_portata_veloce"]', function () {
    if (this.checked) {
        document.getElementsByName("azzera_prezzo_veloce")[0].checked = false;
    } else {

    }
});
$(document).on('change', '[name="azzera_prezzo_veloce"]', function () {
    if (this.checked) {
        document.getElementsByName("modifica_portata_veloce")[0].checked = false;
    } else {

    }
});
$(document).on('change', '[name="tasto_visu_varianti"]', function () {
    if (this.checked) {

    } else {
        document.getElementsByName("tasto_visu_varianti_menopiu")[0].checked = false;
        document.getElementsByName("tasto_visu_varianti_piumeno")[0].checked = false;
    }
});
$(document).on('change', '[name="tasto_visu_varianti_piumeno"]', function () {
    if (this.checked) {
        document.getElementsByName("tasto_visu_varianti_menopiu")[0].checked = false;
        document.getElementsByName("tasto_visu_varianti")[0].checked = true;
    } else {

    }
});
$(document).on('change', '[name="tasto_visu_varianti_menopiu"]', function () {
    if (this.checked) {
        document.getElementsByName("tasto_visu_varianti")[0].checked = true;
        document.getElementsByName("tasto_visu_varianti_piumeno")[0].checked = false;
    } else {

    }
});

function salva_settaggi(tipologia) {



    if (comanda.compatibile_xml === true) {
        switch (tipologia) {
            case "PRINCIPALI":

                var comandapconto = $('#form_settaggi input[name="comandapconto"]:visible').is(':checked') ? 'S' : 'N';
                var filtro_lettera_bloccato = $('#form_settaggi input[name="filtro_lettera_bloccato"]:visible').is(':checked') ? 'S' : 'N';
                var storicizzazioneauto = $('#form_settaggi input[name="storicizzazioneauto"]:visible').is(':checked') ? 'S' : 'N';
                var incassoauto = $('#form_settaggi input[name="incassoauto"]:visible').is(':checked') ? 'S' : 'N';
                var modalitasagra = $('#form_settaggi input[name="modalitasagra"]:visible').is(':checked') ? 'S' : 'N';
                //var menu = $('#form_settaggi input[name="menu"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var controllo_utente = $('#form_settaggi input[name="controllo_utente"]:visible').is(':checked') ? 'S' : 'N';
                var cancellazione_articolo = $('#form_settaggi input[name="cancellazione_articolo"]:visible').is(':checked') ? 'S' : 'N';
                var pwd_canc_articolo = $('#form_settaggi input[name="pwd_canc_articolo"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var listino_palmari = $('#form_settaggi input[name="listino_palmari"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var blocco_tavolo_cameriere = $('#form_settaggi input[name="blocco_tavolo_cameriere"]:visible').is(':checked') ? 'S' : 'N';

                comanda.filtro_lettera_bloccato = filtro_lettera_bloccato;
                comanda.blocco_tavolo_cameriere = blocco_tavolo_cameriere;
                //menu='" + menu + "',\n\
                var testo_query = "UPDATE settaggi_ibrido SET \n\
                                        filtro_lettera_bloccato='" + filtro_lettera_bloccato + "',\n\
                                        comandapconto='" + comandapconto + "',\n\
                                        storicizzazioneauto='" + storicizzazioneauto + "',\n\
                                        incassoauto='" + incassoauto + "',\n\
                                        modalitasagra='" + modalitasagra + "',\n\
                                        controllo_utente='" + controllo_utente + "',\n\
                                        cancellazione_articolo='" + cancellazione_articolo + "',\n\
                                        blocco_tavolo_cameriere='" + blocco_tavolo_cameriere + "',\n\
                                        pwd_canc_articolo='" + pwd_canc_articolo + "',\n\
                                        listino_palmari='" + listino_palmari + "'\n\
                                        WHERE id='" + comanda.nome_servizio + "';";
                comanda.sincro.query_cassa(testo_query, 10000, function () {
                    comanda.sincro.query(testo_query, function () { });
                });

                break;
            case "LAYOUT_VIDEO":

                var tasto_tavolo_esce = $('#form_settaggi input[name="tasto_tavolo_esce"]:visible').is(':checked') ? 'S' : 'N';
                var ultime_battiture_grandi = $('#form_settaggi input[name="ultime_battiture_grandi"]:visible').is(':checked') ? 'S' : 'N';
                var categorie_fullscreen = $('#form_settaggi input[name="categorie_fullscreen"]:visible').is(':checked') ? 'S' : 'N';
                var layout_destri = $('#form_settaggi input[name="layout_destri"]:visible').is(':checked') ? 'S' : 'N';
                var varianti_riunite = $('#form_settaggi input[name="varianti_riunite"]:visible').is(':checked') ? 'S' : 'N';
                var visualizzazione_coperti = $('#form_settaggi input[name="visualizzazione_coperti"]:visible').is(':checked') ? '1' : '0';
                var avviso_mancati_coperti = $('#form_settaggi input[name="avviso_mancati_coperti"]:visible').is(':checked') ? '1' : '0';
                var avviso_coperti_comanda = $('#form_settaggi input[name="avviso_coperti_comanda"]:visible').is(':checked') ? '1' : '0';
                var slide_qta_articolo = $('#form_settaggi input[name="slide_qta_articolo"]:visible').is(':checked') ? 'S' : 'N';
                var p_libero_abilitato = $('#form_settaggi input[name="p_libero_abilitato"]:visible').is(':checked') ? 'S' : 'N';
                var variante_libera = $('#form_settaggi input[name="variante_libera"]:visible').is(':checked') ? 'S' : 'N';
                var grandezza_font_articoli = $('#form_settaggi input[name="grandezza_font_articoli"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var grandezza_font_categorie = $('#form_settaggi input[name="grandezza_font_categorie"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var grandezza_font_tavoli = $('#form_settaggi input[name="grandezza_font_tavoli"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var azzera_prezzo_veloce = $('#form_settaggi input[name="azzera_prezzo_veloce"]:visible').is(':checked') ? 'S' : 'N';
                var modifica_portata_veloce = $('#form_settaggi input[name="modifica_portata_veloce"]:visible').is(':checked') ? 'S' : 'N';
                var modifica_prodotto = $('#form_settaggi input[name="modifica_prodotto"]:visible').is(':checked') ? 'S' : 'N';
                var modifica_prodotto_descrizione = $('#form_settaggi input[name="modifica_prodotto_descrizione"]:visible').is(':checked') ? 'S' : 'N';
                var modifica_prodotto_quantita = $('#form_settaggi input[name="modifica_prodotto_quantita"]:visible').is(':checked') ? 'S' : 'N';
                var modifica_prodotto_prezzo = $('#form_settaggi input[name="modifica_prodotto_prezzo"]:visible').is(':checked') ? 'S' : 'N';
                var modifica_prodotto_raddoppiaprezzo = $('#form_settaggi input[name="modifica_prodotto_raddoppiaprezzo"]:visible').is(':checked') ? 'S' : 'N';
                var modifica_prodotto_destinazione_stampa = $('#form_settaggi input[name="modifica_prodotto_destinazione_stampa"]:visible').is(':checked') ? 'S' : 'N';
                var modifica_prodotto_portata = $('#form_settaggi input[name="modifica_prodotto_portata"]:visible').is(':checked') ? 'S' : 'N';
                var modifica_prodotto_clona = $('#form_settaggi input[name="modifica_prodotto_clona"]:visible').is(':checked') ? 'S' : 'N';
                var clona_diretto = $('#form_settaggi input[name="clona_diretto"]:visible').is(':checked') ? 'S' : 'N';
                var modifica_prodotto_servito = $('#form_settaggi input[name="modifica_prodotto_servito"]:visible').is(':checked') ? 'S' : 'N';
                var tasto_scontrino = $('#form_settaggi input[name="tasto_scontrino"]:visible').is(':checked') ? 'S' : 'N';
                var tasto_fattura = $('#form_settaggi input[name="tasto_fattura"]:visible').is(':checked') ? 'S' : 'N';
                var tasto_conto = $('#form_settaggi input[name="tasto_conto"]:visible').is(':checked') ? 'S' : 'N';
                var tasto_gutshein = $('#form_settaggi input[name="tasto_gutshein"]:visible').is(':checked') ? 'S' : 'N';
                var tasto_comanda_conto = $('#form_settaggi input[name="tasto_comanda_conto"]:visible').is(':checked') ? 'S' : 'N';
                var tasto_comanda_conto_su_comanda = $('#form_settaggi input[name="tasto_comanda_conto_su_comanda"]:visible').is(':checked') ? 'S' : 'N';
                var multiquantita = $('#form_settaggi input[name="multiquantita"]:visible').is(':checked') ? 'S' : 'N';
                var tasto_contosep = $('#form_settaggi input[name="tasto_contosep"]:visible').is(':checked') ? 'S' : 'N';
                var tasto_sconto = $('#form_settaggi input[name="tasto_sconto"]:visible').is(':checked') ? 'S' : 'N';
                var tasto_incasso = $('#form_settaggi input[name="tasto_incasso"]:visible').is(':checked') ? 'S' : 'N';
                var prevenzione_tocco_accidentale_comanda = $('#form_settaggi input[name="prevenzione_tocco_accidentale_comanda"]:visible').is(':checked') ? 'S' : 'N';
                var tasto_carte_rechnung = $('#form_settaggi input[name="tasto_carte_rechnung"]:visible').is(':checked') ? 'S' : 'N';
                var tasto_incasso_manuale = $('#form_settaggi input[name="tasto_incasso_manuale"]:visible').is(':checked') ? 'S' : 'N';
                var avviso_prezzo_zero = $('#form_settaggi input[name="avviso_prezzo_zero"]:visible').is(':checked') ? '1' : '0';
                var avviso_incasso = $('#form_settaggi input[name="avviso_incasso"]:visible').is(':checked') ? 'S' : 'N';
                var avviso_quittung = $('#form_settaggi input[name="avviso_quittung"]:visible').is(':checked') ? 'S' : 'N';
                var avviso_rechnung = $('#form_settaggi input[name="avviso_rechnung"]:visible').is(':checked') ? 'S' : 'N';
                var tasto_fastconto = $('#form_settaggi input[name="tasto_fastconto"]:visible').is(':checked') ? 'S' : 'N';
                var tastiera_ricerca_partenza_numerica = $('#form_settaggi input[name="tastiera_ricerca_partenza_numerica"]:visible').is(':checked') ? 'S' : 'N';

                var tastiera_ricerca_filtro_plu = $('#form_settaggi input[name="tastiera_ricerca_filtro_plu"]:visible').is(':checked') ? 'S' : 'N';
                var tastiera_ricerca_tasto_verde_batte_articolo = $('#form_settaggi input[name="tastiera_ricerca_partenza_numerica"]:visible').is(':checked') ? 'S' : 'N';

                var ricerca_alfabetica = $('#form_settaggi input[name="ricerca_alfabetica"]:visible').is(':checked') ? 'S' : 'N';
                var ricerca_tastiera = $('#form_settaggi input[name="ricerca_tastiera"]:visible').is(':checked') ? 'S' : 'N';
                var filtro_ricerca_generico = $('#form_settaggi input[name="filtro_ricerca_generico"]:visible').is(':checked') ? 'S' : 'N';
                var cerca_prime_lettere = $('#form_settaggi input[name="cerca_prime_lettere"]:visible').is(':checked') ? 'S' : 'N';
                var tasto_visu_varianti = $('#form_settaggi input[name="tasto_visu_varianti"]:visible').is(':checked') ? 'S' : 'N';
                var tasto_visu_varianti_menopiu = $('#form_settaggi input[name="tasto_visu_varianti_menopiu"]:visible').is(':checked') ? 'S' : 'N';
                var tasto_visu_varianti_piumeno = $('#form_settaggi input[name="tasto_visu_varianti_piumeno"]:visible').is(':checked') ? 'S' : 'N';
                var tutte_le_varianti = $('#form_settaggi input[name="tutte_le_varianti"]:visible').is(':checked') ? 'S' : 'N';
                var tasto_pococotto = $('#form_settaggi input[name="tasto_pococotto"]:visible').is(':checked') ? 'S' : 'N';
                var tasto_finecottura = $('#form_settaggi input[name="tasto_finecottura"]:visible').is(':checked') ? 'S' : 'N';
                var ricerca_su_variante = $('#form_settaggi input[name="ricerca_su_variante"]:visible').is(':checked') ? 'S' : 'N';
                var tasto_gusti_gelato = $('#form_settaggi input[name="tasto_gusti_gelato"]:visible').is(':checked') ? 'S' : 'N';
                var stampante_fastingresso = $('#form_settaggi input[name="stampante_fastingresso"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var tastiera_scomparsa_auto = $('#form_settaggi input[name="tastiera_scomparsa_auto"]:visible').is(':checked') ? 'S' : 'N';
                var chiusura_tastiera_dopo_prima_lettera = $('#form_settaggi input[name="chiusura_tastiera_dopo_prima_lettera"]:visible').is(':checked') ? 'S' : 'N';
                var varianti_su_tastiera = $('#form_settaggi input[name="varianti_su_tastiera"]:visible').is(':checked') ? 'S' : 'N';
                var mantieni_lettera = $('#form_settaggi input[name="mantieni_lettera"]:visible').is(':checked') ? 'S' : 'N';

                comanda.tasto_tavolo_esce = tasto_tavolo_esce;
                comanda.tutte_le_varianti = tutte_le_varianti;
                comanda.chiusura_tastiera_dopo_prima_lettera = chiusura_tastiera_dopo_prima_lettera;
                comanda.ultime_battiture_grandi = ultime_battiture_grandi;
                comanda.categorie_fullscreen = categorie_fullscreen;
                comanda.layout_destri = layout_destri;
                comanda.stampante_fastingresso = stampante_fastingresso;
                comanda.filtro_ricerca_generico = filtro_ricerca_generico;
                comanda.cerca_prime_lettere = cerca_prime_lettere;
                comanda.tastiera_ricerca_partenza_numerica = tastiera_ricerca_partenza_numerica;

                comanda.tastiera_ricerca_filtro_plu = tastiera_ricerca_filtro_plu;
                comanda.tastiera_ricerca_tasto_verde_batte_articolo = tastiera_ricerca_tasto_verde_batte_articolo;

                comanda.ricerca_alfabetica = ricerca_alfabetica;
                comanda.ricerca_tastiera = ricerca_tastiera;
                $("#btn_ricerca_tastiera").hide();
                if (comanda.ricerca_tastiera === "S") {
                    $("#btn_ricerca_tastiera").show();
                }
                comanda.varianti_riunite = varianti_riunite;
                comanda.tasto_rechnung = tasto_fattura;
                comanda.tasto_quittung = tasto_scontrino;
                comanda.tasto_konto = tasto_conto;
                comanda.tasto_gutshein = tasto_gutshein;
                comanda.multiquantita = multiquantita;
                comanda.tasto_contosep = tasto_contosep;
                comanda.tasto_sconto = tasto_sconto;
                comanda.avviso_righe_prezzo_zero = avviso_prezzo_zero;
                comanda.tasto_bargeld = tasto_incasso;
                comanda.prevenzione_tocco_accidentale_comanda = prevenzione_tocco_accidentale_comanda;
                comanda.tasto_carte_rechnung = tasto_carte_rechnung;
                comanda.tasto_incasso = tasto_incasso_manuale;
                comanda.avviso_bargeld = avviso_incasso;
                comanda.avviso_quittung = avviso_quittung;
                comanda.avviso_rechnung = avviso_rechnung;
                comanda.tasto_fastconto = tasto_fastconto;
                comanda.tasto_pococotto = tasto_pococotto;
                comanda.tasto_finecottura = tasto_finecottura;
                comanda.ricerca_su_variante = ricerca_su_variante;
                comanda.tasto_gusti_gelato = tasto_gusti_gelato;
                comanda.tasto_comanda_konto = tasto_comanda_conto;
                comanda.tasto_comanda_konto_su_comanda = tasto_comanda_conto_su_comanda;
                comanda.slide_qta_articolo = slide_qta_articolo;
                comanda.clona_diretto = clona_diretto;
                comanda.coperti_obbligatorio = avviso_mancati_coperti;
                comanda.avviso_coperti_comanda = avviso_coperti_comanda;
                if (comanda.coperti_obbligatorio === '1') {
                    comanda.coperti_necessari === true;
                } else {
                    comanda.coperti_necessari === false;
                }

                var testo_query1 = "UPDATE impostazioni_fiscali SET \n\
                                        righe_zero='" + avviso_prezzo_zero + "',\n\
                                        tasto_visu_varianti='" + tasto_visu_varianti + "',\n\
                                        tasto_visu_varianti_menopiu='" + tasto_visu_varianti_menopiu + "',\n\
                                        tasto_visu_varianti_piumeno='" + tasto_visu_varianti_piumeno + "',\n\
                                        filtro_ricerca_generico='" + filtro_ricerca_generico + "',\n\
                                        tastiera_ricerca_partenza_numerica='" + tastiera_ricerca_partenza_numerica + "',\n\
                                        tastiera_ricerca_filtro_plu='" + tastiera_ricerca_filtro_plu + "',\n\
                                        tastiera_ricerca_tasto_verde_batte_articolo='" + tastiera_ricerca_tasto_verde_batte_articolo + "',\n\
                                        ricerca_alfabetica='" + ricerca_alfabetica + "',\n\
                                        ricerca_tastiera='" + ricerca_tastiera + "',\n\
                                        varianti_riunite='" + varianti_riunite + "',\n\
                                        coperti_obbligatorio='" + avviso_mancati_coperti + "',\n\
                                        avviso_coperti_comanda='" + avviso_coperti_comanda + "',\n\
                                        clona_diretto='" + clona_diretto + "',\n\
                                        visualizzazione_coperti='" + visualizzazione_coperti + "',\n\
                                        grandezza_font_articoli='" + grandezza_font_articoli + "',\n\
                                        grandezza_font_categorie='" + grandezza_font_categorie + "',\n\
                                        grandezza_font_tavoli='" + grandezza_font_tavoli + "',\n\
                                        multiquantita='" + multiquantita + "'\n\
                                         where id=" + comanda.folder_number + " ;";
                comanda.sincro.query_cassa(testo_query1, 10000, function () {
                    comanda.sincro.query(testo_query1, function () { });
                });
                /*var modifica_prodotto = $('#form_settaggi input[name="modifica_prodotto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                 var modifica_prodotto_descrizione = $('#form_settaggi input[name="modifica_prodotto_descrizione"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                 var modifica_prodotto_quantita = $('#form_settaggi input[name="modifica_prodotto_quantita"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                 var modifica_prodotto_prezzo = $('#form_settaggi input[name="modifica_prodotto_prezzo"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                 var modifica_prodotto_destinazione_stampa = $('#form_settaggi input[name="modifica_prodotto_destinazione_stampa"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                 var modifica_prodotto_portata = $('#form_settaggi input[name="modifica_prodotto_portata"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                 var modifica_prodotto_clona = $('#form_settaggi input[name="modifica_prodotto_clona"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                 var modifica_prodotto_servito = $('#form_settaggi input[name="modifica_prodotto_servito"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');*/

                var testo_query2 = "UPDATE settaggi_ibrido SET \n\
                                        tasto_tavolo_esce='" + tasto_tavolo_esce + "',\n\
                                        tutte_le_varianti='" + tutte_le_varianti + "',\n\
                                        tasto_carte_rechnung='" + tasto_carte_rechnung + "',\n\
                                        prevenzione_tocco_accidentale_comanda='" + prevenzione_tocco_accidentale_comanda + "',\n\
                                        varianti_su_tastiera='" + varianti_su_tastiera + "',\n\
                                        mantieni_lettera='" + mantieni_lettera + "',\n\
                                        ultime_battiture_grandi='" + ultime_battiture_grandi + "',\n\
                                        categorie_fullscreen='" + categorie_fullscreen + "',\n\
                                        layout_destri='" + layout_destri + "',\n\
                                        cerca_prime_lettere='" + cerca_prime_lettere + "',\n\
                                        slide_qta_articolo='" + slide_qta_articolo + "',\n\
                                        azzera_prezzo_veloce='" + azzera_prezzo_veloce + "',\n\
                                        modifica_portata_veloce='" + modifica_portata_veloce + "',\n\
                                        modifica_prodotto='" + modifica_prodotto + "',\n\
                                        modifica_prodotto_descrizione='" + modifica_prodotto_descrizione + "',\n\
                                        modifica_prodotto_quantita='" + modifica_prodotto_quantita + "',\n\
                                        modifica_prodotto_prezzo='" + modifica_prodotto_prezzo + "',\n\
                                        modifica_prodotto_raddoppiaprezzo='" + modifica_prodotto_raddoppiaprezzo + "',\n\
                                        modifica_prodotto_destinazione_stampa='" + modifica_prodotto_destinazione_stampa + "',\n\
                                        modifica_prodotto_portata='" + modifica_prodotto_portata + "',\n\
                                        modifica_prodotto_clona='" + modifica_prodotto_clona + "',\n\
                                        modifica_prodotto_servito='" + modifica_prodotto_servito + "',\n\
                                        tasto_scontrino='" + tasto_scontrino + "',\n\
                                        tasto_fattura='" + tasto_fattura + "',\n\
                                        tasto_conto='" + tasto_conto + "',\n\
                                        tasto_gutshein='" + tasto_gutshein + "',\n\
                                        tasto_contosep='" + tasto_contosep + "',\n\
                                        tasto_sconto ='" + tasto_sconto + "',\n\
                                        tasto_incasso='" + tasto_incasso + "',\n\
                                        tasto_incasso_manuale='" + tasto_incasso_manuale + "',\n\
                                        avviso_incasso='" + avviso_incasso + "',\n\
                                        avviso_quittung='" + avviso_quittung + "',\n\
                                        avviso_rechnung='" + avviso_rechnung + "',\n\
                                        tasto_fastconto='" + tasto_fastconto + "',\n\
                                        tasto_pococotto='" + tasto_pococotto + "',\n\
                                        tasto_gusti_gelato='" + tasto_gusti_gelato + "',\n\
                                        tasto_finecottura='" + tasto_finecottura + "',\n\
                                        ricerca_su_variante='" + ricerca_su_variante + "',\n\
                                        p_libero_abilitato='" + p_libero_abilitato + "',\n\
                                        variante_libera='" + variante_libera + "',\n\
                                        tasto_comanda_conto='" + tasto_comanda_conto + "',\n\
                                        tasto_comanda_conto_su_comanda='" + tasto_comanda_conto_su_comanda + "',\n\
                                        chiusura_tastiera_dopo_prima_lettera ='" + chiusura_tastiera_dopo_prima_lettera + "',\n\
                                        tastiera_scomparsa_auto='" + tastiera_scomparsa_auto + "',\n\
                                        stampante_fastingresso='" + stampante_fastingresso + "'\n\
                                        WHERE id='" + comanda.nome_servizio + "';";
                comanda.sincro.query_cassa(testo_query2, 10000, function () {
                    comanda.sincro.query(testo_query2, function () { });
                });
                break;
        }
    } else if (comanda.pizzeria_asporto === true) {
        switch (tipologia) {
            case "COMANDA":

                var testo_queryYY = alasql("SELECT  Field22, Field149 FROM settaggi_profili    WHERE id = 1;");

                if (testo_queryYY[0].Field22 === "false" && testo_queryYY[0].Field149 === "false") {
                    $('#form_gestione_portate input[name="visu_articoli_concomitanze"]:visible').hide();
                    $('#aggiungi_con').hide();
                    $('#form_gestione_portate select[name="attributi_font_concomitanze"]:visible').hide();
                    $('#portate_con').hide();
                    $('#visualizza_art_con').hide();
                    $('#attributi_font_con').hide();


                }

                var scritta_consegna_domicilio = $('#form_settaggi input[name="scritta_consegna_domicilio"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var scritta_consegna_ritiro = $('#form_settaggi input[name="scritta_consegna_ritiro"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var scritta_consegna_pizzeria = $('#form_settaggi input[name="scritta_consegna_pizzeria"]:visible').val();
                if (scritta_consegna_pizzeria === undefined) {
                    scritta_consegna_pizzeria = "";
                }
                scritta_consegna_pizzeria = scritta_consegna_pizzeria.replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga1 = $('#form_settaggi input[name="intestazione_riga1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga1_grassetto = $('#form_settaggi select[name="intestazione_riga1_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spaziointestazione1 = $('#form_settaggi input[name="spaziointestazione1"]:visible').is(':checked');
                var intestazione_riga2 = $('#form_settaggi input[name="intestazione_riga2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga2_grassetto = $('#form_settaggi select[name="intestazione_riga2_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spaziointestazione2 = $('#form_settaggi input[name="spaziointestazione2"]:visible').is(':checked');
                var intestazione_riga3 = $('#form_settaggi input[name="intestazione_riga3"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga3_grassetto = $('#form_settaggi select[name="intestazione_riga3_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spaziointestazione3 = $('#form_settaggi input[name="spaziointestazione3"]:visible').is(':checked');
                var intestazione_riga4 = $('#form_settaggi input[name="intestazione_riga4"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga4_grassetto = $('#form_settaggi select[name="intestazione_riga4_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var titolo = $('#form_settaggi input[name="titolo"]:visible').is(':checked');
                var nome_stampante = $('#form_settaggi input[name="nome_stampante"]:visible').is(':checked');
                var operatore = $('#form_settaggi input[name="operatore"]:visible').is(':checked');
                var font_operatore = $('#form_settaggi select[name="font_operatore"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var data = $('#form_settaggi input[name="data"]:visible').is(':checked');
                var font_data = $('#form_settaggi select[name="font_data"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var ora = $('#form_settaggi input[name="ora"]:visible').is(':checked');
                var font_ora = $('#form_settaggi select[name="font_ora"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var portate = $('#form_settaggi input[name="portate"]:visible').is(':checked');
                var portate_grassetto = $('#form_settaggi select[name="portate_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var prezzo = $('#form_settaggi input[name="prezzo"]:visible').is(':checked');
                var totale_prezzo_articoli = $('#form_settaggi input[name="totale_prezzo_articoli"]:visible').is(':checked');
                var font_dati_cliente = $('#form_settaggi select[name="font_data_cliente"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var font_articolo = $('#form_settaggi select[name="font_articolo"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var font_varianti = $('#form_settaggi select[name="font_varianti"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var unione_articoli_varianti = $('#form_settaggi input[name="unione_articoli_varianti"]:visible').is(':checked');
                var ricetta = $('#form_settaggi input[name="ricetta"]:visible').is(':checked');
                var totale_pizze = $('#form_settaggi input[name="totale_pizze"]:visible').is(':checked');
                var totale_bibite = $('#form_settaggi input[name="totale_bibite"]:visible').is(':checked');
                var totale_quantita_articoli = $('#form_settaggi input[name="totale_quantita_articoli"]:visible').is(':checked');
                var msg_riga1 = $('#form_settaggi input[name="msg_riga1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga1_grassetto = $('#form_settaggi select[name="msg_riga1_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga1_allineamento = $('#form_settaggi select[name="msg_riga1_allineamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spazio1 = $('#form_settaggi input[name="spazio1"]:visible').is(':checked');
                var msg_riga2 = $('#form_settaggi input[name="msg_riga2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga2_grassetto = $('#form_settaggi select[name="msg_riga2_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga2_allineamento = $('#form_settaggi select[name="msg_riga2_allineamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spazio2 = $('#form_settaggi input[name="spazio2"]:visible').is(':checked');
                var msg_riga3 = $('#form_settaggi input[name="msg_riga3"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga3_grassetto = $('#form_settaggi select[name="msg_riga3_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga3_allineamento = $('#form_settaggi select[name="msg_riga3_allineamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var concomitanze = $('#form_settaggi input[name="concomitanze"]:visible').is(':checked');
                var dati_cliente = $('#form_settaggi input[name="dati_cliente"]:visible').is(':checked');
                var qrcode_navigatore = $('#form_settaggi input[name="qrcode_navigatore"]:visible').is(':checked');
                var meno_specificato = $('#form_settaggi input[name="meno_specificato"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var variante_meno_evidenziata = $('#form_settaggi input[name="variante_meno_evidenziata"]:visible').is(':checked');
                var variante_piu_evidenziata = $('#form_settaggi input[name="variante_piu_evidenziata"]:visible').is(':checked');
                var variante_poco_evidenziata = $('#form_settaggi input[name="variante_poco_evidenziata"]:visible').is(':checked');
                var variante_fine_cottura_evidenziata = $('#form_settaggi input[name="variante_fine_cottura_evidenziata"]:visible').is(':checked');
                var specifica_evidenziata = $('#form_settaggi input[name="specifica_evidenziata"]:visible').is(':checked');
                var ricetta = $('#form_settaggi input[name="ricetta"]:visible').is(':checked');
                var ricetta_grassetto = $('#form_settaggi  select[name="ricetta_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var ricetta_maiuscola = $('#form_settaggi input[name="ricetta_maiuscola"]:visible').is(':checked');
                var articoli_evidenziati_cat_1 = $('#form_settaggi  select[name="articoli_evidenziati_cat_1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var articoli_evidenziati_cat_2 = $('#form_settaggi  select[name="articoli_evidenziati_cat_2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var bool_orario_comanda_visu_alta = $('#form_settaggi input[name="bool_orario_comanda_visu_alta"]:visible').is(':checked');
                var bool_orario_comanda_visu_bassa = $('#form_settaggi input[name="bool_orario_comanda_visu_bassa"]:visible').is(':checked');
                var tipo_orario_comanda_visu_alta = $('#form_settaggi  select[name="tipo_orario_comanda_visu_alta"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var tipo_orario_comanda_visu_bassa = $('#form_settaggi  select[name="tipo_orario_comanda_visu_bassa"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var barcode_riapertura_ordine_comanda = $('#form_settaggi input[name="barcode_riapertura_ordine_comanda"]:visible').is(':checked');
                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field498='" + barcode_riapertura_ordine_comanda + "',\n\
                                        Field469='" + bool_orario_comanda_visu_alta + "',\n\
                                        Field470='" + bool_orario_comanda_visu_bassa + "',\n\
                                        Field471='" + tipo_orario_comanda_visu_alta + "',\n\
                                        Field472='" + tipo_orario_comanda_visu_bassa + "',\n\
                                        Field383='" + articoli_evidenziati_cat_1 + "',\n\
                                        Field384='" + articoli_evidenziati_cat_2 + "',\n\
                                        Field381='" + totale_pizze + "',\n\
                                        Field382='" + totale_bibite + "',\n\
                                        Field378='" + ricetta + "',\n\
                                        Field379='" + ricetta_maiuscola + "',\n\
                                        Field380='" + ricetta_grassetto + "',\n\
                                        \n\
                                        Field375='" + scritta_consegna_domicilio + "',\n\
                                        Field376='" + scritta_consegna_ritiro + "',\n\
                                        Field377='" + scritta_consegna_pizzeria + "',\n\
                                        Field111='" + intestazione_riga1 + "',\n\
                                        Field112 ='" + intestazione_riga1_grassetto + "', \n\
                                        Field113 ='" + spaziointestazione1 + "', \n\
                                        Field114='" + intestazione_riga2 + "',\n\
                                        Field115 ='" + intestazione_riga2_grassetto + "', \n\
                                        Field116 ='" + spaziointestazione2 + "', \n\
                                        Field117='" + intestazione_riga3 + "',\n\
                                        Field118 ='" + intestazione_riga3_grassetto + "', \n\
                                        Field119 ='" + spaziointestazione3 + "', \n\
                                        Field120='" + intestazione_riga4 + "',\n\
                                        Field121 ='" + intestazione_riga4_grassetto + "', \n\
                                        \n\
                                        Field122 ='" + titolo + "',\n\
                                        Field123 ='" + nome_stampante + "', \n\
                                        Field124 ='" + operatore + "', \n\
                                        Field125  ='" + font_operatore + "',\n\
                                        Field126 ='" + data + "', \n\
                                        Field127  ='" + font_data + "',\n\
                                        Field128 ='" + ora + "', \n\
                                        Field129  ='" + font_ora + "',\n\
                                        Field130 ='" + portate + "',\n\
                                        Field131  ='" + portate_grassetto + "',\n\
                                        Field132 ='" + prezzo + "', \n\
                                        Field189 ='" + totale_prezzo_articoli + "', \n\
                                        Field512 ='" + font_dati_cliente + "', \n\
                                        Field133  ='" + font_articolo + "',\n\
                                        Field134  ='" + font_varianti + "',\n\
                                        Field135 ='" + unione_articoli_varianti + "', \n\
                                        Field136 ='" + ricetta + "', \n\
                                        Field137 ='" + totale_quantita_articoli + "', \n\
                                        Field138 ='" + msg_riga1 + "', \n\
                                        Field139 ='" + msg_riga1_grassetto + "', \n\
                                        Field140 ='" + msg_riga1_allineamento + "', \n\
                                        Field141 ='" + spazio1 + "', \n\
                                        Field142 ='" + msg_riga2 + "', \n\
                                        Field143 ='" + msg_riga2_grassetto + "', \n\
                                        Field144 ='" + msg_riga2_allineamento + "', \n\
                                        Field145 ='" + spazio2 + "', \n\
                                        Field146 ='" + msg_riga3 + "', \n\
                                        Field147 ='" + msg_riga3_grassetto + "', \n\
                                        Field148 ='" + msg_riga3_allineamento + "', \n\
                                        Field149 ='" + concomitanze + "', \n\
                                        Field252 ='" + dati_cliente + "', \n\
                                        Field307 ='" + meno_specificato + "', \n\
                                        Field310 ='" + variante_meno_evidenziata + "', \n\
                                        Field422 ='" + variante_poco_evidenziata + "', \n\
                                        Field423 ='" + variante_fine_cottura_evidenziata + "', \n\
                                        Field410 ='" + variante_piu_evidenziata + "', \n\
                                        Field424 ='" + specifica_evidenziata + "', \n\
                                        Field253 ='" + qrcode_navigatore + "' \n\
                                         where id=" + comanda.folder_number + " ;";
                let resultato = "SELECT * FROM settaggi_profili  where id=" + comanda.folder_number + "  LIMIT 1;"
                let testo = alasql(resultato);
                if (barcode_riapertura_ordine_comanda.toString() === "true" || testo[0].Field499 === "true" || testo[0].Field500 === "true") {
                    $(".fa-barcode").show();
                } else {
                    $(".fa-barcode").hide();
                }
                comanda.scritta_consegna_domicilio = scritta_consegna_domicilio;
                comanda.scritta_consegna_ritiro = scritta_consegna_ritiro;
                comanda.scritta_consegna_pizzeria = scritta_consegna_pizzeria;
                comanda.meno_specificato = meno_specificato;
                comanda.specifica_evidenziata = specifica_evidenziata;
                comanda.variante_meno_evidenziata = variante_meno_evidenziata;
                comanda.variante_poco_evidenziata = variante_poco_evidenziata;
                comanda.variante_fine_cottura_evidenziata = variante_fine_cottura_evidenziata;
                comanda.variante_piu_evidenziata = variante_piu_evidenziata;
                break;
            case "CONTO":

                var intestazione_riga1 = $('#form_settaggi input[name="intestazione_riga1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga1_grassetto = $('#form_settaggi select[name="intestazione_riga1_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spaziointestazione1 = $('#form_settaggi input[name="spaziointestazione1"]:visible').is(':checked');
                var intestazione_riga2 = $('#form_settaggi input[name="intestazione_riga2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga2_grassetto = $('#form_settaggi select[name="intestazione_riga2_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spaziointestazione2 = $('#form_settaggi input[name="spaziointestazione2"]:visible').is(':checked');
                var intestazione_riga3 = $('#form_settaggi input[name="intestazione_riga3"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga3_grassetto = $('#form_settaggi select[name="intestazione_riga3_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spaziointestazione3 = $('#form_settaggi input[name="spaziointestazione3"]:visible').is(':checked');
                var intestazione_riga4 = $('#form_settaggi input[name="intestazione_riga4"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga4_grassetto = $('#form_settaggi select[name="intestazione_riga4_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var titolo = $('#form_settaggi input[name="titolo"]:visible').is(':checked');
                var operatore = $('#form_settaggi input[name="operatore"]:visible').is(':checked');
                var font_operatore = $('#form_settaggi select[name="font_operatore"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var data = $('#form_settaggi input[name="data"]:visible').is(':checked');
                var font_data = $('#form_settaggi select[name="font_data"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var ora = $('#form_settaggi input[name="ora"]:visible').is(':checked');
                var font_ora = $('#form_settaggi select[name="font_ora"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var font_articolo = $('#form_settaggi select[name="font_articolo"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var font_varianti = $('#form_settaggi select[name="font_varianti"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var totale_quantita_articoli = $('#form_settaggi input[name="totale_quantita_articoli"]:visible').is(':checked');
                var msg_riga1 = $('#form_settaggi input[name="msg_riga1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga1_grassetto = $('#form_settaggi select[name="msg_riga1_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga1_allineamento = $('#form_settaggi select[name="msg_riga1_allineamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spazio1 = $('#form_settaggi input[name="spazio1"]:visible').is(':checked');
                var msg_riga2 = $('#form_settaggi input[name="msg_riga2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga2_grassetto = $('#form_settaggi select[name="msg_riga2_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga2_allineamento = $('#form_settaggi select[name="msg_riga2_allineamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spazio2 = $('#form_settaggi input[name="spazio2"]:visible').is(':checked');
                var msg_riga3 = $('#form_settaggi input[name="msg_riga3"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga3_grassetto = $('#form_settaggi select[name="msg_riga3_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga3_allineamento = $('#form_settaggi select[name="msg_riga3_allineamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var doppia_copia = $('#form_settaggi input[name="doppia_copia"]:visible').is(':checked');
                var qrcode_navigatore = $('#form_settaggi input[name="qrcode_navigatore"]:visible').is(':checked');
                var nome_cliente = $('#form_settaggi input[name="nome_cliente"]:visible').is(':checked');
                var tutti_dati_clienti = $('#form_settaggi input[name="tutti_dati_clienti"]:visible').is(':checked');
                var ora_consegna = $('#form_settaggi input[name="ora_consegna"]:visible').is(':checked');
                var barcode_riapertura_ordine_conto = $('#form_settaggi input[name="barcode_riapertura_ordine_conto"]:visible').is(':checked');
                var articoli_evidenziati_cat_1 = $('#form_settaggi  select[name="articoli_evidenziati_cat_1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var articoli_evidenziati_cat_2 = $('#form_settaggi  select[name="articoli_evidenziati_cat_2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var font_dati_cliente_conto = $('#form_settaggi select[name="font_data_cliente_conto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field393='" + articoli_evidenziati_cat_1 + "',\n\
                                        Field394='" + articoli_evidenziati_cat_2 + "',\n\
                                        Field150='" + intestazione_riga1 + "',\n\
                                        Field151 ='" + intestazione_riga1_grassetto + "', \n\
                                        Field152 ='" + spaziointestazione1 + "', \n\
                                        Field153 ='" + intestazione_riga2 + "',\n\
                                        Field154 ='" + intestazione_riga2_grassetto + "', \n\
                                        Field155 ='" + spaziointestazione2 + "', \n\
                                        Field156 ='" + intestazione_riga3 + "',\n\
                                        Field157 ='" + intestazione_riga3_grassetto + "', \n\
                                        Field158 ='" + spaziointestazione3 + "', \n\
                                        Field159 ='" + intestazione_riga4 + "',\n\
                                        Field160 ='" + intestazione_riga4_grassetto + "', \n\
                                        \n\
                                        Field161 ='" + titolo + "',\n\
                                        Field162 ='" + operatore + "', \n\
                                        Field163  ='" + font_operatore + "',\n\
                                        Field164 ='" + data + "', \n\
                                        Field165  ='" + font_data + "',\n\
                                        Field166 ='" + ora + "', \n\
                                        Field167  ='" + font_ora + "',\n\
                                        \n\
                                        Field168  ='" + font_articolo + "',\n\
                                        Field169  ='" + font_varianti + "',\n\
                                        \n\
                                        Field170 ='" + totale_quantita_articoli + "', \n\
                                        Field171 ='" + msg_riga1 + "', \n\
                                        Field172 ='" + msg_riga1_grassetto + "', \n\
                                        Field173 ='" + msg_riga1_allineamento + "', \n\
                                        Field174 ='" + spazio1 + "', \n\
                                        Field175 ='" + msg_riga2 + "', \n\
                                        Field176 ='" + msg_riga2_grassetto + "', \n\
                                        Field177 ='" + msg_riga2_allineamento + "', \n\
                                        Field178 ='" + spazio2 + "', \n\
                                        Field179 ='" + msg_riga3 + "', \n\
                                        Field180='" + msg_riga3_grassetto + "', \n\
                                        Field181 ='" + msg_riga3_allineamento + "', \n\
                                        Field230 ='" + qrcode_navigatore + "', \n\
                                        Field323 ='" + nome_cliente + "', \n\
                                        Field324 ='" + tutti_dati_clienti + "', \n\
                                        Field325 ='" + ora_consegna + "', \n\
                                        Field513 ='" + font_dati_cliente_conto + "', \n\
                                        Field499 ='" + barcode_riapertura_ordine_conto + "' \n\
                                         where id=" + comanda.folder_number + " ;";
                let resultt = "SELECT * FROM settaggi_profili  where id=" + comanda.folder_number + "  LIMIT 1;"
                let test = alasql(resultt);

                if (barcode_riapertura_ordine_conto.toString() === "true" || test[0].Field498 === "true" || test[0].Field500 === "true") {
                    $(".fa-barcode").show();
                } else {
                    $(".fa-barcode").hide();
                }
                break;
            case "DETTAGLIO VENDUTI":

                var intestazione_riga1 = $('#form_settaggi input[name="intestazione_riga1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga1_grassetto = $('#form_settaggi select[name="intestazione_riga1_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spaziointestazione1 = $('#form_settaggi input[name="spaziointestazione1"]:visible').is(':checked');
                var intestazione_riga2 = $('#form_settaggi input[name="intestazione_riga2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga2_grassetto = $('#form_settaggi select[name="intestazione_riga2_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spaziointestazione2 = $('#form_settaggi input[name="spaziointestazione2"]:visible').is(':checked');
                var intestazione_riga3 = $('#form_settaggi input[name="intestazione_riga3"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga3_grassetto = $('#form_settaggi select[name="intestazione_riga3_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spaziointestazione3 = $('#form_settaggi input[name="spaziointestazione3"]:visible').is(':checked');
                var intestazione_riga4 = $('#form_settaggi input[name="intestazione_riga4"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga4_grassetto = $('#form_settaggi select[name="intestazione_riga4_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var report = $('#form_settaggi input[name="report"]:visible').is(':checked');
                var report_grassetto = $('#form_settaggi input[name="report_grassetto"]:visible').is(':checked');
                var creatoil = $('#form_settaggi input[name="creatoil"]:visible').is(':checked');
                var creatoil_grassetto = $('#form_settaggi input[name="creatoil_grassetto"]:visible').is(':checked');
                var ordinamento_articoli = $('#form_settaggi select[name="ordinamento_articoli"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var ordinamento_crescente_decrescente = $('#form_settaggi select[name="ordinamento_crescente_decrescente"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var totaliservizio = $('#form_settaggi input[name="totaliservizio"]:visible').is(':checked');
                var totalioperatore = $('#form_settaggi input[name="totalioperatore"]:visible').is(':checked');
                var totalegenerale = $('#form_settaggi input[name="totalegenerale"]:visible').is(':checked');
                var divisionegruppi = $('#form_settaggi input[name="divisionegruppi"]:visible').is(':checked');
                var divisionecategorie = $('#form_settaggi input[name="divisionecategorie"]:visible').is(':checked');
                var divisionedestinazione = $('#form_settaggi input[name="divisionedestinazione"]:visible').is(':checked');
                var percentuali = $('#form_settaggi input[name="percentuali"]:visible').is(':checked');
                var sconti = $('#form_settaggi input[name="sconti"]:visible').is(':checked');
                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field81='" + intestazione_riga1 + "',\n\
                                        Field82='" + intestazione_riga1_grassetto + "',\n\
                                        Field83='" + spaziointestazione1 + "',\n\
                                        Field84='" + intestazione_riga2 + "',\n\
                                        Field85='" + intestazione_riga2_grassetto + "',\n\
                                        Field86='" + spaziointestazione2 + "',\n\
                                        Field87='" + intestazione_riga3 + "',\n\
                                        Field88='" + intestazione_riga3_grassetto + "',\n\
                                        Field89='" + spaziointestazione3 + "',\n\
                                        Field90='" + intestazione_riga4 + "',\n\
                                        Field91='" + intestazione_riga4_grassetto + "',\n\
                                        Field92='" + report + "',\n\
                                        Field104='" + report_grassetto + "',\n\
                                        Field93='" + creatoil + "',\n\
                                        Field94='" + creatoil_grassetto + "',\n\
                                        Field95='" + ordinamento_articoli + "',\n\
                                        Field96='" + ordinamento_crescente_decrescente + "',\n\
                                        Field97='" + totaliservizio + "',\n\
                                        Field98='" + totalioperatore + "',\n\
                                        Field99='" + totalegenerale + "',\n\
                                        Field412='" + divisionegruppi + "',\n\
                                        Field100='" + divisionecategorie + "',\n\
                                        Field101='" + divisionedestinazione + "',\n\
                                        Field102='" + percentuali + "',\n\
                                        Field103='" + sconti + "'\n\
                                         where id=" + comanda.folder_number + " ;";
                break;
            case "ANAGRAFICA PRODOTTI":

                var descrizione = $('#form_settaggi input[name="descrizione"]:visible').is(':checked');
                var spazio_forno = $('#form_settaggi input[name="spazio_forno"]:visible').is(':checked');
                var prezzo_normale = $('#form_settaggi input[name="prezzo_normale"]:visible').is(':checked');
                var prezzo_maxi = $('#form_settaggi input[name="prezzo_maxi"]:visible').is(':checked');
                var prezzo_bar = $('#form_settaggi input[name="prezzo_bar"]:visible').is(':checked');
                var prezzo_asporto = $('#form_settaggi input[name="prezzo_asporto"]:visible').is(':checked');
                var prezzo_continuo = $('#form_settaggi input[name="prezzo_continuo"]:visible').is(':checked');
                var prezzo_metro = $('#form_settaggi input[name="prezzo_metro"]:visible').is(':checked');
                var unita_misura = $('#form_settaggi input[name="unita_misura"]:visible').is(':checked');
                var prezzo_unita = $('#form_settaggi input[name="prezzo_unita"]:visible').is(':checked');
                var destinazione_stampa = $('#form_settaggi input[name="destinazione_stampa"]:visible').is(':checked');
                var destinazione_stampa_2 = $('#form_settaggi input[name="destinazione_stampa_2"]:visible').is(':checked');
                var portata = $('#form_settaggi input[name="portata"]:visible').is(':checked');
                var categoria_varianti = $('#form_settaggi input[name="categoria_varianti"]:visible').is(':checked');
                var varianti_automatiche = $('#form_settaggi input[name="varianti_automatiche"]:visible').is(':checked');
                var iva_base = $('#form_settaggi input[name="iva_base"]:visible').is(':checked');
                var iva_takeaway = $('#form_settaggi input[name="iva_takeaway"]:visible').is(':checked');
                var riepilogo = $('#form_settaggi input[name="riepilogo"]:visible').is(':checked');
                var codice_promozionale = $('#form_settaggi input[name="codice_promozionale"]:visible').is(':checked');
                var prezzo_poco = $('#form_settaggi input[name="prezzo_poco"]:visible').is(':checked');
                var prezzo_variante_maxi = $('#form_settaggi input[name="prezzo_variante_maxi"]:visible').is(':checked');
                var prezzo_varianti_aggiuntive = $('#form_settaggi input[name="prezzo_varianti_aggiuntive"]:visible').is(':checked');
                var prezzo_maxi_aggiuntive = $('#form_settaggi input[name="prezzo_maxi_aggiuntive"]:visible').is(':checked');
                var testo_query = "UPDATE settaggi_profili SET \n\
                                        Field330='" + descrizione + "',\n\
                                        Field331='" + spazio_forno + "',\n\
                                        Field332='" + prezzo_normale + "',\n\
                                        Field333='" + prezzo_maxi + "',\n\
                                        Field334='" + prezzo_bar + "',\n\
                                        Field335='" + prezzo_asporto + "',\n\
                                        Field336='" + prezzo_continuo + "',\n\
                                        Field337='" + prezzo_metro + "',\n\
                                        Field338='" + unita_misura + "',\n\
                                        Field339='" + prezzo_unita + "',\n\
                                        Field340='" + destinazione_stampa + "',\n\
                                        Field341='" + destinazione_stampa_2 + "',\n\
                                        Field342='" + portata + "',\n\
                                        Field343='" + categoria_varianti + "',\n\
                                        Field344='" + varianti_automatiche + "',\n\
                                        Field345='" + iva_base + "',\n\
                                        Field346='" + iva_takeaway + "',\n\
                                        Field347='" + riepilogo + "',\n\
                                        Field348='" + codice_promozionale + "',\n\
                                        Field349='" + prezzo_poco + "',\n\
                                        Field350='" + prezzo_variante_maxi + "',\n\
                                        Field351='" + prezzo_varianti_aggiuntive + "',\n\
                                        Field352='" + prezzo_maxi_aggiuntive + "'\n\
                                        WHERE id=" + comanda.folder_number + " ;";
                break;
            case "STAMPE_COMANDA":

                var comandapiuconto = $('#form_settaggi input[name="comandapiuconto"]:visible').is(':checked');
                var comandadoppia = $('#form_settaggi input[name="comandadoppia"]:visible').is(':checked');
                var memo_dopo_comanda = $('#form_settaggi input[name="memo_dopo_comanda"]:visible').is(':checked');
                var comanda_temporizzata = $('#form_settaggi input[name="comanda_temporizzata"]:visible').is(':checked');
                var gap_comanda_temporizzata = $('#form_settaggi input[name="gap_comanda_temporizzata"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var gap_comanda_temporizzata_domicilio = $('#form_settaggi input[name="gap_comanda_temporizzata_domicilio"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field108='" + comandapiuconto + "',\n\
                                        Field109 ='" + comandadoppia + "',\n\
                                        Field187 ='" + memo_dopo_comanda + "', \n\
                                        Field465 ='" + comanda_temporizzata + "', \n\
                                        Field466 ='" + gap_comanda_temporizzata + "', \n\
                                        Field467 ='" + gap_comanda_temporizzata_domicilio + "' \n\
                                        where id=" + comanda.folder_number + " ;";
                break;
            case "STAMPE_CONTO":

                var contodoppio = $('#form_settaggi input[name="contodoppio"]:visible').is(':checked');
                var memo_dopo_conto = $('#form_settaggi input[name="memo_dopo_conto"]:visible').is(':checked');
                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field110='" + contodoppio + "',\n\
                                        Field188='" + memo_dopo_conto + "'\n\
                                         where id=" + comanda.folder_number + " ;";
                break;
            case "STAMPE_INCASSI":

                var dettagliovendutidoppiacopia = $('#form_settaggi input[name="dettagliovendutidoppiacopia"]:visible').is(':checked');
                var incassipiudettagliovenduti = $('#form_settaggi input[name="incassipiudettagliovenduti"]:visible').is(':checked');
                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field108='" + dettagliovendutidoppiacopia + "',\n\
                                        Field109='" + incassipiudettagliovenduti + "'\n\
                                         where id=" + comanda.folder_number + " ;";
                break;
            case "STAMPE_BUONI_PASTO":

                var buoniacquistosunonfiscale = $('#form_settaggi input[name="buoniacquistosunonfiscale"]:visible').is(':checked');
                var abilita_buono_acquisto = $('#form_settaggi input[name="abilita_buono_acquisto"]:visible').is(':checked');
                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field361='" + buoniacquistosunonfiscale + "',\n\
                                        Field194='" + abilita_buono_acquisto + "'\n\
                                         where id=" + comanda.folder_number + " ;";
                comanda.abilita_buono_acquisto = abilita_buono_acquisto;
                break;
            case "LAYOUT VISUALIZZAZIONE GENERICI":

                var consumazione_in_pizzeria = $('#form_settaggi input[name="consumazione_in_pizzeria"]:visible').is(':checked');
                var apertura_ordine = $('#form_settaggi input[name="apertura_ordine"]:visible').is(':checked');
                var apertura_comanda = $('#form_settaggi input[name="apertura_comanda"]:visible').is(':checked');
                var apertura_conto = $('#form_settaggi input[name="apertura_conto"]:visible').is(':checked');
                var apertura_scontrino = $('#form_settaggi input[name="apertura_scontrino"]:visible').is(':checked');
                var riquadro_scooter = $('#form_settaggi input[name="riquadro_scooter"]:visible').is(':checked');
                var abilita_chiusura_ordine = $('#form_settaggi input[name="abilita_chiusura_ordine"]:visible').is(':checked');
                var qr_code_fattorini = $('#form_settaggi input[name="qr_code_fattorini"]:visible').is(':checked');
                var rosso_forno = $('#form_settaggi input[name="rosso_forno"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var giallo_forno = $('#form_settaggi input[name="giallo_forno"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var totale_capienza_forno = $('#form_settaggi input[name="totale_capienza_forno"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var avviso_supero_capienza = $('#form_settaggi input[name="avviso_supero_capienza"]:visible').is(':checked');
                var iva_esposta = $('#form_settaggi input[name="iva_esposta"]:visible').is(':checked');
                var tasto_pizza_maxi = $('#form_settaggi input[name="tasto_pizza_maxi"]:visible').is(':checked');
                var capienza_pizza_maxi = $('#form_settaggi input[name="capienza_pizza_maxi"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var pasta_kamut = $('#form_settaggi input[name="pasta_kamut"]:visible').is(':checked');
                var pasta_soja = $('#form_settaggi input[name="pasta_soja"]:visible').is(':checked');
                var pasta_integrale = $('#form_settaggi input[name="pasta_integrale"]:visible').is(':checked');
                var pasta_madre = $('#form_settaggi input[name="pasta_madre"]:visible').is(':checked');
                var pasta_5_cereali = $('#form_settaggi input[name="pasta_5_cereali"]:visible').is(':checked');
                var pasta_7_cereali = $('#form_settaggi input[name="pasta_7_cereali"]:visible').is(':checked');
                var pasta_senza_glutine = $('#form_settaggi input[name="pasta_senza_glutine"]:visible').is(':checked');
                var pasta_farro = $('#form_settaggi input[name="pasta_farro"]:visible').is(':checked');
                var pasta_carbone = $('#form_settaggi input[name="pasta_carbone"]:visible').is(':checked');
                var pasta_napoli = $('#form_settaggi input[name="pasta_napoli"]:visible').is(':checked');
                var pasta_doppia_pasta = $('#form_settaggi input[name="pasta_doppia_pasta"]:visible').is(':checked');
                var pasta_battuta = $('#form_settaggi input[name="pasta_battuta"]:visible').is(':checked');
                var pasta_battuta_farcita = $('#form_settaggi input[name="pasta_battuta_farcita"]:visible').is(':checked');
                var pasta_enkir = $('#form_settaggi input[name="pasta_enkir"]:visible').is(':checked');
                var pasta_canapa = $('#form_settaggi input[name="pasta_canapa"]:visible').is(':checked');
                var pasta_grano_arso = $('#form_settaggi input[name="pasta_grano_arso"]:visible').is(':checked');
                var pasta_manitoba = $('#form_settaggi input[name="pasta_manitoba"]:visible').is(':checked');
                var pasta_patate = $('#form_settaggi input[name="pasta_patate"]:visible').is(':checked');
                var pasta_pinsa_romana = $('#form_settaggi input[name="pasta_pinsa_romana"]:visible').is(':checked');
                var pasta_fagioli = $('#form_settaggi input[name="pasta_fagioli"]:visible').is(':checked');
                var pasta_buratto_tipo2 = $('#form_settaggi input[name="pasta_buratto_tipo2"]:visible').is(':checked');
                var prezzo_pasta_kamut = parseFloat($('#form_settaggi input[name="prezzo_pasta_kamut"]:visible').val()).toFixed(2);
                var prezzo_pasta_soja = parseFloat($('#form_settaggi input[name="prezzo_pasta_soja"]:visible').val()).toFixed(2);
                var prezzo_pasta_integrale = parseFloat($('#form_settaggi input[name="prezzo_pasta_integrale"]:visible').val()).toFixed(2);
                var prezzo_pasta_madre = parseFloat($('#form_settaggi input[name="prezzo_pasta_madre"]:visible').val()).toFixed(2);
                var prezzo_pasta_5_cereali = parseFloat($('#form_settaggi input[name="prezzo_pasta_5_cereali"]:visible').val()).toFixed(2);
                var prezzo_pasta_7_cereali = parseFloat($('#form_settaggi input[name="prezzo_pasta_7_cereali"]:visible').val()).toFixed(2);
                var prezzo_pasta_senza_glutine = parseFloat($('#form_settaggi input[name="prezzo_pasta_senza_glutine"]:visible').val()).toFixed(2);
                var prezzo_pasta_farro = parseFloat($('#form_settaggi input[name="prezzo_pasta_farro"]:visible').val()).toFixed(2);
                var prezzo_pasta_carbone = parseFloat($('#form_settaggi input[name="prezzo_pasta_carbone"]:visible').val()).toFixed(2);
                var prezzo_pasta_napoli = parseFloat($('#form_settaggi input[name="prezzo_pasta_napoli"]:visible').val()).toFixed(2);
                var prezzo_pasta_doppia_pasta = parseFloat($('#form_settaggi input[name="prezzo_pasta_doppia_pasta"]:visible').val()).toFixed(2);
                var prezzo_pasta_battuta = parseFloat($('#form_settaggi input[name="prezzo_pasta_battuta"]:visible').val()).toFixed(2);
                var prezzo_pasta_battuta_farcita = parseFloat($('#form_settaggi input[name="prezzo_pasta_battuta_farcita"]:visible').val()).toFixed(2);
                var prezzo_pasta_enkir = parseFloat($('#form_settaggi input[name="prezzo_pasta_enkir"]:visible').val()).toFixed(2);
                var prezzo_pasta_canapa = parseFloat($('#form_settaggi input[name="prezzo_pasta_canapa"]:visible').val()).toFixed(2);
                var prezzo_pasta_grano_arso = parseFloat($('#form_settaggi input[name="prezzo_pasta_grano_arso"]:visible').val()).toFixed(2);
                var prezzo_pasta_manitoba = parseFloat($('#form_settaggi input[name="prezzo_pasta_manitoba"]:visible').val()).toFixed(2);
                var prezzo_pasta_patate = parseFloat($('#form_settaggi input[name="prezzo_pasta_patate"]:visible').val()).toFixed(2);
                var prezzo_pasta_pinsa_romana = parseFloat($('#form_settaggi input[name="prezzo_pasta_pinsa_romana"]:visible').val()).toFixed(2);
                var prezzo_pasta_fagioli = parseFloat($('#form_settaggi input[name="prezzo_pasta_fagioli"]:visible').val()).toFixed(2);
                var prezzo_pasta_buratto_tipo2 = parseFloat($('#form_settaggi input[name="prezzo_pasta_buratto_tipo2"]:visible').val()).toFixed(2);
                var check_pasta_personalizzata_1 = $('#form_settaggi input[name="check_pasta_personalizzata_1"]:visible').is(':checked');
                var testo_pasta_personalizzata_1 = $('#form_settaggi input[name="testo_pasta_personalizzata_1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var prezzo_pasta_personalizzata_1 = parseFloat($('#form_settaggi input[name="prezzo_pasta_personalizzata_1"]:visible').val()).toFixed(2);
                var abbreviazione_pasta_personalizzata_1 = $('#form_settaggi input[name="abbreviazione_pasta_personalizzata_1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var check_pasta_personalizzata_2 = $('#form_settaggi input[name="check_pasta_personalizzata_2"]:visible').is(':checked');
                var testo_pasta_personalizzata_2 = $('#form_settaggi input[name="testo_pasta_personalizzata_2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var prezzo_pasta_personalizzata_2 = parseFloat($('#form_settaggi input[name="prezzo_pasta_personalizzata_2"]:visible').val()).toFixed(2);
                var abbreviazione_pasta_personalizzata_2 = $('#form_settaggi input[name="abbreviazione_pasta_personalizzata_2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var check_pasta_personalizzata_3 = $('#form_settaggi input[name="check_pasta_personalizzata_3"]:visible').is(':checked');
                var testo_pasta_personalizzata_3 = $('#form_settaggi input[name="testo_pasta_personalizzata_3"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var prezzo_pasta_personalizzata_3 = parseFloat($('#form_settaggi input[name="prezzo_pasta_personalizzata_3"]:visible').val()).toFixed(2);
                var abbreviazione_pasta_personalizzata_3 = $('#form_settaggi input[name="abbreviazione_pasta_personalizzata_3"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var check_pasta_personalizzata_4 = $('#form_settaggi input[name="check_pasta_personalizzata_4"]:visible').is(':checked');
                var testo_pasta_personalizzata_4 = $('#form_settaggi input[name="testo_pasta_personalizzata_4"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var prezzo_pasta_personalizzata_4 = parseFloat($('#form_settaggi input[name="prezzo_pasta_personalizzata_4"]:visible').val()).toFixed(2);
                var abbreviazione_pasta_personalizzata_4 = $('#form_settaggi input[name="abbreviazione_pasta_personalizzata_4"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var check_pasta_personalizzata_5 = $('#form_settaggi input[name="check_pasta_personalizzata_5"]:visible').is(':checked');
                var testo_pasta_personalizzata_5 = $('#form_settaggi input[name="testo_pasta_personalizzata_5"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var prezzo_pasta_personalizzata_5 = parseFloat($('#form_settaggi input[name="prezzo_pasta_personalizzata_5"]:visible').val()).toFixed(2);
                var abbreviazione_pasta_personalizzata_5 = $('#form_settaggi input[name="abbreviazione_pasta_personalizzata_5"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var check_pasta_personalizzata_6 = $('#form_settaggi input[name="check_pasta_personalizzata_6"]:visible').is(':checked');
                var testo_pasta_personalizzata_6 = $('#form_settaggi input[name="testo_pasta_personalizzata_6"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var prezzo_pasta_personalizzata_6 = parseFloat($('#form_settaggi input[name="prezzo_pasta_personalizzata_6"]:visible').val()).toFixed(2);
                var abbreviazione_pasta_personalizzata_6 = $('#form_settaggi input[name="abbreviazione_pasta_personalizzata_6"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var ordine_articoli_video = $('#form_settaggi  select[name="ordine_articoli_video"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var tipo_orario_forno = $('#form_settaggi  select[name="tipo_orario_forno"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var due_gusti_setg = $('#form_settaggi  select[name="tendine_due_gusti_settaggio"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var tasto_pizza_metro = $('#form_settaggi input[name="tasto_pizza_metro"]:visible').is(':checked');
                var tasto_pizza_mezzo_metro = $('#form_settaggi input[name="tasto_pizza_mezzo_metro"]:visible').is(':checked');
                var tasto_pizza_duegusti = $('#form_settaggi input[name="tasto_pizza_duegusti"]:visible').is(':checked');
                /*var tasto_schiacciata = $('#form_settaggi input[name="tasto_schiacciata"]:visible').is(':checked');*/
                var prezzo_metro = $('#form_settaggi input[name="prezzo_metro"]:visible').val();
                var prezzo_mezzo_metro = $('#form_settaggi input[name="prezzo_mezzo_metro"]:visible').val();
                var spazio_metro = $('#form_settaggi input[name="spazio_metro"]:visible').val();
                var spazio_mezzo_metro = $('#form_settaggi input[name="spazio_mezzo_metro"]:visible').val();
                var tasto_un_quarto = $('#form_settaggi input[name="tasto_un_quarto"]:visible').is(':checked');
                var codice_clienti_tessera = $('#form_settaggi input[name="codice_tessera_clienti"]:visible').is(':checked');



                //per prendere valore per impasti

                var prezzo_pasta_kamut_maxi = parseFloat($('#form_settaggi input[name="prezzo_pasta_kamut_maxi"]:visible').val()).toFixed(2);
                var prezzo_pasta_soja_maxi = parseFloat($('#form_settaggi input[name="prezzo_pasta_soja_maxi"]:visible').val()).toFixed(2);
                var prezzo_pasta_integrale_maxi = parseFloat($('#form_settaggi input[name="prezzo_pasta_integrale_maxi"]:visible').val()).toFixed(2);
                var prezzo_pasta_madre_maxi = parseFloat($('#form_settaggi input[name="prezzo_pasta_madre_maxi"]:visible').val()).toFixed(2);
                var prezzo_pasta_5_cereali_maxi = parseFloat($('#form_settaggi input[name="prezzo_pasta_5_cereali_maxi"]:visible').val()).toFixed(2);
                var prezzo_pasta_7_cereali_maxi = parseFloat($('#form_settaggi input[name="prezzo_pasta_7_cereali_maxi"]:visible').val()).toFixed(2);
                var prezzo_pasta_senza_glutine_maxi = parseFloat($('#form_settaggi input[name="prezzo_pasta_senza_glutine_maxi"]:visible').val()).toFixed(2);
                var prezzo_pasta_farro_maxi = parseFloat($('#form_settaggi input[name="prezzo_pasta_farro_maxi"]:visible').val()).toFixed(2);
                var prezzo_pasta_carbone_maxi = parseFloat($('#form_settaggi input[name="prezzo_pasta_carbone_maxi"]:visible').val()).toFixed(2);
                var prezzo_pasta_napoli_maxi = parseFloat($('#form_settaggi input[name="prezzo_pasta_napoli_maxi"]:visible').val()).toFixed(2);
                var prezzo_pasta_doppia_pasta_maxi = parseFloat($('#form_settaggi input[name="prezzo_pasta_doppia_pasta_maxi"]:visible').val()).toFixed(2);
                var prezzo_pasta_battuta_maxi = parseFloat($('#form_settaggi input[name="prezzo_pasta_battuta_maxi"]:visible').val()).toFixed(2);
                var prezzo_pasta_battuta_farcita_maxi = parseFloat($('#form_settaggi input[name="prezzo_pasta_battuta_farcita_maxi"]:visible').val()).toFixed(2);
                var prezzo_pasta_enkir_maxi = parseFloat($('#form_settaggi input[name="prezzo_pasta_enkir_maxi"]:visible').val()).toFixed(2);
                var prezzo_pasta_canapa_maxi = parseFloat($('#form_settaggi input[name="prezzo_pasta_canapa_maxi"]:visible').val()).toFixed(2);
                var prezzo_pasta_grano_arso_maxi = parseFloat($('#form_settaggi input[name="prezzo_pasta_grano_arso_maxi"]:visible').val()).toFixed(2);
                var prezzo_pasta_manitoba_maxi = parseFloat($('#form_settaggi input[name="prezzo_pasta_manitoba_maxi"]:visible').val()).toFixed(2);
                var prezzo_pasta_patate_maxi = parseFloat($('#form_settaggi input[name="prezzo_pasta_patate_maxi"]:visible').val()).toFixed(2);
                var prezzo_pasta_pinsa_romana_maxi = parseFloat($('#form_settaggi input[name="prezzo_pasta_pinsa_romana_maxi"]:visible').val()).toFixed(2);
                var prezzo_pasta_fagioli_maxi = parseFloat($('#form_settaggi input[name="prezzo_pasta_fagioli_maxi"]:visible').val()).toFixed(2);
                var prezzo_pasta_buratto_tipo2_maxi = parseFloat($('#form_settaggi input[name="prezzo_pasta_buratto_tipo2_maxi"]:visible').val()).toFixed(2);
                var prezzo_pasta_personalizzata_1_maxi = parseFloat($('#form_settaggi input[name="prezzo_pasta_personalizzata_1_maxi"]:visible').val()).toFixed(2);
                var prezzo_pasta_personalizzata_2_maxi = parseFloat($('#form_settaggi input[name="prezzo_pasta_personalizzata_2_maxi"]:visible').val()).toFixed(2);
                var prezzo_pasta_personalizzata_3_maxi = parseFloat($('#form_settaggi input[name="prezzo_pasta_personalizzata_3_maxi"]:visible').val()).toFixed(2);
                var prezzo_pasta_personalizzata_4_maxi = parseFloat($('#form_settaggi input[name="prezzo_pasta_personalizzata_4_maxi"]:visible').val()).toFixed(2);
                var prezzo_pasta_personalizzata_5_maxi = parseFloat($('#form_settaggi input[name="prezzo_pasta_personalizzata_5_maxi"]:visible').val()).toFixed(2);
                var prezzo_pasta_personalizzata_6_maxi = parseFloat($('#form_settaggi input[name="prezzo_pasta_personalizzata_6_maxi"]:visible').val()).toFixed(2);




                comanda.qr_code_fattorini = qr_code_fattorini;
                comanda.consumazione_in_pizzeria = consumazione_in_pizzeria;
                comanda.inizio_fascia_gialla = giallo_forno;
                comanda.inizio_fascia_rossa = rosso_forno;
                comanda.tasto_pizza_metro = tasto_pizza_metro;
                comanda.tasto_pizza_mezzo_metro = tasto_pizza_mezzo_metro;
                comanda.tasto_pizza_duegusti = tasto_pizza_duegusti;
                /*comanda.tasto_schiacciata = tasto_schiacciata;*/
                comanda.tasto_pizza_maxi = tasto_pizza_maxi;
                comanda.capienza_pizza_maxi = capienza_pizza_maxi;
                comanda.prezzo_metro = prezzo_metro;
                comanda.prezzo_mezzo_metro = prezzo_mezzo_metro;
                comanda.codice_clienti_tessera = codice_clienti_tessera;
                if (comanda.pizzeria_asporto === true) {
                    comanda.spazio_metro = "4";
                    comanda.spazio_mezzo_metro = "2";
                    if (!isNaN(parseInt(spazio_metro))) {
                        comanda.spazio_metro = spazio_metro;
                    }

                    if (!isNaN(parseInt(spazio_mezzo_metro))) {
                        comanda.spazio_mezzo_metro = spazio_mezzo_metro;
                    }
                }

                comanda.tasto_un_quarto = tasto_un_quarto;
                comanda.iva_esposta = iva_esposta.toString();
                if (comanda.iva_esposta !== "true") {
                    $('#campo_iva_conto').hide();
                }

                comanda.tasto_pasta_kamut = pasta_kamut;
                comanda.tasto_pasta_soja = pasta_soja;
                comanda.tasto_pasta_integrale = pasta_integrale;
                comanda.tasto_pasta_madre = pasta_madre;
                comanda.tasto_pasta_5_cereali = pasta_5_cereali;
                comanda.tasto_pasta_7_cereali = pasta_7_cereali;
                comanda.tasto_pasta_senza_glutine = pasta_senza_glutine;
                comanda.tasto_pasta_farro = pasta_farro;
                comanda.tasto_pasta_carbone = pasta_carbone;
                comanda.tasto_pasta_napoli = pasta_napoli;
                comanda.tasto_pasta_doppia_pasta = pasta_doppia_pasta;
                comanda.tasto_pasta_battuta = pasta_battuta;
                comanda.tasto_pasta_battuta_farcita = pasta_battuta_farcita;
                comanda.tasto_pasta_enkir = pasta_enkir;
                comanda.tasto_pasta_canapa = pasta_canapa;
                comanda.tasto_pasta_grano_arso = pasta_grano_arso;
                comanda.tasto_pasta_manitoba = pasta_manitoba;
                comanda.tasto_pasta_patate = pasta_patate;
                comanda.tasto_pasta_pinsa_romana = pasta_pinsa_romana;
                comanda.tasto_pasta_fagioli = pasta_fagioli;
                comanda.tasto_pasta_buratto_tipo2 = pasta_buratto_tipo2;
                comanda.prezzo_pasta_kamut = prezzo_pasta_kamut;
                comanda.prezzo_pasta_soja = prezzo_pasta_soja;
                comanda.prezzo_pasta_integrale = prezzo_pasta_integrale;
                comanda.prezzo_pasta_madre = prezzo_pasta_madre;
                comanda.prezzo_pasta_5_cereali = prezzo_pasta_5_cereali;
                comanda.prezzo_pasta_7_cereali = prezzo_pasta_7_cereali;
                comanda.prezzo_pasta_senza_glutine = prezzo_pasta_senza_glutine;
                comanda.prezzo_pasta_farro = prezzo_pasta_farro;
                comanda.prezzo_pasta_carbone = prezzo_pasta_carbone;
                comanda.prezzo_pasta_napoli = prezzo_pasta_napoli;
                comanda.prezzo_pasta_doppia_pasta = prezzo_pasta_doppia_pasta;
                comanda.prezzo_pasta_battuta = prezzo_pasta_battuta;
                comanda.prezzo_pasta_battuta_farcita = prezzo_pasta_battuta_farcita;
                comanda.prezzo_pasta_enkir = prezzo_pasta_enkir;
                comanda.prezzo_pasta_canapa = prezzo_pasta_canapa;
                comanda.prezzo_pasta_grano_arso = prezzo_pasta_grano_arso;
                comanda.prezzo_pasta_manitoba = prezzo_pasta_manitoba;
                comanda.prezzo_pasta_patate = prezzo_pasta_patate;
                comanda.prezzo_pasta_pinsa_romana = prezzo_pasta_pinsa_romana;
                comanda.prezzo_pasta_fagioli = prezzo_pasta_fagioli;
                comanda.prezzo_pasta_buratto_tipo2 = prezzo_pasta_buratto_tipo2;
                comanda.check_pasta_personalizzata_1 = check_pasta_personalizzata_1;
                comanda.testo_pasta_personalizzata_1 = testo_pasta_personalizzata_1;
                comanda.prezzo_pasta_personalizzata_1 = prezzo_pasta_personalizzata_1;
                comanda.abbreviazione_pasta_personalizzata_1 = abbreviazione_pasta_personalizzata_1;
                comanda.check_pasta_personalizzata_2 = check_pasta_personalizzata_2;
                comanda.testo_pasta_personalizzata_2 = testo_pasta_personalizzata_2;
                comanda.prezzo_pasta_personalizzata_2 = prezzo_pasta_personalizzata_2;
                comanda.abbreviazione_pasta_personalizzata_2 = abbreviazione_pasta_personalizzata_2;
                comanda.check_pasta_personalizzata_3 = check_pasta_personalizzata_3;
                comanda.testo_pasta_personalizzata_3 = testo_pasta_personalizzata_3;
                comanda.prezzo_pasta_personalizzata_3 = prezzo_pasta_personalizzata_3;
                comanda.abbreviazione_pasta_personalizzata_3 = abbreviazione_pasta_personalizzata_3;
                comanda.check_pasta_personalizzata_4 = check_pasta_personalizzata_4;
                comanda.testo_pasta_personalizzata_4 = testo_pasta_personalizzata_4;
                comanda.prezzo_pasta_personalizzata_4 = prezzo_pasta_personalizzata_4;
                comanda.abbreviazione_pasta_personalizzata_4 = abbreviazione_pasta_personalizzata_4;
                comanda.check_pasta_personalizzata_5 = check_pasta_personalizzata_5;
                comanda.testo_pasta_personalizzata_5 = testo_pasta_personalizzata_5;
                comanda.prezzo_pasta_personalizzata_5 = prezzo_pasta_personalizzata_5;
                comanda.abbreviazione_pasta_personalizzata_5 = abbreviazione_pasta_personalizzata_5;
                comanda.check_pasta_personalizzata_6 = check_pasta_personalizzata_6;
                comanda.testo_pasta_personalizzata_6 = testo_pasta_personalizzata_6;
                comanda.prezzo_pasta_personalizzata_6 = prezzo_pasta_personalizzata_6;
                comanda.abbreviazione_pasta_personalizzata_6 = abbreviazione_pasta_personalizzata_6;
                comanda.ordine_articoli_video = ordine_articoli_video;
                comanda.tipo_orario_forno = tipo_orario_forno;
                comanda.due_gusti_setgg = due_gusti_setg;
                comanda.abilita_chiusura_ordine = abilita_chiusura_ordine;





                //prezzo maxi per impasti



                comanda.prezzo_pasta_kamut_maxi = prezzo_pasta_kamut_maxi;
                comanda.prezzo_pasta_soja_maxi = prezzo_pasta_soja_maxi;
                comanda.prezzo_pasta_integrale_maxi = prezzo_pasta_integrale_maxi;
                comanda.prezzo_pasta_madre_maxi = prezzo_pasta_madre_maxi;
                comanda.prezzo_pasta_5_cereali_maxi = prezzo_pasta_5_cereali_maxi;
                comanda.prezzo_pasta_7_cereali_maxi = prezzo_pasta_7_cereali_maxi;
                comanda.prezzo_pasta_senza_glutine_maxi = prezzo_pasta_senza_glutine_maxi;
                comanda.prezzo_pasta_farro_maxi = prezzo_pasta_farro_maxi;
                comanda.prezzo_pasta_carbone_maxi = prezzo_pasta_carbone_maxi;
                comanda.prezzo_pasta_napoli_maxi = prezzo_pasta_napoli_maxi;
                comanda.prezzo_pasta_doppia_pasta_maxi = prezzo_pasta_doppia_pasta_maxi;
                comanda.prezzo_pasta_battuta_maxi = prezzo_pasta_battuta_maxi;
                comanda.prezzo_pasta_battuta_farcita_maxi = prezzo_pasta_battuta_farcita_maxi;
                comanda.prezzo_pasta_enkir_maxi = prezzo_pasta_enkir_maxi;
                comanda.prezzo_pasta_canapa_maxi = prezzo_pasta_canapa_maxi;
                comanda.prezzo_pasta_grano_arso_maxi = prezzo_pasta_grano_arso_maxi;
                comanda.prezzo_pasta_manitoba_maxi = prezzo_pasta_manitoba_maxi;
                comanda.prezzo_pasta_patate_maxi = prezzo_pasta_patate_maxi;
                comanda.prezzo_pasta_pinsa_romana_maxi = prezzo_pasta_pinsa_romana_maxi;
                comanda.prezzo_pasta_fagioli_maxi = prezzo_pasta_fagioli_maxi;
                comanda.prezzo_pasta_buratto_tipo2_maxi = prezzo_pasta_buratto_tipo2_maxi;
                comanda.prezzo_pasta_personalizzata_1_maxi = prezzo_pasta_personalizzata_1_maxi;
                comanda.prezzo_pasta_personalizzata_2_maxi = prezzo_pasta_personalizzata_2_maxi;
                comanda.prezzo_pasta_personalizzata_3_maxi = prezzo_pasta_personalizzata_3_maxi;
                comanda.prezzo_pasta_personalizzata_4_maxi = prezzo_pasta_personalizzata_4_maxi;
                comanda.prezzo_pasta_personalizzata_5_maxi = prezzo_pasta_personalizzata_5_maxi;
                comanda.prezzo_pasta_personalizzata_6_maxi = prezzo_pasta_personalizzata_6_maxi;



                if (codice_clienti_tessera.toString() === "true") {
                    $("#codice_tessera_clienti").show();
                    $('#form_gestione_clienti input[name="codice_tessera_clienti_gestione"]:visible');


                }
                else {
                    $("#codice_tessera_clienti").hide();
                    $('#form_gestione_clienti input[name="codice_tessera_clienti_gestione"]:hidden');

                }

                /*Field501='" + tasto_schiacciata + "',*/

                if (qr_code_fattorini.toString() === "true") {
                    $("#qr_code_comanda").show();
                } else {
                    $("#qr_code_comanda").hide();
                }


                var testo_query = "     UPDATE settaggi_profili SET \n\
                                        Field511='" + due_gusti_setg + "',\n\
                                         Field508='" + qr_code_fattorini + "',\n\
                                        Field468='" + tipo_orario_forno + "',\n\
                                        Field395='" + ordine_articoli_video + "',\n\
                                        Field353='" + apertura_ordine + "',\n\
                                        Field354='" + apertura_comanda + "',\n\
                                        Field355='" + apertura_conto + "',\n\
                                        Field356='" + apertura_scontrino + "',\n\
                                        Field372='" + consumazione_in_pizzeria + "',\n\
                                        Field311='" + tasto_pizza_maxi + "',\n\
                                        Field326='" + tasto_pizza_duegusti + "',\n\
                                        Field312='" + capienza_pizza_maxi + "',\n\
                                        Field182='" + riquadro_scooter + "',\n\
                                        Field473='" + abilita_chiusura_ordine + "',\n\
                                        Field183='" + rosso_forno + "',\n\
                                        Field184='" + giallo_forno + "',\n\
                                        Field185='" + totale_capienza_forno + "',\n\
                                        Field186='" + avviso_supero_capienza + "',\n\
                                        Field309='" + iva_esposta + "',\n\
                                        Field209='" + tasto_pizza_metro + "',\n\
                                        Field328='" + tasto_pizza_mezzo_metro + "',\n\
                                        Field210='" + prezzo_metro + "',\n\
                                        Field211='" + prezzo_mezzo_metro + "',\n\
                                        Field212='" + tasto_un_quarto + "',\n\
                                        Field213='" + pasta_kamut + "',\n\
                                        Field214='" + pasta_soja + "',\n\
                                        Field215='" + pasta_integrale + "',\n\
                                        Field216='" + pasta_madre + "',\n\
                                        Field217='" + pasta_5_cereali + "',\n\
                                        Field437='" + pasta_7_cereali + "',\n\
                                        Field218='" + pasta_senza_glutine + "',\n\
                                        Field219='" + pasta_farro + "',\n\
                                        Field438='" + pasta_carbone + "',\n\
                                        Field220='" + prezzo_pasta_kamut + "',\n\
                                        Field221='" + prezzo_pasta_soja + "',\n\
                                        Field222='" + prezzo_pasta_integrale + "',\n\
                                        Field223='" + prezzo_pasta_madre + "',\n\
                                        Field224='" + prezzo_pasta_5_cereali + "',\n\
                                        Field439='" + prezzo_pasta_7_cereali + "',\n\
                                        Field225='" + prezzo_pasta_senza_glutine + "',\n\
                                        Field226='" + prezzo_pasta_farro + "',\n\
                                        Field440='" + prezzo_pasta_carbone + "',\n\
                                        Field254='" + spazio_metro + "',\n\
                                        Field255='" + spazio_mezzo_metro + "',\n\
                                        \n\
                                        Field441='" + pasta_napoli + "',\n\
                                        Field442='" + prezzo_pasta_napoli + "',\n\
                                        Field443='" + pasta_doppia_pasta + "',\n\
                                        Field444='" + prezzo_pasta_doppia_pasta + "',\n\
                                        Field445='" + pasta_battuta + "',\n\
                                        Field446='" + prezzo_pasta_battuta + "',\n\
                                        Field447='" + pasta_battuta_farcita + "',\n\
                                        Field448='" + prezzo_pasta_battuta_farcita + "',\n\
                                        Field449='" + pasta_enkir + "',\n\
                                        Field450='" + prezzo_pasta_enkir + "',\n\
                                        Field451='" + pasta_canapa + "',\n\
                                        Field452='" + prezzo_pasta_canapa + "',\n\
                                        Field453='" + pasta_grano_arso + "',\n\
                                        Field454='" + prezzo_pasta_grano_arso + "',\n\
                                        Field455='" + pasta_manitoba + "',\n\
                                        Field456='" + prezzo_pasta_manitoba + "',\n\
                                        Field457='" + pasta_patate + "',\n\
                                        Field458='" + prezzo_pasta_patate + "',\n\
                                        Field459='" + pasta_pinsa_romana + "',\n\
                                        Field460='" + prezzo_pasta_pinsa_romana + "',\n\
                                        Field461='" + pasta_fagioli + "',\n\
                                        Field462='" + prezzo_pasta_fagioli + "',\n\
                                        Field463='" + pasta_buratto_tipo2 + "',\n\
                                        Field464='" + prezzo_pasta_buratto_tipo2 + "', \n\
                                        \n\
                                        Field474='" + check_pasta_personalizzata_1 + "', \n\
                                        Field475='" + testo_pasta_personalizzata_1 + "', \n\
                                        Field476='" + prezzo_pasta_personalizzata_1 + "', \n\
                                        Field477='" + abbreviazione_pasta_personalizzata_1 + "', \n\
                                        \n\
                                        Field478='" + check_pasta_personalizzata_2 + "', \n\
                                        Field479='" + testo_pasta_personalizzata_2 + "', \n\
                                        Field480='" + prezzo_pasta_personalizzata_2 + "', \n\
                                        Field481='" + abbreviazione_pasta_personalizzata_2 + "', \n\
                                        \n\
                                        Field482='" + check_pasta_personalizzata_3 + "', \n\
                                        Field483='" + testo_pasta_personalizzata_3 + "', \n\
                                        Field484='" + prezzo_pasta_personalizzata_3 + "', \n\
                                        Field485='" + abbreviazione_pasta_personalizzata_3 + "', \n\
                                        \n\
                                        Field486='" + check_pasta_personalizzata_4 + "', \n\
                                        Field487='" + testo_pasta_personalizzata_4 + "', \n\
                                        Field488='" + prezzo_pasta_personalizzata_4 + "', \n\
                                        Field489='" + abbreviazione_pasta_personalizzata_4 + "', \n\
                                        \n\
                                        Field490='" + check_pasta_personalizzata_5 + "', \n\
                                        Field491='" + testo_pasta_personalizzata_5 + "', \n\
                                        Field492='" + prezzo_pasta_personalizzata_5 + "', \n\
                                        Field493='" + abbreviazione_pasta_personalizzata_5 + "', \n\
                                        \n\
                                        Field494='" + check_pasta_personalizzata_6 + "', \n\
                                        Field495='" + testo_pasta_personalizzata_6 + "', \n\
                                        Field496='" + prezzo_pasta_personalizzata_6 + "', \n\
                                        Field497='" + abbreviazione_pasta_personalizzata_6 + "', \n\
                                        Field509='"+ codice_clienti_tessera + "' \n\
                                        where id=" + comanda.folder_number + " ;";



                //per salvare prezzi maxi nella database
                                        var testo_queryY = "     UPDATE prezzi_impasti_maxi SET \n\
                                        kumat='" + prezzo_pasta_kamut_maxi + "',\n\
                                        soja='" + prezzo_pasta_soja_maxi + "',\n\
                                        integrale='" + prezzo_pasta_integrale_maxi + "',\n\
                                        pasta_madre='" + prezzo_pasta_madre_maxi + "',\n\
                                        ch_cerali='" + prezzo_pasta_5_cereali_maxi + "',\n\
                                        set_cerali='" + prezzo_pasta_7_cereali_maxi + "',\n\
                                        senza_glutine='" + prezzo_pasta_senza_glutine_maxi + "',\n\
                                        farro='" + prezzo_pasta_farro_maxi + "',\n\
                                        carbone='" + prezzo_pasta_carbone_maxi + "',\n\
                                        napoli='" + prezzo_pasta_napoli_maxi + "',\n\
                                        doppia_pasta='" + prezzo_pasta_doppia_pasta_maxi + "',\n\
                                        batutta='" + prezzo_pasta_battuta_maxi + "',\n\
                                        batutta_farcita='" + prezzo_pasta_battuta_farcita_maxi + "',\n\
                                        enkir='" + prezzo_pasta_enkir_maxi + "',\n\
                                        canapa='" + prezzo_pasta_canapa_maxi + "',\n\
                                        grano_arso='" + prezzo_pasta_grano_arso_maxi + "',\n\
                                        manitoba='" + prezzo_pasta_manitoba_maxi + "',\n\
                                        patate='" + prezzo_pasta_patate_maxi + "',\n\
                                        pinsa_romana='" + prezzo_pasta_pinsa_romana_maxi + "',\n\
                                        fagioli='" + prezzo_pasta_fagioli_maxi + "',\n\
                                        buratto='" + prezzo_pasta_buratto_tipo2_maxi + "', \n\
                                        \n\
                                        testo_pasta_pers_1='" + prezzo_pasta_personalizzata_1_maxi + "', \n\
                                        \n\
                                        testo_pasta_pers_2='" + prezzo_pasta_personalizzata_2_maxi + "', \n\
                                        \n\
                                        testo_pasta_pers_3='" + prezzo_pasta_personalizzata_3_maxi + "', \n\
                                        \n\
                                        testo_pasta_pers_4='" + prezzo_pasta_personalizzata_4_maxi + "', \n\
                                        \n\
                                        testo_pasta_pers_5='" + prezzo_pasta_personalizzata_5_maxi + "', \n\
                                        \n\
                                        testo_pasta_pers_6='" + prezzo_pasta_personalizzata_6_maxi + "' \n\
                                        where id=" + comanda.folder_number + " ;";
                                        comanda.sock.send({ tipo: "aggiornamento", operatore: comanda.operatore, query: testo_queryY, terminale: comanda.terminale, ip: comanda.ip_address });
                    alasql(testo_queryY);
                                    
                break;
            case "TASTI INCASSO":

                var tasto_sconto = $('#form_settaggi input[name="tasto_sconto"]:visible').is(':checked');
                var tasto_resto = $('#form_settaggi input[name="tasto_resto"]:visible').is(':checked');
                var tasto_buonipasto = $('#form_settaggi input[name="tasto_buonipasto"]:visible').is(':checked');
                var tasto_contopiucomanda = $('#form_settaggi input[name="tasto_contopiucomanda"]:visible').is(':checked');
                var tasto_fatturapiucomanda = $('#form_settaggi input[name="tasto_fatturapiucomanda"]:visible').is(':checked');
                var tasto_scontrinopiucomanda = $('#form_settaggi input[name="tasto_scontrinopiucomanda"]:visible').is(':checked');
                var tasto_scontrinopiuconto = $('#form_settaggi input[name="tasto_scontrinopiuconto"]:visible').is(':checked');
                var tasto_conto = $('#form_settaggi input[name="tasto_conto"]:visible').is(':checked');
                var tasto_fattura = $('#form_settaggi input[name="tasto_fattura"]:visible').is(':checked');
                var tasto_scontrino = $('#form_settaggi input[name="tasto_scontrino"]:visible').is(':checked');
                var tasto_nonpagato = $('#form_settaggi input[name="tasto_nonpagato"]:visible').is(':checked');
                var tasto_fiscali = $('#form_settaggi input[name="operazioni"]:visible').is(':checked');
                var pagamenti = $('#form_settaggi input[name="pagamenti"]:visible').is(':checked');
                var storicizza = $('#form_settaggi input[name="storicizza"]:visible').is(':checked');
                var testo_query = "UPDATE settaggi_profili SET \n\
                                        Field313='" + tasto_sconto + "',\n\
                                        Field314='" + tasto_resto + "',\n\
                                        Field315='" + tasto_buonipasto + "',\n\
                                        Field316='" + tasto_contopiucomanda + "',\n\
                                        Field317='" + tasto_fatturapiucomanda + "',\n\
                                        Field318='" + tasto_scontrinopiucomanda + "',\n\
                                        Field319='" + tasto_scontrinopiuconto + "',\n\
                                        Field320='" + tasto_conto + "',\n\
                                        Field321='" + tasto_fattura + "',\n\
                                        Field421='" + tasto_nonpagato + "',\n\
                                        Field364='" + tasto_fiscali + "',\n\
                                        Field200='" + pagamenti + "',\n\
                                        Field204='" + storicizza + "',\n\
                                        Field322='" + tasto_scontrino + "' \n\
                                         where id=" + comanda.folder_number + " ;";
                break;
            case "VISORE ASPORTO":

                var testo_riga1 = $('#form_settaggi input[name="testo_riga1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var testo_riga2 = $('#form_settaggi input[name="testo_riga2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var numero_secondi_visore = $('#form_settaggi input[name="numero_secondi_visore"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var tasto_totale_visore = $('#form_settaggi input[name="tasto_totale_visore"]:visible').is(':checked');
                var numero_secondi_totale_visore = $('#form_settaggi input[name="numero_secondi_totale_visore"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field415 ='" + testo_riga1 + "', \n\
                                        Field416 ='" + testo_riga2 + "', \n\
                                        Field417 ='" + numero_secondi_visore + "', \n\
                                        Field418 ='" + tasto_totale_visore + "', \n\
                                        Field419 ='" + numero_secondi_totale_visore + "' \n\
        \n\
                                        where id=" + comanda.folder_number + ";";
                break;
            case "SCONTRINO":

                var msg_riga1 = $('#form_settaggi input[name="msg_riga1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga1_grassetto = $('#form_settaggi select[name="msg_riga1_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga1_allineamento = $('#form_settaggi select[name="msg_riga1_allineamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spazio1 = $('#form_settaggi input[name="spazio1"]:visible').is(':checked');
                var msg_riga2 = $('#form_settaggi input[name="msg_riga2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga2_grassetto = $('#form_settaggi select[name="msg_riga2_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga2_allineamento = $('#form_settaggi select[name="msg_riga2_allineamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spazio2 = $('#form_settaggi input[name="spazio2"]:visible').is(':checked');
                var msg_riga3 = $('#form_settaggi input[name="msg_riga3"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga3_grassetto = $('#form_settaggi select[name="msg_riga3_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga3_allineamento = $('#form_settaggi select[name="msg_riga3_allineamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spazio3 = $('#form_settaggi input[name="spazio3"]:visible').is(':checked');
                var msg_riga4 = $('#form_settaggi input[name="msg_riga4"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga4_grassetto = $('#form_settaggi select[name="msg_riga4_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga4_allineamento = $('#form_settaggi select[name="msg_riga4_allineamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var url_qr_code = $('#form_settaggi input[name="url_qr_code"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var numero_tavolo_scontrino = $('#form_settaggi input[name="numero_tavolo_scontrino"]:visible').is(':checked');
                var divisione_persone_esclude_divisione_coperti = $('#form_settaggi input[name="divisione_persone_esclude_divisione_coperti"]:visible').is(':checked');
                var barcode_riapertura_ordine_scontrino = $('#form_settaggi input[name="barcode_riapertura_ordine_scontrino"]:visible').is(':checked');
                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field500 ='" + barcode_riapertura_ordine_scontrino + "', \n\
                                        Field360 ='" + divisione_persone_esclude_divisione_coperti + "', \n\
                                        Field327 ='" + numero_tavolo_scontrino + "', \n\
                                        Field236 ='" + msg_riga1 + "', \n\
                                        Field241 ='" + msg_riga1_grassetto + "', \n\
                                        Field242 ='" + msg_riga1_allineamento + "', \n\
                                        Field237 ='" + spazio1 + "', \n\
                                        Field238 ='" + msg_riga2 + "', \n\
                                        Field243 ='" + msg_riga2_grassetto + "', \n\
                                        Field244='" + msg_riga2_allineamento + "', \n\
                                        Field239 ='" + spazio2 + "', \n\
                                        Field240 ='" + msg_riga3 + "', \n\
                                        Field245 ='" + msg_riga3_grassetto + "', \n\
                                        Field246 ='" + msg_riga3_allineamento + "',\n\
                                        Field247 ='" + spazio3 + "', \n\
                                        Field248 ='" + msg_riga4 + "', \n\
                                        Field249 ='" + msg_riga4_grassetto + "', \n\
                                        Field250 ='" + msg_riga4_allineamento + "',\n\
                                        Field251 ='" + url_qr_code + "'\n\
                                         where id=" + comanda.folder_number + " ;";


                let resulttT = "SELECT * FROM settaggi_profili  where id=" + comanda.folder_number + "  LIMIT 1;"
                let tesTt = alasql(resulttT);
                if (barcode_riapertura_ordine_scontrino.toString() === "true" || tesTt[0].Field499 === "true" || tesTt[0].Field498 === "true") {
                    $(".fa-barcode").show();
                } else {
                    $(".fa-barcode").hide();
                }

                comanda.scontrino_msg_riga1 = msg_riga1;
                comanda.scontrino_msg_riga1_grassetto = msg_riga1_grassetto;
                comanda.scontrino_msg_riga1_allineamento = msg_riga1_allineamento;
                comanda.scontrino_spazio1 = spazio1;
                comanda.scontrino_msg_riga2 = msg_riga2;
                comanda.scontrino_msg_riga2_grassetto = msg_riga2_grassetto;
                comanda.scontrino_msg_riga2_allineamento = msg_riga2_allineamento;
                comanda.scontrino_spazio2 = spazio2;
                comanda.scontrino_msg_riga3 = msg_riga3;
                comanda.scontrino_msg_riga3_grassetto = msg_riga3_grassetto;
                comanda.scontrino_msg_riga3_allineamento = msg_riga3_allineamento;
                comanda.scontrino_spazio3 = spazio3;
                comanda.scontrino_msg_riga4 = msg_riga4;
                comanda.scontrino_msg_riga4_grassetto = msg_riga4_grassetto;
                comanda.scontrino_msg_riga4_allineamento = msg_riga4_allineamento;
                comanda.url_qr_code = url_qr_code;
                break;
            case "SCONTISTICHE_SCONTO_TOTALE":
                var intestazione_riga1 = $('#form_settaggi input[name="intestazione_riga1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga1_grassetto = $('#form_settaggi select[name="intestazione_riga1_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spaziointestazione1 = $('#form_settaggi input[name="spaziointestazione1"]:visible').is(':checked');
                var intestazione_riga2 = $('#form_settaggi input[name="intestazione_riga2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga2_grassetto = $('#form_settaggi select[name="intestazione_riga2_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spaziointestazione2 = $('#form_settaggi input[name="spaziointestazione2"]:visible').is(':checked');
                var intestazione_riga3 = $('#form_settaggi input[name="intestazione_riga3"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga3_grassetto = $('#form_settaggi select[name="intestazione_riga3_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spaziointestazione3 = $('#form_settaggi input[name="spaziointestazione3"]:visible').is(':checked');
                var intestazione_riga4 = $('#form_settaggi input[name="intestazione_riga4"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga4_grassetto = $('#form_settaggi select[name="intestazione_riga4_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var data = $('#form_settaggi input[name="data"]:visible').is(':checked');
                var font_data = $('#form_settaggi select[name="font_data"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var valore_scadenza = $('#form_settaggi input[name="valore_scadenza"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intervallo_scadenza = $('#form_settaggi select[name="intervallo_scadenza"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var valore_euro_da_1 = $('#form_settaggi input[name="valore_euro_da_1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var valore_euro_a_1 = $('#form_settaggi input[name="valore_euro_a_1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var valore_sconto_1 = $('#form_settaggi input[name="valore_sconto_1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                //var omaggio_1 = $('#form_settaggi input[name="omaggio_1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var valore_euro_da_2 = $('#form_settaggi input[name="valore_euro_da_2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var valore_euro_a_2 = $('#form_settaggi input[name="valore_euro_a_2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var valore_sconto_2 = $('#form_settaggi input[name="valore_sconto_2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                //var omaggio_2 = $('#form_settaggi input[name="omaggio_2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var valore_euro_da_3 = $('#form_settaggi input[name="valore_euro_da_3"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var valore_euro_a_3 = $('#form_settaggi input[name="valore_euro_a_3"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var valore_sconto_3 = $('#form_settaggi input[name="valore_sconto_3"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                //var omaggio_3 = $('#form_settaggi input[name="omaggio_3"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var valore_euro_da_4 = $('#form_settaggi input[name="valore_euro_da_4"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var valore_euro_a_4 = $('#form_settaggi input[name="valore_euro_a_4"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var valore_sconto_4 = $('#form_settaggi input[name="valore_sconto_4"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                //var omaggio_4 = $('#form_settaggi input[name="omaggio_4"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');

                var array_giorni = new Array();
                var dom = $('#form_settaggi input[name="dom"]:visible').is(':checked');
                if (dom === true) {
                    array_giorni.push("0");
                }

                var lun = $('#form_settaggi input[name="lun"]:visible').is(':checked');
                if (lun === true) {
                    array_giorni.push("1");
                }

                var mar = $('#form_settaggi input[name="mar"]:visible').is(':checked');
                if (mar === true) {
                    array_giorni.push("2");
                }

                var mer = $('#form_settaggi input[name="mer"]:visible').is(':checked');
                if (mer === true) {
                    array_giorni.push("3");
                }

                var gio = $('#form_settaggi input[name="gio"]:visible').is(':checked');
                if (gio === true) {
                    array_giorni.push("4");
                }

                var ven = $('#form_settaggi input[name="ven"]:visible').is(':checked');
                if (ven === true) {
                    array_giorni.push("5");
                }

                var sab = $('#form_settaggi input[name="sab"]:visible').is(':checked');
                if (sab === true) {
                    array_giorni.push("6");
                }

                var stringa_giorni = array_giorni.join();
                var qrcode_sconto = $('#form_settaggi input[name="qrcode_sconto"]:visible').is(':checked');
                var msg_riga1 = $('#form_settaggi input[name="msg_riga1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga1_grassetto = $('#form_settaggi select[name="msg_riga1_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga1_allineamento = $('#form_settaggi select[name="msg_riga1_allineamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spazio1 = $('#form_settaggi input[name="spazio1"]:visible').is(':checked');
                var msg_riga2 = $('#form_settaggi input[name="msg_riga2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga2_grassetto = $('#form_settaggi select[name="msg_riga2_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga2_allineamento = $('#form_settaggi select[name="msg_riga2_allineamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spazio2 = $('#form_settaggi input[name="spazio2"]:visible').is(':checked');
                var msg_riga3 = $('#form_settaggi input[name="msg_riga3"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga3_grassetto = $('#form_settaggi select[name="msg_riga3_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga3_allineamento = $('#form_settaggi select[name="msg_riga3_allineamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field256='" + intestazione_riga1 + "',\n\
                                        Field269 ='" + intestazione_riga1_grassetto + "', \n\
                                        Field257 ='" + spaziointestazione1 + "', \n\
                                        Field258 ='" + intestazione_riga2 + "',\n\
                                        Field270 ='" + intestazione_riga2_grassetto + "', \n\
                                        Field259 ='" + spaziointestazione2 + "', \n\
                                        Field260 ='" + intestazione_riga3 + "',\n\
                                        Field271 ='" + intestazione_riga3_grassetto + "', \n\
                                        Field261 ='" + spaziointestazione3 + "', \n\
                                        Field262 ='" + intestazione_riga4 + "',\n\
                                        Field272 ='" + intestazione_riga4_grassetto + "', \n\
                                        \n\
                                        Field263 ='" + data + "', \n\
                                        Field273  ='" + font_data + "',\n\
                                        \n\
                                        Field264 ='" + msg_riga1 + "', \n\
                                        Field274 ='" + msg_riga1_grassetto + "', \n\
                                        Field275 ='" + msg_riga1_allineamento + "', \n\
                                        Field265 ='" + spazio1 + "', \n\
                                        Field266 ='" + msg_riga2 + "', \n\
                                        Field276 ='" + msg_riga2_grassetto + "', \n\
                                        Field277 ='" + msg_riga2_allineamento + "', \n\
                                        Field267 ='" + spazio2 + "', \n\
                                        Field268 ='" + msg_riga3 + "', \n\
                                        Field278='" + msg_riga3_grassetto + "', \n\
                                        Field279 ='" + msg_riga3_allineamento + "', \n\
                                        Field280 ='" + valore_scadenza + "', \n\
                                        Field281 ='" + intervallo_scadenza + "', \n\
                                        Field282 ='" + valore_euro_da_1 + "', \n\
                                        Field283 ='" + valore_euro_a_1 + "', \n\
                                        Field284 ='" + valore_sconto_1 + "', \n\
                                        Field286 ='" + valore_euro_da_2 + "', \n\
                                        Field287 ='" + valore_euro_a_2 + "', \n\
                                        Field288 ='" + valore_sconto_2 + "', \n\
                                        Field290 ='" + valore_euro_da_3 + "', \n\
                                        Field291 ='" + valore_euro_a_3 + "', \n\
                                        Field292 ='" + valore_sconto_3 + "', \n\
                                        Field294 ='" + valore_euro_da_4 + "', \n\
                                        Field295 ='" + valore_euro_a_4 + "', \n\
                                        Field296 ='" + valore_sconto_4 + "', \n\
                                        Field305 ='" + stringa_giorni + "', \n\
                                        Field306 ='" + qrcode_sconto + "' \n\
                                         where id=" + comanda.folder_number + " ;";
                break;
            case "FASTORDER":
                var url_fastorder = $('#form_settaggi input[name="url_fastorder"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var categorie_come_intellinet = $('#form_settaggi input[name="categorie_come_intellinet"]:visible').is(':checked');
                var modifica_prezzi_fastorder = $('#form_settaggi input[name="modifica_prezzi_fastorder"]:visible').is(':checked');

                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field502='" + url_fastorder + "',\n\
                                        Field506='" + categorie_come_intellinet + "',\n\
                                        Field507='" + modifica_prezzi_fastorder + "' \n\
                                        where id=" + comanda.folder_number + " ;";

                comanda.url_fastorder = url_fastorder;
                break;
                case "POS_1":
                var pos_abilitato = $('#form_settaggi input[name="pos_abilitato"]:visible').is(':checked');
                var ip_pos = $('#form_settaggi input[name="ip_pos"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var porta_pos = $('#form_settaggi input[name="porta_pos"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var id_term = $('#form_settaggi input[name="id_term"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var ricevuta_pos_scontrino = $('#form_settaggi input[name="ricevuta_pos_scontrino"]:visible').is(':checked');
                comanda.pos_abilitato = pos_abilitato;
                comanda.ip_pos = ip_pos;
                comanda.porta_pos = porta_pos;
                comanda.id_term = id_term;
                comanda.ricevuta_pos_scontrino = ricevuta_pos_scontrino;
                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field231='" + ip_pos + "',\n\
                                        Field232='" + porta_pos + "',\n\
                                        Field233='" + id_term + "',\n\
                                        Field234='" + pos_abilitato + "',\n\
                                        Field235='" + ricevuta_pos_scontrino + "'\n\
                                         where id=" + comanda.folder_number + " ;";
                break;
            case "POS_2":
                var pos_abilitato = $('#form_settaggi input[name="pos_abilitato"]:visible').is(':checked');
                var ip_pos = $('#form_settaggi input[name="ip_pos"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var porta_pos = $('#form_settaggi input[name="porta_pos"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var id_term = $('#form_settaggi input[name="id_term"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var ricevuta_pos_scontrino = $('#form_settaggi input[name="ricevuta_pos_scontrino"]:visible').is(':checked');
                comanda.pos_abilitato = pos_abilitato;
                /*comanda.ip_pos = ip_pos;
                 comanda.porta_pos = porta_pos;
                 comanda.id_term = id_term;
                 comanda.ricevuta_pos_scontrino = ricevuta_pos_scontrino;*/

                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field401='" + ip_pos + "',\n\
                                        Field402='" + porta_pos + "',\n\
                                        Field403='" + id_term + "',\n\
                                        Field400='" + pos_abilitato + "',\n\
                                        Field404='" + ricevuta_pos_scontrino + "'\n\
                                         where id=" + comanda.folder_number + " ;";
                break;
            case "POS_3":
                var pos_abilitato = $('#form_settaggi input[name="pos_abilitato"]:visible').is(':checked');
                var ip_pos = $('#form_settaggi input[name="ip_pos"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var porta_pos = $('#form_settaggi input[name="porta_pos"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var id_term = $('#form_settaggi input[name="id_term"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var ricevuta_pos_scontrino = $('#form_settaggi input[name="ricevuta_pos_scontrino"]:visible').is(':checked');
                comanda.pos_abilitato = pos_abilitato;
                /*comanda.ip_pos = ip_pos;
                 comanda.porta_pos = porta_pos;
                 comanda.id_term = id_term;
                 comanda.ricevuta_pos_scontrino = ricevuta_pos_scontrino;*/

                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field406='" + ip_pos + "',\n\
                                        Field407='" + porta_pos + "',\n\
                                        Field408='" + id_term + "',\n\
                                        Field405='" + pos_abilitato + "',\n\
                                        Field409='" + ricevuta_pos_scontrino + "'\n\
                                         where id=" + comanda.folder_number + " ;";
                break;
            case "PRINCIPALI":

                var lingua_visualizzazione = $('#form_settaggi select[name="lingua_visualizzazione"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var lingua_stampa = $('#form_settaggi select[name="lingua_stampa"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var servizio_fino_alle = $('#form_settaggi select[name="servizio_fino_alle"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var coperti_obbligatori = $('#form_settaggi input[name="coperti_obbligatori"]:visible').is(':checked') ? '1' : '0';
                var orario_preparazione = $('#form_settaggi input[name="orario_preparazione"]:visible').is(':checked') ? '1' : '0';
                if (orario_preparazione === "0") {
                    let query_adattamento_tipo_consegna = "update settaggi_profili set Field471='C', Field472='C';";
                    alasql(query_adattamento_tipo_consegna);
                    comanda.sock.send({
                        tipo: "aggiornamento_settaggi_principali",
                        operatore: comanda.operatore,
                        query: query_adattamento_tipo_consegna,
                        terminale: comanda.terminale,
                        ip: comanda.ip_address
                    });
                    $("[name='tempo_preparazione_default']").val("");
                    $("[name='tempo_preparazione_default_domicilio']").val("");
                }


                var tempo_preparazione_default = $('#form_settaggi input[name="tempo_preparazione_default"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var tempo_preparazione_default_domicilio = $('#form_settaggi input[name="tempo_preparazione_default_domicilio"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var time_gap_consegna = $('#form_settaggi input[name="time_gap_consegna"]:visible').is(':checked') ? '1' : '0';
                var abilita_fattorino1 = $('#form_settaggi input[name="abilita_fattorino1"]:visible').is(':checked') ? '1' : '0';
                var abilita_fattorino2 = $('#form_settaggi input[name="abilita_fattorino2"]:visible').is(':checked') ? '1' : '0';
                var abilita_fattorino3 = $('#form_settaggi input[name="abilita_fattorino3"]:visible').is(':checked') ? '1' : '0';
                var nome_fattorino1 = $('#form_settaggi input[name="nome_fattorino1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var nome_fattorino2 = $('#form_settaggi input[name="nome_fattorino2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var nome_fattorino3 = $('#form_settaggi input[name="nome_fattorino3"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var avviso_righe_prezzo_zero = $('#form_settaggi input[name="righe_prezzo_zero"]:visible').is(':checked') ? '1' : '0';
                var valore_coperti = $('#form_settaggi input[name="valore_coperto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var valore_servizio = $('#form_settaggi input[name="valore_servizio"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var consegna_a_pizza = $('#form_settaggi input[name="consegna_a_pizza"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var consegna_mezzo_metro = $('#form_settaggi input[name="consegna_mezzo_metro"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var consegna_maxi = $('#form_settaggi input[name="consegna_maxi"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var consegna_metro = $('#form_settaggi input[name="consegna_metro"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var consegna_su_totale = $('#form_settaggi input[name="consegna_su_totale"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var iva_default = $('#form_settaggi input[name="iva_default"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var iva_takeaway = $('#form_settaggi input[name="iva_takeaway"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var aliquota_consegna = $('#form_settaggi input[name="aliquota_consegna"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var reparto_servizi_default = $('#form_settaggi select[name="reparto_servizi_default"]:visible').val();
                var reparto_beni_default = $('#form_settaggi select[name="reparto_beni_default"]:visible').val();
                var reparto_consegna = $('#form_settaggi select[name="reparto_consegna"]:visible').val();
                var reparto_ritiro = $('#form_settaggi select[name="reparto_ritiro"]:visible').val();
                var reparto_domicilio = $('#form_settaggi select[name="reparto_domicilio"]:visible').val();
                var reparto_mangiaqui = $('#form_settaggi select[name="reparto_mangiaqui"]:visible').val();
                var scelta_categoria = $('#form_settaggi select[name="scelta_categoria"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var fastorder = $('#form_settaggi input[name="fastorder"]:visible').is(':checked') ? '1' : '0';
                var iva_estera_abilitata = $('#form_settaggi input[name="iva_estera_abilitata"]:visible').is(':checked') ? '1' : '0';
                var una_pizza = $('#form_settaggi input[name="una_pizza"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var piu_di_una_pizza = $('#form_settaggi input[name="piu_di_una_pizza"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var da_fascia_1 = $('#form_settaggi input[name="da_fascia_1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var a_fascia_1 = $('#form_settaggi input[name="a_fascia_1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var da_fascia_2 = $('#form_settaggi input[name="da_fascia_2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var a_fascia_2 = $('#form_settaggi input[name="a_fascia_2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var fissa_una_pizza = $('#form_settaggi input[name="fissa_una_pizza"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var fissa_piu_di_una_pizza = $('#form_settaggi input[name="fissa_piu_di_una_pizza"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var fissa_da_fascia_1 = $('#form_settaggi input[name="fissa_da_fascia_1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var fissa_a_fascia_1 = $('#form_settaggi input[name="fissa_a_fascia_1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var fissa_da_fascia_2 = $('#form_settaggi input[name="fissa_da_fascia_2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var fissa_a_fascia_2 = $('#form_settaggi input[name="fissa_a_fascia_2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var disattivo_promo = $('#form_settaggi input[name="disattivi_promo"]:visible').is(':checked') ? '1' : '0';
                var salva_numero_associa = $('#form_settaggi input[name="salva_numero_associa"]:visible').is(':checked') ? '1' : '0';
                var testo_query = "UPDATE impostazioni_fiscali SET \n\
                                        fissa_una_pizza='" + fissa_una_pizza + "',\n\
                                        fissa_piu_di_una_pizza='" + fissa_piu_di_una_pizza + "',\n\
                                        fissa_da_fascia_1='" + fissa_da_fascia_1 + "',\n\
                                        fissa_a_fascia_1='" + fissa_a_fascia_1 + "',\n\
                                        fissa_da_fascia_2='" + fissa_da_fascia_2 + "',\n\
                                        fissa_a_fascia_2='" + fissa_a_fascia_2 + "',\n\
                                        da_fascia_1='" + da_fascia_1 + "',\n\
                                        a_fascia_1='" + a_fascia_1 + "',\n\
                                        da_fascia_2='" + da_fascia_2 + "',\n\
                                        a_fascia_2='" + a_fascia_2 + "',\n\
                                        lingua_stampa='" + lingua_stampa + "',\n\
                                        coperti_obbligatorio='" + coperti_obbligatori + "',\n\
                                        orario_preparazione='" + orario_preparazione + "',\n\
                                        tempo_preparazione_default='" + tempo_preparazione_default + "',\n\
                                        tempo_preparazione_default_domicilio='" + tempo_preparazione_default_domicilio + "',\n\
                                        abilita_fattorino1='" + abilita_fattorino1 + "',\n\
                                        abilita_fattorino2='" + abilita_fattorino2 + "',\n\
                                        abilita_fattorino3='" + abilita_fattorino3 + "',\n\
                                        nome_fattorino1='" + nome_fattorino1 + "',\n\
                                        nome_fattorino2='" + nome_fattorino2 + "',\n\
                                        nome_fattorino3='" + nome_fattorino3 + "',\n\
                                        righe_zero='" + avviso_righe_prezzo_zero + "',\n\
                                        valore_coperto='" + valore_coperti + "',\n\
                                        valore_servizio='" + valore_servizio + "',\n\
                                        consegna_a_pizza='" + consegna_a_pizza + "',\n\
                                        consegna_mezzo_metro='" + consegna_mezzo_metro + "',\n\
                                        consegna_maxi='" + consegna_maxi + "',\n\
                                        consegna_metro='" + consegna_metro + "',\n\
                                        consegna_su_totale='" + consegna_su_totale + "',\n\
                                        una_pizza='" + una_pizza + "',\n\
                                        piu_di_una_pizza='" + piu_di_una_pizza + "',\n\
                                        aliquota_default='" + iva_default + "',\n\
                                        iva_estera_abilitata='" + iva_estera_abilitata + "',\n\
                                        aliquota_takeaway='" + iva_takeaway + "',\n\
                                        reparto_servizi_default='" + reparto_servizi_default + "',\n\
                                        reparto_beni_default='" + reparto_beni_default + "',\n\
                                        aliquota_consegna='" + aliquota_consegna + "',\n\
                                        reparto_consegna='" + reparto_consegna + "',\n\
                                        reparto_ritiro='" + reparto_ritiro + "',\n\
                                        reparto_domicilio='" + reparto_domicilio + "',\n\
                                        reparto_mangiaqui='" + reparto_mangiaqui + "',\n\
                                        categoria_partenza='" + scelta_categoria + "',\n\
                                        categoria_partenza_mobile='" + scelta_categoria + "',\n\
                                        fastorder='" + fastorder + "',\n\
                                        time_gap_consegna='" + time_gap_consegna + "',\n\
                                        salva_numero_associa='"+ salva_numero_associa + "'\n\
                                        where id=" + comanda.folder_number + " ;";
                if (time_gap_consegna === "1") {
                    $(".time_gap_consegna_155").show();
                    $(".time_gap_consegna_300").show();
                    $("#time_gap_consegna_15").show();
                    $("#time_gap_consegna_30").show();
                } else {

                    $(".time_gap_consegna_155").hide();
                    $(".time_gap_consegna_300").hide();
                    $("#time_gap_consegna_15").hide();
                    $("#time_gap_consegna_30").hide();

                }
                if (salva_numero_associa === "1") {
                    $("#salva_numero_associa").show();
                } else {

                    $("#salva_numero_associa").hide();


                }
                var testo_query_servizio = "UPDATE dati_servizio SET ora_finale_servizio='" + servizio_fino_alle + "' where id='" + comanda.folder_number + "';";
                comanda.sock.send({
                    tipo: "aggiornamento_settaggi_principali",
                    operatore: comanda.operatore,
                    query: testo_query_servizio,
                    terminale: comanda.terminale,
                    ip: comanda.ip_address
                });
                var testo_querry_settagio = " UPDATE settaggi_profili SET \n\
                Field510 ='" + disattivo_promo + "' \n\
                where id=" + comanda.folder_number + " ;";
                alasql(testo_querry_settagio);
                comanda.sock.send({
                    tipo: "aggiornamento_settaggi_principali",
                    operatore: comanda.operatore,
                    query: testo_querry_settagio,
                    terminale: comanda.terminale,
                    ip: comanda.ip_address
                });


                comanda.sincro.query(testo_query_servizio, function () {

                    comanda.consegna_a_pizza = consegna_a_pizza;
                    comanda.consegna_maxi = consegna_maxi;
                    comanda.consegna_mezzo_metro = consegna_mezzo_metro;
                    comanda.consegna_metro = consegna_metro;
                    comanda.consegna_su_totale = consegna_su_totale;
                    comanda.fissa_da_fascia_1 = fissa_da_fascia_1;
                    comanda.fissa_a_fascia_1 = fissa_a_fascia_1;
                    comanda.fissa_da_fascia_2 = fissa_da_fascia_2;
                    comanda.fissa_a_fascia_2 = fissa_a_fascia_2;
                    comanda.fissa_una_pizza = fissa_una_pizza;
                    comanda.fissa_piu_di_una_pizza = fissa_piu_di_una_pizza;
                    comanda.da_fascia_1 = da_fascia_1;
                    comanda.a_fascia_1 = a_fascia_1;
                    comanda.da_fascia_2 = da_fascia_2;
                    comanda.a_fascia_2 = a_fascia_2;
                    comanda.una_pizza = una_pizza;
                    comanda.piu_di_una_pizza = piu_di_una_pizza;
                    comanda.valore_coperto = valore_coperti;
                    comanda.coperti_obbligatorio = coperti_obbligatori;
                    comanda.orario_preparazione = orario_preparazione;
                    comanda.tempo_preparazione_default = tempo_preparazione_default;
                    comanda.tempo_preparazione_default_domicilio = tempo_preparazione_default_domicilio;
                    comanda.abilita_fattorino1 = abilita_fattorino1;
                    comanda.abilita_fattorino2 = abilita_fattorino2;
                    comanda.abilita_fattorino3 = abilita_fattorino3;
                    comanda.nome_fattorino1 = nome_fattorino1;
                    comanda.nome_fattorino2 = nome_fattorino2;
                    comanda.nome_fattorino3 = nome_fattorino3;
                    comanda.disattivo_promo = disattivo_promo;
                    if (comanda.nome_fattorino1 !== undefined && comanda.nome_fattorino1 !== null && comanda.nome_fattorino1.trim().length > 0) {
                        $("select#tipologia_fattorino option[value=1]").text(comanda.nome_fattorino1)
                    }
                    if (comanda.nome_fattorino2 !== undefined && comanda.nome_fattorino2 !== null && comanda.nome_fattorino2.trim().length > 0) {
                        $("select#tipologia_fattorino option[value=2]").text(comanda.nome_fattorino2)
                    }
                    if (comanda.nome_fattorino3 !== undefined && comanda.nome_fattorino3 !== null && comanda.nome_fattorino3.trim().length > 0) {
                        $("select#tipologia_fattorino option[value=3]").text(comanda.nome_fattorino3)
                    }

                    comanda.percentuale_servizio = valore_servizio;
                    comanda.categoria_partenza = scelta_categoria;
                    comanda.categoria_predefinita = scelta_categoria;
                    comanda.partita_iva_estera = false;
                    if (iva_estera_abilitata === "1") {
                        comanda.partita_iva_estera = true;
                    }
                    alert("SE VUOI VEDERE EVENTUALI MODIFICHE ALLA LINGUA DI STAMPA E/O VISUALIZZAZIONE, DEVI RIAVVIARE TUTTI I DISPOSITIVI.");
                });
                break;
        }
    } else {
        switch (tipologia) {
            case "COMANDA":



                var intestazione_riga1 = $('#form_settaggi input[name="intestazione_riga1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga1_grassetto = $('#form_settaggi select[name="intestazione_riga1_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spaziointestazione1 = $('#form_settaggi input[name="spaziointestazione1"]:visible').is(':checked');
                var intestazione_riga2 = $('#form_settaggi input[name="intestazione_riga2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga2_grassetto = $('#form_settaggi select[name="intestazione_riga2_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spaziointestazione2 = $('#form_settaggi input[name="spaziointestazione2"]:visible').is(':checked');
                var intestazione_riga3 = $('#form_settaggi input[name="intestazione_riga3"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga3_grassetto = $('#form_settaggi select[name="intestazione_riga3_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spaziointestazione3 = $('#form_settaggi input[name="spaziointestazione3"]:visible').is(':checked');
                var intestazione_riga4 = $('#form_settaggi input[name="intestazione_riga4"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga4_grassetto = $('#form_settaggi select[name="intestazione_riga4_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var titolo = $('#form_settaggi input[name="titolo"]:visible').is(':checked');
                var nome_stampante = $('#form_settaggi input[name="nome_stampante"]:visible').is(':checked');
                var operatore = $('#form_settaggi input[name="operatore"]:visible').is(':checked');
                var data = $('#form_settaggi input[name="data"]:visible').is(':checked');
                var ora = $('#form_settaggi input[name="ora"]:visible').is(':checked');
                var coperti = $('#form_settaggi input[name="coperti"]:visible').is(':checked');
                var portate = $('#form_settaggi input[name="portate"]:visible').is(':checked');
                var portate_grassetto = $('#form_settaggi select[name="portate_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var font_operatore = $('#form_settaggi select[name="font_operatore"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var font_data = $('#form_settaggi select[name="font_data"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var font_ora = $('#form_settaggi select[name="font_ora"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var font_coperti = $('#form_settaggi select[name="font_coperti"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var font_articolo = $('#form_settaggi select[name="font_articolo"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var font_varianti = $('#form_settaggi select[name="font_varianti"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var unione_articoli_varianti = $('#form_settaggi input[name="unione_articoli_varianti"]:visible').is(':checked');
                var prezzo = $('#form_settaggi input[name="prezzo"]:visible').is(':checked');
                var ricetta = $('#form_settaggi input[name="ricetta"]:visible').is(':checked');
                var totale_quantita_articoli = $('#form_settaggi input[name="totale_quantita_articoli"]:visible').is(':checked');
                var msg_riga1 = $('#form_settaggi input[name="msg_riga1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga1_grassetto = $('#form_settaggi select[name="msg_riga1_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga1_allineamento = $('#form_settaggi select[name="msg_riga1_allineamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spazio1 = $('#form_settaggi input[name="spazio1"]:visible').is(':checked');
                var msg_riga2 = $('#form_settaggi input[name="msg_riga2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga2_grassetto = $('#form_settaggi select[name="msg_riga2_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga2_allineamento = $('#form_settaggi select[name="msg_riga2_allineamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spazio2 = $('#form_settaggi input[name="spazio2"]:visible').is(':checked');
                var msg_riga3 = $('#form_settaggi input[name="msg_riga3"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga3_grassetto = $('#form_settaggi select[name="msg_riga3_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga3_allineamento = $('#form_settaggi select[name="msg_riga3_allineamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var meno_specificato = $('#form_settaggi input[name="meno_specificato"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var variante_meno_evidenziata = $('#form_settaggi input[name="variante_meno_evidenziata"]:visible').is(':checked');
                var variante_piu_evidenziata = $('#form_settaggi input[name="variante_piu_evidenziata"]:visible').is(':checked');
                var variante_poco_evidenziata = $('#form_settaggi input[name="variante_poco_evidenziata"]:visible').is(':checked');
                var variante_fine_cottura_evidenziata = $('#form_settaggi input[name="variante_fine_cottura_evidenziata"]:visible').is(':checked');
                var specifica_evidenziata = $('#form_settaggi input[name="specifica_evidenziata"]:visible').is(':checked');
                var concomitanze = $('#form_settaggi input[name="concomitanze"]:visible').is(':checked');
                var ripetizione_tavolo = $('#form_settaggi input[name="ripetizione_tavolo"]:visible').is(':checked');
                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field34='" + intestazione_riga1 + "',\n\
                                        Field35 ='" + intestazione_riga1_grassetto + "', \n\
                                        Field36 ='" + spaziointestazione1 + "', \n\
                                        Field37='" + intestazione_riga2 + "',\n\
                                        Field38 ='" + intestazione_riga2_grassetto + "', \n\
                                        Field39 ='" + spaziointestazione2 + "', \n\
                                        Field40='" + intestazione_riga3 + "',\n\
                                        Field41 ='" + intestazione_riga3_grassetto + "', \n\
                                        Field42 ='" + spaziointestazione3 + "', \n\
                                        Field43='" + intestazione_riga4 + "',\n\
                                        Field44 ='" + intestazione_riga4_grassetto + "', \n\
                                        \n\
                                        Field3 ='" + titolo + "',\n\
                                        Field4 ='" + nome_stampante + "', \n\
                                        Field5 ='" + operatore + "', \n\
                                        Field6 ='" + data + "', \n\
                                        Field7 ='" + ora + "', \n\
                                        Field24 ='" + coperti + "', \n\
                                        Field8 ='" + portate + "',\n\
                                        Field30  ='" + font_operatore + "',\n\
                                        Field31  ='" + font_data + "',\n\
                                        Field32  ='" + font_ora + "',\n\
                                        Field33  ='" + font_coperti + "',\n\
                                        Field28  ='" + font_articolo + "',\n\
                                        Field29  ='" + font_varianti + "',\n\
                                        Field25  ='" + portate_grassetto + "',\n\
                                        Field9 ='" + unione_articoli_varianti + "', \n\
                                        Field10 ='" + prezzo + "', \n\
                                        Field11 ='" + ricetta + "', \n\
                                        Field12 ='" + totale_quantita_articoli + "', \n\
                                        Field13 ='" + msg_riga1 + "', \n\
                                        Field14 ='" + msg_riga1_grassetto + "', \n\
                                        Field15 ='" + msg_riga1_allineamento + "', \n\
                                        Field26 ='" + spazio1 + "', \n\
                                        Field16 ='" + msg_riga2 + "', \n\
                                        Field17 ='" + msg_riga2_grassetto + "', \n\
                                        Field18 ='" + msg_riga2_allineamento + "', \n\
                                        Field27 ='" + spazio2 + "', \n\
                                        Field19 ='" + msg_riga3 + "', \n\
                                        Field20 ='" + msg_riga3_grassetto + "', \n\
                                        Field21 ='" + msg_riga3_allineamento + "', \n\
                                        Field22 ='" + concomitanze + "', \n\
                                        Field307 ='" + meno_specificato + "', \n\
                                        Field310 ='" + variante_meno_evidenziata + "', \n\
                                        Field410 ='" + variante_piu_evidenziata + "', \n\
                                        Field422 ='" + variante_poco_evidenziata + "', \n\
                                        Field423 ='" + variante_fine_cottura_evidenziata + "', \n\
                                        Field424 ='" + specifica_evidenziata + "', \n\
                                        Field427 ='" + ripetizione_tavolo + "' \n\
                                         where id=" + comanda.folder_number + " ;";
                comanda.meno_specificato = meno_specificato;
                comanda.specifica_evidenziata = specifica_evidenziata;
                comanda.variante_meno_evidenziata = variante_meno_evidenziata;
                comanda.variante_piu_evidenziata = variante_piu_evidenziata;
                comanda.variante_fine_cottura_evidenziata = variante_fine_cottura_evidenziata;
                comanda.variante_piu_evidenziata = variante_piu_evidenziata;
                break;
            case "COMANDA_ASPORTO":

                var scritta_consegna_domicilio = $('#form_settaggi input[name="scritta_consegna_domicilio"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var scritta_consegna_ritiro = $('#form_settaggi input[name="scritta_consegna_ritiro"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                /*var scritta_consegna_pizzeria = $('#form_settaggi input[name="scritta_consegna_pizzeria"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');*/




                var intestazione_riga1 = $('#form_settaggi input[name="intestazione_riga1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga1_grassetto = $('#form_settaggi select[name="intestazione_riga1_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spaziointestazione1 = $('#form_settaggi input[name="spaziointestazione1"]:visible').is(':checked');
                var intestazione_riga2 = $('#form_settaggi input[name="intestazione_riga2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga2_grassetto = $('#form_settaggi select[name="intestazione_riga2_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spaziointestazione2 = $('#form_settaggi input[name="spaziointestazione2"]:visible').is(':checked');
                var intestazione_riga3 = $('#form_settaggi input[name="intestazione_riga3"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga3_grassetto = $('#form_settaggi select[name="intestazione_riga3_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spaziointestazione3 = $('#form_settaggi input[name="spaziointestazione3"]:visible').is(':checked');
                var intestazione_riga4 = $('#form_settaggi input[name="intestazione_riga4"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga4_grassetto = $('#form_settaggi select[name="intestazione_riga4_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var titolo = $('#form_settaggi input[name="titolo"]:visible').is(':checked');
                var nome_stampante = $('#form_settaggi input[name="nome_stampante"]:visible').is(':checked');
                var operatore = $('#form_settaggi input[name="operatore"]:visible').is(':checked');
                var font_operatore = $('#form_settaggi select[name="font_operatore"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var data = $('#form_settaggi input[name="data"]:visible').is(':checked');
                var font_data = $('#form_settaggi select[name="font_data"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var ora = $('#form_settaggi input[name="ora"]:visible').is(':checked');
                var font_ora = $('#form_settaggi select[name="font_ora"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var portate = $('#form_settaggi input[name="portate"]:visible').is(':checked');
                var portate_grassetto = $('#form_settaggi select[name="portate_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var prezzo = $('#form_settaggi input[name="prezzo"]:visible').is(':checked');
                var totale_prezzo_articoli = $('#form_settaggi input[name="totale_prezzo_articoli"]:visible').is(':checked');
                var font_articolo = $('#form_settaggi select[name="font_articolo"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var font_varianti = $('#form_settaggi select[name="font_varianti"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var unione_articoli_varianti = $('#form_settaggi input[name="unione_articoli_varianti"]:visible').is(':checked');
                var ricetta = $('#form_settaggi input[name="ricetta"]:visible').is(':checked');
                var totale_pizze = $('#form_settaggi input[name="totale_pizze"]:visible').is(':checked');
                var totale_bibite = $('#form_settaggi input[name="totale_bibite"]:visible').is(':checked');
                var totale_quantita_articoli = $('#form_settaggi input[name="totale_quantita_articoli"]:visible').is(':checked');
                var msg_riga1 = $('#form_settaggi input[name="msg_riga1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga1_grassetto = $('#form_settaggi select[name="msg_riga1_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga1_allineamento = $('#form_settaggi select[name="msg_riga1_allineamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spazio1 = $('#form_settaggi input[name="spazio1"]:visible').is(':checked');
                var msg_riga2 = $('#form_settaggi input[name="msg_riga2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga2_grassetto = $('#form_settaggi select[name="msg_riga2_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga2_allineamento = $('#form_settaggi select[name="msg_riga2_allineamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spazio2 = $('#form_settaggi input[name="spazio2"]:visible').is(':checked');
                var msg_riga3 = $('#form_settaggi input[name="msg_riga3"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga3_grassetto = $('#form_settaggi select[name="msg_riga3_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga3_allineamento = $('#form_settaggi select[name="msg_riga3_allineamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var concomitanze = $('#form_settaggi input[name="concomitanze"]:visible').is(':checked');
                var dati_cliente = $('#form_settaggi input[name="dati_cliente"]:visible').is(':checked');
                var qrcode_navigatore = $('#form_settaggi input[name="qrcode_navigatore"]:visible').is(':checked');
                var meno_specificato = $('#form_settaggi input[name="meno_specificato"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var variante_meno_evidenziata = $('#form_settaggi input[name="variante_meno_evidenziata"]:visible').is(':checked');
                var variante_piu_evidenziata = $('#form_settaggi input[name="variante_piu_evidenziata"]:visible').is(':checked');
                var variante_poco_evidenziata = $('#form_settaggi input[name="variante_poco_evidenziata"]:visible').is(':checked');
                var variante_fine_cottura_evidenziata = $('#form_settaggi input[name="variante_fine_cottura_evidenziata"]:visible').is(':checked');
                var specifica_evidenziata = $('#form_settaggi input[name="specifica_evidenziata"]:visible').is(':checked');
                var ricetta = $('#form_settaggi input[name="ricetta"]:visible').is(':checked');
                var ricetta_grassetto = $('#form_settaggi  select[name="ricetta_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var ricetta_maiuscola = $('#form_settaggi input[name="ricetta_maiuscola"]:visible').is(':checked');
                var articoli_evidenziati_cat_1 = $('#form_settaggi  select[name="articoli_evidenziati_cat_1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var articoli_evidenziati_cat_2 = $('#form_settaggi  select[name="articoli_evidenziati_cat_2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field383='" + articoli_evidenziati_cat_1 + "',\n\
                                        Field384='" + articoli_evidenziati_cat_2 + "',\n\
                                        Field381='" + totale_pizze + "',\n\
                                        Field382='" + totale_bibite + "',\n\
                                        Field378='" + ricetta + "',\n\
                                        Field379='" + ricetta_maiuscola + "',\n\
                                        Field380='" + ricetta_grassetto + "',\n\
                                        \n\
                                        Field375='" + scritta_consegna_domicilio + "',\n\
                                        Field376='" + scritta_consegna_ritiro + "',\n\
                                       /* Field377='" + scritta_consegna_pizzeria + "',*/\n\
                                        Field111='" + intestazione_riga1 + "',\n\
                                        Field112 ='" + intestazione_riga1_grassetto + "', \n\
                                        Field113 ='" + spaziointestazione1 + "', \n\
                                        Field114='" + intestazione_riga2 + "',\n\
                                        Field115 ='" + intestazione_riga2_grassetto + "', \n\
                                        Field116 ='" + spaziointestazione2 + "', \n\
                                        Field117='" + intestazione_riga3 + "',\n\
                                        Field118 ='" + intestazione_riga3_grassetto + "', \n\
                                        Field119 ='" + spaziointestazione3 + "', \n\
                                        Field120='" + intestazione_riga4 + "',\n\
                                        Field121 ='" + intestazione_riga4_grassetto + "', \n\
                                        \n\
                                        Field122 ='" + titolo + "',\n\
                                        Field123 ='" + nome_stampante + "', \n\
                                        Field124 ='" + operatore + "', \n\
                                        Field125  ='" + font_operatore + "',\n\
                                        Field126 ='" + data + "', \n\
                                        Field127  ='" + font_data + "',\n\
                                        Field128 ='" + ora + "', \n\
                                        Field129  ='" + font_ora + "',\n\
                                        Field130 ='" + portate + "',\n\
                                        Field131  ='" + portate_grassetto + "',\n\
                                        Field132 ='" + prezzo + "', \n\
                                        Field189 ='" + totale_prezzo_articoli + "', \n\
                                        Field133  ='" + font_articolo + "',\n\
                                        Field134  ='" + font_varianti + "',\n\
                                        Field135 ='" + unione_articoli_varianti + "', \n\
                                        Field136 ='" + ricetta + "', \n\
                                        Field137 ='" + totale_quantita_articoli + "', \n\
                                        Field138 ='" + msg_riga1 + "', \n\
                                        Field139 ='" + msg_riga1_grassetto + "', \n\
                                        Field140 ='" + msg_riga1_allineamento + "', \n\
                                        Field141 ='" + spazio1 + "', \n\
                                        Field142 ='" + msg_riga2 + "', \n\
                                        Field143 ='" + msg_riga2_grassetto + "', \n\
                                        Field144 ='" + msg_riga2_allineamento + "', \n\
                                        Field145 ='" + spazio2 + "', \n\
                                        Field146 ='" + msg_riga3 + "', \n\
                                        Field147 ='" + msg_riga3_grassetto + "', \n\
                                        Field148 ='" + msg_riga3_allineamento + "', \n\
                                        Field149 ='" + concomitanze + "', \n\
                                        Field252 ='" + dati_cliente + "', \n\
                                        Field307 ='" + meno_specificato + "', \n\
                                        Field310 ='" + variante_meno_evidenziata + "', \n\
                                        Field422 ='" + variante_poco_evidenziata + "', \n\
                                        Field423 ='" + variante_fine_cottura_evidenziata + "', \n\
                                        Field410 ='" + variante_piu_evidenziata + "', \n\
                                        Field424 ='" + specifica_evidenziata + "', \n\
                                        Field253 ='" + qrcode_navigatore + "' \n\
                                         where id=" + comanda.folder_number + " ;";
                comanda.scritta_consegna_domicilio = scritta_consegna_domicilio;
                comanda.scritta_consegna_ritiro = scritta_consegna_ritiro;
                /*comanda.scritta_consegna_pizzeria = scritta_consegna_pizzeria;*/

                comanda.meno_specificato = meno_specificato;
                comanda.specifica_evidenziata = specifica_evidenziata;
                comanda.variante_meno_evidenziata = variante_meno_evidenziata;
                comanda.variante_poco_evidenziata = variante_poco_evidenziata;
                comanda.variante_fine_cottura_evidenziata = variante_fine_cottura_evidenziata;
                comanda.variante_piu_evidenziata = variante_piu_evidenziata;
                break;
            case "CONTO":

                var intestazione_riga1 = $('#form_settaggi input[name="intestazione_riga1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga1_grassetto = $('#form_settaggi select[name="intestazione_riga1_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spaziointestazione1 = $('#form_settaggi input[name="spaziointestazione1"]:visible').is(':checked');
                var intestazione_riga2 = $('#form_settaggi input[name="intestazione_riga2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga2_grassetto = $('#form_settaggi select[name="intestazione_riga2_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spaziointestazione2 = $('#form_settaggi input[name="spaziointestazione2"]:visible').is(':checked');
                var intestazione_riga3 = $('#form_settaggi input[name="intestazione_riga3"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga3_grassetto = $('#form_settaggi select[name="intestazione_riga3_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spaziointestazione3 = $('#form_settaggi input[name="spaziointestazione3"]:visible').is(':checked');
                var intestazione_riga4 = $('#form_settaggi input[name="intestazione_riga4"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga4_grassetto = $('#form_settaggi select[name="intestazione_riga4_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var titolo = $('#form_settaggi input[name="titolo"]:visible').is(':checked');
                var nome_parcheggio_bar = $('#form_settaggi input[name="nome_parcheggio_bar"]:visible').is(':checked');
                var operatore = $('#form_settaggi input[name="operatore"]:visible').is(':checked');
                var font_operatore = $('#form_settaggi select[name="font_operatore"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var data = $('#form_settaggi input[name="data"]:visible').is(':checked');
                var font_data = $('#form_settaggi select[name="font_data"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var ora = $('#form_settaggi input[name="ora"]:visible').is(':checked');
                var font_ora = $('#form_settaggi select[name="font_ora"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var font_articolo = $('#form_settaggi select[name="font_articolo"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var font_varianti = $('#form_settaggi select[name="font_varianti"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var totale_quantita_articoli = $('#form_settaggi input[name="totale_quantita_articoli"]:visible').is(':checked');
                var msg_riga1 = $('#form_settaggi input[name="msg_riga1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga1_grassetto = $('#form_settaggi select[name="msg_riga1_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga1_allineamento = $('#form_settaggi select[name="msg_riga1_allineamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spazio1 = $('#form_settaggi input[name="spazio1"]:visible').is(':checked');
                var msg_riga2 = $('#form_settaggi input[name="msg_riga2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga2_grassetto = $('#form_settaggi select[name="msg_riga2_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga2_allineamento = $('#form_settaggi select[name="msg_riga2_allineamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spazio2 = $('#form_settaggi input[name="spazio2"]:visible').is(':checked');
                var msg_riga3 = $('#form_settaggi input[name="msg_riga3"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga3_grassetto = $('#form_settaggi select[name="msg_riga3_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga3_allineamento = $('#form_settaggi select[name="msg_riga3_allineamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var doppia_copia = $('#form_settaggi input[name="doppia_copia"]:visible').is(':checked');
                var conto_in_lingua = $('#form_settaggi input[name="conto_in_lingua"]:visible').is(':checked');
                var divisione_persone_esclude_divisione_coperti = $('#form_settaggi input[name="divisione_persone_esclude_divisione_coperti"]:visible').is(':checked');
                var barcode_riapertura_ordine_scontrino = $('#form_settaggi input[name="barcode_riapertura_ordine_scontrino"]:visible').is(':checked');
                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field500 ='" + barcode_riapertura_ordine_scontrino + "', \n\
                                        Field45='" + intestazione_riga1 + "',\n\
                                        Field46 ='" + intestazione_riga1_grassetto + "', \n\
                                        Field47 ='" + spaziointestazione1 + "', \n\
                                        Field48='" + intestazione_riga2 + "',\n\
                                        Field49 ='" + intestazione_riga2_grassetto + "', \n\
                                        Field50 ='" + spaziointestazione2 + "', \n\
                                        Field51 ='" + intestazione_riga3 + "',\n\
                                        Field52 ='" + intestazione_riga3_grassetto + "', \n\
                                        Field53 ='" + spaziointestazione3 + "', \n\
                                        Field54='" + intestazione_riga4 + "',\n\
                                        Field55 ='" + intestazione_riga4_grassetto + "', \n\
                                        \n\
                                        Field56 ='" + titolo + "',\n\
                                        Field105 ='" + nome_parcheggio_bar + "',\n\
                                        Field58 ='" + operatore + "', \n\
                                        Field59  ='" + font_operatore + "',\n\
                                        Field60 ='" + data + "', \n\
                                        Field61  ='" + font_data + "',\n\
                                        Field62 ='" + ora + "', \n\
                                        Field63  ='" + font_ora + "',\n\
                                        \n\
                                        Field66  ='" + font_articolo + "',\n\
                                        Field67  ='" + font_varianti + "',\n\
                                        \n\
                                        Field68 ='" + totale_quantita_articoli + "', \n\
                                        Field69 ='" + msg_riga1 + "', \n\
                                        Field70 ='" + msg_riga1_grassetto + "', \n\
                                        Field71 ='" + msg_riga1_allineamento + "', \n\
                                        Field72 ='" + spazio1 + "', \n\
                                        Field73 ='" + msg_riga2 + "', \n\
                                        Field74 ='" + msg_riga2_grassetto + "', \n\
                                        Field75='" + msg_riga2_allineamento + "', \n\
                                        Field76 ='" + spazio2 + "', \n\
                                        Field77 ='" + msg_riga3 + "', \n\
                                        Field78 ='" + msg_riga3_grassetto + "', \n\
                                        Field79 ='" + msg_riga3_allineamento + "', \n\
                                        Field358 ='" + conto_in_lingua + "', \n\
                                        Field359 ='" + divisione_persone_esclude_divisione_coperti + "', \n\
                                        Field80 ='" + doppia_copia + "' \n\
                                         where id=" + comanda.folder_number + " ;";
                if (barcode_riapertura_ordine_scontrino.toString() === "true" || test[0].Field499 === "true" || test[0].Field498 === "true") {
                    $(".fa-barcode").show();
                } else {
                    $(".fa-barcode").hide();
                }
                break;
            case "VISORE ASPORTO":

                var testo_riga1 = $('#form_settaggi input[name="testo_riga1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var testo_riga2 = $('#form_settaggi input[name="testo_riga2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var numero_secondi_visore = $('#form_settaggi input[name="numero_secondi_visore"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var tasto_totale_visore = $('#form_settaggi input[name="tasto_totale_visore"]:visible').is(':checked');
                var numero_secondi_totale_visore = $('#form_settaggi input[name="numero_secondi_totale_visore"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field415 ='" + testo_riga1 + "', \n\
                                        Field416 ='" + testo_riga2 + "', \n\
                                        Field417 ='" + numero_secondi_visore + "', \n\
                                        Field418 ='" + tasto_totale_visore + "', \n\
                                        Field419 ='" + numero_secondi_totale_visore + "' \n\
        \n\
                                        where id=" + comanda.folder_number + ";";
                break;
            case "SCONTRINO":

                var msg_riga1 = $('#form_settaggi input[name="msg_riga1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga1_grassetto = $('#form_settaggi select[name="msg_riga1_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga1_allineamento = $('#form_settaggi select[name="msg_riga1_allineamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spazio1 = $('#form_settaggi input[name="spazio1"]:visible').is(':checked');
                var msg_riga2 = $('#form_settaggi input[name="msg_riga2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga2_grassetto = $('#form_settaggi select[name="msg_riga2_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga2_allineamento = $('#form_settaggi select[name="msg_riga2_allineamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spazio2 = $('#form_settaggi input[name="spazio2"]:visible').is(':checked');
                var msg_riga3 = $('#form_settaggi input[name="msg_riga3"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga3_grassetto = $('#form_settaggi select[name="msg_riga3_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga3_allineamento = $('#form_settaggi select[name="msg_riga3_allineamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spazio3 = $('#form_settaggi input[name="spazio3"]:visible').is(':checked');
                var msg_riga4 = $('#form_settaggi input[name="msg_riga4"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga4_grassetto = $('#form_settaggi select[name="msg_riga4_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga4_allineamento = $('#form_settaggi select[name="msg_riga4_allineamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var url_qr_code = $('#form_settaggi input[name="url_qr_code"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var numero_tavolo_scontrino = $('#form_settaggi input[name="numero_tavolo_scontrino"]:visible').is(':checked');
                var divisione_persone_esclude_divisione_coperti = $('#form_settaggi input[name="divisione_persone_esclude_divisione_coperti"]:visible').is(':checked');
                var barcode_riapertura_ordine_scontrino = $('#form_settaggi input[name="barcode_riapertura_ordine_scontrino"]:visible').is(':checked');
                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field500 ='" + barcode_riapertura_ordine_scontrino + "', \n\
                                        Field360 ='" + divisione_persone_esclude_divisione_coperti + "', \n\
                                        Field327 ='" + numero_tavolo_scontrino + "', \n\
                                        Field236 ='" + msg_riga1 + "', \n\
                                        Field241 ='" + msg_riga1_grassetto + "', \n\
                                        Field242 ='" + msg_riga1_allineamento + "', \n\
                                        Field237 ='" + spazio1 + "', \n\
                                        Field238 ='" + msg_riga2 + "', \n\
                                        Field243 ='" + msg_riga2_grassetto + "', \n\
                                        Field244='" + msg_riga2_allineamento + "', \n\
                                        Field239 ='" + spazio2 + "', \n\
                                        Field240 ='" + msg_riga3 + "', \n\
                                        Field245 ='" + msg_riga3_grassetto + "', \n\
                                        Field246 ='" + msg_riga3_allineamento + "',\n\
                                        Field247 ='" + spazio3 + "', \n\
                                        Field248 ='" + msg_riga4 + "', \n\
                                        Field249 ='" + msg_riga4_grassetto + "', \n\
                                        Field250 ='" + msg_riga4_allineamento + "',\n\
                                        Field251 ='" + url_qr_code + "'\n\
                                         where id=" + comanda.folder_number + " ;";
                if (barcode_riapertura_ordine_scontrino.toString() === "true" || test[0].Field499 === "true" || test[0].Field498 === "true") {
                    $(".fa-barcode").show();
                } else {
                    $(".fa-barcode").hide();
                }
                comanda.numero_tavolo_scontrino = numero_tavolo_scontrino;
                comanda.scontrino_msg_riga1 = msg_riga1;
                comanda.scontrino_msg_riga1_grassetto = msg_riga1_grassetto;
                comanda.scontrino_msg_riga1_allineamento = msg_riga1_allineamento;
                comanda.scontrino_spazio1 = spazio1;
                comanda.scontrino_msg_riga2 = msg_riga2;
                comanda.scontrino_msg_riga2_grassetto = msg_riga2_grassetto;
                comanda.scontrino_msg_riga2_allineamento = msg_riga2_allineamento;
                comanda.scontrino_spazio2 = spazio2;
                comanda.scontrino_msg_riga3 = msg_riga3;
                comanda.scontrino_msg_riga3_grassetto = msg_riga3_grassetto;
                comanda.scontrino_msg_riga3_allineamento = msg_riga3_allineamento;
                comanda.scontrino_spazio3 = spazio3;
                comanda.scontrino_msg_riga4 = msg_riga4;
                comanda.scontrino_msg_riga4_grassetto = msg_riga4_grassetto;
                comanda.scontrino_msg_riga4_allineamento = msg_riga4_allineamento;
                comanda.url_qr_code = url_qr_code;
                break;
            case "DETTAGLIO VENDUTI":

                var intestazione_riga1 = $('#form_settaggi input[name="intestazione_riga1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga1_grassetto = $('#form_settaggi select[name="intestazione_riga1_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spaziointestazione1 = $('#form_settaggi input[name="spaziointestazione1"]:visible').is(':checked');
                var intestazione_riga2 = $('#form_settaggi input[name="intestazione_riga2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga2_grassetto = $('#form_settaggi select[name="intestazione_riga2_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spaziointestazione2 = $('#form_settaggi input[name="spaziointestazione2"]:visible').is(':checked');
                var intestazione_riga3 = $('#form_settaggi input[name="intestazione_riga3"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga3_grassetto = $('#form_settaggi select[name="intestazione_riga3_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spaziointestazione3 = $('#form_settaggi input[name="spaziointestazione3"]:visible').is(':checked');
                var intestazione_riga4 = $('#form_settaggi input[name="intestazione_riga4"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga4_grassetto = $('#form_settaggi select[name="intestazione_riga4_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var report = $('#form_settaggi input[name="report"]:visible').is(':checked');
                var report_grassetto = $('#form_settaggi input[name="report_grassetto"]:visible').is(':checked');
                var creatoil = $('#form_settaggi input[name="creatoil"]:visible').is(':checked');
                var creatoil_grassetto = $('#form_settaggi input[name="creatoil_grassetto"]:visible').is(':checked');
                var ordinamento_articoli = $('#form_settaggi select[name="ordinamento_articoli"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var ordinamento_crescente_decrescente = $('#form_settaggi select[name="ordinamento_crescente_decrescente"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var totaliservizio = $('#form_settaggi input[name="totaliservizio"]:visible').is(':checked');
                var totalioperatore = $('#form_settaggi input[name="totalioperatore"]:visible').is(':checked');
                var totalegenerale = $('#form_settaggi input[name="totalegenerale"]:visible').is(':checked');
                var divisionecategorie = $('#form_settaggi input[name="divisionecategorie"]:visible').is(':checked');
                var divisionegruppi = $('#form_settaggi input[name="divisionegruppi"]:visible').is(':checked');
                var divisionedestinazione = $('#form_settaggi input[name="divisionedestinazione"]:visible').is(':checked');
                var percentuali = $('#form_settaggi input[name="percentuali"]:visible').is(':checked');
                var sconti = $('#form_settaggi input[name="sconti"]:visible').is(':checked');
                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field81='" + intestazione_riga1 + "',\n\
                                        Field82='" + intestazione_riga1_grassetto + "',\n\
                                        Field83='" + spaziointestazione1 + "',\n\
                                        Field84='" + intestazione_riga2 + "',\n\
                                        Field85='" + intestazione_riga2_grassetto + "',\n\
                                        Field86='" + spaziointestazione2 + "',\n\
                                        Field87='" + intestazione_riga3 + "',\n\
                                        Field88='" + intestazione_riga3_grassetto + "',\n\
                                        Field89='" + spaziointestazione3 + "',\n\
                                        Field90='" + intestazione_riga4 + "',\n\
                                        Field91='" + intestazione_riga4_grassetto + "',\n\
                                        Field92='" + report + "',\n\
                                        Field104='" + report_grassetto + "',\n\
                                        Field93='" + creatoil + "',\n\
                                        Field94='" + creatoil_grassetto + "',\n\
                                        Field95='" + ordinamento_articoli + "',\n\
                                        Field96='" + ordinamento_crescente_decrescente + "',\n\
                                        Field97='" + totaliservizio + "',\n\
                                        Field98='" + totalioperatore + "',\n\
                                        Field99='" + totalegenerale + "',\n\
                                        Field412='" + divisionegruppi + "',\n\
                                        Field100='" + divisionecategorie + "',\n\
                                        Field101='" + divisionedestinazione + "',\n\
                                        Field102='" + percentuali + "',\n\
                                        Field103='" + sconti + "'\n\
                                         where id=" + comanda.folder_number + " ;";
                break;
            case "ANAGRAFICA PRODOTTI":

                var descrizione = $('#form_settaggi input[name="descrizione"]:visible').is(':checked');
                var spazio_forno = $('#form_settaggi input[name="spazio_forno"]:visible').is(':checked');
                var prezzo_normale = $('#form_settaggi input[name="prezzo_normale"]:visible').is(':checked');
                var prezzo_maxi = $('#form_settaggi input[name="prezzo_maxi"]:visible').is(':checked');
                var prezzo_bar = $('#form_settaggi input[name="prezzo_bar"]:visible').is(':checked');
                var prezzo_asporto = $('#form_settaggi input[name="prezzo_asporto"]:visible').is(':checked');
                var prezzo_continuo = $('#form_settaggi input[name="prezzo_continuo"]:visible').is(':checked');
                var prezzo_metro = $('#form_settaggi input[name="prezzo_metro"]:visible').is(':checked');
                var unita_misura = $('#form_settaggi input[name="unita_misura"]:visible').is(':checked');
                var prezzo_unita = $('#form_settaggi input[name="prezzo_unita"]:visible').is(':checked');
                var destinazione_stampa = $('#form_settaggi input[name="destinazione_stampa"]:visible').is(':checked');
                var destinazione_stampa_2 = $('#form_settaggi input[name="destinazione_stampa_2"]:visible').is(':checked');
                var portata = $('#form_settaggi input[name="portata"]:visible').is(':checked');
                var categoria_varianti = $('#form_settaggi input[name="categoria_varianti"]:visible').is(':checked');
                var varianti_automatiche = $('#form_settaggi input[name="varianti_automatiche"]:visible').is(':checked');
                var iva_base = $('#form_settaggi input[name="iva_base"]:visible').is(':checked');
                var iva_takeaway = $('#form_settaggi input[name="iva_takeaway"]:visible').is(':checked');
                var riepilogo = $('#form_settaggi input[name="riepilogo"]:visible').is(':checked');
                var codice_promozionale = $('#form_settaggi input[name="codice_promozionale"]:visible').is(':checked');
                var prezzo_poco = $('#form_settaggi input[name="prezzo_poco"]:visible').is(':checked');
                var prezzo_variante_maxi = $('#form_settaggi input[name="prezzo_variante_maxi"]:visible').is(':checked');
                var prezzo_varianti_aggiuntive = $('#form_settaggi input[name="prezzo_varianti_aggiuntive"]:visible').is(':checked');
                var prezzo_maxi_aggiuntive = $('#form_settaggi input[name="prezzo_maxi_aggiuntive"]:visible').is(':checked');
                var testo_query = "UPDATE settaggi_profili SET \n\
                                        Field330='" + descrizione + "',\n\
                                        Field331='" + spazio_forno + "',\n\
                                        Field332='" + prezzo_normale + "',\n\
                                        Field333='" + prezzo_maxi + "',\n\
                                        Field334='" + prezzo_bar + "',\n\
                                        Field335='" + prezzo_asporto + "',\n\
                                        Field336='" + prezzo_continuo + "',\n\
                                        Field337='" + prezzo_metro + "',\n\
                                        Field338='" + unita_misura + "',\n\
                                        Field339='" + prezzo_unita + "',\n\
                                        Field340='" + destinazione_stampa + "',\n\
                                        Field341='" + destinazione_stampa_2 + "',\n\
                                        Field342='" + portata + "',\n\
                                        Field343='" + categoria_varianti + "',\n\
                                        Field344='" + varianti_automatiche + "',\n\
                                        Field345='" + iva_base + "',\n\
                                        Field346='" + iva_takeaway + "',\n\
                                        Field347='" + riepilogo + "',\n\
                                        Field348='" + codice_promozionale + "',\n\
                                        Field349='" + prezzo_poco + "',\n\
                                        Field350='" + prezzo_variante_maxi + "',\n\
                                        Field351='" + prezzo_varianti_aggiuntive + "',\n\
                                        Field352='" + prezzo_maxi_aggiuntive + "'\n\
                                        WHERE id=" + comanda.folder_number + " ;";
                break;
            case "STAMPE_COMANDA":

                var comandapiuconto = $('#form_settaggi input[name="comandapiuconto"]:visible').is(':checked');
                var comandadoppia = $('#form_settaggi input[name="comandadoppia"]:visible').is(':checked');
                var ristampa_intera_asporto = $('#form_settaggi input[name="ristampa_intera_asporto"]:visible').is(':checked');
                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field425='" + ristampa_intera_asporto + "',\n\
                                        Field106='" + comandapiuconto + "',\n\
                                        Field23 ='" + comandadoppia + "' \n\
                                         where id=" + comanda.folder_number + " ;";
                break;
            case "STAMPE_COMANDA_ASPORTO":

                var comandapiuconto = $('#form_settaggi input[name="comandapiuconto"]:visible').is(':checked');
                var comandadoppia = $('#form_settaggi input[name="comandadoppia"]:visible').is(':checked');
                var memo_dopo_comanda = $('#form_settaggi input[name="memo_dopo_comanda"]:visible').is(':checked');
                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field108='" + comandapiuconto + "',\n\
                                        Field109 ='" + comandadoppia + "',\n\
                                        Field187 ='" + memo_dopo_comanda + "' \n\
                                         where id=" + comanda.folder_number + " ;";
                break;
            case "STAMPE_CONTO":

                var contodoppio = $('#form_settaggi input[name="contodoppio"]:visible').is(':checked');
                var associa_tavoli_stampante = $('#form_settaggi input[name="associa_tavoli_stampante"]:visible').is(':checked');
                var scelta_diretta_stampante = $('#form_settaggi input[name="scelta_diretta_stampante"]:visible').is(':checked');
                var conto_comanda_asporto = $('#form_settaggi input[name="conto_comanda_asporto"]:visible').is(':checked');
                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field413='" + associa_tavoli_stampante + "',\n\
                                        Field414='" + scelta_diretta_stampante + "',\n\
                                        Field107='" + contodoppio + "',\n\
                                        Field420='" + conto_comanda_asporto + "'\n\
                                        where id=" + comanda.folder_number + " ;";
                break;
            case "STAMPE_INCASSI":

                var dettagliovendutidoppiacopia = $('#form_settaggi input[name="dettagliovendutidoppiacopia"]:visible').is(':checked');
                var incassipiudettagliovenduti = $('#form_settaggi input[name="incassipiudettagliovenduti"]:visible').is(':checked');
                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field426='" + dettagliovendutidoppiacopia + "',\n\
                                        Field428='" + incassipiudettagliovenduti + "'\n\
                                         where id=" + comanda.folder_number + " ;";
                break;
            case "STAMPE_BUONI_PASTO":

                var buoniacquistosunonfiscale = $('#form_settaggi input[name="buoniacquistosunonfiscale"]:visible').is(':checked');
                var abilita_buono_acquisto = $('#form_settaggi input[name="abilita_buono_acquisto"]:visible').is(':checked');
                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field361='" + buoniacquistosunonfiscale + "',\n\
                                        Field194='" + abilita_buono_acquisto + "'\n\
                                         where id=" + comanda.folder_number + " ;";
                comanda.abilita_buono_acquisto = abilita_buono_acquisto;
                break;
            case "VARIE":

                /*
                 * 
                 * $('#form_settaggi select[name="stampante_cancellazione_articolo"] option[value="' + result[0].Field396 + '"]').attr("selected", true);
                 $('#form_settaggi select[name="stampante_sposta_articolo"] option[value="' + result[0].Field397 + '"]').attr("selected", true);
                 $('#form_settaggi select[name="stampante_sposta_tavolo"] option[value="' + result[0].Field398 + '"]').attr("selected", true);
                 $('#form_settaggi select[name="stampante_unisci_tavolo"] option[value="' + result[0].Field399 + '"]').attr("selected", true);
                 */
                var cancellazione_articolo_lanciato_comanda = $('#form_settaggi input[name="cancellazione_articolo_lanciato_comanda"]:visible').is(':checked');
                var cancellazione_conto_lanciato_comanda = $('#form_settaggi input[name="cancellazione_conto_lanciato_comanda"]:visible').is(':checked');
                var sposta_articolo = $('#form_settaggi input[name="sposta_articolo"]:visible').is(':checked');
                var sposta_tavolo = $('#form_settaggi input[name="sposta_tavolo"]:visible').is(':checked');
                var unisci_tavoli = $('#form_settaggi input[name="unisci_tavoli"]:visible').is(':checked');
                var cancellazione_tavolo = $('#form_settaggi input[name="cancellazione_tavolo"]:visible').is(':checked');
                var stampante_cancellazione_articolo = $('#form_settaggi select[name="stampante_cancellazione_articolo"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var stampante_sposta_articolo = $('#form_settaggi select[name="stampante_sposta_articolo"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var stampante_sposta_tavolo = $('#form_settaggi select[name="stampante_sposta_tavolo"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var stampante_unisci_tavolo = $('#form_settaggi select[name="stampante_unisci_tavolo"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field387='" + cancellazione_articolo_lanciato_comanda + "',\n\
                                        /*Field388='" + cancellazione_conto_lanciato_comanda + "',*/\n\
                                        Field389='" + sposta_articolo + "',\n\
                                        Field390='" + sposta_tavolo + "',\n\
                                        Field391='" + unisci_tavoli + "'/*,\n\
                                        Field392='" + cancellazione_tavolo + "'*/,\n\
                                        \n\
                                        Field396='" + stampante_cancellazione_articolo + "',\n\
                                        Field397='" + stampante_sposta_articolo + "',\n\
                                        Field398='" + stampante_sposta_tavolo + "',\n\
                                        Field399='" + stampante_unisci_tavolo + "'\n\
                                        where id=" + comanda.folder_number + " ;";
                break;
            case "LAYOUT_VIDEO_TASTI":
                var conto = $('#form_settaggi input[name="conto"]:visible').is(':checked');
                var comandapiuconto = $('#form_settaggi input[name="comandapiuconto"]:visible').is(':checked');
                var scontrino = $('#form_settaggi input[name="scontrino"]:visible').is(':checked');
                var comandapiuscontrino = $('#form_settaggi input[name="comandapiuscontrino"]:visible').is(':checked');
                var fattura = $('#form_settaggi input[name="fattura"]:visible').is(':checked');
                var comandapiufattura = $('#form_settaggi input[name="comandapiufattura"]:visible').is(':checked');
                var sconto = $('#form_settaggi input[name="sconto"]:visible').is(':checked');
                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field429='" + conto + "',\n\
                                        Field436='" + comandapiuconto + "',\n\
                                        Field430='" + scontrino + "',\n\
                                        Field431='" + comandapiuscontrino + "',\n\
                                        Field432='" + fattura + "',\n\
                                        Field433='" + comandapiufattura + "',\n\
                                        Field434='" + sconto + "'\n\
                                         where id=" + comanda.folder_number + " ;";
                break;
            case "LAYOUT_VIDEO_VIDEO":
                var colori_come_pc = $('#form_settaggi input[name="colori_come_pc"]:visible').is(':checked');
                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field435='" + colori_come_pc + "' \n\
                                         where id=" + comanda.folder_number + " ;";
                break;
            case "LAYOUT_PC_TASTI":
                var tasto_comanda = $('#form_settaggi input[name="comanda"]:visible').is(':checked');
                var servito = $('#form_settaggi input[name="servito"]:visible').is(':checked');
                var operazioni = $('#form_settaggi input[name="operazioni"]:visible').is(':checked');
                var buoni = $('#form_settaggi input[name="buoni"]:visible').is(':checked');
                var conto = $('#form_settaggi input[name="conto"]:visible').is(':checked');
                var comandapiuconto = $('#form_settaggi input[name="comandapiuconto"]:visible').is(':checked');
                var scontrino = $('#form_settaggi input[name="scontrino"]:visible').is(':checked');
                var comandapiuscontrino = $('#form_settaggi input[name="comandapiuscontrino"]:visible').is(':checked');
                var scontrinononpagato = $('#form_settaggi input[name="scontrinononpagato"]:visible').is(':checked');
                var scontrinopiupagamento = $('#form_settaggi input[name="scontrinopiupagamento"]:visible').is(':checked');
                var scontrinopiuriepilogo = $('#form_settaggi input[name="scontrinopiuriepilogo"]:visible').is(':checked');
                var fattura = $('#form_settaggi input[name="fattura"]:visible').is(':checked');
                var comandapiufattura = $('#form_settaggi input[name="comandapiufattura"]:visible').is(':checked');
                var sconto = $('#form_settaggi input[name="sconto"]:visible').is(':checked');
                var storicizza = $('#form_settaggi input[name="storicizza"]:visible').is(':checked');
                var assegno = $('#form_settaggi input[name="assegno"]:visible').is(':checked');
                var bonifico = $('#form_settaggi input[name="bonifico"]:visible').is(':checked');
                var ragione_sociale = $('#form_settaggi input[name="ragione_sociale"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var iban = $('#form_settaggi input[name="iban"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var bic_swift = $('#form_settaggi input[name="bic_swift"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var note = $('#form_settaggi input[name="note"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var droppay = $('#form_settaggi input[name="droppay"]:visible').is(':checked');

                var sagra_comanda_conto_parcheggia = $('#form_settaggi input[name="sagra_comanda_conto_parcheggia"]:visible').is(':checked');
                var sagra_comanda_conto_scontrino = $('#form_settaggi input[name="sagra_comanda_conto_scontrino"]:visible').is(':checked');
                var sagra_comanda_conto_pagamento = $('#form_settaggi input[name="sagra_comanda_conto_pagamento"]:visible').is(':checked');

                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field195='" + conto + "',\n\
                                        Field207='" + buoni + "',\n\
                                        Field196='" + comandapiuconto + "',\n\
                                        Field197='" + scontrino + "',\n\
                                        Field198='" + comandapiuscontrino + "',\n\
                                        Field199='" + scontrinopiupagamento + "',\n\
                                        Field200='" + scontrinopiuriepilogo + "',\n\
                                        Field329='" + scontrinononpagato + "',\n\
                                        Field201='" + fattura + "',\n\
                                        Field202='" + comandapiufattura + "',\n\
                                        Field203='" + sconto + "', \n\
                                        Field204='" + storicizza + "',\n\
                                        Field208='" + tasto_comanda + "', \n\
                                        Field363='" + servito + "', \n\
                                        Field364='" + operazioni + "', \n\
                                        Field365='" + assegno + "', \n\
                                        Field366='" + bonifico + "', \n\
                                        Field367='" + droppay + "', \n\
                                        Field368='" + ragione_sociale + "', \n\
                                        Field369='" + iban + "', \n\
                                        Field370='" + bic_swift + "', \n\
                                        Field371='" + note + "', \n\
                                        Field503='" + sagra_comanda_conto_parcheggia + "',\n\
                                        Field504='" + sagra_comanda_conto_scontrino + "',\n\
                                        Field505='" + sagra_comanda_conto_pagamento + "'\n\
                                        where id=" + comanda.folder_number + " ;";
                comanda.assegno_abilitato = assegno;
                comanda.bonifico_abilitato = bonifico;
                comanda.droppay_abilitato = droppay;
                break;
            case "LAYOUT_PC_VIDEO":

                var indietroamenu = $('#form_settaggi input[name="indietroamenu"]:visible').is(':checked');
                var colore_sfondo_PC = $('#form_settaggi select[name="colore_sfondo_PC"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var abilita_prodotto_libero_PC = $('#form_settaggi input[name="abilita_prodotto_libero_PC"]:visible').is(':checked');
                var abilita_lente_piu_PC = $('#form_settaggi input[name="abilita_lente_piu_PC"]:visible').is(':checked');
                var frecce_scorrimento_categorie = $('#form_settaggi input[name="frecce_scorrimento_categorie"]:visible').is(':checked');
                var abilita_conto_separato_PC = $('#form_settaggi input[name="abilita_conto_separato_PC"]:visible').is(':checked');
                var autotavolocontinuo = $('#form_settaggi input[name="autotavolocontinuo"]:visible').is(':checked');
                var calendario_ordinazioni = $('#form_settaggi input[name="calendario_ordinazioni"]:visible').is(':checked');
                var scelta_categoria = $('#form_settaggi select[name="scelta_categoria"]:visible').val();
                var tasto_segue_PC = $('#form_settaggi input[name="tasto_segue_PC"]:visible').is(':checked');
                var cancella_conto_abilitato = $('#form_settaggi input[name="cancella_conto_abilitato"]:visible').is(':checked');
                var cancella_conto_gia_lanciati = $('#form_settaggi input[name="cancella_conto_gia_lanciati"]:visible').is(':checked');

                var articoli_ordinati_visibili = $('#form_settaggi input[name="articoli_ordinati_visibili"]:visible').is(':checked');
                var iva_esposta = $('#form_settaggi input[name="iva_esposta"]:visible').is(':checked');
                var apertura_piantina = $('#form_settaggi input[name="apertura_piantina"]:visible').is(':checked');
                var apertura_ordinazione = $('#form_settaggi input[name="apertura_ordinazione"]:visible').is(':checked');
                var apertura_conto = $('#form_settaggi input[name="apertura_conto"]:visible').is(':checked');
                var apertura_scontrino = $('#form_settaggi input[name="apertura_scontrino"]:visible').is(':checked');
                var tastiera_gigante = $('#form_settaggi input[name="tastiera_gigante"]:visible').is(':checked');
                var font_tastierino = $('#form_settaggi input[name="font_tastierino"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var left_tastierino = $('#form_settaggi input[name="left_tastierino"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var left_tastiera = $('#form_settaggi input[name="left_tastiera"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');


                var testo_query = "UPDATE settaggi_profili SET \n\
                                        Field385='" + cancella_conto_abilitato + "', \n\
                                        Field386='" + cancella_conto_gia_lanciati + "', \n\
                                        Field190='" + colore_sfondo_PC + "', \n\
                                        Field191='" + abilita_prodotto_libero_PC + "', \n\
                                        Field205='" + indietroamenu + "', \n\
                                        Field229='" + tasto_segue_PC + "', \n\
                                        Field308='" + articoli_ordinati_visibili + "', \n\
                                        Field309='" + iva_esposta + "', \n\
                                        Field192='" + abilita_lente_piu_PC + "', \n\
                                        Field357='" + frecce_scorrimento_categorie + "', \n\
                                        Field193='" + abilita_conto_separato_PC + "', \n\
                                        Field206='" + autotavolocontinuo + "', \n\
                                        Field227='" + calendario_ordinazioni + "',\n\
                                        Field353='" + apertura_piantina + "',\n\
                                        Field354='" + apertura_ordinazione + "',\n\
                                        Field355='" + apertura_conto + "',\n\
                                        Field356='" + apertura_scontrino + "',\n\
                                        Field228='" + scelta_categoria + "'\n\
                                         where id=" + comanda.folder_number + " ;";
                var sql_locale = "UPDATE settaggi SET valore='" + tastiera_gigante + "' WHERE nome='tastiera_gigante';";
                comanda.sincro.query_locale(sql_locale, function () { });
                var sql_locale = "UPDATE settaggi SET valore='" + font_tastierino + "' WHERE nome='font_tastierino';";
                comanda.sincro.query_locale(sql_locale, function () { });
                var sql_locale = "UPDATE settaggi SET valore='" + left_tastierino + "' WHERE nome='left_tastierino';";
                comanda.sincro.query_locale(sql_locale, function () { });
                var sql_locale = "UPDATE settaggi SET valore='" + left_tastiera + "' WHERE nome='left_tastiera';";
                comanda.sincro.query_locale(sql_locale, function () { });
                comanda.categoria_calendario = scelta_categoria;
                comanda.calendario_ordinazioni = calendario_ordinazioni;



                break;
            case "LAYOUT_PC_VARIANTI":
                var piu = $('#form_settaggi input[name="piu"]:visible').is(':checked');
                var meno = $('#form_settaggi input[name="meno"]:visible').is(':checked');
                var specifiche = $('#form_settaggi input[name="specifiche"]:visible').is(':checked');
                var pococotto = $('#form_settaggi input[name="pococotto"]:visible').is(':checked');
                var finecottura = $('#form_settaggi input[name="finecottura"]:visible').is(':checked');
                var tasto_gusti_gelato = $('#form_settaggi input[name="tasto_gusti_gelato"]:visible').is(':checked');
                var scelta_categoria = $('#form_settaggi select[name="scelta_categoria"]:visible').val();
                var menu_completo = $('#form_settaggi input[name="menu_completo"]:visible').is(':checked');
                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field297='" + piu + "', \n\
                                        Field298='" + meno + "', \n\
                                        Field299='" + specifiche + "', \n\
                                        Field300='" + pococotto + "', \n\
                                        Field301='" + finecottura + "', \n\
                                        Field302='" + tasto_gusti_gelato + "', \n\
                                        Field303='" + scelta_categoria + "', \n\
                                        Field304='" + menu_completo + "' \n\
                                         where id=" + comanda.folder_number + " ;";
                comanda.tasto_menu_completo = menu_completo;
                comanda.tasto_piu = piu;
                comanda.tasto_meno = meno;
                comanda.tasto_specifiche = specifiche;
                comanda.tasto_pococotto = pococotto;
                comanda.tasto_finecottura = finecottura;
                comanda.tasto_gusti_gelato = tasto_gusti_gelato;
                comanda.categoria_gusti = scelta_categoria;
                break;
            case "POS_1":
                var pos_abilitato = $('#form_settaggi input[name="pos_abilitato"]:visible').is(':checked');
                var ip_pos = $('#form_settaggi input[name="ip_pos"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var porta_pos = $('#form_settaggi input[name="porta_pos"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var id_term = $('#form_settaggi input[name="id_term"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var ricevuta_pos_scontrino = $('#form_settaggi input[name="ricevuta_pos_scontrino"]:visible').is(':checked');
                comanda.pos_abilitato = pos_abilitato;
                comanda.ip_pos = ip_pos;
                comanda.porta_pos = porta_pos;
                comanda.id_term = id_term;
                comanda.ricevuta_pos_scontrino = ricevuta_pos_scontrino;
                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field231='" + ip_pos + "',\n\
                                        Field232='" + porta_pos + "',\n\
                                        Field233='" + id_term + "',\n\
                                        Field234='" + pos_abilitato + "',\n\
                                        Field235='" + ricevuta_pos_scontrino + "'\n\
                                         where id=" + comanda.folder_number + " ;";
                break;
            case "POS_2":
                var pos_abilitato = $('#form_settaggi input[name="pos_abilitato"]:visible').is(':checked');
                var ip_pos = $('#form_settaggi input[name="ip_pos"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var porta_pos = $('#form_settaggi input[name="porta_pos"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var id_term = $('#form_settaggi input[name="id_term"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var ricevuta_pos_scontrino = $('#form_settaggi input[name="ricevuta_pos_scontrino"]:visible').is(':checked');
                comanda.pos_abilitato = pos_abilitato;
                /*comanda.ip_pos = ip_pos;
                 comanda.porta_pos = porta_pos;
                 comanda.id_term = id_term;
                 comanda.ricevuta_pos_scontrino = ricevuta_pos_scontrino;*/

                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field401='" + ip_pos + "',\n\
                                        Field402='" + porta_pos + "',\n\
                                        Field403='" + id_term + "',\n\
                                        Field400='" + pos_abilitato + "',\n\
                                        Field404='" + ricevuta_pos_scontrino + "'\n\
                                         where id=" + comanda.folder_number + " ;";
                break;
            case "POS_3":
                var pos_abilitato = $('#form_settaggi input[name="pos_abilitato"]:visible').is(':checked');
                var ip_pos = $('#form_settaggi input[name="ip_pos"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var porta_pos = $('#form_settaggi input[name="porta_pos"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var id_term = $('#form_settaggi input[name="id_term"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var ricevuta_pos_scontrino = $('#form_settaggi input[name="ricevuta_pos_scontrino"]:visible').is(':checked');
                comanda.pos_abilitato = pos_abilitato;
                /*comanda.ip_pos = ip_pos;
                 comanda.porta_pos = porta_pos;
                 comanda.id_term = id_term;
                 comanda.ricevuta_pos_scontrino = ricevuta_pos_scontrino;*/

                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field406='" + ip_pos + "',\n\
                                        Field407='" + porta_pos + "',\n\
                                        Field408='" + id_term + "',\n\
                                        Field405='" + pos_abilitato + "',\n\
                                        Field409='" + ricevuta_pos_scontrino + "'\n\
                                         where id=" + comanda.folder_number + " ;";
                break;
            case "SCONTISTICHE_SCONTO_TOTALE":
                var intestazione_riga1 = $('#form_settaggi input[name="intestazione_riga1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga1_grassetto = $('#form_settaggi select[name="intestazione_riga1_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spaziointestazione1 = $('#form_settaggi input[name="spaziointestazione1"]:visible').is(':checked');
                var intestazione_riga2 = $('#form_settaggi input[name="intestazione_riga2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga2_grassetto = $('#form_settaggi select[name="intestazione_riga2_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spaziointestazione2 = $('#form_settaggi input[name="spaziointestazione2"]:visible').is(':checked');
                var intestazione_riga3 = $('#form_settaggi input[name="intestazione_riga3"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga3_grassetto = $('#form_settaggi select[name="intestazione_riga3_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spaziointestazione3 = $('#form_settaggi input[name="spaziointestazione3"]:visible').is(':checked');
                var intestazione_riga4 = $('#form_settaggi input[name="intestazione_riga4"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intestazione_riga4_grassetto = $('#form_settaggi select[name="intestazione_riga4_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var data = $('#form_settaggi input[name="data"]:visible').is(':checked');
                var font_data = $('#form_settaggi select[name="font_data"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var valore_scadenza = $('#form_settaggi input[name="valore_scadenza"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var intervallo_scadenza = $('#form_settaggi select[name="intervallo_scadenza"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var valore_euro_da_1 = $('#form_settaggi input[name="valore_euro_da_1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var valore_euro_a_1 = $('#form_settaggi input[name="valore_euro_a_1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var valore_sconto_1 = $('#form_settaggi input[name="valore_sconto_1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                //var omaggio_1 = $('#form_settaggi input[name="omaggio_1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var valore_euro_da_2 = $('#form_settaggi input[name="valore_euro_da_2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var valore_euro_a_2 = $('#form_settaggi input[name="valore_euro_a_2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var valore_sconto_2 = $('#form_settaggi input[name="valore_sconto_2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                //var omaggio_2 = $('#form_settaggi input[name="omaggio_2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var valore_euro_da_3 = $('#form_settaggi input[name="valore_euro_da_3"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var valore_euro_a_3 = $('#form_settaggi input[name="valore_euro_a_3"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var valore_sconto_3 = $('#form_settaggi input[name="valore_sconto_3"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                //var omaggio_3 = $('#form_settaggi input[name="omaggio_3"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var valore_euro_da_4 = $('#form_settaggi input[name="valore_euro_da_4"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var valore_euro_a_4 = $('#form_settaggi input[name="valore_euro_a_4"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var valore_sconto_4 = $('#form_settaggi input[name="valore_sconto_4"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                //var omaggio_4 = $('#form_settaggi input[name="omaggio_4"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');

                var array_giorni = new Array();
                var dom = $('#form_settaggi input[name="dom"]:visible').is(':checked');
                if (dom === true) {
                    array_giorni.push("0");
                }

                var lun = $('#form_settaggi input[name="lun"]:visible').is(':checked');
                if (lun === true) {
                    array_giorni.push("1");
                }

                var mar = $('#form_settaggi input[name="mar"]:visible').is(':checked');
                if (mar === true) {
                    array_giorni.push("2");
                }

                var mer = $('#form_settaggi input[name="mer"]:visible').is(':checked');
                if (mer === true) {
                    array_giorni.push("3");
                }

                var gio = $('#form_settaggi input[name="gio"]:visible').is(':checked');
                if (gio === true) {
                    array_giorni.push("4");
                }

                var ven = $('#form_settaggi input[name="ven"]:visible').is(':checked');
                if (ven === true) {
                    array_giorni.push("5");
                }

                var sab = $('#form_settaggi input[name="sab"]:visible').is(':checked');
                if (sab === true) {
                    array_giorni.push("6");
                }

                var stringa_giorni = array_giorni.join();
                var qrcode_sconto = $('#form_settaggi input[name="qrcode_sconto"]:visible').is(':checked');
                var msg_riga1 = $('#form_settaggi input[name="msg_riga1"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga1_grassetto = $('#form_settaggi select[name="msg_riga1_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga1_allineamento = $('#form_settaggi select[name="msg_riga1_allineamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spazio1 = $('#form_settaggi input[name="spazio1"]:visible').is(':checked');
                var msg_riga2 = $('#form_settaggi input[name="msg_riga2"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga2_grassetto = $('#form_settaggi select[name="msg_riga2_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga2_allineamento = $('#form_settaggi select[name="msg_riga2_allineamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var spazio2 = $('#form_settaggi input[name="spazio2"]:visible').is(':checked');
                var msg_riga3 = $('#form_settaggi input[name="msg_riga3"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga3_grassetto = $('#form_settaggi select[name="msg_riga3_grassetto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var msg_riga3_allineamento = $('#form_settaggi select[name="msg_riga3_allineamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field256='" + intestazione_riga1 + "',\n\
                                        Field269 ='" + intestazione_riga1_grassetto + "', \n\
                                        Field257 ='" + spaziointestazione1 + "', \n\
                                        Field258 ='" + intestazione_riga2 + "',\n\
                                        Field270 ='" + intestazione_riga2_grassetto + "', \n\
                                        Field259 ='" + spaziointestazione2 + "', \n\
                                        Field260 ='" + intestazione_riga3 + "',\n\
                                        Field271 ='" + intestazione_riga3_grassetto + "', \n\
                                        Field261 ='" + spaziointestazione3 + "', \n\
                                        Field262 ='" + intestazione_riga4 + "',\n\
                                        Field272 ='" + intestazione_riga4_grassetto + "', \n\
                                        \n\
                                        Field263 ='" + data + "', \n\
                                        Field273  ='" + font_data + "',\n\
                                        \n\
                                        Field264 ='" + msg_riga1 + "', \n\
                                        Field274 ='" + msg_riga1_grassetto + "', \n\
                                        Field275 ='" + msg_riga1_allineamento + "', \n\
                                        Field265 ='" + spazio1 + "', \n\
                                        Field266 ='" + msg_riga2 + "', \n\
                                        Field276 ='" + msg_riga2_grassetto + "', \n\
                                        Field277 ='" + msg_riga2_allineamento + "', \n\
                                        Field267 ='" + spazio2 + "', \n\
                                        Field268 ='" + msg_riga3 + "', \n\
                                        Field278='" + msg_riga3_grassetto + "', \n\
                                        Field279 ='" + msg_riga3_allineamento + "', \n\
                                        Field280 ='" + valore_scadenza + "', \n\
                                        Field281 ='" + intervallo_scadenza + "', \n\
                                        Field282 ='" + valore_euro_da_1 + "', \n\
                                        Field283 ='" + valore_euro_a_1 + "', \n\
                                        Field284 ='" + valore_sconto_1 + "', \n\
                                        Field286 ='" + valore_euro_da_2 + "', \n\
                                        Field287 ='" + valore_euro_a_2 + "', \n\
                                        Field288 ='" + valore_sconto_2 + "', \n\
                                        Field290 ='" + valore_euro_da_3 + "', \n\
                                        Field291 ='" + valore_euro_a_3 + "', \n\
                                        Field292 ='" + valore_sconto_3 + "', \n\
                                        Field294 ='" + valore_euro_da_4 + "', \n\
                                        Field295 ='" + valore_euro_a_4 + "', \n\
                                        Field296 ='" + valore_sconto_4 + "', \n\
                                        Field305 ='" + stringa_giorni + "', \n\
                                        Field306 ='" + qrcode_sconto + "' \n\
                                        where id=" + comanda.folder_number + " ;";
                break;

            case "FASTORDER":
                var url_fastorder = $('#form_settaggi input[name="url_fastorder"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var categorie_come_intellinet = $('#form_settaggi input[name="categorie_come_intellinet"]:visible').is(':checked');
                var modifica_prezzi_fastorder = $('#form_settaggi input[name="modifica_prezzi_fastorder"]:visible').is(':checked');

                var testo_query = " UPDATE settaggi_profili SET \n\
                                        Field502='" + url_fastorder + "',\n\
                                        Field506='" + categorie_come_intellinet + "',\n\
                                        Field507='" + modifica_prezzi_fastorder + "' \n\
                                        where id=" + comanda.folder_number + " ;";

                comanda.url_fastorder = url_fastorder;
                break;
            case "PRINCIPALI":

                var multiprofilo = $('#form_settaggi input[name="multiprofilo"]:visible').is(':checked') ? '1' : '0';
                var lingua_visualizzazione = $('#form_settaggi select[name="lingua_visualizzazione"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var lingua_stampa = $('#form_settaggi select[name="lingua_stampa"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var servizio_fino_alle = $('#form_settaggi select[name="servizio_fino_alle"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var coperti_obbligatori = $('#form_settaggi input[name="coperti_obbligatori"]:visible').is(':checked') ? '1' : '0';
                var avviso_righe_prezzo_zero = $('#form_settaggi input[name="righe_prezzo_zero"]:visible').is(':checked') ? '1' : '0';
                var visualizzazione_coperti = $('#form_settaggi input[name="visualizzazione_coperti"]:visible').is(':checked') ? '1' : '0';
                var valore_coperti = $('#form_settaggi input[name="valore_coperto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var valore_servizio = $('#form_settaggi input[name="valore_servizio"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var iva_default = $('#form_settaggi input[name="iva_default"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var iva_takeaway = $('#form_settaggi input[name="iva_takeaway"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var scelta_categoria = $('#form_settaggi select[name="scelta_categoria"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var fastorder = $('#form_settaggi input[name="fastorder"]:visible').is(':checked') ? '1' : '0';
                var touch_articolo_conto = $('#form_settaggi select[name="touch_articolo_conto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var sconto_su_promo = $('#form_settaggi input[name="sconto_su_promo"]:visible').is(':checked') ? '1' : '0';
                var articolo_a_peso = $('#form_settaggi input[name="articolo_a_peso"]:visible').is(':checked') ? '1' : '0';
                var iva_estera_abilitata = $('#form_settaggi input[name="iva_estera_abilitata"]:visible').is(':checked') ? '1' : '0';
                var aliquota_consegna = $('#form_settaggi input[name="aliquota_consegna"]:visible').is(':checked') ? '1' : '0';
                var reparto_servizi_default = $('#form_settaggi select[name="reparto_servizi_default"]:visible').val();
                var reparto_beni_default = $('#form_settaggi select[name="reparto_beni_default"]:visible').val();
                var reparto_consegna = $('#form_settaggi select[name="reparto_consegna"]:visible').val();
                //DATI LOCALI
                var scelta_categoria_tavoli = $('#form_settaggi select[name="scelta_categoria_tavoli"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var sql_locale = "update settaggi set valore='" + scelta_categoria_tavoli + "' where nome='cp_tavoli';";
                comanda.sincro.query_locale(sql_locale, function () { });
                comanda.cp_tavoli = scelta_categoria_tavoli;
                var scelta_categoria_bar = $('#form_settaggi select[name="scelta_categoria_bar"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var sql_locale = "update settaggi set valore='" + scelta_categoria_bar + "' where nome='cp_bar';";
                comanda.sincro.query_locale(sql_locale, function () { });
                comanda.cp_bar = scelta_categoria_bar;
                var scelta_categoria_asporto = $('#form_settaggi select[name="scelta_categoria_asporto"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var sql_locale = "update settaggi set valore='" + scelta_categoria_asporto + "' where nome='cp_asporto';";
                comanda.sincro.query_locale(sql_locale, function () { });
                comanda.cp_asporto = scelta_categoria_asporto;
                var scelta_categoria_continuo = $('#form_settaggi select[name="scelta_categoria_continuo"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
                var sql_locale = "update settaggi set valore='" + scelta_categoria_continuo + "' where nome='cp_continuo';";
                comanda.sincro.query_locale(sql_locale, function () { });
                comanda.cp_continuo = scelta_categoria_continuo;
                var testo_query = "UPDATE impostazioni_fiscali SET \n\
                                        multiprofilo='" + multiprofilo + "',\n\
                                        sconto_su_promo='" + sconto_su_promo + "',\n\
                                        articolo_a_peso='" + articolo_a_peso + "',\n\
                                        lingua_stampa='" + lingua_stampa + "',\n\
                                        coperti_obbligatorio='" + coperti_obbligatori + "',\n\
                                        righe_zero='" + avviso_righe_prezzo_zero + "',\n\
                                        valore_coperto='" + valore_coperti + "',\n\
                                        valore_servizio='" + valore_servizio + "',\n\
                                        aliquota_default='" + iva_default + "',\n\
                                        aliquota_takeaway='" + iva_takeaway + "',\n\
                                        iva_estera_abilitata='" + iva_estera_abilitata + "',\n\
                                        categoria_partenza='" + scelta_categoria + "',\n\
                                        categoria_partenza_mobile='" + scelta_categoria + "',\n\
                                        visualizzazione_coperti='" + visualizzazione_coperti + "',\n\
                                        touch_articolo_conto='" + touch_articolo_conto + "',\n\
                                        reparto_servizi_default='" + reparto_servizi_default + "',\n\
                                        reparto_beni_default='" + reparto_beni_default + "',\n\
                                        aliquota_consegna='" + aliquota_consegna + "',\n\
                                        reparto_consegna='" + reparto_consegna + "',\n\
                                        fastorder = '" + fastorder + "'\n\
                                        where id=" + comanda.folder_number + " ;";
                var testo_query_servizio = "UPDATE dati_servizio SET ora_finale_servizio='" + servizio_fino_alle + "' where id='" + comanda.folder_number + "';";
                comanda.sock.send({
                    tipo: "aggiornamento_settaggi_principali",
                    operatore: comanda.operatore,
                    query: testo_query_servizio,
                    terminale: comanda.terminale,
                    ip: comanda.ip_address
                });
                comanda.sincro.query(testo_query_servizio, function () {

                    comanda.multiprofilo = multiprofilo;
                    comanda.visualizzazione_coperti = visualizzazione_coperti;
                    comanda.valore_coperto = valore_coperti;
                    comanda.coperti_obbligatorio = coperti_obbligatori;
                    comanda.percentuale_servizio = valore_servizio;
                    comanda.categoria_partenza = scelta_categoria;
                    comanda.categoria_predefinita = scelta_categoria;
                    comanda.touch_articolo_conto = touch_articolo_conto;
                    comanda.sconto_su_promo = sconto_su_promo;
                    comanda.articolo_a_peso = articolo_a_peso;
                    comanda.partita_iva_estera = false;
                    if (iva_estera_abilitata === "1") {
                        comanda.partita_iva_estera = true;
                    }

                    tendina_profili();
                    alert("SE VUOI VEDERE EVENTUALI MODIFICHE ALLA LINGUA DI STAMPA E/O VISUALIZZAZIONE, DEVI RIAVVIARE TUTTI I DISPOSITIVI.");

                });
                break;
        }
    }

    if (testo_query !== undefined) {

        comanda.sock.send({
            tipo: "aggiornamento_settaggi",
            operatore: comanda.operatore,
            query: testo_query,
            terminale: comanda.terminale,
            ip: comanda.ip_address
        });
        comanda.sincro.query(testo_query, function () {
            if (cancella_conto_abilitato == false && cancella_conto_gia_lanciati == true) {
                document.getElementsByName("cancella_conto_gia_lanciati")[0].checked = false;
                var test_query = "UPDATE settaggi_profili SET Field386 ='" + false + "' where id =" + comanda.folder_number + ";";
                alasql(test_query);
                bootbox.alert("ATTENZIONE:  per abilitare Articoli Lanciati in Comanda, abilitare anche Cancella Conto.");
                comanda.sock.send({
                    tipo: "aggiornamento_settaggi",
                    operatore: comanda.operatore,
                    query: test_query,
                    terminale: comanda.terminale,
                    ip: comanda.ip_address
                });

                comanda.sincro.query_locale(test_query, function () { });
            }
            if (comanda.pizzeria_asporto === true) {
                layout_tasti_pc_pizzeria();
            } else if (comanda.compatibile_xml !== true) {
                layout_tasti_pc();
            }
        });
    }
    comanda.sincro.query_cassa();
}
/*var testo_queryYYY = alasql("SELECT  Field385, Field386 FROM settaggi_profili    WHERE id = 1;");

if (testo_queryYYY[0].Field385 === "false" && testo_queryYYY[0].Field386 === "true") {
    document.getElementsByName("cancella_conto_gia_lanciati")[0].checked = false;
    var test_query = "UPDATE settaggi_profili SET field386 ='" + false + "' where id =" + comanda.folder_number + ";";


    comanda.sincro.query_locale(test_query, function() {});
}*/

function salva_operatore(id) {
    var nome = $('#form_gestione_operatori input[name="nome"]:visible').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var password = $('#form_gestione_operatori input[name="password"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var p_statistiche = $('#form_gestione_operatori input[name="p_statistiche"]:visible').is(':checked') ? 'true' : '';
    var p_verifiche = $('#form_gestione_operatori input[name="p_verifiche"]:visible').is(':checked') ? 'true' : '';
    var p_gestioni = $('#form_gestione_operatori input[name="p_gestioni"]:visible').is(':checked') ? 'true' : '';
    var p_layout = $('#form_gestione_operatori input[name="p_layout"]:visible').is(':checked') ? 'true' : '';
    var p_backoffice = $('#form_gestione_operatori input[name="p_backoffice"]:visible').is(':checked') ? 'true' : '';
    var p_chiusura = $('#form_gestione_operatori input[name="p_chiusura"]:visible').is(':checked') ? 'true' : '';
    var p_mappa_tavoli = $('#form_gestione_operatori input[name="p_mappa_tavoli"]:visible').is(':checked') ? 'true' : '';
    var p_mappa_tavoli_phone = $('#form_gestione_operatori input[name="p_mappa_tavoli_phone"]:visible').is(':checked') ? 'true' : '';
    var p_gestione_clienti = $('#form_gestione_operatori input[name="p_gestione_clienti"]:visible').is(':checked') ? 'true' : '';
    var testo_query;
    if (password.length > 0) {
        testo_query = "UPDATE operatori SET nome='" + nome + "',password='" + CryptoJS.MD5(password).toString() + "',p_statistiche='" + p_statistiche + "',p_verifiche='" + p_verifiche + "',p_gestioni='" + p_gestioni + "',p_backoffice='" + p_backoffice + "',p_chiusura='" + p_chiusura + "',p_layout='" + p_layout + "',p_mappa_tavoli='" + p_mappa_tavoli + "',p_mappa_tavoli_phone='" + p_mappa_tavoli_phone + "',p_gestione_clienti='" + p_gestione_clienti + "' WHERE id=" + id + ";";
    } else {
        testo_query = "UPDATE operatori SET nome='" + nome + "',p_statistiche='" + p_statistiche + "',p_verifiche='" + p_verifiche + "',p_gestioni='" + p_gestioni + "',p_backoffice='" + p_backoffice + "',p_chiusura='" + p_chiusura + "',p_layout='" + p_layout + "',p_mappa_tavoli='" + p_mappa_tavoli + "',p_gestione_clienti='" + p_gestione_clienti + "',p_mappa_tavoli_phone='" + p_mappa_tavoli_phone + "' WHERE id=" + id + ";";
    }

    console.log("SALVA OPERATORE", testo_query);
    //comanda.array_dbcentrale_mancanti.push(testo_query);
    
    comanda.sock.send({
        tipo: "aggiornamento_operatori",
        operatore: comanda.operatore,
        query: testo_query,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    comanda.sincro.query(testo_query, function () {

        selezione_operatore("GESTIONE OPERATORI");
    });

}

function crea_oggetto_pony() {

    if (comanda.mobile !== true) {
        var query_selezione_pony = 'SELECT * FROM pony_express ORDER BY nome ASC;';


        comanda.sincro.query(query_selezione_pony, function (array_risultato) {


            array_risultato.forEach(function (singolo_risultato) {

                oggetto_pony[singolo_risultato.id] = singolo_risultato.nome;

            });

        });
    }

}

function salva_pony(id) {
    var nome = $('#form_gestione_pony input[name="nome"]:visible').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var testo_query = "UPDATE pony_express SET nome='" + nome + "' WHERE id=" + id + ";";
    console.log("SALVA OPERATORE", testo_query);
    //comanda.array_dbcentrale_mancanti.push(testo_query);
    comanda.sock.send({
        tipo: "aggiornamento_pony",
        operatore: comanda.operatore,
        query: testo_query,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    comanda.sincro.query(testo_query, function () {

        crea_oggetto_pony();

        selezione_operatore("GESTIONE PONY");

    });

}

function salva_rtengine(id) {
    var ip = $('#form_gestione_rtengine input[name="ip"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var testo_query;
    testo_query = "UPDATE socket_listeners SET ip='" + ip + "' WHERE id='" + id + "';";
    console.log("SALVA RTENGINE", testo_query);
    //comanda.array_dbcentrale_mancanti.push(testo_query);
    comanda.sock.send({
        tipo: "aggiornamento_rtengine",
        operatore: comanda.operatore,
        query: testo_query,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    comanda.sincro.query(testo_query, function () {

        selezione_operatore("GESTIONE RTENGINE");
    });

}
function azienda_privato_campi()
{$('#partita_iva').hide();
$('#ragione_sociale').hide();
$('#label_ragione_sociale_gestione_clienti').hide();
$('.cls_fat_partita_iva').hide();
$('#codice_destinatario').hide();
$('partita_iva_estera').hide();
$('.cls_fat_codice_destinatario').hide();
$('.cls_fat_partita_iva_estera').hide();
$('.cls_fat_pec').hide();
$('#pec').hide();
$('#Regime_Fiscale_label').hide();
$('#regime_fiscale').hide();
$('#fat_fine_mese_label').hide();
$('#fatt_fine_mese_sn').hide();
$('#esente_fattura_ele_label').hide();
$('#esente_fattura_elettronica').hide();
$('#iva_agevolata_label').hide();
$('#iva_non_pagati').hide();

}
function azienda_campi_mostrare()
{$('#partita_iva').show();
$('#ragione_sociale').show();
$('#label_ragione_sociale_gestione_clienti').show();
$('.cls_fat_partita_iva').show();
$('#codice_destinatario').show();
$('partita_iva_estera').show();
$('.cls_fat_codice_destinatario').show();
$('.cls_fat_partita_iva_estera').show();
$('.cls_fat_pec').show();
$('#pec').show();
$('#Regime_Fiscale_label').show();
$('#regime_fiscale').show();
$('#fat_fine_mese_label').show();
$('#fatt_fine_mese_sn').show();
$('#esente_fattura_ele_label').show();
$('#esente_fattura_elettronica').show();
$('#iva_agevolata_label').show();
$('#iva_non_pagati').show();

}
function salva_cliente(id) {
    
    var azienda_privato = $('#tabella_clienti [name="azienda_privato"]:checked').attr("id");
    if (azienda_privato === "gest_cli_ap_privato") {
        azienda_privato = "ap_privato";
    } else {
        azienda_privato = "ap_azienda";
    }
    var ragione_sociale = $('#form_gestione_clienti input[name="ragione_sociale"]:visible').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var nome = $('#form_gestione_clienti input[name="nome"]:visible').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var cognome = $('#form_gestione_clienti input[name="cognome"]:visible').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var indirizzo = $('#form_gestione_clienti input[name="indirizzo"]:visible').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var numero = $('#form_gestione_clienti input[name="numero"]:visible').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var cap = $('#form_gestione_clienti input[name="cap"]:visible').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var provincia = $('#form_gestione_clienti input[name="provincia"]').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var settaggi_profili = alasql("SELECT Field509 FROM settaggi_profili  where id=" + comanda.folder_number + "  LIMIT 1;");

    if (settaggi_profili[0].Field509 === "true") {
        var codice_tessera = $('#form_gestione_clienti input[name="codice_tessera_clienti_gestione"]:visible').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase()
    }
    //    var paese = $('#form_gestione_clienti input[name="paese"]:visible').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var cellulare = $('#form_gestione_clienti input[name="cellulare"]:visible').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var telefono = $('#form_gestione_clienti input[name="telefono"]:visible').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    //    var fax = $('#form_gestione_clienti input[name="fax"]:visible').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var email = $('#form_gestione_clienti input[name="email"]:visible').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var percentuale_sconto_su_totale = $('#form_gestione_clienti input[name="percentuale_sconto_su_totale"]:visible').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var codice_fiscale = $('#form_gestione_clienti input[name="codice_fiscale"]').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var codice_lotteria = $('#form_gestione_clienti input[name="codice_lotteria"]').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var partita_iva = $('#form_gestione_clienti input[name="partita_iva"]').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var partita_iva_estera = $('#form_gestione_clienti input[name="partita_iva_estera"]').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var iva_non_pagati = $('#form_gestione_clienti select[name="iva_non_pagati"]').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var formato_trasmissione = $('#form_gestione_clienti input[name="formato_trasmissione"]').is(':checked') ? 'true' : '';
    var codice_destinatario = $('#form_gestione_clienti input[name="codice_destinatario"]').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var regime_fiscale = $('#form_gestione_clienti select[name="regime_fiscale"]').val();
    var pec = $('#form_gestione_clienti input[name="pec"]').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var comune = $('#form_gestione_clienti input[name="comune"]').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    //    var id_nazione = $('#form_gestione_clienti input[name="id_nazione"]').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var nazione = $('#form_gestione_clienti select[name="nazione"]').val();
    var fatt_fine_mese_sn = $('#form_gestione_clienti input[name="fatt_fine_mese_sn"]').is(':checked') ? 'true' : '';
    var esente_fattura_elettronica = $('#form_gestione_clienti input[name="esente_fattura_elettronica"]').is(':checked') ? 'true' : '';
    var settaggi_profili = alasql("SELECT Field509 FROM settaggi_profili  where id=" + comanda.folder_number + "  LIMIT 1;");
    
    

    if (settaggi_profili[0].Field509 === "true") {
        var testo_query = "UPDATE clienti SET codice_lotteria='" + codice_lotteria + "',percentuale_sconto_su_totale='" + percentuale_sconto_su_totale + "',nome='" + nome + "',cognome='" + cognome + "', tipo_cliente='" + azienda_privato + "',fatt_fine_mese_sn='" + fatt_fine_mese_sn + "',esente_fattura_elettronica='" + esente_fattura_elettronica + "',formato_trasmissione='" + formato_trasmissione + "',codice_destinatario='" + codice_destinatario + "',regime_fiscale='" + regime_fiscale + "',pec='" + pec + "',comune='" + comune + "',id_nazione='" + nazione + "',nazione='" + nazione + "',partita_iva_estera='" + partita_iva_estera + "',iva_non_pagati='" + iva_non_pagati + "',ragione_sociale='" + ragione_sociale + "',indirizzo='" + indirizzo + "',numero='" + numero + "',cap='" + cap + "',provincia='" + provincia + "',cellulare='" + cellulare + "',telefono='" + telefono + "',email='" + email + "',codice_fiscale='" + codice_fiscale + "',partita_iva='" + partita_iva + "',codice_tessera='" + codice_tessera + "' WHERE id=" + id + ";";
    }
    else {
        var testo_query = "UPDATE clienti SET codice_lotteria='" + codice_lotteria + "',percentuale_sconto_su_totale='" + percentuale_sconto_su_totale + "',nome='" + nome + "',cognome='" + cognome + "', tipo_cliente='" + azienda_privato + "',fatt_fine_mese_sn='" + fatt_fine_mese_sn + "',esente_fattura_elettronica='" + esente_fattura_elettronica + "',formato_trasmissione='" + formato_trasmissione + "',codice_destinatario='" + codice_destinatario + "',regime_fiscale='" + regime_fiscale + "',pec='" + pec + "',comune='" + comune + "',id_nazione='" + nazione + "',nazione='" + nazione + "',partita_iva_estera='" + partita_iva_estera + "',iva_non_pagati='" + iva_non_pagati + "',ragione_sociale='" + ragione_sociale + "',indirizzo='" + indirizzo + "',numero='" + numero + "',cap='" + cap + "',provincia='" + provincia + "',cellulare='" + cellulare + "',telefono='" + telefono + "',email='" + email + "',codice_fiscale='" + codice_fiscale + "',partita_iva='" + partita_iva + "' WHERE id=" + id + ";";

    }
    comanda.sock.send({
        tipo: "aggiornamento_clienti",
        operatore: comanda.operatore,
        query: testo_query,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    comanda.sincro.query(testo_query, function () {

        selezione_operatore("GESTIONE CLIENTI");
    });
    

}


function salva_circuito(id) {

    var ragione_sociale = $('#form_gestione_circuiti input[name="ragione_sociale"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var indirizzo = $('#form_gestione_circuiti input[name="indirizzo"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var numero = $('#form_gestione_circuiti input[name="numero"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var cap = $('#form_gestione_circuiti input[name="cap"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var citta = $('#form_gestione_circuiti input[name="citta"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var provincia = $('#form_gestione_circuiti input[name="provincia"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var paese = $('#form_gestione_circuiti input[name="paese"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var cellulare = $('#form_gestione_circuiti input[name="cellulare"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var telefono = $('#form_gestione_circuiti input[name="telefono"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var adeguamento_perc = $('#form_gestione_circuiti input[name="adeguamento_perc"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var iva_non_pagati = $('#form_gestione_circuiti select[name="iva_non_pagati"]').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var email = $('#form_gestione_circuiti input[name="email"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var codice_fiscale = $('#form_gestione_circuiti input[name="codice_fiscale"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var partita_iva = $('#form_gestione_circuiti input[name="partita_iva"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var ordinamento = $('#form_gestione_circuiti input[name="ordinamento"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    //var partita_iva_estera = $('#form_gestione_circuiti input[name="partita_iva_estera"]:visible').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    //var formato_trasmissione = $('#form_gestione_circuiti input[name="formato_trasmissione"]').is(':checked') ? 'true' : '';
    var codice_destinatario = $('#form_gestione_circuiti input[name="codice_destinatario"]').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var regime_fiscale = $('#form_gestione_circuiti input[name="regime_fiscale"]').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var pec = $('#form_gestione_circuiti input[name="pec"]').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var riepilogo_scontrini = $('#form_gestione_circuiti input[name="riepilogo_scontrini"]').is(':checked') ? 'true' : '';
    var testo_query = "UPDATE circuiti SET riepilogo_scontrini='" + riepilogo_scontrini + "',pec='" + pec + "',codice_destinatario='" + codice_destinatario + "',regime_fiscale='" + regime_fiscale + "',ordinamento='" + ordinamento + "',ragione_sociale='" + ragione_sociale + "',indirizzo='" + indirizzo + "',numero='" + numero + "',cap='" + cap + "',citta='" + citta + "',provincia='" + provincia + "',paese='" + paese + "',cellulare='" + cellulare + "',telefono='" + telefono + "',adeguamento_perc='" + adeguamento_perc + "',iva_non_pagati='" + iva_non_pagati + "',email='" + email + "',codice_fiscale='" + codice_fiscale.toUpperCase() + "',partita_iva='" + partita_iva + "' WHERE id=" + id + ";";
    //comanda.array_dbcentrale_mancanti.push(testo_query);
    comanda.sock.send({
        tipo: "aggiornamento_circuiti",
        operatore: comanda.operatore,
        query: testo_query,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    comanda.sincro.query(testo_query, function () {

        selezione_operatore("GESTIONE CIRCUITI");
    });

}

//FINIRE
function salva_prodotto(event, id) {

    var descrizione = $('#form_gestione_prodotti input[name="descrizione"]:visible').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var prezzo = $('#form_gestione_prodotti input[name="prezzo_1"]:visible').val();
    if (prezzo === undefined) {
        prezzo = "0.00";
    } else {
        prezzo = prezzo.replace(",", ".");
    }

    var costo = $('#form_gestione_prodotti input[name="costo"]:visible').val();
    if (costo === undefined) {
        costo = "0.00";
    } else {
        costo = costo.replace(",", ".");
    }

    var prezzo_varianti_aggiuntive = $('#form_gestione_prodotti input[name="prezzo_varianti_aggiuntive"]:visible').val();
    if (prezzo_varianti_aggiuntive === undefined) {
        prezzo_varianti_aggiuntive = "";
    } else {
        prezzo_varianti_aggiuntive = prezzo_varianti_aggiuntive.replace(",", ".");
    }

    var prezzo_2 = $('#form_gestione_prodotti input[name="prezzo_2"]:visible').val();
    if (prezzo_2 === undefined) {
        prezzo_2 = "0.00";
    } else {
        prezzo_2 = prezzo_2.replace(",", ".");
    }

    var prezzo_varianti_maxi = $('#form_gestione_prodotti input[name="prezzo_varianti_maxi"]:visible').val();
    if (prezzo_varianti_maxi === undefined) {
        prezzo_varianti_maxi = "";
    } else {
        prezzo_varianti_maxi = prezzo_varianti_maxi.replace(",", ".");
    }


    var prezzo_maxi_prima = $('#form_gestione_prodotti input[name="prezzo_maxi_prima"]:visible').val();
    if (prezzo_maxi_prima === undefined) {
        prezzo_maxi_prima = "";
    } else {
        prezzo_maxi_prima = prezzo_maxi_prima.replace(",", ".");
    }

    var prezzo_4 = $('#form_gestione_prodotti input[name="prezzo_4"]:visible').val();
    if (prezzo_4 === undefined || prezzo_4 === null || prezzo_4 === "") {
        prezzo_4 = "0.00";
    } else {
        prezzo_4 = prezzo_4.replace(",", ".");
    }

    var prezzo_maxi = $('#form_gestione_prodotti input[name="prezzo_maxi"]:visible').val();
    if (prezzo_maxi === undefined) {
        prezzo_maxi = "0.00";
    } else {
        prezzo_maxi = prezzo_maxi.replace(",", ".");
    }

    var modifica_prezzo_auto = $('#form_gestione_prodotti input[name="modifica_prezzo_auto"]:visible').is(':checked') ? 'S' : 'N';
    if (modifica_prezzo_auto === undefined) {
        modifica_prezzo_auto = "N";
    }

    prezzo = parseFloat(prezzo).toFixed(2);
    costo = parseFloat(costo).toFixed(2);
    prezzo_varianti_aggiuntive = parseFloat(prezzo_varianti_aggiuntive).toFixed(2);
    prezzo_2 = parseFloat(prezzo_2).toFixed(2);
    prezzo_varianti_maxi = parseFloat(prezzo_varianti_maxi).toFixed(2);
    prezzo_maxi_prima = parseFloat(prezzo_maxi_prima).toFixed(2);
    prezzo_4 = parseFloat(prezzo_4).toFixed(2);
    prezzo_maxi = parseFloat(prezzo_maxi).toFixed(2);
    var qta_fissa = $('#form_gestione_prodotti input[name="qta_fissa"]:visible').val();
    if (qta_fissa === undefined || qta_fissa === "undefined") {
        qta_fissa = "";
    }
    var categoria = $('#form_gestione_prodotti select[name="categoria"]:visible').val();
    var dest_stampa = $('#form_gestione_prodotti select[name="dest_stampa"]:visible').val();
    var dest_stampa_2 = $('#form_gestione_prodotti select[name="dest_stampa_2"]:visible').val();
    var portata = $('#form_gestione_prodotti select[name="portata"]:visible').val();
    var gruppi_statistici = $('#form_gestione_prodotti select[name="gruppi_statistici"]:visible').val();
    /*var posizione = $('#form_gestione_prodotti input[name="posizione"]:visible').val();
     var pagina = $('#form_gestione_prodotti input[name="pagina"]:visible').val();*/
    var cat_var = $('#form_gestione_prodotti select[name="cat_var"]:visible').val();
    var cat_var_auto = $('#form_gestione_prodotti select[name="cat_var_auto"]:visible').val();
    var iva_base = $('#form_gestione_prodotti select[name="perc_iva_base"]:visible').val();
    var iva_takeaway = $('#form_gestione_prodotti select[name="perc_iva_takeaway"]:visible').val();
    var reparto_servizi = $('#form_gestione_prodotti select[name="reparto_servizi"]:visible').val();
    var reparto_beni = $('#form_gestione_prodotti select[name="reparto_beni"]:visible').val();
    var ordinamento = $('#form_gestione_prodotti input[name="ordinamento"]:visible').val();
    var ricetta = $('#form_gestione_prodotti input[name="ricetta"]:visible').val();
    var abilita_riepilogo = $('#form_gestione_prodotti input[name="abilita_riepilogo"]:visible').is(':checked') ? 'S' : 'N';
    var cod_promo = $('#form_gestione_prodotti input[name="cod_promo"]:visible').val();
    var descrizione_fastorder = $('#form_gestione_prodotti input[name="descrizione_fastorder"]:visible').val();
    var ricetta_fastorder = $('#form_gestione_prodotti input[name="ricetta_fastorder"]:visible').val();
    var immagine = $('#form_gestione_prodotti input[name="immagine"]:visible').val();
    var peso_ums = $('#form_gestione_prodotti select[name="scelta_unita_misura"]:visible').val();
    var prezzo_3 = $('#form_gestione_prodotti input[name="prezzo_x_um"]:visible').val();
    var listino_bar = $('#form_gestione_prodotti input[name="prezzo_bar"]:visible').val();
    var listino_asporto = $('#form_gestione_prodotti input[name="prezzo_asporto"]:visible').val();
    var prezzo_fattorino1_norm = $('#form_gestione_prodotti input[name="prezzo_fattorino1_norm"]:visible').val();
    var prezzo_fattorino1_maxi = $('#form_gestione_prodotti input[name="prezzo_fattorino1_maxi"]:visible').val();
    var prezzo_fattorino2_norm = $('#form_gestione_prodotti input[name="prezzo_fattorino2_norm"]:visible').val();
    var prezzo_fattorino2_maxi = $('#form_gestione_prodotti input[name="prezzo_fattorino2_maxi"]:visible').val();
    var prezzo_fattorino3_norm = $('#form_gestione_prodotti input[name="prezzo_fattorino3_norm"]:visible').val();
    var prezzo_fattorino3_maxi = $('#form_gestione_prodotti input[name="prezzo_fattorino3_maxi"]:visible').val();
    var listino_continuo = $('#form_gestione_prodotti input[name="prezzo_continuo"]:visible').val();
    var prezzo_variante_meno = $('#form_gestione_prodotti input[name="prezzo_variante_meno"]:visible').val();
    var spazio_forno = '1';

    var esportazione_fastorder_abilitata = $('#form_gestione_prodotti input[name="esportazione_fastorder_abilitata"]:visible').is(':checked') ? 'true' : 'false';

    //false significa che la variabile è vuota
    if (checkEmptyVariable(descrizione_fastorder) === false) {
        descrizione_fastorder = descrizione;
    }

    if (checkEmptyVariable(ricetta_fastorder) === false) {
        ricetta_fastorder = ricetta;
    }

    if ($('#form_gestione_prodotti input[name="spazio_forno"]:visible').val() !== undefined && !isNaN(parseInt($('#form_gestione_prodotti input[name="spazio_forno"]:visible').val()))) {
        spazio_forno = $('#form_gestione_prodotti input[name="spazio_forno"]:visible').val();
    }

    var testo_query = "SELECT * FROM prodotti WHERE categoria!='XXX' and id='" + id + "';";
    comanda.sincro.query(testo_query, function (res) {

        //Sugli articoli cancellati e modificati mette la data di ultima modifica
        var data_modifica = new Date().format("yyyymmddHHMMss");
        if (res !== undefined && res[0] !== undefined && res[0].descrizione !== undefined && typeof (res[0].descrizione) === "string" && (res[0].descrizione === ".NUOVO_PRODOTTO" || res[0].descrizione === ".NUOVO PRODOTTO")) {

            testo_query = "UPDATE prodotti SET esportazione_fastorder_abilitata='" + esportazione_fastorder_abilitata + "',prezzo_fattorino1_norm='" + prezzo_fattorino1_norm + "',prezzo_fattorino1_maxi='" + prezzo_fattorino1_maxi + "',prezzo_fattorino2_norm='" + prezzo_fattorino2_norm + "',prezzo_fattorino2_maxi='" + prezzo_fattorino2_maxi + "',prezzo_fattorino3_norm='" + prezzo_fattorino3_norm + "',prezzo_fattorino3_maxi='" + prezzo_fattorino3_maxi + "',automodifica_prezzo='" + modifica_prezzo_auto + "',ult_mod='" + data_modifica + "',listino_bar='" + listino_bar + "',listino_asporto='" + listino_asporto + "',prezzo_variante_meno='" + prezzo_variante_meno + "',listino_continuo='" + listino_continuo + "',peso_ums='" + peso_ums + "',prezzo_4='" + prezzo_4 + "',prezzo_3='" + prezzo_3 + "',spazio_forno='" + spazio_forno + "',qta_fissa='" + qta_fissa + "',cod_promo='" + cod_promo + "',abilita_riepilogo='" + abilita_riepilogo + "',immagine='" + immagine + "',descrizione_fastorder='" + descrizione_fastorder + "',ricetta_fastorder='" + ricetta_fastorder + "',ricetta='" + ricetta + "',ordinamento='" + ordinamento + "',descrizione='" + descrizione.toUpperCase() + "',prezzo_1='" + parseFloat(prezzo.replace(/,/g, ".")).toFixed(2) + "',costo='" + parseFloat(costo.replace(/,/g, ".")).toFixed(2) + "',prezzo_varianti_aggiuntive='" + parseFloat(prezzo_varianti_aggiuntive.replace(/,/g, ".")).toFixed(2) + "',prezzo_maxi_prima='" + parseFloat(prezzo_maxi_prima.replace(/,/g, ".")).toFixed(2) + "',prezzo_2='" + parseFloat(prezzo_2.replace(/,/g, ".")).toFixed(2) + "',prezzo_varianti_maxi='" + parseFloat(prezzo_varianti_maxi.replace(/,/g, ".")).toFixed(2) + "',prezzo_maxi='" + parseFloat(prezzo_maxi.replace(/,/g, ".")).toFixed(2) + "',categoria='" + categoria + "',dest_st_1='" + dest_stampa + "',dest_st_2='" + dest_stampa_2 + "',portata='" + portata + "',gruppo='" + gruppi_statistici + "',cat_varianti='" + cat_var + "/" + cat_var_auto + "',perc_iva_base='" + iva_base + "',perc_iva_takeaway='" + iva_takeaway + "',reparto_servizi='" + reparto_servizi + "',reparto_beni='" + reparto_beni + "' WHERE categoria!='XXX' and id='" + id + "';";
            console.log("QUERY PRODOTTO", testo_query);
            //comanda.array_dbcentrale_mancanti.push(testo_query);
            comanda.sock.send({
                tipo: "aggiornamento_posizione_prodotti",
                operatore: comanda.operatore,
                query: testo_query,
                terminale: comanda.terminale,
                ip: comanda.ip_address
            });
            comanda.sincro.query(testo_query, function () {

                selezione_operatore("GESTIONE PRODOTTI");
                gestisci_prodotto(event, id);
                crea_griglia_prodotti(comanda.dispositivo, function () { });
            });
        } else {
            testo_query = "UPDATE prodotti SET categoria='XXX',ult_mod='" + data_modifica + "' WHERE categoria!='XXX' and id='" + id + "';";
            comanda.sock.send({
                tipo: "nuovo_prodotto",
                operatore: comanda.operatore,
                query: testo_query,
                terminale: comanda.terminale,
                ip: comanda.ip_address
            });
            comanda.sincro.query(testo_query, function () {

                testo_query = "insert into prodotti (esportazione_fastorder_abilitata,reparto_servizi,reparto_beni,prezzo_fattorino1_norm,prezzo_fattorino1_maxi,prezzo_fattorino2_norm,prezzo_fattorino2_maxi,prezzo_fattorino3_norm,prezzo_fattorino3_maxi,automodifica_prezzo,prezzo_4,prezzo_varianti_maxi,prezzo_maxi,prezzo_maxi_prima,id,descrizione,prezzo_1,costo,prezzo_varianti_aggiuntive,prezzo_2,qta_fissa,categoria,dest_st_1,dest_st_2,portata,gruppo,cat_varianti,perc_iva_base,ordinamento,ricetta,perc_iva_takeaway,abilita_riepilogo,cod_promo,descrizione_fastorder,ricetta_fastorder,immagine,peso_ums,prezzo_3,listino_bar,listino_asporto,listino_continuo,prezzo_variante_meno,spazio_forno,colore_tasto,posizione_1,posizione_2,posizione_3,posizione_4,listino_tavolo,posizione) VALUES ('" + esportazione_fastorder_abilitata + "','" + reparto_servizi + "','" + reparto_beni + "','" + prezzo_fattorino1_norm + "','" + prezzo_fattorino1_maxi + "','" + prezzo_fattorino2_norm + "','" + prezzo_fattorino2_maxi + "','" + prezzo_fattorino3_norm + "','" + prezzo_fattorino3_maxi + "','" + modifica_prezzo_auto + "','" + prezzo_4 + "','" + prezzo_varianti_maxi + "','" + prezzo_maxi + "','" + prezzo_maxi_prima + "','" + id + "','" + descrizione + "','" + prezzo + "','" + costo + "','" + prezzo_varianti_aggiuntive + "','" + prezzo_2 + "','" + qta_fissa + "','" + categoria + "','" + dest_stampa + "','" + dest_stampa_2 + "','" + portata + "','" + gruppi_statistici + "','" + cat_var + "/" + cat_var_auto + "','" + iva_base + "','" + ordinamento + "','" + ricetta + "','" + iva_takeaway + "','" + abilita_riepilogo + "','" + cod_promo + "','" + descrizione_fastorder + "','" + ricetta_fastorder + "','" + immagine + "','" + peso_ums + "','" + prezzo_3 + "','" + listino_bar + "','" + listino_asporto + "','" + listino_continuo + "','" + prezzo_variante_meno + "','" + spazio_forno + "','" + res[0].colore_tasto + "','" + res[0].posizione_1 + "','" + res[0].posizione_2 + "','" + res[0].posizione_3 + "','" + res[0].posizione_4 + "','" + res[0].listino_tavolo + "','" + res[0].posizione + "');";
                console.log("QUERY_PRODOTTO", testo_query);
                comanda.sock.send({
                    tipo: "aggiornamento_posizione_prodotti",
                    operatore: comanda.operatore,
                    query: testo_query,
                    terminale: comanda.terminale,
                    ip: comanda.ip_address
                });
                //comanda.array_dbcentrale_mancanti.push(testo_query);
                comanda.sincro.query(testo_query, function () {

                    selezione_operatore("GESTIONE PRODOTTI");
                    gestisci_prodotto(event, id);
                    crea_griglia_prodotti(comanda.dispositivo, function () { });
                });
                //});
            });
        }

    });
}

function gestisci_cambio_listino(event, id) {

    let tabella_reparti_IVA = REPARTO_controller.tabella_reparti_IVA();
    let tabella_IVA_semplice = IVA_controller.tabella_IVA_semplice();
    if (event !== undefined && event.target !== undefined && event.target !== null) {
        $(event.target).parent().find("a").removeClass('cat_accesa');
        $(event.target).addClass("cat_accesa");
    }

    comanda.sincro.query("SELECT fastorder,articolo_a_peso FROM impostazioni_fiscali  where id=" + comanda.folder_number + "  LIMIT 1;", function (fastorder) {
        var fastorder_abilitato = fastorder[0].fastorder;
        var articolo_a_peso = fastorder[0].articolo_a_peso;
        $('#form_gestione_cambi_listino').html('');
        var form_gestione_cambi_listino = '';
        let result = alasql("SELECT * FROM cambi_listino WHERE categoria!='XXX' and id='" + id + "' ORDER BY id ASC LIMIT 1;");
        if (result.length === 0) {
            result = alasql("SELECT * FROM prodotti WHERE categoria!='XXX' and id='" + id + "' ORDER BY id ASC LIMIT 1;");
        }
        comanda.sincro.query("SELECT * FROM categorie WHERE id='" + comanda.categoria + "' ORDER BY id ASC LIMIT 1;", function (resultcat) {
            for (var key in result[0]) {
                console.log("RESULT0", key, result[0], result[0][key]);
                if (result[0][key] === null) {
                    result[0][key] = '';
                }
            }

            comanda.sincro.query('SELECT descrizione,cod_promo FROM prodotti WHERE cod_promo="1" and cat_varianti LIKE "%' + comanda.categoria + '";', function (r_cod_promo_1) {

                comanda.sincro.query("SELECT * FROM cod_promo_2 WHERE id_articolo='" + id + "' ORDER BY id ASC;", function (r_cod_promo_2) {

                    if (resultcat[0].descrizione.substr(0, 2) === "V.") {
                        form_gestione_prodotti = '<div class="editor_clienti">\n\
                                   <div class="col-md-12 col-xs-12">';
                        if (r_cod_promo_1[0] !== undefined) {
                            form_gestione_prodotti += '<div class="col-md-1 col-xs-1"><label>Qta Fissa</label><input style="margin-bottom:10px;" class="form-control" type="text" name="qta_fissa"   value="' + result[0].qta_fissa + '"></div>';
                        }

                        var prezzo_varianti_aggiuntive = "";
                        if (!isNaN(parseFloat(result[0].prezzo_varianti_aggiuntive))) {
                            prezzo_varianti_aggiuntive = parseFloat(result[0].prezzo_varianti_aggiuntive).toFixed(2);
                        }

                        var prezzo_2 = "0.00";
                        if (!isNaN(parseFloat(result[0].prezzo_2))) {
                            prezzo_2 = parseFloat(result[0].prezzo_2).toFixed(2);
                        }

                        var prezzo_maxi_prima = "";
                        if (!isNaN(parseFloat(result[0].prezzo_maxi_prima))) {
                            prezzo_maxi_prima = parseFloat(result[0].prezzo_maxi_prima).toFixed(2);
                        }

                        var prezzo_varianti_maxi = "";
                        if (!isNaN(parseFloat(result[0].prezzo_varianti_maxi))) {
                            prezzo_varianti_maxi = parseFloat(result[0].prezzo_varianti_maxi).toFixed(2);
                        }

                        form_gestione_prodotti += '<div class="col-md-6 col-xs-6"><label>' + comanda.lang[96] + '</label><input maxlength="20" style="text-transform:uppercase;font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="descrizione" value="' + result[0].descrizione.toUpperCase() + '"></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>' + comanda.lang[51] + '</label><input style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_1"   value="' + parseFloat(result[0].prezzo_1).toFixed(2) + '" placeholder="1.00"></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>Prezzo Variante Maxi</label><input style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_maxi_prima"   value="' + prezzo_maxi_prima + '" placeholder="1.00"></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>Prezzo Varianti Aggiuntive&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'PREZZO VARIANTI AGGIUNTIVE\', \'Inserendo il prezzo in questo campo, la prima variante agigunta alla pizza presenter&agrave; il prezzo regolare; dalla seconda in poi, presenter&agrave; il prezzo inserito in questa casella (prezzo varianti aggiuntive).\')">?</a></label><input style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_varianti_aggiuntive"   value="' + prezzo_varianti_aggiuntive + '" placeholder="0.50"></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>Prezzo Maxi Aggiuntive</label><input style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_varianti_maxi"   value="' + prezzo_varianti_maxi + '" placeholder="1.00"></div>';
                        form_gestione_prodotti += '<div class="col-md-8 col-xs-8">\n\
                                    <div class="col-xs-2"><label>Prezzo POCO</label><input style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_4" value="' + result[0].prezzo_4 + '"></div>\n\
                                    <div class="col-xs-2"><label>Prezzo Bar</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_bar" value="' + result[0].listino_bar + '"></div>\n\
                                    <div class="col-xs-2"><label>Prezzo Asporto</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_asporto" value="' + result[0].listino_asporto + '"></div>\n\
                                    <div class="col-xs-2"><label>Pr. Continuo</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_continuo" value="' + result[0].listino_continuo + '"></div>\n\
                                    <div class="col-xs-2"><label>Prezzo Var. -</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_variante_meno" value="' + result[0].prezzo_variante_meno + '"></div>\n\
                                   </div>';
                        form_gestione_prodotti += '<div class="col-md-12 col-xs-12">';
                        if (comanda.abilita_fattorino1 === "1") {
                            let nome_fattorino = "Fattorino 1";
                            if (comanda.nome_fattorino1 !== undefined && comanda.nome_fattorino1 !== null && comanda.nome_fattorino1.trim().length > 0) {
                                nome_fattorino = comanda.nome_fattorino1.trim();
                            }
                            form_gestione_prodotti += '<div class="col-xs-2"><label>Prezzo ' + nome_fattorino + ' Base</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_fattorino1_norm" value="' + result[0].prezzo_fattorino1_norm + '"></div>';
                            form_gestione_prodotti += '<div class="col-xs-2"><label>Prezzo ' + nome_fattorino + ' Maxi</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_fattorino1_maxi" value="' + result[0].prezzo_fattorino1_maxi + '"></div>';
                        }

                        if (comanda.abilita_fattorino2 === "1") {
                            let nome_fattorino = "Fattorino 2";
                            if (comanda.nome_fattorino2 !== undefined && comanda.nome_fattorino2 !== null && comanda.nome_fattorino2.trim().length > 0) {
                                nome_fattorino = comanda.nome_fattorino2.trim();
                            }
                            form_gestione_prodotti += '<div class="col-xs-2"><label>Prezzo ' + nome_fattorino + ' Base</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_fattorino2_norm" value="' + result[0].prezzo_fattorino2_norm + '"></div>';
                            form_gestione_prodotti += '<div class="col-xs-2"><label>Prezzo ' + nome_fattorino + ' Maxi</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_fattorino2_maxi" value="' + result[0].prezzo_fattorino2_maxi + '"></div>';
                        }

                        if (comanda.abilita_fattorino3 === "1") {
                            let nome_fattorino = "Fattorino 3";
                            if (comanda.nome_fattorino3 !== undefined && comanda.nome_fattorino3 !== null && comanda.nome_fattorino3.trim().length > 0) {
                                nome_fattorino = comanda.nome_fattorino3.trim();
                            }
                            form_gestione_prodotti += '<div class="col-xs-2"><label>Prezzo ' + nome_fattorino + ' Base</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_fattorino3_norm" value="' + result[0].prezzo_fattorino3_norm + '"></div>';
                            form_gestione_prodotti += '<div class="col-xs-2"><label>Prezzo ' + nome_fattorino + ' Maxi</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_fattorino3_maxi" value="' + result[0].prezzo_fattorino3_maxi + '"></div>';
                        }
                        form_gestione_prodotti += '</div>';
                        form_gestione_prodotti += '<div class="col-md-4 col-xs-4"><label>' + comanda.lang[24] + '</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="categoria" ></select></div>\n\
                                   </div>\n\
                                   <div class="col-md-12 col-xs-12">\n\
                                   <div class="col-md-3 col-xs-3" style="display:none"><label>' + comanda.lang[100] + '</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="dest_stampa" ></select></div>\n\
                                   <div class="col-md-3 col-xs-3" style="display:none"><label>' + comanda.lang[100] + ' 2</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="dest_stampa_2" ></select></div>\n\
                                   <div class="col-md-3 col-xs-3" style="display:none"><label>' + comanda.lang[126] + '</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="portata" ></select></div>\n\
                                   <div class="col-md-3 col-xs-3" style="display:none"><label>Gruppo Statistico</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="gruppi_statistici" ></select></div>\n\
                                   <div class="col-md-2 col-xs-2" style="display:none;"><label>' + comanda.lang[171] + '</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="posizione" value="' + result[0].posizione + '"></div>\n\
                                   <div class="col-md-1 col-xs-1" style="display:none;"><label>' + comanda.lang[172] + '</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="pagina" value="' + result[0].pagina + '"></div>\n\
                                   </div>';
                        form_gestione_prodotti += '<div class="col-md-12 col-xs-12">\n\
                                   <div class="col-md-4 col-xs-3" style="display:none"><label>' + comanda.lang[173] + '</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="cat_var" ></select></div>\n\
                                   <div class="col-md-4 col-xs-3" style="display:none"><label>' + comanda.lang[174] + '</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="cat_var_auto" ></select></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>Iva Base / Servizi</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="perc_iva_base" >' + tabella_reparti_IVA + '</select></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>Iva TakeAway / Beni</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="perc_iva_takeaway" >' + tabella_reparti_IVA + '</select></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>' + comanda.lang[177] + '</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="ordinamento" value="' + result[0].ordinamento + '"></div>\n\
                                   <div class="col-md-12 col-xs-12" style="display:none"><label>' + comanda.lang[178] + '</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" maxlength="38" name="ricetta" value="' + result[0].ricetta + '"></div>';
                        if (fastorder_abilitato === '1') {
                            form_gestione_prodotti += '<div class="col-md-12 col-xs-12" style="display:none"><label>Nome Immagine</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="immagine" value="' + result[0].immagine + '"></div>';
                            form_gestione_prodotti += '<div class="col-md-12 col-xs-12" style="display:none"><label>Ricetta fast.order</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="ricetta_fastorder" value="' + result[0].ricetta_fastorder + '"></div>';
                            //form_gestione_prodotti += '<div class="col-md-2 col-xs-2"  style="display:none"><label>Codice Promozionale</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="cod_promo" onkeyup="salva_cambio_listino(event,\'' + result[0].id + '\');" maxlength="2" value=""></div>';
                        }


                        form_gestione_prodotti += '<div class="col-md-12 col-xs-12">\n\
                                  <div class="col-md-12 col-xs-12" style="color:green;"><h2>PROSSIMA MODIFICA:</h2></div>\n\
                                  <div class="col-md-2 col-xs-2"><label>Giorno</label><input style="margin-bottom:10px;" class="form-control" type="text" name="giorno" value="' + result_cambi[0].giorno + '" placeholder="01"></div>\n\
                                  <div class="col-md-2 col-xs-2"><label>Mese</label><input style="margin-bottom:10px;" class="form-control" type="text" name="mese" value="' + result_cambi[0].mese + '" placeholder="01"></div>\n\
                                  <div class="col-md-2 col-xs-2"><label>Anno</label><input style="margin-bottom:10px;" class="form-control" type="text" name="anno" value="' + result_cambi[0].anno + '" placeholder="2001"></div>\n\
                                  \n\<div class="col-md-12 col-xs-12"></div>\n\
                                  <div class="col-md-3 col-xs-3"><label>Prezzo 1</label><input style="margin-bottom:10px;" class="form-control" type="text" name="prezzo_1"   value="' + result_cambi[0].prezzo_1 + '"></div>\n\
                                  <div class="col-md-3 col-xs-3"><label>Prezzo 2</label><input style="margin-bottom:10px;" class="form-control" type="text" name="prezzo_2"   value="' + result_cambi[0].prezzo_2 + '"></div>\n\
                                  <div class="col-md-3 col-xs-3"><label>Prezzo 3</label><input style="margin-bottom:10px;" class="form-control" type="text" name="prezzo_3"   value="' + result_cambi[0].prezzo_3 + '"></div>\n\
                                  <div class="col-md-3 col-xs-3"><label>Prezzo 4</label><input style="margin-bottom:10px;" class="form-control" type="text" name="prezzo_4"   value="' + result_cambi[0].prezzo_4 + '"></div>\n\
                                    </div>';
                        form_gestione_prodotti += '</div>';
                        form_gestione_prodotti += '<div class="col-md-12 col-xs-12">';
                        form_gestione_prodotti += '<button class="btn_salva_prodotto pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_cambio_listino(event,\'' + result[0].id + '\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button>';
                        form_gestione_prodotti += '</div>';
                        $('#form_gestione_cambi_listino').html(form_gestione_prodotti);
                        //RIMUOVO I VECCHI RISULTATI DELLE TENDINE
                        $('select[name="dest_stampa"] option').remove();
                        $('select[name="dest_stampa_2"] option').remove();
                        $('select[name="portata"] option').remove();
                        $('select[name="gruppi_statistici"] option').remove();
                        $('[name="perc_iva_base"] option[value="' + result[0].perc_iva_base + '"]').attr("selected", true);
                        $('[name="perc_iva_takeaway"] option[value="' + result[0].perc_iva_takeaway + '"]').attr("selected", true);
                        var destinazione_stampa = '';
                        var destinazione_stampa_2 = '';
                        var opzione;
                        //RIEMPIO LA TENDINA DELLE CATEGORIE
                        comanda.sincro.query("SELECT id,ordinamento,descrizione FROM categorie ORDER BY ordinamento ASC;", function (results) {
                            results.forEach(function (row) {
                                opzione = "<option value=\"" + row.id + "\">" + row.descrizione + "</option>";
                                $('[name="categoria"]').append(opzione);
                            });
                            var categoria = '';
                            if (result[0].categoria !== undefined && result[0].categoria.length > 0) {
                                categoria = result[0].categoria;
                            } else {
                                categoria = resultcat[0].categoria;
                            }
                            $('[name="categoria"] option[value="' + categoria + '"]').attr("selected", true);
                        });
                    }
                    //SE NON E' UNA CATEGORIA DI VARIANTI
                    else {

                        var spazio_forno = '1';
                        if (!isNaN(parseInt(result[0].spazio_forno))) {
                            spazio_forno = result[0].spazio_forno;
                        }

                        var prezzo_un_quarto = "";
                        if (parseFloat(result[0].prezzo_2).toFixed(2) !== "NaN") {
                            prezzo_un_quarto = parseFloat(result[0].prezzo_2).toFixed(2);
                        }

                        var prezzo_maxi = "0.00";
                        if (!isNaN(parseFloat(result[0].prezzo_maxi))) {
                            prezzo_maxi = parseFloat(result[0].prezzo_maxi).toFixed(2);
                        }

                        form_gestione_prodotti = '<div class="editor_clienti">\n\
                                   <div class="col-md-12 col-xs-12">\n\
                                    <div class="col-xs-6"><label>Descrizione</label><input maxlength="20" style="text-transform:uppercase;font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="descrizione" value="' + result[0].descrizione.toUpperCase() + '"></div>\n\
                                    <div class="col-xs-2"><label>Spazio&nbsp;forno</label><input style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="spazio_forno"   value="' + spazio_forno + '"></div>\n\
                                    <div class="col-xs-2"><label>Prezzo Default</label><input style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_1"   value="' + parseFloat(result[0].prezzo_1).toFixed(2) + '"></div>\n\
                                    <div class="col-xs-2"><label>Prezzo Maxi</label><input style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_maxi"   value="' + prezzo_maxi + '"></div>\n\
                                    <div class="col-xs-2"><label>Prezzo Bar</label><input style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_bar" value="' + result[0].listino_bar + '"></div>\n\
                                    <div class="col-xs-2"><label>Pr. Asporto</label><input style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_asporto" value="' + result[0].listino_asporto + '"></div>\n\
                                   </div>';
                        form_gestione_prodotti += '<div class="col-md-12 col-xs-12">';
                        if (comanda.abilita_fattorino1 === "1") {
                            let nome_fattorino = "Fattorino 1";
                            if (comanda.nome_fattorino1 !== undefined && comanda.nome_fattorino1 !== null && comanda.nome_fattorino1.trim().length > 0) {
                                nome_fattorino = comanda.nome_fattorino1.trim();
                            }
                            form_gestione_prodotti += '<div class="col-xs-2"><label>Prezzo ' + nome_fattorino + ' Base</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_fattorino1_norm" value="' + result[0].prezzo_fattorino1_norm + '"></div>';
                            form_gestione_prodotti += '<div class="col-xs-2"><label>Prezzo ' + nome_fattorino + ' Maxi</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_fattorino1_maxi" value="' + result[0].prezzo_fattorino1_maxi + '"></div>';
                        }

                        if (comanda.abilita_fattorino2 === "1") {
                            let nome_fattorino = "Fattorino 2";
                            if (comanda.nome_fattorino2 !== undefined && comanda.nome_fattorino2 !== null && comanda.nome_fattorino2.trim().length > 0) {
                                nome_fattorino = comanda.nome_fattorino2.trim();
                            }
                            form_gestione_prodotti += '<div class="col-xs-2"><label>Prezzo ' + nome_fattorino + ' Base</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_fattorino2_norm" value="' + result[0].prezzo_fattorino2_norm + '"></div>';
                            form_gestione_prodotti += '<div class="col-xs-2"><label>Prezzo ' + nome_fattorino + ' Maxi</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_fattorino2_maxi" value="' + result[0].prezzo_fattorino2_maxi + '"></div>';
                        }

                        if (comanda.abilita_fattorino3 === "1") {
                            let nome_fattorino = "Fattorino 3";
                            if (comanda.nome_fattorino3 !== undefined && comanda.nome_fattorino3 !== null && comanda.nome_fattorino3.trim().length > 0) {
                                nome_fattorino = comanda.nome_fattorino3.trim();
                            }
                            form_gestione_prodotti += '<div class="col-xs-2"><label>Prezzo ' + nome_fattorino + ' Base</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_fattorino3_norm" value="' + result[0].prezzo_fattorino3_norm + '"></div>';
                            form_gestione_prodotti += '<div class="col-xs-2"><label>Prezzo ' + nome_fattorino + ' Maxi</label><input placeholder="1.50" style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_fattorino3_maxi" value="' + result[0].prezzo_fattorino3_maxi + '"></div>';
                        }
                        form_gestione_prodotti += '</div>';
                        form_gestione_prodotti += '<div class="col-md-9 col-xs-9">\n\
                                    <div class="col-xs-2"><label>Prezzo t. Continuo</label><input style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_continuo" value="' + result[0].listino_continuo + '"></div>\n\
                                    <div class="col-xs-2"><label>Prezzo 1/4 di metro</label><input style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_2"   value="' + prezzo_un_quarto + '"></div>\n\
                                    <div class="col-xs-8"><label>Categoria</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="categoria" ></select></div>\n\
                            </div>';
                        if (articolo_a_peso === '1') {

                            form_gestione_prodotti += '<div class="col-md-3 col-xs-3">\n\
                                   <div class="col-xs-6"><label>Unita\' di Misura</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" name="scelta_unita_misura"><option value="Kg">Kg</option><option value="Hg">Hg</option><option value="u">Unita\'</option></select></div>\n\
                                   <div class="col-xs-6"><label>Prezzo x U.</label><input style="margin-bottom:10px;" class="form-control parte_numerica" type="text" name="prezzo_x_um" value="' + result[0].prezzo_3 + '"></div>\n\
                                   </div>';
                        }



                        form_gestione_prodotti += '<div class="col-md-12 col-xs-12">\n\
                                   <div class="col-md-3 col-xs-3"><label>Destinazione di stampa</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="dest_stampa" ></select></div>\n\
                                   <div class="col-md-3 col-xs-3"><label>Destinazione di stampa 2</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="dest_stampa_2" ></select></div>\n\
                                   <div class="col-md-3 col-xs-3"><label>Portata</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="portata" ></select></div>\n\
                                   <div class="col-md-3 col-xs-3"><label>Gruppo Statistico</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="gruppi_statistici" ></select></div>\n\
                                   <div class="col-md-2 col-xs-2" style="display:none;"><label>' + comanda.lang[171] + '</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="posizione" value="' + result[0].posizione + '"></div>\n\
                                   <div class="col-md-1 col-xs-1" style="display:none;"><label>' + comanda.lang[172] + '</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="pagina" value="' + result[0].pagina + '"></div>\n\
                                   </div>\n\
                                   \n\
                                   <div class="col-md-12 col-xs-12">\n\
                                   <div class="col-md-4 col-xs-3"><label>' + comanda.lang[173] + '</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="cat_var" ></select></div>\n\
                                   <div class="col-md-4 col-xs-3"><label>' + comanda.lang[174] + '</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="cat_var_auto" ></select></div>\n\                                                                     \n\
                                   <div class="col-md-2 col-xs-2"><label>Iva Base / Servizi</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="perc_iva_base" >' + tabella_reparti_IVA + '</select></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>Iva TakeAway / Beni</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="perc_iva_takeaway" >' + tabella_reparti_IVA + '</select></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>' + comanda.lang[177] + '</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control parte_numerica" type="text" name="ordinamento" value="' + result[0].ordinamento + '"></div>';
                        var test1 = result[0].automodifica_prezzo == "S" ? "checked" : "";
                        form_gestione_prodotti += '<div class="col-md-2 col-xs-2"><label>Modifica Prezzo Auto.&nbsp;<a style="font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'MODIFICA PREZZO AUTOMATICA\', \'Spuntando questa casella, una volta battuto l articolo si aprir&agrave; in automatico la finestra del modifica prodotto, con il focus direttamente sulla modifica prezzo.\')">?</a></label><input class="form-control esclusione_css" type="checkbox" name="modifica_prezzo_auto"  ' + test1 + '></div>\n\
                                   <div class="col-md-12 col-xs-12"><label>' + comanda.lang[178] + '</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="ricetta" maxlength="100" value="' + result[0].ricetta + '"></div>';
                        var test1 = result[0].abilita_riepilogo == "S" ? "checked" : "";
                        form_gestione_prodotti += '<div class="col-md-1 col-xs-1"><label>Riepilogo</label><input class="form-control esclusione_css" type="checkbox" name="abilita_riepilogo"  ' + test1 + '></div>';
                        /* form_gestione_prodotti += '<div class="col-md-2 col-xs-2">\n\
                         <label>Codice Promozionale&nbsp;&nbsp;&nbsp;&nbsp;<a style="font-weight:bold;cursor:pointer;" ' + comanda.evento + '="$(\'#popup_spiegazione_cod_promo\').modal(\'show\')">?</a></label>\n\
                         <input style="font-weight:bold;margin-bottom:10px;" class="form-control parte_numerica" type="text" name="cod_promo" placeholder="Es.: 2" onkeyup="salva_cambio_listino(event,\'' + result[0].id + '\');" maxlength="2" value="' + result[0].cod_promo + '">\n\
                         </div>';*/
                        if (result[0].cod_promo === '2') {
                            form_gestione_prodotti += '<div class="col-md-2 col-xs-2"><label>Gestione Scontistica</label><button name="btn_cod_promo_2" class="btn btn-info" style="margin-top: -2px;" ' + comanda.evento + '="popup_aggiungi_promo_2(\'' + result[0].id + '\')">+</button></div>';
                            form_gestione_prodotti += '<div class="col-md-12 col-xs-12" style="font-weight:bold;"><div class="col-xs-2">Soglia</div><div class="col-xs-2">T.Sconto</div><div class="col-xs-2">Sconto</div><div class="col-xs-1">Da ora</div><div class="col-xs-1">A ora</div><div class="col-xs-2">Giorni</div><div class="col-xs-2">Opzioni</div></div>';
                            r_cod_promo_2.forEach(function (v) {

                                var array_giorni_settimana = new Array();
                                if (v.giorni_settimana.match(/\d+/gi) !== null) {
                                    v.giorni_settimana.match(/\d+/gi).forEach(function (v) {

                                        switch (v) {
                                            case "1":
                                                array_giorni_settimana.push("Lu");
                                                break;
                                            case "2":
                                                array_giorni_settimana.push("Ma");
                                                break;
                                            case "3":
                                                array_giorni_settimana.push("Me");
                                                break;
                                            case "4":
                                                array_giorni_settimana.push("Gi");
                                                break;
                                            case "5":
                                                array_giorni_settimana.push("Ve");
                                                break;
                                            case "6":
                                                array_giorni_settimana.push("Sa");
                                                break;
                                            case "0":
                                                array_giorni_settimana.push("Do");
                                                break;
                                            default:
                                                array_giorni_settimana.push("TUTTI");
                                        }


                                    });
                                }

                                form_gestione_prodotti += '<div class="col-md-12 col-xs-12"><div class="col-xs-2">' + v.qta_soglia + '</div><div class="col-xs-2">' + v.tipo_sconto + '</div><div class="col-xs-2">' + v.valore_sconto + '</div><div class="col-xs-1">' + v.da_ora + '</div><div class="col-xs-1">' + v.a_ora + '</div><div class="col-xs-2">' + array_giorni_settimana.join() + '</div><div class="col-xs-2" style="color:red;cursor:pointer;" ' + comanda.evento + '="cancella_cod_promo_2(event,\'' + v.id + '\',\'' + result[0].id + '\')">X</div></div>';
                            });
                        }
                        if (fastorder_abilitato === '1') {
                            form_gestione_prodotti += '<div class="col-md-12 col-xs-12"><label>Nome Immagine</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="immagine" value="' + result[0].immagine + '"></div>';
                            form_gestione_prodotti += '<div class="col-md-12 col-xs-12"><label>Ricetta fast.order</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="ricetta_fastorder" value="' + result[0].ricetta_fastorder + '"></div>';
                        }

                        let result_cambi = alasql("SELECT * FROM cambi_listino WHERE id_prodotto='" + id + "' ORDER BY id_prodotto ASC LIMIT 1;");
                        if (result_cambi !== undefined && result_cambi[0] !== undefined) {
                            if (isNaN(parseFloat(result_cambi[0].prezzo_1))) {
                                result_cambi[0].prezzo_1 = '';
                            } else {
                                result_cambi[0].prezzo_1 = parseFloat(result_cambi[0].prezzo_1).toFixed(2);
                            }
                            if (isNaN(parseFloat(result_cambi[0].prezzo_2))) {
                                result_cambi[0].prezzo_2 = '';
                            } else {
                                result_cambi[0].prezzo_2 = parseFloat(result_cambi[0].prezzo_2).toFixed(2);
                            }
                            if (isNaN(parseFloat(result_cambi[0].prezzo_3))) {
                                result_cambi[0].prezzo_3 = '';
                            } else {
                                result_cambi[0].prezzo_3 = parseFloat(result_cambi[0].prezzo_3).toFixed(2);
                            }
                            if (isNaN(parseFloat(result_cambi[0].prezzo_4))) {
                                result_cambi[0].prezzo_4 = '';
                            } else {
                                result_cambi[0].prezzo_4 = parseFloat(result_cambi[0].prezzo_4).toFixed(2);
                            }
                        } else {
                            result_cambi[0] = new Object();
                            result_cambi[0].giorno = '';
                            result_cambi[0].mese = '';
                            result_cambi[0].anno = '';
                            result_cambi[0].prezzo_1 = '';
                            result_cambi[0].prezzo_2 = '';
                            result_cambi[0].prezzo_3 = '';
                            result_cambi[0].prezzo_4 = '';
                        }


                        form_gestione_prodotti += '<div class="col-md-12 col-xs-12">\n\
                                  <div class="col-md-12 col-xs-12" style="color:green;"><h2>PROSSIMA MODIFICA:</h2></div>\n\
                                  <div class="col-md-2 col-xs-2"><label>Giorno</label><input style="margin-bottom:10px;" class="form-control" type="text" name="giorno" value="' + result_cambi[0].giorno + '" placeholder="01"></div>\n\
                                  <div class="col-md-2 col-xs-2"><label>Mese</label><input style="margin-bottom:10px;" class="form-control" type="text" name="mese" value="' + result_cambi[0].mese + '" placeholder="01"></div>\n\
                                  <div class="col-md-2 col-xs-2"><label>Anno</label><input style="margin-bottom:10px;" class="form-control" type="text" name="anno" value="' + result_cambi[0].anno + '" placeholder="2001"></div>';
                        form_gestione_prodotti += '</div>';
                        form_gestione_prodotti += '<div class="col-md-12 col-xs-12" style="margin-top: 1vh;"><button class="btn_salva_prodotto pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_cambio_listino(event,\'' + result[0].id + '\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>';
                        form_gestione_prodotti += '</div>';
                        $('#form_gestione_cambi_listino').html(form_gestione_prodotti);
                        //RIMUOVO I VECCHI RISULTATI DELLE TENDINE
                        $('select[name="dest_stampa"] option').remove();
                        $('select[name="dest_stampa_2"] option').remove();
                        $('select[name="portata"] option').remove();
                        $('select[name="gruppi_statistici"] option').remove();
                        $('[name="perc_iva_base"] option[value="' + result[0].perc_iva_base + '"]').attr("selected", true);
                        $('[name="perc_iva_takeaway"] option[value="' + result[0].perc_iva_takeaway + '"]').attr("selected", true);
                        $('[name="scelta_unita_misura"] option[value="' + result[0].peso_ums + '"]').attr("selected", true);
                        var destinazione_stampa = '';
                        var destinazione_stampa_2 = '';
                        var opzione;
                        //RIEMPIO LA TENDINA DELLE CATEGORIE
                        comanda.sincro.query("SELECT id,ordinamento,descrizione FROM categorie ORDER BY ordinamento ASC;", function (results) {
                            results.forEach(function (row) {
                                opzione = "<option value=\"" + row.id + "\">" + row.descrizione + "</option>";
                                $('[name="categoria"]').append(opzione);
                            });
                            var categoria = '';
                            if (result[0].categoria !== undefined && result[0].categoria.length > 0) {
                                categoria = result[0].categoria;
                            } else {
                                categoria = resultcat[0].categoria;
                            }
                            $('[name="categoria"] option[value="' + categoria + '"]').attr("selected", true);
                        });
                        //RIEMPIO LA TENDINA DELLE CATEGORIA VARIANTI
                        comanda.sincro.query("SELECT id,ordinamento,descrizione FROM categorie WHERE SUBSTR(descrizione,1,2)=\"V.\" ORDER BY ordinamento ASC;", function (results) {

                            opzione = "<option value=\"ND\">NESSUNA</option>";
                            $('[name="cat_var"]').append(opzione);
                            results.forEach(function (row) {


                                opzione = "<option value=\"" + row.id + "\">" + row.descrizione + "</option>";
                                $('[name="cat_var"]').append(opzione);
                            });
                            var categoria_variante = '';
                            if (result[0].cat_varianti !== undefined && result[0].cat_varianti.length > 0) {
                                categoria_variante = result[0].cat_varianti.split('/')[0];
                            } else {
                                categoria_variante = resultcat[0].cat_var;
                            }

                            $('[name="cat_var"] option[value="' + categoria_variante + '"]').attr("selected", true);
                        });
                        //RIEMPIO LA TENDINA DELLE CATEGORIA VARIANTI AUTOMATICHE
                        comanda.sincro.query("SELECT id,ordinamento,descrizione FROM categorie WHERE SUBSTR(descrizione,1,2)=\"V.\" ORDER BY ordinamento ASC;", function (results) {

                            opzione = "<option value=\"ND\">NESSUNA</option>";
                            $('[name="cat_var_auto"]').append(opzione);
                            results.forEach(function (row) {
                                opzione = "<option value=\"" + row.id + "\">" + row.descrizione + "</option>";
                                $('[name="cat_var_auto"]').append(opzione);
                            });
                            var categoria_variante = '';
                            if (result[0].cat_varianti !== undefined && result[0].cat_varianti.length > 0) {
                                if (result[0].cat_varianti.indexOf('/') !== -1) {
                                    categoria_variante = result[0].cat_varianti.split('/')[1];
                                }
                            }
                            $('[name="cat_var_auto"] option[value="' + categoria_variante + '"]').attr("selected", true);
                        });
                        //RIEMPIO LA TENDINA DELLE STAMPANTI
                        comanda.sincro.query("SELECT " + comanda.lingua_stampa + " as nome,numero FROM nomi_stampanti WHERE fiscale='n' and  cast(numero as int) >= 50;", function (results) {

                            $('[name="dest_stampa_2"]').append("<option></option>");
                            results.forEach(function (row) {
                                opzione = "<option value=\"" + row.numero + "\">" + row.nome + "</option>";
                                $('[name="dest_stampa"]').append(opzione);
                                $('[name="dest_stampa_2"]').append(opzione);
                            });
                            $('[name="dest_stampa"]').append("<option value=\"T\">TUTTE</option>");
                            if (result[0].dest_st_1 !== undefined && result[0].dest_st_1.length > 0) {
                                destinazione_stampa = result[0].dest_st_1;
                            } else {
                                destinazione_stampa = resultcat[0].dest_stampa;
                            }

                            if (result[0].dest_st_2 !== undefined && result[0].dest_st_2.length > 0) {
                                destinazione_stampa_2 = result[0].dest_st_2;
                            }

                            $('[name="dest_stampa"] option[value="' + destinazione_stampa + '"]').attr("selected", true);
                            $('[name="dest_stampa_2"] option[value="' + destinazione_stampa_2 + '"]').attr("selected", true);
                        });
                        //RIEMPIO LA TENDINA DELLA PORTATA
                        var portata = '';
                        comanda.sincro.query("SELECT " + comanda.lingua_stampa + ",numero FROM nomi_portate  order by numero asc;", function (results) {
                            results.forEach(function (row) {
                                opzione = "<option value=\"" + row.numero + "\">" + row[comanda.lingua_stampa] + "</option>";
                                $('[name="portata"]').append(opzione);
                            });
                            if (result[0].dest_st_1 !== undefined && result[0].dest_st_1.length > 0) {
                                portata = result[0].portata;
                            } else {
                                portata = resultcat[0].portata;
                            }
                            $('[name="portata"] option[value="' + portata + '"]').attr("selected", true);
                        });
                        //RIEMPIO LA TENDINA DEI GRUPPI STATISTICI
                        var gruppi_statistici = '';
                        comanda.sincro.query("SELECT nome,id FROM gruppi_statistici order by cast(id as int) asc;", function (results) {
                            results.forEach(function (row) {
                                opzione = "<option value=\"" + row.id + "\">" + row.nome + "</option>";
                                $('[name="gruppi_statistici"]').append(opzione);
                            });
                            if (result[0].gruppo !== undefined && result[0].gruppo.length > 0) {
                                gruppi_statistici = result[0].gruppo;
                            } else {
                                gruppi_statistici = "0";
                            }

                            $('[name="gruppi_statistici"] option[value="' + gruppi_statistici + '"]').attr("selected", true);
                        });
                    }
                });
            });
        });
    });
}

function salva_cambio_listino(event, id) {

    var giorno = $('input[name="giorno"]:visible').val();
    var mese = $('input[name="mese"]:visible').val();
    var anno = $('input[name="anno"]:visible').val();
    var descrizione = $('#form_gestione_cambi_listino input[name="descrizione"]:visible').val().replace(/'/gi, '').replace(/"/gi, '').toUpperCase();
    var prezzo = $('#form_gestione_cambi_listino input[name="prezzo_1"]:visible').val();
    if (prezzo === undefined) {
        prezzo = "0.00";
    } else {
        prezzo = prezzo.replace(",", ".");
    }

    var prezzo_varianti_aggiuntive = $('#form_gestione_cambi_listino input[name="prezzo_varianti_aggiuntive"]:visible').val();
    if (prezzo_varianti_aggiuntive === undefined) {
        prezzo_varianti_aggiuntive = "";
    } else {
        prezzo_varianti_aggiuntive = prezzo_varianti_aggiuntive.replace(",", ".");
    }

    var prezzo_2 = $('#form_gestione_cambi_listino input[name="prezzo_2"]:visible').val();
    if (prezzo_2 === undefined) {
        prezzo_2 = "0.00";
    } else {
        prezzo_2 = prezzo_2.replace(",", ".");
    }

    var prezzo_varianti_maxi = $('#form_gestione_cambi_listino input[name="prezzo_varianti_maxi"]:visible').val();
    if (prezzo_varianti_maxi === undefined) {
        prezzo_varianti_maxi = "";
    } else {
        prezzo_varianti_maxi = prezzo_varianti_maxi.replace(",", ".");
    }


    var prezzo_maxi_prima = $('#form_gestione_cambi_listino input[name="prezzo_maxi_prima"]:visible').val();
    if (prezzo_maxi_prima === undefined) {
        prezzo_maxi_prima = "";
    } else {
        prezzo_maxi_prima = prezzo_maxi_prima.replace(",", ".");
    }

    var prezzo_4 = $('#form_gestione_cambi_listino input[name="prezzo_4"]:visible').val();
    if (prezzo_4 === undefined || prezzo_4 === null || prezzo_4 === "") {
        prezzo_4 = "0.00";
    } else {
        prezzo_4 = prezzo_4.replace(",", ".");
    }

    var prezzo_maxi = $('#form_gestione_cambi_listino input[name="prezzo_maxi"]:visible').val();
    if (prezzo_maxi === undefined) {
        prezzo_maxi = "0.00";
    } else {
        prezzo_maxi = prezzo_maxi.replace(",", ".");
    }

    var modifica_prezzo_auto = $('#form_gestione_cambi_listino input[name="modifica_prezzo_auto"]:visible').is(':checked') ? 'S' : 'N';
    if (modifica_prezzo_auto === undefined) {
        modifica_prezzo_auto = "N";
    }

    prezzo = parseFloat(prezzo).toFixed(2);
    prezzo_varianti_aggiuntive = parseFloat(prezzo_varianti_aggiuntive).toFixed(2);
    prezzo_2 = parseFloat(prezzo_2).toFixed(2);
    prezzo_varianti_maxi = parseFloat(prezzo_varianti_maxi).toFixed(2);
    prezzo_maxi_prima = parseFloat(prezzo_maxi_prima).toFixed(2);
    prezzo_4 = parseFloat(prezzo_4).toFixed(2);
    prezzo_maxi = parseFloat(prezzo_maxi).toFixed(2);
    var qta_fissa = $('#form_gestione_cambi_listino input[name="qta_fissa"]:visible').val();
    if (qta_fissa === undefined || qta_fissa === "undefined") {
        qta_fissa = "";
    }
    var categoria = $('#form_gestione_cambi_listino select[name="categoria"]:visible').val();
    var dest_stampa = $('#form_gestione_cambi_listino select[name="dest_stampa"]:visible').val();
    var dest_stampa_2 = $('#form_gestione_cambi_listino select[name="dest_stampa_2"]:visible').val();
    var portata = $('#form_gestione_cambi_listino select[name="portata"]:visible').val();
    var gruppi_statistici = $('#form_gestione_cambi_listino select[name="gruppi_statistici"]:visible').val();
    /*var posizione = $('#form_gestione_cambi_listino input[name="posizione"]:visible').val();
     var pagina = $('#form_gestione_cambi_listino input[name="pagina"]:visible').val();*/
    var cat_var = $('#form_gestione_cambi_listino select[name="cat_var"]:visible').val();
    var cat_var_auto = $('#form_gestione_cambi_listino select[name="cat_var_auto"]:visible').val();
    var iva_base = $('#form_gestione_cambi_listino select[name="perc_iva_base"]:visible').val();
    var ordinamento = $('#form_gestione_cambi_listino input[name="ordinamento"]:visible').val();
    var ricetta = $('#form_gestione_cambi_listino input[name="ricetta"]:visible').val();
    var iva_takeaway = $('#form_gestione_cambi_listino select[name="perc_iva_takeaway"]:visible').val();
    var abilita_riepilogo = $('#form_gestione_cambi_listino input[name="abilita_riepilogo"]:visible').is(':checked') ? 'S' : 'N';
    var cod_promo = $('#form_gestione_cambi_listino input[name="cod_promo"]:visible').val();
    var fastorder = $('#form_gestione_cambi_listino input[name="ricetta_fastorder"]:visible').val();
    var immagine = $('#form_gestione_cambi_listino input[name="immagine"]:visible').val();
    var peso_ums = $('#form_gestione_cambi_listino select[name="scelta_unita_misura"]:visible').val();
    var prezzo_3 = $('#form_gestione_cambi_listino input[name="prezzo_x_um"]:visible').val();
    var listino_bar = $('#form_gestione_cambi_listino input[name="prezzo_bar"]:visible').val();
    var listino_asporto = $('#form_gestione_cambi_listino input[name="prezzo_asporto"]:visible').val();
    var prezzo_fattorino1_norm = $('#form_gestione_cambi_listino input[name="prezzo_fattorino1_norm"]:visible').val();
    var prezzo_fattorino1_maxi = $('#form_gestione_cambi_listino input[name="prezzo_fattorino1_maxi"]:visible').val();
    var prezzo_fattorino2_norm = $('#form_gestione_cambi_listino input[name="prezzo_fattorino2_norm"]:visible').val();
    var prezzo_fattorino2_maxi = $('#form_gestione_cambi_listino input[name="prezzo_fattorino2_maxi"]:visible').val();
    var prezzo_fattorino3_norm = $('#form_gestione_cambi_listino input[name="prezzo_fattorino3_norm"]:visible').val();
    var prezzo_fattorino3_maxi = $('#form_gestione_cambi_listino input[name="prezzo_fattorino3_maxi"]:visible').val();
    var listino_continuo = $('#form_gestione_cambi_listino input[name="prezzo_continuo"]:visible').val();
    var prezzo_variante_meno = $('#form_gestione_cambi_listino input[name="prezzo_variante_meno"]:visible').val();
    var spazio_forno = '1';
    if ($('#form_gestione_cambi_listino input[name="spazio_forno"]:visible').val() !== undefined && !isNaN(parseInt($('#form_gestione_cambi_listino input[name="spazio_forno"]:visible').val()))) {
        spazio_forno = $('#form_gestione_cambi_listino input[name="spazio_forno"]:visible').val();
    }



    var testo_query = "SELECT * FROM cambi_listino WHERE id_prodotto='" + id + "';";
    comanda.sincro.query(testo_query, function (res) {

        //Sugli articoli cancellati e modificati mette la data di ultima modifica
        var data_modifica = new Date().format("yyyymmddHHMMss");
        var d = new Date();
        var Y = d.getFullYear();
        var M = addZero((d.getMonth() + 1), 2);
        var D = addZero(d.getDate(), 2);
        var data_inserita = anno + "/" + mese + "/" + giorno;
        var data_inseritaa = new Date(data_inserita);
        var oggi = new Date();
        oggi.setHours(0, 0, 0, 0)
        //controllo che sia stata inserita una data e che non sia antecedente a quella odierna 
        if (giorno !== "" && mese !== "" && anno !== "" && data_inseritaa.getTime() >= oggi.getTime() || data_inseritaa.getTime() === oggi.getTime()) {
            if (descrizione !== "" && spazio_forno !== "" && prezzo !== "" && ordinamento !== "") {
                if (res !== undefined && res[0] !== undefined && res[0].descrizione !== undefined && typeof (res[0].descrizione) === "string") {

                    testo_query = "UPDATE cambi_listino SET giorno='" + giorno + "',mese='" + mese + "',anno='" + anno + "',prezzo_fattorino1_norm='" + prezzo_fattorino1_norm + "',prezzo_fattorino1_maxi='" + prezzo_fattorino1_maxi + "',prezzo_fattorino2_norm='" + prezzo_fattorino2_norm + "',prezzo_fattorino2_maxi='" + prezzo_fattorino2_maxi + "',prezzo_fattorino3_norm='" + prezzo_fattorino3_norm + "',prezzo_fattorino3_maxi='" + prezzo_fattorino3_maxi + "',automodifica_prezzo='" + modifica_prezzo_auto + "',ult_mod='" + data_modifica + "',listino_bar='" + listino_bar + "',listino_asporto='" + listino_asporto + "',prezzo_variante_meno='" + prezzo_variante_meno + "',listino_continuo='" + listino_continuo + "',peso_ums='" + peso_ums + "',prezzo_4='" + prezzo_4 + "',prezzo_3='" + prezzo_3 + "',spazio_forno='" + spazio_forno + "',qta_fissa='" + qta_fissa + "',cod_promo='" + cod_promo + "',abilita_riepilogo='" + abilita_riepilogo + "',immagine='" + immagine + "',ricetta_fastorder='" + fastorder + "',ricetta='" + ricetta + "',ordinamento='" + ordinamento + "',descrizione='" + descrizione.toUpperCase() + "',prezzo_1='" + parseFloat(prezzo.replace(/,/g, ".")).toFixed(2) + "',prezzo_varianti_aggiuntive='" + parseFloat(prezzo_varianti_aggiuntive.replace(/,/g, ".")).toFixed(2) + "',prezzo_maxi_prima='" + parseFloat(prezzo_maxi_prima.replace(/,/g, ".")).toFixed(2) + "',prezzo_2='" + parseFloat(prezzo_2.replace(/,/g, ".")).toFixed(2) + "',prezzo_varianti_maxi='" + parseFloat(prezzo_varianti_maxi.replace(/,/g, ".")).toFixed(2) + "',prezzo_maxi='" + parseFloat(prezzo_maxi.replace(/,/g, ".")).toFixed(2) + "',categoria='" + categoria + "',dest_st_1='" + dest_stampa + "',dest_st_2='" + dest_stampa_2 + "',portata='" + portata + "',gruppo='" + gruppi_statistici + "',cat_varianti='" + cat_var + "/" + cat_var_auto + "',perc_iva_base='" + iva_base + "',perc_iva_takeaway='" + iva_takeaway + "' WHERE categoria!='XXX' and id='" + id + "';";
                    console.log("QUERY PRODOTTO", testo_query);
                    //comanda.array_dbcentrale_mancanti.push(testo_query);
                    comanda.sock.send({ tipo: "aggiornamento_posizione_prodotti", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                    comanda.sincro.query(testo_query, function () {

                        selezione_operatore("CAMBIO LISTINO");
                        gestisci_cambio_listino(event, id);
                    });

                } else {


                    testo_query = "insert into cambi_listino (giorno,mese,anno,id_prodotto,prezzo_fattorino1_norm,prezzo_fattorino1_maxi,prezzo_fattorino2_norm,prezzo_fattorino2_maxi,prezzo_fattorino3_norm,prezzo_fattorino3_maxi,automodifica_prezzo,prezzo_4,prezzo_varianti_maxi,prezzo_maxi,prezzo_maxi_prima,id,descrizione,prezzo_1,prezzo_varianti_aggiuntive,prezzo_2,qta_fissa,categoria,dest_st_1,dest_st_2,portata,gruppo,cat_varianti,perc_iva_base,ordinamento,ricetta,perc_iva_takeaway,abilita_riepilogo,cod_promo,ricetta_fastorder,immagine,peso_ums,prezzo_3,listino_bar,listino_asporto,listino_continuo,prezzo_variante_meno,spazio_forno,reparto_servizi,reparto_beni) VALUES ('" + giorno + "','" + mese + "','" + anno + "','" + id + "','" + prezzo_fattorino1_norm + "','" + prezzo_fattorino1_maxi + "','" + prezzo_fattorino2_norm + "','" + prezzo_fattorino2_maxi + "','" + prezzo_fattorino3_norm + "','" + prezzo_fattorino3_maxi + "','" + modifica_prezzo_auto + "','" + prezzo_4 + "','" + prezzo_varianti_maxi + "','" + prezzo_maxi + "','" + prezzo_maxi_prima + "','" + id + "','" + descrizione + "','" + prezzo + "','" + prezzo_varianti_aggiuntive + "','" + prezzo_2 + "','" + qta_fissa + "','" + categoria + "','" + dest_stampa + "','" + dest_stampa_2 + "','" + portata + "','" + gruppi_statistici + "','" + cat_var + "/" + cat_var_auto + "','" + iva_base + "','" + ordinamento + "','" + ricetta + "','" + iva_takeaway + "','" + abilita_riepilogo + "','" + cod_promo + "','" + fastorder + "','" + immagine + "','" + peso_ums + "','" + prezzo_3 + "','" + listino_bar + "','" + listino_asporto + "','" + listino_continuo + "','" + prezzo_variante_meno + "','" + spazio_forno + "','" + comanda.reparto_servizi_default + "','" + comanda.reparto_beni_default + "');";
                    console.log("QUERY_PRODOTTO", testo_query);
                    comanda.sock.send({ tipo: "aggiornamento_posizione_prodotti", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                    //comanda.array_dbcentrale_mancanti.push(testo_query);
                    comanda.sincro.query(testo_query, function () {

                        selezione_operatore("CAMBIO LISTINO");
                        gestisci_cambio_listino(event, id);
                    });
                    //});

                }
                comanda.sincro.query_cassa();
            } else {
                bootbox.alert("mancata compilazione di uno o più dei campi obbligatori: descrizione, spazio forno, prezzo default, ordinamento");
            }
        } else {
            bootbox.alert("inserire una data corretta");
        }
    });
}

/*function salva_cambio_listino(id) {
 
 var testo_update_prodotti = "UPDATE prodotti SET ";
 var virgola = false;
 var prezzo_1 = $('#form_cambio_listino input[name="prezzo_vecchio_1"]:visible').val().replace(',', '.');
 if (!isNaN(parseFloat(prezzo_1))) {
 testo_update_prodotti += "prezzo_1='" + parseFloat(prezzo_1).toFixed(2) + "'";
 virgola = true;
 }
 
 var prezzo_2 = $('#form_cambio_listino input[name="prezzo_vecchio_2"]:visible').val().replace(',', '.');
 if (!isNaN(parseFloat(prezzo_2))) {
 if (virgola === true) {
 testo_update_prodotti += ",";
 }
 testo_update_prodotti += "prezzo_2='" + parseFloat(prezzo_2).toFixed(2) + "'";
 virgola = true;
 }
 
 var prezzo_3 = $('#form_cambio_listino input[name="prezzo_vecchio_3"]:visible').val().replace(',', '.');
 if (!isNaN(parseFloat(prezzo_3))) {
 if (virgola === true) {
 testo_update_prodotti += ",";
 }
 testo_update_prodotti += "prezzo_3='" + parseFloat(prezzo_3).toFixed(2) + "'";
 virgola = true;
 }
 
 var prezzo_4 = $('#form_cambio_listino input[name="prezzo_vecchio_4"]:visible').val().replace(',', '.');
 if (!isNaN(parseFloat(prezzo_4))) {
 if (virgola === true) {
 testo_update_prodotti += ",";
 }
 testo_update_prodotti += "prezzo_4='" + parseFloat(prezzo_4).toFixed(2) + "'";
 }
 
 testo_update_prodotti += " WHERE categoria!='XXX' and id='" + id + "';";
 console.log("SALVA CAMBIO LISTINO", testo_update_prodotti);
 //QUESTA E' PER L'AGGIORNAMENTO DEI PREZZI CORRENTI
 comanda.sock.send({tipo: "aggiornamento_posizione_prodotti", operatore: comanda.operatore, query: testo_update_prodotti, terminale: comanda.terminale, ip: comanda.ip_address});
 comanda.sincro.query_cassa();
 comanda.sincro.query(testo_update_prodotti, function () {
 
 console.log("TESTO UPDATE PRODOTTI", testo_update_prodotti);
 var virgola = false;
 var testo_update_listino = "";
 var testo_insert_listino = "";
 var testo_delete_listino = "";
 var giorno = $('#form_cambio_listino input[name="giorno"]:visible').val();
 testo_update_listino += "giorno='" + giorno + "',";
 virgola = true;
 testo_insert_listino += "'" + giorno + "'";
 var mese = $('#form_cambio_listino input[name="mese"]:visible').val();
 testo_update_listino += "mese='" + mese + "',";
 virgola = true;
 testo_insert_listino += ",'" + mese + "'";
 var anno = $('#form_cambio_listino input[name="anno"]:visible').val();
 testo_update_listino += "anno='" + anno + "'";
 virgola = true;
 testo_insert_listino += ",'" + anno + "'";
 testo_insert_listino += ",'" + id + "'";
 var prezzo_futuro_1 = $('#form_cambio_listino input[name="prezzo_1"]:visible').val().replace(',', '.');
 if (!isNaN(parseFloat(prezzo_futuro_1))) {
 if (virgola === true) {
 testo_update_listino += ",";
 }
 testo_insert_listino += ",'" + parseFloat(prezzo_futuro_1).toFixed(2) + "'";
 testo_update_listino += "prezzo_1='" + parseFloat(prezzo_futuro_1).toFixed(2) + "'";
 virgola = true;
 } else
 {
 testo_insert_listino += ",''";
 }
 
 var prezzo_futuro_2 = $('#form_cambio_listino input[name="prezzo_2"]:visible').val().replace(',', '.');
 if (!isNaN(parseFloat(prezzo_futuro_2))) {
 if (virgola === true) {
 testo_update_listino += ",";
 }
 testo_insert_listino += ",'" + parseFloat(prezzo_futuro_2).toFixed(2) + "'";
 testo_update_listino += "prezzo_2='" + parseFloat(prezzo_futuro_2).toFixed(2) + "'";
 virgola = true;
 } else
 {
 testo_insert_listino += ",''";
 }
 
 var prezzo_futuro_3 = $('#form_cambio_listino input[name="prezzo_3"]:visible').val().replace(',', '.');
 if (!isNaN(parseFloat(prezzo_futuro_3))) {
 if (virgola === true) {
 testo_update_listino += ",";
 }
 testo_insert_listino += ",'" + parseFloat(prezzo_futuro_3).toFixed(2) + "'";
 testo_update_listino += "prezzo_3='" + parseFloat(prezzo_futuro_3).toFixed(2) + "'";
 virgola = true;
 } else
 {
 testo_insert_listino += ",''";
 }
 
 var prezzo_futuro_4 = $('#form_cambio_listino input[name="prezzo_4"]:visible').val().replace(',', '.');
 if (!isNaN(parseFloat(prezzo_futuro_4))) {
 if (virgola === true) {
 testo_update_listino += ",";
 }
 testo_insert_listino += ",'" + parseFloat(prezzo_futuro_4).toFixed(2) + "'";
 testo_update_listino += "prezzo_4='" + parseFloat(prezzo_futuro_4).toFixed(2) + "'";
 virgola = true;
 } else
 {
 testo_insert_listino += ",''";
 }
 
 
 var query_verifica_cambio = "select * from cambi_listino where id_prodotto='" + id + "';";
 comanda.sincro.query(query_verifica_cambio, function (verif) {
 
 if (verif[0] !== undefined && giorno.length === 2 && mese.length === 2 && anno.length === 4 && (!isNaN(parseFloat(prezzo_futuro_1))) || !isNaN(parseFloat(prezzo_futuro_2)) || !isNaN(parseFloat(prezzo_futuro_3)) || !isNaN(parseFloat(prezzo_futuro_4))) {
 
 testo_update_listino = "UPDATE cambi_listino SET " + testo_update_listino + " WHERE id_prodotto='" + id + "';";
 console.log("SALVA CAMBIO LISTINO", testo_update_listino);
 comanda.sock.send({tipo: "aggiornamento_posizione_prodotti", operatore: comanda.operatore, query: testo_update_listino, terminale: comanda.terminale, ip: comanda.ip_address});
 comanda.sincro.query(testo_update_listino, function () {
 
 selezione_operatore("CAMBIO LISTINO");
 });
 comanda.sincro.query_cassa();
 } else if (giorno.length === 2 && mese.length === 2 && anno.length === 4 && (!isNaN(parseFloat(prezzo_futuro_1))) || !isNaN(parseFloat(prezzo_futuro_2)) || !isNaN(parseFloat(prezzo_futuro_3)) || !isNaN(parseFloat(prezzo_futuro_4)))
 {
 
 testo_insert_listino = "INSERT INTO cambi_listino (giorno,mese,anno,id_prodotto,prezzo_1,prezzo_2,prezzo_3,prezzo_4) VALUES ( " + testo_insert_listino + " );";
 console.log("SALVA CAMBIO LISTINO", testo_insert_listino);
 comanda.sock.send({tipo: "aggiornamento_posizione_prodotti", operatore: comanda.operatore, query: testo_insert_listino, terminale: comanda.terminale, ip: comanda.ip_address});
 comanda.sincro.query(testo_insert_listino, function () {
 
 selezione_operatore("CAMBIO LISTINO");
 });
 comanda.sincro.query_cassa();
 } else if ((giorno.length === 0 || mese.length === 0 || anno.length === 0) || (isNaN(parseFloat(prezzo_futuro_1))) && isNaN(parseFloat(prezzo_futuro_2)) || isNaN(parseFloat(prezzo_futuro_3)) || isNaN(parseFloat(prezzo_futuro_4)))
 {
 
 testo_delete_listino = "DELETE FROM cambi_listino WHERE id_prodotto='" + id + "';";
 console.log("SALVA CAMBIO LISTINO", testo_delete_listino);
 comanda.sock.send({tipo: "aggiornamento_posizione_prodotti", operatore: comanda.operatore, query: testo_delete_listino, terminale: comanda.terminale, ip: comanda.ip_address});
 comanda.sincro.query(testo_delete_listino, function () {
 
 selezione_operatore("CAMBIO LISTINO");
 });
 comanda.sincro.query_cassa();
 }
 });
 });
 }*/




function salva_listino_clienti(id) {
    var descrizione = $('#form_gestione_cambi_listino input[name="descrizione"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var prezzo = $('#form_gestione_cambi_listino input[name="prezzo_1"]:visible').val();
    var categoria = $('#form_gestione_cambi_listino select[name="categoria"]:visible').val();
    var dest_stampa = $('#form_gestione_cambi_listino select[name="dest_stampa"]:visible').val();
    var portata = $('#form_gestione_cambi_listino select[name="portata"]:visible').val();
    var posizione = $('#form_gestione_cambi_listino input[name="posizione"]:visible').val();
    var pagina = $('#form_gestione_cambi_listino input[name="pagina"]:visible').val();
    var cat_var = $('#form_gestione_cambi_listino select[name="cat_var"]:visible').val();
    var cat_var_auto = $('#form_gestione_cambi_listino select[name="cat_var_auto"]:visible').val();
    var iva_base = $('#form_gestione_cambi_listino input[name="perc_iva_base"]:visible').val();
    var ordinamento = $('#form_gestione_cambi_listino input[name="ordinamento"]:visible').val();
    var ricetta = $('#form_gestione_cambi_listino input[name="ricetta"]:visible').val();
    var iva_takeaway = $('#form_gestione_cambi_listino input[name="perc_iva_takeaway"]:visible').val();
    var testo_query = "UPDATE listino_clienti SET ricetta='" + ricetta + "',ordinamento='" + ordinamento + "',descrizione='" + descrizione.toUpperCase() + "',prezzo_1='" + parseFloat(prezzo.replace(/,/g, ".")).toFixed(2) + "',categoria='" + categoria + "',dest_st_1='" + dest_stampa + "',portata='" + portata + "',posizione='" + posizione + "',pagina='" + pagina + "',cat_varianti='" + cat_var + "/" + cat_var_auto + "',perc_iva_base='" + iva_base + "',perc_iva_takeaway='" + iva_takeaway + "' WHERE id='" + id + "';";
    console.log("QUERY PRODOTTO", testo_query);
    //comanda.array_dbcentrale_mancanti.push(testo_query);
    comanda.sock.send({
        tipo: "aggiornamento_posizione_listino_clienti",
        operatore: comanda.operatore,
        query: testo_query,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    comanda.sincro.query(testo_query, function () {

        selezione_operatore("LISTINO CLIENTI");
    });
    comanda.sincro.query_cassa();
}

var password_profilo_aziendale_salvata = "";

function password_cambio_licenza() {

    if (password_profilo_aziendale_salvata !== "ae5873da1b195af6feb68318ff42c1be") {
        var prompt = bootbox.prompt({
            size: "small",
            title: comanda.lang[106],
            inputType: "password",
            callback: function (result) {

                if (CryptoJS.MD5(result).toString() === "ae5873da1b195af6feb68318ff42c1be") {
                    $('[name="numero_licenza"').removeAttr('readonly');
                    password_profilo_aziendale_salvata = CryptoJS.MD5(result).toString();
                } else {
                    selezione_dispositivo();
                }

            }
        });
        prompt.init(function () {

            //$("input.bootbox-input").addClass("kb_num");
            $("input.bootbox-input").focus(function () {
                if (comanda.mobile === true) {
                    $(this).blur();
                }
            });
            $("input.bootbox-input").trigger("touchend");
        });
    }
}

function salva_profilo_aziendale() {

    $('[name="numero_licenza"').attr('readonly', 'readonly');
    password_profilo_aziendale_salvata = "";
    var ragione_sociale = $('#tabella_profilo input[name="ragione_sociale"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var numero_licenza = $('#tabella_profilo input[name="numero_licenza"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var nome_locale = $('#tabella_profilo input[name="nome_locale"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var indirizzo = $('#tabella_profilo input[name="indirizzo"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var numero = $('#tabella_profilo input[name="numero"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var cap = $('#tabella_profilo input[name="cap"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var citta = $('#tabella_profilo input[name="citta"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var provincia = $('#tabella_profilo input[name="provincia"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var paese = $('#tabella_profilo input[name="paese"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var cellulare = $('#tabella_profilo input[name="cellulare"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var telefono = $('#tabella_profilo input[name="telefono"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var fax = $('#tabella_profilo input[name="fax"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var email = $('#tabella_profilo input[name="email"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var codice_fiscale = $('#tabella_profilo input[name="codice_fiscale"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var partita_iva = $('#tabella_profilo input[name="partita_iva"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var nome_pc = $('#tabella_profilo input[name="nome_pc"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var id_terminale = $('#tabella_profilo input[name="id_terminale"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var comune = $('#tabella_profilo input[name="comune"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var id_nazione = $('#tabella_profilo input[name="id_nazione"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var nazione = $('#tabella_profilo input[name="nazione"]:visible').val().replace(/'/gi, '').replace(/"/gi, '');
    var testo_query = "UPDATE profilo_aziendale SET numero_licenza='" + numero_licenza + "',comune='" + comune + "',id_nazione='" + id_nazione + "',nazione='" + nazione + "',ragione_sociale='" + ragione_sociale + "',nome_locale='" + nome_locale + "',indirizzo='" + indirizzo + "',numero='" + numero + "',cap='" + cap + "',citta='" + citta + "',provincia='" + provincia + "',paese='" + paese + "',cellulare='" + cellulare + "',telefono='" + telefono + "',fax='" + fax + "',email='" + email + "',codice_fiscale='" + codice_fiscale + "',partita_iva='" + partita_iva + "' ;";
    comanda.locale = $('#tabella_profilo input[name="nome_locale"]:visible').val();
    //comanda.array_dbcentrale_mancanti.push(testo_query);
    comanda.sock.send({
        tipo: "aggiornamento_profilo_aziendale",
        operatore: comanda.operatore,
        query: testo_query,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    comanda.sincro.query(testo_query, function () {
        testo_query = "update settaggi SET valore='" + id_terminale + "' WHERE nome='id_terminale';";
        comanda.sincro.query_locale(testo_query, function () {
            testo_query = "update settaggi SET valore='" + nome_pc + "' WHERE nome='nome_pc';";
            comanda.sincro.query_locale(testo_query, function () {
                importa_profilo_aziendale_ram(function () {
                    selezione_operatore("PROFILO AZIENDALE");
                });
            });
        });
    });
    comanda.sincro.query_cassa();
}

function salva_articolo() {
    var form = $('#modifica_articolo form');
    comanda.funzionidb.salva_articolo(form.serialize(), function () {
        elenco_prodotti();
    });
    $.ajax({
        beforeSend: function () {
            $("body").addClass("loading");
        },
        complete: function () {
            $("body").removeClass("loading");
        },
        type: "POST",
        url: comanda.url_server + 'classi_php/cassa_ajax_' + comanda.lingua_stampa + '.lib.php',
        data: {
            form: form.serialize(),
            action: "salva_articolo"
        },
        success: function (response) {
            $('#modifica_articolo').modal('hide');
        }
    });
    //COSTRUTTORE_AJAX TEMP DISABLED
    //comanda.costruttore_ajax();
}

function salva_mod_tab(quale, id) {
    ////console.log('[name^="tab_' + quale + '[' + id + ']"]');
    var form = $('[name^="tab_' + quale + '[' + id + ']"]');
    ////console.log(form.serialize());
    $.ajax({
        type: "POST",
        url: comanda.url_server + 'classi_php/cassa_ajax_' + comanda.lingua_stampa + '.lib.php',
        data: {
            quale: quale,
            form: form.serialize(),
            id: id,
            action: "salva_mod_tab"
        },
        success: function () {
            elenco_categorie("list", "1", "0");
            elenco_categorie("select", "1", "1");
            //elenco_sconti();
        }
    });
}

function salva_buoni_sconti() {

    var array = new Array();
    var i = 1;
    $('#tabella_sconti_buoni input,select:visible').each(function (index) {

        if (array.length === 3) {
            array = [];
        }

        array.push($(this).val());
        if (array.length === 3) {

            if (array[0].length > 0 && array[1].length > 0) {
                var testo_query = "update tabella_sconti_buoni set descrizione='" + array[0].toUpperCase().replace(/'/gi, '').replace(/"/gi, '') + "',percentuale='" + array[1] + "',tipo='" + array[2] + "' WHERE id=" + ((i / 3) - 1) + ";";
                console.log(testo_query);
                comanda.sock.send({
                    tipo: "aggiornamento_sconti",
                    operatore: comanda.operatore,
                    query: testo_query,
                    terminale: comanda.terminale,
                    ip: comanda.ip_address
                });
                comanda.sincro.query(testo_query, function (result) { });
            } else {
                var testo_query = "update tabella_sconti_buoni set descrizione='',percentuale='',tipo='" + array[2] + "' WHERE id=" + ((i / 3) - 1) + ";";
                console.log(testo_query);
                comanda.sock.send({
                    tipo: "aggiornamento_sconti",
                    operatore: comanda.operatore,
                    query: testo_query,
                    terminale: comanda.terminale,
                    ip: comanda.ip_address
                });
                comanda.sincro.query(testo_query, function (result) { });
            }
        }
        i++;
    });

}

function salva_specifiche() {

    var array = new Array();
    var i = 1;
    $('#tabella_specifiche input,select:visible').each(function (index) {

        if (comanda.pizzeria_asporto === true && array.length === 4) {
            array = [];
        } else if (comanda.pizzeria_asporto !== true && array.length === 3) {
            array = [];
        }

        array.push($(this).val());
        if (comanda.pizzeria_asporto === true && array.length === 4) {
            var testo_query = "update specifiche set descrizione='" + array[0].toUpperCase().trim().replace(/'/gi, '').replace(/"/gi, '') + "',prezzo='" + array[1] + "',spazio_forno='" + array[2] + "',colore='" + array[3] + "' WHERE id='" + aggZero(((i / 4) - 1), 3) + "';";
            console.log("SALVA SPECIFICHE", testo_query);
            comanda.sock.send({
                tipo: "aggiornamento_specifiche",
                operatore: comanda.operatore,
                query: testo_query,
                terminale: comanda.terminale,
                ip: comanda.ip_address
            });
            comanda.sincro.query(testo_query, function () { });
        } else if (comanda.pizzeria_asporto !== true && array.length === 3) {
            var testo_query = "update specifiche set descrizione='" + array[0].toUpperCase().trim().replace(/'/gi, '').replace(/"/gi, '') + "',prezzo='" + array[1] + "',colore='" + array[2] + "' WHERE id='" + aggZero(((i / 3) - 1), 3) + "';";
            console.log("SALVA SPECIFICHE", testo_query);
            comanda.sock.send({
                tipo: "aggiornamento_specifiche",
                operatore: comanda.operatore,
                query: testo_query,
                terminale: comanda.terminale,
                ip: comanda.ip_address
            });
            comanda.sincro.query(testo_query, function () { });
        }


        i++;
    });

}



function nuovo_circuito() {

    var testo_query = "select id from circuiti order by cast(id as int) desc limit 1;";
    comanda.sincro.query(testo_query, function (risultato) {

        var id = 0;
        if (risultato[0] !== undefined && risultato[0].id !== undefined) {
            id = parseInt(risultato[0].id) + 1;
        }
        testo_query = "insert into circuiti (id,ragione_sociale) VALUES (" + id + ",'.Nuovo Circuito');";
        comanda.sock.send({
            tipo: "nuovo_circuito",
            operatore: comanda.operatore,
            query: testo_query,
            terminale: comanda.terminale,
            ip: comanda.ip_address
        });
        //comanda.array_dbcentrale_mancanti.push(testo_query);
        comanda.sincro.query(testo_query, function () {

            selezione_operatore("GESTIONE CIRCUITI");
            scegli_default();
        });

    });
}

function nuova_categoria(tipo) {

    var testo_query = "select id from categorie order by cast(id as int) desc limit 1;";
    comanda.sincro.query(testo_query, function (risultato) {

        var id = 0;
        if (risultato[0] !== undefined && risultato[0].id !== undefined) {
            id = parseInt(risultato[0].id) + 1;
        }

        var nome_nuova_cat = '.NUOVA CATEGORIA';
        if (tipo === "VARIANTE") {
            nome_nuova_cat = 'V.NUOVA CATEGORIA';
        }

        testo_query = "insert into categorie VALUES ('" + id + "','','','" + nome_nuova_cat + "','','','','','','','','" + comanda.percentuale_iva_default + "','','','','','','','','','','','','','','','','','','','');";
        comanda.sock.send({
            tipo: "nuova_categoria",
            operatore: comanda.operatore,
            query: testo_query,
            terminale: comanda.terminale,
            ip: comanda.ip_address
        });
        //comanda.array_dbcentrale_mancanti.push(testo_query);
        alasql(testo_query);
        selezione_operatore("GESTIONE CATEGORIE");
        if (tipo === "VARIANTE") {
            scegli_default("V");
        } else {
            scegli_default();
        }


    });
}

function nuova_portata() {

    var testo_query = "select id from nomi_portate order by cast(id as int) desc limit 1;";
    comanda.sincro.query(testo_query, function (risultato) {

        var id = 0;
        if (risultato[0] !== undefined && risultato[0].id !== undefined) {
            id = parseInt(risultato[0].id) + 1;
        }

        var nome_nuova_portata = '.NUOVA PORTATA';
        testo_query = "insert into nomi_portate (id," + comanda.lingua_stampa + ",ord_fastorder,consegna_abilitata) VALUES ('" + id + "','" + nome_nuova_portata + "','','');";
        comanda.sock.send({
            tipo: "nuova_portata",
            operatore: comanda.operatore,
            query: testo_query,
            terminale: comanda.terminale,
            ip: comanda.ip_address
        });
        //comanda.array_dbcentrale_mancanti.push(testo_query);
        comanda.sincro.query(testo_query, function () {

            selezione_operatore("PORTATE");
            scegli_default();
        });

    });
}


function nuovo_gruppo_statistico() {

    var testo_query = "select id from gruppi_statistici order by cast(id as int) desc limit 1;";
    comanda.sincro.query(testo_query, function (risultato) {

        var id = 0;
        if (risultato[0] !== undefined && risultato[0].id !== undefined) {
            id = parseInt(risultato[0].id) + 1;
        }

        var nome_nuovo_gruppo_statistico = '.NUOVO GRUPPO STATISTICO';
        testo_query = "insert into gruppi_statistici (id,nome) VALUES ('" + id + "','" + nome_nuovo_gruppo_statistico + "');";
        comanda.sock.send({
            tipo: "nuovo_gruppo_statistico",
            operatore: comanda.operatore,
            query: testo_query,
            terminale: comanda.terminale,
            ip: comanda.ip_address
        });
        //comanda.array_dbcentrale_mancanti.push(testo_query);
        comanda.sincro.query(testo_query, function () {

            selezione_operatore("gruppi_statistici");
            scegli_default();
        });

    });
}

function nuovo_operatore() {

    var testo_query = "select id from operatori order by cast(id as int) desc limit 1;";
    comanda.sincro.query(testo_query, function (risultato) {

        var id = 0;
        if (risultato[0] !== undefined && risultato[0].id !== undefined) {
            id = parseInt(risultato[0].id) + 1;
        }

        var nome = '.NUOVO_OPERATORE';
        testo_query = "insert into operatori (id,permesso,nome,password,p_statistiche,p_gestioni,p_layout,p_backoffice,p_chiusura,p_mappa_tavoli,p_gestione_clienti,p_mappa_tavoli_phone) VALUES (" + id + ",'1','" + nome + "','d41d8cd98f00b204e9800998ecf8427e','','','','','','','','');";
        comanda.sock.send({
            tipo: "nuovo_operatore",
            operatore: comanda.operatore,
            query: testo_query,
            terminale: comanda.terminale,
            ip: comanda.ip_address
        });
        //comanda.array_dbcentrale_mancanti.push(testo_query);
        comanda.sincro.query(testo_query, function () {

            selezione_operatore("GESTIONE OPERATORI");
            scegli_default();
        });

    });
}

function nuovo_pony() {

    var testo_query = "select id from pony_express order by cast(id as int) desc limit 1;";
    comanda.sincro.query(testo_query, function (risultato) {

        var id = 0;
        if (risultato[0] !== undefined && risultato[0].id !== undefined) {
            id = parseInt(risultato[0].id) + 1;
        }

        var nome = '.NUOVO_PONY';
        testo_query = "insert into pony_express (id,nome) VALUES (" + id + ",'" + nome + "');";
        comanda.sock.send({
            tipo: "nuovo_pony",
            operatore: comanda.operatore,
            query: testo_query,
            terminale: comanda.terminale,
            ip: comanda.ip_address
        });
        //comanda.array_dbcentrale_mancanti.push(testo_query);
        comanda.sincro.query(testo_query, function () {

            selezione_operatore("GESTIONE PONY");
            scegli_default();
        });

    });
}



function nuova_stampante() {

    var testo_query = "select id from nomi_stampanti order by cast(id as int) desc limit 1;";
    comanda.sincro.query(testo_query, function (risultato) {

        var id = 0;
        if (risultato[0] !== undefined && risultato[0].id !== undefined) {
            id = parseInt(risultato[0].id) + 1;
        }

        var nome_nuova_stampante = '.NUOVA STAMPANTE';
        testo_query = "insert into nomi_stampanti (id,numero,nome) VALUES ('" + id + "','" + id + "','" + nome_nuova_stampante + "');";
        comanda.sock.send({
            tipo: "nuova_stampante",
            operatore: comanda.operatore,
            query: testo_query,
            terminale: comanda.terminale,
            ip: comanda.ip_address
        });
        //comanda.array_dbcentrale_mancanti.push(testo_query);
        comanda.sincro.query(testo_query, function () {

            selezione_operatore("GESTIONE STAMPANTI");
            scegli_default();
        });

    });
}

function nuovo_prodotto() {

    var testo_query = "select id from prodotti order by cast(id as int) desc limit 1;";
    comanda.sincro.query(testo_query, function (risultato) {
        console.log("SELECT PRODOTTO", testo_query);
        testo_query = "select id,perc_iva,dest_stampa,cat_var,portata from categorie  where id='" + comanda.categoria + "' limit 1;";
        comanda.sincro.query(testo_query, function (risultato_cat) {
            console.log("SELECT CATEGORIA", testo_query);
            var id = 0;
            if (risultato[0] !== undefined && risultato[0].id !== undefined) {
                id = parseInt(risultato[0].id) + 1;
            }
            var nome_nuova_cat = '.NUOVO PRODOTTO';
            //risultato_cat[0].cat_var + "','" + risultato_cat[0].portata + "','" + risultato_cat[0].dest_stampa
            //risultato_cat[0].perc_iva + "','" + risultato_cat[0].perc_iva

            testo_query = "insert into prodotti (colore_tasto,id,ordinamento,descrizione,categoria,\n\
                cat_varianti,portata,dest_st_1,prezzo_1,posizione,posizione_1,posizione_2,posizione_3,\n\
                posizione_4,perc_iva_base,perc_iva_takeaway,prezzo_fattorino1_norm,listino_bar,listino_asporto,\n\
                prezzo_fattorino1_maxi,listino_continuo,prezzo_2,costo,prezzo_maxi_prima,prezzo_varianti_aggiuntive,prezzo_varianti_maxi,prezzo_4,prezzo_variante_meno) \n\
                VALUES\n\
                 ('linen','" + id + "','999','" + nome_nuova_cat + "','" + comanda.categoria + "','" + risultato_cat[0].cat_var + "',\n\
                 '" + risultato_cat[0].portata + "','" + risultato_cat[0].dest_stampa + "','0.00','999','999','999','999','999',\n\
                 '" + risultato_cat[0].perc_iva + "','" + risultato_cat[0].perc_iva + "','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00','0.00');";
            console.log("QUERY_PRODOTTO", testo_query);
            comanda.sock.send({
                tipo: "nuovo_prodotto",
                operatore: comanda.operatore,
                query: testo_query,
                terminale: comanda.terminale,
                ip: comanda.ip_address
            });
            //comanda.array_dbcentrale_mancanti.push(testo_query);
            comanda.sincro.query(testo_query, function () {

                selezione_operatore("GESTIONE PRODOTTI");
                scegli_default();
            });

        });
    });
}

function nuovo_cliente() {

    var testo_query = "select id from clienti WHERE id<3000000 order by cast(id as int) desc limit 1;";

    comanda.sincro.query(testo_query, function (risultato) {

        var id = 0;
        if (risultato[0] !== undefined && risultato[0].id !== undefined) {
            id = parseInt(risultato[0].id) + 1;
        }
        testo_query = "insert into clienti (id,ragione_sociale,iva_non_pagati,tipo_cliente,partita_iva,partita_iva_estera,codice_fiscale,cellulare,telefono_3,telefono_4,telefono_5,telefono_6) VALUES (" + id + ",'.Nuovo Cliente','10','ap_azienda','','','','','','','','');";
        comanda.sock.send({
            tipo: "nuovo_cliente",
            operatore: comanda.operatore,
            query: testo_query,
            terminale: comanda.terminale,
            ip: comanda.ip_address
        });
        //comanda.array_dbcentrale_mancanti.push(testo_query);
        comanda.sincro.query(testo_query, function () {

            selezione_operatore("GESTIONE CLIENTI");
            scegli_default();

        });

    });
}


function elimina_portata(id) {
    var testo_query = "DELETE FROM nomi_portate WHERE id='" + id + "';";
    //comanda.array_dbcentrale_mancanti.push(testo_query);
    comanda.sock.send({
        tipo: "eliminazione_portata",
        operatore: comanda.operatore,
        query: testo_query,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    comanda.sincro.query(testo_query, function () {

        selezione_operatore("PORTATE");
        scegli_default();
    });

}

function elimina_gruppo_statistico(id) {
    var testo_query = "DELETE FROM gruppi_statistici WHERE id='" + id + "';";
    //comanda.array_dbcentrale_mancanti.push(testo_query);
    comanda.sock.send({
        tipo: "eliminazione_gruppo_statistico",
        operatore: comanda.operatore,
        query: testo_query,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    comanda.sincro.query(testo_query, function () {

        selezione_operatore("gruppi_statistici");
        scegli_default();
    });

}

function elimina_categoria(id, tipo) {

    var testo_query = "select categoria from prodotti where categoria='" + id + "'; ";
    comanda.sincro.query(testo_query, function (articoli_con_categoria) {

        if (articoli_con_categoria.length > 0) {
            alert("Non puoi cancellare la categoria, perchÃ¨ hai " + articoli_con_categoria.length + " articoli al suo interno.\n\nBisogna prima cancellarli, o spostarli di categoria.");
        } else {
            var testo_query = "DELETE FROM categorie WHERE id='" + id + "';";
            //comanda.array_dbcentrale_mancanti.push(testo_query);
            comanda.sock.send({
                tipo: "eliminazione_categorie",
                operatore: comanda.operatore,
                query: testo_query,
                terminale: comanda.terminale,
                ip: comanda.ip_address
            });
            comanda.sincro.query(testo_query, function () {

                selezione_operatore("GESTIONE CATEGORIE");
                if (tipo === "V") {
                    scegli_default("V");
                } else {
                    scegli_default();
                }

                elenco_categorie("list", "1", "0", "#tab_sx");
            });

        }
    });
}

function elimina_stampante(id) {
    var testo_query = "DELETE FROM nomi_stampanti WHERE id='" + id + "';";
    //comanda.array_dbcentrale_mancanti.push(testo_query);
    comanda.sock.send({
        tipo: "eliminazione_stampanti",
        operatore: comanda.operatore,
        query: testo_query,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    comanda.sincro.query(testo_query, function () {

        selezione_operatore("GESTIONE STAMPANTI");
        scegli_default();
    });

}

function elimina_operatore(id) {
    var testo_query = "DELETE FROM operatori WHERE id=" + id + ";";
    //comanda.array_dbcentrale_mancanti.push(testo_query);
    comanda.sock.send({
        tipo: "eliminazione_operatore",
        operatore: comanda.operatore,
        query: testo_query,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    comanda.sincro.query(testo_query, function () {

        selezione_operatore("GESTIONE OPERATORI");
        scegli_default();
    });

}

function elimina_pony(id) {
    var testo_query = "DELETE FROM pony_express WHERE id=" + id + ";";
    //comanda.array_dbcentrale_mancanti.push(testo_query);
    comanda.sock.send({
        tipo: "eliminazione_pony",
        operatore: comanda.operatore,
        query: testo_query,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    comanda.sincro.query(testo_query, function () {

        selezione_operatore("GESTIONE PONY");
        scegli_default();
    });

}

function elimina_prodotto(id) {

    var query = alasql("select descrizione from prodotti WHERE  id='" + id + "'");
    bootbox.confirm("Sei sicuro di voler cancellare il prodotto  " + query[0].descrizione, (answer) => {

        if (answer === true) {

            var data_modifica = new Date().format("yyyymmddHHMMss");
            var testo_query = "UPDATE prodotti SET categoria='XXX',ult_mod='" + data_modifica + "' WHERE  id='" + id + "';";
            comanda.sock.send({
                tipo: "eliminazione_prodotti",
                operatore: comanda.operatore,
                query: testo_query,
                terminale: comanda.terminale,
                ip: comanda.ip_address
            });
            comanda.sincro.query(testo_query, function () {

                selezione_operatore("GESTIONE PRODOTTI");
                scegli_default();
            });

        }

    });


}

function elimina_circuito(id) {
    var testo_query = "DELETE FROM circuiti WHERE id=" + id + ";";
    //comanda.array_dbcentrale_mancanti.push(testo_query);
    comanda.sock.send({
        tipo: "eliminazione_circuiti",
        operatore: comanda.operatore,
        query: testo_query,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    comanda.sincro.query(testo_query, function () {

        selezione_operatore("GESTIONE CIRCUITI");
        scegli_default();
    });

}



function elimina_cliente(id) {
    var testo_query = "DELETE FROM clienti WHERE id=" + id + ";";
    //comanda.array_dbcentrale_mancanti.push(testo_query);
    comanda.sock.send({
        tipo: "eliminazione_clienti",
        operatore: comanda.operatore,
        query: testo_query,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    comanda.sincro.query(testo_query, function () {

        selezione_operatore("GESTIONE CLIENTI");
        scegli_default();
    });

}


function importa_profilo_aziendale_ram(callback) {
    if (comanda.mobile !== true) {
        comanda.sincro.query_locale("select valore from settaggi where nome='nome_pc';", function (rl) {
            comanda.sincro.query("select * from profilo_aziendale where id=1;", function (profilo_aziendale) {

                comanda.nome_pc = rl[0].valore;
                comanda.locale = profilo_aziendale[0].nome_locale;
                comanda.profilo_aziendale = new Object();
                comanda.profilo_aziendale.ragione_sociale = profilo_aziendale[0].ragione_sociale;
                comanda.profilo_aziendale.indirizzo = profilo_aziendale[0].indirizzo;
                comanda.profilo_aziendale.numero = profilo_aziendale[0].numero;
                comanda.profilo_aziendale.citta = profilo_aziendale[0].citta;
                comanda.profilo_aziendale.cap = profilo_aziendale[0].cap;
                comanda.profilo_aziendale.provincia = profilo_aziendale[0].provincia;
                comanda.profilo_aziendale.paese = profilo_aziendale[0].paese;
                comanda.profilo_aziendale.telefono = profilo_aziendale[0].telefono;
                comanda.profilo_aziendale.partita_iva = profilo_aziendale[0].partita_iva;
                comanda.profilo_aziendale.codice_fiscale = profilo_aziendale[0].codice_fiscale;
                comanda.profilo_aziendale.email = profilo_aziendale[0].email;
                comanda.profilo_aziendale.comune = profilo_aziendale[0].comune;
                comanda.profilo_aziendale.id_nazione = profilo_aziendale[0].id_nazione;
                comanda.profilo_aziendale.nazione = profilo_aziendale[0].nazione;
                comanda.numero_licenza_cliente = profilo_aziendale[0].numero_licenza;
                comanda.centro = profilo_aziendale[0].centro;
                callback(true);
            });
        });
    } else {
        comanda.sincro.query("select * from profilo_aziendale where id=1;", function (profilo_aziendale) {

            comanda.nome_pc = "SMARTPHONE";
            comanda.locale = profilo_aziendale[0].nome_locale;
            comanda.profilo_aziendale = new Object();
            comanda.profilo_aziendale.ragione_sociale = profilo_aziendale[0].ragione_sociale;
            comanda.profilo_aziendale.indirizzo = profilo_aziendale[0].indirizzo;
            comanda.profilo_aziendale.numero = profilo_aziendale[0].numero;
            comanda.profilo_aziendale.citta = profilo_aziendale[0].citta;
            comanda.profilo_aziendale.cap = profilo_aziendale[0].cap;
            comanda.profilo_aziendale.provincia = profilo_aziendale[0].provincia;
            comanda.profilo_aziendale.paese = profilo_aziendale[0].paese;
            comanda.profilo_aziendale.telefono = profilo_aziendale[0].telefono;
            comanda.profilo_aziendale.partita_iva = profilo_aziendale[0].partita_iva;
            comanda.profilo_aziendale.codice_fiscale = profilo_aziendale[0].codice_fiscale;
            comanda.profilo_aziendale.email = profilo_aziendale[0].email;
            comanda.profilo_aziendale.comune = profilo_aziendale[0].comune;
            comanda.profilo_aziendale.id_nazione = profilo_aziendale[0].id_nazione;
            comanda.profilo_aziendale.nazione = profilo_aziendale[0].nazione;
            comanda.numero_licenza_cliente = profilo_aziendale[0].numero_licenza;
            comanda.centro = profilo_aziendale[0].centro;
            callback(true);
        });
    }

}