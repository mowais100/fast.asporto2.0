/****/

/* global epson, comanda, bootbox, ATECO_view, ATECO_model, ATECO_controller, IVA_controller */



function REPARTO_model() {

    let misuratore_presente = false;

    /* Metodo Salva */

    this.salva_dato_singolo = async function (p, p2, p3, p4, i) {

        return new Promise(function (resolve, reject) {

            var xml = '<printerCommand><directIO  command="4002" data="' + aggZero(i, 2) + aggSpaziDopo('REPARTO ' + i, 20) + '' + '' + aggZero("", 28) + aggZero(p[i], 2) + '' + aggZero("", 15) + '' + p2[i] + '' + aggZero(p3[i], 2) + '' + aggZero(p4[i], 2) + '" /></printerCommand>';
            var epos = new epson.fiscalPrint();
            epos.send("http://" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=local_printer&timeout=10000", xml, 10000);
            epos.onreceive = function (result, tag_names_array, add_info, res_add) {
                resolve(true);
            };
            epos.onerror = function () {
                if (alert_aperto_misuratore === false) {
                    alert_aperto_misuratore = true;
                    bootbox.alert("Problema nella lettura dei reparti dal misuratore RT.<br><br>NON LAVORARE SE DEVI STAMPARE SCONTRINI O FATTURE!<br><br>RIAVVIA IL MISURATORE, ATTENDI UN MINUTO, E POI RIAVVIA IL SOFTWARE.<br><br>Non preoccuparti, sono cose di routine.");
                    resolve(false);
                } else {
                    resolve(false);
                }
            };

        });
    };


    this.salva_in_misuratore = async function (p, p2, p3, p4) {

        if (comanda.lingua_stampa === "italiano") {

            let data_length = p.length;

            for (let i = 1; i < data_length; i++) {
                await this.salva_dato_singolo(p, p2, p3, p4, i);
            }
        }
    };

    /* Costruttore classe Model */

    var self = this;

    this.listaREPARTO = new listaREPARTO();

    function listaREPARTO() {

        if (comanda.lingua_stampa === "italiano") {
            this.REPARTO_1 = "";
            this.REPARTO_2 = "";
            this.REPARTO_3 = "";
            this.REPARTO_4 = "";
            this.REPARTO_5 = "";
            this.REPARTO_6 = "";
            this.REPARTO_7 = "";
            this.REPARTO_8 = "";
            this.REPARTO_9 = "";

            this.REPARTO_1_SALES_TYPE = "";
            this.REPARTO_2_SALES_TYPE = "";
            this.REPARTO_3_SALES_TYPE = "";
            this.REPARTO_4_SALES_TYPE = "";
            this.REPARTO_5_SALES_TYPE = "";
            this.REPARTO_6_SALES_TYPE = "";
            this.REPARTO_7_SALES_TYPE = "";
            this.REPARTO_8_SALES_TYPE = "";
            this.REPARTO_9_SALES_TYPE = "";

            this.REPARTO_1_SALES_ATTRIBUTE = "";
            this.REPARTO_2_SALES_ATTRIBUTE = "";
            this.REPARTO_3_SALES_ATTRIBUTE = "";
            this.REPARTO_4_SALES_ATTRIBUTE = "";
            this.REPARTO_5_SALES_ATTRIBUTE = "";
            this.REPARTO_6_SALES_ATTRIBUTE = "";
            this.REPARTO_7_SALES_ATTRIBUTE = "";
            this.REPARTO_8_SALES_ATTRIBUTE = "";
            this.REPARTO_9_SALES_ATTRIBUTE = "";

            this.REPARTO_1_ATECO_INDEX = "";
            this.REPARTO_2_ATECO_INDEX = "";
            this.REPARTO_3_ATECO_INDEX = "";
            this.REPARTO_4_ATECO_INDEX = "";
            this.REPARTO_5_ATECO_INDEX = "";
            this.REPARTO_6_ATECO_INDEX = "";
            this.REPARTO_7_ATECO_INDEX = "";
            this.REPARTO_8_ATECO_INDEX = "";
            this.REPARTO_9_ATECO_INDEX = "";




        } else if (comanda.lingua_stampa === "deutsch") {
            this.REPARTO_1 = "";
            this.REPARTO_2 = "";
            this.REPARTO_3 = "";
            this.REPARTO_4 = "";
            this.REPARTO_5 = "";
            this.REPARTO_6 = "";
            this.REPARTO_7 = "";
            this.REPARTO_8 = "";
            this.REPARTO_9 = "";

            this.REPARTO_1_SALES_TYPE = "";
            this.REPARTO_2_SALES_TYPE = "";
            this.REPARTO_3_SALES_TYPE = "";
            this.REPARTO_4_SALES_TYPE = "";
            this.REPARTO_5_SALES_TYPE = "";
            this.REPARTO_6_SALES_TYPE = "";
            this.REPARTO_7_SALES_TYPE = "";
            this.REPARTO_8_SALES_TYPE = "";
            this.REPARTO_9_SALES_TYPE = "";

            this.REPARTO_1_SALES_ATTRIBUTE = "";
            this.REPARTO_2_SALES_ATTRIBUTE = "";
            this.REPARTO_3_SALES_ATTRIBUTE = "";
            this.REPARTO_4_SALES_ATTRIBUTE = "";
            this.REPARTO_5_SALES_ATTRIBUTE = "";
            this.REPARTO_6_SALES_ATTRIBUTE = "";
            this.REPARTO_7_SALES_ATTRIBUTE = "";
            this.REPARTO_8_SALES_ATTRIBUTE = "";
            this.REPARTO_9_SALES_ATTRIBUTE = "";

            this.REPARTO_1_ATECO_INDEX = "";
            this.REPARTO_2_ATECO_INDEX = "";
            this.REPARTO_3_ATECO_INDEX = "";
            this.REPARTO_4_ATECO_INDEX = "";
            this.REPARTO_5_ATECO_INDEX = "";
            this.REPARTO_6_ATECO_INDEX = "";
            this.REPARTO_7_ATECO_INDEX = "";
            this.REPARTO_8_ATECO_INDEX = "";
            this.REPARTO_9_ATECO_INDEX = "";


        }

    }


    function oggettoRispostaMisuratore(datoGrezzo) {

        this.rispostaGrezza = datoGrezzo.responseData;

        this.numero = this.rispostaGrezza.substr(0, 2);
        this.reparto = this.rispostaGrezza.substr(50, 2);
        this.sales_type = this.rispostaGrezza.substr(67, 1);
        this.sales_attribute = this.rispostaGrezza.substr(68, 2);
        this.ateco_index = this.rispostaGrezza.substr(70, 2);

    }


    this.init = async function () {

        if (comanda.lingua_stampa === "italiano") {

            if (comanda.compatibile_xml !== true) {

                let r = alasql("select * from settaggi_profili");

                if (comanda.pizzeria_asporto === true) {

                    if (r[0].Field317 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field318 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field319 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field321 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field421 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field364 === "true") {
                        misuratore_presente = true;
                    }

                    if (r[0].Field503 === "true") {
                        misuratore_presente = true;
                    }

                    if (r[0].Field504 === "true") {
                        misuratore_presente = true;
                    }

                    if (r[0].Field200 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field322 === "true") {
                        misuratore_presente = true;
                    }

                } else {

                    if (comanda.mobile === true) {

                        if (r[0].Field430 === "true") {
                            misuratore_presente = true;
                        }
                        if (r[0].Field431 === "true") {
                            misuratore_presente = true;
                        }
                        if (r[0].Field432 === "true") {
                            misuratore_presente = true;
                        }
                        if (r[0].Field433 === "true") {
                            misuratore_presente = true;
                        }

                    }

                    if (r[0].Field197 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field198 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field199 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field200 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field329 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field201 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field202 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field364 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field365 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field366 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field367 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field207 === "true") {
                        misuratore_presente = true;
                    }



                }
            }

            for (let i = 1; i <= 20; i++) {
                await this.richiedi_dato(i);
            }

        }
    };


    this.richiedi_dato = async function (i) {




        return new Promise(function (resolve, reject) {

            if (misuratore_presente === true) {

                var xml = '<printerCommand><directIO  command="4202" data="' + aggZero(i, 2) + '" /></printerCommand>';
                var epos = new epson.fiscalPrint();
                epos.send("http://" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=local_printer&timeout=10000", xml, 10000);
                epos.onreceive = function (result, tag_names_array, add_info, res_add) {
                    var ogg_risp = new oggettoRispostaMisuratore(add_info);
                    self.listaREPARTO["REPARTO_" + i] = ogg_risp.reparto;
                    self.listaREPARTO["REPARTO_" + i + "_SALES_TYPE"] = ogg_risp.sales_type;
                    self.listaREPARTO["REPARTO_" + i + "_SALES_ATTRIBUTE"] = ogg_risp.sales_attribute;
                    self.listaREPARTO["REPARTO_" + i + "_ATECO_INDEX"] = ogg_risp.ateco_index;
                    resolve(true);
                };
                epos.onerror = function () {
                    if (alert_aperto_misuratore === false) {
                        alert_aperto_misuratore = true;
                        bootbox.alert("Problema nella lettura dei reparti dal misuratore RT.<br><br>NON LAVORARE SE DEVI STAMPARE SCONTRINI O FATTURE!<br><br>RIAVVIA IL MISURATORE, ATTENDI UN MINUTO, E POI RIAVVIA IL SOFTWARE.<br><br>Non preoccuparti, sono cose di routine.");
                        resolve(false);
                    } else {
                        resolve(false);
                    }

                };

            } else {

                resolve(false);

            }

        });


    };

    //this.init();

    /*indice reparto
     substr(50,2)
     
     in base a indice reparto
     trovi l aliquota
     */

    /* Fine costruttore */

}



function REPARTO_view() {

    for (let i = 1; i <= 20; i++) {
        this["REPARTO_" + i] = document.getElementById("REPARTO_" + i);
        this["REPARTO_" + i + "_SALES_TYPE"] = document.getElementById("REPARTO_" + i + "_SALES_TYPE");
        this["REPARTO_" + i + "_SALES_ATTRIBUTE"] = document.getElementById("REPARTO_" + i + "_SALES_ATTRIBUTE");
        this["REPARTO_" + i + "_ATECO_INDEX"] = document.getElementById("REPARTO_" + i + "_ATECO_INDEX");
    }

}



function REPARTO_controller() {


    this.init = function () {

        /*TABELLE ACCESSORIE*/
        ATECO_controller.init();
        IVA_controller.init();

        for (let i = 1; i <= 20; i++) {

            $("#" + REPARTO_view["REPARTO_" + i].id + " option[value='" + REPARTO_model.listaREPARTO["REPARTO_" + i] + "']").prop("selected", true);

            REPARTO_view["REPARTO_" + i + "_SALES_TYPE"].value = REPARTO_model.listaREPARTO["REPARTO_" + i + "_SALES_TYPE"];
            REPARTO_view["REPARTO_" + i + "_SALES_ATTRIBUTE"].value = REPARTO_model.listaREPARTO["REPARTO_" + i + "_SALES_ATTRIBUTE"];
            REPARTO_view["REPARTO_" + i + "_ATECO_INDEX"].value = REPARTO_model.listaREPARTO["REPARTO_" + i + "_ATECO_INDEX"];

        }

    };

    this.salva = async function () {

        /* p significa parametro */

        let p = new Array();
        let p2 = new Array();
        let p3 = new Array();
        let p4 = new Array();


        for (let i = 1; i <= 20; i++) {
            p[i] = REPARTO_view["REPARTO_" + i].value;
            p2[i] = REPARTO_view["REPARTO_" + i + "_SALES_TYPE"].value;
            p3[i] = REPARTO_view["REPARTO_" + i + "_SALES_ATTRIBUTE"].value;
            p4[i] = REPARTO_view["REPARTO_" + i + "_ATECO_INDEX"].value;

        }

        await REPARTO_model.salva_in_misuratore(p, p2, p3, p4);

        REPARTO_controller.aggiorna();
    };

    this.aggiorna = async function (callback) {
        await REPARTO_model.init();
        REPARTO_controller.init();
        if (typeof callback === "function") {
            callback(true);
        }
    };

    this.indice = function (numero) {

        return REPARTO_model.listaREPARTO["REPARTO_" + numero];

    };

    this.iva_reparto = function (numero) {

        return IVA_model.listaIVA["IVA_" + numero];

    };

    this.tabella_reparti_IVA = function () {

        let tabella = '';

        for (let i = 1; i <= 20; i++) {

            tabella += '<option value="' + i + '">' + i + " - " + REPARTO_controller.iva_reparto(parseInt(REPARTO_controller.indice(i))) + '%</option>';

        }

        return tabella;

    };

}

/*Dichiaro le classi statiche*/
/* le variabili vengono inizializzate quando vedo gli ip delle stampanti su multilanguage.js */

var REPARTO_model;
var REPARTO_view;
var REPARTO_controller;