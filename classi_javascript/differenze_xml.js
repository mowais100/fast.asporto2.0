/* global parseFloat */

var d = new Date();
var Y = d.getFullYear().toString();
var D = addZero(d.getDate(), 2);
var M = addZero((d.getMonth() + 1), 2);
var data_inizio_servizio = D + '/' + M + '/' + Y;

var d = new Date();
var Y = d.getFullYear().toString();
var D = addZero(d.getDate() + 1, 2);
var M = addZero((d.getMonth() + 1), 2);
var data_inizio_servizio_giorno_dopo = D + '/' + M + '/' + Y;

if (comanda.licenza_vera !== "Culinaria") {
    for (var cont = 0; cont < 50; cont++) {
        history.replaceState(null, document.title, location.pathname + location.search);
        history.pushState(null, document.title, location.pathname + location.search);
    }
}

var inserimento_coperti_in_corso = false;
$(document).on('keyup change', '#numero_coperti', function () {
    var numero_coperti = $('#numero_coperti').val();

    salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + aggZero(new Date().getMilliseconds(), 3) + " # COPERTI BATTUTI # NUMERO: " + numero_coperti + " # ");


    if (numero_coperti !== "" && numero_coperti !== "0") {
        $("#tasto_batti_coperti").hide();
        $("#numero_coperti").show();
        if (comanda.compatibile_xml === true && $('#conto>tr[id^=art_]').length === 0 && inserimento_coperti_in_corso !== true) {
            inserimento_coperti_in_corso = true;
            comanda.funzionidb.aggiungi_articolo(null, "descrizione=MEMO INC,SCONTO,DRINK,LISTINO,COPERTI&prezzo_1=0&euro;&quantita=1&cod_articolo=IDCSL_DES", 1, "CONTO", "", null, undefined, undefined, undefined, function () {
                comanda.funzionidb.conto_attivo(function () {
                    inserimento_coperti_in_corso = false;
                });
            });
        } else {
            comanda.funzionidb.conto_attivo();
        }
    } else {
        $("#tasto_batti_coperti").show();
        $("#numero_coperti").hide();
        comanda.funzionidb.conto_attivo();
    }
});


function vedi_giacenze() {

    let query = "select DES,GIAC,GFLASH from ARTCON where GFLASH='S' or GFLASH='E';";

    comanda.sincro.query_sqlserver(query, function (dati) {

        const dataset = dati.map(d => {
            return {DES: d.DES, GIAC: parseFloat(d.GIAC), GFLASH: d.GFLASH};
        });

        let giacenze = "<table class='table table-striped'>";

        dataset.forEach(d => {
            giacenze += "<tr>";

            giacenze += "<td>";
            giacenze += d.DES;
            giacenze += "</td>";

            giacenze += "<td>";
            giacenze += d.GIAC;
            giacenze += "</td>";

            giacenze += "<td>";
            giacenze += d.GFLASH;
            giacenze += "</td>";

            giacenze += "</tr>";
        });

        giacenze += "</table>";

        $("#popup_giacenze .modal-body").html(giacenze);
        $("#popup_giacenze").modal("show");

    });

}

function preconto() {
    stampa_konto();
}

var oggetto_tavoli_presalvataggio = new Object();

var nome_pagina = location.pathname.substring(location.pathname.lastIndexOf("/") + 1);
//PER PC
if (nome_pagina.indexOf("tastiera") !== -1 && nome_pagina.indexOf("tavolo") !== -1) {

    var touch_servito = new Object();
    $(document).on('touchstart', '#conto tr', function (e) {
        var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
        touch_servito.startx = touch.pageX;
        touch_servito.starty = touch.pageY;
        var id_articolo = e.currentTarget.id;
    });
    $(document).on('touchend', '#conto tr', function (e) {
        var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
        var id_articolo = e.currentTarget.id;
        var numero_progressivo = id_articolo.replace('art_', '');
        touch_servito.endx = touch.pageX;
        touch_servito.endy = touch.pageY;
        if ((touch_servito.startx - touch_servito.endx > 150) && (touch_servito.starty - touch_servito.endy <= 10)) {
            try {
                var query = "select contiene_variante,nodo from comanda where ntav_comanda='" + comanda.tavolo + "' and prog_inser='" + numero_progressivo + "' and stato_record='ATTIVO' limit 1;";
                comanda.sincro.query(query, function (result) {
                    try {
                        touch_servito.endx = undefined;
                        touch_servito.endy = undefined;
                        var nodo_principale = result[0].nodo.substr(0, 3);
                        comanda.clona_articolo_nodo = nodo_principale;
                        articolo_servito();
                    } catch (e1) {
                        alert(e1);
                    }
                });
            } catch (e) {
                alert(e);
            }

        } else
        {
            touch_servito.endx = undefined;
            touch_servito.endy = undefined;
        }
    });
} else if (nome_pagina.indexOf("LAYOUT_IBRIDO_IPHONE") !== -1 || nome_pagina.indexOf("tavolo") !== -1)
{
    //PER MOBILE

    var touch_servito = new Object();
    $(document).on('touchstart', '#conto tr:not(.comanda)', function (e) {
        if (comanda.slide_qta_articolo === "S") {
            var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
            touch_servito.startx = touch.pageX;
            touch_servito.starty = touch.pageY;
            var id_articolo = e.currentTarget.id;
        }
    });
    $(document).on('touchend', '#conto tr:not(.comanda)', function (e) {
        if (comanda.slide_qta_articolo === "S") {
            var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
            var id_articolo = e.currentTarget.id;
            var numero_progressivo = id_articolo.replace('art_', '');
            touch_servito.endx = touch.pageX;
            touch_servito.endy = touch.pageY;
            //end-start perchÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¨ in questo caso ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¨ verso destra
            if ((touch_servito.endx - touch_servito.startx > 100) && (touch_servito.starty - touch_servito.endy <= 10)) {
                try {
                    var query = "select contiene_variante,nodo from comanda where ntav_comanda='" + comanda.tavolo + "' and prog_inser='" + numero_progressivo + "' and stato_record='ATTIVO' limit 1;";
                    comanda.sincro.query(query, function (result) {
                        try {
                            touch_servito.endx = undefined;
                            touch_servito.endy = undefined;
                            var nodo_principale = result[0].nodo.substr(0, 3);
                            var nodo_attuale = result[0].nodo;
                            comanda.clona_articolo_nodo = nodo_principale;
                            if (comanda.multiquantita === "S" || result[0].contiene_variante === "1" || nodo_attuale.length > nodo_principale.length) {

                                var query_aggiunta_articolo = "update comanda set quantita=cast(quantita as int)+1 where ntav_comanda='" + comanda.tavolo + "' and substr(nodo,1,3)='" + nodo_principale + "' and stato_record='ATTIVO';";
                                comanda.sock.send({tipo: "aggiunta_articolo", operatore: comanda.operatore, query: query_aggiunta_articolo, terminale: comanda.terminale, ip: comanda.ip_address});
                                comanda.sincro.query(query_aggiunta_articolo, function () {
                                    comanda.funzionidb.conto_attivo();
                                });

                            } else {
                                clona_articolo(1);
                            }
                        } catch (e1) {
                            alert(e1);
                        }
                    });
                } catch (e) {
                    alert(e);
                }

            } else if ((touch_servito.startx - touch_servito.endx > 100) && (touch_servito.starty - touch_servito.endy <= 10)) {
                var query = "select contiene_variante,nodo from comanda where ntav_comanda='" + comanda.tavolo + "' and prog_inser='" + numero_progressivo + "' and stato_record='ATTIVO' limit 1;";
                comanda.sincro.query(query, function (result) {

                    try {

                        var nodo_principale = result[0].nodo.substr(0, 3);
                        var nodo_attuale = result[0].nodo;
                        if (comanda.multiquantita === "S" || result[0].contiene_variante === "1" || nodo_attuale.length > nodo_principale.length) {


                            if (result[0].quantita === "1") {
                                var data = comanda.funzionidb.data_attuale().substr(0, 8);
                                var ora = comanda.funzionidb.data_attuale().substr(9, 8).replace(/-/gi, '/');
                                var query_modifica_articolo = "update comanda set  stato_record='CANCELLATO DURANTE CREAZIONE COMANDA (2) " + data + " " + ora + "'  where ntav_comanda='" + comanda.tavolo + "' and substr(nodo,1,3)='" + nodo_principale + "' and stato_record='ATTIVO';";
                            } else {
                                var query_modifica_articolo = "update comanda set quantita=quantita-1 where ntav_comanda='" + comanda.tavolo + "' and substr(nodo,1,3)='" + nodo_principale + "' and stato_record='ATTIVO';";
                            }
                            comanda.sock.send({tipo: "modifica_articolo", operatore: comanda.operatore, query: query_modifica_articolo, terminale: comanda.terminale, ip: comanda.ip_address});


                            comanda.sincro.query(query_modifica_articolo, function () {
                                comanda.funzionidb.conto_attivo();
                            });

                        } else {
                            //NON DEVE MAI BYPASSARE AVVISO STORNO
                            elimina_articolo(numero_progressivo, false);
                        }

                    } catch (e) {
                        alert(e);
                    }
                });

            } else
            {
                touch_servito.endx = undefined;
                touch_servito.endy = undefined;
            }
        }
    });
    //SU ULTIME BATTITURE
    var touch_servito_ub = new Object();
    $(document).on('touchstart', '.table.ultime_battiture tr', function (e) {
        if (comanda.slide_qta_articolo === "S") {
            var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
            touch_servito_ub.startx = touch.pageX;
            touch_servito_ub.starty = touch.pageY;
            var id_articolo = e.currentTarget.id;
        }
    });
    $(document).on('touchend', '.table.ultime_battiture tr', function (e) {
        if (comanda.slide_qta_articolo === "S") {
            var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
            var id_articolo = e.currentTarget.id;
            var numero_progressivo = id_articolo.replace('art_', '');
            touch_servito_ub.endx = touch.pageX;
            touch_servito_ub.endy = touch.pageY;
            //end-start perchÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¨ in questo caso ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¨ verso destra
            if ((touch_servito_ub.endx - touch_servito_ub.startx > 100) && (touch_servito_ub.starty - touch_servito_ub.endy <= 10)) {
                try {
                    var query = "select contiene_variante,nodo from comanda where ntav_comanda='" + comanda.tavolo + "' and prog_inser='" + numero_progressivo + "' and stato_record='ATTIVO' limit 1;";
                    comanda.sincro.query(query, function (result) {
                        try {
                            touch_servito_ub.endx = undefined;
                            touch_servito_ub.endy = undefined;
                            var nodo_principale = result[0].nodo.substr(0, 3);
                            var nodo_attuale = result[0].nodo;
                            comanda.clona_articolo_nodo = nodo_principale;
                            if (comanda.multiquantita === "S" || result[0].contiene_variante === "1" || nodo_attuale.length > nodo_principale.length) {



                                var query_aggiunta_articolo = "update comanda set quantita=cast(quantita as int)+1 where ntav_comanda='" + comanda.tavolo + "' and substr(nodo,1,3)='" + nodo_principale + "' and stato_record='ATTIVO';";
                                comanda.sock.send({tipo: "aggiunta_articolo", operatore: comanda.operatore, query: query_aggiunta_articolo, terminale: comanda.terminale, ip: comanda.ip_address});
                                comanda.sincro.query(query_aggiunta_articolo, function () {
                                    comanda.funzionidb.conto_attivo();
                                });

                            } else {
                                clona_articolo(1);
                            }
                        } catch (e1) {
                            alert(e1);
                        }
                    });
                } catch (e) {
                    alert(e);
                }

            }
            if ((touch_servito_ub.startx - touch_servito_ub.endx > 100) && (touch_servito_ub.starty - touch_servito_ub.endy <= 10)) {
                var query = "select contiene_variante,nodo,quantita from comanda where ntav_comanda='" + comanda.tavolo + "' and prog_inser='" + numero_progressivo + "' and stato_record='ATTIVO' limit 1;";
                comanda.sincro.query(query, function (result) {

                    try {

                        var nodo_principale = result[0].nodo.substr(0, 3);
                        var nodo_attuale = result[0].nodo;
                        if (comanda.multiquantita === "S" || result[0].contiene_variante === "1" || nodo_attuale.length > nodo_principale.length) {

                            if (result[0].quantita === "1") {
                                var data = comanda.funzionidb.data_attuale().substr(0, 8);
                                var ora = comanda.funzionidb.data_attuale().substr(9, 8).replace(/-/gi, '/');
                                var query_modifica_articolo = "update comanda set  stato_record='CANCELLATO DURANTE CREAZIONE COMANDA (2) " + data + " " + ora + "'  where ntav_comanda='" + comanda.tavolo + "' and substr(nodo,1,3)='" + nodo_principale + "' and stato_record='ATTIVO';";
                            } else if (parseInt(result[0].quantita) > 1) {
                                var query_modifica_articolo = "update comanda set quantita=quantita-1 where ntav_comanda='" + comanda.tavolo + "' and substr(nodo,1,3)='" + nodo_principale + "' and stato_record='ATTIVO';";
                            }
                            comanda.sock.send({tipo: "modifica_articolo", operatore: comanda.operatore, query: query_modifica_articolo, terminale: comanda.terminale, ip: comanda.ip_address});


                            comanda.sincro.query(query_modifica_articolo, function () {
                                comanda.funzionidb.conto_attivo();
                            });

                        } else {
                            //NON DEVE MAI BYPASSARE AVVISO STORNO
                            elimina_articolo(numero_progressivo, false);
                        }

                    } catch (e) {
                        alert(e);
                    }
                });
            } else
            {
                touch_servito_ub.endx = undefined;
                touch_servito_ub.endy = undefined;
            }
        }
    });
}

function confronta_data_locale_server() {


    $.ajax({
        async: true,
        url: comanda.url_server + 'classi_php/data_server.php',
        timeout: 20000,
        success: function (dati) {

            //var now = new Date();

            //var data_locale = now.format("dd/mm/yyyy");

            //Se l'ora attuale ÃƒÂ¨ minore dell'ora del servizio
            //parseInt(new Date().format("HH"))< parseInt(comanda.ora_servizio.toString())


            //La data del servizio ÃƒÂ¨ uguale alla data del server
            //data_inizio_servizio===dati (questa vale solo se l'ora non ÃƒÂ¨ minore all'ora di servizio
            //                              altrimenti va confrontata con la data del giorno dopo di quello del servizio)

            //La data del palmare ÃƒÂ¨ uguale alla data del server (questa vale in tutti i casi)
            //data_locale === dati

            var ora_attuale_locale = new Date().format("HH");

            //parseInt(ora_attuale_locale) +' '+ parseInt(comanda.ora_servizio) +' '+ data_inizio_servizio_giorno_dopo+' '+ dati+' '+ data_inizio_servizio
            if ((parseInt(ora_attuale_locale) < parseInt(comanda.ora_servizio) && data_inizio_servizio_giorno_dopo === dati) || (parseInt(ora_attuale_locale) >= parseInt(comanda.ora_servizio) && data_inizio_servizio === dati))
            {
                //OK
            } else {
                alert("La data del server non corrisponde alla data del dispositivo.\n\n\CHIUDI fast.intellinet e RI-APRILO per RISOLVERE.");
                btn_tavoli();
            }

        },
        error: function () {

            //alert("Il server non ÃƒÆ’Ã‚Â¨ raggiungibile.");
            //confronta_data_locale_server();

        }

    });



}



function btn_tavoli(tipo) {


    salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + aggZero(new Date().getMilliseconds(), 3) + " # BTN_TAVOLI # # ");

    $("#tastiera_ricerca").hide();
    $("#tastiera_lettere").hide();
    $("#tastiera_numeri").hide();

    $("#totale_scontrino").html('0.00');
    comanda.permesso = 0;
    console.log("OP1");
    comanda.tavolo = "0";
    comanda.categoria = comanda.categoria_partenza;
    comanda.pagina = "1";
    $('#numero_coperti').val('1');
    if (comanda.mobile === false) {
        $("#tab_sx a").removeClass("cat_accesa");
        $("#tab_sx a:nth-child(" + comanda.categoria_partenza + ")").addClass("cat_accesa");
    }
    selezione_operatore('MAPPA TAVOLI');
    $('.tasto_konto').css('pointer-events', '');
    $('.tasto_bestellung').css('pointer-events', '');
    $('.tasto_bargeld').css('pointer-events', '');
    $('.tasto_quittung').css('pointer-events', '');
    $('.tasto_rechnung').css('pointer-events', '');

}


var prima_paginata_tavoli = true;
var ultima_sala_mostrata = comanda.nome_sala;
function disegna_tavoli_salvati(numero, cb) {

    if (comanda.selezione_tavoli_piantina === false) {

        let  tastierino_scelta_tavolo = "";

        tastierino_scelta_tavolo += "<div style='position: fixed;bottom: 0;'>";

        tastierino_scelta_tavolo += "<div style='text-align:center;display:black;margin-bottom:0.5vh;'>";
        tastierino_scelta_tavolo += "<input id='campo_numero_tavolo' placeholder='Numero Tavolo' type='text' class='form-control escludi_tastiera'>";
        tastierino_scelta_tavolo += "<button type=\"button\" onclick=\"evento_selettore_tavolo(null,$(\'#campo_numero_tavolo\').val(),$(\'#campo_numero_tavolo\').val())\" class=\"btn btn-primary btn-success\">Entra</button>";
        tastierino_scelta_tavolo += "</div>";

        tastierino_scelta_tavolo += "<div style='display:table;width: 100vw;text-align: center;border-collapse: separate;border-spacing: 2px;'>";

        for (let col = 0; col < 4; col++) {
            tastierino_scelta_tavolo += "<div style='display:table-row;'>";

            if (col === 3) {

                tastierino_scelta_tavolo += "<div class='myButtonTavolo' onclick='$(\"#campo_numero_tavolo\").val(\"\");'>";
                tastierino_scelta_tavolo += "C";
                tastierino_scelta_tavolo += "</div>";

                tastierino_scelta_tavolo += "<div class='myButtonTavolo' onclick='$(\"#campo_numero_tavolo\").val(function() {    return this.value + \"0\"; })'>";
                tastierino_scelta_tavolo += 0;
                tastierino_scelta_tavolo += "</div>";

                tastierino_scelta_tavolo += "<div class='myButtonTavolo' onclick='$(\"#campo_numero_tavolo\").val(function() {    return this.value.slice(0,-1); });'>";
                tastierino_scelta_tavolo += "&lt;";
                tastierino_scelta_tavolo += "</div>";

            } else {
                for (let row = 0; row < 3; row++) {

                    tastierino_scelta_tavolo += "<div class='myButtonTavolo'  onclick='$(\"#campo_numero_tavolo\").val(function() {    return this.value + \"" + ((row + 1) + (col * 3)) + "\"; })'>";
                    tastierino_scelta_tavolo += (row + 1) + (col * 3);
                    tastierino_scelta_tavolo += "</div>";

                }
            }
            tastierino_scelta_tavolo += "</div>";
        }
        tastierino_scelta_tavolo += "</div>";
        tastierino_scelta_tavolo += "</div>";

        tastierino_scelta_tavolo += "   <style> \n\
                                                \n\
                                        .myButtonTavolo {\n\
                                            display:table-cell;\n\
                                            height: 8vh;\n\
                                            vertical-align: middle;\n\
                                            background:linear-gradient(to bottom, #ffffff 5%, #dfeef0 100%);\n\
                                            background-color:#ffffff;\n\
                                            border-radius:8px;\n\
                                            cursor:pointer;\n\
                                            color:#000000;\n\
                                            font-family:Arial;\n\
                                            font-size:20px;\n\
                                            font-weight:bold;\n\
                                            padding:13px 32px;\n\
                                            text-decoration:none;\n\
                                            text-shadow:0px 1px 0px #ffffff;\n\
                                        }\n\
                                         \n\
                                        .myButtonTavolo:hover {\n\
                                            background:linear-gradient(to bottom, #dfeef0 5%, #ffffff 100%);\n\
                                            background-color:#dfeef0;\n\
                                        }\n\
                                         \n\
                                        .myButtonTavolo:active {\n\
                                            position:relative;\n\
                                            top:1px;\n\
                                        }\n\
                                        </style>";


        $('#contenitore_tavoli').html(tastierino_scelta_tavolo);

        if (typeof (cb) === "function") {
            cb(true);
        }

    } else {

        var contenitore_tavoli = '';
        var data_inizio = new Date().getTime();

        console.log("--- PIANTINA INIZIO ---");




        aggiornamento_tavoli_xml(function (cc) {

            console.log("PIANTINA AGG.TAVOLI", new Date().getTime() - data_inizio);


            if (cc === false || prima_paginata_tavoli === true || ultima_sala_mostrata !== comanda.nome_sala) {
                prima_paginata_tavoli = false;

                if (comanda.ingresso !== true) {
                    $('.btn_nome_sala').css('pointer-events', 'none');
                    $('.btn_nome_sala').css('background-image', '-webkit-linear-gradient(top, grey, grey)');
                }

                $('.scritte_piantina,.scritte_piantina_iphone').hide();


                $('.tavolo.rett,.tavolo.cerc').remove();

                var testo_query = "select * from tavoli where nome_sala='" + comanda.nome_sala + "' and estensione='" + comanda.piantina + "';";

                comanda.sincro.query(testo_query, function (result) {


                    var i = 0;
                    var c = result.length;


                    result.forEach(function (obj) {

                        var pos_x = "0";
                        var pos_y = "0";
                        var larghezza = "0";
                        var altezza = "0";

                        if (comanda.mobile === true) {
                            pos_x = proporzione_x(obj.pos_x_mobile);
                            pos_y = proporzione_y(obj.pos_y_mobile);

                            larghezza = proporzione(obj.larghezza);
                            altezza = proporzione(obj.altezza);
                        } else
                        {
                            pos_x = obj.pos_x + 'vh';
                            pos_y = obj.pos_y + 'vh';

                            larghezza = obj.larghezza + 'vh';
                            altezza = obj.altezza + 'vh';
                        }

                        var colore_tavolo;
                        var colore_scritta;
                        if (obj.numero !== "BANCO" && obj.numero !== "BAR" && obj.numero.left(7) !== "ASPORTO" && obj.numero !== "TAKE AWAY") {
                            i++;



                            if (obj.colore === '8') {
                                colore_tavolo = 'background-color:red;background-image: -webkit-linear-gradient(top,pink, lightpink);';
                            } else if (obj.colore === '3') {
                                colore_tavolo = 'background-color:red;background-image: -webkit-linear-gradient(top,red, red);';
                            } else if (obj.colore === '7' || obj.colore === 'azure') {
                                colore_tavolo = 'background-color:#3498db;background-image: -webkit-linear-gradient(top, #3498db, #2677ac);';
                            } else if (obj.colore === '6' || obj.colore === 'fucsia') {
                                colore_tavolo = 'background-color:FUCHSIA;background-image: -webkit-linear-gradient(top,FUCHSIA, #b400b4);';
                            } else if (obj.colore === '2') {
                                if (comanda.discoteca === true) {
                                    colore_tavolo = 'background-color:orange;background-image: -webkit-linear-gradient(top,orange, orange);';
                                } else
                                {
                                    colore_tavolo = 'background-color:#FFA07A;background-image: -webkit-linear-gradient(top,#FFA07A, #b06c52);';
                                }
                            } else if (obj.colore === '4' || obj.colore === 'blue') {
                                colore_tavolo = 'background-color:blue;background-image: -webkit-linear-gradient(top,blue,#000093);';
                            } else if (obj.colore === '1')
                            {
                                if (comanda.discoteca === true) {
                                    colore_tavolo = 'background-color:red;background-image: -webkit-linear-gradient(top,yellow, yellow);';
                                } else
                                {
                                    colore_tavolo = 'background-color:red;background-image: -webkit-linear-gradient(top,red, #800c00);';
                                }
                            } else if (obj.colore === '0')
                            {
                                colore_tavolo = 'background-color:green;background-image: -webkit-linear-gradient(top,green, green);';
                            } else if (obj.colore === "I" + comanda.terminale)
                            {
                                colore_tavolo = 'background-color:yellow;background-image: -webkit-linear-gradient(top,yellow, yellow);';
                            } else
                            {
                                colore_tavolo = '';

                                if (comanda.ingresso !== true) {
                                    comanda.nomi_tavoli.forEach(function (o) {

                                        if (aggZero(obj.numero, 3) === o.tav) {
                                            colore_tavolo = 'background-color:palegreen;background-image: -webkit-linear-gradient(top,palegreen, palegreen);';
                                        }
                                    });
                                }
                            }


                            var id_tavolo = aggZero(obj.numero, 3);

                            var tavolotxt = obj.tavolotxt;
                            if (obj.non_mostrare_prima_cifra === 's') {
                                tavolotxt = tavolotxt.substr(1);
                            }

                            contenitore_tavoli += '<div style="padding:0;' + colore_tavolo + 'font-size:3vh;width:' + comanda.grandezza_tavoli_larghezza + 'vh; height:' + comanda.grandezza_tavoli_altezza + 'vh;top:' + pos_y + ';left:' + pos_x + ';" class="' + obj.classe + '" id="tav_' + id_tavolo + '" ><h6 style="margin:0;vertical-align: middle;display: table-cell;"><b  class="' + obj.tavolotxt + '" style="font-size:' + comanda.grandezza_font_tavoli + 'vh">' + tavolotxt + '</b></h6></div>';



                            if (c === i) {

                                selettore_tavolo();


                                //A QUESTO CONCATENO L'ARRAY ESISTENTE DEI SOCKET CON QUELLA TEMPORANEA E COSI FACENDO LANCIO I SOCKET
                                comanda.socket_ciclizzati = comanda.socket_ciclizzati.concat(comanda.socket_ciclizzati_temp);
                                comanda.socket_ciclizzati_temp = [];
                            }

                        }

                    });


                    $('#contenitore_tavoli').html(contenitore_tavoli);

                    try {

                        if (comanda.iphone !== true) {
                            $('.scritte_piantina.' + result[0].nome_sala.replace(/ /g, "") + result[0].estensione.replace(/ /g, "")).show();
                        } else
                        {
                            $('.scritte_piantina_iphone.' + result[0].nome_sala.replace(/ /g, "") + result[0].estensione.replace(/ /g, "")).show();
                        }

                        ultima_sala_mostrata = result[0].nome_sala;

                        if (comanda.ingresso !== true) {
                            $('.btn_nome_sala').css('pointer-events', '');
                            $('.btn_nome_sala').css('background-image', '');
                        }

                    } catch (e) {

                    }


                    console.log("PIANTINA INTERA", new Date().getTime() - data_inizio);

                    try {
                        $(document).off(comanda.eventino, evento_selettore_tavolo);
                    } catch (e) {
                        console.log(e);
                    }


                    $(document).on(comanda.eventino, 'div.tavolo.rett,div.tavolo.cerc', evento_selettore_tavolo);
                    $('.tasto_konto').css('pointer-events', '');
                    $('.tasto_bestellung').css('pointer-events', '');
                    $('.tasto_bargeld').css('pointer-events', '');
                    $('.tasto_quittung').css('pointer-events', '');
                    $('.tasto_rechnung').css('pointer-events', '');



                    if (typeof (cb) === "function") {
                        cb(true);
                    }
                });
            } else if (cc !== false) {
                var testo_query = "select * from tavoli where nome_sala='" + comanda.nome_sala + "'  and estensione='" + comanda.piantina + "';";

                comanda.sincro.query(testo_query, function (result) {
                    var i = 0;
                    var c = result.length;


                    result.forEach(function (obj) {

                        var colore_tavolo;
                        var background_image;
                        var colore_scritta;
                        if (obj.numero !== "BANCO" && obj.numero !== "BAR" && obj.numero.left(7) !== "ASPORTO" && obj.numero !== "TAKE AWAY") {
                            i++;
                            if (obj.colore === '8') {

                                colore_tavolo = 'red';
                                background_image = '-webkit-linear-gradient(top,pink, lightpink)';

                            } else if (obj.colore === '3') {

                                colore_tavolo = 'red';
                                background_image = '-webkit-linear-gradient(top,red, red)';

                            } else if (obj.colore === '7' || obj.colore === 'azure') {
                                colore_tavolo = '#3498db';
                                background_image = '-webkit-linear-gradient(top, #3498db, #2677ac)';
                            } else if (obj.colore === '6' || obj.colore === 'fucsia') {
                                colore_tavolo = 'FUCHSIA';
                                background_image = '-webkit-linear-gradient(top,FUCHSIA, #b400b4)';
                            } else if (obj.colore === '2') {
                                if (comanda.discoteca === true) {
                                    colore_tavolo = 'orange';
                                    background_image = '-webkit-linear-gradient(top,orange, orange)';
                                } else {
                                    colore_tavolo = '#FFA07A';
                                    background_image = '-webkit-linear-gradient(top,#FFA07A, #b06c52)';
                                }
                            } else if (obj.colore === '4' || obj.colore === 'blue') {
                                colore_tavolo = 'blue';
                                background_image = '-webkit-linear-gradient(top,blue,#000093)';
                            } else if (obj.colore === '1')
                            {
                                if (comanda.discoteca === true) {
                                    colore_tavolo = 'yellow';
                                    background_image = '-webkit-linear-gradient(top,yellow, yellow)';
                                } else {
                                    colore_tavolo = 'red';
                                    background_image = '-webkit-linear-gradient(top,red, #800c00)';
                                }
                            } else if (obj.colore === '0')
                            {
                                colore_tavolo = 'green';
                                background_image = '-webkit-linear-gradient(top,green,green)';
                            } else if (obj.colore === "I" + comanda.terminale)
                            {
                                colore_tavolo = 'yellow';
                                background_image = '-webkit-linear-gradient(top,yellow,yellow)';
                            } else
                            {
                                colore_tavolo = '';
                                background_image = '';

                                if (comanda.ingresso !== true) {

                                    comanda.nomi_tavoli.forEach(function (o) {

                                        if (aggZero(obj.numero, 3) === o.tav) {
                                            background_image = '-webkit-linear-gradient(top,palegreen, palegreen)';
                                            colore_tavolo = 'palegreen';
                                        }
                                    });
                                }

                            }


                            var id_tavolo = aggZero(obj.numero, 3);


                            setTimeout(function () {
                                $('#tav_' + id_tavolo).css('background-color', colore_tavolo);
                                $('#tav_' + id_tavolo).css('background-image', background_image);
                            }, 1);



                        }

                    });

                    console.log("PIANTINA CSS", new Date().getTime() - data_inizio);

                    try {
                        $(document).off(comanda.eventino, evento_selettore_tavolo);
                    } catch (e) {
                        console.log(e);
                    }
                    $(document).on(comanda.eventino, 'div.tavolo.rett,div.tavolo.cerc', evento_selettore_tavolo);
                    $('.tasto_konto').css('pointer-events', '');
                    $('.tasto_bestellung').css('pointer-events', '');
                    $('.tasto_bargeld').css('pointer-events', '');
                    $('.tasto_quittung').css('pointer-events', '');
                    $('.tasto_rechnung').css('pointer-events', '');


                    if (typeof (cb) === "function") {
                        cb(true);
                    }
                });
            }
        });
    }
}

var evento_selettore_tavolo = function (e, tavolo, tavolotxt) {


    try {
        $(document).off(comanda.eventino, evento_selettore_tavolo);
    } catch (e) {
        console.log(e);
    }
    //INSERITO IL 29/05/2017 per prova. Se fa doppi listener eliminare pure.
    //(perÃƒÂ² causa un blocco quando non ÃƒÂ¨ acceso fast comanda per cui bisogna riavviare)
    //$(document).on(comanda.eventino, 'div.tavolo.rett,div.tavolo.cerc', evento_selettore_tavolo);

    if (comanda.tipo_ricerca_articolo === "NUMERICA") {
        rna.reset();
    }

    $('.loader').show();

    $(this).css('background-image', '-webkit-linear-gradient(to bottom, white, white)');




    var testo_query;
    var date = new Date();

    var hour = date.getHours();

    if (parseInt(hour) <= 3) {
        date.setDate(date.getDate() - 1);
    }

    var ora = addZero(date.getHours(), 2) + ':' + addZero(date.getMinutes(), 2);

    confronta_data_locale_server();

    if (tavolo === undefined && tavolotxt === undefined) {
        tavolo = $(this).attr('id').substr(4);

        tavolotxt = $(this).find('h6>b').attr('class');
    }

    comanda.tavolo = tavolo;
    comanda.tavolotxt = tavolotxt;

    var month = aggZero(date.getMonth() + 1, 2); //months from 1-12
    var day = aggZero(date.getDate(), 2);
    var year = aggZero(date.getFullYear(), 2);

    var newdate = year + "-" + month + "-" + day;

    if (comanda.ingresso === true) {
        if (tavolo === ultimo_tavolo_colorato) {
            invia_messaggio_cliente_seduto(tavolo, tavolotxt);
        } else
        {
            try {
                $(document).off(comanda.eventino, evento_selettore_tavolo);
            } catch (e) {
                console.log(e);
            }
            $(document).on(comanda.eventino, 'div.tavolo.rett,div.tavolo.cerc', evento_selettore_tavolo);
            $('.loader').hide();
        }

    } else {



        verifica_esistenza_file("/" + comanda.directory_dati + "/" + newdate + "_" + comanda.folder_number + "/COND/Tav" + tavolo.replace(/^0+/, '') + ".CO1", function (risultato) {
            if (risultato !== true) {

                //PULISCE LE ULTIME BATTITURE PER NON FARE UNO SCHIFOSO EFFETTO REFRESH
                $('.ultime_battiture').html('');

                lettura_tavolo_xml(tavolo, function (l) {

                    if (l === true) {

                        $('#tasto_uscita_tavoli,.scritta_operatore,.numero_tavolo').css('opacity', '0.5');
                        $('#tasto_uscita_tavoli,.scritta_operatore,.numero_tavolo').css('pointer-events', 'none');

                        if (comanda.sagra_mode === 'S') {
                            $('#popup_nome_tavolo input').val('');
                            $('#popup_nome_tavolo').modal('show');
                            $('#popup_nome_tavolo--tavolotxt').focus();
                            $('.loader').hide();
                        } else
                        {
                            blocco_concreto(tavolo, tavolotxt);
                        }
                    }
                });
            } else if (comanda.sblocca_tavolo_cliccato === false)
            {
                salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + aggZero(new Date().getMilliseconds(), 3) + " # TAVOLO IN USO # TAVOLO: " + tavolo + " # ");

                alert(comanda.lang[121]);

                try {
                    $(document).off(comanda.eventino, evento_selettore_tavolo);
                } catch (e) {
                    console.log(e);
                }
                $(document).on(comanda.eventino, 'div.tavolo.rett,div.tavolo.cerc', evento_selettore_tavolo);
                $('.loader').hide();
            }
        });
    }

};

function selettore_tavolo() {

    try {
        $(document).off(comanda.eventino, evento_selettore_tavolo);
        $('div.tavolo.rett,div.tavolo.cerc').draggable();
        $('div.tavolo.rett,div.tavolo.cerc').resizable();
        $('div.tavolo.rett,div.tavolo.cerc').draggable('destroy');
        $('div.tavolo.rett,div.tavolo.cerc').resizable('destroy');
        $('div.tavolo.rett,div.tavolo.cerc').off(comanda.eventino);
    } catch (e) {
        console.log(e);
    }

    $(document).on(comanda.eventino, 'div.tavolo.rett,div.tavolo.cerc', evento_selettore_tavolo);

}




var click_chiusura = 0;
var timeout_chiusura;


$(document).on(comanda.eventino, '#lista_tavoli_aperti', function () {
    click_chiusura++;
    clearTimeout(timeout_chiusura);

    if (click_chiusura === 3) {
        ritorna_comanda();
    }

    timeout_chiusura = setTimeout(function () {
        click_chiusura = 0;
    }, 1500);
});



function apri_fastconto() {
    nascondi_righe();
    $('#numero_tavolo_fastconto').html(comanda.tavolotxt);
    $('#integrazione_fastconto').show();
    $('#pagina_konto #tab_dx_grande td').css('pointer-events', 'none');
}

function ritorna_comanda() {
    nascondi_righe();
    $('#creazione_comanda').show();
    indietro_categoria();
    elenco_prodotti();
    $('#pagina_konto #tab_dx_grande').css('pointer-events', '');
}

function resetScroll(element) {
    element.parentElement.replaceChild(element, element);
}

function blocco_concreto(tavolo, tavolotxt) {



    try {
        resetScroll(document.getElementById("tab_sx"));
    } catch (e) {
        console.log(e);
    }

    var id_tavolo = tavolo.replace(/^0+/, '');

    comanda.socket_ciclizzati = comanda.socket_ciclizzati.concat(comanda.socket_ciclizzati_temp);
    comanda.socket_ciclizzati_temp = [];

    comanda.tavolo = tavolo;


    var BreakException = {};

    $('.scritta_operatore').html("");
    $('#gutshein__importo_gutshein').val("");

    try {
        comanda.nomi_tavoli.forEach(function (obj) {

            if (aggZero(obj.tav, 3) === aggZero(comanda.tavolo, 3)) {

                var nome = obj.nome;
                nome = nome.replace(/\s\x\s\d+/gi, "");
                nome = nome.replace(/\s\x\d+/gi, "");
                nome = nome.replace(/\x\s\d+/gi, "");
                nome = nome.replace(/\x\d+/gi, "");

                $('.scritta_operatore').html("<strong>" + nome + "</strong>");
                throw BreakException;
            }
        });

    } catch (e) {
        if (e !== BreakException)
            throw e;
    }


    //riempie il conto con il tavolo giusto
    $('.nome_parcheggio').html(comanda.parcheggio);
    $('.nome_operatore').html(""/*comanda.operatore*/);
    $('.numero_tavolo').html(comanda.tavolotxt);
    $('#quantita_articolo').val('1');

    comanda.numero_conto_split = 0;
    if (lettera_conto_split() === "")
    {
        $('#numero_conto_split').html("BIS");
    } else
    {
        $('#numero_conto_split').html(lettera_conto_split());
    }

    selezione_operatore("TAVOLO SELEZIONATO");

    if (comanda.mobile === true) {
        window.scrollTo(0, 0);
        if (comanda.apri_subito_categoria_predefinita === true) {
            $('.alfabeto').show();
            mod_categoria(comanda.categoria_predefinita.toString(), '', '1');
            $("#tab_sx a", 'body').removeClass("cat_accesa");
            $('#tab_sx a.cat_' + comanda.categoria_predefinita, 'body').addClass("cat_accesa");
            $('.alfabeto>.position>div', 'body').removeClass('hover');
            $('.alfabeto>.position>div:first-child', 'body').addClass('hover');
            categoria_cliccata();
        } else
        {
            elenco_categorie_main();
        }

        if (comanda.split_abilitato === true) {
            scegli_conto_split();
        }

    }
    try {
        $(document).off(comanda.eventino, evento_selettore_tavolo);
    } catch (e) {
        console.log(e);
    }
    $(document).on(comanda.eventino, 'div.tavolo.rett,div.tavolo.cerc', evento_selettore_tavolo);
    $('.loader').hide();
}

function nomi_portate_txt(callback) {

    var lettere_portate = new Array();
    lettere_portate.push("0");
    lettere_portate.push("1");
    lettere_portate.push("2");
    lettere_portate.push("3");
    lettere_portate.push("4");
    lettere_portate.push("P");
    lettere_portate.push("B");
    lettere_portate.push("N");

    $.ajax({
        async: true,
        url: comanda.url_server + 'classi_php/apri_file_testo.php',
        data: {directory_maga: comanda.directory_maga},
        timeout: 20000,
        dataType: "json",
        success: function (dati) {

            if (dati !== false)
            {

                console.log("DATI", dati);

                dati.forEach(function (el, key) {

                    var testo_query = "update nomi_portate set " + comanda.lingua_stampa + "='" + el + "' where numero='" + lettere_portate[key] + "'; ";

                    comanda.sincro.query(testo_query, function () {

                    });

                });

                callback(true);

            } else
            {
                callback(false);
            }

        },
        error: function () {

            alert(lang[256]);

            callback(false);
        }

    });
}

function nuovo_tavolo() {
    var errori = false;

    var numero = $('#numero_tavolo').val();
    var tavolotxt = $('#nome_tavolo').val();

    if (numero.length < 1) {
        alert("Devi assegnare obbligatoriamente un numero e un nome al tavolo.");
        errori = true;
    }

    $('div.tavolo.rett,div.tavolo.cerc').each(function () {

        if ($(this).find('h4').html() === numero && errori === false) {
            alert("Errore. Il tavolo con quel numero gi&agrave;Â  esiste");
            errori = true;
        }
    });
    var forma = $('.forma_selezionata').attr('class');
    forma = forma.replace('forma_selezionata', '');
    forma = forma.replace('-min', '');
    ////console.log(forma);
    //forma = forma.slice(0, -4);


    if (forma === undefined) {
        alert("Devi scegliere obbligatoriamente una forma per il tavolo.");
    }

    if (errori === false)
    {

        var top = ($('#contenitore_tavoli').offset().top) + 50;
        var left = ($('#contenitore_tavoli').offset().left) + 0;
        var testo_query = "insert or replace into tavoli (class,numero,tavolotxt,abilitato,altezza,larghezza,pos_x,pos_y,colore,ora_apertura_tavolo,ora_ultima_comanda,colore) VALUES ('" + forma + " ui-draggable ui-draggable-handle ui-resizable','" + numero + "','" + tavolotxt + "','1','40','37','" + top + "','" + left + "','grey','-','-','0');";
        comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
        comanda.sincro.query(testo_query, function () {
        });
        comanda.sincro.query_cassa();

        ////console.log(top, left);
        $('#contenitore_tavoli').append('<div style="top:' + top + 'px;left:' + left + 'px;" class="' + forma + '" id="tav_' + numero + '" ><h4></h4><h6><b>' + tavolotxt + '</b></h6></div>');
        make_draggable();
        //deletor();
        $('#numero_tavolo').val('');

    }

}


//FINE SEZIONE TAVOLI

$(document).keypress(function (e) {
    if (e.which == 13) {
        $('.modal.in .btn-success:visible:first').trigger(comanda.eventino);
        console.log("ENTER", $('.modal.in .btn-success:visible:first'));
    }
});


function conferma_nome_tavolo() {

    if ($('#popup_nome_tavolo--tavolotxt').val().trim().length >= 1) {
        $('#popup_nome_tavolo').modal('hide');
        comanda.tavolotxt = comanda.tavolotxt + "/" + $('#popup_nome_tavolo--tavolotxt').val().toUpperCase();
        blocco_concreto(comanda.tavolo, comanda.tavolotxt);
    } else
    {
        alert("Devi inserire un nome almeno di 1 lettera.");
    }
}


//Restituisce in base al nome sala e numero di tavolotxt, l'id del tavolo vero
function id_tavolo(callBack) {
    var tavolotxt = comanda.tavolo;
    var nome_sala = comanda.nome_sala;

    var query_id_tavolo = "select id_tavolo from tavoli where nome_sala='" + nome_sala + "' and numero='" + tavolotxt + "' limit 1;";

    comanda.sincro.query(query_id_tavolo, function (risultato) {

        if (risultato !== undefined && risultato[0] !== undefined && risultato[0].id_tavolo !== undefined && risultato[0].id_tavolo !== null && risultato[0].id_tavolo !== '' && !isNaN(risultato[0].id_tavolo)) {

            var numero_tavolo = risultato[0].id_tavolo;

            callBack(numero_tavolo);

        } else
        {

            callBack(false);

        }

    });
}


function test_portata() {
    var a = new Date().getTime();
    $.ajax({
        url: comanda.url_server + 'TEST_SERVER_VIVO.txt',
        type: "POST",
        timeout: 700,
        success: function () {
            var b = new Date().getTime() - a;

//            console.log("PORTATA", b);

//$('.dati_tecnici,#tasti_cameriere')
//
            //DOPO GLI 800 E' INUTILIZZABILE
            if (b >= 500) {
                $('#indicatore_portata').css('background-color', 'red');
            } else
            //DOPO I 180 E' UN PO UNA MERDA
            if (b >= 180) {
                $('#indicatore_portata').css('background-color', 'yellow');
                //ALTRIMENTI E' BUONA
            } else {
                $('#indicatore_portata').css('background-color', 'green');
            }

        },
        error: function () {
            //SE DA ADDIRITTURA OLTRE I 1000 E' INUTILIZZABILE
            $('#indicatore_portata').css('background-color', 'brown');
        }
    });
}



$(document).ready(
        function () {



            $('body').addClass('loading');

            var date = new Date();

            var month = aggZero(date.getUTCMonth() + 1, 2); //months from 1-12
            var day = aggZero(date.getUTCDate(), 2);
            var year = aggZero(date.getUTCFullYear(), 2);

            var newdate = year + "-" + month + "-" + day;

            console.log("BLOCCO1");


            verifica_esistenza_file("/MAGA/MULTILINGUA.ldb?_=" + newdate, function (risultato) {
                if (risultato !== true) {
                    $('body').removeClass('loading');
                    $('body').addClass('game-over');
                    bootbox.alert("Errore nel caricamento dei dati. Assicurati che il Server sia raggiungibile dal dispositivo.");

                } else {

                    autoAdeguamentoDB(false, function () {

                        seleziona_servizio(function () {




                            seleziona_tastierino_abilitato(function () {

                                leggi_cartelle_maga_dati(function () {

                                    comanda.sincro.sincronizzazione_alasql('settaggi_ibrido', 1, function () {

                                        leggi_nome_menu(function () {
                                            var testo_query = 'SELECT * FROM settaggi_ibrido WHERE id="' + comanda.nome_servizio + '" limit 1;';
                                            comanda.sincro.query(testo_query, function (risultato) {



                                                /*if (comanda.ricerca_numerica_abilitata === true)
                                                 {
                                                 $("#varianti_ricerca_normale").remove();
                                                 } else
                                                 {
                                                 $("#varianti_ricerca_numerica").remove();
                                                 }*/

                                                comanda.tutte_le_varianti = risultato[0].tutte_le_varianti;
                                                comanda.tasto_tavolo_esce = risultato[0].tasto_tavolo_esce;

                                                if (risultato[0].varianti_su_tastiera !== "S") {
                                                    $(".tasto_var_ricerca").remove();
                                                    $(".tasto_spazio_ricerca").css('width', '58vw');
                                                }


                                                comanda.ultime_battiture_grandi = risultato[0].ultime_battiture_grandi;
                                                comanda.categorie_fullscreen = risultato[0].categorie_fullscreen;
                                                comanda.layout_destri = risultato[0].layout_destri;

                                                comanda.sconto_non_eliminabile = risultato[0].sconto_non_eliminabile;

                                                comanda.ricerca_su_variante = risultato[0].ricerca_su_variante;


                                                if (risultato[0].tastiera_scomparsa_auto === "S") {
                                                    $(document).mouseup(function (e)
                                                    {
                                                        var container = $("#tastiera_ricerca,input");

                                                        // if the target of the click isn't the container nor a descendant of the container
                                                        if (!container.is(e.target) && container.has(e.target).length === 0)
                                                        {
                                                            $("#tastiera_ricerca").hide();
                                                        }
                                                    });
                                                }

                                                comanda.chiusura_tastiera_dopo_prima_lettera = risultato[0].chiusura_tastiera_dopo_prima_lettera;


                                                comanda.stampante_fastingresso = risultato[0].stampante_fastingresso;

                                                comanda.filtro_lettera_bloccato = risultato[0].filtro_lettera_bloccato;

                                                comanda.azzera_prezzo_veloce = risultato[0].azzera_prezzo_veloce;

                                                comanda.modifica_portata_veloce = risultato[0].modifica_portata_veloce;

                                                comanda.modifica_prodotto = risultato[0].modifica_prodotto;

                                                comanda.modifica_prodotto_descrizione = risultato[0].modifica_prodotto_descrizione;

                                                if (comanda.modifica_prodotto_descrizione === "N") {
                                                    $(".settmodprod_descrizione input").attr('disabled', 'disabled');
                                                }



                                                comanda.modifica_prodotto_quantita = risultato[0].modifica_prodotto_quantita;

                                                if (comanda.modifica_prodotto_quantita === "N") {
                                                    $(".settmodprod_quantita input").attr('disabled', 'disabled');
                                                }



                                                comanda.modifica_prodotto_prezzo = risultato[0].modifica_prodotto_prezzo;

                                                if (comanda.modifica_prodotto_raddoppiaprezzo === "N") {
                                                    $(".settmodprod_raddoppiaprezzo").remove();
                                                }

                                                if (comanda.modifica_prodotto_prezzo === "N") {
                                                    $(".settmodprod_prezzo input").attr('disabled', 'disabled');
                                                }



                                                comanda.modifica_prodotto_destinazione_stampa = risultato[0].modifica_prodotto_destinazione_stampa;

                                                if (comanda.modifica_prodotto_destinazione_stampa === "N") {
                                                    $(".settmodprod_dest_stampa").remove();
                                                }



                                                comanda.modifica_prodotto_portata = risultato[0].modifica_prodotto_portata;

                                                if (comanda.modifica_prodotto_portata === "N") {
                                                    $(".settmodprod_portata").remove();
                                                }



                                                comanda.modifica_prodotto_clona = risultato[0].modifica_prodotto_clona;

                                                if (comanda.modifica_prodotto_clona === "N") {
                                                    $(".settmodprod_clona ").remove();
                                                }


                                                comanda.modifica_prodotto_servito = risultato[0].modifica_prodotto_servito;

                                                if (comanda.modifica_prodotto_servito === "N") {
                                                    $(".settmodprod_servito").remove();
                                                }



                                                comanda.cerca_prime_lettere = risultato[0].cerca_prime_lettere;


                                                comanda.blocco_tavolo_cameriere = risultato[0].blocco_tavolo_cameriere;

                                                if (comanda.blocco_tavolo_cameriere !== "S") {
                                                    $("#operazioni_blocco_tavolo_cameriere").remove();
                                                }

                                                comanda.p_libero_abilitato = risultato[0].p_libero_abilitato;
                                                comanda.variante_libera = risultato[0].variante_libera;
                                                comanda.mantieni_lettera = risultato[0].mantieni_lettera;
                                                comanda.controllo_utente = risultato[0].controllo_utente;
                                                comanda.cancellazione_articolo = risultato[0].cancellazione_articolo;
                                                comanda.pwd_canc_articolo = risultato[0].pwd_canc_articolo;

                                                comanda.tasto_sconto = risultato[0].tasto_sconto;

                                                comanda.tasto_rechnung = risultato[0].tasto_fattura;
                                                comanda.tasto_quittung = risultato[0].tasto_scontrino;
                                                comanda.tasto_konto = risultato[0].tasto_conto;
                                                comanda.tasto_gutshein = risultato[0].tasto_gutshein;
                                                comanda.tasto_comanda_konto = risultato[0].tasto_comanda_conto;
                                                comanda.tasto_comanda_konto_su_comanda = risultato[0].tasto_comanda_conto_su_comanda;
                                                comanda.tasto_contosep = risultato[0].tasto_contosep;
                                                comanda.tasto_bargeld = risultato[0].tasto_incasso;
                                                comanda.prevenzione_tocco_accidentale_comanda = risultato[0].prevenzione_tocco_accidentale_comanda;
                                                comanda.tasto_carte_rechnung = risultato[0].tasto_carte_rechnung;
                                                comanda.tasto_incasso = risultato[0].tasto_incasso_manuale;
                                                comanda.avviso_bargeld = risultato[0].avviso_incasso;
                                                comanda.avviso_quittung = risultato[0].avviso_quittung;
                                                comanda.avviso_rechnung = risultato[0].avviso_rechnung;
                                                comanda.slide_qta_articolo = risultato[0].slide_qta_articolo;
                                                comanda.tasto_fastconto = risultato[0].tasto_fastconto;

                                                comanda.tasto_pococotto = risultato[0].tasto_pococotto;
                                                comanda.tasto_finecottura = risultato[0].tasto_finecottura;
                                                comanda.tasto_gusti_gelato = risultato[0].tasto_gusti_gelato;


                                                //nome file xml del menu in MAGA
                                                comanda.nome_menu_xml = risultato[0].menu;

                                                //comanda+conto abilitato
                                                comanda.comanda_conto = risultato[0].comandapconto; //nodo=S TS testa=C
                                                //Se ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¨ N allora la comanda ha i TS CORPO con le destinazioni
                                                //mentre il conto ha C anche nei TS CORPO
                                                //Il nodo diventa il normale valore del nodo

                                                //storicizzazione automatica
                                                comanda.storicizzazione_auto = risultato[0].storicizzazioneauto;//TS testa=CS
                                                //in questo caso i colori tavolo vengono disabilitati

                                                //incasso_automatico
                                                comanda.incasso_auto = risultato[0].incassoauto;//calcolo dal totale o variabile dal database

                                                //modalitÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â  sagra (tavoli con numero e richiesta all'entrata del txt)
                                                comanda.sagra_mode = risultato[0].modalitasagra;

                                                if (comanda.tasto_rechnung === 'N') {
                                                    $('.tasto_rechnung').remove();
                                                }
                                                if (comanda.tasto_quittung === "N") {
                                                    $('.tasto_quittung').remove();
                                                }
                                                if (comanda.tasto_konto === "N") {
                                                    $('.tasto_konto').remove();
                                                }

                                                if (comanda.tasto_carte_rechnung === "N") {
                                                    $('.tasto_carte_rechnung').remove();
                                                }

                                                if (comanda.tasto_gutshein === "N") {
                                                    $('.tasto_gutshein').remove();
                                                    $('.tasti_incasso_ibrido').remove();
                                                }

                                                if (comanda.tasto_contosep === "N") {
                                                    $('.tasto_contosep').remove();
                                                }
                                                if (comanda.tasto_bargeld === "N") {
                                                    $('.tasto_bargeld').remove();
                                                }

                                                if (comanda.prevenzione_tocco_accidentale_comanda === "N") {
                                                    $('.prevenzione_tocco_accidentale_comanda').remove();
                                                }

                                                if (comanda.tasto_incasso === "N") {
                                                    $('.tasto_incasso_manuale').remove();
                                                }
                                                if (comanda.tasto_fastconto === "N") {
                                                    $('.tasto_fastconto').remove();
                                                }

                                                if (comanda.tasto_pococotto === "N") {
                                                    $('.tasto_pococotto').remove();
                                                }

                                                if (comanda.tasto_finecottura === "N") {
                                                    $('.tasto_finecottura').remove();
                                                }

                                                if (comanda.tasto_gusti_gelato === "N") {
                                                    $('.tasto_gusti_gelato').remove();
                                                }

                                                if (comanda.tasto_bestellung === "N") {
                                                    $('.tasto_bestellung').remove();
                                                }





                                                //DA IMPOSTARE
                                                comanda.varianti_poco;
                                                comanda.fine_cottura;

                                                //nome_servizio(function () {
                                                comanda.sincro.sincronizzazione_alasql('specifiche', 1, function () {
                                                    comanda.sincro.sincronizzazione_alasql('profilo_aziendale', 1, function () {
                                                        comanda.sincro.query("select * from profilo_aziendale where id=1;", function (profilo_aziendale) {
                                                            comanda.locale = profilo_aziendale[0].nome_locale;


                                                            alasql("CREATE TABLE oggetto_conto ( `ord` TEXT NOT NULL DEFAULT '', `cod_promo` TEXT NOT NULL DEFAULT '', `fiscalizzata_sn` TEXT NOT NULL DEFAULT '', `ora_` TEXT NOT NULL DEFAULT '', `rag_soc_cliente` TEXT NOT NULL DEFAULT '', `perc_iva` TEXT NOT NULL DEFAULT '',`reparto` TEXT NOT NULL DEFAULT '', `nome_comanda` TEXT NOT NULL DEFAULT '', `stampata_sn` TEXT NOT NULL DEFAULT '', `numero_conto` TEXT NOT NULL DEFAULT '', `dest_stampa` TEXT NOT NULL DEFAULT '', `portata` TEXT NOT NULL DEFAULT '', `categoria` TEXT NOT NULL DEFAULT '', `prog_inser` TEXT NOT NULL DEFAULT '', `nodo` TEXT NOT NULL DEFAULT '', `desc_art` TEXT NOT NULL DEFAULT '', `prezzo_un` TEXT NOT NULL DEFAULT '', `prezzo_varianti_aggiuntive` TEXT NOT NULL DEFAULT '', `prezzo_varianti_maxi` TEXT NOT NULL DEFAULT '', `prezzo_maxi_prima` TEXT NOT NULL DEFAULT '', `quantita` TEXT NOT NULL DEFAULT '', `contiene_variante` TEXT NOT NULL DEFAULT '', `tipo_impasto` TEXT NOT NULL DEFAULT '',`BIS` TEXT NOT NULL DEFAULT '')");
                                                            alasql("CREATE TABLE oggetto_conto_2 ( `ord` TEXT NOT NULL DEFAULT '', `cod_promo` TEXT NOT NULL DEFAULT '', `fiscalizzata_sn` TEXT NOT NULL DEFAULT '', `ora_` TEXT NOT NULL DEFAULT '', `rag_soc_cliente` TEXT NOT NULL DEFAULT '', `perc_iva` TEXT NOT NULL DEFAULT '',`reparto` TEXT NOT NULL DEFAULT '', `nome_comanda` TEXT NOT NULL DEFAULT '', `stampata_sn` TEXT NOT NULL DEFAULT '', `numero_conto` TEXT NOT NULL DEFAULT '', `dest_stampa` TEXT NOT NULL DEFAULT '', `portata` TEXT NOT NULL DEFAULT '', `categoria` TEXT NOT NULL DEFAULT '', `prog_inser` TEXT NOT NULL DEFAULT '', `nodo` TEXT NOT NULL DEFAULT '', `desc_art` TEXT NOT NULL DEFAULT '', `prezzo_un` TEXT NOT NULL DEFAULT '', `prezzo_varianti_aggiuntive` TEXT NOT NULL DEFAULT '', `prezzo_varianti_maxi` TEXT NOT NULL DEFAULT '', `prezzo_maxi_prima` TEXT NOT NULL DEFAULT '', `quantita` TEXT NOT NULL DEFAULT '', `contiene_variante` TEXT NOT NULL DEFAULT '', `tipo_impasto` TEXT NOT NULL DEFAULT '',`BIS` TEXT NOT NULL DEFAULT '')");
                                                            alasql("CREATE TABLE oggetto_comanda ( `ord` TEXT NOT NULL DEFAULT '', `cod_articolo` TEXT NOT NULL DEFAULT '', `perc_iva` TEXT NOT NULL DEFAULT '', `nome_comanda` TEXT NOT NULL DEFAULT '', `rag_soc_cliente` TEXT NOT NULL DEFAULT '', `perc_iva` TEXT NOT NULL DEFAULT '',`reparto` TEXT NOT NULL DEFAULT '', `stampata_sn` TEXT NOT NULL DEFAULT '', `numero_conto` TEXT NOT NULL DEFAULT '', `dest_stampa` TEXT NOT NULL DEFAULT '', `portata` TEXT NOT NULL DEFAULT '', `,categoria` TEXT NOT NULL DEFAULT '', `,prog_inser` TEXT NOT NULL DEFAULT '', `nodo` TEXT NOT NULL DEFAULT '', `desc_art` TEXT NOT NULL DEFAULT '', `prezzo_un` TEXT NOT NULL DEFAULT '', `quantita` TEXT NOT NULL DEFAULT '',  `contiene_variante` TEXT NOT NULL DEFAULT '',`BIS` TEXT NOT NULL DEFAULT '')");


                                                            comanda.sincro.query("CREATE TABLE IF NOT EXISTS progressivo_asporto ( `id` INTEGER PRIMARY KEY AUTOINCREMENT, `numero` TEXT )", function () {
                                                                comanda.sincro.query("CREATE TABLE IF NOT EXISTS cambi_listino ( `giorno` TEXT NOT NULL DEFAULT '', `mese` TEXT NOT NULL DEFAULT '', `anno` TEXT NOT NULL DEFAULT '', `id_prodotto` TEXT NOT NULL DEFAULT '', `prezzo_1` TEXT NOT NULL DEFAULT '', `prezzo_2` TEXT NOT NULL DEFAULT '', `prezzo_3` TEXT NOT NULL DEFAULT '', `prezzo_4` TEXT NOT NULL DEFAULT '' )", function () {
                                                                    comanda.sincro.sincronizzazione_alasql('dati_servizio', 1, function () {
                                                                        comanda.sincro.sincronizzazione_alasql('gruppi_statistici', 1, function () {
                                                                            //Sincronizza subito i database da remoto, se possibile, altrimenti tiene i vecchi
                                                                            comanda.sincro.sincronizzazione_alasql('socket_listeners', 1, function () {
                                                                                comanda.sincro.sincronizzazione_alasql('nomi_portate', 1, function () {
                                                                                    nomi_portate_txt(function () {


                                                                                        leggi_settaggi(comanda.file_settaggi, "nomi_stampanti", "SERVIZIO", "id,ncop,ns,dest", "id,numero," + comanda.lingua_stampa + ",nome", 15, function () {

                                                                                            var array_corrispondenze = new Array();
                                                                                            array_corrispondenze["CAT_VAR"] = new Array();
                                                                                            array_corrispondenze["CAT_VAR"].colonna_db = "cat_var";
                                                                                            array_corrispondenze["CAT_VAR"].valori = new Array();

                                                                                            array_corrispondenze["CAT_VAR"].valori["null"] = "";

                                                                                            array_corrispondenze["CAT"] = new Array();
                                                                                            array_corrispondenze["CAT"].colonna_db = "id";
                                                                                            array_corrispondenze["DEST"] = new Array();
                                                                                            array_corrispondenze["DEST"].colonna_db = "dest_stampa";
                                                                                            array_corrispondenze["DES"] = new Array();
                                                                                            array_corrispondenze["DES"].colonna_db = "descrizione";
                                                                                            array_corrispondenze["PORTATA"] = new Array();
                                                                                            array_corrispondenze["PORTATA"].colonna_db = "portata";
                                                                                            array_corrispondenze["ESCL_SERVER"] = new Array();
                                                                                            array_corrispondenze["ESCL_SERVER"].colonna_db = "cod_articolo";
                                                                                            array_corrispondenze["ESCL_PDA"] = new Array();
                                                                                            array_corrispondenze["ESCL_PDA"].colonna_db = "escl_pda";
                                                                                            array_corrispondenze["AGG_GIAC"] = new Array();
                                                                                            array_corrispondenze["AGG_GIAC"].colonna_db = "var_aperte";
                                                                                            array_corrispondenze["ORD"] = new Array();
                                                                                            array_corrispondenze["ORD"].colonna_db = "ordinamento";
                                                                                            array_corrispondenze["IMG"] = new Array();
                                                                                            array_corrispondenze["IMG"].colonna_db = "img_tasto";

                                                                                            var array_campi_fissi = new Array();
                                                                                            array_campi_fissi.push({colonna: "cat_var", valore: null});
                                                                                            array_campi_fissi.push({colonna: "dest_stampa", valore: null});
                                                                                            array_campi_fissi.push({colonna: "dest_stampa_2", valore: null});
                                                                                            comanda.xml = new ClasseXML("/" + comanda.directory_maga + "/" + comanda.nome_menu_xml + ".XML", "CATEGORIE", "categorie", array_corrispondenze, array_campi_fissi, true, true, true);
                                                                                            comanda.xml.importa(function () {




                                                                                                comanda.sincro.sincronizzazione_alasql('lingue', 1, function () {
                                                                                                    comanda.sincro.sincronizzazione_alasql('clienti', 1, function () {
                                                                                                        comanda.sincro.sincronizzazione_alasql('operatori', 1, function () {

                                                                                                            //DISCONNETTE AD OGNI AVVIO
                                                                                                            disconnetti();

                                                                                                            comanda.sincro.sincronizzazione_alasql('comanda', 1, function () {

                                                                                                                comanda.sincro.sincronizzazione_alasql('tavoli', 1, function () {

                                                                                                                    comanda.sincro.sincronizzazione_alasql('tavoli_uniti', 1, function () {

                                                                                                                        comanda.sincro.sincronizzazione_alasql('terminali', 1, function () {


                                                                                                                            comanda.sincro.sincronizzazione_alasql('tabella_iva', 1, function () {

                                                                                                                                comanda.sincro.sincronizzazione_alasql('tabella_sconti_buoni', 1, function () {

                                                                                                                                    var array_corrispondenze = new Array();
                                                                                                                                    array_corrispondenze["DES"] = new Array();
                                                                                                                                    array_corrispondenze["DES"].colonna_db = "descrizione";

                                                                                                                                    array_corrispondenze["CAT"] = new Array();
                                                                                                                                    array_corrispondenze["CAT"].colonna_db = "categoria";
                                                                                                                                    array_corrispondenze["ART"] = new Array();
                                                                                                                                    array_corrispondenze["ART"].colonna_db = new Array();
                                                                                                                                    array_corrispondenze["ART"].colonna_db.push("id");
                                                                                                                                    array_corrispondenze["ART"].colonna_db.push("cod_articolo");
                                                                                                                                    array_corrispondenze["ORD"] = new Array();
                                                                                                                                    array_corrispondenze["ORD"].colonna_db = "posizione";
                                                                                                                                    array_corrispondenze["COEF"] = new Array();
                                                                                                                                    array_corrispondenze["COEF"].colonna_db = "ordinamento";
                                                                                                                                    array_corrispondenze["CATV"] = new Array();
                                                                                                                                    array_corrispondenze["CATV"].colonna_db = "cat_varianti";
                                                                                                                                    array_corrispondenze["CATV"].valori = new Array();
                                                                                                                                    /*if (comanda.variante_libera === "S") {
                                                                                                                                     array_corrispondenze["CATV"].valori["undefined"] = "VVV";
                                                                                                                                     array_corrispondenze["CATV"].valori["null"] = "VVV";
                                                                                                                                     array_corrispondenze["CATV"].valori[""] = "VVV";
                                                                                                                                     } else {*/
                                                                                                                                    array_corrispondenze["CATV"].valori["null"] = "";
                                                                                                                                    /*}*/
                                                                                                                                    array_corrispondenze["CATR"] = new Array();
                                                                                                                                    array_corrispondenze["CATR"].colonna_db = "cat_ricorrente";
                                                                                                                                    array_corrispondenze["IVA"] = new Array();
                                                                                                                                    array_corrispondenze["IVA"].colonna_db = "perc_iva_base";
                                                                                                                                    array_corrispondenze["REP"] = new Array();
                                                                                                                                    array_corrispondenze["REP"].colonna_db = "perc_iva_takeaway";
                                                                                                                                    array_corrispondenze["DEST"] = new Array();
                                                                                                                                    array_corrispondenze["DEST"].colonna_db = "dest_st_1";
                                                                                                                                    array_corrispondenze["DEST2"] = new Array();
                                                                                                                                    array_corrispondenze["DEST2"].colonna_db = "dest_st_2";
                                                                                                                                    //ERA DISABIITATO
                                                                                                                                    array_corrispondenze["ULTPORT"] = new Array();
                                                                                                                                    array_corrispondenze["ULTPORT"].colonna_db = "ultima_portata";

                                                                                                                                    if (risultato[0].listino_palmari !== undefined) {
                                                                                                                                        switch (risultato[0].listino_palmari) {
                                                                                                                                            case "2":
                                                                                                                                                array_corrispondenze["PRZ1"] = new Array();
                                                                                                                                                array_corrispondenze["PRZ1"].colonna_db = "prezzo_2";

                                                                                                                                                array_corrispondenze["PRZ2"] = new Array();
                                                                                                                                                array_corrispondenze["PRZ2"].colonna_db = "prezzo_1";
                                                                                                                                                array_corrispondenze["PRZ3"] = new Array();
                                                                                                                                                array_corrispondenze["PRZ3"].colonna_db = "prezzo_3";
                                                                                                                                                array_corrispondenze["PRZ4"] = new Array();
                                                                                                                                                array_corrispondenze["PRZ4"].colonna_db = "prezzo_4";
                                                                                                                                                break;
                                                                                                                                            case "3":
                                                                                                                                                array_corrispondenze["PRZ1"] = new Array();
                                                                                                                                                array_corrispondenze["PRZ1"].colonna_db = "prezzo_3";

                                                                                                                                                array_corrispondenze["PRZ2"] = new Array();
                                                                                                                                                array_corrispondenze["PRZ2"].colonna_db = "prezzo_2";
                                                                                                                                                array_corrispondenze["PRZ3"] = new Array();
                                                                                                                                                array_corrispondenze["PRZ3"].colonna_db = "prezzo_1";
                                                                                                                                                array_corrispondenze["PRZ4"] = new Array();
                                                                                                                                                array_corrispondenze["PRZ4"].colonna_db = "prezzo_4";
                                                                                                                                                break;
                                                                                                                                            case "4":
                                                                                                                                                array_corrispondenze["PRZ1"] = new Array();
                                                                                                                                                array_corrispondenze["PRZ1"].colonna_db = "prezzo_4";

                                                                                                                                                array_corrispondenze["PRZ2"] = new Array();
                                                                                                                                                array_corrispondenze["PRZ2"].colonna_db = "prezzo_2";
                                                                                                                                                array_corrispondenze["PRZ3"] = new Array();
                                                                                                                                                array_corrispondenze["PRZ3"].colonna_db = "prezzo_3";
                                                                                                                                                array_corrispondenze["PRZ4"] = new Array();
                                                                                                                                                array_corrispondenze["PRZ4"].colonna_db = "prezzo_1";
                                                                                                                                                break;
                                                                                                                                            default:
                                                                                                                                                array_corrispondenze["PRZ1"] = new Array();
                                                                                                                                                array_corrispondenze["PRZ1"].colonna_db = "prezzo_1";

                                                                                                                                                array_corrispondenze["PRZ2"] = new Array();
                                                                                                                                                array_corrispondenze["PRZ2"].colonna_db = "prezzo_2";
                                                                                                                                                array_corrispondenze["PRZ3"] = new Array();
                                                                                                                                                array_corrispondenze["PRZ3"].colonna_db = "prezzo_3";
                                                                                                                                                array_corrispondenze["PRZ4"] = new Array();
                                                                                                                                                array_corrispondenze["PRZ4"].colonna_db = "prezzo_4";
                                                                                                                                        }
                                                                                                                                    }


                                                                                                                                    array_corrispondenze["AGG_GIAC"] = new Array();
                                                                                                                                    array_corrispondenze["AGG_GIAC"].colonna_db = "giacenza_flash";
                                                                                                                                    array_corrispondenze["GREALE"] = new Array();
                                                                                                                                    array_corrispondenze["GREALE"].colonna_db = "giacenza_reale";
                                                                                                                                    array_corrispondenze["GIAC"] = new Array();
                                                                                                                                    array_corrispondenze["GIAC"].colonna_db = "giacenza";
                                                                                                                                    array_corrispondenze["COSTO"] = new Array();
                                                                                                                                    array_corrispondenze["COSTO"].colonna_db = "costo_un";
                                                                                                                                    array_corrispondenze["colore_tasto"] = new Array();
                                                                                                                                    array_corrispondenze["colore_tasto"].colonna_db = "rgb";
                                                                                                                                    array_corrispondenze["PORTATA"] = new Array();
                                                                                                                                    array_corrispondenze["PORTATA"].colonna_db = "portata";
                                                                                                                                    /*array_corrispondenze["PESO"] = new Array();
                                                                                                                                     array_corrispondenze["PESO"].colonna_db = "peso_ums";*/
                                                                                                                                    array_corrispondenze["DES_L"] = new Array();
                                                                                                                                    array_corrispondenze["DES_L"].colonna_db = "desc_lunga";
                                                                                                                                    array_corrispondenze["IMG"] = new Array();
                                                                                                                                    array_corrispondenze["IMG"].colonna_db = "immagine";
                                                                                                                                    array_corrispondenze["SRV"] = new Array();
                                                                                                                                    array_corrispondenze["SRV"].colonna_db = "perc_servizio";
                                                                                                                                    array_corrispondenze["LIB1"] = new Array();
                                                                                                                                    array_corrispondenze["LIB1"].colonna_db = "red";
                                                                                                                                    array_corrispondenze["LIB2"] = new Array();
                                                                                                                                    array_corrispondenze["LIB2"].colonna_db = "green";
                                                                                                                                    array_corrispondenze["LIB3"] = new Array();
                                                                                                                                    array_corrispondenze["LIB3"].colonna_db = "blue";

                                                                                                                                    var array_campi_fissi = new Array();


                                                                                                                                    //array_campi_fissi.push({colonna: "ultima_portata", valore: ""});
                                                                                                                                    array_campi_fissi.push({colonna: "peso_ums", valore: "0"});
                                                                                                                                    array_campi_fissi.push({colonna: "pagina", valore: "1"});
                                                                                                                                    array_campi_fissi.push({colonna: "colore_tasto", valore: "silver"});
                                                                                                                                    //array_campi_fissi.push({colonna: "ordinamento", valore: " "});
                                                                                                                                    array_campi_fissi.push({colonna: "posizione", valore: "999"});
                                                                                                                                    array_campi_fissi.push({colonna: "cod_promo", valore: ""});
                                                                                                                                    array_campi_fissi.push({colonna: "qta_fissa", valore: ""});
                                                                                                                                    array_campi_fissi.push({colonna: "spazio_forno", valore: ""});
                                                                                                                                    array_campi_fissi.push({colonna: "listino_tavoli", valore: ""});
                                                                                                                                    array_campi_fissi.push({colonna: "listino_bar", valore: ""});
                                                                                                                                    array_campi_fissi.push({colonna: "listino_asporto", valore: ""});
                                                                                                                                    array_campi_fissi.push({colonna: "listino_continuo", valore: ""});
                                                                                                                                    //array_campi_fissi.push({colonna: "portata", valore: "N"});
                                                                                                                                    array_campi_fissi.push({colonna: "prezzo_varianti_aggiuntive", valore: ""});
                                                                                                                                    array_campi_fissi.push({colonna: "prezzo_maxi", valore: ""});
                                                                                                                                    array_campi_fissi.push({colonna: "prezzo_maxi_prima", valore: ""});
                                                                                                                                    array_campi_fissi.push({colonna: "prezzo_varianti_maxi", valore: ""});
                                                                                                                                    array_campi_fissi.push({colonna: "abilita_riepilogo", valore: ""});
                                                                                                                                    array_campi_fissi.push({colonna: "ricetta", valore: ""});
                                                                                                                                    array_campi_fissi.push({colonna: "automodifica_prezzo", valore: ""});
                                                                                                                                    array_campi_fissi.push({colonna: "prezzo_variante_meno", valore: ""});
                                                                                                                                    array_campi_fissi.push({colonna: "gruppo", valore: ""});

                                                                                                                                    array_campi_fissi.push({colonna: "prezzo_fattorino1_norm", valore: ""});
                                                                                                                                    array_campi_fissi.push({colonna: "prezzo_fattorino2_norm", valore: ""});
                                                                                                                                    array_campi_fissi.push({colonna: "prezzo_fattorino3_norm", valore: ""});
                                                                                                                                    array_campi_fissi.push({colonna: "prezzo_fattorino1_maxi", valore: ""});
                                                                                                                                    array_campi_fissi.push({colonna: "prezzo_fattorino2_maxi", valore: ""});
                                                                                                                                    array_campi_fissi.push({colonna: "prezzo_fattorino3_maxi", valore: ""});
                                                                                                                                    array_campi_fissi.push({colonna: "costo", valore: ""});
                                                                                                                                    array_campi_fissi.push({colonna: "reparto_servizi", valore: ""});
                                                                                                                                    array_campi_fissi.push({colonna: "reparto_beni", valore: ""});




                                                                                                                                    comanda.xml = new ClasseXML("/" + comanda.directory_maga + "/" + comanda.nome_menu_xml + ".XML", "BIBITE", "prodotti", array_corrispondenze, array_campi_fissi, true, true, true, true);
                                                                                                                                    comanda.xml.importa(function () {

                                                                                                                                        /*comanda.sincro.query("CREATE INDEX IF NOT EXISTS  QTAP_c ON comanda (QTAP ASC);", function () {
                                                                                                                                         comanda.sincro.query("CREATE INDEX IF NOT EXISTS  bis_c ON comanda (BIS ASC);", function () {
                                                                                                                                         comanda.sincro.query("CREATE INDEX IF NOT EXISTS  cont_var_c ON comanda (contiene_variante ASC);", function () {
                                                                                                                                         comanda.sincro.query("CREATE INDEX IF NOT EXISTS  desc_art_c ON comanda (desc_art ASC);", function () {
                                                                                                                                         comanda.sincro.query("CREATE INDEX IF NOT EXISTS  fiscalesospeso_c ON comanda (fiscale_sospeso ASC);", function () {
                                                                                                                                         comanda.sincro.query("CREATE INDEX IF NOT EXISTS  id_c ON comanda (id ASC);", function () {
                                                                                                                                         comanda.sincro.query("CREATE INDEX IF NOT EXISTS  id_p ON prodotti (id ASC);", function () {
                                                                                                                                         comanda.sincro.query("CREATE INDEX IF NOT EXISTS  n_conto_c ON comanda (numero_conto ASC);", function () {
                                                                                                                                         comanda.sincro.query("CREATE INDEX IF NOT EXISTS  ntav_c ON comanda (ntav_comanda ASC);", function () {
                                                                                                                                         comanda.sincro.query("CREATE INDEX IF NOT EXISTS  posizione_c ON comanda (posizione ASC);", function () {
                                                                                                                                         comanda.sincro.query("CREATE INDEX IF NOT EXISTS  st_record_c ON comanda (stato_record ASC);", function () {
                                                                                                                                         comanda.sincro.query("CREATE INDEX IF NOT EXISTS  stampata_sn_c ON comanda (stampata_sn ASC);", function () {*/



                                                                                                                                        //CREATE INDEX IF NOT EXISTS ECC... PER PRODOTTI E COMANDA

                                                                                                                                        comanda.sincro.sincronizzazione_alasql('impostazioni_fiscali', 1, function () {

                                                                                                                                            comanda.sincro.sincronizzazione_alasql('settaggi_profili', 1, function () {
                                                                                                                                                leggi_coperti(function () {
                                                                                                                                                    var testo_query = 'CREATE TABLE IF NOT EXISTS tavoli (id,classe,numero,abilitato,altezza,larghezza,pos_x,pos_y,colore,posti,ora_apertura_tavolo,ora_ultima_comanda);';
                                                                                                                                                    comanda.sincro.query(testo_query, function () {


                                                                                                                                                        var testo_query = 'SELECT * FROM impostazioni_fiscali where id=' + comanda.folder_number + '  limit 1;';
                                                                                                                                                        comanda.sincro.query(testo_query, function (risultato) {


                                                                                                                                                            comanda.avviso_righe_prezzo_zero = risultato[0].righe_zero;



                                                                                                                                                            if (risultato[0].tasto_visu_varianti === "N") {
                                                                                                                                                                $(".visu_varianti").remove();
                                                                                                                                                            }


                                                                                                                                                            if (risultato[0].tasto_visu_varianti_menopiu === "S") {
                                                                                                                                                                $(".variante_piumeno").remove();
                                                                                                                                                            }


                                                                                                                                                            if (risultato[0].tasto_visu_varianti_piumeno === "S") {
                                                                                                                                                                $(".variante_menopiu").remove();
                                                                                                                                                            }



                                                                                                                                                            comanda.multiquantita = "N";
                                                                                                                                                            if (risultato[0].multiquantita === "S") {
                                                                                                                                                                comanda.multiquantita = "S";
                                                                                                                                                            }

                                                                                                                                                            comanda.clona_diretto = "N";
                                                                                                                                                            if (risultato[0].clona_diretto === "S") {
                                                                                                                                                                comanda.clona_diretto = "S";
                                                                                                                                                            }

                                                                                                                                                            comanda.tastiera_ricerca_partenza_numerica = risultato[0].tastiera_ricerca_partenza_numerica;

                                                                                                                                                            if (comanda.tastiera_ricerca_partenza_numerica === "S") {
                                                                                                                                                                $("#btn_ricerca_tastiera").text("123");
                                                                                                                                                            }

                                                                                                                                                            comanda.tastiera_ricerca_filtro_plu = risultato[0].tastiera_ricerca_filtro_plu;
                                                                                                                                                            comanda.tastiera_ricerca_tasto_verde_batte_articolo = risultato[0].tastiera_ricerca_tasto_verde_batte_articolo;

                                                                                                                                                            comanda.ricerca_alfabetica = risultato[0].ricerca_alfabetica;

                                                                                                                                                            comanda.ricerca_tastiera = risultato[0].ricerca_tastiera;


                                                                                                                                                            $("#btn_ricerca_tastiera").hide();

                                                                                                                                                            if (comanda.ricerca_tastiera === "S") {
                                                                                                                                                                $("#btn_ricerca_tastiera").show();
                                                                                                                                                            }

                                                                                                                                                            comanda.filtro_ricerca_generico = risultato[0].filtro_ricerca_generico;


                                                                                                                                                            comanda.grandezza_font_articoli = '3.7';
                                                                                                                                                            if (risultato[0].grandezza_font_articoli !== 0 && risultato[0].grandezza_font_articoli !== null && risultato[0].grandezza_font_articoli !== 'NULL' && risultato[0].grandezza_font_articoli !== '' && risultato[0].grandezza_font_articoli !== '0' && risultato[0].grandezza_font_articoli !== 0)
                                                                                                                                                            {
                                                                                                                                                                comanda.grandezza_font_articoli = risultato[0].grandezza_font_articoli;
                                                                                                                                                                if (comanda.iOS === true) {
                                                                                                                                                                    comanda.grandezza_font_articoli = (parseFloat(comanda.grandezza_font_articoli) / 2).toString();
                                                                                                                                                                }
                                                                                                                                                            }
                                                                                                                                                            comanda.grandezza_font_categorie = '3.5';
                                                                                                                                                            if (risultato[0].grandezza_font_categorie !== 0 && risultato[0].grandezza_font_categorie !== null && risultato[0].grandezza_font_categorie !== 'NULL' && risultato[0].grandezza_font_categorie !== '' && risultato[0].grandezza_font_categorie !== '0' && risultato[0].grandezza_font_categorie !== 0)
                                                                                                                                                            {
                                                                                                                                                                comanda.grandezza_font_categorie = risultato[0].grandezza_font_categorie;
                                                                                                                                                                if (comanda.iOS === true) {
                                                                                                                                                                    comanda.grandezza_font_categorie = (parseFloat(comanda.grandezza_font_categorie) / 1.5).toString();
                                                                                                                                                                }
                                                                                                                                                            }
                                                                                                                                                            comanda.grandezza_font_tavoli = '7';
                                                                                                                                                            if (risultato[0].grandezza_font_tavoli !== 0 && risultato[0].grandezza_font_tavoli !== null && risultato[0].grandezza_font_tavoli !== 'NULL' && risultato[0].grandezza_font_tavoli !== '' && risultato[0].grandezza_font_tavoli !== '0' && risultato[0].grandezza_font_tavoli !== 0)
                                                                                                                                                            {
                                                                                                                                                                comanda.grandezza_font_tavoli = risultato[0].grandezza_font_tavoli;
                                                                                                                                                                if (comanda.iOS === true) {
                                                                                                                                                                    comanda.grandezza_tavoli_altezza = (parseFloat(comanda.grandezza_font_tavoli) / 3).toString();
                                                                                                                                                                    comanda.grandezza_tavoli_larghezza = (parseFloat(comanda.grandezza_font_tavoli) / 2.5).toString();
                                                                                                                                                                    comanda.grandezza_font_tavoli = (parseFloat(comanda.grandezza_font_tavoli) / 4).toString();
                                                                                                                                                                }
                                                                                                                                                            }

                                                                                                                                                            comanda.varianti_riunite = risultato[0].varianti_riunite;

                                                                                                                                                            comanda.touch_articolo_conto = risultato[0].touch_articolo_conto;

                                                                                                                                                            comanda.lingua_stampa = risultato[0].lingua_stampa;

                                                                                                                                                            lng(comanda.lingua);
                                                                                                                                                            //corrispondenze_stampanti(comanda.lingua);
                                                                                                                                                            //corrispondenze_portate(comanda.lingua);
                                                                                                                                                            comanda.valore_coperto = risultato[0].valore_coperto;
                                                                                                                                                            comanda.visualizzazione_coperti = risultato[0].visualizzazione_coperti;
                                                                                                                                                            comanda.coperti_obbligatorio = risultato[0].coperti_obbligatorio;
                                                                                                                                                            comanda.avviso_coperti_comanda = risultato[0].avviso_coperti_comanda;

                                                                                                                                                            leggi_utenti(function () {

                                                                                                                                                                selezione_dispositivo();

                                                                                                                                                                controllo_password_db(function (r) {
                                                                                                                                                                    if (r === false) {
                                                                                                                                                                        testaLogin(function (s) {
                                                                                                                                                                            if (s === false)
                                                                                                                                                                            {
                                                                                                                                                                                selezione_operatore();
                                                                                                                                                                            }
                                                                                                                                                                        });
                                                                                                                                                                    }
                                                                                                                                                                });



                                                                                                                                                                if (comanda.coperti_necessari !== true)
                                                                                                                                                                {
                                                                                                                                                                    $('.tasti_coperti_indietro_cel').remove();
                                                                                                                                                                }

                                                                                                                                                                corrispondenze_stampanti(comanda.lingua);
                                                                                                                                                                corrispondenze_portate(comanda.lingua);



                                                                                                                                                                if (comanda.mobile === true) {
                                                                                                                                                                    ordina_alfabeto();
                                                                                                                                                                }


                                                                                                                                                                var style = "";
                                                                                                                                                                var style_2 = "";
                                                                                                                                                                if (comanda.layout_destri === "S") {
                                                                                                                                                                    style += '#tab_sx { left:0px !important; } .elenco_prodotti li>span { padding-left: 170px;text-align:left !important; }';
                                                                                                                                                                    if (comanda.categorie_fullscreen === "S") {
                                                                                                                                                                        style_2 = "#tab_sx { position:fixed !important; max-height: 73.9vh !important; top: 8.2vh !important; }";
                                                                                                                                                                        style_2 += "#tabella_ultime_battiture .ultime_battiture { width: 60%; margin-left: 40%; } #tabella_ultime_battiture .ultime_battiture tr td:nth-child(4){ display:none; }";
                                                                                                                                                                    }

                                                                                                                                                                } else {
                                                                                                                                                                    style += '.elenco_prodotti li>span { padding-left: 20px;text-align:left !important; }';
                                                                                                                                                                    if (comanda.categorie_fullscreen === "S") {
                                                                                                                                                                        style_2 = "#tab_sx { position:fixed !important; max-height: 73.9vh !important; top: 8.2vh !important; }";
                                                                                                                                                                        style_2 += "#tabella_ultime_battiture .ultime_battiture { width: 60%; } #tabella_ultime_battiture .ultime_battiture tr td:nth-child(4){ display:none; }";
                                                                                                                                                                    }


                                                                                                                                                                }

                                                                                                                                                                if (comanda.ultime_battiture_grandi === "S") {
                                                                                                                                                                    style += '#tabella_ultime_battiture { height: 30.6vh !important;} #prodotti { height: 42vh;}';
                                                                                                                                                                    if (comanda.categorie_fullscreen === "S") {
                                                                                                                                                                        style_2 = "#tab_sx { position:fixed !important; max-height: 72.9vh !important; top: 8.2vh !important; }";
                                                                                                                                                                        if (comanda.layout_destri === "S") {
                                                                                                                                                                            style_2 += "#tabella_ultime_battiture .ultime_battiture { width: 60%; margin-left: 40%; } #tabella_ultime_battiture .ultime_battiture tr td:nth-child(4){ display:none; }";
                                                                                                                                                                        } else {
                                                                                                                                                                            style_2 += "#tabella_ultime_battiture .ultime_battiture { width: 60%; } #tabella_ultime_battiture .ultime_battiture tr td:nth-child(4){ display:none; }";
                                                                                                                                                                        }
                                                                                                                                                                    } else {
                                                                                                                                                                        style_2 = "#tab_sx { max-height: 42vh !important; }";
                                                                                                                                                                    }



                                                                                                                                                                }

                                                                                                                                                                var stile = "<style>" + style + "" + style_2 + "</style>";

                                                                                                                                                                $('html > head').append($(stile));

                                                                                                                                                                //SE LA STORICIZZAZIONE NON E' AUTOMATICA CI VUOLE IL CHIUDI TAVOLO
                                                                                                                                                                if (comanda.storicizzazione_auto === 'S') {                                                                                                                //SE CE LA COMANDA PIU CONTO SERVE SOLO IL TASTO COMANDA
                                                                                                                                                                    if (comanda.comanda_conto === 'S') {
                                                                                                                                                                        $('.layout_comanda_conto_stor').show();
                                                                                                                                                                    } else if (comanda.comanda_conto === 'N') {
                                                                                                                                                                        $('.layout_comanda_senza_conto_stor').show();
                                                                                                                                                                    }
                                                                                                                                                                } else if (comanda.storicizzazione_auto === 'N') {
                                                                                                                                                                    //SE CE LA COMANDA PIU CONTO SERVE SOLO IL TASTO COMANDA
                                                                                                                                                                    if (comanda.comanda_conto === 'S') {
                                                                                                                                                                        $('.layout_comanda_conto_no_stor').show();
                                                                                                                                                                    } else if (comanda.comanda_conto === 'N') {
                                                                                                                                                                        $('.layout_comanda_senza_conto_no_stor').show();
                                                                                                                                                                    }
                                                                                                                                                                }

                                                                                                                                                                //Fa il servizio


                                                                                                                                                                //socket_init();

                                                                                                                                                                aggiornamento_tavoli_xml();



                                                                                                                                                                lngstm(comanda.lingua_stampa, function () {
                                                                                                                                                                    comanda.funzionidb.elenco_categorie("list", "1", "0", "#tab_sx");
                                                                                                                                                                    comanda.funzionidb.elenco_categorie("select", "1", "1", ".elenco_categorie_select");
                                                                                                                                                                    dbgfunzioni();

                                                                                                                                                                    if (comanda.discoteca === true) {
                                                                                                                                                                        $(".nome_operatore").remove();
                                                                                                                                                                        leggi_nomi_tavoli();
                                                                                                                                                                    } else
                                                                                                                                                                    {
                                                                                                                                                                        $(".tessere").remove();
                                                                                                                                                                        leggi_nomi_tavoli();
                                                                                                                                                                    }


                                                                                                                                                                    //RIEMPIO LA TENDINA DELL'IVA
                                                                                                                                                                    var query = "SELECT aliquota FROM tabella_iva WHERE lingua='" + comanda.lingua_stampa + "' ORDER BY cast(aliquota as int) DESC;";
                                                                                                                                                                    var opzione = '';

                                                                                                                                                                    comanda.sincro.query(query, function (results) {

                                                                                                                                                                        results.forEach(function (row) {
                                                                                                                                                                            opzione = "<option value=\"" + row.aliquota + "\">" + row.aliquota + "</option>";
                                                                                                                                                                            $('select[name="iva"]').append(opzione);
                                                                                                                                                                        });
                                                                                                                                                                    });
                                                                                                                                                                    //FINE TENDINA IVA


                                                                                                                                                                    if (comanda.lingua_stampa === 'deutsch')
                                                                                                                                                                    {
                                                                                                                                                                        $('.elimina_tedeschi').remove();
                                                                                                                                                                    }






                                                                                                                                                                    if (comanda.compatibile_xml === true) {

                                                                                                                                                                        if (comanda.controlla_giacenza !== true) {
                                                                                                                                                                            $("#tasto_vedi_giacenze").remove();
                                                                                                                                                                        }



                                                                                                                                                                        setInterval(function () {
                                                                                                                                                                            test_portata();
                                                                                                                                                                        }, 2000);

                                                                                                                                                                        $('.tasto_impostazioni').remove();

                                                                                                                                                                        if (comanda.lingua_stampa === "deutsch") {
                                                                                                                                                                            $('#tasto_uscita_tavoli').remove();
                                                                                                                                                                        }
                                                                                                                                                                        $('#versione_software').html('v:' + comanda.versione);

                                                                                                                                                                        if (comanda.split_abilitato !== true) {
                                                                                                                                                                            $('.bottone_split').remove();
                                                                                                                                                                        }

                                                                                                                                                                        //DI DEFAULT
                                                                                                                                                                        $('.adattamento_colonna_prodotto_libero').addClass('col-xs-8');
                                                                                                                                                                        $('#btn_prodotto_libero').parent().remove();

                                                                                                                                                                        /*if (comanda.p_libero_abilitato === 'N') {
                                                                                                                                                                         $('#btn_prodotto_libero').parent().remove();
                                                                                                                                                                         $('.adattamento_colonna_prodotto_libero').addClass('col-xs-8');
                                                                                                                                                                         } else
                                                                                                                                                                         {
                                                                                                                                                                         $('.adattamento_colonna_prodotto_libero').addClass('col-xs-6');
                                                                                                                                                                         }*/

                                                                                                                                                                        if (comanda.tipo_ricerca_articolo === 'NUMERICA') {
                                                                                                                                                                            $('#tab_sx').remove();
                                                                                                                                                                            $('.alfabeto').remove();
                                                                                                                                                                            $('#contenitore_id_prodotti').removeClass('col-xs-10');
                                                                                                                                                                            $('#contenitore_id_prodotti').addClass('col-xs-12');
                                                                                                                                                                        }

                                                                                                                                                                        setTimeout(function () {
                                                                                                                                                                            layout_tasti_tablet_ibrido(true);
                                                                                                                                                                            $('body').removeClass('loading');



                                                                                                                                                                        }, 3000);

                                                                                                                                                                        if (comanda.creatore_piantine === true) {





                                                                                                                                                                            make_draggable();

                                                                                                                                                                            $('body').css('pointer-events', '');
                                                                                                                                                                            $('body').css('opacity', '');

                                                                                                                                                                            alert("PUOI COMINCIARE");



                                                                                                                                                                            $('#nuovo_tavolo').on(comanda.eventino, function () {
                                                                                                                                                                                nuovo_tavolo();
                                                                                                                                                                            });

                                                                                                                                                                            $('#cancella_tutti_tavoli').on(comanda.eventino, function () {
                                                                                                                                                                                cancella_tutti_tavoli();
                                                                                                                                                                            });

                                                                                                                                                                            $('div.tavolo.rett-min,div.tavolo.cerc-min').on(comanda.eventino, function () {

                                                                                                                                                                                $('div.tavolo.rett-min,div.tavolo.cerc-min').each(function () {
                                                                                                                                                                                    $(this).removeClass('forma_selezionata');
                                                                                                                                                                                });

                                                                                                                                                                                $(this).addClass('forma_selezionata');
                                                                                                                                                                            });
                                                                                                                                                                        }

                                                                                                                                                                        //comanda.abilita_log_palmare = false;
                                                                                                                                                                        comanda.id_tablet = "29";



                                                                                                                                                                        var id_temp = leggi_get_url('id');

                                                                                                                                                                        if (id_temp !== undefined && id_temp !== null && id_temp !== 'null' && id_temp.length === 2) {
                                                                                                                                                                            comanda.id_tablet = id_temp;
                                                                                                                                                                        }

                                                                                                                                                                        salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + aggZero(new Date().getMilliseconds(), 3) + " # SOFTWARE AVVIATO # # ");







                                                                                                                                                                    }


                                                                                                                                                                });


                                                                                                                                                            });
                                                                                                                                                            //orologio_asporto();
                                                                                                                                                            //$('body').removeClass('loading');
                                                                                                                                                        });

                                                                                                                                                    });
                                                                                                                                                });
                                                                                                                                            });
                                                                                                                                        });
                                                                                                                                        /*});
                                                                                                                                         });
                                                                                                                                         });
                                                                                                                                         });
                                                                                                                                         });
                                                                                                                                         });
                                                                                                                                         });
                                                                                                                                         });
                                                                                                                                         });
                                                                                                                                         });
                                                                                                                                         });
                                                                                                                                         });*/
                                                                                                                                        //});
                                                                                                                                        //});
                                                                                                                                    });
                                                                                                                                });
                                                                                                                            });
                                                                                                                        });
                                                                                                                    });
                                                                                                                });
                                                                                                            });
                                                                                                        });
                                                                                                    });
                                                                                                });
                                                                                            });
                                                                                        });
                                                                                    });
                                                                                });
                                                                            });
                                                                        });
                                                                    });
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                        //});
                                    });
                                });
                            });
                        });
                    });
                }
            });


            /* - - - - -TEST ACCENSIONE INIZIALE - - - - - - - - - - */

            pagina();
            calcola_totale();
            if (comanda.mobile === false)
            {
                adatta_zoom();
            }
            var val_iniziale;
        });

function aggiornamento_tavoli_xml(cB) {
    console.trace();
    if (comanda.compatibile_xml === true) {
        leggi_nomi_tavoli();
        console.log("XML CHIAMATO");

        var piantina_ingresso = false;
        if (comanda.ingresso === true) {
            piantina_ingresso = true;
        }
        $.getJSON("./classi_php/piantina_xml.php", {inizio: true, directory_dati: comanda.directory_dati, ora_servizio: comanda.ora_servizio, folder_number: comanda.folder_number, piantina_ingresso: piantina_ingresso}, function (array) {
            console.log("XML LETTO");

            var testo_query = "update tavoli set colore='grey',ora_apertura_tavolo='-',ora_ultima_comanda='-';";
            comanda.sincro.query(testo_query, function () {
                if (typeof (array) === "object" && array != false) {

                    //DIVISO PER 4 PROCI
                    var size = Math.ceil(array.length / 4);

                    var smallarray = new Array();

                    var ic = 0;
                    for (var i = 0; i < array.length; i += size) {
                        smallarray[ic] = array.slice(i, i + size);
                        ic++;
                    }

                    MULTI(
                            function () {
                                if (smallarray !== undefined && smallarray[0] !== undefined && smallarray[0].length > 0) {
                                    smallarray[0].forEach(function (query) {
                                        comanda.sincro.query(query, function () {

                                        });
                                    });
                                }
                            },
                            function () {
                                if (smallarray !== undefined && smallarray[1] !== undefined && smallarray[1].length > 0) {
                                    smallarray[1].forEach(function (query) {
                                        comanda.sincro.query(query, function () {

                                        });
                                    });
                                }
                            },
                            function () {
                                if (smallarray !== undefined && smallarray[2] !== undefined && smallarray[2].length > 0) {
                                    smallarray[2].forEach(function (query) {
                                        comanda.sincro.query(query, function () {

                                        });
                                    });
                                }
                            },
                            function () {
                                if (smallarray !== undefined && smallarray[3] !== undefined && smallarray[3].length > 0) {
                                    smallarray[3].forEach(function (query) {
                                        comanda.sincro.query(query, function () {

                                        });
                                    });
                                }
                            },
                            function () {
                                if (typeof (cB) === "function")
                                    cB(true);
                            });

                } else if (typeof (cB) === "function") {
                    console.log("aggiornamento_tavoli_xml cb2");
                    cB(false);

                }
            });
        }).fail(function (event) {


            //RIGA SOSPETTA 16-01-2019
            //event.preventDefault();
            alert("ATTENZIONE! Sei fuori portata WiFi. Avvicinati all'antenna e riprova.");

            try {
                $(document).off(comanda.eventino, evento_selettore_tavolo);
            } catch (e) {
                console.log(e);
            }
            $(document).on(comanda.eventino, 'div.tavolo.rett,div.tavolo.cerc', evento_selettore_tavolo);
            $('.tasto_konto').css('pointer-events', '');
            $('.tasto_bestellung').css('pointer-events', '');
            $('.tasto_bargeld').css('pointer-events', '');
            $('.tasto_quittung').css('pointer-events', '');
            $('.tasto_rechnung').css('pointer-events', '');

        });
    } else if (typeof (cB) === "function") {
        console.log("aggiornamento_tavoli_xml cb3");

        cB(true);
    }

}


function leggi_get_url(name, url) {
    if (!url)
        url = location.href;
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(url);
    return results === null ? null : results[1];
}




function incasso_manuale_xml() {


    $('#popup_incasso_importo').val("0.00");

    calcoli_incasso();

    $("#popup_incasso").modal('show');



}

$(document).on("change", "#popup_incasso_importo", function () {
    calcoli_incasso();
});

function calcoli_incasso() {

    var query = "select incassato from comanda where desc_art LIKE 'RECORD TESTA%' and stato_record='ATTIVO';";
    comanda.sincro.query(query, function (risultato) {

        var incassato = "0,00";
        if (risultato !== undefined && risultato[0] !== undefined && risultato[0].incassato !== undefined && risultato[0].incassato !== null) {
            incassato = risultato[0].incassato;
        }
        incassato = parseFloat(conv_prz_fc_ibr(incassato)).toFixed(2);

        var totale = parseFloat($(comanda.totale_scontrino).html()).toFixed(2);

        $('#popup_incasso_totale').html(totale);


        $('#popup_incasso_pagato').html(incassato);


        comanda.nuovo_incasso_testa = parseFloat(parseFloat(incassato) + parseFloat($('#popup_incasso_importo').val())).toFixed(2).replace('.', ',');

        var da_pagare = parseFloat(totale - incassato).toFixed(2);
        comanda.popup_incasso_da_pagare = da_pagare;

        $('#popup_incasso_da_pagare').html(da_pagare);

        var resto = parseFloat(parseFloat($('#popup_incasso_importo').val()) - da_pagare).toFixed(2);
        comanda.popup_incasso_resto = resto;

        if (resto >= 0) {
            $('#popup_incasso_resto').html(resto);
        } else
        {
            $('#popup_incasso_resto').html("---");
        }
    });

}

function incassa_soldi() {
    /*if (comanda.popup_incasso_resto > 0) {
     bootbox.alert("Devi incassare i soldi giusti. Non deve esserci Resto. Il resto serve solo a facilitare i conteggi.");
     } else if (comanda.popup_incasso_resto <= 0) {*/
    if (comanda.compatibile_xml === true && $('#conto>tr[id^=art_]').length === 0) {

        comanda.funzionidb.aggiungi_articolo(null, "descrizione=MEMO INC,SCONTO,DRINK,LISTINO,COPERTI&prezzo_1=0&euro;&quantita=1&cod_articolo=IDCSL_DES", 1, "CONTO", "", null, undefined, undefined, undefined, function () {
            comanda.funzionidb.conto_attivo(function () {
                $("#popup_incasso").modal('hide');
                esci_tavolo(comanda.nuovo_incasso_testa)
            });
        });

    } else {
        $("#popup_incasso").modal('hide');
        esci_tavolo(comanda.nuovo_incasso_testa)
    }

    /*}*/
}
















function invia_messaggio_cliente_seduto(tavolo, tavolotxt) {

    var variabile_tavolo = tavolo;

    lettura_tavolo_xml(variabile_tavolo, function () {

        salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + aggZero(new Date().getMilliseconds(), 3) + " # INIZIO STAMPA CLIENTE TAVOLO # TAVOLO: " + variabile_tavolo + " # ");

        var numero_coperti = $("#numero_coperti").val();

        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)] = new Object();


        //VALORI DEFAULT
        var storicizzazione = 'N';
        var settaggio_comandapiuconto = 'N';
        var incasso_auto = 'N';
        var BIS = '';

        if (comanda.split_abilitato === true) {
            if (lettera_conto_split().length === 1)
            {
                BIS = "-" + lettera_conto_split();
            }
        }

        if (comanda.storicizzazione_auto === 'S')
        {
            storicizzazione = "CS";
        } else if (comanda.storicizzazione_auto === 'N')
        {
            if (comanda.comanda_conto === 'S')
            {
                //ELIMINATO 26 OTTOBRE 2016
                //PRIMA ERA "C" MA FACEVA 2 CONTI
                storicizzazione = " ";
            } else
            {
                storicizzazione = " ";
            }
        }

        if (comanda.comanda_conto === 'S')
        {
            settaggio_comandapiuconto = "S";
            var lib5 = 'S';
        } else if (comanda.comanda_conto === 'N')
        {
            settaggio_comandapiuconto = " ";
            var lib5 = ' ';
        }

        if (comanda.incasso_auto === 'S')
        {
            incasso_auto = "S";
        } else if (comanda.incasso_auto === 'N')
        {
            incasso_auto = "N";
        }

        var d = new Date();
        var Y = d.getFullYear().toString();
        var D = addZero(d.getDate(), 2);
        var M = addZero((d.getMonth() + 1), 2);
        var h = addZero(d.getHours(), 2);
        var m = addZero(d.getMinutes(), 2);
        var s = addZero(d.getSeconds(), 2);
        var ms = addZero(d.getMilliseconds(), 3);
        var data = D + '/' + M + '/' + Y;
        var ora = h + ':' + m + ':' + s;
        var variabile = '';
        var specifica_comanda = "";

        var testo_query = "select LIB4,dest_stampa_2,ora_,operatore,sconto_perc,perc_iva,BIS,NSEGN,QTAP,ordinamento,ultima_portata,peso_ums,cod_articolo,dest_stampa,contiene_variante,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita,stampata_sn from comanda where desc_art NOT LIKE 'RECORD TESTA%' and  contiene_variante='1' and ntav_comanda='" + variabile_tavolo + "' and posizione='CONTO' and stato_record='ATTIVO' " + specifica_comanda + " \n\
                            union all\n\
                            select LIB4,dest_stampa_2,ora_,operatore,sconto_perc,perc_iva,BIS,NSEGN,QTAP,ordinamento,ultima_portata,peso_ums,cod_articolo,dest_stampa,contiene_variante,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita,stampata_sn from comanda where desc_art NOT LIKE 'RECORD TESTA%' and  (contiene_variante !='1' or contiene_variante is null) and length(nodo)=3 and ntav_comanda='" + variabile_tavolo + "' and posizione='CONTO' and stato_record='ATTIVO'  " + specifica_comanda + " \n\
                            union all\n\
                            select LIB4,dest_stampa_2,ora_,operatore,sconto_perc,perc_iva,BIS,NSEGN,QTAP,ordinamento,ultima_portata,peso_ums,cod_articolo,dest_stampa,contiene_variante,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita,stampata_sn from comanda where desc_art NOT LIKE 'RECORD TESTA%' and  length(nodo)=7 and ntav_comanda='" + variabile_tavolo + "'  and posizione='CONTO' and stato_record='ATTIVO' " + specifica_comanda + ";";

        comanda.sincro.query(testo_query, function (result) {

            result = alasql("select * from ? order by dest_stampa,portata,nodo,prog_inser", [result]);

            //INTESTAZIONE
            var output = '<?xml version="1.0" standalone="yes"?>\r\n\
                        <dataroot xmlns="http://www.w3.org/2001/TAVOLI.xsd">\r\n';

            var stampante = "1";
            if (comanda.stampante_fastingresso && comanda.stampante_fastingresso !== "") {
                stampante = comanda.stampante_fastingresso;
            }

            output += '<TAVOLI>\r\n\
                           <TC>C</TC>\r\n\
                            <DATA>' + data_inizio_servizio + '</DATA>\r\n\
                            <TAVOLO>' + tavolo + '</TAVOLO>\r\n\
                            <BIS></BIS>\r\n\
                            <PRG>001</PRG>\r\n\
                            <ART>999 001</ART>\r\n\
                            <ART2>999 001</ART2>\r\n\
                            <VAR></VAR>\r\n\
                            <DES>IL CLIENTE E\' AL TAVOLO</DES>\r\n\
                            <PRZ>0,00</PRZ>\r\n\
                            <COSTO>0</COSTO>\r\n\
                            <QTA>1</QTA>\r\n\
                            <SN>N</SN>\r\n\
                            <CAT>ZZZ</CAT>\r\n\
                            <TOTALE />\r\n\
                            <SCONTO />\r\n\
                            <AUTORIZ></AUTORIZ>\r\n\
                            <INC>0</INC>\r\n\
                            <CARTACRED/>\r\n\
                            <BANCOMAT/>\r\n\
                            <ASSEGNI />\r\n\
                            <NCARD1 />\r\n\
                            <NCARD2/>\r\n\
                            <NCARD3/>\r\n\
                            <NCARD4/>\r\n\
                            <NCARD5 />\r\n\
                            <NSEGN />\r\n\
                            <NEXIT/>\r\n\
                            <TS>' + stampante + '</TS>\r\n\
                            <NOME>' + comanda.operatore + '</NOME>\r\n\
                            <ORA>' + ora + '</ORA>\r\n\
                            <LIB1>1</LIB1>\r\n\
                            <LIB2/>\r\n\
                            <LIB3/>\r\n\
                            <LIB4>0</LIB4>\r\n\
                            <LIB5/>\r\n\
                            <CLI/>\r\n\
                            <QTAP>0</QTAP>\r\n\
                            <NODO>001</NODO>\r\n\
                            <PORTATA>N</PORTATA>\r\n\
                            <NUMP>0</NUMP>\r\n\
                            <TAVOLOTXT>' + tavolotxt + '</TAVOLOTXT>\r\n\
                            <ULTPORT></ULTPORT>\r\n\
                            <AGG_GIAC>N</AGG_GIAC>\r\n\
                            </TAVOLI>\r\n';

            var i = 0;
            var cod_articolo_princ;
            var ts_princ;
            var prog_principale;
            var descrizione_articolo;
            var ordinamento = '';
            var testa_totale = 0;
            var qta_princ = 1;


            result.forEach(function (obj) {
                ////console.log(obj);

                //PRIMA ERA +1 MA NON SE NE CAPISCE IL MOTIVO
                obj.prog_inser = parseInt(obj.prog_inser);

                if (obj.BIS === BIS) {
                    obj.dest_stampa !== undefined ? ts_princ = obj.dest_stampa : ts_princ = '1';
                } else
                {
                    ts_princ = obj.dest_stampa;
                }

                obj.peso_ums !== undefined ? obj.peso_ums : obj.peso_ums = '';
                obj.ordinamento !== undefined ? ordinamento = obj.ordinamento : ordinamento = '01';
                prog_principale = aggZero(obj.prog_inser, 3);
                descrizione_articolo = obj.desc_art;
                console.log(obj.dest_stampa);
                console.log(obj.dest_stampa !== undefined);
                console.log(ts_princ);
                var contiene_variante = '';
                obj.contiene_variante === '1' ? contiene_variante = 'SI' : contiene_variante = '';
//---
                if (obj.desc_art[0] === "+" || obj.desc_art[0] === "-" || obj.desc_art[0] === "x" || obj.desc_art[0] === "*" || obj.desc_art[0] === "=")
                {
                    prog_principale = aggZero(prog_principale.substring(0, 3), 3) + ' ' + aggZero(obj.prog_inser, 3) + ' ';
                    descrizione_articolo = obj.desc_art.substring(1);
                    variabile = obj.desc_art[0];
                } else if (obj.desc_art[0] === "(") {
                    prog_principale = aggZero(prog_principale.substring(0, 3), 3) + ' ' + aggZero(obj.prog_inser, 3) + ' ';
                    variabile = "*";
                } else
                {
                    cod_articolo_princ = obj.cod_articolo;
                    variabile = '';
                }

                var stampata_sn = 'N';
                if (obj.stampata_sn === 'S')
                {
                    stampata_sn = 'S';
                }

                var qtap = '0';
                if (!isNaN(obj.QTAP) && obj.QTAP > 0) {
                    qtap = obj.QTAP;
                }

                var nsegn = '0';
                if (!isNaN(obj.NSEGN) && obj.NSEGN > 0) {
                    nsegn = obj.NSEGN;
                }

                if (stampata_sn === 'N' && descrizione_articolo !== "MEMO INC,SCONTO,DRINK,LISTINO,COPERTI")
                {


                    var ora = h + ':' + m + ':' + s;

                    if (obj.ultima_portata === "S") {
                        ultima_portata_non_stampata = "S";
                    }
                } else
                {
                    var ora = obj.ora_;
                }

                var operatore = obj.operatore;
                if (operatore.length === 0) {
                    operatore = comanda.operatore;
                }


                testa_totale += parseFloat(conv_prz_fc_ibr(obj.prezzo_un)) * parseInt(obj.quantita);

                output += '<TAVOLI>\r\n';
                output += '<TC>C</TC>\r\n';
                output += '<DATA>' + data_inizio_servizio + '</DATA>\r\n';
                output += '<TAVOLO>' + aggZero(variabile_tavolo, 3) + '</TAVOLO>\r\n';
                output += '<BIS>' + obj.BIS + '</BIS>\r\n';
                output += '<PRG>' + aggZero(obj.prog_inser, 3) + '</PRG>\r\n';
                output += '<ART>' + cod_articolo_princ + '</ART>\r\n';
                output += '<ART2>' + obj.cod_articolo + '</ART2>\r\n';
                output += '<VAR>' + variabile + '</VAR>\r\n';
                output += '<DES>' + descrizione_articolo + '</DES>\r\n';
                output += '<PRZ>' + parseFloat(conv_prz_fc_ibr(obj.prezzo_un)).toFixed(2).replace('.', ',') + '</PRZ>\r\n';
                output += '<COSTO>0</COSTO>\r\n';
                output += '<QTA>' + obj.quantita + '</QTA>\r\n';
                output += '<SN>' + stampata_sn + '</SN>\r\n';
                output += '<CAT>' + obj.categoria + '</CAT>\r\n';
                output += '<TOTALE />\r\n';
                output += '<SCONTO />\r\n';
                output += '<AUTORIZ>' + obj.sconto_perc + '</AUTORIZ>\r\n';
                output += '<INC>0</INC>\r\n';
                output += '<CARTACRED/>\r\n';
                output += '<BANCOMAT/>\r\n';
                output += '<ASSEGNI>' + obj.perc_iva + '</ASSEGNI>\r\n';
                output += '<NCARD1>' + contiene_variante + '</NCARD1>\r\n';
                output += '<NCARD2/>\r\n';
                output += '<NCARD3/>\r\n';
                output += '<NCARD4/>\r\n';
                output += '<NCARD5>' + ordinamento + '</NCARD5>\r\n'; //ORDINAMENTO
                output += '<NSEGN>' + nsegn + '</NSEGN>\r\n';
                output += '<NEXIT/>\r\n';
                output += '<TS>' + ts_princ + '</TS>\r\n'; //' + ts_princ + '
                output += '<NOME>' + operatore + '</NOME>\r\n';
                output += '<ORA>' + ora + '</ORA>\r\n';
                output += '<LIB1>1</LIB1>\r\n';
                output += '<LIB2/>\r\n';
                output += '<LIB3/>\r\n';
                output += '<LIB4>' + obj.LIB4 + '</LIB4>\r\n';
                output += '<LIB5>' + obj.dest_stampa_2 + '</LIB5>\r\n';
                output += '<CLI/>\r\n';
                output += '<QTAP>' + qtap + '</QTAP>\r\n';
                output += '<NODO>' + obj.nodo + '</NODO>\r\n';
                output += '<PORTATA>' + obj.portata + '</PORTATA>\r\n';
                output += '<NUMP>0</NUMP>\r\n';
                output += '<TAVOLOTXT>' + tavolotxt + '</TAVOLOTXT>\r\n';
                output += '<ULTPORT>' + obj.ultima_portata + '</ULTPORT>\r\n';
                output += '<AGG_GIAC>N</AGG_GIAC>\r\n';
                output += '</TAVOLI>\r\n';
                i++;
            });
            var testo_query = 'select (select desc_art from comanda where stato_record="ATTIVO" and ntav_comanda="' + variabile_tavolo + '" and posizione="SCONTO") as desc_sconto,(select prezzo_un from comanda where stato_record="ATTIVO" and ntav_comanda="' + variabile_tavolo + '" and posizione="SCONTO") as sconto,(select incassato from comanda where desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as incassato,operatore, (select nump_xml from comanda where stato_record="ATTIVO" and ntav_comanda="' + variabile_tavolo + '" order by nump_xml desc limit 1) as numero_coperti,(select contiene_variante from comanda where desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as NCARD1,(select NCARD2 from comanda where desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as NCARD2,(select NCARD3 from comanda where desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as NCARD3,(select NCARD4 from comanda where desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as NCARD4,(select NCARD5 from comanda where desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as NCARD5,(select NSEGN  from comanda where desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as NSEGN,(select NEXIT from comanda where desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as NEXIT,(select carta_credito from comanda where desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as carta_credito,(select bancomat from comanda where desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as bancomat,BIS \n\ from comanda where stato_record="ATTIVO" and posizione="CONTO" and ntav_comanda="' + variabile_tavolo + '" group by id,BIS order by prog_inser asc;';

            console.log("QUERY SCONTO INCASSATO", testo_query);

            comanda.sincro.query(testo_query, function (record_testa) {

                if (record_testa.length === 0) {

                    output += '<TAVOLI>\r\n\
                <TC>T</TC>\r\n\
                <DATA>' + data_inizio_servizio + '</DATA>\r\n\
                <TAVOLO>' + aggZero(variabile_tavolo, 3) + '</TAVOLO>\r\n\
                <BIS/>\r\n\
                <PRG>000</PRG>\r\n\
                <ART>01</ART>\r\n\
                <ART2/>\r\n\
                <VAR/>\r\n\
                <DES>RECORD TESTA</DES>\r\n\
                <PRZ/>\r\n\
                <COSTO>0</COSTO>\r\n\
                <QTA/>\r\n\
                <SN>N</SN>\r\n\
                <CAT/>\r\n\
                <TOTALE>0,00</TOTALE>\r\n\
                <SCONTO/>\r\n\
                <AUTORIZ/>\r\n\
                <INC>0,00</INC>\r\n\
                <CARTACRED>0,00</CARTACRED>\r\n\
                <BANCOMAT>0,00</BANCOMAT>\r\n\
                <ASSEGNI>0,00</ASSEGNI>\r\n\
                <NCARD1>0</NCARD1>\r\n\
                <NCARD2>0</NCARD2>\r\n\
                <NCARD3>0</NCARD3>\r\n\
                <NCARD4>0</NCARD4>\r\n\
                <NCARD5>0</NCARD5>\r\n\
                <NSEGN>0</NSEGN>\r\n\
                <NEXIT>0</NEXIT>\r\n\
                <TS/>\r\n\
                <NOME>' + comanda.operatore + '</NOME>\r\n\
                <ORA>' + ora + '</ORA>\r\n\
                <LIB1>1</LIB1>\r\n\
                <LIB2/>\r\n\
                <LIB3/>\r\n\
                <LIB4/>\r\n\
                <LIB5>' + lib5 + '</LIB5>\r\n\
                <CLI/>\r\n\
                <QTAP>0</QTAP>\r\n\
                <NODO>' + settaggio_comandapiuconto + '</NODO>\r\n\
                <PORTATA/>\r\n\
                <NUMP>0</NUMP>\r\n\
                <TAVOLOTXT>' + tavolotxt + '</TAVOLOTXT>\r\n\
                <ULTPORT>N</ULTPORT>\r\n\
                <AGG_GIAC>N</AGG_GIAC>\r\n\
                </TAVOLI>\r\n';

                    output += '</dataroot>';
                    output = output.replace(/>null</gi, '><');
                    output = output.replace(/>undefined</gi, '><');



                    var d = new Date();
                    var Y = d.getFullYear();
                    var M = addZero(d.getDate(), 2);
                    var D = addZero((d.getMonth() + 1), 2);
                    var h = addZero(d.getHours(), 2);
                    var m = addZero(d.getMinutes(), 2);
                    var s = addZero(d.getSeconds(), 2);
                    var ms = addZero(d.getMilliseconds(), 3);
                    var data_tablet = h + '' + m + '' + s;

                    var id_tablet = "29";
                    var id_temp = leggi_get_url('id');

                    if (id_temp !== undefined && id_temp !== null && id_temp !== 'null' && id_temp.length === 2) {
                        id_tablet = id_temp;
                    }


                    var nome_file = "/PLANING/P" + aggZero(tavolo, 3) + '.I';

                    comanda.xml.salva_file_xml("_" + comanda.folder_number + "/" + nome_file, "", "PLANING", function () {

                        nome_file = "/SDS/PDA" + id_tablet + "_" + data_tablet + BIS + ".XM_";

                        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].nome_file_sds = "_" + comanda.folder_number + "/" + nome_file;
                        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].contenuto_file_sds = output;
                        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].tipo_file_sds = "SDS";

                        comanda.xml.salva_file_xml("_" + comanda.folder_number + "/" + nome_file, output, "SDS", function () {

                            vedi_clienti_seduti = false;

                            elenco_nomi_prenotati();

                            //LO SBLOCCA IL SDS
                        });

                    });

                } else {
                    record_testa.forEach(function (rec) {

                        var incassato = 0;



                        var autoriz = 0;


                        var sconto = '0.00';
                        if (numero_coperti !== undefined && !isNaN(numero_coperti) && numero_coperti !== "")
                        {
                            testa_totale += parseInt(numero_coperti) * parseFloat(comanda.valore_coperto);


                            if (parseInt(numero_coperti) - comanda.ultimi_coperti_importati !== 0) {
                                annulla_comanda = false;
                            }
                        }
                        if (typeof rec.sconto === 'string') {
                            if (rec.sconto !== undefined && rec.sconto.substr(-1) === "%") {

                                //Bisogna fare il calcolo
                                sconto = testa_totale / 100 * rec.sconto.substr(1).slice(0, -1);
                                //autoriz = record_testa[0].sconto.substr(1).slice(0, -1);

                                sconto = sconto.toString();
                                console.log("CALCOLO CONTO SCONTO", sconto);
                            } else if (rec.sconto !== undefined) {
                                sconto = rec.sconto.substr(1);
                                //autoriz = record_testa[0].desc_sconto;
                            }
                            autoriz = rec.desc_sconto;


                        }

                        //PER LA SAGRA NON SERVE PERCHE' INCASSA IN AUTOMATICO
                        //E POI SE SI RIPRENDE UNA COMANDA DOPO AVER LASCIATO LA L'INCASSO FA CASINO
                        //DA VERIFICARE




                        if (incasso_auto === 'N')
                        {
                            if ((rec.incassato !== undefined && rec !== null && rec.incassato !== 'undefined' && !isNaN(parseFloat(rec.incassato))))
                            {
                                incassato = rec.incassato;
                            } else
                            {
                                incassato = '0,00';
                            }
                        } else if (incasso_auto === 'S')
                        {

                            incassato = testa_totale - parseFloat(sconto);
                            incassato = incassato.toString();

                            console.log("CALCOLO CONTO INCASSO", incassato);

                        }



                        //Nel fare parseFloat bisogna convertire la , in ., altrimenti restituisce il numero senza decimali, se era un incasso non automatico
                        incassato = parseFloat(conv_prz_fc_ibr(incassato)).toFixed(2).replace('.', ',');
                        sconto = parseFloat(conv_prz_fc_ibr(sconto)).toFixed(2).replace('.', ',');


                        testa_totale = parseFloat(testa_totale).toFixed(2).replace(".", ",");


                        output += '<TAVOLI>\r\n\
                <TC>T</TC>\r\n\
                <DATA>' + data_inizio_servizio + '</DATA>\r\n\
                <TAVOLO>' + aggZero(variabile_tavolo, 3) + '</TAVOLO>\r\n\
                <BIS>' + rec.BIS + '</BIS>\r\n\
                <PRG>000</PRG>\r\n\
                <ART>01</ART>\r\n\
                <ART2/>\r\n\
                <VAR/>\r\n\
                <DES>RECORD TESTA</DES>\r\n\
                <PRZ/>\r\n\
                <COSTO>0</COSTO>\r\n\
                <QTA/>\r\n\
                <SN>N</SN>\r\n\
                <CAT/>\r\n\
                <TOTALE>' + testa_totale + '</TOTALE>\r\n\
                <SCONTO>' + sconto + '</SCONTO>\r\n\
                <AUTORIZ>' + autoriz + '</AUTORIZ>\r\n\
                <INC>' + incassato + '</INC>\r\n\
                <CARTACRED>' + record_testa[0].carta_credito + '</CARTACRED>\r\n\
                <BANCOMAT>' + record_testa[0].bancomat + '</BANCOMAT>\r\n\
                <ASSEGNI>0,00</ASSEGNI>\r\n\
                <NCARD1>' + record_testa[0].NCARD1 + '</NCARD1>\r\n\
                <NCARD2>' + record_testa[0].NCARD2 + '</NCARD2>\r\n\
                <NCARD3>' + record_testa[0].NCARD3 + '</NCARD3>\r\n\
                <NCARD4>' + record_testa[0].NCARD4 + '</NCARD4>\r\n\
                <NCARD5>' + record_testa[0].NCARD5 + '</NCARD5>\r\n\
                <NSEGN>' + record_testa[0].NSEGN + '</NSEGN>\r\n\
                <NEXIT>' + record_testa[0].NEXIT + '</NEXIT>\r\n\
                <TS>' + storicizzazione + '</TS>\r\n\
                <NOME>' + comanda.operatore + '</NOME>\r\n\
                <ORA>' + ora + '</ORA>\r\n\
                <LIB1>1</LIB1>\r\n\
                <LIB2/>\r\n\
                <LIB3/>\r\n\
                <LIB4/>\r\n\
                <LIB5>' + lib5 + '</LIB5>\r\n\
                <CLI/>\r\n\
                <QTAP>0</QTAP>\r\n\
                <NODO>' + settaggio_comandapiuconto + '</NODO>\r\n\
                <PORTATA/>\r\n\
                <NUMP>' + numero_coperti + '</NUMP>\r\n\
                <TAVOLOTXT>' + tavolotxt + '</TAVOLOTXT>\r\n\
                <ULTPORT>N</ULTPORT>\r\n\
                <AGG_GIAC>N</AGG_GIAC>\r\n\
                </TAVOLI>\r\n';
                        //LIB5 S=CONTO STAMPATO

                    });

                    output += '</dataroot>';
                    output = output.replace(/>null</gi, '><');
                    output = output.replace(/>undefined</gi, '><');



                    var d = new Date();
                    var Y = d.getFullYear();
                    var M = addZero(d.getDate(), 2);
                    var D = addZero((d.getMonth() + 1), 2);
                    var h = addZero(d.getHours(), 2);
                    var m = addZero(d.getMinutes(), 2);
                    var s = addZero(d.getSeconds(), 2);
                    var ms = addZero(d.getMilliseconds(), 3);
                    var data_tablet = h + '' + m + '' + s;

                    var id_tablet = "29";
                    var id_temp = leggi_get_url('id');

                    if (id_temp !== undefined && id_temp !== null && id_temp !== 'null' && id_temp.length === 2) {
                        id_tablet = id_temp;
                    }


                    var nome_file = "/PLANING/P" + aggZero(tavolo, 3) + '.I';

                    comanda.xml.salva_file_xml("_" + comanda.folder_number + "/" + nome_file, "", "PLANING", function () {

                        nome_file = "/SDS/PDA" + id_tablet + "_" + data_tablet + BIS + ".XM_";

                        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].nome_file_sds = "_" + comanda.folder_number + "/" + nome_file;
                        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].contenuto_file_sds = output;
                        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].tipo_file_sds = "SDS";

                        comanda.xml.salva_file_xml("_" + comanda.folder_number + "/" + nome_file, output, "SDS", function () {

                            vedi_clienti_seduti = false;

                            elenco_nomi_prenotati();

                            //LO SBLOCCA IL SDS
                        });

                    });
                }
            });
        });
    });
}









function gutshein_xml() {
    $('#metodo_pagamento_gutshein').modal('show');

    $('#gutshein_importo_contanti').val(parseFloat($(comanda.totale_scontrino).html()).toFixed(2));

    $('#gutshein__importo_gutshein').val('0.00');

    calcoli_gutshein_pagamento();

}





function scambia_xml_tavolo(t1, t2, operatore, cB) {

    var d = new Date();

    var h = addZero(d.getHours(), 2);
    var m = addZero(d.getMinutes(), 2);
    var s = addZero(d.getSeconds(), 2);

    var data = h + '' + m + '' + s;

    var nome_file = "PDA" + operatore + "_" + data + ".XM_";

    salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + aggZero(new Date().getMilliseconds(), 3) + " # SCAMBIA TAVOLO # T1: " + t1 + " - _T2: " + t2 + " # ");


    var pagina_scambio = '<?xml version="1.0" standalone="yes"?>\r\n\
        <dataroot xmlns="http://www.w3.org/2001/TAVOLI.xsd">\r\n\
            <TAVOLI>\r\n\
                <TC>X</TC>\r\n\
                <DATA>01/01/2005</DATA>\r\n\
                <TAVOLO>X</TAVOLO>\r\n\
                <BIS />\r\n\
                <PRG />\r\n\
                <ART />\r\n\
                <ART2 />\r\n\
                <VAR />\r\n\
                <DES />\r\n\
                <PRZ />\r\n\
                <COSTO />\r\n\
                <QTA />\r\n\
                <SN>N</SN>\r\n\
                <CAT />\r\n\
                <TOTALE />\r\n\
                <SCONTO />\r\n\
                <AUTORIZ />\r\n\
                 <INC />\r\n\
                <CARTACRED />\r\n\
                <BANCOMAT />\r\n\
                <ASSEGNI />\r\n\
                <NCARD1 />\r\n\
                <NCARD2 />\r\n\
                <NCARD3 />\r\n\
                <NCARD4 />\r\n\
                <NCARD5 />\r\n\
                <NSEGN />\r\n\
                <NEXIT />\r\n\
                <TS />\r\n\
                <NOME />\r\n\
                <ORA />\r\n\
                <LIB1 />\r\n\
                <LIB2 />\r\n\
                <LIB3 />\r\n\
                <LIB4 />\r\n\
                <CLI />\r\n\
                <QTAP />\r\n\
                <NODO />\r\n\
                <PORTATA />\r\n\
                <NUMP />\r\n\
                <TAVOLOTXT />\r\n\
                <ULTPORT />\r\n\
                <LIB5 />\r\n\
                <AGG_GIAC />\r\n\
            </TAVOLI>\r\n\
            \r\n\
            <TAVOLI>\r\n\
                <TC>T</TC>\r\n\
                <DATA>01/01/2005</DATA>\r\n\
                <TAVOLO>000</TAVOLO>\r\n\
                <BIS />\r\n\
                <PRG>000</PRG>\r\n\
                <ART>' + t1 + '</ART>\r\n\
                <ART2>' + t2 + '</ART2>\r\n\
                <DES></DES>\r\n\
                <PRZ></PRZ>\r\n\
                <COSTO></COSTO>\r\n\
                <QTA></QTA>\r\n\
                <SN></SN>\r\n\
                <CAT />\r\n\
                <TOTALE></TOTALE>\r\n\
                <SCONTO></SCONTO>\r\n\
                <AUTORIZ></AUTORIZ>\r\n\
                <INC></INC>\r\n\
                <CARTACRED></CARTACRED>\r\n\
                <BANCOMAT></BANCOMAT>\r\n\
                <ASSEGNI></ASSEGNI>\r\n\
                <NCARD1></NCARD1>\r\n\
                <NCARD2></NCARD2>\r\n\
                <NCARD3></NCARD3>\r\n\
                <NCARD4></NCARD4>\r\n\
                <NCARD5></NCARD5>\r\n\
                <NSEGN></NSEGN>\r\n\
                <NEXIT></NEXIT>\r\n\
                <TS>SW</TS>\r\n\
                <NOME>PDA ' + operatore + '</NOME>\r\n\
                <ORA></ORA>\r\n\
                <LIB1></LIB1>\r\n\
                <LIB2 />\r\n\
                <LIB3></LIB3>\r\n\
                <LIB4></LIB4>\r\n\
                <CLI />\r\n\
                <QTAP />\r\n\
                <NODO />\r\n\
                <PORTATA />\r\n\
                <NUMP></NUMP>\r\n\
                <TAVOLOTXT></TAVOLOTXT>\r\n\
                <ULTPORT></ULTPORT>\r\n\
                <LIB5 />\r\n\
                <AGG_GIAC />\r\n\
            </TAVOLI>\r\n\
        </dataroot>\r\n';

    console.log("SCAMBIA TAVOLI", pagina_scambio);

    comanda.xml.salva_file_xml("_" + comanda.folder_number + "/SDS/" + nome_file, pagina_scambio, "SDS", function () {

        if (typeof (cB) === "function") {
            cB(true);
        }

    });
    //});
}

function esci_tavolo(incasso_definito, bottone) {

    if ((bottone === true && comanda.tasto_tavolo_esce === "S") || bottone !== true) {
        if (incasso_definito === undefined) {
            comanda.nuovo_incasso_testa = undefined;
        }

        $('#tasto_uscita_tavoli,.scritta_operatore,.numero_tavolo').css('opacity', '0.5');
        $('#tasto_uscita_tavoli,.scritta_operatore,.numero_tavolo').css('pointer-events', 'none');

        var variabile_tavolo = comanda.tavolo;

        if (variabile_tavolo !== undefined && typeof (variabile_tavolo) === "string") {

            salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + aggZero(new Date().getMilliseconds(), 3) + " # INIZIO ESCI TAVOLO # TAVOLO: " + variabile_tavolo + " # ");


            esporta_xml_tavolo('tavolo', function () {

                comanda.xml.elimina_tavolo_occupato("_" + comanda.folder_number + "/COND/Tav" + variabile_tavolo.replace(/^0+/, '') + ".CO1", false, function () {

                    salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + aggZero(new Date().getMilliseconds(), 3) + " # FINE ESCI TAVOLO # TAVOLO: " + variabile_tavolo + " # ");

                    btn_tavoli();

                });
            }, incasso_definito);
        }
    }
}

function stampa_storno(result) {

    var variabile_tavolo = comanda.tavolo;
    var numero_coperti = $("#numero_coperti").val();


    esporta_xml_tavolo(null, function () {

        console.log("stampa storno", result);

        $('#body_replacement').css('pointer-events', 'none');
        $('#body_replacement').css('opacity', '0.6');

        //KS0 GERMANIA
        //C ITALIA
        //S CONTO CHE CHIUDE E STORICIZZA AUTOMATICO

        //VALORI DEFAULT
        var storicizzazione = 'N';
        var settaggio_comandapiuconto = 'N';
        var incasso_auto = 'N';

        if (comanda.storicizzazione_auto === 'S')
        {
            storicizzazione = "CS";
        } else if (comanda.storicizzazione_auto === 'N')
        {
            if (comanda.comanda_conto === 'S')
            {
                //ELIMINATO 26 OTTOBRE 2016
                //PRIMA ERA "C" MA FACEVA 2 CONTI
                storicizzazione = " ";
            } else
            {
                storicizzazione = " ";
            }
        }

        if (comanda.comanda_conto === 'S')
        {
            settaggio_comandapiuconto = "S";
            var lib5 = 'S';
        } else if (comanda.comanda_conto === 'N')
        {
            settaggio_comandapiuconto = " ";
            var lib5 = ' ';
        }

        if (comanda.incasso_auto === 'S')
        {
            incasso_auto = "S";
        } else if (comanda.incasso_auto === 'N')
        {
            incasso_auto = "N";
        }


        var d = new Date();
        var Y = d.getFullYear().toString();
        var D = addZero(d.getDate(), 2);
        var M = addZero((d.getMonth() + 1), 2);
        var h = addZero(d.getHours(), 2);
        var m = addZero(d.getMinutes(), 2);
        var s = addZero(d.getSeconds(), 2);
        var ms = addZero(d.getMilliseconds(), 3);
        var data = D + '/' + M + '/' + Y;
        var ora = h + ':' + m + ':' + s;
        var variabile = '';
        var specifica_comanda = '';

        //INTESTAZIONE
        var output = '<?xml version="1.0" standalone="yes"?>\r\n\
                        <dataroot xmlns="http://www.w3.org/2001/TAVOLI.xsd">\r\n';
        var i = 0;
        var cod_articolo_princ;
        var ts_princ;
        var prog_principale;
        var descrizione_articolo;
        var ordinamento = '';
        var testa_totale = 0;
        var qta_princ = 1;


        result.forEach(function (obj) {
            ////console.log(obj);

            salva_log_sempreattivo(comanda.id_tablet + " # STORNO ARTICOLO " + obj.desc_art);

            //PRIMA ERA +1 MA NON SE NE CAPISCE IL MOTIVO
            obj.prog_inser = parseInt(obj.prog_inser);
            obj.dest_stampa !== undefined ? ts_princ = obj.dest_stampa : ts_princ = '1';
            obj.peso_ums !== undefined ? obj.peso_ums : obj.peso_ums = '';
            obj.ordinamento !== undefined ? ordinamento = obj.ordinamento : ordinamento = '01';
            prog_principale = aggZero(obj.prog_inser, 3);
            descrizione_articolo = obj.desc_art;
            console.log(obj.dest_stampa);
            console.log(obj.dest_stampa !== undefined);
            console.log(ts_princ);
            var contiene_variante = '';
            obj.contiene_variante === '1' ? contiene_variante = 'SI' : contiene_variante = '';
//---
            if (obj.desc_art[0] === "+" || obj.desc_art[0] === "-" || obj.desc_art[0] === "x" || obj.desc_art[0] === "*" || obj.desc_art[0] === "=")
            {
                prog_principale = aggZero(prog_principale.substring(0, 3), 3) + ' ' + aggZero(obj.prog_inser, 3) + ' ';
                descrizione_articolo = obj.desc_art.substring(1);
                variabile = obj.desc_art[0];
            } else if (obj.desc_art[0] === "(") {
                prog_principale = aggZero(prog_principale.substring(0, 3), 3) + ' ' + aggZero(obj.prog_inser, 3) + ' ';
                variabile = "*";
            } else
            {
                cod_articolo_princ = obj.cod_articolo;
                variabile = '';
            }

            var stampata_sn = 'N';
            if (obj.stampata_sn === 'S')
            {
                stampata_sn = 'S';
            }

            testa_totale += parseFloat(conv_prz_fc_ibr(obj.prezzo_un)) * parseInt(obj.quantita);

            output += '<TAVOLI>\r\n';
            output += '<TC>C</TC>\r\n';
            output += '<DATA>' + data_inizio_servizio + '</DATA>\r\n';
            output += '<TAVOLO>' + aggZero(variabile_tavolo, 3) + '</TAVOLO>\r\n';
            output += '<BIS />\r\n';
            output += '<PRG>' + aggZero(obj.prog_inser, 3) + '</PRG>\r\n';
            output += '<ART>' + cod_articolo_princ + '</ART>\r\n';
            output += '<ART2>' + obj.cod_articolo + '</ART2>\r\n';
            output += '<VAR>' + variabile + '</VAR>\r\n';
            output += '<DES>' + descrizione_articolo + '</DES>\r\n';
            output += '<PRZ>' + parseFloat(conv_prz_fc_ibr(obj.prezzo_un)).toFixed(2).replace('.', ',') + '</PRZ>\r\n';
            output += '<COSTO>0</COSTO>\r\n';
            output += '<QTA>' + obj.quantita + '</QTA>\r\n';
            output += '<SN>' + stampata_sn + '</SN>\r\n';
            output += '<CAT>' + obj.categoria + '</CAT>\r\n';
            output += '<TOTALE />\r\n';
            output += '<SCONTO />\r\n';
            output += '<AUTORIZ></AUTORIZ>\r\n';
            output += '<INC>0</INC>\r\n';
            output += '<CARTACRED/>\r\n';
            output += '<BANCOMAT/>\r\n';
            output += '<ASSEGNI>' + obj.perc_iva + '</ASSEGNI>\r\n';
            output += '<NCARD1>' + contiene_variante + '</NCARD1>\r\n';
            output += '<NCARD2/>\r\n';
            output += '<NCARD3/>\r\n';
            output += '<NCARD4/>\r\n';
            output += '<NCARD5>' + ordinamento + '</NCARD5>\r\n'; //ORDINAMENTO
            output += '<NSEGN>0</NSEGN>\r\n';
            output += '<NEXIT/>\r\n';
            output += '<TS>' + ts_princ + '</TS>\r\n'; //' + ts_princ + '
            output += '<NOME>' + comanda.operatore + '</NOME>\r\n';
            output += '<ORA>' + ora + '</ORA>\r\n';
            output += '<LIB1>1</LIB1>\r\n';
            output += '<LIB2/>\r\n';
            output += '<LIB3/>\r\n';
            output += '<LIB4 />\r\n';
            output += '<LIB5>' + obj.dest_stampa_2 + '</LIB5>\r\n';
            output += '<CLI/>\r\n';
            output += '<QTAP>0</QTAP>\r\n';
            output += '<NODO>' + obj.nodo + '</NODO>\r\n';
            output += '<PORTATA>' + obj.portata + '</PORTATA>\r\n';
            output += '<NUMP>0</NUMP>\r\n';
            output += '<TAVOLOTXT>' + comanda.tavolotxt + '</TAVOLOTXT>\r\n';
            output += '<ULTPORT>' + obj.ultima_portata + '</ULTPORT>\r\n';
            output += '<AGG_GIAC>N</AGG_GIAC>\r\n';
            output += '</TAVOLI>\r\n';
            i++;
        });
        var testo_query = 'select (select desc_art from comanda where stato_record="ATTIVO" and ntav_comanda="' + variabile_tavolo + '" and posizione="SCONTO") as desc_sconto,(select prezzo_un from comanda where stato_record="ATTIVO" and ntav_comanda="' + variabile_tavolo + '" and posizione="SCONTO") as sconto,(select incassato from comanda where desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as incassato,(select contiene_variante from comanda where desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as NCARD1,(select NCARD2 from comanda where desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as NCARD2,(select NCARD3 from comanda where desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as NCARD3,(select NCARD4 from comanda where  desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as NCARD4,(select NCARD5 from comanda where  desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as NCARD5,(select NSEGN  from comanda where  desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as NSEGN,(select NEXIT from comanda where  desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as NEXIT,(select carta_credito from comanda where  desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as carta_credito,(select bancomat from comanda where  desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as bancomat,operatore, (select nump_xml from comanda where stato_record="ATTIVO" and ntav_comanda="' + variabile_tavolo + '" order by nump_xml desc limit 1) as numero_coperti \n\ from comanda where stato_record="ATTIVO" and posizione="CONTO" and ntav_comanda="' + variabile_tavolo + '" group by id order by prog_inser asc;';

        console.log("QUERY SCONTO INCASSATO", testo_query);

        comanda.sincro.query(testo_query, function (record_testa) {

            var incassato = 0;


            var autoriz = 0;


            var sconto = '0.00';
            if (numero_coperti !== undefined && !isNaN(numero_coperti) && numero_coperti !== "")
            {
                testa_totale += parseInt(numero_coperti) * parseFloat(comanda.valore_coperto);
            }
            if (record_testa !== undefined && record_testa[0] !== undefined && typeof record_testa[0].sconto === 'string') {
                if (record_testa[0].sconto !== undefined && record_testa[0].sconto.substr(-1) === "%") {

                    //Bisogna fare il calcolo
                    sconto = testa_totale / 100 * record_testa[0].sconto.substr(1).slice(0, -1);
                    //autoriz = record_testa[0].sconto.substr(1).slice(0, -1);

                    sconto = sconto.toString();
                    console.log("CALCOLO CONTO SCONTO", sconto);
                } else if (record_testa[0].sconto !== undefined) {
                    sconto = record_testa[0].sconto.substr(1);
                    //autoriz = record_testa[0].desc_sconto;
                }
                autoriz = record_testa[0].desc_sconto;


            }

            //PER LA SAGRA NON SERVE PERCHE' INCASSA IN AUTOMATICO
            //E POI SE SI RIPRENDE UNA COMANDA DOPO AVER LASCIATO LA L'INCASSO FA CASINO
            //DA VERIFICARE


            if (incasso_auto === 'N')
            {
                if ((record_testa[0].incassato !== undefined && record_testa[0].incassato !== null && record_testa[0].incassato !== 'undefined' && !isNaN(parseFloat(record_testa[0].incassato))))
                {
                    incassato = record_testa[0].incassato;
                } else
                {
                    incassato = '0,00';
                }
            } else if (incasso_auto === 'S')
            {
                incassato = testa_totale - parseFloat(sconto);
                incassato = incassato.toString();

                console.log("CALCOLO CONTO INCASSO", incassato);

            }

            //Nel fare parseFloat bisogna convertire la , in ., altrimenti restituisce il numero senza decimali, se era un incasso non automatico
            incassato = parseFloat(conv_prz_fc_ibr(incassato).replace(',', '.')).toFixed(2).replace('.', ',');
            sconto = parseFloat(conv_prz_fc_ibr(sconto)).toFixed(2).replace('.', ',');




            testa_totale = parseFloat(testa_totale).toFixed(2).replace(".", ",");

            output += '<TAVOLI>\r\n\
                <TC>T</TC>\r\n\
                <DATA>' + data_inizio_servizio + '</DATA>\r\n\
                <TAVOLO>' + aggZero(variabile_tavolo, 3) + '</TAVOLO>\r\n\
                <BIS/>\r\n\
                <PRG>000</PRG>\r\n\
                <ART>01</ART>\r\n\
                <ART2/>\r\n\
                <VAR/>\r\n\
                <DES>RECORD TESTA</DES>\r\n\
                <PRZ/>\r\n\
                <COSTO>0</COSTO>\r\n\
                <QTA/>\r\n\
                <SN>N</SN>\r\n\
                <CAT/>\r\n\
                <TOTALE>' + testa_totale + '</TOTALE>\r\n\
                <SCONTO>' + sconto + '</SCONTO>\r\n\
                <AUTORIZ>' + autoriz + '</AUTORIZ>\r\n\
                <INC>' + incassato + '</INC>\r\n\
                <CARTACRED>' + record_testa[0].carta_credito + '</CARTACRED>\r\n\
                <BANCOMAT>' + record_testa[0].bancomat + '</BANCOMAT>\r\n\
                <ASSEGNI>0,00</ASSEGNI>\r\n\
                <NCARD1>' + record_testa[0].NCARD1 + '</NCARD1>\r\n\
                <NCARD2>' + record_testa[0].NCARD2 + '</NCARD2>\r\n\
                <NCARD3>' + record_testa[0].NCARD3 + '</NCARD3>\r\n\
                <NCARD4>' + record_testa[0].NCARD4 + '</NCARD4>\r\n\
                <NCARD5>' + record_testa[0].NCARD5 + '</NCARD5>\r\n\
                <NSEGN>' + record_testa[0].NSEGN + '</NSEGN>\r\n\
                <NEXIT>' + record_testa[0].NEXIT + '</NEXIT>\r\n\
                <TS>CR</TS>\r\n\
                <NOME>' + comanda.operatore + '</NOME>\r\n\
                <ORA>' + ora + '</ORA>\r\n\
                <LIB1>1</LIB1>\r\n\
                <LIB2/>\r\n\
                <LIB3/>\r\n\
                <LIB4/>\r\n\
                <LIB5>' + lib5 + '</LIB5>\r\n\
                <CLI/>\r\n\
                <QTAP>0</QTAP>\r\n\
                <NODO>' + settaggio_comandapiuconto + '</NODO>\r\n\
                <PORTATA/>\r\n\
                <NUMP>' + numero_coperti + '</NUMP>\r\n\
                <TAVOLOTXT>' + comanda.tavolotxt + '</TAVOLOTXT>\r\n\
                <ULTPORT>N</ULTPORT>\r\n\
                <AGG_GIAC>N</AGG_GIAC>\r\n\
                </TAVOLI>\r\n\
                </dataroot>';
            //LIB5 S=CONTO STAMPATO

            output = output.replace(/>null</gi, '><');
            output = output.replace(/>undefined</gi, '><');

            console.log("OUTPUT XML STORNO" + comanda.tavolotxt, output);


            var d = new Date();
            var Y = d.getFullYear();
            var M = addZero(d.getDate(), 2);
            var D = addZero((d.getMonth() + 1), 2);
            var h = addZero(d.getHours(), 2);
            var m = addZero(d.getMinutes(), 2);
            var s = addZero(d.getSeconds(), 2);
            var ms = addZero(d.getMilliseconds(), 3);
            var data_tablet = h + '' + m + '' + s;

            var id_tablet = "29";
            var id_temp = leggi_get_url('id');

            if (id_temp !== undefined && id_temp !== null && id_temp !== 'null' && id_temp.length === 2) {
                id_tablet = id_temp;
            }

            var nome_file = "/SDS/PDA" + id_tablet + "_" + data_tablet + ".XM_";

            comanda.xml.salva_file_xml("_" + comanda.folder_number + "/" + nome_file, output, "SDS", function () {

                comanda.funzionidb.conto_attivo();


            });
        });

    });
}

function tav_ripara_tavolo() {
    var password_r = bootbox.prompt({
        size: "small",
        title: lang[250],
        inputType: "password",
        callback: function (result) {

            if (result === "RIPRI") {

                bootbox.alert(lang[251]);

                $('#tav_ripara_tavolo').removeClass('btn-tav');
                $('#tav_ripara_tavolo').addClass('btn-danger');
                comanda.ripara_tavolo_cliccato = true;
                $('.tavolo.cerc,.tavolo.rett').on(comanda.eventino,
                        function (event) {
                            event.preventDefault();
                            var tavolo = $(this).attr('id').substr(4);

                            ricrea_tavolo_danneggiato(tavolo, function () {

                                $('#tav_ripara_tavolo').addClass('btn-tav');
                                $('#tav_ripara_tavolo').removeClass('btn-danger');

                                salva_log_sempreattivo(comanda.id_tablet + " # RIPARA TAVOLO " + tavolo);

                                salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + aggZero(new Date().getMilliseconds(), 3) + " # RIPARA TAVOLO # TAVOLO: " + tavolo + " # ");

                                disegna_tavoli_salvati(tavolo);

                                setTimeout(function () {
                                    comanda.xml.elimina_tavolo_occupato("_" + comanda.folder_number + "/COND/Tav" + tavolo.replace(/^0+/, '') + ".CO1", false, function () {
                                        btn_tavoli();
                                    });
                                    $('.tavolo.cerc,.tavolo.rett').off();
                                }, 1000);

                                comanda.ripara_tavolo_cliccato = false;

                            });

                        });

            } else
            {
                bootbox.alert(lang[248]);
            }
        }
    });

    password_r.init(function () {

        $("input.bootbox-input").focus(function () {
            $("input.bootbox-input").trigger("touchend");
            $(this).blur();
        });


    });

}

function ricrea_tavolo_danneggiato(id, callback) {

    if (oggetto_tavoli_presalvataggio[id] !== undefined) {

        comanda.xml.elimina_tavolo_occupato("_" + comanda.folder_number + "/COND/Tav" + id.replace(/^0+/, '') + ".CO1", false, function () {

            comanda.xml.salva_file_xml(oggetto_tavoli_presalvataggio[id].nome_file_sds, oggetto_tavoli_presalvataggio[id].contenuto_file_sds, oggetto_tavoli_presalvataggio[id].tipo_file_sds, function () {

                comanda.xml.salva_file_xml(oggetto_tavoli_presalvataggio[id].nome_file_planing, oggetto_tavoli_presalvataggio[id].contenuto_file_planing, oggetto_tavoli_presalvataggio[id].tipo_file_planing, function () {

                    callback(true);

                });
            });
        });
    } else
    {
        callback(false);
    }
}



//FA PARTE DELLA FUNZIONE SOTTO
var esperimento_output;
var oggetto_articoli_numerati;
var prodotti_fuoriscope = new Array();
comanda.funzionidb.elenco_prodotti = function (callBack) {

    console.log("INIZIO CALLBACK PRODOTTI");
    oggetto_articoli_numerati = new Object();
    esperimento_output = new Object();
    if (comanda.tipo_ricerca_articolo === 'NUMERICA') {
        var contatore_prodotti_numerici = 0;
        var query_prodotti = 'select id, ordinamento, cat_varianti, red, green, blue, descrizione,prezzo_1, categoria, pagina, posizione, perc_iva_base, perc_iva_takeaway from prodotti WHERE categoria!="XXX" and  descrizione!="" ORDER BY descrizione ASC;';
        //FUNZIONE DELLA QUERY
        comanda.sincro.query(query_prodotti, function (prodotto) {
            prodotto.forEach(function (prod, i2) {
                oggetto_articoli_numerati[contatore_prodotti_numerici] = prod;
                contatore_prodotti_numerici++;
            });
            callBack(true);
        });
    } else {

//QUERY DI SELEZIONE CATEGORIE

        var query_categorie = "select * from categorie where(categorie.escl_pda!='S' OR categorie.escl_pda is null) and descrizione NOT LIKE '%PREFERITI_%';";

        var prodotti = new Array();
        //VARIABILE DI OUTPUT
        comanda.sincro.query(query_categorie, function (categoria) {

            var count_cat_number = categoria.length;
            var cat_n = 1;

            if (comanda.tutte_le_varianti === 'S') {
                prodotti["VTU"] = new Object();
                prodotti["VTU"]["1"] = new Array();

                esperimento_output["VTU"] = new Object();
                esperimento_output["VTU"]["1"] = '';
                esperimento_output["VTU"]["1"] += "<div class='pag_1 cat_VVV' >";
                esperimento_output["VTU"]["1"] += "<div class='bs-docs-section btn_COMANDA'>";
                esperimento_output["VTU"]["1"] += "<div class='bs-glyphicons'>";
                esperimento_output["VTU"]["1"] += "<ul class='bs-glyphicons-list elenco_prodotti_draggable'>";
            }

            categoria.forEach(function (cat, i1) {

                //RIEMPIO I DATI DELLE CATEGORIE
                if (prodotti[cat.id] === undefined) {
                    prodotti[cat.id] = new Object();
                }

                var query_canc_prod_mod = "delete from prodotti where categoria='XXX';";
                comanda.sincro.query(query_canc_prod_mod, function () {

                    //QUERY DI SELEZIONE PRODOTTI
                    //IN QUESTO CASO LA POSIZIONE HA SOLO UN NUMERO E NON DELLE COORDINATE
                    var query_prodotti = '';

                    /*if (comanda.numero_licenza_cliente === "0351") {
                     query_prodotti = 'select id, ordinamento, cat_varianti, red, green, blue, descrizione,prezzo_1, categoria, pagina, posizione, perc_iva_base, perc_iva_takeaway from prodotti WHERE categoria!="XXX" and  descrizione!="" AND categoria="' + cat.id + '" and posizione!="" ORDER BY ordinamento ASC,descrizione ASC;';
                     } else {*/
                    query_prodotti = 'select id, ordinamento, cat_varianti, red, green, blue, descrizione,prezzo_1, categoria, pagina, posizione, perc_iva_base, perc_iva_takeaway from prodotti WHERE categoria!="XXX" and  descrizione!="" AND categoria="' + cat.id + '" ORDER BY ordinamento ASC,descrizione ASC;';
                    /* }*/
                    //FUNZIONE DELLA QUERY
                    comanda.sincro.query(query_prodotti, function (prodotto) {
                        var i = 0;
                        var pagina = 0;
                        //PER OGNI PRODOTTO VADO A CREARE UNA SERIE DI ARRAY CON VARI DATI



                        prodotto.forEach(function (prod, i2) {

                            //CALCOLA LA PAGINA IN BASE AL NUMERO DI PRODOTTI
                            //Al 50esimo articolo cambia pagina e non 49esimo!
                            if (i % 49 === 0 && comanda.mobile !== true)
                            {
                                pagina++;
                            } else
                            {
                                pagina = 1;
                            }

                            if (prodotti[cat.id][pagina] === undefined) {
                                prodotti[cat.id][pagina] = new Object();
                            }

                            if (prodotti[cat.id][pagina][i] === undefined) {
                                prodotti[cat.id][pagina][i] = new Object();
                            }

                            //DATI PRODOTTO
                            prodotti[cat.id][pagina][i].id = prod.id;
                            prodotti[cat.id][pagina][i].cat_varianti = prod.cat_varianti;
                            prodotti[cat.id][pagina][i].red = prod.red;
                            prodotti[cat.id][pagina][i].green = prod.green;
                            prodotti[cat.id][pagina][i].blue = prod.blue;
                            prodotti[cat.id][pagina][i].descrizione = prod.descrizione;

                            if (prod.prezzo_1 === undefined) {
                                console.log(prod, i2);
                            }

                            if (prod.prezzo_1 !== undefined && prod.prezzo_1 !== null && prod.prezzo_1 !== 'null' && prod.prezzo_1 !== '')
                            {
                                prodotti[cat.id][pagina][i].prezzo_1 = prod.prezzo_1.replace(",", ".");
                            } else
                            {
                                prodotti[cat.id][pagina][i].prezzo_1 = '0.00';
                            }
                            prodotti[cat.id][pagina][i].categoria = prod.categoria;
                            prodotti[cat.id][pagina][i].pagina = prod.pagina;
                            prodotti[cat.id][pagina][i].ordinamento = prod.ordinamento;
                            prodotti[cat.id][pagina][i].posizione = prod.posizione;
                            prodotti[cat.id][pagina][i].perc_iva_base = prod.perc_iva_base;
                            prodotti[cat.id][pagina][i].perc_iva_takeaway = prod.perc_iva_takeaway;

                            //-------------------------------------------------------------------------------------------------//
                            if (comanda.tutte_le_varianti === 'S') {
                                if (prod.categoria !== undefined && prod.categoria[0] !== undefined && prod.categoria[0] === 'V') {


                                    //DATI PRODOTTO
                                    /*prodotti["VTU"]["1"][i].id = prod.id;
                                     prodotti["VTU"]["1"][i].cat_varianti = prod.cat_varianti;
                                     prodotti["VTU"]["1"][i].red = prod.red;
                                     prodotti["VTU"]["1"][i].green = prod.green;
                                     prodotti["VTU"]["1"][i].blue = prod.blue;
                                     prodotti["VTU"]["1"][i].descrizione = prod.descrizione;*/


                                    var prezzo_1 = '0.00';
                                    if (prod.prezzo_1 !== undefined && prod.prezzo_1 !== null && prod.prezzo_1 !== 'null' && prod.prezzo_1 !== '')
                                    {
                                        //prodotti["VTU"]["1"][i].prezzo_1 = prod.prezzo_1.replace(",", ".");
                                        prezzo_1 = prod.prezzo_1.replace(",", ".");
                                    } else
                                    {
                                        //prodotti["VTU"]["1"][i].prezzo_1 = '0.00';
                                        prezzo_1 = '0.00';
                                    }
                                    /*prodotti["VTU"]["1"][i].categoria = prod.categoria;
                                     prodotti["VTU"]["1"][i].pagina = prod.pagina;
                                     prodotti["VTU"]["1"][i].ordinamento = prod.ordinamento;
                                     prodotti["VTU"]["1"][i].posizione = prod.posizione;
                                     prodotti["VTU"]["1"][i].perc_iva_base = prod.perc_iva_base;
                                     prodotti["VTU"]["1"][i].perc_iva_takeaway = prod.perc_iva_takeaway;*/

                                    var obj_length = prodotti["VTU"]["1"].length;

                                    prodotti["VTU"]["1"][obj_length] = {id: prod.id, cat_varianti: prod.cat_varianti, red: prod.red, green: prod.green, blue: prod.blue, descrizione: prod.descrizione, prezzo_1: prezzo_1, categoria: prod.categoria, pagina: "1", ordinamento: prod.ordinamento, posizione: prod.posizione, perc_iva_base: prod.perc_iva_base, perc_iva_takeaway: prod.perc_iva_takeaway};

                                }
                            }
                            i++;
                        });


                        /*ESPERIMENTO VARIANTI LIBERE SENZA CATEGORIE*/
                        if (comanda.tutte_le_varianti === 'S') {
                            esperimento_output["VVV"] = new Object();

                            esperimento_output["VVV"]["1"] = '';
                            esperimento_output["VVV"]["1"] += "<div class='pag_1 cat_VVV' >";
                            esperimento_output["VVV"]["1"] += "<div class='bs-docs-section btn_COMANDA'>";
                            esperimento_output["VVV"]["1"] += "<div class='bs-glyphicons'>";
                            esperimento_output["VVV"]["1"] += "<ul class='bs-glyphicons-list elenco_prodotti_draggable'>";

                            esperimento_output["VVV"]["1"] += '<li style="background-color: lightblue; background-image:-webkit-linear-gradient(top, lightblue, rgb(97, 174, 199)); font-size: 2.5vh;" class="btn_variante_libera btn_comanda prodotto_esistente"  ' + comanda.evento + '="comanda.categoria=\'VTU\';elenco_prodotti();"><span class="glyphicon-class"><strong>TUTTE</strong></span></li>';

                            esperimento_output["VVV"]["1"] += '<li style="background-color: lightblue; background-image:-webkit-linear-gradient(top, lightblue, rgb(97, 174, 199)); font-size: 2.5vh;" value="VARIANTELIBERA" class="btn_variante_libera btn_comanda prodotto_esistente"  ' + comanda.evento + '="popup_variante_libera();"><span class="glyphicon-class"><strong>+</strong></span></li>';

                            esperimento_output["VVV"]["1"] += "</ul>";
                            esperimento_output["VVV"]["1"] += "</div>";
                            esperimento_output["VVV"]["1"] += "</div>";
                            esperimento_output["VVV"]["1"] += "</div>";
                        }



                        //PRENDO LE PAGINE NEI PRODOTTI
                        for (var p in prodotti[cat.id]) {
                            if (esperimento_output[cat.id] === undefined)
                            {
                                esperimento_output[cat.id] = new Object();
                            }

                            if (esperimento_output[cat.id][p] === undefined)
                            {
                                esperimento_output[cat.id][p] = '';
                            }

                            esperimento_output[cat.id][p] += "<div class='pag_" + p + " cat_" + cat.id + "' >";
                            esperimento_output[cat.id][p] += "<div class='bs-docs-section btn_COMANDA'>";
                            esperimento_output[cat.id][p] += "<div class='bs-glyphicons'>";
                            esperimento_output[cat.id][p] += "<ul class='bs-glyphicons-list elenco_prodotti_draggable'>";
                            var i = 0;

                            if (comanda.variante_libera === "S" && cat.descrizione.substr(0, 2) === 'V.' && i === 0) {
                                if (comanda.tutte_le_varianti === 'S') {
                                    i++;
                                    esperimento_output[cat.id][p] += '<li style="background-color: lightblue; background-image:-webkit-linear-gradient(top, lightblue, rgb(97, 174, 199)); font-size: 2.5vh;" class="btn_variante_libera btn_comanda prodotto_esistente"  ' + comanda.evento + '="comanda.categoria=\'VTU\';elenco_prodotti();"><span class="glyphicon-class"><strong>TUTTE</strong></span></li>';
                                }
                                i++;
                                esperimento_output[cat.id][p] += '<li style="background-color: lightblue; background-image:-webkit-linear-gradient(top, lightblue, rgb(97, 174, 199)); font-size: 2.5vh;" value="VARIANTELIBERA" class="btn_variante_libera btn_comanda prodotto_esistente"  ' + comanda.evento + '="popup_variante_libera();"><span class="glyphicon-class"><strong>+</strong></span></li>';

                            } else if (comanda.p_libero_abilitato === "S" && cat.descrizione.substr(0, 2) !== 'V.' && i === 0) {
                                i++;
                                esperimento_output[cat.id][p] += '<li style="background-color:rgb(255, 245, 238);background-image:-webkit-linear-gradient(top, rgb(255, 245, 238), rgb(197, 197, 197)); font-size: 2.5vh;" class="btn_variante_libera btn_comanda prodotto_esistente"  ' + comanda.evento + '="popup_prodotto_libero();"><span class="glyphicon-class"><strong>+</strong></span></li>';

                            }
                            //PRENDO I PRODOTTI
                            for (var art in prodotti[cat.id][p]) {

                                var articolo = prodotti[cat.id][p][art];
                                var colore = '';
                                if (cat.id[0] === 'V')
                                {
                                    if (articolo.red !== undefined && articolo.green !== undefined && articolo.blue !== undefined && articolo.red.trim() !== "" && articolo.green.trim() !== "" && articolo.blue.trim() !== "" && articolo.red.trim() !== "N" && articolo.green.trim() !== "N" && articolo.blue.trim() !== "N") {
                                        colore = ' style="background-color:rgb(' + articolo.red + ',' + articolo.green + ',' + articolo.blue + ');background-image:-webkit-linear-gradient(top, rgb(' + articolo.red + ',' + articolo.green + ',' + articolo.blue + '), rgba(0, 0, 0, 0.35));" ';

                                    } else
                                    {
                                        colore = ' style="background-color:lightblue;background-image:-webkit-linear-gradient(top, lightblue,#61aec7);" ';
                                    }
                                    //colore = ' style="background-color:lightblue;background-image:-webkit-linear-gradient(top, lightblue,#61aec7);" ';
                                } else if (comanda.colore_tasti_mobile === true && (articolo.red !== undefined && articolo.red !== null && articolo.red.length > 0) && (articolo.green !== undefined && articolo.green !== null && articolo.green.length > 0) && (articolo.blue !== undefined && articolo.blue !== null && articolo.blue.length > 0))
                                {
                                    colore = ' style="background-color: rgb(' + articolo.red + ',' + articolo.green + ',' + articolo.blue + ');" ';
                                } else if (comanda.mobile === true)
                                {
                                    colore = ' style="background-color:rgb(255, 245, 238);background-image:-webkit-linear-gradient(top, rgb(255, 245, 238), rgb(197, 197, 197));" ';
                                }

                                var classe_ord = '';
                                if (articolo.ordinamento !== '999') {
                                    classe_ord = 'escl_ord';
                                }

                                //SE LA VARIANTE E' AUTOMATICA
                                console.log("cat_varianti", articolo.cat_varianti, articolo.descrizione);

                                if (articolo.cat_varianti !== undefined && articolo.cat_varianti !== null /*&& articolo.cat_varianti.trim() !== "" */ && articolo.cat_varianti.search("/") !== -1) {

                                    console.log("cat_varianti", "IF1");
                                    if (cat.var_aperte === 'S') {
                                        //SE LA VARIANTE AUTOMATICA E' APERTA

                                        esperimento_output[cat.id][p] += '<li ' + colore + ' value="' + articolo.id + '" class="' + classe_ord + ' btn_comanda prodotto_esistente"  ' + comanda.evento + '="if (sblocco_tasti === true) {sblocco_tasti = false;comanda.flag_tipologia_articolo = \'AUTOAPERTA\';aggiungi_articolo(\'' + articolo.id + '\',\'' + articolo.descrizione.replace(/'/gi, ' ').replace('\'', "\\'") + '\',\'&euro;' + articolo.prezzo_1 + '\',null,$(\'#quantita_articolo\').val(),null,\'' + articolo.cat_varianti.match(/\w+/gi)[0] + '\',null,null,\'' + articolo.perc_iva_base + '\',\'' + articolo.perc_iva_takeaway + '\');setTimeout(function(){comanda.ultima_cat_variante=\'' + articolo.cat_varianti.match(/\w+/gi)[0] + '\';},250);}">';
                                        /*esperimento_output["VTU"]["1"] += '<li ' + colore + ' value="' + articolo.id + '" class="' + classe_ord + ' btn_comanda prodotto_esistente"  ' + comanda.evento + '="if (sblocco_tasti === true) {sblocco_tasti = false;comanda.flag_tipologia_articolo = \'AUTOAPERTA\';aggiungi_articolo(\'' + articolo.id + '\',\'' + articolo.descrizione.replace(/'/gi, ' ').replace('\'', "\\'") + '\',\'&euro;' + articolo.prezzo_1 + '\',null,$(\'#quantita_articolo\').val(),null,\'' + articolo.cat_varianti.match(/\w+/gi)[0] + '\',null,null,\'' + articolo.perc_iva_base + '\',\'' + articolo.perc_iva_takeaway + '\');setTimeout(function(){comanda.ultima_cat_variante=\'' + articolo.cat_varianti.match(/\w+/gi)[0] + '\';},250);}">';
                                         esperimento_output["VTU"]["1"] += '<span class="glyphicon-class"><strong>' + articolo.descrizione.replace(/'/gi, ' ').replace('\'', "\\'") + '</strong><br/>&euro; ' + articolo.prezzo_1 + '</span>';
                                         esperimento_output["VTU"]["1"] += '</li>';*/
                                    } else {
                                        //SE LA VARIANTE AUTOMATICA NON E' APERTA

                                        esperimento_output[cat.id][p] += '<li ' + colore + ' value="' + articolo.id + '" class="' + classe_ord + ' btn_comanda prodotto_esistente"  ' + comanda.evento + '="if (sblocco_tasti === true) {sblocco_tasti = false;comanda.flag_tipologia_articolo = \'AUTO\';aggiungi_articolo(\'' + articolo.id + '\',\'' + articolo.descrizione.replace(/'/gi, ' ').replace('\'', "\\'") + '\',\'&euro;' + articolo.prezzo_1 + '\',null,$(\'#quantita_articolo\').val(),null,\'' + articolo.cat_varianti.match(/\w+/gi)[0] + '\',null,null,\'' + articolo.perc_iva_base + '\',\'' + articolo.perc_iva_takeaway + '\');setTimeout(function(){comanda.ultima_cat_variante=\'' + articolo.cat_varianti.match(/\w+/gi)[0] + '\';mod_categoria(\'' + articolo.cat_varianti.match(/\w+/gi)[1] + '\', \'=\');},250);}">';
                                        /*esperimento_output["VTU"]["1"] += '<li ' + colore + ' value="' + articolo.id + '" class="' + classe_ord + ' btn_comanda prodotto_esistente"  ' + comanda.evento + '="if (sblocco_tasti === true) {sblocco_tasti = false;comanda.flag_tipologia_articolo = \'AUTO\';aggiungi_articolo(\'' + articolo.id + '\',\'' + articolo.descrizione.replace(/'/gi, ' ').replace('\'', "\\'") + '\',\'&euro;' + articolo.prezzo_1 + '\',null,$(\'#quantita_articolo\').val(),null,\'' + articolo.cat_varianti.match(/\w+/gi)[0] + '\',null,null,\'' + articolo.perc_iva_base + '\',\'' + articolo.perc_iva_takeaway + '\');setTimeout(function(){comanda.ultima_cat_variante=\'' + articolo.cat_varianti.match(/\w+/gi)[0] + '\';mod_categoria(\'' + articolo.cat_varianti.match(/\w+/gi)[1] + '\', \'=\');},250);}">';
                                         esperimento_output["VTU"]["1"] += '<span class="glyphicon-class"><strong>' + articolo.descrizione.replace(/'/gi, ' ').replace('\'', "\\'") + '</strong><br/>&euro; ' + articolo.prezzo_1 + '</span>';
                                         esperimento_output["VTU"]["1"] += '</li>';*/
                                    }

                                    //SE E' UNA VARIANTE
                                } else if (articolo.categoria[0] === 'V')
                                {
                                    console.log("cat_varianti", "IF2");

                                    if (cat.var_aperte === 'S') {
                                        //SE E' UNA VARIANTE APERTA (IN REALTA' E' UN CASO AL LIMITE DELL'UMANO, MA NON SI SA MAI

                                        esperimento_output[cat.id][p] += '<li ' + colore + ' value="' + articolo.id + '" class="' + classe_ord + ' btn_comanda prodotto_esistente"  ' + comanda.evento + '="if (sblocco_tasti === true) {sblocco_tasti = false;comanda.flag_tipologia_articolo = \'APERTA\';aggiungi_articolo(\'' + articolo.id + '\',\'' + articolo.descrizione.replace(/'/gi, ' ').replace('\'', "\\'") + '\',\'&euro;' + articolo.prezzo_1 + '\',null,$(\'#quantita_articolo\').val(),null,\'' + articolo.cat_varianti + '\',null,null,\'' + articolo.perc_iva_base + '\',\'' + articolo.perc_iva_takeaway + '\');}">';
                                        if (comanda.tutte_le_varianti === 'S') {
                                            esperimento_output["VTU"]["1"] += '<li ' + colore + ' value="' + articolo.id + '" class="' + classe_ord + ' btn_comanda prodotto_esistente"  ' + comanda.evento + '="if (sblocco_tasti === true) {sblocco_tasti = false;comanda.flag_tipologia_articolo = \'VARIANTE\';aggiungi_articolo(\'' + articolo.id + '\',\'' + articolo.descrizione.replace(/'/gi, ' ').replace('\'', "\\'") + '\',\'&euro;' + articolo.prezzo_1 + '\',null,$(\'#quantita_articolo\').val(),null,\'' + articolo.cat_varianti + '\',null,null,\'' + articolo.perc_iva_base + '\',\'' + articolo.perc_iva_takeaway + '\');}">';
                                            esperimento_output["VTU"]["1"] += '<span class="glyphicon-class"><strong>' + articolo.descrizione.replace(/'/gi, ' ').replace('\'', "\\'") + '</strong><br/>&euro; ' + articolo.prezzo_1 + '</span>';
                                            esperimento_output["VTU"]["1"] += '</li>';
                                        }
                                    } else {
                                        //SE E' UNA VARIANTE NON APERTA

                                        esperimento_output[cat.id][p] += '<li ' + colore + ' value="' + articolo.id + '" class="' + classe_ord + ' btn_comanda prodotto_esistente"  ' + comanda.evento + '="if (sblocco_tasti === true) {sblocco_tasti = false;comanda.flag_tipologia_articolo =\'VARIANTE\';aggiungi_articolo(\'' + articolo.id + '\',\'' + articolo.descrizione.replace(/'/gi, ' ').replace('\'', "\\'") + '\',\'&euro;' + articolo.prezzo_1 + '\',null,$(\'#quantita_articolo\').val(),null,\'' + articolo.cat_varianti + '\',null,null,\'' + articolo.perc_iva_base + '\',\'' + articolo.perc_iva_takeaway + '\');}">';
                                        if (comanda.tutte_le_varianti === 'S') {
                                            esperimento_output["VTU"]["1"] += '<li ' + colore + ' value="' + articolo.id + '" class="' + classe_ord + ' btn_comanda prodotto_esistente"  ' + comanda.evento + '="if (sblocco_tasti === true) {sblocco_tasti = false;comanda.flag_tipologia_articolo =\'VARIANTE\';aggiungi_articolo(\'' + articolo.id + '\',\'' + articolo.descrizione.replace(/'/gi, ' ').replace('\'', "\\'") + '\',\'&euro;' + articolo.prezzo_1 + '\',null,$(\'#quantita_articolo\').val(),null,\'' + articolo.cat_varianti + '\',null,null,\'' + articolo.perc_iva_base + '\',\'' + articolo.perc_iva_takeaway + '\');}">';
                                            esperimento_output["VTU"]["1"] += '<span class="glyphicon-class"><strong>' + articolo.descrizione.replace(/'/gi, ' ').replace('\'', "\\'") + '</strong><br/>&euro; ' + articolo.prezzo_1 + '</span>';
                                            esperimento_output["VTU"]["1"] += '</li>';
                                        }
                                    }
                                }
                                //SE E' UN NORMALISSIMO ARTICOLO
                                else {
                                    console.log("cat_varianti", "IF3");

                                    if (articolo.cat_varianti === undefined) {
                                        articolo.cat_varianti = "VVV";
                                    }

                                    esperimento_output[cat.id][p] += '<li ' + colore + ' value="' + articolo.id + '" class="' + classe_ord + ' btn_comanda prodotto_esistente"  ' + comanda.evento + '="if (sblocco_tasti === true) {sblocco_tasti = false;comanda.flag_tipologia_articolo = \'NORMALE\';aggiungi_articolo(\'' + articolo.id + '\',\'' + articolo.descrizione.replace(/'/gi, ' ').replace('\'', "\\'") + '\',\'&euro;' + articolo.prezzo_1 + '\',null,$(\'#quantita_articolo\').val(),null,\'' + articolo.cat_varianti + '\',null,null,\'' + articolo.perc_iva_base + '\',\'' + articolo.perc_iva_takeaway + '\');}">';
                                    /*esperimento_output["VTU"]["1"] += '<li ' + colore + ' value="' + articolo.id + '" class="' + classe_ord + ' btn_comanda prodotto_esistente"  ' + comanda.evento + '="if (sblocco_tasti === true) {sblocco_tasti = false;comanda.flag_tipologia_articolo = \'NORMALE\';aggiungi_articolo(\'' + articolo.id + '\',\'' + articolo.descrizione.replace(/'/gi, ' ').replace('\'', "\\'") + '\',\'&euro;' + articolo.prezzo_1 + '\',null,$(\'#quantita_articolo\').val(),null,\'' + articolo.cat_varianti + '\',null,null,\'' + articolo.perc_iva_base + '\',\'' + articolo.perc_iva_takeaway + '\');}">';
                                     esperimento_output["VTU"]["1"] += '<span class="glyphicon-class"><strong>' + articolo.descrizione.replace(/'/gi, ' ').replace('\'', "\\'") + '</strong><br/>&euro; ' + articolo.prezzo_1 + '</span>';
                                     esperimento_output["VTU"]["1"] += '</li>';*/
                                }

                                esperimento_output[cat.id][p] += '<span class="glyphicon-class"><strong>' + articolo.descrizione.replace(/'/gi, ' ').replace('\'', "\\'") + '</strong><br/>&euro; ' + articolo.prezzo_1 + '</span>';
                                esperimento_output[cat.id][p] += '</li>';



                                i++;
                            }

                            if (comanda.mobile !== true) {
                                //Serve a riempire le restanti caselle della griglia
                                for (; i % 49 !== 0; i++) {
                                    esperimento_output[cat.id][p] += '<li class="btn_comanda" >';
                                    esperimento_output[cat.id][p] += '<span class="glyphicon-class"><br/></span>';
                                    esperimento_output[cat.id][p] += '</li>';
                                }
                            }

                            esperimento_output[cat.id][p] += "</ul>";
                            esperimento_output[cat.id][p] += "</div>";
                            esperimento_output[cat.id][p] += "</div>";
                            esperimento_output[cat.id][p] += "</div>";
                        }



                        prodotti_fuoriscope = prodotti;
                        if (cat_n === count_cat_number) {
                            console.log("FINE CALLBACK PRODOTTI");
                            callBack(true);
                            //SERVE AD EVITARE CHE SCORRENDO GLI ARTICOLI CLICCHI
                            $(".bs-glyphicons").scrollstart(function (event) {

                                comanda.drag = true;
                            });
                            $(".bs-glyphicons").scrollstop(function (event) {

                                comanda.drag = false;
                            });
                        }

                        cat_n++;
                    });
                });
            });
            if (comanda.tutte_le_varianti === 'S') {
                esperimento_output["VTU"]["1"] += "</ul>";
                esperimento_output["VTU"]["1"] += "</div>";
                esperimento_output["VTU"]["1"] += "</div>";
                esperimento_output["VTU"]["1"] += "</div>";
            }


        });
    }
};
var blocco_comanda = false;

var situazione_estrema;
function lettura_tavolo_xml(tavolo, callback, tavolo_da_parametro) {



    //aggiunto altrimenti teneva in memoria la lettera dell'ultimo conto chiuso
    //e quando aprivi un bis dopo avere storicizzato quella lettera
    //non legge la testa in modo corretto
    //errore rilevato da me e andrea il 7 settembre 2020
    comanda.numero_conto_split = 0;

    salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + aggZero(new Date().getMilliseconds(), 3) + " # INIZIO LETTURA TAVOLO # TAVOLO: " + tavolo + " # ");

    if (blocco_comanda === false) {
        blocco_comanda = true;

        $(".scritta_totale").html("");
        $(".totale_incassato_lente").html("");
        $("#tessere_drink").html("0");
        $("#tessere_exit").html("0");
        $(".totale_incassato_lente").closest("div").hide();

        $("#tasto_batti_coperti").show();
        $("#numero_coperti").hide();

        $("#numero_coperti").val("");

        //Dopo 3 secondi a prescindere il tavolo si deve sbloccare
        //Altrimenti andando fuori portata possono esserci molti problemi
        situazione_estrema = setTimeout(function () {
            blocco_comanda = false;
            $('.loader').hide();
        }, 6000);

        console.log("LETTURA TAVOLO XML");
        var variabile_tavolo = comanda.tavolo;
        if (tavolo_da_parametro === true) {
            variabile_tavolo = tavolo;
        }

        if (variabile_tavolo !== undefined && typeof (variabile_tavolo) === "string") {

            var log = new Date().format("HH:mm:ss");
            log += " " + comanda.operatore;

            comanda.xml.salva_file_xml("_" + comanda.folder_number + "/COND/Tav" + variabile_tavolo.replace(/^0+/, '') + ".CO1", log, "COND", function (cbs) {

                if (cbs !== false) {



                    var tasti_da_nascondere;
                    if (comanda.storicizzazione_auto === 'S') {                                                                                                                //SE CE LA COMANDA PIU CONTO SERVE SOLO IL TASTO COMANDA
                        if (comanda.comanda_conto === 'S') {
                            tasti_da_nascondere = $('.layout_comanda_conto_stor');
                        } else if (comanda.comanda_conto === 'N') {
                            tasti_da_nascondere = $('.layout_comanda_senza_conto_stor');
                        }
                    } else if (comanda.storicizzazione_auto === 'N') {
//SE CE LA COMANDA PIU CONTO SERVE SOLO IL TASTO COMANDA
                        if (comanda.comanda_conto === 'S') {
                            tasti_da_nascondere = $('.layout_comanda_conto_no_stor');
                        } else if (comanda.comanda_conto === 'N') {
                            tasti_da_nascondere = $('.layout_comanda_senza_conto_no_stor');
                        }
                    }

                    if (tasti_da_nascondere !== undefined) {
                        tasti_da_nascondere.css('pointer-events', 'none');
                        setTimeout(function () {
                            tasti_da_nascondere.css('pointer-events', '');
                        }, 1000);
                    }
                    setTimeout(function () {
                        $('#tasto_uscita_tavoli,.scritta_operatore,.numero_tavolo').css('opacity', '');
                        $('#tasto_uscita_tavoli,.scritta_operatore,.numero_tavolo').css('pointer-events', '');
                    }, 2000);
                    var array_corrispondenze = new Array();
                    //DICHIARAZIONE ARRAY DELLE CORRISPONDENZE
                    array_corrispondenze["TC"] = new Array();
                    array_corrispondenze["ART"] = new Array();
                    array_corrispondenze["CARTACRED"] = new Array();
                    array_corrispondenze["BANCOMAT"] = new Array();
                    array_corrispondenze["ASSEGNI"] = new Array();
                    array_corrispondenze["NCARD2"] = new Array();
                    array_corrispondenze["NCARD3"] = new Array();
                    array_corrispondenze["NCARD4"] = new Array();
                    array_corrispondenze["NCARD5"] = new Array();
                    array_corrispondenze["NSEGN"] = new Array();
                    array_corrispondenze["NEXIT"] = new Array();
                    array_corrispondenze["TS"] = new Array();
                    array_corrispondenze["ORA"] = new Array();
                    array_corrispondenze["LIB1"] = new Array();
                    array_corrispondenze["LIB2"] = new Array();
                    array_corrispondenze["LIB3"] = new Array();
                    array_corrispondenze["LIB4"] = new Array();
                    array_corrispondenze["CLI"] = new Array();
                    array_corrispondenze["QTAP"] = new Array();
                    array_corrispondenze["TAVOLO"] = new Array();
                    array_corrispondenze["ULTPORT"] = new Array();
                    array_corrispondenze["LIB5"] = new Array();
                    array_corrispondenze["AGG_GIAC"] = new Array();
                    array_corrispondenze["DATA"] = new Array();
                    //CANCELLAZIONE COLONNE CHE NON SERVONO AL MOMENTO
                    array_corrispondenze["TC"].colonna_db = "DEL";
                    array_corrispondenze["ART"].colonna_db = "DEL";
                    array_corrispondenze["CARTACRED"].colonna_db = "carta_credito";
                    array_corrispondenze["BANCOMAT"].colonna_db = "bancomat";
                    array_corrispondenze["ASSEGNI"].colonna_db = "perc_iva";
                    array_corrispondenze["NCARD2"].colonna_db = "NCARD2";
                    array_corrispondenze["NCARD3"].colonna_db = "NCARD3";
                    array_corrispondenze["NCARD4"].colonna_db = "NCARD4";
                    array_corrispondenze["NCARD5"].colonna_db = "NCARD5";
                    array_corrispondenze["NSEGN"].colonna_db = "NSEGN";
                    array_corrispondenze["NEXIT"].colonna_db = "NEXIT";
                    array_corrispondenze["ORA"].colonna_db = "ora_";
                    array_corrispondenze["LIB1"].colonna_db = "LIB1";
                    array_corrispondenze["LIB2"].colonna_db = "LIB2";
                    array_corrispondenze["LIB3"].colonna_db = "DEL";
                    array_corrispondenze["LIB4"].colonna_db = "LIB4";
                    array_corrispondenze["CLI"].colonna_db = "DEL";
                    array_corrispondenze["QTAP"].colonna_db = "QTAP";
                    array_corrispondenze["TAVOLO"].colonna_db = "ntav_comanda";
                    array_corrispondenze["ULTPORT"].colonna_db = "ultima_portata";
                    array_corrispondenze["LIB5"].colonna_db = "dest_stampa_2";
                    array_corrispondenze["AGG_GIAC"].colonna_db = "DEL";
                    array_corrispondenze["DATA"].colonna_db = "DEL";
                    array_corrispondenze["TS"].colonna_db = "dest_stampa";
                    //CAMPI SOSTITUITI NEL DATABASE
                    array_corrispondenze["AUTORIZ"] = new Array();
                    array_corrispondenze["AUTORIZ"].colonna_db = "sconto_perc";
                    array_corrispondenze["SCONTO"] = new Array();
                    array_corrispondenze["SCONTO"].colonna_db = "sconto_imp";
                    array_corrispondenze["VAR"] = new Array();
                    array_corrispondenze["VAR"].colonna_db = "VAR";
                    array_corrispondenze["INC"] = new Array();
                    array_corrispondenze["INC"].colonna_db = "incassato";
                    array_corrispondenze["DES"] = new Array();
                    array_corrispondenze["DES"].colonna_db = "desc_art";
                    array_corrispondenze["TAVOLOTXT"] = new Array();
                    array_corrispondenze["TAVOLOTXT"].colonna_db = "DEL";
                    array_corrispondenze["BIS"] = new Array();
                    array_corrispondenze["BIS"].colonna_db = "BIS";
                    array_corrispondenze["ART2"] = new Array();
                    array_corrispondenze["ART2"].colonna_db = "cod_articolo";
                    array_corrispondenze["PRG"] = new Array();
                    array_corrispondenze["PRG"].colonna_db = "prog_inser";
                    array_corrispondenze["PRZ"] = new Array();
                    array_corrispondenze["PRZ"].colonna_db = "prezzo_un";
                    array_corrispondenze["QTA"] = new Array();
                    array_corrispondenze["QTA"].colonna_db = "quantita";
                    array_corrispondenze["SN"] = new Array();
                    array_corrispondenze["SN"].colonna_db = "stampata_sn";
                    array_corrispondenze["SN"].valori = new Array();
                    array_corrispondenze["SN"].valori["N"] = "N";
                    array_corrispondenze["SN"].valori["S"] = "S";
                    array_corrispondenze["CAT"] = new Array();
                    array_corrispondenze["CAT"].colonna_db = "categoria";
                    array_corrispondenze["NOME"] = new Array();
                    array_corrispondenze["NOME"].colonna_db = "operatore";
                    array_corrispondenze["NODO"] = new Array();
                    array_corrispondenze["NODO"].colonna_db = "nodo";
                    array_corrispondenze["PORTATA"] = new Array();
                    array_corrispondenze["PORTATA"].colonna_db = "portata";
                    array_corrispondenze["COSTO"] = new Array();
                    array_corrispondenze["COSTO"].colonna_db = "costo";
                    array_corrispondenze["NCARD1"] = new Array();
                    array_corrispondenze["NCARD1"].colonna_db = "contiene_variante";
                    array_corrispondenze["NCARD1"].valori = new Array();
                    array_corrispondenze["NCARD1"].valori["SI"] = "1";
                    array_corrispondenze["NUMP"] = new Array();
                    array_corrispondenze["NUMP"].colonna_db = "nump_xml";
                    array_corrispondenze["TOTALE"] = new Array();
                    array_corrispondenze["TOTALE"].colonna_db = "PRZ";
                    //CAMPI FISSI
                    var array_campi_fissi = new Array();
                    array_campi_fissi.push({colonna: "id", valore: id_comanda(false)});
                    array_campi_fissi.push({colonna: "tipo_record", valore: "CORPO"});
                    array_campi_fissi.push({colonna: "posizione", valore: "CONTO"});
                    array_campi_fissi.push({colonna: "stato_record", valore: "ATTIVO"});
                    array_campi_fissi.push({colonna: "prezzo_vero", valore: ""});
                    array_campi_fissi.push({colonna: "riepilogo", valore: ""});
                    array_campi_fissi.push({colonna: "data_servizio", valore: ""});
                    array_campi_fissi.push({colonna: "data_comanda", valore: ""});
                    array_campi_fissi.push({colonna: "ora_comanda", valore: ""});
                    array_campi_fissi.push({colonna: "numero_paganti", valore: ""});
                    //DELETE FROM comanda WHERE stato_record="ATTIVO" and (ntav_comanda="' + tavolo + '" OR desc_art LIKE "RECORD TESTA%" OR desc_art IS NULL)

                    comanda.sincro.query('DELETE FROM comanda;', function () {

                        comanda.xml = new ClasseXML("/" + comanda.directory_dati + "/" + comanda.funzionidb.data_cartella_xml() + "_" + comanda.folder_number + "/TAVOLO" + aggZero(tavolo, 3) + ".XML", "TAVOLI", "comanda", array_corrispondenze, array_campi_fissi, false, false, false);
                        comanda.xml.importa(function () {

                            let togli_tasto_bargeld = false;

                            let query_testa_gutshein = "select * from comanda where desc_art LIKE 'RECORD_TESTA%';";
                            let rtg = alasql(query_testa_gutshein);


                            if (rtg.length > 0) {


                                if (rtg[0].incassato === "0,00" && (rtg[0].costo === "0,00" || rtg[0].costo === ""))
                                {

                                } else {
                                    if (comanda.SETB36 === 'S' && comanda.PRF2 === 'SI') {
                                        togli_tasto_bargeld = true;
                                    }
                                }
                            }

                            if (togli_tasto_bargeld === true) {
                                toggle_tasti_incasso_mobile();
                                toggle_tasti_incasso_mobile();

                            } else {

                            }

                            if (comanda.SETB36 === 'S' && comanda.PRF2 === 'SI') {
                                $('.tasto_bargeld').hide();
                            }


                            comanda.sincro.query('SELECT desc_art,operatore FROM comanda WHERE  desc_art LIKE "RECORD TESTA%" AND operatore!="" AND stato_record="ATTIVO" ORDER BY operatore DESC LIMIT 1;', function (operatore) {

                                if (operatore[0] === undefined)
                                {
                                    operatore[0] = new Object();
                                }

                                if (operatore[0].operatore === undefined) {
                                    operatore[0].operatore = comanda.operatore;
                                }

                                if (operatore[0].desc_art !== undefined) {
                                    var indice_tid = operatore[0].desc_art.indexOf("TID=");

                                    if (indice_tid !== -1) {
                                        comanda.TID = operatore[0].desc_art.substr(indice_tid + 4);
                                    }
                                }

                                if (tavolo_da_parametro !== true && comanda.fastconto !== true && typeof (callback) === 'function' && comanda.blocco_tavolo_cameriere === 'S' && (comanda.operatore !== operatore[0].operatore)) {
                                    bootbox.alert("TAVOLO INIZIATO DA " + operatore[0].operatore + " e non puo' essere modificato da nessun altro.");
                                    comanda.xml.elimina_tavolo_occupato("_" + comanda.folder_number + "/COND/Tav" + variabile_tavolo.replace(/^0+/, '') + ".CO1", false, function () {

                                        try {
                                            $(document).off(comanda.eventino, evento_selettore_tavolo);
                                        } catch (e) {
                                            console.log(e);
                                        }
                                        $(document).on(comanda.eventino, 'div.tavolo.rett,div.tavolo.cerc', evento_selettore_tavolo);
                                        $('.tasto_bestellung').css('pointer-events', '');
                                        $('.tasto_bargeld').css('pointer-events', '');
                                        $('.tasto_quittung').css('pointer-events', '');
                                        $('.tasto_rechnung').css('pointer-events', '');
                                        $('.tasto_konto').css('pointer-events', '');

                                        salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + aggZero(new Date().getMilliseconds(), 3) + " # ERRORE LETTURA TAVOLO # TAVOLO: " + tavolo + " # ");

                                        callback(false);
                                        clearTimeout(situazione_estrema);
                                        blocco_comanda = false;
                                    });
                                } else {



                                    //var query_sposta_simbolo_variante = "UPDATE comanda SET VAR=null,desc_art= CASE WHEN VAR is not null AND (VAR='+' OR VAR='-' OR VAR='(' OR VAR='='OR VAR='*'OR VAR='x') THEN VAR||desc_art ELSE desc_art END;";
                                    var query_sposta_simbolo_variante = "UPDATE comanda SET desc_art= VAR||desc_art,VAR='';";
                                    console.log("AAA: POTREBBE MANCARE LA COLONNA JOLLY O VAR NELLA TAB. 'COMANDA'", query_sposta_simbolo_variante);
                                    comanda.sincro.query(query_sposta_simbolo_variante, function () {
                                        console.log("UPDATE ESEGUITO");


                                        var query_coperti = "SELECT nump_xml,contiene_variante,NEXIT,incassato,bancomat,carta_credito FROM comanda WHERE desc_art LIKE 'RECORD TESTA%' and ntav_comanda='" + variabile_tavolo + "' AND stato_record='ATTIVO'  AND BIS='" + lettera_conto_split() + "' ORDER BY nump_xml DESC limit 1;";
                                        console.log("lettura_tavolo_xml", query_coperti);
                                        comanda.sincro.query(query_coperti, function (rc) {
                                            comanda.ultimi_coperti_importati = '0';
                                            if (rc !== undefined && rc.length === 1)
                                            {



                                                if (rc[0].contiene_variante === "") {
                                                    rc[0].contiene_variante = "0";
                                                }

                                                if (rc[0].NEXIT === "") {
                                                    rc[0].NEXIT = "0";
                                                }

                                                $("#tessere_drink").html(rc[0].contiene_variante);
                                                $("#tessere_exit").html(rc[0].NEXIT);

                                                var contanti = 0;
                                                if (parseFloat(rc[0].incassato) > 0) {
                                                    contanti = parseFloat(rc[0].incassato.replace(".", "").replace(",", "."));
                                                }
                                                var bancomat = 0;
                                                if (parseFloat(rc[0].bancomat) > 0) {
                                                    bancomat = parseFloat(rc[0].bancomat.replace(".", "").replace(",", "."));
                                                }
                                                var carta_credito = 0;
                                                if (parseFloat(rc[0].carta_credito) > 0) {
                                                    carta_credito = parseFloat(rc[0].carta_credito.replace(".", "").replace(",", "."));
                                                }

                                                var incassato = contanti + bancomat + carta_credito;

                                                if (incassato > 0) {

                                                    $(".dati_tecnici_grande").css("visibility", "visible");

                                                    $(".totale_incassato_lente").html(parseFloat(incassato).toFixed(2));
                                                    $(".dati_tecnici_grande").show();
                                                    $(".scritta_totale").html("t: ");
                                                } else
                                                {



                                                    $(".dati_tecnici_grande").css("visibility", "hidden");

                                                    $(".scritta_totale").html("");
                                                    $(".totale_incassato_lente").html("");
                                                    $(".dati_tecnici_grande").show();
                                                }



                                                if (!isNaN(parseInt(rc[0].nump_xml)) && parseInt(rc[0].nump_xml) > 0) {
                                                    comanda.ultimi_coperti_importati = rc[0].nump_xml;

                                                    $("#numero_coperti").val(comanda.ultimi_coperti_importati);

                                                    if (comanda.ultimi_coperti_importati !== "" && comanda.ultimi_coperti_importati !== "0") {
                                                        $("#tasto_batti_coperti").hide();
                                                        $("#numero_coperti").show();
                                                    } else
                                                    {
                                                        $("#tasto_batti_coperti").show();
                                                        $("#numero_coperti").hide();
                                                    }



                                                }
                                            }
                                        });
                                        var query_sconto = "SELECT BIS,sconto_imp,sconto_perc FROM comanda WHERE desc_art LIKE 'RECORD TESTA%' and ntav_comanda='" + variabile_tavolo + "' AND stato_record='ATTIVO' ORDER BY sconto_imp DESC;";
                                        console.log("lettura_tavolo_xml", query_sconto);
                                        comanda.sincro.query(query_sconto, function (rc) {

                                            rc.forEach(function (e) {
                                                if (!isNaN(parseFloat(e.sconto_perc)) && parseFloat(e.sconto_perc.replace(',', '.')) > 0)
                                                {
                                                    setTimeout(function () {

                                                        var descrizione = 'Sconto';
                                                        if (typeof e.sconto_perc === 'string') {
                                                            descrizione = e.sconto_perc;
                                                        }
                                                        aggiungi_articolo(null, 'Sconto', '-' + parseFloat(e.sconto_perc.replace(',', '.')) + '%', 'BIS=' + e.BIS + '&descrizione=' + descrizione + '&prezzo_1=-' + parseFloat(e.sconto_perc.replace(',', '.')) + '%' + '&quantita=1', '1', 'SCONTO', null, null);
                                                        //aggiungi_articolo(null, 'SCONTO', '-5.00%', 'descrizione=ITALIANI&prezzo_1=-5.00%&quantita=1', '1', 'SCONTO',null,null);
                                                        //aggiungi_articolo(null, 'SCONTO', '-' + rc[0].sconto_perc + '%', 'descrizione=STAFF&prezzo_1=-' + rc[0].sconto_perc + '%&quantita=1', '1', 'SCONTO', null, null);
                                                    }, 600);
                                                } else if (!isNaN(parseFloat(e.sconto_imp)) && parseFloat(e.sconto_imp.replace(',', '.')) > 0)
                                                {
                                                    setTimeout(function () {

                                                        var descrizione = 'Sconto';
                                                        if (typeof e.sconto_perc === 'string') {
                                                            descrizione = e.sconto_perc;
                                                        }

                                                        aggiungi_articolo(null, 'Sconto', '-' + parseFloat(e.sconto_imp.replace(',', '.')), 'BIS=' + e.BIS + '&descrizione=' + descrizione + '&prezzo_1=-' + parseFloat(e.sconto_imp.replace(',', '.')) + '&quantita=1', '1', 'SCONTO', null, null);
                                                    }, 600);
                                                }
                                            });
                                        });
                                        comanda.funzionidb.conto_attivo();

                                        salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + aggZero(new Date().getMilliseconds(), 3) + " # FINE LETTURA TAVOLO # TAVOLO: " + tavolo + " # ");

                                        callback(true);
                                        clearTimeout(situazione_estrema);
                                        blocco_comanda = false;
                                    });
                                }
                            });
                        });
                    });
                }
            });
        }
    } else
    {
        try {
            $(document).off(comanda.eventino, evento_selettore_tavolo);
        } catch (e) {
            console.log(e);
        }
        $(document).on(comanda.eventino, 'div.tavolo.rett,div.tavolo.cerc', evento_selettore_tavolo);
    }
}

function colonna_valore(colonna, valore) {
    if (colonna !== undefined && valore !== undefined) {
        this[colonna] = valore;
    }
}