var lang = new Array();

var lng = function (lingua, callback) {

    //estrae le lingue in base alla colonna
    //la frase si richiama tramite id numerico
    comanda.lang = new Array();

    comanda.lingue = new Object();

    comanda.lingue["italiano"] = new Object();
    comanda.lingue["english"] = new Object();
    comanda.lingue["deutsch"] = new Object();

    var testo_query = "SELECT id,italiano,deutsch,english FROM lingue order by id DESC;";
    ////console.log(testo_query);

    var replaced = $('body').html();

    comanda.sincro.query(testo_query, function (result) {



        result.forEach(function (obj) {

            comanda.lingue["italiano"][obj.id] = obj["italiano"];
            comanda.lingue["english"][obj.id] = obj["english"];
            comanda.lingue["deutsch"][obj.id] = obj["deutsch"];


            lang[obj.id] = obj[lingua];

            comanda.lang[obj.id] = obj[lingua];

            var regex = new RegExp(">lang_" + obj.id + "<", "gi");

            replaced = replaced.replace(regex, '>' + obj[lingua].capitalize() + '<');

            var regex = new RegExp('placeholder="lang_' + obj.id + '"', 'gi');

            replaced = replaced.replace(regex, 'placeholder="' + obj[lingua].capitalize() + '"');

            var regex = new RegExp('onclick', 'gi');

            replaced = replaced.replace(regex, comanda.evento);

            console.log("LINGUA - ID: " + obj.id + " - " + obj[lingua]);

        });


        $('body').html(replaced);
        ////console.log("lng");

        if (typeof (callback) === 'function')
        {
            callback(true);
        }

    });

};

var lang_stampa = new Array();
var lngstm = function (lingua, callBack) {

    comanda.lang_stampa = new Array();

    var testo_query = "SELECT id," + lingua + " FROM lingue order by id DESC;";
    ////console.log(testo_query);


    //TASTI IN TEDESCO
    comanda.sincro.query(testo_query, function (result) {
        var replaced = $('body').html();
        ////console.log(replaced);
        result.forEach(function (obj) {

            comanda.lang_stampa[obj.id] = obj[lingua];

            var regex = new RegExp(">LANG_STAMPA_" + obj.id + "<", "ig");
            replaced = replaced.replace(regex, '>' + obj[lingua].toUpperCase() + '<');

        });

        $('body').html(replaced);

        if (comanda.fastconto !== true) {
            if (comanda.numero_licenza_cliente !== "0629"&&comanda.numero_licenza_cliente !== "0656") {
                listen_tastierino_num();
            }
        }

        if (comanda.prezzo_non_modificabile === true) {
            $('#popup_mod_art [name="prezzo_1"]').attr('readonly', 'readonly');
            $('#popup_mod_art [name="prezzo_1"]').removeClass('kb_num');
        }

        //console.log($('#totale_conto_separato_grande button'));
        $('.totale_conto_separato_grande_barra button').not('.totale_conto_separato_grande_barra button:first-child').on(comanda.eventino, function () {
            console.log("totale toggle");
            $('.totale_conto_separato_2_grande_barra').hide();

            $('.totale_conto_separato_grande_barra').slideToggle('fast');
        });

        $('.totale_conto_separato_2_grande_barra button').not('.totale_conto_separato_2_grande_barra button:first-child').on(comanda.eventino, function () {
            console.log("totale 2 toggle");
            $('.totale_conto_separato_grande_barra').hide();

            $('.totale_conto_separato_2_grande_barra').slideToggle('fast');
        });

        //FINITO IL CARICAMENTO
        comanda.loading = false;

        callBack(true);

    });
};


var corrispondenze_portate = function (lingua) {

    comanda.nome_portata = new Array();

    var testo_query = "SELECT numero," + lingua + " FROM nomi_portate order by numero ASC;";

    comanda.sincro.query(testo_query, function (result) {

        result.forEach(function (obj) {

            if (obj[lingua] !== null) {

                comanda.nome_portata[obj.numero] = obj[lingua].replace(/\//gi, '-');
            } else
            {
                comanda.nome_portata[obj.numero] = obj.id;
            }

        });

    });

};

var numero_layout = "";
var corrispondenze_stampanti = function (lingua) {

    comanda.nome_stampante = new Array();

    var testo_query = "SELECT numero," + lingua + ",ip,ip_alternativo,matricola,dimensione,nome,intelligent,fiscale,devid_nf,devicetype,devicemodel FROM nomi_stampanti order by numero ASC;";

    let result = alasql(testo_query);

    //nmis è il numero identificativo del misuratore

    if (leggi_get_url('nmis') === '2' || leggi_get_url('nmis') === '3' || leggi_get_url('nmis') === '4') {
        numero_layout = "_" + leggi_get_url('nmis');
    }

    result.forEach(function (obj) {

        comanda.nome_stampante[obj[lingua]] = new Array();

        comanda.nome_stampante[obj[lingua]].nome = obj['nome'];
        comanda.nome_stampante[obj[lingua]].ip = obj['ip'];
        comanda.nome_stampante[obj[lingua]].ip_alternativo = obj['ip_alternativo'];
        comanda.nome_stampante[obj[lingua]].intelligent = obj['intelligent'];
        comanda.nome_stampante[obj[lingua]].devid_nf = obj['devid_nf'];
        comanda.nome_stampante[obj[lingua]].matricola = obj['matricola'];

        comanda.nome_stampante[obj.numero] = new Array();
        comanda.nome_stampante[obj.numero].nome = obj['nome'].toUpperCase();
        comanda.nome_stampante[obj.numero].nome_umano = obj[lingua];
        comanda.nome_stampante[obj.numero].ip = obj['ip'];
        comanda.nome_stampante[obj.numero].ip_alternativo = obj['ip_alternativo'];
        comanda.nome_stampante[obj.numero].dimensione = obj['dimensione'];
        comanda.nome_stampante[obj.numero].intelligent = obj['intelligent'];
        comanda.nome_stampante[obj.numero].fiscale = obj['fiscale'];
        comanda.nome_stampante[obj.numero].devid_nf = obj['devid_nf'];
        comanda.nome_stampante[obj.numero].devicetype = obj['devicetype'];
        comanda.nome_stampante[obj.numero].devicemodel = obj['devicemodel'];
        comanda.nome_stampante[obj.numero].matricola = obj['matricola'];

        //AGGIUNGE LE CASSE ITALIANE E/O TEDESCHE
        switch (comanda.nome_stampante[obj.numero].nome_umano) {
            case "QUITTUNG":
                comanda.indirizzo_non_fiscale_quittung = comanda.nome_stampante[obj.numero].ip;
                if (comanda.nome_stampante[obj.numero].dimensione === 'p')
                {
                    comanda.stampante_piccola_quittung = "S";
                    comanda.carattere_stampa_piccola = String.fromCharCode(18);
                } else
                {
                    comanda.stampante_piccola_quittung = "N";
                }
                break;
            case "RECHNUNG":
                comanda.indirizzo_non_fiscale_rechnung = comanda.nome_stampante[obj.numero].ip;
                if (comanda.nome_stampante[obj.numero].dimensione === 'p')
                {
                    comanda.stampante_piccola_rechnung = "S";
                    comanda.carattere_stampa_piccola = String.fromCharCode(18);
                } else
                {
                    comanda.stampante_piccola_rechnung = "N";
                }
                break;

            case "SCONTRINO" + numero_layout:
            case "FATTURA" + numero_layout:
                comanda.indirizzo_cassa = comanda.nome_stampante[obj.numero].ip;

                comanda.istr_cass = new Object();

                if (comanda.nome_stampante[obj.numero].intelligent.toLowerCase() === "n") {
                    comanda.cassa_intellinet = "N";
                    comanda.matricola_misuratore = "";
                    comanda.istr_cass["department"] = "Dep";
                    comanda.istr_cass["justification"] = "Just";
                    comanda.istr_cass["description"] = "Text";
                    comanda.istr_cass["message"] = "Text";
                    comanda.istr_cass["messageType"] = "Type";
                    comanda.istr_cass["operator"] = "Ope";
                    comanda.istr_cass["quantity"] = "Qty";
                    comanda.istr_cass["unitPrice"] = "UnitCost";
                    comanda.istr_cass["option"] = "Type";
                    comanda.istr_cass["paymentType"] = "Type";
                    comanda.istr_cass["adjustmentType"] = "Type";
                    comanda.istr_cass["payment"] = "amount";
                    comanda.istr_cass["documentAmount"] = "Amount";
                    comanda.istr_cass["endFiscalReceipt"] = "closeFiscalReceipt";
                } else
                {
                    comanda.cassa_intellinet = "S";
                    comanda.matricola_misuratore = comanda.nome_stampante[obj.numero].matricola;
                    comanda.istr_cass["department"] = "department";
                    comanda.istr_cass["justification"] = "justification";
                    comanda.istr_cass["description"] = "description";
                    comanda.istr_cass["message"] = "message";
                    comanda.istr_cass["messageType"] = "messageType";
                    comanda.istr_cass["operator"] = "operator";
                    comanda.istr_cass["quantity"] = "quantity";
                    comanda.istr_cass["unitPrice"] = "unitPrice";
                    comanda.istr_cass["option"] = "option";
                    comanda.istr_cass["paymentType"] = "paymentType";
                    comanda.istr_cass["adjustmentType"] = "adjustmentType";
                    comanda.istr_cass["payment"] = "payment";
                    comanda.istr_cass["documentAmount"] = "documentAmount";
                    comanda.istr_cass["endFiscalReceipt"] = "endFiscalReceipt";
                }

                if (comanda.nome_stampante[obj.numero].dimensione === 'p')
                {
                    comanda.stampante_piccola = "S";
                    comanda.carattere_stampa_piccola = String.fromCharCode(18);
                } else
                {
                    comanda.stampante_piccola = "N";
                }

                break;

            case "KONTO" + numero_layout:
            case "CONTO" + numero_layout:
                if (comanda.nome_stampante[obj.numero].dimensione === 'p')
                {
                    comanda.stampante_piccola_conto = "S";
                    comanda.carattere_stampa_piccola = String.fromCharCode(18);
                } else
                {
                    comanda.stampante_piccola_conto = "N";
                }
                comanda.intelligent_non_fiscale = comanda.nome_stampante[obj.numero].ip;
                break;
        }

        //AGGIUNGE LE STAMPANTI NON INTELLIGENT AI SETTAGGI DELLA STAMPANTE ROOT
        if (comanda.nome_stampante[obj.numero].devid_nf !== undefined && comanda.nome_stampante[obj.numero].devid_nf !== null && comanda.nome_stampante[obj.numero].devid_nf !== '')
        {
            aggiungi_dispositivo(comanda.nome_stampante[obj.numero].devid_nf, comanda.nome_stampante[obj.numero].devicetype, comanda.nome_stampante[obj.numero].devicemodel, comanda.nome_stampante[obj.numero].ip);

        }

        console.log("COMANDA NOME STAMPANTE", comanda.nome_stampante, obj.numero);

    });




    /*if (comanda.compatibile_xml !== true) {
     try {
     
     
     ATECO_model = new ATECO_model();
     ATECO_view = new ATECO_view();
     ATECO_controller = new ATECO_controller();
     
     IVA_model = new IVA_model();
     IVA_view = new IVA_view();
     IVA_controller = new IVA_controller();
     
     REPARTO_model = new REPARTO_model();
     REPARTO_view = new REPARTO_view();
     REPARTO_controller = new REPARTO_controller();
     
     
     
     } catch (e) {
     }
     }*/



};