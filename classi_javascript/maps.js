/* global google */

// Note: This example requires that you consent to location sharing when
// prompted by your browser. If you see the error "The Geolocation service
// failed.", it means you probably did not give permission for the browser to
// locate you.
var map, infoWindow;
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -34.397, lng: 150.644},
        zoom: 14
    });
 
    var geocoder = new google.maps.Geocoder();

    infoWindow = new google.maps.InfoWindow;

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            var luogo_partenza = comanda.profilo_aziendale;

            geocodeAddress(luogo_partenza.indirizzo + ", " + luogo_partenza.numero + "," + luogo_partenza.citta + " " + luogo_partenza.provincia, "P", geocoder, map, true, true);

            var indirizzo = $('#popup_scelta_cliente input[name="indirizzo"]:visible').val();
            var numero = $('#popup_scelta_cliente input[name="numero"]:visible').val();
            var cap = $('#popup_scelta_cliente input[name="cap"]:visible').val();
            var citta = $('#popup_scelta_cliente input[name="comune"]:visible').val();
            var provincia = $('#popup_scelta_cliente input[name="provincia"]:visible').val();

            if (indirizzo && numero && citta) {
                geocodeAddress(indirizzo + ", " + numero + " , " + cap + " " + citta + " " + provincia, "", geocoder, map, true, false, "Scegliere Giro", "yellow");
                let directionsService = new google.maps.DirectionsService();
                let directionsRenderer = new google.maps.DirectionsRenderer();
                directionsRenderer.setMap(map); // Existing map object displays directions
                // Create route from existing points used for markers
              
             
                              var luogo_partenza = comanda.profilo_aziendale;
                              var origin_x =luogo_partenza.indirizzo+','+luogo_partenza.comune+','+luogo_partenza.citta;
                              var destination_x = indirizzo+','+citta+','+numero;
                const route = {
                    
                    origin: origin_x,
                    destination:destination_x,
                    travelMode: 'DRIVING'
                }
              
                directionsService.route(route,
                  function(response, status) { // anonymous function to capture directions
                    if (status !== 'OK') {
                      window.alert('Directions request failed due to ' + status);
                      return;
                    } else {
                      directionsRenderer.setDirections(response); // Add route to the map
                      var directionsData = response.routes[0].legs[0]; // Get data about the mapped route
                      if (!directionsData) {
                        window.alert('Directions request failed');
                        return;
                      }
                      else {
                          if(directionsData.distance.text)
                          {
                              document.getElementById('msg').innerHTML='';
                          }
                        document.getElementById('msg').innerHTML += " distanza del percorso di guida " + directionsData.distance.text + " (" + directionsData.duration.text + ").";
                      }
                    }
                  });
            }


            var d = new Date();
            var Y = d.getFullYear();
            var M = addZero((d.getMonth() + 1), 2);
            var D = addZero(d.getDate(), 2);
            var h = addZero(d.getHours(), 2);
            var m = addZero(d.getMinutes(), 2);
            var s = addZero(d.getSeconds(), 2);
            var ms = addZero(d.getMilliseconds(), 3);
            var data_oggi = Y + '' + M + '' + D;

            /*var query_forno = "select comanda.id,comanda.desc_art,comanda.portata,clienti.comune,clienti.indirizzo,clienti.numero,comanda.tipo_consegna,comanda.ora_consegna,comanda.nome_comanda,sum(comanda.quantita) as quantita from comanda,clienti where substr(comanda.id,5,8)='" + data_oggi + "' and clienti.id=comanda.rag_soc_cliente and (stato_record='FORNO' or stato_record='ATTIVO') and comanda.tipo_consegna!='NIENTE' and comanda.tipo_consegna!='' and portata='P' and (substr(desc_art,1,1)!='+' and substr(desc_art,1,1)!='-' and substr(desc_art,1,1)!='=' and substr(desc_art,1,1)!='*' and substr(desc_art,1,1)!='x' and substr(desc_art,1,1)!='(') group by comanda.id,comanda.nome_comanda,comanda.ora_consegna\n\
             union all\n\
             select id,comanda.desc_art,comanda.portata,'','','',tipo_consegna,ora_consegna,rag_soc_cliente,sum(quantita) as quantita from comanda where substr(comanda.id,5,8)='" + data_oggi + "' and SUBSTR(rag_soc_cliente,1,1)='P' and (stato_record='FORNO' or stato_record='ATTIVO') and comanda.tipo_consegna!='NIENTE' and comanda.tipo_consegna!='' and portata='P' and (substr(desc_art,1,1)!='+' and substr(desc_art,1,1)!='-' and substr(desc_art,1,1)!='=' and substr(desc_art,1,1)!='*' and substr(desc_art,1,1)!='x' and substr(desc_art,1,1)!='(') group by comanda.id,comanda.nome_comanda,comanda.ora_consegna order by comanda.ora_consegna asc;";*/

            /*select id,comanda.desc_art,comanda.portata,'','','',tipo_consegna,ora_consegna,rag_soc_cliente,sum(quantita) as quantita from comanda where substr(comanda.id,5,8)='" + data_oggi + "' and SUBSTR(rag_soc_cliente,1,1)='P' and (stato_record='FORNO' or stato_record='ATTIVO') and comanda.tipo_consegna!='NIENTE' and comanda.tipo_consegna!='' and portata='P' and (substr(desc_art,1,1)!='+' and substr(desc_art,1,1)!='-' and substr(desc_art,1,1)!='=' and substr(desc_art,1,1)!='*' and substr(desc_art,1,1)!='x' and substr(desc_art,1,1)!='(') group by comanda.id,comanda.nome_comanda,comanda.ora_consegna order by comanda.ora_consegna asc*/

            /*select comanda.id,comanda.desc_art,comanda.portata,clienti.comune,clienti.indirizzo,clienti.numero,comanda.tipo_consegna,comanda.ora_consegna,comanda.nome_comanda,sum(comanda.quantita) as quantita from comanda,clienti where substr(comanda.id,5,8)='" + data_oggi + "' and clienti.id=comanda.rag_soc_cliente and (stato_record='FORNO' or stato_record='ATTIVO') and comanda.tipo_consegna!='NIENTE' and comanda.tipo_consegna!='' and portata='P' and (substr(desc_art,1,1)!='+' and substr(desc_art,1,1)!='-' and substr(desc_art,1,1)!='=' and substr(desc_art,1,1)!='*' and substr(desc_art,1,1)!='x' and substr(desc_art,1,1)!='(') group by comanda.id,comanda.nome_comanda,comanda.ora_consegna*/

            /*and clienti.id=comanda.rag_soc_cliente 
             order by ora_consegna asc*/

            var dati_clienti = new Array();

            /*group by id,nome_comanda,ora_consegna */

            var n = "select id,desc_art,portata,tipo_consegna,ora_consegna,rag_soc_cliente,quantita,nome_comanda from comanda where substr(id,5,8)='" + data_oggi + "' and SUBSTR(rag_soc_cliente,1,1)='P' and (stato_record='FORNO' or stato_record='ATTIVO') and tipo_consegna!='NIENTE' and tipo_consegna!='' and portata='P' and (substr(desc_art,1,1)!='+' and substr(desc_art,1,1)!='-' and substr(desc_art,1,1)!='=' and substr(desc_art,1,1)!='*' and substr(desc_art,1,1)!='x' and substr(desc_art,1,1)!='(') ;";
            var rn = alasql(n);

            rn.forEach(function (v) {

                dati_clienti.push({id: v.id, desc_art: v.desc_art, portata: v.portata, comune: "", indirizzo: "", numero: "", tipo_consegna: v.tipo_consegna, ora_consegna: v.ora_consegna, rag_soc_cliente: v.rag_soc_cliente, quantita: v.quantita, nome_comanda: v.nome_comanda});

            });


            var c1 = "select id,desc_art,portata,tipo_consegna,ora_consegna,rag_soc_cliente,quantita,nome_comanda from comanda where substr(id,5,8)='" + data_oggi + "' and SUBSTR(rag_soc_cliente,1,1)!='P' and (stato_record='FORNO' or stato_record='ATTIVO') and tipo_consegna!='NIENTE' and tipo_consegna!='' and portata='P' and (substr(desc_art,1,1)!='+' and substr(desc_art,1,1)!='-' and substr(desc_art,1,1)!='=' and substr(desc_art,1,1)!='*' and substr(desc_art,1,1)!='x' and substr(desc_art,1,1)!='(') ;";

            var rc1 = alasql(c1);

            rc1.forEach(function (v) {

                var c2 = "select comune,indirizzo,numero from clienti where id=" + v.rag_soc_cliente + " limit 1;";

                var rc2 = alasql(c2);
                
                

                dati_clienti.push({id: v.id, desc_art: v.desc_art, portata: v.portata, comune: rc2[0].comune, indirizzo: rc2[0].indirizzo, numero: rc2[0].numero, tipo_consegna: v.tipo_consegna, ora_consegna: v.ora_consegna, rag_soc_cliente: v.rag_soc_cliente, quantita: v.quantita, nome_comanda: v.nome_comanda});
               
            });



            /*order by ora_consegna asc*/

            /*group by id,nome_comanda,ora_consegna */

            var raggruppato = new Object();

            dati_clienti.forEach(function (v) {

                if (raggruppato[v.id] === undefined) {
                    raggruppato[v.id] = new Object();
                }

                if (raggruppato[v.id][v.nome_comanda] === undefined) {
                    raggruppato[v.id][v.nome_comanda] = new Object();
                }

                if (raggruppato[v.id][v.nome_comanda][v.ora_consegna] === undefined) {
                    raggruppato[v.id][v.nome_comanda][v.ora_consegna] = new Object();
                    raggruppato[v.id][v.nome_comanda][v.ora_consegna] = v;
                    raggruppato[v.id][v.nome_comanda][v.ora_consegna].quantita=parseInt(v.quantita);
                } else
                {
                    raggruppato[v.id][v.nome_comanda][v.ora_consegna].quantita += parseInt(v.quantita);
                }
            });

            var raggruppato_finale = new Array();

            for (var id in raggruppato) {
                for (var nome_comanda in raggruppato[id]) {
                    for (var ora_consegna in raggruppato[id][nome_comanda]) {
                        raggruppato_finale.push(raggruppato[id][nome_comanda][ora_consegna]);
                    }
                }
            }

            var raggruppato_finale_ordinato = raggruppato_finale.sort(function (a, b) {
                return parseInt(a.ora_consegna.substr(0,2)+''+a.ora_consegna.substr(3,2)) - parseInt(b.ora_consegna.substr(0,2)+''+b.ora_consegna.substr(3,2));
            });

            var i = 0;
            var ultima_ora_consegna = "xx:xx";

            var numero_consegne_per_orario = new Object();

            raggruppato_finale_ordinato.forEach(function (obj) {

                if (obj['tipo_consegna'] === "DOMICILIO") {
                    if (numero_consegne_per_orario[obj["ora_consegna"]] === undefined) {
                        numero_consegne_per_orario[obj["ora_consegna"]] =  parseInt(obj.quantita);
                    } else
                    {
                        numero_consegne_per_orario[obj["ora_consegna"]] += parseInt(obj.quantita);
                    }
                }
            });

            raggruppato_finale_ordinato.forEach(function (obj) {

                if (obj['tipo_consegna'] === "DOMICILIO") {

                    if (ultima_ora_consegna !== obj['ora_consegna']) {
                        ultima_ora_consegna = obj['ora_consegna'];
                        i++;
                    }

                    geocodeAddress(obj["indirizzo"] + ", " + obj["numero"] + ", " + obj["comune"] + " " + " VE", i.toString(), geocoder, map, false, false, "Consegna: <strong>" + obj['ora_consegna'] + "</strong><br/>Qta Giro:<strong>" + numero_consegne_per_orario[obj["ora_consegna"]] + "</strong>");

                }
            });



        }, function () {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        handleLocationError(false, infoWindow, map.getCenter());
    }
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
            'Error: The Geolocation service failed.' :
            'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
}


function geocodeAddress(address, label, geocoder, resultsMap, center, home, infoConsegna, colore) {
    geocoder.geocode({'address': address}, function (results, status) {
        if (status === 'OK') {


            if (home === true) {
                var marker = new mapIcons.Marker({
                    map: resultsMap,
                    position: results[0].geometry.location,
                    icon: {
                        path: mapIcons.shapes.SQUARE_PIN,
                        fillColor: 'red',
                        fillOpacity: 1,
                        strokeColor: '',
                        strokeWeight: 0
                    },
                    map_icon_label: '<span class="map-icon map-icon-restaurant"></span>'
                });
            } else
            {
                var contentString = '<div class="contenuto_info">' +
                        '<h2 class="firstHeading" class="firstHeading">Info</h2>' +
                        '<div id="bodyContent">' +
                        '<h3>' + infoConsegna + '</h3>' +
                        '</div>' +
                        '</div>';

                var infowindow = new google.maps.InfoWindow({
                    content: contentString
                });




                var marker = new google.maps.Marker({
                    map: resultsMap,
                    position: results[0].geometry.location,
                    label: label
                });

                if (colore !== undefined) {
                    marker.setIcon('http://maps.google.com/mapfiles/ms/icons/' + colore + '-dot.png');
                }

                marker.addListener('click', function () {
                    infowindow.open(map, marker);
                });

            }
            if (center === true) {
                resultsMap.setCenter(results[0].geometry.location);
            }
        } else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });
   
}