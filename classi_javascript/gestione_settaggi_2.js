function contr_mancanza_prog_fisc_scontr(data, tavolo) {
    //ESEMPIO:
    //20170408

    comanda.sincro.query_incassi("select numero_fiscale from record_teste where tipologia='SCONTRINO' and progressivo_comanda LIKE '%" + data + "%' and tavolo='" + tavolo + "';", 10000, function (t) {

        var i = 1;
        t.forEach(function (v) {
            if (i != v.numero_fiscale) {
                console.log("PROGRESSIVO MANCANTE TESTA", i);
                return false;
            }
            i++;
        });

    });

}

function changeFunc(event) {
    var selectBox = document.getElementById("selezione_profilo_settaggi_tendina");
    comanda.folder_number = selectBox.options[selectBox.selectedIndex].getAttribute('id_profilo');
    comanda.nome_servizio = selectBox.options[selectBox.selectedIndex].getAttribute('nome_profilo');
    comanda.servizio = comanda.nome_servizio;
    gestisci_settaggio(event, comanda.settaggio_selezionato);
}


function controllo_differenze_teste_corpi(data) {

    var query_corpi = "select id,sum((QTA+prezzo_un)*quantita)/100*(100-sconto_perc) as totale_corpo from comanda where  (posizione='CONTO' OR posizione='COPERTO') and fiscalizzata_sn='S' and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and tipo_ricevuta!='' and substr(id,5,8) ='" + data + "'  group by id;";

    var query_teste = "select progressivo_comanda,sum(contanti)-sum(resto)+sum(droppay)+sum(bancomat)+sum(carte_credito)+sum(buoni_pasto*numero_buoni_pasto)+sum(buoni_pasto_2*numero_buoni_pasto_2)+sum(buoni_pasto_3*numero_buoni_pasto_3)+sum(buoni_pasto_4*numero_buoni_pasto_4)+sum(buoni_pasto_5*numero_buoni_pasto_5)+sum(buoni_acquisto)-sum(buoni_acquisto_emessi) as totale_testa from record_teste where substr(progressivo_comanda,5,8)= '" + data + "' group by progressivo_comanda;";

    comanda.sincro.query_incassi(query_corpi, 10000, function (r_corpi) {
        comanda.sincro.query_incassi(query_teste, 10000, function (r_teste) {

            var oggetto_confronto = new Object();

            oggetto_confronto["CORPO"] = new Array();
            oggetto_confronto["TESTA"] = new Array();

            r_corpi.forEach(function (el) {
                oggetto_confronto["CORPO"].push([el.id, el.totale_corpo]);
            });
            r_teste.forEach(function (el) {
                oggetto_confronto["TESTA"].push([el.progressivo_comanda, el.totale_testa]);
            });

            oggetto_confronto["CORPO"].forEach(function (el, index) {

                if (el[0] === oggetto_confronto["TESTA"][index][0]) {
                    if (parseFloat(el[1]).toFixed(2) !== parseFloat(oggetto_confronto["TESTA"][index][1]).toFixed(2)) {

                        console.log("ALERT CONFRONTO TOTALI", oggetto_confronto["CORPO"][index][0], oggetto_confronto["TESTA"][index][0], el[1], oggetto_confronto["TESTA"][index][1]);

                    }
                }

            });

            try {
                //FARE CONFRONTO INVERSO PARTENDO DALLE TESTE PER VEDERE ID IN MENO
                oggetto_confronto["TESTA"].forEach(function (el, index) {

                    if (oggetto_confronto["CORPO"][index] === undefined || oggetto_confronto["CORPO"][index][0] === undefined) {
                        throw el[0];
                    }

                    if (el[0] === oggetto_confronto["CORPO"][index][0]) {
                        if (parseFloat(el[1]).toFixed(2) !== parseFloat(oggetto_confronto["CORPO"][index][1]).toFixed(2)) {

                            console.log("ALERT CONFRONTO TOTALI", oggetto_confronto["CORPO"][index][0], oggetto_confronto["TESTA"][index][0], el[1], oggetto_confronto["TESTA"][index][1]);
                        }
                    }

                });
            } catch (e) {
                console.log("ERRORE ID", e);
            }

            console.log("CONFRONTO TOTALI", oggetto_confronto);
        });
    });

}

function controllo_differenze_teste_corpi_sqlite(data) {

    let avviso = "DIFFERENZA SQLITE";

    var query_corpi = "select id,sum((QTA+prezzo_un)*quantita)/100*(100-sconto_perc) as totale_corpo from comanda where  (posizione='CONTO' OR posizione='COPERTO') and fiscalizzata_sn='S' and (tipo_ricevuta='scontrino' or tipo_ricevuta='FATTURA' or tipo_ricevuta='SCONTRINO' or stato_record='RAPPORTOZ') and tipo_ricevuta!='' and substr(id,5,8) ='" + data + "'  group by id;";

    var query_teste = "select progressivo_comanda,sum(contanti)-sum(resto)+sum(droppay)+sum(bancomat)+sum(carte_credito)+sum(buoni_pasto*numero_buoni_pasto)+sum(buoni_pasto_2*numero_buoni_pasto_2)+sum(buoni_pasto_3*numero_buoni_pasto_3)+sum(buoni_pasto_4*numero_buoni_pasto_4)+sum(buoni_pasto_5*numero_buoni_pasto_5)+sum(buoni_acquisto)-sum(buoni_acquisto_emessi) as totale_testa from record_teste where substr(progressivo_comanda,5,8)= '" + data + "' group by progressivo_comanda;";


    var r_corpi = new Object();
    var r_teste = new Object();

    comanda.sincro.query_cassa(query_corpi, 10000, function (ris_corpi) {

        r_corpi = jQuery.extend(true, {}, ris_corpi);

        comanda.sincro.query_cassa(query_teste, 10000, function (ris_teste) {

            r_teste = jQuery.extend(true, {}, ris_teste);

            var oggetto_confronto = new Object();

            oggetto_confronto["CORPO"] = new Array();
            oggetto_confronto["TESTA"] = new Array();

            Object.values(r_corpi).forEach(function (el) {
                oggetto_confronto["CORPO"].push([el.id, el.totale_corpo]);
            });
            Object.values(r_teste).forEach(function (el) {
                oggetto_confronto["TESTA"].push([el.progressivo_comanda, el.totale_testa]);
            });

            oggetto_confronto["CORPO"].forEach(function (el, index) {

                if (el[0] === oggetto_confronto["TESTA"][index][0]) {
                    if (parseFloat(el[1]).toFixed(2) !== parseFloat(oggetto_confronto["TESTA"][index][1]).toFixed(2)) {

                        avviso += "<br>C " + oggetto_confronto["CORPO"][index][0] + " " + el[1] + " " + oggetto_confronto["TESTA"][index][1];

                    }
                }

            });

            try {
                //FARE CONFRONTO INVERSO PARTENDO DALLE TESTE PER VEDERE ID IN MENO
                oggetto_confronto["TESTA"].forEach(function (el, index) {

                    if (oggetto_confronto["CORPO"][index] === undefined || oggetto_confronto["CORPO"][index][0] === undefined) {
                        throw el[0];
                    }

                    if (el[0] === oggetto_confronto["CORPO"][index][0]) {
                        if (parseFloat(el[1]).toFixed(2) !== parseFloat(oggetto_confronto["CORPO"][index][1]).toFixed(2)) {

                            avviso += "<br>T " + oggetto_confronto["CORPO"][index][0] + " " + el[1] + " " + oggetto_confronto["TESTA"][index][1];
                        }
                    }

                });
            } catch (e) {
                console.log("ERRORE ID", e);
            }

            if (avviso !== "DIFFERENZA SQLITE") {
                bootbox.alert(avviso);
            }

            console.log("CONFRONTO TOTALI", oggetto_confronto);
        });
    });

}


function array_differenze_teste_corpi(query_data, callBack) {


    var query_corpi = "select id,sum((QTA+prezzo_un)*quantita)/100*(100-sconto_perc) as totale_corpo from comanda where  (posizione='CONTO' OR posizione='COPERTO') and fiscalizzata_sn='S' and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and tipo_ricevuta!='' and " + query_data + "  group by id;";

    var query_teste = "select progressivo_comanda,sum(contanti)-sum(resto)+sum(droppay)+sum(bancomat)+sum(carte_credito)+sum(buoni_pasto*numero_buoni_pasto)+sum(buoni_pasto_2*numero_buoni_pasto_2)+sum(buoni_pasto_3*numero_buoni_pasto_3)+sum(buoni_pasto_4*numero_buoni_pasto_4)+sum(buoni_pasto_5*numero_buoni_pasto_5)+sum(buoni_acquisto)-sum(buoni_acquisto_emessi) as totale_testa from record_teste where " + query_data + " group by progressivo_comanda;";

    comanda.sincro.query_incassi(query_corpi, 10000, function (r_corpi) {
        comanda.sincro.query_incassi(query_teste, 10000, function (r_teste) {

            var oggetto_confronto = new Object();
            var array_differenze = new Array();
            oggetto_confronto["CORPO"] = new Array();
            oggetto_confronto["TESTA"] = new Array();
            r_corpi.forEach(function (el) {
                oggetto_confronto["CORPO"].push([el.id, el.totale_corpo]);
            });
            r_teste.forEach(function (el) {
                oggetto_confronto["TESTA"].push([el.progressivo_comanda, el.totale_testa]);
            });
            oggetto_confronto["CORPO"].forEach(function (el, index) {

                if (oggetto_confronto["TESTA"][index] !== undefined) {
                    if (el[0] === oggetto_confronto["TESTA"][index][0]) {
                        if (parseFloat(el[1]).toFixed(2) !== parseFloat(oggetto_confronto["TESTA"][index][1]).toFixed(2)) {

                            array_differenze.push([oggetto_confronto["CORPO"][index][0], oggetto_confronto["TESTA"][index][0], el[1], oggetto_confronto["TESTA"][index][1], "C"]);
                            console.log("ALERT CONFRONTO TOTALI", oggetto_confronto["CORPO"][index][0], oggetto_confronto["TESTA"][index][0], el[1], oggetto_confronto["TESTA"][index][1]);
                        }
                    }

                } else {
                    //bootbox.alert("Indice di testa " + index + " non trovato.");
                }
            });
            try {
                //FARE CONFRONTO INVERSO PARTENDO DALLE TESTE PER VEDERE ID IN MENO
                oggetto_confronto["TESTA"].forEach(function (el, index) {

                    if (oggetto_confronto["CORPO"][index] === undefined || oggetto_confronto["CORPO"][index][0] === undefined) {
                        throw el[0];
                    }

                    if (el[0] === oggetto_confronto["CORPO"][index][0]) {
                        if (parseFloat(el[1]).toFixed(2) !== parseFloat(oggetto_confronto["CORPO"][index][1]).toFixed(2)) {

                            array_differenze.push([oggetto_confronto["CORPO"][index][0], oggetto_confronto["TESTA"][index][0], oggetto_confronto["CORPO"][index][1], el[1], "T"]);
                            console.log("ALERT CONFRONTO TOTALI", oggetto_confronto["CORPO"][index][0], oggetto_confronto["TESTA"][index][0], el[1], oggetto_confronto["TESTA"][index][1]);
                        }
                    }

                });
            } catch (e) {
                console.log("ERRORE ID", e);
            }

            callBack(array_differenze);
            console.log("CONFRONTO TOTALI", oggetto_confronto);
        });
    });
}



function visualizza_fattura_buoni_pasto(id) {

    comanda.id_buono = id;
    if (event !== undefined && event.target !== undefined && event.target !== null) {
        $(event.target).parent().find("a").removeClass('cat_accesa');
        $(event.target).addClass("cat_accesa");
    }

    var data = comanda.funzionidb.data_attuale(true).substr(0, 10).replace(/-/gi, "/");
    var form_gestione_circuiti = '';
    comanda.sincro.query("SELECT * FROM circuiti WHERE id=" + id + " ORDER BY ordinamento asc,ragione_sociale ASC LIMIT 1;", function (result) {
        comanda.sincro.query("SELECT * FROM profilo_aziendale;", function (emit) {
            comanda.sincro.query_cassa("select sum(quantita) as quantita,bp*sum(quantita) as totale_buoni,bp  from ( \n\
                                        SELECT sum(numero_buoni_pasto) as quantita,buoni_pasto as bp,* FROM record_teste WHERE cast(buoni_pasto as float)>0 and nome_buono='" + id + "' GROUP BY buoni_pasto \n\
                                        union all \n\
                                        SELECT sum(numero_buoni_pasto_2) as quantita,buoni_pasto_2 as bp,* FROM record_teste WHERE cast(buoni_pasto_2 as float)>0 and nome_buono_2='" + id + "' GROUP BY buoni_pasto_2 \n\
                                        union all \n\
                                        SELECT sum(numero_buoni_pasto_3) as quantita,buoni_pasto_3 as bp,* FROM record_teste WHERE cast(buoni_pasto_3 as float)>0 and nome_buono_3='" + id + "'GROUP BY buoni_pasto_3 \n\
                                        union all \n\
                                        SELECT sum(numero_buoni_pasto_4) as quantita,buoni_pasto_4 as bp,* FROM record_teste WHERE cast(buoni_pasto_4 as float)>0 and nome_buono_4='" + id + "' GROUP BY buoni_pasto_4 \n\
                                        union all \n\
                                        SELECT sum(numero_buoni_pasto_5) as quantita,buoni_pasto_5 as bp,* FROM record_teste WHERE cast(buoni_pasto_5 as float)>0 and nome_buono_5='" + id + "' GROUP BY buoni_pasto_5 \n\
                                        ORDER BY id DESC) group by bp;", 30000, function (movimenti) {


                console.log("MOVIMENTI BUONI PASTO", movimenti);
                var righe = '';
                var numero_buoni = 0;
                var totale_buoni = 0.00;
                for (var key in movimenti) {
                    var mov = movimenti[key];
                    righe += '<tr class="riga"><td class="quantita">' + mov.quantita + '</td><td class="valore">' + parseFloat(mov.bp).toFixed(2) + '</td><td  class="totale">' + parseFloat(parseFloat(mov.bp) * parseInt(mov.quantita)).toFixed(2) + '</td></tr>';
                    numero_buoni += parseInt(mov.quantita);
                    totale_buoni += parseFloat(mov.bp) * parseInt(mov.quantita);
                }

                var sconto_percentuale = 0;
                var sconto_imponibile = 0;

                if (result[0].adeguamento_perc === undefined || result[0].adeguamento_perc === null || typeof result[0].adeguamento_perc.trim() !== "string" || result[0].adeguamento_perc.trim() === "") {
                    bootbox.alert("IVA del circuito mancante. Definirla sui settaggi.");
                    return false;
                }


                sconto_percentuale = parseFloat(result[0].adeguamento_perc);
                sconto_imponibile = totale_buoni / 100 * sconto_percentuale;

                totale_buoni -= sconto_imponibile;

                righe += '<tr class="riga_adeguamento"><td>Adeguamento Percentuale</td><td class="percentuale_adeguamento">' + sconto_percentuale + '%</td><td  class="importo_adeguamento">' + sconto_imponibile.toFixed(2) + '</td></tr>';


                for (var key in result[0]) {
                    if (result[0][key] === null) {
                        result[0][key] = '';
                    }
                }
                var randomId = new Date().getTime();
                form_gestione_circuiti = '<div class="lista_movimenti_buoni">\n\
            <div class="col-md-4 col-xs-4" style="overflow:auto;">\n\
                <img style="height:200px;width:auto;background-color:white;" class="logo" src="./uploads/logo?random=' + randomId + '" />\n\
                 \n\
            </div>\n\
             <div class="col-md-8 col-xs-8" style="overflow:auto;">\n\
                <table class="table table-responsive table-hover">\n\
                <tr style="background-color:rgb(61, 61, 61)"><th>EMITTENTE</th></tr>\n\
                <tr><td style="font-weight:bold;" class="emit_ragione_sociale">' + emit[0].ragione_sociale + '</td></tr>\n\
                <tr><td class="emit_indirizzo">' + emit[0].indirizzo + ' ' + emit[0].numero + ' ' + emit[0].cap + ' - ' + emit[0].citta + ' (' + emit[0].provincia + ') ' + emit[0].paese + '</td></tr>\n\
                <tr><td class="emit_contatti">Tel: ' + emit[0].telefono + ' - Email: ' + emit[0].email + '</td></tr>\n\
                <tr><td class="emit_info_fiscali">C.F: ' + emit[0].codice_fiscale + ' - P.I: ' + emit[0].partita_iva + '</td></tr>\n\
                </table>\n\
            </div>\n\
            <div class="col-md-8 col-xs-8 pull-right text-right" style="overflow:auto;">\n\
                <table class="table table-responsive table-hover">\n\
                <tr style="background-color:rgb(61, 61, 61)"><th class="text-right">INTESTATARIO</th></tr>\n\
                <tr><td  class="text-right intest_ragione_sociale" style="font-weight:bold;" >' + result[0].ragione_sociale + '</td></tr>\n\
                <tr><td  class="text-right intest_indirizzo" >' + result[0].indirizzo + ' ' + result[0].numero + ' ' + result[0].cap + ' - ' + result[0].citta + ' (' + result[0].provincia + ') ' + result[0].paese + '</td></tr>\n\
                <tr><td  class="text-right intest_contatti">Tel: ' + result[0].telefono + ' - Email: ' + result[0].email + '</td></tr>\n\
                <tr><td  class="text-right intest_info_fiscali" >C.F: ' + result[0].codice_fiscale + ' - P.I: ' + result[0].partita_iva + '</td></tr>\n\
                </table>\n\
            </div>\n\
            <div class="col-md-12 col-xs-12" style="padding-bottom:10px;"><h2>FATTURA NUMERO <span class="numero_fattura_circuito"></span> DEL <span class="data_fattura_circuito"></span></h2></div>\n\
            <div class="col-md-12 col-xs-12" style="height:460px;overflow:auto;">\n\
            <table class="table table-responsive table-hover">\n\
            <tr style="background-color:rgb(61, 61, 61)"><th>Numero Buoni</th><th>Valore Unitario</th><th>Totale Buoni</th></tr>';
                form_gestione_circuiti += righe;
                form_gestione_circuiti += '</table>';

                //totale_buoni : X = 110 : 10

                var aliquota_iva = parseFloat(result[0].iva_non_pagati);

                //totale buoni x 10 / 110
                var imposta = parseFloat(totale_buoni) * aliquota_iva / (100 + aliquota_iva);

                //totale_buoni - imposta
                var imponibile = parseFloat(totale_buoni) - parseFloat(imposta);



                form_gestione_circuiti += '<table class="table table-responsive table-striped table-hover">\n\
                                        <tr><th>Numero fattura</th><th>Data fattura</th><th>Imponibile</th><th>IVA</th><th>Imposta</th><th>Totale</th></tr>\n\
                                        <tr><td class="numero_fattura_circuito"></td><td class="data_fattura_circuito"></td><td class="imponibile">&euro; ' + parseFloat(imponibile).toFixed(2) + '</td><td class="iva">' + aliquota_iva + '%</td><td class="imposta">&euro; ' + parseFloat(imposta).toFixed(2) + '</td><td class="totale" id="totale_generale_fattura_circuito">&euro; ' + parseFloat(totale_buoni).toFixed(2) + '</td></tr>\n\
                                    </table>';

                form_gestione_circuiti += '</div>\n\
                        </div>\n\
                    </div>';
                $('#fattura_circuito').html(form_gestione_circuiti);
                //NUMERO FATTURA
                fattura_circuito("vedi");
                $('.data_fattura_circuito').html(data);
                $('#popup_fattura_circuito').modal('show');
            });
        });
    });
}

function gestisci_circuito(event, id, visualizza = "ANAGRAFICA") {

    let tabella_reparti_IVA = REPARTO_controller.tabella_reparti_IVA();
    let tabella_IVA_semplice = IVA_controller.tabella_IVA_semplice();

    if (event !== undefined && event.target !== undefined && event.target !== null) {
        $(event.target).parent().find("a").removeClass('cat_accesa');
        $(event.target).addClass("cat_accesa");
    }

    $('#form_gestione_circuiti').html('');
    var form_gestione_circuiti = '';

    comanda.id_circuito = id;

    comanda.sincro.query("SELECT * FROM circuiti WHERE id=" + id + " ORDER BY ragione_sociale ASC LIMIT 1;", function (result) {

        comanda.sincro.query("SELECT * FROM tagli_buoni WHERE id_buono='" + id + "' ORDER BY cast(importo as float) ASC;", function (result_tagli) {

            console.log("GESTISCI CIRCUITO RICHIESTA");
            comanda.sincro.query_cassa("SELECT id,data,ora,tipologia,numero_fiscale,numero_buoni_pasto,buoni_pasto,locale,tavolo,operatore,totale FROM record_teste WHERE cast(buoni_pasto as float)>0 and nome_buono='" + id + "' \n\
                                        union all \n\
                                        SELECT id,data,ora,tipologia,numero_fiscale,numero_buoni_pasto_2 as numero_buoni_pasto ,buoni_pasto_2 as buoni_pasto,locale,tavolo,operatore,totale FROM record_teste WHERE cast(buoni_pasto_2 as float)>0 and nome_buono_2='" + id + "' \n\
                                        union all \n\
                                        SELECT id,data,ora,tipologia,numero_fiscale,numero_buoni_pasto_3 as numero_buoni_pasto,buoni_pasto_3 as buoni_pasto,locale,tavolo,operatore,totale FROM record_teste WHERE cast(buoni_pasto_3 as float)>0 and nome_buono_3='" + id + "' \n\
                                        union all \n\
                                        SELECT id,data,ora,tipologia,numero_fiscale,numero_buoni_pasto_4 as numero_buoni_pasto,buoni_pasto_4 as buoni_pasto,locale,tavolo,operatore,totale FROM record_teste WHERE cast(buoni_pasto_4 as float)>0 and nome_buono_4='" + id + "' \n\
                                        union all \n\
                                        SELECT id,data,ora,tipologia,numero_fiscale,numero_buoni_pasto_5 as numero_buoni_pasto,buoni_pasto_5 as buoni_pasto,locale,tavolo,operatore,totale FROM record_teste WHERE cast(buoni_pasto_5 as float)>0 and nome_buono_5='" + id + "' \n\
                                        ORDER BY id DESC;", 20000, function (movimenti) {



                console.log("MOVIMENTI BUONI PASTO", movimenti);
                for (var key in result[0]) {
                    console.log("RESULT0", key, result[0], result[0][key]);
                    if (result[0][key] === null) {
                        result[0][key] = '';
                    }
                }

                var righe = '';
                var numero_buoni = 0;
                var totale_buoni = 0.00;
                for (var key in movimenti) {
                    var mov = movimenti[key];
                    righe += '<tr><td style="text-align:center;">' + mov.data.replace(/-/gi, "/") + '</td><td style="text-align:center;">' + mov.ora + '</td><td style="text-align:center;">' + mov.tipologia.toLowerCase() + '</td><td style="text-align:center;">' + mov.numero_fiscale + '</td><td style="text-align:center;">' + mov.numero_buoni_pasto + '</td><td style="text-align:right;padding-right:2%;">' + parseFloat(mov.buoni_pasto).toFixed(2) + '</td><td style="text-align:right;padding-right:3%;">' + parseFloat(parseFloat(mov.buoni_pasto) * parseInt(mov.numero_buoni_pasto)).toFixed(2) + '</td><td style="text-align:center;">' + mov.locale + '</td><td style="text-align:center;">' + mov.tavolo + '</td><td style="text-align:center;">' + mov.operatore + '</td><td style="text-align:right;padding-right:3%;">' + parseFloat(mov.totale).toFixed(2) + '</td></tr>';
                    numero_buoni += parseFloat(mov.numero_buoni_pasto);
                    totale_buoni += parseFloat(mov.buoni_pasto);
                }


                form_gestione_circuiti = '<div class="editor_circuiti" style="display:none;">\n\
                                    <div class="col-md-12 col-xs-12"><label>' + comanda.lang[54] + '</label><input style="font-weight:bold;margin-bottom:10px;" class="form-control open" type="text" name="ragione_sociale" value="' + result[0].ragione_sociale + '"></div>\n\
                                    <div class="col-md-9 col-xs-9"><label>' + comanda.lang[143] + '</label><input style="margin-bottom:10px;" class="form-control" type="text" name="indirizzo"  value="' + result[0].indirizzo + '"></div>\n\
                                    <div class="col-md-3 col-xs-3"><label>' + comanda.lang[144] + '</label><input style="margin-bottom:10px;" class="form-control" type="text" name="numero"  value="' + result[0].numero + '"></div>\n\
                                    <div class="col-md-3 col-xs-3"><label>' + comanda.lang[145] + '</label><input style="margin-bottom:10px;" class="form-control" type="text" name="cap"  value="' + result[0].cap + '"></div>\n\
                                    <div class="col-md-3 col-xs-3"><label>' + comanda.lang[146] + '</label><input style="margin-bottom:10px;" class="form-control" type="text" name="citta"  value="' + result[0].citta + '"></div>\n\
                                    <div class="col-md-3 col-xs-3"><label>' + comanda.lang[147] + '</label><input style="margin-bottom:10px;" class="form-control" type="text" name="provincia"   value="' + result[0].provincia + '"></div>\n\
                                    <div class="col-md-3 col-xs-3"><label>Nazione</label><input style="margin-bottom:10px;" class="form-control" type="text" name="paese"   value="' + result[0].paese + '"></div>\n\
                                    <div class="col-md-3 col-xs-3"><label>' + comanda.lang[149] + '</label><input style="margin-bottom:10px;" class="form-control" type="text" name="cellulare"   value="' + result[0].cellulare + '"></div>\n\
                                    <div class="col-md-2 col-xs-2"><label>' + comanda.lang[69] + '</label><input style="margin-bottom:10px;" class="form-control" type="text" name="telefono" value="' + result[0].telefono + '"></div>\n\
                                    <div class="col-md-3 col-xs-3"><label>Adeguamento %</label><input style="margin-bottom:10px;" class="form-control" type="text" name="adeguamento_perc"  value="' + result[0].adeguamento_perc + '"></div>';


                form_gestione_circuiti += '<div class="col-md-3 col-xs-3"><label>IVA</label><select style="height:auto;font-size:25.5px;margin-bottom:14px;" class="form-control" name="iva_non_pagati">' + tabella_reparti_IVA + '</select></div>';

                form_gestione_circuiti += '<div class="col-md-4 col-xs-4"><label>' + comanda.lang[20] + '</label><input style="margin-bottom:10px;" class="form-control" type="text" name="email"  value="' + result[0].email + '"></div>\n\
                                    <div class="col-md-6 col-xs-6"><label>' + comanda.lang[10] + '</label><input style="margin-bottom:10px;text-transform:uppercase;" class="form-control" type="text" name="codice_fiscale"  value="' + result[0].codice_fiscale + '"></div>\n\
                                    <div class="col-md-4 col-xs-4"><label>' + comanda.lang[46] + '</label><input style="margin-bottom:10px;" class="form-control" type="text" name="partita_iva"  value="' + result[0].partita_iva + '"></div>\n\
                                    <div class="col-md-2 col-xs-2"><label>Ordinamento</label><input style="margin-bottom:10px;" class="form-control" type="text" name="ordinamento"  value="' + result[0].ordinamento + '"></div>\n\
                                    <!--<div class="col-md-3 col-xs-3"><label>Partita IVA Estera</label><input style="text-transform:uppercase;margin-bottom:10px;" class="form-control" type="text" name="partita_iva_estera"  value="' + result[0].partita_iva_estera + '"></div>-->\n\
                                    <div class="col-md-3 col-xs-3"><label>Cod. Destinatario</label><input style="text-transform:uppercase;margin-bottom:10px;" class="form-control" type="text" name="codice_destinatario"  value="' + result[0].codice_destinatario + '"></div>\n\
                                    <div class="col-md-6 col-xs-6"><label>PEC</label><input style="text-transform:uppercase;margin-bottom:10px;" class="form-control" type="text" name="pec"  value="' + result[0].pec + '"></div>\n\
                                    <div class="col-md-3 col-xs-3"><label>Regime Fiscale</label><input style="text-transform:uppercase;margin-bottom:10px;" class="form-control" type="text" name="regime_fiscale"  value="' + result[0].regime_fiscale + '"></div>';

                /*
                 var test1 = result[0].formato_trasmissione === "true" ? "checked" : "";
                 form_gestione_circuiti += '<div class="col-md-3 col-xs-3"><label>Pubblica Amministrazione</label><input style="text-transform:uppercase;margin-bottom:10px;height:5vh;" class="form-control" type="checkbox" name="formato_trasmissione"  ' + test1 + '></div>';
                 */

                var test1 = result[0].riepilogo_scontrini === "true" ? "checked" : "";
                form_gestione_circuiti += '<div class="col-md-3 col-xs-3"><label>Riepilogo Scontrini</label><input style="text-transform:uppercase;margin-bottom:10px;height:5vh;" class="form-control" type="checkbox" name="riepilogo_scontrini"  ' + test1 + '></div>';


                form_gestione_circuiti += '<div class="col-md-12 col-xs-12"><div style="cursor:pointer; float:left;padding-top:15px"><a class="simbolo_operazione" >ANAGRAFICA</a></div><div style="cursor:pointer; float:left; padding-top:15px;padding-left:15px;"><a ' + comanda.evento + '="$(\'.editor_circuiti\').hide();$(\'.lista_movimenti_buoni\').hide();$(\'.lista_tagli_buoni\').show();"  class="simbolo_operazione" >TAGLI</a></div><div style="cursor:pointer; float:left; padding-top:15px;padding-left:15px;"><a ' + comanda.evento + '="$(\'.editor_circuiti\').hide();$(\'.lista_movimenti_buoni\').show();$(\'.lista_tagli_buoni\').hide();"  class="simbolo_operazione" >FATTURE</a></div><div><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_circuito(\'' + result[0].id + '\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button><button class="pull-right col-md-2 col-xs-2 btn btn-danger" ' + comanda.evento + '="elimina_circuito(\'' + result[0].id + '\');" style="text-transform:uppercase;">' + comanda.lang[179] + '</button></div></div>\n\
                                </div>\n\
                                \n\
                                <div class="lista_movimenti_buoni" style="display:none;">\n\
                                    <div class="col-md-12 col-xs-12" style="height:644px;overflow:auto;">\n\
                                        <div class="text-center">\n\
                                            <h3 style="font-weight:bold;">MOVIMENTI MESE CORRENTE <span style="font-weight:normal;">(n. ' + numero_buoni + '/ Totale &euro; ' + parseFloat(totale_buoni).toFixed(2) + ')</span><button style="margin-bottom:5px;" class="pull-right col-md-2 col-xs-2 btn btn-info" >PRECEDENTI</button>\n\</h3>\n\
                                        </div>\n\
                                    \n\
                                    <table class="tabella_movimenti_mese_corrente table table-responsive table-striped table-hover">\n\
                                        <tr><th style="text-align:center">' + comanda.lang[206] + '</th><th style="text-align:center">' + comanda.lang[207] + '</th><th style="text-align:center">Tipo</th><th style="text-align:center">NF</th><th style="text-align:center">N. Buoni</th><th style="text-align:center">Taglio</th><th style="text-align:center">T. Buoni</th><th style="text-align:center">Locale</th><th style="text-align:center">Tavolo</th><th style="text-align:center">' + comanda.lang[43] + '</th><th style="text-align:center">Totale &euro;</th></tr>';

                form_gestione_circuiti += righe;
                form_gestione_circuiti += '</table>\n\
                                        </div>\n\
                                <div class="col-md-12 col-xs-12">\n\
                                    <div style="cursor:pointer; float:left;padding-top:15px;">\n\
                                        <a class="simbolo_operazione" ' + comanda.evento + '="$(\'.lista_tagli_buoni\').hide();$(\'.lista_movimenti_buoni\').hide();$(\'.editor_circuiti\').show();" >ANAGRAFICA</a>\n\
                                    </div>\n\
\n\                                 \n\
                                    <div style="cursor:pointer; float:left;padding-top:15px;padding-left:15px;">\n\
                                        <a class="simbolo_operazione" ' + comanda.evento + '="$(\'.lista_movimenti_buoni\').hide();$(\'.lista_tagli_buoni\').show();$(\'.editor_circuiti\').hide();" >TAGLI</a>\n\
                                    </div>\n\
                                    \n\
                                    <div style="cursor:pointer; float:left; padding-top:15px;padding-left:15px;">\n\
                                        <a class="simbolo_operazione" >FATTURE</a>\n\
                                    </div>\n\
                                <div>\n\
                                <button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="visualizza_fattura_buoni_pasto(' + id + ')">VEDI FATTURA</button>\n\
                            </div>\n\
                        </div>\n\
                    </div>';

                var righe = '';
                var numero_tagli = 0;
                var totale_tagli = 0.00;
                for (var key in result_tagli) {
                    var mov = result_tagli[key];
                    righe += '<tr><td style="font-weight:bold;font-size:25.5px;">&euro;</td><td style="text-align:center;"><input readonly style="font-weight:bold;font-size:25.5px;" class="form-control kb_num" type="text" value="' + mov.importo + '"></td><td onclick="elimina_taglio(\'' + id + '\',\'' + mov.id + '\')" style="color:red;font-weight:bold;font-size:25.5px;">X</td></tr>';
                    totale_tagli += parseFloat(mov.importo);
                    numero_tagli++;
                }

                form_gestione_circuiti += '<div class="lista_tagli_buoni" style="display:none;">\n\
                                    <div class="col-xs-3 col-xs-offset-4" style="height:644px;overflow:auto;">\n\
                                        <div class="text-center">\n\
                                            <h3 style="font-weight:bold;">TAGLI<span style="font-weight:normal;"> (n. ' + numero_tagli + ')</span>\n\</h3>\n\
                                        </div>\n\
                                    \n\
                                    <div class="col-xs-12">\n\
                                    <table class="table table-responsive">\n\
                                        <tr><th></th><th style="text-align:center">Importo</th><th></th></tr>';



                form_gestione_circuiti += righe;
                form_gestione_circuiti += '</table></div>\n\
                                        </div>\n\
                                <div class="col-md-12 col-xs-12">\n\
                                    <div style="cursor:pointer; float:left;padding-top:15px;">\n\
                                        <a class="simbolo_operazione" ' + comanda.evento + '="$(\'.lista_tagli_buoni\').hide();$(\'.lista_movimenti_buoni\').hide();$(\'.editor_circuiti\').show();" >ANAGRAFICA</a>\n\
                                    </div>\n\
                                 \n\
                                    <div style="cursor:pointer; float:left;padding-top:15px;padding-left:15px;">\n\
                                        <a class="simbolo_operazione">TAGLI</a>\n\
                                    </div>\n\
                                    \n\
                                    <div style="cursor:pointer; float:left; padding-top:15px;padding-left:15px;">\n\
                                        <a class="simbolo_operazione" ' + comanda.evento + '="$(\'.lista_tagli_buoni\').hide();$(\'.lista_movimenti_buoni\').show();$(\'.editor_circuiti\').hide();">FATTURE</a>\n\
                                    </div>\n\
                                <div>\n\
                                <button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="nuovo_taglio(\'' + id + '\')">AGG. TAGLIO</button>\n\
                            </div>\n\
                        </div>\n\
                    </div>';



                $('#form_gestione_circuiti').html(form_gestione_circuiti);

                $('#form_gestione_circuiti select[name="iva_non_pagati"] option[value="' + result[0].iva_non_pagati + '"]').attr("selected", true);

                switch (visualizza) {
                    case "ANAGRAFICA":
                        $('.editor_circuiti').show();
                        break;
                    case "FATTURE":
                        $('.lista_movimenti_buoni').show();
                        break;
                    case "TAGLI":
                        $('.lista_tagli_buoni').show();
                        break;
                }
            });
        });
    });
}

function elimina_taglio(id_buono, id_taglio) {

    var testo_query = "DELETE FROM tagli_buoni WHERE id=" + id_taglio + " and id_buono='" + id_buono + "';";
    //comanda.array_dbcentrale_mancanti.push(testo_query);
    comanda.sock.send({
        tipo: "eliminazione_tagli",
        operatore: comanda.operatore,
        query: testo_query,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    comanda.sincro.query(testo_query, function () {

        gestisci_circuito(undefined, id_buono, "TAGLI");
        //apri_popup_buoni($('[name="scelta_buoni__nome_circuito"]').val(), $('[name="scelta_buoni__importo_taglio"]').val());
    });
    comanda.sincro.query_cassa();
}

function nuovo_taglio(id_buono) {

    bootbox.prompt({

        size: "medium",
        title: "Precisa l'importo",
        value: "",
        className: 'kb_num',
        callback: function (importo) {

            if (importo !== null) {
                var testo_query = "select importo from tagli_buoni where importo='" + parseFloat(importo).toFixed(2) + "' and id_buono='" + id_buono + "' order by cast(id as int) desc limit 1;";
                var check = alasql(testo_query);

                if (check[0] !== undefined) {
                    bootbox.alert("L'importo del taglio &egrave; gi&agrave; stato inserito.");
                    return false;
                }

                var testo_query = "select id from tagli_buoni order by cast(id as int) desc limit 1;";
                comanda.sincro.query(testo_query, function (risultato) {

                    var id = 0;
                    if (risultato[0] !== undefined && risultato[0].id !== undefined) {
                        id = parseInt(risultato[0].id) + 1;
                    }

                    testo_query = "insert into tagli_buoni (id,id_buono,importo) VALUES (" + id + ",'" + id_buono + "','" + parseFloat(importo.replace(/,/g, '.')).toFixed(2) + "');";
                    console.log("QUERY BUONO FLY", testo_query);
                    comanda.sock.send({
                        tipo: "nuovo_taglio",
                        operatore: comanda.operatore,
                        query: testo_query,
                        terminale: comanda.terminale,
                        ip: comanda.ip_address
                    });
                    //comanda.array_dbcentrale_mancanti.push(testo_query);
                    comanda.sincro.query(testo_query, function () {
                        gestisci_circuito(undefined, id_buono, "TAGLI");
                        //apri_popup_buoni(id_buono, parseFloat(importo.replace(/,/g, '.')).toFixed(2));
                    });
                    comanda.sincro.query_cassa();
                });
            }
        }
    });
    setTimeout(function () {
        $('.bootbox-input-text:visible').trigger('touchend');
    }, 500);
}



function tendina_profili() {
    var select_servizi = "";

    if (comanda.multiprofilo === "1") {
        var r = alasql("select id,nome_profilo from settaggi_profili;");


        select_servizi += "<select id='selezione_profilo_settaggi_tendina' class='form-control' style='font-size:25.5px;height:auto;'  onchange='changeFunc(event);'>";

        r.forEach(function (e) {
            var selected = "";
            //LASCIARE UGUALIANZA ASTRATTA (NON STRETTA)
            if (e.id == comanda.folder_number) {
                selected = " selected='selected' ";
            }
            select_servizi += "<option " + selected + " nome_profilo='" + e.nome_profilo + "' id_profilo='" + e.id + "';'>" + e.nome_profilo + "</option>";
        });
        select_servizi += "</select>";
        $("#selezione_profilo_settaggi_tasti").show();
    } else {
        $("#selezione_profilo_settaggi_tasti").hide();
    }

    $(".selezione_profilo_settaggi").html(select_servizi);
}

function clona_profilo(nome_profilo) {
    var s1 = alasql("select * from settaggi_profili order by id desc limit 1;");
    var last_id_1 = s1[0].id;
    var next_id_1 = last_id_1 + 1;
    s1[0].id = next_id_1;
    s1[0].nome_profilo = nome_profilo;
    var indici_1 = Object.keys(s1[0]);
    var righe_1 = Object.values(s1[0]).map(function (date) {
        if (Number.isInteger(date)) {
            return date;
        } else {
            return "'" + date + "'";
        }
    }).join(",");

    var s2 = alasql("select * from impostazioni_fiscali order by id desc limit 1;");
    var last_id_2 = s2[0].id;
    var next_id_2 = last_id_2 + 1;
    s2[0].id = next_id_2;
    var indici_2 = Object.keys(s2[0]);
    var righe_2 = Object.values(s2[0]).map(function (date) {
        if (Number.isInteger(date)) {
            return date;
        } else {
            return "'" + date + "'";
        }
    }).join(",");

    var s3 = alasql("select * from dati_servizio order by cast(id as int) desc limit 1;");
    var last_id_3 = parseInt(s3[0].id);
    var next_id_3 = last_id_3 + 1;
    s3[0].id = next_id_3;
    var indici_3 = Object.keys(s3[0]);
    var righe_3 = Object.values(s3[0]).map(function (date) {
        if (Number.isInteger(date)) {
            return date;
        } else {
            return "'" + date + "'";
        }
    }).join(",");

    var q1 = "insert into settaggi_profili (" + indici_1 + ") values(" + righe_1 + ");";
    comanda.sock.send({
        tipo: "aggiornamento_settaggi",
        operatore: comanda.operatore,
        query: q1,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    comanda.sincro.query(q1, function () {

    });

    var q2 = "insert into impostazioni_fiscali (" + indici_2 + ") values(" + righe_2 + ");";
    comanda.sock.send({
        tipo: "aggiornamento_settaggi",
        operatore: comanda.operatore,
        query: q2,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    comanda.sincro.query(q2, function () {

    });

    var q3 = "insert into dati_servizio (" + indici_3 + ") values(" + righe_3 + ");";
    comanda.sock.send({
        tipo: "aggiornamento_settaggi",
        operatore: comanda.operatore,
        query: q3,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    comanda.sincro.query(q3, function () {

    });

    comanda.sincro.query_cassa();

    tendina_profili();



}

function elimina_profilo(event, id) {

    var q1 = "delete from settaggi_profili where id=" + id + ";";
    comanda.sock.send({
        tipo: "aggiornamento_settaggi",
        operatore: comanda.operatore,
        query: q1,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    comanda.sincro.query(q1, function () {

    });

    var q2 = "delete from impostazioni_fiscali where id=" + id + ";";
    comanda.sock.send({
        tipo: "aggiornamento_settaggi",
        operatore: comanda.operatore,
        query: q2,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    comanda.sincro.query(q2, function () {

    });

    var q3 = "delete from dati_servizio where id='" + id + "';";
    comanda.sock.send({
        tipo: "aggiornamento_settaggi",
        operatore: comanda.operatore,
        query: q3,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    comanda.sincro.query(q3, function () {

    });

    comanda.sincro.query_cassa();

    bootbox.alert("Il profilo " + comanda.servizio + " e' stato eliminato.");

    tendina_profili();

    changeFunc(event);

}

function popup_nuovo_profilo() {
    var box = bootbox.dialog({
        message: "Nome: <input type='text' id='nome_profilo'>",
        title: "Nuovo Profilo",
        buttons: {
            main: {
                label: "OK",
                className: "btn-primary",
                callback: function () {
                    clona_profilo($('#nome_profilo').val());
                    bootbox.alert("Il profilo " + $('#nome_profilo').val() + " e' stato creato con successo.");
                }
            }
        }
    });

    box.bind('shown.bs.modal', function () {
        $("#nome_profilo").focus(function () {
            if (comanda.mobile === true) {
                $(this).blur();
            }
        });
        $("#nome_profilo").trigger("touchend");
    });
}

function avviso_elimina_profilo(event) {
    bootbox.confirm("Sei sicuro di voler eliminare il profilo " + comanda.servizio + "?", function (result) {
        if (result === true) {
            elimina_profilo(event, comanda.folder_number);
        }
    });
}

function stampa_anagrafica_clienti() {

    comanda.sincro.query("SELECT * FROM clienti ORDER BY ragione_sociale ASC;", function (result) {

        var intestazione = '';

        intestazione += '<tr>';

        intestazione += '<th colspan="8">';
        intestazione += comanda.profilo_aziendale.ragione_sociale + ' - ' + comanda.profilo_aziendale.indirizzo + ', ' + comanda.profilo_aziendale.numero + ' - ' + comanda.profilo_aziendale.cap + ' - ' + comanda.profilo_aziendale.citta + ' (' + comanda.profilo_aziendale.provincia + ') - ' + comanda.profilo_aziendale.partita_iva;
        intestazione += '</th>';

        intestazione += '</tr>';


        intestazione += '<tr>';

        intestazione += '<th>';
        intestazione += 'Ragione Sociale';
        intestazione += '</th>';

        intestazione += '<th>';
        intestazione += 'Indirizzo';
        intestazione += '</th>';

        intestazione += '<th>';
        intestazione += 'n.';
        intestazione += '</th>';

        intestazione += '<th>';
        intestazione += 'CAP';
        intestazione += '</th>';

        intestazione += '<th>';
        intestazione += 'Citta';
        intestazione += '</th>';

        intestazione += '<th>';
        intestazione += 'PR';
        intestazione += '</th>';

        intestazione += '<th>';
        intestazione += 'C.F.';
        intestazione += '</th>';

        intestazione += '<th>';
        intestazione += 'P. IVA';
        intestazione += '</th>';

        intestazione += '<th>';
        intestazione += 'P. IVA Estera';
        intestazione += '</th>';

        intestazione += '</tr>';


        var impaginazione = '<table width="100%">';

        var i = 0;
        result.forEach(function (el) {
            if (i % 42 === 0) {
                impaginazione += intestazione;
            }
            i++;

            impaginazione += '<tr>';

            impaginazione += '<td>';
            impaginazione += el.ragione_sociale;
            impaginazione += '</td>';

            impaginazione += '<td>';
            impaginazione += el.indirizzo;
            impaginazione += '</td>';

            impaginazione += '<td>';
            impaginazione += el.numero;
            impaginazione += '</td>';

            impaginazione += '<td>';
            impaginazione += el.cap;
            impaginazione += '</td>';

            impaginazione += '<td>';
            impaginazione += el.citta;
            impaginazione += '</td>';

            impaginazione += '<td>';
            impaginazione += el.provincia;
            impaginazione += '</td>';

            impaginazione += '<td>';
            impaginazione += el.codice_fiscale;
            impaginazione += '</td>';

            impaginazione += '<td>';
            impaginazione += el.partita_iva;
            impaginazione += '</td>';

            impaginazione += '<td>';
            impaginazione += el.partita_iva_estera;
            impaginazione += '</td>';

            impaginazione += '</tr>';
        });

        impaginazione += '</table>';

        //Questo ï¿½ il tag per STAMPARE sempre!
        //screen ï¿½ per lo schermo invece
        $('.print').html(impaginazione);
        window.print();

        console.log('ANAGRAFICA CLIENTI', impaginazione);

    });
}

function dict_reverse(object) {
    var newObject = {};
    var keys = [];

    for (var key in object) {
        keys.push(key);
    }

    for (var i = keys.length - 1; i >= 0; i--) {
        var value = object[keys[i]];
        newObject[keys[i]] = value;
    }

    return newObject;
}

function tasto_gestisci_cliente() {



    $.ajax({
        type: "POST",
        async: true,
        url: comanda.url_server + 'classi_php/trasferimentosqlitemysql_temp.lib.php',
        dataType: "json",
        beforeSend: function () { },
        success: function (rar) {
            if (rar === true) {

                selezione_operatore('lang_132');
                scegli_default();
                $('#tasto_gestione_clienti').hide();

            }

        }
    });

}


function gestisci_cliente(event, id) {

    let tabella_reparti_IVA = REPARTO_controller.tabella_reparti_IVA();
    let tabella_IVA_semplice = IVA_controller.tabella_IVA_semplice();

    if (event !== undefined && event.target !== undefined && event.target !== null) {
        $(event.target).parent().find("a").removeClass('cat_accesa');
        $(event.target).addClass("cat_accesa");
    }

    $('#form_gestione_clienti').html('');
    var form_gestione_clienti = '';
    comanda.sincro.query("SELECT * FROM clienti WHERE id=" + id + " ORDER BY ragione_sociale ASC LIMIT 1;", function (result) {

        for (var key in result[0]) {
            console.log("RESULT0", key, result[0], result[0][key]);
            if (result[0][key] === null) {
                result[0][key] = '';
            }
        }

        var righe = new Object();




        comanda.sincro.query_incassi("SELECT * FROM record_teste WHERE id_cliente='" + id + "' union all SELECT * FROM record_teste_temp WHERE id_cliente='" + id + "' ORDER BY data DESC,ora DESC;", 1000000, function (record_testa) {

            for (var key in record_testa) {

                var obj = record_testa[key];
                if (parseFloat(obj.non_pagato) > 0) {
                    obj.tipo = 'NON PAGATO';
                } else if (obj.tipologia === 'FATTURA') {
                    obj.tipo = 'FATTURA';
                } else {
                    obj.tipo = 'TEST';
                }

                if (righe[obj.data.substr(3, 2)] === undefined) {
                    righe[obj.data.substr(3, 2)] = new Array();
                }

                var numero_fattura = aggZero(obj.numero_fiscale, 4);
                var anno = "20" + obj.data.substr(6, 2);
                var mese = aggZero(obj.data.substr(3, 2), 2);
                var giorno = aggZero(obj.data.substr(0, 2), 2);

                if (obj.tipo === "FATTURA") {
                    righe[obj.data.substr(3, 2)].push('<tr><th style="text-align:center">' + obj.data.replace(/-/gi, '/') + '</th><th style="text-align:center">' + obj.ora + '</th><th style="text-align:center">' + obj.tipo + '</th><th style="text-align:center">' + obj.numero_fiscale + '</th><th style="text-align:center">' + obj.iva_non_pagato + '%</th><th style="text-align:center">&euro; ' + parseFloat(obj.totale).toFixed(2) + '</th><th style="text-align:right;"><button type="button" onclick="ristampa_veloce_fattura(\'\',\'' + numero_fattura + '\',\'' + anno + '\',\'' + mese + '\',\'' + giorno + '\')" class="btn btn-info" style="text-transform:uppercase;font-size:initial;">Ri-stampa</button></th></tr>');
                } else {
                    righe[obj.data.substr(3, 2)].push('<tr><th style="text-align:center">' + obj.data.replace(/-/gi, '/') + '</th><th style="text-align:center">' + obj.ora + '</th><th style="text-align:center">' + obj.tipo + '</th><th style="text-align:center">' + obj.numero_fiscale + '</th><th style="text-align:center">' + obj.iva_non_pagato + '%</th><th style="text-align:center">&euro; ' + parseFloat(obj.totale).toFixed(2) + '</th><th style="text-align:right;"><button type="button" onclick="ristampa_veloce_scontrino(\'\',\'' + numero_fattura + '\',\'' + anno + '\',\'' + mese + '\',\'' + giorno + '\')" class="btn btn-info" style="text-transform:uppercase;font-size:initial;">Ri-stampa</button></th></tr>');
                }
            }




            form_gestione_clienti = '<div class="editor_clienti">\n\
            <div class="col-md-2 col-xs-2 cls_fat_nazione"><label>Nazione</label>\n\
                                 <select style="height:auto;font-size:25.5px;margin-bottom:14px;" class="form-control" name="nazione">\n\
                                                <option value="AF">Afghanistan</option>\n\
                                                <option value="AX">Åland Islands</option>\n\
                                                <option value="AL">Albania</option>\n\
                                                <option value="DZ">Algeria</option>\n\
                                                <option value="AS">American Samoa</option>\n\
                                                <option value="AD">Andorra</option>\n\
                                                <option value="AO">Angola</option>\n\
                                                <option value="AI">Anguilla</option>\n\
                                                <option value="AQ">Antarctica</option>\n\
                                                <option value="AG">Antigua and Barbuda</option>\n\
                                                <option value="AR">Argentina</option>\n\
                                                <option value="AM">Armenia</option>\n\
                                                <option value="AW">Aruba</option>\n\
                                                <option value="AU">Australia</option>\n\
                                                <option value="AT">Austria</option>\n\
                                                <option value="AZ">Azerbaijan</option>\n\
                                                <option value="BS">Bahamas</option>\n\
                                                <option value="BH">Bahrain</option>\n\
                                                <option value="BD">Bangladesh</option>\n\
                                                <option value="BB">Barbados</option>\n\
                                                <option value="BY">Belarus</option>\n\
                                                <option value="BE">Belgium</option>\n\
                                                <option value="BZ">Belize</option>\n\
                                                <option value="BJ">Benin</option>\n\
                                                <option value="BM">Bermuda</option>\n\
                                                <option value="BT">Bhutan</option>\n\
                                                <option value="BO">Bolivia, Plurinational State of</option>\n\
                                                <option value="BQ">Bonaire, Sint Eustatius and Saba</option>\n\
                                                <option value="BA">Bosnia and Herzegovina</option>\n\
                                                <option value="BW">Botswana</option>\n\
                                                <option value="BV">Bouvet Island</option>\n\
                                                <option value="BR">Brazil</option>\n\
                                                <option value="IO">British Indian Ocean Territory</option>\n\
                                                <option value="BN">Brunei Darussalam</option>\n\
                                                <option value="BG">Bulgaria</option>\n\
                                                <option value="BF">Burkina Faso</option>\n\
                                                <option value="BI">Burundi</option>\n\
                                                <option value="KH">Cambodia</option>\n\
                                                <option value="CM">Cameroon</option>\n\
                                                <option value="CA">Canada</option>\n\
                                                <option value="CV">Cape Verde</option>\n\
                                                <option value="KY">Cayman Islands</option>\n\
                                                <option value="CF">Central African Republic</option>\n\
                                                <option value="TD">Chad</option>\n\
                                                <option value="CL">Chile</option>\n\
                                                <option value="CN">China</option>\n\
                                                <option value="CX">Christmas Island</option>\n\
                                                <option value="CC">Cocos (Keeling) Islands</option>\n\
                                                <option value="CO">Colombia</option>\n\
                                                <option value="KM">Comoros</option>\n\
                                                <option value="CG">Congo</option>\n\
                                                <option value="CD">Congo, the Democratic Republic of the</option>\n\
                                                <option value="CK">Cook Islands</option>\n\
                                                <option value="CR">Costa Rica</option>\n\
                                                <option value="CI">Côte d\'Ivoire</option>\n\
                                                <option value="HR">Croatia</option>\n\
                                                <option value="CU">Cuba</option>\n\
                                                <option value="CW">Curaçao</option>\n\
                                                <option value="CY">Cyprus</option>\n\
                                                <option value="CZ">Czech Republic</option>\n\
                                                <option value="DK">Denmark</option>\n\
                                                <option value="DJ">Djibouti</option>\n\
                                                <option value="DM">Dominica</option>\n\
                                                <option value="DO">Dominican Republic</option>\n\
                                                <option value="EC">Ecuador</option>\n\
                                                <option value="EG">Egypt</option>\n\
                                                <option value="SV">El Salvador</option>\n\
                                                <option value="GQ">Equatorial Guinea</option>\n\
                                                <option value="ER">Eritrea</option>\n\
                                                <option value="EE">Estonia</option>\n\
                                                <option value="ET">Ethiopia</option>\n\
                                                <option value="FK">Falkland Islands (Malvinas)</option>\n\
                                                <option value="FO">Faroe Islands</option>\n\
                                                <option value="FJ">Fiji</option>\n\
                                                <option value="FI">Finland</option>\n\
                                                <option value="FR">France</option>\n\
                                                <option value="GF">French Guiana</option>\n\
                                                <option value="PF">French Polynesia</option>\n\
                                                <option value="TF">French Southern Territories</option>\n\
                                                <option value="GA">Gabon</option>\n\
                                                <option value="GM">Gambia</option>\n\
                                                <option value="GE">Georgia</option>\n\
                                                <option value="DE">Germany</option>\n\
                                                <option value="GH">Ghana</option>\n\
                                                <option value="GI">Gibraltar</option>\n\
                                                <option value="GR">Greece</option>\n\
                                                <option value="GL">Greenland</option>\n\
                                                <option value="GD">Grenada</option>\n\
                                                <option value="GP">Guadeloupe</option>\n\
                                                <option value="GU">Guam</option>\n\
                                                <option value="GT">Guatemala</option>\n\
                                                <option value="GG">Guernsey</option>\n\
                                                <option value="GN">Guinea</option>\n\
                                                <option value="GW">Guinea-Bissau</option>\n\
                                                <option value="GY">Guyana</option>\n\
                                                <option value="HT">Haiti</option>\n\
                                                <option value="HM">Heard Island and McDonald Islands</option>\n\
                                                <option value="VA">Holy See (Vatican City State)</option>\n\
                                                <option value="HN">Honduras</option>\n\
                                                <option value="HK">Hong Kong</option>\n\
                                                <option value="HU">Hungary</option>\n\
                                                <option value="IS">Iceland</option>\n\
                                                <option value="IN">India</option>\n\
                                                <option value="ID">Indonesia</option>\n\
                                                <option value="IR">Iran, Islamic Republic of</option>\n\
                                                <option value="IQ">Iraq</option>\n\
                                                <option value="IE">Ireland</option>\n\
                                                <option value="IM">Isle of Man</option>\n\
                                                <option value="IL">Israel</option>\n\
                                                <option value="IT" selected="">Italy</option>\n\
                                                <option value="JM">Jamaica</option>\n\
                                                <option value="JP">Japan</option>\n\
                                                <option value="JE">Jersey</option>\n\
                                                <option value="JO">Jordan</option>\n\
                                                <option value="KZ">Kazakhstan</option>\n\
                                                <option value="KE">Kenya</option>\n\
                                                <option value="KI">Kiribati</option>\n\
                                                <option value="KP">Korea, Democratic People\'s Republic of</option>\n\
                                                <option value="KR">Korea, Republic of</option>\n\
                                                <option value="KW">Kuwait</option>\n\
                                                <option value="KG">Kyrgyzstan</option>\n\
                                                <option value="LA">Lao People\'s Democratic Republic</option>\n\
                                                <option value="LV">Latvia</option>\n\
                                                <option value="LB">Lebanon</option>\n\
                                                <option value="LS">Lesotho</option>\n\
                                                <option value="LR">Liberia</option>\n\
                                                <option value="LY">Libya</option>\n\
                                                <option value="LI">Liechtenstein</option>\n\
                                                <option value="LT">Lithuania</option>\n\
                                                <option value="LU">Luxembourg</option>\n\
                                                <option value="MO">Macao</option>\n\
                                                <option value="MK">Macedonia, the former Yugoslav Republic of</option>\n\
                                                <option value="MG">Madagascar</option>\n\
                                                <option value="MW">Malawi</option>\n\
                                                <option value="MY">Malaysia</option>\n\
                                                <option value="MV">Maldives</option>\n\
                                                <option value="ML">Mali</option>\n\
                                                <option value="MT">Malta</option>\n\
                                                <option value="MH">Marshall Islands</option>\n\
                                                <option value="MQ">Martinique</option>\n\
                                                <option value="MR">Mauritania</option>\n\
                                                <option value="MU">Mauritius</option>\n\
                                                <option value="YT">Mayotte</option>\n\
                                                <option value="MX">Mexico</option>\n\
                                                <option value="FM">Micronesia, Federated States of</option>\n\
                                                <option value="MD">Moldova, Republic of</option>\n\
                                                <option value="MC">Monaco</option>\n\
                                                <option value="MN">Mongolia</option>\n\
                                                <option value="ME">Montenegro</option>\n\
                                                <option value="MS">Montserrat</option>\n\
                                                <option value="MA">Morocco</option>\n\
                                                <option value="MZ">Mozambique</option>\n\
                                                <option value="MM">Myanmar</option>\n\
                                                <option value="NA">Namibia</option>\n\
                                                <option value="NR">Nauru</option>\n\
                                                <option value="NP">Nepal</option>\n\
                                                <option value="NL">Netherlands</option>\n\
                                                <option value="NC">New Caledonia</option>\n\
                                                <option value="NZ">New Zealand</option>\n\
                                                <option value="NI">Nicaragua</option>\n\
                                                <option value="NE">Niger</option>\n\
                                                <option value="NG">Nigeria</option>\n\
                                                <option value="NU">Niue</option>\n\
                                                <option value="NF">Norfolk Island</option>\n\
                                                <option value="MP">Northern Mariana Islands</option>\n\
                                                <option value="NO">Norway</option>\n\
                                                <option value="OM">Oman</option>\n\
                                                <option value="PK">Pakistan</option>\n\
                                                <option value="PW">Palau</option>\n\
                                                <option value="PS">Palestinian Territory, Occupied</option>\n\
                                                <option value="PA">Panama</option>\n\
                                                <option value="PG">Papua New Guinea</option>\n\
                                                <option value="PY">Paraguay</option>\n\
                                                <option value="PE">Peru</option>\n\
                                                <option value="PH">Philippines</option>\n\
                                                <option value="PN">Pitcairn</option>\n\
                                                <option value="PL">Poland</option>\n\
                                                <option value="PT">Portugal</option>\n\
                                                <option value="PR">Puerto Rico</option>\n\
                                                <option value="QA">Qatar</option>\n\
                                                <option value="RE">Réunion</option>\n\
                                                <option value="RO">Romania</option>\n\
                                                <option value="RU">Russian Federation</option>\n\
                                                <option value="RW">Rwanda</option>\n\
                                                <option value="BL">Saint Barthélemy</option>\n\
                                                <option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>\n\
                                                <option value="KN">Saint Kitts and Nevis</option>\n\
                                                <option value="LC">Saint Lucia</option>\n\
                                                <option value="MF">Saint Martin (French part)</option>\n\
                                                <option value="PM">Saint Pierre and Miquelon</option>\n\
                                                <option value="VC">Saint Vincent and the Grenadines</option>\n\
                                                <option value="WS">Samoa</option>\n\
                                                <option value="SM">San Marino</option>\n\
                                                <option value="ST">Sao Tome and Principe</option>\n\
                                                <option value="SA">Saudi Arabia</option>\n\
                                                <option value="SN">Senegal</option>\n\
                                                <option value="RS">Serbia</option>\n\
                                                <option value="SC">Seychelles</option>\n\
                                                <option value="SL">Sierra Leone</option>\n\
                                                <option value="SG">Singapore</option>\n\
                                                <option value="SX">Sint Maarten (Dutch part)</option>\n\
                                                <option value="SK">Slovakia</option>\n\
                                                <option value="SI">Slovenia</option>\n\
                                                <option value="SB">Solomon Islands</option>\n\
                                                <option value="SO">Somalia</option>\n\
                                                <option value="ZA">South Africa</option>\n\
                                                <option value="GS">South Georgia and the South Sandwich Islands</option>\n\
                                                <option value="SS">South Sudan</option>\n\
                                                <option value="ES">Spain</option>\n\
                                                <option value="LK">Sri Lanka</option>\n\
                                                <option value="SD">Sudan</option>\n\
                                                <option value="SR">Suriname</option>\n\
                                                <option value="SJ">Svalbard and Jan Mayen</option>\n\
                                                <option value="SZ">Swaziland</option>\n\
                                                <option value="SE">Sweden</option>\n\
                                                <option value="CH">Switzerland</option>\n\
                                                <option value="SY">Syrian Arab Republic</option>\n\
                                                <option value="TW">Taiwan, Province of China</option>\n\
                                                <option value="TJ">Tajikistan</option>\n\
                                                <option value="TZ">Tanzania, United Republic of</option>\n\
                                                <option value="TH">Thailand</option>\n\
                                                <option value="TL">Timor-Leste</option>\n\
                                                <option value="TG">Togo</option>\n\
                                                <option value="TK">Tokelau</option>\n\
                                                <option value="TO">Tonga</option>\n\
                                                <option value="TT">Trinidad and Tobago</option>\n\
                                                <option value="TN">Tunisia</option>\n\
                                                <option value="TR">Turkey</option>\n\
                                                <option value="TM">Turkmenistan</option>\n\
                                                <option value="TC">Turks and Caicos Islands</option>\n\
                                                <option value="TV">Tuvalu</option>\n\
                                                <option value="UG">Uganda</option>\n\
                                                <option value="UA">Ukraine</option>\n\
                                                <option value="AE">United Arab Emirates</option>\n\
                                                <option value="GB">United Kingdom</option>\n\
                                                <option value="US">United States</option>\n\
                                                <option value="UM">United States Minor Outlying Islands</option>\n\
                                                <option value="UY">Uruguay</option>\n\
                                                <option value="UZ">Uzbekistan</option>\n\
                                                <option value="VU">Vanuatu</option>\n\
                                                <option value="VE">Venezuela, Bolivarian Republic of</option>\n\
                                                <option value="VN">Viet Nam</option>\n\
                                                <option value="VG">Virgin Islands, British</option>\n\
                                                <option value="VI">Virgin Islands, U.S.</option>\n\
                                                <option value="WF">Wallis and Futuna</option>\n\
                                                <option value="EH">Western Sahara</option>\n\
                                                <option value="YE">Yemen</option>\n\
                                                <option value="ZM">Zambia</option>\n\
                                                <option value="ZW">Zimbabwe</option>\n\
                                            </select></div>';
            var test1 = result[0].formato_trasmissione === "true" ? "checked" : "";
            form_gestione_clienti += '<div class="col-md-1 col-xs-1 cls_fat_formato_trasmissione"><label>P.A.</label><input style="text-transform:uppercase;margin-bottom:10px;height:5vh;" class="form-control" type="checkbox" name="formato_trasmissione"  ' + test1 + '></div>';


            form_gestione_clienti += '<div class="col-md-1 col-xs-1">\n\
                                            <label>Azienda</label>\n\
                                            <input style="margin-bottom:10px;height:5vh;" class="form-control" type="radio" name="azienda_privato" id="gest_cli_ap_azienda" checked="checked" onClick=azienda_campi_mostrare()>\n\
                                        </div>\n\
                                        \n\
                                        <div class="col-md-1 col-xs-1">\n\
                                            <label>Privato</label>\n\
                                            <input style="margin-bottom:10px;height:5vh;" class="form-control" type="radio" name="azienda_privato" id="gest_cli_ap_privato" onClick=azienda_privato_campi()>\n\
                                        </div>';


            form_gestione_clienti += '<div class="col-md-12 col-xs-12"><label id="label_ragione_sociale_gestione_clienti">' + comanda.lang[54] + '</label><input style="text-transform:uppercase;font-weight:bold;margin-bottom:10px;" id= "ragione_sociale"class="form-control" type="text" name="ragione_sociale" value="' + result[0].ragione_sociale + '"></div>';

            form_gestione_clienti += '<div class="col-md-6 col-xs-6"><label id="label_nome_gestione_clienti">Nome</label><input style="text-transform:uppercase;font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="nome" value="' + result[0].nome + '"></div>                                 \n\
                                        <div class="col-md-6 col-xs-6"><label id="label_cognome_gestione_clienti">Cognome</label><input style="text-transform:uppercase;font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="cognome" value="' + result[0].cognome + '"></div>';
            var settaggi_profili = alasql("SELECT Field509 FROM settaggi_profili  where id=" + comanda.folder_number + "  LIMIT 1;");
            var settaggi_profilo = settaggi_profili[0];
            if (settaggi_profili[0].Field509 === "true") {
                form_gestione_clienti += '<div class="col-md-3 col-xs-3 cls_fat_codice_tessera"><label>Codice Tessera</label><input style="text-transform:uppercase;margin-bottom:10px;" class="form-control" type="text" name="codice_tessera_clienti_gestione" id="codice_tessera_clienti_gestione"  value="' + result[0].codice_tessera + '"></div>';
            }

            form_gestione_clienti += '<div class="col-md-9 col-xs-9"><label>' + comanda.lang[143] + '</label><input style="text-transform:uppercase;margin-bottom:10px;" class="form-control" type="text" name="indirizzo"  value="' + result[0].indirizzo + '"></div>\n\
                                 <div class="col-md-3 col-xs-3"><label>' + comanda.lang[144] + '</label><input style="text-transform:uppercase;margin-bottom:10px;" class="form-control" type="text" name="numero"  value="' + result[0].numero + '"></div>\n\
                                 <div class="col-md-2 col-xs-2"><label>' + comanda.lang[145] + '</label><input style="text-transform:uppercase;margin-bottom:10px;" class="form-control" type="text" name="cap"  value="' + result[0].cap + '"></div>\n\
                                 <div class="col-md-3 col-xs-3"><label>Comune</label><input style="text-transform:uppercase;margin-bottom:10px;" class="form-control" type="text" name="comune"  value="' + result[0].comune + '"></div>\n\
                                 <div class="col-md-2 col-xs-2 cls_fat_provincia"><label>' + comanda.lang[147] + '</label><input style="text-transform:uppercase;margin-bottom:10px;" class="form-control" type="text" name="provincia"   value="' + result[0].provincia + '"></div>\n\
                                 <div class="col-md-3 col-xs-3"><label>' + comanda.lang[69] + '</label><input style="text-transform:uppercase;margin-bottom:10px;" class="form-control" type="text" name="cellulare"   value="' + result[0].cellulare + '"></div>\n\
                                 <div class="col-md-3 col-xs-3"><label>' + comanda.lang[69] + '</label><input style="text-transform:uppercase;margin-bottom:10px;" class="form-control" type="text" name="telefono" value="' + result[0].telefono + '"></div>\n\
                                 <div class="col-md-6 col-xs-6"><label>' + comanda.lang[20] + '</label><input style="text-transform:uppercase;margin-bottom:10px;" class="form-control" type="text" name="email"  value="' + result[0].email + '"></div>\n\
                                 <div class="col-md-3 col-xs-3"><label>Codice Lotteria</label><input style="text-transform:uppercase;margin-bottom:10px;" class="form-control" type="text" name="codice_lotteria"  value="' + result[0].codice_lotteria + '" maxlength="8"></div>\n\
                                 <div class="col-md-6 col-xs-6 cls_fat_codice_fiscale"><label>' + comanda.lang[10] + '</label><input style="text-transform:uppercase;margin-bottom:10px;" class="form-control" type="text" name="codice_fiscale"  value="' + result[0].codice_fiscale + '"></div>\n\
                                 <div class="col-md-6 col-xs-6 cls_fat_partita_iva"><label>' + comanda.lang[46] + '</label><input style="text-transform:uppercase;margin-bottom:10px;" class="form-control" type="text" name="partita_iva" id="partita_iva"  value="' + result[0].partita_iva + '"></div>\n\
                                 <div class="col-md-3 col-xs-3 cls_fat_partita_iva_estera"><label>Partita IVA Estera</label><input style="text-transform:uppercase;margin-bottom:10px;" class="form-control" type="text" name="partita_iva_estera" id="partita_iva_estera"  value="' + result[0].partita_iva_estera + '"></div>\n\
                                 <div class="col-md-3 col-xs-3 cls_fat_codice_destinatario"><label>Cod. Destinatario</label><input style="text-transform:uppercase;margin-bottom:10px;" class="form-control" type="text" name="codice_destinatario" id="codice_destinatario" value="' + result[0].codice_destinatario + '"></div>\n\
                                 <div class="col-md-6 col-xs-6 cls_fat_pec"><label>PEC</label><input style="text-transform:uppercase;margin-bottom:10px;" class="form-control" type="text" name="pec" id="pec" value="' + result[0].pec + '"></div>\n\
                                 <div class="col-md-3 col-xs-3"><label id="Regime_Fiscale_label">Regime Fiscale</label>';

            form_gestione_clienti += '<select style="height:auto;font-size:25.5px;margin-bottom:14px;" class="form-control" name="regime_fiscale" id="regime_fiscale">';
            form_gestione_clienti += '<option value="RF01">Ordinario</option>';
            form_gestione_clienti += '<option value="RF02">Contribuenti minimi (art.1, c.96-117, L. 244/07)</option>';
            form_gestione_clienti += '<option value="RF04">Agricoltura e attività connesse e pesca (artt.34 e 34-bis, DPR 633/72)</option>';
            form_gestione_clienti += '<option value="RF05">Vendita sali e tabacchi (art.74, c.1, DPR. 633/72)</option>';
            form_gestione_clienti += '<option value="RF06">Commercio fiammiferi (art.74, c.1, DPR  633/72)</option>';
            form_gestione_clienti += '<option value="RF07">Editoria (art.74, c.1, DPR  633/72)</option>';
            form_gestione_clienti += '<option value="RF08">Gestione servizi telefonia pubblica (art.74, c.1, DPR 633/72) </option>';
            form_gestione_clienti += '<option value="RF09">Rivendita documenti di trasporto pubblico e di sosta (art.74, c.1, DPR  633/72)</option>';
            form_gestione_clienti += '<option value="RF10">Intrattenimenti, giochi e altre attività di cui alla tariffa allegata al DPR 640/72 (art.74, c.6, DPR 633/72)</option>';
            form_gestione_clienti += '<option value="RF11">Agenzie viaggi e turismo (art.74-ter, DPR 633/72)</option>';
            form_gestione_clienti += '<option value="RF12">Agriturismo (art.5, c.2, L. 413/91)</option>';
            form_gestione_clienti += '<option value="RF13">Vendite a domicilio (art.25-bis, c.6, DPR  600/73)</option>';
            form_gestione_clienti += '<option value="RF14">Vendite a domicilio (art.25-bis, c.6, DPR  600/73)</option>';
            form_gestione_clienti += '<option value="RF15">Agenzie di vendite all’asta di oggetti d’arte, antiquariato o da collezione (art.40-bis, DL 41/95)</option>';
            form_gestione_clienti += '<option value="RF16">IVA per cassa P.A. (art.6, c.5, DPR 633/72)</option>';
            form_gestione_clienti += '<option value="RF17">IVA per cassa (art. 32-bis, DL 83/2012)</option>';
            form_gestione_clienti += '<option value="RF18">Altro</option>';
            form_gestione_clienti += '<option value="RF19">Regime forfettario (art.1, c.54-89, L. 190/2014)</option>';
            form_gestione_clienti += '</select>';

            form_gestione_clienti += '</div>';

            var test1 = result[0].fatt_fine_mese_sn === "true" ? "checked" : "";
            form_gestione_clienti += '<div class="col-md-2 col-xs-2"><label id="fat_fine_mese_label">Fatt. Fine Mese Abilitata</label><input style="text-transform:uppercase;margin-bottom:16px;height:5vh;" class="form-control" type="checkbox" name="fatt_fine_mese_sn" id="fatt_fine_mese_sn" ' + test1 + '></div>';
            var test1 = result[0].esente_fattura_elettronica === "true" ? "checked" : "";
            form_gestione_clienti += '<div class="col-md-2 col-xs-2"><label id="esente_fattura_ele_label">Esente Fattura Elettronica</label><input style="text-transform:uppercase;margin-bottom:16px;height:5vh;" class="form-control" type="checkbox" name="esente_fattura_elettronica" id="esente_fattura_elettronica" ' + test1 + '></div>';
            form_gestione_clienti += '<div class="col-md-3 col-xs-3"><label id="iva_agevolata_label">Iva Agevolata</label><select style="height:auto;font-size:25.5px;margin-bottom:14px;" class="form-control" name="iva_non_pagati" id="iva_non_pagati"><option style="font-weight:bold;" value="NO">NO</option>' + tabella_reparti_IVA + '</select></div>\n\
                                      <div class="col-md-2 col-xs-2"><label>Sconto su Totale %</label><input style="text-transform:uppercase;margin-bottom:10px;" class="form-control" type="text" name="percentuale_sconto_su_totale"  value="' + result[0].percentuale_sconto_su_totale + '"></div>\n\
                                        <div class="col-md-12 col-xs-12"><div style="cursor:pointer; float:left;padding-top:15px"><a class="simbolo_operazione" >ANAGRAFICA</a></div><div style="cursor:pointer; float:left; padding-top:15px;padding-left:15px;"><a ' + comanda.evento + '="$(\'.editor_clienti\').hide();$(\'.lista_movimenti_clienti\').show();"  class="simbolo_operazione" >FATTURE</a></div><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_cliente(\'' + result[0].id + '\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button><button class="pull-right col-md-2 col-xs-2 btn btn-danger" ' + comanda.evento + '="elimina_cliente(\'' + result[0].id + '\');" style="text-transform:uppercase;">' + comanda.lang[179] + '</button></div>\n\
                                 <div class="col-md-12 col-xs-12"></div>\n\
                                 </div>\n\
                                 \n\
                                <div class="lista_movimenti_clienti" style="display:none;">\n\
                                    <div class="col-md-12 col-xs-12" style="height:66.4vh;overflow:auto;">\n\
                                        <div class="text-center">\n\
                                            <h3 style="font-weight:bold;">MOVIMENTI ANNUALI <span style="font-weight:normal;display:none;">(Tot. pizze: _____ / Tot. consegne: _____)</span>\n\</h3>\n\
                                        </div>\n\
                                    \n\
                                    <table class="table table-responsive table-striped table-hover">\n\
                                        <tr><th style="text-align:center;width:10vw;">' + comanda.lang[206] + '</th><th style="text-align:center;width:10vw;">' + comanda.lang[207] + '</th><th style="text-align:center;width:10vw;">Tipo</th><th style="text-align:center;width:8vw;">Numero</th><th style="text-align:center;width:5vw;">Aliquota</th><th style="text-align:center">Totale</th><th style="text-align:right">Ri-stampa</th><th></th></tr>\n\
                                    \n\</table>';
            //form_gestione_clienti += righe;




            form_gestione_clienti += '<div class="accordion" id="accordionExample">';

            var rev = dict_reverse(righe);


            for (var data in rev) {

                form_gestione_clienti += '<div class="card">';
                form_gestione_clienti += '<div class="card-header" id="heading' + data + '">';
                form_gestione_clienti += '<h2 class="mb-0">';
                form_gestione_clienti += ' <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse' + data + '" aria-expanded="true" aria-controls="collapseOne">';

                var nome_mese = "";

                switch (data) {
                    case "01":
                        nome_mese = "Gennaio";
                        break;
                    case "02":
                        nome_mese = "Febbraio";
                        break;
                    case "03":
                        nome_mese = "Marzo";
                        break;
                    case "04":
                        nome_mese = "Aprile";
                        break;
                    case "05":
                        nome_mese = "Maggio";
                        break;
                    case "06":
                        nome_mese = "Giugno";
                        break;
                    case "07":
                        nome_mese = "Luglio";
                        break;
                    case "08":
                        nome_mese = "Agosto";
                        break;
                    case "09":
                        nome_mese = "Settembre";
                        break;
                    case "10":
                        nome_mese = "Ottobre";
                        break;
                    case "11":
                        nome_mese = "Novembre";
                        break;
                    case "12":
                        nome_mese = "Dicembre";
                        break;

                }
                form_gestione_clienti += nome_mese;
                form_gestione_clienti += '</button>';
                form_gestione_clienti += '</h2>';
                form_gestione_clienti += '</div>';
                form_gestione_clienti += '<div id="collapse' + data + '" class="collapse" aria-labelledby="heading' + data + '" data-parent="#accordionExample">';
                form_gestione_clienti += '<div class="card-body">';
                form_gestione_clienti += '<table class="table table-responsive table-striped table-hover">';

                righe[data].forEach(function (value) {

                    form_gestione_clienti += value;

                });
                form_gestione_clienti += '</table>';

                form_gestione_clienti += '</div>';
                form_gestione_clienti += '</div>';
                form_gestione_clienti += '</div>';
            }

            form_gestione_clienti += '</div>';


            form_gestione_clienti += '</div>\n\
                                <div class="col-md-12 col-xs-12">';

            if (result[0].fatt_fine_mese_sn === "true") {
                form_gestione_clienti += '<button class="pull-right col-md-2 col-xs-2 btn btn-info" ' + comanda.evento + '="scelta_mese_fattura_non_pagati(\'' + id + '\');">Estratto Conto</button>\n\
                                    <button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="scelta_mese_fattura_non_pagati_vera(\'' + id + '\');">Fattura N.P.</button>';
            }


            form_gestione_clienti += '<div style="cursor:pointer; float:left;padding-top:15px">\n\
                                        <a class="simbolo_operazione" ' + comanda.evento + '="$(\'.lista_movimenti_clienti\').hide();$(\'.editor_clienti\').show();" >ANAGRAFICA</a>\n\
                                    </div>\n\
                                    \n\
                                    <div style="cursor:pointer; float:left; padding-top:15px;padding-left:15px;">\n\
                                        <a class="simbolo_operazione" >FATTURE</a>\n\
                                    </div>\n\
                                <div>\n\
                            </div>\n\
                        </div>\n\
                    </div>';
            $('#form_gestione_clienti').html(form_gestione_clienti);

            $('#form_gestione_clienti select[name="nazione"] option[value="' + result[0].nazione + '"]').attr("selected", true);
            $('#form_gestione_clienti select[name="iva_non_pagati"] option[value="' + result[0].iva_non_pagati + '"]').attr("selected", true);
            $('#form_gestione_clienti select[name="regime_fiscale"] option[value="' + result[0].regime_fiscale + '"]').attr("selected", true);

            if (result[0].tipo_cliente === "ap_azienda") {
                azienda_campi_mostrare();
                $('#gest_cli_ap_azienda').prop('checked', 'checked');
            } else if (result[0].tipo_cliente === "ap_privato") {
                azienda_privato_campi();
                $('#gest_cli_ap_privato').prop('checked', 'checked');
            }


            //selezione_campi_gestione_cliente();



        });


    });


}

function gestisci_categoria(event, id) {

    let tabella_reparti_IVA = REPARTO_controller.tabella_reparti_IVA();
    let tabella_IVA_semplice = IVA_controller.tabella_IVA_semplice();

    if (event !== undefined && event.target !== undefined && event.target !== null) {
        $(event.target).parent().parent().find("a").removeClass('cat_accesa');
        $(event.target).addClass("cat_accesa");
    }

    $('#form_gestione_clienti').html('');
    var form_gestione_categorie = '';
    comanda.sincro.query("SELECT * FROM categorie WHERE id='" + id + "' ORDER BY ordinamento ASC LIMIT 1;", function (result) {

        for (var key in result[0]) {
            console.log("RESULT0", key, result[0], result[0][key]);
            if (result[0][key] === null) {
                result[0][key] = '';
            }
        }

        var tipo = '';
        if (result[0].descrizione.indexOf("PREFERITI") !== -1) {
            form_gestione_categorie = '<div class="editor_clienti">\n\
                                    <div class="col-md-12 col-xs-12">\n\
                                    <div class="col-md-6 col-xs-6"><label>' + comanda.lang[96].capitalize() + '</label>&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'PREFERITI\', \'Si hanno a disposizione 3 categorie di preferiti. Ognuna potr&agrave; essere visualizzata in pi&ugrave; parti differenti, come tavolo, asporto, bar, ... Baster&agrave; abilitare i check successivi. Per posizionare i prodotti nella categoria dei preferiti, baster&agrave; andare su <i>Menu - Layout Tasti</i>, selezionare sulle destra l&#8217;  articolo da posizionare, e posizionarlo su un quadrato a propria scelta sulla sinistra. Gli articoli che si visualizzano sulla destra sono divisi per categoria. Qualora si colora un articolo dai preferiti, presenter&agrave; lo stesso colore anche sulla categoria originale.<br><br>Ricordarsi di salvare dopo aver effettuato la modifica.\')">?</a><input maxlength="20" style="text-transform:uppercase;font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="descrizione"  value="' + result[0].descrizione.toUpperCase() + '"></div>\n\
                                    <div class="col-md-2 col-xs-2"><label>' + comanda.lang[177] + '</label><input style="margin-bottom:10px;" class="form-control" type="text" name="ordinamento"   value="' + result[0].ordinamento + '"></div>';

            var test1 = result[0].visu_tavoli === "true" ? "checked" : "";
            form_gestione_categorie += '<div class="col-md-1 col-xs-1"><label>Tavoli</label><input class="form-control esclusione_css" type="checkbox" name="visu_tavoli" ' + test1 + '></div>';
            var test1 = result[0].visu_bar === "true" ? "checked" : "";
            form_gestione_categorie += '<div class="col-md-1 col-xs-1"><label>Bar</label><input class="form-control esclusione_css" type="checkbox" name="visu_bar" ' + test1 + '></div>';
            var test1 = result[0].visu_asporto === "true" ? "checked" : "";
            form_gestione_categorie += '<div class="col-md-1 col-xs-1"><label>Asporto</label><input class="form-control esclusione_css" type="checkbox" name="visu_asporto" ' + test1 + '></div>';
            var test1 = result[0].visu_continuo === "true" ? "checked" : "";
            form_gestione_categorie += '<div class="col-md-1 col-xs-1"><label>Continuo&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'VISUALIZZAZIONE PREFERITI\', \'Contrassegnare il quadrato corrispondente al tavolo sul quale si vuol far visualizzare la seguente categoria di preferiti.<br><br>Ricordarsi di salvare dopo aver effettuato la modifica.\')">?</a></label><input class="form-control esclusione_css" type="checkbox" name="visu_continuo" ' + test1 + '></div>';

            form_gestione_categorie += '<div class="col-md-8 col-xs-8"><label>' + comanda.lang[96].capitalize() + '</label><input maxlength="20" style="text-transform:uppercase;font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="alias"  value="' + result[0].alias.toUpperCase() + '"></div>';
            form_gestione_categorie += '<div class="col-md-12 col-xs-12"><div class="raggruppamento_applica_tutti_articoli" style="display:none;"><div class="col-md-1 col-xs-1"><input class="form-control esclusione_css" type="checkbox" name="applica_tutti_articoli"></div><div class="col-md-5 col-xs-5" style="font-size:30px">Applica a tutti gli articoli</div></div><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_categoria(\'' + result[0].id + '\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                   </div>';
        } else if (result[0].descrizione.toUpperCase().substr(0, 2) !== "V.") {
            form_gestione_categorie = '<div class="editor_clienti">\n\
                                   <div class="col-md-12 col-xs-12">\n\
                                   <div class="col-md-6 col-xs-6"><label>' + comanda.lang[96].capitalize() + '</label><input maxlength="20" style="text-transform:uppercase;font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="descrizione"  value="' + result[0].descrizione.toUpperCase() + '"></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>' + comanda.lang[177] + '</label><input style="margin-bottom:10px;" class="form-control" type="text" name="ordinamento"   value="' + result[0].ordinamento + '"></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>IVA Base / Servizi</label><select style="height:auto;font-size:25px;margin-bottom:10px;padding-left:0;padding-right:0;" class="form-control comparsa_applica_tutti_articoli" type="text" name="perc_iva" >' + tabella_IVA_semplice + '</select></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>IVA T.Away / Beni</label><select style="height:auto;font-size:25px;margin-bottom:10px;padding-left:0;padding-right:0;" class="form-control comparsa_applica_tutti_articoli" type="text" name="perc_iva_takeaway" >' + tabella_IVA_semplice + '</select></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>Reparto Servizi</label><select style="height:auto;font-size:25px;margin-bottom:10px;padding-left:0;padding-right:0;" class="form-control comparsa_applica_tutti_articoli" type="text" name="reparto_servizi" >' + tabella_reparti_IVA + '</select></div>\n\
                                   <div class="col-md-2 col-xs-2"><label>Reparto Beni</label><select style="height:auto;font-size:25px;margin-bottom:10px;padding-left:0;padding-right:0;" class="form-control comparsa_applica_tutti_articoli" type="text" name="reparto_beni" >' + tabella_reparti_IVA + '</select></div>\n\
                                   </div>\n\
                                   \n\
                                   <div class="col-md-12 col-xs-12">\n\
                                   <div class="col-md-4 col-xs-4"><label>' + comanda.lang[173] + '</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control comparsa_applica_tutti_articoli" type="text" name="cat_var" ></select></div>\n\
                                   <div class="col-md-4 col-xs-4"><label>' + comanda.lang[100].capitalize() + '</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control comparsa_applica_tutti_articoli" type="text" name="dest_stampa" ></select></div>\n\
                                   <div class="col-md-4 col-xs-4"><label>' + comanda.lang[100].capitalize() + ' 2</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control comparsa_applica_tutti_articoli" type="text" name="dest_stampa_2" ></select></div>\n\
                                   </div>';


            form_gestione_categorie += '<div class="col-md-12 col-xs-12">\n\
                                            <div class="col-md-4 col-xs-4"><label>' + comanda.lang[126] + '</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control comparsa_applica_tutti_articoli" type="text" name="portata" ></select></div>\n\
                                        </div>';
            var test1 = result[0].abilita_asporto === "true" ? "checked" : "";

            var test3 = result[0].disabilita_server === "S" ? "checked" : "";
            var test4 = result[0].disabilita_pda === "S" ? "checked" : "";

            form_gestione_categorie += '<div class="col-md-12 col-xs-12"><div class="col-md-1 col-xs-1"><input class="form-control esclusione_css" type="checkbox" name="disabilita_server" ' + test3 + '></div><div class="col-md-5 col-xs-5" style="font-size:30px">Disabilita Visu. PC</div><div class="col-md-1 col-xs-1"><input class="form-control esclusione_css" type="checkbox" name="abilita_asporto" ' + test1 + '></div><div class="col-md-5 col-xs-5" style="font-size:30px">Abilita su asporto</div></div>';


            var test1 = result[0].consegna_abilitata === "true" ? "checked" : "";

            form_gestione_categorie += '<div class="col-md-12 col-xs-12"><div class="col-md-1 col-xs-1"><input class="form-control esclusione_css" type="checkbox" name="disabilita_pda" ' + test4 + '></div><div class="col-md-5 col-xs-5" style="font-size:30px">Disabilita Visu. Cellulare</div> <div class="col-md-1 col-xs-1"><input class="form-control esclusione_css" type="checkbox" name="consegna_abilitata" ' + test1 + '></div><div class="col-md-5 col-xs-5" style="font-size:30px">Soggetto a Consegna&nbsp;<a style="font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'SOGGETTO A CONSEGNA\', \'Spuntando questa impostazione, qualsiasi articolo in questa categoria sar&agrave; soggetto al calcolo del prezzo di consegna.\')">?</a></div></div>';


            form_gestione_categorie += '<div class="col-md-12 col-xs-12 raggruppamento_applica_tutti_articoli" style="display:none;"><div class="col-md-1 col-xs-1"><input class="form-control esclusione_css" type="checkbox" name="applica_tutti_articoli"></div><div class="col-md-5 col-xs-5" style="font-size:30px">Applica a tutti gli articoli</div></div></div>';

            form_gestione_categorie += '<div class="col-md-12 col-xs-12" style="margin-top:1vh" ><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_categoria(\'' + result[0].id + '\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button><button class="pull-right col-xs-4 btn btn-danger" ' + comanda.evento + '="elimina_articoli_categoria(\'' + result[0].id + '\');"  style="text-transform:uppercase;">' + comanda.lang[181] + '</button><button class="pull-right col-xs-4 btn btn-danger" ' + comanda.evento + '="elimina_categoria(\'' + result[0].id + '\',\'' + tipo + '\');"  style="text-transform:uppercase;">' + comanda.lang[180] + '</button></div>\n\
                                   </div>';

        } else if (result[0].descrizione.toUpperCase().substr(0, 2) === "V.") {
            tipo = 'V';
            form_gestione_categorie = '<div class="editor_clienti">\n\
                                   <div class="col-md-12 col-xs-12">\n\
                                   <label>' + comanda.lang[96] + '</label><input maxlength="20" style="text-transform:uppercase;font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="descrizione"  value="' + result[0].descrizione.toUpperCase() + '">\n\
                                   </div>';

            var test1 = result[0].var_aperte === "S" ? "checked" : "";

            form_gestione_categorie += '<div class="col-md-1 col-xs-1"><input class="form-control esclusione_css" type="checkbox" name="var_aperte" ' + test1 + '></div><div class="col-md-5 col-xs-5" style="font-size:30px">Variante Aperta</div>';

            form_gestione_categorie += '<div class="col-md-12 col-xs-12"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_categoria(\'' + result[0].id + '\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button><button class="pull-right col-xs-4 btn btn-danger" ' + comanda.evento + '="elimina_articoli_categoria(\'' + result[0].id + '\');" style="text-transform:uppercase;">' + comanda.lang[181] + '</button><button class="pull-right col-xs-4 btn btn-danger" ' + comanda.evento + '="elimina_categoria(\'' + result[0].id + '\',\'' + tipo + '\');"  style="text-transform:uppercase;">' + comanda.lang[180] + '</button></div>\n\
                                   </div>';
        }

        $('#form_gestione_categorie').html(form_gestione_categorie);


        /* Selettore dell'IVA */
        $('[name="perc_iva"] option[value="' + result[0].perc_iva + '"]').attr("selected", true);
        $('[name="perc_iva_takeaway"] option[value="' + result[0].perc_iva_takeaway + '"]').attr("selected", true);
        /* Selettore dei reparti */
        $('[name="reparto_servizi"] option[value="' + result[0].reparto_servizi + '"]').attr("selected", true);
        $('[name="reparto_beni"] option[value="' + result[0].reparto_beni + '"]').attr("selected", true);

        //RIMUOVO I VECCHI RISULTATI DELLE TENDINE
        $('select[name="dest_stampa"] option').remove();
        $('select[name="dest_stampa_2"] option').remove();
        $('select[name="portata"] option').remove();
        var opzione = "";
        //RIEMPIO LA TENDINA DELLE STAMPANTI
        comanda.sincro.query("SELECT " + comanda.lingua_stampa + " as nome,numero FROM nomi_stampanti WHERE fiscale='n' and cast(numero as int) >= 50;", function (results) {


            $('[name="dest_stampa_2"]').append("<option></option>");
            results.forEach(function (row) {
                opzione = "<option value=\"" + row.numero + "\">" + row.nome + "</option>";
                $('[name="dest_stampa"]').append(opzione);
                $('[name="dest_stampa_2"]').append(opzione);
            });
            $('[name="dest_stampa"]').append("<option value=\"T\">TUTTE</option>");

            $('[name="dest_stampa"] option[value="' + result[0].dest_stampa + '"]').attr("selected", true);
            $('[name="dest_stampa_2"] option[value="' + result[0].dest_stampa_2 + '"]').attr("selected", true);
        });
        //RIEMPIO LA TENDINA DELLE CATEGORIA VARIANTI
        comanda.sincro.query("SELECT id,ordinamento,descrizione FROM categorie WHERE SUBSTR(descrizione,1,2)=\"V.\" ORDER BY ordinamento ASC;", function (results) {

            opzione = "<option value=\"ND\">NESSUNA</option>";
            $('[name="cat_var"]').append(opzione);

            results.forEach(function (row) {
                opzione = "<option value=\"" + row.id + "\">" + row.descrizione + "</option>";
                $('[name="cat_var"]').append(opzione);
            });
            //MODIFICATO  04/11/2016 altrimenti nella categoria non prende la variante
            $('[name="cat_var"] option[value="' + result[0].cat_var.split('/')[0] + '"]').attr("selected", true);
        });
        //RIEMPIO LA TENDINA DELLA PORTATA
        comanda.sincro.query("SELECT " + comanda.lingua + ",numero FROM nomi_portate  order by numero asc;", function (results) {

            results.forEach(function (row) {
                opzione = "<option value=\"" + row.numero + "\">" + row[comanda.lingua] + "</option>";
                $('[name="portata"]').append(opzione);
            });
            $('[name="portata"] option[value="' + result[0].portata + '"]').attr("selected", true);
        });
    });
}

function gestisci_portata(event, id) {

    if (event !== undefined && event.target !== undefined && event.target !== null) {
        $(event.target).parent().parent().find("a").removeClass('cat_accesa');
        $(event.target).addClass("cat_accesa");
    }

    $('#form_gestione_clienti').html('');
    var form_gestione_portate = '';
    comanda.sincro.query("SELECT * FROM nomi_portate WHERE id='" + id + "' ORDER BY " + comanda.lingua_stampa + " ASC LIMIT 1;", function (result) {

        for (var key in result[0]) {
            console.log("RESULT0", key, result[0], result[0][key]);

            if (result[0][key] === null) {
                result[0][key] = '';
            }
        }



        form_gestione_portate = '<div class="editor_clienti">\n\
                                   <div class="col-md-12 col-xs-12">\n\
                                   <div class="col-md-9 col-xs-9"><label>Descrizione</label><input maxlength="20" style="text-transform:uppercase;font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="descrizione"  value="' + result[0][comanda.lingua_stampa] + '"></div>\n\
                                   <div class="col-md-3 col-xs-3"><label>Codice Concomitanze&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'CODICE CONCOMITANZA\', \'Codice univoco che serve ad identificare il nome della concomitanza.<br><br>I prodotti che presenteranno la portata ' + "P" + ' saranno conteggiati nel totale pizze ordinate nella stampa della comanda e in quella per i fattorini.<br></br>Inserendo invece la lettere B nella portata delle bibite, queste saranno conteggiate come le pizze nella stampa dei fattorini e nella comanda.  \')">?</a></label><input style="margin-bottom:10px;" class="form-control" type="text" name="numero"   value="' + result[0].numero + '"></div>\n\
                                   <div class="col-md-3 col-xs-3"><label>Ordinamento su Comanda&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'ORDINAMENTO STAMPA\', \'Inserendo su questo campo un numero a due cifre progressivo (es.05) sar&agrave; possibile ordinare le portate nella stampa della comanda. \')">?</a></label><input style="margin-bottom:10px;" class="form-control" type="text" name="ordinamento_stampa"   maxlength="2" value="' + result[0].ordinamento_stampa + '"></div>';

        var test1 = result[0].visu_articoli_concomitanze == "true" ? "checked" : "";

        form_gestione_portate += '<div class="col-md-3 col-xs-3" id="visualizza_art_con"><label>Visualizzazione Articoli Concomitanze&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'VISUALIZZAZIONE ARTICOLI IN CONCOMITANZE\', \'Abilitando questo settaggio della sezione a piè pagina nella concomitanza comparir&agrave; non solo la portata con la destinazione di stampa ma anche i prodotti. \')">?</a></label><input class="form-control esclusione_css" type="checkbox" name="visu_articoli_concomitanze" ' + test1 + '></div>\n\
                                  <div class="col-md-4 col-xs-4" id="attributi_font_con"><label>Attributi Font Concomitanze</label><select style="height:auto;font-size:25.5px;margin-bottom:10px;" class="form-control" type="text" name="attributi_font_concomitanze"><option value="D">Doppio</option><option value="N">Normale</option><option value="G">Grassetto</option></select></div>';

        form_gestione_portate += '<div class="col-md-8 col-xs-8" id="portate_con"><h2>Portate Concomitanti<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'VISUALIZZAZIONE ARTICOLI IN CONCOMITANZE\', \'In questa sezione premendo la' + " + " + 'si potrà scegliere che portata dello stesso ordine far vedere a pi&egrave pagina della comanda qualora ci dovesse essere la medesima sopra riportata. Esempio: Nel caso in cui andassimo ad impostare nella portata PIZZERIA la concomitanza della portata FRITTI, nella stampa della comanda qualora ci dovesse essere sia un fritto che una pizza, nella stampa della pizzeria comparir&agrave sotto la concomitanza del fritto. \')">?</a></h2></div>\n\
                                  <div class="col-md-4 col-xs-4"><button type="button" id="aggiungi_con" class="btn btn-info" onclick="aggiungi_concomitanza_settaggio()">+</button></div>\n\
                                  <div class="col-md-12" id="tendine_concomitanze_settaggio"></div>';


        form_gestione_portate += '</div>';





        form_gestione_portate += '<div class="col-md-12 col-xs-12"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_portata(\'' + result[0].id + '\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button><button class="pull-right col-xs-4 btn btn-danger" ' + comanda.evento + '="elimina_portata(\'' + result[0].id + '\');"  style="text-transform:uppercase;">ELIMINA PORTATA</button></div>\n\
                                   </div></div>';



        $('#form_gestione_portate').html(form_gestione_portate);

        $('#form_gestione_portate select[name="attributi_font_concomitanze"] option[value="' + result[0].attributi_font_concomitanze + '"]').attr("selected", true);

        if (result[0].concomitanze === "N") {
            var testo_queryYY = alasql("SELECT  Field22, Field149 FROM settaggi_profili    WHERE id = 1;");

            if (testo_queryYY[0].Field22 === "false" && testo_queryYY[0].Field149 === "false") {
                $('#form_gestione_portate input[name="visu_articoli_concomitanze"]:visible').hide();
                $('#aggiungi_con').hide();
                $('#form_gestione_portate select[name="attributi_font_concomitanze"]:visible').hide();
                $('#portate_con').hide();
                $('#visualizza_art_con').hide();
                $('#attributi_font_con').hide();


            }
        }
        mostrate_tendine(JSON.parse(result[0].concomitanze));



    });
}

function mostrate_tendine(elenco_concomitanze) {


    let id_arrr = alasql("select * from nomi_portate");
    let tendina = '';

    var testo_queryYY = alasql("SELECT  Field22, Field149 FROM settaggi_profili    WHERE id = 1;");

    if (testo_queryYY[0].Field22 === "false" && testo_queryYY[0].Field149 === "false") {
        $('#form_gestione_portate input[name="visu_articoli_concomitanze"]:visible').hide();
        $('#aggiungi_con').hide();
        $('#form_gestione_portate select[name="attributi_font_concomitanze"]:visible').hide();
        $('#portate_con').hide();
        $('#visualizza_art_con').hide();
        $('#attributi_font_con').hide();


    } else {

        for (let i = 0; i < elenco_concomitanze.length; i++) {

            tendina += '<div class="linea_concomitanza"><div class="col-md-8 col-xs-8"><select class="tendina_concomitanza form-control">';

            id_arrr.forEach((v) => {

                if (elenco_concomitanze[i] === v.numero) {
                    tendina += '<option value="' + v.numero + '" selected>' + v[comanda.lingua] + '</option>';
                } else {
                    tendina += '<option value="' + v.numero + '">' + v[comanda.lingua] + '</option>';
                }

            });
            tendina += '</select></div>'
            tendina += '<div class="col-md-4 col-xs-4"><button type="button" class="btn btn-danger" onclick="$(this).closest(\'.linea_concomitanza\').remove();">-</button></div></div>';

        }



        $("#tendine_concomitanze_settaggio").append(tendina);

    }

}


function aggiungi_concomitanza_settaggio() {

    let tendina = '<div class="linea_concomitanza"><div class="col-md-8 col-xs-8"><select class="tendina_concomitanza form-control">';

    let id_arr = alasql("select * from nomi_portate");

    id_arr.forEach((v) => {

        tendina += '<option value="' + v.numero + '">' + v[comanda.lingua] + '</option>';

    });

    tendina += '</select></div>'

    tendina += '<div class="col-md-4 col-xs-4"><button type="button" class="btn btn-danger" onclick="$(this).closest(\'.linea_concomitanza\').remove();">-</button></div></div>';


    $("#tendine_concomitanze_settaggio").append(tendina);

}

function gestisci_ATECO() {

    ATECO_controller.init();

    $('#tabella_ATECO').show();
}

function gestisci_IVA() {

    IVA_controller.init();

    $('#tabella_IVA').show();
}

function gestisci_reparti() {

    REPARTO_controller.init();

    $('#tabella_reparti').show();
}


function gestisci_gruppo_statistico(event, id) {

    if (event !== undefined && event.target !== undefined && event.target !== null) {
        $(event.target).parent().parent().find("a").removeClass('cat_accesa');
        $(event.target).addClass("cat_accesa");
    }

    $('#form_gestione_gruppi_statistici').html('');
    var form_gestione_gruppi_statistici = '';
    comanda.sincro.query("SELECT * FROM gruppi_statistici WHERE id='" + id + "' ORDER BY nome ASC LIMIT 1;", function (result) {

        for (var key in result[0]) {
            console.log("RESULT0", key, result[0], result[0][key]);
            if (result[0][key] === null) {
                result[0][key] = '';
            }
        }



        form_gestione_gruppi_statistici = '<div class="editor_clienti">\n\
                                   <div class="col-md-12 col-xs-12">\n\
                                   <div class="col-md-9 col-xs-9">\n\
<label>Nome</label>\n\
<input maxlength="20" style="text-transform:uppercase;font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="nome"  value="' + result[0].nome + '">\n\
</div>\n\
                                </div>';

        form_gestione_gruppi_statistici += '<div class="col-md-12 col-xs-12"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_gruppo_statistico(\'' + result[0].id + '\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button><button class="pull-right col-xs-4 btn btn-danger" ' + comanda.evento + '="elimina_gruppo_statistico(\'' + result[0].id + '\');"  style="text-transform:uppercase;">ELIMINA GRUPPO</button></div>\n\
                                   </div></div>';



        $('#form_gestione_gruppi_statistici').html(form_gestione_gruppi_statistici);

    });
}

function gestisci_stampante(event, id) {

    if (event !== undefined && event.target !== undefined && event.target !== null) {
        $(event.target).parent().parent().find("a").removeClass('cat_accesa');
        $(event.target).addClass("cat_accesa");
    }

    var settaggi_profili = alasql("SELECT Field413 FROM settaggi_profili  where id=" + comanda.folder_number + "  LIMIT 1;");
    var settaggi_profilo = settaggi_profili[0];

    $('#form_gestione_clienti').html('');
    var form_gestione_stampanti = '';
    comanda.sincro.query("SELECT * FROM nomi_stampanti WHERE id='" + id + "' LIMIT 1;", function (result) {

        for (var key in result[0]) {
            console.log("RESULT0", key, result[0], result[0][key]);
            if (result[0][key] === null) {
                result[0][key] = '';
            }
        }

        var tipo = '';
        //Nome(stampante),Lingua(stanza),Ip,Piccola,Intelligent,Fiscale

        var readonly = '',
            tasto_elimina = '';
        switch (result[0][comanda.lingua_stampa]) {
            case "QUITTUNG":
            case "RESCHNUNG":
            case "CONTO":
            case "KONTO":
            case "FATTURA":
            case "SCONTRINO":
                readonly = 'readonly';
                break;
            default:
                tasto_elimina += '<button class="pull-right col-md-2 col-xs-2 btn btn-danger" ' + comanda.evento + '="elimina_stampante(\'' + result[0].id + '\',\'' + tipo + '\');" style="text-transform:uppercase;">' + comanda.lang[179] + '</button>';
        }

        var doppia = result[0].doppia === "true" ? "checked" : "";
        var sds_comanda = result[0].sds_controllo === "true" ? "checked" : "";



        form_gestione_stampanti = '<div class="editor_stampanti">\n\
                                   <div class="col-md-12 col-xs-12">\n\
                                     <div class="col-md-4 col-xs-4"><label>' + comanda.lang[182] + '</label><input style="text-transform:uppercase;margin-bottom:10px;" class="form-control" type="text" name="' + comanda.lingua_stampa + '"   value="' + result[0][comanda.lingua_stampa] + '" ' + readonly + '></div>\n\
                                        \n\
                                        <div class="col-md-4 col-xs-4"><label>' + comanda.lang[183] + '</label><select style="font-size:25px;height:56px;" class="form-control nopadding" name="nome"><option>CUBO</option><option>TMT88-Vi</option><option>P20</option><option>MISURATORE FISCALE</option></select></div>\n\
                                        \n\
                                        <div class="col-md-4 col-xs-4"><label>IP</label><input style="margin-bottom:10px;" class="form-control" type="text" name="ip"  value="' + result[0].ip + '"></div>\n\
                                   \n\
                                   </div>\n\
                                    <div class="col-md-12 col-xs-12">\n\
                                        <div class="col-md-4 col-xs-4" id="ip_alternativo_sds"><label>IP della stampante alternativa</label><input style="margin-bottom:10px;" class="form-control" type="text" name="ip_alternativo"  value="' + result[0].ip_alternativo + '">\n\
                                        </div>\n\
                                        <div class="col-md-4 col-xs-4"><label>Numero di matricola (solo RT)</label><input style="margin-bottom:10px;" class="form-control" type="text" name="matricola"  value="' + result[0].matricola + '">\n\
                                    </div>\n\
                                    <div class="col-md-12 col-xs-12">\n\
                                    <div class="col-md-3 col-xs-3" style="font-size:30px;margin-top:6px">' + "SDS Controllo " + '</div><input style="height: 50px;width:50px" class=""  type="checkbox" name="sds_controllo" ' + sds_comanda + '>\n\
                                    </div>\n\
                                    <div class="col-md-4 col-xs-4" id="nome_stampa_sds" style="display:none"><label>Nome di Stampa </label><input style="margin-bottom:10px;" class="form-control" type="text"  name="nome_stampa_sds"  value="' + result[0].devid_nf + '">\n\
                                    </div>\n\  <div class="col-md-12 col-xs-12">\n\
                         <div class="col-md-3 col-xs-3" style="font-size:30px;margin-top:6px">' + comanda.lang[197] + '</div><input style="height: 50px;width:50px" class=""  type="checkbox" name="comandadoppia" ' + doppia + '>\n\
                                                    </div>\n\
                                   <div class="col-md-12 col-xs-12">';

        $(document).on(comanda.eventino, '#form_gestione_stampanti input[name="sds_controllo"]', function (event) {

            var calendario_ordinazioni = $('#form_gestione_stampanti input[name="sds_controllo"]').is(':checked');

            if (calendario_ordinazioni === true) {

                $('#nome_stampa_sds').show();
                $('#ip_alternativo_sds').hide();

            } else {
                $('#nome_stampa_sds').hide();
                $('#ip_alternativo_sds').show();
            }
        });

        if (settaggi_profilo.Field413 === "true" && result[0][comanda.lingua_stampa].indexOf("CONTO") !== -1) {
            form_gestione_stampanti += '<button class="pull-left col-md-2 col-xs-2 btn btn-info" ' + comanda.evento + '="assegna_stampante_tavoli(\'' + result[0][comanda.lingua_stampa] + '\');" style="text-transform:uppercase;">TAVOLI</button>';
        }

        form_gestione_stampanti += '<button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_stampante(\'' + result[0].id + '\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button>';

        form_gestione_stampanti += tasto_elimina;
        form_gestione_stampanti += '</div>\n\
                                   </div>';
        $('#form_gestione_stampanti').html(form_gestione_stampanti);
        $(document).ready(
            function () {
                if (sds_comanda === "checked") {
                    $('#ip_alternativo_sds').hide();
                    $('#nome_stampa_sds').show();
                    document.getElementById("nome_stampa_sds").style.display = '';
                }
            });
        $('[name="nome"] option:containsExact("' + result[0].nome + '")').attr("selected", true);
    });

}




//EVENT LISTENER CONFLITTI (GESTISCI SETTAGGI)

$(document).on('keyup', '#form_settaggi input[name="scritta_consegna_domicilio"]', function (event) {
    var scritta_consegna_domicilio = $('#form_settaggi input[name="scritta_consegna_domicilio"]').val();

    if (scritta_consegna_domicilio.trim() !== "") {

        $('#form_settaggi input[name="qrcode_navigatore"]').prop('checked', false);
        $('#form_settaggi input[name="dati_cliente"]').prop('checked', false);


    }
});

$(document).on(comanda.eventino, '#form_settaggi input[name="qrcode_navigatore"]', function (event) {

    var qrcode_navigatore = $('#form_settaggi input[name="qrcode_navigatore"]').is(':checked');

    if (qrcode_navigatore === true) {

        $('#form_settaggi input[name="scritta_consegna_domicilio"]').val("");

    }
});

$(document).on(comanda.eventino, '#form_settaggi input[name="dati_cliente"]', function (event) {

    var dati_cliente = $('#form_settaggi input[name="dati_cliente"]').is(':checked');

    if (dati_cliente === true) {

        $('#form_settaggi input[name="scritta_consegna_domicilio"]').val("");

    }
});


$(document).on(comanda.eventino, '#form_settaggi input[name="calendario_ordinazioni"].calendario_ordinazioni', function (event) {

    var calendario_ordinazioni = $('#form_settaggi input[name="calendario_ordinazioni"].calendario_ordinazioni').is(':checked');

    if (calendario_ordinazioni === true) {

        $('#form_settaggi .sottogruppo_calendario_ordinazioni').show();

    } else {
        $('#form_settaggi .sottogruppo_calendario_ordinazioni').hide();
    }
});

$(document).on('change', '#form_settaggi input[name="prezzo"].settaggi_comanda', function (event) {
    var fontarticolo = $('#form_settaggi select[name="font_articolo"].settaggi_comanda').val();
    var fontvarianti = $('#form_settaggi select[name="font_varianti"].settaggi_comanda').val();
    var prezzo = $('#form_settaggi input[name="prezzo"].settaggi_comanda').is(':checked');
    console.log("listener settaggi", fontarticolo, fontvarianti, prezzo);
    if (prezzo === true && (fontarticolo === 'D' || fontvarianti === 'D')) {
        alert("Puoi scegliere se mettere O l'articolo doppio/variante doppia O il prezzo sugli articoli.");
        $('#form_settaggi input[name="prezzo"].settaggi_comanda').prop('checked', false);
    }
});

$(document).on('change', '#form_settaggi select[name="font_articolo"].settaggi_comanda', function (event) {
    var fontarticolo = $('#form_settaggi select[name="font_articolo"].settaggi_comanda').val();
    var prezzo = $('#form_settaggi input[name="prezzo"].settaggi_comanda').is(':checked');
    console.log("listener settaggi", fontarticolo, prezzo);
    if (fontarticolo === 'D' && prezzo === true) {
        alert("Puoi scegliere se mettere O l'articolo doppio O il prezzo sugli articoli.");
        $('#form_settaggi input[name="prezzo"].settaggi_comanda').prop('checked', false);
    }
});
$(document).on('change', '#form_settaggi select[name="font_varianti"].settaggi_comanda', function (event) {
    var fontvarianti = $('#form_settaggi select[name="font_varianti"].settaggi_comanda').val();
    var prezzo = $('#form_settaggi input[name="prezzo"].settaggi_comanda').is(':checked');
    console.log("listener settaggi", fontvarianti, prezzo);
    if (fontvarianti === 'D' && prezzo === true) {
        alert("Puoi scegliere se mettere O la variante doppia O il prezzo sugli articoli.");
        $('#form_settaggi input[name="prezzo"].settaggi_comanda').prop('checked', false);
    }
});
$(document).on('change', '#form_settaggi input[name="prezzo"].settaggi_comanda', function (event) {
    var fontarticolo = $('#form_settaggi select[name="font_articolo"].settaggi_comanda').val();
    var fontvarianti = $('#form_settaggi select[name="font_varianti"].settaggi_comanda').val();
    var prezzo = $('#form_settaggi input[name="prezzo"].settaggi_comanda').is(':checked');
    console.log("listener settaggi", fontarticolo, fontvarianti, prezzo);
    if (prezzo === true && (fontarticolo === 'D' || fontvarianti === 'D')) {
        alert("Puoi scegliere se mettere O l'articolo doppio/variante doppia O il prezzo sugli articoli.");
        $('#form_settaggi input[name="prezzo"].settaggi_comanda').prop('checked', false);
    }
});



$(document).on(comanda.eventino, '#form_settaggi input[name="percentuali"].settaggi_percentuali', function (event) {

    var divisionegruppi = $('#form_settaggi input[name="divisionegruppi"].settaggi_percentuali').is(':checked');
    var divisionecategorie = $('#form_settaggi input[name="divisionecategorie"].settaggi_percentuali').is(':checked');
    var divisionedestinazione = $('#form_settaggi input[name="divisionedestinazione"].settaggi_percentuali').is(':checked');
    var percentuali = $('#form_settaggi input[name="percentuali"].settaggi_percentuali').is(':checked');
    if (percentuali === true) {
        if (percentuali === true && divisionedestinazione === false && divisionecategorie === false && divisionegruppi === false) {
            alert("Puoi visualizzare le percentuali solo se c'e l'ordinamento per categoria o per destinazione o per gruppo statistico.");
            $('#form_settaggi input[name="percentuali"].settaggi_percentuali').prop('checked', false);
        }
    }
});
$(document).on(comanda.eventino, '#form_settaggi input[name="divisionecategorie"].settaggi_percentuali', function (event) {
    var divisionegruppi = $('#form_settaggi input[name="divisionegruppi"].settaggi_percentuali').is(':checked');

    var divisionecategorie = $('#form_settaggi input[name="divisionecategorie"].settaggi_percentuali').is(':checked');
    var divisionedestinazione = $('#form_settaggi input[name="divisionedestinazione"].settaggi_percentuali').is(':checked');
    var percentuali = $('#form_settaggi input[name="percentuali"].settaggi_percentuali').is(':checked');
    if (percentuali === true) {
        if (percentuali === true && divisionedestinazione === false && divisionecategorie === false && divisionegruppi === false) {
            alert("Puoi visualizzare le percentuali solo se c'e l'ordinamento per categoria o per destinazione o per gruppo statistico.");
            $('#form_settaggi input[name="percentuali"].settaggi_percentuali').prop('checked', false);
        }
    }
});
$(document).on(comanda.eventino, '#form_settaggi input[name="divisionedestinazione"].settaggi_percentuali', function (event) {
    var divisionegruppi = $('#form_settaggi input[name="divisionegruppi"].settaggi_percentuali').is(':checked');

    var divisionecategorie = $('#form_settaggi input[name="divisionecategorie"].settaggi_percentuali').is(':checked');
    var divisionedestinazione = $('#form_settaggi input[name="divisionedestinazione"].settaggi_percentuali').is(':checked');
    var percentuali = $('#form_settaggi input[name="percentuali"].settaggi_percentuali').is(':checked');
    if (percentuali === true) {
        if (percentuali === true && divisionedestinazione === false && divisionecategorie === false && divisionegruppi === false) {
            alert("Puoi visualizzare le percentuali solo se c'e l'ordinamento per categoria o per destinazione o per gruppo statistico.");
            $('#form_settaggi input[name="percentuali"].settaggi_percentuali').prop('checked', false);
        }
    }
});


$(document).on(comanda.eventino, '#form_settaggi input[name="divisionecategorie"].settaggi_percentuali', function (event) {

    var check = $('#form_settaggi input[name="divisionecategorie"].settaggi_percentuali').is(':checked');

    if (check === true) {

        $('#form_settaggi input[name="divisionedestinazione"].settaggi_percentuali').prop("checked", false);
        $('#form_settaggi input[name="divisionegruppi"].settaggi_percentuali').prop("checked", false);

    }
});

$(document).on(comanda.eventino, '#form_settaggi input[name="divisionedestinazione"].settaggi_percentuali', function (event) {

    var check = $('#form_settaggi input[name="divisionedestinazione"].settaggi_percentuali').is(':checked');

    if (check === true) {

        $('#form_settaggi input[name="divisionecategorie"].settaggi_percentuali').prop("checked", false);
        $('#form_settaggi input[name="divisionegruppi"].settaggi_percentuali').prop("checked", false);

    }
});

$(document).on(comanda.eventino, '#form_settaggi input[name="divisionegruppi"].settaggi_percentuali', function (event) {

    var check = $('#form_settaggi input[name="divisionegruppi"].settaggi_percentuali').is(':checked');

    if (check === true) {

        $('#form_settaggi input[name="divisionedestinazione"].settaggi_percentuali').prop("checked", false);
        $('#form_settaggi input[name="divisionecategorie"].settaggi_percentuali').prop("checked", false);

    }
});



function check_settaggi_consegne(posizione_attuale) {

    var array_numeri = [1, 2, 3, 4];

    array_numeri.splice(posizione_attuale - 1, 1);

    var controllo = false;

    array_numeri.forEach(function (numero_riga) {
        $(".riga_consegna_" + numero_riga + " input").each(function (index, campo) {
            if ($(campo).val().length > 0) {
                controllo = numero_riga;
            }

        });
    });

    /* MODIFICA 21/10/2020 CONSEGNE MISTE */
    /*if (controllo !== false) {
     bootbox.alert("Per compilare questi campi, cancellare la tipologia di consegna numero " + controllo);
     }*/
}


function gestisci_settaggio(event, nome_settaggio) {

    comanda.settaggio_selezionato = nome_settaggio;

    if (event !== undefined && event.target !== undefined && event.target !== null && event.target.type !== 'select-one') {
        $(event.target).parent().parent().find("a").removeClass('cat_accesa');
        $(event.target).addClass("cat_accesa");
    }

    var livello_superiore = $('#tendina_gestione_settaggi').val();
    $('#form_settaggi').html('');
    var form_settaggi = '';
    if (comanda.compatibile_xml === true) {
        $(document).on('change', '#form_settaggi input[name=\'tasto_fastconto\']', function (event) {
            if ($(this).is('checked') === false) {

                bootbox.prompt({
                    size: "small",
                    title: comanda.lang[106],
                    inputType: "password",
                    callback: function (result) {

                        if (CryptoJS.MD5(result).toString() === "ae5873da1b195af6feb68318ff42c1be") {
                            $('#form_settaggi input[name=\'tasto_fastconto\']').prop("checked", true);
                        } else {
                            $('#form_settaggi input[name=\'tasto_fastconto\']').prop("checked", false);
                        }

                    }
                });
            } else {
                $('#form_settaggi input[name=\'tasto_fastconto\']').prop("checked", false);
            }
        });

        $(document).on(comanda.eventino, '#form_settaggi input[name=\'modifica_prodotto\']', function () {
            $('#form_settaggi input[name=\'modifica_prodotto_descrizione\']').prop("checked", false);
            $('#form_settaggi input[name=\'modifica_prodotto_quantita\']').prop("checked", false);
            $('#form_settaggi input[name=\'modifica_prodotto_prezzo\']').prop("checked", false);
            $('#form_settaggi input[name=\'modifica_prodotto_raddoppiaprezzo\']').prop("checked", false);
            $('#form_settaggi input[name=\'modifica_prodotto_destinazione_stampa\']').prop("checked", false);
            $('#form_settaggi input[name=\'modifica_prodotto_portata\']').prop("checked", false);
            $('#form_settaggi input[name=\'modifica_prodotto_clona\']').prop("checked", false);
            $('#form_settaggi input[name=\'modifica_prodotto_servito\']').prop("checked", false);
        });

        $(document).on(comanda.eventino, '#form_settaggi input[name=\'modifica_prodotto_raddoppiaprezzo\']', function () {
            $('#form_settaggi input[name=\'modifica_prodotto\']').prop("checked", true);
        });

        $(document).on(comanda.eventino, '#form_settaggi input[name=\'modifica_prodotto_descrizione\']', function () {
            $('#form_settaggi input[name=\'modifica_prodotto\']').prop("checked", true);
        });
        $(document).on(comanda.eventino, '#form_settaggi input[name=\'modifica_prodotto_quantita\']', function () {
            $('#form_settaggi input[name=\'modifica_prodotto\']').prop("checked", true);
        });
        $(document).on(comanda.eventino, '#form_settaggi input[name=\'modifica_prodotto_prezzo\']', function () {
            $('#form_settaggi input[name=\'modifica_prodotto\']').prop("checked", true);
        });
        $(document).on(comanda.eventino, '#form_settaggi input[name=\'modifica_prodotto_destinazione_stampa\']', function () {
            $('#form_settaggi input[name=\'modifica_prodotto\']').prop("checked", true);
        });
        $(document).on(comanda.eventino, '#form_settaggi input[name=\'modifica_prodotto_portata\']', function () {
            $('#form_settaggi input[name=\'modifica_prodotto\']').prop("checked", true);
        });
        $(document).on(comanda.eventino, '#form_settaggi input[name=\'modifica_prodotto_clona\']', function () {
            $('#form_settaggi input[name=\'modifica_prodotto\']').prop("checked", true);
        });
        $(document).on(comanda.eventino, '#form_settaggi input[name=\'modifica_prodotto_servito\']', function () {
            $('#form_settaggi input[name=\'modifica_prodotto\']').prop("checked", true);
        });


        switch (livello_superiore) {
            case "generici":
                switch (nome_settaggio) {
                    case "PRINCIPALI":

                        comanda.sincro.query("SELECT * FROM settaggi_ibrido WHERE id='" + comanda.nome_servizio + "' LIMIT 1;", function (result) {

                            for (var key in result[0]) {
                                console.log("RESULT0", key, result[0], result[0][key]);
                                if (result[0][key] === null) {
                                    result[0][key] = '';
                                }
                            }

                            comanda.sincro.query("SELECT * FROM impostazioni_fiscali where id=" + comanda.folder_number + "  LIMIT 1;", function (resultifsc) {

                                for (var key in resultifsc[0]) {
                                    console.log("RESULT0", key, resultifsc[0], resultifsc[0][key]);
                                    if (resultifsc[0][key] === null) {
                                        resultifsc[0][key] = '';
                                    }
                                }

                                var font_size = "30px";
                                if (comanda.iphone === true) {
                                    font_size = "20px";
                                }

                                form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = result[0].comandapconto == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">' + comanda.lang[196] + '</div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="comandapconto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                var test1 = result[0].filtro_lettera_bloccato == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Filtro Lettera Bloccato</div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="filtro_lettera_bloccato" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                var test1 = result[0].storicizzazioneauto == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Storicizzazione Automatica</div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="storicizzazioneauto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                var test1 = result[0].incassoauto == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Incasso Auto</div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="incassoauto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                var test1 = result[0].modalitasagra == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Configurazione Sagra</div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="modalitasagra" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = result[0].blocco_tavolo_cameriere == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Blocco Tavolo Cameriere</div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="blocco_tavolo_cameriere" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                var test1 = result[0].cancellazione_articolo == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Storno Articolo</div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="cancellazione_articolo" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                form_settaggi += '<div class="col-md-7 col-xs-7" style="font-size:' + font_size + '">Password Storno</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="pwd_canc_articolo"  style="font-size:25px" value="' + result[0].pwd_canc_articolo + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                                form_settaggi += '<div class="col-md-7 col-xs-7" style="font-size:' + font_size + '">Listino</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="listino_palmari"  style="font-size:25px" value="' + result[0].listino_palmari + '" placeholder="1/2/3/4"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';


                                form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12 text-center" style="margin-top: 0px;"><button class="btn btn-success" ' + comanda.evento + '="salva_settaggi(\'PRINCIPALI\');" style="font-size: 3vh;text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                                $('#form_settaggi').html(form_settaggi);
                            });
                        });
                        break;

                    case "LAYOUT_VIDEO":
                        comanda.sincro.query("SELECT * FROM settaggi_ibrido WHERE id='" + comanda.nome_servizio + "' LIMIT 1;", function (result) {

                            for (var key in result[0]) {
                                console.log("RESULT0", key, result[0], result[0][key]);
                                if (result[0][key] === null) {
                                    result[0][key] = '';
                                }
                            }

                            comanda.sincro.query("SELECT * FROM impostazioni_fiscali  where id=" + comanda.folder_number + "  LIMIT 1;", function (resultifsc) {

                                for (var key in resultifsc[0]) {
                                    console.log("RESULT0", key, resultifsc[0], resultifsc[0][key]);
                                    if (resultifsc[0][key] === null) {
                                        resultifsc[0][key] = '';
                                    }
                                }

                                var font_size = "30px";
                                if (comanda.iphone === true) {
                                    font_size = "20px";
                                }

                                //&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'TITOLO\', \'Scritta\')">?</a>

                                var test1 = result[0].tasto_tavolo_esce == "S" ? "checked" : "";
                                form_settaggi = '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Tasto Tavolo Esce&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Tasto Tavolo Esce\', \'Dentro la schermata ordine, cliccando il numero del tavolo, posizionato in alto a sinistra, torna alla mappa dei tavoli.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="tasto_tavolo_esce" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = result[0].ultime_battiture_grandi == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Anteprima Articoli Grande&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Anteprima Articoli Grande\', \'Aumenta la grandezza dell anteprima del carrello dei prodotti appena battuti, e diminuisce la grandezza dei tasti di ordinazione.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="ultime_battiture_grandi" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = result[0].categorie_fullscreen == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Categorie FullScreen&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Categorie Fullscreen\', \'Ingrandisce la sezione delle categorie sulla schermata ordine, partendo completamente dall alto.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="categorie_fullscreen" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = result[0].layout_destri == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Categorie a Sinistra&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Categorie a Sinistra\', \'Posiziona la sezione delle categorie a sinistra anzich&egrave; a destra.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="layout_destri" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                form_settaggi += '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12 nopadding">';

                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Grandezza Font Articoli&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Grandezza Font Articolo\', \'Modifica la grandezza del font degli articoli.\')">?</a></div><div class="col-md-3 col-xs-3 nopadding"><input maxlength="42" class="form-control" type="number" name="grandezza_font_articoli"  style="font-size:25px" value="' + resultifsc[0].grandezza_font_articoli + '"  placeholder="3.7"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Grandezza Font Categorie&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Grandezza Font Categorie\', \'Modifica la grandezza del font delle categorie.\')">?</a></div><div class="col-md-3 col-xs-3 nopadding"><input maxlength="42" class="form-control" type="number" name="grandezza_font_categorie"  style="font-size:25px" value="' + resultifsc[0].grandezza_font_categorie + '" placeholder="3.5"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Grandezza Font Tavoli&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Grandezza Font Tavoli\', \'Modifica la grandezza del font dei tavoli.\')">?</a></div><div class="col-md-3 col-xs-3 nopadding"><input maxlength="42" class="form-control" type="number" name="grandezza_font_tavoli"  style="font-size:25px" value="' + resultifsc[0].grandezza_font_tavoli + '" placeholder="8"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                                var test1 = result[0].tasto_fastconto == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">fast.konto&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Fast Konto\', \'Abilita un bottone blu alla sinistra del totale del tavolo, con il simbolo K. Serve per mostrare il conto del tavolo ai clienti, in formato digitale. Per uscire da questa modalit&agrave; cliccare tre volte consecutive sul vostro logo in alto.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="tasto_fastconto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = resultifsc[0].visualizzazione_coperti == "1" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Coperti&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Coperti\', \'Abilita il Tasto Coperti, con il simbolo della forchetta e coltello, in alto.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="visualizzazione_coperti" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = resultifsc[0].coperti_obbligatorio == "1" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Avviso Mancati Coperti&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Avviso Mancati Coperti\', \'Lanciando lo SCONTRINO senza aver inserito i coperti, avvisa il cameriere della mancanza.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="avviso_mancati_coperti" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = resultifsc[0].avviso_coperti_comanda == "1" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="padding-left:6vh;font-size:' + font_size + '">su Comanda&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Avviso Mancati Coperti su Comanda\', \'Lanciando la COMANDA senza aver inserito i coperti, avvisa il cameriere della mancanza.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="avviso_coperti_comanda" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = result[0].slide_qta_articolo == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Slide Qta Articolo:&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Slide Quantit&agrave; Articolo\', \'Aumenta o diminuisce la quantit&agrave; dell articolo sul carrello qualora si effettuasse una slide con il dito verso destra o sinistra.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="slide_qta_articolo" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = result[0].modifica_portata_veloce == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Modifica Portata Veloce:&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'MODIFICA PORTATA VELOCE\', \'Spuntando questo campo, selezionando la descrizione dell articolo nel carrello, si aprir&agrave; in automatico la lista delle portate, per dare la possibilit&agrave; al cameriere di cambiarla pi&ugrave; velocemente. Per quando riguarda il popup della modifica del prodotto, per aprirlo baster&agrave; premere sul prezzo dell articolo in carrello.<br><br>ATTENZIONE: questo settaggio non pu&ograve; essere abilitato assieme a AZZERA PREZZO VELOCE.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="modifica_portata_veloce" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = result[0].azzera_prezzo_veloce == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Azzera Prezzo Veloce:&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'MODIFICA PREZZO VELOCE\', \'Spuntando questo campo, facendo doppio click sul prezzo dell articolo presente nel carrello, il prezzo verr&agrave; azzerato. Battendo invece sulla descrizione si aprir&agrave; il popup di modifica prodotto. Questo vale anche per le varianti. <br><br>ATTENZIONE: questo settaggio non pu&ograve; essere abilitato assieme a CAMBIO PORTATA VELOCE.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="azzera_prezzo_veloce" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = result[0].modifica_prodotto == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Modifica Prodotto:&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Modifica Prodotto\', \'Permette di abilitare e disabilitare le varie modifiche inerenti ad un prodotto gi&agrave; inserito nell ordine.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="modifica_prodotto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';



                                var test1 = result[0].modifica_prodotto_descrizione == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="padding-left:6vh;font-size:' + font_size + '">Modifica Descrizione&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Modifica Descrizione\', \'Permette di modificare la descrizione del prodotto prima di lanciarlo in comanda.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="modifica_prodotto_descrizione" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = result[0].modifica_prodotto_quantita == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="padding-left:6vh;font-size:' + font_size + '">Modifica Quantita&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Modifica Quantit&agrave;\', \'Permette di modificare la quantit&agrave; del prodotto prima di lanciarlo in comanda.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="modifica_prodotto_quantita" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';



                                var test1 = result[0].modifica_prodotto_prezzo == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="padding-left:6vh;font-size:' + font_size + '">Modifica Prezzo&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Modifica Prezzo\', \'Permette di modificare il prezzo del prodotto prima di lanciarlo in comanda.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="modifica_prodotto_prezzo" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = result[0].modifica_prodotto_raddoppiaprezzo == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="padding-left:6vh;font-size:' + font_size + '">Tasto Raddoppia Prezzo&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Tasto Raddoppia Prezzo\', \'Aggiunge un tasto per raddoppiare il prezzo del prodotto, prima di lanciarlo in comanda. Alla descrizione del prodotto verr&agrave; aggiunta in automatico la parola DOPPIO/A.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="modifica_prodotto_raddoppiaprezzo" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = result[0].modifica_prodotto_destinazione_stampa == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="padding-left:6vh;font-size:' + font_size + '">Modifica Destinazione Stampa&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Modifica Destinazione Stampa\', \'Permette di modificare la destinazione di stampa del prodotto prima di lanciarlo in comanda.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="modifica_prodotto_destinazione_stampa" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = result[0].modifica_prodotto_portata == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="padding-left:6vh;font-size:' + font_size + '">Modifica Portata&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Modifica Portata\', \'Permette di modificare la portata del prodotto prima di lanciarlo in comanda.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="modifica_prodotto_portata" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = result[0].modifica_prodotto_clona == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="padding-left:6vh;font-size:' + font_size + '">Clona:&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Clona\', \'Aggiunge il tasto Clona, che serve a duplicare un prodotto, eventualmente con varianti legate.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="modifica_prodotto_clona" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = resultifsc[0].clona_diretto == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="padding-left:8vh;font-size:' + font_size + '">Clona Diretto&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'CLONAZIONE DIRETTA\', \'Spuntando questo campo la clonazione dell articolo sar&agrave; diretta, altrimenti chieder&agrave; la quantit&agrave;\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="clona_diretto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = result[0].modifica_prodotto_servito == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="padding-left:6vh;font-size:' + font_size + '">Servito&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Servito\', \'Aggiunge il tasto servito, che permette di rendere gi&agrave; lanciato in comanda, ma senza lanciarlo fisicamente, il prodotto\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="modifica_prodotto_servito" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';



                                var test1 = result[0].modifica_prodotto == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Ricerca Articolo:&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Ricerca Articolo\', \'Sezione dove si pu&ograve; parametrizzare la ricerca degli articoli con la tastiera.\')">?</a></div><div class="col-md-3 col-xs-3"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = resultifsc[0].tastiera_ricerca_partenza_numerica == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="padding-left:6vh;font-size:' + font_size + '">Parti da Ricerca Numerica&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Parti da Ricerca Numerica\', \'Cliccando sulla tastiera di ricerca grigia, in basso a destra, si aprir&agrave; il tastierino numerico, prima del tastierino alfabetico.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="tastiera_ricerca_partenza_numerica" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';



                                var test1 = resultifsc[0].ricerca_alfabetica == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="display:none;padding-left:6vh;font-size:' + font_size + '">Alfabetico&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Alfabetico\', \'Scritta\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" style="display:none;" type="checkbox" name="ricerca_alfabetica" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = resultifsc[0].ricerca_tastiera == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="padding-left:6vh;font-size:' + font_size + '">Con tastierino:&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Con Tastierino\', \'Settaggi per configurare il tastierino numerico di ricerca articoli.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="ricerca_tastiera" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';


                                var test1 = resultifsc[0].tastiera_ricerca_filtro_plu == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="padding-left:12vh;font-size:' + font_size + '">Filtra Solo PLU&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Filtra Solo PLU\', \'Utilizzando il tastierino numerico, andr&agrave; a filtrare solo i PLU o Codici posti tra parentesi tonda alla destra del nome del prodotto. Attenzione: il filtro inizia dal primo numero del PLU all ultimo, e non in mezzo. es. se premo il numero 2 filtrera articolo (2), (215), ma non il (125)\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="tastiera_ricerca_filtro_plu" ' + test1 + '></div>\n\
                                                                        </div>\n\
                                                                        \n\
                                                                        <div class="col-md-12 col-xs-12">';

                                var test1 = resultifsc[0].tastiera_ricerca_tasto_verde_batte_articolo == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="padding-left:12vh;font-size:' + font_size + '">Spunta Verde Inserisce Articolo&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Tasto Verde Batte Articolo\', \'Cliccando sul tastierino numerico la V verde in basso a destra, qualora sia inserito un codice PLU esistente e intero, andr&agrave; ad inserirlo nel carrello in automatico.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="tastiera_ricerca_tasto_verde_batte_articolo" ' + test1 + '></div>\n\
                                                                        </div>\n\
                                                                        \n\
                                                                        <div class="col-md-12 col-xs-12">';

                                if (test1 === "checked") {
                                    var test1 = resultifsc[0].filtro_ricerca_generico != "N" ? "checked" : "";
                                    var test2 = resultifsc[0].filtro_ricerca_generico == "N" ? "checked" : "";
                                }
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="padding-left:12vh;font-size:' + font_size + '">Su tutte le categorie&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Su Tutte le Categorie\', \'Utilizzando il tastierino di ricerca, cerca tra i prodotti di tutte le categorie.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="filtro_ricerca_generico" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';


                                form_settaggi += '<div class="col-md-9 col-xs-9" style="padding-left:12vh;font-size:' + font_size + '">Su una categoria&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Su Una Categoria\', \'Utilizzando il tastierino di ricerca, cerca tra i prodotti solo nella categoria selezionata.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="filtro_ricerca_categorie" ' + test2 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = result[0].cerca_prime_lettere == "S" ? "checked" : "";

                                form_settaggi += '<div class="col-md-9 col-xs-9" style="padding-left:12vh;font-size:' + font_size + '">Cerca dalla prima lettera&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Cerca Prime Lettere\', \'Il tastierino di ricerca filtrer&agrave; il nome del prodotto partendo dalla prima lettera. es. scrivendo COC trover&ograve; COCA LIGHT. Disabilitando questo settaggio invece la ricerca verr&agrave; effettuata in mezzo al nome del prodotto. es. scrivendo LIG trover&ograve; COCA LIGHT.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="cerca_prime_lettere" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = result[0].mantieni_lettera == "S" ? "checked" : "";

                                form_settaggi += '<div class="col-md-9 col-xs-9" style="padding-left:12vh;font-size:' + font_size + '">Mantieni lettera Filtrata&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Mantieni Lettera Filtrata\', \'Dopo aver battuto il prodotto, il tastierino rester&agrave; aperto e il filtro di ricerca rester&agrave; attivo.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="mantieni_lettera" ' + test1 + ' ></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = result[0].varianti_su_tastiera == "S" ? "checked" : "";


                                form_settaggi += '<div class="col-md-9 col-xs-9" style="padding-left:12vh;font-size:' + font_size + '">Varianti Su Tastiera&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Varianti Su Tastiera\', \'Inserisce i bottoni di aggiunta e riduzione sul tastierino di ricerca.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="varianti_su_tastiera" ' + test1 + ' ></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';



                                var test1 = result[0].tastiera_scomparsa_auto == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Nascondi Tastiera Ricerca&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Nascondi Tastiera Ricerca\', \'Dopo aver selezionato il prodotto, la tastiera si chiuder&agrave; in automatico.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="tastiera_scomparsa_auto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = result[0].chiusura_tastiera_dopo_prima_lettera == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Nascondi Tastiera Dopo Prima Lettera&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Nascondi Tastiera Dopo Prima Lettera\', \'Dopo aver selezionato una lettera per ricercare il prodotto, la tastiera si chiuder&agrave; in automatico, filtrando i prodotti.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="chiusura_tastiera_dopo_prima_lettera" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = resultifsc[0].tasto_visu_varianti == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Abilita Varianti - / +&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Abilita Varianti - +\', \'Serve a visualizzare i bottoni delle varianti di aggiunta e riduzione, nella schermata dell ordine.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="tasto_visu_varianti" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = resultifsc[0].tasto_visu_varianti_menopiu == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="padding-left:6vh;font-size:' + font_size + '">- / +&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'- +\', \'I bottoni delle varianti saranno prima - e poi +.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="tasto_visu_varianti_menopiu" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = resultifsc[0].tasto_visu_varianti_piumeno == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="padding-left:6vh;font-size:' + font_size + '">+ / -&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'+ -\', \'I bottoni delle varianti saranno prima + e poi -.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="tasto_visu_varianti_piumeno" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';


                                var test1 = result[0].tutte_le_varianti == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Tutte le Varianti&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'TUTTE LE VARIANTI\', \'Questo settaggio, se abilitato, permetterà di poter aggiungere nell articolo principale anche le varianti degli altri articoli, premendo il tasto TUTTE che verr&agrave; visualizzato all interno delle categorie di varianti.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="tutte_le_varianti" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';


                                var test1 = result[0].tasto_pococotto == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Poco Cotto&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Poco Cotto\', \'Abilita il bottone P per inserimento delle varianti POCO.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="tasto_pococotto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = result[0].tasto_finecottura == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Fine Cottura&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Fine Cottura\', \'Abilita il bottone F per inserimento delle varianti FINE COTTURA.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="tasto_finecottura" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = result[0].ricerca_su_variante == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Tastiera Auto Su Variante&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Tastiera Auto su Variante\', \'Quando si clicca un qualsiasi bottone di variante, si apre in automatico la tastiera di ricerca.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="ricerca_su_variante" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                if (comanda.nome_servizio.indexOf("EIS") !== -1) {
                                    var test1 = result[0].tasto_gusti_gelato == "S" ? "checked" : "";
                                    form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Gusti Gelato&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Gusti Gelato\', \'Abilita il bottone G per inserimento delle varianti GUSTO, per le gelaterie.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="tasto_gusti_gelato" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                }

                                var test1 = result[0].p_libero_abilitato == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Prodotto Libero&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Prodotto Libero\', \'Il primo prodotto che verr&agrave; visualizzato all interno della categoria sar&agrave; il simbolo +. Questo verr&agrave; riproposto in tutte le categorie, e permette di inserire un prodotto libero, mantenedo le impostazioni di stampa e di portata della categoria in cui si trova.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="p_libero_abilitato" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = result[0].variante_libera == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Variante Libera&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Variante Libera\', \'Il primo prodotto che verr&agrave; visualizzato all interno della categoria di varianti, sar&agrave; il simbolo +. Questo verr&agrave; riproposto in tutte le categorie di varianti, e permette di inserire una variante libera.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="variante_libera" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = resultifsc[0].multiquantita == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Multiquantita&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'MULTIQUANTITA\', \'Se spuntato, riunisce le quantit&agrave; degli articoli con variante. Es. 5 CAFFE + PANNA. Se non abilitato l ordine sarebbe CAFFE + PANNA scritto per 5 volte.<br><br>ATTENZIONE: Questo settaggio non &egrave; compatibile con il conto separato abilitato.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="multiquantita" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = result[0].tasto_contosep == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Conto Separato&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'CONTO SEPARATO\', \'Se spuntato permette di incassare il conto separatamente. <br><br>ATTENZIONE: Questo settaggio non &egrave; compatibile con il multiquantit&agrave; abilitato.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="tasto_contosep" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                if (comanda.lingua_stampa === 'italiano') {
                                    var test1 = result[0].tasto_incasso == "S" ? "checked" : "";
                                    form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Storicizza&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Storicizza\', \'Abilita il tasto Storicizza per le versioni di test in ufficio.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="tasto_incasso" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                }

                                var test1 = result[0].tasto_scontrino == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Scontrino&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Scontrino\', \'Aggiunge il tasto nella sezione dei pagamenti.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="tasto_scontrino" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';


                                var test1 = result[0].tasto_fattura == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Fattura&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Fattura\', \'Aggiunge il tasto fattura nella sezione dei pagamenti.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="tasto_fattura" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                form_settaggi += '<div class="col-md-12 col-xs-12" style="font-size:' + font_size + '">Comanda + Conto&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Comanda + Conto\', \'Aggiunge il tasto Comanda + Conto.\')">?</a></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';



                                var test1 = result[0].tasto_comanda_conto == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="padding-left:6vh;font-size:' + font_size + '">tra i tasti di incasso&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Tra i Tasti di Incasso\', \'Il tasto Comanda+Conto verr&agrave; aggiunto alla sezione pagamenti.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="tasto_comanda_conto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = result[0].tasto_comanda_conto_su_comanda == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="padding-left:6vh;font-size:' + font_size + '">a fianco a Comanda&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'A Fianco a Comanda\', \'Il tasto Comanda+Conto verr&agrave; aggiunto in basso alla schermata dell ordine, a fianco al tasto Comanda.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="tasto_comanda_conto_su_comanda" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                if (comanda.lingua_stampa === 'deutsch') {
                                    var test1 = result[0].tasto_gutshein == "S" ? "checked" : "";
                                    form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Gutshein&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Gutshein\', \'Solo per i clienti in Germania aggiunge il bottone Gutshein tra i tasti di pagamento.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="tasto_gutshein" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                }

                                var test1 = result[0].tasto_conto == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Conto&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Conto\', \'Aggiunge il tasto Conto alla sezione pagamenti.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="tasto_conto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = result[0].tasto_sconto == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Sconto&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Sconto\', \'Aggiunge il tasto Sconto alla sezione pagamenti.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="tasto_sconto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                if (comanda.lingua_stampa === 'italiano') {
                                    var test1 = result[0].tasto_incasso_manuale == "S" ? "checked" : "";
                                    form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Incasso&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'TASTO INCASSO\', \'Il tasto incasso serve nell eventualit&agrave; che il cameriere incassi parzialmente il tavolo durante la serata. Il totale incassato si potr&agrave; vedere sia da server che nel cellulare vicino al totale in alto a destra.<br><br>Quando il tavolo sar&agrave; incassato completamente diventer&agrave; Blu Scuro.<br><br>Ricordardi di emettere lo scontrino fiscale o la fattura una volta incassato tutto il tavolo.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="tasto_incasso_manuale" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                }
                                if (comanda.lingua_stampa === 'deutsch') {
                                    var test1 = result[0].tasto_carte_rechnung == "S" ? "checked" : "";
                                    form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Tasto Carte&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'TASTO CARTE\', \'Il tasto incasso serve nell eventualit&agrave; che il cameriere incassi parzialmente il tavolo durante la serata. Il totale incassato si potr&agrave; vedere sia da server che nel cellulare vicino al totale in alto a destra.<br><br>Quando il tavolo sar&agrave; incassato completamente diventer&agrave; Blu Scuro.<br><br>Ricordardi di emettere lo scontrino fiscale o la fattura una volta incassato tutto il tavolo.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="tasto_carte_rechnung" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                }

                                var test1 = result[0].prevenzione_tocco_accidentale_comanda == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Prevenzione Comanda Accidentale&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'PREVENZIONE COMANDA ACCIDENTALE\', \'In alcuni terminali pi&ugrave; lenti pu&ograve; succedere che premendo il tasto di chiusura della tastiera di ricerca ABC, questa non si chiuda, dando modo cos&igrave; al cameriere di premerlo pi&ugrave; volte e lanciare per sbaglio la comanda.<br><br>Abilitando questo settaggio, le estremit&agrave; del tasto comanda dx e sx sono disabilitate.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="prevenzione_tocco_accidentale_comanda" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';



                                if (comanda.lingua_stampa === 'deutsch') {
                                    var test1 = result[0].tasto_incasso == "S" ? "checked" : "";
                                    /*form_settaggi += '<div class="col-md-12 col-xs-12" style="font-size:' + font_size + '">Impostare settaggio tasto incasso tedesco su fast.comanda</div>';*/
                                    form_settaggi += '<div class="col-md-9 col-xs-9" style="display:block;font-size:' + font_size + '">Bargeld&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Bargeld\', \'Per i clienti tedesci aggiunge il tasto Bargeld, che significa incasso senza stampa.\')">?</a></div><div class="col-md-3 col-xs-3" style="display:block;"><input class="form-control" type="checkbox" name="tasto_incasso" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                }

                                if (comanda.lingua_stampa === 'deutsch') {
                                    var test1 = result[0].avviso_incasso == "S" ? "checked" : "";
                                    form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Avviso Incasso&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Avviso Incasso\', \'Appare un messaggio di conferma, una volta premuto in tasto Incasso.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="avviso_incasso" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                    var test1 = result[0].avviso_quittung == "S" ? "checked" : "";
                                    form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Avviso Quittung&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Avviso Quittung\', \'Appare un messaggio di conferma, una volta premuto in tasto Quittung.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="avviso_quittung" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                    var test1 = result[0].avviso_rechnung == "S" ? "checked" : "";
                                    form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Avviso Rechnung&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Avviso Rechnung\', \'Appare un messaggio di conferma, una volta premuto in tasto Rechnung.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="avviso_rechnung" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                }

                                var test1 = resultifsc[0].righe_zero == "1" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Avviso Righe Prezzo Zero&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Avviso Righe Prezzo Zero\', \'Appare un messaggio di conferma, se ci sono articoli battuti a prezzo zero.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="avviso_prezzo_zero" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                var test1 = resultifsc[0].varianti_riunite == "S" ? "checked" : "";
                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Acquisizione Multipla con Variante&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Acquisizione Multipla con Variante\', \'Quando batto più varianti utilizzando il tasto di quantit&agrave;, le varianti verranno unite in un unica riga, invece di essere ognuna su una riga separata.\')">?</a></div><div class="col-md-3 col-xs-3"><input class="form-control" type="checkbox" name="varianti_riunite" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                form_settaggi += '<div class="col-md-9 col-xs-9" style="font-size:' + font_size + '">Stampante fast.ingresso&nbsp;<a style="font-size:15px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Stampante fast.ingresso\', \'ID della stampante di fast.ingresso (vedi tabella nomi_stampanti)\')">?</a></div><div class="col-md-3 col-xs-3  nopadding"><input maxlength="42" class="form-control" type="number" name="stampante_fastingresso"  style="font-size:25px" value="' + resultifsc[0].stampante_fastingresso + '" ></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';



                                form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12 text-center" style="margin-top: 0px;"><button class="btn btn-success" ' + comanda.evento + '="salva_settaggi(\'LAYOUT_VIDEO\');" style="font-size: 3vh;text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                                $('#form_settaggi').html(form_settaggi);

                                if (resultifsc[0].tastiera_ricerca_filtro_plu === "S") {
                                    document.getElementsByName("tastiera_ricerca_tasto_verde_batte_articolo")[0].disabled = false;
                                } else {
                                    document.getElementsByName("tastiera_ricerca_tasto_verde_batte_articolo")[0].checked = false;
                                    document.getElementsByName("tastiera_ricerca_tasto_verde_batte_articolo")[0].disabled = true;
                                }

                                if (result[0].varianti_su_tastiera == "S") {
                                    document.getElementsByName("tastiera_scomparsa_auto")[0].checked = false;
                                    document.getElementsByName("tastiera_scomparsa_auto")[0].disabled = true;
                                } else {
                                    document.getElementsByName("tastiera_scomparsa_auto")[0].disabled = false;
                                }

                                if (result[0].tasto_contosep == "S") {
                                    document.getElementsByName("multiquantita")[0].checked = false;
                                    document.getElementsByName("multiquantita")[0].disabled = true;
                                } else {
                                    document.getElementsByName("multiquantita")[0].disabled = false;
                                }

                                if (result[0].multiquantita == "S") {
                                    document.getElementsByName("tasto_contosep")[0].checked = false;
                                    document.getElementsByName("tasto_contosep")[0].disabled = true;
                                } else {
                                    document.getElementsByName("tasto_contosep")[0].disabled = false;
                                }

                                if (resultifsc[0].coperti_obbligatorio != "1") {
                                    document.getElementsByName("avviso_coperti_comanda")[0].checked = false;
                                    document.getElementsByName("avviso_coperti_comanda")[0].disabled = true;
                                } else {
                                    document.getElementsByName("avviso_coperti_comanda")[0].disabled = false;
                                }

                                if (result[0].azzera_prezzo_veloce == "S") {
                                    document.getElementsByName("modifica_portata_veloce")[0].checked = false;
                                } else {

                                }

                                if (result[0].modifica_portata_veloce == "S") {
                                    document.getElementsByName("azzera_prezzo_veloce")[0].checked = false;
                                } else {

                                }


                                if (resultifsc[0].tasto_visu_varianti_menopiu == "S") {
                                    document.getElementsByName("tasto_visu_varianti_piumeno")[0].checked = false;
                                } else {

                                }


                                if (resultifsc[0].tasto_visu_varianti_piumeno == "S") {
                                    document.getElementsByName("tasto_visu_varianti_menopiu")[0].checked = false;
                                } else { }


                                if (resultifsc[0].tasto_visu_varianti == "S") {

                                } else {
                                    document.getElementsByName("tasto_visu_varianti_menopiu")[0].checked = false;
                                    document.getElementsByName("tasto_visu_varianti_piumeno")[0].checked = false;
                                }
                            });
                        });
                        break;

                    case "FASTORDER":


                        form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';

                        form_settaggi += '<h4>Attenzione: questi settaggi sono solo temporanei</h4>';

                        form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:20px">URL fast.order</div><div class="col-md-7 col-xs-7"><input class="form-control" type="text" name="url_fastorder"  style="font-size:20px" value="" placeholder="es. http://fastorder.club/Profilo/"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';


                        form_settaggi += '<div class="col-md-11 col-xs-11" style="font-size:20px">Categorie Intellinet SI / Importazione NO</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="categorie_come_intellinet" ></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';


                        form_settaggi += '<div class="col-md-11 col-xs-11" style="font-size:20px">Modifica Prezzi in Aggiornamento</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="modifica_prezzi_fastorder" ></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                        form_settaggi += '<div class="col-md-12 col-xs-12"><button class="btn btn-default btn-warning" type="button" onclick="fastorder.export_products_fastorder()">Esporta</button</div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                        form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <!--<div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'FASTORDER\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>-->\n\
                                        </div>';

                        $('#form_settaggi').html(form_settaggi);



                        break;

                }
                break;



        }
    } else if (comanda.pizzeria_asporto === true)
    //SETTAGGI DELLA PIZZERIA DA ASPORTO
    {
        comanda.sincro.query("SELECT * FROM settaggi_profili  where id=" + comanda.folder_number + "  LIMIT 1;", function (result) {

            for (var key in result[0]) {
                console.log("RESULT0", key, result[0], result[0][key]);
                if (result[0][key] === null) {
                    result[0][key] = '';
                }
            }
            switch (livello_superiore) {
                case "generici":
                    switch (nome_settaggio) {
                        case "PRINCIPALI":

                            let tabella_reparti_IVA = REPARTO_controller.tabella_reparti_IVA();
                            let tabella_IVA_semplice = IVA_controller.tabella_IVA_semplice();

                            comanda.sincro.query("SELECT * FROM impostazioni_fiscali  where id=" + comanda.folder_number + "  LIMIT 1;", function (result) {

                                var testo_query = "SELECT id,ordinamento,descrizione FROM categorie WHERE SUBSTR(descrizione,1,2)!=\"V.\" ORDER BY ordinamento ASC;";

                                comanda.sincro.query(testo_query, function (result_categorie) {

                                    for (var key in result[0]) {
                                        console.log("RESULT0", key, result[0], result[0][key]);
                                        if (result[0][key] === null) {
                                            result[0][key] = '';
                                        }
                                    }

                                    comanda.sincro.query("SELECT * FROM dati_servizio where id='" + comanda.folder_number + "' LIMIT 1;", function (dati_servizio) {

                                        form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';
                                        form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[186] + '</div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="lingua_visualizzazione"><option value="I">Italiano</option><option value="T">Tedesco</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                        form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[187] + '</div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="lingua_stampa"><option value="italiano">Italiano</option><option value="deutsch">Deutsch</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                        form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[188] + '</div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="servizio_fino_alle"><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                        var test1 = result[0].righe_zero == "1" ? "checked" : "";
                                        form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[189] + '</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="righe_prezzo_zero" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                        form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[190] + '</div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control" type="number" name="valore_coperto"  style="font-size:25px" value="' + result[0].valore_coperto + '"></div><div class="col-md-3 col-xs-3" style="font-size:20px">Esempio: 1,00</div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                        var test1 = result[0].coperti_obbligatorio == "1" ? "checked" : "";
                                        form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[191] + '</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="coperti_obbligatori" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                        form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[192] + '</div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control" type="number" name="valore_servizio"  style="font-size:25px" value="' + result[0].valore_servizio + '"></div><div class="col-md-3 col-xs-3" style="font-size:20px">Esempio: 10</div>\n\
                                                </div>';

                                                var test1 = result[0].time_gap_consegna == "1" ? "checked" : "";
                                                form_settaggi += '<div class="col-md-12 col-xs-12">';
                                                form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Time Gap Consegna&nbsp;<a style="font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Time Gap Consegna\', \'Questo Settagio permette di acquisire gli ordini in un gap di tempo (15 o 30 minuti) successivi all orario di consegna inserito.<br><br>compariranno due bottoni sopra il campo di inserimento orario su schermata.\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="time_gap_consegna" ' + test1 + '></div>';
                                                form_settaggi += '</div>';

                                        var test1 = result[0].orario_preparazione == "1" ? "checked" : "";

                                        form_settaggi += '<div class="col-md-12 col-xs-12">';
                                        form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Abilita Tempo Preparazione&nbsp;<a style="font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'ABILITA TEMPO DI PREPARAZIONE\', \'Abilitando questa impostazione potrai abilitare l automatismo del calcolo dell orario di preparazione dell ordine, in base all orario di consegna. Questo verr&agrave; visualizzato nella schermata Ordine alla destra della selezione della tipologia di consegna, e potr&agrave; facoltativamente essere visibile anche nell orario delle righe della sezione forno e nella comanda.<br><br>Impostare nelle due voci successive i minuti che occorrono per la preparazione dell ordine e i minuti che impiega generalmente il fattorino a consegnare.<br>Nel caso di un ordine da ritirare presso il vostro locale l orario di preparazione sar&agrave; l orario di consegna - i minuti di preparazione per il ritiro.<br>Nel caso invece di un ordine da consegnare a domicilio, l orario di preparazione sar&ugrave; l orario di consegna - i minuti di preparazione per il ritiro - il tempo di consegna.<br><br>Il parametro &egrave; comunque modificabile dalla schermata di inserimento cliente.\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="orario_preparazione" ' + test1 + '></div>';
                                        form_settaggi += '</div>';

                                        form_settaggi += '<div class="col-md-12 col-xs-12">';
                                        form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Minuti di Preparazione Ritiro</div><div class="col-md-4 col-xs-4"><input placeholder="Tempo Preparazione Ritiro" class="form-control" type="text" name="tempo_preparazione_default"  style="font-size:25px" value="' + result[0].tempo_preparazione_default + '"></div>';
                                        form_settaggi += '</div>';

                                        form_settaggi += '<div class="col-md-12 col-xs-12">';
                                        form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Tempo di Consegna Domicilio</div>';
                                        form_settaggi += '<div class="col-md-4 col-xs-4"><input placeholder="Tempo di Consegna Domicilio" class="form-control" type="text" name="tempo_preparazione_default_domicilio"  style="font-size:25px" value="' + result[0].tempo_preparazione_default_domicilio + '"></div>';

                                        var test1 = result[0].abilita_fattorino1 == "1" ? "checked" : "";

                                        form_settaggi += '<div class="col-md-12 col-xs-12">';
                                        form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Abilita Fattorino 1&nbsp;<a style="font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'ABILITA FATTORINO 1\', \'Abilitando il fattorino aggiuntivo (che puo essere JustEat, Deliveroo, eccetera) compariranno due campi del relativo prezzo normale e maxi, inserendo i prodotti.\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="abilita_fattorino1" ' + test1 + '></div>';
                                        form_settaggi += '<div class="col-md-4 col-xs-4"><input placeholder="Nome Fattorino 1" class="form-control" type="text" name="nome_fattorino1"  style="font-size:25px" value="' + result[0].nome_fattorino1 + '"></div>';

                                        form_settaggi += '</div></div>';

                                        var test1 = result[0].abilita_fattorino2 == "1" ? "checked" : "";
                                        form_settaggi += '<div class="col-md-12 col-xs-12">';
                                        form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Abilita Fattorino 2&nbsp;<a style="font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'ABILITA FATTORINO 2\', \'Abilitando il fattorino aggiuntivo (che puo essere JustEat, Deliveroo, eccetera) compariranno due campi del relativo prezzo normale e maxi, inserendo i prodotti.\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="abilita_fattorino2" ' + test1 + '></div>';
                                        form_settaggi += '<div class="col-md-4 col-xs-4"><input placeholder="Nome Fattorino 2" class="form-control" type="text" name="nome_fattorino2"  style="font-size:25px" value="' + result[0].nome_fattorino2 + '"></div>';
                                        form_settaggi += '</div></div>';

                                        var test1 = result[0].abilita_fattorino3 == "1" ? "checked" : "";
                                        form_settaggi += '<div class="col-md-12 col-xs-12">';
                                        form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Abilita Fattorino 3&nbsp;<a style="font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'ABILITA FATTORINO 3\', \'Abilitando il fattorino aggiuntivo (che puo essere JustEat, Deliveroo, eccetera) compariranno due campi del relativo prezzo normale e maxi, inserendo i prodotti.\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="abilita_fattorino3" ' + test1 + '></div>';
                                        form_settaggi += '<div class="col-md-4 col-xs-4"><input placeholder="Nome Fattorino 3" class="form-control" type="text" name="nome_fattorino3"  style="font-size:25px" value="' + result[0].nome_fattorino3 + '"></div>';
                                        form_settaggi += '</div></div>';

                                        form_settaggi += '<div class="col-md-12 col-xs-12"><div class="col-md-5 col-xs-5" style="font-size:30px">Consegna&nbsp;<a style="font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'IMPORTO CONSEGNA\', \'Ci sono 4 modalit&agrave; di calcolo dell importo di consegna: <ul><li>la prima inserendo il valore per ogni pizza, mezzo metro e metro di pizza, che verr&agrave; calcolato automaticamente dal software.</li><li>La seconda inserendo un importo in percentuale o fisso da applicare al totale.</li><li>Inserire il range delle pizze e il valore in Euro da assoggettare alla singola pizza.</li><li>Inserire il range delle pizze e il valore in Euro da assoggettare all&rsquot;intera consegna.</li></ul>\')">?</a></div>\n\
                                                                  </div>';

                                        form_settaggi += '<div class="col-md-12 col-xs-12 riga_consegna_1" ' + comanda.evento + '="check_settaggi_consegne(1);">';

                                        form_settaggi += '<div class="col-md-3 col-xs-3"><label>a pizza</label><input maxlength="5" class="form-control" placeholder="0.50" type="number" name="consegna_a_pizza"  style="font-size:25px" value="' + result[0].consegna_a_pizza + '"></div><div class="col-md-3 col-xs-3"><label>per mezzo metro di pizza</label><input maxlength="5" class="form-control" placeholder="1.00" type="number" name="consegna_mezzo_metro"  style="font-size:25px" value="' + result[0].consegna_mezzo_metro + '"></div><div class="col-md-3 col-xs-3"><label>per un metro di pizza</label><input maxlength="5" class="form-control" placeholder="1.50" type="number" name="consegna_metro"  style="font-size:25px" value="' + result[0].consegna_metro + '"></div><div class="col-md-3 col-xs-3"><label>per pizza maxi</label><input maxlength="5" class="form-control" placeholder="0.80" type="number" name="consegna_maxi"  style="font-size:25px" value="' + result[0].consegna_maxi + '"></div>\n\
                                                </div>';

                                        form_settaggi += '<div class="col-md-12 col-xs-12 riga_consegna_2"  ' + comanda.evento + '="check_settaggi_consegne(2);" style="padding-top: 10px;">';

                                        form_settaggi += '<div class="col-md-3 col-xs-3"><label>sul totale</label><input maxlength="5" class="form-control" placeholder="10%/2.00" type="text" name="consegna_su_totale"  style="font-size:25px" value="' + result[0].consegna_su_totale + '"></div>\n\
                                                </div>';

                                        form_settaggi += '<div class="col-md-12 col-xs-12 riga_consegna_3"  ' + comanda.evento + '="check_settaggi_consegne(3);" style="padding-top: 10px;">';

                                        form_settaggi += '<div class="col-md-2 col-xs-2"><label>da n. pizza:</label><input maxlength="5" class="form-control" placeholder="1" type="number" name="da_fascia_1"  style="font-size:25px" value="' + result[0].da_fascia_1 + '"></div><div class="col-md-2 col-xs-2"><label>a n. pizza:</label><input maxlength="5" class="form-control" placeholder="4" type="number" name="a_fascia_1"  style="font-size:25px" value="' + result[0].a_fascia_1 + '"></div><div class="col-md-2 col-xs-2"><label>euro a pizza:</label><input maxlength="5" class="form-control" placeholder="1.00" type="number" name="una_pizza"  style="font-size:25px" value="' + result[0].una_pizza + '"></div><div class="col-md-2 col-xs-2"><label>da n. pizza:</label><input maxlength="5" class="form-control" placeholder="5" type="number" name="da_fascia_2"  style="font-size:25px" value="' + result[0].da_fascia_2 + '"></div><div class="col-md-2 col-xs-2"><label>a n. pizza:</label><input maxlength="5" class="form-control" placeholder="100" type="number" name="a_fascia_2"  style="font-size:25px" value="' + result[0].a_fascia_2 + '"></div><div class="col-md-2 col-xs-2"><label>euro per pizza:</label><input maxlength="5" class="form-control" placeholder="0.60" type="number" name="piu_di_una_pizza"  style="font-size:25px" value="' + result[0].piu_di_una_pizza + '"></div>\n\
                                                </div>';


                                        form_settaggi += '<div class="col-md-12 col-xs-12 riga_consegna_4"  ' + comanda.evento + '="check_settaggi_consegne(4);" style="padding-top: 10px;">';

                                        form_settaggi += '<div class="col-md-2 col-xs-2"><label>da n. pizza:</label><input maxlength="5" class="form-control" placeholder="1" type="number" name="fissa_da_fascia_1"  style="font-size:25px" value="' + result[0].fissa_da_fascia_1 + '"></div><div class="col-md-2 col-xs-2"><label>a n. pizza:</label><input maxlength="5" class="form-control" placeholder="3" type="number" name="fissa_a_fascia_1"  style="font-size:25px" value="' + result[0].fissa_a_fascia_1 + '"></div><div class="col-md-2 col-xs-2"><label>euro a consegna:</label><input maxlength="5" class="form-control" placeholder="1.00" type="number" name="fissa_una_pizza"  style="font-size:25px" value="' + result[0].fissa_una_pizza + '"></div><div class="col-md-2 col-xs-2"><label>da n. pizza:</label><input maxlength="5" class="form-control" placeholder="4" type="number" name="fissa_da_fascia_2"  style="font-size:25px" value="' + result[0].fissa_da_fascia_2 + '"></div><div class="col-md-2 col-xs-2"><label>a n. pizza:</label><input maxlength="5" class="form-control" placeholder="100" type="number" name="fissa_a_fascia_2"  style="font-size:25px" value="' + result[0].fissa_a_fascia_2 + '"></div><div class="col-md-2 col-xs-2"><label>euro a consegna:</label><input maxlength="5" class="form-control" placeholder="2.00" type="number" name="fissa_piu_di_una_pizza"  style="font-size:25px" value="' + result[0].fissa_piu_di_una_pizza + '"></div>\n\
                                                </div>';


                                        form_settaggi += '<div class="col-md-12 col-xs-12" style="padding-top: 10px;">';

                                        form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[193] + '</div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control" type="number" name="iva_default"  style="font-size:25px" value="' + result[0].aliquota_default + '"></div><div class="col-md-3 col-xs-3" style="font-size:20px">Esempio: 22</div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                        form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Reparto Servizi Default</div><div class="col-md-3 col-xs-3"><select class="form-control" name="reparto_servizi_default"  style="font-size:25px;height: auto;">' + tabella_reparti_IVA + '</select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                        var test1 = result[0].iva_estera_abilitata == "1" ? "checked" : "";
                                        form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">P.IVA Estera Abilitata</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="iva_estera_abilitata" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                        form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[194] + '</div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control" type="number" name="iva_takeaway"  style="font-size:25px" value="' + result[0].aliquota_takeaway + '"></div><div class="col-md-3 col-xs-3" style="font-size:20px">Esempio: 22</div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                        form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Reparto Beni Default</div><div class="col-md-3 col-xs-3"><select class="form-control" name="reparto_beni_default"  style="font-size:25px;height: auto;">' + tabella_reparti_IVA + '</select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                        form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">IVA Consegna</div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control" type="number" name="aliquota_consegna"  style="font-size:25px" value="' + result[0].aliquota_consegna + '"></div><div class="col-md-3 col-xs-3" style="font-size:20px">Esempio: 22</div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                        form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Reparto Consegna</div><div class="col-md-3 col-xs-3"><select class="form-control" name="reparto_consegna"  style="font-size:25px;height: auto;">' + tabella_reparti_IVA + '</select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                        form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Reparto Ritiro</div><div class="col-md-3 col-xs-3"><select class="form-control" name="reparto_ritiro"  style="font-size:25px;height: auto;"><option value="0">Servizi</option><option value="1">Beni</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                        form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Reparto Domicilio</div><div class="col-md-3 col-xs-3"><select class="form-control" name="reparto_domicilio"  style="font-size:25px;height: auto;"><option value="0">Servizi</option><option value="1">Beni</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                        form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Reparto Consumazione sul Posto</div><div class="col-md-3 col-xs-3"><select class="form-control" name="reparto_mangiaqui"  style="font-size:25px;height: auto;"><option value="0">Servizi</option><option value="1">Beni</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                        var test1 = result[0].fastorder == "1" ? "checked" : "";
                                        form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">fast.order</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="fastorder" ' + test1 + '></div></div>';


                                        form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[195] + '</div><div class="col-md-5 col-xs-5">';
                                        form_settaggi += '<select class="form-control nopadding" style="font-size:25.5px" name="scelta_categoria">';
                                        result_categorie.forEach(function (value, index) {

                                            form_settaggi += '<option value="' + value.id + '">' + value.descrizione + '</option>';
                                        });
                                        form_settaggi += '</select>';
                                        form_settaggi += '</div>';
                                        var testo_query = "SELECT Field510 from settaggi_profili";
                                        var querry_settagio = alasql(testo_query);
                                        var test2 = querry_settagio[0].Field510 == "1" ? "checked" : "";
                                        form_settaggi += '<div class="col-md-12 col-xs-12"><div class="col-md-5 col-xs-5" style="font-size:30px">Disattivo Promo&nbsp;<a style="font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'DISATTIVA PROMO\', \'Abilitando questo settaggio tutte le promozioni applicate ai prodotti verranno disabilitate finch&egrave non verr&agrave disabilitato il settaggio.</li></ul>\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="disattivi_promo" ' + test2 + '></div>\n\
                                                    <div class="col-md-12 col-xs-12">';
                                                    var test1 = result[0].salva_numero_associa == "1" ? "checked" : "";
                                                form_settaggi += '<div class="col-md-12 col-xs-12">';
                                                form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Salva Numero&nbsp;<a style="font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'SALVA NUMERO\', \'Abilitando questo settaggio comparir&agrave; un bottone &quot;Salva Num&quot; nella sezione ordini, sotto i dati anagrafici.<br><br>Questo, utilizzando il rilevatore telefonico, dar&agrave; la possibilit&agrave; di associare il numero chiamante all anagrafica desiderata, esistente o nuova.\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="salva_numero_associa" ' + test1 + '></div>';
                                                form_settaggi += '</div>';





                                        form_settaggi += '</div>\n\
                                                                     \n\
                                                                    <div class="col-md-12 col-xs-12">';
                                        form_settaggi += '</div>\n\
                                                    \n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'PRINCIPALI\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                                        $('#form_settaggi').html(form_settaggi);

                                        $('#form_settaggi select[name="reparto_servizi_default"] option[value="' + result[0].reparto_servizi_default + '"]').attr("selected", true);
                                        $('#form_settaggi select[name="reparto_beni_default"] option[value="' + result[0].reparto_beni_default + '"]').attr("selected", true);
                                        $('#form_settaggi select[name="reparto_consegna"] option[value="' + result[0].reparto_consegna + '"]').attr("selected", true);

                                        $('#form_settaggi select[name="reparto_ritiro"] option[value="' + result[0].reparto_ritiro + '"]').attr("selected", true);
                                        $('#form_settaggi select[name="reparto_domicilio"] option[value="' + result[0].reparto_domicilio + '"]').attr("selected", true);
                                        $('#form_settaggi select[name="reparto_mangiaqui"] option[value="' + result[0].reparto_mangiaqui + '"]').attr("selected", true);



                                        $('#form_settaggi select[name="lingua_visualizzazione"] option[value="' + '' + '"]').attr("selected", true);
                                        $('#form_settaggi select[name="lingua_stampa"] option[value="' + result[0].lingua_stampa + '"]').attr("selected", true);
                                        $('#form_settaggi select[name="servizio_fino_alle"] option[value="' + dati_servizio[0].ora_finale_servizio + '"]').attr("selected", true);
                                        $('#form_settaggi select[name="scelta_categoria"] option[value="' + result[0].categoria_partenza + '"]').attr("selected", true);
                                    });
                                });
                            });
                            break;
                    }
                    break;
                case "stampe":
                    switch (nome_settaggio) {

                        case "COMANDA":
                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field108 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[196] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="comandapiuconto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            //MEMO SU COMANDA ASPORTO
                            var test1 = result[0].Field187 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Memo dopo comanda</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="memo_dopo_comanda" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field465 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Comanda Temporizzata</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="comanda_temporizzata" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field466 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Gap Minuti Com. Temp. Rit.</div><div class="col-md-5 col-xs-5"><input maxlength="5" class="form-control" type="text" name="gap_comanda_temporizzata"  style="font-size:25px" value="' + result[0].Field466 + '"></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field467 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Gap Minuti Com. Temp. Dom.</div><div class="col-md-5 col-xs-5"><input maxlength="5" class="form-control" type="text" name="gap_comanda_temporizzata_domicilio"  style="font-size:25px" value="' + result[0].Field467 + '"></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'STAMPE_COMANDA\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            $('#form_settaggi').html(form_settaggi);
                            break;
                        case "CONTO":
                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field110 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[197] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="contodoppio" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            //MEMO SU CONTO ASPORTO
                            var test1 = result[0].Field188 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Memo dopo conto</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="memo_dopo_conto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'STAMPE_CONTO\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            $('#form_settaggi').html(form_settaggi);
                            break;
                        case "INCASSI":
                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field108 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Venduti Doppia Copia</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="dettagliovendutidoppiacopia" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field109 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[199] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="incassipiudettagliovenduti" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'STAMPE_INCASSI\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            $('#form_settaggi').html(form_settaggi);
                            break;
                        case "BUONI PASTO":
                            form_settaggi = '<div class="editor_settaggi">';
                            form_settaggi += '<div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field361 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[200] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="buoniacquistosunonfiscale" ' + test1 + '></div>';
                            form_settaggi += '</div>';
                            form_settaggi += '<div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field194 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Abilita Buono Acquisto Mono e Multi uso</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="abilita_buono_acquisto" ' + test1 + '></div>';
                            form_settaggi += '</div>';
                            form_settaggi += '<div class="col-md-12 col-xs-12">';
                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'STAMPE_BUONI_PASTO\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            $('#form_settaggi').html(form_settaggi);
                            break;
                    }
                    break;
                case "layout_stampa":
                    switch (nome_settaggio) {

                        case "COMANDA":

                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[201] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga1"  style="font-size:25px" value="' + result[0].Field111 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga1_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field113 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spaziointestazione1" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[203] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga2"  style="font-size:25px" value="' + result[0].Field114 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga2_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field116 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spaziointestazione2" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[204] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga3"  style="font-size:25px" value="' + result[0].Field117 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga3_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field119 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spaziointestazione3" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[205] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga4"  style="font-size:25px" value="' + result[0].Field120 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga4_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field122 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Titolo</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="titolo" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field123 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[182] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="nome_stampante"  ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            /*visualizzazione orario abilitata*/
                            var test1 = result[0].Field469 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Visualizza Orario&nbsp;<a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'VISUALIZZA ORARIO\', \'Si intende la visualizzazione dell orario di consegna o di preparazione dell ordine acquisito.<br><br>Dalla tendina si potr&agrave; selezionare quale delle due tipologie si desidera visualizzare.<br><br>L orario di preparazione sar&agrave; visibile solo se attivato dai settaggi principali.<br><br>Questo sar&agrave; visibile solo nel caso in cui si spunti la casella bianca.\')">?</a></div><div class="col-md-3 col-xs-3"><select style="font-size:25px" class="form-control nopadding" name="tipo_orario_comanda_visu_alta">';

                            form_settaggi += '<option value="C">Consegna</option>';

                            if (comanda.orario_preparazione === "1") {
                                form_settaggi += '<option value="P">Preparazione</option>';
                            }

                            form_settaggi += '</select></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="bool_orario_comanda_visu_alta"  ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';



                            var test1 = result[0].Field124 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[43] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="operatore"  ' + test1 + '></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="font_operatore"><option value="N">Normale</option><option value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field126 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[206] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="data" ' + test1 + '></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="font_data"><option value="N">Normale</option><option value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field128 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[207] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="ora" ' + test1 + '></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="font_ora"><option value="N">Normale</option><option value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field130 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[209] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="portate" ' + test1 + ' ></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="portate_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option><option style="font-weight:bold;" value="D">Doppio</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Segno Meno Specificato&nbsp;<a style="font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'SEGNO MENO SPECIFICATO\', \'Nel campo a seguire inserire la parola che verr&agrave; posta automaticamente dopo il segno MENO (-) in comanda.<br><br>Es: se nel campo scriviamo SENZA la comanda verr&agrave;: - SENZA e la variante scelta.\')">?</a></div><div class="col-md-3 col-xs-3"><input placeholder="Es: SENZA" maxlength="42" class="form-control" type="text" name="meno_specificato"  style="font-size:25px" value="' + result[0].Field307 + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field310 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Variante Meno Evidenziata</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_comanda" type="checkbox" name="variante_meno_evidenziata" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field422 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Variante Poco Evidenziata</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_comanda" type="checkbox" name="variante_poco_evidenziata" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field423 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Variante Fine Cottura Evidenziata</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_comanda" type="checkbox" name="variante_fine_cottura_evidenziata" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field410 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Variante Pi&ugrave; Evidenziata</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_comanda" type="checkbox" name="variante_piu_evidenziata" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field424 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Specifica Evidenziata</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_comanda" type="checkbox" name="specifica_evidenziata" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var el_tendina = alasql("select * from categorie where substr(descrizione,1,2)!='V.';");
                            var tendina = '<option value="N">Nessuno</option>';

                            el_tendina.forEach(function (e) {
                                tendina += '<option value="' + e.id + '">' + e.descrizione + '</option>';
                            });
                            tendina += '';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Articoli Evidenziati</div><div class="col-md-3 col-xs-3"><select style="font-size:25px" class="form-control nopadding" name="articoli_evidenziati_cat_1">' + tendina + '</select></div><div class="col-md-3 col-xs-3"><select style="font-size:25px" class="form-control nopadding" name="articoli_evidenziati_cat_2">' + tendina + '</select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';


                            var test1 = result[0].Field132 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[210] + '</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_comanda" type="checkbox" name="prezzo" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field189 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Totale prezzo articoli</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_comanda" type="checkbox" name="totale_prezzo_articoli" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[211] + '</div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding settaggi_comanda" name="font_articolo"><option value="N">Normale</option><option value="G">Grassetto</option><option value="D">Doppio</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[212] + '</div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding settaggi_comanda" name="font_varianti"><option value="N">Normale</option><option value="G">Grassetto</option><option value="D">Doppio</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field135 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[213] + '&nbsp;<a style="font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'UNIONE ARTICOLI CON VARIANTI\', \'Questa impostazione unisce la quantit&agrave; totale dello stesso prodotto ordinato, e sotto riporta la quantit&agrave; totale per differenziazione di aggiunte.<br><br>Es.<br>3 Margherite<br>&nbsp;&nbsp;1 +Salamino<br>&nbsp;&nbsp;2 +Origano<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+ Wurstel<br><br>NB: Abilitando questa impostazione si disabiliter&agrave; in automatico &quot;Variante Meno Evidenziata&quot;\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="unione_articoli_varianti" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field378 === "true" ? "checked" : "";
                            var test2 = result[0].Field379 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[178] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="ricetta" ' + test1 + '></div><div class="col-md-3 col-xs-3"><select style="font-size:25px" class="form-control nopadding" name="ricetta_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option><option style="font-weight:bold;" value="D">Doppio</option></select></div><div class="col-md-2 col-xs-2"><h4>Tutto Maiuscolo</h4></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="ricetta_maiuscola" ' + test2 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field381 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Totale Pizze</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="totale_pizze"' + test1 + ' ></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field382 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Totale Bibite</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="totale_bibite"' + test1 + ' ></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field137 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Totale Quantit&agrave; Prodotti</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="totale_quantita_articoli"' + test1 + ' ></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[215] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="msg_riga1"  style="font-size:25px" value="' + result[0].Field138 + '"></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga1_grassetto"><option value="N">N</option><option style="font-weight:bold;" value="G">G</option></select></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga1_allineamento"><option value="x--">x--</option><option value="-x-">-x-</option><option value="--x">--x</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field141 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spazio1" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[216] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="msg_riga2"  style="font-size:25px" value="' + result[0].Field142 + '"></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga2_grassetto"><option value="N">N</option><option style="font-weight:bold;" value="G">G</option></select></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga2_allineamento"><option value="x--">x--</option><option value="-x-">-x-</option><option value="--x">--x</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field145 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spazio2" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[217] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="msg_riga3"  style="font-size:25px" value="' + result[0].Field146 + '"></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga3_grassetto"><option value="N">N</option><option style="font-weight:bold;" value="G">G</option></select></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga3_allineamento"><option value="x--">x--</option><option value="-x-">-x-</option><option value="--x">--x</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field149 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[218] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="concomitanze" style="font-size:25px" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Consegna a Domicilio:</div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';



                            var test1 = result[0].Field252 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-4 col-xs-4 col-xs-offset-1" style="font-size:30px">Dati Cliente&nbsp;<a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'DATI CLIENTE\', \'Abilitando questo settaggio, nelle comande per le consegne a domicilio compariranno in basso i dati del cliente (nome, cognome, indirizzo e numeri di telefono).\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="dati_cliente" style="font-size:25px" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                                    form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + 'Font Dati Cliente' + '</div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding settaggi_comanda" name="font_data_cliente"><option value="N">Normale</option><option value="D">Doppio</option></select></div>\n\
                                                                                                </div>\n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field253 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-4 col-xs-4 col-xs-offset-1" style="font-size:30px">QRCode Navigatore&nbsp;<a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'QRCODE NAVIGATORE\', \'Una volta abilitato, comparir&agrave; un QRCode che post-scannerizzazione mostrer&agrave; tramite Google Maps il percorso pi&ugrave; breve per arrivare dal cliente. \')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="qrcode_navigatore" style="font-size:25px" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-4 col-xs-4 col-xs-offset-1" style="font-size:30px">Testo Libero&nbsp;<a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'TESTO LIBERO\', \'Ci&ograve; che verr&agrave; scritto in questo campo comparir&agrave; a pi&egrave; pagina della stampa della comanda, in caso di Consegna a Domicilio. Non potranno essere abilitati &quot;Dati Cliente&quot; e &quot;QRCode&quot; assieme alla scritta.\')">?</a></div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="scritta_consegna_domicilio"  style="font-size:25px" value="' + result[0].Field375 + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';


                            var test1 = result[0].Field373 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Ritiro in Pizzeria:</div></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-4 col-xs-4 col-xs-offset-1" style="font-size:30px">Testo Libero&nbsp;<a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'TESTO LIBERO\', \'Ci&ograve; che verr&agrave; scritto in questo campo comparir&agrave; a pi&egrave; pagina della stampa della comanda, in caso il cliente venga a Ritirare l&rsquo;Ordine in Pizzeria.\')">?</a></div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="scritta_consegna_ritiro"  style="font-size:25px" value="' + result[0].Field376 + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            if (result[0].Field372 === "true") {
                                var test1 = result[0].Field374 === "true" ? "checked" : "";
                                form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Consumazione in Pizzeria:</div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                form_settaggi += '<div class="col-md-4 col-xs-4 col-xs-offset-1" style="font-size:30px">Testo Libero&nbsp;<a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'TESTO LIBERO\', \'Ci&ograve; che verr&agrave; scritto in questo campo comparir&agrave; a pi&egrave; pagina della stampa della comanda, in caso di Consumazione della pizza in Pizzeria.\')">?</a></div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="scritta_consegna_pizzeria"  style="font-size:25px" value="' + result[0].Field377 + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            }

                            /*visualizzazione orario abilitata*/
                            var test1 = result[0].Field470 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Visualizza Orario&nbsp;<a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'VISUALIZZA ORARIO\', \'Si intende la visualizzazione dell orario di consegna o di preparazione dell ordine acquisito.<br><br>Dalla tendina si potr&agrave; selezionare quale delle due tipologie si desidera visualizzare.<br><br>L orario di preparazione sar&agrave; visibile solo se attivato dai settaggi principali.<br><br>Questo sar&agrave; visibile solo nel caso in cui si spunti la casella bianca.\')">?</a></div><div class="col-md-3 col-xs-3"><select style="font-size:25px" class="form-control nopadding" name="tipo_orario_comanda_visu_bassa">';


                            form_settaggi += '<option value="C">Consegna</option>';

                            if (comanda.orario_preparazione === "1") {
                                form_settaggi += '<option value="P">Preparazione</option>';
                            }

                            form_settaggi += '</select></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="bool_orario_comanda_visu_bassa"  ' + test1 + '></div>\n\
                                                    </div><div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field498 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Barcode&nbsp;<a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'BARCODE\', \'Abilitando questo settaggio comparir&agrave; il barcode a pi&egrave; pagina della comanda. Questo servir&agrave; per ri-aprire la comanda dal forno utilizzando lo scanner.\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="barcode_riapertura_ordine_comanda" style="font-size:25px" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';


                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'COMANDA\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            $('#form_settaggi').html(form_settaggi);
                            $('#form_settaggi select[name="intestazione_riga1_grassetto"] option[value="' + result[0].Field112 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="intestazione_riga2_grassetto"] option[value="' + result[0].Field115 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="intestazione_riga3_grassetto"] option[value="' + result[0].Field118 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="intestazione_riga4_grassetto"] option[value="' + result[0].Field121 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="font_operatore"] option[value="' + result[0].Field125 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="font_data"] option[value="' + result[0].Field127 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="font_ora"] option[value="' + result[0].Field129 + '"]').attr("selected", true);

                            $('#form_settaggi select[name="tipo_orario_comanda_visu_alta"] option[value="' + result[0].Field471 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="tipo_orario_comanda_visu_bassa"] option[value="' + result[0].Field472 + '"]').attr("selected", true);



                            if (result[0].Field131 === "G") {
                                result[0].Field131 = "Grassetto";
                            } else if (result[0].Field131 === "D") {
                                result[0].Field131 = "Doppio";
                            }
                            $('#form_settaggi select[name="portate_grassetto"] option:containsExact("' + result[0].Field131 + '")').attr('selected', true);

                            if (result[0].Field380 === "G") {
                                result[0].Field380 = "Grassetto";
                            } else if (result[0].Field380 === "D") {
                                result[0].Field380 = "Doppio";
                            }
                            $('#form_settaggi select[name="ricetta_grassetto"] option:containsExact("' + result[0].Field380 + '")').attr('selected', true);

                            $('#form_settaggi select[name="articoli_evidenziati_cat_1"] option[value="' + result[0].Field383 + '"]').attr('selected', true);
                            $('#form_settaggi select[name="articoli_evidenziati_cat_2"] option[value="' + result[0].Field384 + '"]').attr('selected', true);

                            $('#form_settaggi select[name="font_articolo"] option[value="' + result[0].Field133 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="font_varianti"] option[value="' + result[0].Field134 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="font_data_cliente"] option[value="' + result[0].Field512 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="msg_riga1_grassetto"] option:containsExact("' + result[0].Field139 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga1_allineamento"] option:containsExact("' + result[0].Field140 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga2_grassetto"] option:containsExact("' + result[0].Field143 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga2_allineamento"] option:containsExact("' + result[0].Field144 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga3_grassetto"] option:containsExact("' + result[0].Field147 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga3_allineamento"] option:containsExact("' + result[0].Field148 + '")').attr('selected', true);
                            break;
                        case "CONTO":

                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[201] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga1"  style="font-size:25px" value="' + result[0].Field150 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga1_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field152 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spaziointestazione1" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[203] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga2"  style="font-size:25px" value="' + result[0].Field153 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga2_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field155 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spaziointestazione2" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[204] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga3"  style="font-size:25px" value="' + result[0].Field156 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga3_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field158 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spaziointestazione3" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[205] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga4"  style="font-size:25px" value="' + result[0].Field159 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga4_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field161 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Titolo</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="titolo" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field323 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Nome Cliente&nbsp;<a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'NOME CLIENTE\', \'Abilitando questo settaggio, sotto la parola CONTO comparir&agrave; il nome della persona o il progressivo della comanda memorizzata.\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="nome_cliente" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field162 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[43] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="operatore"  ' + test1 + '></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="font_operatore"><option value="N">Normale</option><option value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field164 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[206] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="data" ' + test1 + '></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="font_data"><option value="N">Normale</option><option value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field166 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[207] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="ora" ' + test1 + '></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="font_ora"><option value="N">Normale</option><option value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var el_tendina = alasql("select * from categorie where substr(descrizione,1,2)!='V.';");
                            var tendina = '<option value="N">Nessuno</option>';

                            el_tendina.forEach(function (e) {
                                tendina += '<option value="' + e.id + '">' + e.descrizione + '</option>';
                            });
                            tendina += '';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Articoli Evidenziati</div><div class="col-md-3 col-xs-3"><select style="font-size:25px" class="form-control nopadding" name="articoli_evidenziati_cat_1">' + tendina + '</select></div><div class="col-md-3 col-xs-3"><select style="font-size:25px" class="form-control nopadding" name="articoli_evidenziati_cat_2">' + tendina + '</select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[211] + '</div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="font_articolo"><option value="N">Normale</option><option value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[212] + '</div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="font_varianti"><option value="N">Normale</option><option value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field170 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Totale Quantit&agrave; Prodotti</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="totale_quantita_articoli"' + test1 + ' ></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[215] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="msg_riga1"  style="font-size:25px" value="' + result[0].Field171 + '"></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga1_grassetto"><option value="N">N</option><option style="font-weight:bold;" value="G">G</option></select></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga1_allineamento"><option value="x--">x--</option><option value="-x-">-x-</option><option value="--x">--x</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field174 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spazio1" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[216] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="msg_riga2"  style="font-size:25px" value="' + result[0].Field175 + '"></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga2_grassetto"><option value="N">N</option><option style="font-weight:bold;" value="G">G</option></select></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga2_allineamento"><option value="x--">x--</option><option value="-x-">-x-</option><option value="--x">--x</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field178 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spazio2" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[217] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="msg_riga3"  style="font-size:25px" value="' + result[0].Field179 + '"></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga3_grassetto"><option value="N">N</option><option style="font-weight:bold;" value="G">G</option></select></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga3_allineamento"><option value="x--">x--</option><option value="-x-">-x-</option><option value="--x">--x</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field230 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">QRCode Navigatore</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="qrcode_navigatore" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field324 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Tutti i Dati Clienti&nbsp;<a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'TUTTI I DATI CLIENTI\', \'Abilitando questo settaggio, alla fine della stampa compariranno i dati dei clienti che vengono a ritirare le pizze direttamente in pizzeria.\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="tutti_dati_clienti" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                                    form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + 'Font Dati Cliente' + '</div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding settaggi_comanda" name="font_data_cliente_conto"><option value="N">Normale</option><option value="D">Doppio</option></select></div>\n\
                                                                                                </div>\n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field325 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Ora Consegna&nbsp;<a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'ORA CONSEGNA\', \'Abilitando questo settaggio, a pi&egrave; pagina della stampa comparir&agrave; l ora di consegna della comanda.\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="ora_consegna" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field499 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Barcode&nbsp;<a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'BARCODE\', \'Abilitando questo settaggio comparir&agrave; il barcode a pi&egrave; pagina del conto. Questo servir&agrave; per ri-aprire la comanda dal forno utilizzando lo scanner.\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="barcode_riapertura_ordine_conto" style="font-size:25px" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            let resultt = "SELECT * FROM settaggi_profili  where id=" + comanda.folder_number + "  LIMIT 1;"
                            let test = alasql(resultt);
                            if (test[0].Field498 === "true" || test[0].Field499 === "true" || test[0].Field500 === "true") {
                                $(".fa-barcode").show();
                            } else {
                                $(".fa-barcode").hide();
                            }
                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'CONTO\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            $('#form_settaggi').html(form_settaggi);
                            $('#form_settaggi select[name="intestazione_riga1_grassetto"] option[value="' + result[0].Field151 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="intestazione_riga2_grassetto"] option[value="' + result[0].Field154 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="intestazione_riga3_grassetto"] option[value="' + result[0].Field157 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="intestazione_riga4_grassetto"] option[value="' + result[0].Field160 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="font_operatore"] option[value="' + result[0].Field163 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="font_data"] option[value="' + result[0].Field165 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="font_ora"] option[value="' + result[0].Field167 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="font_articolo"] option[value="' + result[0].Field168 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="font_varianti"] option[value="' + result[0].Field169 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="msg_riga1_grassetto"] option:containsExact("' + result[0].Field172 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga1_allineamento"] option:containsExact("' + result[0].Field173 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga2_grassetto"] option:containsExact("' + result[0].Field176 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga2_allineamento"] option:containsExact("' + result[0].Field177 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga3_grassetto"] option:containsExact("' + result[0].Field180 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga3_allineamento"] option:containsExact("' + result[0].Field181 + '")').attr('selected', true);
                            $('#form_settaggi select[name="font_data_cliente_conto"] option[value="' + result[0].Field513 + '"]').attr("selected", true);


                            $('#form_settaggi select[name="articoli_evidenziati_cat_1"] option[value="' + result[0].Field393 + '"]').attr('selected', true);
                            $('#form_settaggi select[name="articoli_evidenziati_cat_2"] option[value="' + result[0].Field394 + '"]').attr('selected', true);
                            break;

                        case "DETTAGLIO VENDUTI":

                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[201] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga1"  style="font-size:25px" value="' + result[0].Field81 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga1_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field83 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spaziointestazione1" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[203] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga2"  style="font-size:25px" value="' + result[0].Field84 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga2_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field86 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spaziointestazione2" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[204] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga3"  style="font-size:25px" value="' + result[0].Field87 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga3_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field89 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spaziointestazione3" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[205] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga4"  style="font-size:25px" value="' + result[0].Field90 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga4_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            //--------------------------------------------------------------------//

                            var test1 = result[0].Field92 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Report</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="report" ' + test1 + '></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="report_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field93 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[221] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="creatoil" ' + test1 + '></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="creatoil_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[222] + '</div><div class="col-md-3 col-xs-3"><select style="font-size:25px" class="form-control nopadding" name="ordinamento_articoli"><option value="D">' + comanda.lang[96] + '</option><option value="Q">Quantita</option><option value="T">Totale</option></select></div><div class="col-md-3 col-xs-3"><select style="font-size:25px" class="form-control nopadding" name="ordinamento_crescente_decrescente"><option value="C">' + comanda.lang[229] + '</option><option  value="D">' + comanda.lang[230] + '</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field97 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[223] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="totaliservizio" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field98 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[224] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="totalioperatore" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field99 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[225] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="totalegenerale" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field412 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Ordinamento per Gruppi</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="divisionegruppi" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field100 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[226] + '</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="divisionecategorie" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field101 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[227] + '</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="divisionedestinazione" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field102 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[228] + '</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="percentuali" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field103 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[28] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="sconti" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'DETTAGLIO VENDUTI\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            //--------------------------------------------------------------------//


                            $('#form_settaggi').html(form_settaggi);
                            $('#form_settaggi select[name="intestazione_riga1_grassetto"] option[value="' + result[0].Field82 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="intestazione_riga2_grassetto"] option[value="' + result[0].Field85 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="intestazione_riga3_grassetto"] option[value="' + result[0].Field88 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="intestazione_riga4_grassetto"] option[value="' + result[0].Field91 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="creatoil_grassetto"] option[value="' + result[0].Field94 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="report_grassetto"] option[value="' + result[0].Field104 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="ordinamento_articoli"] option[value="' + result[0].Field95 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="ordinamento_crescente_decrescente"] option[value="' + result[0].Field96 + '"]').attr("selected", true);
                            break;

                        case "SCONTRINO":

                            /* PARTIRE DA FIELD 236 */

                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="margin-bottom:30px;font-size:30px">' + comanda.lang[215] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="msg_riga1"  style="font-size:25px" value="' + result[0].Field236 + '"></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga1_grassetto"><option value="N">N</option><option style="font-weight:bold;" value="G">G</option></select></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga1_allineamento"><option value="x--">x--</option><option value="-x-">-x-</option><option value="--x">--x</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            /*var test1 = result[0].Field237 === "true" ? "checked" : "";
                             form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spazio1" ' + test1 + '></div>\n\
                             </div>\n\
                             \n\
                             <div class="col-md-12 col-xs-12">';*/

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="margin-bottom:30px;font-size:30px">' + comanda.lang[216] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="msg_riga2"  style="font-size:25px" value="' + result[0].Field238 + '"></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga2_grassetto"><option value="N">N</option><option style="font-weight:bold;" value="G">G</option></select></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga2_allineamento"><option value="x--">x--</option><option value="-x-">-x-</option><option value="--x">--x</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            /* var test1 = result[0].Field239 == "true" ? "checked" : "";
                             form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spazio2" ' + test1 + '></div>\n\
                             </div>\n\
                             \n\
                             <div class="col-md-12 col-xs-12">';*/

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="margin-bottom:30px;font-size:30px">' + comanda.lang[217] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="msg_riga3"  style="font-size:25px" value="' + result[0].Field240 + '"></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga3_grassetto"><option value="N">N</option><option style="font-weight:bold;" value="G">G</option></select></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga3_allineamento"><option value="x--">x--</option><option value="-x-">-x-</option><option value="--x">--x</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            /*var test1 = result[0].Field247 == "true" ? "checked" : "";
                             form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spazio3" ' + test1 + '></div>\n\
                             </div>\n\
                             \n\
                             <div class="col-md-12 col-xs-12">';*/

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="margin-bottom:30px;font-size:30px">' + comanda.lang[235] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="msg_riga4"  style="font-size:25px" value="' + result[0].Field248 + '"></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga4_grassetto"><option value="N">N</option><option style="font-weight:bold;" value="G">G</option></select></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga4_allineamento"><option value="x--">x--</option><option value="-x-">-x-</option><option value="--x">--x</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';



                            var test1 = result[0].Field327 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Numero Tavolo</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="numero_tavolo_scontrino" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="margin-bottom:30px;font-size:30px">URL QR Code</div><div class="col-md-7 col-xs-7"><input maxlength="256" class="form-control" type="text" name="url_qr_code"  style="font-size:25px" value="' + result[0].Field251 + '"></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field360 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Divisione per Persone esclude Divisione per Coperti</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="divisione_persone_esclude_divisione_coperti"' + test1 + ' ></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field500 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Barcode&nbsp;<a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'BARCODE\', \'Abilitando questo settaggio comparir&agrave; il barcode a pi&egrave; pagina dello scontrino. Questo servir&agrave; per ri-aprire la comanda dal forno utilizzando lo scanner.\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="barcode_riapertura_ordine_scontrino" style="font-size:25px" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'SCONTRINO\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';

                            $('#form_settaggi').html(form_settaggi);

                            $('#form_settaggi select[name="msg_riga1_grassetto"] option:containsExact("' + result[0].Field241 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga1_allineamento"] option:containsExact("' + result[0].Field242 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga2_grassetto"] option:containsExact("' + result[0].Field243 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga2_allineamento"] option:containsExact("' + result[0].Field244 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga3_grassetto"] option:containsExact("' + result[0].Field245 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga3_allineamento"] option:containsExact("' + result[0].Field246 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga4_grassetto"] option:containsExact("' + result[0].Field249 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga4_allineamento"] option:containsExact("' + result[0].Field250 + '")').attr('selected', true);

                            break;

                        case "ANAGRAFICA PRODOTTI":

                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-12 col-xs-12" style="font-size:30px"><h3 style="text-decoration:underline">PRODOTTI:</h3></div>';

                            var test1 = result[0].Field330 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Descrizione</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="descrizione" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field331 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Spazio Forno</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="spazio_forno" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field332 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Prezzo Default</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="prezzo_normale" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field333 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Prezzo Maxi</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="prezzo_maxi" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field334 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Prezzo Bar</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="prezzo_bar" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field335 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Prezzo Asporto</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="prezzo_asporto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';



                            var test1 = result[0].Field336 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Prezzo Tav. Continuo</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="prezzo_continuo" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field337 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Prezzo 1/4 Metro</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="prezzo_metro" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field338 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Unit&agrave; di misura</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="unita_misura" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field339 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Prezzo per unit&agrave;</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="prezzo_unita" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field340 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Destinazione Stampa</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="destinazione_stampa" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field341 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Destinazione Stampa 2</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="destinazione_stampa_2" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field342 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Portata</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="portata" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field343 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Categoria Varianti</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="categoria_varianti" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field344 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Varianti Automatiche</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="varianti_automatiche" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field345 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">IVA Base / Servizi</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="iva_base" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field346 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">IVA Takeaway / Beni</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="iva_takeaway" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field347 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Riepilogo</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="riepilogo" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field348 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Codice Promozionale</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="codice_promozionale" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-12 col-xs-12" style="font-size:30px"><h3 style="text-decoration:underline">VARIANTI:</h3></div>';

                            var test1 = result[0].Field349 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Prezzo Poco</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="prezzo_poco" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field350 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Prezzo Variante Maxi</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="prezzo_variante_maxi" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field351 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Prezzo Varianti Aggiuntive</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="prezzo_varianti_aggiuntive" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field352 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Prezzo Maxi Aggiuntive</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="prezzo_maxi_aggiuntive" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            /*                                             var test1 = result[0].Field349 == "true" ? "checked" : "";
                             form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">fast.order Abilitato</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="000" ' + test1 + '></div>\n\
                             </div>\n\
                             \n\
                             <div class="col-md-12 col-xs-12">';*/



                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'ANAGRAFICA PRODOTTI\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            $('#form_settaggi').html(form_settaggi);

                            break;

                    }
                    break;
                case "layout_visualizzazione":

                    switch (nome_settaggio) {

                        case "LAYOUT VISUALIZZAZIONE GENERICI":
                            form_settaggi = '<div class="editor_settaggi">\n\
                                                     <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;">Apertura Cassetto Fiscale:</div>';
                            form_settaggi += '</div>';

                            form_settaggi += '<div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field353 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-1 col-xs-1"></div><div class="col-md-4 col-xs-4" style="font-size:30px;">tasto su ordine</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="apertura_ordine" ' + test1 + '></div>';
                            form_settaggi += '</div>';

                            form_settaggi += '<div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field354 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-1 col-xs-1"></div><div class="col-md-4 col-xs-4" style="font-size:30px;">tasto su comanda</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="apertura_comanda" ' + test1 + '></div>';
                            form_settaggi += '</div>';

                            form_settaggi += '<div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field355 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-1 col-xs-1"></div><div class="col-md-4 col-xs-4" style="font-size:30px;">apertura su conto</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="apertura_conto" ' + test1 + '></div>';
                            form_settaggi += '</div>';

                            form_settaggi += '<div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field356 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-1 col-xs-1"></div><div class="col-md-4 col-xs-4" style="font-size:30px;">apertura su scontrino</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="apertura_scontrino" ' + test1 + '></div>';
                            form_settaggi += '</div>';


                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Ordine Articoli a Video&nbsp;<a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'ORDINE ARTICOLI A VIDEO\', \'Selezionando &quot;Alfabetico&quot; gli articoli che verranno visualizzati su &quot;Lente Pi&ugrave;&quot; verranno visualizzati in ordine alfabetico. Selezionando &quot;Battitura&quot; verranno visualizzati in ordine di inserimento.<br><br>La stampa della comanda e del conto sar&agrave; visualizzata SEMPRE in ordine alfabetico.\')">?</a></div><div class="col-md-3 col-xs-3"><select style="font-size:25px" class="form-control nopadding" name="ordine_articoli_video"><option value="A">Alfabetico</option><option value="B">Battitura</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';


                            var test1 = result[0].Field372 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Consumazione In Pizzeria <a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'CONSUMAZIONE IN PIZZERIA\', \'Abilitando questa impostazione, al momento dell&rsquo;ordine comparir&agrave;, oltre all&rsquo;icona di consegna a domicilio e ritiro da asporto, l&rsquo;icona che identifica che il cliente la consuma in pizzeria.\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="consumazione_in_pizzeria" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field209 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Tasto Pizza al Metro</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="tasto_pizza_metro" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field328 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Tasto Pizza Mezzo Metro</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="tasto_pizza_mezzo_metro" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Prezzo Metro: </div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_metro"  style="text-align:center;font-size:25px" value="' + result[0].Field210 + '"></div><div class="col-md-2 col-xs-2" style="padding-top:10px;font-size:20px">Es: 20.00</div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" placeholder="4" name="spazio_metro"  style="text-align:center;font-size:25px" value="' + result[0].Field254 + '"></div><div class="col-md-1 col-xs-1" style="padding-top:10px;font-size:20px"><a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'UN METRO DI PIZZA\', \'Nel primo campo sulla sinistra va indicato il prezzo base della pizza al metro.<br><br>Nel campo a destra va indicato lo spazio che occupa un metro di pizza all interno del forno; di default &egrave; 4.\')">?</a></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Prezzo Mezzo Metro: </div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_mezzo_metro"  style="text-align:center;font-size:25px" value="' + result[0].Field211 + '"></div><div class="col-md-2 col-xs-2" style="padding-top:10px;font-size:20px">Es: 12.00</div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" placeholder="2" name="spazio_mezzo_metro"  style="text-align:center;font-size:25px" value="' + result[0].Field255 + '"></div><div class="col-md-1 col-xs-1" style="padding-top:10px;font-size:20px"><a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'MEZZO METRO DI PIZZA\', \'Nel primo campo sulla sinistra va indicato il prezzo base di mezzo metro di pizza.<br><br>Nel campo a destra va indicato lo spazio che occupa mezzo metro di pizza all interno del forno; di default &egrave; 2.\')">?</a></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field212 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Tasto 1/4</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="tasto_un_quarto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field311 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Pizza Maxi</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="tasto_pizza_maxi" ' + test1 + '></div><div class="col-md-3 col-xs-3" style="font-size:20px">Totale capienza forno: </div><div class="col-md-2 col-xs-2"><input class="form-control kb_num" type="number" name="capienza_pizza_maxi"  style="text-align:center;font-size:25px" value="' + result[0].Field312 + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field326 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Pizza Due Gusti</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="tasto_pizza_duegusti" ' + test1 + '></div>\n\
                            <div class="col-md-2 col-xs-2" style="padding-top:10px;font-size:20px;font-weight:bold;cursor:pointer;">Destinazione di Stampa&nbsp;<a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'Destinazione di Stampa\', \'Dalla tendina selezione la destinazione di stampa.\')">?</a></div><div class="col-md-4 col-xs-4"id="tendine_due_gusti_settaggio"></div></div>\n\
                                                    <div class="col-md-12 col-xs-12">'
                                                    




                            /*var test1 = result[0].Field501 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Tasto Schiacciata</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="tasto_schiacciata" ' + test1 + '></div>\n\
                                            </div>\n\
                                            \n\
                                            <div class="col-md-12 col-xs-12">';*/
                            var test1 = result[0].Field182 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Riquadro "Scooter"</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="riquadro_scooter" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">'
                            var test1 = result[0].Field473 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-1 col-xs-1"></div><div class="col-md-4 col-xs-4" style="font-size:30px;">abilita chiusura ordine&nbsp;<a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'ABILITA CHIUSURA ORDINE\', \'Abilitando questo settaggio, alla destra di ogni riga dell ordine nella sezione domicilio si visualizzer&agrave; il bottone per poter rendere chiuso, quindi consegnato, l ordine.\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="abilita_chiusura_ordine" ' + test1 + '></div>';
                            form_settaggi += '</div>'
                            var test1 = result[0].Field508 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-1 col-xs-1"></div><div class="col-md-4 col-xs-4" style="font-size:30px;">QR Code Fattorini<a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'QR FATTORINO\', \'Abilitando questa impostazione comparir&agrave; nella sezione DOMICILIO un icona che consentir&agrave di scannerizzare con il lettore il qrcode del fattorino e successivamente i codici a barre delle comande da assegnarli.\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="qr_code_fattorini" ' + test1 + '></div>';
                            form_settaggi += '</div>\n\
                            <div class="col-md-12 col-xs-12">';


                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Tipo Orario Forno<a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'TIPO ORARIO FORNO\', \'\')">?</a></div><div class="col-md-3 col-xs-3"><select style="font-size:25px" class="form-control nopadding" name="tipo_orario_forno"><option value="C">Ora Consegna</option><option value="P">Orario Preparazione</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Capienza forno da: &nbsp;<a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'CAPIENZA DEL FORNO\', \'Le fasce della capienza del forno sono VERDE, GIALLO e ROSSO.<br><br>Le righe si coloreranno in base alla quantita di pizze inserite per ogni fascia oraria.<br><br>Per impostare la colorazione delle righe, indicare:<br><ul><li>Il numero di pizze di partenza per avere le righe gialle nel campo giallo.</li><li>Il numero di pizze di partenza per avere le righe rosse nel campo rosso</li><li>Il verde si auto-imposter&agrave; in base a ci&ograve; che &egrave; stato inserito nel campo giallo.</li></ul><br>Esempio:<br><br>Se all interno del rettangolo giallo inserisco il numero 6, e nel rettangolo rosso inserisco il 9, le fasce saranno:<br><ul><li>Da 1 a 5 pizze VERDE</li><li>Da 6 a 8 pizze GIALLO</li><li>Da 9 pizze in poi ROSSO</li></ul>\')">?</a></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="giallo_forno"  style="text-align: center;font-size:25px;border: 2px solid #c87f0a;" value="' + result[0].Field184 + '" placeholder="6"></div><div class="col-md-2 col-xs-2"  ><input maxlength="42" class="form-control kb_num" type="number" name="rosso_forno"  style="text-align: center;font-size:25px;border: 2px solid #d62c1a;" value="' + result[0].Field183 + '" placeholder="9"></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Totale capienza forno: </div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="totale_capienza_forno"  style="text-align:center;font-size:25px" value="' + result[0].Field185 + '"></div><div class="col-md-3 col-xs-3" style="padding-top:10px;font-size:20px">Esempio: 10</div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field186 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Avviso supero capienza</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="avviso_supero_capienza" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Tipi Impasto:</div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field213 == "true" ? "checked" : "";
                            var query= "select * from prezzi_impasti_maxi where id= 1"
                            var test2 = alasql(query);
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:65px;">Kamut</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="pasta_kamut" ' + test1 + '></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_kamut"  style="text-align:center;font-size:25px" value="' + result[0].Field220 + '"></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_kamut_maxi" placeholder="MAXI"  style="text-align:center;font-size:25px" value="' +test2[0].kumat + '"></div><div class="col-xs-2" style="padding-top:10px;font-size:20px">Esempio: 2.50</div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field219 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:65px;">Farro</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="pasta_farro" ' + test1 + '></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_farro"  style="text-align:center;font-size:25px" value="' + result[0].Field226 + '"></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_farro_maxi"   style="text-align:center;font-size:25px" value="' + test2[0].farro + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field214 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:65px;">Soja</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="pasta_soja" ' + test1 + '></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_soja"  style="text-align:center;font-size:25px" value="' + result[0].Field221 + '"></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_soja_maxi"   style="text-align:center;font-size:25px" value="' + test2[0].soja+ '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field215 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:65px;">Integrale</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="pasta_integrale" ' + test1 + '></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_integrale"  style="text-align:center;font-size:25px" value="' + result[0].Field222 + '"></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_integrale_maxi"   style="text-align:center;font-size:25px" value="' + test2[0].integrale + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field216 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:65px;">Pasta Madre</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="pasta_madre" ' + test1 + '></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_madre"  style="text-align:center;font-size:25px" value="' + result[0].Field223 + '"></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_madre_maxi"   style="text-align:center;font-size:25px" value="' + test2[0].pasta_madre + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field217 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:65px;">5 Cereali</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="pasta_5_cereali" ' + test1 + '></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_5_cereali"  style="text-align:center;font-size:25px" value="' + result[0].Field224 + '"></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_5_cereali_maxi"   style="text-align:center;font-size:25px" value="' + test2[0].ch_cerali + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field437 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:65px;">7 Cereali</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="pasta_7_cereali" ' + test1 + '></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_7_cereali"  style="text-align:center;font-size:25px" value="' + result[0].Field439 + '"></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_7_cereali_maxi"   style="text-align:center;font-size:25px" value="' + test2[0].set_cerali + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field218 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:65px;">Senza Glutine</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="pasta_senza_glutine" ' + test1 + '></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_senza_glutine"  style="text-align:center;font-size:25px" value="' + result[0].Field225 + '"></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_senza_glutine_maxi"   style="text-align:center;font-size:25px" value="' + test2[0].senza_glutine + '"></div>\n\
                                                    </div>\n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field438 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:65px;">Carbone</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="pasta_carbone" ' + test1 + '></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_carbone"  style="text-align:center;font-size:25px" value="' + result[0].Field440 + '"></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_carbone_maxi"   style="text-align:center;font-size:25px" value="' + test2[0].carbone + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';



                            var test1 = result[0].Field441 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:65px;">Napoli</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="pasta_napoli" ' + test1 + '></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_napoli"  style="text-align:center;font-size:25px" value="' + result[0].Field442 + '"></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_napoli_maxi"   style="text-align:center;font-size:25px" value="' + test2[0].napoli + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field443 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:65px;">Doppia Pasta</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="pasta_doppia_pasta" ' + test1 + '></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_doppia_pasta"  style="text-align:center;font-size:25px" value="' + result[0].Field444 + '"></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_doppia_pasta_maxi"   style="text-align:center;font-size:25px" value="' + test2[0].doppia_pasta + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field445 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:65px;">Battuta</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="pasta_battuta" ' + test1 + '></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_battuta"  style="text-align:center;font-size:25px" value="' + result[0].Field446 + '"></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_battuta_maxi"   style="text-align:center;font-size:25px" value="' + test2[0].batutta + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field447 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:65px;">Battuta Farcita</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="pasta_battuta_farcita" ' + test1 + '></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_battuta_farcita"  style="text-align:center;font-size:25px" value="' + result[0].Field448 + '"></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_battuta_farcita_maxi"   style="text-align:center;font-size:25px" value="' + test2[0].batutta_farcita + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field449 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:65px;">Enkir</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="pasta_enkir" ' + test1 + '></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_enkir"  style="text-align:center;font-size:25px" value="' + result[0].Field450 + '"></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_enkir_maxi"   style="text-align:center;font-size:25px" value="' + test2[0].enkir + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field451 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:65px;">Canapa</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="pasta_canapa" ' + test1 + '></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_canapa"  style="text-align:center;font-size:25px" value="' + result[0].Field452 + '"></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_canapa_maxi"   style="text-align:center;font-size:25px" value="' + test2[0].canapa + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field453 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:65px;">Grano Arso</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="pasta_grano_arso" ' + test1 + '></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_grano_arso"  style="text-align:center;font-size:25px" value="' + result[0].Field454 + '"></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_grano_arso_maxi"   style="text-align:center;font-size:25px" value="' + test2[0].grana_arso + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field455 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:65px;">Manitoba</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="pasta_manitoba" ' + test1 + '></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_manitoba"  style="text-align:center;font-size:25px" value="' + result[0].Field456 + '"></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_manitoba_maxi"   style="text-align:center;font-size:25px" value="' + test2[0].manitoba + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field457 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:65px;">Patate</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="pasta_patate" ' + test1 + '></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_patate"  style="text-align:center;font-size:25px" value="' + result[0].Field458 + '"></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_patate_maxi"   style="text-align:center;font-size:25px" value="' + test2[0].patate + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field459 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:65px;">Pinsa Romana</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="pasta_pinsa_romana" ' + test1 + '></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_pinsa_romana"  style="text-align:center;font-size:25px" value="' + result[0].Field460 + '"></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_pinsa_romana_maxi"   style="text-align:center;font-size:25px" value="' + test2[0].pinsa_romana + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field461 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:65px;">Fagioli</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="pasta_fagioli" ' + test1 + '></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_fagioli"  style="text-align:center;font-size:25px" value="' + result[0].Field462 + '"></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_fagioli_maxi"   style="text-align:center;font-size:25px" value="' + test2[0].fagioli + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field463 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:65px;">Buratto</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="pasta_buratto_tipo2" ' + test1 + '></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_buratto_tipo2"  style="text-align:center;font-size:25px" value="' + result[0].Field464 + '"></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_buratto_tipo2_maxi"   style="text-align:center;font-size:25px" value="' + test2[0].buratto + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field474 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:65px;"><input maxlength="42" class="form-control" type="text" name="testo_pasta_personalizzata_1"  style="text-align:left;font-size:25px" value="' + result[0].Field475 + '" placeholder="Impasto Personalizzato"></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="check_pasta_personalizzata_1" ' + test1 + '></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_personalizzata_1"  style="text-align:center;font-size:25px" value="' + result[0].Field476 + '"></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_personalizzata_1_maxi"   style="text-align:center;font-size:25px" value="' + test2[0].testo_pasta_pers_1 + '"></div><div class="col-md-1 col-xs-1" style="font-size:30px;"><input maxlength="2" class="form-control" type="text" name="abbreviazione_pasta_personalizzata_1"  style="text-align:left;font-size:25px" value="' + result[0].Field477 + '" placeholder="Abbr."></div><div class="col-md-1 col-xs-1"><a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" onclick="popup_spiegazione(\'IMPASTO PERSONALIZZATO\', \'In questi campi andranno inseriti il nome dell impasto personalizzato, la scelta se abilitare o meno il campo, il prezzo e l abbreviazione che comparir&agrave; come bottone nel popup di modifica articolo assieme ai tasti di modifica impasto.\')">?</a></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field478 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:65px;"><input maxlength="42" class="form-control" type="text" name="testo_pasta_personalizzata_2"  style="text-align:left;font-size:25px" value="' + result[0].Field479 + '" placeholder="Impasto Personalizzato"></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="check_pasta_personalizzata_2" ' + test1 + '></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_personalizzata_2"  style="text-align:center;font-size:25px" value="' + result[0].Field480 + '"></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_personalizzata_2_maxi"   style="text-align:center;font-size:25px" value="' + test2[0].testo_pasta_pers_2 + '"></div><div class="col-md-2 col-xs-2" style="font-size:30px;"><input maxlength="2" class="form-control" type="text" name="abbreviazione_pasta_personalizzata_2"  style="text-align:left;font-size:25px" value="' + result[0].Field481 + '" placeholder="Abbr."></div><div class="col-md-1 col-xs-1"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field482 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:65px;"><input maxlength="42" class="form-control" type="text" name="testo_pasta_personalizzata_3"  style="text-align:left;font-size:25px" value="' + result[0].Field483 + '" placeholder="Impasto Personalizzato"></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="check_pasta_personalizzata_3" ' + test1 + '></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_personalizzata_3"  style="text-align:center;font-size:25px" value="' + result[0].Field484 + '"></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_personalizzata_3_maxi"   style="text-align:center;font-size:25px" value="' + test2[0].testo_pasta_pers_3 + '"></div><div class="col-md-2 col-xs-2" style="font-size:30px;"><input maxlength="2" class="form-control" type="text" name="abbreviazione_pasta_personalizzata_3"  style="text-align:left;font-size:25px" value="' + result[0].Field485 + '" placeholder="Abbr."></div><div class="col-md-1 col-xs-1"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field486 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:65px;"><input maxlength="42" class="form-control" type="text" name="testo_pasta_personalizzata_4"  style="text-align:left;font-size:25px" value="' + result[0].Field487 + '" placeholder="Impasto Personalizzato"></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="check_pasta_personalizzata_4" ' + test1 + '></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_personalizzata_4"  style="text-align:center;font-size:25px" value="' + result[0].Field488 + '"></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_personalizzata_4_maxi"   style="text-align:center;font-size:25px" value="' + test2[0].testo_pasta_pers_4 + '"></div><div class="col-md-2 col-xs-2" style="font-size:30px;"><input maxlength="2" class="form-control" type="text" name="abbreviazione_pasta_personalizzata_4"  style="text-align:left;font-size:25px" value="' + result[0].Field489 + '" placeholder="Abbr."></div><div class="col-md-1 col-xs-1"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field490 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:65px;"><input maxlength="42" class="form-control" type="text" name="testo_pasta_personalizzata_5"  style="text-align:left;font-size:25px" value="' + result[0].Field491 + '" placeholder="Impasto Personalizzato"></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="check_pasta_personalizzata_5" ' + test1 + '></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_personalizzata_5"  style="text-align:center;font-size:25px" value="' + result[0].Field492 + '"></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_personalizzata_5_maxi"   style="text-align:center;font-size:25px" value="' + test2[0].testo_pasta_pers_5 + '"></div><div class="col-md-2 col-xs-2" style="font-size:30px;"><input maxlength="2" class="form-control" type="text" name="abbreviazione_pasta_personalizzata_5"  style="text-align:left;font-size:25px" value="' + result[0].Field493 + '" placeholder="Abbr."></div><div class="col-md-1 col-xs-1"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field494 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:65px;"><input maxlength="42" class="form-control" type="text" name="testo_pasta_personalizzata_6"  style="text-align:left;font-size:25px" value="' + result[0].Field495 + '" placeholder="Impasto Personalizzato"></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="check_pasta_personalizzata_6" ' + test1 + '></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_personalizzata_6"  style="text-align:center;font-size:25px" value="' + result[0].Field496 + '"></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control kb_num" type="number" name="prezzo_pasta_personalizzata_6_maxi"   style="text-align:center;font-size:25px" value="' + test2[0].testo_pasta_pers_6 + '"></div><div class="col-md-2 col-xs-2" style="font-size:30px;"><input maxlength="2" class="form-control" type="text" name="abbreviazione_pasta_personalizzata_6"  style="text-align:left;font-size:25px" value="' + result[0].Field497 + '" placeholder="Abbr."></div><div class="col-md-1 col-xs-1"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';


                            var test1 = result[0].Field309 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Visualizzazione IVA &nbsp;<a ' + comanda.evento + '="popup_spiegazione(\'VISUALIZZAZIONE IVA\', \'Qualora questo settaggio sia abilitato, visualizzerai l IVA a fianco all articolo ordinato.\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="iva_esposta" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    </div>\n\
                                                    \n\
                                                    <br/>\n\
                                                    <div class="col-md-12 col-xs-12">';


                            var test1 = result[0].Field509 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Tessere A punti</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="codice_tessera_clienti" ' + test1 + '></div>\n\
                                                        </div>\n\
                                                    <div class="col-md-12 col-xs-12">';


                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'LAYOUT VISUALIZZAZIONE GENERICI\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            //--------------------------------------------------------------------//


                            $('#form_settaggi').html(form_settaggi);

                            let tendina = '';
                            tendina += '<div class="linea_due_gusti"><div class="col-md-8 col-xs-8"><select class="linea_due_gusti form-control" name ="tendine_due_gusti_settaggio">';
                            let id_arr = alasql("Select * from nomi_stampanti WHERE cast(numero as int) >= 50;");
                            let due_guisti_stampante = alasql("Select Field511 from settaggi_profili");

                            id_arr.forEach((v) => {
                                if (due_guisti_stampante[0].Field511 === v.numero) {
                                    tendina += '<option value="' + v.numero + '" selected>' + v[comanda.lingua] + '</option>';
                                } else {
                                    tendina += '<option value="' + v.numero + '">' + v[comanda.lingua] + '</option>';
                                }

                               

                            });

                            tendina += '</select></div>'

                            $("#tendine_due_gusti_settaggio").append(tendina);



                            if (result[0].Field395 === "A") {
                                result[0].Field395 = "Alfabetico";
                            } else if (result[0].Field395 === "B") {
                                result[0].Field395 = "Battitura";
                            }
                            $('#form_settaggi select[name="ordine_articoli_video"] option:containsExact("' + result[0].Field395 + '")').attr('selected', true);

                            $('#form_settaggi select[name="tipo_orario_forno"] option[value="' + result[0].Field468 + '"]').attr('selected', true);
                            break;
                        case "TASTI INCASSO":
                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field313 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Tasto Sconto</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="tasto_sconto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field314 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Tasto Resto</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="tasto_resto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field315 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Tasto Buoni Pasto</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="tasto_buonipasto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field316 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Tasto Conto + Comanda</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="tasto_contopiucomanda" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field317 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Tasto Fattura + Comanda</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="tasto_fatturapiucomanda" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field318 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Tasto Scontrino + Comanda</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="tasto_scontrinopiucomanda" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field319 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Tasto Scontrino + Conto</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="tasto_scontrinopiuconto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field320 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Tasto Conto</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="tasto_conto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field321 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Tasto Fattura</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="tasto_fattura" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field322 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Tasto Scontrino</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="tasto_scontrino" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field421 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Tasto Non Pagato</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="tasto_nonpagato" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field364 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;">Tasto Fiscali</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="operazioni" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field200 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;">Tasto Pagamenti</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="pagamenti" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field204 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;">Tasto Storicizza</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="storicizza" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';



                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'TASTI INCASSO\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';

                            $('#form_settaggi').html(form_settaggi);
                            //--------------------------------------------------------------------//
                            break;

                        case "VISORE ASPORTO":

                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';


                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Testo Prima Riga</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="testo_riga1"  style="font-size:25px" value="' + result[0].Field415 + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Testo Seconda Riga</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="testo_riga2"  style="font-size:25px" value="' + result[0].Field416 + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-4 col-xs-4" style="font-size:30px">che compare dopo </div><div class="col-md-1 col-xs-1"><input maxlength="42" class="form-control" type="text" name="numero_secondi_visore"  style="font-size:25px;padding-left: 5px;padding-right: 5px;text-align: center;" value="' + result[0].Field417 + '"></div><div class="col-md-7 col-xs-7" style="font-size:30px">secondi dall&rsquo;emissione del documento commerciale</div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-4 col-xs-4" style="font-size:30px">&nbsp;</div><div class="col-md-1 col-xs-1"></div><div class="col-md-7 col-xs-7" style="font-size:30px"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';


                            var test1 = result[0].Field418 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Testo Totale su Visore</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="tasto_totale_visore" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-4 col-xs-4" style="font-size:30px">che compare per </div><div class="col-md-1 col-xs-1"><input maxlength="42" class="form-control" type="text" name="numero_secondi_totale_visore"  style="font-size:25px;padding-left: 5px;padding-right: 5px;text-align: center;" value="' + result[0].Field419 + '"></div><div class="col-md-7 col-xs-7" style="font-size:30px">secondi dal click sul simbolo dell&rsquo; occhio</div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';


                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'VISORE ASPORTO\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';

                            $('#form_settaggi').html(form_settaggi);

                            break;
                    }
                    break;
                case "scontistiche":
                    switch (nome_settaggio) {
                        case "SCONTISTICHE_SCONTO_TOTALE":

                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[201] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga1"  style="font-size:25px" value="' + result[0].Field256 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga1_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field257 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spaziointestazione1" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[203] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga2"  style="font-size:25px" value="' + result[0].Field258 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga2_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field259 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spaziointestazione2" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[204] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga3"  style="font-size:25px" value="' + result[0].Field260 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga3_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field261 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spaziointestazione3" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[205] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga4"  style="font-size:25px" value="' + result[0].Field262 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga4_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field263 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[206] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="data" ' + test1 + '></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="font_data"><option value="N">Normale</option><option value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Scadenza Sconto<a style="padding-left:1vw;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'SCADENZA SCONTO\', \'Selezionare dalla tendina il periodo e nel campo alla sinistra inserire il numero di giorni, mesi o anni.<br><br>Se dalla tendina viene selezionato NESSUNO significa che gli sconti non avranno scadenza.\')">?</a></div><div class="col-md-2 col-xs-2"><input maxlength="2" class="form-control" type="number" name="valore_scadenza"  style="font-size:25px" value="' + result[0].Field280 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intervallo_scadenza"><option value="Niente">Niente</option><option value="Giorni">Giorni</option><option value="Mesi">Mesi</option><option value="Anni">Anni</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12"  style="font-size:30px;padding-top:3px;">';

                            form_settaggi += '<div class="col-md-12 col-xs-12" style="font-size:30px">Tipologie Sconto:<a style="padding-left:1vw;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'TIPOLOGIE SCONTO\', \'Si possono stabilire 4 fasce di scontistiche per prezzo totale differenti, e decidere la percentuale di sconto e l eventuale omaggio.<br><br><ul><li>Nel primo campo va inserito il totale di partenza nel quale applicare la percentuale di sconto e l omaggio.</li><li>Nel secondo campo viene inserito il totale massimo per quella fascia di sconto.</li><li>Nel terzo campo va indicata l eventuale percentuale di sconto da applicare.</li><li>Nel quarto campo viene inserito l eventuale omaggio, a parole.</li></ul><br><br>NB: Nel primo campo il totale dev essere maggiore al valore inserito, mentre nel secondo campo il totale dev essere minore o uguale al valore inserito. Se il secondo campo &egrave; vuoto significa che dal primo valore in poi si applica lo sconto.\')">?</a></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12"  style="font-size:30px;padding-top:3px;">';

                            form_settaggi += '<div class="col-md-1 col-xs-1 col-xs-offset-2">Da</div><div class="col-md-1 col-xs-1"><input maxlength="4" class="form-control" type="text" name="valore_euro_da_1"  style="text-align:center;padding:2px;font-size:25px" value="' + result[0].Field282 + '"></div><div class="col-md-1 col-xs-1" style="text-align:center">a</div><div class="col-md-1 col-xs-1"><input maxlength="4" class="form-control" type="text" name="valore_euro_a_1"  style="text-align:center;padding:2px;font-size:25px" value="' + result[0].Field283 + '"></div><div class="col-md-3 col-xs-3" style="text-align:center">&euro; sconto del</div><div class="col-md-1 col-xs-1"><input maxlength="4" class="form-control" type="text" name="valore_sconto_1"  style="text-align:center;padding:2px;font-size:25px" value="' + result[0].Field284 + '"></div><div class="col-md-1 col-xs-1">%</div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12"  style="font-size:30px;padding-top:3px;">';

                            form_settaggi += '<div class="col-md-1 col-xs-1 col-xs-offset-2">Da</div><div class="col-md-1 col-xs-1"><input maxlength="4" class="form-control" type="text" name="valore_euro_da_2"  style="text-align:center;padding:2px;font-size:25px" value="' + result[0].Field286 + '"></div><div class="col-md-1 col-xs-1" style="text-align:center">a</div><div class="col-md-1 col-xs-1"><input maxlength="4" class="form-control" type="text" name="valore_euro_a_2"  style="text-align:center;padding:2px;font-size:25px" value="' + result[0].Field287 + '"></div><div class="col-md-3 col-xs-3" style="text-align:center">&euro; sconto del</div><div class="col-md-1 col-xs-1"><input maxlength="4" class="form-control" type="text" name="valore_sconto_2"  style="text-align:center;padding:2px;font-size:25px" value="' + result[0].Field288 + '"></div><div class="col-md-1 col-xs-1">%</div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12"  style="font-size:30px;padding-top:3px;">';

                            form_settaggi += '<div class="col-md-1 col-xs-1 col-xs-offset-2">Da</div><div class="col-md-1 col-xs-1"><input maxlength="4" class="form-control" type="text" name="valore_euro_da_3"  style="text-align:center;padding:2px;font-size:25px" value="' + result[0].Field290 + '"></div><div class="col-md-1 col-xs-1" style="text-align:center">a</div><div class="col-md-1 col-xs-1"><input maxlength="4" class="form-control" type="text" name="valore_euro_a_3"  style="text-align:center;padding:2px;font-size:25px" value="' + result[0].Field291 + '"></div><div class="col-md-3 col-xs-3" style="text-align:center">&euro; sconto del</div><div class="col-md-1 col-xs-1"><input maxlength="4" class="form-control" type="text" name="valore_sconto_3"  style="text-align:center;padding:2px;font-size:25px" value="' + result[0].Field292 + '"></div><div class="col-md-1 col-xs-1">%</div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12"  style="font-size:30px;padding-top:3px;">';

                            form_settaggi += '<div class="col-md-1 col-xs-1 col-xs-offset-2">Da</div><div class="col-md-1 col-xs-1"><input maxlength="4" class="form-control" type="text" name="valore_euro_da_4"  style="text-align:center;padding:2px;font-size:25px" value="' + result[0].Field294 + '"></div><div class="col-md-1 col-xs-1" style="text-align:center">a</div><div class="col-md-1 col-xs-1"><input maxlength="4" class="form-control" type="text" name="valore_euro_a_4"  style="text-align:center;padding:2px;font-size:25px" value="' + result[0].Field295 + '"></div><div class="col-md-3 col-xs-3" style="text-align:center">&euro; sconto del</div><div class="col-md-1 col-xs-1"><input maxlength="4" class="form-control" type="text" name="valore_sconto_4"  style="text-align:center;padding:2px;font-size:25px" value="' + result[0].Field296 + '"></div><div class="col-md-1 col-xs-1">%</div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12"  style="font-size:30px;padding-top:3px;">';


                            form_settaggi += '<div class="col-md-12 col-xs-12" style="font-size:30px">Giorni Sconto:<a style="padding-left:1vw;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'GIORNI SCONTO\', \'Spunta i giorni nei quali verranno emessi i buoni allegati allo scontrino.\')">?</a></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12 col-xs-offset-2"  style="font-size:30px;padding-top:3px;">';

                            var test1 = result[0].Field305.indexOf("1") !== -1 ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Luned&igrave;</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="lun" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12 col-xs-offset-2">';

                            var test1 = result[0].Field305.indexOf("2") !== -1 ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Marted&igrave;</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="mar" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12 col-xs-offset-2">';

                            var test1 = result[0].Field305.indexOf("3") !== -1 ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Mercoled&igrave;</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="mer" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12 col-xs-offset-2">';

                            var test1 = result[0].Field305.indexOf("4") !== -1 ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Gioved&igrave;</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="gio" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12 col-xs-offset-2">';

                            var test1 = result[0].Field305.indexOf("5") !== -1 ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Vened&igrave;</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="ven" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12 col-xs-offset-2">';

                            var test1 = result[0].Field305.indexOf("6") !== -1 ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Sabato</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="sab" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12 col-xs-offset-2">';

                            var test1 = result[0].Field305.indexOf("0") !== -1 ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Domenica</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="dom" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field306 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">QRCode Sconto:<a style="padding-left:1vw;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'QRCODE SCONTO\', \'Se spuntato, nello scontrino oltre al codice sconto apparir&agrave; il relativo QRCode.\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="qrcode_sconto" ' + test1 + '></div>\n\
                                     </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12"  style="font-size:30px;padding-top:3px;">';



                            form_settaggi += '<div class="col-md-5 col-xs-5">' + comanda.lang[215] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="msg_riga1"  style="font-size:25px" value="' + result[0].Field264 + '"></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga1_grassetto"><option value="N">N</option><option style="font-weight:bold;" value="G">G</option></select></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga1_allineamento"><option value="x--">x--</option><option value="-x-">-x-</option><option value="--x">--x</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field265 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spazio1" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[216] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="msg_riga2"  style="font-size:25px" value="' + result[0].Field266 + '"></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga2_grassetto"><option value="N">N</option><option style="font-weight:bold;" value="G">G</option></select></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga2_allineamento"><option value="x--">x--</option><option value="-x-">-x-</option><option value="--x">--x</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field267 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spazio2" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[217] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="msg_riga3"  style="font-size:25px" value="' + result[0].Field268 + '"></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga3_grassetto"><option value="N">N</option><option style="font-weight:bold;" value="G">G</option></select></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga3_allineamento"><option value="x--">x--</option><option value="-x-">-x-</option><option value="--x">--x</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';




                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'SCONTISTICHE_SCONTO_TOTALE\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            $('#form_settaggi').html(form_settaggi);
                            $('#form_settaggi select[name="intestazione_riga1_grassetto"] option[value="' + result[0].Field269 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="intestazione_riga2_grassetto"] option[value="' + result[0].Field270 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="intestazione_riga3_grassetto"] option[value="' + result[0].Field271 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="intestazione_riga4_grassetto"] option[value="' + result[0].Field272 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="font_data"] option[value="' + result[0].Field273 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="msg_riga1_grassetto"] option:containsExact("' + result[0].Field274 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga1_allineamento"] option:containsExact("' + result[0].Field275 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga2_grassetto"] option:containsExact("' + result[0].Field276 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga2_allineamento"] option:containsExact("' + result[0].Field277 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga3_grassetto"] option:containsExact("' + result[0].Field278 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga3_allineamento"] option:containsExact("' + result[0].Field279 + '")').attr('selected', true);
                            $('#form_settaggi select[name="intervallo_scadenza"] option:containsExact("' + result[0].Field281 + '")').attr('selected', true);
                            break;
                    }
                    break;



                case "fastorder":
                    switch (nome_settaggio) {
                        case "FASTORDER":

                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">URL fast.order</div><div class="col-md-5 col-xs-5"><input class="form-control" type="text" name="url_fastorder"  style="font-size:25px" value="' + result[0].Field502 + '" placeholder="es. http://fastorder.club/Profilo/"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field506 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Categorie Intellinet SI / Importazione NO</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="categorie_come_intellinet" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field507 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Modifica Prezzi in Aggiornamento</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="modifica_prezzi_fastorder" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Esporta Prodotti</div><div class="col-md-1 col-xs-1"><button class="btn btn-default btn-warning" type="button" onclick="fastorder.export_products_fastorder()">Esporta</button</div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';




                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'FASTORDER\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            $('#form_settaggi').html(form_settaggi);



                            break;
                    }
                    break;
                    //SETAGGIO PER POS IN ASPORTO.
                    case "set_pos":
                    switch (nome_settaggio) {
                        case "POS_1":

                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field234 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">POS Abilitato</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="pos_abilitato" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="margin-bottom:10px;font-size:30px">IP POS</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="ip_pos"  style="font-size:25px" value="' + result[0].Field231 + '" placeholder="xxx.xxx.xxx.xxx"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="margin-bottom:10px;font-size:30px">PORTA POS</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="porta_pos"  style="font-size:25px" value="' + result[0].Field232 + '" placeholder="es. 10000"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="margin-bottom:10px;font-size:30px">ID TERM</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="id_term"  style="font-size:25px" value="' + result[0].Field233 + '" placeholder="8 cifre numeriche"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field235 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">Ricevuta POS su Scontrino</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="ricevuta_pos_scontrino" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'POS_1\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            $('#form_settaggi').html(form_settaggi);
                            break;
                        case "POS_2":

                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field400 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">POS Abilitato</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="pos_abilitato" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="margin-bottom:10px;font-size:30px">IP POS</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="ip_pos"  style="font-size:25px" value="' + result[0].Field401 + '" placeholder="xxx.xxx.xxx.xxx"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="margin-bottom:10px;font-size:30px">PORTA POS</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="porta_pos"  style="font-size:25px" value="' + result[0].Field402 + '" placeholder="es. 10000"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="margin-bottom:10px;font-size:30px">ID TERM</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="id_term"  style="font-size:25px" value="' + result[0].Field403 + '" placeholder="8 cifre numeriche"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field404 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">Ricevuta POS su Scontrino</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="ricevuta_pos_scontrino" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'POS_2\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            $('#form_settaggi').html(form_settaggi);
                            break;
                        case "POS_3":

                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field405 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">POS Abilitato</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="pos_abilitato" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="margin-bottom:10px;font-size:30px">IP POS</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="ip_pos"  style="font-size:25px" value="' + result[0].Field406 + '" placeholder="xxx.xxx.xxx.xxx"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="margin-bottom:10px;font-size:30px">PORTA POS</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="porta_pos"  style="font-size:25px" value="' + result[0].Field407 + '" placeholder="es. 10000"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="margin-bottom:10px;font-size:30px">ID TERM</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="id_term"  style="font-size:25px" value="' + result[0].Field408 + '" placeholder="8 cifre numeriche"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field409 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">Ricevuta POS su Scontrino</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="ricevuta_pos_scontrino" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'POS_3\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            $('#form_settaggi').html(form_settaggi);
                            break;}
                            break;
            }
        });
    } else {
        comanda.sincro.query("SELECT * FROM settaggi_profili  where id=" + comanda.folder_number + "  LIMIT 1;", function (result) {

            for (var key in result[0]) {
                console.log("RESULT0", key, result[0], result[0][key]);
                if (result[0][key] === null) {
                    result[0][key] = '';
                }
            }
            switch (livello_superiore) {
                case "generici":
                    switch (nome_settaggio) {
                        case "PRINCIPALI":

                            let tabella_reparti_IVA = REPARTO_controller.tabella_reparti_IVA();

                            var testo_query = 'SELECT * FROM settaggi;';
                            comanda.sincro.query_locale(testo_query, function (risultato_settaggi_locali) {
                                comanda.sincro.query("SELECT * FROM impostazioni_fiscali  where id=" + comanda.folder_number + "  LIMIT 1;", function (result) {
                                    var testo_query = "SELECT id,ordinamento,descrizione,alias FROM categorie WHERE SUBSTR(descrizione,1,2)!=\"V.\" ORDER BY ordinamento ASC;";
                                    comanda.sincro.query(testo_query, function (result_categorie) {
                                        for (var key in result[0]) {
                                            console.log("RESULT0", key, result[0], result[0][key]);
                                            if (result[0][key] === null) {
                                                result[0][key] = '';
                                            }
                                        }

                                        comanda.sincro.query("SELECT * FROM dati_servizio where id='" + comanda.folder_number + "' LIMIT 1;", function (dati_servizio) {

                                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';
                                            var test1 = result[0].multiprofilo == "1" ? "checked" : "";
                                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Tendina Profili</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="multiprofilo" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[186] + '</div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="lingua_visualizzazione"><option value="I">Italiano</option><option value="T">Tedesco</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[187] + '</div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="lingua_stampa"><option value="italiano">Italiano</option><option value="deutsch">Deutsch</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[188] + '</div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="servizio_fino_alle"><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                            var test1 = result[0].righe_zero == "1" ? "checked" : "";
                                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[189] + '</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="righe_prezzo_zero" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                            var test1 = result[0].visualizzazione_coperti == "1" ? "checked" : "";
                                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[234] + '</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="visualizzazione_coperti" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[190] + '</div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control" type="number" name="valore_coperto"  style="font-size:25px" value="' + result[0].valore_coperto + '"></div><div class="col-md-3 col-xs-3" style="font-size:20px">Esempio: 1,00</div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                            var test1 = result[0].coperti_obbligatorio == "1" ? "checked" : "";
                                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[191] + '</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="coperti_obbligatori" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[192] + '</div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control" type="number" name="valore_servizio"  style="font-size:25px" value="' + result[0].valore_servizio + '"></div><div class="col-md-3 col-xs-3" style="font-size:20px">Esempio: 10</div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[193] + '</div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control" type="number" name="iva_default"  style="font-size:25px" value="' + result[0].aliquota_default + '"></div><div class="col-md-3 col-xs-3" style="font-size:20px">Esempio: 22</div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                            var test1 = result[0].iva_estera_abilitata == "1" ? "checked" : "";
                                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">P.IVA Estera Abilitata</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="iva_estera_abilitata" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Reparto Beni Default</div><div class="col-md-3 col-xs-3"><select class="form-control" name="reparto_beni_default"  style="font-size:25px;height: auto;">' + tabella_reparti_IVA + '</select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[194] + '</div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control" type="number" name="iva_takeaway"  style="font-size:25px" value="' + result[0].aliquota_takeaway + '"></div><div class="col-md-3 col-xs-3" style="font-size:20px">Esempio: 22</div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Reparto Servizi Default</div><div class="col-md-3 col-xs-3"><select class="form-control" name="reparto_servizi_default"  style="font-size:25px;height: auto;">' + tabella_reparti_IVA + '</select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">IVA Consegna</div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control" type="number" name="aliquota_consegna"  style="font-size:25px" value="' + result[0].aliquota_consegna + '"></div><div class="col-md-3 col-xs-3" style="font-size:20px">Esempio: 22</div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Reparto Consegna</div><div class="col-md-3 col-xs-3"><select class="form-control" name="reparto_consegna"  style="font-size:25px;height: auto;">' + tabella_reparti_IVA + '</select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[195] + '</div><div class="col-md-5 col-xs-5">';
                                            form_settaggi += '<select class="form-control nopadding" style="font-size:25.5px" name="scelta_categoria">';
                                            form_settaggi += '<option value="PREFERITI">PREFERITI (SE DISPONIBILE)</option>';

                                            result_categorie.forEach(function (value, index) {

                                                var descrizione = value.descrizione;

                                                if (descrizione.indexOf("PREFERITI") !== -1) {
                                                    descrizione = false;
                                                }

                                                if (typeof (descrizione) === "string") {
                                                    descrizione = descrizione.toUpperCase();
                                                    form_settaggi += '<option value="' + value.id + '">' + descrizione + '</option>';
                                                }



                                            });
                                            form_settaggi += '</select>';
                                            form_settaggi += '</div>';

                                            form_settaggi += '<div class="col-xs-2" style="font-size: 24px;padding:0;">(solo cellulare)</div>';


                                            form_settaggi += '</div>\n\
                                                                     \n\
                                                                    <div class="col-md-12 col-xs-12">';

                                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:60px">dei Tavoli:&nbsp;<a style="font-size:25px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'CATEGORIA PREDEFINITA\', \'Scegliere dalla tendina la categoria da visualizzare all apertura dei tavoli normali, bar, asporto o continuo.\')">?</a></div><div class="col-md-5 col-xs-5">';
                                            form_settaggi += '<select class="form-control nopadding" style="font-size:25.5px" name="scelta_categoria_tavoli">';

                                            result_categorie.forEach(function (value, index) {

                                                var descrizione = value.descrizione;

                                                if (descrizione.indexOf("PREFERITI") !== -1) {
                                                    descrizione = value.alias + " (PREFERITI)";
                                                }
                                                if (typeof (descrizione) === "string") {
                                                    descrizione = descrizione.toUpperCase();
                                                }

                                                form_settaggi += '<option value="' + value.id + '">' + descrizione + '</option>';

                                            });
                                            form_settaggi += '</select>';
                                            form_settaggi += '</div>';




                                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:60px">del Bar:</div><div class="col-md-5 col-xs-5">';
                                            form_settaggi += '<select class="form-control nopadding" style="font-size:25.5px" name="scelta_categoria_bar">';

                                            result_categorie.forEach(function (value, index) {

                                                var descrizione = value.descrizione;

                                                if (descrizione.indexOf("PREFERITI") !== -1) {
                                                    descrizione = value.alias + " (PREFERITI)";
                                                }
                                                if (typeof (descrizione) === "string") {
                                                    descrizione = descrizione.toUpperCase();
                                                }

                                                form_settaggi += '<option value="' + value.id + '">' + descrizione + '</option>';
                                            });
                                            form_settaggi += '</select>';
                                            form_settaggi += '</div>';




                                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:60px">dell\'Asporto:</div><div class="col-md-5 col-xs-5">';
                                            form_settaggi += '<select class="form-control nopadding" style="font-size:25.5px" name="scelta_categoria_asporto">';

                                            result_categorie.forEach(function (value, index) {

                                                var descrizione = value.descrizione;

                                                if (descrizione.indexOf("PREFERITI") !== -1) {
                                                    descrizione = value.alias + " (PREFERITI)";
                                                }
                                                if (typeof (descrizione) === "string") {
                                                    descrizione = descrizione.toUpperCase();
                                                }

                                                form_settaggi += '<option value="' + value.id + '">' + descrizione + '</option>';
                                            });
                                            form_settaggi += '</select>';
                                            form_settaggi += '</div>';




                                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;padding-left:60px">del Continuo:</div><div class="col-md-5 col-xs-5">';
                                            form_settaggi += '<select class="form-control nopadding" style="font-size:25.5px" name="scelta_categoria_continuo">';

                                            result_categorie.forEach(function (value, index) {

                                                var descrizione = value.descrizione;

                                                if (descrizione.indexOf("PREFERITI") !== -1) {
                                                    descrizione = value.alias + " (PREFERITI)";
                                                }
                                                if (typeof (descrizione) === "string") {
                                                    descrizione = descrizione.toUpperCase();
                                                }

                                                form_settaggi += '<option value="' + value.id + '">' + descrizione + '</option>';
                                            });
                                            form_settaggi += '</select>';
                                            form_settaggi += '</div>';




                                            form_settaggi += '</div>\n\
                                                                     \n\
                                                                    <div class="col-md-12 col-xs-12">';


                                            var test1 = result[0].fastorder == "1" ? "checked" : "";
                                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">fast.order</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="fastorder" ' + test1 + '></div></div>';
                                            form_settaggi += '<div class="col-md-12 col-xs-12">\n\
                                                        <div class="col-md-5 col-xs-5" style="font-size:30px">Mod. Articolo Conto</div><div class="col-md-3 col-xs-3"><select style="font-size:25px" class="form-control nopadding" name="touch_articolo_conto"><option value="1">Scheda Prodotto</option><option value="2">Solo Prezzo</option><option value="3">Nessuna</option></select></div>\n\
                                                </div>';

                                            var test1 = result[0].sconto_su_promo == "1" ? "checked" : "";
                                            form_settaggi += '<div class="col-md-12 col-xs-12">\n\
                                                        <div class="col-md-5 col-xs-5" style="font-size:30px">Sconto su Promo</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="sconto_su_promo" ' + test1 + '></div>\n\
                                                </div>';
                                            var test1 = result[0].articolo_a_peso == "1" ? "checked" : "";
                                            form_settaggi += '<div class="col-md-12 col-xs-12">\n\
                                                        <div class="col-md-5 col-xs-5" style="font-size:30px">Articolo a Peso</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="articolo_a_peso" ' + test1 + '></div>\n\
                                                </div>';
                                            form_settaggi += '<div class="col-md-12 col-xs-12">';
                                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'PRINCIPALI\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                                            $('#form_settaggi').html(form_settaggi);

                                            $('#form_settaggi select[name="reparto_servizi_default"] option[value="' + result[0].reparto_servizi_default + '"]').attr("selected", true);
                                            $('#form_settaggi select[name="reparto_beni_default"] option[value="' + result[0].reparto_beni_default + '"]').attr("selected", true);
                                            $('#form_settaggi select[name="reparto_consegna"] option[value="' + result[0].reparto_consegna + '"]').attr("selected", true);

                                            $('#form_settaggi select[name="touch_articolo_conto"] option[value="' + result[0].touch_articolo_conto + '"]').attr("selected", true);
                                            $('#form_settaggi select[name="lingua_visualizzazione"] option[value="' + '' + '"]').attr("selected", true);
                                            $('#form_settaggi select[name="lingua_stampa"] option[value="' + result[0].lingua_stampa + '"]').attr("selected", true);
                                            $('#form_settaggi select[name="servizio_fino_alle"] option[value="' + dati_servizio[0].ora_finale_servizio + '"]').attr("selected", true);
                                            $('#form_settaggi select[name="scelta_categoria"] option[value="' + result[0].categoria_partenza_mobile + '"]').attr("selected", true);

                                            comanda.funzionidb.nomi_categorie(function () {




                                                var cp_tavoli = "";
                                                var cp_bar = "";
                                                var cp_asporto = "";
                                                var cp_continuo = "";



                                                risultato_settaggi_locali.forEach(function (o, i) {

                                                    switch (o.nome) {

                                                        case "cp_tavoli":
                                                            $('#form_settaggi select[name="scelta_categoria_tavoli"] option[value="' + o.valore + '"]').attr("selected", true);
                                                            break;

                                                        case "cp_bar":
                                                            $('#form_settaggi select[name="scelta_categoria_bar"] option[value="' + o.valore + '"]').attr("selected", true);

                                                            break;

                                                        case "cp_asporto":
                                                            $('#form_settaggi select[name="scelta_categoria_asporto"] option[value="' + o.valore + '"]').attr("selected", true);

                                                            break;

                                                        case "cp_continuo":
                                                            $('#form_settaggi select[name="scelta_categoria_continuo"] option[value="' + o.valore + '"]').attr("selected", true);

                                                            break;

                                                    }

                                                });



                                            });
                                        });
                                    });
                                });
                            });
                            break;

                    }
                    break;
                case "stampe":
                    switch (nome_settaggio) {

                        case "COMANDA":
                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field425 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Ristampa intera su Asporto</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="ristampa_intera_asporto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field106 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[196] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="comandapiuconto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field23 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[197] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="comandadoppia" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'STAMPE_COMANDA\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            $('#form_settaggi').html(form_settaggi);
                            break;

                        case "COMANDA_ASPORTO":
                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field108 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[196] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="comandapiuconto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field109 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[197] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="comandadoppia" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            //MEMO SU COMANDA ASPORTO
                            var test1 = result[0].Field187 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Memo dopo comanda</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="memo_dopo_comanda" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'STAMPE_COMANDA_ASPORTO\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            $('#form_settaggi').html(form_settaggi);
                            break;

                        case "CONTO":
                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field107 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[197] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="contodoppio" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';


                            var test1 = result[0].Field413 === "true" ? "checked" : "";
                            if (result[0].Field414 === "true") {
                                test1 = "";
                            }
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Associa tavoli a stampante <a style="font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'ASSOCIA TAVOLI A STAMPANTE\', \'Abilitando questa impostazione, sulla sezione MENU > GESTIONI > STAMPANTI comparir&agrave; all&rsquo; interno delle stampanti CONTO un bottone che vi rimander&agrave; alla piantina dei tavoli. <br>Cliccando sul numero del tavolo, questo si associer&agrave; alla stampante CONTO desiderata. <br>I tavoli avranno colori diversi in base alla destinazione di stampa.\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="associa_tavoli_stampante" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';


                            var test1 = result[0].Field414 === "true" ? "checked" : "";

                            if (result[0].Field413 === "true") {
                                test1 = "";
                            }
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Scelta diretta stampante <a style="font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'SCELTA DIRETTA DELLA STAMPANTE\', \'Questa impostazione permetter&agrave; al cameriere di scegliere il nome della stampante su cui stampare il conto, al momento dell&rsquo; emissione dello stesso.\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="scelta_diretta_stampante" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field420 === "true" ? "checked" : "";


                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Conto + Comanda su Asporto <a style="font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'CONTO + COMANDA SU ASPORTO\', \'Questa impostazione permetter&agrave; di stampare anche la comanda, quando verr&agrave; premuto il tasto CONTO sul tavolo Asporto.\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="conto_comanda_asporto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'STAMPE_CONTO\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';



                            $('#form_settaggi').html(form_settaggi);
                            break;
                        case "INCASSI":
                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field426 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Venduti Doppia Copia</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="dettagliovendutidoppiacopia" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field428 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[199] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="incassipiudettagliovenduti" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'STAMPE_INCASSI\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';

                            $('#form_settaggi').html(form_settaggi);
                            break;
                        case "BUONI PASTO":
                            form_settaggi = '<div class="editor_settaggi">';
                            form_settaggi += '<div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field361 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[200] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="buoniacquistosunonfiscale" ' + test1 + '></div>';
                            form_settaggi += '</div>';
                            form_settaggi += '<div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field194 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Abilita Buono Acquisto Mono e Multi uso</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="abilita_buono_acquisto" ' + test1 + '></div>';
                            form_settaggi += '</div>';
                            form_settaggi += '<div class="col-md-12 col-xs-12">';
                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'STAMPE_BUONI_PASTO\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            $('#form_settaggi').html(form_settaggi);
                            break;
                        case "VARIE":

                            form_settaggi = '<div class="editor_settaggi">';

                            var results = alasql("SELECT " + comanda.lingua_stampa + " as nome,numero FROM nomi_stampanti WHERE cast(numero as int) >= 50;");

                            var opzione = "";

                            opzione += "<option value=\"STAMPANTE_COMPETENZA\">STAMPANTE DI COMPETENZA</option>";

                            results.forEach(function (row) {

                                opzione += "<option value=\"" + row.numero + "\">" + row.nome + "</option>";

                            });
                            opzione += "<option value=\"TUTTE\">TUTTE</option>";


                            form_settaggi += '<div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field387 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Cancellazione Articolo Lanciato in Comanda</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="cancellazione_articolo_lanciato_comanda" ' + test1 + '></div><div class="col-md-6 col-xs-6"><select style="font-size:25px" class="form-control nopadding" name="stampante_cancellazione_articolo">' + opzione + '</select></div>';
                            form_settaggi += '</div>';

                            /*form_settaggi += '<div class="col-md-12 col-xs-12">';
                             var test1 = result[0].Field388 === "true" ? "checked" : "";
                             form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Cancellazione Conto Lanciato in Comanda</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="cancellazione_conto_lanciato_comanda" ' + test1 + '></div>';
                             form_settaggi += '</div>';*/

                            var results = alasql("SELECT " + comanda.lingua_stampa + " as nome,numero FROM nomi_stampanti WHERE cast(numero as int) >= 50;");

                            var opzione = "";



                            results.forEach(function (row) {

                                var selected = "";
                                if (row.nome === "CONTO" + numero_layout || row.nome === "KONTO" + numero_layout) {
                                    selected = "selected";
                                }

                                opzione += "<option " + selected + " value=\"" + row.numero + "\">" + row.nome + "</option>";

                            });

                            opzione += "<option value=\"TUTTE\">TUTTE</option>";

                            form_settaggi += '<div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field389 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Sposta Articolo da/a Tavolo/Parcheggio</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="sposta_articolo" ' + test1 + '></div><div class="col-md-6 col-xs-6"><select style="font-size:25px" class="form-control nopadding" name="stampante_sposta_articolo">' + opzione + '</select></div>';
                            form_settaggi += '</div>';

                            form_settaggi += '<div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field390 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Sposta Tavolo/Parcheggio</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="sposta_tavolo" ' + test1 + '></div><div class="col-md-6 col-xs-6"><select style="font-size:25px" class="form-control nopadding" name="stampante_sposta_tavolo">' + opzione + '</select></div>';
                            form_settaggi += '</div>';

                            form_settaggi += '<div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field391 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Unisci Tavolo</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="unisci_tavoli" ' + test1 + '></div><div class="col-md-6 col-xs-6"><select style="font-size:25px" class="form-control nopadding" name="stampante_unisci_tavolo">' + opzione + '</select></div>';
                            form_settaggi += '</div>';

                            /*form_settaggi += '<div class="col-md-12 col-xs-12">';
                             var test1 = result[0].Field392 === "true" ? "checked" : "";
                             form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Cancellazione Tavolo</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="cancellazione_tavolo" ' + test1 + '></div>';
                             form_settaggi += '</div>';*/



                            form_settaggi += '<div class="col-md-12 col-xs-12">';
                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'VARIE\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            $('#form_settaggi').html(form_settaggi);


                            if (result[0].Field396 !== "") {
                                $('#form_settaggi select[name="stampante_cancellazione_articolo"] option[value="' + result[0].Field396 + '"]').prop("selected", true);
                            }

                            if (result[0].Field397 !== "") {
                                $('#form_settaggi select[name="stampante_sposta_articolo"] option[value="' + result[0].Field397 + '"]').prop("selected", true);
                            }

                            if (result[0].Field398 !== "") {
                                $('#form_settaggi select[name="stampante_sposta_tavolo"] option[value="' + result[0].Field398 + '"]').prop("selected", true);
                            }

                            if (result[0].Field399 !== "") {
                                $('#form_settaggi select[name="stampante_unisci_tavolo"] option[value="' + result[0].Field399 + '"]').prop("selected", true);
                            }

                            break;
                    }
                    break;
                case "layout_stampa":
                    switch (nome_settaggio) {

                        case "COMANDA":

                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[201] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga1"  style="font-size:25px" value="' + result[0].Field34 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga1_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field36 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spaziointestazione1" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[203] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga2"  style="font-size:25px" value="' + result[0].Field37 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga2_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field39 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spaziointestazione2" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[204] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga3"  style="font-size:25px" value="' + result[0].Field40 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga3_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field42 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spaziointestazione3" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[205] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga4"  style="font-size:25px" value="' + result[0].Field43 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga4_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field3 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Titolo</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="titolo" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field4 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[182] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="nome_stampante"  ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field5 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[43] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="operatore"  ' + test1 + '></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="font_operatore"><option value="N">Normale</option><option value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field6 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[206] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="data" ' + test1 + '></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="font_data"><option value="N">Normale</option><option value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field7 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[207] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="ora" ' + test1 + '></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="font_ora"><option value="N">Normale</option><option value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field24 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[208] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="coperti" ' + test1 + '></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="font_coperti"><option value="N">Normale</option><option value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field8 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[209] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="portate" ' + test1 + ' ></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="portate_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Segno Meno Specificato <a style="font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'SEGNO MENO SPECIFICATO\', \'Nel campo a seguire inserire la parola che verr&agrave; posta automaticamente dopo il segno MENO (-) in comanda.<br><br>Es: se nel campo scriviamo SENZA la comanda verr&agrave;: - SENZA e la variante scelta.\')">?</a></div><div class="col-md-3 col-xs-3"><input placeholder="Es: SENZA" maxlength="42" class="form-control" type="text" name="meno_specificato"  style="font-size:25px" value="' + result[0].Field307 + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field310 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Variante Meno Evidenziata</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_comanda" type="checkbox" name="variante_meno_evidenziata" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field410 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Variante Pi&ugrave; Evidenziata</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_comanda" type="checkbox" name="variante_piu_evidenziata" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field422 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Variante Poco Evidenziata</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_comanda" type="checkbox" name="variante_poco_evidenziata" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field423 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Variante Fine Cottura Evidenziata</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_comanda" type="checkbox" name="variante_fine_cottura_evidenziata" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field424 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Specifica Evidenziata</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_comanda" type="checkbox" name="specifica_evidenziata" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field10 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[210] + '</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_comanda" type="checkbox" name="prezzo" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[211] + '</div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding settaggi_comanda" name="font_articolo"><option value="N">Normale</option><option value="G">Grassetto</option><option value="D">Doppio</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[212] + '</div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding settaggi_comanda" name="font_varianti"><option value="N">Normale</option><option value="G">Grassetto</option><option value="D">Doppio</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field9 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[213] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="unione_articoli_varianti" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field11 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[178] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="ricetta" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field12 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Totale Quantit&agrave; Prodotti</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="totale_quantita_articoli"' + test1 + ' ></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[215] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="msg_riga1"  style="font-size:25px" value="' + result[0].Field13 + '"></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga1_grassetto"><option value="N">N</option><option style="font-weight:bold;" value="G">G</option></select></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga1_allineamento"><option value="x--">x--</option><option value="-x-">-x-</option><option value="--x">--x</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field26 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spazio1" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[216] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="msg_riga2"  style="font-size:25px" value="' + result[0].Field16 + '"></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga2_grassetto"><option value="N">N</option><option style="font-weight:bold;" value="G">G</option></select></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga2_allineamento"><option value="x--">x--</option><option value="-x-">-x-</option><option value="--x">--x</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field27 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spazio2" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[217] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="msg_riga3"  style="font-size:25px" value="' + result[0].Field19 + '"></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga3_grassetto"><option value="N">N</option><option style="font-weight:bold;" value="G">G</option></select></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga3_allineamento"><option value="x--">x--</option><option value="-x-">-x-</option><option value="--x">--x</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field22 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[218] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="concomitanze" style="font-size:25px" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field427 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Ripetizione tavolo</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="ripetizione_tavolo" style="font-size:25px" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field23 === "true" ? "checked" : "";
                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'COMANDA\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            $('#form_settaggi').html(form_settaggi);
                            $('#form_settaggi select[name="intestazione_riga1_grassetto"] option[value="' + result[0].Field35 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="intestazione_riga2_grassetto"] option[value="' + result[0].Field48 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="intestazione_riga3_grassetto"] option[value="' + result[0].Field41 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="intestazione_riga4_grassetto"] option[value="' + result[0].Field44 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="font_operatore"] option[value="' + result[0].Field30 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="font_data"] option[value="' + result[0].Field31 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="font_ora"] option[value="' + result[0].Field32 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="font_coperti"] option[value="' + result[0].Field33 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="font_articolo"] option[value="' + result[0].Field28 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="font_varianti"] option[value="' + result[0].Field29 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="msg_riga1_grassetto"] option:containsExact("' + result[0].Field14 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga1_allineamento"] option:containsExact("' + result[0].Field15 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga2_grassetto"] option:containsExact("' + result[0].Field17 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga2_allineamento"] option:containsExact("' + result[0].Field18 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga3_grassetto"] option:containsExact("' + result[0].Field20 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga3_allineamento"] option:containsExact("' + result[0].Field21 + '")').attr('selected', true);
                            $('#form_settaggi select[name="portate_grassetto"] option[value="' + result[0].Field25 + '"]').attr('selected', true);
                            break;

                        case "COMANDA_ASPORTO":

                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[201] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga1"  style="font-size:25px" value="' + result[0].Field111 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga1_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field113 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spaziointestazione1" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[203] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga2"  style="font-size:25px" value="' + result[0].Field114 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga2_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field116 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spaziointestazione2" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[204] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga3"  style="font-size:25px" value="' + result[0].Field117 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga3_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field119 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spaziointestazione3" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[205] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga4"  style="font-size:25px" value="' + result[0].Field120 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga4_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field122 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Titolo</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="titolo" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field123 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[182] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="nome_stampante"  ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field124 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[43] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="operatore"  ' + test1 + '></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="font_operatore"><option value="N">Normale</option><option value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field126 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[206] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="data" ' + test1 + '></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="font_data"><option value="N">Normale</option><option value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field128 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[207] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="ora" ' + test1 + '></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="font_ora"><option value="N">Normale</option><option value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field130 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[209] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="portate" ' + test1 + ' ></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="portate_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option><option style="font-weight:bold;" value="D">Doppio</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Segno Meno Specificato&nbsp;<a style="font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'SEGNO MENO SPECIFICATO\', \'Nel campo a seguire inserire la parola che verr&agrave; posta automaticamente dopo il segno MENO (-) in comanda.<br><br>Es: se nel campo scriviamo SENZA la comanda verr&agrave;: - SENZA e la variante scelta.\')">?</a></div><div class="col-md-3 col-xs-3"><input placeholder="Es: SENZA" maxlength="42" class="form-control" type="text" name="meno_specificato"  style="font-size:25px" value="' + result[0].Field307 + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field310 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Variante Meno Evidenziata</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_comanda" type="checkbox" name="variante_meno_evidenziata" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field422 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Variante Poco Evidenziata</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_comanda" type="checkbox" name="variante_poco_evidenziata" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field423 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Variante Fine Cottura Evidenziata</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_comanda" type="checkbox" name="variante_fine_cottura_evidenziata" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field410 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Variante Pi&ugrave; Evidenziata</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_comanda" type="checkbox" name="variante_piu_evidenziata" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field424 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Specifica Evidenziata</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_comanda" type="checkbox" name="specifica_evidenziata" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var el_tendina = alasql("select * from categorie where substr(descrizione,1,2)!='V.';");
                            var tendina = '<option value="N">Nessuno</option>';

                            el_tendina.forEach(function (e) {
                                tendina += '<option value="' + e.id + '">' + e.descrizione + '</option>';
                            });
                            tendina += '';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Articoli Evidenziati</div><div class="col-md-3 col-xs-3"><select style="font-size:25px" class="form-control nopadding" name="articoli_evidenziati_cat_1">' + tendina + '</select></div><div class="col-md-3 col-xs-3"><select style="font-size:25px" class="form-control nopadding" name="articoli_evidenziati_cat_2">' + tendina + '</select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';


                            var test1 = result[0].Field132 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[210] + '</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_comanda" type="checkbox" name="prezzo" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field189 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Totale prezzo articoli</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_comanda" type="checkbox" name="totale_prezzo_articoli" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[211] + '</div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding settaggi_comanda" name="font_articolo"><option value="N">Normale</option><option value="G">Grassetto</option><option value="D">Doppio</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[212] + '</div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding settaggi_comanda" name="font_varianti"><option value="N">Normale</option><option value="G">Grassetto</option><option value="D">Doppio</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field135 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[213] + '&nbsp;<a style="font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'UNIONE ARTICOLI CON VARIANTI\', \'Questa impostazione unisce la quantit&agrave; totale dello stesso prodotto ordinato, e sotto riporta la quantit&agrave; totale per differenziazione di aggiunte.<br><br>Es.<br>3 Margherite<br>&nbsp;&nbsp;1 +Salamino<br>&nbsp;&nbsp;2 +Origano<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+ Wurstel<br><br>NB: Abilitando questa impostazione si disabiliter&agrave; in automatico &quot;Variante Meno Evidenziata&quot;\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="unione_articoli_varianti" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field378 === "true" ? "checked" : "";
                            var test2 = result[0].Field379 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[178] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="ricetta" ' + test1 + '></div><div class="col-md-3 col-xs-3"><select style="font-size:25px" class="form-control nopadding" name="ricetta_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option><option style="font-weight:bold;" value="D">Doppio</option></select></div><div class="col-md-2 col-xs-2"><h4>Tutto Maiuscolo</h4></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="ricetta_maiuscola" ' + test2 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field381 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Totale Pizze</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="totale_pizze"' + test1 + ' ></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field382 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Totale Bibite</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="totale_bibite"' + test1 + ' ></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field137 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Totale Quantit&agrave; Prodotti</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="totale_quantita_articoli"' + test1 + ' ></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[215] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="msg_riga1"  style="font-size:25px" value="' + result[0].Field138 + '"></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga1_grassetto"><option value="N">N</option><option style="font-weight:bold;" value="G">G</option></select></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga1_allineamento"><option value="x--">x--</option><option value="-x-">-x-</option><option value="--x">--x</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field141 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spazio1" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[216] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="msg_riga2"  style="font-size:25px" value="' + result[0].Field142 + '"></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga2_grassetto"><option value="N">N</option><option style="font-weight:bold;" value="G">G</option></select></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga2_allineamento"><option value="x--">x--</option><option value="-x-">-x-</option><option value="--x">--x</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field145 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spazio2" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[217] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="msg_riga3"  style="font-size:25px" value="' + result[0].Field146 + '"></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga3_grassetto"><option value="N">N</option><option style="font-weight:bold;" value="G">G</option></select></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga3_allineamento"><option value="x--">x--</option><option value="-x-">-x-</option><option value="--x">--x</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field149 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[218] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="concomitanze" style="font-size:25px" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Consegna a Domicilio:</div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';



                            var test1 = result[0].Field252 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-4 col-xs-4 col-xs-offset-1" style="font-size:30px">Dati Cliente&nbsp;<a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'DATI CLIENTE\', \'Abilitando questo settaggio, nelle comande per le consegne a domicilio compariranno in basso i dati del cliente (nome, cognome, indirizzo e numeri di telefono).\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="dati_cliente" style="font-size:25px" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field253 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-4 col-xs-4 col-xs-offset-1" style="font-size:30px">QRCode Navigatore&nbsp;<a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'QRCODE NAVIGATORE\', \'Una volta abilitato, comparir&agrave; un QRCode che post-scannerizzazione mostrer&agrave; tramite Google Maps il percorso pi&ugrave; breve per arrivare dal cliente. \')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="qrcode_navigatore" style="font-size:25px" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-4 col-xs-4 col-xs-offset-1" style="font-size:30px">Testo Libero&nbsp;<a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'TESTO LIBERO\', \'Ci&ograve; che verr&agrave; scritto in questo campo comparir&agrave; a pi&egrave; pagina della stampa della comanda, in caso di Consegna a Domicilio. Non potranno essere abilitati &quot;Dati Cliente&quot; e &quot;QRCode&quot; assieme alla scritta.\')">?</a></div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="scritta_consegna_domicilio"  style="font-size:25px" value="' + result[0].Field375 + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';


                            var test1 = result[0].Field373 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Ritiro in Pizzeria:</div></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-4 col-xs-4 col-xs-offset-1" style="font-size:30px">Testo Libero&nbsp;<a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'TESTO LIBERO\', \'Ci&ograve; che verr&agrave; scritto in questo campo comparir&agrave; a pi&egrave; pagina della stampa della comanda, in caso il cliente venga a Ritirare l&rsquo;Ordine in Pizzeria.\')">?</a></div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="scritta_consegna_ritiro"  style="font-size:25px" value="' + result[0].Field376 + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            /*if (result[0].Field372 === "true") {
                             var test1 = result[0].Field374 === "true" ? "checked" : "";
                             form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Consumazione in Pizzeria:</div>\n\
                             </div>\n\
                             \n\
                             <div class="col-md-12 col-xs-12">';
                             
                             form_settaggi += '<div class="col-md-4 col-xs-4 col-xs-offset-1" style="font-size:30px">Testo Libero&nbsp;<a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'TESTO LIBERO\', \'Ci&ograve; che verr&agrave; scritto in questo campo comparir&agrave; a pi&egrave; pagina della stampa della comanda, in caso di Consumazione della pizza in Pizzeria.\')">?</a></div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="scritta_consegna_pizzeria"  style="font-size:25px" value="' + result[0].Field377 + '"></div>\n\
                             </div>\n\
                             \n\
                             <div class="col-md-12 col-xs-12">';
                             
                             }*/

                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'COMANDA_ASPORTO\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            $('#form_settaggi').html(form_settaggi);
                            $('#form_settaggi select[name="intestazione_riga1_grassetto"] option[value="' + result[0].Field112 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="intestazione_riga2_grassetto"] option[value="' + result[0].Field115 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="intestazione_riga3_grassetto"] option[value="' + result[0].Field118 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="intestazione_riga4_grassetto"] option[value="' + result[0].Field121 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="font_operatore"] option[value="' + result[0].Field125 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="font_data"] option[value="' + result[0].Field127 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="font_ora"] option[value="' + result[0].Field129 + '"]').attr("selected", true);

                            if (result[0].Field131 === "G") {
                                result[0].Field131 = "Grassetto";
                            } else if (result[0].Field131 === "D") {
                                result[0].Field131 = "Doppio";
                            }
                            $('#form_settaggi select[name="portate_grassetto"] option:containsExact("' + result[0].Field131 + '")').attr('selected', true);

                            if (result[0].Field380 === "G") {
                                result[0].Field380 = "Grassetto";
                            } else if (result[0].Field380 === "D") {
                                result[0].Field380 = "Doppio";
                            }
                            $('#form_settaggi select[name="ricetta_grassetto"] option:containsExact("' + result[0].Field380 + '")').attr('selected', true);

                            $('#form_settaggi select[name="articoli_evidenziati_cat_1"] option[value="' + result[0].Field383 + '"]').attr('selected', true);
                            $('#form_settaggi select[name="articoli_evidenziati_cat_2"] option[value="' + result[0].Field384 + '"]').attr('selected', true);

                            $('#form_settaggi select[name="font_articolo"] option[value="' + result[0].Field133 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="font_varianti"] option[value="' + result[0].Field134 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="msg_riga1_grassetto"] option:containsExact("' + result[0].Field139 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga1_allineamento"] option:containsExact("' + result[0].Field140 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga2_grassetto"] option:containsExact("' + result[0].Field143 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga2_allineamento"] option:containsExact("' + result[0].Field144 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga3_grassetto"] option:containsExact("' + result[0].Field147 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga3_allineamento"] option:containsExact("' + result[0].Field148 + '")').attr('selected', true);
                            break;
                        case "CONTO":

                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[201] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga1"  style="font-size:25px" value="' + result[0].Field45 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga1_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field47 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spaziointestazione1" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[203] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga2"  style="font-size:25px" value="' + result[0].Field48 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga2_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field50 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spaziointestazione2" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[204] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga3"  style="font-size:25px" value="' + result[0].Field51 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga3_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field53 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spaziointestazione3" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[205] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga4"  style="font-size:25px" value="' + result[0].Field54 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga4_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field56 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Titolo</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="titolo" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field105 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Nome Parcheggio su Bar</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="nome_parcheggio_bar" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field58 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[43] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="operatore"  ' + test1 + '></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="font_operatore"><option value="N">Normale</option><option value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field60 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[206] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="data" ' + test1 + '></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="font_data"><option value="N">Normale</option><option value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field62 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[207] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="ora" ' + test1 + '></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="font_ora"><option value="N">Normale</option><option value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[211] + '</div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="font_articolo"><option value="N">Normale</option><option value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[212] + '</div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="font_varianti"><option value="N">Normale</option><option value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field68 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Totale Quantit&agrave; Prodotti</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="totale_quantita_articoli"' + test1 + ' ></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[215] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="msg_riga1"  style="font-size:25px" value="' + result[0].Field69 + '"></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga1_grassetto"><option value="N">N</option><option style="font-weight:bold;" value="G">G</option></select></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga1_allineamento"><option value="x--">x--</option><option value="-x-">-x-</option><option value="--x">--x</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field72 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spazio1" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[216] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="msg_riga2"  style="font-size:25px" value="' + result[0].Field73 + '"></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga2_grassetto"><option value="N">N</option><option style="font-weight:bold;" value="G">G</option></select></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga2_allineamento"><option value="x--">x--</option><option value="-x-">-x-</option><option value="--x">--x</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field76 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spazio2" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[217] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="msg_riga3"  style="font-size:25px" value="' + result[0].Field77 + '"></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga3_grassetto"><option value="N">N</option><option style="font-weight:bold;" value="G">G</option></select></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga3_allineamento"><option value="x--">x--</option><option value="-x-">-x-</option><option value="--x">--x</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field358 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Conto In Lingua</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="conto_in_lingua"' + test1 + ' ></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field359 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Divisione per Persone esclude Divisione per Coperti</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="divisione_persone_esclude_divisione_coperti"' + test1 + ' ></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field500 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Barcode&nbsp;<a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'BARCODE\', \'Abilitando questo settaggio comparir&agrave; il barcode a pi&egrave; pagina dello scontrino. Questo servir&agrave; per ri-aprire la comanda dal forno utilizzando lo scanner.\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="barcode_riapertura_ordine_scontrino" style="font-size:25px" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field80 === "true" ? "checked" : "";
                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'CONTO\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            $('#form_settaggi').html(form_settaggi);
                            $('#form_settaggi select[name="intestazione_riga1_grassetto"] option[value="' + result[0].Field46 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="intestazione_riga2_grassetto"] option[value="' + result[0].Field49 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="intestazione_riga3_grassetto"] option[value="' + result[0].Field52 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="intestazione_riga4_grassetto"] option[value="' + result[0].Field55 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="font_operatore"] option[value="' + result[0].Field59 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="font_data"] option[value="' + result[0].Field61 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="font_ora"] option[value="' + result[0].Field63 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="font_articolo"] option[value="' + result[0].Field66 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="font_varianti"] option[value="' + result[0].Field67 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="msg_riga1_grassetto"] option:containsExact("' + result[0].Field70 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga1_allineamento"] option:containsExact("' + result[0].Field71 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga2_grassetto"] option:containsExact("' + result[0].Field74 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga2_allineamento"] option:containsExact("' + result[0].Field75 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga3_grassetto"] option:containsExact("' + result[0].Field78 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga3_allineamento"] option:containsExact("' + result[0].Field79 + '")').attr('selected', true);
                            break;
                        case "SCONTRINO":

                            /* PARTIRE DA FIELD 236 */

                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="margin-bottom:30px;font-size:30px">' + comanda.lang[215] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="msg_riga1"  style="font-size:25px" value="' + result[0].Field236 + '"></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga1_grassetto"><option value="N">N</option><option style="font-weight:bold;" value="G">G</option></select></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga1_allineamento"><option value="x--">x--</option><option value="-x-">-x-</option><option value="--x">--x</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            /*var test1 = result[0].Field237 === "true" ? "checked" : "";
                             form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spazio1" ' + test1 + '></div>\n\
                             </div>\n\
                             \n\
                             <div class="col-md-12 col-xs-12">';*/
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="margin-bottom:30px;font-size:30px">' + comanda.lang[216] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="msg_riga2"  style="font-size:25px" value="' + result[0].Field238 + '"></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga2_grassetto"><option value="N">N</option><option style="font-weight:bold;" value="G">G</option></select></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga2_allineamento"><option value="x--">x--</option><option value="-x-">-x-</option><option value="--x">--x</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            /* var test1 = result[0].Field239 == "true" ? "checked" : "";
                             form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spazio2" ' + test1 + '></div>\n\
                             </div>\n\
                             \n\
                             <div class="col-md-12 col-xs-12">';*/
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="margin-bottom:30px;font-size:30px">' + comanda.lang[217] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="msg_riga3"  style="font-size:25px" value="' + result[0].Field240 + '"></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga3_grassetto"><option value="N">N</option><option style="font-weight:bold;" value="G">G</option></select></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga3_allineamento"><option value="x--">x--</option><option value="-x-">-x-</option><option value="--x">--x</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            /*var test1 = result[0].Field247 == "true" ? "checked" : "";
                             form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spazio3" ' + test1 + '></div>\n\
                             </div>\n\
                             \n\
                             <div class="col-md-12 col-xs-12">';*/
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="margin-bottom:30px;font-size:30px">' + comanda.lang[235] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="msg_riga4"  style="font-size:25px" value="' + result[0].Field248 + '"></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga4_grassetto"><option value="N">N</option><option style="font-weight:bold;" value="G">G</option></select></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga4_allineamento"><option value="x--">x--</option><option value="-x-">-x-</option><option value="--x">--x</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';



                            var test1 = result[0].Field327 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Numero Tavolo</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="numero_tavolo_scontrino" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="margin-bottom:30px;font-size:30px">URL QR Code</div><div class="col-md-7 col-xs-7"><input maxlength="256" class="form-control" type="text" name="url_qr_code"  style="font-size:25px" value="' + result[0].Field251 + '"></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field360 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Divisione per Persone esclude Divisione per Coperti</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="divisione_persone_esclude_divisione_coperti"' + test1 + ' ></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field500 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Barcode&nbsp;<a style="line-height:40px;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'BARCODE\', \'Abilitando questo settaggio comparir&agrave; il barcode a pi&egrave; pagina dello scontrino. Questo servir&agrave; per ri-aprire la comanda dal forno utilizzando lo scanner.\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="barcode_riapertura_ordine_scontrino" style="font-size:25px" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'SCONTRINO\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            $('#form_settaggi').html(form_settaggi);

                            $('#form_settaggi select[name="msg_riga1_grassetto"] option:containsExact("' + result[0].Field241 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga1_allineamento"] option:containsExact("' + result[0].Field242 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga2_grassetto"] option:containsExact("' + result[0].Field243 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga2_allineamento"] option:containsExact("' + result[0].Field244 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga3_grassetto"] option:containsExact("' + result[0].Field245 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga3_allineamento"] option:containsExact("' + result[0].Field246 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga4_grassetto"] option:containsExact("' + result[0].Field249 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga4_allineamento"] option:containsExact("' + result[0].Field250 + '")').attr('selected', true);

                            break;
                        case "DETTAGLIO VENDUTI":

                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[201] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga1"  style="font-size:25px" value="' + result[0].Field81 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga1_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field83 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spaziointestazione1" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[203] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga2"  style="font-size:25px" value="' + result[0].Field84 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga2_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field86 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spaziointestazione2" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[204] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga3"  style="font-size:25px" value="' + result[0].Field87 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga3_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field89 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spaziointestazione3" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[205] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga4"  style="font-size:25px" value="' + result[0].Field90 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga4_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            //--------------------------------------------------------------------//

                            var test1 = result[0].Field92 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Report</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="report" ' + test1 + '></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="report_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field93 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[221] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="creatoil" ' + test1 + '></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="creatoil_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[222] + '</div><div class="col-md-3 col-xs-3"><select style="font-size:25px" class="form-control nopadding" name="ordinamento_articoli"><option value="D">' + comanda.lang[96] + '</option><option value="Q">Quantita</option><option value="T">Totale</option></select></div><div class="col-md-3 col-xs-3"><select style="font-size:25px" class="form-control nopadding" name="ordinamento_crescente_decrescente"><option value="C">' + comanda.lang[229] + '</option><option  value="D">' + comanda.lang[230] + '</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field97 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[223] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="totaliservizio" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field98 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[224] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="totalioperatore" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field99 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[225] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="totalegenerale" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field412 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Ordinamento per Gruppi</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="divisionegruppi" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field100 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[226] + '</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="divisionecategorie" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field101 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[227] + '</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="divisionedestinazione" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field102 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[228] + '</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="percentuali" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field103 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[28] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="sconti" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'DETTAGLIO VENDUTI\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            //--------------------------------------------------------------------//


                            $('#form_settaggi').html(form_settaggi);
                            $('#form_settaggi select[name="intestazione_riga1_grassetto"] option[value="' + result[0].Field82 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="intestazione_riga2_grassetto"] option[value="' + result[0].Field85 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="intestazione_riga3_grassetto"] option[value="' + result[0].Field88 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="intestazione_riga4_grassetto"] option[value="' + result[0].Field91 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="creatoil_grassetto"] option[value="' + result[0].Field94 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="report_grassetto"] option[value="' + result[0].Field104 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="ordinamento_articoli"] option[value="' + result[0].Field95 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="ordinamento_crescente_decrescente"] option[value="' + result[0].Field96 + '"]').attr("selected", true);
                            break;

                        case "ANAGRAFICA PRODOTTI":

                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-12 col-xs-12" style="font-size:30px"><h3 style="text-decoration:underline">PRODOTTI:</h3></div>';

                            var test1 = result[0].Field330 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Descrizione</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="descrizione" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field331 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Spazio Forno</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="spazio_forno" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field332 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Prezzo Default</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="prezzo_normale" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field333 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Prezzo Maxi</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="prezzo_maxi" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field334 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Prezzo Bar</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="prezzo_bar" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field335 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Prezzo Asporto</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="prezzo_asporto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';



                            var test1 = result[0].Field336 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Prezzo Tav. Continuo</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="prezzo_continuo" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field337 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Prezzo 1/4 Metro</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="prezzo_metro" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field338 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Unit&agrave; di misura</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="unita_misura" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field339 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Prezzo per unit&agrave;</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="prezzo_unita" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field340 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Destinazione Stampa</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="destinazione_stampa" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field341 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Destinazione Stampa 2</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="destinazione_stampa_2" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field342 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Portata</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="portata" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field343 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Categoria Varianti</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="categoria_varianti" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field344 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Varianti Automatiche</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="varianti_automatiche" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field345 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">IVA Base / Servizi</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="iva_base" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field346 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">IVA Takeaway / Beni</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="iva_takeaway" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field347 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Riepilogo</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="riepilogo" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field348 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Codice Promozionale</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="codice_promozionale" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-12 col-xs-12" style="font-size:30px"><h3 style="text-decoration:underline">VARIANTI:</h3></div>';

                            var test1 = result[0].Field349 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Prezzo Poco</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="prezzo_poco" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field350 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Prezzo Variante Maxi</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="prezzo_variante_maxi" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field351 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Prezzo Varianti Aggiuntive</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="prezzo_varianti_aggiuntive" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field352 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Prezzo Maxi Aggiuntive</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="prezzo_maxi_aggiuntive" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            /*                                             var test1 = result[0].Field349 == "true" ? "checked" : "";
                             form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">fast.order Abilitato</div><div class="col-md-1 col-xs-1"><input class="form-control settaggi_percentuali" type="checkbox" name="000" ' + test1 + '></div>\n\
                             </div>\n\
                             \n\
                             <div class="col-md-12 col-xs-12">';*/



                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'ANAGRAFICA PRODOTTI\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            $('#form_settaggi').html(form_settaggi);

                            break;
                    }
                    break;
                case "layout_video":
                    switch (nome_settaggio) {

                        case "TASTI":

                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field429 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">' + comanda.lang[60] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="conto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field436 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">' + comanda.lang[196] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="comandapiuconto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field430 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">' + comanda.lang[59] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="scontrino" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field431 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">' + 'Comanda+Scontrino' + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="comandapiuscontrino" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field432 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">' + comanda.lang[22] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="fattura" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field433 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">' + 'Comanda+Fattura' + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="comandapiufattura" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field434 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">' + comanda.lang[58] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="sconto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">' + comanda.lang[11] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" checked="checked" disabled name="comanda"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">' + comanda.lang[11] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" checked="checked" disabled name="comanda"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';


                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'LAYOUT_VIDEO_TASTI\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';


                            $('#form_settaggi').html(form_settaggi);
                            break;
                        case "VIDEO":


                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field435 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">COLORI COME SU PC</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="colori_come_pc" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';



                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'LAYOUT_VIDEO_VIDEO\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            $('#form_settaggi').html(form_settaggi);


                            break;

                    }
                    break;
                case "layout_pc":
                    switch (nome_settaggio) {
                        case "TASTI":

                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field207 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">Buoni pasto</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="buoni" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field195 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">' + comanda.lang[60] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="conto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field196 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">' + comanda.lang[196] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="comandapiuconto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field197 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">' + comanda.lang[59] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="scontrino" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field198 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">' + 'Comanda+Scontrino' + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="comandapiuscontrino" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field199 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">' + 'Scontrino + riepilogo' + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="scontrinopiupagamento" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field200 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">' + 'Pagamento' + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="scontrinopiuriepilogo" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field329 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">' + 'Non Pagato' + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="scontrinononpagato" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field201 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">' + comanda.lang[22] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="fattura" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field202 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">' + 'Comanda+Fattura' + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="comandapiufattura" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field203 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">' + comanda.lang[58] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="sconto" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field204 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">' + 'Storicizza' + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="storicizza" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field208 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">' + comanda.lang[11] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="comanda" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field363 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">SERVITO</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="servito" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field364 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">FISCALI</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="operazioni" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field503 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">COMANDA + CONTO (con prog.)</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="sagra_comanda_conto_parcheggia" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field504 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">SCONTR. + COMANDA + CONTO (con prog.)</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="sagra_comanda_conto_scontrino" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field505 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">PAGAM. + COMANDA + CONTO (con prog.)</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="sagra_comanda_conto_pagamento" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            //-------------------------------------------------------------------                                                                                                            

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;">Pagamento:</div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';


                            var test1 = result[0].Field365 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-4 col-xs-4 col-xs-offset-1" style="font-size:30px;">Assegno</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="assegno" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field366 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-4 col-xs-4 col-xs-offset-1" style="font-size:30px;">Bonifico</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="bonifico" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-4 col-xs-4 col-xs-offset-2" style="padding-top:10px;font-size:30px;">Ragione Sociale</div><div class="col-md-6 col-xs-6"><input maxlength="42" class="form-control" type="text" name="ragione_sociale"  style="font-size:25px" value="' + result[0].Field368 + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-4 col-xs-4 col-xs-offset-2" style="padding-top:10px;font-size:30px;">IBAN</div><div class="col-md-6 col-xs-6"><input maxlength="42" class="form-control" type="text" name="iban"  style="font-size:25px" value="' + result[0].Field369 + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-4 col-xs-4 col-xs-offset-2" style="padding-top:10px;font-size:30px;">BIC / SWIFT</div><div class="col-md-6 col-xs-6"><input maxlength="42" class="form-control" type="text" name="bic_swift"  style="font-size:25px" value="' + result[0].Field370 + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-4 col-xs-4 col-xs-offset-2" style="padding-top:10px;font-size:30px;">Note</div><div class="col-md-6 col-xs-6"><input maxlength="42" class="form-control" type="text" name="note"  style="font-size:25px" value="' + result[0].Field371 + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field367 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-4 col-xs-4 col-xs-offset-1" style="font-size:30px;">DropPay</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="droppay" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            //-------------------------------------------------------------------                                                                                         

                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'LAYOUT_PC_TASTI\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            $('#form_settaggi').html(form_settaggi);
                            break;
                        case "VIDEO":
                            var testo_query = "SELECT id,ordinamento,descrizione FROM categorie WHERE descrizione NOT LIKE '%PREFERITI%' and SUBSTR(descrizione,1,2)!=\"V.\" ORDER BY ordinamento ASC;";
                            comanda.sincro.query(testo_query, function (result_categorie) {
                                var testo_query = 'SELECT * FROM settaggi;';
                                comanda.sincro.query_locale(testo_query, function (risultato_settaggi_locali) {

                                    var tastiera_gigante = "",
                                        font_tastiera = "",
                                        font_tastierino = "",
                                        left_tastiera = "",
                                        left_tastierino = "";
                                    risultato_settaggi_locali.forEach(function (o, i) {

                                        switch (o.nome) {
                                            case "tastiera_gigante":
                                                tastiera_gigante = o.valore;
                                                break;

                                            case "font_tastierino":
                                                font_tastierino = o.valore;
                                                break;

                                            case "left_tastiera":
                                                left_tastiera = o.valore;
                                                break;

                                            case "left_tastierino":
                                                left_tastierino = o.valore;
                                                break;

                                        }
                                    });

                                    form_settaggi = '<div class="editor_settaggi">';
                                    form_settaggi += '<div class="col-md-12 col-xs-12">';
                                    var test1 = result[0].Field205 === "true" ? "checked" : "";
                                    form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;">' + 'Indietro a Menu' + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="indietroamenu" ' + test1 + '></div>\n\
                                                    </div>';
                                    form_settaggi += '<div class="col-md-12 col-xs-12">';
                                    form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;">Colore Sfondo</div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="colore_sfondo_PC"><option style="font-weight:bold;" value="1">Nero</option><option style="font-weight:bold;" value="2">Mix</option><option style="font-weight:bold;" value="3">Grigio</option></select></div>';
                                    form_settaggi += '</div>';
                                    form_settaggi += '<div class="col-md-12 col-xs-12">';
                                    var test1 = result[0].Field191 === "true" ? "checked" : "";
                                    form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;">Abilita P.Libero</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="abilita_prodotto_libero_PC" ' + test1 + '></div>';
                                    form_settaggi += '</div>';
                                    form_settaggi += '<div class="col-md-12 col-xs-12">';
                                    var test1 = result[0].Field192 === "true" ? "checked" : "";
                                    form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;">Abilita Lente+</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="abilita_lente_piu_PC" ' + test1 + '></div>';
                                    form_settaggi += '</div>';
                                    form_settaggi += '<div class="col-md-12 col-xs-12">';
                                    var test1 = result[0].Field229 === "true" ? "checked" : "";
                                    form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;">Tasto Segue</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="tasto_segue_PC" ' + test1 + '></div>';
                                    form_settaggi += '</div>';
                                    form_settaggi += '<div class="col-md-12 col-xs-12">';
                                    var test1 = result[0].Field385 === "true" ? "checked" : "";
                                    var test2 = result[0].Field386 === "true" ? "checked" : "";
                                    form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;">Cancella Conto&nbsp;<a ' + comanda.evento + '="popup_spiegazione(\'CANCELLA CONTO\', \'Abilitando questa impostazione, comparir&agrave; una &quot;X&quot; sopra il conto del tavolo alla destra, che permette la cancellazione di tutti gli articoli NON lanciati in comanda.<br><br>Abilitando l&rsquo;impostazione successiva si cancelleranno anche gli articoli lanciati in comanda, senza l&rsquo; avviso di storno nelle stampanti di competenza.\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="cancella_conto_abilitato" ' + test1 + '></div><div class="col-md-5 col-xs-5" style="font-size:30px;">Articoli Lanciati in Comanda</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="cancella_conto_gia_lanciati" ' + test2 + '></div>';
                                    form_settaggi += '</div>';
                                    form_settaggi += '<div class="col-md-12 col-xs-12">';
                                    var test1 = result[0].Field308 === "true" ? "checked" : "";
                                    form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;">Articoli Ordinati Visibili&nbsp;<a ' + comanda.evento + '="popup_spiegazione(\'ARTICOLI ORDINATI VISIBILI\', \'Qualora questo settaggio sia abilitato, gli articoli gi&agrave; lanciati in comanda saranno sempre visibili. A settaggio disabilitato verranno nascosti all interno del gruppo della portata a loro assegnata.\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="articoli_ordinati_visibili" ' + test1 + '></div>';
                                    form_settaggi += '</div>';
                                    form_settaggi += '<div class="col-md-12 col-xs-12">';
                                    var test1 = result[0].Field309 === "true" ? "checked" : "";
                                    form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;">Visualizzazione IVA&nbsp;<a ' + comanda.evento + '="popup_spiegazione(\'VISUALIZZAZIONE IVA\', \'Qualora questo settaggio sia abilitato, visualizzerai l IVA a fianco all articolo ordinato.\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="iva_esposta" ' + test1 + '></div>';
                                    form_settaggi += '</div>';

                                    /**/

                                    form_settaggi += '<div class="col-md-12 col-xs-12">';
                                    form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;">Apertura Cassetto Fiscale:</div>';
                                    form_settaggi += '</div>';

                                    form_settaggi += '<div class="col-md-12 col-xs-12">';
                                    var test1 = result[0].Field353 === "true" ? "checked" : "";
                                    form_settaggi += '<div class="col-md-1 col-xs-1"></div><div class="col-md-4 col-xs-4" style="font-size:30px;">tasto su piantina</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="apertura_piantina" ' + test1 + '></div>';
                                    form_settaggi += '</div>';

                                    form_settaggi += '<div class="col-md-12 col-xs-12">';
                                    var test1 = result[0].Field354 === "true" ? "checked" : "";
                                    form_settaggi += '<div class="col-md-1 col-xs-1"></div><div class="col-md-4 col-xs-4" style="font-size:30px;">tasto su ordinazione</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="apertura_ordinazione" ' + test1 + '></div>';
                                    form_settaggi += '</div>';

                                    form_settaggi += '<div class="col-md-12 col-xs-12">';
                                    var test1 = result[0].Field355 === "true" ? "checked" : "";
                                    form_settaggi += '<div class="col-md-1 col-xs-1"></div><div class="col-md-4 col-xs-4" style="font-size:30px;">apertura su conto</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="apertura_conto" ' + test1 + '></div>';
                                    form_settaggi += '</div>';

                                    form_settaggi += '<div class="col-md-12 col-xs-12">';
                                    var test1 = result[0].Field356 === "true" ? "checked" : "";
                                    form_settaggi += '<div class="col-md-1 col-xs-1"></div><div class="col-md-4 col-xs-4" style="font-size:30px;">apertura su scontrino</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="apertura_scontrino" ' + test1 + '></div>';
                                    form_settaggi += '</div>';

                                    /**/

                                    form_settaggi += '<div class="col-md-12 col-xs-12">';
                                    var test1 = result[0].Field357 === "true" ? "checked" : "";
                                    form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;">Frecce Scorrimento Categorie</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="frecce_scorrimento_categorie" ' + test1 + '></div>';
                                    form_settaggi += '</div>';


                                    form_settaggi += '<div class="col-md-12 col-xs-12">';
                                    var test1 = result[0].Field193 === "true" ? "checked" : "";
                                    form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;">Abilita C. Separato</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="abilita_conto_separato_PC" ' + test1 + '></div>';
                                    form_settaggi += '</div>';
                                    //CAMBIARE CAMPO

                                    form_settaggi += '<div class="col-md-12 col-xs-12">';
                                    var test1 = result[0].Field206 === "true" ? "checked" : "";
                                    form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;">Auto Tav. Continuo</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="autotavolocontinuo" ' + test1 + '></div>';
                                    form_settaggi += '</div>';

                                    form_settaggi += '<div class="col-md-12 col-xs-12">';
                                    var test1 = result[0].Field227 === "true" ? "checked" : "";
                                    form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;">Calendario Ordinazioni <a ' + comanda.evento + '="$(\'#popup_spiegazione_calendario_ordinazioni\').modal(\'show\');">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control calendario_ordinazioni" type="checkbox" name="calendario_ordinazioni" ' + test1 + '></div>';
                                    form_settaggi += '</div>';





                                    var display = "display:none;";
                                    if (test1 === "checked") {
                                        display = "display:block;";
                                    }
                                    form_settaggi += '<div class="col-md-12 col-xs-12 sottogruppo_calendario_ordinazioni" style="' + display + '">';

                                    form_settaggi += '<div class="col-xs-4 col-xs-offset-1" style="font-size:30px">Categoria Predefinita</div><div class="col-md-5 col-xs-5">';
                                    form_settaggi += '<select class="form-control nopadding" style="font-size:25.5px" name="scelta_categoria">';

                                    form_settaggi += '<option value="PREFERITI">PREFERITI (se disponibili)</option>';
                                    result_categorie.forEach(function (value, index) {

                                        form_settaggi += '<option value="' + value.id + '">' + value.descrizione + '</option>';

                                    });
                                    form_settaggi += '</select>';
                                    form_settaggi += ' </div>';
                                    form_settaggi += '</div>';


                                    //////////////////////////////////////////////////////////////



                                    form_settaggi += '<div class="col-md-12 col-xs-12">';
                                    var test1 = tastiera_gigante === "true" ? "checked" : "";
                                    form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;">Articoli in Grande</a></div><div class="col-md-1 col-xs-1"><input class="form-control calendario_ordinazioni" type="checkbox" name="tastiera_gigante" ' + test1 + '></div>';
                                    form_settaggi += '</div>';

                                    form_settaggi += '<div class="col-md-12 col-xs-12">';
                                    form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;">Font Tastierino</a></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control" type="number" name="font_tastierino"  style="font-size:25px" value="' + font_tastierino + '"></div>';
                                    form_settaggi += '</div>';

                                    form_settaggi += '<div class="col-md-12 col-xs-12">';
                                    form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;">Left Tastiera</a></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control" type="number" name="left_tastiera"  style="font-size:25px" value="' + left_tastiera + '"></div>';
                                    form_settaggi += '</div>';

                                    form_settaggi += '<div class="col-md-12 col-xs-12">';
                                    form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;">Left Tastierino</a></div><div class="col-md-2 col-xs-2"><input maxlength="42" class="form-control" type="number" name="left_tastierino"  style="font-size:25px" value="' + left_tastierino + '"></div>';
                                    form_settaggi += '</div>';

                                    ///////////////////////////////////


                                    form_settaggi += '</div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'LAYOUT_PC_VIDEO\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                                    $('#form_settaggi').html(form_settaggi);

                                    $('#form_settaggi select[name="scelta_categoria"] option[value="' + result[0].Field228 + '"]').attr("selected", true);
                                    $('#form_settaggi select[name="colore_sfondo_PC"] option[value="' + result[0].Field190 + '"]').attr("selected", true);
                                });
                            });

                            break;
                        case "VARIANTI":
                            var testo_query = "SELECT id,ordinamento,descrizione FROM categorie WHERE descrizione NOT LIKE '%PREFERITI%' and SUBSTR(descrizione,1,2)=\"V.\" ORDER BY ordinamento ASC;";
                            comanda.sincro.query(testo_query, function (result_categorie) {

                                form_settaggi = '<div class="editor_settaggi">';
                                form_settaggi += '<div class="col-md-12 col-xs-12">';
                                var test1 = result[0].Field297 === "true" ? "checked" : "";
                                form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;">Variante +</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="piu" ' + test1 + '></div>\n\
                                                    </div>';
                                form_settaggi += '<div class="col-md-12 col-xs-12">';
                                var test1 = result[0].Field298 === "true" ? "checked" : "";
                                form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;">Variante -</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="meno" ' + test1 + '></div>\n\
                                                    </div>';
                                form_settaggi += '<div class="col-md-12 col-xs-12">';
                                var test1 = result[0].Field299 === "true" ? "checked" : "";
                                form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;">Specifiche (...)</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="specifiche" ' + test1 + '></div>\n\
                                                    </div>';
                                form_settaggi += '<div class="col-md-12 col-xs-12">';
                                var test1 = result[0].Field300 === "true" ? "checked" : "";
                                form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;">Poco Cotto</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="pococotto" ' + test1 + '></div>\n\
                                                    </div>';
                                form_settaggi += '<div class="col-md-12 col-xs-12">';
                                var test1 = result[0].Field301 === "true" ? "checked" : "";
                                form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;">Fine Cottura</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="finecottura" ' + test1 + '></div>\n\
                                                    </div>';

                                form_settaggi += '<div class="col-md-12 col-xs-12">';
                                var test1 = result[0].Field304 === "true" ? "checked" : "";
                                form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;">Menu Completo</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="menu_completo" ' + test1 + '></div>\n\
                                                    </div>';

                                form_settaggi += '<div class="col-md-12 col-xs-12">';
                                var test1 = result[0].Field302 === "true" ? "checked" : "";
                                form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;">Gusti Gelato</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="tasto_gusti_gelato" ' + test1 + '></div>';

                                form_settaggi += '<div class="col-md-5 col-xs-5">';

                                form_settaggi += '<select class="form-control nopadding" style="font-size:25.5px" name="scelta_categoria">';

                                result_categorie.forEach(function (value, index) {

                                    form_settaggi += '<option value="' + value.id + '">' + value.descrizione + '</option>';

                                });
                                form_settaggi += '</select>';
                                form_settaggi += ' </div>';
                                form_settaggi += ' </div>';

                                form_settaggi += '</div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'LAYOUT_PC_VARIANTI\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                                $('#form_settaggi').html(form_settaggi);
                                $('#form_settaggi select[name="scelta_categoria"] option[value="' + result[0].Field303 + '"]').attr("selected", true);

                            });


                            break;

                        case "VISORE ASPORTO":

                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';


                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Testo Prima Riga</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="testo_riga1"  style="font-size:25px" value="' + result[0].Field415 + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Testo Seconda Riga</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="testo_riga2"  style="font-size:25px" value="' + result[0].Field416 + '"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-4 col-xs-4" style="font-size:30px">che compare dopo </div><div class="col-md-1 col-xs-1"><input maxlength="42" class="form-control" type="text" name="numero_secondi_visore"  style="font-size:25px;padding-left: 5px;padding-right: 5px;text-align: center;" value="' + result[0].Field417 + '"></div><div class="col-md-7 col-xs-7" style="font-size:30px">secondi dall&rsquo;emissione del documento commerciale</div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-4 col-xs-4" style="font-size:30px">&nbsp;</div><div class="col-md-1 col-xs-1"></div><div class="col-md-7 col-xs-7" style="font-size:30px"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';


                            var test1 = result[0].Field418 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Testo Totale su Visore</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="tasto_totale_visore" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-4 col-xs-4" style="font-size:30px">che compare per </div><div class="col-md-1 col-xs-1"><input maxlength="42" class="form-control" type="text" name="numero_secondi_totale_visore"  style="font-size:25px;padding-left: 5px;padding-right: 5px;text-align: center;" value="' + result[0].Field419 + '"></div><div class="col-md-7 col-xs-7" style="font-size:30px">secondi dal click sul simbolo dell&rsquo; occhio</div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';


                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'VISORE ASPORTO\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';

                            $('#form_settaggi').html(form_settaggi);

                            break;
                    }
                    break;
                case "scontistiche":
                    switch (nome_settaggio) {
                        case "SCONTISTICHE_SCONTO_TOTALE":

                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[201] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga1"  style="font-size:25px" value="' + result[0].Field256 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga1_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field257 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spaziointestazione1" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[203] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga2"  style="font-size:25px" value="' + result[0].Field258 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga2_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field259 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spaziointestazione2" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[204] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga3"  style="font-size:25px" value="' + result[0].Field260 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga3_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field261 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spaziointestazione3" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[205] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="intestazione_riga4"  style="font-size:25px" value="' + result[0].Field262 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intestazione_riga4_grassetto"><option value="N">Normale</option><option style="font-weight:bold;" value="G">Grassetto</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field263 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[206] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="data" ' + test1 + '></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="font_data"><option value="N">Normale</option><option value="G">Grassetto</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Scadenza Sconto<a style="padding-left:1vw;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'SCADENZA SCONTO\', \'Selezionare dalla tendina il periodo e nel campo alla sinistra inserire il numero di giorni, mesi o anni.<br><br>Se dalla tendina viene selezionato NESSUNO significa che gli sconti non avranno scadenza.\')">?</a></div><div class="col-md-2 col-xs-2"><input maxlength="2" class="form-control" type="number" name="valore_scadenza"  style="font-size:25px" value="' + result[0].Field280 + '"></div><div class="col-md-2 col-xs-2"><select style="font-size:25px" class="form-control nopadding" name="intervallo_scadenza"><option value="Niente">Niente</option><option value="Giorni">Giorni</option><option value="Mesi">Mesi</option><option value="Anni">Anni</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12"  style="font-size:30px;padding-top:3px;">';

                            form_settaggi += '<div class="col-md-12 col-xs-12" style="font-size:30px">Tipologie Sconto:<a style="padding-left:1vw;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'TIPOLOGIE SCONTO\', \'Si possono stabilire 4 fasce di scontistiche per prezzo totale differenti, e decidere la percentuale di sconto e l eventuale omaggio.<br><br><ul><li>Nel primo campo va inserito il totale di partenza nel quale applicare la percentuale di sconto e l omaggio.</li><li>Nel secondo campo viene inserito il totale massimo per quella fascia di sconto.</li><li>Nel terzo campo va indicata l eventuale percentuale di sconto da applicare.</li><li>Nel quarto campo viene inserito l eventuale omaggio, a parole.</li></ul><br><br>NB: Nel primo campo il totale dev essere maggiore al valore inserito, mentre nel secondo campo il totale dev essere minore o uguale al valore inserito. Se il secondo campo &egrave; vuoto significa che dal primo valore in poi si applica lo sconto.\')">?</a></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12"  style="font-size:30px;padding-top:3px;">';

                            form_settaggi += '<div class="col-md-1 col-xs-1 col-xs-offset-2">Da</div><div class="col-md-1 col-xs-1"><input maxlength="4" class="form-control" type="text" name="valore_euro_da_1"  style="text-align:center;padding:2px;font-size:25px" value="' + result[0].Field282 + '"></div><div class="col-md-1 col-xs-1" style="text-align:center">a</div><div class="col-md-1 col-xs-1"><input maxlength="4" class="form-control" type="text" name="valore_euro_a_1"  style="text-align:center;padding:2px;font-size:25px" value="' + result[0].Field283 + '"></div><div class="col-md-3 col-xs-3" style="text-align:center">&euro; sconto del</div><div class="col-md-1 col-xs-1"><input maxlength="4" class="form-control" type="text" name="valore_sconto_1"  style="text-align:center;padding:2px;font-size:25px" value="' + result[0].Field284 + '"></div><div class="col-md-1 col-xs-1">%</div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12"  style="font-size:30px;padding-top:3px;">';

                            form_settaggi += '<div class="col-md-1 col-xs-1 col-xs-offset-2">Da</div><div class="col-md-1 col-xs-1"><input maxlength="4" class="form-control" type="text" name="valore_euro_da_2"  style="text-align:center;padding:2px;font-size:25px" value="' + result[0].Field286 + '"></div><div class="col-md-1 col-xs-1" style="text-align:center">a</div><div class="col-md-1 col-xs-1"><input maxlength="4" class="form-control" type="text" name="valore_euro_a_2"  style="text-align:center;padding:2px;font-size:25px" value="' + result[0].Field287 + '"></div><div class="col-md-3 col-xs-3" style="text-align:center">&euro; sconto del</div><div class="col-md-1 col-xs-1"><input maxlength="4" class="form-control" type="text" name="valore_sconto_2"  style="text-align:center;padding:2px;font-size:25px" value="' + result[0].Field288 + '"></div><div class="col-md-1 col-xs-1">%</div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12"  style="font-size:30px;padding-top:3px;">';

                            form_settaggi += '<div class="col-md-1 col-xs-1 col-xs-offset-2">Da</div><div class="col-md-1 col-xs-1"><input maxlength="4" class="form-control" type="text" name="valore_euro_da_3"  style="text-align:center;padding:2px;font-size:25px" value="' + result[0].Field290 + '"></div><div class="col-md-1 col-xs-1" style="text-align:center">a</div><div class="col-md-1 col-xs-1"><input maxlength="4" class="form-control" type="text" name="valore_euro_a_3"  style="text-align:center;padding:2px;font-size:25px" value="' + result[0].Field291 + '"></div><div class="col-md-3 col-xs-3" style="text-align:center">&euro; sconto del</div><div class="col-md-1 col-xs-1"><input maxlength="4" class="form-control" type="text" name="valore_sconto_3"  style="text-align:center;padding:2px;font-size:25px" value="' + result[0].Field292 + '"></div><div class="col-md-1 col-xs-1">%</div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12"  style="font-size:30px;padding-top:3px;">';

                            form_settaggi += '<div class="col-md-1 col-xs-1 col-xs-offset-2">Da</div><div class="col-md-1 col-xs-1"><input maxlength="4" class="form-control" type="text" name="valore_euro_da_4"  style="text-align:center;padding:2px;font-size:25px" value="' + result[0].Field294 + '"></div><div class="col-md-1 col-xs-1" style="text-align:center">a</div><div class="col-md-1 col-xs-1"><input maxlength="4" class="form-control" type="text" name="valore_euro_a_4"  style="text-align:center;padding:2px;font-size:25px" value="' + result[0].Field295 + '"></div><div class="col-md-3 col-xs-3" style="text-align:center">&euro; sconto del</div><div class="col-md-1 col-xs-1"><input maxlength="4" class="form-control" type="text" name="valore_sconto_4"  style="text-align:center;padding:2px;font-size:25px" value="' + result[0].Field296 + '"></div><div class="col-md-1 col-xs-1">%</div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12"  style="font-size:30px;padding-top:3px;">';


                            form_settaggi += '<div class="col-md-12 col-xs-12" style="font-size:30px">Giorni Sconto:<a style="padding-left:1vw;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'GIORNI SCONTO\', \'Spunta i giorni nei quali verranno emessi i buoni allegati allo scontrino.\')">?</a></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12 col-xs-offset-2"  style="font-size:30px;padding-top:3px;">';

                            var test1 = result[0].Field305.indexOf("1") !== -1 ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Luned&igrave;</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="lun" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12 col-xs-offset-2">';

                            var test1 = result[0].Field305.indexOf("2") !== -1 ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Marted&igrave;</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="mar" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12 col-xs-offset-2">';

                            var test1 = result[0].Field305.indexOf("3") !== -1 ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Mercoled&igrave;</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="mer" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12 col-xs-offset-2">';

                            var test1 = result[0].Field305.indexOf("4") !== -1 ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Gioved&igrave;</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="gio" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12 col-xs-offset-2">';

                            var test1 = result[0].Field305.indexOf("5") !== -1 ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Vened&igrave;</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="ven" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12 col-xs-offset-2">';

                            var test1 = result[0].Field305.indexOf("6") !== -1 ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Sabato</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="sab" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12 col-xs-offset-2">';

                            var test1 = result[0].Field305.indexOf("0") !== -1 ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Domenica</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="dom" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field306 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">QRCode Sconto:<a style="padding-left:1vw;font-size:20px;font-weight:bold;cursor:pointer;" ' + comanda.evento + '="popup_spiegazione(\'QRCODE SCONTO\', \'Se spuntato, nello scontrino oltre al codice sconto apparir&agrave; il relativo QRCode.\')">?</a></div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="qrcode_sconto" ' + test1 + '></div>\n\
                                     </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12"  style="font-size:30px;padding-top:3px;">';



                            form_settaggi += '<div class="col-md-5 col-xs-5">' + comanda.lang[215] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="msg_riga1"  style="font-size:25px" value="' + result[0].Field264 + '"></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga1_grassetto"><option value="N">N</option><option style="font-weight:bold;" value="G">G</option></select></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga1_allineamento"><option value="x--">x--</option><option value="-x-">-x-</option><option value="--x">--x</option></select></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field265 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spazio1" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[216] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="msg_riga2"  style="font-size:25px" value="' + result[0].Field266 + '"></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga2_grassetto"><option value="N">N</option><option style="font-weight:bold;" value="G">G</option></select></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga2_allineamento"><option value="x--">x--</option><option value="-x-">-x-</option><option value="--x">--x</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            var test1 = result[0].Field267 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[202] + '</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="spazio2" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">' + comanda.lang[217] + '</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="msg_riga3"  style="font-size:25px" value="' + result[0].Field268 + '"></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga3_grassetto"><option value="N">N</option><option style="font-weight:bold;" value="G">G</option></select></div><div class="col-md-1 col-xs-1"><select style="font-size:25px" class="form-control nopadding" name="msg_riga3_allineamento"><option value="x--">x--</option><option value="-x-">-x-</option><option value="--x">--x</option></select></div>\n\
                                                </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';




                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'SCONTISTICHE_SCONTO_TOTALE\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            $('#form_settaggi').html(form_settaggi);
                            $('#form_settaggi select[name="intestazione_riga1_grassetto"] option[value="' + result[0].Field269 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="intestazione_riga2_grassetto"] option[value="' + result[0].Field270 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="intestazione_riga3_grassetto"] option[value="' + result[0].Field271 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="intestazione_riga4_grassetto"] option[value="' + result[0].Field272 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="font_data"] option[value="' + result[0].Field273 + '"]').attr("selected", true);
                            $('#form_settaggi select[name="msg_riga1_grassetto"] option:containsExact("' + result[0].Field274 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga1_allineamento"] option:containsExact("' + result[0].Field275 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga2_grassetto"] option:containsExact("' + result[0].Field276 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga2_allineamento"] option:containsExact("' + result[0].Field277 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga3_grassetto"] option:containsExact("' + result[0].Field278 + '")').attr('selected', true);
                            $('#form_settaggi select[name="msg_riga3_allineamento"] option:containsExact("' + result[0].Field279 + '")').attr('selected', true);
                            $('#form_settaggi select[name="intervallo_scadenza"] option:containsExact("' + result[0].Field281 + '")').attr('selected', true);
                            break;
                    }
                    break;
                case "fastorder":
                    switch (nome_settaggio) {
                        case "FASTORDER":

                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">URL fast.order</div><div class="col-md-5 col-xs-5"><input class="form-control" type="text" name="url_fastorder"  style="font-size:25px" value="' + result[0].Field502 + '" placeholder="es. http://fastorder.club/Profilo/"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field506 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Categorie uguali a Intellinet</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="categorie_come_intellinet" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field507 == "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Modifica Prezzi in Aggiornamento</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="modifica_prezzi_fastorder" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px">Esporta Prodotti</div><div class="col-md-1 col-xs-1"><button class="btn btn-default btn-warning" type="button" onclick="fastorder.export_products_fastorder()">Esporta</button</div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';




                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'FASTORDER\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            $('#form_settaggi').html(form_settaggi);



                            break;
                    }
                    break;
                case "set_pos":
                    switch (nome_settaggio) {
                        case "POS_1":

                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field234 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">POS Abilitato</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="pos_abilitato" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="margin-bottom:10px;font-size:30px">IP POS</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="ip_pos"  style="font-size:25px" value="' + result[0].Field231 + '" placeholder="xxx.xxx.xxx.xxx"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="margin-bottom:10px;font-size:30px">PORTA POS</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="porta_pos"  style="font-size:25px" value="' + result[0].Field232 + '" placeholder="es. 10000"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="margin-bottom:10px;font-size:30px">ID TERM</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="id_term"  style="font-size:25px" value="' + result[0].Field233 + '" placeholder="8 cifre numeriche"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field235 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">Ricevuta POS su Scontrino</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="ricevuta_pos_scontrino" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'POS_1\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            $('#form_settaggi').html(form_settaggi);
                            break;
                        case "POS_2":

                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field400 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">POS Abilitato</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="pos_abilitato" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="margin-bottom:10px;font-size:30px">IP POS</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="ip_pos"  style="font-size:25px" value="' + result[0].Field401 + '" placeholder="xxx.xxx.xxx.xxx"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="margin-bottom:10px;font-size:30px">PORTA POS</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="porta_pos"  style="font-size:25px" value="' + result[0].Field402 + '" placeholder="es. 10000"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="margin-bottom:10px;font-size:30px">ID TERM</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="id_term"  style="font-size:25px" value="' + result[0].Field403 + '" placeholder="8 cifre numeriche"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field404 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">Ricevuta POS su Scontrino</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="ricevuta_pos_scontrino" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'POS_2\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            $('#form_settaggi').html(form_settaggi);
                            break;
                        case "POS_3":

                            form_settaggi = '<div class="editor_settaggi">\n\
                                                    <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field405 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">POS Abilitato</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="pos_abilitato" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="margin-bottom:10px;font-size:30px">IP POS</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="ip_pos"  style="font-size:25px" value="' + result[0].Field406 + '" placeholder="xxx.xxx.xxx.xxx"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="margin-bottom:10px;font-size:30px">PORTA POS</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="porta_pos"  style="font-size:25px" value="' + result[0].Field407 + '" placeholder="es. 10000"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            form_settaggi += '<div class="col-md-5 col-xs-5" style="margin-bottom:10px;font-size:30px">ID TERM</div><div class="col-md-5 col-xs-5"><input maxlength="42" class="form-control" type="text" name="id_term"  style="font-size:25px" value="' + result[0].Field408 + '" placeholder="8 cifre numeriche"></div>\n\
                                                    </div>\n\
                                                    \n\
                                                <div class="col-md-12 col-xs-12">';

                            var test1 = result[0].Field409 === "true" ? "checked" : "";
                            form_settaggi += '<div class="col-md-5 col-xs-5" style="font-size:30px;text-transform:uppercase;">Ricevuta POS su Scontrino</div><div class="col-md-1 col-xs-1"><input class="form-control" type="checkbox" name="ricevuta_pos_scontrino" ' + test1 + '></div>\n\
                                                    </div>\n\
                                                    \n\
                                                    <div class="col-md-12 col-xs-12">';

                            form_settaggi += '</div>\n\
                                                </div>\n\
                                                \n\
                                                <br/>\n\
                                            <div class="col-md-12 col-xs-12" style="margin-top: 0px;"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_settaggi(\'POS_3\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button></div>\n\
                                        </div>';
                            $('#form_settaggi').html(form_settaggi);
                            break;

                    }
                    break;
            }
        });
    }
}


function reset_password_operatore(id) {
    var testo_query = "UPDATE operatori SET password='d41d8cd98f00b204e9800998ecf8427e' WHERE id=" + id + ";";
    comanda.sock.send({
        tipo: "aggiornamento_operatori",
        operatore: comanda.operatore,
        query: testo_query,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    comanda.sincro.query(testo_query, function () {

        bootbox.alert("Password Resettata Correttamente.");

    });
    comanda.sincro.query_cassa();
}




function gestisci_operatore(event, id) {



    if (event !== undefined && event.target !== undefined && event.target !== null) {
        $(event.target).parent().parent().find("a").removeClass('cat_accesa');
        $(event.target).addClass("cat_accesa");
    }

    $('#form_gestione_clienti').html('');
    var form_gestione_stampanti = '';
    comanda.sincro.query("SELECT * FROM operatori WHERE id=" + id + " LIMIT 1;", function (result) {

        for (var key in result[0]) {
            console.log("RESULT0", key, result[0], result[0][key]);
            if (result[0][key] === null) {
                result[0][key] = '';
            }
        }

        var tipo = '';
        //Nome(stampante),Lingua(stanza),Ip,Piccola,Intelligent,Fiscale

        form_gestione_stampanti = '<div class="editor_operatori">\n\
                                   <div class="col-md-12 col-xs-12">\n\
                                        <div class="col-md-5 col-xs-5"><label>' + comanda.lang[99] + '</label><input maxlength="20" style="text-transform:uppercase;font-weight:bold;margin-bottom:10px;" class="form-control" type="text" name="nome"  value="' + result[0].nome + '"></div>\n\
                                        <div class="col-md-5 col-xs-5"><label>Eventuale Nuova Password</label><input style="margin-bottom:10px;" class="form-control" type="text" name="password"   value=""></div>\n\
                                        <div class="col-md-2 col-xs-2"><label>Reset Password</label><button class="btn btn-danger" ' + comanda.evento + '="reset_password_operatore(\'' + result[0].id + '\');" style="text-transform:uppercase;">X</button></div>\n\
                                   </div>';


        form_gestione_stampanti += '<div class="col-md-12 col-xs-12">';

        var test1 = result[0].p_statistiche === "true" ? "checked" : "";
        form_gestione_stampanti += '<div class="col-md-3 col-xs-3" style="text-align:center;"><label>Statistiche</label><input  style="height:40px;" class="form-control" type="checkbox" name="p_statistiche" ' + test1 + '></div>';

        var test1 = result[0].p_verifiche === "true" ? "checked" : "";
        form_gestione_stampanti += '<div class="col-md-3 col-xs-3" style="text-align:center;"><label>Verifiche</label><input  style="height:40px;" class="form-control" type="checkbox" name="p_verifiche" ' + test1 + '></div>';

        var test2 = result[0].p_gestioni === "true" ? "checked" : "";
        form_gestione_stampanti += '<div class="col-md-3 col-xs-3" style="text-align:center;"><label>Gestioni</label><input  style="height:40px;" class="form-control" type="checkbox" name="p_gestioni" ' + test2 + '></div>';

        var test7 = result[0].p_gestione_clienti === "true" ? "checked" : "";
        form_gestione_stampanti += '<div class="col-md-3 col-xs-3" style="text-align:center;"><label>Gestione Clienti</label><input style="height:40px;" class="form-control" type="checkbox" name="p_gestione_clienti" ' + test7 + '></div>';

        form_gestione_stampanti += '</div>';


        form_gestione_stampanti += '<div class="col-md-12 col-xs-12">';

        var test4 = result[0].p_backoffice === "true" ? "checked" : "";
        form_gestione_stampanti += '<div class="col-md-3 col-xs-3" style="text-align:center;"><label>Back Office</label><input  style="height:40px;" class="form-control" type="checkbox" name="p_backoffice" ' + test4 + '></div>';

        var test5 = result[0].p_chiusura === "true" ? "checked" : "";
        form_gestione_stampanti += '<div class="col-md-3 col-xs-3" style="text-align:center;"><label>Chiusure Fiscali</label><input  style="height:40px;" class="form-control" type="checkbox" name="p_chiusura" ' + test5 + '></div>';

        var test6 = result[0].p_mappa_tavoli === "true" ? "checked" : "";
        form_gestione_stampanti += '<div class="col-md-3 col-xs-3" style="text-align:center;"><label>Mappa Tavoli</label><input style="height:40px;" class="form-control" type="checkbox" name="p_mappa_tavoli" ' + test6 + '></div>';

        var test8 = result[0].p_mappa_tavoli_phone === "true" ? "checked" : "";
        form_gestione_stampanti += '<div class="col-md-3 col-xs-3" style="text-align:center;"><label>Mappa Tavoli Phone</label><input style="height:40px;" class="form-control" type="checkbox" name="p_mappa_tavoli_phone" ' + test8 + '></div>';

        form_gestione_stampanti += '</div>';


        form_gestione_stampanti += '<div class="col-md-12 col-xs-12">';

        var test3 = result[0].p_layout === "true" ? "checked" : "";
        form_gestione_stampanti += '<div class="col-md-3 col-xs-3" style="text-align:center;"><label>Layout Tasti</label><input style="height:40px;"  class="form-control" type="checkbox" name="p_layout" ' + test3 + '></div>';

        form_gestione_stampanti += '</div>';


        form_gestione_stampanti += '<div class="col-md-12 col-xs-12"><button class="pull-right col-md-2 col-xs-2 btn btn-success" ' + comanda.evento + '="salva_operatore(\'' + result[0].id + '\');" style="text-transform:uppercase;">' + comanda.lang[104] + '</button><button class="pull-right col-md-2 col-xs-2 btn btn-danger" ' + comanda.evento + '="elimina_operatore(\'' + result[0].id + '\',\'' + tipo + '\');" style="text-transform:uppercase;">' + comanda.lang[179] + '</button></div>\n\
                                   </div>';

        $('#form_gestione_operatori').html(form_gestione_stampanti);

        $(document).on("change", "[name='p_gestione_clienti']", function () {
            if (this.checked) {
                document.getElementsByName("p_gestioni")[0].checked = false;
            }
        });

        $(document).on("change", "[name='p_gestioni']", function () {
            if (this.checked) {
                document.getElementsByName("p_gestione_clienti")[0].checked = false;
            }
        });

    });
}