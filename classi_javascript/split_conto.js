/* global comanda */

var lettere_split_base = ["", "A", "B", "C", "D", "E", "F", "G", "H", "I", "L", "M"];
var lettere = [];

function scegli_conto_split(bypass) {

    $('[id^="tot_bis_"]').html('');
    $('[class*="apm_bis_"]').css('height', '12vh');
    $('[id^="righe_bis_"]').html('');
    $('[id^="tot_bis_"]').parent().css('background-image', 'linear-gradient(to bottom,  #f39c12, #a46809)');

    var query_bis = "select incassato,dest_stampa,PRZ,quantita,replace(prezzo_un,',','.') as prezzo_un,desc_art,ntav_comanda,BIS,nodo,sconto_perc from comanda where stato_record='ATTIVO' and  ntav_comanda='" + comanda.tavolo + "';";

    var r = alasql(query_bis);

    var array_totale = new Array();


    var array_corpi_temp = new Array();
    var array_varianti_temp = new Array();
    var array_teste = new Array();

    r.forEach(function (e) {
        if (e.nodo.length === 3 && e.contiene_variante !== '1') {
            if (array_corpi_temp[e.desc_art] === undefined) {
                array_corpi_temp[e.desc_art] = new Object();
            }
            if (array_corpi_temp[e.desc_art][e.BIS] === undefined) {
                e.quantita = parseInt(e.quantita);
                e.prezzo_un = parseFloat(e.prezzo_un);
                if (e.sconto_perc === "" || isNaN(e.sconto_perc)) {
                    e.sconto_perc = "0";
                }
                e.prezzo_tot = e.prezzo_un * e.quantita / 100 * (100 - parseInt(e.sconto_perc));
                array_corpi_temp[e.desc_art][e.BIS] = e;
            } else
            {
                array_corpi_temp[e.desc_art][e.BIS].quantita += parseInt(e.quantita);
                array_corpi_temp[e.desc_art][e.BIS].prezzo_un += parseFloat(e.prezzo_un);
                if (e.sconto_perc === "" || isNaN(e.sconto_perc)) {
                    e.sconto_perc = "0";
                }
                array_corpi_temp[e.desc_art][e.BIS].prezzo_tot += parseFloat(e.prezzo_un) * parseInt(e.quantita) / 100 * (100 - parseInt(e.sconto_perc));
            }
        } else if (e.nodo.length === 7 || e.contiene_variante === '1') {
            if (array_varianti_temp[e.nodo] === undefined) {
                e.quantita = parseInt(e.quantita);
                e.prezzo_un = parseFloat(e.prezzo_un);
                if (e.sconto_perc === "" || isNaN(e.sconto_perc)) {
                    e.sconto_perc = "0";
                }
                e.prezzo_tot = e.prezzo_un * e.quantita / 100 * (100 - parseInt(e.sconto_perc));
                array_varianti_temp[e.nodo] = e;
            } else
            {
                array_varianti_temp[e.nodo].quantita += parseInt(e.quantita);
                array_varianti_temp[e.nodo].prezzo_un += parseFloat(e.prezzo_un);
                if (e.sconto_perc === "" || isNaN(e.sconto_perc)) {
                    e.sconto_perc = "0";
                }
                array_varianti_temp[e.nodo].prezzo_tot += parseFloat(e.prezzo_un) * parseInt(e.quantita) / 100 * (100 - parseInt(e.sconto_perc));
            }
        } else if (e.desc_art.indexOf('RECORD TESTA') !== -1) {
            e.nodo = "999";
            array_teste.push(e);
        }
    });

    //IF AGGIUNTIVI
    //and length(nodo)=3 AND contiene_variante!='1' 
    //(length(nodo)=7 OR contiene_variante='1')
    //and desc_art='RECORD TESTA'

    for (var desc_art in array_corpi_temp) {
        for (var bis in array_corpi_temp[desc_art]) {
            if (typeof array_corpi_temp[desc_art][bis] !== "function") {
                array_totale.push(array_corpi_temp[desc_art][bis]);
            }
        }
    }

    for (var nodo in array_varianti_temp) {
        if (typeof array_varianti_temp[nodo] !== "function") {
            array_totale.push(array_varianti_temp[nodo]);
        }
    }

    array_teste.forEach(function (e) {
        array_totale.push(e);
    });

    //SUM E OPERAZIONI VARIABILI
    //sum(quantita) as qt,sum(replace(prezzo_un,',','.'))*quantita/100*(100-sconto_perc) as prezzo_tot,
    //sum(quantita) as qt,sum(replace(prezzo_un,',','.'))*quantita/100*(100-sconto_perc) as prezzo_tot,
    //'999' (nodo)

    //GROUP
    //group by desc_art,bis
    //group by nodo

    //ORDER order by BIS asc,nodo asc;



    var o = array_totale;

    //comanda.sincro.query(query_bis, function (o) {

    if (bypass === true) {
        $('#popup_bis').modal('show');
    }

    lettere = lettere_split_base.slice(0);
    $('#popup_bis_nomi_conti_bis').html("");

    if (o.length > 0) {



        o.forEach(function (v) {

            if (lettere.indexOf(v.BIS) === -1) {
                lettere.push(v.BIS);
                $('#popup_bis_nomi_conti_bis').append('<div class="row non-nascondere" onclick="apri_conto_split(\'' + v.BIS + '\')" style="border: 4px solid black;"><div class="col-xs-4 btn btn-warning nopadding" style="padding-top: 1.5vh !important;padding-bottom: 1.5vh !important;font-size:3vh">' + v.BIS + '<br/>&nbsp;<span id="tot_bis_' + v.BIS.replace(" ", "_") + '"></span></div><div class="col-xs-8 apm_bis_' + v.BIS + '" style="height:12vh;overflow:auto;"><table id="righe_bis_' + v.BIS + '" class="table table-responsive"></table></div></div>');
            }

            if (v.desc_art.indexOf('RECORD TESTA') !== -1) {



                if (v.dest_stampa === "C") {
                    $('#tot_bis_' + v.BIS).parent().css('background-image', 'linear-gradient(to bottom, #3498db, #2370a4)');
                } else
                {
                    $('#tot_bis_' + v.BIS).parent().css('background-image', 'linear-gradient(to bottom,  #f39c12, #a46809)');
                }



                if (v.incassato !== "" && parseFloat(v.incassato) > 0) {
                    $('#tot_bis_' + v.BIS).html('i: &euro;' + parseFloat(v.incassato.replace(",", ".")).toFixed(2) + '<br>' + 't: &euro;' + parseFloat(v.PRZ.replace(",", ".")).toFixed(2));
                    $('.apm_bis_' + v.BIS).css('height', '16vh');


                } else
                {
                    $('#tot_bis_' + v.BIS).html('&euro;' + parseFloat(v.PRZ.replace(",", ".")).toFixed(2));
                    $('.apm_bis_' + v.BIS).css('height', '12vh');

                }


            } else
            {



                if (v.desc_art[0] === '+' || v.desc_art[0] === '-' || v.desc_art[0] === '*' || v.desc_art[0] === 'x' || v.desc_art[0] === '(' || v.desc_art[0] === '=')
                {
                    $('#righe_bis_' + v.BIS).append('<tr><td style="padding-bottom: 0;width:10%;"></td><td style="padding-bottom: 0;width:65%;">' + v.desc_art + '</td><td style="padding-bottom: 0;width:25%;">&euro;' + parseFloat(v.prezzo_tot).toFixed(2) + '</td></tr>');
                } else
                {
                    $('#righe_bis_' + v.BIS).append('<tr><td style="padding-bottom: 0;width:10%;">' + v.quantita + '</td><td style="padding-bottom: 0;width:65%;">' + v.desc_art + '</td><td style="padding-bottom: 0;width:25%;">&euro;' + parseFloat(v.prezzo_tot).toFixed(2) + '</td></tr>');
                }

            }


        });

        /*<div class="row non-nascondere" onclick="apri_conto_split('M')" style="border: 4px solid black;"><div class="col-xs-4 btn btn-warning nopadding" style="padding-top: 1.5vh !important;padding-bottom: 1.5vh !important;font-size:3vh">M<br/>&nbsp;<span id="tot_bis_M"></span></div><div class="col-xs-8 apm_bis_M" style="height:12vh;overflow:auto;"><table id="righe_bis_M" class="table table-responsive"></table></div></div>*/


        o.forEach(function (v) {
            if (v.BIS !== null && v.BIS !== '') {
                $('#popup_bis').modal('show');
            }
        });
    }

}



function lettera_conto_split() {
    if (comanda.numero_conto_split > 0) {

        return lettere[comanda.numero_conto_split];
    } else
    {
        return "";
    }
}

function apri_conto_split(lettera) {
    $("#tasto_batti_coperti").show();
    $("#numero_coperti").hide();
    $("#numero_coperti").val("");

    var variabile_tavolo = comanda.tavolo;

    comanda.numero_conto_split = lettere.indexOf(lettera);

    if (lettera_conto_split() === "")
    {
        $('#numero_conto_split').html("BIS");
    } else
    {
        $('#numero_conto_split').html(lettera_conto_split());
    }


    var query_coperti = "SELECT nump_xml,contiene_variante,NEXIT,incassato,bancomat,carta_credito FROM comanda WHERE desc_art LIKE 'RECORD TESTA%' and ntav_comanda='" + variabile_tavolo + "' AND stato_record='ATTIVO'  AND BIS='" + lettera_conto_split() + "' ORDER BY nump_xml DESC limit 1;";
    console.log("lettura_tavolo_xml", query_coperti);
    comanda.sincro.query(query_coperti, function (rc) {
        comanda.ultimi_coperti_importati = '0';
        if (rc !== undefined && rc.length === 1)
        {

            if (rc[0].contiene_variante === "") {
                rc[0].contiene_variante = "0";
            }

            if (rc[0].NEXIT === "") {
                rc[0].NEXIT = "0";
            }

            $("#tessere_drink").html(rc[0].contiene_variante);
            $("#tessere_exit").html(rc[0].NEXIT);

            var contanti = 0;
            if (parseFloat(rc[0].incassato) > 0) {
                contanti = parseFloat(rc[0].incassato.replace(".", "").replace(",", "."));
            }
            var bancomat = 0;
            if (parseFloat(rc[0].bancomat) > 0) {
                bancomat = parseFloat(rc[0].bancomat.replace(".", "").replace(",", "."));
            }
            var carta_credito = 0;
            if (parseFloat(rc[0].carta_credito) > 0) {
                carta_credito = parseFloat(rc[0].carta_credito.replace(".", "").replace(",", "."));
            }

            var incassato = contanti + bancomat + carta_credito;

            if (incassato > 0) {

                $(".dati_tecnici_grande").css("visibility", "visible");

                $(".totale_incassato_lente").html(parseFloat(incassato).toFixed(2));
                $(".dati_tecnici_grande").show();
                $(".scritta_totale").html("t: ");
            } else
            {



                $(".dati_tecnici_grande").css("visibility", "hidden");

                $(".scritta_totale").html("");
                $(".totale_incassato_lente").html("");
                $(".dati_tecnici_grande").show();
            }



            if (!isNaN(parseInt(rc[0].nump_xml)) && parseInt(rc[0].nump_xml) > 0) {
                comanda.ultimi_coperti_importati = rc[0].nump_xml;

                $("#numero_coperti").val(comanda.ultimi_coperti_importati);

                if (comanda.ultimi_coperti_importati !== "" && comanda.ultimi_coperti_importati !== "0") {
                    $("#tasto_batti_coperti").hide();
                    $("#numero_coperti").show();
                } else
                {
                    $("#tasto_batti_coperti").show();
                    $("#numero_coperti").hide();
                }



            }
        }
    });
    var query_sconto = "SELECT BIS,sconto_imp,sconto_perc FROM comanda WHERE desc_art LIKE 'RECORD TESTA%' and ntav_comanda='" + variabile_tavolo + "' AND stato_record='ATTIVO' ORDER BY sconto_imp DESC limit 1;";
    console.log("lettura_tavolo_xml", query_sconto);
    comanda.sincro.query(query_sconto, function (rc) {


        rc.forEach(function (e) {
            if (!isNaN(parseFloat(e.sconto_perc)) && parseFloat(e.sconto_perc.replace(',', '.')) > 0)
            {
                setTimeout(function () {

                    var descrizione = 'Sconto';
                    if (typeof e.sconto_perc === 'string') {
                        descrizione = e.sconto_perc;
                    }
                    aggiungi_articolo(null, 'Sconto', '-' + parseFloat(e.sconto_perc.replace(',', '.')) + '%', 'BIS=' + e.BIS + '&descrizione=' + descrizione + '&prezzo_1=-' + parseFloat(e.sconto_perc.replace(',', '.')) + '%' + '&quantita=1', '1', 'SCONTO', null, null);
                    //aggiungi_articolo(null, 'SCONTO', '-5.00%', 'descrizione=ITALIANI&prezzo_1=-5.00%&quantita=1', '1', 'SCONTO',null,null);
                    //aggiungi_articolo(null, 'SCONTO', '-' + rc[0].sconto_perc + '%', 'descrizione=STAFF&prezzo_1=-' + rc[0].sconto_perc + '%&quantita=1', '1', 'SCONTO', null, null);
                }, 600);
            } else if (!isNaN(parseFloat(e.sconto_imp)) && parseFloat(e.sconto_imp.replace(',', '.')) > 0)
            {
                setTimeout(function () {

                    var descrizione = 'Sconto';
                    if (typeof e.sconto_perc === 'string') {
                        descrizione = e.sconto_perc;
                    }

                    aggiungi_articolo(null, 'Sconto', '-' + parseFloat(e.sconto_imp.replace(',', '.')), 'BIS=' + e.BIS + '&descrizione=' + descrizione + '&prezzo_1=-' + parseFloat(e.sconto_imp.replace(',', '.')) + '&quantita=1', '1', 'SCONTO', null, null);
                }, 600);
            }
        });
    });
    $('#tab_sx').scrollTop(0);
    comanda.funzionidb.conto_attivo();
    $('#popup_bis').modal('hide');
}

function split_inizia_conto_successivo() {
    if (comanda.numero_conto_split < 11) {
        $('.split_precedente').show();
        $('.split_successivo').removeClass('btn-warning');
        $('.split_successivo').addClass('btn-success');
        comanda.numero_conto_split++;
        $('#numero_conto_split').html(lettera_conto_split());
        comanda.funzionidb.conto_attivo();
    }
}

function split_inizia_conto_precedente() {
    if (comanda.numero_conto_split > 0) {
        comanda.numero_conto_split--;
        $('#numero_conto_split').html(lettera_conto_split());
        comanda.funzionidb.conto_attivo();
    }
}

