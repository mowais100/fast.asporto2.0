$(document).on('click', '#fatture_spesometro', function () {

    var clienti = new Object();
    var c = false;

    var TRIMESTRE = "";

    if ($('#data_incassi_partenza').multiDatesPicker('getDates')[0] && $('#data_incassi_arrivo').multiDatesPicker('getDates')[0]) {

        var DATA_PARTENZA = $('#data_incassi_partenza').multiDatesPicker('getDates')[0];
        var DATA_ARRIVO = $('#data_incassi_arrivo').multiDatesPicker('getDates')[0];

        var DP = DATA_PARTENZA.substr(6, 4) + '' + DATA_PARTENZA.substr(3, 2) + '' + DATA_PARTENZA.substr(0, 2);
        var DA = DATA_ARRIVO.substr(6, 4) + '' + DATA_ARRIVO.substr(3, 2) + '' + DATA_ARRIVO.substr(0, 2);

        TRIMESTRE = $('#data_incassi_arrivo').multiDatesPicker('getDates')[0].substr(3, 2);
    } else
    {
        bootbox.alert("SELEZIONARE L'INTERVALLO DI DATE DA ESTRARRE DAL CALENDARIO");
        return false;
    }

    var query_clienti = "select * from clienti;";
    comanda.sincro.query(query_clienti, function (dati_clienti) {

        dati_clienti.forEach(function (obj) {
            if (clienti[obj.id] === undefined) {
                clienti[obj.id] = obj;
            }
        });

        var query_teste = "select * from record_teste where substr(progressivo_comanda,5,8)>='" + DP + "' and substr(progressivo_comanda,5,8)<='" + DA + "' and tipologia='FATTURA';";
        comanda.sincro.query_incassi(query_teste, 100000, function (dati_teste) {

            var output = "CFIS-DITTA;REGISTRO;MESE;IDPAESE;PARTITA-IVA;CFIS-CLIFOR;DENOMINAZIONE;COGNOME;NOME;TIPO-DOCUMENTO;DATA-FATTURA(GG/MM/AAAA);NUM-FATTURA;DATA-REG;IMPONIBILE;IMPOSTA;ALIQUOTA;NATURA;DETRAIBILE(PERC);DEDUCIBILE(S/N);ESIGIB-IVA;SEDE-INDIR;SEDE-NUM;SEDE-CAP;SEDE-COMUNE;SEDE-PROV;SEDE-NAZ;SORG-INDIR;SORG-NUM;SORG-CAP;SORG-COMUNE;SORG-PROV;SORG-NAZ;RFIS-ID-PAESE;RFIS-PIVA;RFIS-DENOM;RFIS-NOME;RFIS-COGNOME;\r\n";
            dati_teste.forEach(function (obj) {


                if (obj.id_cliente && clienti[obj.id_cliente] && obj.id_cliente !== "N") {

                    if (clienti[obj.id_cliente].ragione_sociale === clienti[obj.id_cliente].partita_iva) {
                        clienti[obj.id_cliente].ragione_sociale = "";
                    }

                    var ALIQUOTA = 10;
                    var IMPONIBILE = parseFloat(((100 * parseFloat(obj.totale)) / (100 + ALIQUOTA)).toFixed(2));
                    var IMPOSTA = parseFloat(parseFloat(obj.totale) - IMPONIBILE).toFixed(2);
                    var PROVINCIA = "DA";
                    if (clienti[obj.id_cliente].provincia && clienti[obj.id_cliente].provincia !== "undefined" && clienti[obj.id_cliente].provincia !== "null" && typeof (clienti[obj.id_cliente].provincia) === "string" && clienti[obj.id_cliente].provincia.length === 2) {
                        PROVINCIA = clienti[obj.id_cliente].provincia.toUpperCase();
                    }

                    var PAESE = "IT";
                    if (clienti[obj.id_cliente].paese && clienti[obj.id_cliente].paese !== "undefined" && clienti[obj.id_cliente].paese !== "null") {
                        PAESE = clienti[obj.id_cliente].paese.substr(0, 2).toUpperCase();
                    }


                    output += comanda.profilo_aziendale.codice_fiscale + ";";
                    output += "E;";
                    output += TRIMESTRE + ";";
                    output += ";";
                    output += clienti[obj.id_cliente].partita_iva + ";";
                    output += clienti[obj.id_cliente].codice_fiscale.toUpperCase() + ";";
                    output += clienti[obj.id_cliente].ragione_sociale + ";";
                    output += ";";
                    output += ";";
                    output += "TD01;";
                    output += stringToDate(obj.data, "dd-mm-yy", "-") + ";";
                    output += obj.numero_fiscale + ";";
                    output += stringToDate(obj.data, "dd-mm-yy", "-") + ";";
                    output += IMPONIBILE + ";";
                    output += IMPOSTA + ";";
                    output += "10;";
                    output += ";";
                    output += ";";
                    output += ";";
                    output += "I;";
                    output += clienti[obj.id_cliente].indirizzo + ";";
                    output += clienti[obj.id_cliente].numero + ";";
                    output += clienti[obj.id_cliente].cap + ";";
                    output += clienti[obj.id_cliente].citta + ";";
                    output += PROVINCIA + ";";
                    output += PAESE + ";";
                    output += ";";
                    output += ";";
                    output += ";";
                    output += ";";
                    output += ";";
                    output += ";";
                    output += ";";
                    output += ";";
                    output += ";";
                    output += ";";
                    output += ";";
                    output += "\r\n";
                } else
                {




                    var ALIQUOTA = 10;
                    var IMPONIBILE = parseFloat(((100 * parseFloat(obj.totale)) / (100 + ALIQUOTA)).toFixed(2));
                    var IMPOSTA = parseFloat(parseFloat(obj.totale) - IMPONIBILE).toFixed(2);
                    var PROVINCIA = "DA";


                    //QUELLE SENZA PAESE SONO DISASSOCIATE DALL ANAGRAFICA
                    var PAESE = "";



                    output += comanda.profilo_aziendale.codice_fiscale + ";";
                    output += "E;";
                    output += TRIMESTRE + ";";
                    output += ";";
                    output += ";";
                    output += ";";
                    output += ";";
                    output += ";";
                    output += ";";
                    output += "TD01;";
                    output += stringToDate(obj.data, "dd-mm-yy", "-") + ";";
                    output += obj.numero_fiscale + ";";
                    output += stringToDate(obj.data, "dd-mm-yy", "-") + ";";
                    output += IMPONIBILE + ";";
                    output += IMPOSTA + ";";
                    output += "10;";
                    output += ";";
                    output += ";";
                    output += ";";
                    output += "I;";
                    output += ";";
                    output += ";";
                    output += ";";
                    output += ";";
                    output += PROVINCIA + ";";
                    output += PAESE + ";";
                    output += ";";
                    output += ";";
                    output += ";";
                    output += ";";
                    output += ";";
                    output += ";";
                    output += ";";
                    output += ";";
                    output += ";";
                    output += ";";
                    output += ";";
                    output += "\r\n";

                    if (c === false) {
                        bootbox.alert("ATTENZIONE: CI SONO FATTURE ASSOCIATE A CLIENTI CANCELLATI DALL'ANAGRAFICA (LE P.IVE SONO STATE INSERITE COMUNQUE NEL FILE ESPORTATO).");
                        c = true;
                    }
                }

            });

            console.log("SPESOMETRO", output);

            $.ajax({
                type: "POST",
                url: comanda.url_server + "classi_php/salva_file_generico.php",
                data: {nome: "SPESOMETRO_" + new Date().format("ddmmyyyy") + "_" + comanda.operatore + ".csv", contenuto: output},
                success: function () {
                    bootbox.alert("SPESOMETRO ESPORTATO IN FILE_SALVATI");
                }
            });

        });
    });
});
$(document).on('click', '#btn_info_fiscali', function () {
    var info_chiusura = 0;
    var epos = new epson.fiscalPrint();
    //Chiamata ajax asincrona dove va eseguita dentro tutta l'azione del programma
    epos.onreceive = function (result, tag_names_array, add_info, res_add) {
        //console.log(result);
        //console.log(add_info);

        if (add_info !== undefined && add_info.responseData !== undefined) {
            var risposta = add_info.responseData;
            info_chiusura = risposta;
        }
    };
    epos.onerror = function (result) {
    };
    //INTESTAZIONE PER TUTTI I COMANDI GENERICI
    var com_open = '<printerCommand>';
    var com_close = '</printerCommand>';
    //FUNZIONI PER OGNI LETTURA

    //contanti
    var com_contanti = com_open + '<directIO  command="2050" data="2700" />' + com_close;
    epos.send(window.location.protocol + "//" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi", com_contanti, 10000);
    setTimeout(function () {
        info_chiusura = info_chiusura[2];
        info_chiusura = info_chiusura.split("+");
        info_chiusura = parseInt(info_chiusura);
        $('#chius_numero_chiusure').val(info_chiusura);
        var com_contanti = com_open + '<directIO  command="1077" data="1" />' + com_close;
        epos.send(window.location.protocol + "//" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi", com_contanti, 10000);
    }, 200);
    setTimeout(function () {

        info_chiusura = info_chiusura.substr(2, 2);
        info_chiusura = parseInt(info_chiusura);
        console.log(info_chiusura);
        $('#chius_utilizzo_dgfe').val(info_chiusura);
    }, 600);
});
$(document).on('click', '#btn_void', function () {
    var epos = new epson.fiscalPrint();
    epos.send(window.location.protocol + "//" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi", "<printerCommand><resetPrinter  operator='1' /></printerCommand>", 10000);
});
$(document).on('click', '#btn_chiusura_fiscale_totali', function () {
    chiusura_fiscale(true, true);
});

$(document).on('click', '#btn_chiusura_venduti', function () {
    stampa_totali_giornata('dettagliovenduti');
    chiusura_fiscale(true, false);
});

$(document).on('click', '#btn_solochiusura', function () {
    chiusura_fiscale(true, false);
});
$(document).on('click', '#totali_fine_giornata', function () {
    chiusura_fiscale(false);
});

$(document).on('click', '#btn_ristampa_documenti_fiscali', function () {
    popup_ristampa_documenti_fiscali();
});

$(document).on('click', '#stampa_incassi_nf', function () {
    $('#stampa_incassi_nf').css('pointer-events', 'none');
    $('#stampa_incassi_nf').css('opacity', '0.5');
    setTimeout(function () {
        $('#stampa_incassi_nf').css('pointer-events', '');
        $('#stampa_incassi_nf').css('opacity', '');
    }, 10000);
    stampa_totali_giornata('soloincassi');
});
$(document).on('click', '#incassi_puntocassa_nf', function () {
    stampa_totali_giornata('soloincassi', undefined, 'PUNTOCASSA');
});
$(document).on('click', '#stampa_incassi_nf_giornata', function () {
    stampa_totali_giornata('soloincassi', undefined, 'giornata');
});
$(document).on('click', '#visu_incassi_nf_giornata', function () {
    stampa_totali_giornata("soloincassi", undefined, "giornata", true);
});
$(document).on('click', '#riepilogo_buoni_giornata', function () {
    riepilogo_buoni_pasto();
});
$(document).on('click', '#scelta_data', function () {
    $('#popup_scelta_data').modal('show');
    var testo_query = "select distinct nome_pc from comanda;";
    comanda.sincro.query(testo_query, function (t) {
        var html = '<div class="row"><div class="col-xs-12">Per specificare da quali punti cassa estrarre i dati, clicca sulle caselle, altrimenti lascia così.</div></div><div class="row">&nbsp;</div>';
        t.forEach(function (o) {
            if (o.nome_pc.length >= 1) {
                html += '<div class="row"><div class="col-xs-1"><input class="form-control" type="checkbox" value="' + o.nome_pc + '"></div><div class="col-xs-11">' + o.nome_pc + '</div></div>';
            }
        });
        //html += '<div class="row">&nbsp;</div><div class="row"><div class="col-xs-1"><input class="form-control" type="checkbox" name=""></div><div class="col-xs-11">TUTTI</div></div>';


        $('#selezione_pc_incassi').html(html);
    });
});
$(document).on('click', '#scelta_data_2', function () {
    $('#popup_scelta_data').modal('show');
    var testo_query = "select distinct nome_pc from comanda;";
    comanda.sincro.query(testo_query, function (t) {
        var html = '<div class="row"><div class="col-xs-12">Per specificare da quali punti cassa estrarre i dati, clicca sulle caselle, altrimenti lascia così.</div></div>';
        t.forEach(function (o) {
            if (o.nome_pc.length >= 1) {
                html += '<div class="row"><div class="col-xs-1"><input class="form-control" type="checkbox" value="' + o.nome_pc + '"></div><div class="col-xs-11">' + o.nome_pc + '</div></div>';
            }
        });
        //html += '<div class="row">&nbsp;</div><div class="row"><div class="col-xs-1"><input class="form-control" type="checkbox" name=""></div><div class="col-xs-11">TUTTI</div></div>';

        $('#selezione_pc_incassi').html(html);
    });
});
$(document).on('click', '#fattura_buoni', function () {
    fatture_buoni_a4();
});
$(document).on('click', '#btn_lettura_totali_conto', lettura_totali_fiscali);

async function lettura_totali_fiscali() {

    await lettura_totali_fiscali_contanti();
    await lettura_totali_fiscali_carte_credito();
    await lettura_totali_fiscali_assegni();
    //await lettura_totali_fiscali_buoni_acquisto();
    await lettura_totali_fiscali_non_pagato();
    await lettura_totali_fiscali_non_riscosso_beni();
    await lettura_totali_fiscali_non_riscosso_servizi();
    //await lettura_totali_fiscali_buoni_pasto_monouso();
    await lettura_totali_fiscali_pagamento_buoni_pasto();
    //await lettura_totali_fiscali_prepagato();   
    await lettura_totali_fiscali_sconto_pagare();
    
    await lettura_totali_fiscali_totale_giornaliero();

}

async function lettura_totali_fiscali_parte_fissa(a, b, c) {

    return new Promise((resolve, reject) => {
        //INTESTAZIONE PER TUTTI I COMANDI GENERICI
        var com_open = '<printerCommand>';
        var com_close = '</printerCommand>';
        //FUNZIONI PER OGNI LETTURA

        var epos = new epson.fiscalPrint();
        //Chiamata ajax asincrona dove va eseguita dentro tutta l'azione del programma
        epos.onreceive = function (result, tag_names_array, add_info, res_add) {
            //console.log(result);
            //console.log(add_info);

            if (add_info !== undefined && add_info.responseData !== undefined) {
                var risposta = add_info.responseData;
                risposta = risposta.split(/\+|-/ig);
                risposta[2] = risposta[2].slice(0, -2) + "." + risposta[2].slice(-2);
                risposta[2] = parseFloat(risposta[2]).toFixed(2);
                console.log("risposta = '" + risposta[2] + "'");
                risposta_pubblica = risposta[2];
                resolve(risposta_pubblica);
            } else {
                resolve(null);
            }

        };
        epos.onerror = function (result) {
            resolve(null);
        };

        epos.send(a, com_open + b + com_close, c);

    });
}





async function lettura_totali_fiscali_carte_credito() {

    let valore = 0;
    let risposta_pubblica = "";

    for (let i = 0; i <= 10; i++) {
        var com_princ = '<directIO  command="2050" data="18' + aggZero(i, 2) + '" />';
        risposta_pubblica = await lettura_totali_fiscali_parte_fissa(window.location.protocol + "//" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi", com_princ, 10000);
        valore += parseFloat(risposta_pubblica);
    }

    $('#chius_carte_credito').val(valore.toFixed(2));
    console.log(valore);

}

async function lettura_totali_fiscali_assegni() {
    let valore = 0;
    let risposta_pubblica = "";
    for (let i = 0; i <= 6; i++) {
        var com_princ = '<directIO  command="2050" data="17' + aggZero(i, 2) + '" />';
        risposta_pubblica = await lettura_totali_fiscali_parte_fissa(window.location.protocol + "//" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi", com_princ, 10000);
        valore += parseFloat(risposta_pubblica);
    }
    $('#chius_assegni').val(valore.toFixed(2));
    console.log(risposta_pubblica);
}

async function lettura_totali_fiscali_buoni_acquisto() {
    var com_princ = '<directIO  command="2050" data="1906" />';
    let risposta_pubblica = await lettura_totali_fiscali_parte_fissa(window.location.protocol + "//" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi", com_princ, 10000);
    $('#chius_buoni_acquisto').val(risposta_pubblica);
    console.log(risposta_pubblica);
}

async function lettura_totali_fiscali_pagamento_buoni_pasto() {
    let valore = 0;
    let risposta_pubblica = "";

    for (let i = 1; i <= 10; i++) {
        var com_princ = '<directIO  command="2050" data="19' + aggZero(i, 2) + '" />';
        risposta_pubblica = await lettura_totali_fiscali_parte_fissa(window.location.protocol + "//" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi", com_princ, 10000);
        valore += parseFloat(risposta_pubblica);
    }

    $('#chius_buoni_pagamento_buoni_pasto').val(valore.toFixed(2));
    console.log(valore);
}

async function lettura_totali_fiscali_buoni_pasto_monouso() {
    var com_princ = '<directIO  command="2050" data="1905" />';
    let risposta_pubblica = await lettura_totali_fiscali_parte_fissa(window.location.protocol + "//" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi", com_princ, 10000);
    $('#chius_buoni_pasto_monouso').val(risposta_pubblica);
    console.log(risposta_pubblica);
}

async function lettura_totali_fiscali_buoni_pasto() {
    var com_princ = '<directIO  command="2050" data="1904" />';
    let risposta_pubblica = await lettura_totali_fiscali_parte_fissa(window.location.protocol + "//" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi", com_princ, 10000);
    $('#chius_buoni_pasto').val(risposta_pubblica);
    console.log(risposta_pubblica);
}


async function lettura_totali_fiscali_prepagato() {
    var com_princ = '<directIO  command="2050" data="1902" />';
    let risposta_pubblica = await lettura_totali_fiscali_parte_fissa(window.location.protocol + "//" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi", com_princ, 10000);
    $('#chius_prepagato').val(risposta_pubblica);
    console.log(risposta_pubblica);
}

async function lettura_totali_fiscali_non_pagato() {
    var com_princ = '<directIO  command="2050" data="7300" />';
    let risposta_pubblica = await lettura_totali_fiscali_parte_fissa(window.location.protocol + "//" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi", com_princ, 10000);
    $('#chius_non_pagato').val(risposta_pubblica);
    console.log(risposta_pubblica);
}

async function lettura_totali_fiscali_non_riscosso_beni() {
    var com_princ = '<directIO  command="2050" data="7400" />';
    let risposta_pubblica = await lettura_totali_fiscali_parte_fissa(window.location.protocol + "//" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi", com_princ, 10000);
    $('#chius_non_riscosso_beni').val(risposta_pubblica);
    console.log(risposta_pubblica);
}

async function lettura_totali_fiscali_non_riscosso_servizi() {
    var com_princ = '<directIO  command="2050" data="7500" />';
    let risposta_pubblica = await lettura_totali_fiscali_parte_fissa(window.location.protocol + "//" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi", com_princ, 10000);
    $('#chius_non_riscosso_servizi').val(risposta_pubblica);
    console.log(risposta_pubblica);
}


async function lettura_totali_fiscali_sconto_pagare() {
    var com_princ = '<directIO  command="2050" data="7900" />';
    let risposta_pubblica = await lettura_totali_fiscali_parte_fissa(window.location.protocol + "//" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi", com_princ, 10000);
    $('#chius_sconto_pagare').val(risposta_pubblica);
    console.log(risposta_pubblica);
}

async function lettura_totali_fiscali_contanti() {
    var com_princ = '<directIO  command="2050" data="1300" />';
    risposta_pubblica = await lettura_totali_fiscali_parte_fissa(window.location.protocol + "//" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi", com_princ, 10000);
    $('#chius_contanti').val(risposta_pubblica);
    console.log(risposta_pubblica);
}

async function lettura_totali_fiscali_totale_giornaliero() {
    var com_princ = '<directIO  command="2050" data="2800" />';
    risposta_pubblica = await lettura_totali_fiscali_parte_fissa(window.location.protocol + "//" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi", com_princ, 10000);
    $('#chius_totale_giorno').val(risposta_pubblica);
    console.log(risposta_pubblica);
}
