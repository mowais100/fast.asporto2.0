//Esempio per ricevere socket già inviati dagli altri
//comanda.sock.send({tipo: "richiesta_aggiornamento", operatore: comanda.operatore, query: '0', terminale: comanda.terminale, ip: comanda.ip_address});
//devi ricevere da tutti i socket attivi un array ordinata per tempo (time)
//quando sono state ricevute tutte (20 secondi massimissimo)
//si mergono, si ordinano per tempo e si fanno le query


function socket_cassa(ip) {

    var url = 'http://' + ip + '/fast.intellinet/socketMISURATORE.php';

    var xhr = new XMLHttpRequest();
    xhr.timeout = 2000;
    xhr.open('GET', url, true);
    xhr.send();

}

//CHIAMO LA FUNZIONE ALLA FINE DELLA PAGINA

var IntervalloSocketSend;
var test_standby_socket;

var websocket_0 = new Object();
var websocket_1 = new Object();
var websocket_2 = new Object();



function socket_init(CBC) {

    if (comanda.compatibile_xml === true) {

        if (typeof (CBC) === "function") {
            CBC(true);
        } else {
            bootbox.alert("Errore tecnico nella Callback del Socket. Probabilmente hai la cassa impostata come cassa_singola e hai socket impostati nella tabella socket_listener. Contatta l'assistenza per risolvere il problema immediatamente.")
        }

    } else {

        //VARIABILE PER INTERVALLI

        var addr = new Array(3);

        comanda.ora_ultimo_socket_inviato = 0;

        comanda.errore_socket_1 = false;
        comanda.errore_socket_2 = false;
        comanda.errore_socket_3 = false;


        //VA BENE COSI PERCHE ANCHE QUANDO LO CHIAMI IN CASSA SEI NEL SUO IP SULLA BARRA

        var testo_query = "SELECT ip,tipo FROM socket_listeners;";

        comanda.sincro.query(testo_query, function (indirizzo) {

            //-SOCKET 1-//

            if (indirizzo !== undefined) {

                if (indirizzo[0] !== undefined && indirizzo[0].ip !== undefined && indirizzo[0].ip !== null && indirizzo[0].ip !== '') {
                    //Socket su server separato
                    var socket_address = indirizzo[0].ip;

                    if (socket_address !== null && socket_address !== undefined && socket_address !== '') {

                        addr[0] = "ws://" + socket_address + ":9000/socket_listener_1.php";
                    }
                }


                //--SOCKET2--/

                if (indirizzo[1] !== undefined && indirizzo[1].ip !== undefined && indirizzo[1].ip !== null && indirizzo[1].ip !== '') {
                    //Socket su server separato
                    var socket_address = indirizzo[1].ip;

                    //Socket su cassa/server
                    //var socket_address = window.location.hostname;

                    if (socket_address !== null && socket_address !== undefined && socket_address !== '') {


                        addr[1] = "ws://" + socket_address + ":9000/socket_listener_1.php";

                    }
                }


                //---SOCKET3---//

                if (indirizzo[2] !== undefined && indirizzo[2].ip !== undefined && indirizzo[2].ip !== null && indirizzo[2].ip !== '') {
                    //Socket su server separato
                    var socket_address = indirizzo[2].ip;

                    //Socket su cassa/server
                    //var socket_address = window.location.hostname;

                    if (socket_address !== null && socket_address !== undefined && socket_address !== '') {

                        addr[2] = "ws://" + socket_address + ":9000/socket_listener_1.php";

                    }

                }
            }
        });

        var intervallosocket0 = setInterval(function () {

            if (addr[0] !== undefined && typeof (addr[0]) === 'string') {

                if (websocket[0] !== undefined) {
                    //console.log("SOCKET TEMPI", websocket[0] === undefined, "OR", Math.floor(Date.now() / 1000) - parseInt(websocket[0].ultimavolta) > 3, "OR", isNaN(websocket[0].ultimavolta), "VAL", websocket[0]);
                }
                if (websocket[0] === undefined || Math.floor(Date.now() / 1000) - parseInt(websocket[0].ultimavolta) > 10 || isNaN(websocket[0].ultimavolta)) {


                    if (websocket[0] !== undefined) {
                        console.log("SOCKET TEMPI", websocket[0] !== undefined, "AND", websocket[0].readyState < 2, "VAL", websocket[0]);
                    }
                    if (websocket[0] !== undefined && websocket[0].readyState === 1)
                    {
                        $('.spia.rtengine1').css('background-color', 'red');

                        console.log("SOCKET TEMPI", "CLOSE", websocket[0]);
                        websocket[0].close();
                    }

                    if (websocket[0] === undefined || websocket[0].readyState > 1) {

                        console.log("CREAZIONE SOCKET");
                        websocket[0] = new WebSocket(addr[0]);

                        websocket[0].RAMsize = 0;
                        websocket[0].indici = 0;

                        websocket[0].ora_disattivazione = 0;

                    }

                    websocket[0].onopen = function (ev) {

                        //console.log("TENTATA RICHIESTA AGGIORNAMENTO", websocket[0].readyState, websocket_0.orario_aggiornamenti !== undefined);
                        if (websocket[0] !== undefined && websocket[0].readyState === 1 && websocket_0.orario_aggiornamenti !== undefined) {

                            //console.log("RICHIESTA AGGIORNAMENTO", websocket[0].readyState, websocket_0.orario_aggiornamenti !== undefined);

                            websocket[0].send(JSON.stringify({tipo: "richiesta_aggiornamento", operatore: comanda.operatore, query: websocket_0.orario_aggiornamenti.toString(), terminale: comanda.terminale, ip: comanda.ip_address}));

                        }

                        //alert("RTEngine1 ACCESO");

                        $('.spia.rtengine1').css('background-color', 'green');

                        comanda.errore_socket_1 = false;

                        if (typeof (CBC) === "function") {
                            CBC(true);
                        } else {
                            bootbox.alert("Errore tecnico nella Callback del Socket. Probabilmente hai la cassa impostata come cassa_singola e hai socket impostati nella tabella socket_listener. Contatta l'assistenza per risolvere il problema immediatamente.")
                        }

                    };

                    websocket[0].onmessage = function (ev) {
                        websocket[0].ultimavolta = Math.floor(Date.now() / 1000);

                        var msg = JSON.parse(ev.data);

                        comanda.socket_ricevuto_mod = msg;


                        if (comanda.socket_ricevuto_mod.orario !== undefined)
                        {
                            websocket_0.orario_aggiornamenti = comanda.socket_ricevuto_mod.orario;

                            delete comanda.socket_ricevuto_mod.orario;
                        }

                        if (msg !== undefined && msg !== null && msg.operatore !== undefined && comanda.terminale === msg.terminale && comanda.ip_address === msg.ip) {
                            comanda.socket_ricevuto = msg;

                            //SERVE A CONFRONTARE IL SOCKET RICEVUTO SENZA ORARIO DEL SERVER PERCHE' ALTRIMENTI SAREBBE PER FORZA DIVERSO


                            if (JSON.stringify(comanda.socket_ricevuto_mod) === comanda.socket_inviato) {

                                comanda.socket_ciclizzati.shift();

                                clearInterval(test_standby_socket);

                                IntervalloSocketSend.resume();

                            }
                        }

                        if (msg !== null && (comanda.operatore !== msg.operatore || comanda.terminale !== msg.terminale || comanda.ip_address !== msg.ip)) {

                            var testo_query;

                            switch (msg.tipo) {
                                default:
                                    testo_query = msg.query;
                                    break;

                            }

                            if (msg !== undefined && msg !== null && msg.query !== undefined && msg.query !== null && msg.tipo === 'notification' && msg.query.indexOf('inizio download comanda') !== -1 && msg.query.indexOf(comanda.ip_address) === -1) {


                                var r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;

                                var t = msg.query.match(r);

                                comanda.array_dbcentrale_mancanti.forEach(function (messaggio) {

                                    comanda.sock.send({tipo: "coda", operatore: comanda.operatore, query: messaggio, terminale: comanda.terminale, ip: comanda.ip_address, ip_dest: t[0]});

                                });

                            } else if (msg.tipo === "stampa_comanda") {

                                if (comanda.stampa_ordini_qui === true) {
                                    comanda.tavolo = msg.query;
                                    stampa_comanda(undefined, undefined, undefined, undefined, undefined, true);
                                    console.log("STOP");
                                }

                            } else if (msg.tipo === "stampa_comanda_fastorder") {


                                comanda.tavolo = msg.query;
                                stampa_comanda(undefined, undefined, undefined, undefined, undefined, true);


                            } else if (msg.tipo === "risposta_dati_db_locali") {

                                $('#popup_test_allineamento table').append('<tr><td>' + msg.ip + '</td><td>' + msg.terminale + '</td><td>' + msg.operatore + '</td><td>' + msg.query.split(';')[0] + '</td><td>' + parseFloat(msg.query.split(';')[1]).toFixed(2) + '</td><td>' + msg.query.split(';')[2] + '</td></tr>');

                            } else if (msg.tipo === "richiesta_riallineamento_dati") {
                                risposta_riallineamento_dati();
                            } else if (comanda.verifica_periodica_tavoli_abilitata === true && msg.tipo === "richiesta_status_tavoli") {
                                conferma_propri_tavoli_bloccati();
                            } else if (comanda.verifica_periodica_tavoli_abilitata === true && msg.tipo === "risposta_status_tavoli") {
                                aggiungi_tavolo_bloccato_davvero(msg);
                            } else if (msg.tipo === "richiesta_status_terminali") {
                                risposta_status_terminali();
                            } else if (msg.tipo === "risposta_status_terminali") {
                                aggiungi_terminale_acceso_davvero(msg);
                            } else {

                                if (testo_query !== undefined && comanda.loading_tab_tavoli !== true && comanda.loading_tab_comanda !== true) {

                                    if (testo_query.indexOf("record_teste") !== -1) {
                                        //Query Asincrona per le teste
                                        comanda.sincro.query(testo_query, function () {});
                                    } else
                                    {
                                        comanda.sincro.query_sync(testo_query, function () {
                                            if (msg.tipo === "aggiornamento_settaggi_principali") {
                                                var query_impostazioni = "SELECT valore_coperto,coperti_obbligatorio,valore_servizio FROM impostazioni_fiscali;";
                                                comanda.sincro.query_sync(query_impostazioni, function (risultato_impostazioni) {

                                                    comanda.valore_coperto = risultato_impostazioni[0].valore_coperto;
                                                    comanda.coperti_obbligatorio = risultato_impostazioni[0].coperti_obbligatorio;
                                                    comanda.percentuale_servizio = risultato_impostazioni[0].valore_servizio;

                                                    alert("CHIEDI ALL'AMMINISTRATORE SE E' STATA CAMBIATA LINGUA DI STAMPA E/O VISUALIZZAZIONE, ED EVENTUALMENTE RIAVVIA IL DISPOSITIVO.");
                                                });
                                            } else if (msg.tipo === "aggiornamento_righe_forno") {
                                                righe_forno(true);
                                            } else if (msg.tipo === "aggiornamento_stampanti") {
                                                corrispondenze_stampanti(comanda.lingua_stampa);
                                            } else if (msg.tipo === "aggiornamento_profilo_aziendale") {
                                                importa_profilo_aziendale_ram(function () {});
                                            } else if (msg.tipo === "richiesta_dati_db_locali") {
                                                risposta_test_allineamento();
                                            } else if (msg.tipo === "tavolo_occupato" || msg.tipo === "comanda_stampata" || msg.tipo === "comanda_lanciata" || msg.tipo === "chiudi_scontrino")
                                            {
                                                if (typeof disegna_ultimo_tavolo_modificato == 'function' && comanda.permesso === 0) {
                                                    disegna_ultimo_tavolo_modificato(testo_query);
                                                }
                                            } else if (msg.tipo === "cancella_tutti_tavoli") {
                                                disegna_tavoli_salvati();
                                            } else if (msg.tipo === "refresh")
                                            {
                                                refresh();
                                            } else if (msg.tipo === "aggiornamento_categorie" || msg.tipo === "eliminazione_categorie")
                                            {
                                                elenco_categorie("list", "1", "0", "#tab_sx");
                                            } else if (msg.tipo === "aggiornamento_posizione_prodotti" || msg.tipo === "aggiornamento_prodotti" || msg.tipo === "eliminazione_prodotti") {
                                                crea_griglia_prodotti(comanda.dispositivo, function () {
                                                    if (comanda.mobile !== true) {
                                                        /* se non usassi questa condizione, se sono su lente + sul telefono mi scazza tutto il layout */
                                                        elenco_prodotti();
                                                    }
                                                });
                                            }

                                        });
                                    }
                                } else if (testo_query !== undefined)
                                {
                                    if (comanda.loading_tab_comanda === true) {
                                        comanda.socket_sospesi.push(testo_query);
                                        console.log("SOCKET SOSPESI COMANDA", comanda.socket_sospesi);
                                    } else if (comanda.loading_tab_tavoli === true) {
                                        console.log("SOCKET SOSPESI TAVOLI", comanda.socket_sospesi_tavoli);
                                        comanda.socket_sospesi_tavoli.push(testo_query);

                                    }
                                }
                            }
                        }

                    };

                    websocket[0].onerror = function (ev) {

                        console.log("SOCKET ONERROR", ev);

                        if (comanda.errore_socket_1 === false) {

                            $('.spia.rtengine1').css('background-color', 'red');


                            //QUESTO SIGNIFICA CHE TUTTI I SOCKET SONO DISCONNESSI
                            if (comanda.socket_starting !== true && ($('.spia.rtengine1').css('background-color') !== "rgb(0, 128, 0)" && $('.spia.rtengine2').css('background-color') !== "rgb(0, 128, 0)" && $('.spia.rtengine1').css('background-color') !== "rgb(0, 128, 0)"))
                            {
                                //alert("ERRORE DI RETE (1-2)!\n\n\n\nHAI PERSO IL COLLEGAMENTO A TUTTI GLI RTEngine!\n\nRIPRISTINA SUBITO IL COLLEGAMENTO.");

                            } else
                            {
                                //alert("RTEngine1 SPENTO");

                            }

                        }

                        comanda.errore_socket_1 = true;

                    };

                    websocket[0].onclose = function (ev) {

                        console.log("SOCKET ONCLOSE", ev);

                        $('.spia.rtengine1').css('background-color', 'red');

                        comanda.errore_socket_1 = true;

                    };

                } else if (Date.now() - websocket[0].ora_disattivazione > 2000)
                {
                    $('.spia.rtengine1').css('background-color', 'green');

                }
            }

        }, 2500);

        var intervallosocket1 = setInterval(function () {

            if (addr[1] !== undefined && typeof (addr[1]) === 'string') {

                if (websocket[1] !== undefined) {
                    //console.log("SOCKET TEMPI", websocket[1] === undefined, "OR", Math.floor(Date.now() / 1000) - parseInt(websocket[1].ultimavolta) > 3, "OR", isNaN(websocket[1].ultimavolta), "VAL", websocket[1]);
                }
                if (websocket[1] === undefined || Math.floor(Date.now() / 1000) - parseInt(websocket[1].ultimavolta) > 10 || isNaN(websocket[1].ultimavolta)) {


                    if (websocket[1] !== undefined) {
                        console.log("SOCKET TEMPI", websocket[1] !== undefined, "AND", websocket[1].readyState < 2, "VAL", websocket[1]);
                    }
                    if (websocket[1] !== undefined && websocket[1].readyState === 1)
                    {
                        $('.spia.rtengine2').css('background-color', 'red');

                        console.log("SOCKET TEMPI", "CLOSE", websocket[1]);
                        websocket[1].close();
                    }

                    if (websocket[1] === undefined || websocket[1].readyState > 1) {

                        console.log("CREAZIONE SOCKET");
                        websocket[1] = new WebSocket(addr[1]);

                        websocket[1].RAMsize = 0;

                        websocket[1].indici = 0;

                        websocket[1].ora_disattivazione = 0;

                    }

                    websocket[1].onopen = function (ev) {

                        //console.log("TENTATA RICHIESTA AGGIORNAMENTO", websocket[1].readyState, websocket_1.orario_aggiornamenti !== undefined);
                        if (websocket[1] !== undefined && websocket[1].readyState === 1 && websocket_1.orario_aggiornamenti !== undefined) {

                            //console.log("RICHIESTA AGGIORNAMENTO", websocket[1].readyState, websocket_1.orario_aggiornamenti !== undefined);

                            websocket[1].send(JSON.stringify({tipo: "richiesta_aggiornamento", operatore: comanda.operatore, query: websocket_1.orario_aggiornamenti.toString(), terminale: comanda.terminale, ip: comanda.ip_address}));

                        }

                        //alert("RTEngine2 ACCESO");

                        $('.spia.rtengine2').css('background-color', 'green');

                        comanda.errore_socket_2 = false;

                        if (typeof (CBC) === "function") {
                            CBC(true);
                        } else {
                            bootbox.alert("Errore tecnico nella Callback del Socket. Probabilmente hai la cassa impostata come cassa_singola e hai socket impostati nella tabella socket_listener. Contatta l'assistenza per risolvere il problema immediatamente.")
                        }

                    };

                    websocket[1].onmessage = function (ev) {
                        websocket[1].ultimavolta = Math.floor(Date.now() / 1000);

                        var msg = JSON.parse(ev.data);

                        comanda.socket_ricevuto_mod = msg;


                        if (comanda.socket_ricevuto_mod.orario !== undefined)
                        {
                            websocket_1.orario_aggiornamenti = comanda.socket_ricevuto_mod.orario;

                            delete comanda.socket_ricevuto_mod.orario;
                        }

                        if (msg !== undefined && msg !== null && msg.operatore !== undefined && comanda.terminale === msg.terminale && comanda.ip_address === msg.ip) {
                            comanda.socket_ricevuto = msg;

                            //SERVE A CONFRONTARE IL SOCKET RICEVUTO SENZA ORARIO DEL SERVER PERCHE' ALTRIMENTI SAREBBE PER FORZA DIVERSO


                            if (JSON.stringify(comanda.socket_ricevuto_mod) === comanda.socket_inviato) {

                                comanda.socket_ciclizzati.shift();

                                clearInterval(test_standby_socket);

                                IntervalloSocketSend.resume();

                            }
                        }

                        if (msg !== null && (comanda.operatore !== msg.operatore || comanda.terminale !== msg.terminale || comanda.ip_address !== msg.ip)) {

                            var testo_query;

                            switch (msg.tipo) {
                                default:
                                    testo_query = msg.query;
                                    break;

                            }

                            if (msg !== undefined && msg !== null && msg.query !== undefined && msg.query !== null && msg.tipo === 'notification' && msg.query.indexOf('inizio download comanda') !== -1 && msg.query.indexOf(comanda.ip_address) === -1) {

                                var r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;

                                var t = msg.query.match(r);

                                comanda.array_dbcentrale_mancanti.forEach(function (messaggio) {

                                    comanda.sock.send({tipo: "coda", operatore: comanda.operatore, query: messaggio, terminale: comanda.terminale, ip: comanda.ip_address, ip_dest: t[0]});

                                });

                            } else if (msg.tipo === "stampa_comanda") {

                                if (comanda.stampa_ordini_qui === true) {
                                    comanda.tavolo = msg.query;
                                    stampa_comanda(undefined, undefined, undefined, undefined, undefined, true);
                                    console.log("STOP");
                                }

                            } else if (msg.tipo === "stampa_comanda_fastorder") {


                                comanda.tavolo = msg.query;
                                stampa_comanda(undefined, undefined, undefined, undefined, undefined, true);


                            } else if (msg.tipo === "risposta_dati_db_locali") {

                                $('#popup_test_allineamento table').append('<tr><td>' + msg.ip + '</td><td>' + msg.terminale + '</td><td>' + msg.operatore + '</td><td>' + msg.query.split(';')[0] + '</td><td>' + parseFloat(msg.query.split(';')[1]).toFixed(2) + '</td><td>' + msg.query.split(';')[2] + '</td></tr>');

                            } else if (msg.tipo === "richiesta_riallineamento_dati") {
                                risposta_riallineamento_dati();
                            } else if (comanda.verifica_periodica_tavoli_abilitata === true && msg.tipo === "richiesta_status_tavoli") {
                                conferma_propri_tavoli_bloccati();
                            } else if (comanda.verifica_periodica_tavoli_abilitata === true && msg.tipo === "risposta_status_tavoli") {
                                aggiungi_tavolo_bloccato_davvero(msg);
                            } else if (msg.tipo === "richiesta_status_terminali") {
                                risposta_status_terminali();
                            } else if (msg.tipo === "risposta_status_terminali") {
                                aggiungi_terminale_acceso_davvero(msg);
                            } else {

                                if (testo_query !== undefined && comanda.loading_tab_tavoli !== true && comanda.loading_tab_comanda !== true) {
                                    if (testo_query.indexOf("record_teste") !== -1) {
                                        //Query Asincrona per le teste
                                        comanda.sincro.query(testo_query, function () {});
                                    } else
                                    {
                                        comanda.sincro.query_sync(testo_query, function () {
                                            if (msg.tipo === "aggiornamento_settaggi_principali") {
                                                var query_impostazioni = "SELECT valore_coperto,coperti_obbligatorio,valore_servizio FROM impostazioni_fiscali;";
                                                comanda.sincro.query_sync(query_impostazioni, function (risultato_impostazioni) {

                                                    comanda.valore_coperto = risultato_impostazioni[0].valore_coperto;
                                                    comanda.coperti_obbligatorio = risultato_impostazioni[0].coperti_obbligatorio;
                                                    comanda.percentuale_servizio = risultato_impostazioni[0].valore_servizio;

                                                    alert("CHIEDI ALL'AMMINISTRATORE SE E' STATA CAMBIATA LINGUA DI STAMPA E/O VISUALIZZAZIONE, ED EVENTUALMENTE RIAVVIA IL DISPOSITIVO.");
                                                });
                                            } else if (msg.tipo === "aggiornamento_righe_forno") {
                                                righe_forno(true);
                                            } else if (msg.tipo === "aggiornamento_stampanti") {
                                                corrispondenze_stampanti(comanda.lingua_stampa);
                                            } else if (msg.tipo === "aggiornamento_profilo_aziendale") {
                                                importa_profilo_aziendale_ram(function () {});
                                            } else if (msg.tipo === "richiesta_dati_db_locali") {
                                                risposta_test_allineamento();
                                            } else if (msg.tipo === "tavolo_occupato" || msg.tipo === "comanda_stampata" || msg.tipo === "comanda_lanciata" || msg.tipo === "chiudi_scontrino")
                                            {
                                                if (typeof disegna_ultimo_tavolo_modificato == 'function' && comanda.permesso === 0) {
                                                    disegna_ultimo_tavolo_modificato(testo_query);
                                                }
                                            } else if (msg.tipo === "cancella_tutti_tavoli") {
                                                disegna_tavoli_salvati();
                                            } else if (msg.tipo === "refresh")
                                            {
                                                refresh();
                                            } else if (msg.tipo === "aggiornamento_categorie" || msg.tipo === "eliminazione_categorie")
                                            {
                                                elenco_categorie("list", "1", "0", "#tab_sx");
                                            } else if (msg.tipo === "aggiornamento_posizione_prodotti" || msg.tipo === "aggiornamento_prodotti" || msg.tipo === "eliminazione_prodotti") {
                                                crea_griglia_prodotti(comanda.dispositivo, function () {
                                                    if (comanda.mobile !== true) {
                                                        /* se non usassi questa condizione, se sono su lente + sul telefono mi scazza tutto il layout */
                                                        elenco_prodotti();
                                                    }
                                                    //$("#tab_sx a").removeClass("cat_accesa");
                                                    //$('#tab_sx a.cat_' + comanda.categoria_predefinita).addClass("cat_accesa");

                                                });
                                            }

                                        });
                                    }
                                } else if (testo_query !== undefined)
                                {
                                    if (comanda.loading_tab_comanda === true) {
                                        comanda.socket_sospesi.push(testo_query);
                                        console.log("SOCKET SOSPESI COMANDA", comanda.socket_sospesi);
                                    } else if (comanda.loading_tab_tavoli === true) {
                                        console.log("SOCKET SOSPESI TAVOLI", comanda.socket_sospesi_tavoli);
                                        comanda.socket_sospesi_tavoli.push(testo_query);

                                    }
                                }
                            }
                        }

                    };

                    websocket[1].onerror = function (ev) {

                        console.log("SOCKET ONERROR", ev);

                        if (comanda.errore_socket_2 === false) {

                            $('.spia.rtengine2').css('background-color', 'red');


                            //QUESTO SIGNIFICA CHE TUTTI I SOCKET SONO DISCONNESSI
                            if (comanda.socket_starting !== true && ($('.spia.rtengine1').css('background-color') !== "rgb(0, 128, 0)" && $('.spia.rtengine2').css('background-color') !== "rgb(0, 128, 0)" && $('.spia.rtengine2').css('background-color') !== "rgb(0, 128, 0)"))
                            {
                                //alert("ERRORE DI RETE (1-2)!\n\n\n\nHAI PERSO IL COLLEGAMENTO A TUTTI GLI RTEngine!\n\nRIPRISTINA SUBITO IL COLLEGAMENTO.");

                            } else
                            {
                                //alert("RTEngine2 SPENTO");

                            }

                        }

                        comanda.errore_socket_2 = true;

                    };

                    websocket[1].onclose = function (ev) {

                        console.log("SOCKET ONCLOSE", ev);

                        $('.spia.rtengine2').css('background-color', 'red');

                        //QUESTO SIGNIFICA CHE TUTTI I SOCKET SONO DISCONNESSI
                        /*if (comanda.socket_starting !== true && ($('.spia.rtengine1').css('background-color') !== "rgb(0, 128, 0)" && $('.spia.rtengine2').css('background-color') !== "rgb(0, 128, 0)" && $('.spia.rtengine2').css('background-color') !== "rgb(0, 128, 0)"))
                         {
                         alert("ERRORE DI RETE (1-3)!\n\n\n\nHAI PERSO IL COLLEGAMENTO A TUTTI GLI RTEngine!\n\nRIPRISTINA SUBITO IL COLLEGAMENTO.");
                         
                         } else {
                         
                         alert("RTEngine2 SPENTO");
                         
                         }*/

                        comanda.errore_socket_2 = true;

                    };

                } else if (Date.now() - websocket[1].ora_disattivazione > 2000)
                {
                    $('.spia.rtengine2').css('background-color', 'green');

                }
            }

        }, 2500);

        var intervallosocket2 = setInterval(function () {

            if (addr[2] !== undefined && typeof (addr[2]) === 'string') {

                if (websocket[2] !== undefined) {
                    //console.log("SOCKET TEMPI", websocket[2] === undefined, "OR", Math.floor(Date.now() / 1000) - parseInt(websocket[2].ultimavolta) > 3, "OR", isNaN(websocket[2].ultimavolta), "VAL", websocket[2]);
                }

                if (websocket[2] === undefined || Math.floor(Date.now() / 1000) - parseInt(websocket[2].ultimavolta) > 10 || isNaN(websocket[2].ultimavolta)) {

                    if (websocket[2] !== undefined && websocket[2].ultimavolta !== undefined) {
                        console.log("CONDIZIONI SOCKET", websocket[2] === undefined, "OR", Math.floor(Date.now() / 1000) - parseInt(websocket[2].ultimavolta) > 10, "OR", isNaN(websocket[2].ultimavolta));
                    }

                    if (websocket[2] !== undefined) {
                        console.log("SOCKET TEMPI", websocket[2] !== undefined, "AND", websocket[2].readyState < 2, "VAL", websocket[2]);
                    }
                    if (websocket[2] !== undefined && websocket[2].readyState === 1)
                    {
                        $('.spia.rtengine3').css('background-color', 'red');

                        console.log("SOCKET TEMPI", "CLOSE", websocket[2]);
                        websocket[2].close();
                    }

                    if (websocket[2] === undefined || websocket[2].readyState > 1) {

                        console.log("CREAZIONE SOCKET");
                        websocket[2] = new WebSocket(addr[2]);

                        websocket[2].RAMsize = 0;

                        websocket[2].indici = 0;

                        websocket[2].ora_disattivazione = 0;

                    }

                    websocket[2].onopen = function (ev) {

                        //console.log("TENTATA RICHIESTA AGGIORNAMENTO", websocket[2].readyState, websocket_2.orario_aggiornamenti !== undefined);
                        if (websocket[2] !== undefined && websocket[2].readyState === 1 && websocket_2.orario_aggiornamenti !== undefined) {

                            //console.log("RICHIESTA AGGIORNAMENTO", websocket[2].readyState, websocket_2.orario_aggiornamenti !== undefined);

                            websocket[2].send(JSON.stringify({tipo: "richiesta_aggiornamento", operatore: comanda.operatore, query: websocket_2.orario_aggiornamenti.toString(), terminale: comanda.terminale, ip: comanda.ip_address}));

                        }

                        //alert("RTEngine3 ACCESO");

                        $('.spia.rtengine3').css('background-color', 'green');

                        comanda.errore_socket_3 = false;

                        if (typeof (CBC) === "function") {
                            CBC(true);
                        } else {
                            bootbox.alert("Errore tecnico nella Callback del Socket. Probabilmente hai la cassa impostata come cassa_singola e hai socket impostati nella tabella socket_listener. Contatta l'assistenza per risolvere il problema immediatamente.")
                        }

                    };

                    websocket[2].onmessage = function (ev) {
                        websocket[2].ultimavolta = Math.floor(Date.now() / 1000);

                        var msg = JSON.parse(ev.data);

                        comanda.socket_ricevuto_mod = msg;


                        if (comanda.socket_ricevuto_mod.orario !== undefined)
                        {
                            websocket_2.orario_aggiornamenti = comanda.socket_ricevuto_mod.orario;

                            delete comanda.socket_ricevuto_mod.orario;
                        }

                        if (msg !== undefined && msg !== null && msg.operatore !== undefined && comanda.terminale === msg.terminale && comanda.ip_address === msg.ip) {
                            comanda.socket_ricevuto = msg;

                            //SERVE A CONFRONTARE IL SOCKET RICEVUTO SENZA ORARIO DEL SERVER PERCHE' ALTRIMENTI SAREBBE PER FORZA DIVERSO


                            if (JSON.stringify(comanda.socket_ricevuto_mod) === comanda.socket_inviato) {

                                comanda.socket_ciclizzati.shift();

                                clearInterval(test_standby_socket);

                                IntervalloSocketSend.resume();

                            }
                        }

                        if (msg !== null && (comanda.operatore !== msg.operatore || comanda.terminale !== msg.terminale || comanda.ip_address !== msg.ip)) {

                            var testo_query;

                            switch (msg.tipo) {
                                default:
                                    testo_query = msg.query;
                                    break;

                            }

                            if (msg !== undefined && msg !== null && msg.query !== undefined && msg.query !== null && msg.tipo === 'notification' && msg.query.indexOf('inizio download comanda') !== -1 && msg.query.indexOf(comanda.ip_address) === -1) {

                                var r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;

                                var t = msg.query.match(r);

                                comanda.array_dbcentrale_mancanti.forEach(function (messaggio) {

                                    comanda.sock.send({tipo: "coda", operatore: comanda.operatore, query: messaggio, terminale: comanda.terminale, ip: comanda.ip_address, ip_dest: t[0]});

                                });

                            } else if (msg.tipo === "stampa_comanda") {

                                if (comanda.stampa_ordini_qui === true) {
                                    comanda.tavolo = msg.query;
                                    stampa_comanda(undefined, undefined, undefined, undefined, undefined, true);
                                    console.log("STOP");
                                }

                            } else if (msg.tipo === "stampa_comanda_fastorder") {


                                comanda.tavolo = msg.query;
                                stampa_comanda(undefined, undefined, undefined, undefined, undefined, true);


                            } else if (msg.tipo === "risposta_dati_db_locali") {

                                $('#popup_test_allineamento table').append('<tr><td>' + msg.ip + '</td><td>' + msg.terminale + '</td><td>' + msg.operatore + '</td><td>' + msg.query.split(';')[0] + '</td><td>' + parseFloat(msg.query.split(';')[1]).toFixed(2) + '</td><td>' + msg.query.split(';')[2] + '</td></tr>');

                            } else if (msg.tipo === "richiesta_riallineamento_dati") {
                                risposta_riallineamento_dati();
                            } else if (comanda.verifica_periodica_tavoli_abilitata === true && msg.tipo === "richiesta_status_tavoli") {
                                conferma_propri_tavoli_bloccati();
                            } else if (comanda.verifica_periodica_tavoli_abilitata === true && msg.tipo === "risposta_status_tavoli") {
                                aggiungi_tavolo_bloccato_davvero(msg);
                            } else if (msg.tipo === "richiesta_status_terminali") {
                                risposta_status_terminali();
                            } else if (msg.tipo === "risposta_status_terminali") {
                                aggiungi_terminale_acceso_davvero(msg);
                            } else {

                                if (testo_query !== undefined && comanda.loading_tab_tavoli !== true && comanda.loading_tab_comanda !== true) {
                                    if (testo_query.indexOf("record_teste") !== -1) {
                                        //Query Asincrona per le teste
                                        comanda.sincro.query(testo_query, function () {});
                                    } else
                                    {
                                        comanda.sincro.query_sync(testo_query, function () {

                                            if (msg.tipo === "aggiornamento_settaggi_principali") {
                                                var query_impostazioni = "SELECT valore_coperto,coperti_obbligatorio,valore_servizio FROM impostazioni_fiscali;";
                                                comanda.sincro.query_sync(query_impostazioni, function (risultato_impostazioni) {

                                                    comanda.valore_coperto = risultato_impostazioni[0].valore_coperto;
                                                    comanda.coperti_obbligatorio = risultato_impostazioni[0].coperti_obbligatorio;
                                                    comanda.percentuale_servizio = risultato_impostazioni[0].valore_servizio;

                                                    alert("CHIEDI ALL'AMMINISTRATORE SE E' STATA CAMBIATA LINGUA DI STAMPA E/O VISUALIZZAZIONE, ED EVENTUALMENTE RIAVVIA IL DISPOSITIVO.");
                                                });
                                            } else if (msg.tipo === "aggiornamento_righe_forno") {
                                                righe_forno(true);
                                            } else if (msg.tipo === "aggiornamento_stampanti") {
                                                corrispondenze_stampanti(comanda.lingua_stampa);
                                            } else if (msg.tipo === "aggiornamento_profilo_aziendale") {
                                                importa_profilo_aziendale_ram(function () {});
                                            } else if (msg.tipo === "richiesta_dati_db_locali") {
                                                risposta_test_allineamento();
                                            } else if (msg.tipo === "tavolo_occupato" || msg.tipo === "comanda_stampata" || msg.tipo === "comanda_lanciata" || msg.tipo === "chiudi_scontrino")
                                            {
                                                if (typeof disegna_ultimo_tavolo_modificato == 'function' && comanda.permesso === 0) {
                                                    disegna_ultimo_tavolo_modificato(testo_query);
                                                }
                                            } else if (msg.tipo === "cancella_tutti_tavoli") {
                                                disegna_tavoli_salvati();
                                            } else if (msg.tipo === "refresh")
                                            {
                                                refresh();
                                            } else if (msg.tipo === "aggiornamento_categorie" || msg.tipo === "eliminazione_categorie")
                                            {
                                                elenco_categorie("list", "1", "0", "#tab_sx");
                                            } else if (msg.tipo === "aggiornamento_posizione_prodotti" || msg.tipo === "aggiornamento_prodotti" || msg.tipo === "eliminazione_prodotti") {
                                                crea_griglia_prodotti(comanda.dispositivo, function () {
                                                    if (comanda.mobile !== true) {
                                                        /* se non usassi questa condizione, se sono su lente + sul telefono mi scazza tutto il layout */
                                                        elenco_prodotti();
                                                    }
                                                    //$("#tab_sx a").removeClass("cat_accesa");
                                                    //$('#tab_sx a.cat_' + comanda.categoria_predefinita).addClass("cat_accesa");

                                                });
                                            }

                                        });
                                    }
                                } else if (testo_query !== undefined)
                                {
                                    if (comanda.loading_tab_comanda === true) {
                                        comanda.socket_sospesi.push(testo_query);
                                        console.log("SOCKET SOSPESI COMANDA", comanda.socket_sospesi);
                                    } else if (comanda.loading_tab_tavoli === true) {
                                        console.log("SOCKET SOSPESI TAVOLI", comanda.socket_sospesi_tavoli);
                                        comanda.socket_sospesi_tavoli.push(testo_query);

                                    }
                                }
                            }
                        }

                    };

                    websocket[2].onerror = function (ev) {

                        console.log("SOCKET ONERROR", ev);

                        if (comanda.errore_socket_3 === false) {

                            $('.spia.rtengine3').css('background-color', 'red');


                            //QUESTO SIGNIFICA CHE TUTTI I SOCKET SONO DISCONNESSI
                            if (comanda.socket_starting !== true && ($('.spia.rtengine1').css('background-color') !== "rgb(0, 128, 0)" && $('.spia.rtengine2').css('background-color') !== "rgb(0, 128, 0)" && $('.spia.rtengine3').css('background-color') !== "rgb(0, 128, 0)"))
                            {
                                //alert("ERRORE DI RETE (1-2)!\n\n\n\nHAI PERSO IL COLLEGAMENTO A TUTTI GLI RTEngine!\n\nRIPRISTINA SUBITO IL COLLEGAMENTO.");

                            } else
                            {
                                //alert("RTEngine3 SPENTO");

                            }

                        }

                        comanda.errore_socket_3 = true;

                    };

                    websocket[2].onclose = function (ev) {

                        console.log("SOCKET ONCLOSE", ev);

                        $('.spia.rtengine3').css('background-color', 'red');

                        comanda.errore_socket_3 = true;

                    };

                } else if (Date.now() - websocket[2].ora_disattivazione > 2000)
                {
                    $('.spia.rtengine3').css('background-color', 'green');

                }
            }

        }, 2500);

//-----END OF SOCKETS-----//

        function roughSizeOfObject(object) {

            var objectList = [];
            var stack = [object];
            var bytes = 0;

            while (stack.length) {
                var value = stack.pop();

                if (typeof value === 'boolean') {
                    bytes += 4;
                } else if (typeof value === 'string') {
                    bytes += value.length * 2;
                } else if (typeof value === 'number') {
                    bytes += 8;
                } else if
                        (
                                typeof value === 'object'
                                && objectList.indexOf(value) === -1
                                )
                {
                    objectList.push(value);

                    for (var i in value) {
                        stack.push(value[ i ]);
                    }
                }
            }
            return bytes;
        }

        var d = new Date();
        var n = d.getTime();
        var ms = n.toString().length;
        try {
            var ts = comanda.terminale.length;
        } catch (e) {
            var ts = 0;
        }

        IntervalloSocketSend = {
            start: function () {

                this.interval = setInterval(function () {

                    comanda.sock.waitForSocketConnection(websocket, function (numero_socket) {

                        if ((comanda.socket_ciclizzati !== undefined && comanda.socket_ciclizzati[0] !== undefined && typeof (comanda.socket_ciclizzati[0].query) === 'string' && comanda.socket_ciclizzati[0].query !== undefined && comanda.socket_ciclizzati[0].query.search("CREATE") === -1 && comanda.socket_ciclizzati[0].query.search("SELECT") === -1 && comanda.socket_ciclizzati[0].query.search("select") === -1) || (comanda.socket_ciclizzati !== undefined && comanda.socket_ciclizzati[0] !== undefined && comanda.socket_ciclizzati[0].tipo !== undefined && (comanda.socket_ciclizzati[0].tipo === "richiesta_dati_db_locali" || comanda.socket_ciclizzati[0].tipo === "risposta_dati_db_locali" || comanda.socket_ciclizzati[0].tipo === "aggiornamento_righe_forno"))) {

                            IntervalloSocketSend.pause();

                            if (websocket[numero_socket] !== undefined) {

                                websocket[numero_socket].indici += 1;

                                websocket[numero_socket].RAMsize += roughSizeOfObject(JSON.stringify(comanda.socket_ciclizzati[0])) + ms + ts;

                                websocket[numero_socket].send(JSON.stringify(comanda.socket_ciclizzati[0]));

                                console.log("QUERY LOCALE SOCKET INVIATA", JSON.stringify(comanda.socket_ciclizzati[0]));

                                comanda.socket_inviato = JSON.stringify(comanda.socket_ciclizzati[0]);

                                comanda.ora_ultimo_socket_inviato = Date.now();

                                comanda.ultimonumerosocketinviato = numero_socket;

                                comanda.socksend_standby = false;

                            }

                            test_standby_socket = setInterval(function () {

                                if (Date.now() - comanda.ora_ultimo_socket_inviato >= 1000) {

                                    if (comanda.socksend_standby !== true && websocket[comanda.ultimonumerosocketinviato] !== undefined) {

                                        websocket[comanda.ultimonumerosocketinviato].ora_disattivazione = Date.now();

                                        $('.spia.rtengine' + (comanda.ultimonumerosocketinviato + 1)).css('background-color', 'rgb(255, 0, 0)');

                                        comanda.socksend_standby = true;

                                        //QUESTO SIGNIFICA CHE TUTTI I SOCKET SONO DISCONNESSI
                                        if (comanda.socket_starting !== true && ($('.spia.rtengine1').css('background-color') !== "rgb(0, 128, 0)" && $('.spia.rtengine2').css('background-color') !== "rgb(0, 128, 0)" && $('.spia.rtengine3').css('background-color') !== "rgb(0, 128, 0)"))
                                        {
                                            //CAPIRE QUESTO ERRORE

                                            //alert("ERRORE DI RETE (4)!\n\n\n\nHAI PERSO IL COLLEGAMENTO A TUTTI GLI RTEngine!\n\nRIPRISTINA SUBITO IL COLLEGAMENTO." + (Date.now() - comanda.ora_ultimo_socket_inviato));
                                        }

                                    } else

                                    //NON SERVE PIU PERCHé E' IL CLIENT CHE RICHIEDERA I DATI VECCHI
                                    //quando rivede un socket acceso ritenta l'invio dopo 20 secondi così si è sicuri che anche gli altri dispositivi abbiano ripreso i socket
                                    if ($('.spia.rtengine1').css('background-color') === "rgb(0, 128, 0)" || $('.spia.rtengine2').css('background-color') === "rgb(0, 128, 0)" || $('.spia.rtengine3').css('background-color') === "rgb(0, 128, 0)") {
                                        //va cancellato prima del timeout altrimenti nei 20 secondi si ripete l'ntervallo 6 volte
                                        //e quindi fa 6 cancellazioni e sei resume a casaccio

                                        clearInterval(test_standby_socket);

                                        IntervalloSocketSend.resume();

                                    }

                                }

                            }, 1000);


                        } else if (comanda.socket_ciclizzati !== undefined /*&& comanda.socket_ciclizzati[0] !== undefined*/) {

                            comanda.socket_ciclizzati.shift();

                            IntervalloSocketSend.resume();

                        }
                    });

                }, 50);
            },
            pause: function () {
                clearInterval(this.interval);
                delete this.interval;
            },
            resume: function () {
                if (!this.interval)
                    this.start();
            }
        };

        IntervalloSocketSend.start();


    }
}




var Funzioni_Socket = function () {

    this.waitForSocketConnection = function (socket, callback) {

        if (websocket !== undefined && websocket[0] !== undefined && websocket[0].readyState === 1 && $('.spia.rtengine1').css('background-color') === "rgb(0, 128, 0)") {
            if (callback !== null) {
                callback(0);
            }
            return;
        } else if (websocket !== undefined && websocket[1] !== undefined && websocket[1].readyState === 1 && $('.spia.rtengine2').css('background-color') === "rgb(0, 128, 0)") {
            if (callback !== null) {
                callback(1);
            }
            return;
        } else if (websocket !== undefined && websocket[2] !== undefined && websocket[2].readyState === 1 && $('.spia.rtengine3').css('background-color') === "rgb(0, 128, 0)") {
            if (callback !== null) {
                callback(2);
            }
            return;
        } else
        {
            if (callback !== null) {
                callback(false);
            }
            return;
        }
    };

    this.send = function (fields_json_array, query_cassa_abilitato) {



        //DEBUG SOCKET undefined indietro BAR
        console.log("INVIA SOCKET", fields_json_array);
        /*console.trace();*/

        var mymessage = fields_json_array;

        if (comanda.query_su_socket === false && query_cassa_abilitato !== false) {

            if (mymessage.tipo !== "aggiornamento_righe_forno" && mymessage.tipo !== "richiesta_status_tavoli" && mymessage.tipo !== "richiesta_status_terminali" && mymessage.tipo !== "risposta_status_tavoli" && mymessage.tipo !== "risposta_status_terminali" && mymessage.tipo !== "richiesta_aggiornamento" && mymessage.tipo !== "cancellazione_comande_vecchie" && mymessage.tipo !== "richiesta_dati_db_locali" && mymessage.tipo !== "risposta_dati_db_locali" && mymessage.tipo !== "progressivo_fiscale" && comanda.loading === false && mymessage.query !== undefined && mymessage.query.search("create") === -1 && mymessage.query.search("CREATE") === -1 && mymessage.query.search("spooler_stampa") === -1 && ((mymessage.query.search("SELECT") === -1 && mymessage.query.search("select") === -1) || (mymessage.query.search("delete") !== -1 || mymessage.query.search("DELETE") !== -1))) {
                if ((comanda.compatibile_xml !== true) || (comanda.compatibile_xml === true && mymessage.query !== undefined && (mymessage.query.toLowerCase().search("insert into comanda") === -1 && mymessage.query.toLowerCase().search("update comanda") === -1) && mymessage.query.toLowerCase().search("delete from comanda") === -1))
                {
                    console.trace("CENTRALE MANCANTI PUSH");
                    console.log("ARRAY_DBCENTRALE_MANCANTI.PUSH",mymessage.query);
                    comanda.array_dbcentrale_mancanti.push(mymessage.query);
                    if (comanda.compatibile_xml !== true) {
                    log_query_db_centrale(mymessage.query);}
                    console.log("DB CENTRALE", mymessage.query);
                }
            }
        }

        if (comanda.cassa_singola !== true) {

            var mymessage = fields_json_array;

            comanda.socket_ciclizzati.push(comprimi_oggetto(mymessage));

        }
    };
};

function comprimi_oggetto(oggetto_mymessage) {
    try {
        var nuovo_oggetto = oggetto_mymessage;
        nuovo_oggetto.query = nuovo_oggetto.query.replace(/\n/gi, '').replace(/\s\s+/g, ' ');
        return nuovo_oggetto;
    } catch (e) {
        console.log(e, oggetto_mymessage);
    }
}


function rigenera_colore_tavoli() {

    var conferma = confirm("Sei sicuro di voler rigenerare il colore dei tavoli?\nQuesta operazione dev'essere effettuata solo qualche minuto dopo dopo aver riallineato i tavoli.");

    if (conferma === true) {

        //Quando dal socket mi arriva il messaggio rigenera_colore_tavoli
        var ora = comanda.funzionidb.data_attuale().substr(9, 8).trim();

        comanda.sincro.query("update tavoli set colore='grey',occupato='0',ora_ultima_comanda='-',ora_apertura_tavolo='-';", function () {

            comanda.sincro.query("select fiscalizzata_sn,stampata_sn,ntav_comanda,numero_conto from comanda where stato_record='ATTIVO' group by ntav_comanda,fiscalizzata_sn,stampata_sn;", function (result) {

                var numero_tavoli_da_sincronizzare = result.length;

                var promessa_rigenerazione_tavoli = new Promise(function (resolve, reject) {


                    //Se length===5

                    var iter = 0;

                    result.forEach(function (tav) {

                        switch (tav.fiscalizzata_sn) {

                            //CONTO
                            case "n":
                            case "N":
                                comanda.sincro.query("update tavoli set colore='4',ora_ultima_comanda='RIALLINEATO',ora_apertura_tavolo='-' where numero='" + tav.ntav_comanda + "' ;", function () {

                                    //In questo caso iter va prima perchè è una funziona asincrona e si va meglio così
                                    iter++;

                                    if (iter === numero_tavoli_da_sincronizzare)
                                    {
                                        resolve(true);
                                    }

                                });
                                break;

                                //COMANDA
                            case "c":
                            case "C":
                                comanda.sincro.query("update tavoli set colore='0',ora_ultima_comanda='RIALLINEATO',ora_apertura_tavolo='" + ora + "' where numero='" + tav.ntav_comanda + "' ;", function () {

                                    //In questo caso iter va prima perchè è una funziona asincrona e si va meglio così
                                    iter++;

                                    if (iter === numero_tavoli_da_sincronizzare)
                                    {
                                        resolve(true);
                                    }

                                });
                                break;

                                //BHO SALMONE
                            case "null":
                            case "NULL":
                            case null:
                            case "":
                            default:
                                comanda.sincro.query("update tavoli set colore='2',ora_ultima_comanda='RIALLINEATO',ora_apertura_tavolo='-' where numero='" + tav.ntav_comanda + "' ;", function () {

                                    //In questo caso iter va prima perchè è una funziona asincrona e si va meglio così
                                    iter++;

                                    if (iter === numero_tavoli_da_sincronizzare)
                                    {
                                        resolve(true);
                                    }

                                });
                                break;
                        }
                    });
                });

                promessa_rigenerazione_tavoli.then(function () {

                    disegna_tavoli_salvati();

                });

            });
        });
    }
}