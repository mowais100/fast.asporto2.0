/* global epson, parseFloat, comanda, id_comanda, bootbox */

//NB E' quasi giusto, ma in rarissimi casi la stampante raggiunge 30 secondi di timeout! 
//Correggere dopo riunione epson

String.prototype.insert = function(index, string) {
    if (index > 0)
        return this.substring(0, index) + string + this.substring(index, this.length);
    return string + this;
};
//SPOOLER DI STAMPA
function leggi_matricola(cb) {
    var cb_counter = 0;
    var xml = '<printerCommand><directIO  command="3217" data="01" /></printerCommand>';
    var epos = new epson.fiscalPrint();
    epos.send("http://" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=local_printer&timeout=2000", xml, 2000);
    epos.onreceive = function(result, tag_names_array, add_info) {
        if (cb_counter === 0) {
            var matricola = add_info.responseData.substr(2, 6);
            var mod = add_info.responseData.substr(8, 2);
            var prod = add_info.responseData.substr(10, 2);
            if (typeof(cb) === "function") {
                console.log(add_info.responseData, mod + '' + prod + '' + matricola);
                cb(mod + '' + prod + '' + matricola);
            } else {
                console.log(mod + '' + prod + '' + matricola);
                console.log("CALLBACK NON DISPONIBILE");
            }

        }
        cb_counter++;
    };
    epos.onerror = function(err) {
        if (cb_counter === 0) {
            if (typeof(cb) === "function") {
                console.log("MATRICOLA NON DISPONIBILE");
                cb("MATRICOLA NON DISPONIBILE");
            } else {
                console.log("MATRICOLA NON DISPONIBILE");
                console.log("CALLBACK NON DISPONIBILE");
            }

        }
        cb_counter++;
    };
}

function stampa_integrale_memoria_fiscale() {
    var xml = '<printerCommand><directIO  command="3015" data="01" /></printerCommand>';
    var epos = new epson.fiscalPrint();
    epos.send("http://" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=local_printer&timeout=2000", xml, 2000);
    epos.onreceive = function(result, tag_names_array, add_info) {}
    epos.onerror = function(err) {}
}

//QUESTA VARIABILE DEVE STAR FUORI SENNO' SI ACCUMULANO SPOOLERS ALLA DOG'S DICK! :)
var spool_tempo_reinvio;
var riattiva_spooler;
function aggiungi_spool_stampa(indirizzo_completo_stampante, stream_xml, tipo, nome_operazione, dati_testa, indirizzo_alternativo, avviso_stampa_elaborazione)
{
    

    comanda.provenienza_conto = false;
    if (indirizzo_completo_stampante !== "http:///cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000") {
        var numero_tavolo = comanda.tavolo;
        if (indirizzo_alternativo === undefined) {
            indirizzo_alternativo = "";
        }

        console.log("SPOOLER RICEZIONE", indirizzo_completo_stampante, stream_xml, tipo, nome_operazione, dati_testa, indirizzo_alternativo);
        //let orario_insert = id_comanda(false).substr(4);
        //modificato il 30/04/2021 alle 18.23 (venerdi) perche senno alla stampa di comanda creava un nuovo id e non va bene
        let orario_insert = new Date().format("yyyymmddHHMMss");

        console.log("INSERIMENTO SPOOLER", orario_insert, dati_testa);

        testo_query = "UPDATE spooler_stampa SET tentativo='0' WHERE tentativo='ATTESA';";
        comanda.sincro.query(testo_query, function() {

            testo_query = "INSERT INTO spooler_stampa ( tentativo, orario, indirizzo , xml , tipo, dati_testa, id_univoco,indirizzo_alternativo ) VALUES ( '0','" + orario_insert + "',' " + indirizzo_completo_stampante + " ' , ' " + stream_xml + " ', ' " + tipo + " ','" + JSON.stringify(dati_testa) + "' ,'" + CryptoJS.MD5(JSON.stringify(dati_testa) + orario_insert + stream_xml).toString() + "','" + indirizzo_alternativo + "') ; ";
            comanda.sincro.query(testo_query, function() {

                /*testo_query = "INSERT INTO spooler_stampa_LOG ( tentativo, orario, indirizzo , xml , tipo, dati_testa, id_univoco,indirizzo_alternativo ) VALUES ( '0','" + orario_insert + "',' " + indirizzo_completo_stampante + " ' , ' " + stream_xml + " ', ' " + tipo + " ','" + JSON.stringify(dati_testa) + "' ,'" + CryptoJS.MD5(JSON.stringify(dati_testa) + orario_insert+stream_xml).toString() + "','" + indirizzo_alternativo + "') ; ";
                 comanda.sincro.query(testo_query, function () {
                 
                 });*/
            });
        });
        //if (comanda.pizzeria_asporto !== true) {
        //MODIFICATO IL 2 SETTEMBRE 2020 perchè mi dava l'avviso se stampavo il qr_cliente dall'altro schermo
        if (avviso_stampa_elaborazione !== false) {
            righe_forno();
            if (comanda.pizzeria_asporto !== true) {
                //Stampa in fase di elaborazione nei conti
                $(comanda.intestazione_conto).html("");
                $(comanda.conto).html("<tr><td>Stampa in fase di elaborazione...</td></tr>");
                calcola_totale('spooler');
                righe_forno('conto');
            }


        }
      



        //CALCOLA IL TOTALE PERCHE ALTIMENTI IL PREZZO RESTEREBBE QUELLO CHE E' NELL'ATTESA
        //VA AI TAVOLI DOPO LO SCONTRINO
        /*if (comanda.conto !== '#prodotti_conto_separato_2_grande' && comanda.conto !== '#prodotti_conto_separato_grande' && comanda.tavolo !== "BANCO" && comanda.tavolo !== "BAR" && comanda.tavolo.left(7) !== "ASPORTO" && comanda.tavolo !== "TAKE AWAY") {
         btn_tavoli();
         console.log("BTN TAVOLI");
         }*/

        var articoli_minimi = 0;
        if (parseFloat(comanda.percentuale_servizio) > 0) {
            articoli_minimi = 1;
        }

        //TUTTI E DUE DEVONO ESSERE MAGGIORI DEL MINIMO PER ESSERE UN CONTO SEPARATO
        if (($('#prodotti_conto_separato_2_grande tr').length > articoli_minimi && $('#prodotti_conto_separato_grande tr').length) > articoli_minimi) {

            console.log("CONTI SEPARATI", $('#prodotti_conto_separato_2_grande tr').length, $('#prodotti_conto_separato_grande tr').length);
            var testo_query = "update tavoli set colore='3',ora_apertura_tavolo='-',ora_ultima_comanda='-' where numero='" + numero_tavolo + "' ";
            disegna_ultimo_tavolo_modificato(testo_query);
            comanda.sock.send({ tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
            comanda.sincro.query(testo_query, function() {

                if (comanda.compatibile_xml !== true && comanda.conto === "#conto" && nome_operazione === "stampa_fiscale") {
                    var testo_query = "update tavoli set occupato='0',colore='0',ora_apertura_tavolo='-',ora_ultima_comanda='-' where numero='" + numero_tavolo + "' ";
                    comanda.sincro.query(testo_query, function() {
                        btn_tavoli("scontrino");
                    });
                }

            });

        } else if (nome_operazione === "stampa_fiscale")
        //HO TOLTO (comanda.tavolo !== "BANCO" && comanda.tavolo !== "BAR" && comanda.tavolo.left(7) !== "ASPORTO" && comanda.tavolo !== "TAKE AWAY")
        //PERCHE' ANCHE SU BAR DEVE USCIRE AI TAVOLI
        {

            console.log("SCONTRINO INTERO");
            if (comanda.conto !== '#prodotti_conto_separato_2_grande' && comanda.conto !== '#prodotti_conto_separato_grande') {
                $('#conto_separato_grande').modal('hide');

                if (comanda.pizzeria_asporto !== true && comanda.tavolo.left(7) === "ASPORTO") {

                    var testo_query = "select 0;";

                            }
                        }
            } 

            disegna_ultimo_tavolo_modificato(testo_query);
            comanda.sock.send({ tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
            comanda.sincro.query(testo_query, function() {



            });
        }
        //$("#conto").hide();

    }





function aggiungi_spool_attesa(indirizzo_completo_stampante, stream_xml, tipo, nome_operazione, dati_testa, indirizzo_alternativo) {

    console.log("SPOOLER RICEZIONE ATTESA", indirizzo_completo_stampante, stream_xml, tipo, nome_operazione, dati_testa, indirizzo_alternativo);
    var orario_insert = new Date().format("yyyymmddHHMMss");
    testo_query = "INSERT INTO spooler_stampa ( tentativo, orario, indirizzo , xml , tipo, dati_testa, id_univoco,indirizzo_alternativo ) VALUES ( 'ATTESA','" + orario_insert + "',' " + indirizzo_completo_stampante + " ' , ' " + stream_xml + " ', ' " + tipo + " ','" + JSON.stringify(dati_testa) + "' ,'" + CryptoJS.MD5(JSON.stringify(dati_testa) + orario_insert + stream_xml).toString() + "','" + indirizzo_alternativo + "') ; ";
    alasql(testo_query);
}

function start_spooler() {

    //lang_8 sarebbe la pagina delle chiusure fiscali tedesche
    if (comanda.mobile === true || comanda.dispositivo === 'TAVOLO SELEZIONATO' || comanda.dispositivo === 'MAPPA TAVOLI' || comanda.dispositivo === 'lang_8' || comanda.dispositivo === 'lang_132') {

        //console.trace();
        //SOLO SE LO SPOOLER AL MOMENTO E' CHIUSO PUO' PARTIRE
        //SI CHIUDE LO SPOOLER SOLO DOPO L'ONCHANGE



        //SE ORDINO AL CONTRARIO MI PRENDE GLI ULTIMI SCONTRINI FATTI PER PRIMI, COSI' NON SI INCEPPA CON LE EVENTUALI STAMPANTI SPENTE DELLE COMANDE
        //var testo_query = "select * from spooler_stampa order by orario asc limit 1;";

        //TEST MULTI STAMPA
        var testo_query = "select * from spooler_stampa where tentativo!='ATTESA' order by orario asc;";
        comanda.sincro.query(testo_query, function(result) {

            var stampante = new Object();
            result.forEach(function(el, key) {
                if (stampante[el.indirizzo] === undefined) {
                    stampante[el.indirizzo] = new Object();
                    stampante[el.indirizzo] = el;
                }
            });
            //console.log("QUERY SPOOLER", testo_query, result, Date.now());

            //console.log("RESULT", result);
            if (result.length > 0) {

                $('.spia.status_spooler').css('background-color', 'red');
                comanda.ora_ultimo_spooler = Math.floor(Date.now() / 1000);
                console.log("SPOOLER - PRECICLO...", result);
                console.trace();
                for (var indirizzo in stampante) {
                    var el = stampante[indirizzo];
                    if (comanda.id_stampa.indexOf(el.id_univoco) === -1) {
                        comanda.id_stampa.push(el.id_univoco);
                        console.log("SPOOLER APERTO INVIO DATI CASSA ID ", el.id_univoco);
                        invia_dati_cassa(el.orario, el.indirizzo, el.xml, null, result.length, el.tipo, el.dati_testa, el.tentativo, el.id_univoco, el.indirizzo_alternativo);
                    }
                }
            }
        });
    }
}

/*Tipo è facoltativo*/
function invia_dati_cassa(orario, indirizzo, xml, numero_record, totale_record, tipo, dati_testa, tent, id_univoco, indirizzo_alternativo) {
    //console.log("DATI TESTA VERIFICA", dati_testa, dati_testa !== 'undefined', dati_testa !== null, dati_testa.length, ((dati_testa !== 'undefined' && dati_testa !== undefined && dati_testa !== null) && (tipo.toLowerCase().trim() === "stampante fiscale" || tipo.toLowerCase().trim() === "scontrino" || tipo.toLowerCase().trim() === "scontrino fiscale" || tipo.toLowerCase().trim() === "fattura")));



    if (comanda.tentativo_fallito[indirizzo] === undefined) {
        comanda.tentativo_fallito[indirizzo] = new Array();
    }
    var tentativo = 0;
    if (tent !== undefined && tent > 0) {
        tentativo = tent;
    }


    var url = indirizzo;
    //Il ping serve a testare la rete, ma è molto lento
    pong(indirizzo, function(risultato_ping) {
        test_pagina_cgi(indirizzo, function(risposta_cgi) {

            if (risultato_ping === true && risposta_cgi === true) {

                //if tentativo != 0 devi dare conferma a mano

                if (tentativo > 0 && !$("#popup_errore_fine_carta").hasClass('in') && !$("#popup_errore_coperchio_aperto").hasClass('in') && !$("#popup_errore_stampante_sconosciuto").hasClass('in')) {

                    let testo_spooler = "";
                    let titolo_popup_spooler = "Ho gi&agrave; provato a fare questa stampa.<br>Controlla se &egrave; uscita, altrimenti invia una ri-stampa.";

                    if (indirizzo.indexOf("fpmate.cgi") !== -1) {

                        titolo_popup_spooler = "Attenzione: la stampante FISCALE risultava spenta o fuori rete.<br>Controlla se questi prodotti sono gi&agrave; stati scontrinati, altrimenti invia una ri-stampa.";

                        $(xml).find("printrecitem").each((a, v) => {
                            testo_spooler += $(v).attr("description") + "  " + $(v).attr("quantity") + "  " + $(v).attr("unitprice") + "<br>";
                        });

                        testo_spooler += "Totale: " + $(xml).find("printrectotal").attr("payment") + "<br>";
                    } else {
                        testo_spooler += $(xml).find("text:not(:empty)").append(" ").text().replace(/\n/gi, "<br>");
                    }


                    bootbox.confirm({
                        closeButton: false,
                        message: titolo_popup_spooler + "\n\
                                <br><br>\n\
                                \n\
                                <pre>" + testo_spooler + "</pre>\n\
                                <br>\n\
                                Come vuoi procedere?",

                        buttons: {
                            confirm: {
                                label: 'RI-STAMPA',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'ELIMINA',
                                className: 'btn-danger'
                            }
                        },
                        callback: function(result) {
                            if (result === true) {
                                invio_confermato();
                            } else {
                                //CANCELLA DA SPOOLER
                                var testo_query_cancellazione = "DELETE FROM spooler_stampa WHERE id_univoco='" + id_univoco + "';";
                                alasql(testo_query_cancellazione);
                                $('#errore_stampante_spenta').addClass("hidden");
                            }
                        }
                    });
                } else {
                    invio_confermato();
                }


            } else {

                console.log("AVVISO STAMPANTE COND1", comanda.tentativo_fallito[indirizzo], Date.now() / 1000, Date.now() / 1000 - comanda.tentativo_fallito[indirizzo].timestamp, comanda.tentativo_fallito[indirizzo].buon_fine !== true);
                if (comanda.tentativo_fallito[indirizzo].buon_fine !== true || Date.now() / 1000 - comanda.tentativo_fallito[indirizzo].timestamp >= 15) {

                    /*$('.nome_stampante_errata').html(tipo);
                     $('#popup_errore_stampante_spenta').modal('show');*/

                    //TRIANGOLO STAMPANTE SPENTA
                    $("#errore_stampante_spenta").removeClass("hidden");


                    comanda.tentativo_fallito[indirizzo].timestamp = Date.now() / 1000;
                    console.log("AVVISO STAMPANTE SET", comanda.tentativo_fallito[indirizzo].timestamp);
                }

                var query_spostamento_record = "";
                if (indirizzo_alternativo !== undefined && indirizzo_alternativo !== null && indirizzo_alternativo !== "undefined" && indirizzo_alternativo !== "") {
                    query_spostamento_record = "update spooler_stampa set indirizzo=indirizzo_alternativo,tentativo=cast(tentativo as int)+1,orario='" + new Date().format("yyyymmddHHMMss") + "' where id_univoco='" + id_univoco + "' ;";
                    $('.dirottamento_stampa').html("Dirottamento Stampa in corso...");
                } else {
                    query_spostamento_record = "update spooler_stampa set tentativo=cast(tentativo as int)+1,orario='" + new Date().format("yyyymmddHHMMss") + "' where id_univoco='" + id_univoco + "' ;";
                    $('.dirottamento_stampa').html("");
                }

                console.log(query_spostamento_record);
                comanda.sincro.query(query_spostamento_record, function() {

                    comanda.tentativo_fallito[indirizzo].buon_fine = true;
                    var index = comanda.id_stampa.indexOf(id_univoco);
                    comanda.id_stampa.splice(index, 1);
                });
            }
        });
    });
    //SUBROUTINE
    function invio_confermato() {

        //Apre una richiesta XML
        var xhr = new XMLHttpRequest();
        //Metto anche un listener per la risposta
        var risposta_xml;
        //Url del cgi per stampare

        xhr.open('POST', url, true);
        //Settaggio headers
        xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
        xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
        xhr.setRequestHeader('SOAPAction', '""');
        //DEV'ESSERE UGUALE AL TIMEOUT SI STAMPA ALTRIMENTI SI BLOCCA PRIMA E RISTAMPA
        xhr.timeout = 30000;
        console.log("QUERY SPOOLER GIRO STAMPA");
        console.trace();
        console.log("OP. SPOOLER - SELECT RECORD", orario);
        xhr.send(xml.trim());
        xhr.onreadystatechange = function() {

            //ATTENZIONE. QUI DENTRO I DATI SONO ASINCRONI RISPETTO AL CICLO FOR PRECEDENTE
            if (xhr.readyState === 4 && xhr.status === 200) {

                risposta_xml = xhr.responseXML;
                //console.log(xhr);
                //Prendo i dati per capire dalla risposta se ci sono errori, e quali sono
                var dati_importanti_risposta = risposta_xml.getElementsByTagName("response")[0];
                //Variabile che dice se è andato tutto a buon fine
                var buon_fine = dati_importanti_risposta.attributes.success.nodeValue;
                //Variabile che dice se qual'è il codice d'errore, se la variabile buon_fine è false
                var codice_errore = dati_importanti_risposta.attributes.code.nodeValue;
                console.log("STAMPATA: " + buon_fine + " - CODICE ERRORE: " + codice_errore);
                //Condizione per vedere come continuare il ciclo

                //In ogni caso procede, ma con valori c diversi
                //Se non ha stampato tifà lo stesso
                //Altrimenti va avanti


                if (buon_fine === "true") {

                    $('#errore_stampante_spenta').addClass("hidden");

                    if (comanda.pizzeria_asporto === true) {

                        try {
                            var totale_visore = parseFloat(JSON.parse(dati_testa).totale).toFixed(2);
                            visore(["TOTALE", "€ " + totale_visore], "CENTRO");
                            var testo_query = "select Field415,Field416,Field417 from settaggi_profili where id=" + comanda.folder_number + " ";
                            var s = alasql(testo_query);
                            var settaggi_profilo = s[0];
                            if (settaggi_profilo.Field415 !== null && settaggi_profilo.Field415 !== "") {
                                setTimeout(function() {
                                    visore([settaggi_profilo.Field415, settaggi_profilo.Field416], "CENTRO");
                                }, parseInt(settaggi_profilo.Field417) * 1000);
                            }

                        } catch (e) {
                            console.log(e);
                        }
                    } else {
                        try {


                            var totale_visore = parseFloat(JSON.parse(dati_testa).totale).toFixed(2);
                            visore(["TOTALE", "€ " + totale_visore], "CENTRO");
                            var testo_query = "select Field415,Field416,Field417 from settaggi_profili where id=" + comanda.folder_number + " ";
                            var s = alasql(testo_query);
                            var settaggi_profilo = s[0];
                            if (settaggi_profilo.Field415 !== null && settaggi_profilo.Field415 !== "") {
                                setTimeout(function() {
                                    visore([settaggi_profilo.Field415, settaggi_profilo.Field416], "CENTRO");
                                }, parseInt(settaggi_profilo.Field417) * 1000);
                            }

                        } catch (e) {
                            console.log(e);
                        }
                    }



                    var testo_query_cancellazione = "DELETE FROM spooler_stampa WHERE id_univoco='" + id_univoco + "';";
                    comanda.importo_acconto_fattura = 0;
                    //console.log("SPOOLER DELETE", indirizzo, xml);
                    comanda.sincro.query(testo_query_cancellazione, function() {
                        console.log("OP. SPOOLER - DELETE RECORD", orario);
                        console.log("QUERY SPOOLER", testo_query_cancellazione);
                        console.log("CONTEGGIO SPOOLER", comanda.spooler_risposte_catturate, (totale_record - 1));
                        if (comanda.spooler_risposte_catturate >= (totale_record - 1)) {

                            console.log("SPOOLER AZZERATO");
                            comanda.spooler_risposte_catturate = 0;
                            //console.log("SPOOLER RIAPERTO 1");
                            //Lo spooler alla fine dev'essere riaperto
                            $('.spia.status_spooler').css('background-color', 'green');
                        } else if (comanda.spooler_risposte_catturate < (totale_record - 1)) {
                            comanda.spooler_risposte_catturate++;
                        }

                        console.log("CHIUDI SCONTRINO CONDIZIONE", tipo.toLowerCase());
                        //NEL CASO ITALIANO IL PROGRESSIVO VIENE MEMORIZZATO DOPO LA STAMPA
                        //PERCHE' E' LA CASSA AD ASSEGNARE IL PROGRESSIVO
                        if ((dati_testa !== 'undefined' && dati_testa !== undefined && dati_testa !== null) && (tipo.toLowerCase().trim() === "stampante fiscale" || tipo.toLowerCase().trim() === "scontrino" || tipo.toLowerCase().trim() === "scontrino fiscale" || tipo.toLowerCase().trim() === "fattura")) {

                            comanda.funzionidb.chiudi_scontrino(tipo.toLowerCase().trim(), dati_testa, risposta_xml);
                            console.log("CHIUDI SCONTRINO 0");
                        }

                        comanda.tentativo_fallito[indirizzo].buon_fine = false;
                        comanda.tentativo_fallito[indirizzo].timestamp = Date.now() / 1000;
                        console.log("AVVISO STAMPANTE SET", comanda.tentativo_fallito[indirizzo].timestamp);
                        var index = comanda.id_stampa.indexOf(id_univoco);
                        comanda.id_stampa.splice(index, 1);
                    });
                } else if (codice_errore === "PRINTER ERROR" || codice_errore === "PARSER_ERROR" || codice_errore === "INCOMPLETE FILE") {

                    salva_log_scontrino_annullato(indirizzo, xml);

                    var testo_query_cancellazione = "DELETE FROM spooler_stampa WHERE id_univoco='" + id_univoco + "' ; ";
                    //console.log("SPOOLER DELETE", indirizzo, xml);
                    comanda.sincro.query(testo_query_cancellazione, function() {

                        console.log("CONTEGGIO SPOOLER", comanda.spooler_risposte_catturate, (totale_record - 1));
                        if (comanda.spooler_risposte_catturate >= (totale_record - 1)) {

                            console.log("SPOOLER AZZERATO");
                            comanda.spooler_risposte_catturate = 0;
                            //console.log("SPOOLER RIAPERTO 1");
                            //Lo spooler alla fine dev'essere riaperto

                            $('.spia.status_spooler').css('background-color', 'green');
                        } else if (comanda.spooler_risposte_catturate < (totale_record - 1)) {
                            comanda.spooler_risposte_catturate++;
                        }

                        console.log("CHIUDI SCONTRINO CONDIZIONE", tipo.toLowerCase());
                        //NEL CASO ITALIANO IL PROGRESSIVO VIENE MEMORIZZATO DOPO LA STAMPA
                        //PERCHE' E' LA CASSA AD ASSEGNARE IL PROGRESSIVO
                        if ((dati_testa !== 'undefined' && dati_testa !== undefined && dati_testa !== null) && (tipo.toLowerCase().trim() === "stampante fiscale" || tipo.toLowerCase().trim() === "scontrino" || tipo.toLowerCase().trim() === "scontrino fiscale" || tipo.toLowerCase().trim() === "fattura")) {
                            console.log("AGGIUNGI SPOOLER QUERY", dati_testa);
                            //comanda.funzionidb.chiudi_scontrino(tipo.toLowerCase().trim(), dati_testa);
                            console.log("CHIUDI SCONTRINO 0");
                        }

                        if (codice_errore === "PRINTER ERROR") {
                            var epos = new epson.fiscalPrint();
                            epos.send(indirizzo, "<printerCommand><resetPrinter  operator='1' /></printerCommand>", 10000);
                        }

                        $('.nome_stampante_errata').html(tipo);
                        $('#popup_errore_stampante_sconosciuto').modal('show');
                        comanda.tentativo_fallito[indirizzo].timestamp = Date.now() / 1000;
                        console.log("AVVISO STAMPANTE SET", comanda.tentativo_fallito[indirizzo].timestamp);
                        comanda.tentativo_fallito[indirizzo].buon_fine = false;
                        var index = comanda.id_stampa.indexOf(id_univoco);
                        comanda.id_stampa.splice(index, 1);
                    });
                } else {
                    console.log(comanda.tentativo_fallito[indirizzo], Date.now() / 1000, comanda.tentativo_fallito[indirizzo].timestamp);
                    if ("AVVISO STAMPANTE COND1", Date.now() / 1000 - comanda.tentativo_fallito[indirizzo].timestamp, comanda.tentativo_fallito[indirizzo].buon_fine !== true) {

                        switch (codice_errore) {

                            case "FP_NO_ANSWER":
                                /*$('.nome_stampante_errata').html(tipo);
                                 $('#popup_errore_stampante_spenta').modal('show');*/

                                //TRIANGOLO STAMPANTE SPENTA
                                $("#errore_stampante_spenta").removeClass("hidden");

                                break;
                            case "EPTR_REC_EMPTY":
                                $('.nome_stampante_errata').html(tipo);
                                $('#popup_errore_fine_carta').modal('show');
                                break;
                            case "EPTR_COVER_OPEN":
                                $('.nome_stampante_errata').html(tipo);
                                $('#popup_errore_coperchio_aperto').modal('show');
                                break;
                            default:

                                salva_log_scontrino_annullato(indirizzo, xml);

                                $('.nome_stampante_errata').html(tipo);
                                $('#popup_errore_stampante_sconosciuto').modal('show');
                        }


                        comanda.tentativo_fallito[indirizzo].timestamp = Date.now() / 1000;
                        console.log("AVVISO STAMPANTE SET", comanda.tentativo_fallito[indirizzo].timestamp);
                    }


                    comanda.tentativo_fallito[indirizzo].buon_fine = true;
                    var query_spostamento_record = "update spooler_stampa set tentativo=cast(tentativo as int)+1,orario='" + new Date().format("yyyymmddHHMMss") + "' where id_univoco='" + id_univoco + "' ;";
                    console.log(query_spostamento_record);
                    comanda.sincro.query(query_spostamento_record, function() {

                        var index = comanda.id_stampa.indexOf(id_univoco);
                        comanda.id_stampa.splice(index, 1);
                    });
                }

            } else if (xhr.readyState === 4 && xhr.status === 0) {
                console.log("AVVISO STAMPANTE COND2", comanda.tentativo_fallito[indirizzo], Date.now() / 1000, comanda.tentativo_fallito[indirizzo].timestamp, comanda.tentativo_fallito[indirizzo].buon_fine !== true || Date.now() / 1000 - comanda.tentativo_fallito[indirizzo].timestamp >= 15);
                if (comanda.tentativo_fallito[indirizzo].buon_fine !== true || Date.now() / 1000 - comanda.tentativo_fallito[indirizzo].timestamp >= 15) {

                    /*$('.nome_stampante_errata').html(tipo);
                     $('#popup_errore_stampante_spenta').modal('show');*/

                    //TRIANGOLO STAMPANTE SPENTA
                    $("#errore_stampante_spenta").removeClass("hidden");



                    comanda.tentativo_fallito[indirizzo].timestamp = Date.now() / 1000;
                    console.log("AVVISO STAMPANTE SET", comanda.tentativo_fallito[indirizzo].timestamp);
                }

                var query_spostamento_record = "";
                if (indirizzo_alternativo !== undefined && indirizzo_alternativo !== null && indirizzo_alternativo !== "undefined" && indirizzo_alternativo !== "") {
                    query_spostamento_record = "update spooler_stampa set indirizzo=indirizzo_alternativo,tentativo=cast(tentativo as int)+1,orario='" + new Date().format("yyyymmddHHMMss") + "' where id_univoco='" + id_univoco + "' ;";
                    $('.dirottamento_stampa').html("Dirottamento Stampa in corso...");
                } else {
                    query_spostamento_record = "update spooler_stampa set tentativo=cast(tentativo as int)+1,orario='" + new Date().format("yyyymmddHHMMss") + "' where id_univoco='" + id_univoco + "' ;";
                    $('.dirottamento_stampa').html("");
                }
 //                    console.log(query_spostamento_record);
                comanda.sincro.query(query_spostamento_record, function () {

                    comanda.tentativo_fallito[indirizzo].buon_fine = true;
                    var index = comanda.id_stampa.indexOf(id_univoco);
                    comanda.id_stampa.splice(index, 1);
                });
               
            } /*else {
             
             console.log("AVVISO STAMPANTE COND3", comanda.tentativo_fallito[indirizzo], Date.now() / 1000, comanda.tentativo_fallito[indirizzo].timestamp, comanda.tentativo_fallito[indirizzo].buon_fine !== true || Date.now() / 1000 - comanda.tentativo_fallito[indirizzo].timestamp >= 15);
             
             if (comanda.tentativo_fallito[indirizzo].buon_fine !== true || Date.now() / 1000 - comanda.tentativo_fallito[indirizzo].timestamp >= 15) {
             
             $('.nome_stampante_errata').html(tipo);
             
             $('#popup_errore_stampante_spenta').modal('show');
             
             
             comanda.tentativo_fallito[indirizzo].timestamp = Date.now() / 1000;
             console.log("AVVISO STAMPANTE SET", comanda.tentativo_fallito[indirizzo].timestamp);
             }
             
             var query_spostamento_record = "";
             
             if (indirizzo_alternativo !== undefined && indirizzo_alternativo !== null && indirizzo_alternativo !== "undefined" && indirizzo_alternativo !== "") {
             query_spostamento_record = "update spooler_stampa set indirizzo=indirizzo_alternativo,tentativo=tentativo+1,orario='" + id_comanda().substr(4) + "' where id_univoco='" + id_univoco + "' ;";
             $('.dirottamento_stampa').html("Dirottamento Stampa in corso...");
             } else {
             query_spostamento_record = "update spooler_stampa set tentativo=tentativo+1,orario='" + id_comanda().substr(4) + "' where id_univoco='" + id_univoco + "' ;";
             $('.dirottamento_stampa').html("");
             }
             //                    console.log(query_spostamento_record);
             comanda.sincro.query(query_spostamento_record, function () {
             
             comanda.tentativo_fallito[indirizzo].buon_fine = true;
             
             var index = comanda.id_stampa.indexOf(id_univoco);
             comanda.id_stampa.splice(index, 1);
             
             });
             
             }*/
        };
        //RISULTATO PING
    }

}



function salva_log_scontrino_annullato(indirizzo, xml) {
    //SALVARE XML SCONTRINO ANNULLATO
    /*if (comanda.pizzeria_asporto === true) {*/

    if (comanda.mobile !== true & comanda.compatibile_xml !== true) {
        if (indirizzo !== undefined && typeof indirizzo === "string" && indirizzo.indexOf("fpmate.cgi") !== -1 && xml !== undefined && typeof xml === "string") {

            $.ajax({
                type: "POST",
                url: comanda.url_server + "classi_php/salva_file_generico.php",
                data: { nome: "LogScontriniErrati/ScontrinoErrato_" + new Date().format("ddmmyyyy_HHMMss") + "_" + comanda.operatore + ".xml", contenuto: xml.trim() },
                success: function() {

                }
            });

        }
    }
}