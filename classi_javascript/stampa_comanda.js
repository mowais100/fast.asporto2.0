/* global bootbox, comanda */

function aggiungi_dispositivo(devid_nf, devicetype, devicemodel, deviceip)
{
    var aggiunta_dispositivo = '<fpMateConfiguration><addDevice  deviceId = "' + devid_nf + '" deviceIp = "' + deviceip + '" deviceType = "' + devicetype + '" deviceModel = "' + devicemodel + '" deviceRetry = "10" /></fpMateConfiguration>';
    var test = new epson.fiscalPrint();
    test.send(window.location.protocol + "//" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi", aggiunta_dispositivo, 30000);
}

/*function stampa_comanda(saltafaseparcheggio, disabilita_avviso, cambio_orario_consegna, torna_a_tavolo, solo_riepiloghi, bool_ristampa) {

    var data = comanda.funzionidb.data_attuale().substr(0, 8);
    var ora = comanda.funzionidb.data_attuale().substr(9, 8);

    var promessa = new Promise(function (resolve, reject) {



        var query = "select * from comanda where ntav_comanda='" + comanda.tavolo + "' and stato_record='ATTIVO' and desc_art!='EXTRA' and desc_art!='=EXTRA' order by posizione asc,nodo asc;";

        comanda.sincro.query(query, function (calcolo_varianti) {

            var buffer_nodo_princ = new Object();

            var passa_funzione = true;
            var codici_promo_1_presenti = false;


            calcolo_varianti.forEach(function (e) {
                if (e.cod_promo === '1') {
                    codici_promo_1_presenti = true;
                    buffer_nodo_princ[e.nodo] = new Object();
                    buffer_nodo_princ[e.nodo].totale_chiamante = parseFloat(e.prezzo_un) * parseInt(e.quantita);
                    buffer_nodo_princ[e.nodo].totale_varianti = 0;
                } else if (e.cod_promo === 'V_1' && e.nodo.length > 3 && buffer_nodo_princ[e.nodo.substr(0, 3)] !== undefined) {
                    buffer_nodo_princ[e.nodo.substr(0, 3)].totale_varianti += parseFloat(e.prezzo_un) * parseInt(e.quantita);
                } else if (codici_promo_1_presenti === true && e.posizione === "SCONTO" && comanda.sconto_su_promo === "0") {
                    passa_funzione = "ERR_SCONTO_PROMO";
                }

            });


            for (var nodo in buffer_nodo_princ) {
                if (parseFloat(buffer_nodo_princ[nodo].totale_chiamante).toFixed(2) !== parseFloat(buffer_nodo_princ[nodo].totale_varianti).toFixed(2)) {
                    passa_funzione = "ERR_TOTALI";
                }
            }


            if (passa_funzione === true) {

                if (comanda.tavolo === "BAR" || comanda.tavolo.indexOf("CONTINUO") !== -1) {
                    if (comanda.stampa_comanda === false) {
                        var testo_query = "update comanda set stampata_sn='s',data_stampa='" + data + "',ora_stampa='" + ora + "',fiscalizzata_sn='c' where comanda.stato_record='ATTIVO' and comanda.ntav_comanda='" + comanda.tavolo + "'";
                    } else
                    {
                        var testo_query = "update comanda set fiscalizzata_sn='c' where comanda.stato_record='ATTIVO' and comanda.ntav_comanda='" + comanda.tavolo + "'";
                    }
                    comanda.sock.send({tipo: "comanda_stampata", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                    comanda.sincro.query(testo_query, function () {
                        //salva_comanda(comanda.parcheggio, true);
                        if (comanda.array_dbcentrale_mancanti.length > 0) {
                            comanda.sincro.query_cassa();
                        }
                        resolve(true);
                    });
                } else if (comanda.licenza_vera === 'BrovedaniVerona' && comanda.avviso_righe_prezzo_zero === '1' && $(comanda.conto).not(comanda.intestazione_conto).find('tr[id^="art_"]:contains("€"):contains("€0.00")').not('.non-contare-riga').length > 0) {

                    bootbox.confirm("Attenzione: ci sono righe a prezzo zero. Vuoi continuare a stampare lo scontrino?", function (a) {

                        if (a === true) {
                            resolve(true);
                        } else
                        {
                            resolve(false);
                        }

                    });

                } else
                {
                    resolve(true);
                }

            } else if (passa_funzione === "ERR_TOTALI")
            {
                bootbox.alert("Il totale delle componenti non corrisponde al valore della promozione." +
                        "<br><br>Totale dovuto: &euro; " + parseFloat(buffer_nodo_princ[nodo].totale_chiamante).toFixed(2) +
                        "<br>Totale inserito: &euro; " + parseFloat(buffer_nodo_princ[nodo].totale_varianti).toFixed(2));
            } else if (passa_funzione === "ERR_SCONTO_PROMO") {
                bootbox.alert("Non si possono effettuare sconti su una promozione.");
            }

        });

    });

    promessa.then(
            // Scrivi un log con un messaggio e un valore
                    function (val) {

                        if (val === true) {

                            test_copertura(function (stato_copertura) {

                                var query = "select * from settaggi_profili;";
                                comanda.sincro.query(query, function (settaggi_profili) {
                                    settaggi_profili = settaggi_profili[0];

                                    if (stato_copertura === true) {

                                        if (solo_riepiloghi !== true && saltafaseparcheggio !== true && (comanda.tavolo.indexOf("CONTINUO") !== -1 || comanda.tavolo === "BANCO" || comanda.tavolo === "BAR" || comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === "TAKE AWAY")) {
                                            comanda.stampato = false;
                                            btn_parcheggia();
                                        } else
                                        {
                                            comanda.stampato = true;
                                            console.log("stampa_comanda asporto", comanda.stampato, saltafaseparcheggio, comanda.tavolo, comanda.parcheggio);
                                            var nome_cliente = comanda.parcheggio;
                                            if (bool_ristampa !== true && $("#conto tr").not('.comanda').not('.non-contare-riga').length === 0)
                                            {

                                                if (disabilita_avviso !== true && comanda.tavolo !== 'BAR' && comanda.tavolo.indexOf('CONTINUO') === -1) {
                                                    bootbox.confirm("Non hai nuovi articoli da lanciare in comanda. Vuoi ristampare la comanda vecchia?", function (risp) {
                                                        if (risp === true) {
                                                            stampa_comanda(undefined, undefined, undefined, undefined, undefined, true);
                                                        }
                                                    });
                                                } else if (solo_riepiloghi === true && comanda.tavolo.indexOf("CONTINUO") !== -1) {
                                                    comanda.funzionidb.conto_attivo(function () {
                                                        //seleziona_metodo_pagamento('SCONTRINO FISCALE');
                                                        stampa_scontrino('SCONTRINO FISCALE', undefined, undefined);
                                                    });
                                                }

                                            } else
                                            {
                                                var testo_query = "select id,ricetta from prodotti;";
                                                console.log("STAMPA COMANDA", testo_query);

                                                comanda.sincro.query(testo_query, function (result_prodotti) {

                                                    var corrispondenza_id_prodotti = new Object();

                                                    result_prodotti.forEach(function (element) {

                                                        corrispondenza_id_prodotti[element.id] = element.ricetta;

                                                    });

                                                    var testo_query = "";

                                                    if (solo_riepiloghi !== true) {
                                                        if (bool_ristampa === true) {
                                                            testo_query = "select  substr(desc_art,1,3)||'M'|| substr(nodo,1,3) as ord,c1.cod_promo,cod_articolo,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,dest_stampa_2,portata,categoria,prog_inser,nodo,desc_art,tasto_segue,prezzo_un,quantita from comanda as c1 where contiene_variante='1' and ntav_comanda='" + comanda.tavolo + "'  and posizione='CONTO'  and stato_record='ATTIVO' \n\
                                                    union all\n\
                                                    select  substr(desc_art,1,3)||'M'||substr(nodo,1,3) as ord,c1.cod_promo,cod_articolo,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,dest_stampa_2,portata,categoria,prog_inser,nodo,desc_art,tasto_segue,prezzo_un,sum(quantita) as quantita from comanda as c1 where (contiene_variante !='1' or contiene_variante is null or contiene_variante='null') and length(nodo)=3 and ntav_comanda='" + comanda.tavolo + "' and posizione='CONTO'  and stato_record='ATTIVO' group by tasto_segue,desc_art,nome_comanda\n\
                                                    union all\n\
                                                    select  (select substr(desc_art,1,3) from comanda where nodo=substr(c1.nodo,1,3) and stato_record='ATTIVO'  and ntav_comanda='" + comanda.tavolo + "')||'M' ||substr(nodo,1,3) as ord,c1.cod_promo,cod_articolo,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,dest_stampa_2,portata,categoria,prog_inser,nodo,desc_art,tasto_segue,prezzo_un,quantita from comanda as c1 where length(nodo)=7 and substr(desc_art,1,1)='-' and ntav_comanda='" + comanda.tavolo + "'   and posizione='CONTO'  and stato_record='ATTIVO'\n\
                                                    union all\n\
                                                    select (select substr(desc_art,1,3) from comanda where nodo=substr(c1.nodo,1,3) and stato_record='ATTIVO'  and ntav_comanda='" + comanda.tavolo + "')||'M'||substr(nodo,1,3) as ord,c1.cod_promo,cod_articolo,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,dest_stampa_2,portata,categoria,prog_inser,nodo,desc_art,tasto_segue,prezzo_un,quantita from comanda as c1 where length(nodo)=7 and substr(desc_art,1,1)!='-' and ntav_comanda='" + comanda.tavolo + "'  and posizione='CONTO'   and stato_record='ATTIVO'\n\
                                                    order by nome_comanda DESC, ord ASC;";
                                                        } else {
                                                            testo_query = "select  substr(desc_art,1,3)||'M'|| substr(nodo,1,3) as ord,c1.cod_promo,cod_articolo,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,dest_stampa_2,portata,categoria,prog_inser,nodo,desc_art,tasto_segue,prezzo_un,quantita from comanda as c1 where contiene_variante='1' and ntav_comanda='" + comanda.tavolo + "'  and posizione='CONTO' and (stampata_sn is null or stampata_sn='null' or stampata_sn='n' or stampata_sn='')  and stato_record='ATTIVO' \n\
                                                    union all\n\
                                                    select  substr(desc_art,1,3)||'M'||substr(nodo,1,3) as ord,c1.cod_promo,cod_articolo,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,dest_stampa_2,portata,categoria,prog_inser,nodo,desc_art,tasto_segue,prezzo_un,sum(quantita) as quantita from comanda as c1 where (contiene_variante !='1' or contiene_variante is null or contiene_variante='null') and length(nodo)=3 and ntav_comanda='" + comanda.tavolo + "' and posizione='CONTO' and (stampata_sn is null or stampata_sn='null' or stampata_sn='n' or stampata_sn='')  and stato_record='ATTIVO' group by tasto_segue,desc_art,nome_comanda\n\
                                                    union all\n\
                                                    select  (select substr(desc_art,1,3) from comanda where nodo=substr(c1.nodo,1,3) and stato_record='ATTIVO'  and ntav_comanda='" + comanda.tavolo + "')||'M' ||substr(nodo,1,3) as ord,c1.cod_promo,cod_articolo,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,dest_stampa_2,portata,categoria,prog_inser,nodo,desc_art,tasto_segue,prezzo_un,quantita from comanda as c1 where length(nodo)=7 and substr(desc_art,1,1)='-' and ntav_comanda='" + comanda.tavolo + "'   and posizione='CONTO' and (stampata_sn is null or stampata_sn='null' or stampata_sn='n' or stampata_sn='')  and stato_record='ATTIVO'\n\
                                                    union all\n\
                                                    select (select substr(desc_art,1,3) from comanda where nodo=substr(c1.nodo,1,3) and stato_record='ATTIVO'  and ntav_comanda='" + comanda.tavolo + "')||'M'||substr(nodo,1,3) as ord,c1.cod_promo,cod_articolo,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,dest_stampa_2,portata,categoria,prog_inser,nodo,desc_art,tasto_segue,prezzo_un,quantita from comanda as c1 where length(nodo)=7 and substr(desc_art,1,1)!='-' and ntav_comanda='" + comanda.tavolo + "'  and posizione='CONTO' and (stampata_sn is null or stampata_sn='null' or stampata_sn='n' or stampata_sn='')  and stato_record='ATTIVO'\n\
                                                    order by nome_comanda DESC, ord ASC;";
                                                        }
                                                    } else {
                                                        testo_query = "select  substr(desc_art,1,3)||'M'|| substr(nodo,1,3) as ord,c1.cod_promo,c1.cod_articolo as cod_articolo,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,dest_stampa_2,c1.portata as portata,c1.categoria as categoria,prog_inser,nodo,desc_art,tasto_segue,tasto_segue,prezzo_un,quantita from comanda as c1, prodotti as p where contiene_variante='1' and ntav_comanda='" + comanda.tavolo + "'  and c1.posizione='CONTO' and (stampata_sn is null or stampata_sn='null' or stampata_sn='n' or stampata_sn='')  and stato_record='ATTIVO' and p.id=c1.cod_articolo and p.abilita_riepilogo='S' \n\
                                                    union all\n\
                                                    select  substr(desc_art,1,3)||'M'||substr(nodo,1,3) as ord,c1.cod_promo,c1.cod_articolo as cod_articolo,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,dest_stampa_2,c1.portata as portata,c1.categoria as categoria,prog_inser,nodo,desc_art,tasto_segue,tasto_segue,prezzo_un,sum(quantita) as quantita from comanda as c1, prodotti as p where (contiene_variante !='1' or contiene_variante is null or contiene_variante='null') and length(nodo)=3 and ntav_comanda='" + comanda.tavolo + "' and c1.posizione='CONTO' and (stampata_sn is null or stampata_sn='null' or stampata_sn='n' or stampata_sn='')  and stato_record='ATTIVO'  and p.id=c1.cod_articolo and p.abilita_riepilogo='S' group by tasto_segue,desc_art,nome_comanda\n\
                                                    union all\n\
                                                    select (select substr(desc_art,1,3) from comanda where nodo=substr(c1.nodo,1,3) and stato_record='ATTIVO'  and ntav_comanda='" + comanda.tavolo + "')||'M' ||substr(nodo,1,3) as ord,c1.cod_promo,c1.cod_articolo as cod_articolo,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,dest_stampa_2,c1.portata as portata,c1.categoria as categoria,prog_inser,nodo,desc_art,tasto_segue,tasto_segue,prezzo_un,quantita from comanda as c1, prodotti as p where length(nodo)=7 and substr(desc_art,1,1)='-' and ntav_comanda='" + comanda.tavolo + "'   and c1.posizione='CONTO' and (stampata_sn is null or stampata_sn='null' or stampata_sn='n' or stampata_sn='')  and stato_record='ATTIVO' and p.id=c1.cod_articolo  and p.abilita_riepilogo='S'\n\
                                                    union all\n\
                                                    select (select substr(desc_art,1,3) from comanda where nodo=substr(c1.nodo,1,3) and stato_record='ATTIVO'  and ntav_comanda='" + comanda.tavolo + "')||'M'||substr(nodo,1,3) as ord,c1.cod_promo,c1.cod_articolo as cod_articolo,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,dest_stampa_2,c1.portata as portata,c1.categoria as categoria,prog_inser,nodo,desc_art,tasto_segue,tasto_segue,prezzo_un,quantita from comanda as c1, prodotti as p where length(nodo)=7 and substr(desc_art,1,1)!='-' and ntav_comanda='" + comanda.tavolo + "'  and c1.posizione='CONTO' and (stampata_sn is null or stampata_sn='null' or stampata_sn='n' or stampata_sn='')  and stato_record='ATTIVO'  and p.id=c1.cod_articolo and p.abilita_riepilogo='S'\n\
                                                    order by nome_comanda DESC, ord ASC;";
                                                    }

                                                    console.log("STAMPA COMANDA", testo_query);

                                                    var prom = new Promise(function (resolve, reject) {
                                                        if (settaggi_profili.Field9 === 'true')
                                                        {
                                                            dati_comanda(function (res) {
                                                                resolve(res);
                                                            });
                                                        } else
                                                        {
                                                            comanda.sincro.query(testo_query, function (res) {
                                                                resolve(res);
                                                            });
                                                        }
                                                    });
                                                    var array_comanda = new Array();
                                                    var array_dest_stampa = new Array();

                                                    prom.then(function (result) {

                                                        if (result.length > 0) {
                                                            fa_richiesta_progressivo(function (n_prog) {

                                                                console.log("RISULTATI COMANDA", result);

                                                                var progressivo = 0;
                                                                result.forEach(function (obj) {
                                                                    if (obj['cod_promo'] === "V_1") {
                                                                        var descrizione = obj['desc_art'].replace(/&#37;/gi, "%").replace(/&#8364;/gi, "�").replace(/=/gi, "...");
                                                                    } else {
                                                                        var descrizione = obj['desc_art'].replace(/&#37;/gi, "%").replace(/&#8364;/gi, "�");
                                                                    }
                                                                    var prezzo = obj['prezzo_un'];
                                                                    var nodo = obj['nodo'];
                                                                    var quantita = obj['quantita'];

                                                                    var cod_articolo = obj['cod_articolo'];
                                                                    if (cod_articolo !== undefined && cod_articolo !== 'null' && cod_articolo.length > 0)
                                                                    {
                                                                        console.log("ricette", corrispondenza_id_prodotti, cod_articolo);
                                                                        var ricetta = corrispondenza_id_prodotti[cod_articolo];
                                                                    }

                                                                    var dest_stampa = obj['dest_stampa'];
                                                                    var portata = obj['portata'];
                                                                    var tasto_segue = obj['tasto_segue'];
                                                                    var categoria = obj['categoria'];

                                                                    if (dest_stampa === "T") {
                                                                        //CICLO STAMPANTI
                                                                        comanda.nome_stampante.forEach(function (value, index) {

                                                                            if (value.fiscale === "n") {

                                                                                if (array_comanda[index] === undefined)
                                                                                {
                                                                                    array_comanda[index] = new Object();
                                                                                }

                                                                                if (array_dest_stampa[index] === undefined)
                                                                                {
                                                                                    array_dest_stampa[index] = new Object();
                                                                                    array_dest_stampa[index].val = comanda.nome_stampante[index].nome;
                                                                                    array_dest_stampa[index].dimensione = comanda.nome_stampante[index].dimensione;
                                                                                    array_dest_stampa[index].ip = comanda.nome_stampante[index].ip;
                                                                                    array_dest_stampa[index].ip_alternativo = comanda.nome_stampante[index].ip_alternativo;
                                                                                    array_dest_stampa[index].intelligent = comanda.nome_stampante[index].intelligent;
                                                                                    array_dest_stampa[index].fiscale = comanda.nome_stampante[index].fiscale;
                                                                                    array_dest_stampa[index].nome_umano = comanda.nome_stampante[index].nome_umano;
                                                                                    array_dest_stampa[index].devid_nf = comanda.nome_stampante[index].devid_nf;
                                                                                    array_dest_stampa[index].devicetype = comanda.nome_stampante[index].devicetype;
                                                                                    array_dest_stampa[index].devicemodel = comanda.nome_stampante[index].devicemodel;
                                                                                }

                                                                                if (array_comanda[index][tasto_segue] === undefined)
                                                                                {
                                                                                    array_comanda[index][tasto_segue] = new Object();
                                                                                }

                                                                                if (array_comanda[index][tasto_segue][portata] === undefined)
                                                                                {
                                                                                    array_comanda[index][tasto_segue][portata] = new Object();
                                                                                    array_comanda[index][tasto_segue][portata].val = comanda.nome_portata[portata];
                                                                                }


                                                                                if (array_comanda[index][tasto_segue][portata][(progressivo + 1)] === undefined)
                                                                                {
                                                                                    array_comanda[index][tasto_segue][portata][(progressivo + 1)] = [quantita, descrizione, prezzo, ricetta];

                                                                                }
                                                                                progressivo++;

                                                                            }
                                                                        });


                                                                    } else
                                                                    {
                                                                        if (array_comanda[dest_stampa] === undefined)
                                                                        {
                                                                            array_comanda[dest_stampa] = new Object();
                                                                        }

                                                                        if (array_dest_stampa[dest_stampa] === undefined)
                                                                        {
                                                                            array_dest_stampa[dest_stampa] = new Object();
                                                                            array_dest_stampa[dest_stampa].val = comanda.nome_stampante[dest_stampa].nome;
                                                                            array_dest_stampa[dest_stampa].dimensione = comanda.nome_stampante[dest_stampa].dimensione;
                                                                            array_dest_stampa[dest_stampa].ip = comanda.nome_stampante[dest_stampa].ip;
                                                                            array_dest_stampa[dest_stampa].ip_alternativo = comanda.nome_stampante[dest_stampa].ip_alternativo;
                                                                            array_dest_stampa[dest_stampa].intelligent = comanda.nome_stampante[dest_stampa].intelligent;
                                                                            array_dest_stampa[dest_stampa].fiscale = comanda.nome_stampante[dest_stampa].fiscale;
                                                                            array_dest_stampa[dest_stampa].nome_umano = comanda.nome_stampante[dest_stampa].nome_umano;
                                                                            array_dest_stampa[dest_stampa].devid_nf = comanda.nome_stampante[dest_stampa].devid_nf;
                                                                            array_dest_stampa[dest_stampa].devicetype = comanda.nome_stampante[dest_stampa].devicetype;
                                                                            array_dest_stampa[dest_stampa].devicemodel = comanda.nome_stampante[dest_stampa].devicemodel;
                                                                        }

                                                                        if (array_comanda[dest_stampa][tasto_segue] === undefined)
                                                                        {
                                                                            array_comanda[dest_stampa][tasto_segue] = new Object();
                                                                        }

                                                                        if (array_comanda[dest_stampa][tasto_segue][portata] === undefined)
                                                                        {
                                                                            array_comanda[dest_stampa][tasto_segue][portata] = new Object();
                                                                            array_comanda[dest_stampa][tasto_segue][portata].val = comanda.nome_portata[portata];
                                                                        }


                                                                        if (array_comanda[dest_stampa][tasto_segue][portata][(progressivo + 1)] === undefined)
                                                                        {
                                                                            array_comanda[dest_stampa][tasto_segue][portata][(progressivo + 1)] = [quantita, descrizione, prezzo, ricetta];
                                                                        }
                                                                        progressivo++;


                                                                    }


                                                                    //DESTINAZIONE STAMPA 2

                                                                    if (obj['cod_promo'] === "V_1") {
                                                                        var descrizione = obj['desc_art'].replace(/&#37;/gi, "%").replace(/&#8364;/gi, "�").replace(/=/gi, "...");
                                                                    } else {
                                                                        var descrizione = obj['desc_art'].replace(/&#37;/gi, "%").replace(/&#8364;/gi, "�");
                                                                    }
                                                                    var prezzo = obj['prezzo_un'];
                                                                    var nodo = obj['nodo'];
                                                                    var quantita = obj['quantita'];

                                                                    var cod_articolo = obj['cod_articolo'];
                                                                    if (cod_articolo !== undefined && cod_articolo !== 'null' && cod_articolo.length > 0)
                                                                    {
                                                                        console.log("ricette", corrispondenza_id_prodotti, cod_articolo);
                                                                        var ricetta = corrispondenza_id_prodotti[cod_articolo];
                                                                    }

                                                                    var dest_stampa = obj['dest_stampa_2'];
                                                                    var portata = obj['portata'];
                                                                    var tasto_segue = obj['tasto_segue'];
                                                                    var categoria = obj['categoria'];


                                                                    if (dest_stampa !== null && dest_stampa !== "" && dest_stampa !== "null"  && dest_stampa !== obj['dest_stampa'] && obj['dest_stampa'] !== "T") {


                                                                        if (array_comanda[dest_stampa] === undefined)
                                                                        {
                                                                            array_comanda[dest_stampa] = new Object();
                                                                        }

                                                                        if (array_dest_stampa[dest_stampa] === undefined)
                                                                        {
                                                                            array_dest_stampa[dest_stampa] = new Object();
                                                                            array_dest_stampa[dest_stampa].val = comanda.nome_stampante[dest_stampa].nome;
                                                                            array_dest_stampa[dest_stampa].dimensione = comanda.nome_stampante[dest_stampa].dimensione;
                                                                            array_dest_stampa[dest_stampa].ip = comanda.nome_stampante[dest_stampa].ip;
                                                                            array_dest_stampa[dest_stampa].ip_alternativo = comanda.nome_stampante[dest_stampa].ip_alternativo;
                                                                            array_dest_stampa[dest_stampa].intelligent = comanda.nome_stampante[dest_stampa].intelligent;
                                                                            array_dest_stampa[dest_stampa].fiscale = comanda.nome_stampante[dest_stampa].fiscale;
                                                                            array_dest_stampa[dest_stampa].nome_umano = comanda.nome_stampante[dest_stampa].nome_umano;
                                                                            array_dest_stampa[dest_stampa].devid_nf = comanda.nome_stampante[dest_stampa].devid_nf;
                                                                            array_dest_stampa[dest_stampa].devicetype = comanda.nome_stampante[dest_stampa].devicetype;
                                                                            array_dest_stampa[dest_stampa].devicemodel = comanda.nome_stampante[dest_stampa].devicemodel;
                                                                        }

                                                                        if (array_comanda[dest_stampa][tasto_segue] === undefined)
                                                                        {
                                                                            array_comanda[dest_stampa][tasto_segue] = new Object();
                                                                        }

                                                                        if (array_comanda[dest_stampa][tasto_segue][portata] === undefined)
                                                                        {
                                                                            array_comanda[dest_stampa][tasto_segue][portata] = new Object();
                                                                            array_comanda[dest_stampa][tasto_segue][portata].val = comanda.nome_portata[portata];
                                                                        }


                                                                        if (array_comanda[dest_stampa][tasto_segue][portata][(progressivo + 1)] === undefined)
                                                                        {
                                                                            array_comanda[dest_stampa][tasto_segue][portata][(progressivo + 1)] = [quantita, descrizione, prezzo, ricetta];

                                                                        }
                                                                        progressivo++;

                                                                    }

                                                                });
                                                                //---COMANDA INTELLIGENT---//

                                                                for (var dest_stampa in array_comanda) {

                                                                    console.log("NOME STAMPANTI", comanda.nome_stampante[dest_stampa]);
                                                                    switch (array_dest_stampa[dest_stampa].intelligent) {


                                                                        case "N":
                                                                        case "n":
                                                                            console.log("STAMPA COMANDA NORMALE", array_dest_stampa[dest_stampa].ip);
                                                                            //---COMANDA NON INTELLIGENT---//

                                                                            var corpo;
                                                                            corpo = '<epos-print>\n\
                    <text  dw="1" dh="1" description="' + comanda.lang_stampa[11] + '" />\n\
                    <feed  type="1" />\n\
                    <feed  type="1" />';
                                                                            corpo += '<text  dw="1" dh="1" description="' + comanda.lang_stampa[68].toUpperCase() + ': ' + comanda.tavolo + '" />\n\
                    <feed  type="1" />\n\
                    <feed  type="1" />';
                                                                            corpo += '<text  dw="0" dh="0" description="--------------------------------------" />\n\
                    <feed  type="1" />\n\
                    <feed  type="1" />';
                                                                            corpo += '<text  dw="0" dh="0" description="' + comanda.lang_stampa[109] + ': ' + comanda.operatore + '" />\n\
                    <feed  type="1" />\n\
                    <feed  type="1" />';
                                                                            corpo += '<text  dw="0" dh="0" description="' + comanda.lang_stampa[110] + ': ' + comanda.funzionidb.data_attuale() + '" />\n\
                    <feed  type="1" />\n\
                    <feed  type="1" />';
                                                                            corpo += '<text  dw="0" dh="0" description="' + comanda.lang_stampa[111] + ': [ ' + array_dest_stampa[dest_stampa].nome_umano + ' ]" />\n\
                    <feed  type="1" />\n\
                    <feed  type="1" />';
                                                                            corpo += '<text  dw="0" dh="0" description="--------------------------------------" />\n\
                    <feed  type="1" />\n\
                    <feed  type="1" />';
                                                                            var totale_quantita = 0;
                                                                            for (var portata in array_comanda[dest_stampa]) {
                                                                                if (array_comanda[dest_stampa][portata].val !== undefined) {

                                                                                    corpo += '<text  dw="0" dh="0" description="- - - ' + array_comanda[dest_stampa][portata].val + ' - - -" />\n\
                                            <feed  type="1" />\n\
                                                <feed  type="1" />';
                                                                                    for (var node in array_comanda[dest_stampa][portata]) {

                                                                                        var nodo = array_comanda[dest_stampa][portata][node];
                                                                                        if (nodo !== undefined && nodo[1] !== undefined && nodo[1].length > 1) {

                                                                                            var articolo = filtra_accenti(nodo[1]);
                                                                                            var quantita = nodo[0];
                                                                                            if (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(" && articolo[0] !== "=" && articolo[0] !== "." && articolo[0] !== "*") {
                                                                                                totale_quantita = parseInt(quantita) + totale_quantita;
                                                                                                corpo += '  <text  dw="0" dh="0" description="' + quantita + ' ' + articolo + '" />\n\
                                <feed  type="1" />\n\
                                <feed  type="1" />';
                                                                                            } else {

                                                                                                corpo += '  <text  dw="0" dh="0" description="' + articolo + '" />\n\
                                <feed  type="1" />\n\
                                <feed  type="1" />';
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }

                                                                                for (var dest_stampa_concomitanza in array_comanda) {

                                                                                    corpo += '<text  dw="0" dh="0" description="-------------CONCOMITANZE-------------" />\n';
                                                                                    corpo += '<text  dw="0" dh="0" description="' + comanda.lang_stampa[111] + ': [ ' + array_dest_stampa[dest_stampa_concomitanza].nome_umano + ' ]" />\n';
                                                                                    if (dest_stampa_concomitanza !== dest_stampa) {

                                                                                        corpo += '<text  dw="0" dh="0" description="- - - ' + array_comanda[dest_stampa][portata].val + ' - - -" />\n\
                                                    <feed  type="1" />\n\
                                                    <feed  type="1" />';
                                                                                        for (var node_concomitanza in array_comanda[dest_stampa_concomitanza][portata]) {

                                                                                            var nodo = array_comanda[dest_stampa_concomitanza][portata][node_concomitanza];
                                                                                            if (nodo !== undefined && nodo[1] !== undefined && nodo[1].length > 1) {

                                                                                                var articolo = filtra_accenti(nodo[1]);
                                                                                                var quantita = nodo[0];
                                                                                                if (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(" && articolo[0] !== "=" && articolo[0] !== "." && articolo[0] !== "*") {
                                                                                                    totale_quantita = parseInt(quantita) + totale_quantita;
                                                                                                    corpo += '  <text  dw="0" dh="0" description="' + quantita + ' ' + articolo + '" />\n\
                                                                <feed  type="1" />\n\
                                                                <feed  type="1" />';
                                                                                                } else {

                                                                                                    corpo += '  <text  dw="0" dh="0" description="' + articolo + '" />\n\
                                                                <feed  type="1" />\n\
                                                                <feed  type="1" />';
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                }
                                                                            }

                                                                            corpo += '  <text  dw="0" dh="0" description="' + totale_quantita + '" />\n\
                                <feed  type="1" />\n\
                                <feed  type="1" />';
                                                                            corpo += '  <feed  type="1" />\n\
                                <feed  type="1" />\n\
                                    <feed  type="1" />\n\
                                        <feed  type="1" />\n\
                                            <feed  type="1" />\n\
                                                <cut  type="feed" />\n\
                                                    </epos-print>';
                                                                            //CAMBIANDO QUESTO PARAMETRO CAMBIA L'IP DI STAMPA (quindi si potrebbe prendere da comanda.nome_stampante[num].ip
                                                                            //SI POTREBBE TESTARE LA DIMENSIONE CON comanda.nome_stampante[num].dimensione
                                                                            //E SE E' O NO FISCALE CON IL PARAMETRO comanda.nome_stampante[num].fiscale
                                                                            //test.send(window.location.protocol + "//" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=" + deviceid, corpo, 10000);

                                                                            corpo = '<?xml version="1.0" encoding="utf-8"?>\n' +
                                                                                    '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">\n' +
                                                                                    '<s:Body>\n' + corpo +
                                                                                    '</s:Body>\n' +
                                                                                    '</s:Envelope>\n';
                                                                            console.log("CORPO", corpo);
                                                                            aggiungi_spool_stampa(window.location.protocol + "//" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=" + array_dest_stampa[dest_stampa].devid_nf + "&timeout=30000", corpo, array_dest_stampa[dest_stampa].nome_umano);
                                                                            //---FINE COMANDA NON INTELLIGENT---//

                                                                            break;
                                                                        case "S":
                                                                        case "s":
                                                                        case "SDS":
                                                                            if (array_dest_stampa[dest_stampa].fiscale.toUpperCase() === 'N') {

                                                                                var concomitanze_presenti = false;
                                                                                console.log("STAMPA COMANDA INTELLIGENT", array_dest_stampa[dest_stampa].ip);
                                                                                var com = '';
                                                                                var data = comanda.funzionidb.data_attuale();
                                                                                var builder = new epson.ePOSBuilder();
                                                                                var messaggio_finale = new epson.ePOSBuilder();
                                                                                var concomitanze = new epson.ePOSBuilder();
                                                                                var parola_conto = comanda.lang_stampa[11];
                                                                                builder.addTextFont(builder.FONT_A);
                                                                                builder.addTextAlign(builder.ALIGN_CENTER);

                                                                                if (settaggi_profili.Field34.trim().length > 0) {
                                                                                    switch (settaggi_profili.Field35) {
                                                                                        case "G":
                                                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                            break;
                                                                                        case "N":
                                                                                        default:
                                                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                                    }
                                                                                    builder.addText(settaggi_profili.Field34 + '\n');
                                                                                    if (settaggi_profili.Field36 === 'true') {
                                                                                        builder.addFeedLine(1);
                                                                                    }
                                                                                }


                                                                                if (settaggi_profili.Field37.trim().length > 0) {
                                                                                    switch (settaggi_profili.Field38) {
                                                                                        case "G":
                                                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                            break;
                                                                                        case "N":
                                                                                        default:
                                                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                                    }
                                                                                    builder.addText(settaggi_profili.Field37 + '\n');
                                                                                    if (settaggi_profili.Field39 === 'true') {
                                                                                        builder.addFeedLine(1);
                                                                                    }
                                                                                }


                                                                                if (settaggi_profili.Field40.trim().length > 0) {
                                                                                    switch (settaggi_profili.Field41) {
                                                                                        case "G":
                                                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                            break;
                                                                                        case "N":
                                                                                        default:
                                                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                                    }
                                                                                    builder.addText(settaggi_profili.Field40 + '\n');
                                                                                    if (settaggi_profili.Field42 === 'true') {
                                                                                        builder.addFeedLine(1);
                                                                                    }
                                                                                }


                                                                                if (settaggi_profili.Field43.trim().length > 0) {
                                                                                    switch (settaggi_profili.Field44) {
                                                                                        case "G":
                                                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                            break;
                                                                                        case "N":
                                                                                        default:
                                                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                                    }
                                                                                    builder.addText(settaggi_profili.Field43 + '\n');
                                                                                }

                                                                                builder.addText('\n\n');


                                                                                builder.addTextSize(2, 2);

                                                                                if (comanda.tavolo !== "BANCO" && comanda.tavolo !== "BAR" && comanda.tavolo.left(7) !== "ASPORTO" && comanda.tavolo !== "TAKE AWAY")
                                                                                {


                                                                                    if (settaggi_profili.Field3 === 'true') {
                                                                                        builder.addText(parola_conto + ' - ');
                                                                                    }

                                                                                    if (solo_riepiloghi === true && comanda.tavolo.indexOf("CONTINUO") !== -1) {
                                                                                        builder.addText('COMANDA ' + n_prog + '\n');
                                                                                    } else {
                                                                                       
                                                                                        builder.addText( 'n° ' + comanda.tavolo + '\n');
                                                                                    }


                                                                                    builder.addTextFont(builder.FONT_A);
                                                                                    builder.addFeedLine(1);
                                                                                    if (settaggi_profili.Field4 === 'true') {
                                                                                         var scritta_ristampa = "";
                                                                                        if (bool_ristampa === true) {
                                                                                            scritta_ristampa = 'RISTAMPA ';
                                                                                        }
                                                                                        builder.addText(scritta_ristampa + array_dest_stampa[dest_stampa].nome_umano + '\n');
                                                                                        builder.addFeedLine(1);
                                                                                    }
                                                                                } else if (comanda.tavolo === "BANCO" || comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === "TAKE AWAY") {
                                                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                    //var numeretto = $('#popup_scelta_cliente input[name="numeretto"]').val();
                                                                                    //builder.addText('' + numeretto + '\n');

                                                                                    builder.addText('ASPORTO\n');

                                                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                                    builder.addFeedLine(1);
                                                                                } else if (comanda.tavolo === "BAR") {
                                                                                    if (settaggi_profili.Field3 === 'true') {
                                                                                        builder.addText(parola_conto + ' - ');
                                                                                    }

                                                                                    builder.addText(comanda.parcheggio + '\n');
                                                                                    builder.addTextFont(builder.FONT_A);
                                                                                    builder.addFeedLine(1);
                                                                                    if (settaggi_profili.Field4 === 'true') {
                                                                                        var scritta_ristampa = "";
                                                                                        if (bool_ristampa === true) {
                                                                                            scritta_ristampa = 'RISTAMPA ';
                                                                                        }
                                                                                        builder.addText(scritta_ristampa + array_dest_stampa[dest_stampa].nome_umano + '\n');
                                                                                        builder.addFeedLine(1);
                                                                                    }
                                                                                }

                                                                                builder.addTextAlign(builder.ALIGN_LEFT);
                                                                                if (comanda.tavolo !== "BANCO" && comanda.tavolo.left(7) !== "ASPORTO" && comanda.tavolo !== "TAKE AWAY")
                                                                                {
                                                                                    //QUI PRIMA C'ERA IL TAVOLO
                                                                                } else
                                                                                {
                                                                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                                                                    builder.addText(nome_cliente + '\n');
                                                                                    builder.addTextAlign(builder.ALIGN_LEFT);
                                                                                }

                                                                                if (comanda.tavolo === "BANCO" || comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === "TAKE AWAY")
                                                                                {
                                                                                    if (cambio_orario_consegna === true) {
                                                                                        builder.addFeedLine(1);
                                                                                        builder.addTextAlign(builder.ALIGN_CENTER);
                                                                                        builder.addText('CAMBIO ORA CONSEGNA!\n');
                                                                                        builder.addTextAlign(builder.ALIGN_LEFT);
                                                                                    }

                                                                                    if ($('#parcheggia_comanda input[name="ora_consegna"]').val().length > 0) {
                                                                                        builder.addFeedLine(1);

                                                                                        builder.addTextAlign(builder.ALIGN_CENTER);
                                                                                        builder.addTextSize(2, 2);
                                                                                        builder.addText('CONSEGNA ' + ' ' + aggZero($('#parcheggia_comanda input[name="ora_consegna"]').val(), 2) + ' : ' + aggZero($('#parcheggia_comanda input[name="minuti_consegna"]').val(), 2) + '\n');
                                                                                        builder.addTextSize(1, 1);
                                                                                        builder.addFeedLine(1);
                                                                                        builder.addTextAlign(builder.ALIGN_LEFT);
                                                                                    }
                                                                                }


                                                                                builder.addTextSize(1, 1);
                                                                                builder.addFeedLine(1);
                                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);


                                                                                if (settaggi_profili.Field5 === 'true') {
                                                                                    switch (settaggi_profili.Field30) {
                                                                                        case "G":
                                                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                            break;
                                                                                        case "N":
                                                                                        default:
                                                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                                    }
                                                                                    builder.addText(comanda.lang_stampa[109].toLowerCase().capitalize() + ': ' + comanda.operatore + '\n');
                                                                                }

                                                                                if (settaggi_profili.Field6 === 'true' || settaggi_profili.Field7 === 'true') {

                                                                                    if (settaggi_profili.Field6 === 'true') {
                                                                                        switch (settaggi_profili.Field31) {
                                                                                            case "G":
                                                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                                break;
                                                                                            case "N":
                                                                                            default:
                                                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                                        }
                                                                                        builder.addText(data.substr(0, 9).replace(/-/gi, '/') + '   ');
                                                                                    }

                                                                                    if (settaggi_profili.Field7 === 'true') {
                                                                                        switch (settaggi_profili.Field32) {
                                                                                            case "G":
                                                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                                break;
                                                                                            case "N":
                                                                                            default:
                                                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                                        }
                                                                                        builder.addText(data.substr(9, 5));
                                                                                    }

                                                                                    builder.addText('\n');
                                                                                }

                                                                                //COPERTI
                                                                                if (settaggi_profili.Field24 === 'true' && comanda.tavolo !== 'BAR') {

                                                                                    builder.addText('\n');

                                                                                    var descrizione, quantita;

                                                                                    $(comanda.intestazione_conto).find('tr:contains("COPERTI"),tr:contains("GEDECKE")').each(function () {
                                                                                        descrizione = $(this).find('td:nth-child(2)').html().replace(/&nbsp;/g, "");
                                                                                        quantita = $(this).find('td:nth-child(1)').html();
                                                                                    });

                                                                                    if (quantita === undefined || quantita === null || quantita === 'undefined')
                                                                                    {
                                                                                        quantita = 1;
                                                                                    }
                                                                                    switch (settaggi_profili.Field33) {
                                                                                        case "G":
                                                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                            break;
                                                                                        case "N":
                                                                                        default:
                                                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                                    }
                                                                                    builder.addText(comanda.lang_stampa[15].toLowerCase().capitalize() + ': ' + quantita + '\n');
                                                                                }

                                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                                builder.addTextSize(1, 1);

                                                                                var totale_quantita = 0;
                                                                                var totale_prezzo = 0.00;
                                                                                var giro_tasto_segue = 0;

                                                                                var ordered = {};
                                                                                Object.keys(array_comanda[dest_stampa]).sort().forEach(function (key) {
                                                                                    ordered[key] = array_comanda[dest_stampa][key];
                                                                                });


                                                                                for (var tasto_segue in ordered) {

                                                                                    if (giro_tasto_segue > 0) {
                                                                                        builder.addTextSize(2, 2);
                                                                                        builder.addTextAlign(builder.ALIGN_CENTER);
                                                                                        builder.addText("\nSEGUE\n");
                                                                                        builder.addTextSize(1, 1);
                                                                                    }
                                                                                    giro_tasto_segue++;


                                                                                    for (var portata in array_comanda[dest_stampa][tasto_segue]) {

                                                                                        var nome_portata = array_comanda[dest_stampa][tasto_segue][portata].val;


                                                                                        if (nome_portata !== undefined) {

                                                                                            builder.addFeedLine(1);
                                                                                            builder.addTextAlign(builder.ALIGN_CENTER);

                                                                                            if (nome_portata === "ZZZ") {
                                                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                                builder.addText(trattini("MESSAGGI"));
                                                                                            } else if (settaggi_profili.Field8 === 'true') {
                                                                                                if (settaggi_profili.Field25 === 'G') {
                                                                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                                }
                                                                                                builder.addText(trattini(nome_portata));
                                                                                            } else {
                                                                                                builder.addText("-----------------------------------------\n");
                                                                                            }
                                                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                                            builder.addTextAlign(builder.ALIGN_LEFT);


                                                                                            for (var node in array_comanda[dest_stampa][tasto_segue][portata]) {

                                                                                                var nodo = array_comanda[dest_stampa][tasto_segue][portata][node];


                                                                                                console.log("nodo comanda", nodo);

                                                                                                if (nodo !== undefined && nodo[1] !== undefined && nodo[1].length > 1) {

                                                                                                    var articolo = filtra_accenti(nodo[1]);
                                                                                                    var quantita = nodo[0];
                                                                                                    var prezzo = nodo[2];

                                                                                                    if (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(" && articolo[0] !== "=" && articolo[0] !== "." && articolo[0] !== "*") {

                                                                                                        switch (settaggi_profili.Field28) {
                                                                                                            case "D":
                                                                                                                builder.addTextSize(2, 2);
                                                                                                                articolo = articolo.slice(0, 20);
                                                                                                                break;
                                                                                                            case "G":
                                                                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                                                articolo = articolo.slice(0, 40);
                                                                                                                break;
                                                                                                            case "N":
                                                                                                            default:
                                                                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                                                                articolo = articolo.slice(0, 40);
                                                                                                        }

                                                                                                        if (nome_portata === "ZZZ") {
                                                                                                            builder.addTextAlign(builder.ALIGN_CENTER);
                                                                                                            builder.addText(articolo);
                                                                                                            builder.addTextAlign(builder.ALIGN_LEFT);
                                                                                                        } else {

                                                                                                            builder.addText(quantita);
                                                                                                            builder.addTextPosition(50);
                                                                                                            totale_quantita += parseInt(quantita);
                                                                                                            builder.addTextPosition(50);

                                                                                                            builder.addText(articolo);

                                                                                                            if (settaggi_profili.Field10 === 'true')
                                                                                                            {
                                                                                                                builder.addTextPosition(420);
                                                                                                                builder.addText('€ ' + prezzo);
                                                                                                            }
                                                                                                        }

                                                                                                        builder.addText('\n');

                                                                                                        builder.addTextSize(1, 1);
                                                                                                        builder.addTextStyle(false, false, false, builder.COLOR_1);

                                                                                                    } else {

                                                                                                        switch (settaggi_profili.Field29) {
                                                                                                            case "D":
                                                                                                                builder.addTextSize(2, 2);
                                                                                                                break;
                                                                                                            case "G":
                                                                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                                                break;
                                                                                                            case "N":
                                                                                                            default:
                                                                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                                                        }

                                                                                                        if (settaggi_profili.Field9 === 'true') {
                                                                                                            articolo = ' \t' + quantita + ' ' + articolo;
                                                                                                        } else {
                                                                                                            articolo = ' \t' + articolo;
                                                                                                        }

                                                                                                        builder.addTextPosition(50);

                                                                                                        //AGGIUNGERE PREZZO QUI
                                                                                                        builder.addText(articolo.slice(0, 40))

                                                                                                        if (settaggi_profili.Field10 === 'true')
                                                                                                        {
                                                                                                            builder.addTextPosition(420);
                                                                                                            builder.addText('€ ' + prezzo);
                                                                                                        }


                                                                                                        builder.addText('\n');


                                                                                                        builder.addTextSize(1, 1);
                                                                                                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                                                    }

                                                                                                    totale_prezzo += prezzo * quantita;



                                                                                                    if (nodo[3] !== undefined && settaggi_profili.Field11 === 'true' && (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(" && articolo[0] !== "=" && articolo[0] !== "." && articolo[0] !== "*")) {

                                                                                                        var ricetta = filtra_accenti(nodo[3]);
                                                                                                        if (ricetta.length > 1)
                                                                                                        {
                                                                                                            builder.addTextPosition(50);
                                                                                                            builder.addText('(' + ricetta + ') \n');
                                                                                                        }

                                                                                                    }
                                                                                                }
                                                                                            }




                                                                                            builder.addTextAlign(builder.ALIGN_CENTER);
                                                                                            if (settaggi_profili.Field22 === 'true') {

                                                                                                for (var dest_stampa_concomitanza in array_comanda) {
                                                                                                    if (array_dest_stampa[dest_stampa_concomitanza].nome_umano !== undefined && dest_stampa_concomitanza !== dest_stampa && array_comanda[dest_stampa_concomitanza][tasto_segue][portata] !== undefined && array_comanda[dest_stampa_concomitanza][tasto_segue][portata] !== null && typeof (array_comanda[dest_stampa_concomitanza][tasto_segue][portata]) === "object" && Object.keys(array_comanda[dest_stampa_concomitanza][tasto_segue][portata]).length > 0) {
                                                                                                        concomitanze_presenti = true;
                                                                                                        concomitanze.addTextAlign(builder.ALIGN_CENTER);
                                                                                                        concomitanze.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                                        concomitanze.addText('\n' + array_dest_stampa[dest_stampa_concomitanza].nome_umano + ' - ' + array_comanda[dest_stampa][tasto_segue][portata].val + '\n');
                                                                                                        concomitanze.addTextStyle(false, false, false, builder.COLOR_1);
                                                                                                        concomitanze.addTextAlign(builder.ALIGN_LEFT);

                                                                                                        for (var node_concomitanza in array_comanda[dest_stampa_concomitanza][tasto_segue][portata]) {

                                                                                                            var nodo = array_comanda[dest_stampa_concomitanza][tasto_segue][portata][node_concomitanza];
                                                                                                            if (nodo !== undefined && nodo[1] !== undefined && nodo[1].length > 1) {

                                                                                                                var articolo = filtra_accenti(nodo[1]);
                                                                                                                if (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(" && articolo[0] !== "=" && articolo[0] !== "." && articolo[0] !== "*") {
                                                                                                                    concomitanze.addText(quantita);
                                                                                                                    concomitanze.addTextPosition(50);
                                                                                                                } else {
                                                                                                                    articolo = ' \t' + articolo;
                                                                                                                }

                                                                                                                concomitanze.addTextPosition(50);
                                                                                                                concomitanze.addText(articolo.slice(0, 40) + '\n');
                                                                                                            }
                                                                                                        }

                                                                                                        concomitanze.addTextAlign(builder.ALIGN_LEFT);
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            builder.addText("-----------------------------------------\n");
                                                                                            builder.addTextAlign(builder.ALIGN_LEFT);



                                                                                        }
                                                                                    }//FINE CICLO PORTATA
                                                                                }//FINE CICLO TASTO SEGUE

                                                                                //SETTAGGI DEL MESSAGGIO FINALE

                                                                                if (settaggi_profili.Field14 === 'G')
                                                                                    messaggio_finale.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                else if (settaggi_profili.Field14 === 'N')
                                                                                    messaggio_finale.addTextStyle(false, false, false, builder.COLOR_1);

                                                                                if (settaggi_profili.Field15 === 'x--')
                                                                                    messaggio_finale.addTextAlign(builder.ALIGN_LEFT);
                                                                                else if (settaggi_profili.Field15 === '-x-')
                                                                                    messaggio_finale.addTextAlign(builder.ALIGN_CENTER);
                                                                                else if (settaggi_profili.Field15 === '--x')
                                                                                    messaggio_finale.addTextAlign(builder.ALIGN_RIGHT);

                                                                                if (settaggi_profili.Field13.trim().length > 0)
                                                                                    messaggio_finale.addText(settaggi_profili.Field13 + '\n');

                                                                                if (settaggi_profili.Field26 === 'true')
                                                                                    messaggio_finale.addFeedLine(1);




                                                                                if (settaggi_profili.Field17 === 'G')
                                                                                    messaggio_finale.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                else if (settaggi_profili.Field17 === 'N')
                                                                                    messaggio_finale.addTextStyle(false, false, false, builder.COLOR_1);

                                                                                if (settaggi_profili.Field18 === 'x--')
                                                                                    messaggio_finale.addTextAlign(builder.ALIGN_LEFT);
                                                                                else if (settaggi_profili.Field18 === '-x-')
                                                                                    messaggio_finale.addTextAlign(builder.ALIGN_CENTER);
                                                                                else if (settaggi_profili.Field18 === '--x')
                                                                                    messaggio_finale.addTextAlign(builder.ALIGN_RIGHT);

                                                                                if (settaggi_profili.Field16.trim().length > 0)
                                                                                    messaggio_finale.addText(settaggi_profili.Field16 + '\n');

                                                                                if (settaggi_profili.Field27 === 'true')
                                                                                    messaggio_finale.addFeedLine(1);



                                                                                if (settaggi_profili.Field20 === 'G')
                                                                                    messaggio_finale.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                else if (settaggi_profili.Field20 === 'N')
                                                                                    messaggio_finale.addTextStyle(false, false, false, builder.COLOR_1);

                                                                                if (settaggi_profili.Field21 === 'x--')
                                                                                    messaggio_finale.addTextAlign(builder.ALIGN_LEFT);
                                                                                else if (settaggi_profili.Field21 === '-x-')
                                                                                    messaggio_finale.addTextAlign(builder.ALIGN_CENTER);
                                                                                else if (settaggi_profili.Field21 === '--x')
                                                                                    messaggio_finale.addTextAlign(builder.ALIGN_RIGHT);

                                                                                if (settaggi_profili.Field19.trim().length > 0)
                                                                                    messaggio_finale.addText(settaggi_profili.Field19 + '\n');

                                                                                //FINE SETTAGGI MESSAGGIO FINALE

                                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                builder.addFeedLine(1);

                                                                                if (settaggi_profili.Field12 === 'true') {
                                                                                    builder.addText(totale_quantita);
                                                                                    builder.addTextPosition(50);
                                                                                    builder.addText('articoli' + '               '); //quantita
                                                                                }

                                                                                if (settaggi_profili.Field10 === 'true')
                                                                                {
                                                                                    builder.addText("Totale: ");
                                                                                    builder.addTextPosition(415);
                                                                                    builder.addText('€ ' + parseFloat(totale_prezzo).toFixed(2)); //quantita
                                                                                }

                                                                                builder.addText('\n\n');



                                                                                //METTERE SOTTO LE CONCOMITANZE E CON SETTAGGIO
                                                                                if (settaggi_profili.Field108 === 'true' && (comanda.tavolo !== "BANCO" && comanda.tavolo !== "BAR" ))
                                                                                {
                                                                                    messaggio_finale.addTextStyle(false, false, false, builder.COLOR_1);
                                                                                    messaggio_finale.addFeedLine(1);
                                                                                    messaggio_finale.addTextSize(2, 2);
                                                                                    messaggio_finale.addTextAlign(builder.ALIGN_CENTER);
                                                                                    messaggio_finale.addText('n° ' + comanda.tavolo + '\n');
                                                                                    messaggio_finale.addTextFont(builder.FONT_A);

                                                                                }

                                                                                //AGGIUNTO IL 19-01-2017 PER EVITARE DI MOSTRARE LA SCRITTA CONCOMITANZE SE NON CE NE SONO
                                                                                if (settaggi_profili.Field22 === 'true' && concomitanze_presenti === true) {

                                                                                    console.log("OGGETTO CONCOMITANZE", array_comanda);

                                                                                    if (Object.keys(array_comanda).length > 0) {

                                                                                        builder.addTextSize(1, 1);
                                                                                        builder.addTextStyle(false, false, true, builder.COLOR_1);

                                                                                        builder.addTextAlign(builder.ALIGN_CENTER);

                                                                                        builder.addText("-----------------------------------------\n");
                                                                                        builder.addText('CONCOMITANZE\n');

                                                                                        builder.addTextAlign(builder.ALIGN_LEFT);
                                                                                    }
                                                                                }

                                                                                
                                                                                builder.addTextAlign(builder.ALIGN_LEFT);
                                                                                var testo_concomitanze = '';

                                                                                //19-01-2017 CAMBIATO OR CON AND, PERCHE' TUTTE DUE LE CONDIZIONI DEVONO ESSERE VERE
                                                                                //SE E' VERA SOLO UNA DELLE DUE NON VUOL DIRE NIENTE
                                                                                if (concomitanze_presenti === true && settaggi_profili.Field22 === 'true')
                                                                                {
                                                                                    testo_concomitanze = '<feed line="0"/>' + concomitanze.toString().substr(72).slice(0, -13);
                                                                                }

                                                                                var messaggio_finale = '<feed line="1"/>' + messaggio_finale.toString().substr(72).slice(0, -13);

                                                                                var request = builder.toString().slice(0, -13) + testo_concomitanze + messaggio_finale + '<feed line="2"/><cut type="feed"/></epos-print>';
                                                                                console.log("REQUEST", request);
                                                                                var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
                                                                                //CAMBIANDO QUESTO PARAMETRO CAMBIA L'IP DI STAMPA (quindi si potrebbe prendere da comanda.nome_stampante[num].ip
                                                                                //SI POTREBBE TESTARE LA DIMENSIONE CON comanda.nome_stampante[num].dimensione
                                                                                //E SE E' O NO FISCALE CON IL PARAMETRO comanda.nome_stampante[num].fiscale

                                                                                var url = "";
                                                                                var url_alternativo = "";
                                                                                if (array_dest_stampa[dest_stampa].intelligent === "SDS") {
                                                                                    url = 'http://' + array_dest_stampa[dest_stampa].ip + ':8000/SPOOLER?stampante=' + array_dest_stampa[dest_stampa].devid_nf;
                                                                                } else {
                                                                                    url = 'http://' + array_dest_stampa[dest_stampa].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                                                                                    if (array_dest_stampa[dest_stampa].ip_alternativo !== undefined && array_dest_stampa[dest_stampa].ip_alternativo !== "") {
                                                                                        url_alternativo = 'http://' + array_dest_stampa[dest_stampa].ip_alternativo + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                                                                                    }
                                                                                }

                                                                                if (settaggi_profili.Field106 === 'true') {
                                                                                    preconto(function (cb) {

                                                                                        if (cb[3] === 'true') {
                                                                                            aggiungi_spool_stampa(cb[0], cb[1], cb[2]);
                                                                                        }
                                                                                        aggiungi_spool_stampa(cb[0], cb[1], cb[2]);

                                                                                        if (settaggi_profili.Field23 === 'true') {
                                                                                            var url2 = 'http://' + comanda.intelligent_non_fiscale + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                                                                                            //LO STAMPA NELLA STAMPANTE PRINCIPALE
                                                                                            aggiungi_spool_stampa(url2, soap, array_dest_stampa[dest_stampa].nome_umano);
                                                                                        }

                                                                                        aggiungi_spool_stampa(url, soap, array_dest_stampa[dest_stampa].nome_umano);

                                                                                    });
                                                                                } else {
                                                                                    if (settaggi_profili.Field23 === 'true') {
                                                                                        aggiungi_spool_stampa(url, soap, array_dest_stampa[dest_stampa].nome_umano, undefined, undefined, url_alternativo);
                                                                                    }
                                                                                    aggiungi_spool_stampa(url, soap, array_dest_stampa[dest_stampa].nome_umano, undefined, undefined, url_alternativo);
                                                                                }

                                                                            } else if (array_dest_stampa[dest_stampa].fiscale.toUpperCase() === 'S') {

                                                                                var corpo = '';
                                                                                var data = comanda.funzionidb.data_attuale();
                                                                                corpo += '<printerNonFiscal>\n\
                                            <beginNonFiscal  operator="1" />';
                                                                                corpo += '<printNormal  operator="1" font="1" data="---' + comanda.lang_stampa[68].toUpperCase() + ' N. ' + comanda.tavolo + '---" />';
                                                                                corpo += '<printNormal  operator="1" font="1" data="---' + array_dest_stampa[dest_stampa].nome_umano + '---" />';
                                                                                corpo += '<printNormal  operator="1" font="1" data=" " />';
                                                                                corpo += '<printNormal  operator="1" font="1" data="---' + data + '---" />';
                                                                                corpo += '<printNormal  operator="1" font="1" data=" " />';
                                                                                corpo += '<printNormal  operator="1" font="1" data="---' + comanda.operatore + '---" />';
                                                                                corpo += '<printNormal  operator="1" font="1" data=" " />';
                                                                                var totale_quantita = 0;
                                                                                for (var portata in array_comanda[dest_stampa]) {
                                                                                    if (array_comanda[dest_stampa][portata].val !== undefined) {

                                                                                        corpo += '<printNormal  operator="1" font="1" data="---' + array_comanda[dest_stampa][portata].val + '---" />';
                                                                                        corpo += '<printNormal  operator="1" font="1" data=" " />';
                                                                                        for (var node in array_comanda[dest_stampa][portata]) {

                                                                                            var nodo = array_comanda[dest_stampa][portata][node];
                                                                                            if (nodo !== undefined && nodo[1] !== undefined && nodo[1].length > 1) {

                                                                                                var articolo = filtra_accenti(nodo[1]);
                                                                                                var quantita = nodo[0];
                                                                                                if (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(" && articolo[0] !== "=" && articolo[0] !== "." && articolo[0] !== "*") {
                                                                                                    corpo += '<printNormal  operator="1" font="1" data="' + quantita + ' ' + articolo + '" />';
                                                                                                    totale_quantita += parseInt(quantita);
                                                                                                } else {
                                                                                                    corpo += '<printNormal  operator="1" font="1" data="' + articolo + '" />';
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }

                                                                                corpo += '<printNormal  operator="1" font="1" data="' + totale_quantita + '" />';
                                                                                concomitanze += '<printNormal  operator="1" font="1" data="\n" />';
                                                                                concomitanze += '<printNormal  operator="1" font="1" data="\nCONCOMITANZE:\n" />';
                                                                                for (var dest_stampa_concomitanza in array_comanda) {
                                                                                    if (array_dest_stampa[dest_stampa_concomitanza].nome_umano !== undefined && dest_stampa_concomitanza !== dest_stampa && array_comanda[dest_stampa_concomitanza][portata] !== undefined && array_comanda[dest_stampa_concomitanza][portata] !== null && typeof (array_comanda[dest_stampa_concomitanza][portata]) === "object" && Object.keys(array_comanda[dest_stampa_concomitanza][portata]).length > 0) {
                                                                                        concomitanze_presenti = true;
                                                                                        console.log("CONCOMITANZE: " + dest_stampa + " P: " + portata, array_comanda[dest_stampa_concomitanza][portata], Object.keys(array_comanda[dest_stampa_concomitanza][portata]).length);
                                                                                        concomitanze += '<printNormal  operator="1" font="1" data="\n' + array_dest_stampa[dest_stampa_concomitanza].nome_umano + ' - ' + array_comanda[dest_stampa][portata].val + '\n" />';
                                                                                        for (var node_concomitanza in array_comanda[dest_stampa_concomitanza][portata]) {

                                                                                            var nodo = array_comanda[dest_stampa_concomitanza][portata][node_concomitanza];
                                                                                            if (nodo !== undefined && nodo[1] !== undefined && nodo[1].length > 1) {

                                                                                                var articolo = filtra_accenti(nodo[1]);
                                                                                                var quantita = nodo[0];
                                                                                                if (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(" && articolo[0] !== "=" && articolo[0] !== "." && articolo[0] !== "*") {
                                                                                                    concomitanze += '<printNormal  operator="1" font="1" data="' + quantita + ' ' + articolo + '" />';
                                                                                                } else {
                                                                                                    concomitanze += '<printNormal  operator="1" font="1" data="' + articolo + '" />';
                                                                                                }

                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }

                                                                                var fine = '<endNonFiscal  operator="1" />\n\
                                            </printerNonFiscal>';
                                                                                var url = 'http://' + array_dest_stampa[dest_stampa].ip + '/cgi-bin/fpmate.cgi?devid=local_printer&timeout=30000';
                                                                                var soap = '';
                                                                                if (concomitanze_presenti === true) {
                                                                                    soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + corpo + concomitanze + fine + '</s:Body></s:Envelope>';
                                                                                } else
                                                                                {
                                                                                    soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + corpo + fine + '</s:Body></s:Envelope>';
                                                                                }

                                                                                aggiungi_spool_stampa(url, soap, array_dest_stampa[dest_stampa].nome_umano);
                                                                                break;
                                                                            }

                                                                    }
                                                                }

                                                                //---FINE COMANDA INTELLIGENT---//

                                                                if (comanda.tavolo === "BANCO" || comanda.tavolo == "BAR" || comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === "TAKE AWAY") {

                                                                    comanda.funzionidb.conto_attivo();

                                                                }


                                                                var _testo_query;
                                                                if (comanda.tavolo.left(7) !== "ASPORTO" && comanda.tavolo !== "TAKEAWAY" && comanda.tavolo !== "TAKE AWAY") {
                                                                    testo_query = "update comanda set stampata_sn='s',data_stampa='" + data + "',ora_stampa='" + ora + "',fiscalizzata_sn='c' where comanda.stato_record='ATTIVO' and comanda.ntav_comanda='" + comanda.tavolo + "'";
                                                                } else
                                                                {
                                                                    testo_query = "update comanda set data_stampa='" + data + "',ora_stampa='" + ora + "',fiscalizzata_sn='c' where comanda.stato_record='ATTIVO' and comanda.ntav_comanda='" + comanda.tavolo + "'";
                                                                }
                                                                comanda.sock.send({tipo: "comanda_stampata", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                                                                comanda.sincro.query(testo_query, function () {
                                                                    //CHIUSURA
                                                                    //comanda.sincro.query_cassa();
                                                                    if (solo_riepiloghi === true && comanda.tavolo.indexOf("CONTINUO") !== -1) {
                                                                        comanda.funzionidb.conto_attivo(function () {
                                                                            //seleziona_metodo_pagamento('SCONTRINO FISCALE');
                                                                            stampa_scontrino('SCONTRINO FISCALE', undefined, n_prog);
                                                                        });
                                                                    } else if (torna_a_tavolo !== false && (comanda.licenza === 'mattia131' || (comanda.tavolo !== "BANCO" && comanda.tavolo !== "BAR" && comanda.tavolo.left(7) !== "ASPORTO" && comanda.tavolo !== "TAKEAWAY" && comanda.tavolo !== "TAKE AWAY"))) {
                                                                        if (comanda.tavolo === "BAR" || comanda.tavolo.indexOf("CONTINUO") !== -1)
                                                                        {
                                                                            comanda.funzionidb.conto_attivo();
                                                                        } else {
                                                                            btn_tavoli("comanda");
                                                                        }
                                                                    } else
                                                                    {
                                                                        comanda.funzionidb.conto_attivo();
                                                                    }

                                                                    //Così sincronizza subito le comande stampate col server centrale
                                                                    if (comanda.array_dbcentrale_mancanti.length > 0) {
                                                                        comanda.sincro.query_cassa();
                                                                    }

                                                                });

                                                                if (settaggi_profili.Field106 === 'true') {
                                                                    var testo_query = "update tavoli SET occupato='0',colore='4',ora_apertura_tavolo = ( CASE WHEN ( ora_apertura_tavolo = '-' OR ora_apertura_tavolo = 'null' OR ora_apertura_tavolo = ''  OR ora_apertura_tavolo = NULL ) THEN '" + ora + "' ELSE ora_apertura_tavolo END ), ora_ultima_comanda = (  CASE WHEN ( ora_apertura_tavolo = 'null'  OR ora_apertura_tavolo = '' OR ora_apertura_tavolo = '-'  OR ora_apertura_tavolo = NULL ) THEN ora_ultima_comanda ELSE '" + ora + "' END )  where numero='" + comanda.tavolo + "';";
                                                                } else {
                                                                    var testo_query = "update tavoli SET occupato='0',colore='0',ora_apertura_tavolo = ( CASE WHEN ( ora_apertura_tavolo = '-' OR ora_apertura_tavolo = 'null' OR ora_apertura_tavolo = ''  OR ora_apertura_tavolo = NULL ) THEN '" + ora + "' ELSE ora_apertura_tavolo END ), ora_ultima_comanda = (  CASE WHEN ( ora_apertura_tavolo = 'null'  OR ora_apertura_tavolo = '' OR ora_apertura_tavolo = '-'  OR ora_apertura_tavolo = NULL ) THEN ora_ultima_comanda ELSE '" + ora + "' END )  where numero='" + comanda.tavolo + "';";
                                                                }

                                                                //RALLENTA L'ENTRATA DI DATI NEL SOCKET ONDE EVITARE INTERFERENZE

                                                                comanda.sock.send({tipo: "comanda_stampata", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                                                                comanda.sincro.query(testo_query, function () {

                                                                    disegna_ultimo_tavolo_modificato(testo_query);
                                                                });




                                                            });
                                                        }//FINE CONDIZIONE DI STAMPA COMANDA
                                                        else if (solo_riepiloghi === true && comanda.tavolo.indexOf("CONTINUO") !== -1) {
                                                            comanda.funzionidb.conto_attivo(function () {
                                                                //seleziona_metodo_pagamento('SCONTRINO FISCALE');
                                                                stampa_scontrino('SCONTRINO FISCALE', undefined, undefined);
                                                            });
                                                        }

                                                    });
                                                });
                                            }

                                            //});
                                        }
                                    }
                                });
                            });
                        }
                    });
        }*/
//FUNZIONI PER LA COMANDA CON GLI ARTICOLI E LE VARIANTI UNITI



function dati_comanda(CallBack) {

    var testo_query = "select cod_articolo,desc_art,quantita,prezzo_un,nodo,prog_inser,contiene_variante,categoria,dest_stampa,portata,(select desc_art from comanda as c2 where stato_record='ATTIVO' and ntav_comanda='" + comanda.tavolo + "' and stampata_sn is null and c2.nodo=substr(c1.nodo,1,3)) as articolo_main from comanda as c1 where stato_record='ATTIVO' and ntav_comanda='" + comanda.tavolo + "' and posizione='CONTO' and stampata_sn is null order by articolo_main,nodo asc;";

    comanda.sincro.query(testo_query, function (risultato) {

        var conto = new Array();
        var buffer_varianti = new Array();

        risultato.forEach(function (obj) {


            var posizione = -1; //INDEX OF FOUND ELEMENT IN CONTO


            //Solo per gli articoli principali
            if (obj.desc_art[0] !== '+' && obj.desc_art[0] !== '-' && obj.desc_art[0] !== '(' && obj.desc_art[0] !== '*')
            {

                //Iteratore per vedere se ci sono articoli uguali
                conto.some(function (iterator, index, _ary) {

                    console.log(iterator.desc_art, obj.desc_art);

                    if (iterator.desc_art === obj.desc_art)
                    {

                        posizione = index;

                        return true;

                    }

                });

                console.log("POSIZIONE TROVATA: ", posizione);

                //FINE ITERATORE

                console.log(posizione);

                if (posizione !== -1)
                {
                    conto[posizione].quantita = (parseInt(conto[posizione].quantita) + parseInt(obj.quantita)).toString();
                } else
                {
                    conto.push(obj);
                }
            }
            //Solo per le varianti
            else
            {


                //Se la variante ha lo stesso appartiene allo stesso articolo di quella di prima la unisce nella stessa riga
                //NB Quella di prima perÃ² dev'essere una variante
                if (conto[conto.length - 1] !== undefined && conto[conto.length - 1].nodo !== undefined && conto[conto.length - 1].nodo.length > 3 && obj.nodo.substr(0, 3) === conto[conto.length - 1].nodo.substr(0, 3))
                {
                    if (conto[conto.length - 1].desc_art.length >= 25) {
                        conto[conto.length - 1].desc_art += '\n\t\t' + obj.desc_art;
                    } else
                    {
                        conto[conto.length - 1].desc_art += ' ' + obj.desc_art;
                    }

                } else
                {
                    conto.push(obj);
                }
            }

        });

        var conto_varianti_perfezionate = new Array();



        conto.forEach(function (obj) {

            console.log(obj);

            var posizione = -1; //INDEX OF FOUND ELEMENT IN CONTO

            //Solo per gli articoli principali
            if (obj.desc_art[0] !== '+' && obj.desc_art[0] !== '-' && obj.desc_art[0] !== '(' && obj.desc_art[0] !== '*')
            {

                //Iteratore per vedere se ci sono articoli uguali
                conto_varianti_perfezionate.some(function (iterator, index, _ary) {

                    console.log(iterator.desc_art, obj.desc_art);

                    if (iterator.desc_art === obj.desc_art)
                    {

                        posizione = index;

                        return true;

                    }

                });

                console.log("POSIZIONE TROVATA: ", posizione);

                //FINE ITERATORE

                console.log(posizione);

                if (posizione !== -1)
                {
                    conto_varianti_perfezionate[posizione].quantita = (parseInt(conto_varianti_perfezionate[posizione].quantita) + parseInt(obj.quantita)).toString();
                } else
                {
                    conto_varianti_perfezionate.push(obj);
                }
            } else
            {
                //Se la variante ha lo stesso nome dell' articolo precedente aggiunge solo la quantitÃ 

                if (conto_varianti_perfezionate[conto_varianti_perfezionate.length - 1] !== undefined && conto_varianti_perfezionate[conto_varianti_perfezionate.length - 1].desc_art !== undefined && obj.desc_art === conto_varianti_perfezionate[conto_varianti_perfezionate.length - 1].desc_art)
                {
                    conto_varianti_perfezionate[conto_varianti_perfezionate.length - 1].quantita = (parseInt(conto_varianti_perfezionate[conto_varianti_perfezionate.length - 1].quantita) + parseInt(obj.quantita)).toString();
                } else
                {
                    conto_varianti_perfezionate.push(obj);
                }
            }

        });


        console.log(conto_varianti_perfezionate);

        if (typeof CallBack === "function") {
            CallBack(conto_varianti_perfezionate);
        }

    });

}

