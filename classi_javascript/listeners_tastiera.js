var listener_tastiera_coperti = function () {
    $('#atp_testo_n').off();

    $("#tastiera_numeri .accetta").attr("onclick", "aggiungi_coperti($('#atp_testo_n').val())");
    $('#atp_testo_n').val("");
    $('#tastiera_lettere').hide();
    $('#tastiera_numeri').show();

    $("#tasto_punto_numeri").show();
    $('#tastiera_numeri').show();
    $('#tastiera_numeri').css("left", comanda.left_tastierino + "%");
    $('#tasto_dopo_punto_numeri').css("margin-left", "0");

    /* BUG */
    try {
        $(document).off('mouseup', hide_coperti);
    } catch (e) {
    }
    $(document).on('mouseup', hide_coperti);

};

function hide_coperti(e)
{
    var container = $("#qta_menu_fisso,#tastiera_numeri,input");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0)
    {
        $("#qta_menu_fisso,#tastiera_numeri").hide();
    }
}

var listener_tastiera_paganti = function () {
    $('#atp_testo_n').off();


    $("#tastiera_numeri .accetta").attr("onclick", "aggiungi_paganti($('#atp_testo_n').val())");
    $('#atp_testo_n').val("");
    $('#tastiera_lettere').hide();
    $('#tastiera_numeri').show();

    $("#tasto_punto_numeri").show();
    $('#tastiera_numeri').show();
    $('#tastiera_numeri').css("left", comanda.left_tastierino + "%");
    $('#tasto_dopo_punto_numeri').css("margin-left", "0");

    /* BUG */
    try {
        $(document).off('mouseup', hide_paganti);
    } catch (e) {
    }
    $(document).on('mouseup', hide_paganti);

};

function hide_paganti(e)
{
    var container = $("#qta_menu_fisso,#tastiera_numeri,input");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0)
    {
        $("#qta_menu_fisso,#tastiera_numeri").hide();
    }
}

var listener_tastiera_lettere = function (event) {

    $('#atp_testo_n').off();
    var target = event.target;

    $("#tastiera_numeri .accetta").attr("onclick", "");

    $('#atp_testo_n').val("");
    $('#tastiera_lettere').hide();
    $('#tastiera_numeri').show();

    if ($(target).hasClass("ora_consegna")) {
        $("#tasto_punto_numeri").hide();
        $('#tastiera_numeri').css("left", "34.5%");
        $('#tasto_dopo_punto_numeri').css("margin-left", "5.7vw");
    } else
    {
        $("#tasto_punto_numeri").show();
        $('#tastiera_numeri').show();
        $('#tastiera_numeri').css("left", comanda.left_tastierino + "%");
        $('#tasto_dopo_punto_numeri').css("margin-left", "0");

    }

    $('#atp_testo_n').change(function () {

        if ($(target).hasClass("ora_consegna")) {
            if ($('#atp_testo_n').val().length === 2)
            {
                $('#atp_testo_n').val($('#atp_testo_n').val() + ":");
            }
        }

        $(target).val($('#atp_testo_n').val());
        $(target).trigger('change');

    });

    /* BUG */
    try {
        $(document).off('mouseup', hide_lettere);
    } catch (e) {
    }
    $(document).on('mouseup', hide_lettere);


};

function hide_lettere(e)
{
    var container = $("#qta_menu_fisso,#tastiera_numeri,input");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0)
    {
        $("#qta_menu_fisso,#tastiera_numeri").hide();
    }
}

var listener_tastiera_numeri = function (event) {

    $('#atp_testo_l').off();

    var target = event.target;

    maxLength_campo = target.maxLength;

    $('#atp_testo_l').val("");
    $('#atp_testo_l').val($(target).val());
    c = $(target).val().length;

    vedi_tastiera_lettere();

    if ($(target).attr("type") === "password") {
        $("#tastiera_lettere .anteprima_testo").hide();
    } else
    {
        $("#tastiera_lettere .anteprima_testo").show();
    }

    $('#tastiera_numeri').hide();
    $('#tastiera_lettere').show();

    if ($(target).hasClass("parte_numerica")) {

        vedi_tastiera_numeri();

    } else {
        vedi_tastiera_lettere();
    }

    $('#atp_testo_l').change(function () {
        $(target).val($('#atp_testo_l').val());
        $(target).trigger('change');
    });

    /* BUG */
    try {
        $(document).off('mouseup', hide_numeri);
    } catch (e) {
    }
    $(document).on('mouseup', hide_numeri);



};

function hide_numeri(e)
{
    var container = $("#tastiera_lettere,input");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0)
    {
        $("#tastiera_lettere").hide();
    }
}

function listen_tastierino_num() {



    if (comanda.tastiera === true) {
        var val_iniziale;
        //IN REALTA QUESTA E' LA TASTIERA DEI NUMERI

        try {
            $(document).off('touchend', listener_tastiera_lettere);
        } catch (e) {
            console.log(e);
        }


        $(document).on('touchend', '.kb_num:not([readonly]):not([disabled]):not(.escludi_tastiera):not(.editor_settaggi input):not(#atp_testo_l):not(#atp_testo)', listener_tastiera_lettere);

        if (comanda.mobile === true) {
            $('.kb_num:not([readonly]):not([disabled]):not(.escludi_tastiera):not(.editor_settaggi input):not(#atp_testo_l):not(#atp_testo)').focus(function () {
                $(this).blur();
            });
        }

    }

    $('input').not('.kb_num').removeAttr('onfocus');
    //IN REALTA QUESTA E' LA TASTIERA DELLE LETTERE
    if (comanda.palmare !== true && comanda.virtual_keyboard === true) {

        try {
            $(document).off('touchend', listener_tastiera_numeri);
        } catch (e) {
            console.log(e);
        }

        //NOT:DISABLED DEVE RESTARE PER EVITARE LA MODIFICA DESCRIZIONE E ALTRI CAMPI SE C'E' IL SETTAGGIO CHE NON LO PERMETTE
        $(document).on('touchend', 'input:not([disabled]):not(.kb_num):not([readonly]):not([type="checkbox"]):not(.escludi_tastiera):not(.editor_settaggi input):not(#atp_testo_l):not(#atp_testo)', listener_tastiera_numeri);

        if (comanda.mobile === true) {
            $('input:not(.kb_num):not([disabled]):not([readonly]):not([type="checkbox"]):not(.escludi_tastiera):not(.editor_settaggi input):not(#atp_testo_l):not(#atp_testo)').focus(function () {
                $(this).blur();
            });
        }

    }

}

