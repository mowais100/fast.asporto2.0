function conferma_metodo_pagamento(bypass, gia_infornato) {

    var testa = new Object();
    testa.totale = parseFloat($(comanda.totale_scontrino).html()).toFixed(2);
    testa.contanti = 0;
    testa.pos = 0;
    testa.droppay = 0;
    testa.bancomat = 0;
    testa.cartadicredito = 0;
    testa.cartadicredito2 = 0;
    testa.cartadicredito3 = 0;
    testa.buoniacquisto = 0;
    testa.buonipasto = 0;
    testa.nonpagato = 0;
    testa.non_riscosso_servizi = 0;
    if (comanda.compatibile_xml === true || bypass === true) {
        testa.contanti = testa.totale;
        /*stampa_scontrino_asporto(comanda.tipologia_ricevuta, testa);*/
        stampa_scontrino(comanda.tipologia_ricevuta, testa);

        setTimeout(function () {
            if (gia_infornato !== true) {
                inforna(true);
            }
            if ($('.scelta_buoni__tipo_pagamento_restante')[0] !== undefined) {
                $('.scelta_buoni__tipo_pagamento_restante')[0].selectedIndex = 0;
            }
            $('.scelta_buoni__restante').html(0);
            $('[name="scelta_buoni__quantita_buoni"]').val(1);
            $('.scelta_buoni__ammontare_buono').html(0);
            $('#scelta_buoni [name="scelta_buoni__buono_spesa"]').val(0);
            if ($('.scelta_buoni__tipo_pagamento_restante')[0] !== undefined) {
                $('.scelta_buoni__tipo_pagamento_restante')[0].selectedIndex = 0;
            }
            buoni_memorizzati = [];
        }, 2000);
    } else if ($('#popup_metodo_pagamento_da_pagare:visible').html() !== '0.00' && $('#popup_metodo_pagamento_da_pagare:visible').html() !== null && $('#popup_metodo_pagamento_da_pagare:visible').html() !== undefined) {
        alert("Mancano ancora da pagare " + " € " + $('#popup_metodo_pagamento_da_pagare').html());
    } else
    {

        if ($('#scelta_buoni:visible').length === 1) {

            var buoni_spesa = $('#scelta_buoni [name="scelta_buoni__buono_spesa"]').val();
            if (parseFloat(buoni_spesa) > 0) {
                testa.buoniacquisto = parseFloat(buoni_spesa);
            }

            testa.totale_buoni_pasto = 0;
            testa.numero_buoni = 0;
            testa.nome_buono = '';
            testa.buoni_pasto = '0';
            testa.numero_buoni_pasto = '0';
            testa.nome_buono_2 = '';
            testa.buoni_pasto_2 = '0';
            testa.numero_buoni_pasto_2 = '0';
            testa.nome_buono_3 = '';
            testa.buoni_pasto_3 = '0';
            testa.numero_buoni_pasto_3 = '0';
            testa.nome_buono_4 = '';
            testa.buoni_pasto_4 = '0';
            testa.numero_buoni_pasto_4 = '0';
            testa.nome_buono_5 = '';
            testa.buoni_pasto_5 = '0';
            testa.numero_buoni_pasto_5 = '0';
            var i = 1;
            if (buoni_memorizzati.length >= 1) {
                buoni_memorizzati.forEach(function (val, index) {
                    switch (i) {
                        case 1:

                            testa.nome_buono = val.id_circuito;
                            testa.buoni_pasto = val.importo_taglio;
                            testa.numero_buoni_pasto = val.quantita;
                            //SOLO PER USO INTERNO ALLO SCONTRINO
                            testa.totale_buoni_pasto += parseFloat(val.importo_taglio) * parseInt(val.quantita);
                            testa.numero_buoni += parseInt(val.quantita);
                            break;
                        case 2:
                            testa.nome_buono_2 = val.id_circuito;
                            testa.buoni_pasto_2 = val.importo_taglio;
                            testa.numero_buoni_pasto_2 = val.quantita;
                            //SOLO PER USO INTERNO ALLO SCONTRINO
                            testa.totale_buoni_pasto += parseFloat(val.importo_taglio) * parseInt(val.quantita);
                            testa.numero_buoni += parseInt(val.quantita);
                            break;
                        case 3:
                            testa.nome_buono_3 = val.id_circuito;
                            testa.buoni_pasto_3 = val.importo_taglio;
                            testa.numero_buoni_pasto_3 = val.quantita;
                            //SOLO PER USO INTERNO ALLO SCONTRINO
                            testa.totale_buoni_pasto += parseFloat(val.importo_taglio) * parseInt(val.quantita);
                            testa.numero_buoni += parseInt(val.quantita);
                            break;
                        case 4:
                            testa.nome_buono_4 = val.id_circuito;
                            testa.buoni_pasto_4 = val.importo_taglio;
                            testa.numero_buoni_pasto_4 = val.quantita;
                            //SOLO PER USO INTERNO ALLO SCONTRINO
                            testa.totale_buoni_pasto += parseFloat(val.importo_taglio) * parseInt(val.quantita);
                            testa.numero_buoni += parseInt(val.quantita);
                            break;
                        case 5:
                            testa.nome_buono_5 = val.id_circuito;
                            testa.buoni_pasto_5 = val.importo_taglio;
                            testa.numero_buoni_pasto_5 = val.quantita;
                            //SOLO PER USO INTERNO ALLO SCONTRINO
                            testa.totale_buoni_pasto += parseFloat(val.importo_taglio) * parseInt(val.quantita);
                            testa.numero_buoni += parseInt(val.quantita);
                            break;
                    }
                    i++;
                });
            }

            var totale_conto = parseFloat($(comanda.totale_scontrino).html());
            var restante = parseFloat(totale_conto - testa.totale_buoni_pasto - testa.buoniacquisto).toFixed(2);
            if (restante > 0) {

                var tipo_pagamento__restante = $('.scelta_buoni__tipo_pagamento_restante option:selected').val();
                if (tipo_pagamento__restante !== null) {

                    switch (tipo_pagamento__restante) {
                        case "CONTANTI":
                            var resto = $('#popup_metodo_pagamento_resto').html();
                            if (resto === undefined) {
                                resto = "0";
                            }
                            testa.contanti = restante - parseFloat(resto);
                            break;
                        case "POS":
                            testa.pos = restante;
                            break;
                        case "DROPPAY":
                            testa.droppay = restante;
                            break;
                        case "BANCOMAT":
                            testa.bancomat = restante;
                            break;
                        case "CARTA DI CREDITO":
                            testa.cartadicredito = restante;
                            break;
                        case "CARTA DI CREDITO 2":
                            testa.cartadicredito2 = restante;
                            break;
                        case "CARTA DI CREDITO 3":
                            testa.cartadicredito3 = restante;
                            break;

                    }

                } else
                {
                    alert("Devi scegliere un metodo di pagamento per l'importo restante.");
                    return false;
                }
            }



            console.log("DATI TESTA TEMPORANEI", testa);
            $('#scelta_metodo_pagamento').modal('hide');
            $('#scelta_buoni').modal('hide');
            /*stampa_scontrino_asporto(comanda.tipologia_ricevuta, testa);*/
            stampa_scontrino(comanda.tipologia_ricevuta, testa);

            setTimeout(function () {
                if (gia_infornato !== true) {
                    inforna(true);
                }
                if ($('.scelta_buoni__tipo_pagamento_restante')[0] !== undefined) {
                    $('.scelta_buoni__tipo_pagamento_restante')[0].selectedIndex = 0;
                }
                $('.scelta_buoni__restante').html(0);
                $('[name="scelta_buoni__quantita_buoni"]').val(1);
                $('.scelta_buoni__ammontare_buono').html(0);
                $('#scelta_buoni [name="scelta_buoni__buono_spesa"]').val(0);
                if ($('.scelta_buoni__tipo_pagamento_restante')[0] !== undefined) {
                    $('.scelta_buoni__tipo_pagamento_restante')[0].selectedIndex = 0;
                }
                buoni_memorizzati = [];
            }, 2000);
        } else if ($('#popup_nonpagato:visible').length === 1) {

            comanda.tipologia_ricevuta = "SCONTRINO FISCALE";

            var ragione_sociale = $('#popup_nonpagato [name="ragione_sociale"]').val();
            var indirizzo = $('#popup_nonpagato [name="indirizzo"]').val();
            var numero = $('#popup_nonpagato [name="numero"]').val();
            var cap = $('#popup_nonpagato [name="cap"]').val();
            var citta = $('#popup_nonpagato [name="citta"]').val();
            var comune = $('#popup_nonpagato [name="comune"]').val();
            var provincia = $('#popup_nonpagato [name="provincia"]').val();
            var paese = $('#popup_nonpagato [name="paese"]').val();
            var cellulare = $('#popup_nonpagato [name="cellulare"]').val();
            var telefono = $('#popup_nonpagato [name="telefono"]').val();
            var fax = $('#popup_nonpagato [name="fax"]').val();
            var email = $('#popup_nonpagato [name="email"]').val();
            var codice_fiscale = $('#popup_nonpagato [name="codice_fiscale"]').val();
            var partita_iva = $('#popup_nonpagato [name="partita_iva"]').val();
            var partita_iva_estera = $('#popup_nonpagato [name="partita_iva_estera"]').val();
            //-----

            var tendina = $('#popup_nonpagato_elenco_clienti option:selected').val();


            if ((tendina === undefined || tendina.length < 1) && ragione_sociale.length > 2 && ((comanda.partita_iva_estera === true && partita_iva_estera.length > 0) || (partita_iva.length === 11 || partita_iva.length === 16))) {
                var testo_query = "SELECT id FROM clienti ORDER BY cast(id as int) DESC LIMIT 1";
                comanda.sincro.query(testo_query, function (result) {
                    var id = 0;
                    if (result[0] !== undefined)
                    {
                        id = parseInt(result[0].id) + 1;
                    }
                    testo_query = "INSERT INTO clienti (id,ragione_sociale,indirizzo,numero,cap,citta,comune,provincia,paese,cellulare,telefono,fax,email,codice_fiscale,partita_iva,iva_non_pagati,partita_iva_estera) VALUES (" + id + ",'" + ragione_sociale + "','" + indirizzo + "','" + numero + "','" + cap + "','" + comune + "','" + comune + "','" + provincia + "','" + paese + "','" + cellulare + "','" + telefono + "','" + fax + "','" + email + "','" + codice_fiscale + "','" + partita_iva + "','10','" + partita_iva_estera + "')";
                    comanda.sock.send({tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                    comanda.sincro.query(testo_query, function () {
                        testa.id_cliente = id;
                        testa.nonpagato = $(comanda.totale_scontrino).html();
                        console.log("DATI TESTA TEMPORANEI", testa);
                        $('#scelta_metodo_pagamento').modal('hide');

                        /*stampa_scontrino_asporto(comanda.tipologia_ricevuta, testa);*/
                        stampa_scontrino(comanda.tipologia_ricevuta, testa);

                        setTimeout(function () {
                            if (gia_infornato !== true) {
                                inforna(true);
                            }
                            if ($('.scelta_buoni__tipo_pagamento_restante')[0] !== undefined) {
                                $('.scelta_buoni__tipo_pagamento_restante')[0].selectedIndex = 0;
                            }
                            $('.scelta_buoni__restante').html(0);
                            $('[name="scelta_buoni__quantita_buoni"]').val(1);
                            $('.scelta_buoni__ammontare_buono').html(0);
                            $('#scelta_buoni [name="scelta_buoni__buono_spesa"]').val(0);
                            if ($('.scelta_buoni__tipo_pagamento_restante')[0] !== undefined) {
                                $('.scelta_buoni__tipo_pagamento_restante')[0].selectedIndex = 0;
                            }
                            buoni_memorizzati = [];
                        }, 2000);
                        $('#popup_nonpagato').modal('hide');
                    });
                });
            } else if ((tendina === undefined || tendina.length < 1) && ragione_sociale.length < 2 && ((comanda.partita_iva_estera === true && partita_iva_estera.length > 0) || (partita_iva.length === 11 || partita_iva.length === 16))) {
                var testo_query = "SELECT id FROM clienti ORDER BY cast(id as int) DESC LIMIT 1";
                comanda.sincro.query(testo_query, function (result) {
                    var id = 0;
                    if (result[0] !== undefined)
                    {
                        id = parseInt(result[0].id) + 1;
                    }
                    testo_query = "INSERT INTO clienti (id,ragione_sociale,indirizzo,numero,cap,citta,comune,provincia,paese,cellulare,telefono,fax,email,codice_fiscale,partita_iva,iva_non_pagati,partita_iva_estera) VALUES (" + id + ",'" + partita_iva + "','" + indirizzo + "','" + numero + "','" + cap + "','" + comune + "','" + comune + "','" + provincia + "','" + paese + "','" + cellulare + "','" + telefono + "','" + fax + "','" + email + "','" + codice_fiscale + "','" + partita_iva + "','10','" + partita_iva_estera + "')";
                    comanda.sock.send({tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                    comanda.sincro.query(testo_query, function () {
                        testa.id_cliente = id;
                        testa.nonpagato = $(comanda.totale_scontrino).html();
                        console.log("DATI TESTA TEMPORANEI", testa);
                        $('#scelta_metodo_pagamento').modal('hide');
                        /*stampa_scontrino_asporto(comanda.tipologia_ricevuta, testa);*/
                        stampa_scontrino(comanda.tipologia_ricevuta, testa);

                        setTimeout(function () {
                            if (gia_infornato !== true) {
                                inforna(true);
                            }
                            if ($('.scelta_buoni__tipo_pagamento_restante')[0] !== undefined) {
                                $('.scelta_buoni__tipo_pagamento_restante')[0].selectedIndex = 0;
                            }
                            $('.scelta_buoni__restante').html(0);
                            $('[name="scelta_buoni__quantita_buoni"]').val(1);
                            $('.scelta_buoni__ammontare_buono').html(0);
                            $('#scelta_buoni [name="scelta_buoni__buono_spesa"]').val(0);
                            if ($('.scelta_buoni__tipo_pagamento_restante')[0] !== undefined) {
                                $('.scelta_buoni__tipo_pagamento_restante')[0].selectedIndex = 0;
                            }
                            buoni_memorizzati = [];
                        }, 2000);
                        $('#popup_nonpagato').modal('hide');
                    });
                });
            } else if (tendina !== "" && ragione_sociale.length > 2 && ((comanda.partita_iva_estera === true && partita_iva_estera.length > 0) || (partita_iva.length === 11 || partita_iva.length === 16)))
            {
                testa.id_cliente = tendina;
                testa.nonpagato = $(comanda.totale_scontrino).html();
                console.log("DATI TESTA TEMPORANEI", testa);
                $('#scelta_metodo_pagamento').modal('hide');

                /*stampa_scontrino_asporto(comanda.tipologia_ricevuta, testa);*/
                stampa_scontrino(comanda.tipologia_ricevuta, testa);

                setTimeout(function () {
                    if (gia_infornato !== true) {
                        inforna(true);
                    }
                    if ($('.scelta_buoni__tipo_pagamento_restante')[0] !== undefined) {
                        $('.scelta_buoni__tipo_pagamento_restante')[0].selectedIndex = 0;
                    }
                    $('.scelta_buoni__restante').html(0);
                    $('[name="scelta_buoni__quantita_buoni"]').val(1);
                    $('.scelta_buoni__ammontare_buono').html(0);
                    $('#scelta_buoni [name="scelta_buoni__buono_spesa"]').val(0);
                    if ($('.scelta_buoni__tipo_pagamento_restante')[0] !== undefined) {
                        $('.scelta_buoni__tipo_pagamento_restante')[0].selectedIndex = 0;
                    }
                    buoni_memorizzati = [];
                }, 2000);
                $('#popup_nonpagato').modal('hide');
            } else
            {
                alert('DEVI INSERIRE I DATI DEL CLIENTE.');
            }



        } else if ($('#scelta_metodo_pagamento:visible').length === 1) {

            var premessa = new Promise(function (resolve, reject) {

                if (comanda.tipologia_ricevuta === "FATTURA") {

                    var ragione_sociale = $('#fattura [name="ragione_sociale"]').val();
                    var indirizzo = $('#fattura [name="indirizzo"]').val();
                    var numero = $('#fattura [name="numero"]').val();
                    var cap = $('#fattura [name="cap"]').val();
                    var citta = $('#fattura [name="citta"]').val();
                    var provincia = $('#fattura [name="provincia"]').val();
                    var paese = $('#fattura [name="paese"]').val();
                    var cellulare = $('#fattura [name="cellulare"]').val();
                    var telefono = $('#fattura [name="telefono"]').val();
                    var fax = $('#fattura [name="fax"]').val();
                    var email = $('#fattura [name="email"]').val();
                    var codice_fiscale = $('#fattura [name="codice_fiscale"]').val();
                    var partita_iva = $('#fattura [name="partita_iva"]').val();
                    var formato_trasmissione = $('#fattura [name="formato_trasmissione"]').prop("checked");
                    var codice_destinatario = $('#fattura [name="codice_destinatario"]').val();
                    /* non serve metterlo obbligatoriamente. Se non c'è diventa privato con 7 zeri*/
                    var regime_fiscale = $('#fattura [name="regime_fiscale"]').val();
                    var comune = $('#fattura [name="comune"]').val();
                    var nazione = $('v [name="nazione"]').val();
                    var partita_iva_estera = $('#fattura [name="partita_iva_estera"]').val();
                    var pec = $('#fattura [name="pec"]').val();
                    //-----

                    var tendina = $('#fattura_elenco_clienti option:selected').val();
                    if ((tendina === undefined || tendina.length < 1) && ragione_sociale.length > 2 && ((comanda.partita_iva_estera === true && partita_iva_estera.length > 0) || (partita_iva.length === 11 || partita_iva.length === 16))) {

                        var testo_query = "SELECT id FROM clienti ORDER BY cast(id as int) DESC LIMIT 1";
                        comanda.sincro.query(testo_query, function (result) {
                            var id = 0;
                            if (result[0] !== undefined)
                            {
                                id = parseInt(result[0].id) + 1;
                            }
                            testo_query = "INSERT INTO clienti (partita_iva_estera,formato_trasmissione,codice_destinatario,regime_fiscale,pec,comune,id_nazione,nazione,id,ragione_sociale,indirizzo,numero,cap,citta,provincia,paese,cellulare,telefono,fax,email,codice_fiscale,partita_iva,iva_non_pagati,partita_iva_estera) VALUES ('" + partita_iva_estera + "','" + formato_trasmissione + "','" + codice_destinatario + "','" + regime_fiscale + "','" + pec + "','" + comune + "','" + nazione + "','" + nazione + "'," + id + ",'" + ragione_sociale.toUpperCase() + "','" + indirizzo + "','" + numero + "','" + cap + "','" + comune + "','" + provincia + "','" + paese + "','" + cellulare + "','" + telefono + "','" + fax + "','" + email + "','" + codice_fiscale + "','" + partita_iva + "','10','" + partita_iva_estera + "')";
                            comanda.sock.send({tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                            comanda.sincro.query(testo_query, function () {
                                resolve(id);
                            });
                        });
                    } else if ((tendina === undefined || tendina.length < 1) && ragione_sociale.length < 2 && ((comanda.partita_iva_estera === true && partita_iva_estera.length > 0) || (partita_iva.length === 11 || partita_iva.length === 16))) {


                        var testo_query = "SELECT * FROM clienti WHERE partita_iva='" + partita_iva + "' ORDER BY ragione_sociale DESC LIMIT 1;";
                        comanda.sincro.query(testo_query, function (result_clienti_corrispondenti) {

                            if (result_clienti_corrispondenti[0].id) {

                                resolve(result_clienti_corrispondenti[0].id);
                            } else {

                                testo_query = "SELECT id FROM clienti ORDER BY cast(id as int) DESC LIMIT 1;";
                                comanda.sincro.query(testo_query, function (result) {
                                    var id = 0;
                                    if (result[0] !== undefined)
                                    {
                                        id = parseInt(result[0].id) + 1;
                                    }
                                    testo_query = "INSERT INTO clienti (partita_iva_estera,formato_trasmissione,codice_destinatario,regime_fiscale,pec,comune,id,ragione_sociale,indirizzo,numero,cap,citta,provincia,paese,cellulare,telefono,fax,email,codice_fiscale,partita_iva,iva_non_pagati,partita_iva_estera) VALUES ('" + partita_iva_estera + "','" + formato_trasmissione + "','" + codice_destinatario + "','" + regime_fiscale + "','" + pec + "','" + comune + "','" + nazione + "','" + nazione + "'," + id + ",'" + partita_iva + "','" + indirizzo + "','" + numero + "','" + cap + "','" + comune + "','" + provincia + "','" + paese + "','" + cellulare + "','" + telefono + "','" + fax + "','" + email + "','" + codice_fiscale + "','" + partita_iva + "','10','" + partita_iva_estera + "')";
                                    comanda.sock.send({tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                                    comanda.sincro.query(testo_query, function () {
                                        resolve(id);
                                    });
                                });
                            }
                        });
                    } else if (tendina !== "" && ragione_sociale.length > 2 && ((comanda.partita_iva_estera === true && partita_iva_estera.length > 0) || (partita_iva.length === 11 || partita_iva.length === 16))) {
                        resolve(tendina);
                    } else
                    {
                        resolve('N');
                    }

                } else if (comanda.tipologia_ricevuta.indexOf("SCONTRINO") !== -1)
                {
                    resolve('N');
                }

            });
            premessa.then(function (id_cliente) {

                testa.id_cliente = id_cliente;
                var totale_progressivo = 0;
                var contanti = $('#popup_metodo_pagamento_importo_contanti').val();
                if (parseFloat(contanti) > 0 && totale_progressivo < testa.totale) {
                    var resto = $('#popup_metodo_pagamento_resto').html();
                    testa.contanti = parseFloat(contanti) - parseFloat(resto);
                    totale_progressivo += parseFloat(contanti);
                } else if (parseFloat(contanti) > 0)
                {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai già raggiunto o superato il totale.");
                    return false;
                }

                var pos = $('#popup_metodo_pagamento_importo_pos').val();
                if (parseFloat(pos) > 0 && totale_progressivo < testa.totale) {
                    testa.pos = pos;
                    totale_progressivo += parseFloat(pos);
                } else if (parseFloat(pos) > 0)
                {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai già raggiunto o superato il totale.");
                    return false;
                }

                var droppay = $('#popup_metodo_pagamento_importo_droppay').val();
                if (parseFloat(droppay) > 0 && totale_progressivo < testa.totale) {
                    testa.droppay = droppay;
                    totale_progressivo += parseFloat(droppay);
                } else if (parseFloat(droppay) > 0)
                {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai già raggiunto o superato il totale.");
                    return false;
                }

                var bancomat = $('#popup_metodo_pagamento_importo_bancomat').val();
                if (parseFloat(bancomat) > 0 && totale_progressivo < testa.totale) {
                    testa.bancomat = bancomat;
                    totale_progressivo += parseFloat(bancomat);
                } else if (parseFloat(bancomat) > 0)
                {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai già raggiunto o superato il totale.");
                    return false;
                }

                var cartadicredito = $('#popup_metodo_pagamento_importo_cc').val();
                if (parseFloat(cartadicredito) > 0 && totale_progressivo < testa.totale) {
                    testa.cartadicredito = cartadicredito;
                    totale_progressivo += parseFloat(cartadicredito);
                } else if (parseFloat(cartadicredito) > 0)
                {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai già raggiunto o superato il totale.");
                    return false;
                }

                var cartadicredito2 = $('#popup_metodo_pagamento_importo_cc2').val();
                if (parseFloat(cartadicredito2) > 0 && totale_progressivo < testa.totale) {
                    testa.cartadicredito2 = cartadicredito2;
                    totale_progressivo += parseFloat(cartadicredito2);
                } else if (parseFloat(cartadicredito2) > 0)
                {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai giÃ Â  raggiunto o superato il totale.");
                    return false;
                }

                var cartadicredito3 = $('#popup_metodo_pagamento_importo_cc3').val();
                if (parseFloat(cartadicredito3) > 0 && totale_progressivo < testa.totale) {
                    testa.cartadicredito3 = cartadicredito3;
                    totale_progressivo += parseFloat(cartadicredito3);
                } else if (parseFloat(cartadicredito3) > 0)
                {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai giÃ Â  raggiunto o superato il totale.");
                    return false;
                }

                var monouso = $('#popup_metodo_pagamento_importo_monouso').val();
                if (parseFloat(monouso) > 0 && totale_progressivo < testa.totale) {
                    testa.monouso = monouso;
                    totale_progressivo += parseFloat(monouso);
                } else if (parseFloat(monouso) > 0)
                {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai giÃ Â  raggiunto o superato il totale.");
                    return false;
                }


                var buoni_acquisto = $('#popup_metodo_pagamento_importo_buoni_acquisto').val();
                if (parseFloat(buoni_acquisto) > 0 && totale_progressivo < testa.totale) {
                    testa.buoniacquisto = buoni_acquisto;
                    totale_progressivo += parseFloat(buoni_acquisto);
                } else if (parseFloat(buoni_acquisto) > 0)
                {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai già raggiunto o superato il totale.");
                    return false;
                }

                var non_riscosso_servizi = $('#popup_metodo_pagamento_importo_non_riscosso_servizi').val();
                if (parseFloat(non_riscosso_servizi) > 0 && totale_progressivo < testa.totale) {
                    testa.non_riscosso_servizi = non_riscosso_servizi;
                    totale_progressivo += parseFloat(non_riscosso_servizi);
                } else if (parseFloat(non_riscosso_servizi) > 0)
                {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai già raggiunto o superato il totale.");
                    return false;
                }



                console.log("DATI TESTA TEMPORANEI", testa);
                $('#scelta_metodo_pagamento').modal('hide');

                /*stampa_scontrino_asporto(comanda.tipologia_ricevuta, testa);*/
                stampa_scontrino(comanda.tipologia_ricevuta, testa);

                setTimeout(function () {

                    if (gia_infornato !== true) {
                        /* aggiunto true, true il 17/11/2020 per evitare che chieda l intestazione del cliente in caso di scontrino diretto*/
                        inforna(true, true);
                    }
                    if ($('.scelta_buoni__tipo_pagamento_restante')[0] !== undefined) {
                        $('.scelta_buoni__tipo_pagamento_restante')[0].selectedIndex = 0;
                    }
                    $('.scelta_buoni__restante').html(0);
                    $('[name="scelta_buoni__quantita_buoni"]').val(1);
                    $('.scelta_buoni__ammontare_buono').html(0);
                    $('#scelta_buoni [name="scelta_buoni__buono_spesa"]').val(0);
                    if ($('.scelta_buoni__tipo_pagamento_restante')[0] !== undefined) {
                        $('.scelta_buoni__tipo_pagamento_restante')[0].selectedIndex = 0;
                    }
                    buoni_memorizzati = [];
                }, 2000);
            });
        }
    }
}