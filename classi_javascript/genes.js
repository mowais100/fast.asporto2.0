function genCharArray(charA, charZ) {
    var a = [], i = charA.charCodeAt(0), j = charZ.charCodeAt(0);
    for (; i <= j; ++i) {
        a.push(String.fromCharCode(i));
    }
    return a;
}

function tria(genes) {

    /*prima di tutto bisogna sapere di che lettere sono composti i geni da ricercare */


    let charsArray = new Array();

    /* se ste lettere non ci sono già tra i caratteri, vanno aggiunte */

    for (let gene of genes) {
        for (let char of gene) {
            if (charsArray.indexOf(char) === -1) {
                charsArray.push(char); }
        }
    }

    //console.log(charsArray);

    /* adesso sappiamo di che lettere sono composti i geni dell'albero */

    /* inizializzo l'oggetto albero */

    let ahoCorasick = new Object();

    /* partiamo con la funzione ricorsiva */

    giro(ahoCorasick, 0, charsArray, genes, "");

    console.log(ahoCorasick);
}

function giro(oggetto, iter, charsArray, genes, path) {

    for (let char of charsArray) {

        for (let gene of genes) {

            let a = gene.substr(0, iter);
            let b = gene.substr(iter, 1);
            
            console.log(a,"-",path,"-",b,"-",char);


            /*qui qualcosa non quaglia*/
            /*dovrebbe essere che se il gene ha lo stesso path e lo stesso carattere successivo va avanti la ricorsione */

            if (a === path && b === char) {

                oggetto[char] = new Object();

                path += char;

                giro(oggetto[char], iter + 1, charsArray, genes, path);
            }

        }
    }


}


let genes = ["ab", "bc", "b", "bb", "abc", "bbcb"];
let stringa = "ababbcbcabbabcabd";


tria(genes);
