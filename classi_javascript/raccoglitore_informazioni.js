//INDICE DI GRAVITA' DELL'ERRORE
//  Errore che comporta problemi "fiscali", o riguardanti il misuratore fiscale
//  Comporta l'impossibilità assoluta a svolgere qualsiasi mansione
//  Comporta l'impossibilità di svolgere correttamente le mansioni più frequenti
//  Comporta una parziale compromissione delle funzioni
//  Si tratta di un errore che cpaita con una certa frequenza e che disturba
//  Si tratta di un errore che compare poche volte
//  E' stato un errore "una tantum"





function test_rete()
{

    var info_utente = prompt("MODULO DI SEGNALAZIONE ERRORI\n\n" + "Che cos'è successo precisamente? \n\nCerca di essere più chiaro ed esplicativo possibile,\nper facilitare l'identificazione e la risoluzione del problema.\n\nNon utilizzare termini offensivi, altrimenti l'errore non verrà preso in considerazione.");

    if (info_utente) {

        var mappa_rete = '';
        var info = new Array();

        var r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/; //http://www.regular-expressions.info/examples.html

        //SERVER
        info.push(comanda.url_server.match(r)[0]);
        mappa_rete += "SERVER CENTRALE: " + comanda.url_server + "\r\n";

        //STAMPANTi
        info.push(comanda.indirizzo_access_point.match(r)[0]);
        mappa_rete += "ACCESS POINT: " + comanda.indirizzo_access_point + "\r\n";

        info.push(comanda.intelligent_non_fiscale.match(r)[0]);
        mappa_rete += "STAMPANTE CONTO: " + comanda.intelligent_non_fiscale + "\r\n";

        //MANCA PER LA STAMPANTE FISCALE TEDESCA
        info.push(comanda.intelligent_non_fiscale.match(r)[0]);
        mappa_rete += "STAMPANTE FISCALE: " + comanda.indirizzo_cassa + "\r\n";

        //NE MANCANO ALCUNE (QUELLE DINAMICHE)
        for (var numero in comanda.nome_stampante) {
            if (comanda.nome_stampante[numero].nome_umano !== undefined) {
                info.push(comanda.nome_stampante[numero].ip.match(r)[0]);
                mappa_rete += "STAMPANTE: " + comanda.nome_stampante[numero].nome_umano + " - " + comanda.nome_stampante[numero].ip + "\r\n";
            }
        }

        //RTENGINE
        //BISOGNA AGGIUNGERLI
        info.push(websocket[0].url.match(r)[0]);
        mappa_rete += "RTENGINE0: " + websocket[0].url + " - " + websocket[0].readyState + "\r\n";

        info.push(websocket[1].url.match(r)[0]);
        mappa_rete += "RTENGINE1: " + websocket[1].url + " - " + websocket[1].readyState + "\r\n";

        info.push(websocket[2].url.match(r)[0]);
        mappa_rete += "RTENGINE2: " + websocket[2].url + " - " + websocket[2].readyState + "\r\n";

        var unique = info.filter(function (elem, index, self) {
            return index == self.indexOf(elem);
        })

        $.ajax({
            type: "POST",
            async: true,
            url: 'http://localhost:' + comanda.porta_server_remoto + '/fast.intellinet/classi_php/test_rete.php',
            data: {
                ip_server_centrale: comanda.url_server,
                lista_ip: unique
            },
            beforeSend: function () {},
            success: function (risultato) {
                var contenuto_segnalazione = "MAPPA DI RETE:\r\n" + mappa_rete + "\r\n\r\nTEST DEI DISPOSITIVI:\r\n" + risultato  + "\r\n\r\nRICORDA CHE HAI IL DATABASE LOCALE SUL PC DEL CLIENTE SU SEGNALAZIONI\r\n\r\nRISPOSTA UTENTE:\r\n" + info_utente;
                $.ajax({
                    type: "POST",
                    async: true,
                    url: 'http://localhost:' + comanda.porta_server_remoto + '/fast.intellinet/classi_php/salva_segnalazione.php',
                    data: {
                        contenuto_segnalazione: contenuto_segnalazione
                    },
                    dataType: "json",
                    beforeSend: function () {},
                    success: function (risultato) {
                        console.log("STATO SEGNALAZIONE", risultato);
                    },
                    error: function (errore) {
                        console.log("STATO SEGNALAZIONE", errore);
                    }
                });
            },
            error: function (errore) {
            }
        });
    }
}

//CON IL TASTO F9 APRO LA SEGNALAZIONE ERRORI
/*if (comanda.mobile !== true) {
    $(document).keyup(function (e) {
        if (e.which == 120) {
            test_rete();
        }
    });
} else
{
//NEL TABLET CLICCANDO 10 VOLTE VELOCEMENTE MI APRE LA SEGNALAZIONE BUG
    var numero_click_segnalazione_bug = 0;
    var pos_x_bug = 0;
    var pos_y_bug = 0;
    var timeout_click_segnalazione_bug;

    $(document).on('touchstart', function (event) {

        if (numero_click_segnalazione_bug < 9 && (event.originalEvent.changedTouches[0].pageX - pos_x_bug < 50 && event.originalEvent.changedTouches[0].pageY - pos_y_bug < 50)) {

            console.log("CSB", numero_click_segnalazione_bug);
            numero_click_segnalazione_bug++;

            //DOPO UN SECONDO IL NUMERO DI CLICK SI RESETTA
            clearTimeout(timeout_click_segnalazione_bug);
            timeout_click_segnalazione_bug = setTimeout(function () {
                numero_click_segnalazione_bug = 0;
            }, 1000);
        } else if (event.originalEvent.changedTouches[0].pageX - pos_x_bug < 50 && event.originalEvent.changedTouches[0].pageY - pos_y_bug < 50)
        {
            numero_click_segnalazione_bug = 0;
            clearTimeout(timeout_click_segnalazione_bug);
            test_rete();
        }

        pos_x_bug = event.originalEvent.changedTouches[0].pageX;
        pos_y_bug = event.originalEvent.changedTouches[0].pageY;


    });
}*/

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

var simulatore;
var ultimo_time = 0;

function simulatore_click(time1, time2) {

    simulatore = setTimeout(function () {

        var possibilita_scontrino = getRandomInt(1, 50);

        if (possibilita_scontrino === 1) {
            $('#tasti_fiscali_da_nascondere > div > a.sincro_tavoli.gra3.btn.btn-success').trigger('click');
            console.log("SIMULATORE CLICK", new Date().getTime() - ultimo_time, "SCONTRINO");
            ultimo_time = new Date().getTime();
        } else {
            var number = getRandomInt(1, 55);

            $('#prodotti li:nth-child(' + number + ')').trigger('click');
            console.log("SIMULATORE CLICK", new Date().getTime(), number);
        }

        simulatore_click();

    }, getRandomInt(time1, time2));

}

function stop_simulatore_click() {
    clearInterval(simulatore);
}

function aggiorna_giorni_db() {
    var arrupdate = new Array();

    var qcomanda = "select substr(id,5,8) as id from comanda group by substr(id,5,8);";
    comanda.sincro.query_incassi(qcomanda, 20000, function (result_comanda) {
        result_comanda.forEach(function (v) {
            var anno = v.id.substr(0, 4);
            var mese = v.id.substr(4, 2);
            var giorno = v.id.substr(6, 2);
            var update = "update comanda set giorno='" + torna_giorno_modifica_db(giorno, mese, anno) + "' where substr(id,5,8)='" + v.id + "';";
            arrupdate.push(update);

        });

        var testa = "select substr(progressivo_comanda,5,8) as progressivo_comanda from record_teste group by substr(progressivo_comanda,5,8);";
        comanda.sincro.query_incassi(testa, 20000, function (result_testa) {
            result_testa.forEach(function (v) {
                var anno = v.progressivo_comanda.substr(0, 4);
                var mese = v.progressivo_comanda.substr(4, 2);
                var giorno = v.progressivo_comanda.substr(6, 2);
                var update = "update record_teste set giorno='" + torna_giorno_modifica_db(giorno, mese, anno) + "' where substr(progressivo_comanda,5,8)='" + v.progressivo_comanda + "';";

                arrupdate.push(update);


            });

            ricorsione_interna();

        });

    });

    function ricorsione_interna() {
        if (arrupdate.length > 0) {
            comanda.sincro.query_incassi(arrupdate[0], 10000, function () {

                arrupdate.shift();
                ricorsione_interna();

            });
        } else
        {
            alert("PROCEDURA TERMINATA");
        }
    }


}

function torna_giorno_modifica_db(giorno, mese, anno) {
    var mydate = new Date(anno + '-' + mese + '-' + giorno);

    var days = ['7', '1', '2', '3', '4', '5', '6'];

    var day = days[ mydate.getDay() ];

    return day;
}


function segna_pony(id_pony, id_comanda) {
    var query_selezione_pony = 'UPDATE comanda SET id_pony="' + id_pony + '" WHERE id="' + id_comanda + '";';

    comanda.sock.send({tipo: "aggiornamento_pony", operatore: comanda.operatore, query: query_selezione_pony, terminale: comanda.terminale, ip: comanda.ip_address});

    comanda.sincro.query(query_selezione_pony, function () {

        $("#popup_assegna_pony").modal('hide');

        righe_forno();

    });

    comanda.sincro.query_cassa();
}