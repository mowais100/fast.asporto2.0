/* global socket_fast_split, epson */

function data_fattura_elettronica(data_formato_teste) {

    //ESEMPIO: 
    //data_formato_teste: 30-04-19

    var giorno = data_formato_teste.substr(0, 2);
    var mese = data_formato_teste.substr(3, 2);
    var anno = data_formato_teste.substr(6, 2);

    var data_convertita = "20" + anno + "-" + mese + "-" + giorno;

    return data_convertita;

}

function riepilogo_dati_cliente_fattura() {

    var ragione_sociale = $("#fattura [name='ragione_sociale']").val();
    var indirizzo = $("#fattura [name='indirizzo']").val();
    var numero = $("#fattura [name='numero']").val();
    var cap = $("#fattura [name='cap']").val();
    var citta = $("#fattura [name='citta']").val();
    var comune = $("#fattura [name='comune']").val();
    var provincia = $("#fattura [name='provincia']").val();
    var nazione = $("#fattura [name='nazione']").val();
    var cellulare = $("#fattura [name='cellulare']").val();
    var telefono = $("#fattura [name='telefono']").val();
    var email = $("#fattura [name='email']").val();
    var codice_fiscale = $("#fattura [name='codice_fiscale']").val();
    var partita_iva = $("#fattura [name='partita_iva']").val();
    var partita_iva_estera = $("#fattura [name='partita_iva_estera']").val();
    var codice_destinatario = $("#fattura [name='codice_destinatario']").val();
    var pec = $("#fattura [name='pec']").val();
    var regime_fiscale = $("#fattura [name='regime_fiscale'] option:selected").text();
    var formato_trasmissione = $("#fattura [name='formato_trasmissione']").prop("checked") === true ? "Si" : "No";
    var split_payment = $("#fattura [name='split_payment']").prop("checked") === true ? "Si" : "No";

    socket_fast_split.send(JSON.stringify({riepilogo_dati_cliente: "true", ragione_sociale: ragione_sociale, indirizzo: indirizzo, numero: numero, cap: cap,
        citta: citta, comune: comune, provincia: provincia, nazione: nazione, cellulare: cellulare, telefono: telefono, email: email, codice_fiscale: codice_fiscale,
        partita_iva: partita_iva, partita_iva_estera: partita_iva_estera, codice_destinatario: codice_destinatario, pec: pec, regime_fiscale: regime_fiscale,
        formato_trasmissione: formato_trasmissione, split_payment: split_payment}));

    var builder = new epson.ePOSBuilder();
    builder.addTextFont(builder.FONT_A);
    builder.addTextAlign(builder.ALIGN_CENTER);
    builder.addTextSize(1, 1);
    builder.addText("-----------------------------------------\n");
    builder.addTextSize(2, 2);
    builder.addText('RIEPILOGO DATI CLIENTE\n');
    builder.addTextSize(1, 1);
    builder.addText("-----------------------------------------\n");
    builder.addTextSize(1, 1);
    builder.addFeedLine(1);
    builder.addTextAlign(builder.ALIGN_LEFT);
    builder.addText('Ragione Sociale:');
    builder.addTextStyle(false, false, true, builder.COLOR_1);
    builder.addText(ragione_sociale + '\n');
    builder.addTextStyle(false, false, false, builder.COLOR_1);
    builder.addText('Indirizzo:');
    builder.addTextStyle(false, false, true, builder.COLOR_1);
    builder.addText(indirizzo + '\n');
    builder.addTextStyle(false, false, false, builder.COLOR_1);
    builder.addText('Civico:');
    builder.addTextStyle(false, false, true, builder.COLOR_1);
    builder.addText(numero + '\n');
    builder.addTextStyle(false, false, false, builder.COLOR_1);
    builder.addText('CAP:');
    builder.addTextStyle(false, false, true, builder.COLOR_1);
    builder.addText(cap + '\n');
    builder.addTextStyle(false, false, false, builder.COLOR_1);
    builder.addText('CittÃ :');
    builder.addTextStyle(false, false, true, builder.COLOR_1);
    builder.addText(citta + '\n');
    builder.addTextStyle(false, false, false, builder.COLOR_1);
    builder.addText('Comune:');
    builder.addTextStyle(false, false, true, builder.COLOR_1);
    builder.addText(comune + '\n');
    builder.addTextStyle(false, false, false, builder.COLOR_1);
    builder.addText('Provincia:');
    builder.addTextStyle(false, false, true, builder.COLOR_1);
    builder.addText(provincia + '\n');
    builder.addTextStyle(false, false, false, builder.COLOR_1);
    builder.addText('Nazione:');
    builder.addTextStyle(false, false, true, builder.COLOR_1);
    builder.addText(nazione + '\n');
    builder.addTextStyle(false, false, false, builder.COLOR_1);
    builder.addText('Cellulare:');
    builder.addTextStyle(false, false, true, builder.COLOR_1);
    builder.addText(cellulare + '\n');
    builder.addTextStyle(false, false, false, builder.COLOR_1);
    builder.addText('Telefono:');
    builder.addTextStyle(false, false, true, builder.COLOR_1);
    builder.addText(telefono + '\n');
    builder.addTextStyle(false, false, false, builder.COLOR_1);
    builder.addText('E-mail:');
    builder.addTextStyle(false, false, true, builder.COLOR_1);
    builder.addText(email + '\n');
    builder.addTextStyle(false, false, false, builder.COLOR_1);
    builder.addText('Codice Fiscale:');
    builder.addTextStyle(false, false, true, builder.COLOR_1);
    builder.addText(codice_fiscale + '\n');
    builder.addTextStyle(false, false, false, builder.COLOR_1);
    builder.addText('Partita IVA:');
    builder.addTextStyle(false, false, true, builder.COLOR_1);
    builder.addText(partita_iva + '\n');
    builder.addTextStyle(false, false, false, builder.COLOR_1);
    builder.addText('Partita IVA Estera:');
    builder.addTextStyle(false, false, true, builder.COLOR_1);
    builder.addText(partita_iva_estera + '\n');
    builder.addTextStyle(false, false, false, builder.COLOR_1);
    builder.addText('Codice Destinatario:');
    builder.addTextStyle(false, false, true, builder.COLOR_1);
    builder.addText(codice_destinatario + '\n');
    builder.addTextStyle(false, false, false, builder.COLOR_1);
    builder.addText('PEC:');
    builder.addTextStyle(false, false, true, builder.COLOR_1);
    builder.addText(pec + '\n');
    builder.addTextStyle(false, false, false, builder.COLOR_1);
    builder.addText('Regime Fiscale:');
    builder.addTextStyle(false, false, true, builder.COLOR_1);
    builder.addText(regime_fiscale + '\n');
    builder.addTextStyle(false, false, false, builder.COLOR_1);
    builder.addText('Pubblica Amministrazione:');
    builder.addTextStyle(false, false, true, builder.COLOR_1);
    builder.addText(formato_trasmissione + '\n');
    builder.addTextStyle(false, false, false, builder.COLOR_1);
    builder.addText('Split Payment:');
    builder.addTextStyle(false, false, true, builder.COLOR_1);
    builder.addText(split_payment + '\n');
    builder.addTextStyle(false, false, false, builder.COLOR_1);
    builder.addFeedLine(1);
    builder.addTextAlign(builder.ALIGN_CENTER);
    builder.addText("-----------------------------------------\n");
    builder.addCut(builder.CUT_FEED);
    var request = builder.toString();
    //CONTENUTO CONTO
    //console.log(request);

    //Create a SOAP envelop
    var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
    //Create an XMLHttpRequest object
    var xhr = new XMLHttpRequest();
    //Set the end point address
    var url = 'http://' + comanda.intelligent_non_fiscale + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
    //Open an XMLHttpRequest object
    xhr.open('POST', url, true);
    //<Header settings>
    xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
    xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
    xhr.setRequestHeader('SOAPAction', '""');
    // Send print document
    xhr.send(soap);

}

function estrai_data_type_date(valore_calendario) {


    var anno = valore_calendario.substr(2, 2); //anno
    var mese = parseInt(valore_calendario.substr(5, 2)); //mese
    var giorno = parseInt(valore_calendario.substr(8, 2)); //giorno

    return [anno, mese, giorno];
}

var scontrino_ristampa = new Object();

function ristampa_fiscale(ristampa = false) {

    //Azzeramento
    scontrino_ristampa = new Object();

    $("#riquadro_ristampa_fiscale").hide();

    $("#titoli_documenti_selezionati").hide();
    $("#lista_documenti_selezionati").hide();


    var istruzione = '';

    var comando = "query";
    if (ristampa === true) {
        comando = "print";
    }

    switch (comanda.tipo_ristampa_fiscale) {

        case "SCONTRINO":

            if ($('[name="ristampa_da_data"]').val() !== "" && $('[name="ristampa_a_data"]').val() !== "") {

                var d = estrai_data_type_date($('[name="ristampa_da_data"]').val());
                var d1 = estrai_data_type_date($('[name="ristampa_a_data"]').val());


                // Esempio scontrino selezionato per intervallo di data:
                istruzione = '  <printerCommand>\n\
                            <' + comando + 'ContentByDate  operator="1" dataType="1" fromDay="' + d[2] + '" fromMonth="' + d[1] + '" fromYear="' + d[0] + '" toDay="' + d1[2] + '" toMonth="' + d1[1] + '" toYear="' + d1[0] + '" />\n\
                            </printerCommand>';
            } else if ($('[name="ristampa_da_data"]').val() !== "" && $('[name="ristampa_da_n_scontrino"]').val() !== "" && $('[name="ristampa_a_n_scontrino"]').val() !== "") {
                var d = estrai_data_type_date($('[name="ristampa_da_data"]').val());
                //Esempio scontrino selezionato per intervallo di numeri fiscali (si deve indicare solo una singola data):

                var da_n_1 = $('[name="ristampa_da_n_scontrino"]').val().split("-");
                var a_n_1 = $('[name="ristampa_a_n_scontrino"]').val().split("-");

                var da_n_2 = da_n_1[da_n_1.length - 1];
                var a_n_2 = a_n_1[da_n_1.length - 1];

                var da_n_3 = parseInt(da_n_2);
                var a_n_3 = parseInt(a_n_2);

                var da_n_scontrino = da_n_3.toString();
                var a_n_scontrino = a_n_3.toString();

                istruzione = '  <printerCommand>\n\
                            <' + comando + 'ContentByNumbers  operator="1" dataType="1" day="' + d[2] + '" month="' + d[1] + '" year="' + d[0] + '" fromNumber="' + da_n_scontrino + '" toNumber="' + a_n_scontrino + '" />\n\
                            </printerCommand>';
            }
            break;


        case "FATTURA":

            if ($('[name="ristampa_da_data"]').val() !== "" && $('[name="ristampa_a_data"]').val() !== "") {
                var d = estrai_data_type_date($('[name="ristampa_da_data"]').val());
                var d1 = estrai_data_type_date($('[name="ristampa_a_data"]').val());

                //Tenere presente che dataType = 1 comprende scontrini fiscali, documenti commerciali, documenti commerciali di annullo, documenti commerciali di reso, note di credito e chiusure fiscali (solo modelli MF in quanto la stampa delle chiusure sugli RT non Ã¨ fiscale e di conseguenza non Ã¨ salvata).

                //-       Fattura (o documento gestionale analogo dellâ€™RT) per intervallo data e numeri    
                //Per le fatture, esattamente come sopra tranne l'attributo dataType che si imposta al valore 2. I documenti gestionali / scontrini non-fiscali non sono salvati.

                istruzione = '  <printerCommand>\n\
                            <' + comando + 'ContentByDate  operator="1" dataType="2" fromDay="' + d[2] + '" fromMonth="' + d[1] + '" fromYear="' + d[0] + '" toDay="' + d1[2] + '" toMonth="' + d1[1] + '" toYear="' + d1[0] + '" />\n\
                            </printerCommand>';
            } else if ($('[name="ristampa_da_data"]').val() !== "" && $('[name="ristampa_da_n_fattura"]').val() !== "" && $('[name="ristampa_a_n_fattura"]').val() !== "") {
                var d = estrai_data_type_date($('[name="ristampa_da_data"]').val());
                istruzione = '  <printerCommand>\n\
                            <' + comando + 'ContentByNumbers  operator="1" dataType="2" day="' + d[2] + '" month="' + d[1] + '" year="' + d[0] + '" fromNumber="' + $('[name="ristampa_da_n_fattura"]').val() + '" toNumber="' + $('[name="ristampa_a_n_fattura"]').val() + '" />\n\
                            </printerCommand>';
            }
            break;

        case "CHIUSURA":
            if ($('[name="ristampa_da_data"]').val() !== "" && $('[name="ristampa_a_data"]').val() !== "") {
                var d = estrai_data_type_date($('[name="ristampa_da_data"]').val());
                var d1 = estrai_data_type_date($('[name="ristampa_a_data"]').val());

                //-       Chiusura fiscale o documento analogo dellâ€™RT
                //Esattamente come sopra tranne l'attributo dataType che si imposta al valore 5. Solo modelli MF in quanto la stampa delle chiusure sugli RT non Ã¨ fiscale e di conseguenza non Ã¨ salvata).
                istruzione = '  <printerCommand>\n\
                            <' + comando + 'ContentByDate  operator="1" dataType="5" fromDay="' + d[2] + '" fromMonth="' + d[1] + '" fromYear="' + d[0] + '" toDay="' + d1[2] + '" toMonth="' + d1[1] + '" toYear="' + d1[0] + '" />\n\
                            </printerCommand>';
            } else if ($('[name="ristampa_da_data"]').val() !== "" && $('[name="ristampa_da_n_chiusura"]').val() !== "" && $('[name="ristampa_a_n_chiusura"]').val() !== "") {
                var d = estrai_data_type_date($('[name="ristampa_da_data"]').val());
                istruzione = '  <printerCommand>\n\
                            <' + comando + 'ContentByNumbers  operator="1" dataType="5"  day="' + d[2] + '" month="' + d[1] + '" year="' + d[0] + '" fromNumber="' + $('[name="ristampa_da_n_chiusura"]').val() + '" toNumber="' + $('[name="ristampa_a_n_chiusura"]').val() + '" />\n\
                            </printerCommand>';
            }
            break;

    }

    var epos = new epson.fiscalPrint();
    epos.send("http://" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=local_printer&timeout=30000", istruzione, 30000);

    $("#caricamento_rist_fisc_in_corso").show();

    epos.onreceive = function (result, tag_names_array, add_info, res_add) {



        console.log("RISTAMPA FISCALE", result, tag_names_array, add_info, res_add);

        var a = "", b = "", buf = "";
        var indice_data = "", indice_numero = "", totale_complessivo = "";
        var i = 0;


        var counter = 0;
        res_add[0].childNodes[7].childNodes.forEach(function (obj, index) {

            if (obj.nodeName.indexOf("lineNumber") !== -1) {

                if (comanda.tipo_ristampa_fiscale === "SCONTRINO") {
                    if (i > 0 && (obj.textContent.indexOf("DOCUMENTO  COMMERCIALE") !== -1 || (obj.textContent.indexOf("EURO") !== -1 && obj.textContent.indexOf("TOTALE EURO") === -1))) {
                        scontrino_ristampa[counter] = new Object();
                        scontrino_ristampa[counter][indice_data] = new Object();
                        scontrino_ristampa[counter][indice_data][indice_numero] = new Object();
                        scontrino_ristampa[counter][indice_data][indice_numero].indice_totale = totale_complessivo;
                        scontrino_ristampa[counter][indice_data][indice_numero].testo = buf;
                        buf = "";
                        counter++;
                    }
                }

                buf += (obj.textContent + '\n');

                if (comanda.tipo_ristampa_fiscale === "FATTURA") {
                    if (obj.textContent.indexOf("NUM. REGISTRATORE") !== -1) {
                        scontrino_ristampa[counter] = new Object();
                        scontrino_ristampa[counter][indice_data] = new Object();
                        scontrino_ristampa[counter][indice_data][indice_numero] = new Object();

                        scontrino_ristampa[counter][indice_data][indice_numero].indice_totale = totale_complessivo;
                        scontrino_ristampa[counter][indice_data][indice_numero].testo = buf;
                        counter++;
                        buf = "";



                    }
                }

                if (comanda.tipo_ristampa_fiscale === "SCONTRINO" && (obj.textContent.indexOf("TOTALE COMPLESSIVO") !== -1 || obj.textContent.indexOf("TOTALE EURO") !== -1)) {
                    totale_complessivo = obj.textContent.trim();
                } else if (comanda.tipo_ristampa_fiscale === "FATTURA" && obj.textContent.indexOf("TOTALE EURO") !== -1) {
                    totale_complessivo = obj.textContent.trim();
                }

                if (comanda.tipo_ristampa_fiscale === "SCONTRINO") {
                    if (obj.textContent.indexOf("DOCUMENTO N. ") !== -1) {
                        indice_numero = obj.textContent.trim();

                        indice_data = res_add[0].childNodes[7].childNodes[index - 2].textContent.trim();

                        a += indice_numero + " del " + indice_data + "\n";
                    } else if (obj.textContent.indexOf("SF. ") !== -1) {
                        indice_numero = obj.textContent.trim();

                        indice_data = res_add[0].childNodes[7].childNodes[index].textContent.trim();

                        a += indice_numero + " del " + indice_data + "\n";
                    }
                } else if (comanda.tipo_ristampa_fiscale === "FATTURA") {
                    if (obj.textContent.indexOf("FATTURA N. ") !== -1) {
                        indice_numero = obj.textContent.trim();

                        indice_data = res_add[0].childNodes[7].childNodes[index + 2].textContent.trim();

                        a += indice_numero + " del " + indice_data + "\n";
                    }
                }


                //b += (obj.textContent + '<br>');

                i++;
            }
        });



        if (buf !== "") {
            scontrino_ristampa[counter] = new Object();
            scontrino_ristampa[counter][indice_data] = new Object();
            scontrino_ristampa[counter][indice_data][indice_numero] = new Object();
            scontrino_ristampa[counter][indice_data][indice_numero].indice_totale = totale_complessivo;
            scontrino_ristampa[counter][indice_data][indice_numero].testo = buf;
            buf = "";
            counter++;
        }

        console.log(scontrino_ristampa);

        var righe = "<table class='elenco_scontrini_fiscali table table-responsive table-hover table-striped' style='padding-top:2vh;' >";

        for (var c in  scontrino_ristampa) {
            for (var data in  scontrino_ristampa[c]) {
                for (var numero in  scontrino_ristampa[c][data]) {
                    if (comanda.tipo_ristampa_fiscale === "SCONTRINO") {
                        var data2 = "";
                        data2 = data.substr(11, 5).trim();
                        if (data2.length < 5) {
                            data2 = data.substr(10, 5).trim();
                        }
                        righe += "<tr class='riga_documenti_fiscali_visualizzabili' onclick='vedi_doc_fisc_salvato(\"" + c + "\",\"" + data + "\",\"" + numero + "\",event);'><td>" + data.substr(0, 10).replace(/-/gi, "/") + "</td><td>" + data2 + "</td><td>" + numero.replace("SF.", "").slice(-9) + "</td><td class='text-right'>" + scontrino_ristampa[c][data][numero].indice_totale.replace("TOTALE COMPLESSIVO", "").replace("TOTALE EURO", "").replace(" ", "").trim() + "</td></tr>";
                    } else if (comanda.tipo_ristampa_fiscale === "FATTURA") {
                        righe += "<tr class='riga_documenti_fiscali_visualizzabili' onclick='vedi_doc_fisc_salvato(\"" + c + "\",\"" + data + "\",\"" + numero + "\",event);'><td>" + data.replace("DATA", "").trim().substr(0, 10).replace(/-/gi, "/") + "</td><td>" + data.slice(-5).replace("DATA", "") + "</td><td tyle='text-align: right;padding-right: 20px;'>" + numero.replace("FATTURA N.", "").slice(-9) + "</td><td class='text-right'>" + scontrino_ristampa[c][data][numero].indice_totale.replace("TOTALE EURO", "").trim() + "</td></tr>";
                    }
                }

            }
        }



        var intestazione = "<table class='elenco_scontrini_fiscali table table-responsive table-hover table-striped' style='padding-top:2vh;' >";
        intestazione += "<tr><th style='text-align:center;font-size:25.5px;'>Data</th><th style='text-align:center;font-size:25.5px;'>Ora</th><th style='text-align:center;font-size:25.5px;'>Documento</th><th style='text-align:center;font-size:25.5px;'>Totale</th></tr>";
        intestazione += "</table>";

        $("#titoli_documenti_selezionati").html(intestazione);
        $("#lista_documenti_selezionati").html(righe);
        $("#lista_documenti_selezionati").show();
        $("#titoli_documenti_selezionati").show();

        console.log(a, b);

        $("#caricamento_rist_fisc_in_corso").hide();

    };

}

function vedi_doc_fisc_salvato(c, indice_data, indice_numero, event) {
    $(".riga_documenti_fiscali_visualizzabili").removeClass("btn-info2");
    $(event.currentTarget).addClass("btn-info2");
    $('#riquadro_ristampa_fiscale').html(scontrino_ristampa[c][indice_data][indice_numero].testo);
    $("#riquadro_ristampa_fiscale").show();
}