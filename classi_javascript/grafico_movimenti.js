/* global Chart, comanda, parseFloat */

function torna_giorno(giorno, mese, anno) {
    var mydate = new Date(anno + '-' + mese + '-' + giorno);

    var days = ['Domenica', 'Lunedì', 'Martedì', 'Mercoledì', 'Giovedì', 'Venerdì', 'Sabato'];

    var day = days[ mydate.getDay() ];

    return day;
}
/* String */
var tabella_anno_corrente_movimenti = "";
var tabella_mese_corrente_movimenti = "";



function elenco_movimenti() {

    /* Float */
    var totale_totale = 0;

    var data = new Date();

    var giorno = aggZero(data.getDate().toString(), 2);
    var mese = aggZero((data.getMonth() + 1).toString(), 2);
    var anno = data.getFullYear().toString().slice(2);

    window.chartColors = {
        red: 'rgb(255, 99, 132)',
        orange: 'rgb(255, 159, 64)',
        yellow: 'rgb(255, 205, 86)',
        green: 'forestgreen',
        blue: 'rgb(54, 162, 235)',
        purple: 'rgb(153, 102, 255)',
        grey: 'rgb(231,233,237)'
    };

    window.randomScalingFactor = function () {
        return Math.floor(Math.random() * (500000 - 0 + 1) + 0);
    };

    Chart.defaults.global.defaultFontColor = '#fff';



    var testo_query = "select substr(data,4,2) as mese,sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,7,2)='" + anno + "' group by substr(data,4,2) order by substr(data,4,2) asc;";
    comanda.sincro.query_movimenti(testo_query, 3000, function (res) {
        var dati = new Array();

        for (var i = 1; i <= 12; i++) {
            var a = 0.00;
            if (res !== undefined && res[0] !== undefined) {
                res.forEach(function (v) {
                    if (aggZero(i.toString(), 2) === v.mese) {
                        a = v.prezzo_tot;
                    }
                });
            }
            totale_totale += parseFloat(a);
            dati.push(parseFloat(a).toFixed(2));

        }
        console.log("DATI_1", dati);

        tabella_anno_corrente_movimenti = '';
        tabella_anno_corrente_movimenti += '<div class="row">';
        tabella_anno_corrente_movimenti += '<div style="font-size:1vh;text-align:center;">';
        tabella_anno_corrente_movimenti += '<table style="width:100%;" id="tabella_movimenti_anno_corrente">';

        tabella_anno_corrente_movimenti += '<tr><td>Gennaio</td><td>&euro; ' + dati[0] + '</td><td></td>  <td>Aprile</td><td>&euro; ' + dati[3] + '</td><td></td>    <td>Luglio</td><td>&euro; ' + dati[6] + '</td><td></td>        <td>Ottobre</td><td>&euro; ' + dati[9] + '</td></tr>';
        tabella_anno_corrente_movimenti += '<tr><td>Febbraio</td><td>&euro; ' + dati[1] + '</td><td></td> <td>Maggio</td><td>&euro; ' + dati[4] + '</td><td></td>    <td>Agosto</td><td>&euro; ' + dati[7] + '</td><td></td>        <td>Novembre</td><td>&euro; ' + dati[10] + '</td></tr>';
        tabella_anno_corrente_movimenti += '<tr><td>Marzo</td><td>&euro; ' + dati[2] + '</td><td></td>    <td>Giugno</td><td>&euro; ' + dati[5] + '</td><td></td>    <td>Settembre</td><td>&euro; ' + dati[8] + '</td><td></td>     <td>Dicembre</td><td>&euro; ' + dati[11] + '</td></tr>';

        tabella_anno_corrente_movimenti += '</table>';
        tabella_anno_corrente_movimenti += '</div>';
        tabella_anno_corrente_movimenti += '</div>';


        tabella_anno_corrente_movimenti += '<div class="row">';

        tabella_anno_corrente_movimenti += '<div style="padding-top:1vh;font-size:1vh;text-align:center;">';
        tabella_anno_corrente_movimenti += '<table style="width:100%;" id="tabella_movimenti_totali_mese_corrente">';
        tabella_anno_corrente_movimenti += '<tr><td style="font-weight:bold;">TOTALE:</td>              <td style="font-weight:bold;">&euro; ' + totale_totale.toFixed(2) + '</td>  <td></td><td>Media Mensile</td><td>&euro; ' + (totale_totale / 12).toFixed(2) + '</td></tr>';
        tabella_anno_corrente_movimenti += '</table>';
        tabella_anno_corrente_movimenti += '</div>';
        tabella_anno_corrente_movimenti += '</div>';


        var config = {
            type: 'line',
            data: {
                labels: ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto",
                    "Settembre", "Ottobre", "Novembre", "Dicembre"],
                datasets: [{
                        label: "INCASSO",
                        backgroundColor: window.chartColors.green,
                        borderColor: window.chartColors.green,
                        data: dati,
                        fill: false
                    }]
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: ''
                },
                legend: {
                    display: false
                },
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem) {
                            return tooltipItem.yLabel;
                        }
                    }
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                            ticks: {fontSize: 20},
                            gridLines: {

                                zeroLineColor: "white"
                            },
                            display: true,
                            scaleLabel: {
                                display: false,
                                labelString: 'Mesi'
                            }
                        }],
                    yAxes: [{
                            ticks: {fontSize: 20},
                            gridLines: {

                                zeroLineColor: "white"
                            },
                            display: true,
                            scaleLabel: {
                                display: false,
                                labelString: 'Euro'
                            }
                        }]
                }
            }
        };
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myLine = new Chart(ctx, config);
        window.myLine.update();

    });



    var data = new Date();
    var mese = aggZero((data.getMonth() + 1).toString(), 2);



    var testo_query = "select substr(data,1,2) as giorno,sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,4,2)='" + mese + "' and substr(data,7,2)='" + anno + "' group by substr(data,1,2) order by substr(data,1,2) asc;";
    comanda.sincro.query_movimenti(testo_query, 3000, function (res) {
        var dati = new Array();
        var dati_giorni = new Array();
        var bold = new Array();

        /* Float */
        var totale_feriali = 0;
        var totale_weekend = 0;

        /* Ri-utilizzo la variabile di prima */
        totale_totale = 0;

        var incassi_giornalieri = new Object();

        for (var i = 1; i <= 31; i++) {
            var a = 0.00;
            if (res !== undefined && res[0] !== undefined) {
                res.forEach(function (v) {
                    if (aggZero(i.toString(), 2) === v.giorno) {
                        a = v.prezzo_tot;
                    }
                });
            }
            dati.push(parseFloat(a).toFixed(2));
            totale_totale += parseFloat(a);

            var giorno = torna_giorno(aggZero(i.toString(), 2), aggZero(mese, 2), "20" + anno).substr(0, 3) + " ";


            dati_giorni.push(giorno);
            if (giorno === "Sabato" || giorno === "Domenica") {
                totale_weekend += parseFloat(a);
                bold.push(" style='font-weight:bold;' ");
            } else
            {
                totale_feriali += parseFloat(a);
                bold.push("");
            }
        }

        tabella_mese_corrente_movimenti = '';
        tabella_mese_corrente_movimenti += '<div class="row">';
        tabella_mese_corrente_movimenti += '<div style="font-size:1vh;text-align:center;">';
        tabella_mese_corrente_movimenti += '<table style="width:100%;" id="tabella_movimenti_mese_corrente">';
        tabella_mese_corrente_movimenti += '<tr><td ' + bold[0] + '>' + dati_giorni[0] + '01</td><td ' + bold[0] + '>&euro; ' + dati[0] + '</td><td></td><td ' + bold[5] + '>' + dati_giorni[5] + '06</td><td ' + bold[5] + '>&euro; ' + dati[5] + '</td><td></td><td ' + bold[10] + '>' + dati_giorni[10] + '11</td><td ' + bold[10] + '>&euro; ' + dati[10] + '</td><td></td><td ' + bold[15] + '>' + dati_giorni[15] + '16</td><td ' + bold[15] + '>&euro; ' + dati[15] + '</td><td></td><td ' + bold[20] + '>' + dati_giorni[20] + '21</td><td ' + bold[20] + '>&euro; ' + dati[20] + '</td><td></td><td ' + bold[25] + '>' + dati_giorni[25] + '26</td><td ' + bold[25] + '>&euro; ' + dati[25] + '</td></tr>';
        tabella_mese_corrente_movimenti += '<tr><td ' + bold[1] + '>' + dati_giorni[1] + '02</td><td ' + bold[1] + '>&euro; ' + dati[1] + '</td><td></td><td ' + bold[6] + '>' + dati_giorni[6] + '07</td><td ' + bold[6] + '>&euro; ' + dati[6] + '</td><td></td><td ' + bold[11] + '>' + dati_giorni[11] + '12</td><td ' + bold[11] + '>&euro; ' + dati[11] + '</td><td></td><td ' + bold[16] + '>' + dati_giorni[16] + '17</td><td ' + bold[16] + '>&euro; ' + dati[16] + '</td><td></td><td ' + bold[21] + '>' + dati_giorni[21] + '22</td><td ' + bold[21] + '>&euro; ' + dati[21] + '</td><td></td><td ' + bold[26] + '>' + dati_giorni[26] + '27</td><td ' + bold[26] + '>&euro; ' + dati[26] + '</td></tr>';
        tabella_mese_corrente_movimenti += '<tr><td ' + bold[2] + '>' + dati_giorni[2] + '03</td><td ' + bold[2] + '>&euro; ' + dati[2] + '</td><td></td><td ' + bold[7] + '>' + dati_giorni[7] + '08</td><td ' + bold[7] + '>&euro; ' + dati[7] + '</td><td></td><td ' + bold[12] + '>' + dati_giorni[12] + '13</td><td ' + bold[12] + '>&euro; ' + dati[12] + '</td><td></td><td ' + bold[17] + '>' + dati_giorni[17] + '18</td><td ' + bold[17] + '>&euro; ' + dati[17] + '</td><td></td><td ' + bold[22] + '>' + dati_giorni[22] + '23</td><td ' + bold[22] + '>&euro; ' + dati[22] + '</td><td></td><td ' + bold[27] + '>' + dati_giorni[27] + '28</td><td ' + bold[27] + '>&euro; ' + dati[27] + '</td></tr>';
        tabella_mese_corrente_movimenti += '<tr><td ' + bold[3] + '>' + dati_giorni[3] + '04</td><td ' + bold[3] + '>&euro; ' + dati[3] + '</td><td></td><td ' + bold[8] + '>' + dati_giorni[8] + '09</td><td ' + bold[8] + '>&euro; ' + dati[8] + '</td><td></td><td ' + bold[13] + '>' + dati_giorni[13] + '14</td><td ' + bold[13] + '>&euro; ' + dati[13] + '</td><td></td><td ' + bold[18] + '>' + dati_giorni[18] + '19</td><td ' + bold[18] + '>&euro; ' + dati[18] + '</td><td></td><td ' + bold[23] + '>' + dati_giorni[23] + '24</td><td ' + bold[23] + '>&euro; ' + dati[23] + '</td><td></td><td ' + bold[28] + '>' + dati_giorni[28] + '29</td><td ' + bold[28] + '>&euro; ' + dati[28] + '</td></tr>';
        tabella_mese_corrente_movimenti += '<tr><td ' + bold[4] + '>' + dati_giorni[4] + '05</td><td ' + bold[4] + '>&euro; ' + dati[4] + '</td><td></td><td ' + bold[9] + '>' + dati_giorni[9] + '10</td><td ' + bold[9] + '>&euro; ' + dati[9] + '</td><td></td><td ' + bold[14] + '>' + dati_giorni[14] + '15</td><td ' + bold[14] + '>&euro; ' + dati[14] + '</td><td></td><td ' + bold[19] + '>' + dati_giorni[19] + '20</td><td ' + bold[19] + '>&euro; ' + dati[19] + '</td><td></td><td ' + bold[24] + '>' + dati_giorni[24] + '25</td><td ' + bold[24] + '>&euro; ' + dati[24] + '</td><td></td><td ' + bold[29] + '>' + dati_giorni[29] + '30</td><td ' + bold[29] + '>&euro; ' + dati[29] + '</td></tr>';

        var numero_giorni_mese = calcola_giorni_mese(parseInt(mese), parseInt("20" + anno));
        if (numero_giorni_mese === 31) {
            tabella_mese_corrente_movimenti += '<tr class="questa_no"><td colspan="15"></td><td ' + bold[30] + '>' + dati_giorni[30] + '31</td><td ' + bold[30] + '>&euro; ' + dati[30] + '</td></tr>';
        }
        tabella_mese_corrente_movimenti += '</table>';
        tabella_mese_corrente_movimenti += '</div>';
        tabella_mese_corrente_movimenti += '</div>';


        tabella_mese_corrente_movimenti += '<div class="row">';

        /* String */
        var padding_top = " padding-top:1vh; ";
        if (numero_giorni_mese === 31) {
            padding_top = " padding-top:0; ";
        }
        tabella_mese_corrente_movimenti += '<div style="' + padding_top + 'font-size:1vh;text-align:center;">';
        tabella_mese_corrente_movimenti += '<table style="width:100%;" id="tabella_movimenti_totali_mese_corrente">';
        tabella_mese_corrente_movimenti += '<tr><td>Lunedì - Venerdì</td>                               <td>&euro; ' + totale_feriali.toFixed(2) + '</td>                           <td></td><td>Media Giornaliera (Lun-Ven)</td><td>&euro; ' + (totale_feriali / numero_giorni_mese).toFixed(2) + '</td><tr>';
        tabella_mese_corrente_movimenti += '<tr><td>Sabato - Domenica</td>                              <td>&euro; ' + totale_weekend.toFixed(2) + '</td>                           <td></td><td>Media Giornaliera (Sab-Dom)</td><td>&euro; ' + (totale_weekend / numero_giorni_mese).toFixed(2) + '</td></tr>';
        tabella_mese_corrente_movimenti += '<tr><td style="font-weight:bold;">TOTALE:</td>              <td style="font-weight:bold;">&euro; ' + totale_totale.toFixed(2) + '</td>  <td></td><td>Media Giornaliera (Totale)</td><td>&euro; ' + (totale_totale / numero_giorni_mese).toFixed(2) + '</td></tr>';
        tabella_mese_corrente_movimenti += '</table>';
        tabella_mese_corrente_movimenti += '</div>';
        tabella_mese_corrente_movimenti += '</div>';


        console.log("DATI_2", dati);


        Chart.defaults.global.defaultFontColor = '#fff';
        var config = {
            type: 'line',
            data: {
                labels: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"],
                datasets: [{
                        label: "INCASSI",
                        backgroundColor: window.chartColors.green,
                        borderColor: window.chartColors.green,
                        data: dati,
                        fill: false
                    }]
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: ''
                },
                legend: {
                    display: false
                },
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem) {
                            return tooltipItem.yLabel;
                        }
                    }
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                            ticks: {
                                fontSize: 20
                            },
                            gridLines: {
                                zeroLineColor: "white"
                            },
                            display: true,
                            scaleLabel: {
                                display: false,
                                labelString: 'Giorni'
                            }
                        }],
                    yAxes: [{
                            ticks: {fontSize: 20},
                            gridLines: {

                                zeroLineColor: "white"
                            },
                            display: true,
                            scaleLabel: {
                                display: false,
                                labelString: 'Euro'
                            }
                        }]
                }
            }
        };
        var ctx = document.getElementById("canvas2").getContext("2d");
        window.myLine = new Chart(ctx, config);
        window.myLine.update();
    });



    Chart.defaults.global.defaultFontColor = '#fff';
    var config = {
        type: 'bar',
        data: {
            labels: [],
            datasets: [{
                    label: "...",
                    backgroundColor: window.chartColors.green,
                    borderColor: window.chartColors.green,
                    data: [0],
                    fill: false
                }]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: ''
            },
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function (tooltipItem) {
                        return tooltipItem.yLabel;
                    }
                }
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                        ticks: {fontSize: 20},
                        gridLines: {

                            zeroLineColor: "white"
                        },
                        display: true,
                        scaleLabel: {
                            display: false,
                            labelString: '...'
                        }
                    }],
                yAxes: [{
                        ticks: {fontSize: 20},
                        gridLines: {

                            zeroLineColor: "white"
                        },
                        display: true,
                        scaleLabel: {
                            display: false,
                            labelString: 'Euro'
                        }
                    }]
            }
        }
    };
    var ctx = document.getElementById("canvas3").getContext("2d");
    window.myLine_confronto1 = new Chart(ctx, config);





    window.chartColors = {
        red: 'rgb(255, 99, 132)',
        orange: 'rgb(255, 159, 64)',
        yellow: 'rgb(255, 205, 86)',
        green: 'forestgreen',
        blue: 'rgb(54, 162, 235)',
        purple: 'rgb(153, 102, 255)',
        grey: 'rgb(231,233,237)'
    };
    Chart.defaults.global.defaultFontColor = '#fff';
    var config = {
        type: 'bar',
        data: {
            labels: [],
            datasets: [{
                    label: "...",
                    backgroundColor: window.chartColors.green,
                    borderColor: window.chartColors.green,
                    data: [],
                    fill: false
                }]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: ''
            },
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    label: function (tooltipItem) {
                        return tooltipItem.yLabel;
                    }
                }
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                        ticks: {fontSize: 20},
                        gridLines: {

                            zeroLineColor: "white"
                        },
                        display: true,
                        scaleLabel: {
                            display: false,
                            labelString: '...'
                        }
                    }],
                yAxes: [{
                        ticks: {fontSize: 20},
                        gridLines: {

                            zeroLineColor: "white"
                        },
                        display: true,
                        scaleLabel: {
                            display: false,
                            labelString: 'Euro'
                        }
                    }]
            }
        }
    };
    var ctx = document.getElementById("canvas4").getContext("2d");
    window.myLine_confronto2 = new Chart(ctx, config);






    var data = new Date();
    data.setDate(data.getDate() - 1);
    var giorno = aggZero(data.getDate().toString(), 2);
    var mese = aggZero((data.getMonth() + 1).toString(), 2);
    var anno = data.getFullYear().toString().slice(2);
    $('[name="giorno_1"] option:containsExact(' + giorno + ')').attr('selected', true);
    $('[name="mese_1"] option[value=' + mese + ']').attr('selected', true);
    $('[name="anno_1"] option[value=' + anno + ']').attr('selected', true);
    confronta_periodo(1);
    var data = new Date();
    var giorno = aggZero(data.getDate().toString(), 2);
    var mese = aggZero((data.getMonth() + 1).toString(), 2);
    var anno = data.getFullYear().toString().slice(2);

    $('#gm_anno_corrente').html(data.getFullYear().toString());

    var mesi = ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'];

    $('#gm_mese_corrente').html(mesi[data.getMonth()]);

    $('[name="giorno_2"] option:containsExact(' + giorno + ')').attr('selected', true);
    $('[name="mese_2"] option[value=' + mese + ']').attr('selected', true);
    $('[name="anno_2"] option[value=' + anno + ']').attr('selected', true);
    confronta_periodo(2);


}


var tabella_confronto_movimenti = new Array();

function confronta_periodo(numero_tasto_partenza)
{

    var totale_feriali = 0, totale_weekend = 0, totale_totale = 0;
    tabella_confronto_movimenti[numero_tasto_partenza] = new Array();

    if (numero_tasto_partenza === 1) {
        var grafico = window.myLine_confronto1;
    } else
    {
        var grafico = window.myLine_confronto2;
    }

    var giorno_inviato = $("#pagina_movimenti [name=giorno_" + numero_tasto_partenza + "]").val();
    var mese_inviato = $("#pagina_movimenti [name=mese_" + numero_tasto_partenza + "]").val();
    var anno_inviato = $("#pagina_movimenti [name=anno_" + numero_tasto_partenza + "]").val();

    //Se scelgo l'anno come confronto vedo i mesi                     
    var mesi = ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'];

    //Se scelgo i mesi vedo i giorni
    var giorni = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'];

    //Se scelgo i giorni solo quel giorno
    var ore = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24'];

    if (giorno_inviato !== 'Giorno')
    {
        if (mese_inviato !== "Mese")
        {
            if (anno_inviato !== "Anno")
            {

                //LEGENDA ASSE X
                //grafico.config.options.scales.xAxes[0].scaleLabel.labelString = "Ore";

                //VALORI ASSE X
                grafico.config.data.labels = ore.map(function (el) {
                    return el + ':00';
                });

                var testo_query = "select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='01'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='02'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='03'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='04'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='05'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='06'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='07'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='08'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='09'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='10'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='11'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='12'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='13'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='14'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='15'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='16'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='17'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='18'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='19'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='20'\n\
 union all\n\
 \select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='21'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='22'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='23'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='24'";

                //GIORNO-MESE-ANNO (TAB ORE)
                tabella_confronto_movimenti[numero_tasto_partenza] = "";
                comanda.sincro.query_movimenti(testo_query, 3000, function (dato) {

                    //DATI GRAFICO - MANCA CICLO FOR
                    grafico.config.data.datasets[0].data = new Array();


                    dato.forEach(function (value) {
                        if (value.prezzo_tot === undefined || value.prezzo_tot === null) {
                            value.prezzo_tot = 0;
                        } else
                        {
                            //PER RISOLVERE ERRORE VIRGOLE
                            value.prezzo_tot = value.prezzo_tot.replace(",", "");
                            value.prezzo_tot = parseFloat(value.prezzo_tot);
                        }

                        console.log("QUERY CONFRONTO", value.prezzo_tot);


                        totale_totale += parseFloat(value.prezzo_tot);

                        //DATI GRAFICO - MANCA CICLO FOR
                        grafico.config.data.datasets[0].data.push(parseFloat(value.prezzo_tot).toFixed(2));


                    });

                    var dati = grafico.config.data.datasets[0].data;


                    tabella_confronto_movimenti[numero_tasto_partenza] = "";
                    tabella_confronto_movimenti[numero_tasto_partenza] += '<div class="row">';
                    tabella_confronto_movimenti[numero_tasto_partenza] += '<div style="font-size:1vh;text-align:center;">';
                    tabella_confronto_movimenti[numero_tasto_partenza] += '<table style="width:100%;" id="tabella_movimenti_mese_corrente">';
                    tabella_confronto_movimenti[numero_tasto_partenza] += '<tr><td>00</td><td>&euro; ' + dati[0] + '</td><td></td><td>04</td><td>&euro; ' + dati[4] + '</td><td></td><td>08</td><td>&euro; ' + dati[8] + '</td><td></td><td>12</td><td>&euro; ' + dati[12] + '</td><td></td><td>16</td><td>&euro; ' + dati[16] + '</td><td></td><td>20</td><td>&euro; ' + dati[20] + '</td></tr>';
                    tabella_confronto_movimenti[numero_tasto_partenza] += '<tr><td>01</td><td>&euro; ' + dati[1] + '</td><td></td><td>05</td><td>&euro; ' + dati[5] + '</td><td></td><td>09</td><td>&euro; ' + dati[9] + '</td><td></td><td>13</td><td>&euro; ' + dati[13] + '</td><td></td><td>17</td><td>&euro; ' + dati[17] + '</td><td></td><td>21</td><td>&euro; ' + dati[21] + '</td></tr>';
                    tabella_confronto_movimenti[numero_tasto_partenza] += '<tr><td>02</td><td>&euro; ' + dati[2] + '</td><td></td><td>06</td><td>&euro; ' + dati[6] + '</td><td></td><td>10</td><td>&euro; ' + dati[10] + '</td><td></td><td>14</td><td>&euro; ' + dati[14] + '</td><td></td><td>18</td><td>&euro; ' + dati[18] + '</td><td></td><td>22</td><td>&euro; ' + dati[22] + '</td></tr>';
                    tabella_confronto_movimenti[numero_tasto_partenza] += '<tr><td>03</td><td>&euro; ' + dati[3] + '</td><td></td><td>07</td><td>&euro; ' + dati[7] + '</td><td></td><td>11</td><td>&euro; ' + dati[11] + '</td><td></td><td>15</td><td>&euro; ' + dati[15] + '</td><td></td><td>19</td><td>&euro; ' + dati[19] + '</td><td></td><td>23</td><td>&euro; ' + dati[23] + '</td></tr>';

                    tabella_confronto_movimenti[numero_tasto_partenza] += '</table>';
                    tabella_confronto_movimenti[numero_tasto_partenza] += '</div>';
                    tabella_confronto_movimenti[numero_tasto_partenza] += '</div>';


                    tabella_confronto_movimenti[numero_tasto_partenza] += '<div class="row">';


                    tabella_confronto_movimenti[numero_tasto_partenza] += '<div style="padding-top:1vh;font-size:1vh;text-align:center;">';
                    tabella_confronto_movimenti[numero_tasto_partenza] += '<table style="width:100%;" id="tabella_movimenti_totali_mese_corrente">';
                    tabella_confronto_movimenti[numero_tasto_partenza] += '<tr><td style="font-weight:bold;">TOTALE:</td>              <td style="font-weight:bold;">&euro; ' + totale_totale.toFixed(2) + '</td>  <td></td><td>Media Oraria</td><td>&euro; ' + (totale_totale / 24).toFixed(2) + '</td></tr>';
                    tabella_confronto_movimenti[numero_tasto_partenza] += '</table>';
                    tabella_confronto_movimenti[numero_tasto_partenza] += '</div>';
                    tabella_confronto_movimenti[numero_tasto_partenza] += '</div>';

                    $('#grafico_selezione_periodo_' + numero_tasto_partenza).html(giorno_inviato + ' ' + mesi[mese_inviato - 1] + ' 20' + anno_inviato);

                    //AGGIORNAMENTO LINEE
                    grafico.update();

                });

            } else
            {
                alert('Per confrontare i dati di una giornata, DEVI INSERIRE anche l\'ANNO');
            }
        } else
        {
            alert('Per confrontare i dati di una giornata, DEVI INSERIRE il MESE e l\'ANNO');
        }
    } else

    if (mese_inviato !== "Mese")
    {
        if (anno_inviato !== "Anno")
        {

            //LEGENDA ASSE X
            //grafico.config.options.scales.xAxes[0].scaleLabel.labelString = "Giorni";

            //VALORI ASSE X
            grafico.config.data.labels = giorni;

            var testo_query = "select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='01' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='02' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='03' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='04' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='05' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='06' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='07' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='08' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='09' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='10' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='11' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='12' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='13' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='14' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='15' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='16' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
  select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='17' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='18' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='19' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='20' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='21' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='22' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='23' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='24' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='25' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='26' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='27' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='28' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='29' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='30' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='31' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "';";


            console.log("QUERY CONFRONTO", testo_query);

            //MESE-ANNO (TAB GIORNI)

            var bold = new Array();
            var dati_giorni = new Array();

            var i = 1;

            comanda.sincro.query_movimenti(testo_query, 3000, function (dato) {

                console.log("QUERY CONFRONTO", dato);


                grafico.config.data.datasets[0].data = new Array();


                dato.forEach(function (value) {
                    if (value.prezzo_tot === undefined || value.prezzo_tot === null) {
                        value.prezzo_tot = 0;
                    } else
                    {
                        //PER RISOLVERE ERRORE VIRGOLE
                        value.prezzo_tot = value.prezzo_tot.replace(",", "");
                        value.prezzo_tot = parseFloat(value.prezzo_tot);
                    }

                    console.log("QUERY CONFRONTO", value.prezzo_tot);

                    //DATI GRAFICO - MANCA CICLO FOR
                    grafico.config.data.datasets[0].data.push(parseFloat(value.prezzo_tot).toFixed(2));

                    var giorno = torna_giorno(aggZero(i.toString(), 2), aggZero(mese_inviato, 2), "20" + anno_inviato).substr(0, 3) + " ";

                    dati_giorni.push(giorno);

                    if (giorno === "Sabato" || giorno === "Domenica") {
                        totale_weekend += parseFloat(value.prezzo_tot);
                        bold.push(" style='font-weight:bold;' ");
                    } else
                    {
                        totale_feriali += parseFloat(value.prezzo_tot);
                        bold.push("");
                    }

                    totale_totale += parseFloat(value.prezzo_tot);

                    i++;

                });


                var dati = grafico.config.data.datasets[0].data;


                tabella_confronto_movimenti[numero_tasto_partenza] = "";
                tabella_confronto_movimenti[numero_tasto_partenza] += '<div class="row">';
                tabella_confronto_movimenti[numero_tasto_partenza] += '<div style="font-size:1vh;text-align:center;">';
                tabella_confronto_movimenti[numero_tasto_partenza] += '<table style="width:100%;" id="tabella_movimenti_mese_corrente">';
                tabella_confronto_movimenti[numero_tasto_partenza] += '<tr><td ' + bold[0] + '>' + dati_giorni[0] + '01</td><td ' + bold[0] + '>&euro; ' + dati[0] + '</td><td></td><td ' + bold[5] + '>' + dati_giorni[5] + '06</td><td ' + bold[5] + '>&euro; ' + dati[5] + '</td><td></td><td ' + bold[10] + '>' + dati_giorni[10] + '11</td><td ' + bold[10] + '>&euro; ' + dati[10] + '</td><td></td><td ' + bold[15] + '>' + dati_giorni[15] + '16</td><td ' + bold[15] + '>&euro; ' + dati[15] + '</td><td></td><td ' + bold[20] + '>' + dati_giorni[20] + '21</td><td ' + bold[20] + '>&euro; ' + dati[20] + '</td><td></td><td ' + bold[25] + '>' + dati_giorni[25] + '26</td><td ' + bold[25] + '>&euro; ' + dati[25] + '</td></tr>';
                tabella_confronto_movimenti[numero_tasto_partenza] += '<tr><td ' + bold[1] + '>' + dati_giorni[1] + '02</td><td ' + bold[1] + '>&euro; ' + dati[1] + '</td><td></td><td ' + bold[6] + '>' + dati_giorni[6] + '07</td><td ' + bold[6] + '>&euro; ' + dati[6] + '</td><td></td><td ' + bold[11] + '>' + dati_giorni[11] + '12</td><td ' + bold[11] + '>&euro; ' + dati[11] + '</td><td></td><td ' + bold[16] + '>' + dati_giorni[16] + '17</td><td ' + bold[16] + '>&euro; ' + dati[16] + '</td><td></td><td ' + bold[21] + '>' + dati_giorni[21] + '22</td><td ' + bold[21] + '>&euro; ' + dati[21] + '</td><td></td><td ' + bold[26] + '>' + dati_giorni[26] + '27</td><td ' + bold[26] + '>&euro; ' + dati[26] + '</td></tr>';
                tabella_confronto_movimenti[numero_tasto_partenza] += '<tr><td ' + bold[2] + '>' + dati_giorni[2] + '03</td><td ' + bold[2] + '>&euro; ' + dati[2] + '</td><td></td><td ' + bold[7] + '>' + dati_giorni[7] + '08</td><td ' + bold[7] + '>&euro; ' + dati[7] + '</td><td></td><td ' + bold[12] + '>' + dati_giorni[12] + '13</td><td ' + bold[12] + '>&euro; ' + dati[12] + '</td><td></td><td ' + bold[17] + '>' + dati_giorni[17] + '18</td><td ' + bold[17] + '>&euro; ' + dati[17] + '</td><td></td><td ' + bold[22] + '>' + dati_giorni[22] + '23</td><td ' + bold[22] + '>&euro; ' + dati[22] + '</td><td></td><td ' + bold[27] + '>' + dati_giorni[27] + '28</td><td ' + bold[27] + '>&euro; ' + dati[27] + '</td></tr>';
                tabella_confronto_movimenti[numero_tasto_partenza] += '<tr><td ' + bold[3] + '>' + dati_giorni[3] + '04</td><td ' + bold[3] + '>&euro; ' + dati[3] + '</td><td></td><td ' + bold[8] + '>' + dati_giorni[8] + '09</td><td ' + bold[8] + '>&euro; ' + dati[8] + '</td><td></td><td ' + bold[13] + '>' + dati_giorni[13] + '14</td><td ' + bold[13] + '>&euro; ' + dati[13] + '</td><td></td><td ' + bold[18] + '>' + dati_giorni[18] + '19</td><td ' + bold[18] + '>&euro; ' + dati[18] + '</td><td></td><td ' + bold[23] + '>' + dati_giorni[23] + '24</td><td ' + bold[23] + '>&euro; ' + dati[23] + '</td><td></td><td ' + bold[28] + '>' + dati_giorni[28] + '29</td><td ' + bold[28] + '>&euro; ' + dati[28] + '</td></tr>';
                tabella_confronto_movimenti[numero_tasto_partenza] += '<tr><td ' + bold[4] + '>' + dati_giorni[4] + '05</td><td ' + bold[4] + '>&euro; ' + dati[4] + '</td><td></td><td ' + bold[9] + '>' + dati_giorni[9] + '10</td><td ' + bold[9] + '>&euro; ' + dati[9] + '</td><td></td><td ' + bold[14] + '>' + dati_giorni[14] + '15</td><td ' + bold[14] + '>&euro; ' + dati[14] + '</td><td></td><td ' + bold[19] + '>' + dati_giorni[19] + '20</td><td ' + bold[19] + '>&euro; ' + dati[19] + '</td><td></td><td ' + bold[24] + '>' + dati_giorni[24] + '25</td><td ' + bold[24] + '>&euro; ' + dati[24] + '</td><td></td><td ' + bold[29] + '>' + dati_giorni[29] + '30</td><td ' + bold[29] + '>&euro; ' + dati[29] + '</td></tr>';

                var numero_giorni_mese = calcola_giorni_mese(parseInt(mese_inviato), parseInt("20" + anno_inviato));
                if (numero_giorni_mese === 31) {
                    tabella_confronto_movimenti[numero_tasto_partenza] += '<tr class="questa_no"><td colspan="15"></td><td ' + bold[30] + '>' + dati_giorni[30] + '31</td><td ' + bold[30] + '>&euro; ' + dati[30] + '</td></tr>';
                }
                tabella_confronto_movimenti[numero_tasto_partenza] += '</table>';
                tabella_confronto_movimenti[numero_tasto_partenza] += '</div>';
                tabella_confronto_movimenti[numero_tasto_partenza] += '</div>';


                tabella_confronto_movimenti[numero_tasto_partenza] += '<div class="row">';

                /* String */
                var padding_top = " padding-top:1vh; ";
                if (numero_giorni_mese === 31) {
                    padding_top = " padding-top:0; ";
                }
                tabella_confronto_movimenti[numero_tasto_partenza] += '<div style="' + padding_top + 'font-size:1vh;text-align:center;">';
                tabella_confronto_movimenti[numero_tasto_partenza] += '<table style="width:100%;" id="tabella_movimenti_totali_mese_corrente">';
                tabella_confronto_movimenti[numero_tasto_partenza] += '<tr><td>Lunedì - Venerdì</td>                               <td>&euro; ' + totale_feriali.toFixed(2) + '</td>                           <td></td><td>Media Giornaliera (Lun-Ven)</td><td>&euro; ' + (totale_feriali / numero_giorni_mese).toFixed(2) + '</td><tr>';
                tabella_confronto_movimenti[numero_tasto_partenza] += '<tr><td>Sabato - Domenica</td>                              <td>&euro; ' + totale_weekend.toFixed(2) + '</td>                           <td></td><td>Media Giornaliera (Sab-Dom)</td><td>&euro; ' + (totale_weekend / numero_giorni_mese).toFixed(2) + '</td></tr>';
                tabella_confronto_movimenti[numero_tasto_partenza] += '<tr><td style="font-weight:bold;">TOTALE:</td>              <td style="font-weight:bold;">&euro; ' + totale_totale.toFixed(2) + '</td>  <td></td><td>Media Giornaliera (Totale)</td><td>&euro; ' + (totale_totale / numero_giorni_mese).toFixed(2) + '</td></tr>';
                tabella_confronto_movimenti[numero_tasto_partenza] += '</table>';
                tabella_confronto_movimenti[numero_tasto_partenza] += '</div>';
                tabella_confronto_movimenti[numero_tasto_partenza] += '</div>';

                $('#grafico_selezione_periodo_' + numero_tasto_partenza).html(mesi[mese_inviato - 1] + ' 20' + anno_inviato);

                //AGGIORNAMENTO LINEE
                grafico.update();
            });
        } else
        {
            alert('Se indichi un mese da confrontare con un altro mese, VA INSERITO anche l\'ANNO');
        }
    } else

    if (anno_inviato !== "Anno")
    {

        //LEGENDA ASSE X
        //grafico.config.options.scales.xAxes[0].scaleLabel.labelString = "Mesi";

        //VALORI ASSE X
        grafico.config.data.labels = mesi;

        var testo_query = "select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,4,2)='01' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,4,2)='02' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,4,2)='03' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,4,2)='04' and substr(data,7,2)='" + anno_inviato + "'\n\
  union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,4,2)='05' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,4,2)='06' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,4,2)='07' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,4,2)='08' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,4,2)='09' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,4,2)='10' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,4,2)='11' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,4,2)='12' and substr(data,7,2)='" + anno_inviato + "';";

        console.log("QUERY CONFRONTO", testo_query);

        //ANNO (TAB MESI)
        tabella_confronto_movimenti[numero_tasto_partenza] = "";


        comanda.sincro.query_movimenti(testo_query, 3000, function (dato) {

            console.log("QUERY CONFRONTO", dato);


            grafico.config.data.datasets[0].data = new Array();

            dato.forEach(function (value) {
                if (value.prezzo_tot === undefined || value.prezzo_tot === null) {
                    value.prezzo_tot = 0;
                } else
                {
                    //PER RISOLVERE ERRORE VIRGOLE
                    value.prezzo_tot = value.prezzo_tot.replace(",", "");
                    value.prezzo_tot = parseFloat(value.prezzo_tot);
                }

                console.log("QUERY CONFRONTO", value.prezzo_tot);

                //DATI GRAFICO - MANCA CICLO FOR
                grafico.config.data.datasets[0].data.push(parseFloat(value.prezzo_tot).toFixed(2));

                totale_totale += parseFloat(value.prezzo_tot);

            });

            var dati = grafico.config.data.datasets[0].data;


            tabella_confronto_movimenti[numero_tasto_partenza] += '<div class="row">';
            tabella_confronto_movimenti[numero_tasto_partenza] += '<div style="font-size:1vh;text-align:center;">';
            tabella_confronto_movimenti[numero_tasto_partenza] += '<table style="width:100%;" id="tabella_movimenti_anno_corrente">';

            tabella_confronto_movimenti[numero_tasto_partenza] += '<tr><td>Gennaio</td><td>&euro; ' + dati[0] + '</td><td></td>  <td>Aprile</td><td>&euro; ' + dati[3] + '</td><td></td>    <td>Luglio</td><td>&euro; ' + dati[6] + '</td><td></td>        <td>Ottobre</td><td>&euro; ' + dati[9] + '</td></tr>';
            tabella_confronto_movimenti[numero_tasto_partenza] += '<tr><td>Febbraio</td><td>&euro; ' + dati[1] + '</td><td></td> <td>Maggio</td><td>&euro; ' + dati[4] + '</td><td></td>    <td>Agosto</td><td>&euro; ' + dati[7] + '</td><td></td>        <td>Novembre</td><td>&euro; ' + dati[10] + '</td></tr>';
            tabella_confronto_movimenti[numero_tasto_partenza] += '<tr><td>Marzo</td><td>&euro; ' + dati[2] + '</td><td></td>    <td>Giugno</td><td>&euro; ' + dati[5] + '</td><td></td>    <td>Settembre</td><td>&euro; ' + dati[8] + '</td><td></td>     <td>Dicembre</td><td>&euro; ' + dati[11] + '</td></tr>';

            tabella_confronto_movimenti[numero_tasto_partenza] += '</table>';
            tabella_confronto_movimenti[numero_tasto_partenza] += '</div>';
            tabella_confronto_movimenti[numero_tasto_partenza] += '</div>';


            tabella_confronto_movimenti[numero_tasto_partenza] += '<div class="row">';

            tabella_confronto_movimenti[numero_tasto_partenza] += '<div style="padding-top:1vh;font-size:1vh;text-align:center;">';
            tabella_confronto_movimenti[numero_tasto_partenza] += '<table style="width:100%;" id="tabella_movimenti_totali_mese_corrente">';
            tabella_confronto_movimenti[numero_tasto_partenza] += '<tr><td style="font-weight:bold;">TOTALE:</td>              <td style="font-weight:bold;">&euro; ' + totale_totale.toFixed(2) + '</td>  <td></td><td>Media Mensile</td><td>&euro; ' + (totale_totale / 12).toFixed(2) + '</td></tr>';
            tabella_confronto_movimenti[numero_tasto_partenza] += '</table>';
            tabella_confronto_movimenti[numero_tasto_partenza] += '</div>';
            tabella_confronto_movimenti[numero_tasto_partenza] += '</div>';

            $('#grafico_selezione_periodo_' + numero_tasto_partenza).html('20' + anno_inviato);
            //AGGIORNAMENTO LINEE
            grafico.update();
        });

    } else
    {
        alert('Che periodo vuoi confrontare?');
    }

}


function stampa_periodo(numero_tasto_partenza)
{

    var giorno_inviato = $("#pagina_movimenti [name=giorno_" + numero_tasto_partenza + "]").val();
    var mese_inviato = $("#pagina_movimenti [name=mese_" + numero_tasto_partenza + "]").val();
    var anno_inviato = $("#pagina_movimenti [name=anno_" + numero_tasto_partenza + "]").val();

    //Se scelgo l'anno come confronto vedo i mesi                     
    var mesi = ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'];

    //Se scelgo i mesi vedo i giorni
    var giorni = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'];

    //Se scelgo i giorni solo quel giorno
    var ore = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24'];

    if (giorno_inviato !== 'Giorno')
    {
        if (mese_inviato !== "Mese")
        {
            if (anno_inviato !== "Anno")
            {

                var testo_query = "select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='01'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='02'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='03'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='04'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='05'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='06'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='07'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='08'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='09'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='10'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='11'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='12'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='13'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='14'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='15'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='16'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='17'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='18'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='19'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='20'\n\
 union all\n\
 \select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='21'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='22'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='23'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='" + giorno_inviato + "' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "' and substr(ora,1,2)='24'";

                comanda.sincro.query_movimenti(testo_query, 3000, function (dato) {

                    var builder = new epson.ePOSBuilder();

                    builder.addTextFont(builder.FONT_A);
                    builder.addTextAlign(builder.ALIGN_CENTER);
                    builder.addTextSize(2, 2);
                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                    builder.addText('STATISTICHE\n');
                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                    builder.addTextSize(1, 1);
                    builder.addText('__________________________________________\n');
                    builder.addText('__________________________________________\n');
                    builder.addTextSize(2, 2);
                    builder.addFeedLine(1);
                    builder.addText(giorno_inviato + ' ' + mesi[mese_inviato - 1] + ' 20' + anno_inviato + '\n');
                    builder.addFeedLine(1);

                    builder.addText(comanda.locale + '\n');
                    builder.addTextAlign(builder.ALIGN_LEFT);
                    builder.addTextSize(1, 1);

                    builder.addFeedLine(1);
                    builder.addText('__________________________________________\n');
                    builder.addFeedLine(1);

                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                    builder.addTextPosition(0);
                    builder.addText('ORA');

                    builder.addTextPosition(400);
                    builder.addText('IMPORTO');
                    builder.addText('\n');
                    builder.addFeedLine(1);
                    builder.addTextStyle(false, false, false, builder.COLOR_1);

                    var i = 1;
                    var totale = 0;

                    dato.forEach(function (value) {
                        if (value.prezzo_tot === undefined || value.prezzo_tot === null) {
                            value.prezzo_tot = 0;
                        } else
                        {
                            //PER RISOLVERE ERRORE VIRGOLE
                            value.prezzo_tot = value.prezzo_tot.replace(",", "");
                            totale += parseFloat(value.prezzo_tot);
                            value.prezzo_tot = parseFloat(value.prezzo_tot);
                        }

                        builder.addTextPosition(0);
                        builder.addText(aggZero(i, 2) + ":00");
                        builder.addTextPosition(200);
                        //builder.addText(value.quantita_tot);
                        builder.addTextPosition(400);
                        builder.addText("€ " + parseFloat(value.prezzo_tot).toFixed(2) + '\n');

                        i++;

                    });

                    builder.addFeedLine(1);
                    builder.addText('__________________________________________\n');
                    builder.addFeedLine(1);

                    builder.addTextPosition(0);

                    builder.addTextStyle(false, false, true, builder.COLOR_1);

                    builder.addText('TOTALE:');

                    builder.addTextPosition(400);

                    builder.addText('€ ' + parseFloat(totale).toFixed(2));
                    builder.addText('\n');
                    builder.addTextStyle(false, false, false, builder.COLOR_1);

                    builder.addFeedLine(1);
                    builder.addFeedLine(2);
                    builder.addCut(builder.CUT_FEED);

                    var request = builder.toString();

                    //Create a SOAP envelop
                    var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
                    //Create an XMLHttpRequest object
                    var xhr = new XMLHttpRequest();
                    //Set the end point address
                    var url = 'http://' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
//Open an XMLHttpRequest object
                    if (comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].intelligent === "SDS") {
                        url = 'http://' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "") + numero_layout].ip + ':8000/SPOOLER?stampante=' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].devid_nf;
                    }
                    //Open an XMLHttpRequest object
                    xhr.open('POST', url, true);
                    //<Header settings>
                    xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
                    xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
                    xhr.setRequestHeader('SOAPAction', '""');

                    xhr.send(soap);
                });

            } else
            {
                alert('Per confrontare i dati di una giornata, DEVI INSERIRE anche l\'ANNO');
            }
        } else
        {
            alert('Per confrontare i dati di una giornata, DEVI INSERIRE il MESE e l\'ANNO');
        }
    } else

    if (mese_inviato !== "Mese")
    {
        if (anno_inviato !== "Anno")
        {


            var testo_query = "select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='01' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='02' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='03' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='04' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='05' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='06' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='07' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='08' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='09' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='10' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='11' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='12' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='13' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='14' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='15' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='16' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='17' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='18' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='19' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='20' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='21' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='22' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='23' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='24' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='25' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='26' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='27' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='28' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='29' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='30' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,1,2)='31' and substr(data,4,2)='" + mese_inviato + "' and substr(data,7,2)='" + anno_inviato + "';";


            console.log("QUERY CONFRONTO", testo_query);

            comanda.sincro.query_movimenti(testo_query, 3000, function (dato) {
                var builder = new epson.ePOSBuilder();

                builder.addTextFont(builder.FONT_A);
                builder.addTextAlign(builder.ALIGN_CENTER);
                builder.addTextSize(2, 2);
                builder.addText('STATISTICHE\n');
                builder.addTextStyle(false, false, false, builder.COLOR_1);
                builder.addTextSize(1, 1);
                builder.addText('__________________________________________\n');
                builder.addText('__________________________________________\n');
                builder.addTextSize(2, 2);
                builder.addFeedLine(1);
                builder.addText(mesi[mese_inviato - 1] + ' 20' + anno_inviato + '\n');
                builder.addFeedLine(1);
                builder.addText(comanda.locale + '\n');
                builder.addTextAlign(builder.ALIGN_LEFT);
                builder.addTextSize(1, 1);

                builder.addFeedLine(1);
                builder.addText('__________________________________________\n');
                builder.addFeedLine(1);

                builder.addTextStyle(false, false, true, builder.COLOR_1);
                builder.addTextPosition(0);
                builder.addText('GIORNO');
                builder.addTextPosition(400);
                builder.addText('IMPORTO');
                builder.addText('\n');
                builder.addFeedLine(1);
                builder.addTextStyle(false, false, false, builder.COLOR_1);

                var i = 1;
                var totale = 0;
                var totale_weekend = 0;
                var totale_feriale = 0;

                dato.forEach(function (value) {
                    if (value.prezzo_tot === undefined || value.prezzo_tot === null) {
                        value.prezzo_tot = 0;
                    } else
                    {
                        value.prezzo_tot = value.prezzo_tot.replace(",", "");
                        totale += parseFloat(value.prezzo_tot);
                        value.prezzo_tot = parseFloat(value.prezzo_tot);
                    }

                    if (torna_giorno(aggZero(i, 2), mese_inviato, '20' + anno_inviato) === 'Domenica' || torna_giorno(aggZero(i, 2), mese_inviato, '20' + anno_inviato) === 'Sabato') {
                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                        totale_weekend += parseFloat(value.prezzo_tot);
                    } else
                    {
                        totale_feriale += parseFloat(value.prezzo_tot);
                    }
                    builder.addTextPosition(0);
                    builder.addText(torna_giorno(aggZero(i, 2), mese_inviato, '20' + anno_inviato) + ' ' + aggZero(i, 2));
                    builder.addTextPosition(200);
                    //builder.addText(value.quantita_tot);
                    builder.addTextPosition(400);
                    builder.addText("€ " + parseFloat(value.prezzo_tot).toFixed(2) + '\n');
                    builder.addTextStyle(false, false, false, builder.COLOR_1);

                    i++;

                });

                builder.addFeedLine(1);
                builder.addText('__________________________________________\n');
                builder.addFeedLine(1);

                builder.addTextPosition(0);
                builder.addTextStyle(false, false, true, builder.COLOR_1);
                builder.addText('LUNEDI-VENERDI:');
                builder.addTextPosition(400);
                builder.addText('€ ' + parseFloat(totale_feriale).toFixed(2));
                builder.addText('\n');
                builder.addTextStyle(false, false, false, builder.COLOR_1);

                builder.addTextPosition(0);
                builder.addTextStyle(false, false, true, builder.COLOR_1);
                builder.addText('SABATO-DOMENICA:');
                builder.addTextPosition(400);
                builder.addText('€ ' + parseFloat(totale_weekend).toFixed(2));
                builder.addText('\n');
                builder.addTextStyle(false, false, false, builder.COLOR_1);

                builder.addTextPosition(0);
                builder.addTextStyle(false, false, true, builder.COLOR_1);
                builder.addText('TOTALE:');
                builder.addTextPosition(400);
                builder.addText('€ ' + parseFloat(totale).toFixed(2));
                builder.addText('\n');
                builder.addTextStyle(false, false, false, builder.COLOR_1);

                builder.addFeedLine(1);
                builder.addFeedLine(2);
                builder.addCut(builder.CUT_FEED);

                var request = builder.toString();

                //Create a SOAP envelop
                var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
                //Create an XMLHttpRequest object
                var xhr = new XMLHttpRequest();
                //Set the end point address
                var url = 'http://' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
//Open an XMLHttpRequest object
                if (comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].intelligent === "SDS") {
                    url = 'http://' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "") + numero_layout].ip + ':8000/SPOOLER?stampante=' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].devid_nf;
                }
                //Open an XMLHttpRequest object
                xhr.open('POST', url, true);
                //<Header settings>
                xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
                xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
                xhr.setRequestHeader('SOAPAction', '""');

                xhr.send(soap);
            });
        } else
        {
            alert('Se indichi un mese da confrontare con un altro mese, VA INSERITO anche l\'ANNO');
        }
    } else

    if (anno_inviato !== "Anno")
    {


        var testo_query = "select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,4,2)='01' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,4,2)='02' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,4,2)='03' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,4,2)='04' and substr(data,7,2)='" + anno_inviato + "'\n\
  union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,4,2)='05' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,4,2)='06' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,4,2)='07' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,4,2)='08' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,4,2)='09' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,4,2)='10' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,4,2)='11' and substr(data,7,2)='" + anno_inviato + "'\n\
 union all\n\
 select sum(replace(totale,',','.')) as prezzo_tot from record_teste where substr(data,4,2)='12' and substr(data,7,2)='" + anno_inviato + "';";

        console.log("QUERY CONFRONTO", testo_query);

        comanda.sincro.query_movimenti(testo_query, 3000, function (dato) {

            var builder = new epson.ePOSBuilder();

            builder.addTextFont(builder.FONT_A);
            builder.addTextAlign(builder.ALIGN_CENTER);
            builder.addTextSize(2, 2);
            builder.addText('STATISTICHE\n');
            builder.addTextStyle(false, false, false, builder.COLOR_1);
            builder.addTextSize(1, 1);
            builder.addText('__________________________________________\n');
            builder.addText('__________________________________________\n');
            builder.addTextSize(2, 2);
            builder.addFeedLine(1);
            builder.addText('20' + anno_inviato + '\n');
            builder.addFeedLine(1);
            builder.addText(comanda.locale + '\n');
            builder.addTextAlign(builder.ALIGN_LEFT);
            builder.addTextSize(1, 1);

            builder.addFeedLine(1);
            builder.addText('__________________________________________\n');
            builder.addFeedLine(1);

            builder.addTextStyle(false, false, true, builder.COLOR_1);
            builder.addTextPosition(0);
            builder.addText('MESE');

            builder.addTextPosition(400);
            builder.addText('IMPORTO');
            builder.addText('\n');
            builder.addFeedLine(1);
            builder.addTextStyle(false, false, false, builder.COLOR_1);

            var i = 1;
            var totale = 0;

            dato.forEach(function (value) {
                if (value.prezzo_tot === undefined || value.prezzo_tot === null) {
                    value.prezzo_tot = 0;
                } else
                {
                    value.prezzo_tot = value.prezzo_tot.replace(",", "");
                    totale += parseFloat(value.prezzo_tot);
                    value.prezzo_tot = parseFloat(value.prezzo_tot);
                }

                builder.addTextPosition(0);
                builder.addText(mesi[i - 1]);
                builder.addTextPosition(200);
                //builder.addText(value.quantita_tot);
                builder.addTextPosition(400);
                builder.addText("€ " + parseFloat(value.prezzo_tot).toFixed(2) + '\n');

                i++;

            });

            builder.addFeedLine(1);
            builder.addText('__________________________________________\n');
            builder.addFeedLine(1);

            builder.addTextPosition(0);

            builder.addTextStyle(false, false, true, builder.COLOR_1);

            builder.addText('TOTALE:');

            builder.addTextPosition(400);

            builder.addText('€ ' + parseFloat(totale).toFixed(2));
            builder.addText('\n');
            builder.addTextStyle(false, false, false, builder.COLOR_1);

            builder.addFeedLine(1);
            builder.addFeedLine(2);
            builder.addCut(builder.CUT_FEED);

            var request = builder.toString();

            //Create a SOAP envelop
            var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
            //Create an XMLHttpRequest object
            var xhr = new XMLHttpRequest();
            //Set the end point address
            var url = 'http://' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
//Open an XMLHttpRequest object
            if (comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].intelligent === "SDS") {
                url = 'http://' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "") + numero_layout].ip + ':8000/SPOOLER?stampante=' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].devid_nf;
            }
            //Open an XMLHttpRequest object
            xhr.open('POST', url, true);
            //<Header settings>
            xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
            xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
            xhr.setRequestHeader('SOAPAction', '""');

            xhr.send(soap);
        });

    } else
    {
        alert('Che periodo vuoi confrontare?');
    }

}