function mod_articolo(opz) {

    console.log("DEBUG MOD.ART. - OPZ: " + opz);
    var codice_articolo;
    var desc_art = $('#popup_mod_art [name="desc_art"]').val().toUpperCase().replace(/'/gi, '').replace(/"/gi, '');
    console.log("DEBUG MOD.ART. - DESC_ART: " + desc_art);
    var quantita = $('#popup_mod_art [name="quantita"]').val().replace(/'/gi, '').replace(/"/gi, '');
    console.log("DEBUG MOD.ART. - QUANTITA': " + quantita);
    var prezzo_un = parseFloat($('#popup_mod_art [name="prezzo_1"]').val().replace(/'/gi, '').replace(/"/gi, '')).toFixed(2);
    console.log("DEBUG MOD.ART. - PREZZO_UN: " + prezzo_un);
    var dest_stampa = $('#popup_mod_art [name="dest_stampa"]').val().replace(/'/gi, '').replace(/"/gi, '');
    console.log("DEBUG MOD.ART. - DEST_STAMPA: " + dest_stampa);
    var portata = $('#popup_mod_art [name="portata"]').val().replace(/'/gi, '').replace(/"/gi, '');
    console.log("DEBUG MOD.ART. - PORTATA: " + portata);
    $('#popup_sposta_articolo').modal('hide');
    var differenza = comanda.qt_partenza - quantita;
    console.log("DEBUG MOD.ART. - DIFFERENZA: " + differenza);


    var testo_query = "SELECT * FROM comanda WHERE prog_inser='" + comanda.ultimo_progressivo_articolo + "' and ntav_comanda='" + comanda.tavolo + "' AND stato_record='ATTIVO' order by nodo asc; ";

    comanda.sincro.query(testo_query, function (risultato) {

        console.log("DEBUG MOD.ART. - TESTO QUERY 1: " + testo_query, risultato);

        switch (true) {
            case differenza < 0:
                testo_query = "SELECT null;";
                quantita = differenza * -1;
                console.log("DEBUG MOD.ART. - CASE 1");
                break;
            case differenza > 0:
                testo_query = "DELETE FROM comanda WHERE ntav_comanda='" + comanda.tavolo + "' AND stato_record='ATTIVO' AND (contiene_variante is null OR contiene_variante='') AND desc_art='" + risultato[0].desc_art + "' and portata='" + risultato[0].portata + "' and cast(prog_inser as int)>=(select prog_inser from comanda where ntav_comanda='" + comanda.tavolo + "' AND stato_record='ATTIVO' AND (contiene_variante is null OR contiene_variante='') AND desc_art='" + risultato[0].desc_art + "' and portata='" + risultato[0].portata + "' order by cast(prog_inser as int) desc limit " + (differenza - 1) + ",0); ";
                quantita = 0;
                console.log("DEBUG MOD.ART. - CASE 2");
                break;
            case differenza === 0:
                testo_query = "SELECT null;";
                quantita = 0;
                console.log("DEBUG MOD.ART. - CASE 3");
                break;
        }

        console.log("DEBUG MOD.ART. - FINE SWITCH: ", testo_query, "QUANTITA: ", quantita);


        comanda.sock.send({tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
        comanda.sincro.query(testo_query, function () {

            var tot = risultato.length;
            console.log("DEBUG MOD.ART. - TOT: " + tot);

            var i = 0;

            var promessa1 = new Promise(
                    function (resolve, reject) {
                        risultato.forEach(function (obj) {
                            console.log("DEBUG MOD.ART. - RISULTATO FOREACH");

                            i++;

                            console.log("DEBUG MOD.ART. - I: " + i);

                            codice_articolo = obj.cod_articolo;

                            console.log("DEBUG MOD.ART. - CODICE ARTICOLO: " + codice_articolo);

                            //VARIANTE
                            if (obj.desc_art[0] === "+" || obj.desc_art[0] === "-" || obj.desc_art[0] === "(" || obj.desc_art[0] === "*" || obj.desc_art[0] === "x" || obj.desc_art[0] === "=")
                            {

                                console.log("DEBUG MOD.ART. - IF 1");

                                if (i === tot) {

                                    console.log("DEBUG MOD.ART. - RESOLVE 1 -- PROMISE 1");

                                    resolve(true);
                                }
                            } else
                            {

                                if (codice_articolo !== 'null') {

                                    console.log("DEBUG MOD.ART. - IF 2");

                                    aggiungi_articolo(obj.cod_articolo, desc_art, prezzo_un, null, quantita, null, null, '', comanda.tavolo, null, null, false, function () {
                                        console.log("DEBUG MOD.ART. - AGGIUNGI ARTICOLO 1: ", obj.cod_articolo, desc_art, prezzo_un, null, quantita, null, null, '', comanda.tavolo, null, null, false);

                                        //Quantita è la quantità aggiuntiva eventuale
                                        testo_query = "SELECT desc_art,nodo,prog_inser,portata FROM COMANDA WHERE ntav_comanda='" + comanda.tavolo + "' AND stato_record='ATTIVO' ORDER BY cast (prog_inser as int) DESC LIMIT " + quantita + ";";
                                        comanda.sincro.query(testo_query, function (r2) {
                                            console.log("DEBUG MOD.ART. - TESTO QUERY 2: " + testo_query, r2);

                                            testo_query = "UPDATE COMANDA SET portata='" + portata + "' WHERE ntav_comanda='" + comanda.tavolo + "' AND stato_record='ATTIVO' and nodo ='" + comanda.clona_articolo_nodo + "';";
                                            comanda.sock.send({tipo: "aggiornamento_comanda", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                                            comanda.sincro.query(testo_query, function () {
                                                console.log("DEBUG MOD.ART. - TESTO QUERY 3: " + testo_query);


                                                if (r2 !== undefined && r2.length > 0 && quantita > 0) {
                                                    console.log("DEBUG MOD.ART. - IF 2");

                                                    var pp = r2[r2.length - 1].prog_inser;
                                                    console.log("DEBUG MOD.ART. - PP: ", pp);

                                                    testo_query = "UPDATE COMANDA SET desc_art='" + desc_art + "', prezzo_un='" + prezzo_un + "',dest_stampa='" + dest_stampa + "',portata='" + portata + "' WHERE ntav_comanda='" + comanda.tavolo + "' AND stato_record='ATTIVO' and cast(prog_inser as int)>='" + pp + "';";
                                                    console.log("DEBUG MOD.ART. - TESTO QUERY 4: " + testo_query);

                                                } else
                                                {
                                                    testo_query = "select null";
                                                    console.log("DEBUG MOD.ART. - TESTO QUERY 4: " + testo_query);

                                                }
                                                comanda.sock.send({tipo: "aggiornamento_comanda", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                                                comanda.sincro.query(testo_query, function () {


                                                    if (i === tot) {
                                                        console.log("DEBUG MOD.ART. - IF 3");

                                                        if (risultato[0].contiene_variante === null || risultato[0].contiene_variante === 'null' || risultato[0].contiene_variante === '')
                                                        {
                                                            testo_query = "UPDATE COMANDA SET dest_stampa='" + dest_stampa + "',desc_art='" + desc_art + "', prezzo_un='" + prezzo_un + "' WHERE ntav_comanda='" + comanda.tavolo + "' AND stato_record='ATTIVO' and portata='" + portata + "' and desc_art='" + comanda.desc_art_partenza + "' and (contiene_variante is null OR contiene_variante='null' or contiene_variante='');";
                                                            console.log("DEBUG MOD.ART. - IF 4", testo_query);

                                                        } else {

                                                            testo_query = "UPDATE COMANDA SET dest_stampa='" + dest_stampa + "',portata='" + portata + "' WHERE ntav_comanda='" + comanda.tavolo + "' AND stato_record='ATTIVO' and portata='" + risultato[0].portata + "' and substr(nodo,1,3)='" + comanda.clona_articolo_nodo + "';";
                                                            console.log("DEBUG MOD.ART. - IF 4", testo_query);
                                                        }

                                                        comanda.sock.send({tipo: "aggiornamento_comanda", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                                                        comanda.sincro.query(testo_query, function () {
                                                            console.log("DEBUG MOD.ART. - RESOLVE 2 -- PROMISE 1");
                                                            resolve(true);

                                                        });
                                                    }
                                                });
                                            });
                                        });
                                    });

                                } else
                                {
                                    var form = "categoria=" + obj.categoria + "&descrizione=" + obj.desc_art + "&prezzo_1=" + obj.prezzo_un + "&iva=" + obj.perc_iva + "&dest_stampa=" + obj.dest_stampa + "&portata=" + obj.portata;
                                    aggiungi_articolo(null, desc_art, prezzo_un, form, quantita, null, null, '', comanda.tavolo, obj.perc_iva, obj.perc_iva, false, function () {
                                        console.log("DEBUG MOD.ART. - AGGIUNGI ARTICOLO 2: ", null, desc_art, prezzo_un, form, quantita, null, null, '', comanda.tavolo, obj.perc_iva, obj.perc_iva, false);

                                        //Quantita è la quantità aggiuntiva eventuale
                                        testo_query = "SELECT desc_art,nodo,prog_inser,portata FROM COMANDA WHERE ntav_comanda='" + comanda.tavolo + "' AND stato_record='ATTIVO' ORDER BY cast (prog_inser as int) DESC LIMIT " + quantita + ";";
                                        comanda.sincro.query(testo_query, function (r2) {
                                            console.log("DEBUG MOD.ART. - TESTO QUERY 5: " + testo_query, r2);

                                            testo_query = "UPDATE COMANDA SET portata='" + portata + "' WHERE ntav_comanda='" + comanda.tavolo + "' AND stato_record='ATTIVO' and nodo ='" + comanda.clona_articolo_nodo + "';";
                                            comanda.sock.send({tipo: "aggiornamento_comanda", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                                            comanda.sincro.query(testo_query, function () {

                                                console.log("DEBUG MOD.ART. - TESTO QUERY 6: " + testo_query);

                                                if (r2 !== undefined && r2.length > 0 && quantita > 0 && (obj.desc_art[0] === "+" || obj.desc_art[0] === "-" || obj.desc_art[0] === "(" || obj.desc_art[0] === "*" || obj.desc_art[0] === "x" || obj.desc_art[0] === "=")) {
                                                    var pp = r2[r2.length - 1].prog_inser;
                                                    testo_query = "UPDATE COMANDA SET desc_art='" + desc_art + "', prezzo_un='" + prezzo_un + "',dest_stampa='" + dest_stampa + "',portata='" + portata + "' WHERE ntav_comanda='" + comanda.tavolo + "' AND stato_record='ATTIVO' and cast(prog_inser as int)>='" + pp + "';";

                                                    console.log("DEBUG MOD.ART. - IF 5 PP/TESTO_QUERY", pp, testo_query);
                                                } else
                                                {
                                                    testo_query = "select null";
                                                    console.log("DEBUG MOD.ART. - IF 5 PP/TESTO_QUERY", testo_query);

                                                }
                                                comanda.sock.send({tipo: "aggiornamento_comanda", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                                                comanda.sincro.query(testo_query, function () {
                                                    console.log("DEBUG MOD.ART. - TESTO QUERY 6: " + testo_query);


                                                    if (i === tot) {
                                                        console.log("DEBUG MOD.ART. - IF 6");
                                                        if (risultato[0].contiene_variante === null || risultato[0].contiene_variante === 'null' || risultato[0].contiene_variante === '')
                                                        {
                                                            testo_query = "UPDATE COMANDA SET dest_stampa='" + dest_stampa + "',desc_art='" + desc_art + "', prezzo_un='" + prezzo_un + "' WHERE ntav_comanda='" + comanda.tavolo + "' AND stato_record='ATTIVO' and portata='" + portata + "' and desc_art='" + comanda.desc_art_partenza + "' and (contiene_variante is null OR contiene_variante='null' or contiene_variante='');";
                                                        } else {

                                                            testo_query = "UPDATE COMANDA SET dest_stampa='" + dest_stampa + "',portata='" + portata + "' WHERE ntav_comanda='" + comanda.tavolo + "' AND stato_record='ATTIVO' and portata='" + risultato[0].portata + "' and substr(nodo,1,3)='" + comanda.clona_articolo_nodo + "';";
                                                        }

                                                        console.log("DEBUG MOD.ART. - TESTO QUERY 7", testo_query);


                                                        comanda.sock.send({tipo: "aggiornamento_comanda", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                                                        comanda.sincro.query(testo_query, function () {
                                                            
                                                                                                                    console.log("DEBUG MOD.ART. - QUERY 7 ESEGUITA");

                                                            console.log("DEBUG MOD.ART. - RESOLVE 3 -- PROMISE 1");
                                                            resolve(true);
                                                        });
                                                    }
                                                });
                                            });
                                        });
                                    });
                                }






                            }

                        });
                    });
            //PROBLEMA LENTEZZA OPZ VAR
            //BISOGNA ASPETTARE AGGIUNGI ARTICOLO
            //DI CUI E' MOLTO DIFFICILE FARE IL CALLBACK

            promessa1.then(function () {
                console.log("CALLBACK PROMESSA3");
                $('#popup_mod_art').modal('hide');
                if (comanda.popup_modifica_articolo === true) {
                    $('#conto_grande').modal('show');
                }

                if (opz !== undefined) {
                    modifica_variante_da_popup(opz, function () {
                        if (opz === "+" || opz === "-")
                        {
                            mod_categoria(comanda.ultima_cat_variante, opz);
                        } else if (opz === '(') {
                            popup_scelte_specifiche();
                        } else if (opz === 'OMAGGIO') {
                            articolo_omaggio();
                        }

                    });
                }


                testo_query = "UPDATE COMANDA SET desc_art='" + desc_art + "', prezzo_un='" + prezzo_un + "' WHERE ntav_comanda='" + comanda.tavolo + "' AND stato_record='ATTIVO' and portata='" + risultato[0].portata + "' and prog_inser='" + comanda.ultimo_progressivo_articolo + "';";
                comanda.sock.send({tipo: "aggiornamento_comanda", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                comanda.sincro.query(testo_query, function () {


                    comanda.funzionidb.conto_attivo();
                });

            });
        });
    });
}