/* global comanda, epson, numero_layout, parseFloat, sunmiInnerPrinter, Promise */

function preconto(callback, scontrino_con_bonifico) {

    if (comanda.pizzeria_asporto !== true && comanda.tavolo.left(7) === "ASPORTO") {
        stampa(["conto"]);
    } else {

        comanda.provenienza_conto = true;
        var righe_conto = $('#conto tr').not('.non-contare-riga');
        var intestazioni_conto = $('#intestazioni_conto tr').not('.non-contare-riga');
        var intestazioni_conto_2 = $('#intestazioni_conto tr:contains(%)').not(':contains(' + comanda.lang[15] + ')');
        var intestazioni_conto_3 = $('#intestazioni_conto tr').not(':contains(%)').not(':contains(' + comanda.lang[15] + ')');
        var totale_iniziale = $('#totale_scontrino').html();
        var numero_coperti = $('#numero_coperti_effettivi').html();
        var numero_paganti = $("#numero_paganti").html();


        var tavolo = comanda.tavolo;
        var parcheggio = comanda.parcheggio;

        var settaggi_profili = alasql("select Field414 from settaggi_profili where id=" + comanda.folder_number + " ;");
        var settaggi_profilo = settaggi_profili[0];

        if (scontrino_con_bonifico === true) {

            var numero_riferimento_scontrino = "ND";
            var xml = '<printerCommand><directIO  command="1070" data="01" /></printerCommand>';
            var epos = new epson.fiscalPrint();
            epos.send("http://" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=local_printer&timeout=30000", xml, 30000);
            epos.onreceive = function (result, tag_names_array, add_info, res_add) {
                console.log("CHIUDI SCONTRINO 3");
                numero_riferimento_scontrino = parseInt(add_info.responseData.substr(2, 4));


                if (comanda.compatibile_xml === true) {
                    stampa_konto();
                } else {

                    var promessa = new Promise(function (resolve, reject) {
                        if (comanda.conto_in_lingua === true) {
                            $("#popup_scelta_lingua").modal("show");

                            $(document).on(comanda.eventino, '#popup_scelta_lingua td', function (event) {
                                var lingua = event.target.getAttribute('lingua');

                                $('#popup_scelta_lingua').modal('hide');
                                $(document).off(comanda.eventino, '#popup_scelta_lingua td');
                                resolve(lingua);
                            });


                        } else
                        {
                            resolve(comanda.lingua_stampa);
                        }
                    });

                    promessa.then(function (lingua) {
                        if (settaggi_profilo.Field414 === "true") {
                            scelta_stampante_conto(function () {
                                funzione_interna(lingua, stampante);
                            });

                        } else {
                            funzione_interna(lingua);
                        }
                    });

                }
            };
        } else
        {
            if (settaggi_profilo.Field414 === "true") {
                scelta_stampante_conto(function (stampante) {
                    funzione_interna(comanda.lingua_stampa, stampante);
                });
            } else {
                funzione_interna(comanda.lingua_stampa);
            }
        }

        function scelta_stampante_conto(callback) {

            var lista_stampanti = "";

            var stampanti = alasql("select * from nomi_stampanti where italiano LIKE '%CONTO%';");

            stampanti.forEach(function (nome_stampante) {

                lista_stampanti += "<button class='btn btn-info' type='button' nome_stampante='" + nome_stampante.italiano + "'>" + nome_stampante.italiano + "<br>" + nome_stampante.nome + "</button>";

            });

            lista_stampanti += "";


            $("#popup_scelta_stampante_conto .modal-body").html(lista_stampanti);

            $("#popup_scelta_stampante_conto").modal("show");

            try {
                $("#popup_scelta_stampante_conto .modal-body button").off('click');
            } catch (e) {

            }

            $("#popup_scelta_stampante_conto .modal-body button").on(comanda.eventino, function () {

                $("#popup_scelta_stampante_conto").modal("hide");

                callback($(this).attr("nome_stampante"));

            });

        }

        function funzione_interna(lingua, eventuale_stampante = "") {

            //QUI MANCA UN SETTAGGIO NEL SENSO CHE SE E' CONTO PIU' COMANDA NON SO COSA SUCCEDA
            if (comanda.licenza === 'mattia131' && (parcheggio === 0 || parcheggio === '') && (tavolo.left(8) === "CONTINUO" || tavolo === "BANCO" || tavolo === "BAR" || tavolo.left(7) === "ASPORTO" || tavolo === "TAKE AWAY")) {

                comanda.conto_in_attesa = true;
                btn_parcheggia();

            } else {

                if (comanda.licenza === 'mattia131' && (tavolo.left(8) === "CONTINUO" || tavolo === "BANCO" || tavolo === "BAR" || tavolo.left(7) === "ASPORTO" || tavolo === "TAKE AWAY")) {
                    btn_parcheggia();
                }

                test_copertura(function (stato_copertura) {
                    if (stato_copertura === true) {

                        if (comanda.persona === null && tavolo === '*ASPORTO*') {

                            var query_prog = "select numero from progressivi_asporto limit 1;";
                            var n_prog = alasql(query_prog);

                            var progressivo_attuale = parseInt(n_prog[0].numero) + 1;
                            comanda.persona = progressivo_attuale;
                            var query_nuovo_prog = "update progressivi_asporto set numero='" + progressivo_attuale + "';";
                            alasql(query_nuovo_prog);

                            //resolve(1);

                            comanda.sock.send({tipo: "aggiornamento", operatore: comanda.operatore, query: query_nuovo_prog, terminale: comanda.terminale, ip: comanda.ip_address});
                            comanda.sincro.query_cassa();

                        } else if (comanda.persona !== null && tavolo === '*ASPORTO*')
                        {
                            //resolve(2);
                        } else
                        {
                            comanda.persona = '';
                            //resolve(3);
                        }



                        var query = "select * from settaggi_profili where id=" + comanda.folder_number + " ;";
                        comanda.sincro.query(query, function (settaggi_profili) {
                            settaggi_profili = settaggi_profili[0];
                            if (comanda.persona !== null) {

                                var stampante_tavolo = comanda.lang_stampa[60].replace(/\<br\/>/gi, "") + numero_layout;

                                var query_stampante_tavolo = "select n_st_conto from tavoli where numero='" + comanda.tavolo + "';";
                                var result_st_tav = alasql(query_stampante_tavolo);

                                if (result_st_tav[0] !== undefined && result_st_tav[0].n_st_conto !== undefined && result_st_tav[0].n_st_conto !== null && result_st_tav[0].n_st_conto !== "undefined" && result_st_tav[0].n_st_conto !== "null" && result_st_tav[0].n_st_conto.trim() !== "") {
                                    if (result_st_tav[0].n_st_conto === "1") {
                                        stampante_tavolo = comanda.lang_stampa[60].replace(/\<br\/>/gi, "");
                                    } else {
                                        stampante_tavolo = comanda.lang_stampa[60].replace(/\<br\/>/gi, "") + "_" + result_st_tav[0].n_st_conto;
                                    }
                                }

                                if (eventuale_stampante !== "") {
                                    stampante_tavolo = eventuale_stampante;
                                }

                                var data = comanda.funzionidb.data_attuale();
                                var builder = new epson.ePOSBuilder();

                                var android_object = new Array();

                                var parola_conto = comanda.lingue[lingua][60].replace(/\<br\/>/gi, "");

                                android_object.push({f: "printerInit", p: ""});
                                android_object.push({f: "setFontSize", p: "26"});
                                builder.addTextFont(builder.FONT_A);

                                android_object.push({f: "setAlignment", p: "1"});
                                builder.addTextAlign(builder.ALIGN_CENTER);


                                if (settaggi_profili.Field45.length > 0) {
                                    switch (settaggi_profili.Field46) {
                                        case "G":
                                            android_object.push({f: "BOLD", p: ""});
                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                            break;
                                        case "N":
                                        default:
                                            android_object.push({f: "UNBOLD", p: ""});
                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                    }

                                    android_object.push({f: "printOriginalText", p: settaggi_profili.Field45});
                                    android_object.push({f: "lineWrap", p: "1"});

                                    builder.addText(settaggi_profili.Field45 + '\n');
                                    if (settaggi_profili.Field47 === 'true') {

                                        android_object.push({f: "lineWrap", p: "1"});
                                        builder.addFeedLine(1);
                                    }
                                }


                                if (settaggi_profili.Field48.length > 0) {
                                    switch (settaggi_profili.Field49) {
                                        case "G":
                                            android_object.push({f: "BOLD", p: ""});
                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                            break;
                                        case "N":
                                        default:
                                            android_object.push({f: "UNBOLD", p: ""});
                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                    }

                                    android_object.push({f: "printOriginalText", p: settaggi_profili.Field48});
                                    android_object.push({f: "lineWrap", p: "1"});
                                    builder.addText(settaggi_profili.Field48 + '\n');
                                    if (settaggi_profili.Field50 === 'true') {
                                        android_object.push({f: "lineWrap", p: "1"});
                                        builder.addFeedLine(1);
                                    }
                                }


                                if (settaggi_profili.Field51.length > 0) {
                                    switch (settaggi_profili.Field52) {
                                        case "G":
                                            android_object.push({f: "BOLD", p: ""});
                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                            break;
                                        case "N":
                                        default:
                                            android_object.push({f: "UNBOLD", p: ""});
                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                    }

                                    android_object.push({f: "printOriginalText", p: settaggi_profili.Field51});
                                    android_object.push({f: "lineWrap", p: "1"});
                                    builder.addText(settaggi_profili.Field51 + '\n');


                                    if (settaggi_profili.Field53 === 'true') {
                                        android_object.push({f: "lineWrap", p: "1"});
                                        builder.addFeedLine(1);
                                    }
                                }


                                if (settaggi_profili.Field54.length > 0) {
                                    switch (settaggi_profili.Field55) {
                                        case "G":
                                            android_object.push({f: "BOLD", p: ""});
                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                            break;
                                        case "N":
                                        default:
                                            android_object.push({f: "UNBOLD", p: ""});
                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                    }
                                    android_object.push({f: "printOriginalText", p: settaggi_profili.Field54});
                                    android_object.push({f: "lineWrap", p: "1"});
                                    builder.addText(settaggi_profili.Field54 + '\n');
                                }

                                android_object.push({f: "lineWrap", p: "2"});
                                builder.addText('\n\n');
                                android_object.push({f: "setFontSize", p: "60"});
                                builder.addTextSize(2, 2);
                                if (tavolo === '*ASPORTO*' || tavolo === '*BAR*')
                                {
                                    android_object.push({f: "printOriginalText", p: parola_conto + ' ' + tavolo});
                                    android_object.push({f: "lineWrap", p: "1"});
                                    builder.addText(parola_conto + ' ' + tavolo + '\n');
                                    android_object.push({f: "lineWrap", p: "1"});
                                    builder.addFeedLine(1);
                                    if (tavolo === '*ASPORTO*') {
                                        android_object.push({f: "printOriginalText", p: comanda.persona});
                                        android_object.push({f: "lineWrap", p: "1"});
                                        builder.addText(comanda.persona + '\n');

                                    }
                                } else
                                {
                                    if (scontrino_con_bonifico !== true && settaggi_profili.Field56 === 'true') {
                                        android_object.push({f: "printOriginalText", p: parola_conto});
                                        android_object.push({f: "lineWrap", p: "1"});
                                        builder.addText(parola_conto);
                                    }

                                    //if (comanda.nome_stampante[stampante_tavolo].intelligent !== "SDS") {
                                    if (tavolo === 'CALENDARIO') {
                                        android_object.push({f: "lineWrap", p: "1"});
                                        builder.addText(' \n');
                                    } else if (tavolo.left(8) === 'CONTINUO' && checkEmptyVariable(parcheggio) === true) {
                                        android_object.push({f: "printOriginalText", p: ' - ' + parcheggio});
                                        android_object.push({f: "lineWrap", p: "1"});
                                        builder.addText(' \n' + parcheggio + '\n');
                                    } else if ((tavolo === 'BAR' || tavolo.left(7) === "ASPORTO" || tavolo === 'TAKEAWAY' || tavolo === 'TAKE AWAY') && settaggi_profili.Field105 === 'true') {
                                        android_object.push({f: "printOriginalText", p: ' - ' + parcheggio});
                                        android_object.push({f: "lineWrap", p: "1"});
                                        builder.addText(' \n' + parcheggio + '\n');
                                    } else if (tavolo !== 'BAR' && tavolo.left(7) !== "ASPORTO" && tavolo !== 'TAKEAWAY' && tavolo !== 'TAKE AWAY')
                                    {
                                        if (scontrino_con_bonifico === true) {
                                            android_object.push({f: "setFontSize", p: "60"});
                                            builder.addTextSize(2, 2);

                                            android_object.push({f: "printOriginalText", p: ' - ' + parcheggio});
                                            android_object.push({f: "lineWrap", p: "1"});
                                            builder.addText('COPIA PER ESERCENTE\n');
                                        } else {
                                            //android_object.push({f: "lineWrap", p: "1"});
                                            android_object.push({f: "printOriginalText", p: 'Tavolo n° ' + tavolo});
                                            android_object.push({f: "lineWrap", p: "1"});
                                            builder.addText('\nTavolo n° ' + tavolo + '\n');
                                        }
                                    } else {
                                        android_object.push({f: "lineWrap", p: "1"});
                                        builder.addText(' \n');
                                    }
                                    //}
                                }


                                builder.addTextFont(builder.FONT_A);

                                android_object.push({f: "setAlignment", p: "0"});
                                builder.addTextAlign(builder.ALIGN_LEFT);

                                android_object.push({f: "setFontSize", p: "26"});
                                builder.addTextSize(1, 1);

                                android_object.push({f: "lineWrap", p: "1"});
                                builder.addFeedLine(2);
                                android_object.push({f: "BOLD", p: ""});
                                builder.addTextStyle(false, false, true, builder.COLOR_1);


                                if (scontrino_con_bonifico === true) {
                                    android_object.push({f: "UNBOLD", p: ""});
                                    builder.addTextStyle(false, false, false, builder.COLOR_1);

                                    android_object.push({f: "printOriginalText", p: ' - ' + comanda.lingue[lingua][109].toLowerCase().capitalize() + ': ' + comanda.operatore});
                                    android_object.push({f: "lineWrap", p: "1"});

                                    builder.addText(comanda.lingue[lingua][109].toLowerCase().capitalize() + ': ' + comanda.operatore + '\n');

                                    android_object.push({f: "UNBOLD", p: ""});
                                    builder.addTextStyle(false, false, false, builder.COLOR_1);

                                    android_object.push({f: "printOriginalText", p: ' - ' + data.substr(0, 9).replace(/-/gi, '/')});
                                    android_object.push({f: "lineWrap", p: "1"});

                                    builder.addText(data.substr(0, 9).replace(/-/gi, '/') + '   ');

                                    android_object.push({f: "UNBOLD", p: ""});
                                    builder.addTextStyle(false, false, false, builder.COLOR_1);

                                    android_object.push({f: "printOriginalText", p: ' - ' + data.substr(9, 5)});
                                    android_object.push({f: "lineWrap", p: "1"});

                                    builder.addText(data.substr(9, 5));

                                    android_object.push({f: "lineWrap", p: "1"});

                                    builder.addText('\n');

                                    android_object.push({f: "printOriginalText", p: 'Tavolo: ' + tavolo});
                                    android_object.push({f: "lineWrap", p: "1"});

                                    builder.addText('Tavolo: ' + tavolo + '\n');

                                } else
                                {
                                    if (settaggi_profili.Field58 === 'true') {
                                        switch (settaggi_profili.Field59) {
                                            case "G":
                                                android_object.push({f: "BOLD", p: ""});
                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                break;
                                            case "N":
                                            default:
                                                android_object.push({f: "UNBOLD", p: ""});
                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                        }

                                        android_object.push({f: "printOriginalText", p: comanda.lingue[lingua][109].toLowerCase().capitalize() + ': ' + comanda.operatore});
                                        android_object.push({f: "lineWrap", p: "1"});

                                        builder.addText(comanda.lingue[lingua][109].toLowerCase().capitalize() + ': ' + comanda.operatore + '\n');
                                    }


                                    if (settaggi_profili.Field60 === 'true' || settaggi_profili.Field62 === 'true') {

                                        if (settaggi_profili.Field60 === 'true') {
                                            switch (settaggi_profili.Field63) {
                                                case "G":
                                                    android_object.push({f: "BOLD", p: ""});
                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                    break;
                                                case "N":
                                                default:
                                                    android_object.push({f: "UNBOLD", p: ""});
                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                            }

                                            android_object.push({f: "printOriginalText", p: data.substr(0, 9).replace(/-/gi, '/') + '   '});
                                            //android_object.push({f: "lineWrap", p: "1"});

                                            builder.addText(data.substr(0, 9).replace(/-/gi, '/') + '   ');
                                        }

                                        if (settaggi_profili.Field62 === 'true') {
                                            switch (settaggi_profili.Field63) {
                                                case "G":
                                                    android_object.push({f: "BOLD", p: ""});
                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                    break;
                                                case "N":
                                                default:
                                                    android_object.push({f: "UNBOLD", p: ""});
                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                            }

                                            android_object.push({f: "printOriginalText", p: data.substr(9, 5)});
                                            android_object.push({f: "lineWrap", p: "1"});

                                            builder.addText(data.substr(9, 5));
                                        }

                                        //android_object.push({f: "lineWrap", p: "1"});
                                        builder.addText('\n');
                                    }
                                }
                                android_object.push({f: "UNBOLD", p: ""});
                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                //android_object.push({f: "lineWrap", p: "1"});
                                builder.addFeedLine(1);
                                android_object.push({f: "setFontSize", p: "26"});
                                builder.addTextSize(1, 1);

                                builder.addTextAlign(builder.ALIGN_CENTER);

                                if (comanda.AndroidPrinter === true) {
                                    android_object.push({f: "setAlignment", p: "0"});
                                    android_object.push({f: "printOriginalText", p: "-------------------------------------------"});
                                    android_object.push({f: "lineWrap", p: "1"});
                                } else if (comanda.nome_stampante[stampante_tavolo].nome === 'TMT88-Vi') {
                                    android_object.push({f: "printOriginalText", p: "-----------------------------------------"});
                                    android_object.push({f: "lineWrap", p: "1"});
                                    builder.addText("-----------------------------------------\n");
                                } else if (comanda.nome_stampante[stampante_tavolo].nome === 'CUBO')
                                {
                                    android_object.push({f: "printOriginalText", p: "------------------------------------------------"});
                                    android_object.push({f: "lineWrap", p: "1"});
                                    builder.addText("------------------------------------------------\n");
                                } else if (comanda.nome_stampante[stampante_tavolo].nome === 'P20')
                                {
                                    android_object.push({f: "printOriginalText", p: "--------------------------------"});
                                    android_object.push({f: "lineWrap", p: "1"});
                                    builder.addText("--------------------------------\n");
                                } else
                                {
                                    android_object.push({f: "printOriginalText", p: "-----------------------------------------"});
                                    android_object.push({f: "lineWrap", p: "1"});
                                    builder.addText("-----------------------------------------\n");
                                }

                                android_object.push({f: "setAlignment", p: "0"});
                                builder.addTextAlign(builder.ALIGN_LEFT);
                                var quantita_totale = 0;
                                var prezzo_righe = 0;


                                //--------------------------------
                                builder.addTextFont(builder.FONT_B);
                                android_object.push({f: "UNBOLD", p: ""});
                                builder.addTextStyle(false, false, false, builder.COLOR_1);

                                var prodotto = "";
                                var quantita = "";
                                var prezzo_unitario = "";
                                var prezzo_totale = "";




                                righe_conto.each(function () {

                                    var rf_descrizione = "";
                                    var rf_quantita = "";
                                    var rf_prezzo_un = "";
                                    var rf_prezzo_tot;


                                    builder.addTextFont(builder.FONT_A);

                                    if (comanda.nome_stampante[stampante_tavolo].nome === 'P20')
                                    {
                                        builder.addTextFont(builder.FONT_B);
                                    }


                                    var articolo = $(this).find('td:nth-child(2)').html().replace(/&nbsp;/g, '');
                                    var quantita = $(this).find('td:nth-child(1)').html();
                                    var prezzo = $(this).find('td:nth-child(4)').html().replace(/&euro;/g, '');
                                    prezzo = prezzo.replace(/€/g, '');
                                    if (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(" && articolo[0] !== "*" && articolo[0] !== "x" && articolo[0] !== "=") {

                                        switch (settaggi_profili.Field66) {
                                            case "D":
                                                android_object.push({f: "setFontSize", p: "60"});
                                                builder.addTextSize(2, 2);
                                                break;
                                            case "G":
                                                android_object.push({f: "BOLD", p: ""});
                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                break;
                                            case "N":
                                            default:
                                                android_object.push({f: "UNBOLD", p: ""});
                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                        }
                                        if (articolo.indexOf('SERVIZIO') === -1) {
                                            // builder.addText(quantita);
                                            quantita_totale += parseInt(quantita);
                                        }
                                        // builder.addTextPosition(35);
                                    } else {
                                        switch (settaggi_profili.Field67) {
                                            case "D":
                                                android_object.push({f: "setFontSize", p: "60"});
                                                builder.addTextSize(2, 2);
                                                break;
                                            case "G":
                                                android_object.push({f: "BOLD", p: ""});
                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                break;
                                            case "N":
                                            default:
                                                android_object.push({f: "UNBOLD", p: ""});
                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                        }

                                        articolo = '   ' + articolo;
                                    }

                                    rf_descrizione = articolo;
                                    //builder.addText(a_capi(articolo));

                                    if (comanda.nome_stampante[stampante_tavolo].nome === 'TMT88-Vi') {
                                        builder.addTextPosition(230);
                                    } else if (comanda.nome_stampante[stampante_tavolo].nome === 'CUBO')
                                    {
                                        builder.addTextPosition(310);
                                    } else if (comanda.nome_stampante[stampante_tavolo].nome === 'P20')
                                    {



                                    } else
                                    {
                                        builder.addTextPosition(230);
                                    }


                                    rf_quantita = quantita;


                                    rf_prezzo_un = prezzo;

                                    var numero_caratteri = 42;
                                    if (comanda.nome_stampante[stampante_tavolo].nome === 'CUBO')
                                    {
                                        numero_caratteri = 48;
                                    }


                                    if (prezzo.substr(0, 1) !== "-") {
                                        rf_prezzo_tot = (parseFloat(prezzo) * parseFloat(quantita)).toFixed(2);
                                        prezzo_righe += prezzo * quantita;
                                    } else {
                                        rf_prezzo_tot = prezzo;
                                    }


                                    builder.addTextPosition(0);

                                    android_object.push({f: "printOriginalText", p: riga_conto_formattata(rf_descrizione, rf_quantita, rf_prezzo_un, rf_prezzo_tot, numero_caratteri)});
                                    //android_object.push({f: "lineWrap", p: "1"});

                                    builder.addText(riga_conto_formattata(rf_descrizione, rf_quantita, rf_prezzo_un, rf_prezzo_tot, numero_caratteri));

                                    builder.addTextFont(builder.FONT_A);
                                });

                                var cifra_sconto = 0;
                                intestazioni_conto.each(function () {

                                    var rf_descrizione = "";
                                    var rf_quantita = "";
                                    var rf_prezzo_un = "";
                                    var rf_prezzo_tot;

                                    var quantita = $(this).find('td:nth-child(1)').html();
                                    var prezzo = $(this).find('td:nth-child(4)').html().replace(/&euro;/g, '');
                                    var articolo = $(this).find('td:nth-child(2)').html().replace(/&nbsp;/g, '');
                                    if (articolo !== comanda.lingue[lingua][15].toUpperCase()) {
                                        articolo = $(this).find('td:nth-child(2)').html();
                                        if (prezzo.slice(-1) === '%') {
                                            articolo += ' ' + parseInt(prezzo.slice(1, -1)) + '%';
                                        } else {
                                            articolo += ' € ' + parseFloat(prezzo.substr(1)).toFixed(2);
                                        }
                                    } else
                                    {
                                        //CALCOLO COPERTI DA TOGLIERE DAL TOTALE PER SERVIZIO
                                        prezzo_righe += prezzo * quantita;
                                    }

                                    console.log("PREZZO SCONTO", prezzo);
                                    prezzo = prezzo.replace(/€/g, '');
                                    if (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(" && articolo[0] !== "*" && articolo[0] !== "x" && articolo[0] !== "=") {

                                        switch (settaggi_profili.Field66) {
                                            case "D":
                                                android_object.push({f: "setFontSize", p: "60"});
                                                builder.addTextSize(2, 2);
                                                break;
                                            case "G":
                                                android_object.push({f: "BOLD", p: ""});
                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                break;
                                            case "N":
                                            default:
                                                android_object.push({f: "UNBOLD", p: ""});
                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                        }
                                        //quantita_totale += parseInt(quantita);
                                        if (articolo.indexOf(comanda.lingue[lingua][15]) !== -1) {
                                            //builder.addText(quantita);
                                        }
                                        //builder.addTextPosition(35);
                                    } else {
                                        switch (settaggi_profili.Field67) {
                                            case "D":
                                                android_object.push({f: "setFontSize", p: "60"});
                                                builder.addTextSize(2, 2);
                                                break;
                                            case "G":
                                                android_object.push({f: "BOLD", p: ""});
                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                break;
                                            case "N":
                                            default:
                                                android_object.push({f: "UNBOLD", p: ""});
                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                        }

                                        articolo = '   ' + articolo;
                                    }

                                    rf_descrizione = articolo;



                                    if (prezzo.substr(0, 1) !== "-") {
                                        cifra_sconto = prezzo;

                                        rf_quantita = quantita;

                                        rf_prezzo_un = cifra_sconto;
                                    }

                                    var numero_caratteri = 42;
                                    if (comanda.nome_stampante[stampante_tavolo].nome === 'P20')
                                    {
                                        builder.addTextFont(builder.FONT_B);
                                    } else if (comanda.nome_stampante[stampante_tavolo].nome === 'CUBO')
                                    {

                                        numero_caratteri = 48;
                                    }

                                    if (prezzo.substr(0, 1) !== "-") {
                                        cifra_sconto = (parseFloat(prezzo) * parseFloat(quantita)).toFixed(2);
                                        rf_prezzo_tot = cifra_sconto;
                                    } else if (prezzo.slice(-1) === '%') {
                                        console.log("CALCOLO SCONTO PERC", prezzo_righe, prezzo.slice(1, -1), prezzo_righe / 100 * parseInt(prezzo.slice(1, -1)));
                                        cifra_sconto = (prezzo_righe / 100 * parseInt(prezzo.slice(1, -1))).toFixed(2);
                                        rf_prezzo_tot = cifra_sconto;
                                    } else {
                                        cifra_sconto = prezzo.substr(1);
                                        rf_prezzo_tot = cifra_sconto;
                                    }

                                    builder.addTextPosition(0);

                                    android_object.push({f: "printOriginalText", p: riga_conto_formattata(rf_descrizione, rf_quantita, rf_prezzo_un, rf_prezzo_tot, numero_caratteri)});
                                    android_object.push({f: "lineWrap", p: "1"});

                                    builder.addText(riga_conto_formattata(rf_descrizione, rf_quantita, rf_prezzo_un, rf_prezzo_tot, numero_caratteri));
                                });

                                builder.addTextFont(builder.FONT_A);


                                builder.addTextAlign(builder.ALIGN_CENTER);

                                if (comanda.AndroidPrinter === true) {
                                    android_object.push({f: "setAlignment", p: "0"});
                                    android_object.push({f: "printOriginalText", p: "-------------------------------------------"});
                                    android_object.push({f: "lineWrap", p: "1"});
                                } else if (comanda.nome_stampante[stampante_tavolo].nome === 'TMT88-Vi') {
                                    android_object.push({f: "printOriginalText", p: "-----------------------------------------"});
                                    android_object.push({f: "lineWrap", p: "1"});
                                    builder.addText("-----------------------------------------\n");
                                } else if (comanda.nome_stampante[stampante_tavolo].nome === 'CUBO')
                                {
                                    android_object.push({f: "printOriginalText", p: "------------------------------------------------"});
                                    android_object.push({f: "lineWrap", p: "1"});
                                    builder.addText("------------------------------------------------\n");
                                } else if (comanda.nome_stampante[stampante_tavolo].nome === 'P20')
                                {
                                    android_object.push({f: "printOriginalText", p: "--------------------------------"});
                                    android_object.push({f: "lineWrap", p: "1"});
                                    builder.addText("--------------------------------\n");
                                } else
                                {
                                    android_object.push({f: "printOriginalText", p: "-----------------------------------------"});
                                    android_object.push({f: "lineWrap", p: "1"});
                                    builder.addText("-----------------------------------------\n");
                                }

                                android_object.push({f: "setAlignment", p: "0"});
                                builder.addTextAlign(builder.ALIGN_LEFT);
                                //totale
                                //tasse
                                builder.addFeedLine(1);
                                android_object.push({f: "BOLD", p: ""});
                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                //QUANTITA TOTALE
                                if (settaggi_profili.Field68 === 'true') {

                                    android_object.push({f: "printOriginalText", p: quantita_totale.toString()});


                                    builder.addText(quantita_totale);
                                    builder.addTextPosition(35);

                                    android_object.push({f: "printOriginalText", p: " prodotti"});
                                    android_object.push({f: "lineWrap", p: "1"});

                                    builder.addText("prodotti");
                                }


                                //TOTALE




                                builder.addTextPosition(160);

                                if (comanda.nome_stampante[stampante_tavolo].nome === 'TMT88-Vi') {
                                    builder.addTextPosition(280);
                                } else if (comanda.nome_stampante[stampante_tavolo].nome === 'CUBO')
                                {
                                    builder.addTextPosition(360);

                                }

                                android_object.push({f: "printOriginalText", p: comanda.lingue[lingua][70].capitalize() + "                          "});


                                builder.addText(comanda.lingue[lingua][70].capitalize() + ': ');

                                android_object.push({f: "printOriginalText", p: '€ ' + riga_conto_totale(totale_iniziale)});
                                android_object.push({f: "lineWrap", p: "1"});

                                builder.addText('€ ' + riga_conto_totale(totale_iniziale) + '\n');


                                if (settaggi_profili.Field359 !== "true" && numero_paganti !== "" && parseInt(numero_coperti) > 1) {
                                    android_object.push({f: "lineWrap", p: "1"});
                                    builder.addFeedLine(1);
                                }
                                //A PERSONA
                                if (!(settaggi_profili.Field359 === "true" && numero_paganti !== "") && parseInt(numero_coperti) > 1) {


                                    builder.addTextPosition(125);

                                    if (comanda.nome_stampante[stampante_tavolo].nome === 'TMT88-Vi') {
                                        builder.addTextPosition(245);
                                    } else if (comanda.nome_stampante[stampante_tavolo].nome === 'CUBO')
                                    {
                                        builder.addTextPosition(323);

                                    }

                                    android_object.push({f: "printOriginalText", p: 'A persona: '});
                                    android_object.push({f: "lineWrap", p: "1"});

                                    builder.addText('A persona: ');

                                    android_object.push({f: "printOriginalText", p: "€ " + riga_conto_totale(parseFloat(parseFloat(totale_iniziale) / parseInt(numero_coperti)).toFixed(2))});
                                    android_object.push({f: "lineWrap", p: "1"});

                                    builder.addText("€ " + riga_conto_totale(parseFloat(parseFloat(totale_iniziale) / parseInt(numero_coperti)).toFixed(2)));
                                }

                                if (numero_paganti !== undefined && numero_paganti !== "") {
                                    android_object.push({f: "lineWrap", p: "1"});
                                    builder.addFeedLine(1);
                                    builder.addTextPosition(0);
                                    builder.addTextFont(builder.FONT_B);

                                    if (comanda.nome_stampante[stampante_tavolo].nome === 'TMT88-Vi') {
                                        builder.addTextPosition(85);
                                        builder.addTextFont(builder.FONT_A);
                                    } else if (comanda.nome_stampante[stampante_tavolo].nome === 'CUBO')
                                    {
                                        builder.addTextPosition(167);
                                        builder.addTextFont(builder.FONT_A);

                                    }

                                    android_object.push({f: "printOriginalText", p: 'Diviso per ' + aggSpazi(numero_paganti, 3) + ' persone: '});
                                    android_object.push({f: "lineWrap", p: "1"});
                                    builder.addText('Diviso per ' + aggSpazi(numero_paganti, 3) + ' persone: ');

                                    android_object.push({f: "printOriginalText", p: "€ " + riga_conto_totale(parseFloat(parseFloat(totale_iniziale) / parseInt(numero_paganti)).toFixed(2))});
                                    android_object.push({f: "lineWrap", p: "1"});
                                    builder.addText("€ " + riga_conto_totale(parseFloat(parseFloat(totale_iniziale) / parseInt(numero_paganti)).toFixed(2)));

                                    builder.addTextFont(builder.FONT_A);
                                }

                                //SERVIZIO
                                //builder.addText('Servizio ' + settaggi_profili.Field70 + ' %: ');
                                //builder.addText('€ ' + (parseFloat(parseFloat(totale) - parseInt($('#numero_coperti_effettivi').html())))/100*settaggi_profili.Field70 + '\n');

                                //--

                                //SETTAGGI DEL MESSAGGIO FINALE
                                if (scontrino_con_bonifico === true) {

                                    android_object.push({f: "lineWrap", p: "2"});
                                    builder.addFeedLine(2);

                                    android_object.push({f: "setAlignment", p: "1"});
                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                    android_object.push({f: "BOLD", p: ""});
                                    builder.addTextStyle(false, false, true, builder.COLOR_1);

                                    android_object.push({f: "setFontSize", p: "60"});
                                    builder.addTextSize(2, 2);

                                    android_object.push({f: "printOriginalText", p: 'IN ATTESA DI BONIFICO'});
                                    android_object.push({f: "lineWrap", p: "1"});
                                    builder.addText('IN ATTESA DI BONIFICO\n');

                                    android_object.push({f: "setFontSize", p: "26"});
                                    builder.addTextSize(1, 1);


                                    android_object.push({f: "UNBOLD", p: ""});
                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                    builder.addFeedLine(1);

                                    android_object.push({f: "lineWrap", p: "1"});
                                    android_object.push({f: "printOriginalText", p: 'Rif. Documento Commerciale n°' + numero_riferimento_scontrino});

                                    builder.addText('Rif. Documento Commerciale n°' + numero_riferimento_scontrino);
                                    android_object.push({f: "BOLD", p: ""});
                                    builder.addTextStyle(false, false, true, builder.COLOR_1);

                                    android_object.push({f: "lineWrap", p: "2"});
                                    builder.addFeedLine(2);

                                    android_object.push({f: "setFontSize", p: "26"});
                                    builder.addTextSize(1, 1);

                                    android_object.push({f: "printOriginalText", p: 'DA PARTE DI:'});
                                    android_object.push({f: "lineWrap", p: "5"});
                                    builder.addText('DA PARTE DI:\n');
                                    builder.addFeedLine(4);

                                    android_object.push({f: "setAlignment", p: "2"});
                                    builder.addTextAlign(builder.ALIGN_RIGHT);

                                    android_object.push({f: "printOriginalText", p: 'Firma Cliente:   '});
                                    android_object.push({f: "lineWrap", p: "5"});
                                    builder.addText('Firma Cliente:   \n');
                                    ndroid_object.push({f: "lineWrap", p: "3"});
                                    builder.addFeedLine(2);
                                    android_object.push({f: "printOriginalText", p: '---------------------'});
                                    android_object.push({f: "lineWrap", p: "5"});
                                    builder.addText('---------------------\n');

                                } else {
                                    builder.addFeedLine(2);
                                    if (settaggi_profili.Field70 === 'G') {
                                        android_object.push({f: "BOLD", p: ""});
                                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                                    } else if (settaggi_profili.Field70 === 'N') {
                                        android_object.push({f: "UNBOLD", p: ""});
                                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                                    }

                                    if (settaggi_profili.Field71 === 'x--')
                                    {
                                        android_object.push({f: "setAlignment", p: "0"});
                                        builder.addTextAlign(builder.ALIGN_LEFT);
                                    } else if (settaggi_profili.Field71 === '-x-')
                                    {
                                        android_object.push({f: "setAlignment", p: "1"});
                                        builder.addTextAlign(builder.ALIGN_CENTER);
                                    } else if (settaggi_profili.Field71 === '--x')
                                    {
                                        android_object.push({f: "setAlignment", p: "2"});
                                        builder.addTextAlign(builder.ALIGN_RIGHT);
                                    }


                                    if (settaggi_profili.Field69.trim().length > 0)
                                        builder.addText(settaggi_profili.Field69 + '\n');
                                    if (settaggi_profili.Field72 === 'true')
                                        builder.addFeedLine(1);
                                    if (settaggi_profili.Field74 === 'G') {
                                        android_object.push({f: "BOLD", p: ""});
                                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                                    } else if (settaggi_profili.Field74 === 'N') {
                                        android_object.push({f: "UNBOLD", p: ""});
                                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                                    }

                                    if (settaggi_profili.Field75 === 'x--') {
                                        android_object.push({f: "setAlignment", p: "0"});
                                        builder.addTextAlign(builder.ALIGN_LEFT);
                                    } else if (settaggi_profili.Field75 === '-x-') {
                                        android_object.push({f: "setAlignment", p: "1"});
                                        builder.addTextAlign(builder.ALIGN_CENTER);
                                    } else if (settaggi_profili.Field75 === '--x')
                                    {
                                        android_object.push({f: "setAlignment", p: "2"});
                                        builder.addTextAlign(builder.ALIGN_RIGHT);
                                    }


                                    if (settaggi_profili.Field73.trim().length > 0)
                                        builder.addText(settaggi_profili.Field73 + '\n');
                                    if (settaggi_profili.Field76 === 'true')
                                        builder.addFeedLine(1);
                                    if (settaggi_profili.Field78 === 'G') {
                                        android_object.push({f: "BOLD", p: ""});
                                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                                    } else if (settaggi_profili.Field78 === 'N') {
                                        android_object.push({f: "UNBOLD", p: ""});
                                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                                    }

                                    if (settaggi_profili.Field79 === 'x--') {
                                        android_object.push({f: "setAlignment", p: "0"});
                                        builder.addTextAlign(builder.ALIGN_LEFT);
                                    } else if (settaggi_profili.Field79 === '-x-') {
                                        android_object.push({f: "setAlignment", p: "1"});
                                        builder.addTextAlign(builder.ALIGN_CENTER);
                                    } else if (settaggi_profili.Field79 === '--x') {
                                        android_object.push({f: "setAlignment", p: "2"});
                                        builder.addTextAlign(builder.ALIGN_RIGHT);
                                    }
                                    if (settaggi_profili.Field77.trim().length > 0)
                                        builder.addText(settaggi_profili.Field77 + '\n');
                                    //FINE SETTAGGI MESSAGGIO FINALE
                                }
                                android_object.push({f: "UNBOLD", p: ""});
                                builder.addTextStyle(false, false, false, builder.COLOR_1);

                                android_object.push({f: "lineWrap", p: "2"});
                                builder.addFeedLine(2);

                                android_object.push({f: "CUT_FEED", p: ""});


                                builder.addCut(builder.CUT_FEED);
                                builder.addPulse();

                                if (comanda.AndroidPrinter === true) {
                                    invia_stampa_android(android_object);
                                }

                                var request = builder.toString();
                                //CONTENUTO CONTO

                                //Create a SOAP envelop
                                var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
                                var url = "";
                                var url_alternativo = "";

                                /*var stampante_tavolo = comanda.lang_stampa[60].replace(/\<br\/>/gi, "") + numero_layout;
                                 
                                 var query_stampante_tavolo = "select n_st_conto from tavoli where numero='" + comanda.tavolo + "';";
                                 var result_st_tav = alasql(query_stampante_tavolo);
                                 
                                 if (result_st_tav[0] !== undefined && result_st_tav[0].n_st_conto !== undefined && result_st_tav[0].n_st_conto !== null && result_st_tav[0].n_st_conto !== "undefined" && result_st_tav[0].n_st_conto !== "null" && result_st_tav[0].n_st_conto.trim() !== "") {
                                 if (result_st_tav[0].n_st_conto === "1") {
                                 stampante_tavolo = comanda.lang_stampa[60].replace(/\<br\/>/gi, "");
                                 } else {
                                 stampante_tavolo = comanda.lang_stampa[60].replace(/\<br\/>/gi, "") + "_" + result_st_tav[0].n_st_conto;
                                 }
                                 }*/

                                if (comanda.nome_stampante[stampante_tavolo].intelligent === "SDS") {
                                    url = 'http://' + comanda.nome_stampante[stampante_tavolo + numero_layout].ip + ':8000/SPOOLER?stampante=' + comanda.nome_stampante[stampante_tavolo + numero_layout].devid_nf;
                                } else {
                                    if (comanda.nome_stampante[stampante_tavolo].ip_alternativo !== undefined && comanda.nome_stampante[stampante_tavolo].ip_alternativo !== null && comanda.nome_stampante[stampante_tavolo].ip_alternativo !== "") {
                                        url_alternativo = 'http://' + comanda.nome_stampante[stampante_tavolo + numero_layout].ip_alternativo + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                                    }
                                    url = 'http://' + comanda.nome_stampante[stampante_tavolo].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                                }

                                var sconto_perc = 0;
                                var sconto_imp = 0;
                                var tipo_sconto = 'N';
                                //SCONTI PERCENTUALI
                                intestazioni_conto_2.each(function (index, element) {
                                    sconto_perc += parseFloat($(element).find('td:nth-child(4)').html().slice(1, -1).replace(',', '.'));
                                    console.log("CALCOLO SCONTISTICA TOTALI", $(element).find('td:nth-child(4)').html().slice(1, -1), typeof ($(element).find('td:nth-child(4)').html().slice(1, -1)));
                                    tipo_sconto = 'P';
                                });
                                //SCONTI IMPONIBILI
                                intestazioni_conto_3.each(function (index, element) {
                                    sconto_imp += parseFloat($(element).find('td:nth-child(4)').html().substr(1).replace(',', '.'));
                                    console.log("CALCOLO SCONTISTICA TOTALI", $(element).find('td:nth-child(4)').html(), typeof ($(element).find('td:nth-child(4)').html()));
                                    tipo_sconto = 'F';
                                });
                                //CALCOLO PERCENTUALE IN BASE ALLO SCONTO FISSO
                                var netto_dopo_sconto = parseFloat(totale_iniziale.replace(',', '.'));
                                var totale = netto_dopo_sconto + sconto_imp;
                                sconto_perc += 100 / (totale / sconto_imp);
                                console.log("CALCOLO SCONTISTICA TOTALI", totale_iniziale, sconto_imp, sconto_perc, 100 / (totale_iniziale / sconto_imp));
                                if (typeof callback === 'function')
                                {
                                    callback([url, soap, comanda.nome_stampante[stampante_tavolo].nome_umano], settaggi_profili.Field80);
                                } else
                                {
                                    if (tavolo.left(7) === "ASPORTO" || tavolo === 'TAKEAWAY' || tavolo === 'TAKE AWAY' || tavolo === 'BAR') {
                                        indietro_categoria();
                                    }


                                    if (comanda.AndroidPrinter === true) {

                                    } else if (comanda.pizzeria_asporto === true && (settaggi_profili.Field80 === 'true' || settaggi_profili.Field436 === 'true')) {
                                        aggiungi_spool_stampa(url, soap, "CONTO", undefined, undefined, url_alternativo);
                                    } else if (comanda.pizzeria_asporto !== true && settaggi_profili.Field107 === 'true') {
                                        aggiungi_spool_stampa(url, soap, "CONTO", undefined, undefined, url_alternativo);
                                    }
                                    if (comanda.apertura_conto === "true") {
                                        apri_cassetto();
                                    }

                                    if (comanda.AndroidPrinter === true) {

                                    } else {
                                        aggiungi_spool_stampa(url, soap, "CONTO", undefined, undefined, url_alternativo);
                                    }
                                    //TEMP DISABLED


                                    var data = comanda.funzionidb.data_attuale().substr(0, 8);
                                    var ora = comanda.funzionidb.data_attuale().substr(9, 8);
                                    var testo_query = "update comanda set sconto_perc='" + sconto_perc.toFixed(2) + "',sconto_imp='" + tipo_sconto + "',netto='" + cifra_sconto + "',stampata_sn='S',data_stampa='" + data + "',ora_stampa='" + ora + "',fiscalizzata_sn='N'  where desc_art!='EXTRA' and stato_record='ATTIVO' and ntav_comanda='" + tavolo + "'";
                                    if (tavolo.left(7) === "ASPORTO" || tavolo === 'TAKEAWAY') {
                                        var testo_query = "update comanda set sconto_perc='" + sconto_perc.toFixed(2) + "',sconto_imp='" + tipo_sconto + "',netto='" + cifra_sconto + "',stampata_sn='N',data_stampa='" + data + "',ora_stampa='" + ora + "',fiscalizzata_sn='N'  where desc_art!='EXTRA' and stato_record='ATTIVO' and ntav_comanda='" + tavolo + "'";
                                    }

                                    comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                                    comanda.sincro.query(testo_query, function () {

                                        if (scontrino_con_bonifico === true) {

                                            if (/*comanda.licenza === 'mattia131' || */((typeof (tavolo) === "string" && tavolo.indexOf("CONTINUO") === -1) && tavolo !== '*BAR*' && tavolo !== '*ASPORTO*' && tavolo !== "BANCO" && tavolo !== "BAR" && tavolo.left(7) !== "ASPORTO" && tavolo !== "TAKE AWAY"))
                                            {
                                                btn_tavoli();
                                            } else
                                            {
                                                comanda.funzionidb.conto_attivo();
                                            }

                                            comanda.persona = null;
                                            //CHIUSURA
                                            comanda.sincro.query_cassa();

                                        } else
                                        {

                                            testo_query = "update tavoli set colore='4',occupato='0',ora_ultima_comanda='-',ora_apertura_tavolo='-' where numero='" + tavolo + "' ";
                                            comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                                            comanda.sincro.query(testo_query, function () {

                                                disegna_ultimo_tavolo_modificato(testo_query);
                                                if (comanda.AndroidPrinter === true) {
                                                    btn_tavoli("scontrino");
                                                } else if (/*comanda.licenza === 'mattia131' || */((typeof (tavolo) === "string" && tavolo.indexOf("CONTINUO") === -1) && tavolo !== '*BAR*' && tavolo !== '*ASPORTO*' && tavolo !== "BANCO" && tavolo !== "BAR" && tavolo.left(7) !== "ASPORTO" && tavolo !== "TAKE AWAY"))
                                                {
                                                    btn_tavoli();
                                                } else
                                                {
                                                    comanda.funzionidb.conto_attivo();
                                                }

                                                comanda.persona = null;
                                                //CHIUSURA
                                                comanda.sincro.query_cassa();
                                            });

                                        }
                                    });
                                }
                            }
                        });

                    }
                });
        }
        }

    }

    function invia_stampa_android(android_object) {

        var unbold = [0x1B, 0x45, 0x0];
        var unboldUint8 = new Uint8Array(unbold);
        var unboldBase64 = BufferToBase64(unboldUint8);


        var bold = [0x1B, 0x45, 0x1];
        var boldUint8 = new Uint8Array(bold);
        var boldBase64 = BufferToBase64(boldUint8);


        var cutpaper = [0x1d, 0x56, 0x42, 0x00];
        var cutpaperUint8 = new Uint8Array(cutpaper);
        var cutpaperBase64 = BufferToBase64(cutpaperUint8);



        android_object.reduce((previousPromise, v) => {
            return previousPromise.then(() => {
                switch (v.f) {

                    case "printerInit":
                        return sunmiInnerPrinter.printerInit();
                        break;

                    case "printOriginalText":

                        return sunmiInnerPrinter.printOriginalText(v.p);

                        break;

                    case "setFontSize":

                        return sunmiInnerPrinter.setFontSize(parseInt(v.p));

                        break;

                    case "setAlignment":

                        return sunmiInnerPrinter.setAlignment(parseInt(v.p));

                        break;

                    case "lineWrap":

                        return sunmiInnerPrinter.lineWrap(parseInt(v.p));

                        break;

                    case "CUT_FEED":


                        return sunmiInnerPrinter.sendRAWData(cutpaperBase64);


                        break;

                    case "BOLD":



                        return sunmiInnerPrinter.sendRAWData(boldBase64);

                        break;

                    case "UNBOLD":



                        return sunmiInnerPrinter.sendRAWData(unboldBase64);


                        break;

                    default:
                        console.log("");
                }
            });
        }, Promise.resolve());
    }
}