var add = MT.process(
  function(a, b) { return a + b; },
  function(r) { console.log(r) }
);

var diff = MT.process(
  function(a, b) { return a - b; },
  function(r) { console.log(r) }
);

var molt = MT.process(
  function(a, b) { return a * b; },
  function(r) { console.log(r) }
);

var div = MT.process(
  function(a, b) { return a / b; },
  function(r) { console.log(r) }
);