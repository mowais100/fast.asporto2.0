function chiudi_popup_scelta_pos(nome_pos) {

    var query_dati_pos = "";

    if (nome_pos === "POS 2") {
        query_dati_pos = "select Field401 as ip,Field402 as porta,Field403 as term,Field404 as ric from settaggi_profili where id=" + comanda.folder_number + "  LIMIT 1";
    } else if (nome_pos === "POS 3") {
        query_dati_pos = "select Field406 as ip,Field407 as porta,Field408 as term,Field409 as ric from settaggi_profili where id=" + comanda.folder_number + "  LIMIT 1";
    } else
    {
        query_dati_pos = "select Field231 as ip,Field232 as porta,Field233 as term,Field235 as ric from settaggi_profili where id=" + comanda.folder_number + "  LIMIT 1";
    }

    var dati_pos = alasql(query_dati_pos);

    comanda.ip_pos = dati_pos[0].ip;
    comanda.porta_pos = dati_pos[0].porta;
    comanda.id_term = dati_pos[0].term;
    comanda.ricevuta_pos_scontrino = dati_pos[0].ric;

    comanda.numero_pos_utilizzato = nome_pos;
    $("#popup_scelta_pos").modal("hide");
}

function scelta_numero_pos() {

    var query_verifica_numero_pos = "select Field234,Field400,Field405 from settaggi_profili where id=" + comanda.folder_number + "  LIMIT 1";

    var verifica_numero_pos = alasql(query_verifica_numero_pos);

    var contatore_pos = 0;
    var righe_scelta_pos = "";

    if (verifica_numero_pos[0].Field234 === "true") {
        righe_scelta_pos += "<tr><td onclick=\"chiudi_popup_scelta_pos('POS 1')\" style='height: 20vh;vertical-align: inherit;display: table-cell;'>POS 1</td></tr>";

        contatore_pos++;
    }

    if (verifica_numero_pos[0].Field400 === "true") {
        righe_scelta_pos += "<tr><td onclick=\"chiudi_popup_scelta_pos('POS 2')\" style='height: 20vh;vertical-align: inherit;display: table-cell;'>POS 2</td></tr>";

        contatore_pos++;
    }

    if (verifica_numero_pos[0].Field405 === "true") {
        righe_scelta_pos += "<tr><td onclick=\"chiudi_popup_scelta_pos('POS 3')\" style='height: 20vh;vertical-align: inherit;display: table-cell;'>POS 3</td></tr>";

        contatore_pos++;
    }

    if (contatore_pos > 1) {
        //Apri popup scelta
        $("#righe_scelta_pos").html(righe_scelta_pos);


        $("#popup_scelta_pos").modal("show");
    }

}

function Ingenico() {

    //QUERY

    this.tipo_carta = "";
    this.scontrino_carta = "";

    this.abilita_stampa_ECR = function (prezzo, callBack) {

        if (parseFloat(dati_testa.pos) > 0) {

            var query_verifica_numero_pos = "select Field234,Field400,Field405 from settaggi_profili where id=" + comanda.folder_number + "  LIMIT 1";

            var verifica_numero_pos = alasql(query_verifica_numero_pos);

            var contatore_pos = 0;
            var righe_scelta_pos = "";

            if (verifica_numero_pos[0].Field234 === "true") {
                contatore_pos++;
            }

            if (verifica_numero_pos[0].Field400 === "true") {
                contatore_pos++;
            }

            if (verifica_numero_pos[0].Field405 === "true") {
                contatore_pos++;
            }

            if (contatore_pos > 0) {

                if (!isNaN(prezzo) && prezzo > 0) {


                    var importo = parseFloat(prezzo).toFixed(2).replace('.', '');

                    var request = $.ajax({
                        timeout: 60000,
                        method: "GET",
                        url: 'http://' + comanda.ip_server + ':8010/INGENICO',
                        data: {ip_pos: comanda.ip_pos, porta_pos: comanda.porta_pos, id_term: comanda.id_term, importo: importo, ricevuta_pos_scontrino: comanda.ricevuta_pos_scontrino}
                    });

                    request.done(function (data) {

                        if (typeof (data) === "string" && data.substr(0, 2) === "OK") {
                            ing.tipo_carta = data.substr(2, 1);

                            var array_scontrino = data.substr(3).split('\r\n');

                            var stringa_output = "";

                            stringa_output += '<printRecMessage  operator="1" message="' + fat_centra('----------------------------------------------') + '" messageType="3" index="' + index_recmessage + '" font="1" />';
                            index_recmessage++;

                            var contatore = index_recmessage;

                            array_scontrino.forEach(function (line) {
                                stringa_output += '<printRecMessage  operator="1" message="' + fat_centra(line.replace(/\s/gi, ' ')) + '" messageType="3" index="' + contatore + '" font="1" />';
                                contatore++;
                                index_recmessage++;
                            });

                            ing.scontrino_carta = stringa_output;

                            callBack(true);
                        } else
                        {

                            callBack(false);
                        }
                    });

                    request.fail(function (event) {

                        callBack(false);
                    });
                } else
                {
                    callBack(false);
                }
            } else
            {
                callBack("MANUALE");
            }
        } else
        {
            callBack("MANUALE");
        }
    };

    this.richiesta_abilitazione_pagamento = function () {

    };

    this.invio_messaggi_dati_aggiuntivi_gt = function () {

    };

    this.messaggio_conferma = function () {

    };

    //VALIDAZIONE RISPOSTE


}

var ing = new Ingenico();

