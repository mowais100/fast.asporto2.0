/* global bootbox */

function oggetto_efsta() {

    this.transazione = "";
    this.totale = "";


    this.elenco_articoli = new Array();
    this.elenco_reparti = new Array();
    this.elenco_pagamenti = new Array();




    this.testa = function (transazione, totale) {
        this.transazione = transazione;
        this.totale = totale;
    };

    this.corpo = function (descrizione, reparto, quantita, prezzo) {
        this.elenco_articoli.push({'descrizione': descrizione, 'reparto': reparto, 'quantita': quantita, 'prezzo': prezzo});
    };

    this.reparto = function (reparto, percentuale, netto, imposta, totale) {
        this.elenco_reparti.push({'reparto': reparto, 'percentuale': percentuale, 'netto': netto, 'imposta': imposta, 'totale': totale});
    };

    this.pagamento = function (descrizione, totale) {
        this.elenco_pagamenti.push({'descrizione': descrizione, 'totale': totale});
    };

    this.crea_XML = function () {

        var xml = "<Tra>";

        /* TL è l'id del locale, TT è l'id del terminale. Di default 1 */
        var s_pc = "TWJSNJ470137"
        var testo_query = "select TID,start_data_tid from comanda where id ='" +comanda.id_comanda_attuale + "'";
        var resulto = alasql(testo_query);
        var tid_id = resulto[0].TID;
        var start_data= resulto[0].start_data_tid;
       

        xml += "<ESR TID='" + tid_id + "' TL='"+comanda.numero_licenza_cliente+"' TT='"+s_pc+"' T='" + this.totale + "' Opr='" + comanda.operatore + "'>";

        xml += "<PosA>";

        /* ciclo */

        this.elenco_articoli.forEach(function (v) {

            if (parseFloat(v.prezzo) >= 0) {

                xml += "<Pos PN='" + v.quantita + "' Dsc='" + v.descrizione + "' TaxG='" + v.reparto + "' Amt='" + v.prezzo + "' />";

            } else {

                xml += "<Mod PN='" + v.quantita + "' Dsc='" + v.descrizione + "' Amt='" + v.prezzo + "' />";

            }
        });

        /* fine ciclo */

        xml += "</PosA>";

        xml += "<PayA>";

        /* ciclo */

        this.elenco_pagamenti.forEach(function (v) {

            xml += "<Pay Dsc='" + v.descrizione + "' Amt='" + v.totale + "' PayG='" +"0" + "' />";

        });

        /* fine ciclo */

        xml += "</PayA>";

        xml += "<TaxA>";

        /* ciclo */

        this.elenco_reparti.forEach(function (v) {

            xml += "<Tax TaxG='" + v.reparto + "' Prc='" + v.percentuale + "' Net='" + v.netto + "' TAmt='" + v.imposta + "' Amt='" + v.totale + "' />";

        });

        /* fine ciclo */

        xml += "</TaxA>";

        xml += "</ESR>";

        xml += "</Tra>";

        return xml;

    };

    this.invia = function () {
       
        
       

        var xhr = new XMLHttpRequest();


        var xml_finito = this.crea_XML();

        // If specified, responseType must be empty string or "document"
        xhr.responseType = 'document';

    // Force the response to be parsed as XML
        xhr.overrideMimeType('text/xml');

        xhr.onload = function () {
            if (xhr.readyState === xhr.DONE && xhr.status === 200) {
                var testo_query = "select TID,start_data_tid from comanda where id ='" +comanda.id_comanda_attuale + "'";
                var resulto = alasql(testo_query);
        
                var start_data= resulto[0].start_data_tid;
                window.TID_idd = resulto[0].TID;
                
                console.log(xhr.response, xhr.responseXML);
        
                 window.startee = start_data;;
                 Ende = $(xhr.responseXML).find("TraC").find("Fis").find("Tag")[2];
                 window.Endee = Ende.attributes.Value.value;
                var TSE = $(xhr.responseXML).find("TraC").find("Fis").find("Tag")[3];
                window.TSEE  = TSE.attributes.Value.value;
                var SigZ = $(xhr.responseXML).find("TraC").find("Fis").find("Tag")[4];
                window.SigZZ  = SigZ.attributes.Value.value;
                var Sign = $(xhr.responseXML).find("TraC").find("Fis").find("Tag")[5];
                window.Signn = Sign.attributes.Value.value;
                window. data2 =  comanda.TID.substr(4, 11) 
                window. data3= comanda.TID.substr(14);
                var data6 = Endee.substr(8, 2) + "/" + Endee.substr(5, 2) + "/" + Endee.substr(0, 4) + " " + Endee.substr(11);
                window. data4 =  data6.substr(0, 11) 
                window. data5= data6.substr(11);
                window. result = [Endee,TSEE,SigZZ,Signn];
                window.Code = $(xhr.responseXML).find("TraC").find("Fis").find("Code");
                window.codee=Code.context.all[Code.context.all.length-9].innerHTML;
               
              
                console.log(Endee,TSEE,SigZZ,Signn);
                
                
            } else
            {
                bootbox.alert("Errore nell'invio della transazione all'EFSTA.");
            }
        };
        $(".table-responsive").css("pointer-events","auto");
        xhr.open("POST", "http://"+comanda.ip_server+":5618/register", true);
        xhr.send(xml_finito);
   

    };

}

function inserisci_tra_stringa(stringa_originale, stringa_da_inserire, posizione, allineamento) {

    var pos = 0;
    /* D vuol dire destra */
    if (allineamento === "D") {
        pos = posizione - stringa_da_inserire.length;
    } else {
        pos = posizione;
    }

    var output = [stringa_originale.slice(0, pos), stringa_da_inserire, stringa_originale.slice(pos + stringa_da_inserire.length)].join('');

    return output;

}