function comandapiuconto(saltafaseparcheggio, disabilita_avviso, cambio_orario_consegna, numero_progressivo) {


    let intestazione_conto = $(comanda.intestazione_conto);
    let intestazione_conto_tr = $('#intestazioni_conto tr');

    let intestazioni_conto_percentuale = $('#intestazioni_conto tr:contains(%)');

    let conto = $('#conto tr');

    test_copertura(function (stato_copertura) {

        var query = "select * from settaggi_profili;";
        comanda.sincro.query(query, function (settaggi_profili) {
            settaggi_profili = settaggi_profili[0];

            if (stato_copertura === true) {

                if (saltafaseparcheggio !== true && (comanda.tavolo.indexOf("CONTINUO") !== -1 || comanda.tavolo === "BANCO" || comanda.tavolo == "BAR" || comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === "TAKE AWAY")) {
                    comanda.stampato = false;
                    btn_parcheggia();
                } else
                {
                    comanda.stampato = true;
                    console.log("stampa_comanda asporto", comanda.stampato, saltafaseparcheggio, comanda.tavolo, comanda.parcheggio)
                    var nome_cliente = comanda.parcheggio;
                    if ($("#conto tr").not('.comanda').length === 0)
                    {
                        if (disabilita_avviso !== true) {
                            alert("Non hai nuovi articoli da lanciare in comanda.");
                        }
                    } else
                    {
                        var testo_query = "select id,ricetta from prodotti;";
                        console.log("STAMPA COMANDA", testo_query);

                        comanda.sincro.query(testo_query, function (result_prodotti) {

                            var corrispondenza_id_prodotti = new Object();

                            result_prodotti.forEach(function (element) {

                                corrispondenza_id_prodotti[element.id] = element.ricetta;

                            });

                            var testo_query = "select  substr(desc_art,1,3)||'M'|| substr(nodo,1,3) as ord,cod_articolo,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita from comanda as c1 where contiene_variante='1' and ntav_comanda='" + comanda.tavolo + "'  and posizione='CONTO' and (stampata_sn is null or stampata_sn='null' or stampata_sn='n')  and stato_record='ATTIVO' \n\
                           union all\n\
                           select  substr(desc_art,1,3)||'M'||substr(nodo,1,3) as ord,cod_articolo,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,sum(quantita) as quantita from comanda as c1 where (contiene_variante !='1' or contiene_variante is null or contiene_variante='null') and length(nodo)=3 and ntav_comanda='" + comanda.tavolo + "' and posizione='CONTO' and (stampata_sn is null or stampata_sn='null' or stampata_sn='n')  and stato_record='ATTIVO' group by desc_art,nome_comanda\n\
                           union all\n\
                           select  (select substr(desc_art,1,3) from comanda where nodo=substr(c1.nodo,1,3) and stato_record='ATTIVO'  and ntav_comanda='" + comanda.tavolo + "')||'M' ||substr(nodo,1,3) as ord,cod_articolo,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita from comanda as c1 where length(nodo)=7 and substr(desc_art,1,1)='-' and ntav_comanda='" + comanda.tavolo + "'   and posizione='CONTO' and (stampata_sn is null or stampata_sn='null' or stampata_sn='n')  and stato_record='ATTIVO'\n\
                           union all\n\
                           select (select substr(desc_art,1,3) from comanda where nodo=substr(c1.nodo,1,3) and stato_record='ATTIVO'  and ntav_comanda='" + comanda.tavolo + "')||'M'||substr(nodo,1,3) as ord,cod_articolo,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita from comanda as c1 where length(nodo)=7 and substr(desc_art,1,1)!='-' and ntav_comanda='" + comanda.tavolo + "'  and posizione='CONTO' and (stampata_sn is null or stampata_sn='null' or stampata_sn='n')  and stato_record='ATTIVO'\n\
                           order by nome_comanda DESC, ord ASC;";

                            console.log("STAMPA COMANDA", testo_query);

                            var prom = new Promise(function (resolve, reject) {
                                /*if (settaggi_profili.Field9 === 'true')
                                 {*/
                                dati_comanda(false, function (res) {
                                    resolve(res);
                                });
                                /*} else
                                 {
                                 comanda.sincro.query(testo_query, function (res) {
                                 resolve(res);
                                 });
                                 }*/
                            });
                            var array_comanda = new Array();
                            var array_dest_stampa = new Array();

                            prom.then(function (result) {

                                console.log("RISULTATI COMANDA", result);

                                var progressivo = 0;
                                result.forEach(function (obj) {

                                    var descrizione = obj['desc_art'];
                                    var prezzo = obj['prezzo_un'];
                                    var nodo = obj['nodo'];
                                    var quantita = obj['quantita'];

                                    var cod_articolo = obj['cod_articolo'];
                                    if (cod_articolo !== undefined && cod_articolo !== 'null' && cod_articolo.length > 0)
                                    {
                                        console.log("ricette", corrispondenza_id_prodotti, cod_articolo);
                                        var ricetta = corrispondenza_id_prodotti[cod_articolo];
                                    }

                                    var dest_stampa = obj['dest_stampa'];
                                    var portata = obj['portata'];
                                    var categoria = obj['categoria'];
                                    if (array_comanda[dest_stampa] === undefined)
                                    {
                                        array_comanda[dest_stampa] = new Object();
                                    }

                                    if (array_dest_stampa[dest_stampa] === undefined)
                                    {
                                        array_dest_stampa[dest_stampa] = new Object();
                                        array_dest_stampa[dest_stampa].val = comanda.nome_stampante[dest_stampa].nome;
                                        array_dest_stampa[dest_stampa].dimensione = comanda.nome_stampante[dest_stampa].dimensione;
                                        array_dest_stampa[dest_stampa].ip = comanda.nome_stampante[dest_stampa].ip;
                                        array_dest_stampa[dest_stampa].intelligent = comanda.nome_stampante[dest_stampa].intelligent;
                                        array_dest_stampa[dest_stampa].fiscale = comanda.nome_stampante[dest_stampa].fiscale;
                                        array_dest_stampa[dest_stampa].nome_umano = comanda.nome_stampante[dest_stampa].nome_umano;
                                        array_dest_stampa[dest_stampa].devid_nf = comanda.nome_stampante[dest_stampa].devid_nf;
                                        array_dest_stampa[dest_stampa].devicetype = comanda.nome_stampante[dest_stampa].devicetype;
                                        array_dest_stampa[dest_stampa].devicemodel = comanda.nome_stampante[dest_stampa].devicemodel;
                                    }

                                    if (array_comanda[dest_stampa][portata] === undefined)
                                    {
                                        array_comanda[dest_stampa][portata] = new Object();
                                        array_comanda[dest_stampa][portata].val = comanda.nome_portata[portata];
                                    }

                                    if (array_comanda[dest_stampa][portata][(progressivo + 1)] === undefined)
                                    {
                                        array_comanda[dest_stampa][portata][(progressivo + 1)] = [quantita, descrizione, prezzo, ricetta];
                                    }



                                    progressivo++;



                                    var descrizione = obj['desc_art'];
                                    var prezzo = obj['prezzo_un'];
                                    var nodo = obj['nodo'];
                                    var quantita = obj['quantita'];

                                    var cod_articolo = obj['cod_articolo'];
                                    if (cod_articolo !== undefined && cod_articolo !== 'null' && cod_articolo.length > 0)
                                    {
                                        console.log("ricette", corrispondenza_id_prodotti, cod_articolo);
                                        var ricetta = corrispondenza_id_prodotti[cod_articolo];
                                    }

                                    var dest_stampa = obj['dest_stampa_2'];
                                    var portata = obj['portata'];
                                    var categoria = obj['categoria'];



                                    if (dest_stampa !== undefined && dest_stampa !== null && dest_stampa !== "" && dest_stampa !== "undefined" && dest_stampa !== "null" && dest_stampa !== obj['dest_stampa'] && obj['dest_stampa'] !== "T") {

                                        if (array_comanda[dest_stampa] === undefined)
                                        {
                                            array_comanda[dest_stampa] = new Object();
                                        }

                                        array_dest_stampa[dest_stampa] = new Object();
                                        array_dest_stampa[dest_stampa].val = comanda.nome_stampante[dest_stampa].nome;
                                        array_dest_stampa[dest_stampa].dimensione = comanda.nome_stampante[dest_stampa].dimensione;
                                        array_dest_stampa[dest_stampa].ip = comanda.nome_stampante[dest_stampa].ip;
                                        array_dest_stampa[dest_stampa].intelligent = comanda.nome_stampante[dest_stampa].intelligent;
                                        array_dest_stampa[dest_stampa].fiscale = comanda.nome_stampante[dest_stampa].fiscale;
                                        array_dest_stampa[dest_stampa].nome_umano = comanda.nome_stampante[dest_stampa].nome_umano;
                                        array_dest_stampa[dest_stampa].devid_nf = comanda.nome_stampante[dest_stampa].devid_nf;
                                        array_dest_stampa[dest_stampa].devicetype = comanda.nome_stampante[dest_stampa].devicetype;
                                        array_dest_stampa[dest_stampa].devicemodel = comanda.nome_stampante[dest_stampa].devicemodel;

                                        if (array_comanda[dest_stampa][portata] === undefined)
                                        {
                                            array_comanda[dest_stampa][portata] = new Object();
                                            array_comanda[dest_stampa][portata].val = comanda.nome_portata[portata];
                                        }

                                        if (array_comanda[dest_stampa][portata][(progressivo + 1)] === undefined)
                                        {
                                            array_comanda[dest_stampa][portata][(progressivo + 1)] = [quantita, descrizione, prezzo, ricetta];
                                        }

                                        progressivo++;

                                    }



                                });
                                //---COMANDA INTELLIGENT---//

                                for (var dest_stampa in array_comanda) {

                                    console.log("NOME STAMPANTI", comanda.nome_stampante[dest_stampa]);
                                    switch (array_dest_stampa[dest_stampa].intelligent) {

                                        case "S":
                                        case "s":
                                        case "SDS":
                                            if (array_dest_stampa[dest_stampa].fiscale.toUpperCase() === 'N') {

                                                var concomitanze_presenti = false;
                                                console.log("STAMPA COMANDA INTELLIGENT", array_dest_stampa[dest_stampa].ip);
                                                var com = '';
                                                var data = comanda.funzionidb.data_attuale();
                                                var builder = new epson.ePOSBuilder();
                                                var messaggio_finale = new epson.ePOSBuilder();
                                                var concomitanze = new epson.ePOSBuilder();
                                                var parola_conto = comanda.lang_stampa[11];
                                                builder.addTextFont(builder.FONT_A);
                                                builder.addTextAlign(builder.ALIGN_CENTER);

                                                if (settaggi_profili.Field34.trim().length > 0) {
                                                    switch (settaggi_profili.Field35) {
                                                        case "G":
                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                            break;
                                                        case "N":
                                                        default:
                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                    }
                                                    builder.addText(settaggi_profili.Field34 + '\n');
                                                    if (settaggi_profili.Field36 === 'true') {
                                                        builder.addFeedLine(1);
                                                    }
                                                }


                                                if (settaggi_profili.Field37.trim().length > 0) {
                                                    switch (settaggi_profili.Field38) {
                                                        case "G":
                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                            break;
                                                        case "N":
                                                        default:
                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                    }
                                                    builder.addText(settaggi_profili.Field37 + '\n');
                                                    if (settaggi_profili.Field39 === 'true') {
                                                        builder.addFeedLine(1);
                                                    }
                                                }


                                                if (settaggi_profili.Field40.trim().length > 0) {
                                                    switch (settaggi_profili.Field41) {
                                                        case "G":
                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                            break;
                                                        case "N":
                                                        default:
                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                    }
                                                    builder.addText(settaggi_profili.Field40 + '\n');
                                                    if (settaggi_profili.Field42 === 'true') {
                                                        builder.addFeedLine(1);
                                                    }
                                                }


                                                if (settaggi_profili.Field43.trim().length > 0) {
                                                    switch (settaggi_profili.Field44) {
                                                        case "G":
                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                            break;
                                                        case "N":
                                                        default:
                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                    }
                                                    builder.addText(settaggi_profili.Field43 + '\n');
                                                }

                                                builder.addText('\n\n');


                                                builder.addTextSize(2, 2);

                                                if (comanda.tavolo !== "BANCO" && comanda.tavolo !== "BAR" && comanda.tavolo.left(7) !== "ASPORTO" && comanda.tavolo !== "TAKE AWAY")
                                                {
                                                    if (settaggi_profili.Field3 === 'true') {
                                                        builder.addText(parola_conto + ' - ');
                                                    }

                                                    if (comanda.tavolo.indexOf("CONTINUO") !== -1 && numero_progressivo !== undefined) {
                                                        builder.addText('n° ' + numero_progressivo + '\n');
                                                    } else {
                                                        builder.addText('n° ' + comanda.tavolo + '\n');
                                                    }

                                                    builder.addTextFont(builder.FONT_A);
                                                    builder.addFeedLine(1);
                                                    if (settaggi_profili.Field4 === 'true') {
                                                        builder.addText('' + array_dest_stampa[dest_stampa].nome_umano + '\n');
                                                        builder.addFeedLine(1);
                                                    }
                                                } else if (comanda.tavolo === "BANCO" || comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === "TAKE AWAY") {
                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                    //var numeretto = $('#popup_scelta_cliente input[name="numeretto"]').val();
                                                    //builder.addText('' + numeretto + '\n');

                                                    builder.addText('ASPORTO\n');

                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                    builder.addFeedLine(1);
                                                } else if (comanda.tavolo === "BAR") {
                                                    if (settaggi_profili.Field3 === 'true') {
                                                        builder.addText(parola_conto + ' - ');
                                                    }

                                                    builder.addText(comanda.parcheggio + '\n');
                                                    builder.addTextFont(builder.FONT_A);
                                                    builder.addFeedLine(1);
                                                    if (settaggi_profili.Field4 === 'true') {
                                                        builder.addText('' + array_dest_stampa[dest_stampa].nome_umano + '\n');
                                                        builder.addFeedLine(1);
                                                    }
                                                }

                                                builder.addTextAlign(builder.ALIGN_LEFT);
                                                if (comanda.tavolo !== "BANCO" && comanda.tavolo.left(7) !== "ASPORTO" && comanda.tavolo !== "TAKE AWAY")
                                                {
                                                    //QUI PRIMA C'ERA IL TAVOLO
                                                } else
                                                {
                                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                                    builder.addText(nome_cliente + '\n');
                                                    builder.addTextAlign(builder.ALIGN_LEFT);
                                                }

                                                if (comanda.tavolo === "BANCO" || comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === "TAKE AWAY")
                                                {
                                                    if (cambio_orario_consegna === true) {
                                                        builder.addFeedLine(1);
                                                        builder.addText('CAMBIO ORA CONSEGNA!\n');
                                                    }

                                                    if ($('#parcheggia_comanda input[name="ora_consegna"]').val().length > 0) {
                                                        builder.addFeedLine(1);

                                                        builder.addTextAlign(builder.ALIGN_CENTER);
                                                        builder.addTextSize(2, 2);
                                                        builder.addText('CONSEGNA ' + ' ' + aggZero($('#parcheggia_comanda input[name="ora_consegna"]').val(), 2) + ' : ' + aggZero($('#parcheggia_comanda input[name="minuti_consegna"]').val(), 2) + '\n');
                                                        builder.addTextSize(1, 1);
                                                        builder.addFeedLine(1);
                                                        builder.addTextAlign(builder.ALIGN_LEFT);
                                                    }
                                                }


                                                builder.addTextSize(1, 1);
                                                builder.addFeedLine(1);
                                                builder.addTextStyle(false, false, true, builder.COLOR_1);


                                                if (settaggi_profili.Field5 === 'true') {
                                                    switch (settaggi_profili.Field30) {
                                                        case "G":
                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                            break;
                                                        case "N":
                                                        default:
                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                    }
                                                    builder.addText(comanda.lang_stampa[109].toLowerCase().capitalize() + ': ' + comanda.operatore + '\n');
                                                }

                                                if (settaggi_profili.Field6 === 'true' || settaggi_profili.Field7 === 'true') {

                                                    if (settaggi_profili.Field6 === 'true') {
                                                        switch (settaggi_profili.Field31) {
                                                            case "G":
                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                break;
                                                            case "N":
                                                            default:
                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                        }
                                                        builder.addText(data.substr(0, 9).replace(/-/gi, '/') + '   ');
                                                    }

                                                    if (settaggi_profili.Field7 === 'true') {
                                                        switch (settaggi_profili.Field32) {
                                                            case "G":
                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                break;
                                                            case "N":
                                                            default:
                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                        }
                                                        builder.addText(data.substr(9, 5));
                                                    }

                                                    builder.addText('\n');
                                                }

                                                //COPERTI
                                                if (settaggi_profili.Field24 === 'true' && comanda.tavolo !== 'BAR') {

                                                    builder.addText('\n');

                                                    var descrizione, quantita;

                                                    intestazione_conto.find('tr:contains("COPERTI"),tr:contains("GEDECKE")').each(function () {
                                                        descrizione = $(this).find('td:nth-child(2)').html().replace(/&nbsp;/g, "");
                                                        quantita = $(this).find('td:nth-child(1)').html();
                                                    });

                                                    if (quantita === undefined || quantita === null || quantita === 'undefined')
                                                    {
                                                        quantita = 1;
                                                    }
                                                    switch (settaggi_profili.Field33) {
                                                        case "G":
                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                            break;
                                                        case "N":
                                                        default:
                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                    }
                                                    builder.addText(comanda.lang_stampa[15].toLowerCase().capitalize() + ': ' + quantita + '\n');
                                                }

                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                builder.addTextSize(1, 1);

                                                var totale_quantita = 0;
                                                var totale_prezzo = 0.00;

                                                for (var portata in array_comanda[dest_stampa]) {
                                                    if (array_comanda[dest_stampa][portata].val !== undefined) {

                                                        builder.addFeedLine(1);
                                                        builder.addTextAlign(builder.ALIGN_CENTER);

                                                        if (settaggi_profili.Field8 === 'true') {
                                                            if (settaggi_profili.Field25 === 'G') {
                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                            }
                                                            builder.addText(trattini(array_comanda[dest_stampa][portata].val));
                                                        } else {
                                                            builder.addText("-----------------------------------------\n");
                                                        }
                                                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                        builder.addTextAlign(builder.ALIGN_LEFT);


                                                        for (var node in array_comanda[dest_stampa][portata]) {

                                                            var nodo = array_comanda[dest_stampa][portata][node];

                                                            console.log("nodo comanda", nodo);

                                                            if (nodo !== undefined && nodo[1] !== undefined && nodo[1].length > 1) {

                                                                var articolo = filtra_accenti(nodo[1]);
                                                                var quantita = nodo[0];
                                                                var prezzo = nodo[2];

                                                                if (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(" && articolo[0] !== "=") {

                                                                    switch (settaggi_profili.Field28) {
                                                                        case "D":
                                                                            builder.addTextSize(2, 2);
                                                                            break;
                                                                        case "G":
                                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                            break;
                                                                        case "N":
                                                                        default:
                                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                    }

                                                                    builder.addText(quantita);
                                                                    builder.addTextPosition(50);
                                                                    totale_quantita += parseInt(quantita);
                                                                    builder.addTextPosition(50);
                                                                    builder.addText(articolo.slice(0, 40));

                                                                    if (settaggi_profili.Field10 === 'true')
                                                                    {
                                                                        builder.addTextPosition(420);
                                                                        builder.addText('€ ' + prezzo);
                                                                    }

                                                                    builder.addText('\n');

                                                                    builder.addTextSize(1, 1);
                                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);

                                                                } else {

                                                                    switch (settaggi_profili.Field29) {
                                                                        case "D":
                                                                            builder.addTextSize(2, 2);
                                                                            break;
                                                                        case "G":
                                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                            break;
                                                                        case "N":
                                                                        default:
                                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                    }

                                                                    if (settaggi_profili.Field9 === 'true') {
                                                                        articolo = ' \t' + quantita + ' ' + articolo;
                                                                    } else {
                                                                        articolo = ' \t' + articolo;
                                                                    }

                                                                    builder.addTextPosition(50);

                                                                    //AGGIUNGERE PREZZO QUI
                                                                    builder.addText(articolo.slice(0, 40))

                                                                    if (settaggi_profili.Field10 === 'true')
                                                                    {
                                                                        builder.addTextPosition(420);
                                                                        builder.addText('€ ' + prezzo);
                                                                    }


                                                                    builder.addText('\n');


                                                                    builder.addTextSize(1, 1);
                                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                }

                                                                totale_prezzo += prezzo * quantita;



                                                                if (nodo[3] !== undefined && settaggi_profili.Field11 === 'true' && (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(" && articolo[0] !== "=")) {

                                                                    var ricetta = filtra_accenti(nodo[3]);
                                                                    if (ricetta.length > 1)
                                                                    {
                                                                        builder.addTextPosition(50);
                                                                        builder.addText('(' + ricetta + ') \n');
                                                                    }

                                                                }
                                                            }
                                                        }




                                                        builder.addTextAlign(builder.ALIGN_CENTER);
                                                        if (settaggi_profili.Field22 === 'true') {

                                                            for (var dest_stampa_concomitanza in array_comanda) {
                                                                if (array_dest_stampa[dest_stampa_concomitanza].nome_umano !== undefined && dest_stampa_concomitanza !== dest_stampa && array_comanda[dest_stampa_concomitanza][portata] !== undefined && array_comanda[dest_stampa_concomitanza][portata] !== null && typeof (array_comanda[dest_stampa_concomitanza][portata]) === "object" && Object.keys(array_comanda[dest_stampa_concomitanza][portata]).length > 0) {
                                                                    concomitanze_presenti = true;
                                                                    console.log("CONCOMITANZE: " + dest_stampa + " P: " + portata, array_comanda[dest_stampa_concomitanza][portata], Object.keys(array_comanda[dest_stampa_concomitanza][portata]).length);
                                                                    concomitanze.addTextAlign(builder.ALIGN_CENTER);
                                                                    concomitanze.addTextStyle(false, false, true, builder.COLOR_1);
                                                                    concomitanze.addText('\n' + array_dest_stampa[dest_stampa_concomitanza].nome_umano + ' - ' + array_comanda[dest_stampa][portata].val + '\n');
                                                                    concomitanze.addTextStyle(false, false, false, builder.COLOR_1);
                                                                    concomitanze.addTextAlign(builder.ALIGN_LEFT);

                                                                    if (array_comanda[dest_stampa_concomitanza][portata] !== undefined && array_comanda[dest_stampa_concomitanza][portata] !== null && typeof (array_comanda[dest_stampa_concomitanza][portata]) === "object" && Object.keys(array_comanda[dest_stampa_concomitanza][portata]).length > 0) {

                                                                        var caratteristiche_concomitanza = alasql("select * from nomi_portate where " + comanda.lingua + "='" + array_comanda[dest_stampa_concomitanza][portata].val + "' limit 1;");

                                                                        if (caratteristiche_concomitanza.length > 0) {

                                                                            if (caratteristiche_concomitanza[0].visu_articoli_concomitanze === "true") {

                                                                                for (var node_concomitanza in array_comanda[dest_stampa_concomitanza][portata]) {

                                                                                    var nodo = array_comanda[dest_stampa_concomitanza][portata][node_concomitanza];
                                                                                    if (nodo !== undefined && nodo[1] !== undefined && nodo[1].length > 1) {

                                                                                        var articolo = filtra_accenti(nodo[1]);
                                                                                        if (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(" && articolo[0] !== "=") {
                                                                                            concomitanze.addText(quantita);
                                                                                            concomitanze.addTextPosition(50);
                                                                                        } else {
                                                                                            articolo = ' \t' + articolo;
                                                                                        }

                                                                                        concomitanze.addTextPosition(50);
                                                                                        concomitanze.addText(articolo.slice(0, 40) + '\n');
                                                                                    }
                                                                                }

                                                                            }
                                                                        }

                                                                    }

                                                                    concomitanze.addTextAlign(builder.ALIGN_LEFT);
                                                                }
                                                            }
                                                        }
                                                        builder.addText("-----------------------------------------\n");
                                                        builder.addTextAlign(builder.ALIGN_LEFT);



                                                    }
                                                }//FINE CICLO PORTATA

                                                //SETTAGGI DEL MESSAGGIO FINALE

                                                if (settaggi_profili.Field14 === 'G')
                                                    messaggio_finale.addTextStyle(false, false, true, builder.COLOR_1);
                                                else if (settaggi_profili.Field14 === 'N')
                                                    messaggio_finale.addTextStyle(false, false, false, builder.COLOR_1);

                                                if (settaggi_profili.Field15 === 'x--')
                                                    messaggio_finale.addTextAlign(builder.ALIGN_LEFT);
                                                else if (settaggi_profili.Field15 === '-x-')
                                                    messaggio_finale.addTextAlign(builder.ALIGN_CENTER);
                                                else if (settaggi_profili.Field15 === '--x')
                                                    messaggio_finale.addTextAlign(builder.ALIGN_RIGHT);

                                                if (settaggi_profili.Field13.trim().length > 0)
                                                    messaggio_finale.addText(settaggi_profili.Field13 + '\n');

                                                if (settaggi_profili.Field26 === 'true')
                                                    messaggio_finale.addFeedLine(1);




                                                if (settaggi_profili.Field17 === 'G')
                                                    messaggio_finale.addTextStyle(false, false, true, builder.COLOR_1);
                                                else if (settaggi_profili.Field17 === 'N')
                                                    messaggio_finale.addTextStyle(false, false, false, builder.COLOR_1);

                                                if (settaggi_profili.Field18 === 'x--')
                                                    messaggio_finale.addTextAlign(builder.ALIGN_LEFT);
                                                else if (settaggi_profili.Field18 === '-x-')
                                                    messaggio_finale.addTextAlign(builder.ALIGN_CENTER);
                                                else if (settaggi_profili.Field18 === '--x')
                                                    messaggio_finale.addTextAlign(builder.ALIGN_RIGHT);

                                                if (settaggi_profili.Field16.trim().length > 0)
                                                    messaggio_finale.addText(settaggi_profili.Field16 + '\n');

                                                if (settaggi_profili.Field27 === 'true')
                                                    messaggio_finale.addFeedLine(1);



                                                if (settaggi_profili.Field20 === 'G')
                                                    messaggio_finale.addTextStyle(false, false, true, builder.COLOR_1);
                                                else if (settaggi_profili.Field20 === 'N')
                                                    messaggio_finale.addTextStyle(false, false, false, builder.COLOR_1);

                                                if (settaggi_profili.Field21 === 'x--')
                                                    messaggio_finale.addTextAlign(builder.ALIGN_LEFT);
                                                else if (settaggi_profili.Field21 === '-x-')
                                                    messaggio_finale.addTextAlign(builder.ALIGN_CENTER);
                                                else if (settaggi_profili.Field21 === '--x')
                                                    messaggio_finale.addTextAlign(builder.ALIGN_RIGHT);

                                                if (settaggi_profili.Field19.trim().length > 0)
                                                    messaggio_finale.addText(settaggi_profili.Field19 + '\n');

                                                //FINE SETTAGGI MESSAGGIO FINALE

                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                builder.addFeedLine(1);

                                                if (settaggi_profili.Field12 === 'true') {
                                                    builder.addText(totale_quantita);
                                                    builder.addTextPosition(50);
                                                    builder.addText(comanda.lang_stampa[53] + '               '); //quantita
                                                }

                                                if (settaggi_profili.Field10 === 'true')
                                                {
                                                    builder.addText("Totale: ");
                                                    builder.addTextPosition(415);
                                                    builder.addText('€ ' + parseFloat(totale_prezzo).toFixed(2)); //quantita
                                                }

                                                builder.addText('\n\n');



                                                //METTERE SOTTO LE CONCOMITANZE E CON SETTAGGIO
                                                if (settaggi_profili.Field108 === 'true' && (comanda.tavolo !== "BANCO" && comanda.tavolo !== "BAR" && comanda.tavolo.left(7) !== "ASPORTO" && comanda.tavolo !== "TAKE AWAY"))
                                                {
                                                    messaggio_finale.addTextStyle(false, false, false, builder.COLOR_1);
                                                    messaggio_finale.addFeedLine(1);
                                                    messaggio_finale.addTextSize(2, 2);
                                                    messaggio_finale.addTextAlign(builder.ALIGN_CENTER);

                                                    if (comanda.tavolo.indexOf("CONTINUO") !== -1 && numero_progressivo !== undefined) {
                                                        messaggio_finale.addText('n° ' + numero_progressivo + '\n');
                                                    } else {
                                                        messaggio_finale.addText('n° ' + comanda.tavolo + '\n');
                                                    }
                                                    messaggio_finale.addTextFont(builder.FONT_A);

                                                }

                                                if (settaggi_profili.Field22 === 'true') {

                                                    console.log("OGGETTO CONCOMITANZE", array_comanda);

                                                    if (Object.keys(array_comanda).length > 0) {

                                                        builder.addTextSize(1, 1);
                                                        builder.addTextStyle(false, false, true, builder.COLOR_1);

                                                        builder.addTextAlign(builder.ALIGN_CENTER);

                                                        builder.addText("-----------------------------------------\n");
                                                        builder.addText('CONCOMITANZE\n');

                                                        builder.addTextAlign(builder.ALIGN_LEFT);
                                                    }
                                                }

                                                builder.addTextAlign(builder.ALIGN_LEFT);
                                                var testo_concomitanze = '';

                                                if (concomitanze_presenti === true || settaggi_profili.Field22 === 'true')
                                                {
                                                    testo_concomitanze = '<feed line="0"/>' + concomitanze.toString().substr(72).slice(0, -13);
                                                }

                                                var messaggio_finale = '<feed line="1"/>' + messaggio_finale.toString().substr(72).slice(0, -13);

                                                var request = builder.toString().slice(0, -13) + testo_concomitanze + messaggio_finale + '<feed line="2"/><cut type="feed"/></epos-print>';
                                                console.log("REQUEST", request);
                                                var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';

                                                var url = 'http://' + array_dest_stampa[dest_stampa].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                                                if (array_dest_stampa[dest_stampa].intelligent === "SDS") {
                                                    url = 'http://' + array_dest_stampa[dest_stampa].ip + ':8000/SPOOLER?stampante=' + array_dest_stampa[dest_stampa].devid_nf;
                                                }

                                                if (settaggi_profili.Field23 === 'true') {
                                                    aggiungi_spool_attesa(url, soap, array_dest_stampa[dest_stampa].nome_umano);
                                                }
                                                aggiungi_spool_attesa(url, soap, array_dest_stampa[dest_stampa].nome_umano);
                                            }
                                    }
                                }

                                var data = comanda.funzionidb.data_attuale().substr(0, 8);
                                var ora = comanda.funzionidb.data_attuale().substr(9, 8);

                                var testo_query;
                                if (comanda.tavolo.left(7) !== "ASPORTO" && comanda.tavolo !== "TAKEAWAY" && comanda.tavolo !== "TAKE AWAY") {
                                    testo_query = "update comanda set stampata_sn='s',data_stampa='" + data + "',ora_stampa='" + ora + "',fiscalizzata_sn='c' where stato_record='ATTIVO' and ntav_comanda='" + comanda.tavolo + "'";
                                } else
                                {
                                    testo_query = "update comanda set data_stampa='" + data + "',ora_stampa='" + ora + "',fiscalizzata_sn='c' where stato_record='ATTIVO' and ntav_comanda='" + comanda.tavolo + "'";
                                }
                                comanda.sock.send({tipo: "comanda_stampata", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                                comanda.sincro.query(testo_query, function () {

                                    /* INIZIO STAMPA CONTO */
                                    if (comanda.compatibile_xml === true) {
                                        stampa_konto();
                                    } else {

                                        //QUI MANCA UN SETTAGGIO NEL SENSO CHE SE E' CONTO PIU' COMANDA NON SO COSA SUCCEDA
                                        if (comanda.licenza === 'mattia131' && comanda.parcheggio === 0 && (comanda.tavolo === "BANCO" || comanda.tavolo === "BAR" || comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === "TAKE AWAY")) {
                                            comanda.conto_in_attesa = true;
                                            btn_parcheggia();
                                        } else {
                                            if (comanda.licenza === 'mattia131' && (comanda.tavolo === "BANCO" || comanda.tavolo === "BAR" || comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === "TAKE AWAY")) {
                                                btn_parcheggia();
                                            }
                                            test_copertura(function (stato_copertura) {
                                                if (stato_copertura === true) {

                                                    var promise_progressivo = new Promise(function (resolve, reject) {

                                                        if (comanda.persona === null && comanda.tavolo === '*ASPORTO*') {

                                                            var query_prog = "select numero from progressivi_asporto limit 1;";
                                                            comanda.sincro.query(query_prog, function (n_prog) {

                                                                var progressivo_attuale = parseInt(n_prog[0].numero) + 1;
                                                                comanda.persona = progressivo_attuale;
                                                                var query_nuovo_prog = "update progressivi_asporto set numero='" + progressivo_attuale + "';";
                                                                comanda.sincro.query(query_nuovo_prog, function () {

                                                                    resolve(1);
                                                                });
                                                                comanda.sock.send({tipo: "aggiornamento", operatore: comanda.operatore, query: query_nuovo_prog, terminale: comanda.terminale, ip: comanda.ip_address});
                                                                comanda.sincro.query_cassa();
                                                            });
                                                        } else if (comanda.persona !== null && comanda.tavolo === '*ASPORTO*')
                                                        {
                                                            resolve(2);
                                                        } else
                                                        {
                                                            comanda.persona = '';
                                                            resolve(3);
                                                        }
                                                    });
                                                    promise_progressivo.then(function () {
                                                        var query = "select * from settaggi_profili;";
                                                        comanda.sincro.query(query, function (settaggi_profili) {
                                                            settaggi_profili = settaggi_profili[0];
                                                            if (comanda.persona !== null) {

                                                                var data = comanda.funzionidb.data_attuale();
                                                                var builder = new epson.ePOSBuilder();
                                                                var parola_conto = comanda.lang_stampa[60].replace(/\<br\/>/gi, "");
                                                                builder.addTextFont(builder.FONT_A);
                                                                builder.addTextAlign(builder.ALIGN_CENTER);
                                                                if (settaggi_profili.Field45.length > 0) {
                                                                    switch (settaggi_profili.Field46) {
                                                                        case "G":
                                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                            break;
                                                                        case "N":
                                                                        default:
                                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                    }
                                                                    builder.addText(settaggi_profili.Field45 + '\n');
                                                                    if (settaggi_profili.Field47 === 'true') {
                                                                        builder.addFeedLine(1);
                                                                    }
                                                                }


                                                                if (settaggi_profili.Field48.length > 0) {
                                                                    switch (settaggi_profili.Field49) {
                                                                        case "G":
                                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                            break;
                                                                        case "N":
                                                                        default:
                                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                    }
                                                                    builder.addText(settaggi_profili.Field48 + '\n');
                                                                    if (settaggi_profili.Field50 === 'true') {
                                                                        builder.addFeedLine(1);
                                                                    }
                                                                }


                                                                if (settaggi_profili.Field51.length > 0) {
                                                                    switch (settaggi_profili.Field52) {
                                                                        case "G":
                                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                            break;
                                                                        case "N":
                                                                        default:
                                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                    }
                                                                    builder.addText(settaggi_profili.Field51 + '\n');
                                                                    if (settaggi_profili.Field53 === 'true') {
                                                                        builder.addFeedLine(1);
                                                                    }
                                                                }


                                                                if (settaggi_profili.Field54.length > 0) {
                                                                    switch (settaggi_profili.Field55) {
                                                                        case "G":
                                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                            break;
                                                                        case "N":
                                                                        default:
                                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                    }
                                                                    builder.addText(settaggi_profili.Field54 + '\n');
                                                                }

                                                                builder.addText('\n\n');
                                                                builder.addTextSize(2, 2);

                                                                let riga_titolo = '';

                                                                if (comanda.tavolo === '*ASPORTO*' || comanda.tavolo === '*BAR*')
                                                                {
                                                                    riga_titolo += parola_conto + ' ' + comanda.tavolo + '\n';
                                                                    riga_titolo += '\n';
                                                                    if (comanda.tavolo === '*ASPORTO*') {
                                                                        riga_titolo += comanda.persona + '\n';
                                                                    }
                                                                } else
                                                                {
                                                                    if (settaggi_profili.Field56 === 'true') {
                                                                        riga_titolo += parola_conto;
                                                                    }
                                                                    if ((comanda.tavolo === 'BAR' || comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === 'TAKEAWAY' || comanda.tavolo === 'TAKE AWAY') && settaggi_profili.Field105 === 'true') {
                                                                        riga_titolo += ' - ' + comanda.parcheggio + '\n';
                                                                    } else if (comanda.tavolo.indexOf("CONTINUO") !== -1 && numero_progressivo !== undefined) {
                                                                        riga_titolo += ' n° ' + numero_progressivo + '\n';
                                                                    } else if (comanda.tavolo !== 'BAR' && comanda.tavolo.left(7) !== "ASPORTO" && comanda.tavolo !== 'TAKEAWAY' && comanda.tavolo !== 'TAKE AWAY')
                                                                    {
                                                                        riga_titolo += ' - n° ' + comanda.tavolo + '\n';
                                                                    } else {
                                                                        riga_titolo += ' \n';
                                                                    }
                                                                }

                                                                builder.addText(riga_titolo);

                                                                builder.addTextFont(builder.FONT_A);
                                                                builder.addTextAlign(builder.ALIGN_LEFT);
                                                                builder.addTextSize(1, 1);
                                                                builder.addFeedLine(2);
                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                if (settaggi_profili.Field58 === 'true') {
                                                                    switch (settaggi_profili.Field59) {
                                                                        case "G":
                                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                            break;
                                                                        case "N":
                                                                        default:
                                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                    }
                                                                    builder.addText(comanda.lang_stampa[109].toLowerCase().capitalize() + ': ' + comanda.operatore + '\n');
                                                                }

                                                                if (settaggi_profili.Field60 === 'true' || settaggi_profili.Field62 === 'true') {

                                                                    if (settaggi_profili.Field60 === 'true') {
                                                                        switch (settaggi_profili.Field63) {
                                                                            case "G":
                                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                break;
                                                                            case "N":
                                                                            default:
                                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                        }
                                                                        builder.addText(data.substr(0, 9).replace(/-/gi, '/') + '   ');
                                                                    }

                                                                    if (settaggi_profili.Field62 === 'true') {
                                                                        switch (settaggi_profili.Field63) {
                                                                            case "G":
                                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                break;
                                                                            case "N":
                                                                            default:
                                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                        }
                                                                        builder.addText(data.substr(9, 5));
                                                                    }

                                                                    builder.addText('\n');
                                                                }
                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                builder.addFeedLine(1);
                                                                builder.addTextSize(1, 1);
                                                                builder.addTextAlign(builder.ALIGN_CENTER);
                                                                if (comanda.stampante_piccola_conto === 'N') {
                                                                    builder.addText("-----------------------------------------\n");
                                                                } else
                                                                {
                                                                    builder.addText("--------------------------------\n");
                                                                }
                                                                builder.addTextAlign(builder.ALIGN_LEFT);
                                                                var quantita_totale = 0;
                                                                var prezzo_righe = 0;
                                                                conto.not('.non-contare-riga').each(function () {

                                                                    var articolo = $(this).find('td:nth-child(2)').html().replace(/&nbsp;/g, '');
                                                                    var quantita = $(this).find('td:nth-child(1)').html();
                                                                    var prezzo = $(this).find('td:nth-child(4)').html().replace(/&euro;/g, '');
                                                                    prezzo = prezzo.replace(/€/g, '');
                                                                    if (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(" && articolo[0] !== "*" && articolo[0] !== "x" && articolo[0] !== "=") {

                                                                        switch (settaggi_profili.Field66) {
                                                                            case "D":
                                                                                builder.addTextSize(2, 2);
                                                                                break;
                                                                            case "G":
                                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                break;
                                                                            case "N":
                                                                            default:
                                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                        }
                                                                        if (articolo.indexOf('SERVIZIO') === -1) {
                                                                            builder.addText(quantita);
                                                                            quantita_totale += parseInt(quantita);
                                                                        }
                                                                        builder.addTextPosition(35);
                                                                    } else {
                                                                        switch (settaggi_profili.Field67) {
                                                                            case "D":
                                                                                builder.addTextSize(2, 2);
                                                                                break;
                                                                            case "G":
                                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                break;
                                                                            case "N":
                                                                            default:
                                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                        }

                                                                        articolo = ' \t' + articolo;
                                                                    }

                                                                    builder.addText(articolo.slice(0, 27));
                                                                    if (comanda.stampante_piccola_conto === 'N') {

                                                                        builder.addTextPosition(400);
                                                                    } else
                                                                    {
                                                                        builder.addTextPosition(276);
                                                                    }
                                                                    if (prezzo.substr(0, 1) !== "-") {
                                                                        builder.addText('€ ' + prezzo + '\n');
                                                                        prezzo_righe += prezzo * quantita;
                                                                    } else {
                                                                        builder.addText(prezzo + '\n');
                                                                    }

                                                                });
                                                                var cifra_sconto = 0;
                                                                intestazione_conto_tr.not('.non-contare-riga').each(function () {

                                                                    var quantita = $(this).find('td:nth-child(1)').html();
                                                                    var prezzo = $(this).find('td:nth-child(4)').html().replace(/&euro;/g, '');
                                                                    var articolo = $(this).find('td:nth-child(2)').html().replace(/&nbsp;/g, '');
                                                                    if (articolo !== comanda.lang_stampa[15].toUpperCase()) {
                                                                        articolo = comanda.lang_stampa[58].toUpperCase();
                                                                        if (prezzo.slice(-1) === '%') {
                                                                            articolo += ' ' + parseInt(prezzo.slice(1, -1)) + '%';
                                                                        } else {
                                                                            articolo += ' € ' + parseFloat(prezzo.substr(1)).toFixed(2);
                                                                        }
                                                                    } else
                                                                    {
                                                                        //CALCOLO COPERTI DA TOGLIERE DAL TOTALE PER SERVIZIO
                                                                        prezzo_righe += prezzo * quantita;
                                                                    }

                                                                    console.log("PREZZO SCONTO", prezzo);
                                                                    prezzo = prezzo.replace(/€/g, '');
                                                                    if (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(" && articolo[0] !== "*" && articolo[0] !== "x" && articolo[0] !== "=") {

                                                                        switch (settaggi_profili.Field66) {
                                                                            case "D":
                                                                                builder.addTextSize(2, 2);
                                                                                break;
                                                                            case "G":
                                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                break;
                                                                            case "N":
                                                                            default:
                                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                        }
                                                                        //quantita_totale += parseInt(quantita);
                                                                        if (articolo.indexOf(comanda.lang_stampa[15]) !== -1) {
                                                                            builder.addText(quantita);
                                                                        }
                                                                        builder.addTextPosition(35);
                                                                    } else {
                                                                        switch (settaggi_profili.Field67) {
                                                                            case "D":
                                                                                builder.addTextSize(2, 2);
                                                                                break;
                                                                            case "G":
                                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                break;
                                                                            case "N":
                                                                            default:
                                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                        }

                                                                        articolo = ' \t' + articolo;
                                                                    }

                                                                    builder.addText(articolo.slice(0, 27));
                                                                    if (comanda.stampante_piccola_conto === 'N') {

                                                                        builder.addTextPosition(400);
                                                                    } else
                                                                    {
                                                                        builder.addTextPosition(276);
                                                                    }
                                                                    if (prezzo.substr(0, 1) !== "-") {
                                                                        cifra_sconto = prezzo;
                                                                        builder.addText('€ ' + cifra_sconto + '\n');
                                                                    } else if (prezzo.slice(-1) === '%') {
                                                                        console.log("CALCOLO SCONTO PERC", prezzo_righe, prezzo.slice(1, -1), prezzo_righe / 100 * parseInt(prezzo.slice(1, -1)));
                                                                        cifra_sconto = (prezzo_righe / 100 * parseInt(prezzo.slice(1, -1))).toFixed(2);
                                                                        builder.addText('€ ' + cifra_sconto + '\n');
                                                                    } else {
                                                                        cifra_sconto = prezzo.substr(1)
                                                                        builder.addText('€ ' + cifra_sconto + '\n');
                                                                    }

                                                                });
                                                                builder.addTextAlign(builder.ALIGN_CENTER);
                                                                if (comanda.stampante_piccola_conto === 'N') {
                                                                    builder.addText("-----------------------------------------\n");
                                                                } else
                                                                {
                                                                    builder.addText("--------------------------------\n");
                                                                }
                                                                builder.addTextAlign(builder.ALIGN_LEFT);
                                                                //totale
                                                                //tasse
                                                                builder.addFeedLine(1);
                                                                var totale = $('#totale_scontrino').html();
                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                //QUANTITA TOTALE
                                                                if (settaggi_profili.Field68 === 'true') {
                                                                    builder.addText(quantita_totale);
                                                                    builder.addTextPosition(35);
                                                                    builder.addText(comanda.lang_stampa[53]); //quantita
                                                                }
                                                                //TOTALE

                                                                if (comanda.lingua_stampa === 'deutsch') {
                                                                    if (comanda.stampante_piccola_conto === 'N') {
                                                                        builder.addTextPosition(310);
                                                                    } else {
                                                                        builder.addTextPosition(180);
                                                                    }
                                                                    builder.addText('Totale: ');
                                                                    builder.addText('€ ' + totale + '\n');
                                                                } else if (comanda.lingua_stampa === 'italiano') {
                                                                    if (comanda.stampante_piccola_conto === 'N') {
                                                                        builder.addTextPosition(310);
                                                                    } else {
                                                                        builder.addTextPosition(180);
                                                                    }
                                                                    builder.addText(comanda.lang_stampa[70].capitalize() + ': ');
                                                                    builder.addText('€ ' + totale + '\n');
                                                                }


                                                                //A PERSONA
                                                                if (parseInt($('#numero_coperti_effettivi').html()) > 1) {
                                                                    if (comanda.stampante_piccola_conto === 'N') {

                                                                        builder.addTextPosition(274);
                                                                    } else
                                                                    {
                                                                        builder.addTextPosition(144);
                                                                    }
                                                                    builder.addText('A persona: ');
                                                                    builder.addText('€ ' + parseFloat(parseFloat(totale) / parseInt($('#numero_coperti_effettivi').html())).toFixed(2) + '\n');
                                                                }


                                                                //--

                                                                //SETTAGGI DEL MESSAGGIO FINALE
                                                                builder.addFeedLine(2);
                                                                if (settaggi_profili.Field70 === 'G')
                                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                else if (settaggi_profili.Field70 === 'N')
                                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                if (settaggi_profili.Field71 === 'x--')
                                                                    builder.addTextAlign(builder.ALIGN_LEFT);
                                                                else if (settaggi_profili.Field71 === '-x-')
                                                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                                                else if (settaggi_profili.Field71 === '--x')
                                                                    builder.addTextAlign(builder.ALIGN_RIGHT);
                                                                if (settaggi_profili.Field69.trim().length > 0)
                                                                    builder.addText(settaggi_profili.Field69 + '\n');
                                                                if (settaggi_profili.Field72 === 'true')
                                                                    builder.addFeedLine(1);
                                                                if (settaggi_profili.Field74 === 'G')
                                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                else if (settaggi_profili.Field74 === 'N')
                                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                if (settaggi_profili.Field75 === 'x--')
                                                                    builder.addTextAlign(builder.ALIGN_LEFT);
                                                                else if (settaggi_profili.Field75 === '-x-')
                                                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                                                else if (settaggi_profili.Field75 === '--x')
                                                                    builder.addTextAlign(builder.ALIGN_RIGHT);
                                                                if (settaggi_profili.Field73.trim().length > 0)
                                                                    builder.addText(settaggi_profili.Field73 + '\n');
                                                                if (settaggi_profili.Field76 === 'true')
                                                                    builder.addFeedLine(1);
                                                                if (settaggi_profili.Field78 === 'G')
                                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                else if (settaggi_profili.Field78 === 'N')
                                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                if (settaggi_profili.Field79 === 'x--')
                                                                    builder.addTextAlign(builder.ALIGN_LEFT);
                                                                else if (settaggi_profili.Field79 === '-x-')
                                                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                                                else if (settaggi_profili.Field79 === '--x')
                                                                    builder.addTextAlign(builder.ALIGN_RIGHT);
                                                                if (settaggi_profili.Field77.trim().length > 0)
                                                                    builder.addText(settaggi_profili.Field77 + '\n');
                                                                //FINE SETTAGGI MESSAGGIO FINALE

                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                builder.addFeedLine(2);
                                                                builder.addCut(builder.CUT_FEED);
                                                                var request = builder.toString();
                                                                //CONTENUTO CONTO

                                                                //Create a SOAP envelop
                                                                var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
                                                                var url = 'http://' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                                                                if (comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].intelligent === "SDS") {
                                                                    url = 'http://' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].ip + ':8000/SPOOLER?stampante=' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].devid_nf;
                                                                }

                                                                var sconto_perc = 0;
                                                                var sconto_imp = 0;
                                                                var tipo_sconto = 'N';
                                                                //SCONTI PERCENTUALI
                                                                intestazioni_conto_percentuale.not(':contains(' + comanda.lang[15] + ')').each(function (index, element) {
                                                                    sconto_perc += parseFloat($(element).find('td:nth-child(4)').html().slice(1, -1).replace(',', '.'));
                                                                    console.log("CALCOLO SCONTISTICA TOTALI", $(element).find('td:nth-child(4)').html().slice(1, -1), typeof ($(element).find('td:nth-child(4)').html().slice(1, -1)));
                                                                    tipo_sconto = 'P';
                                                                });
                                                                //SCONTI IMPONIBILI
                                                                intestazione_conto_tr.not(':contains(%)').not(':contains(' + comanda.lang[15] + ')').each(function (index, element) {
                                                                    sconto_imp += parseFloat($(element).find('td:nth-child(4)').html().substr(1).replace(',', '.'));
                                                                    console.log("CALCOLO SCONTISTICA TOTALI", $(element).find('td:nth-child(4)').html(), typeof ($(element).find('td:nth-child(4)').html()));
                                                                    tipo_sconto = 'F';
                                                                });
                                                                //CALCOLO PERCENTUALE IN BASE ALLO SCONTO FISSO
                                                                var netto_dopo_sconto = parseFloat($('#totale_scontrino').html().replace(',', '.'));
                                                                var totale = netto_dopo_sconto + sconto_imp;
                                                                //Lo sommo alla percentuale che c'era già quindi 0
                                                                sconto_perc += 100 / (totale / sconto_imp);
                                                                console.log("CALCOLO SCONTISTICA TOTALI", totale, sconto_imp, sconto_perc, 100 / (totale / sconto_imp));
                                                                if (typeof callback === 'function')
                                                                {
                                                                    callback([url, soap, comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].nome_umano], settaggi_profili.Field80);
                                                                } else
                                                                {
                                                                    if (comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === 'TAKEAWAY' || comanda.tavolo === 'TAKE AWAY' || comanda.tavolo === 'BAR') {
                                                                        indietro_categoria();
                                                                    }
                                                                    if (settaggi_profili.Field80 === 'true') {
                                                                        aggiungi_spool_stampa(url, soap, "CONTO");
                                                                    }
                                                                    aggiungi_spool_stampa(url, soap, "CONTO");
                                                                    //TEMP DISABLED


                                                                    var data = comanda.funzionidb.data_attuale().substr(0, 8);
                                                                    var ora = comanda.funzionidb.data_attuale().substr(9, 8);
                                                                    var testo_query = "update comanda set sconto_perc='" + sconto_perc.toFixed(2) + "',sconto_imp='" + tipo_sconto + "',netto='" + cifra_sconto + "',stampata_sn='s',data_stampa='" + data + "',ora_stampa='" + ora + "',fiscalizzata_sn='n'  where stato_record='ATTIVO' and ntav_comanda='" + comanda.tavolo + "'";
                                                                    if (comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === 'TAKEAWAY') {
                                                                        var testo_query = "update comanda set sconto_perc='" + sconto_perc.toFixed(2) + "',sconto_imp='" + tipo_sconto + "',netto='" + cifra_sconto + "',stampata_sn='n',data_stampa='" + data + "',ora_stampa='" + ora + "',fiscalizzata_sn='n'  where stato_record='ATTIVO' and ntav_comanda='" + comanda.tavolo + "'";
                                                                    }

                                                                    comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                                                                    comanda.sincro.query(testo_query, function () {

                                                                        //testo_query = "update tavoli set colore='4' where numero='" + comanda.tavolo + "' ";

                                                                        testo_query = "update tavoli set colore='4',occupato='0',ora_ultima_comanda='-',ora_apertura_tavolo='-' where numero='" + comanda.tavolo + "' ";
                                                                        comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                                                                        comanda.sincro.query(testo_query, function () {

                                                                            disegna_ultimo_tavolo_modificato(testo_query);
                                                                            if (comanda.tavolo.indexOf("CONTINUO") !== -1) {
                                                                                comanda.funzionidb.conto_attivo();
                                                                            } else if (comanda.licenza === 'mattia131' || (comanda.tavolo !== '*BAR*' && comanda.tavolo !== '*ASPORTO*' && comanda.tavolo !== "BANCO" && comanda.tavolo !== "BAR" && comanda.tavolo.left(7) !== "ASPORTO" && comanda.tavolo !== "TAKE AWAY"))
                                                                            {
                                                                                btn_tavoli();
                                                                            } else
                                                                            {
                                                                                comanda.funzionidb.conto_attivo();
                                                                            }

                                                                            comanda.persona = null;
                                                                            //CHIUSURA
                                                                            comanda.sincro.query_cassa();
                                                                        });
                                                                    });
                                                                }
                                                            }
                                                        });
                                                    });
                                                }
                                            });
                                        }
                                    }
                                    /* FINE STAMPA CONTO */


                                });


                            });
                        });
                    }
                }
            }
        });
    });
}