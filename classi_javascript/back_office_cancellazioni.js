/* global riordina, parseFloat, comanda */

function popup_venduti_categorie_cancellazioni(categoria) {
    $('#popup_venduti_categorie_cancellazioni').modal('show');
}

var myChart_1_cancellazioni, myChart_2_cancellazioni;
var c_cancellazioni = "";

//ATTENZIONE: DEVE ANDARE ANCHE SE NON E' CONFIGURATO CON L'ORDINAMENTO PER CATEGORIE E NON DEVE STAMPARE QUINDI I BUILDER NON SERVONO



function graf_torta_categorie_cancellazioni() {
     comanda.array_db_centri = new Array();

    let dati_centri_statistica = alasql("select * from centri_statistica");

    

    let url_server = comanda.url_server + 'classi_php/trasferimentosqlitemysql_temp.lib.php';
    if (comanda.url_server_mysql !== undefined && comanda.url_server_mysql !== "") {
        url_server = comanda.url_server_mysql + 'classi_php/trasferimentosqlitemysql_temp.lib.php';
    }
    $.ajax({
        type: "POST",
        async: true,
        data: {url_db_replica: JSON.stringify(dati_centri_statistica.map(v => {
                return {url_db_sqlite: v.url_db_sqlite, nome_db: v.nome_db};
            })), secondi_attesa_replica_mysql: comanda.secondi_attesa_replica_mysql,
            download_local_db: download_local_db},
        url: url_server,
        dataType: "json",
        beforeSend: function () {
            $('.loader2').show();
        },
        success: function (rar) {
            if (rar === true) {

                download_local_db = false;


                $('.loader2').show();

                var prezzo_vero = " prezzo_un as ";
                var prezzo_vero_2 = " prezzo_un ";
                if (comanda.pizzeria_asporto === true) {
                    prezzo_vero = " prezzo_vero as ";
                    prezzo_vero_2 = " prezzo_vero  ";
                }


                var array_data_partenza = $('#selettore_data_partenza_grafico_cancellazioni').val();
                var array_data_arrivo = $('#selettore_data_arrivo_grafico_cancellazioni').val();


                var layout = "dettagliovenduti";
                var dati_categorie = new Array();
                var labels_categorie = new Array();
                var backgroundColor_categorie = new Array();

                var query = "select tipo,percentuale from tabella_sconti_buoni where percentuale!='';";
                comanda.sincro.query(query, function (tsb) {

                    var tabella_sconti_buoni = new Array();
                    tsb.forEach(function (e) {
                        if (e.tipo === "SCONTO") {
                            tabella_sconti_buoni.push(e.percentuale + " %");
                        } else if (e.tipo === "BUONO") {
                            tabella_sconti_buoni.push("€ " + e.percentuale);
                        }
                    });


                    var query = "select * from settaggi_profili where id=" + comanda.folder_number + ";";
                    comanda.sincro.query(query, function (settaggi_profili) {
                        settaggi_profili = settaggi_profili[0];
                        /*Tipo:
                         *
                         * soloincassi 3
                         *
                         * dettagliovenduti 2
                         *
                         * misto 1
                         *
                         */

//alert("ATTENZIONE: PRIMA DI STAMPARE I TOTALI, \nASSICURATI CHE OGNI DISPOSITIVO FUNZIONI CORRETTAMENTE \nE CHE NON CI SIANO PROBLEMI DI RETE.");

//PARTE CHIUSURA

//SETTAGGI BOOL
//DISABILITA RAGGRUPPAMENTO PORTATA
                        var set_disabilita_raggruppamento_portata = true;
                        //DISABILITA RAGGRUPPAMENTO DESTINAZIONE
                        var set_disabilita_raggruppamento_destinazione = false;
//DISABILITA RAGGRUPPAMENTO CATEGORIA
                        var set_disabilita_raggruppamento_categoria = false;
//DICHIARAZIONE VARIABILI
//TOT:0 Totale
                        var totale = 0;
//SCONTO ARTICOLO SERVE NEL CICLO
                        var prezzo_articolo = 0;
                        var sconto_articolo = 0;
                        var incasso_singolo = 0;
//TOTALE SCONTI:0
                        var totale_sconti = 0;
//NON PAGATO
                        var non_pagato = 0;
//INCASSO: 0
                        var incasso = 0;
//INIZIALIZZAZIONE OGGETTI


                        var totale_incassato = 0;
                        var totale_contanti = 0;
                        var totale_bancomat = 0;
                        var totale_cartecredito = 0;
                        var totale_buonipasto = 0;
                        var totale_buoniacquisto = 0;
                        var totale_non_pagato = 0;
                        var totale_totale = 0;
                        var totale_buoniacquisto_emessi = 0;


                        var consegna_metro = false;
                        if (comanda.consegna_mezzo_metro.length > 0 || comanda.consegna_metro.length > 0) {
                            consegna_metro = true;
                        }


                        var qta_coperti = 0;
                        var totale_coperti = 0.00;


                        var totale_consegne = new Object();


                        if (consegna_metro === true) {
                            totale_consegne["MAXI"] = {"importo": 0, "qta": 0};
                            totale_consegne["MEZZO"] = {"importo": 0, "qta": 0};
                            totale_consegne["METRO"] = {"importo": 0, "qta": 0};
                            totale_consegne["NORMALI"] = {"importo": 0, "qta": 0};
                        } else {
                            totale_consegne["PIZZE"] = {"importo": 0, "qta": 0};
                        }

                        var totale_qta_consegne = 0;
                        var totale_qta_ritiri = 0;
                        var totale_qta_mangiarequi = 0;

                        var totale_qta_ritiri = 0;
                        var totale_qta_mangiarequi = 0;
//IVA
                        var iva = new Object();
//OPERAZIONI_OPERATORE
                        var operazioni_operatore = new Object();
//ARTICOLO
                        //var articolo = new Object();
                        var articolo_generico = new Array();
                        var id_consegne = new Array();
                        var id_consegne_ritiri = new Array();
                        var id_consegne_mangiarequi = new Array();

                        //STATISTICHE PER GIORNO
                        var statistiche_giorno = new Object();
                        var giorni_estratti = new Array();

//STATISTICO
                        var statistico = new Object();
//STATISTICO['BAR']
                        statistico['BAR'] = new Object();
                        statistico['BAR'].incasso = 0;
//STATISTICO['TAKEAWAY']
                        statistico['TAKEAWAY'] = new Object();
                        statistico['TAKEAWAY'].incasso = 0;
//STATISTICO['RISTORAZIONE']
                        statistico['RISTORAZIONE'] = new Object();
                        statistico['RISTORAZIONE'].incasso = 0;
                        var fisco_teste = new Object;

                        fisco_teste['SCONTRINI'] = new Object();
                        fisco_teste['SCONTRINI'].numero = 0;
                        fisco_teste['SCONTRINI'].importo = 0;
                        fisco_teste['FATTURE'] = new Object();
                        fisco_teste['FATTURE'].numero = 0;
                        fisco_teste['FATTURE'].importo = 0;


                        fisco_teste['QUITTUNG'] = new Object();
                        fisco_teste['QUITTUNG'].numero = 0;
                        fisco_teste['QUITTUNG'].importo = 0;
                        fisco_teste['RECHNUNG'] = new Object();
                        fisco_teste['RECHNUNG'].numero = 0;
                        fisco_teste['RECHNUNG'].importo = 0;


                        //CONTEGGIO FATTURE SCONTRINI
                        var fisco = new Object();
                        fisco['FATTURE'] = new Object();
                        fisco['FATTURE'].incasso = 0;
                        fisco['FATTURE'].numero_min = null;
                        fisco['FATTURE'].numero_max = 0;
                        fisco['SCONTRINI'] = new Object();
                        fisco['SCONTRINI'].incasso = 0;
                        fisco['SCONTRINI'].numero_min = null;
                        fisco['SCONTRINI'].numero_max = 0;
                        console.log("STAMPA TOTALI QUERY OK");
                        var filtri = '';
                        var filtri_stampa = '';
                        var filtro_data = '';
                        var where_terminali = "";
                        var where_categorie = "";
                        var where_gruppi = "";
                        var where_giorni = "";
                        if (array_data_partenza.length > 1 && array_data_arrivo.length > 1) {

                            if ($('[name^=gbo_][type="checkbox"]:checked').length > 0) {

                                var array_giorni = new Array();

                                filtri += "<strong>GIORNI:</strong> ";
                                filtri_stampa += "<br/><strong>GIORNI:</strong> ";

                                $('[name^=gbo_][type="checkbox"]:checked').each(function (a, b) {


                                    array_giorni.push("'" + $(b).val() + "'");
                                    filtri += $(b).attr('nome_filtro').toLowerCase().capitalize() + ", ";
                                    filtri_stampa += $(b).attr('nome_filtro').toLowerCase().capitalize() + ", ";

                                });
                                filtri = filtri.slice(0, -2);
                                filtri_stampa = filtri_stampa.slice(0, -2);

                                var array_giorni_joined = array_giorni.join(',');


                                if (array_giorni_joined.length > 0) {
                                    where_giorni = " AND giorno IN (" + array_giorni_joined + ") ";
                                }
                            }

                            $('#elenco_centri_backoffice [type="checkbox"]:checked').each(function (a, b) {


                                comanda.array_db_centri.push($(b).val());
                                filtri += $(b).val().toLowerCase().capitalize() + ", ";
                                filtri_stampa += $(b).val().toLowerCase().capitalize() + ", ";

                            });

                            if ($('#elenco_terminali_backoffice_cancellazioni [type="checkbox"]:checked').length > 0) {

                                var array_terminali = new Array();

                                filtri += "<br/><strong>TERMINALI:</strong> ";
                                filtri_stampa += "<br/><strong>TERMINALI:</strong> ";

                                $('#elenco_terminali_backoffice_cancellazioni [type="checkbox"]:checked').each(function (a, b) {


                                    array_terminali.push("'" + $(b).val() + "'");
                                    filtri += $(b).val().toLowerCase().capitalize() + ", ";
                                    filtri_stampa += $(b).val().toLowerCase().capitalize() + ", ";

                                });

                                filtri = filtri.slice(0, -2);
                                filtri_stampa = filtri_stampa.slice(0, -2);

                                var array_terminali_joined = array_terminali.join(',');


                                if (array_terminali_joined.length > 0) {
                                    where_terminali = " AND nome_pc IN (" + array_terminali_joined + ") ";
                                }
                            }

                            if ($('#elenco_categorie_backoffice_cancellazioni [type="checkbox"]:checked').length > 0) {
                                var array_categorie = new Array();
                                filtri += "<br/><strong>CATEGORIE:</strong> ";
                                filtri_stampa += "\n<strong>CATEGORIE:</strong> ";

                                $('#elenco_categorie_backoffice_cancellazioni [type="checkbox"]:checked').each(function (a, b) {

                                    array_categorie.push("'" + $(b).val() + "'");
                                    filtri += $(b).attr('name').substr(4).toLowerCase().capitalize() + ", ";
                                    filtri_stampa += $(b).attr('name').substr(4).toLowerCase().capitalize() + ", ";


                                });

                                filtri = filtri.slice(0, -2);
                                filtri_stampa = filtri_stampa.slice(0, -2);

                                var array_categorie_joined = array_categorie.join(',');


                                if (array_categorie_joined.length > 0) {
                                    where_categorie = " AND categoria IN (" + array_categorie_joined + ") ";
                                }
                            }

                            if ($('#elenco_operatori_backoffice_cancellazioni [type="checkbox"]:checked').length > 0) {
                                var array_operatori = new Array();
                                filtri += "<br/><strong>OPERATORI:</strong><br/>";
                                filtri_stampa += "\nOPERATORI:\n";

                                $('#elenco_operatori_backoffice_cancellazioni [type="checkbox"]:checked').each(function (a, b) {

                                    array_operatori.push("'" + $(b).attr('name').substr(4) + "'");
                                    filtri += "- " + $(b).attr('name').substr(4) + "<br/>";
                                    filtri_stampa += "- " + $(b).attr('name').substr(4) + "\n";


                                });

                                var array_operatori_joined = array_operatori.join(',');


                                if (array_operatori_joined.length > 0) {
                                    where_gruppi = " AND operatore IN (" + array_operatori_joined + ") ";
                                }
                            }

                            var orario_partenza = $('#selettore_ora_partenza_grafico_cancellazioni').val().split(':');

                            var yY = array_data_partenza.substr(0, 4);
                            var yD = array_data_partenza.substr(8, 2);
                            var yM = array_data_partenza.substr(5, 2);
                            var yh = orario_partenza[0]/*"00"*/;
                            var ym = orario_partenza[1]/*"00"*/;
                            var ys = "00";
                            var yms = "000";
                            var data_ieri = yY + '' + yM + '' + yD + '' + yh + '' + ym /*+ comanda.ora_servizio*/;


                            var orario_arrivo = $('#selettore_ora_arrivo_grafico_cancellazioni').val().split(':');

                            var Y = array_data_arrivo.substr(0, 4);
                            var D = array_data_arrivo.substr(8, 2);
                            var M = array_data_arrivo.substr(5, 2);
                            var h = orario_arrivo[0]/*"00"*/;
                            var m = orario_arrivo[1]/*"00"*/;
                            var s = "00";
                            var ms = "000";
                            var data_oggi = Y + '' + M + '' + D + '' + h + '' + m /*+ comanda.ora_servizio*/;

                            var check_orario_multigiorno = $('#multigiorno_ora_grafico_cancellazioni')[0].checked;

                            if (check_orario_multigiorno === false) {
                                var orario_query = " (data_comanda>='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00') and (data_comanda<='" + Y + "-" + M + "-" + D + "' and ora_comanda <='" + h + ":" + m + ":59') ";
                                var orario_query_tes = " (data_comanda>='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00') and (data_comanda<='" + Y + "-" + M + "-" + D + "' and ora_comanda <='" + h + ":" + m + ":59') ";
                            } else {
                                var orario_query = " data_comanda>='" + yY + "-" + yM + "-" + yD + "' and data_comanda<='" + Y + "-" + M + "-" + D + "' and ora_comanda >='" + yh + ":" + ym + ":00'  and ora_comanda <='" + h + ":" + m + ":59' ";
                                var orario_query_tes = " data_comanda>='" + yY + "-" + yM + "-" + yD + "' and data_comanda<='" + Y + "-" + M + "-" + D + "' and ora_comanda >='" + yh + ":" + ym + ":00'  and ora_comanda <='" + h + ":" + m + ":59' ";

                            }


                            var cancellazioni_tavolo_cancellato = false;
                            var cancellazioni_stato_record_servizio = 0;
                            var cancellazioni_stampata_sn = 0;

                            var check_tavolo_cancellazioni = $('#check_tavolo_cancellazioni')[0].checked;

                            if (check_tavolo_cancellazioni === true) {

                                cancellazioni_tavolo_cancellato = true;
                            }


                            var check_storno_cancellazioni = $('#check_storno_cancellazioni')[0].checked;

                            if (check_storno_cancellazioni === true) {


                                cancellazioni_stato_record_servizio = -1;
                                cancellazioni_stampata_sn += +1;
                            }


                            var check_cancellazione_cancellazioni = $('#check_cancellazione_cancellazioni')[0].checked;

                            if (check_cancellazione_cancellazioni === true) {


                                cancellazioni_stato_record_servizio = -1;
                                cancellazioni_stampata_sn += -1;
                            }


                            var check_servizio_cancellazioni = $('#check_servizio_cancellazioni')[0].checked;

                            if (check_servizio_cancellazioni === true) {


                                cancellazioni_stato_record_servizio += +1;

                            }

                            var query_intera_cancellazioni = "";


                            query_intera_cancellazioni += " and (stato_record LIKE '%CANCELLATO%' ";

                            if (cancellazioni_tavolo_cancellato) {
                                query_intera_cancellazioni += "  and stato_record LIKE 'TAVOLO CANCELLATO%' ";
                            }

                            if (cancellazioni_stato_record_servizio === +1) {
                                query_intera_cancellazioni += "  and stato_record LIKE '%SERVIZIO%' ";
                            } else if (cancellazioni_stato_record_servizio === -1) {
                                query_intera_cancellazioni += "  and stato_record NOT LIKE '%SERVIZIO%' ";
                            }

                            if (cancellazioni_stampata_sn === +1) {
                                query_intera_cancellazioni += "  and stampata_sn = 'S' ";
                            } else if (cancellazioni_stampata_sn === -1) {
                                query_intera_cancellazioni += "  and stampata_sn != 'S' ";
                            }


                            query_intera_cancellazioni += ")";



                            if (array_data_partenza === array_data_arrivo) {
                                if ($('#selettore_ora_partenza_grafico').val() !== '00:00' || $('#selettore_ora_arrivo_grafico_cancellazioni').val() !== '23:59') {
                                    var data_report = yD + '/' + yM + '/' + yY + ' dalle ' + $('#selettore_ora_partenza_grafico_cancellazioni').val() + ' alle ' + $('#selettore_ora_arrivo_grafico_cancellazioni').val();
                                } else {
                                    var data_report = yD + '/' + yM + '/' + yY;
                                }
                                filtro_data += data_report + '<br/>';
                            } else {


                                if ($('#selettore_ora_partenza_grafico_cancellazioni').val() !== '00:00' || $('#selettore_ora_arrivo_grafico_cancellazioni').val() !== '23:59') {
                                    var data_report = 'da: ' + yD + '/' + yM + '/' + yY + ' alle ' + $('#selettore_ora_partenza_grafico_cancellazioni').val() + '\na: ' + D + '/' + M + '/' + Y + ' alle ' + $('#selettore_ora_arrivo_grafico_cancellazioni').val();
                                    filtro_data += 'da: ' + yD + '/' + yM + '/' + yY + ' alle ' + $('#selettore_ora_partenza_grafico_cancellazioni').val() + ' a: ' + D + '/' + M + '/' + Y + ' alle ' + $('#selettore_ora_arrivo_grafico_cancellazioni').val() + '<br/>';
                                } else {
                                    var data_report = 'da: ' + yD + '/' + yM + '/' + yY + '\na: ' + D + '/' + M + '/' + Y;
                                    filtro_data += 'da: ' + yD + '/' + yM + '/' + yY + ' a: ' + D + '/' + M + '/' + Y + '<br/>';
                                }
                            }
                        } else {

                            var d = new Date();
                            d.setDate(d.getDate() + 1);
                            var Y = d.getFullYear();
                            var M = addZero((d.getMonth() + 1), 2);
                            var D = addZero(d.getDate(), 2);
                            var h = addZero(d.getHours(), 2);
                            var m = addZero(d.getMinutes(), 2);
                            var s = addZero(d.getSeconds(), 2);
                            var ms = addZero(d.getMilliseconds(), 3);
                            var data_oggi = Y + '' + M + '' + D + '' + comanda.ora_servizio + '00';
                            var y = new Date();
                            y.setDate(y.getDate());
                            var yY = y.getFullYear();
                            var yM = addZero((y.getMonth() + 1), 2);
                            var yD = addZero(y.getDate(), 2);
                            var yh = addZero(y.getHours(), 2);
                            var ym = addZero(y.getMinutes(), 2);
                            var ys = addZero(y.getSeconds(), 2);
                            var yms = addZero(y.getMilliseconds(), 3);
                            var data_ieri = yY + '' + yM + '' + yD + '' + comanda.ora_servizio + '00';

                            var check_orario_multigiorno = $('#multigiorno_ora_grafico_cancellazioni')[0].checked;

                            if (check_orario_multigiorno === false) {
                                var orario_query = " (data_comanda>='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00') and (data_comanda<='" + yY + "-" + yM + "-" + yD + "' and ora_comanda <='" + h + ":" + m + ":59') ";
                                var orario_query_tes = " (data_comanda>='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00') and (data_comanda<='" + yY + "-" + yM + "-" + yD + "' and ora_comanda <='" + h + ":" + m + ":59') ";

                            } else {
                                var orario_query = " data_comanda>='" + yY + "-" + yM + "-" + yD + "' and data_comanda<='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00'  and ora_comanda <='" + h + ":" + m + ":59' ";
                                var orario_query_tes = " data_comanda>='" + yY + "-" + yM + "-" + yD + "' and data_comanda<='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00'  and ora_comanda <='" + h + ":" + m + ":59' ";

                            }

                            var cancellazioni_tavolo_cancellato = false;
                            var cancellazioni_stato_record_servizio = 0;
                            var cancellazioni_stampata_sn = 0;

                            var check_tavolo_cancellazioni = $('#check_tavolo_cancellazioni')[0].checked;

                            if (check_tavolo_cancellazioni === true) {

                                cancellazioni_tavolo_cancellato = true;
                            }


                            var check_storno_cancellazioni = $('#check_storno_cancellazioni')[0].checked;

                            if (check_storno_cancellazioni === true) {


                                cancellazioni_stato_record_servizio = -1;
                                cancellazioni_stampata_sn += +1;
                            }


                            var check_cancellazione_cancellazioni = $('#check_cancellazione_cancellazioni')[0].checked;

                            if (check_cancellazione_cancellazioni === true) {


                                cancellazioni_stato_record_servizio = -1;
                                cancellazioni_stampata_sn += -1;
                            }


                            var check_servizio_cancellazioni = $('#check_servizio_cancellazioni')[0].checked;

                            if (check_servizio_cancellazioni === true) {


                                cancellazioni_stato_record_servizio += +1;

                            }

                            var query_intera_cancellazioni = "";


                            query_intera_cancellazioni += " and (stato_record LIKE '%CANCELLATO%' ";

                            if (cancellazioni_tavolo_cancellato) {
                                query_intera_cancellazioni += "  and stato_record LIKE 'TAVOLO CANCELLATO%' ";
                            }

                            if (cancellazioni_stato_record_servizio === +1) {
                                query_intera_cancellazioni += "  and stato_record LIKE '%SERVIZIO%' ";
                            } else if (cancellazioni_stato_record_servizio === -1) {
                                query_intera_cancellazioni += "  and stato_record NOT LIKE '%SERVIZIO%' ";
                            }

                            if (cancellazioni_stampata_sn === +1) {
                                query_intera_cancellazioni += "  and stampata_sn = 'S' ";
                            } else if (cancellazioni_stampata_sn === -1) {
                                query_intera_cancellazioni += "  and stampata_sn != 'S' ";
                            }


                            query_intera_cancellazioni += ")";


                            var data_report = yD + '/' + yM + '/' + yY;
                            filtro_data += data_report + '<br/>';
                        }
//CICLO DB CENTRALE WHERE STATO RECORD CHIUSO AND POSIZIONE CONTO

                        var tempo_iniziale = new Date().getTime();

                        var orario_multigiorno = $('#multigiorno_ora_grafico_cancellazioni')[0].checked;

                        /* String */
                        var condizione_fiscalizzata_sn = " and fiscalizzata_sn='NONVALIDA' ";

                        var password = CryptoJS.MD5($('#campo_password_cancellazioni').val()).toString();
                        var pass_statistiche_teste = CryptoJS.MD5("FI" + comanda.numero_licenza_cliente).toString();

                        var pass_statistiche_comande = CryptoJS.MD5("FI" + comanda.numero_licenza_cliente + "TOTALE").toString();
                        if (comanda.numero_licenza_cliente === "0595") {
                            pass_statistiche_comande = CryptoJS.MD5("FI" + comanda.numero_licenza_cliente + "IMPERIAL").toString();
                        }

                        if (password === pass_statistiche_teste) {
                            condizione_fiscalizzata_sn = " and fiscalizzata_sn='S' ";
                        } else if (comanda.visualizza_tutti_venduti === true && password === pass_statistiche_comande)
                        {
                            condizione_fiscalizzata_sn = " and (fiscalizzata_sn='S' OR tipo_ricevuta='conto') ";
                        } else {
                            $('.loader2').hide();
                            bootbox.alert("Inserire una password valida!");
                            return false;
                        }


                        var query_articoli = "select numero, " + comanda.lingua_stampa + " from nomi_portate order by numero asc;";
                        comanda.sincro.query_cassa(query_articoli, 1000000, function (result_portate) {

                            var query_articoli = "select id, descrizione from categorie order by id asc;";
                            comanda.sincro.query_cassa(query_articoli, 1000000, function (result_categorie) {

                                //Nomi categorie da id
                                var nomi_categorie = new Object();
                                for (var a in result_categorie) {
                                    nomi_categorie[result_categorie[a].id] = result_categorie[a].descrizione;
                                }

                                var query_articoli = "select id, nome from gruppi_statistici order by id asc;";
                                comanda.sincro.query_cassa(query_articoli, 1000000, function (result_gruppi) {


                                    var nomi_gruppi = new Object();
                                    for (var a in result_gruppi) {
                                        nomi_gruppi[result_gruppi[a].id] = result_gruppi[a].nome;
                                    }

                                    var query_articoli = "update comanda set sconto_perc='0.00' where sconto_perc is null;";
                                    comanda.sincro.query_cassa(query_articoli, 1000000, function () {

                                        var query_articoli = "SELECT id,gruppo,giorno,sconto_imp," + prezzo_vero_2 + " as valore_sconto, sum(quantita) as quantita_totale, sum(netto) as totale_sconti FROM comanda WHERE  desc_art NOT LIKE '%COD:%' and  posizione='SCONTO' " + where_terminali + where_categorie + where_gruppi + where_giorni + " " + query_intera_cancellazioni + "  and " + orario_query + " group by " + prezzo_vero_2 + " UNION ALL ";
                                        query_articoli += "SELECT id,gruppo,giorno,sconto_imp," + prezzo_vero_2 + " as valore_sconto, sum(quantita) as quantita_totale, sum(netto) as totale_sconti FROM comanda_temp WHERE  desc_art NOT LIKE '%COD:%' and  posizione='SCONTO' " + where_terminali + where_categorie + where_gruppi + where_giorni + " " + query_intera_cancellazioni + " group by " + prezzo_vero_2 + " ;";

                                        if (comanda.array_db_centri.length > 0) {

                                            var query_articoli = "";
                                            var i = 1;

                                            comanda.array_db_centri.forEach(centro => {

                                                query_articoli += "SELECT id,gruppo,giorno,sconto_imp," + prezzo_vero_2 + " as valore_sconto, sum(quantita) as quantita_totale, sum(netto) as totale_sconti FROM " + centro + ".comanda WHERE  desc_art NOT LIKE '%COD:%' and  posizione='SCONTO' " + where_terminali + where_categorie + where_gruppi + where_giorni + " " + query_intera_cancellazioni + "  and " + orario_query + " group by " + prezzo_vero_2 + " UNION ALL ";
                                                query_articoli += "SELECT id,gruppo,giorno,sconto_imp," + prezzo_vero_2 + " as valore_sconto, sum(quantita) as quantita_totale, sum(netto) as totale_sconti FROM " + centro + ".comanda_temp WHERE  desc_art NOT LIKE '%COD:%' and  posizione='SCONTO' " + where_terminali + where_categorie + where_gruppi + where_giorni + " " + query_intera_cancellazioni + " group by " + prezzo_vero_2 + " ";

                                                if (i !== comanda.array_db_centri.length) {
                                                    query_articoli += " UNION ALL ";
                                                }

                                                i++;

                                            });
                                        }


                                        //DA ORDINARE MANUALMENTE
                                        //CAMBIARE WHERE GIORNI E WHERE TERMINALI

                                        //FARE PROMISE 
                                        var p1 = new Promise(function (resolve, reject) {
                                            comanda.sincro.query_incassi(query_articoli, 1000000, function (result_sconti) {

                                                var rs = new Array();

                                                result_sconti.forEach(function (e) {
                                                    if (e.sconto_imp === "P") {
                                                        e.valore_sconto = e.valore_sconto + " %";
                                                    }
                                                    rs.push(e);
                                                });

                                                resolve(rs);
                                            });

                                        });


                                        var giorno_incasso_gb = "";
                                        if (array_giorni_joined !== undefined) {
                                            giorno_incasso_gb = "and (giorno_incasso IN (" + array_giorni_joined + ") or giorno_emissione IN(" + array_giorni_joined + "))";
                                        }

                                        var terminali_gb = "";
                                        if (array_terminali_joined !== undefined) {
                                            terminali_gb = "and (nome_pc_incasso IN (" + array_terminali_joined + ") or nome_pc_emissione IN (" + array_terminali_joined + "))";
                                        }
                                        var query_articoli = "SELECT * FROM gestione_buoni_sconto WHERE  ((data_emissione_ascii>='" + yY + "" + yM + "" + yD + "" + yh + "" + ym + "" + ys + "' and data_emissione<'" + Y + "" + M + "" + D + "" + h + "" + m + "" + s + "') or (data_utilizzo_ascii>='" + yY + "" + yM + "" + yD + "" + yh + "" + ym + "" + ys + "' and data_utilizzo_ascii<'" + Y + "" + M + "" + D + "" + h + "" + m + "" + s + "')) " + giorno_incasso_gb + " " + terminali_gb + " order by percentuale_buono asc;";


                                        var p2 = new Promise(function (resolve, reject) {
                                            comanda.sincro.query_centrale(query_articoli, function (result_buoni_sconto) {
                                                resolve(result_buoni_sconto);
                                            });

                                        });


                                        var query_articoli_zero = "";
                                        if (comanda.escludi_art_zero_statistiche === true) {
                                            query_articoli_zero = " and " + prezzo_vero_2 + "!=0 ";
                                        }

                                        var ordinamento = "descrizione";
                                        //SE E' NEVODI ORDINA PER COD.ARTICOLO
                                        if (comanda.numero_licenza_cliente === "0568") {
                                            ordinamento = "cod_articolo";
                                        }

                                        if (ordinamento === "cod_articolo") {

                                            prodotti = new Object();

                                            alasql.tables.prodotti.data.forEach(function (prodotto) {
                                                prodotti[prodotto.id] = new Object();
                                                prodotti[prodotto.id] = prodotto.descrizione;
                                            });
                                        }


                                        var query_articoli = "select stampata_sn,nome_comanda,data_comanda,ora_comanda,stato_record,nome_pc,id,gruppo,giorno,fiscalizzata_sn,tipo_ricevuta,n_fiscale,operatore,operatore_cancellazione,ntav_comanda,quantita,desc_art,sconto_perc,perc_iva,categoria,dest_stampa,portata," + prezzo_vero_2 + " as prezzo_un,QTA,tipo_consegna,cod_articolo from comanda where (posizione='CONTO' OR posizione='COPERTO') " + where_terminali + where_categorie + where_gruppi + where_giorni + "  " + query_intera_cancellazioni + " and " + orario_query + " and portata!='ZZZ'  and cod_promo!=1 " + query_articoli_zero + "";
                                        query_articoli += " UNION ALL select stampata_sn,nome_comanda,data_comanda,ora_comanda,stato_record,nome_pc,id,gruppo,giorno,fiscalizzata_sn,tipo_ricevuta,n_fiscale,operatore,operatore_cancellazione,ntav_comanda,quantita,desc_art,sconto_perc,perc_iva,categoria,dest_stampa,portata," + prezzo_vero_2 + " as prezzo_un,QTA,tipo_consegna,cod_articolo from comanda_temp where (posizione='CONTO' OR posizione='COPERTO') " + where_terminali + where_categorie + where_gruppi + where_giorni + "  " + query_intera_cancellazioni + " and " + orario_query + " and portata!='ZZZ'  and cod_promo!=1 " + query_articoli_zero + ";";

                                        if (comanda.array_db_centri.length > 0) {

                                            var query_articoli = "";
                                            var i = 1;

                                            comanda.array_db_centri.forEach(centro => {

                                                query_articoli += "select stampata_sn,nome_comanda,data_comanda,ora_comanda,stato_record,nome_pc,id,gruppo,giorno,fiscalizzata_sn,tipo_ricevuta,n_fiscale,operatore,operatore_cancellazione,ntav_comanda,quantita,desc_art,sconto_perc,perc_iva,categoria,dest_stampa,portata," + prezzo_vero_2 + " as prezzo_un,QTA,tipo_consegna,cod_articolo from " + centro + ".comanda where (posizione='CONTO' OR posizione='COPERTO') " + where_terminali + where_categorie + where_gruppi + where_giorni + "  " + query_intera_cancellazioni + " and " + orario_query + " and portata!='ZZZ'  and cod_promo!=1 " + query_articoli_zero + "";
                                                query_articoli += " UNION ALL select stampata_sn,nome_comanda,data_comanda,ora_comanda,stato_record,nome_pc,id,gruppo,giorno,fiscalizzata_sn,tipo_ricevuta,n_fiscale,operatore,operatore_cancellazione,ntav_comanda,quantita,desc_art,sconto_perc,perc_iva,categoria,dest_stampa,portata," + prezzo_vero_2 + " as prezzo_un,QTA,tipo_consegna,cod_articolo from " + centro + ".comanda_temp where (posizione='CONTO' OR posizione='COPERTO') " + where_terminali + where_categorie + where_gruppi + where_giorni + "  " + query_intera_cancellazioni + " and " + orario_query + " and portata!='ZZZ'  and cod_promo!=1 " + query_articoli_zero + " ";
                                                if (i !== comanda.array_db_centri.length) {
                                                    query_articoli += " UNION ALL ";
                                                }

                                                i++;

                                            });
                                        }

                                        console.log("STAMPA TOTALI QUERY ARTICOLI", query_articoli);
                                        var p3 = new Promise(function (resolve, reject) {
                                            comanda.sincro.query_incassi(query_articoli, 1000000, function (result) {
                                                resolve(result);
                                            });

                                        });

                                        var p4 = new Promise(function (resolve, reject) {
                                            resolve([]);
                                        });


                                        var oggetto_gestione_buoni = new Object();

                                        //AGGIUNGERE TUTTE LE PROMISE
                                        Promise.all([p1, p2, p3, p4]).then(function (values) {

                                            var result_sconti = values[0];
                                            var result_buoni_sconto = values[1];
                                            var result = values[2];
                                            var result_teste = values[3];

                                            result_buoni_sconto.forEach(function (v) {
                                                if (oggetto_gestione_buoni[v.percentuale_buono] === undefined) {
                                                    oggetto_gestione_buoni[v.percentuale_buono] = new Object();
                                                    oggetto_gestione_buoni[v.percentuale_buono].emessi = 0;
                                                    oggetto_gestione_buoni[v.percentuale_buono].riscossi = 0;
                                                    oggetto_gestione_buoni[v.percentuale_buono].scaduti = 0;
                                                    oggetto_gestione_buoni[v.percentuale_buono].euro = 0; //TEMP
                                                }
                                                if (v.data_utilizzo_ascii >= yY + "" + yM + "" + yD + "" + yh + "" + ym + "" + ys && v.data_utilizzo_ascii < Y + "" + M + "" + D + "" + h + "" + m + "" + s) {
                                                    oggetto_gestione_buoni[v.percentuale_buono].riscossi++;
                                                    oggetto_gestione_buoni[v.percentuale_buono].euro += parseFloat(v.valore_euro);
                                                }
                                                if (new Date() > parseDate(v.data_scadenza)) {
                                                    oggetto_gestione_buoni[v.percentuale_buono].scaduti++;
                                                }
                                                if (v.data_emissione_ascii >= yY + "" + yM + "" + yD + "" + yh + "" + ym + "" + ys && v.data_emissione_ascii < Y + "" + M + "" + D + "" + h + "" + m + "" + s)
                                                {
                                                    oggetto_gestione_buoni[v.percentuale_buono].emessi++;
                                                }


                                            });




                                            var tempo2 = new Date().getTime();
                                            console.log("FINE PARTE 1 - CARICAMENTO DATI DA DATABASE", (tempo2 - tempo_iniziale) / 1000);

                                            for (var key in result_teste) {
                                                var el = result_teste[key];

                                                if (isNaN(parseFloat(el.resto))) {
                                                    el.resto = "0";
                                                }

                                                if (isNaN(parseFloat(el.contanti))) {
                                                    el.contanti = "0";
                                                }

                                                if (isNaN(parseFloat(el.bancomat))) {
                                                    el.bancomat = "0";
                                                }

                                                if (isNaN(parseFloat(el.carte_credito2))) {
                                                    el.carte_credito2 = "0";
                                                }

                                                if (isNaN(parseFloat(el.carte_credito3))) {
                                                    el.carte_credito3 = "0";
                                                }

                                                if (isNaN(parseFloat(el.buoni_pasto))) {
                                                    el.buoni_pasto = "0";
                                                }

                                                if (isNaN(parseFloat(el.numero_buoni_pasto))) {
                                                    el.numero_buoni_pasto = "0";
                                                }

                                                if (isNaN(parseFloat(el.buoni_pasto_2))) {
                                                    el.buoni_pasto_2 = "0";
                                                }

                                                if (isNaN(parseFloat(el.numero_buoni_pasto_2))) {
                                                    el.numero_buoni_pasto_2 = "0";
                                                }

                                                if (isNaN(parseFloat(el.buoni_pasto_3))) {
                                                    el.buoni_pasto_3 = "0";
                                                }

                                                if (isNaN(parseFloat(el.numero_buoni_pasto_3))) {
                                                    el.numero_buoni_pasto_3 = "0";
                                                }

                                                if (isNaN(parseFloat(el.buoni_pasto_4))) {
                                                    el.buoni_pasto_4 = "0";
                                                }

                                                if (isNaN(parseFloat(el.numero_buoni_pasto_4))) {
                                                    el.numero_buoni_pasto_4 = "0";
                                                }

                                                if (isNaN(parseFloat(el.buoni_pasto_5))) {
                                                    el.buoni_pasto_5 = "0";
                                                }

                                                if (isNaN(parseFloat(el.numero_buoni_pasto_5))) {
                                                    el.numero_buoni_pasto_5 = "0";
                                                }

                                                if (isNaN(parseFloat(el.buoni_acquisto))) {
                                                    el.buoni_acquisto = "0";
                                                }

                                                if (isNaN(parseFloat(el.non_pagato))) {
                                                    el.non_pagato = "0";
                                                }

                                                if (isNaN(parseFloat(el.buoni_acquisto_emessi))) {
                                                    el.buoni_acquisto_emessi = "0";
                                                }



                                                switch (el.tipologia) {
                                                    case 'SCONTRINO':
                                                        fisco_teste['SCONTRINI'].numero++;
                                                        fisco_teste['SCONTRINI'].importo += parseFloat(el.contanti) + parseFloat(el.bancomat) + parseFloat(el.carte_credito) + parseFloat(el.carte_credito2) + parseFloat(el.carte_credito3) + (parseFloat(el.buoni_pasto) * parseFloat(el.numero_buoni_pasto)) + parseFloat(el.buoni_acquisto) + parseFloat(el.non_pagato);
                                                        break;
                                                    case 'FATTURA':
                                                        fisco_teste['FATTURE'].numero++;
                                                        fisco_teste['FATTURE'].importo += parseFloat(el.contanti) + parseFloat(el.bancomat) + parseFloat(el.carte_credito) + parseFloat(el.carte_credito2) + parseFloat(el.carte_credito3) + (parseFloat(el.buoni_pasto) * parseFloat(el.numero_buoni_pasto)) + parseFloat(el.buoni_acquisto) + parseFloat(el.non_pagato);
                                                        break;
                                                    case 'QUITTUNG':
                                                        fisco_teste['QUITTUNG'].numero++;
                                                        fisco_teste['QUITTUNG'].importo += parseFloat(el.contanti) + parseFloat(el.bancomat) + parseFloat(el.carte_credito) + parseFloat(el.carte_credito2) + parseFloat(el.carte_credito3) + (parseFloat(el.buoni_pasto) * parseFloat(el.numero_buoni_pasto)) + parseFloat(el.buoni_acquisto) + parseFloat(el.non_pagato);
                                                        break;
                                                    case 'RECHNUNG':
                                                        fisco_teste['RECHNUNG'].numero++;
                                                        fisco_teste['RECHNUNG'].importo += parseFloat(el.contanti) + parseFloat(el.bancomat) + parseFloat(el.carte_credito) + parseFloat(el.carte_credito2) + parseFloat(el.carte_credito3) + (parseFloat(el.buoni_pasto) * parseFloat(el.numero_buoni_pasto)) + parseFloat(el.buoni_acquisto) + parseFloat(el.non_pagato);
                                                        break;
                                                }

                                                totale_contanti += parseFloat(el.contanti);
                                                totale_bancomat += parseFloat(el.bancomat);
                                                totale_cartecredito += parseFloat(el.carte_credito);
                                                totale_cartecredito += parseFloat(el.carte_credito2);
                                                totale_cartecredito += parseFloat(el.carte_credito3);
                                                totale_buonipasto += parseFloat(el.buoni_pasto) * parseFloat(el.numero_buoni_pasto);
                                                totale_buoniacquisto += parseFloat(el.buoni_acquisto);
                                                totale_buoniacquisto_emessi += parseFloat(el.buoni_acquisto_emessi);
                                                totale_totale += parseFloat(el.contanti) + parseFloat(el.bancomat) + parseFloat(el.carte_credito) + parseFloat(el.carte_credito2) + parseFloat(el.carte_credito3) + (parseFloat(el.buoni_pasto) * parseFloat(el.numero_buoni_pasto)) + parseFloat(el.buoni_acquisto) + parseFloat(el.non_pagato);
                                                totale_incassato += parseFloat(el.contanti) + parseFloat(el.bancomat) + parseFloat(el.carte_credito) + parseFloat(el.carte_credito2) + parseFloat(el.carte_credito3);
                                                totale_non_pagato += parseFloat(el.non_pagato);


                                            }

                                            var tempo3 = new Date().getTime();
                                            console.log("FINE PARTE 2 - PARSING TESTE", (tempo3 - tempo2) / 1000);



                                            var i = 0;

                                            var gbo = new Object();
                                            gbo["1"] = $('[name="gbo_1"]')[0].checked;
                                            gbo["2"] = $('[name="gbo_2"]')[0].checked;
                                            gbo["3"] = $('[name="gbo_3"]')[0].checked;
                                            gbo["4"] = $('[name="gbo_4"]')[0].checked;
                                            gbo["5"] = $('[name="gbo_5"]')[0].checked;
                                            gbo["6"] = $('[name="gbo_6"]')[0].checked;
                                            gbo["7"] = $('[name="gbo_7"]')[0].checked;

                                            if (gbo["1"] !== true && gbo["2"] !== true && gbo["3"] !== true && gbo["4"] !== true && gbo["5"] !== true && gbo["6"] !== true && gbo["7"] !== true) {
                                                gbo["1"] = true;
                                                gbo["2"] = true;
                                                gbo["3"] = true;
                                                gbo["4"] = true;
                                                gbo["5"] = true;
                                                gbo["6"] = true;
                                                gbo["7"] = true;
                                            }

                                            for (var index in result) {

                                                //INIZIALIZZO L'OGGETTO OGNI VOLTA
                                                var obj = result[index];




                                                if (gbo[obj.giorno] === true)
                                                {


                                                    if (statistiche_giorno[obj.giorno] === undefined) {
                                                        statistiche_giorno[obj.giorno] = new Object();
                                                        statistiche_giorno[obj.giorno].numero = 0;
                                                    }
                                                    var ora_comanda = obj.id.substr(12, 2);

                                                    if (statistiche_giorno[obj.giorno][ora_comanda] === undefined) {
                                                        statistiche_giorno[obj.giorno][ora_comanda] = new Object();

                                                        statistiche_giorno[obj.giorno][ora_comanda].totale = 0.0;

                                                    }
                                                    statistiche_giorno[obj.giorno].numero++;

                                                    var prezzo_un = parseFloat(obj.prezzo_un);
                                                    if (obj.sconto_perc) {
                                                        prezzo_un = (parseFloat(obj.prezzo_un) / 100) * (100 - parseFloat(obj.sconto_perc));
                                                    }
                                                    statistiche_giorno[obj.giorno][ora_comanda].totale += prezzo_un;
                                                }

                                                //console.log("GIORNO MEDIA", obj.id.substr(4, 8));
                                                if (obj !== undefined && obj.id !== undefined && giorni_estratti.indexOf(obj.id.substr(4, 8)) === -1) {
                                                    giorni_estratti.push(obj.id.substr(4, 8));
                                                }


                                                var numero_giorni_media = 0;
                                                if (giorni_estratti !== undefined) {
                                                    numero_giorni_media = giorni_estratti.length;
                                                }

                                                if (set_disabilita_raggruppamento_portata === true)
                                                {
                                                    obj.portata = "default";
                                                }

                                                if (set_disabilita_raggruppamento_destinazione === true)
                                                {
                                                    obj.dest_stampa = "default";
                                                }

                                                if (set_disabilita_raggruppamento_categoria === true)
                                                {
                                                    obj.categoria = "default";
                                                }

                                                if (obj.prezzo_un === undefined || obj.prezzo_un === null || isNaN(obj.prezzo_un)) {
                                                    obj.prezzo_un = 0;
                                                }

                                                if (obj.quantita === undefined || obj.quantita === null || isNaN(obj.quantita)) {
                                                    obj.prezzo_un = 0;
                                                }


                                                if (obj.sconto_perc === undefined || obj.sconto_perc === null || isNaN(obj.sconto_perc) || obj.sconto_perc === "") {
                                                    obj.sconto_perc = 0;
                                                }

                                                if (obj.QTA === undefined || obj.QTA === null || isNaN(obj.QTA)) {
                                                    obj.QTA = 0;

                                                }


                                                if (obj.desc_art === "COPERTI") {
                                                    qta_coperti += parseInt(obj.quantita);
                                                    totale_coperti += ((parseFloat(obj.prezzo_un) * parseInt(obj.quantita)) * 100) / (100 - parseFloat(obj.sconto_perc));

                                                }



                                                if (comanda.pizzeria_asporto === true) {
                                                    if (parseFloat(obj.QTA) > 0) {
                                                        if (obj.tipo_consegna === "DOMICILIO") {

                                                            if (id_consegne.find(o => o.id === obj.id) === undefined) {

                                                                id_consegne.push({id: obj.id});
                                                                totale_qta_consegne++;

                                                            }

                                                            if (consegna_metro === true && comanda.categorie_con_consegna_abilitata.indexOf(obj.categoria) !== -1) {
                                                                if (obj.desc_art.indexOf("MAXI") !== -1) {
                                                                    totale_consegne['MAXI'].importo += parseFloat(obj.QTA) * parseFloat(obj.quantita) - (parseFloat(obj.QTA) * parseFloat(obj.quantita) / 100 * parseFloat(obj.sconto_perc));
                                                                    totale_consegne['MAXI'].qta++;
                                                                } else if (obj.desc_art.indexOf("MEZZO METRO") !== -1) {
                                                                    totale_consegne['MEZZO'].importo += parseFloat(obj.QTA) * parseFloat(obj.quantita) - (parseFloat(obj.QTA) * parseFloat(obj.quantita) / 100 * parseFloat(obj.sconto_perc));
                                                                    totale_consegne['MEZZO'].qta++;
                                                                } else if (obj.desc_art.indexOf("UN METRO") !== -1) {
                                                                    totale_consegne['METRO'].importo += parseFloat(obj.QTA) * parseFloat(obj.quantita) - (parseFloat(obj.QTA) * parseFloat(obj.quantita) / 100 * parseFloat(obj.sconto_perc));
                                                                    totale_consegne['METRO'].qta++;
                                                                } else {
                                                                    totale_consegne['NORMALI'].importo += parseFloat(obj.QTA) * parseFloat(obj.quantita) - (parseFloat(obj.QTA) * parseFloat(obj.quantita) / 100 * parseFloat(obj.sconto_perc));
                                                                    totale_consegne['NORMALI'].qta++;
                                                                }
                                                            } else {
                                                                totale_consegne['PIZZE'].importo += parseFloat(obj.QTA) * parseFloat(obj.quantita) - (parseFloat(obj.QTA) * parseFloat(obj.quantita) / 100 * parseFloat(obj.sconto_perc));
                                                                totale_consegne['PIZZE'].qta++;
                                                            }

                                                        }
                                                    } else if (obj.tipo_consegna === "RITIRO") {

                                                        if (id_consegne_ritiri.find(o => o.id === obj.id) === undefined) {

                                                            id_consegne_ritiri.push({id: obj.id});
                                                            totale_qta_ritiri++;
                                                        }

                                                    } else if (obj.tipo_consegna === "PIZZERIA") {


                                                        if (id_consegne_mangiarequi.find(o => o.id === obj.id) === undefined) {

                                                            id_consegne_mangiarequi.push({id: obj.id});
                                                            totale_qta_mangiarequi++;
                                                        }

                                                    }
                                                }

//-------------CONTABILE-----------

                                                //console.log("STAMPA TOTALI IN CICLO", obj);
                                                //
                                                var ordinamento = "descrizione";
                                                //SE E' NEVODI ORDINA PER COD.ARTICOLO
                                                if (comanda.numero_licenza_cliente === "0568") {
                                                    ordinamento = "cod_articolo";
                                                }
                                                if (ordinamento === "cod_articolo" && prodotti[obj.cod_articolo] !== undefined) {
                                                    obj.desc_art = prodotti[obj.cod_articolo];
                                                }

                                                if (obj.gruppo === undefined || obj.gruppo === null || obj.gruppo === "undefined" || obj.gruppo === "null") {
                                                    obj.gruppo = "";
                                                }

                                                //ARTICOLO GENERICO

                                                var tipo_cancellazione = "";

                                                if (obj.stampata_sn === "S") {
                                                    if (obj.stato_record.indexOf("TAVOLO CANCELLATO") !== -1) {
                                                        tipo_cancellazione = "S.TAV";
                                                    } else if (obj.stato_record.indexOf("SERVIZIO") !== -1) {
                                                        tipo_cancellazione = "S.C.SERV";
                                                    } else {
                                                        tipo_cancellazione = "STORNO";
                                                    }

                                                } else {
                                                    if (obj.stato_record.indexOf("TAVOLO CANCELLATO") !== -1) {
                                                        tipo_cancellazione = "TAV.C";
                                                    } else if (obj.stato_record.indexOf("SERVIZIO") !== -1) {
                                                        tipo_cancellazione = "C.SERV";
                                                    } else {
                                                        tipo_cancellazione = "CANC.";
                                                    }
                                                }

                                                var ora_cancellazione = "";

                                                if (obj.stato_record.match(/(?:[01]\d|2[0123]):(?:[012345]\d)/g) !== null) {
                                                    ora_cancellazione = obj.stato_record.match(/(?:[01]\d|2[0123]):(?:[012345]\d)/g)[0] + " - " + obj.operatore_cancellazione.substr(0, 6);
                                                }

                                                var data_cancellazione = "";

                                                if (obj.stato_record.match(/([0-2][0-9]|(3)[0-1])(-)(((0)[0-9])|((1)[0-2]))(-)\d{2}/g) !== null) {
                                                    data_cancellazione = obj.stato_record.match(/([0-2][0-9]|(3)[0-1])(-)(((0)[0-9])|((1)[0-2]))(-)\d{2}/g)[0].replace(/-/gi, "/");
                                                }

                                                var ora_battitura = obj.ora_comanda.substr(0, 5) + " - " + obj.operatore.substr(0, 6);

                                                var data_battitura = obj.data_comanda.substr(8, 2) + "/" + obj.data_comanda.substr(5, 2) + "/" + obj.data_comanda.substr(2, 2);


                                                var nome_tavolo = obj.ntav_comanda;
                                                if (obj.nome_comanda && (nome_tavolo.indexOf("ASPORTO") !== -1 || nome_tavolo.indexOf("BAR") !== -1 || nome_tavolo.indexOf("CONTINUO") !== -1)) {
                                                    nome_tavolo += " - " + obj.nome_comanda;
                                                }

                                                articolo_generico[i] = {'id': obj.id, 'gruppo': obj.gruppo, 'perc_iva': obj.perc_iva, 'giorno': obj.giorno, 'destinazione': obj.dest_stampa, 'categoria': obj.categoria, 'portata': obj.portata, 'descrizione': obj.desc_art.trim(), 'quantita': obj.quantita, 'prezzo_un': obj.prezzo_un, sconto_perc: obj.sconto_perc, 'posizione': obj.posizione, 'QTA': obj.QTA, 'cod_articolo': obj.cod_articolo, 'ntav_comanda': nome_tavolo, 'ora_comanda': ora_battitura, 'data_comanda': data_battitura, 'stato_record': tipo_cancellazione, 'nome_pc': obj.nome_pc, 'data_cancellazione': data_cancellazione, 'ora_cancellazione': ora_cancellazione};
                                                i++;
//-----DICHIARAZIONE OGGETTI ARTICOLO-----

                                                /*if if (articolo[obj.perc_iva.trim()] === undefined) {
                                                 articolo[obj.perc_iva.trim()] = new Object();
                                                 }
                                                 
                                                 //SE DISABILITA RAGGRUPPAMENTO PORTATA
                                                 //PORTATA = DEFAULT
                                                 //SE ARTICOLO[IVA][PORTATA] UNDEFINED
                                                 (articolo[obj.perc_iva.trim()][obj.portata] === undefined) {
                                                 articolo[obj.perc_iva.trim()][obj.portata] = new Object();
                                                 }
                                                 
                                                 //SE ARTICOLO[IVA][PORTATA][DEST_STAMPA] UNDEFINED
                                                 if (articolo[obj.perc_iva.trim()][obj.portata][obj.dest_stampa] === undefined) {
                                                 articolo[obj.perc_iva.trim()][obj.portata][obj.dest_stampa] = new Object();
                                                 }
                                                 
                                                 ////DISABILITA RAGGRUPPAMENTO CATEGORIA
                                                 //CATEGORIA = DEFAULT
                                                 //SE ARTICOLO[IVA][PORTATA][DEST_STAMPA][CATEGORIA] UNDEFINED
                                                 if (articolo[obj.perc_iva.trim()][obj.portata][obj.dest_stampa][obj.categoria] === undefined) {
                                                 articolo[obj.perc_iva.trim()][obj.portata][obj.dest_stampa][obj.categoria] = new Object();
                                                 }
                                                 
                                                 //RAGGRUPPAMENTO SCONTO PERCENTUALE
                                                 if (articolo[obj.perc_iva.trim()][obj.portata][obj.dest_stampa][obj.categoria][obj.sconto_perc] === undefined) {
                                                 articolo[obj.perc_iva.trim()][obj.portata][obj.dest_stampa][obj.categoria][obj.sconto_perc] = new Object();
                                                 }
                                                 
                                                 //SE ARTICOLO[IVA][PORTATA][DEST_STAMPA][CATEGORIA][DESCRIZIONE] UNDEFINED
                                                 if (articolo[obj.perc_iva.trim()][obj.portata][obj.dest_stampa][obj.categoria][obj.sconto_perc][obj.desc_art] === undefined) {
                                                 articolo[obj.perc_iva.trim()][obj.portata][obj.dest_stampa][obj.categoria][obj.sconto_perc][obj.desc_art] = new Object();
                                                 articolo[obj.perc_iva.trim()][obj.portata][obj.dest_stampa][obj.categoria][obj.sconto_perc][obj.desc_art].quantita = obj.quantita;
                                                 articolo[obj.perc_iva.trim()][obj.portata][obj.dest_stampa][obj.categoria][obj.sconto_perc][obj.desc_art].prezzo_tot = (obj.prezzo_un * obj.quantita) / 100 * (100 - obj.sconto_perc);
                                                 } else
                                                 {
                                                 articolo[obj.perc_iva.trim()][obj.portata][obj.dest_stampa][obj.categoria][obj.sconto_perc][obj.desc_art].quantita += obj.quantita;
                                                 articolo[obj.perc_iva.trim()][obj.portata][obj.dest_stampa][obj.categoria][obj.sconto_perc][obj.desc_art].prezzo_tot += (obj.prezzo_un * obj.quantita) / 100 * (100 - obj.sconto_perc);
                                                 }
                                                 
                                                 
                                                 //SE ARTICOLO[IVA][PORTATA][DEST_STAMPA][CATEGORIA][DESCRIZIONE][QT] UNDEFINED
                                                 if (articolo[obj.perc_iva.trim()][obj.portata][obj.dest_stampa][obj.categoria][obj.sconto_perc][obj.desc_art][obj.quantita] === undefined) {
                                                 articolo[obj.perc_iva.trim()][obj.portata][obj.dest_stampa][obj.categoria][obj.sconto_perc][obj.desc_art][obj.quantita] = new Object();
                                                 }
                                                 
                                                 //SE ARTICOLO[IVA][PORTATA][DEST_STAMPA][CATEGORIA][DESCRIZIONE][QT][PREZZO] UNDEFINED
                                                 if (articolo[obj.perc_iva.trim()][obj.portata][obj.dest_stampa][obj.categoria][obj.sconto_perc][obj.desc_art][obj.quantita][obj.prezzo_un] === undefined) {
                                                 
                                                 //THIS.CARATTERISTICHE COME DA DB
                                                 articolo[obj.perc_iva.trim()][obj.portata][obj.dest_stampa][obj.categoria][obj.sconto_perc][obj.desc_art][obj.quantita][obj.prezzo_un] = obj;
                                                 } else
                                                 {
                                                 articolo[obj.perc_iva.trim()][obj.portata][obj.dest_stampa][obj.categoria][obj.sconto_perc][obj.desc_art][obj.quantita][obj.prezzo_un].desc_art += ' / ' + obj.desc_art;
                                                 articolo[obj.perc_iva.trim()][obj.portata][obj.dest_stampa][obj.categoria][obj.sconto_perc][obj.desc_art][obj.quantita][obj.prezzo_un].quantita += obj.quantita;
                                                 }*/






//----FINE OGGETTO-----



//------FINE DICHIARAZIONE OPERAZIONI_OPERATORE----------
//***

//TOTALE += QUANTITA X PREZZO_UN
                                                prezzo_articolo = parseInt(obj.quantita) * parseFloat(obj.prezzo_un);



                                                totale += prezzo_articolo;
//TOTALE SCONTI
                                                sconto_articolo = (parseInt(obj.quantita) * parseFloat(obj.prezzo_un)) / 100 * parseFloat(obj.sconto_perc);


                                                //IF SCONTO_PERC>0
                                                if (parseFloat(obj.sconto_perc) > 0) {
                                                    //TOT_SCONTI += TOT/100*SCONTO_PERC
                                                    totale_sconti += sconto_articolo;
                                                } else {
                                                    totale_sconti += 0;
                                                }

                                                //NON PAGATO PER ORA VUOTO (C'E' IL CAMPO INCASSO COMUNQUE)
                                                non_pagato += 0;
//INCASSO TOTALE
//INCASSO : TOTALE-TOT_SCONTI (IN REALTA SAREBBE INCASSATI-SCONTI INCASSATI)
//temporaneamente disabilitato lo fa alla fine





                                                incasso_singolo = prezzo_articolo - sconto_articolo;
                                                incasso += parseFloat(incasso_singolo);
                                                //console.log("INCASSI - CALCOLO PER ARTICOLO", 'DES', obj.desc_art, 'PRU', obj.prezzo_un, 'QTA', obj.quantita, 'SCP', obj.sconto_perc, 'CP', prezzo_articolo, 'CS', sconto_articolo, 'AQS', incasso_singolo, 'INCT', incasso);
//-----DICHIARAZIONE OGGETTO OPERAZIONI_OPERATORE (PER I TOTALI SOLAMENTE)-----

//IF OPERAZIONI_OPERATORE[OPERATORE] UNDEFINED

//INCASSO PER OPERATORE
//OPERAZIONI_OPERATORE[OPERATORE].INCASSO+=INCASSO;
                                                if (operazioni_operatore[obj.operatore] === undefined) {
                                                    operazioni_operatore[obj.operatore] = new Object();
                                                    operazioni_operatore[obj.operatore].incasso = 0;
                                                }
//CREA
                                                operazioni_operatore[obj.operatore].incasso += incasso_singolo;
                                                operazioni_operatore[obj.operatore].operatore = obj.operatore;
//SE IVA[IVA] UNDEFINED
                                                if (iva[obj.perc_iva] === undefined) {
                                                    iva[obj.perc_iva] = new Object();
                                                    //CALCOLO FISCALE
                                                    iva[obj.perc_iva].percentuale = 0;
                                                    iva[obj.perc_iva].imposta = 0;
                                                    iva[obj.perc_iva].netto = 0;
                                                    iva[obj.perc_iva].imponibile = 0;
                                                }

                                                //FISCO
                                                if (obj.fiscalizzata_sn === 'S')
                                                {
                                                    switch (obj.tipo_ricevuta) {

                                                        case "scontrino":
                                                        case "scontrino fiscale":
                                                        case "SCONTRINO":
                                                            fisco['SCONTRINI'].incasso += incasso_singolo;
                                                            if (parseInt(obj.n_fiscale) < parseInt(fisco['SCONTRINI'].numero_min) || fisco['SCONTRINI'].numero_min === null)
                                                                fisco['SCONTRINI'].numero_min = obj.n_fiscale;
                                                            if (parseInt(obj.n_fiscale) > parseInt(fisco['SCONTRINI'].numero_min))
                                                                fisco['SCONTRINI'].numero_max = obj.n_fiscale;
                                                            break;
                                                        case "fattura":
                                                        case "FATTURA":
                                                            fisco['FATTURE'].incasso += incasso_singolo;
                                                            if (parseInt(obj.n_fiscale) < parseInt(fisco['FATTURE'].numero_min) || fisco['FATTURE'].numero_min === null)
                                                                fisco['FATTURE'].numero_min = obj.n_fiscale;
                                                            if (parseInt(obj.n_fiscale) > parseInt(fisco['FATTURE'].numero_min))
                                                                fisco['FATTURE'].numero_max = obj.n_fiscale;
                                                            break;
                                                    }
                                                }

                                                iva[obj.perc_iva].percentuale = obj.perc_iva;
                                                iva[obj.perc_iva].imposta += incasso_singolo / parseFloat('1.' + obj.perc_iva);
                                                iva[obj.perc_iva].netto += incasso_singolo - (incasso_singolo / parseFloat('1.' + obj.perc_iva));
                                                iva[obj.perc_iva].imponibile += incasso_singolo;
                                                //console.log("STAMPA TOTALI CALCOLO IVA", incasso_singolo, parseFloat('1.' + obj.perc_iva));
                                                //STATISTICO
                                                //SWITCH
                                                switch (obj.ntav_comanda) {
                                                    // TAVOLO BAR
                                                    case 'BAR':
                                                    case '*BAR*':
                                                        statistico['BAR'].incasso += incasso_singolo;
                                                        //STATISTICO['BAR'].INCASSO+=INCASSO
                                                        break;
                                                        //TAKEAWAY ASPORTO
                                                    case 'ASPORTO':
                                                    case '*ASPORTO*':
                                                    case 'TAKE AWAY':
                                                    case 'TAKEAWAY':
                                                    case '*TAKE AWAY*':
                                                    case '*TAKEAWAY*':
                                                        statistico['TAKEAWAY'].incasso += incasso_singolo;
                                                        //STATISTICO['TAKEAWAY'].INCASSO+=INCASSO
                                                        break;
                                                        //DEFAULT
                                                    default:
                                                        statistico['RISTORAZIONE'].incasso += incasso_singolo;
//STATISTICO['RISTORAZIONE'].INCASSO+=INCASSO
                                                }
                                            }

                                            var tempo4 = new Date().getTime();
                                            console.log("FINE PARTE 4 - PARSING RECORD COMANDA", (tempo4 - tempo3) / 1000);

                                            var y = new Date();
                                            y.setDate(y.getDate());
                                            var yY = y.getFullYear();
                                            var yM = addZero((y.getMonth() + 1), 2);
                                            var yD = addZero(y.getDate(), 2);
                                            var yh = addZero(y.getHours(), 2);
                                            var ym = addZero(y.getMinutes(), 2);
                                            var ys = addZero(y.getSeconds(), 2);
                                            var yms = addZero(y.getMilliseconds(), 3);
                                            var data = yD + '/' + yM + '/' + yY;

                                            if (layout === 'soloincassi' || layout === 'misto') {

                                                var totale_ive = 0;
                                                for (var key in iva) {
                                                    totale_ive += parseFloat(iva[key].imponibile);
                                                }

                                                var tempo5 = new Date().getTime();
                                                console.log("FINE PARTE 5 - PARSING IVA", (tempo5 - tempo4) / 1000);



                                            }

                                            if (layout === 'dettagliovenduti' || layout === 'misto') {

                                                var totale_generico = 0;


                                                var totale_quantita_generica = 0;
                                                var totale_dest_stampa = 0;
                                                var articolo_generico_2;
                                                var decrescente;
                                                switch (settaggi_profili.Field96)
                                                {
                                                    case "C":
                                                        decrescente = false;
                                                        break;
                                                    case "D":
                                                    default:
                                                        decrescente = true;
                                                }

                                                //default descrizione
                                                var ordinamento = "descrizione";

                                                switch (settaggi_profili.Field95) {



                                                    case "Q":
                                                        articolo_generico_2 = riordina(articolo_generico, 'quantita_tot', 'date', false, false);
                                                        break;
                                                    case "D":
                                                        articolo_generico_2 = riordina(articolo_generico, 'descrizione', 'date', false, false);
                                                        break;
                                                    case "T":
                                                    default:
                                                        articolo_generico_2 = riordina(articolo_generico, 'prezzo', 'date', false, false);
                                                }

                                                var tempo6 = new Date().getTime();
                                                console.log("FINE PARTE 6 - RIORDINAMENTO ARTICOLI", (tempo6 - tempo5) / 1000);



                                                for (var num in articolo_generico_2) {

                                                    var obj_art = articolo_generico_2[num];
                                                    totale_quantita_generica += parseInt(obj_art.quantita_tot);
                                                    totale_generico += parseFloat(obj_art.prezzo);
                                                    totale_dest_stampa += parseFloat(obj_art.prezzo);
                                                }

                                                var tempo7 = new Date().getTime();
                                                console.log("FINE PARTE 7 - RIORDINAMENTO ARTICOLI", (tempo7 - tempo6) / 1000);

                                                if (settaggi_profili.Field99 === 'true') {

                                                    var totale_generico = 0;

                                                    var totale_quantita_generica = 0;
                                                    var totale_dest_stampa = 0;
                                                    var articolo_generico_2;
                                                    var decrescente;
                                                    switch (settaggi_profili.Field96)
                                                    {
                                                        case "C":
                                                            decrescente = false;
                                                            break;
                                                        case "D":
                                                        default:
                                                            decrescente = true;
                                                    }
//default descrizione
                                                    var ordinamento = "descrizione";

                                                    switch (settaggi_profili.Field95) {

                                                        case "Q":
                                                            articolo_generico_2 = riordina(articolo_generico, 'quantita_tot', 'date', false, false);
                                                            break;
                                                        case "D":
                                                            articolo_generico_2 = riordina(articolo_generico, 'descrizione', 'date', false, false);
                                                            break;
                                                        case "T":
                                                        default:
                                                            articolo_generico_2 = riordina(articolo_generico, 'prezzo', 'date', false, false);
                                                    }

                                                    var tempo8 = new Date().getTime();
                                                    console.log("FINE PARTE 8 - RIORDINAMENTO ARTICOLI", (tempo8 - tempo7) / 1000);

                                                    for (var num in articolo_generico_2) {

                                                        var obj_art = articolo_generico_2[num];

                                                        totale_quantita_generica += parseInt(obj_art.quantita_tot);

                                                        totale_generico += parseFloat(obj_art.prezzo);

                                                        totale_dest_stampa += parseFloat(obj_art.prezzo);
                                                    }
                                                }

                                                if (settaggi_profili.Field101 === 'true') {

                                                    var nome_colonna = 'destinazione'; //destinazione o categoria

                                                    var array_codici_univoci = estrazione_univoca_colonna(articolo_generico, nome_colonna);
                                                    var array_percentuali = new Array();
                                                    var contatore_categorie = 0;
                                                    var contatore_stampanti = 1;
                                                    for (var codice_univoco in array_codici_univoci) {

                                                        var totale_quantita_reparto = 0;
                                                        var totale_dest_stampa = 0;

                                                        var nome_stampante;
                                                        if (comanda.nome_stampante[array_codici_univoci[codice_univoco]] !== undefined) {
                                                            nome_stampante = comanda.nome_stampante[array_codici_univoci[codice_univoco]].nome_umano;
                                                        } else
                                                        {
                                                            nome_stampante = 'STAMPANTE CANCELLATA ' + contatore_stampanti;
                                                            contatore_stampanti++;
                                                        }

                                                        var totale_prezzo_categoria = 0;
                                                        var totale_quantita_categoria = 0;

                                                        if (contatore_categorie === 0) {

                                                            contatore_categorie = 1;
                                                        }

                                                        var decrescente;
                                                        switch (settaggi_profili.Field96)
                                                        {
                                                            case "C":
                                                                decrescente = false;
                                                                break;
                                                            case "D":
                                                            default:
                                                                decrescente = true;
                                                        }
                                                        var array_suddiviso;

                                                        //default descrizione
                                                        var ordinamento = "descrizione";

                                                        switch (settaggi_profili.Field95) {

                                                            case "Q":
                                                                array_suddiviso = estrazione_array(riordina(articolo_generico, 'quantita_tot', 'date', false, false), nome_colonna, array_codici_univoci[codice_univoco]);
                                                                break;
                                                            case "D":
                                                                array_suddiviso = estrazione_array(riordina(articolo_generico, 'descrizione', 'date', false, false), nome_colonna, array_codici_univoci[codice_univoco]);
                                                                break;
                                                            case "T":
                                                            default:
                                                                array_suddiviso = estrazione_array(riordina(articolo_generico, 'prezzo', 'date', false, false), nome_colonna, array_codici_univoci[codice_univoco]);
                                                        }


                                                        for (num in array_suddiviso) {

                                                            var obj_art = array_suddiviso[num];

                                                            totale_prezzo_categoria += parseFloat(obj_art.prezzo);
                                                            totale_quantita_categoria += parseInt(obj_art.quantita_tot);
                                                            totale_quantita_reparto += parseInt(obj_art.quantita_tot);

                                                            totale_dest_stampa += parseFloat(obj_art.prezzo);
                                                        }

                                                        array_percentuali.push({nome_categoria: nome_stampante, quantita_totale: totale_quantita_categoria, prezzo_totale: totale_prezzo_categoria, percentuale: (parseFloat(totale_dest_stampa) / parseFloat(totale_generico) * 100).toFixed(2)});
                                                    }
                                                }

                                                if (settaggi_profili.Field100 === 'true') {

                                                    var nome_colonna = 'categoria'; //destinazione o categoria

                                                    var array_codici_univoci = estrazione_univoca_colonna(articolo_generico, nome_colonna);
                                                    var array_percentuali = new Array();
                                                    var contatore_categorie = 0;
                                                    var contatore_stampanti = 1;
                                                    var popup_venduti_categorie = '<div id="tabella_elenco_venduti_001_cancellazioni">';
                                                    var quantita_totale_generale = 0;

                                                    var somma_media_qta = 0;
                                                    var somma_media_prezzo = 0;

                                                    for (var codice_univoco in array_codici_univoci) {

                                                        var totale_quantita_reparto = 0;
                                                        var totale_dest_stampa = 0;

                                                        var totale_prezzo_categoria = 0;
                                                        var totale_quantita_categoria = 0;

                                                        popup_venduti_categorie += '<h4>' + nomi_categorie[array_codici_univoci[codice_univoco]] + '</h4>';





                                                        popup_venduti_categorie += '<table class="tabella_categoria_backoffice_cancellazioni table table-responsive table-striped table-hover text-left">';

                                                        //if (contatore_categorie === 0) {

                                                        popup_venduti_categorie += '<tr style="text-align:center;font-weight:bold;"><td style="width:12%;">ORA CANC.</td><td style="width:7%;">DATA CANC.</td><td style="width:10%;">TAVOLO</td><td style="width:25%;">ARTICOLO</td><td style="width:4%;">QTA</td><td style="width:12%;">ORA BATT.</td><td style="width:7%;">DATA BATT.</td><td style="width:7%;">STATO</td><td style="width:10%;">NOME PC</td><td style="width:6%;">EURO</td></tr>';
                                                        //contatore_categorie = 1;
                                                        //}

                                                        var decrescente;
                                                        switch (settaggi_profili.Field96)
                                                        {
                                                            case "C":
                                                                decrescente = false;
                                                                break;
                                                            case "D":
                                                            default:
                                                                decrescente = true;
                                                        }
                                                        var array_suddiviso;

                                                        //default descrizione
                                                        var ordinamento = "descrizione";

                                                        switch (settaggi_profili.Field95) {

                                                            case "Q":
                                                                array_suddiviso = estrazione_array(riordina(articolo_generico, 'quantita_tot', 'date', false, false), nome_colonna, array_codici_univoci[codice_univoco]);
                                                                break;
                                                            case "D":
                                                                array_suddiviso = estrazione_array(riordina(articolo_generico, 'descrizione', 'date', false, false), nome_colonna, array_codici_univoci[codice_univoco]);
                                                                break;
                                                            case "T":
                                                            default:
                                                                array_suddiviso = estrazione_array(riordina(articolo_generico, 'prezzo', 'date', false, false), nome_colonna, array_codici_univoci[codice_univoco]);



                                                        }


                                                        var quantita_totale_categoria = 0;
                                                        var media_qta_totale = 0;
                                                        var media_prezzo_totale = 0;
                                                        for (num in array_suddiviso) {

                                                            var obj_art = array_suddiviso[num];


                                                            totale_prezzo_categoria += parseFloat(obj_art.prezzo);
                                                            totale_quantita_categoria += parseInt(obj_art.quantita_tot);
                                                            totale_quantita_reparto += parseInt(obj_art.quantita_tot);

                                                            var media_qta = parseFloat(parseFloat(obj_art.quantita_tot) / numero_giorni_media);
                                                            var media_prezzo = parseFloat(parseFloat(obj_art.prezzo) / numero_giorni_media);

                                                            media_qta_totale += media_qta;
                                                            media_prezzo_totale += media_prezzo;

                                                            somma_media_qta += media_qta;
                                                            somma_media_prezzo += media_prezzo;

                                                            popup_venduti_categorie += '<tr><td>' + obj_art.ora_cancellazione + '</td><td>' + obj_art.data_cancellazione + '</td><td style="text-align:center;">' + obj_art.ntav_comanda + '</td><td>' + obj_art.descrizione + '</td><td style="text-align:center;">' + obj_art.quantita_tot + '</td><td>' + obj_art.ora_comanda + '</td><td>' + obj_art.data_comanda + '</td><td style="text-align:center;">' + obj_art.stato_record + '</td><td>' + obj_art.nome_pc.substr(0, 10) + '</td><td style="text-align:right;">' + parseFloat(obj_art.prezzo).toFixed(2) + '</td></tr>';
                                                            /* popup_venduti_categorie += '<tr><td>' + obj_art.descrizione + '</td><td>' + obj_art.perc_iva + '</td><td>' + obj_art.quantita_tot + '</td><td>' + media_qta.toFixed(2) + '</td><td>&euro; ' + media_prezzo.toFixed(2) + '</td><td>' + '&euro; ' + parseFloat(obj_art.prezzo).toFixed(2) + '</td></tr>';*/

                                                            quantita_totale_categoria += parseInt(obj_art.quantita_tot);
                                                            quantita_totale_generale += parseInt(obj_art.quantita_tot);
                                                            totale_dest_stampa += parseFloat(obj_art.prezzo);
                                                        }

                                                        popup_venduti_categorie += '<tr style="font-weight:bold"><td>TOTALE CATEGORIA</td><td></td><td></td><td></td><td style="text-align:center;">' + quantita_totale_categoria + '</td><td></td><td></td><td></td><td></td><td style="text-align:right;">' + parseFloat(totale_prezzo_categoria).toFixed(2) + '</td></tr>';
                                                        popup_venduti_categorie += '<tr style="font-weight:bold"><td>MEDIA CATEGORIA ' + numero_giorni_media + ' GIORNI</td><td></td><td></td><td></td><td style="text-align:center;">' + media_qta_totale.toFixed(2) + '</td><td></td><td></td><td></td><td></td><!--<td>&euro; ' + media_prezzo_totale.toFixed(2) + '</td>--><td style="text-align:right;">' + parseFloat(parseFloat(totale_prezzo_categoria) / numero_giorni_media).toFixed(2) + '</td></tr>';

                                                        array_percentuali.push({nome_categoria: nomi_categorie[array_codici_univoci[codice_univoco]], quantita_totale: totale_quantita_categoria, prezzo_totale: totale_prezzo_categoria, percentuale: (parseFloat(totale_prezzo_categoria) / parseFloat(totale_generico) * 100).toFixed(2)});


                                                        popup_venduti_categorie += '</table>';
                                                    }


                                                    popup_venduti_categorie += '<table class="tabella_totale_backoffice_cancellazioni table table-responsive table-striped table-hover text-left">';

                                                    var totale_generico_piu_consegna = 0;
                                                    var prezzo_totale_consegne = 0;


                                                    if (comanda.pizzeria_asporto === true) {
                                                        popup_venduti_categorie += '<tr><td  style="text-align:left;width:40%;font-weight:bold;">TOTALE CONSEGNE</td><td style="width:5%;"></td><td style="width:15%;text-align:right;font-weight:bold;">' + totale_qta_consegne + '</td><td style="width:15%;text-align:right;">' + (parseInt(totale_qta_consegne) / numero_giorni_media).toFixed(2) + '</td><td style="width:15%;text-align:right;">&euro; ' + (parseFloat(prezzo_totale_consegne) / numero_giorni_media).toFixed(2) + '</td><td style="width:20%;text-align:right;font-weight:bold;">&euro; ' + parseFloat(prezzo_totale_consegne).toFixed(2) + '</td></tr>';
                                                        popup_venduti_categorie += '<tr><td  style="text-align:left;width:40%;font-weight:bold;">TOTALE RITIRI</td><td style="width:5%;"></td><td style="width:15%;text-align:right;font-weight:bold;">' + totale_qta_ritiri + '</td><td style="width:15%;text-align:right;">' + (parseInt(totale_qta_ritiri) / numero_giorni_media).toFixed(2) + '</td><td style="width:15%;text-align:right;">0</td><td style="width:20%;text-align:right;font-weight:bold;">&euro; ' + parseFloat(prezzo_totale_consegne).toFixed(2) + '</td></tr>';
                                                        popup_venduti_categorie += '<tr><td  style="text-align:left;width:40%;font-weight:bold;">TOTALE CONSUMAZIONI SUL POSTO</td><td style="width:5%;"></td><td style="width:15%;text-align:right;font-weight:bold;">' + totale_qta_mangiarequi + '</td><td style="width:15%;text-align:right;">' + (parseInt(totale_qta_mangiarequi) / numero_giorni_media).toFixed(2) + '</td><td style="width:15%;text-align:right;">0</td><td style="width:20%;text-align:right;font-weight:bold;">&euro; ' + parseFloat(prezzo_totale_consegne).toFixed(2) + '</td></tr>';
                                                    }

                                                    /*<td style="width:12%;">ORA CANC.</td><td style="width:7%;">DATA CANC.</td><td style="width:10%;">TAVOLO</td><td style="width:25%;">ARTICOLO</td><td style="width:4%;">QTA</td><td style="width:12%;">ORA BATT.</td><td style="width:7%;">DATA BATT.</td><td style="width:7%;">STATO</td><td style="width:10%;">NOME PC</td><td style="width:6%;">EURO</td>*/

                                                    popup_venduti_categorie += '<tr style="font-weight:bold"><td style="width:12%;">TOTALE GENERALE</td><td style="width:7%;"></td><td style="width:10%;"></td><td style="width:25%;"></td><td style="text-align:center;width:4%;">' + quantita_totale_generale + '</td><td style="width:12%;"></td><td style="width:7%;"></td><td style="width:7%;"></td><td style="width:10%;"></td><td style="width:6%;text-align:right;">' + parseFloat(totale_generico).toFixed(2) + '</td></tr>';
                                                    popup_venduti_categorie += '<tr style="font-weight:bold"><td>MEDIA GENERALE ' + numero_giorni_media + ' GIORNI</td><td></td><td></td><td></td><td style="text-align:center;">' + somma_media_qta.toFixed(2) + '</td><td></td><td></td><td></td><td></td><!--<td>&euro; ' + somma_media_prezzo.toFixed(2) + '</td>--><td style="text-align:right;">' + parseFloat(parseFloat(totale_generico) / numero_giorni_media).toFixed(2) + '</td></tr>';


                                                    popup_venduti_categorie += '</table>';



                                                    popup_venduti_categorie += '</div>';

                                                    $('#popup_venduti_categorie_cancellazioni div.modal-body').html(popup_venduti_categorie);



                                                }

                                                if (settaggi_profili.Field412 === 'true') {

                                                    var nome_colonna = 'gruppo'; //destinazione o categoria

                                                    var array_codici_univoci = estrazione_univoca_colonna(articolo_generico, nome_colonna);
                                                    var array_percentuali = new Array();
                                                    var contatore_categorie = 0;
                                                    var contatore_stampanti = 1;
                                                    var popup_venduti_categorie = '<div id="tabella_elenco_venduti_001_cancellazioni">';
                                                    var quantita_totale_generale = 0;

                                                    var somma_media_qta = 0;
                                                    var somma_media_prezzo = 0;

                                                    for (var codice_univoco in array_codici_univoci) {

                                                        var totale_quantita_reparto = 0;
                                                        var totale_dest_stampa = 0;

                                                        var totale_prezzo_categoria = 0;
                                                        var totale_quantita_categoria = 0;

                                                        popup_venduti_categorie += '<h4>' + nomi_gruppi[array_codici_univoci[codice_univoco]] + '</h4>';

                                                        popup_venduti_categorie += '<table class="table table-responsive table-striped table-hover text-left">';

                                                        //if (contatore_categorie === 0) {

                                                        popup_venduti_categorie += '<tr><td style="text-align:center;width:40%;font-weight:bold;">DESCRIZIONE</td><td style="font-weight:bold;text-align:center;width:5%;">IVA</td><td style="font-weight:bold;text-align:center;width:10%;">QTA</td><td  style="text-align:center;width:15%;font-weight:bold;">MEDIA/QTA</td><td  style="text-align:center;width:15%;font-weight:bold;">MEDIA/&euro;</td><td  style="text-align:center;width:20%;font-weight:bold;">TOTALE</td></tr>';
                                                        //contatore_categorie = 1;
                                                        //}

                                                        var decrescente;
                                                        switch (settaggi_profili.Field96)
                                                        {
                                                            case "C":
                                                                decrescente = false;
                                                                break;
                                                            case "D":
                                                            default:
                                                                decrescente = true;
                                                        }
                                                        var array_suddiviso;

                                                        //default descrizione
                                                        var ordinamento = "descrizione";

                                                        switch (settaggi_profili.Field95) {

                                                            case "Q":
                                                                array_suddiviso = estrazione_array(riordina(articolo_generico, 'quantita_tot', 'date', false, false), nome_colonna, array_codici_univoci[codice_univoco]);
                                                                break;
                                                            case "D":
                                                                array_suddiviso = estrazione_array(riordina(articolo_generico, 'descrizione', 'date', false, false), nome_colonna, array_codici_univoci[codice_univoco]);
                                                                break;
                                                            case "T":
                                                            default:
                                                                array_suddiviso = estrazione_array(riordina(articolo_generico, 'prezzo', 'date', false, false), nome_colonna, array_codici_univoci[codice_univoco]);



                                                        }


                                                        var quantita_totale_categoria = 0;
                                                        var media_qta_totale = 0;
                                                        var media_prezzo_totale = 0;
                                                        for (num in array_suddiviso) {

                                                            var obj_art = array_suddiviso[num];


                                                            totale_prezzo_categoria += parseFloat(obj_art.prezzo);
                                                            totale_quantita_categoria += parseInt(obj_art.quantita_tot);
                                                            totale_quantita_reparto += parseInt(obj_art.quantita_tot);

                                                            var media_qta = parseFloat(parseFloat(obj_art.quantita_tot) / numero_giorni_media);
                                                            var media_prezzo = parseFloat(parseFloat(obj_art.prezzo) / numero_giorni_media);

                                                            media_qta_totale += media_qta;
                                                            media_prezzo_totale += media_prezzo;

                                                            somma_media_qta += media_qta;
                                                            somma_media_prezzo += media_prezzo;

                                                            popup_venduti_categorie += '<tr><td style="text-align:left;width:40%;">' + obj_art.descrizione + '</td><td style="text-align:right;width:5%;">' + obj_art.perc_iva + '</td><td style="text-align:right;width:10%;">' + obj_art.quantita_tot + '</td><td style="width:15%;text-align:right;">' + media_qta.toFixed(2) + '</td><td style="width:15%;text-align:right;">&euro; ' + media_prezzo.toFixed(2) + '</td><td  style="width:20%;text-align:right;">' + '&euro; ' + parseFloat(obj_art.prezzo).toFixed(2) + '</td></tr>';

                                                            quantita_totale_categoria += parseInt(obj_art.quantita_tot);
                                                            quantita_totale_generale += parseInt(obj_art.quantita_tot);
                                                            totale_dest_stampa += parseFloat(obj_art.prezzo);
                                                        }

                                                        popup_venduti_categorie += '<tr><td  style="text-align:left;width:40%;font-weight:bold;">TOTALE GRUPPO</td><td style="width:5%;"></td><td style="width:10%;text-align:right;font-weight:bold;">' + quantita_totale_categoria + '</td><td style="width:15%;text-align:right;font-weight:bold;"></td><td style="width:15%;text-align:right;font-weight:bold;"></td><td  style="width:20%;text-align:right;font-weight:bold;">&euro; ' + parseFloat(totale_prezzo_categoria).toFixed(2) + '</td></tr>';
                                                        popup_venduti_categorie += '<tr><td  style="text-align:left;width:40%;font-weight:bold;">MEDIA GRUPPO' + numero_giorni_media + ' GIORNI</td><td style="width:5%;"></td><td style="width:15%;"></td><td style="width:10%;text-align:right;font-weight:bold;">' + media_qta_totale.toFixed(2) + '</td><td style="width:15%;text-align:right;font-weight:bold;">&euro; ' + media_prezzo_totale.toFixed(2) + '</td><td style="width:20%;text-align:right;font-weight:bold;">&euro; ' + parseFloat(parseFloat(totale_prezzo_categoria) / numero_giorni_media).toFixed(2) + '</td></tr>';



                                                        array_percentuali.push({nome_categoria: nomi_gruppi[array_codici_univoci[codice_univoco]], quantita_totale: totale_quantita_categoria, prezzo_totale: totale_prezzo_categoria, percentuale: (parseFloat(totale_prezzo_categoria) / parseFloat(totale_generico) * 100).toFixed(2)});


                                                        popup_venduti_categorie += '</table>';
                                                    }


                                                    popup_venduti_categorie += '<table class="table table-responsive table-striped table-hover text-left">';

                                                    var totale_generico_piu_consegna = 0;
                                                    var prezzo_totale_consegne = 0;


                                                    if (comanda.pizzeria_asporto === true) {
                                                        popup_venduti_categorie += '<tr><td  style="text-align:left;width:40%;font-weight:bold;">TOTALE CONSEGNE</td><td style="width:5%;"></td><td style="width:15%;text-align:right;font-weight:bold;">' + totale_qta_consegne + '</td><td style="width:15%;text-align:right;">' + (parseInt(totale_qta_consegne) / numero_giorni_media).toFixed(2) + '</td><td style="width:15%;text-align:right;">&euro; ' + (parseFloat(prezzo_totale_consegne) / numero_giorni_media).toFixed(2) + '</td><td style="width:20%;text-align:right;font-weight:bold;">&euro; ' + parseFloat(prezzo_totale_consegne).toFixed(2) + '</td></tr>';
                                                        popup_venduti_categorie += '<tr><td  style="text-align:left;width:40%;font-weight:bold;">TOTALE RITIRI</td><td style="width:5%;"></td><td style="width:15%;text-align:right;font-weight:bold;">' + totale_qta_ritiri + '</td><td style="width:15%;text-align:right;">' + (parseInt(totale_qta_ritiri) / numero_giorni_media).toFixed(2) + '</td><td style="width:15%;text-align:right;">0</td><td style="width:20%;text-align:right;font-weight:bold;">&euro; ' + parseFloat(prezzo_totale_consegne).toFixed(2) + '</td></tr>';
                                                        popup_venduti_categorie += '<tr><td  style="text-align:left;width:40%;font-weight:bold;">TOTALE CONSUMAZIONI SUL POSTO</td><td style="width:5%;"></td><td style="width:15%;text-align:right;font-weight:bold;">' + totale_qta_mangiarequi + '</td><td style="width:15%;text-align:right;">' + (parseInt(totale_qta_mangiarequi) / numero_giorni_media).toFixed(2) + '</td><td style="width:15%;text-align:right;">0</td><td style="width:20%;text-align:right;font-weight:bold;">&euro; ' + parseFloat(prezzo_totale_consegne).toFixed(2) + '</td></tr>';
                                                    }
                                                    popup_venduti_categorie += '<tr><td  style="text-align:left;width:40%;font-weight:bold;">TOTALE GENERALE</td><td style="width:5%;"></td><td style="width:15%;text-align:right;font-weight:bold;">' + quantita_totale_generale + '</td><td style="width:15%;"></td><td style="width:15%;"></td><td style="width:20%;text-align:right;font-weight:bold;">&euro; ' + parseFloat(totale_generico).toFixed(2) + '</td></tr>';
                                                    popup_venduti_categorie += '<tr><td  style="text-align:left;width:40%;font-weight:bold;">MEDIA GENERALE ' + numero_giorni_media + ' GIORNI</td><td style="width:5%;"></td><td style="width:15%;"></td><td style="width:15%;text-align:right;font-weight:bold;">' + somma_media_qta.toFixed(2) + '</td><td style="width:15%;text-align:right;font-weight:bold;">&euro; ' + somma_media_prezzo.toFixed(2) + '</td><td style="width:20%;text-align:right;font-weight:bold;">€ ' + parseFloat(parseFloat(totale_generico) / numero_giorni_media).toFixed(2) + '</td></tr>';


                                                    popup_venduti_categorie += '</table>';

                                                    popup_venduti_categorie += '</div>';

                                                    $('#popup_venduti_categorie_cancellazioni div.modal-body').html(popup_venduti_categorie);

                                                }

                                                if (settaggi_profili.Field102 === 'true') {


                                                    riordina(array_percentuali, 'percentuale', 'float', true, false).forEach(function (element, index) {



                                                        labels_categorie.push('<td style="padding:2px;"><a style="text-decoration:none;text-align:right;color:white;" ' + comanda.evento + '="stampa_totali_giornata(\'dettagliovenduti\',\'' + element.nome_categoria + '\');">' + element.percentuale + '%</a></td><td style="padding:2px;text-align:right;"><a style="text-decoration:none;color:white;" ' + comanda.evento + '="stampa_totali_giornata(\'dettagliovenduti\',\'' + element.nome_categoria + '\');"> € ' + parseFloat(element.prezzo_totale).toFixed(2) + ' </a></td><td style="padding:2px;text-align:center;"><a style="color:white;text-decoration:none;" ' + comanda.evento + '="stampa_totali_giornata(\'dettagliovenduti\',\'' + element.nome_categoria + '\');"> ' + element.nome_categoria + '</a></td>');

                                                        dati_categorie.push(parseFloat(element.prezzo_totale).toFixed(2));
                                                        backgroundColor_categorie.push("rgb(" + (Math.floor(Math.random() * 255) + 1) + ", " + (Math.floor(Math.random() * 255) + 1) + ", " + (Math.floor(Math.random() * 255) + 1) + ")");

                                                    });

                                                    var config = {
                                                        type: 'pie',
                                                        data: {
                                                            datasets: [{
                                                                    data: dati_categorie,
                                                                    backgroundColor: backgroundColor_categorie,
                                                                    borderColor: 'black'
                                                                }],
                                                            labels: labels_categorie
                                                        },
                                                        options: {
                                                            responsive: true,
                                                            maintainAspectRatio: false,
                                                            legend: {
                                                                display: false
                                                            },
                                                            title: {
                                                                display: false,
                                                                fontSize: 34,
                                                                fontColor: 'white'
                                                            },
                                                            animation: {
                                                                animateScale: true,
                                                                animateRotate: true
                                                            },
                                                            tooltips: {
                                                                enabled: true,
                                                                callbacks: {
                                                                    label: function (tooltipItems, data) {
                                                                        console.log(tooltipItems, data.labels[tooltipItems.index]);
                                                                        return data.labels[tooltipItems.index].replace(/\<(.*?)\>/ig, "");
                                                                    }
                                                                }
                                                            }

                                                        }
                                                    };
                                                    var ctx = document.getElementById("chart-area_cancellazioni").getContext("2d");
                                                    if (myChart_1_cancellazioni === undefined) {
                                                        myChart_1_cancellazioni = new Chart(ctx, config);
                                                    } else
                                                    {
                                                        myChart_1_cancellazioni.destroy();
                                                        myChart_1_cancellazioni = new Chart(ctx, config);
                                                    }
                                                    //myChart_1_cancellazioni.update();
                                                    document.getElementById('js-legend_cancellazioni').innerHTML = myChart_1_cancellazioni.generateLegend();

                                                    //GIORNI
                                                    var dataset = new Array();




                                                    var scale = new Array();
                                                    var numero_oggetto = 0;

                                                    var media_giornaliera = new Object();

                                                    for (var numero_giorno in statistiche_giorno) {



                                                        var giorno = statistiche_giorno[numero_giorno];

                                                        for (var c_ora = 0; c_ora < 24; c_ora++) {

                                                            if (media_giornaliera[c_ora] === undefined) {
                                                                media_giornaliera[c_ora] = new Object();
                                                                media_giornaliera[c_ora].totale = 0;
                                                                media_giornaliera[c_ora].numero = 0;
                                                                media_giornaliera[c_ora].media = 0.00;
                                                            }
                                                            var ora = giorno[aggZero(c_ora, 2)];
                                                            if (giorno[aggZero(c_ora, 2)] !== undefined) {
                                                                media_giornaliera[c_ora].totale += ora.totale;
                                                            } else
                                                            {
                                                                media_giornaliera[c_ora].totale += 0;
                                                            }
                                                            media_giornaliera[c_ora].numero++;
                                                        }
                                                    }

                                                    //contenitore_grafico_2

                                                    //-------------------INIZIO VENDUTI PER GIORNO---------------------//


                                                    RAM.tabella_media = new Array();
                                                    RAM.tabella_totali = new Array();


                                                    //QUI VA CALCOLATA LA MEDIA 
                                                    var dati = new Array();


                                                    for (var index in media_giornaliera) {

                                                        media_giornaliera[index].media = media_giornaliera[index].totale / media_giornaliera[index].numero;

                                                        RAM.tabella_media.push(parseFloat(media_giornaliera[index].media).toFixed(2));

                                                        RAM.tabella_totali.push(parseFloat(media_giornaliera[index].totale).toFixed(2));

                                                        dati.push(parseFloat(media_giornaliera[index].media).toFixed(2));

                                                        dataset.push({
                                                            label: 'Media',
                                                            borderColor: 'black',
                                                            backgroundColor: 'black',
                                                            fill: false,
                                                            data: dati
                                                        });

                                                    }

                                                    RAM.tabella_venduti_giornalieri = new Object();


                                                    for (var numero_giorno in statistiche_giorno) {


                                                        var dati = new Array();

                                                        var giorno = statistiche_giorno[numero_giorno];

                                                        RAM.tabella_venduti_giornalieri[numero_giorno] = new Array();


                                                        for (var c_ora = 0; c_ora < 24; c_ora++) {

                                                            if (giorno[aggZero(c_ora, 2)] !== undefined) {
                                                                var ora = giorno[aggZero(c_ora, 2)];
                                                                RAM.tabella_venduti_giornalieri[numero_giorno].push(parseFloat(ora.totale).toFixed(2));
                                                                dati.push(parseFloat(ora.totale).toFixed(2));
                                                            } else
                                                            {
                                                                RAM.tabella_venduti_giornalieri[numero_giorno].push(0);
                                                                dati.push(0);
                                                            }
                                                        }

                                                        window.chartColors.red = 'red';
                                                        window.chartColors.orange = 'orange';
                                                        window.chartColors.yellow = 'yellow';
                                                        window.chartColors.green = 'green';
                                                        window.chartColors.blue = 'blue';
                                                        window.chartColors.grey = 'aqua';
                                                        window.chartColors.purple = 'purple';

                                                        var label = 'Generico';
                                                        var colore = "white";
                                                        switch (numero_giorno) {
                                                            case '1':
                                                                label = 'Lun';
                                                                colore = window.chartColors.yellow;
                                                                break;
                                                            case '2':
                                                                label = 'MAr';
                                                                colore = window.chartColors.orange;
                                                                break;
                                                            case '3':
                                                                label = 'MEr';
                                                                colore = window.chartColors.red;
                                                                break;
                                                            case '4':
                                                                label = 'Gio';
                                                                colore = window.chartColors.purple;
                                                                break;
                                                            case '5':
                                                                label = 'Ven';
                                                                colore = window.chartColors.blue;
                                                                break;
                                                            case '6':
                                                                label = 'Sab';
                                                                colore = window.chartColors.grey;
                                                                break;
                                                            case '7':
                                                                label = 'Dom';
                                                                colore = window.chartColors.green;
                                                                break;
                                                        }

                                                        dataset.push({
                                                            label: label,
                                                            borderColor: colore,
                                                            backgroundColor: colore,
                                                            fill: false,
                                                            data: dati
                                                        });

                                                        if (numero_oggetto === 0) {
                                                            scale.push({
                                                                type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                                                                display: true,
                                                                position: "left"

                                                            });
                                                        }
                                                        numero_oggetto++;
                                                    }



                                                    int_venduti_giornalieri = "<div style='text-align:center;font-size:3vh;'><strong>Prodotti in &euro; per giorno ed ora<br/><br/></strong></div>";
                                                    int_venduti_giornalieri_stampa = "<div style='text-align:center;font-size:1.5vh;'><strong>Prodotti in &euro; per giorno ed ora<br/><br/></strong></div>";

                                                    lay_venduti_giornalieri = "<table class='table table-responsive table-bordered'>";



                                                    lay_venduti_giornalieri += "<tr  style='padding:2px;'>";

                                                    lay_venduti_giornalieri += "<td style='padding:2px;text-align:center;'><strong>Ore</strong></td>";

                                                    //MEDIA GIORNI RAGGRUPPATI
                                                    //lay_venduti_giornalieri += "<td style='padding:2px;text-align:center;'><strong>Media</strong></td>";

                                                    lay_venduti_giornalieri += "<td style='padding:2px;text-align:center;'><strong>Media/gg</strong></td>";

                                                    for (var key in RAM.tabella_venduti_giornalieri) {

                                                        var nome_giorno = "";

                                                        switch (key) {
                                                            case '1':
                                                                nome_giorno = 'Lun';
                                                                break;
                                                            case '2':
                                                                nome_giorno = 'Mar';
                                                                break;
                                                            case '3':
                                                                nome_giorno = 'Mer';
                                                                break;
                                                            case '4':
                                                                nome_giorno = 'Gio';
                                                                break;
                                                            case '5':
                                                                nome_giorno = 'Ven';
                                                                break;
                                                            case '6':
                                                                nome_giorno = 'Sab';
                                                                break;
                                                            case '7':
                                                                nome_giorno = 'Dom';
                                                                break;
                                                        }

                                                        lay_venduti_giornalieri += "<td style='padding:2px;text-align:center;'><strong>" + nome_giorno + "</strong></td>";
                                                    }

                                                    lay_venduti_giornalieri += "</tr>";


                                                    var totale_giorno = new Object();





                                                    for (var ora = 0; ora < 24; ora++) {
                                                        lay_venduti_giornalieri += "<tr  style='padding:2px;'>";

                                                        lay_venduti_giornalieri += "<td style='padding:2px;text-align:center;'><strong>" + aggZero(parseInt(ora), 2) + "</strong></td>";

                                                        //MEDIA GIORNI RAGGRUPPATI
                                                        //lay_venduti_giornalieri += "<td style='padding:2px;text-align:right;'>" + parseFloat(RAM.tabella_media[ora]).toFixed(2) + "</td>";

                                                        lay_venduti_giornalieri += "<td style='padding:2px;text-align:right;'>" + (parseFloat(RAM.tabella_totali[ora]) / numero_giorni_media).toFixed(2) + "</td>";

                                                        if (totale_giorno['0'] === undefined) {
                                                            totale_giorno['0'] = parseFloat(RAM.tabella_media[ora]);
                                                        } else
                                                        {
                                                            totale_giorno['0'] += parseFloat(RAM.tabella_media[ora]);
                                                        }

                                                        if (totale_giorno['MGG'] === undefined) {
                                                            totale_giorno['MGG'] = parseFloat(RAM.tabella_totali[ora]);
                                                        } else
                                                        {
                                                            totale_giorno['MGG'] += parseFloat(RAM.tabella_totali[ora]);
                                                        }


                                                        for (var key in RAM.tabella_venduti_giornalieri) {

                                                            if (totale_giorno[key] === undefined) {
                                                                totale_giorno[key] = parseFloat(RAM.tabella_venduti_giornalieri[key][ora]);
                                                            } else
                                                            {
                                                                totale_giorno[key] += parseFloat(RAM.tabella_venduti_giornalieri[key][ora]);
                                                            }

                                                            lay_venduti_giornalieri += "<td style='padding:2px;text-align:right;'>" + parseFloat(RAM.tabella_venduti_giornalieri[key][ora]).toFixed(2) + "</td>";

                                                        }

                                                        lay_venduti_giornalieri += "</tr>";
                                                    }

                                                    lay_venduti_giornalieri += "<tr  style='padding:2px;'>";

                                                    lay_venduti_giornalieri += "<td style='padding:2px;text-align:center;'><strong>TOT.</strong></td>";

                                                    //MEDIA GIORNI RAGGRUPPATI
                                                    // lay_venduti_giornalieri += "<td style='padding:2px;text-align:right;'><strong>" + parseFloat(totale_giorno['0']).toFixed(2) + "</strong></td>";
                                                    lay_venduti_giornalieri += "<td style='padding:2px;text-align:right;'><strong>" + (parseFloat(totale_giorno['MGG']) / numero_giorni_media).toFixed(2) + "</strong></td>";

                                                    for (var key in RAM.tabella_venduti_giornalieri) {

                                                        lay_venduti_giornalieri += "<td style='padding:2px;text-align:right;'><strong>" + parseFloat(totale_giorno[key]).toFixed(2) + "</strong></td>";

                                                    }


                                                    lay_venduti_giornalieri += "</tr>";


                                                    lay_venduti_giornalieri += "</table>";

                                                    if ($('#selettore_data_partenza_grafico_cancellazioni').val() !== "") {
                                                        $('#tabella_venduti_giorno_cancellazioni').html(int_venduti_giornalieri + lay_venduti_giornalieri);
                                                    } else
                                                    {
                                                        $('#tabella_venduti_giorno_cancellazioni').html("");
                                                    }


                                                    Chart.pluginService.register({
                                                        beforeRender: function (chart) {
                                                            if (chart.config.options.showAllTooltips) {
                                                                // create an array of tooltips
                                                                // we can't use the chart tooltip because there is only one tooltip per chart
                                                                chart.pluginTooltips = [];
                                                                chart.config.data.datasets.forEach(function (dataset, i) {

                                                                    if (dataset.label === 'Media') {
                                                                        var iteratore_dataset = 1;


                                                                        chart.getDatasetMeta(i).data.forEach(function (sector, j) {
                                                                            if (iteratore_dataset % 2 === 0) {

                                                                                chart.pluginTooltips.push(new Chart.Tooltip({
                                                                                    _chart: chart.chart,
                                                                                    _chartInstance: chart,
                                                                                    _data: chart.data,
                                                                                    _options: chart.options.tooltips,
                                                                                    _active: [sector]
                                                                                }, chart));
                                                                            }
                                                                            iteratore_dataset++;
                                                                        });
                                                                    }
                                                                });

                                                                // turn off normal tooltips
                                                                chart.options.tooltips.enabled = false;
                                                            }
                                                        },
                                                        afterDraw: function (chart, easing) {
                                                            if (chart.config.options.showAllTooltips) {
                                                                // we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
                                                                if (!chart.allTooltipsOnce) {
                                                                    if (easing !== 1)
                                                                        return;
                                                                    chart.allTooltipsOnce = true;
                                                                }

                                                                // turn on tooltips
                                                                chart.options.tooltips.enabled = true;
                                                                Chart.helpers.each(chart.pluginTooltips, function (tooltip) {
                                                                    tooltip.initialize();
                                                                    tooltip.update();
                                                                    // we don't actually need this since we are not animating tooltips
                                                                    tooltip.pivot();
                                                                    tooltip.transition(easing).draw();
                                                                });
                                                                chart.options.tooltips.enabled = false;
                                                            }
                                                        }
                                                    });




                                                    var config = {
                                                        type: 'line',

                                                        data: {
                                                            labels: ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"],
                                                            datasets: dataset
                                                        },
                                                        options: {
                                                            showAllTooltips: false,
                                                            responsive: true,
                                                            stacked: false,
                                                            hoverMode: 'index',
                                                            tooltips: {
                                                                callbacks: {
                                                                    label: function (tooltipItem) {
                                                                        console.log(tooltipItem)
                                                                        return 'E ' + parseFloat(tooltipItem.yLabel).toFixed(2);
                                                                    }
                                                                }
                                                            },
                                                            legend: {
                                                                display: false
                                                            },
                                                            title: {
                                                                display: false,
                                                                fontSize: 34,
                                                                fontColor: 'white'
                                                            },
                                                            scales: {
                                                                yAxes: scale
                                                            }

                                                        }
                                                    };

                                                    if ($('#selettore_data_partenza_grafico_cancellazioni').val() !== "") {
                                                        var ctx = document.getElementById("chart-area-2_cancellazioni").getContext("2d");
                                                        if (myChart_2_cancellazioni === undefined) {
                                                            myChart_2_cancellazioni = new Chart(ctx, config);

                                                        } else
                                                        {
                                                            myChart_2_cancellazioni.destroy();
                                                            myChart_2_cancellazioni = new Chart(ctx, config);
                                                        }
                                                    } else
                                                    {
                                                        if (myChart_2_cancellazioni != undefined) {
                                                            myChart_2_cancellazioni.destroy();
                                                        }
                                                    }

                                                    //-------------------FINE VENDUTI PER GIORNO---------------------//


                                                    if ($('#selettore_data_partenza_grafico_cancellazioni').val() !== "") {
                                                        $('#titolo_grafico_2_cancellazioni').html('Cancellazioni per Categoria');

                                                        var totale_generico_piu_consegna = 0;

                                                        for (var e in totale_consegne) {
                                                            totale_generico_piu_consegna = totale_generico + totale_consegne[e].importo;
                                                        }

                                                        $('#totale_grafici_cancellazioni').html('Tot: € ' + parseFloat(totale_generico_piu_consegna).toFixed(2) + "<br/>Media " + numero_giorni_media + " gg: € " + parseFloat(parseFloat(totale_generico_piu_consegna) / numero_giorni_media).toFixed(2));
                                                        $('#titolo_grafico_cancellazioni').html('Cancellazioni per Giorno');
                                                    } else
                                                    {
                                                        $('#titolo_grafico_2_cancellazioni').html('');
                                                        $('#totale_grafici_cancellazioni').html('');
                                                        $('#titolo_grafico_cancellazioni').html('');
                                                    }
                                                }

                                                if (settaggi_profili.Field103 === 'true') {

                                                    var totale_sconti_prezzo = 0;
                                                    var totale_sconti_quantita = 0;

                                                    var tab_sconti = "<table class='table table-responsive table-bordered' style='margin-left:15%;width:90%'>";

                                                    tab_sconti += "<tr style='padding:0;font-weight:bold;'>";

                                                    tab_sconti += "<td style='padding:0;text-align:center;'>";
                                                    tab_sconti += "Sconto"
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "<td  style='padding:0;text-align:center;'>";
                                                    tab_sconti += "Qta"
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "<td  style='padding:0;text-align:center;'>";
                                                    tab_sconti += "Media/Qta"
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "<td  style='padding:0;text-align:center;'>";
                                                    tab_sconti += "Media/&euro;"
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "<td style='padding:0;text-align:center;'>";
                                                    tab_sconti += "Totale"
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "</tr>";

                                                    var sconto_2;
                                                    var sconto_decrescente;
                                                    switch (settaggi_profili.Field96)
                                                    {
                                                        case "C":
                                                            sconto_decrescente = false;
                                                            break;
                                                        case "D":
                                                        default:
                                                            sconto_decrescente = true;
                                                    }

                                                    switch (settaggi_profili.Field95) {

                                                        case "Q":
                                                            sconto_2 = riordina(somma_array_oggetti(result_sconti, ["quantita_totale", "totale_sconti"], "valore_sconto"), 'quantita_totale', 'int', sconto_decrescente, 'valore_sconto');
                                                            break;
                                                        case "D":
                                                            sconto_2 = riordina(somma_array_oggetti(result_sconti, ["quantita_totale", "totale_sconti"], "valore_sconto"), 'valore_sconto', 'default', sconto_decrescente, 'valore_sconto');
                                                            break;
                                                        case "T":
                                                        default:
                                                            sconto_2 = riordina(somma_array_oggetti(result_sconti, ["quantita_totale", "totale_sconti"], "valore_sconto"), 'totale_sconti', 'float', sconto_decrescente, 'valore_sconto');
                                                    }

                                                    var totale_sconti_riuniti = 0.00;
                                                    var quantita_sconti_riuniti = 0;
                                                    for (var num in sconto_2) {

                                                        var element = sconto_2[num];
                                                        if (element.descrizione !== '0.00%') {


                                                            var valore_sconto = element.valore_sconto.substr(1);
                                                            var valore_sconto_confronto = element.valore_sconto.substr(1);
                                                            if (valore_sconto.slice(-1) === '%') {
                                                                valore_sconto_confronto = valore_sconto.slice(0, -2) + ' %';
                                                                valore_sconto = parseInt(valore_sconto.slice(0, -1)) + ' %';
                                                            } else
                                                            {
                                                                valore_sconto = '€ ' + valore_sconto;
                                                                valore_sconto_confronto = valore_sconto;
                                                            }

                                                            totale_sconti_quantita += parseInt(element.quantita_totale);
                                                            totale_sconti_prezzo += parseFloat(element.totale_sconti);

                                                            if (tabella_sconti_buoni.indexOf(valore_sconto_confronto) !== -1) {
                                                                tab_sconti += "<tr style='padding:0;'>";

                                                                tab_sconti += "<td style='padding:0;'>";
                                                                tab_sconti += valore_sconto;
                                                                tab_sconti += "</td>";

                                                                tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                                tab_sconti += parseInt(element.quantita_totale);
                                                                tab_sconti += "</td>";

                                                                tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                                tab_sconti += parseFloat(parseInt(element.quantita_totale) / numero_giorni_media).toFixed(2);
                                                                tab_sconti += "</td>";

                                                                tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                                tab_sconti += '€ ' + parseFloat(parseFloat(element.totale_sconti) / numero_giorni_media).toFixed(2);
                                                                tab_sconti += "</td>";

                                                                tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                                tab_sconti += '€ ' + parseFloat(element.totale_sconti).toFixed(2);
                                                                tab_sconti += "</td>";

                                                                tab_sconti += "</tr>";
                                                            } else {
                                                                totale_sconti_riuniti += parseFloat(element.totale_sconti);
                                                                quantita_sconti_riuniti += parseInt(element.quantita_totale);
                                                            }


                                                        }
                                                    }
                                                    if (totale_sconti_riuniti > 0) {
                                                        tab_sconti += "<tr style='padding:0;'>";

                                                        tab_sconti += "<td style='padding:0;'>";
                                                        tab_sconti += "Sconti Generici";
                                                        tab_sconti += "</td>";

                                                        tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                        tab_sconti += quantita_sconti_riuniti;
                                                        tab_sconti += "</td>";

                                                        tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                        tab_sconti += parseFloat(quantita_sconti_riuniti / numero_giorni_media).toFixed(2);
                                                        tab_sconti += "</td>";

                                                        tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                        tab_sconti += '€ ' + parseFloat(totale_sconti_riuniti / numero_giorni_media).toFixed(2);
                                                        tab_sconti += "</td>";

                                                        tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                        tab_sconti += '€ ' + totale_sconti_riuniti.toFixed(2);
                                                        tab_sconti += "</td>";

                                                        tab_sconti += "</tr>";
                                                    }


                                                    tab_sconti += "<tr style='font-weight:bold;padding:0;'>";

                                                    tab_sconti += "<td style='padding:0;'>";
                                                    tab_sconti += "Totale"
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                    tab_sconti += ""
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                    tab_sconti += ""
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                    tab_sconti += '€ ' + parseFloat(totale_sconti_prezzo / numero_giorni_media).toFixed(2);
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "<td style='text-align:right;padding:0;'>";
                                                    tab_sconti += '€ ' + parseFloat(totale_sconti_prezzo).toFixed(2);
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "</tr>";

                                                    tab_sconti += "</table>";

                                                    $("#tabella_sconti_backoffice_cancellazioni").html(tab_sconti);
                                                    $("#titolo_sconti_backoffice_cancellazioni").html("Sconti");


                                                    //BUONI SCONTO

                                                    var buoni_totale_emessi = 0;
                                                    var buoni_totale_riscossi = 0;
                                                    var buoni_totale_scaduti = 0;
                                                    var buoni_totale_euro = 0;

                                                    var tab_sconti = "<table class='table table-responsive table-bordered' style='margin-left:15%;width:90%'>";

                                                    tab_sconti += "<tr style='padding:0;font-weight:bold;'>";

                                                    tab_sconti += "<td style='padding:0;text-align:center;'>";
                                                    tab_sconti += "Buono"
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "<td  style='padding:0;text-align:center;'>";
                                                    tab_sconti += "Qta Emessi"
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "<td  style='padding:0;text-align:center;'>";
                                                    tab_sconti += "Qta Riscossi"
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "<td  style='padding:0;text-align:center;'>";
                                                    tab_sconti += "Qta Scaduti"
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "<td style='padding:0;text-align:center;'>";
                                                    tab_sconti += "Valore &euro;"
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "</tr>";

                                                    var sconto_2;
                                                    var sconto_decrescente;
                                                    switch (settaggi_profili.Field96)
                                                    {
                                                        case "C":
                                                            sconto_decrescente = false;
                                                            break;
                                                        case "D":
                                                        default:
                                                            sconto_decrescente = true;
                                                    }



                                                    for (var perc in oggetto_gestione_buoni) {

                                                        var element = oggetto_gestione_buoni[perc];

                                                        buoni_totale_emessi += parseInt(element.emessi);
                                                        buoni_totale_riscossi += parseInt(element.riscossi);
                                                        buoni_totale_scaduti += parseInt(element.scaduti);
                                                        buoni_totale_euro += parseFloat(element.euro);

                                                        tab_sconti += "<tr style='padding:0;'>";

                                                        tab_sconti += "<td style='padding:0;'>";
                                                        tab_sconti += perc + " " + "&#37";
                                                        tab_sconti += "</td>";

                                                        tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                        tab_sconti += element.emessi;
                                                        tab_sconti += "</td>";

                                                        tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                        tab_sconti += element.riscossi;
                                                        tab_sconti += "</td>";

                                                        tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                        tab_sconti += element.scaduti;
                                                        tab_sconti += "</td>";

                                                        tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                        tab_sconti += "&euro; " + arrotonda_prezzo(element.euro);
                                                        tab_sconti += "</td>";

                                                        tab_sconti += "</tr>";

                                                        //console.log("TOTALE SCONTI", valore_sconto, totale_sconti_quantita, parseFloat(element.totale_sconti).toFixed(2));


                                                    }


                                                    tab_sconti += "<tr style='font-weight:bold;padding:0;'>";

                                                    tab_sconti += "<td style='padding:0;'>";
                                                    tab_sconti += "Totale";
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "<td style='text-align:right;padding:0;'>";
                                                    tab_sconti += buoni_totale_emessi
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                    tab_sconti += buoni_totale_riscossi
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                    tab_sconti += buoni_totale_scaduti
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                    tab_sconti += "&euro; " + arrotonda_prezzo(buoni_totale_euro);
                                                    tab_sconti += "</td>";



                                                    tab_sconti += "</tr>";

                                                    tab_sconti += "</table>";

                                                    $("#tabella_buoni_sconto_backoffice_cancellazioni").html(tab_sconti);
                                                    $("#titolo_buoni_sconto_backoffice_cancellazioni").html("Buoni");

                                                }

                                                //COPERTI

                                                var media_qta_coperti = (parseInt(qta_coperti) / numero_giorni_media).toFixed(2);
                                                var media_totale_coperti = (parseFloat(totale_coperti) / numero_giorni_media).toFixed(2);


                                                var tab_sconti = "<table class='table table-responsive table-bordered' style='margin-left:15%;width:90%'>";

                                                tab_sconti += "<tr style='padding:0;font-weight:bold;'>";

                                                tab_sconti += "<td style='padding:0;text-align:center;'>";
                                                tab_sconti += "Qta"
                                                tab_sconti += "</td>";

                                                tab_sconti += "<td  style='padding:0;text-align:center;'>";
                                                tab_sconti += "Media Qta"
                                                tab_sconti += "</td>";

                                                tab_sconti += "<td style='padding:0;text-align:center;'>";
                                                tab_sconti += "Media &euro;"
                                                tab_sconti += "</td>";

                                                tab_sconti += "<td style='padding:0;text-align:center;'>";
                                                tab_sconti += "Totale &euro;"
                                                tab_sconti += "</td>";

                                                tab_sconti += "</tr>";


                                                tab_sconti += "<tr style='padding:0;font-weight:bold;'>";

                                                tab_sconti += "<td style='padding:0;text-align:center;'>";
                                                tab_sconti += qta_coperti;
                                                tab_sconti += "</td>";

                                                tab_sconti += "<td  style='padding:0;text-align:center;'>";
                                                tab_sconti += media_qta_coperti; /*media qta*/
                                                tab_sconti += "</td>";

                                                tab_sconti += "<td style='padding:0;text-align:center;'>";
                                                tab_sconti += media_totale_coperti; /*media euro*/
                                                tab_sconti += "</td>";

                                                tab_sconti += "<td style='padding:0;text-align:center;'>";
                                                tab_sconti += totale_coperti.toFixed(2);
                                                tab_sconti += "</td>";

                                                tab_sconti += "</tr>";









                                                tab_sconti += "</table>";

                                                $("#tabella_coperti_backoffice_cancellazioni").html(tab_sconti);
                                                $("#titolo_coperti_backoffice_cancellazioni").html("Coperti (gi&agrave; scontati)");

                                                //CONSEGNA



                                                var tab_sconti = "<table class='table table-responsive table-bordered' style='margin-left:15%;width:90%'>";

                                                tab_sconti += "<tr style='padding:0;font-weight:bold;'>";

                                                tab_sconti += "<td style='padding:0;text-align:center;'>";
                                                tab_sconti += "Descrizione"
                                                tab_sconti += "</td>";

                                                tab_sconti += "<td  style='padding:0;text-align:center;'>";
                                                tab_sconti += "Qta Pizze"
                                                tab_sconti += "</td>";

                                                tab_sconti += "<td style='padding:0;text-align:center;'>";
                                                tab_sconti += "Valore &euro;"
                                                tab_sconti += "</td>";

                                                tab_sconti += "</tr>";

                                                if (consegna_metro === true) {
                                                    tab_sconti += "<tr style='padding:0;'>";

                                                    tab_sconti += "<td style='padding:0;'>";
                                                    tab_sconti += "PIZZE NORMALI"
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                    tab_sconti += totale_consegne['NORMALI'].qta;
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                    tab_sconti += "&euro; " + arrotonda_prezzo(totale_consegne['NORMALI'].importo);
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "</tr>";

                                                    tab_sconti += "<tr style='padding:0;'>";

                                                    tab_sconti += "<td style='padding:0;'>";
                                                    tab_sconti += "MAXI";
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                    tab_sconti += totale_consegne['MAXI'].qta;
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                    tab_sconti += "&euro; " + arrotonda_prezzo(totale_consegne['MAXI'].importo);
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "</tr>";

                                                    tab_sconti += "<tr style='padding:0;'>";

                                                    tab_sconti += "<td style='padding:0;'>";
                                                    tab_sconti += "MEZZO METRO";
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                    tab_sconti += totale_consegne['MEZZO'].qta;
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                    tab_sconti += "&euro; " + arrotonda_prezzo(totale_consegne['MEZZO'].importo);
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "</tr>";

                                                    tab_sconti += "<tr style='padding:0;'>";

                                                    tab_sconti += "<td style='padding:0;'>";
                                                    tab_sconti += "UN METRO";
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                    tab_sconti += totale_consegne['METRO'].qta;
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                    tab_sconti += "&euro; " + arrotonda_prezzo(totale_consegne['METRO'].importo);
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "</tr>";

                                                    tab_sconti += "<tr style='padding:0;font-weight:bold;'>";

                                                    tab_sconti += "<td style='padding:0;'>";
                                                    tab_sconti += totale_qta_consegne + " Consegne";
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                    tab_sconti += parseInt(totale_consegne['NORMALI'].qta) + parseInt(totale_consegne['MAXI'].qta) + parseInt(totale_consegne['MEZZO'].qta) + parseInt(totale_consegne['METRO'].qta);
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                    tab_sconti += "&euro; " + arrotonda_prezzo(parseFloat(totale_consegne['NORMALI'].importo) + parseFloat(totale_consegne['MAXI'].importo) + parseFloat(totale_consegne['MEZZO'].importo) + parseFloat(totale_consegne['METRO'].importo));
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "</tr>";



                                                } else {


                                                    tab_sconti += "<tr style='padding:0;'>";

                                                    tab_sconti += "<td style='padding:0;'>";
                                                    tab_sconti += totale_qta_consegne + " Consegne";
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                    tab_sconti += totale_consegne['PIZZE'].qta;
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "<td  style='text-align:right;padding:0;'>";
                                                    tab_sconti += "&euro; " + arrotonda_prezzo(totale_consegne['PIZZE'].importo);
                                                    tab_sconti += "</td>";

                                                    tab_sconti += "</tr>";
                                                }





                                                tab_sconti += "</table>";

                                                $("#tabella_totale_consegne_backoffice_cancellazioni").html(tab_sconti);
                                                $("#titolo_totale_consegne_backoffice_cancellazioni").html("Consegna");





                                            }

                                            $('#filtri_utilizzati_backoffice_cancellazioni').html(filtro_data + '' + filtri);
                                            $('.loader2').hide();

                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            } else
            {
                bootbox.alert("Errore di Runtime nella sincronia dei dati del giorno.");
            }
        },
        error: function () {
            bootbox.alert("Errore di Ajax nella sincronia dei dati del giorno.");
        }
    });


}

function stampa_statistiche_A4_cancellazioni() {


    myChart_2_cancellazioni.options.showAllTooltips = false;
    myChart_2_cancellazioni.options.scales.yAxes[0].ticks.fontSize = 30;
    myChart_2_cancellazioni.options.scales.xAxes[0].ticks.fontSize = 30;
    myChart_2_cancellazioni.update();
    setTimeout(function () {

        //Azzero la parte di stampa
        $('.print').html('');

//Creo le pagine
        var div_element = document.createElement('div');
        div_element.id = "print_container_0_cancellazioni";
        div_element.style.position = 'block';
        $(div_element).css('height', '100vh');
        $(div_element).css('width', '100%');
        $(div_element).css('float', 'left');

        document.getElementsByClassName('print')[0].appendChild(div_element);

        //Creo le pagine
        var div_element = document.createElement('div');
        div_element.id = "print_container_00_cancellazioni";
        div_element.style.position = 'block';
        $(div_element).css('margin-bottom', '10px');
        $(div_element).css('width', '100%');
        $(div_element).css('clear', 'both');

        document.getElementById('print_container_0_cancellazioni').appendChild(div_element);

        //Creo le pagine
        var div_element = document.createElement('div');
        div_element.id = "print_container_1_cancellazioni";
        div_element.style.position = 'block';

        $(div_element).css('width', '50%');
        $(div_element).css('float', 'left');

        document.getElementById('print_container_0_cancellazioni').appendChild(div_element);

        var div_element = document.createElement('div');
        div_element.id = "print_container_2_cancellazioni";
        div_element.style.position = 'block';

        $(div_element).css('width', '50%');
        $(div_element).css('float', 'left');


        document.getElementById('print_container_0_cancellazioni').appendChild(div_element);

        var div_element = document.createElement('div');
        div_element.id = "print_container_3_cancellazioni";
        div_element.style.position = 'block';
        $(div_element).css('height', '100vh');


        document.getElementsByClassName('print')[0].appendChild(div_element);

        var div_element = document.createElement('div');
        div_element.id = "print_container_4_cancellazioni";
        div_element.style.position = 'block';

        document.getElementsByClassName('print')[0].appendChild(div_element);


        //Profilo aziendale

        var div_element = document.createElement('div');
        div_element.id = "print_100_cancellazioni";
        div_element.style.position = 'block';
        $(div_element).css('text-align', 'left');
        $(div_element).css('font-size', '1.5vh');


        document.getElementById('print_container_00_cancellazioni').appendChild(div_element);

        var a = '<div style="padding:5px;border:1px solid grey; overflow: auto;">';

        a += '<div style="float:left;"><strong>' + comanda.profilo_aziendale.ragione_sociale + '</strong></div>';
        a += '<div style="float:right;"><strong>' + comanda.locale + '</strong></div>';

        var data = new Date().format('dd/mm/yyyy');
        var ora = new Date().format('HH:MM');

        a += '<br/>' + '<div style="float:left;">Stampa effettuata il: <strong>' + data + '</strong></div>';
        a += '<div style="float:right;">' + 'Alle ore: <strong>' + ora + '</strong></div>';

        a += '</div>';

        div_element.innerHTML += a;


//Copio il titolo del grafico delle medie in area di stampa


        var div_element = document.createElement('div');
        div_element.id = "print_102_cancellazioni";
        div_element.style.position = 'block';
        $(div_element).css('font-size', '1.8vh');
        $(div_element).css('text-align', 'left');
        $(div_element).css('text-decoration', 'underline');
        $(div_element).css('padding-top', '10px');
        $(div_element).css('padding-left', '9vh');
        $(div_element).css('margin-bottom', '10px');



        document.getElementById('print_container_00_cancellazioni').appendChild(div_element);

        div_element.innerHTML = "FILTRI DI RICERCA<br/>";

        //Copio la legenda in area di stampa

        var firstDivContent = document.getElementById('filtri_utilizzati_backoffice_cancellazioni');

        var div_element = document.createElement('div');
        div_element.id = "print_103_cancellazioni";
        div_element.style.position = 'block';
        $(div_element).css('font-size', '1.5vh');
        $(div_element).css('text-align', 'left');

        document.getElementById('print_container_00_cancellazioni').appendChild(div_element);

        var secondDivContent = document.getElementById('print_103_cancellazioni');
        secondDivContent.innerHTML = firstDivContent.innerHTML;


//Copio il titolo del grafico delle medie in area di stampa


        var div_element = document.createElement('div');
        div_element.id = "print_109_cancellazioni";
        div_element.style.position = 'block';
        $(div_element).css('font-size', '1.8vh');
        $(div_element).css('margin-bottom', '4vh');
        $(div_element).css('text-align', 'center');
        $(div_element).css('text-decoration', 'underline');

        document.getElementById('print_container_1_cancellazioni').appendChild(div_element);

        div_element.innerHTML = "CANCELLAZIONI PER GIORNO<br/>";



//Copio il grafico in area di stampa
        var sourceCanvas = document.getElementById('chart-area-2_cancellazioni');

        var canvas = document.createElement('canvas');

        canvas.id = "print_110_cancellazioni";
        canvas.width = 1000;
        canvas.height = 500;
        canvas.style.zIndex = 8;
        canvas.style.position = "block";
        $(canvas).css('zoom', '40%');

        document.getElementById('print_container_1_cancellazioni').appendChild(canvas);

        destinationCtx = document.getElementById('print_110_cancellazioni').getContext('2d');

        destinationCtx.drawImage(sourceCanvas, 0, 0);




//Copio il titolo del grafico delle medie in area di stampa


        var div_element = document.createElement('div');
        div_element.id = "print_120_cancellazioni";
        div_element.style.position = 'block';
        $(div_element).css('font-size', '1vh');
        $(div_element).css('text-align', 'left');



        document.getElementById('print_container_1_cancellazioni').appendChild(div_element);

        div_element.innerHTML = int_venduti_giornalieri_stampa + lay_venduti_giornalieri;







        //Copio il titolo in area di stampa

        var div_element = document.createElement('div');
        div_element.id = "print_104_cancellazioni";
        div_element.style.position = 'block';
        $(div_element).css('font-size', '1.8vh');
        $(div_element).css('text-decoration', 'underline');
        $(div_element).css('margin-left', '18%');


        document.getElementById('print_container_2_cancellazioni').appendChild(div_element);

        div_element.innerHTML = "CANCELLAZIONI PER CATEGORIA<br/>";

        //Copio il totale in area di stampa

        var firstDivContent = document.getElementById('totale_grafici_cancellazioni');

        var div_element = document.createElement('div');
        div_element.id = "print_105_cancellazioni";
        div_element.style.position = 'block';
        $(div_element).css('font-size', '1.8vh');
        $(div_element).css('margin-top', '10px');
        $(div_element).css('margin-bottom', '10px');
        $(div_element).css('margin-left', '18%');

        document.getElementById('print_container_2_cancellazioni').appendChild(div_element);

        var secondDivContent = document.getElementById('print_105_cancellazioni');

        secondDivContent.innerHTML = firstDivContent.innerHTML;




        //Copio il grafico in area di stampa
        var sourceCanvas = document.getElementById('chart-area_cancellazioni');

        var canvas = document.createElement('canvas');

        canvas.id = "print_106_cancellazioni";
        canvas.width = 600;
        canvas.height = 500;
        canvas.style.zIndex = 8;
        canvas.style.position = "block";
        $(canvas).css('zoom', '40%');
        $(canvas).css('margin-left', '18%');

        document.getElementById('print_container_2_cancellazioni').appendChild(canvas);

        destinationCtx = document.getElementById('print_106_cancellazioni').getContext('2d');

        destinationCtx.drawImage(sourceCanvas, 0, 0);


        //Titolo della legenda

        var div_element = document.createElement('div');
        div_element.id = "print_107_cancellazioni";
        div_element.style.position = 'block';
        $(div_element).css('font-size', '1.5vh');
        $(div_element).css('margin-left', '18%');

        document.getElementById('print_container_2_cancellazioni').appendChild(div_element);

        div_element.innerHTML = "<strong>Lettura delle percentuali di cancellazione<br/>sul grafico in senso orario</strong><br/><br/>";



        //Copio la legenda in area di stampa

        var firstDivContent = document.getElementById('js-legend_cancellazioni');

        var div_element = document.createElement('div');
        div_element.id = "print_108_cancellazioni";
        div_element.style.position = 'block';
        $(div_element).css('font-size', '1vh');

        document.getElementById('print_container_2_cancellazioni').appendChild(div_element);

        var secondDivContent = document.getElementById('print_108_cancellazioni');

        secondDivContent.innerHTML = firstDivContent.innerHTML;

        $(secondDivContent).find('table').css('float', 'right');
        $(secondDivContent).find('table').css('width', '75%');
        $(secondDivContent).find('table').css('margin-left', '0');


        //Copio il titolo in area di stampa

        var div_element = document.createElement('div');
        div_element.id = "print_115_cancellazioni";
        div_element.style.position = 'block';
        $(div_element).css('font-size', '1.5vh');
        $(div_element).css('margin-left', '25%');
        $(div_element).css('font-family:', "'Lato','Helvetica Neue',Helvetica,Arial,sans-serif");
        $(div_element).css('text-align', 'center');
        $(div_element).css('font-weight', 'bold');
        $(div_element).css('clear', 'both');
        $(div_element).css('height', '4vh');

        document.getElementById('print_container_2_cancellazioni').appendChild(div_element);

        div_element.innerHTML = "Sconti<br/>";



        //Copio gli sconti in area stampa

        var firstDivContent = document.getElementById('tabella_sconti_backoffice_cancellazioni');

        var div_element = document.createElement('div');
        div_element.id = "print_114_cancellazioni";
        div_element.style.position = 'block';
        $(div_element).css('font-size', '1vh');

        document.getElementById('print_container_2_cancellazioni').appendChild(div_element);

        var secondDivContent = document.getElementById('print_114_cancellazioni');

        secondDivContent.innerHTML = firstDivContent.innerHTML;

        $(secondDivContent).find('table').css('float', 'right');
        $(secondDivContent).find('table').css('width', '75%');
        $(secondDivContent).find('table').css('margin-left', '0');

        if (comanda.pizzeria_asporto === true) {
            //Copio il titolo in area di stampa

            var div_element = document.createElement('div');
            div_element.id = "print_117_cancellazioni";
            div_element.style.position = 'block';
            $(div_element).css('font-size', '1.5vh');
            $(div_element).css('margin-left', '25%');
            $(div_element).css('font-family:', "'Lato','Helvetica Neue',Helvetica,Arial,sans-serif");
            $(div_element).css('text-align', 'center');
            $(div_element).css('font-weight', 'bold');
            $(div_element).css('clear', 'both');
            $(div_element).css('height', '4vh');

            document.getElementById('print_container_2_cancellazioni').appendChild(div_element);

            div_element.innerHTML = "Consegne<br/>";


            //Copio le consegne in area stampa

            var firstDivContent = document.getElementById('tabella_totale_consegne_backoffice_cancellazioni');
            if (firstDivContent === null)
            {
                firstDivContent = "";
            }
            var div_element = document.createElement('div');
            div_element.id = "print_118_cancellazioni";
            div_element.style.position = 'block';
            $(div_element).css('font-size', '1vh');

            document.getElementById('print_container_2_cancellazioni').appendChild(div_element);

            var secondDivContent = document.getElementById('print_118_cancellazioni');

            secondDivContent.innerHTML = firstDivContent.innerHTML;

            $(secondDivContent).find('table').css('float', 'right');
            $(secondDivContent).find('table').css('width', '75%');
            $(secondDivContent).find('table').css('margin-left', '0');
        } else if (comanda.pizzeria_asporto !== true) {
            //Copio il titolo in area di stampa

            var div_element = document.createElement('div');
            div_element.id = "print_117_cancellazioni";
            div_element.style.position = 'block';
            $(div_element).css('font-size', '1.5vh');
            $(div_element).css('margin-left', '25%');
            $(div_element).css('font-family:', "'Lato','Helvetica Neue',Helvetica,Arial,sans-serif");
            $(div_element).css('text-align', 'center');
            $(div_element).css('font-weight', 'bold');
            $(div_element).css('clear', 'both');
            $(div_element).css('height', '4vh');

            document.getElementById('print_container_2_cancellazioni').appendChild(div_element);

            div_element.innerHTML = "Coperti<br/>";


            //Copio le consegne in area stampa

            var firstDivContent = document.getElementById('tabella_coperti_backoffice_cancellazioni');
            if (firstDivContent === null)
            {
                firstDivContent = "";
            }
            var div_element = document.createElement('div');
            div_element.id = "print_118_cancellazioni";
            div_element.style.position = 'block';
            $(div_element).css('font-size', '1vh');

            document.getElementById('print_container_2_cancellazioni').appendChild(div_element);

            var secondDivContent = document.getElementById('print_118_cancellazioni');

            secondDivContent.innerHTML = firstDivContent.innerHTML;

            $(secondDivContent).find('table').css('float', 'right');
            $(secondDivContent).find('table').css('width', '75%');
            $(secondDivContent).find('table').css('margin-left', '0');
        }


        var div_element = document.createElement('div');
        div_element.id = "print_119_cancellazioni";
        div_element.style.position = 'block';
        $(div_element).css('font-size', '1.5vh');
        $(div_element).css('margin-left', '25%');
        $(div_element).css('font-family:', "'Lato','Helvetica Neue',Helvetica,Arial,sans-serif");
        $(div_element).css('text-align', 'center');
        $(div_element).css('font-weight', 'bold');
        $(div_element).css('clear', 'both');
        $(div_element).css('height', '4vh');

        document.getElementById('print_container_2_cancellazioni').appendChild(div_element);

        div_element.innerHTML = "Buoni<br/>";
//Copio i buoni in area stampa

        var firstDivContent = document.getElementById('tabella_buoni_sconto_backoffice_cancellazioni');

        var div_element = document.createElement('div');
        div_element.id = "print_116_cancellazioni";
        div_element.style.position = 'block';
        $(div_element).css('font-size', '1vh');

        document.getElementById('print_container_2_cancellazioni').appendChild(div_element);

        var secondDivContent = document.getElementById('print_116_cancellazioni');

        secondDivContent.innerHTML = firstDivContent.innerHTML;

        $(secondDivContent).find('table').css('float', 'right');
        $(secondDivContent).find('table').css('width', '75%');
        $(secondDivContent).find('table').css('margin-left', '0');



        //Copio il titolo dei venduti in area di stampa


        var popup_venduti_categorie = '<table class="table table-responsive table-striped table-hover text-left">';

        popup_venduti_categorie += '<tr style="font-weight:bold;">';

        popup_venduti_categorie += '<td colspan="3">';

        popup_venduti_categorie += 'LEGENDA STATO CANCELLAZIONE';

        popup_venduti_categorie += '</td>';

        popup_venduti_categorie += '</tr>';



        popup_venduti_categorie += '<tr>';

        popup_venduti_categorie += '<td style="width:10%;font-weight:bold;">';

        popup_venduti_categorie += 'CANC.';

        popup_venduti_categorie += '</td>';

        popup_venduti_categorie += '<td  style="width:20%;">';

        popup_venduti_categorie += 'CANCELLATO';

        popup_venduti_categorie += '</td>';

        popup_venduti_categorie += '<td style="width:70%;text-transform:uppercase;">';

        popup_venduti_categorie += 'Articolo eliminato dal carrello, ancor prima di essere lanciato in comanda/conto';

        popup_venduti_categorie += '</td>';

        popup_venduti_categorie += '</tr>';



        popup_venduti_categorie += '<tr>';

        popup_venduti_categorie += '<td style="font-weight:bold;">';

        popup_venduti_categorie += 'TAV.C';

        popup_venduti_categorie += '</td>';

        popup_venduti_categorie += '<td>';

        popup_venduti_categorie += 'TAVOLO CANCELLATO';

        popup_venduti_categorie += '</td>';

        popup_venduti_categorie += '<td style="text-transform:uppercase;">';

        popup_venduti_categorie += 'Tavolo eliminato del tutto prima di essere stato lanciato in comanda/conto';

        popup_venduti_categorie += '</td>';

        popup_venduti_categorie += '</tr>';


        popup_venduti_categorie += '<tr>';

        popup_venduti_categorie += '<td style="font-weight:bold;">';

        popup_venduti_categorie += 'C.SERV';

        popup_venduti_categorie += '</td>';

        popup_venduti_categorie += '<td>';

        popup_venduti_categorie += 'CAMBIO SERVIZIO';

        popup_venduti_categorie += '</td>';

        popup_venduti_categorie += '<td  style="text-transform:uppercase;">';

        popup_venduti_categorie += 'Articolo eliminato automaticamente dal cambio servizio che non &egrave; stato lanciato in comanda. Per tanto il tavolo/articolo non &egrave; stato incassato, n&egrave; cancellato durante il servizio.';

        popup_venduti_categorie += '</td>';
        popup_venduti_categorie += '</tr>';


        popup_venduti_categorie += '<tr>';

        popup_venduti_categorie += '<td style="font-weight:bold;">';

        popup_venduti_categorie += 'STORNO';

        popup_venduti_categorie += '</td>';

        popup_venduti_categorie += '<td>';

        popup_venduti_categorie += 'STORNO';

        popup_venduti_categorie += '</td>';

        popup_venduti_categorie += '<td style="text-transform:uppercase;">';

        popup_venduti_categorie += 'Articolo eliminato dopo essere stato lanciato in comanda/conto';

        popup_venduti_categorie += '</td>';

        popup_venduti_categorie += '</tr>';


        popup_venduti_categorie += '<tr>';

        popup_venduti_categorie += '<td style="font-weight:bold;">';

        popup_venduti_categorie += 'S.TAV';

        popup_venduti_categorie += '</td>';

        popup_venduti_categorie += '<td>';

        popup_venduti_categorie += 'STORNO TAVOLO';

        popup_venduti_categorie += '</td>';

        popup_venduti_categorie += '<td style="text-transform:uppercase;">';

        popup_venduti_categorie += 'Tavolo eliminato del tutto dopo essere stato lanciato in comanda/conto';

        popup_venduti_categorie += '</td>';

        popup_venduti_categorie += '</tr>';


        popup_venduti_categorie += '<tr>';

        popup_venduti_categorie += '<td style="font-weight:bold;">';

        popup_venduti_categorie += 'S.C.SERV';

        popup_venduti_categorie += '</td>';

        popup_venduti_categorie += '<td>';

        popup_venduti_categorie += 'STORNO CAMBIO SERVIZIO';

        popup_venduti_categorie += '</td>';

        popup_venduti_categorie += '<td style="text-transform:uppercase;">';

        popup_venduti_categorie += 'Articoli eliminati dal cambio di servizio dopo essere stato lanciati in comanda/conto. Per tanto gli articoli non sono stati incassati n&egrave; cancellati durante il servizio.';

        popup_venduti_categorie += '</td>';

        popup_venduti_categorie += '</tr>';


        popup_venduti_categorie += '</table>';

        var div_element = document.createElement('div');
        div_element.id = "print_111bis_cancellazioni";
        div_element.style.position = 'block';

        document.getElementById('print_container_4_cancellazioni').appendChild(div_element);



        div_element.innerHTML = popup_venduti_categorie;

        $("#print_111bis_cancellazioni").find("table td").css("font-size", "1vh");


        var div_element = document.createElement('div');
        div_element.id = "print_111_cancellazioni";
        div_element.style.position = 'block';
        $(div_element).css('font-size', '1.8vh');
        $(div_element).css('text-decoration', 'underline');

        document.getElementById('print_container_4_cancellazioni').appendChild(div_element);

        div_element.innerHTML = "ELENCO PRODOTTI:";



        //Copio i Venduti


        var div_element = document.createElement('div');
        div_element.id = "print_112_cancellazioni";
        div_element.style.position = 'block';


        var table_element = document.createElement('div');
        table_element.id = "print_113_cancellazioni";
        table_element.width = "100%";


        div_element.appendChild(table_element);



        document.getElementById('print_container_4_cancellazioni').appendChild(div_element);

        var firstDivContent = $('#tabella_elenco_venduti_001_cancellazioni')[0];

        var secondDivContent = document.getElementById('print_113_cancellazioni');

        secondDivContent.innerHTML = firstDivContent.innerHTML;

        $("#print_113_cancellazioni").find("table td").css("border", "1px solid rgb(221,221,221)");
        $("#print_113_cancellazioni").find("table td").css("padding", "2px");
        $("#print_113_cancellazioni").find("table td").css("font-size", "1vh");


        //Allineo e Stampo

        $('.print').css('text-align', 'center');

        window.print();


        myChart_2_cancellazioni.options.showAllTooltips = true;
        myChart_2_cancellazioni.options.scales.xAxes[0].ticks.fontSize = 15;
        myChart_2_cancellazioni.options.scales.yAxes[0].ticks.fontSize = 15;
        myChart_2_cancellazioni.update();
    }

    , 1000);

}