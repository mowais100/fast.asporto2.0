/* global comanda, alasql, RAM */

var oggetto_conto = new Array();

function riga_conto(prezzo_maxi_prima, prezzo_varianti_maxi, prezzo_varianti_aggiuntive, tasto_segue, spazio_forno, cod_promo, QTAP, giorno, nome_pc, stampata_sn, terminale, perc_iva, id, cod_articolo, ordinamento, peso_ums, ultima_portata, numero_conto, nodo, cat_variante, dest_stampa, dest_stampa_2, portata, desc_art, quantita, prezzo_un, categoria, ntav_comanda, operatore, tipo_record, stato_record, prog_inser, posizione, BIS, numero_paganti) {
    this.prezzo_maxi_prima = prezzo_maxi_prima;
    this.prezzo_varianti_maxi = prezzo_varianti_maxi;
    this.prezzo_varianti_aggiuntive = prezzo_varianti_aggiuntive;
    this.tasto_segue = tasto_segue;
    this.spazio_forno = spazio_forno;
    this.cod_promo = cod_promo;
    this.QTAP = QTAP;
    this.giorno = giorno;
    this.nome_pc = nome_pc;
    this.stampata_sn = stampata_sn;
    this.terminale = terminale;
    this.perc_iva = perc_iva;
    this.id = id;
    this.cod_articolo = cod_articolo;
    this.ordinamento = ordinamento;
    this.peso_ums = peso_ums;
    this.ultima_portata = ultima_portata;
    this.numero_conto = numero_conto;
    this.nodo = nodo;
    this.cat_variante = cat_variante;
    this.dest_stampa = dest_stampa;
    this.dest_stampa_2 = dest_stampa_2;
    this.portata = portata;
    this.desc_art = desc_art;
    this.quantita = quantita;
    this.prezzo_un = prezzo_un;
    this.categoria = categoria;
    this.ntav_comanda = ntav_comanda;
    this.operatore = operatore;
    this.tipo_record = tipo_record;
    this.stato_record = stato_record;
    this.prog_inser = prog_inser;
    this.posizione = posizione;
    this.BIS = BIS;
    this.numero_paganti = numero_paganti;
}

/* Int */
var ms_iniziali_battitura = 0;
/* String */
var log_velocita_battitura = "";

/* Boolean inizio, String nome_funzione */
function log_velocita(inizio, nome_funzione) {
    /*if (inizio === true) {
     ms_iniziali_battitura = new Date().getTime();
     log_velocita_battitura = "";
     }
     log_velocita_battitura += nome_funzione + ": " + (new Date().getTime() - ms_iniziali_battitura) + '\n';*/
}

function aggiungi_articolo(id_prodotto, descrizione, prezzo, form, quantita, pos, id_categoria_varianti, operazione, ntav_comanda, perc_iva_base, perc_iva_takeaway, prechiuso, callBACCO) {

    /*String*/
    comanda.alert_giacenza = false;
    comanda.tavolo_appena_aperto = false;

    var operazione_memorizzata_inizio = comanda.ultima_opz_variante;

    salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + aggZero(new Date().getMilliseconds(), 3) + " # TENTATVO BATTITURA ARTICOLO  # " + id_prodotto + " - " + descrizione + " - " + operazione + " - " + operazione_memorizzata_inizio + " # TASTO BLOCCATO: " + sblocco_tasti + "");

    log_velocita(true, "battitura");


    if ((pizza_metro === true && !(operazione_memorizzata_inizio === "=" || operazione_memorizzata_inizio === "+" || operazione_memorizzata_inizio === "-" || operazione_memorizzata_inizio === "*" || operazione_memorizzata_inizio === "("))) {
        sblocco_tasti = true;
    } else {


        $('#tasti_pizza_metro button').removeClass('cat_accesa');

        $('#tasti_fiscali_da_nascondere', 'body #creazione_comanda').css('pointer-events', 'none');
        $('#tasti_fiscali_da_nascondere', 'body #creazione_comanda').css('opacity', '0.5');
        $('.tasto_fiscale_bloccabile').css('pointer-events', '');
        $('.tasto_fiscale_bloccabile').css('opacity', '');
        if (comanda.dispositivo === "lang_33") {

            if (comanda.colora_tasto !== true) {
                var numero_layout = "_1";
                if (leggi_get_url('id') === '2' || leggi_get_url('id') === '3' || leggi_get_url('id') === '4') {
                    numero_layout = "_" + leggi_get_url('id');
                }


                var q1 = alasql("select id,categoria from prodotti where categoria!='XXX' and id='" + id_prodotto + "' limit 1;");
                if (q1 !== undefined && q1[0] !== undefined) {
                    var categoria_prodotto = q1[0].categoria;
                    var r = alasql("select id,descrizione from categorie where id='" + categoria_prodotto + "' limit 1;");


                    if (r !== undefined && r[0] !== undefined && r[0].descrizione === 'PREFERITI') {
                        comanda.prodotto_globale = true;
                    } else {
                        comanda.prodotto_globale = false;
                    }

                    var testo_query = "";
                    if (comanda.nome_categoria[comanda.categoria] === "PREFERITI") {
                        testo_query = "UPDATE prodotti SET  posizione" + numero_layout + " = '999' WHERE categoria!='XXX' and id = '" + id_prodotto + "' ; ";
                    } else if (comanda.nome_categoria[comanda.categoria] === "PREFERITI_1") {
                        testo_query = "UPDATE prodotti SET  posizione_1 = '999' WHERE categoria!='XXX' and id = '" + id_prodotto + "' ; ";
                    } else if (comanda.nome_categoria[comanda.categoria] === "PREFERITI_2") {
                        testo_query = "UPDATE prodotti SET  posizione_2 = '999' WHERE categoria!='XXX' and id = '" + id_prodotto + "' ; ";
                    } else if (comanda.nome_categoria[comanda.categoria] === "PREFERITI_3") {
                        testo_query = "UPDATE prodotti SET  posizione_3 = '999' WHERE categoria!='XXX' and id = '" + id_prodotto + "' ; ";
                    } else if (comanda.nome_categoria[comanda.categoria] === "PREFERITI_4") {
                        testo_query = "UPDATE prodotti SET  posizione_4 = '999' WHERE categoria!='XXX' and id = '" + id_prodotto + "' ; ";
                    } else {
                        testo_query = "UPDATE prodotti SET  posizione = '999' WHERE categoria!='XXX' and id = '" + id_prodotto + "' ; ";
                    }
                    comanda.sock.send({ tipo: "aggiornamento_pos_prodotti", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                    alasql(testo_query, function () {

                        crea_griglia_prodotti(null, function () {
                            elenco_prodotti();
                        });
                        comanda.sincro.query_cassa();
                        console.log("BODY POINTER EVENTS RESET");
                        $('body').css('pointer-events', '');
                        console.log("BODY POINTER EVENTS FREE C6");
                        if (typeof (callBACCO) === "function") {
                            callBACCO(true);
                        }


                    });
                }
            } else {
                console.log("BODY POINTER EVENTS RESET");
                $('body').css('pointer-events', '');
                console.log("BODY POINTER EVENTS FREE C7");
            }
            sblocco_tasti = true;
        } else if (comanda.dispositivo === "TAVOLO SELEZIONATO") {

            //MODIFICATO 10/02/2017 MA SECONDO ME CE' UN MOTIVO SE E' STATO FATTO COM ERA PRIMA

            if (quantita === 0 || quantita === '0' || quantita === '' || quantita === 'nï¿½' || isNaN(quantita)) {
                if (pos === "MOD_ART") {
                    console.log("BODY POINTER EVENTS RESET");
                    $('body').css('pointer-events', '');
                    $('#tasti_fiscali_da_nascondere', 'body #creazione_comanda').css('pointer-events', '');
                    $('#tasti_fiscali_da_nascondere', 'body #creazione_comanda').css('opacity', '');
                    console.log("BODY POINTER EVENTS FREE C8");
                    if (typeof (callBACCO) === "function") {
                        callBACCO(true);
                    }
                    return false;
                } else {
                    $('#quantita_articolo').val('1');
                    quantita = 1;
                }

            }

            if (comanda.ricerca_numerica_abilitata === true) {
                comanda.categoria = "";
            }

            if (comanda.categoria === "" || (comanda.nome_categoria[comanda.categoria.split('/')[0]] !== undefined && comanda.nome_categoria[comanda.categoria.split('/')[0]][0] !== 'V' && comanda.categoria.split('/')[1] === undefined)) {
                RAM.quantita_battuta = quantita;
            }

            console.log("aggiungi_articolo *", id_prodotto, descrizione, prezzo, form, quantita, pos, id_categoria_varianti, operazione, ntav_comanda, perc_iva_base, perc_iva_takeaway, prechiuso);


            if ((operazione === undefined || operazione === null || operazione === '') && (operazione_memorizzata_inizio === "+" || operazione_memorizzata_inizio === "-" || operazione_memorizzata_inizio === "(" || operazione_memorizzata_inizio === "*" || operazione_memorizzata_inizio === "x" || operazione_memorizzata_inizio === "=")) {
                salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + aggZero(new Date().getMilliseconds(), 3) + " # SET SEGNO VARIANTE  # " + operazione_memorizzata_inizio + "");

                operazione = operazione_memorizzata_inizio;
            } else if (operazione !== undefined && operazione !== null && operazione !== '') {

                salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + aggZero(new Date().getMilliseconds(), 3) + " # SET SEGNO VARIANTE  # LASCIO COSI ");

            } else {
                operazione = '';

                salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + aggZero(new Date().getMilliseconds(), 3) + " # SET SEGNO VARIANTE  # NIENTE ");

                //12102017 elimina specifica se operazione ï¿½ vuota

                comanda.specifica_tasti_sotto = '';
            }

            //AGGIUNGE AL DB L'ARTICOLO (TRAMITE FUNZIONIDBLOCALE)
            var aggiungi_articolo_db = function (callback) {
                comanda.funzionidb.aggiungi_articolo(id_prodotto, form, quantita, pos, operazione, null, perc_iva_base, perc_iva_takeaway, prechiuso, function () {
                    console.log("aggiungi_articolo_db call", ",", typeof (id_prodotto), id_prodotto, ",", typeof (form), form, ",", typeof (quantita), quantita, ",", typeof (pos), pos, ",", typeof (operazione), operazione, ",", null);

                    callback(true);
                });
            };
            //INSERISCE L'ARTICOLO PER UN CERTO NUMERO DI VOLTE
            var ins_art = function (i, numero_di_inserimenti) {
                if (i < numero_di_inserimenti) {
                    aggiungi_articolo_db(function () {
                        i++;
                        ins_art(i, numero_di_inserimenti, function () {
                        });
                    });
                } else {
                    if (descrizione !== "EXTRA") {
                        comanda.funzionidb.conto_attivo(function () {



                            if (typeof (callBACCO) === "function") {
                                console.log("CALLBACK CONTO ATTIVO ARTICOLI");
                                callBACCO(true);


                            }

                            //VARIANTE AUTOMATICA
                            if (comanda.variante_automatica !== false) {
                                comanda.ultima_cat_variante = comanda.variante_automatica;
                                mod_categoria(comanda.variante_automatica, '=', '', true);
                            }
                            comanda.variante_automatica = false;
                            //---//

                            if (comanda.gutshein === true) {
                                stampa_scontrino("SCONTRINO FISCALE");
                            }
                        });
                    } else {
                        if (typeof (callBACCO) === "function") {
                            console.log("CALLBACK CONTO ATTIVO ARTICOLI");
                            callBACCO(true);
                        }
                    }
                }
            };
            //SE NON STO SCORRENDO LA PAGINA LATERALMENTE SUI CELLULARI
            if (comanda.drag === false) {

                var numero_di_inserimenti = 1;
                //SCELGO L'AZIONE IN BASE ALLE POSIZIONI
                switch (pos) {
                    case "COPERTO":
                        console.log("aggiungi_articolo cassa", $('#numero_coperti_effettivi'));
                        if (comanda.tasto_contosep === "S" || comanda.multiquantita === "N") {
                            numero_di_inserimenti = quantita;
                            quantita = 1;
                        }
                        var i = 0;
                        ins_art(i, numero_di_inserimenti);
                        break;
                    case "SCONTO":

                        /* serve se cambi lo sconto nella schermata del pagamento per resettare gli importi */
                        /* altrimenti se batti prima l'importo e poi fai sconto ti viene fuori il resto */

                        if (($("#scelta_metodo_pagamento").data('bs.modal') || {}).isShown === true) {

                            $('#scelta_metodo_pagamento').modal('show');

                            var zIndex = Math.max.apply(null, Array.prototype.map.call(document.querySelectorAll('*'), function (el) {
                                return +el.style.zIndex;
                            })) + 10;

                            $('#scelta_metodo_pagamento').css('z-index', zIndex);

                            $('.campo_importo_metodo_pagamento').val('0.00');
                            $('.campo_importo_metodo_pagamento').css('font-weight', '');
                            var totale = parseFloat($(comanda.totale_scontrino).html()).toFixed(2);
                            $('#popup_metodo_pagamento_totale').html(totale);

                            $('#popup_metodo_pagamento_acconto').html(comanda.importo_acconto_fattura);
                            var acconto = parseFloat($('#popup_metodo_pagamento_acconto').html());
                            if (parseFloat(comanda.importo_acconto_fattura) > 0) {
                                $('.riga_pagamento_acconto').show();
                            } else {
                                $('.riga_pagamento_acconto').hide();
                            }
                            $('#popup_metodo_pagamento_pagato').html('0.00');

                            if (isNaN(acconto)) {
                                acconto = 0;
                            }
                            $('#popup_metodo_pagamento_da_pagare').html((totale - acconto).toFixed(2));
                            $('#popup_metodo_pagamento_da_pagare').css('font-weight', 'bold');
                            $('#popup_metodo_pagamento_resto').html('0.00');

                        }


                        comanda.ultima_opz_variante = "";
                        operazione_memorizzata_inizio = "";
                        operazione = operazione_memorizzata_inizio;

                        var BIS = lettera_conto_split();
                        if (form) {
                            var arr = {};
                            parse_str(form.replace(/\%/g, '%25'), arr);
                            var prodotto = arr;
                            if (prodotto.BIS !== undefined) {
                                BIS = prodotto.BIS;
                            }
                        }

                        var query_cancella_sconti = "DELETE FROM comanda WHERE ntav_comanda='" + comanda.tavolo + "' and stato_record='ATTIVO' and posizione='SCONTO' and BIS='" + BIS + "';";
                        comanda.sock.send({ tipo: "aggiornamento", operatore: comanda.operatore, query: query_cancella_sconti, terminale: comanda.terminale, ip: comanda.ip_address });
                        comanda.sincro.query(query_cancella_sconti, function () {
                            if (comanda.tasto_contosep === "S" || comanda.multiquantita === "N") {
                                numero_di_inserimenti = quantita;
                                quantita = 1;
                            }
                            var i = 0;
                            ins_art(i, numero_di_inserimenti);
                        });
                        break;
                    default:

                        //In questo caso pos diventa "CONTO"

                        pos = "CONTO";
                        if (comanda.ultima_opz_variante === "-" || operazione === "-") {

                            if ((comanda.licenza_vera === 'IBRIDO_Padovanelle' || comanda.licenza_vera === 'IBRIDO_Buonocore') && descrizione === 'BABY') {

                            } else {
                                prezzo = 'â‚¬0.00';
                            }

                        }

                        if (comanda.progressivo_modificabile === undefined || comanda.progressivo_modificabile === null) {
                            if (operazione_memorizzata_inizio === '+' || operazione_memorizzata_inizio === '-' || operazione_memorizzata_inizio === '*' || operazione_memorizzata_inizio === '=' || operazione_memorizzata_inizio === '(') {
                                comanda.funzionidb.aggiungi_articolo(id_prodotto, form, quantita, pos, operazione, comanda.progressivo_modificabile, perc_iva_base, perc_iva_takeaway, prechiuso, function () {
                                    if (descrizione !== "EXTRA") {
                                        comanda.funzionidb.conto_attivo(function () {
                                        });
                                    }
                                });
                            } else {
                                if (comanda.varianti_riunite === "S" && $('#quantita_articolo').val() > 1) {


                                    comanda.funzionidb.aggiungi_articolo(id_prodotto, form, quantita, pos, operazione, comanda.progressivo_modificabile, perc_iva_base, perc_iva_takeaway, prechiuso, function () {
                                        comanda.funzionidb.conto_attivo(function () {

                                            if (typeof (callBACCO) === "function") {
                                                console.log("CALLBACK CONTO ATTIVO ARTICOLI");
                                                callBACCO(true);
                                            }
                                        });
                                    });
                                } else {

                                    if (comanda.tasto_contosep === "S" || comanda.multiquantita === "N") {
                                        numero_di_inserimenti = quantita;
                                        quantita = 1;
                                    }
                                    var i = 0;
                                    ins_art(i, numero_di_inserimenti);
                                }
                            }
                        } else {
                            //console.log('aggiungi_articolo', id_prodotto, form, quantita, pos, operazione, comanda.progressivo_modificabile);
                            comanda.funzionidb.aggiungi_articolo(id_prodotto, form, quantita, pos, operazione, comanda.progressivo_modificabile, perc_iva_base, perc_iva_takeaway, prechiuso, function () {
                                if (descrizione !== "EXTRA") {
                                    comanda.funzionidb.conto_attivo(function () {

                                    });
                                }
                                comanda.progressivo_modificabile = null;
                            });
                        }



                        if (comanda.popup_modifica_articolo === true) {
                            ingrandisci_conto();
                        }




                        if (comanda.compatibile_xml !== true) {

                            /* se fai + da una variante automatica allora puoi aggiungere roba sulle varianti automatiche, che sia aperta o no la categoria
                             * non cambia una minchia */
                            if (comanda.pizzeria_asporto === true && comanda.categoria.indexOf("/") !== -1 && comanda.categoria.indexOf("ND") === -1 && (comanda.ultima_opz_variante === "+" || comanda.ultima_opz_variante === "-" || comanda.ultima_opz_variante === "*")) {
                                setTimeout(function () {
                                    mod_categoria(comanda.categoria, "=", false, true);
                                }, 250);
                            } else if (comanda.variante_aperta !== 'S' && comanda.ultima_cat_variante === comanda.categoria || comanda.flag_tipologia_articolo === "AUTO") {
                                setTimeout(function () {
                                    //Se è stato chiamato dai totali mobile
                                    if (comanda.scheda_precedente === "TOTALI-MOBILE") {
                                        //Torna anche ai totali dei cellulari e reimposta la scheda
                                        comanda.scheda_precedente = "RESET";
                                        mod_categoria(comanda.ultima_categoria_principale);
                                        //16/11/2016
                                        //rivedere perchè lo fa in ogni caso
                                        //btn_toggle_conto();
                                    } else {
                                        //Altrimenti torna alla categoria principale solamente
                                        mod_categoria(comanda.ultima_categoria_principale);
                                    }
                                }, 250);
                            }

                            // DEVE STARE IN QUESTA POSIZIONE, ALTRIMENTI SE L'ID DELLA CATEGORIA DELLA VARIANTE NON CORRISPONDE ALL'ID VARIANTE
                            // IN QUANTO C'E' UNA SOTTOVARIANTE, NON FUNZIONA UN CAZZO, OVVERO NON TORNA ALLA CATEGORIA PRINCIPALE DOPO AVER SELEZIONATO LA VARIANTE STESSA!

                            //AGGIUNTO && id_categoria_varianti !== 'undefined/ND' IL 19/01/2017 PER EVITARE ERRORE SU NUOVE VARIANTI CHE NON TORNA ALLA CAT.PRINC


                            if (id_categoria_varianti !== undefined && id_categoria_varianti !== 'undefined' && id_categoria_varianti !== 'undefined/undefined' && id_categoria_varianti !== 'undefined/ND' && id_categoria_varianti !== 0 && id_categoria_varianti !== null && id_categoria_varianti !== '' && id_categoria_varianti !== 'null') {
                                if (id_categoria_varianti.length !== 0) {
                                    comanda.ultima_cat_variante = id_categoria_varianti;
                                }
                                //In ogni caso, se la condizione è rispettata:
                                /*if (pizza_due_gusti !== true) {
                                 
                                 comanda.ultima_opz_variante = '';
                                 operazione_memorizzata_inizio = '';
                                 
                                 }*/

                                if (pizza_due_gusti !== true /*&& boolean_schiacciata !== true*/) {

                                    switch (comanda.flag_tipologia_articolo) {
                                        case "AUTOAPERTA":
                                            if (comanda.ultima_opz_variante !== '=') {
                                                comanda.ultima_opz_variante = '';
                                                operazione_memorizzata_inizio = '';
                                                mod_categoria(comanda.ultima_categoria_principale);
                                            }
                                            break;
                                        case "AUTO":
                                            comanda.ultima_opz_variante = '';
                                            operazione_memorizzata_inizio = '';
                                            mod_categoria(comanda.ultima_categoria_principale);
                                            break;
                                        case "APERTA":
                                            /*if (comanda.ultima_opz_variante !== '=') {
                                             comanda.ultima_opz_variante = '';
                                             operazione_memorizzata_inizio = '';
                                             mod_categoria(comanda.ultima_categoria_principale);
                                             }*/
                                            break;
                                        case "VARIANTE":
                                            comanda.ultima_opz_variante = '';
                                            operazione_memorizzata_inizio = '';
                                            mod_categoria(comanda.ultima_categoria_principale);
                                            break;
                                        case "NORMALE":
                                            comanda.ultima_cat_variante = id_categoria_varianti;
                                            comanda.ultima_opz_variante = '';
                                            operazione_memorizzata_inizio = '';
                                            break;
                                    }

                                }
                            }
                        } else if (pizza_due_gusti !== true /*&& boolean_schiacciata !== true*/) {
                            switch (comanda.flag_tipologia_articolo) {
                                case "AUTOAPERTA":
                                    if (comanda.ultima_opz_variante !== '=') {
                                        comanda.ultima_opz_variante = '';
                                        operazione_memorizzata_inizio = '';
                                        mod_categoria(comanda.ultima_categoria_principale);
                                    }
                                    break;
                                case "AUTO":
                                    comanda.ultima_opz_variante = '';
                                    operazione_memorizzata_inizio = '';
                                    mod_categoria(comanda.ultima_categoria_principale);
                                    break;
                                case "APERTA":
                                    /*if (comanda.ultima_opz_variante !== '=') {
                                     comanda.ultima_opz_variante = '';
                                     operazione_memorizzata_inizio = '';
                                     mod_categoria(comanda.ultima_categoria_principale);
                                     }*/
                                    break;
                                case "VARIANTE":
                                    comanda.ultima_opz_variante = '';
                                    operazione_memorizzata_inizio = '';
                                    mod_categoria(comanda.ultima_categoria_principale);
                                    break;
                                case "NORMALE":
                                    comanda.ultima_cat_variante = id_categoria_varianti;
                                    comanda.ultima_opz_variante = '';
                                    operazione_memorizzata_inizio = '';
                                    break;
                            }
                        }
                        break;
                }


                $('#quantita_articolo').val(1);
            } else {
                console.log("BODY POINTER EVENTS RESET");
                $('body').css('pointer-events', '');
                console.log("BODY POINTER EVENTS FREE C9");
                comanda.drag = false;
            }
            //}
        } else {
            console.log("BODY POINTER EVENTS RESET");
            $('body').css('pointer-events', '');
            console.log("BODY POINTER EVENTS FREE C10");
            if (typeof (callBACCO) === "function") {
                callBACCO(true);
            }
        }

    }
}

comanda.ult_prog_libero = undefined;
comanda.funzionidb.aggiungi_articolo = function (id, form, quantita, pos, operazione, progressivo, perc_iva_base, perc_iva_takeaway, prechiuso, cb) {

    var cod_articolo = '_DES';


    var time1 = new Date().getTime();
    console.log("TIME_AA1", 0);

    console.log("aggiungi_articolo **", id, form, quantita, pos, operazione, progressivo, perc_iva_base, perc_iva_takeaway, prechiuso, cb);
    var testo_query;
    var prodotto;
    var id_prodotto = id;
    //-------------------------------------------------------------------------------------------------
    //QUI INIZIA IL PROGRAMMA IN REALTA', MA IL SECONDO BLOCCO E' PRIMA ALTRIMENTI LA FUNZIONE SAREBBE UNDEFINED

    //se è un articolo con id
    if (id && form === null && id === "IDCSL_DES") {

        if (comanda.ultimo_progressivo_articolo !== undefined) {
            comanda.ult_prog_libero = comanda.ultimo_progressivo_articolo;
        }


        var r1 = alasql("SELECT cat_variante as cat_varianti,id,categoria,costo,prezzo_un as prezzo_1,cod_articolo,desc_art as descrizione,dest_stampa as dest_st_1,portata FROM comanda WHERE prog_inser='" + comanda.ult_prog_libero + "' and cod_articolo='" + id + "' and stato_record='ATTIVO' limit 1;");

        /*var cb1 = alasql("SELECT cat_var,ordinamento as ordinamento_prod,dest_stampa,dest_stampa_2,portata as portata_cat FROM categorie WHERE id='" + r1[0].categoria + "' limit 1;");*/
        var cb1 = comanda.caratteristiche_categoria[r1[0].categoria];

        //Memorizzo in prodotto le informazioni del prodotto da DataBase
        prodotto = $.extend({}, cb1, r1[0]);
        console.log("funzioni db aggiungi_articolo", prodotto);

        var time2 = new Date().getTime();
        console.log("TIME_AA2", time2 - time1);

        secondo_blocco(id, id_prodotto, progressivo, prodotto, quantita, pos, operazione, perc_iva_base, perc_iva_takeaway, prechiuso, cb);

        console.log("passaggio1 riga>");
    } else if (id && form === null) {

        //aggiunto il 28/06/2021 WHERE categoria !== XXX 
        //altrimenti fa un casino bestiale, tipo quando legge il prodotto magari prende la destinazione di stampa da uno già modificato

        var r1 = alasql("SELECT reparto_servizi,reparto_beni,prezzo_maxi,gruppo,automodifica_prezzo,spazio_forno,cat_varianti,id,abilita_riepilogo,categoria,costo,prezzo_1,prezzo_2,prezzo_3,prezzo_4,prezzo_fattorino1_norm,prezzo_fattorino2_norm,prezzo_fattorino3_norm,prezzo_fattorino1_maxi,prezzo_fattorino2_maxi,prezzo_fattorino3_maxi,prezzo_variante_meno,prezzo_varianti_aggiuntive,listino_bar,listino_asporto,listino_continuo,prezzo_2,cod_articolo,descrizione,prezzo_varianti_maxi,prezzo_maxi,prezzo_maxi_prima,prezzo_varianti_maxi,ordinamento as ordinamento_cat,peso_ums,dest_st_1,dest_st_2,cod_promo,portata as portata_prod,ultima_portata FROM prodotti WHERE categoria!='XXX' and descrizione!='.NUOVO PRODOTTO' and id='" + id + "' and categoria!='XXX' limit 1;");

        /* var cb1 = alasql("SELECT cat_var,ordinamento as ordinamento_prod,dest_stampa,dest_stampa_2,portata as portata_cat FROM categorie WHERE id='" + r1[0].categoria + "' limit 1;"); */
        var cb1 = comanda.caratteristiche_categoria[r1[0].categoria];

        //Memorizzo in prodotto le informazioni del prodotto da DataBase
        prodotto = $.extend({}, cb1, r1[0]);
        console.log("funzioni db aggiungi_articolo", prodotto);

        var time2 = new Date().getTime();
        console.log("TIME_AA2", time2 - time1);

        secondo_blocco(id, id_prodotto, progressivo, prodotto, quantita, pos, operazione, perc_iva_base, perc_iva_takeaway, prechiuso, cb);

        console.log("passaggio2 riga>");

    } else if (form && id === null) {
        var arr = {};
        parse_str(form.replace(/\%/g, '%25'), arr);
        //Memorizzo in prodotto le informazioni del prodotto da form
        prodotto = arr;
        console.log("funzioni db aggiungi_articolo", prodotto);
        var time2 = new Date().getTime();
        console.log("TIME_AA2", time2 - time1);

        secondo_blocco(id, id_prodotto, progressivo, prodotto, quantita, pos, operazione, perc_iva_base, perc_iva_takeaway, prechiuso, cb);

    }

    log_velocita(false, "aggiunta articolo funzionidb");
};

function secondo_blocco(id, id_prodotto, progressivo, prodotto, quantita, pos, operazione, perc_iva_base, perc_iva_takeaway, prechiuso, cb) {

    var peso_ums = "";
    var ultima_portata = "";
    var cod_articolo = "";
    var LIB4 = "";

    log_velocita(false, "inizio secondo blocco");

    var time1 = new Date().getTime();
    console.log("TIME_SB2", 0);

    var testo_query = 'SELECT id,perc_iva,reparto,cod_promo,quantita,prog_inser,prezzo_un,numero_conto,categoria,dest_stampa,dest_stampa_2,portata,substr(nodo, 1,3) as ultimo_nodo_utile FROM comanda WHERE ntav_comanda="' + comanda.tavolo + '" and stato_record="ATTIVO"  and fiscale_sospeso!="STAMPAFISCALESOSPESO" ORDER BY cast(prog_inser as int) DESC LIMIT 1;';
    console.log("cb2 query1", testo_query);
    if (comanda.ultimo_progressivo_variante === false && comanda.ultimo_progressivo_articolo !== undefined && operazione !== undefined && operazione !== null && ((operazione !== "" && operazione.length === 1) || prodotto.descrizione[0] === "(")) {
        //DEBUG 19/01/2017
        //QUESTA PUO' DARE EFFETTIVAMENTE UNDEFINED, SE NON CI SONO VARIANTI IN QUEL PROGRESSIVO ARTICOLO O SE NON C'E' QUEL PROGRESSIVO DAVVERO
        testo_query = 'SELECT id,perc_iva,reparto,cod_promo,quantita,(SELECT prog_inser FROM comanda WHERE ntav_comanda="' + comanda.tavolo + '" and stato_record="ATTIVO" ORDER BY cast(prog_inser as int) DESC LIMIT 1) as prog_inser,prezzo_un,numero_conto,categoria,dest_stampa,dest_stampa_2,portata,substr(nodo, 1,3) as ultimo_nodo_utile,stampata_sn,desc_art FROM comanda WHERE prog_inser="' + comanda.ultimo_progressivo_articolo + '" and ntav_comanda="' + comanda.tavolo + '" and stato_record="ATTIVO" and fiscale_sospeso!="STAMPAFISCALESOSPESO" ORDER BY cast(prog_inser as int) DESC LIMIT 1;';
        console.log("cb2 query2", testo_query);
    }


    //Lo cancello così non rompe più la minchia se aggiungo o voglio aggiungere all'ultimo articolo una specifica

    delete comanda.ultimo_progressivo_articolo;
    comanda.sincro.query(testo_query, function (cb2) {

        console.log("cb2 result", cb2);
        //Imposto cb2 come array non multidimensionale

        var prog_inser;
        var prog_temp;
        cb2 = cb2[0];
        console.log("passaggio2 riga>");
        console.log(cb2);
        console.log(prodotto);
        if (prodotto.gruppo === undefined || prodotto.gruppo === null || prodotto.gruppo === "null" || prodotto.gruppo === "undefined") {
            prodotto.gruppo = "";
        }
        var id_gruppo_statistico = prodotto.gruppo;
        //per quando aggiungo un progressivo a mano, ciè aggiungo le varianti da popup
        //C'è lo spostatore di progressivi nella parte di aggiungi articolo precedente
        if (cb2 !== undefined && cb2.id !== undefined && progressivo !== undefined && progressivo !== null) {


            //Aggiunge il campo variante per il raggruppamento
            /*
             *
             
             var query_aggiungi_variante = "update comanda set contiene_variante='1' where prog_inser='" + progressivo + "' and ntav_comanda='" + comanda.tavolo + "';";
             comanda.sincro.query(query_aggiungi_variante, function () {
             });
             
             **/
            id = cb2.id;
            prog_temp = progressivo;
            prog_inser = parseInt(parseInt(cb2.prog_inser) + 1);
            //prog_inser = progressivo + 1;
            //console.log("if: ", id);



            console.log("SCELTA PROG 1");
        }


        //Se c'è già  almeno un progressivo della comanda stessa, aggiunge uno
        else if (cb2 !== undefined && cb2.id && cb2.id !== undefined && cb2.id !== null) {

            console.log("SCELTA PROG 2");
            id = cb2.id;
            if (((operazione !== undefined && operazione !== "") || prodotto.descrizione[0] === "(") && prog_inser !== 0) {
                //console.log(true);
                prog_temp = cb2.ultimo_nodo_utile;
            } else {
                console.log("passaggio3 riga>");
                prog_temp = parseInt(parseInt(cb2.prog_inser) + 1);
            }

            progressivo = cb2.prog_inser;
            prog_inser = parseInt(parseInt(cb2.prog_inser) + 1);
            //console.log("elseif: ", id);


        } else {

            console.log("SCELTA PROG 3");
            //Id comanda è l'id per la comanda nuova
            //Unisco con tre lettere del prefisso dell'operatore per non incasinare il DB nella sincronizzazione (lo fa in id comanda)
            id = id_comanda();
            prog_temp = 1;
            prog_inser = 1;
            //console.log("else: ", id);

        }

        console.log("SCELTA PROG: VALUE " + prog_inser + "," + prog_temp);
        if (comanda.pizzeria_asporto === true) {
            var numero = comanda.tipologia_fattorino;
            switch (numero) {
                case "1":
                    if (prodotto.prezzo_fattorino1_norm !== undefined) {
                        prodotto.prezzo_1 = prodotto.prezzo_fattorino1_norm;
                        prodotto.prezzo_maxi_prima = prodotto.prezzo_fattorino1_maxi;
                        prodotto.prezzo_varianti_maxi = prodotto.prezzo_fattorino1_maxi;
                        prodotto.prezzo_varianti_aggiuntive = prodotto.prezzo_fattorino1_maxi;
                    }
                    break;
                case "2":
                    if (prodotto.prezzo_fattorino2_norm !== undefined) {
                        prodotto.prezzo_1 = prodotto.prezzo_fattorino2_norm;
                        prodotto.prezzo_maxi_prima = prodotto.prezzo_fattorino2_maxi;
                        prodotto.prezzo_varianti_maxi = prodotto.prezzo_fattorino2_maxi;
                        prodotto.prezzo_varianti_aggiuntive = prodotto.prezzo_fattorino2_maxi;
                    }
                    break;
                case "3":
                    if (prodotto.prezzo_fattorino3_norm !== undefined) {
                        prodotto.prezzo_1 = prodotto.prezzo_fattorino3_norm;
                        prodotto.prezzo_maxi_prima = prodotto.prezzo_fattorino3_maxi;
                        prodotto.prezzo_varianti_maxi = prodotto.prezzo_fattorino3_maxi;
                        prodotto.prezzo_varianti_aggiuntive = prodotto.prezzo_fattorino3_maxi;
                    }
                    break;
                default:

            }

        } else {
            if (comanda.tavolo.left(7) === "ASPORTO") {
                if (prodotto.listino_asporto !== undefined && prodotto.listino_asporto !== null && prodotto.listino_asporto !== "" && prodotto.listino_asporto !== "undefined") {
                    prodotto.prezzo_1 = prodotto.listino_asporto;
                }
            } else if (comanda.tavolo === "BAR") {
                if (prodotto.listino_asporto !== undefined && prodotto.listino_bar !== null && prodotto.listino_bar !== "" && prodotto.listino_bar !== "undefined") {
                    prodotto.prezzo_1 = prodotto.listino_bar;
                }
            } else if (comanda.tavolo.indexOf("CONTINUO") !== -1) {
                if (prodotto.listino_asporto !== undefined && prodotto.listino_continuo !== null && prodotto.listino_continuo !== "" && prodotto.listino_continuo !== "undefined") {
                    prodotto.prezzo_1 = prodotto.listino_continuo;
                }
            }
        }

        if (!(comanda.compatibile_xml === true && comanda.ARRAY_VAR_PICCOLA !== null && comanda.ARRAY_VAR_PICCOLA.indexOf(id_prodotto) !== -1)) {
            if (operazione === "-" || (prodotto.descrizione === comanda.lang_stampa[15] && (comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === "TAKEAWAY" || comanda.tavolo === "TAKE AWAY"))) {
                if ((comanda.licenza_vera === 'IBRIDO_Padovanelle' || comanda.licenza_vera === 'IBRIDO_Buonocore') && prodotto.descrizione === 'BABY') {
                    prodotto.prezzo_1 = parseFloat(prodotto.prezzo_1.replace(',', '.')) * -1;
                    prodotto.prezzo_1 = parseFloat(prodotto.prezzo_1).toFixed(2);
                } else {
                    prodotto.prezzo_1 = "0.00";
                }
            }
        }




        if (operazione === "*" && comanda.specifica_tasti_sotto === "POCO") {
            if (!isNaN(parseFloat(prodotto.prezzo_4))) {
                prodotto.prezzo_1 = prodotto.prezzo_4;
                prodotto.prezzo_maxi_prima = prodotto.prezzo_4;
                prodotto.prezzo_varianti_maxi = prodotto.prezzo_4;
                prodotto.prezzo_varianti_aggiuntive = prodotto.prezzo_4;
            } else {
                prodotto.prezzo_1 = "0.00";
            }
        }

        if (!(comanda.compatibile_xml === true && comanda.ARRAY_VAR_PICCOLA !== null && comanda.ARRAY_VAR_PICCOLA.indexOf(id_prodotto) !== -1)) {
            if (operazione === "-" && comanda.VARMENO === "S") {
                switch (comanda.SETB35) {
                    case "4":
                        if (!isNaN(parseFloat(prodotto.prezzo_4))) {
                            prodotto.prezzo_1 = prodotto.prezzo_4;
                        } else {
                            prodotto.prezzo_1 = "0.00";
                        }

                        break;
                    case "3":
                        if (!isNaN(parseFloat(prodotto.prezzo_3))) {
                            prodotto.prezzo_1 = prodotto.prezzo_3;
                        } else {
                            prodotto.prezzo_1 = "0.00";
                        }
                        break;
                    case "2":
                        if (!isNaN(parseFloat(prodotto.prezzo_2))) {
                            prodotto.prezzo_1 = prodotto.prezzo_2;
                        } else {
                            prodotto.prezzo_1 = "0.00";
                        }
                        break;
                    case "1":
                    default:

                }
                if (parseFloat(prodotto.prezzo_1.replace(",", ".")) > 0) {
                    prodotto.prezzo_1 = "-" + prodotto.prezzo_1;
                }
            }
        }

        if (operazione === "-" && prodotto.prezzo_variante_meno !== undefined && prodotto.prezzo_variante_meno !== null && prodotto.prezzo_variante_meno.trim().length > 0 && parseFloat(prodotto.prezzo_variante_meno) > 0) {
            prodotto.prezzo_1 = "-" + prodotto.prezzo_variante_meno;
        }

        var GIACENZA = "N";
        var QUANTITA_GIACENZA = 0;
        var promessa = new Promise(function (resolve, reject) {


            if (checkEmptyVariable(prodotto.cod_articolo)) {
                cod_articolo = prodotto.cod_articolo;
            } else if (checkEmptyVariable(id_prodotto)) {
                cod_articolo = id_prodotto;
            }

            /*prodotto.cod_articolo !== undefined && prodotto.cod_articolo !== '' && prodotto.cod_articolo !== null ? cod_articolo = prodotto.cod_articolo : cod_articolo = id_prodotto;*/

            if (prodotto !== undefined && prodotto.descrizione !== undefined && prodotto.descrizione !== null && prodotto.descrizione.indexOf("[P") !== -1 && cod_articolo !== comanda.SET16) {

                $("#popup_prezzo_prodotto [name='prezzo_1']").val(prodotto.prezzo_1.replace(",", "."));
                $("#popup_prezzo_prodotto [name='prezzo_um']").val(prodotto.prezzo_1.replace(",", "."));
                apri_popup_prezzo_prodotto(function (cb) {


                    //bootbox.prompt("Inserisci Prezzo", function (prezzo_box) {
                    quantita = $("#popup_prezzo_prodotto [name='quantita']").val();
                    prodotto.prezzo_1 = $("#popup_prezzo_prodotto [name='prezzo_1']").val();
                    var etti = $("#popup_prezzo_prodotto [name='peso_etti']").val();
                    if (parseFloat(etti) > 1) {
                        prodotto.descrizione = prodotto.descrizione.replace(" [P", " " + etti + " hg.");
                    } else {
                        prodotto.descrizione = prodotto.descrizione.replace(" [P", "");
                    }
                    LIB4 = "S";
                    if (comanda.compatibile_xml === true && comanda.controlla_giacenza === true && comanda.alert_giacenza === false) {

                        let query = "select * from ARTCON where ART='" + prodotto.id + "' AND GFLASH!='N';";
                        comanda.sincro.query_sqlserver(query, function (dati) {

                            if (dati.length > 0) {

                                QUANTITA_GIACENZA = dati[0].GIAC;
                                GIACENZA = "S";
                                var qt = alasql("select sum(cast(quantita as int)) as qt from comanda where stato_record='ATTIVO' and cod_articolo= '" + prodotto.id + "'");
                                if (qt.length > 0) {
                                    qt = parseFloat(qt[0].qt);
                                } else {
                                    qt = 0;
                                }

                                if (GIACENZA === "S" && (parseFloat(qt) + 1) > QUANTITA_GIACENZA) {
                                    comanda.alert_giacenza = true;
                                    bootbox.alert("Quantit&agrave; non sufficiente. <br> Disponibilit&agrave;: " + QUANTITA_GIACENZA);
                                } else if (GIACENZA === "S" && QUANTITA_GIACENZA <= 3) {
                                    comanda.alert_giacenza = true;
                                    bootbox.alert("Quantit&agrave; bassa. <br> Disponibilit&agrave;: " + QUANTITA_GIACENZA);
                                }

                                resolve(true);
                            } else {
                                resolve(false);
                            }

                        });
                    } else {
                        resolve(false);
                    }

                });
            } else {

                if (comanda.compatibile_xml === true && comanda.controlla_giacenza === true && comanda.alert_giacenza === false) {


                    let query = "select * from ARTCON where ART='" + prodotto.id + "' AND GFLASH!='N';";
                    comanda.sincro.query_sqlserver(query, function (dati) {

                        if (dati.length > 0) {

                            QUANTITA_GIACENZA = dati[0].GIAC;
                            GIACENZA = "S";
                            var qt = alasql("select sum(cast(quantita as int)) as qt from comanda where stato_record='ATTIVO' and  cod_articolo= '" + prodotto.id + "'");
                            if (qt.length > 0) {
                                qt = parseFloat(qt[0].qt);
                            } else {
                                qt = 0;
                            }
                            if (GIACENZA === "S" && (parseFloat(qt) + 1) > QUANTITA_GIACENZA) {
                                comanda.alert_giacenza = true;
                                bootbox.alert("Quantit&agrave; non sufficiente. <br> Disponibilit&agrave;: " + QUANTITA_GIACENZA);
                            } else if (GIACENZA === "S" && QUANTITA_GIACENZA <= 3) {
                                comanda.alert_giacenza = true;
                                bootbox.alert("Quantit&agrave; bassa. <br> Disponibilit&agrave;: " + QUANTITA_GIACENZA);
                            }


                            resolve(true);
                        } else {
                            resolve(false);
                        }

                    });
                } else {
                    resolve(false);
                }
            }

        });
        promessa.then(function () {
            var numero_conto = '1';
            var BIS = '';
            var categoria = '';
            var cat_varianti = '';
            var dest_stampa = '';
            var dest_stampa_2 = '';
            var portata = '';
            var nodo = '';
            var ordinamento = '';
            //console.log(cb2);

            if (cb2 !== undefined && cb2.numero_conto !== '1' && cb2.numero_conto !== '0' && cb2.numero_conto !== undefined) {
                numero_conto = cb2.numero_conto;
            }

            if (prodotto.descrizione === 'EXTRA') {
                if (comanda.conto === '#prodotti_conto_separato_2_grande') {
                    numero_conto = 2;
                } else {
                    numero_conto = 1;
                }
            }

            if (comanda.split_abilitato === true) {
                /*if (prodotto.BIS === undefined) {
                 BIS = lettera_conto_split();
                 } else
                 {
                 BIS = prodotto.BIS;
                 }*/

                if (checkEmptyVariable(prodotto.BIS)) {
                    BIS = prodotto.BIS;
                } else {
                    BIS = lettera_conto_split();
                }
            }




            console.log("FORM SCOMPOSTO_2", prodotto);
            console.log("VERIFICA_PROD_2", prodotto.dest_st_1);
            //era prodotto.ordinamento_prod)!==undefined il 10/03/2021 e else senza condizioni
            /* if (prodotto.ordinamento_prod) {
             ordinamento = prodotto.ordinamento_prod;
             } else {
             ordinamento = prodotto.ordinamento_cat;
             }*/

            if (checkEmptyVariable(prodotto.ordinamento_prod)) {
                ordinamento = prodotto.ordinamento_prod;
            } else if (checkEmptyVariable(prodotto.ordinamento_cat)) {
                ordinamento = prodotto.ordinamento_cat;
            }

            //era prodotto.cat_varianti !== "" il 10/03/2021 e else senza condizioni
            /*if (prodotto.cat_varianti !== "") {
             cat_varianti = prodotto.cat_varianti;
             } else {
             cat_varianti = prodotto.cat_var;
             }*/


            if (checkEmptyVariable(prodotto.cat_varianti) && prodotto.cat_varianti !== "undefined/undefined") {
                cat_varianti = prodotto.cat_varianti;
            } else if (checkEmptyVariable(prodotto.cat_var) && prodotto.cat_var !== "undefined/undefined") {
                cat_varianti = prodotto.cat_var;
            }

            //era (prodotto.dest_st_1 === undefined || prodotto.dest_st_1 === null || prodotto.dest_st_1 === "" || prodotto.dest_st_1 === "null") il 10/03/2021 e else senza condizioni
            /*if (prodotto.dest_st_1 === undefined || prodotto.dest_st_1 === null || prodotto.dest_st_1 === "" || prodotto.dest_st_1 === "null") {
             dest_stampa = prodotto.dest_stampa;
             } else {
             dest_stampa = prodotto.dest_st_1;
             }*/

            //era (prodotto.dest_st_2 === undefined || prodotto.dest_st_2 === null || prodotto.dest_st_2 === "" || prodotto.dest_st_2 === "null")  il 10/03/2021 e else senza condizioni
            /*if (prodotto.dest_st_2 === undefined || prodotto.dest_st_2 === null || prodotto.dest_st_2 === "" || prodotto.dest_st_2 === "null") {
             dest_stampa_2 = prodotto.dest_stampa_2;
             } else {
             dest_stampa_2 = prodotto.dest_st_2;
             }*/

            /* ho dovuto girare l'ordine degli else if, sia su questo che su quello sotto, altrimenti prendeva come prima scelta la destinazione di stampa della categoria
             * e poi quella del prodotto, sia per la destinazione di stampa 1 che la 2
             * 
             * modifica del 28/06/2021 alle 11:30
             */

            if (checkEmptyVariable(prodotto.dest_st_1)) {
                dest_stampa = prodotto.dest_st_1;
            } else if (checkEmptyVariable(prodotto.dest_stampa)) {
                dest_stampa = prodotto.dest_stampa;
            }

            if (checkEmptyVariable(prodotto.dest_st_2)) {
                dest_stampa_2 = prodotto.dest_st_2;
            } else if (checkEmptyVariable(prodotto.dest_stampa_2)) {
                dest_stampa_2 = prodotto.dest_stampa_2;
            }

            //console.log("2761" + prodotto.dest_st_1 + "-" + prodotto.dest_stampa + "+" + dest_stampa);

            //Doppio operatore ternario, perchè possono esserci varie possibilità

            //era (prodotto.portata_prod !== "undefined" && prodotto.portata_prod !== undefined && prodotto.portata_prod !== null && prodotto.portata_prod !== '')  il 10/03/2021 e else senza condizioni
            /*if (prodotto.portata_prod !== "undefined" && prodotto.portata_prod !== undefined && prodotto.portata_prod !== null && prodotto.portata_prod !== '') {
             portata = prodotto.portata_prod;
             } else {
             portata = prodotto.portata_cat;
             }*/

            //era (portata !== "undefined" && portata !== undefined && portata !== null && portata !== '') il 10/03/2021 e else senza condizioni
            /*if (portata !== "undefined" && portata !== undefined && portata !== null && portata !== '') {
             //resta com'era la portata. il  10/03/2021 è stata commentata la riga tanto non dovrebbe servire a niente se non c'è l'operatore ternario
             //portata;
             } else {
             portata = prodotto.portata;
             }*/

            //rivalutato il 10/03/2021 a partire dalle condizioni precedenti
            //prima valutazione
            /*if (prodotto.portata_prod !== "undefined" && prodotto.portata_prod !== undefined && prodotto.portata_prod !== null && prodotto.portata_prod !== '') {
             
             } else if (portata !== "undefined" && portata !== undefined && portata !== null && portata !== '') {
             
             } else if (prodotto.portata) {
             
             }*/

            //Seconda valutazione togliendo le condizioni già incluse nel controllo della variabile esistente, ovvero quelle sotto elencate
            /*      
             null
             undefined
             NaN
             empty string ("")
             0
             false
             */

            if (checkEmptyVariable(prodotto.portata_prod)) {
                portata = prodotto.portata_prod;
            } else if (checkEmptyVariable(prodotto.portata_cat)) {
                portata = prodotto.portata_cat;
            } else if (checkEmptyVariable(prodotto.portata)) {
                portata = prodotto.portata;
            }




            //-------------------------------------------------------------------//

            //era senza condizioni il  10/03/2021 
            if (checkEmptyVariable(prodotto.categoria)) {
                categoria = prodotto.categoria;
            }


            //console.log("DEST_STAMPA2", prodotto.dest_st_1 !== "", prodotto.dest_st_1 === undefined, dest_stampa, prodotto);


            nodo = aggZero(prog_temp, 3);
            //MODIFICATO IL 25/01/2017 PER RISOLVERE IL BUG DEL CB2
            if (((operazione !== undefined && operazione !== "") || prodotto.descrizione[0] === "(") && prog_inser > 1) {
                if (comanda.compatibile_xml === true) {

                    var AUTORIZ = "VAR";
                    if (parseFloat(prodotto.prezzo_1) > 0) {
                        AUTORIZ += '1 ';
                    } else {
                        AUTORIZ += '0 ';
                    }

                    if (operazione === '=') {
                        AUTORIZ += '=';
                        if (typeof (prog_temp) === "string") {
                            var query_aggiungi_autoriz = "update comanda set sconto_perc='" + AUTORIZ + "' where sconto_perc!='VAR1=' and stato_record='ATTIVO' and nodo LIKE '" + nodo + "%' and ntav_comanda='" + comanda.tavolo + "';";
                        } else {
                            var query_aggiungi_autoriz = "update comanda set sconto_perc='" + AUTORIZ + "' where sconto_perc!='VAR1=' and stato_record='ATTIVO' and prog_inser='" + prog_temp + "' and ntav_comanda='" + comanda.tavolo + "';";
                        }

                    } else {
                        if (typeof (prog_temp) === "string") {
                            var query_aggiungi_autoriz = "update comanda set sconto_perc='" + AUTORIZ + "' where sconto_perc!='VAR1' and sconto_perc!='VAR1='  and stato_record='ATTIVO' and nodo LIKE '" + nodo + "%' and ntav_comanda='" + comanda.tavolo + "';";
                        } else {
                            var query_aggiungi_autoriz = "update comanda set sconto_perc='" + AUTORIZ + "' where sconto_perc!='VAR1' and sconto_perc!='VAR1='  and stato_record='ATTIVO' and prog_inser='" + prog_temp + "' and ntav_comanda='" + comanda.tavolo + "';";
                        }

                    }

                }

                if (typeof (prog_temp) === "string") {
                    var query_aggiungi_variante = "update comanda set contiene_variante='1' where stato_record='ATTIVO' and nodo='" + nodo + "' and ntav_comanda='" + comanda.tavolo + "';";
                } else {
                    var query_aggiungi_variante = "update comanda set contiene_variante='1' where stato_record='ATTIVO' and prog_inser='" + prog_temp + "' and ntav_comanda='" + comanda.tavolo + "';";
                }

                console.log("query aggiungi variante", query_aggiungi_variante);
                comanda.sock.send({ tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: query_aggiungi_variante, terminale: comanda.terminale, ip: comanda.ip_address });
                comanda.sincro.query(query_aggiungi_variante, function () {

                    if (comanda.compatibile_xml === true) {
                        comanda.sock.send({ tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: query_aggiungi_autoriz, terminale: comanda.terminale, ip: comanda.ip_address });
                        comanda.sincro.query(query_aggiungi_autoriz, function () { });
                    }


                });
                nodo = nodo + ' ' + aggZero(prog_inser, 3);
                //la variante e nella stessa portata e categorie e destinazione dell'articolo principale
                //Bug categorie 

                //Modifica del 21/06/2019 di test cosi non mette le varianti e categorie nella stessa categoria principale
                ////Modifica del 16/07/2019 per mettere la stessa categoria della principale nelle varianti, ma solo sull'ibrido
                //altrimenti non riunisce le varianti all'articolo padre in modo corretto (verificato da Andrea)
                if (comanda.compatibile_xml === true) {
                    categoria = cb2.categoria;
                }

                dest_stampa = cb2.dest_stampa;
                dest_stampa_2 = cb2.dest_stampa_2;
                portata = cb2.portata;
                //console.log("VARIANTE", progressivo, prog_inser);
            } else {
                //AGGIUNTO IL 25/01/2017 PER RISOLVERE IL BUG DEL CB2
                operazione = '';
            }

            var perc_iva = "";
            var reparto = "";
            /* CONDIZIONI DEL REPARTO FINO AL 11/06/2021*/
            /*if (comanda.compatibile_xml !== true) {
             if (comanda.pizzeria_asporto === true || (comanda.tavolo === 'TAKE AWAY' || comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === '*TAKEAWAY*' || comanda.tavolotxt === '*TAKEAWAY*')) {
             reparto = prodotto.reparto_beni;
             } else {
             reparto = prodotto.reparto_servizi;
             }
             }*/


            if (operazione !== undefined && operazione !== null && (operazione === '+' || operazione === '-' || operazione === '*' || operazione === '=' || prodotto.descrizione[0] === '(')) {
                //se è variante prende IVA articolo padre a prescindere da tutto quanto
                perc_iva = cb2.perc_iva;
                reparto = cb2.reparto;
            } else if (comanda.lingua_stampa === 'deutsch') {
                if (comanda.tavolo === 'TAKE AWAY' || comanda.tavolo === '*TAKEAWAY*' || comanda.tavolotxt === '*TAKEAWAY*') {
                    if (perc_iva_takeaway !== undefined && perc_iva_takeaway !== null && perc_iva_takeaway !== '') {
                        perc_iva = perc_iva_takeaway;
                    } else {
                        perc_iva = '7';
                    }
                } else {
                    if (perc_iva_base !== undefined && perc_iva_base !== null && perc_iva_base !== '') {
                        perc_iva = perc_iva_base;
                    } else {
                        perc_iva = '19';
                    }
                }
            } else {

                let reparti = alasql('SELECT aliquota_default,aliquota_takeaway,reparto_beni_default,reparto_servizi_default,reparto_mangiaqui,reparto_ritiro,reparto_domicilio from impostazioni_fiscali LIMIT 1;');

                if (comanda.tipo_consegna === "NIENTE") {

                    if (reparti[0].reparto_ritiro === "0") {

                        if (perc_iva_base !== undefined && perc_iva_base !== null && perc_iva_base !== '') {
                            perc_iva = perc_iva_base;
                            if (comanda.compatibile_xml !== true) {
                                reparto = prodotto.reparto_servizi;
                            }
                        } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                            perc_iva = comanda.perc_iva_default;
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_servizi_default;
                            }
                        } else {
                            perc_iva = "10";
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_servizi_default;
                            }
                        }

                    } else if (reparti[0].reparto_ritiro === "1") {

                        if (perc_iva_takeaway !== undefined && perc_iva_takeaway !== null && !isNaN(perc_iva_takeaway) && perc_iva_takeaway !== '') {
                            perc_iva = perc_iva_takeaway;
                            if (comanda.compatibile_xml !== true) {
                                reparto = prodotto.reparto_beni;
                            }
                        } else if (comanda.percentuale_iva_takeaway !== undefined && comanda.percentuale_iva_takeaway !== null && !isNaN(comanda.percentuale_iva_takeaway) && comanda.percentuale_iva_takeaway !== '') {
                            perc_iva = comanda.percentuale_iva_takeaway;
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_beni_default;
                            }
                        } else if (perc_iva_base !== undefined && perc_iva_base !== null && perc_iva_base !== '') {
                            perc_iva = p.perc_iva_base;
                            if (comanda.compatibile_xml !== true) {
                                reparto = prodotto.reparto_servizi;
                            }
                        } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                            perc_iva = comanda.perc_iva_default;
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_servizi_default;
                            }
                        } else {
                            perc_iva = "22";
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_beni_default;
                            }
                        }

                    } else {

                        if (perc_iva_takeaway !== undefined && perc_iva_takeaway !== null && !isNaN(perc_iva_takeaway) && perc_iva_takeaway !== '') {
                            perc_iva = perc_iva_takeaway;
                            if (comanda.compatibile_xml !== true) {
                                reparto = prodotto.reparto_beni;
                            }
                        } else if (comanda.percentuale_iva_takeaway !== undefined && comanda.percentuale_iva_takeaway !== null && !isNaN(comanda.percentuale_iva_takeaway) && comanda.percentuale_iva_takeaway !== '') {
                            perc_iva = comanda.percentuale_iva_takeaway;
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_beni_default;
                            }
                        } else if (perc_iva_base !== undefined && perc_iva_base !== null && perc_iva_base !== '') {
                            perc_iva = p.perc_iva_base;
                            if (comanda.compatibile_xml !== true) {
                                reparto = prodotto.reparto_servizi;
                            }
                        } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                            perc_iva = comanda.perc_iva_default;
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_servizi_default;
                            }
                        } else {
                            perc_iva = "22";
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_beni_default;
                            }
                        }

                    }

                } else if (comanda.tipo_consegna === "PIZZERIA") {

                    if (reparti[0].reparto_mangiaqui === "0") {

                        if (perc_iva_base !== undefined && perc_iva_base !== null && perc_iva_base !== '') {
                            perc_iva = perc_iva_base;
                            if (comanda.compatibile_xml !== true) {
                                reparto = prodotto.reparto_servizi;
                            }
                        } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                            perc_iva = comanda.perc_iva_default;
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_servizi_default;
                            }
                        } else {
                            perc_iva = "10";
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_servizi_default;
                            }
                        }

                    } else if (reparti[0].reparto_mangiaqui === "1") {

                        if (perc_iva_takeaway !== undefined && perc_iva_takeaway !== null && !isNaN(perc_iva_takeaway) && perc_iva_takeaway !== '') {
                            perc_iva = perc_iva_takeaway;
                            if (comanda.compatibile_xml !== true) {
                                reparto = prodotto.reparto_beni;
                            }
                        } else if (comanda.percentuale_iva_takeaway !== undefined && comanda.percentuale_iva_takeaway !== null && !isNaN(comanda.percentuale_iva_takeaway) && comanda.percentuale_iva_takeaway !== '') {
                            perc_iva = comanda.percentuale_iva_takeaway;
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_beni_default;
                            }
                        } else if (perc_iva_base !== undefined && perc_iva_base !== null && perc_iva_base !== '') {
                            perc_iva = p.perc_iva_base;
                            if (comanda.compatibile_xml !== true) {
                                reparto = prodotto.reparto_servizi;
                            }
                        } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                            perc_iva = comanda.perc_iva_default;
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_servizi_default;
                            }
                        } else {
                            perc_iva = "22";
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_beni_default;
                            }
                        }

                    } else {

                        if (perc_iva_base !== undefined && perc_iva_base !== null && perc_iva_base !== '') {
                            perc_iva = perc_iva_base;
                            if (comanda.compatibile_xml !== true) {
                                reparto = prodotto.reparto_servizi;
                            }
                        } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                            perc_iva = comanda.perc_iva_default;
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_servizi_default;
                            }
                        } else {
                            perc_iva = "10";
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_servizi_default;
                            }
                        }

                    }



                } else if (comanda.tipo_consegna === 'RITIRO') {

                    if (reparti[0].reparto_ritiro === "0") {

                        if (perc_iva_base !== undefined && perc_iva_base !== null && perc_iva_base !== '') {
                            perc_iva = perc_iva_base;
                            if (comanda.compatibile_xml !== true) {
                                reparto = prodotto.reparto_servizi;
                            }
                        } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                            perc_iva = comanda.perc_iva_default;
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_servizi_default;
                            }
                        } else {
                            perc_iva = "10";
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_servizi_default;
                            }
                        }

                    } else if (reparti[0].reparto_ritiro === "1") {

                        if (perc_iva_takeaway !== undefined && perc_iva_takeaway !== null && !isNaN(perc_iva_takeaway) && perc_iva_takeaway !== '') {
                            perc_iva = perc_iva_takeaway;
                            if (comanda.compatibile_xml !== true) {
                                reparto = prodotto.reparto_beni;
                            }
                        } else if (comanda.percentuale_iva_takeaway !== undefined && comanda.percentuale_iva_takeaway !== null && !isNaN(comanda.percentuale_iva_takeaway) && comanda.percentuale_iva_takeaway !== '') {
                            perc_iva = comanda.percentuale_iva_takeaway;
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_beni_default;
                            }
                        } else if (perc_iva_base !== undefined && perc_iva_base !== null && perc_iva_base !== '') {
                            perc_iva = p.perc_iva_base;
                            if (comanda.compatibile_xml !== true) {
                                reparto = prodotto.reparto_servizi;
                            }
                        } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                            perc_iva = comanda.perc_iva_default;
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_servizi_default;
                            }
                        } else {
                            perc_iva = "22";
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_beni_default;
                            }
                        }

                    } else {

                        if (perc_iva_takeaway !== undefined && perc_iva_takeaway !== null && !isNaN(perc_iva_takeaway) && perc_iva_takeaway !== '') {
                            perc_iva = perc_iva_takeaway;
                            if (comanda.compatibile_xml !== true) {
                                reparto = prodotto.reparto_beni;
                            }
                        } else if (comanda.percentuale_iva_takeaway !== undefined && comanda.percentuale_iva_takeaway !== null && !isNaN(comanda.percentuale_iva_takeaway) && comanda.percentuale_iva_takeaway !== '') {
                            perc_iva = comanda.percentuale_iva_takeaway;
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_beni_default;
                            }
                        } else if (perc_iva_base !== undefined && perc_iva_base !== null && perc_iva_base !== '') {
                            perc_iva = p.perc_iva_base;
                            if (comanda.compatibile_xml !== true) {
                                reparto = prodotto.reparto_servizi;
                            }
                        } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                            perc_iva = comanda.perc_iva_default;
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_servizi_default;
                            }
                        } else {
                            perc_iva = "22";
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_beni_default;
                            }
                        }

                    }

                } else if (comanda.tipo_consegna === 'DOMICILIO') {

                    if (reparti[0].reparto_domicilio === "0") {

                        if (perc_iva_base !== undefined && perc_iva_base !== null && perc_iva_base !== '') {
                            perc_iva = perc_iva_base;
                            if (comanda.compatibile_xml !== true) {
                                reparto = prodotto.reparto_servizi;
                            }
                        } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                            perc_iva = comanda.perc_iva_default;
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_servizi_default;
                            }
                        } else {
                            perc_iva = "10";
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_servizi_default;
                            }
                        }

                    } else if (reparti[0].reparto_domicilio === "1") {

                        if (perc_iva_takeaway !== undefined && perc_iva_takeaway !== null && !isNaN(perc_iva_takeaway) && perc_iva_takeaway !== '') {
                            perc_iva = perc_iva_takeaway;
                            if (comanda.compatibile_xml !== true) {
                                reparto = prodotto.reparto_beni;
                            }
                        } else if (comanda.percentuale_iva_takeaway !== undefined && comanda.percentuale_iva_takeaway !== null && !isNaN(comanda.percentuale_iva_takeaway) && comanda.percentuale_iva_takeaway !== '') {
                            perc_iva = comanda.percentuale_iva_takeaway;
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_beni_default;
                            }
                        } else if (perc_iva_base !== undefined && perc_iva_base !== null && perc_iva_base !== '') {
                            perc_iva = p.perc_iva_base;
                            if (comanda.compatibile_xml !== true) {
                                reparto = prodotto.reparto_servizi;
                            }
                        } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                            perc_iva = comanda.perc_iva_default;
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_servizi_default;
                            }
                        } else {
                            perc_iva = "22";
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_beni_default;
                            }
                        }

                    } else {

                        if (perc_iva_takeaway !== undefined && perc_iva_takeaway !== null && !isNaN(perc_iva_takeaway) && perc_iva_takeaway !== '') {
                            perc_iva = perc_iva_takeaway;
                            if (comanda.compatibile_xml !== true) {
                                reparto = prodotto.reparto_beni;
                            }
                        } else if (comanda.percentuale_iva_takeaway !== undefined && comanda.percentuale_iva_takeaway !== null && !isNaN(comanda.percentuale_iva_takeaway) && comanda.percentuale_iva_takeaway !== '') {
                            perc_iva = comanda.percentuale_iva_takeaway;
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_beni_default;
                            }
                        } else if (perc_iva_base !== undefined && perc_iva_base !== null && perc_iva_base !== '') {
                            perc_iva = p.perc_iva_base;
                            if (comanda.compatibile_xml !== true) {
                                reparto = prodotto.reparto_servizi;
                            }
                        } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                            perc_iva = comanda.perc_iva_default;
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_servizi_default;
                            }
                        } else {
                            perc_iva = "22";
                            if (comanda.compatibile_xml !== true) {
                                reparto = comanda.reparto_beni_default;
                            }
                        }

                    }

                } else if (comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === 'TAKE AWAY' || comanda.tavolo === '*TAKEAWAY*' || comanda.tavolotxt === '*TAKEAWAY*') {

                    if (perc_iva_takeaway !== undefined && perc_iva_takeaway !== null && !isNaN(perc_iva_takeaway) && perc_iva_takeaway !== '') {
                        perc_iva = perc_iva_takeaway;
                        if (comanda.compatibile_xml !== true) {
                            reparto = prodotto.reparto_beni;
                        }
                    } else if (comanda.percentuale_iva_takeaway !== undefined && comanda.percentuale_iva_takeaway !== null && !isNaN(comanda.percentuale_iva_takeaway) && comanda.percentuale_iva_takeaway !== '') {
                        perc_iva = comanda.percentuale_iva_takeaway;
                        if (comanda.compatibile_xml !== true) {
                            reparto = comanda.reparto_beni_default;
                        }
                    } else if (perc_iva_base !== undefined && perc_iva_base !== null && perc_iva_base !== '') {
                        perc_iva = p.perc_iva_base;
                        if (comanda.compatibile_xml !== true) {
                            reparto = prodotto.reparto_servizi;
                        }
                    } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                        perc_iva = comanda.perc_iva_default;
                        if (comanda.compatibile_xml !== true) {
                            reparto = comanda.reparto_servizi_default;
                        }
                    } else {
                        perc_iva = "22";
                        if (comanda.compatibile_xml !== true) {
                            reparto = comanda.reparto_beni_default;
                        }
                    }

                } else {

                    if (perc_iva_base !== undefined && perc_iva_base !== null && perc_iva_base !== '') {
                        perc_iva = perc_iva_base;
                        if (comanda.compatibile_xml !== true) {
                            reparto = prodotto.reparto_servizi;
                        }
                    } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                        perc_iva = comanda.perc_iva_default;
                        if (comanda.compatibile_xml !== true) {
                            reparto = comanda.reparto_servizi_default;
                        }
                    } else {
                        perc_iva = "10";
                        if (comanda.compatibile_xml !== true) {
                            reparto = comanda.reparto_servizi_default;
                        }
                    }

                }

            }

            var prezzo = '0.00';
            if (comanda.prezzo_tasti_sotto !== "0.00") {
                prezzo = parseFloat(comanda.prezzo_tasti_sotto.replace(/,/g, '.'));
            } else {
                if (prodotto.prezzo_1 === null || prodotto.prezzo_1 === 'null' || prodotto.prezzo_1 === '') {
                    prodotto.prezzo_1 = '0.00';
                }

                prezzo = parseFloat(prodotto.prezzo_1.replace(/,/g, '.')).toFixed(2);
                if (comanda.lingua_stampa === 'deutsch' && comanda.licenza_vera === 'IBRIDO_DiegoCampo' && comanda.tavolotxt === '*TAKEAWAY*') {
                    prezzo = parseFloat(prodotto.prezzo_2.replace(/,/g, '.')).toFixed(2);
                }

                if (prodotto.prezzo_1.substr(-1) === "%") {
                    prezzo = prezzo + "%";
                }
            }

            //PER LE SPECIFICHE DOPPIO E META'
            /*if (prodotto.descrizione === '(DOPPIO)' || prodotto.descrizione === '(ABBONDANTE)') {
             prezzo = cb2.prezzo_un;
             }
             
             if (prodotto.descrizione === '(META)' || prodotto.descrizione === '(MEZZO)') {
             prezzo = parseFloat((parseFloat(cb2.prezzo_un) / 2) * -1).toFixed(2);
             }*/


            //FINE SPECIFICHE DOPPIO E META'

            var descrizione_articolo = '';

            /* messo il 16/04/2021 perchè altrimenti se mettevi una specifica dopo un gusto ti metteva l'uguale davanti alla specifica e prezzo a 0 */
            if (operazione === "=" && prodotto.descrizione[0] === "(") {
                operazione = "";
            }

            if (comanda.compatibile_xml === true && comanda.ARRAY_VAR_PICCOLA !== null && comanda.ARRAY_VAR_PICCOLA.indexOf(id_prodotto) !== -1) {
                operazione = "+";
                descrizione_articolo += "-";
                if (parseFloat(prezzo) > 0) {
                    prezzo = "-" + prezzo;
                }
            } else if (operazione !== undefined && operazione !== null && operazione.length > 0) {
                descrizione_articolo += operazione;
            }

            if (comanda.specifica_tasti_sotto !== undefined && comanda.specifica_tasti_sotto !== null && comanda.specifica_tasti_sotto.length > 1 && pizza_due_gusti !== true /*&& boolean_schiacciata !== true*/) {
                descrizione_articolo += comanda.specifica_tasti_sotto + ' ';
            } else if (comanda.specifica_maxi === "MAXI") {

                if (comanda.pizzeria_asporto === true) {

                    if (!isNaN(parseFloat(prodotto.prezzo_maxi))) {
                        prezzo = prodotto.prezzo_maxi;
                    }

                    var numero = comanda.tipologia_fattorino;
                    switch (numero) {
                        case "1":
                            if (prodotto.prezzo_fattorino1_norm !== undefined) {
                                if (!isNaN(parseFloat(prodotto.prezzo_fattorino1_maxi))) {
                                    prezzo = prodotto.prezzo_fattorino1_maxi;
                                }
                            }
                            break;
                        case "2":
                            if (prodotto.prezzo_fattorino2_norm !== undefined) {
                                if (!isNaN(parseFloat(prodotto.prezzo_fattorino2_maxi))) {
                                    prezzo = prodotto.prezzo_fattorino2_maxi;
                                }
                            }
                            break;
                        case "3":
                            if (prodotto.prezzo_fattorino3_norm !== undefined) {
                                if (!isNaN(parseFloat(prodotto.prezzo_fattorino3_maxi))) {
                                    prezzo = prodotto.prezzo_fattorino3_maxi;
                                }
                            }
                            break;
                        default:

                    }

                } else {

                    if (!isNaN(parseFloat(prodotto.prezzo_maxi))) {
                        prezzo = prodotto.prezzo_maxi;
                    }
                }
                descrizione_articolo += 'MAXI ';
            }





            if ((operazione === '+' || operazione === '-' || operazione === '*' || operazione === '=' || prodotto.descrizione[0] === '(')) {
                if (parseInt(quantita) > 1) {

                    descrizione_articolo += "N° " + quantita + " " + prodotto.descrizione;
                    prezzo = (parseFloat(prezzo) * quantita).toFixed(2);
                    quantita = RAM.quantita_battuta;
                } else {
                    quantita = 1;
                    if (comanda.multiquantita === "S") {
                        if (cb2 !== undefined && cb2.quantita !== undefined && cb2.quantita !== "null" && cb2.quantita !== "" && !isNaN(cb2.quantita)) {
                            quantita = parseInt(cb2.quantita);
                        }
                    }
                    descrizione_articolo += prodotto.descrizione;
                }
            } else {
                descrizione_articolo += prodotto.descrizione;
            }



            if (comanda.pizzeria_asporto === true) {
                if (pizza_metro === true && operazione === "=") {
                    var coefficente_quarti = 1;
                    if (quantita_pizza_metro === 'MEZZO') {
                        switch (comanda.specifica_tasti_sotto) {
                            case "1/2":
                                coefficente_quarti = 1;
                                break;
                            case "":
                                coefficente_quarti = 2;
                                break;
                        }
                    } else {
                        switch (comanda.specifica_tasti_sotto) {
                            case "1/4":
                                coefficente_quarti = 1;
                                break;
                            case "1/2":
                                coefficente_quarti = 2;
                                break;
                            case "":
                                coefficente_quarti = 4;
                                break;
                        }
                    }

                    if (isNaN(parseFloat(prodotto.prezzo_2))) {
                        prezzo = parseFloat(parseFloat(prodotto.prezzo_1) * coefficente_quarti).toFixed(2);
                    } else {
                        prezzo = parseFloat(parseFloat(prodotto.prezzo_2) * coefficente_quarti).toFixed(2);
                    }
                } else if (prodotto.descrizione[0] === "(") {


                    if (prodotto.prezzo_1.search(/[\+]\d+\%/gi) !== -1) {
                        var perc = prodotto.prezzo_1.match(/\d+/gi)[0];
                        prezzo = ((parseFloat(cb2.prezzo_un) / 100 * parseFloat(perc))).toFixed(2);
                        descrizione_articolo = descrizione_articolo.slice(0, -1) + ' ' + prodotto.prezzo_1 + ')';
                    } else if (prodotto.prezzo_1.search(/[\-]\d+\%/gi) !== -1) {
                        var perc = prodotto.prezzo_1.match(/\d+/gi)[0];
                        prezzo = ((parseFloat(cb2.prezzo_un) / 100 * parseFloat(perc)) * -1).toFixed(2);
                        descrizione_articolo = descrizione_articolo.slice(0, -1) + ' ' + prodotto.prezzo_1 + ')';
                    }


                } /*else if (boolean_schiacciata === true && operazione === "=") {
                    if (descrizione_articolo[1] !== undefined && descrizione_articolo[1] !== "(") {
                        descrizione_articolo = "= " + descrizione_articolo.substr(1);
                    } else {
                        descrizione_articolo = descrizione_articolo.substr(1);
                    }
                } */else if (pizza_due_gusti === true && operazione === "=") {
                    if (descrizione_articolo[1] !== undefined && descrizione_articolo[1] !== "(") {
                        descrizione_articolo = "= 1/2 " + descrizione_articolo.substr(1);
                    } else {
                        descrizione_articolo = descrizione_articolo.substr(1);
                    }
                }


            }
            //PARTE NUOVA DA TESTARE (PER ORA NON VA BENE)
            else if (prodotto.descrizione[0] === "(") {


                if (prodotto.prezzo_1.search(/[\+]\d+\%/gi) !== -1) {
                    var perc = prodotto.prezzo_1.match(/\d+/gi)[0];
                    prezzo = ((parseFloat(cb2.prezzo_un) / 100 * parseFloat(perc))).toFixed(2);
                    descrizione_articolo = descrizione_articolo.slice(0, -1) + ' ' + prodotto.prezzo_1 + ')';
                } else if (prodotto.prezzo_1.search(/[\-]\d+\%/gi) !== -1) {
                    var perc = prodotto.prezzo_1.match(/\d+/gi)[0];
                    prezzo = ((parseFloat(cb2.prezzo_un) / 100 * parseFloat(perc)) * -1).toFixed(2);
                    descrizione_articolo = descrizione_articolo.slice(0, -1) + ' ' + prodotto.prezzo_1 + ')';
                }


            }
           // settaggio per diattivare cod_promo.
            var testo_query = "SELECT Field510 from settaggi_profili";
            var querry_settagio = alasql(testo_query);
            if( querry_settagio[0].Field510!=="1"){
            if (comanda.compatibile_xml !== true && RAM.tabella_sconti_cod_promo_2 !== undefined && RAM.tabella_sconti_cod_promo_2[prodotto.id] !== undefined) {

                //Deve scegliere lo sconto più alto tra quelli disponibili quindi deve fare dei calcoli

                var d = new Date();
                var ora_attuale = d.format('HH:MM');
                var prezzo_migliore = new Object();
                prezzo_migliore.prezzo = parseFloat(prezzo);
                prezzo_migliore.tipo_sconto = '';
                prezzo_migliore.valore_sconto = '';
                RAM.tabella_sconti_cod_promo_2[prodotto.id].forEach(function (v) {
                    //




                    var calcolo_temporaneo_prezzo = 0;
                    if (v.qta_soglia === '') {
                        v.qta_soglia = '0';
                    }

                    if (v.da_ora === '') {
                        v.da_ora = '00:00';
                    }

                    if (v.a_ora === '') {
                        v.a_ora = '23:59';
                    }

                    var giorni_settimana = '0;1;2;3;4;5;6';
                    if (v.giorni_settimana !== '') {
                        giorni_settimana = v.giorni_settimana;
                    }

                    var oggi = d.getDay();
                    if (giorni_settimana.indexOf(oggi.toString()) !== -1 && parseInt(RAM.quantita_battuta) >= parseInt(v.qta_soglia) && ora_attuale >= v.da_ora && ora_attuale < v.a_ora) {
                        if (v.tipo_sconto === "P") {
                            calcolo_temporaneo_prezzo = parseFloat(prezzo) / 100 * (100 - v.valore_sconto);
                        } else if (v.tipo_sconto === "I") {
                            calcolo_temporaneo_prezzo = parseFloat(prezzo) - parseFloat(v.valore_sconto);
                        } else if (v.tipo_sconto === "E") {
                            calcolo_temporaneo_prezzo = parseFloat(v.valore_sconto);
                        }

                        if (calcolo_temporaneo_prezzo < parseFloat(prezzo_migliore.prezzo)) {
                            prezzo_migliore.prezzo = calcolo_temporaneo_prezzo;
                            prezzo_migliore.tipo_sconto = v.tipo_sconto;
                            prezzo_migliore.valore_sconto = v.valore_sconto;
                        }
                    }

                    //
                });
                prezzo = prezzo_migliore.prezzo;
                var tipo_sconto = "";
                var sconto_presente = "";
                if (prezzo_migliore.tipo_sconto === 'P') {
                    tipo_sconto = /*' Percento'/*/'&#37;';
                    sconto_presente = " Sc. ";
                } else if (prezzo_migliore.tipo_sconto === 'I') {
                    tipo_sconto = '&#8364;'/*'&euro;'*/;
                    sconto_presente = " Sc. ";
                }
                descrizione_articolo += sconto_presente + " " + prezzo_migliore.valore_sconto + " " + tipo_sconto + "";
                prezzo = prezzo.toFixed(2);
            }

        }
            var stato_record = 'ATTIVO';
            if (prechiuso === true) {
                stato_record = "STAMPAFISCALESOSPESO";
            }

            var stampata_sn = 'N';
            if (comanda.compatibile_xml !== true && comanda.pizzeria_asporto !== true && cb2 !== undefined && cb2.stampata_sn !== undefined && cb2.stampata_sn === 'S') {
                console.log("CB", cb2);
                stampata_sn = "S";
                //IN QUESTO CASO ASINCRONAMENTE MI DEVE STAMPARE UN AVVISO DI MODIFICA ARTICOLO
                var data = comanda.funzionidb.data_attuale();
                var builder = new epson.ePOSBuilder();
                builder.addTextFont(builder.FONT_A);
                builder.addTextAlign(builder.ALIGN_CENTER);
                builder.addTextSize(2, 2);
                //OVVERO SE L'ARTICOLO E' UNA VARIANTE

                builder.addTextSize(1, 1);
                builder.addText("-----------------------------------------\n");
                builder.addTextSize(2, 2);
                builder.addText('VARIANTE AGGIUNTA\n');
                builder.addTextSize(1, 1);
                builder.addText("-----------------------------------------\n");
                builder.addTextSize(2, 2);
                builder.addFeedLine(1);
                builder.addText(descrizione_articolo + '\n');
                builder.addFeedLine(1);
                builder.addText('SU: ' + cb2.desc_art + '\n');
                builder.addFeedLine(1);
                builder.addText('TAVOLO:' + comanda.tavolo + '\n');
                if (comanda.tavolo === "BANCO" || comanda.tavolo === "BAR" || comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === "TAKE AWAY") {
                    builder.addText('PARCHEGGIO:' + comanda.parcheggio + '\n');
                }
                builder.addFeedLine(1);
                builder.addTextSize(1, 1);
                builder.addText("-----------------------------------------\n");
                builder.addText('OPERATORE: ' + comanda.operatore + '\n');
                builder.addText(data.replace(/-/gi, '/') + '\n');
                builder.addCut(builder.CUT_FEED);
                var request = builder.toString();
                //CONTENUTO CONTO
                //console.log(request);

                //Create a SOAP envelop
                var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
                //Create an XMLHttpRequest object
                var xhr = new XMLHttpRequest();
                //Set the end point address

                var url = 'http://' + comanda.intelligent_non_fiscale + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                if (cb2.dest_stampa !== undefined && comanda.nome_stampante[cb2.dest_stampa] !== undefined && comanda.nome_stampante[cb2.dest_stampa].ip !== undefined) {
                    url = 'http://' + comanda.nome_stampante[cb2.dest_stampa].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                }
                if (cb2.dest_stampa_2 !== undefined && comanda.nome_stampante[cb2.dest_stampa_2] !== undefined && comanda.nome_stampante[cb2.dest_stampa_2].ip !== undefined) {
                    url = 'http://' + comanda.nome_stampante[cb2.dest_stampa_2].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                }


                //Open an XMLHttpRequest object
                xhr.open('POST', url, true);
                //<Header settings>
                xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
                xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
                xhr.setRequestHeader('SOAPAction', '""');
                // Send print document
                xhr.send(soap);
            }

            //NEL BAR E' TUTTO AUTOMATICAMENTE SERVITO
            /*if(comanda.tavolo==="BAR")
             {
             stampata_sn="s";
             }*/

            var cod_promo = "";

            if (checkEmptyVariable(prodotto.cod_promo)) {
                cod_promo = prodotto.cod_promo;
            }

            if (operazione === '=' && checkEmptyVariable(cb2.cod_promo)) {
                if (cb2.cod_promo.indexOf("V_") === -1) {
                    cod_promo = "V_" + cb2.cod_promo;
                } else {
                    cod_promo = cb2.cod_promo;
                }
            }

            if (cod_promo === "V_") {
                cod_promo = "";
            }

            //TEMPORANEO BROVE DA TESTARE BENE
            //SE E UN ARTICOLO PRINCIPALE DI PROMO FA S
            //SE E' UNA VARIANTE LASCIA COM ERA L ULTIMA PRINCIPALE IN TEORIA
            //SENNO DIVENTA N
            if (comanda.categoria[0] !== "V" && comanda.nome_categoria[comanda.categoria] !== undefined && typeof (comanda.nome_categoria[comanda.categoria]) === 'string' && comanda.nome_categoria[comanda.categoria].substr(0, 2) !== "V.") {
                RAM.prg_art_principale = prog_inser;
            }

            if (cod_promo === "1") {
                RAM.prg_art_promo_principale = prog_inser;
                comanda.varianti_riunite = "S";
            } else if (operazione === "+" || operazione === "-" || operazione === "=" || operazione === "*" || operazione === "x") {

            } else {
                comanda.varianti_riunite = "N";
            }

            if (comanda.pizzeria_asporto === true) {
                if (isNaN(parseInt(prodotto.spazio_forno))) {


                    if (descrizione_articolo[0] === "(") {
                        categoria = "V_666";
                    } else if (descrizione_articolo[0] === "+" || descrizione_articolo[0] === "-" || descrizione_articolo[0] === "=" || descrizione_articolo[0] === "*" || descrizione_articolo[0] === "x") {
                        prodotto.spazio_forno = "0";
                    } else {
                        prodotto.spazio_forno = "1";
                    }

                    if (descrizione_articolo.indexOf("MEZZO METRO") !== -1) {
                        prodotto.spazio_forno = comanda.spazio_mezzo_metro;
                    } else if (descrizione_articolo.indexOf("UN METRO") !== -1) {
                        prodotto.spazio_forno = comanda.spazio_metro;
                    }

                } else if (descrizione_articolo.indexOf("MAXI") !== -1) {
                    prodotto.spazio_forno = comanda.capienza_pizza_maxi;
                }
            }

            comanda.gutshein = false;
            if (cod_articolo === comanda.SET16) {
                if ($("#conto tr").not(".non-contare-riga").length === 0) {
                    prezzo = prompt(lang[252], "0.00");
                    comanda.gutshein = true;
                } else {
                    bootbox.alert(lang[253]);
                    sblocco_tasti = true;
                    return false;
                }
            }

            /* String */
            //var progressivo_segue = "";

            //CAMBIA AD OGNI BATTITURA ARTICOLO
            var d = new Date();
            var h = addZero(d.getHours(), 2);
            var m = addZero(d.getMinutes(), 2);
            var s = addZero(d.getSeconds(), 2);
            comanda.ora_comanda = h + ':' + m + ':' + s;
            var data = comanda.funzionidb.data_attuale().substr(0, 8);
            var ora = comanda.funzionidb.data_attuale().substr(9, 8).replace(/-/gi, '/');

            var costo = "0.00";

            if (checkEmptyVariable(prodotto.costo, "number")) {
                costo = prodotto.costo;
            }

            if (checkEmptyVariable(prodotto.peso_ums, "number")) {
                peso_ums = prodotto.peso_ums;
            }

            if (checkEmptyVariable(prodotto.ultima_portata, "number")) {
                ultima_portata = prodotto.ultima_portata;
            }

            /*già era stato eliminato però */
            /*if (comanda.pizzeria_asporto === true) {
             comanda.progr_gg_uni = 'ASPORTO' + comanda.centro + aggZero(comanda.terminale, 2);
             comanda.tavolo = comanda.progr_gg_uni;
             }*/


            if (descrizione_articolo === "EXTRA") {
                testo_query = 'INSERT INTO comanda (progr_gg_uni,centro,tipologia_fattorino,importo_totale_fiscale,progr_gg_uni,reparto,costo,AGG_GIAC,numero_paganti,TID,operatore_cancellazione,misuratore_riferimento,totale_scontato_ivato,imponibile_rep_1,imponibile_rep_2,imponibile_rep_3,imponibile_rep_4,imponibile_rep_5,imposta_rep_1,imposta_rep_2,imposta_rep_3,imposta_rep_4,imposta_rep_5,numero_servizio,LIB4,data_servizio,data_comanda,ora_comanda,riepilogo,spazio_forno,QTAP,giorno,nome_pc,fiscalizzata_sn,stampata_sn,terminale,perc_iva,id,cod_articolo,ordinamento,peso_ums,ultima_portata,numero_conto,nodo,cat_variante,dest_stampa,dest_stampa_2,portata,desc_art,quantita,prezzo_un,categoria,ntav_comanda,operatore,tipo_record,stato_record,\n\
                                    prog_inser,posizione,BIS,fiscale_sospeso) VALUES ("' + comanda.progr_gg_uni + '","' + comanda.centro + '","","","","' + reparto + '","' + costo + '","' + GIACENZA + '","","' + comanda.TID + '","","' + numero_layout + '","0","0","0","0","0","0","0","0","0","0","0","' + comanda.folder_number + '","' + LIB4 + '","' + comanda.data_servizio + '","' + comanda.data_comanda + '","' + comanda.ora_comanda + '","N","0","0","' + comanda.giorno + '","' + comanda.nome_pc + '","N","' + stampata_sn + '","' + comanda.terminale + '","' + perc_iva + '","' + id + '","' + cod_articolo + '","' + ordinamento + '","' + peso_ums + '","' + ultima_portata + '","' + numero_conto + '","' + nodo + '","' + cat_varianti + '","' + dest_stampa + '","' + dest_stampa_2 + '","' + portata + '","' + descrizione_articolo + '","' + quantita + '","' + prezzo + '",\n\
                                        "' + categoria + '","' + comanda.tavolo + '","' + comanda.operatore + '","CORPO","' + stato_record + '","' + prog_inser + '","' + pos + '","' + BIS + '","STAMPAFISCALESOSPESO");';
            } else {
                //CONROLLO DI SICUREZZA TIZIANA E TATIANA
                /* modifiche del 6 luglio 2020. prima al posto di cod_articolo c'era scritto categoria, ma era sbagliato, 
                 * perchè a causa di una modifica precedente la categoria della variante nell'ibrido è come la categoria padre.
                 * 
                 *  Ora è fatto cosi per prevenire errori di legatura delle varianti */
                if (comanda.compatibile_xml === true && cod_articolo !== undefined && cod_articolo !== null &&
                    cod_articolo[0] === 'V' && descrizione_articolo[0] !== '+' && descrizione_articolo[0] !== '-' && descrizione_articolo[0] !== '(' && descrizione_articolo[0] !== '=' && descrizione_articolo[0] !== 'x' && descrizione_articolo[0] !== '*') {
                    bootbox.alert("Non puoi battere una variante come se fosse un articolo normale.");
                    testo_query = 'SELECT 0;';
                    //Siccome di solito capita mentre si fa qualcos'altro non metto l'avviso per ora
                    //bootbox.alert("La variante &egrave; senza segno. Clicca sull'articolo e modificalo da l&iacute;.")
                } else if (comanda.compatibile_xml === true && cod_articolo !== undefined && cod_articolo !== null &&
                    cod_articolo[0] !== 'V' && cod_articolo !== 'IDCSL_DES' && (descrizione_articolo[0] === '+' || descrizione_articolo[0] === '-' || descrizione_articolo[0] === '(' || descrizione_articolo[0] === '=' || descrizione_articolo[0] === 'x' || descrizione_articolo[0] === '*')) {
                    bootbox.alert("Non puoi legare un articolo che non &egrave; una variante ad un altro articolo!");
                    testo_query = 'SELECT 0;';
                    //Siccome di solito capita mentre si fa qualcos'altro non metto l'avviso per ora
                    //bootbox.alert("La variante &egrave; senza segno. Clicca sull'articolo e modificalo da l&iacute;.")
                } else {

                    let abilita_riepilogo = "N";
                    let prezzo_maxi_prima = "";
                    let prezzo_varianti_maxi = "";
                    let prezzo_varianti_aggiuntive = "";
                    let spazio_forno = "0";
                    let prezzo_maxi = "";

                    if (checkEmptyVariable(prodotto.abilita_riepilogo)) {
                        abilita_riepilogo = prodotto.abilita_riepilogo;
                    }
                    if (checkEmptyVariable(prodotto.prezzo_maxi_prima, "number")) {
                        prezzo_maxi_prima = prodotto.prezzo_maxi_prima;
                    }
                    if (checkEmptyVariable(prodotto.prezzo_varianti_maxi, "number")) {
                        prezzo_varianti_maxi = prodotto.prezzo_varianti_maxi;
                    }
                    if (checkEmptyVariable(prodotto.prezzo_varianti_aggiuntive, "number")) {
                        prezzo_varianti_aggiuntive = prodotto.prezzo_varianti_aggiuntive;
                    }
                    if (checkEmptyVariable(prodotto.spazio_forno, "number")) {
                        spazio_forno = prodotto.spazio_forno;
                    }
                    if (checkEmptyVariable(prodotto.prezzo_maxi, "number")) {
                        prezzo_maxi = prodotto.prezzo_maxi;
                    }

                    var dati_comanda_precedenti = alasql("select id_pony from comanda where id='" + id + "' limit 1;");

                    let id_pony = "";
                    if (dati_comanda_precedenti.length > 0) {
                        id_pony = dati_comanda_precedenti[0].id_pony;
                    }

                    /* if (comanda.pizzeria_asporto === true) {
                     comanda.progr_gg_uni = 'ASPORTO' + comanda.centro + aggZero(comanda.terminale, 2) + id;
                     comanda.tavolo = comanda.progr_gg_uni;
                     }*/

                    //INSERISCE I CAMPI NECESSARI NELLA COMANDA
                    testo_query = 'INSERT INTO comanda (id_pony,progr_gg_uni,centro,tipologia_fattorino,importo_totale_fiscale,progr_gg_uni,reparto,costo,AGG_GIAC,numero_paganti,TID,nome_comanda,operatore_cancellazione,misuratore_riferimento,gruppo,totale_scontato_ivato,imponibile_rep_1,imponibile_rep_2,imponibile_rep_3,imponibile_rep_4,imponibile_rep_5,imposta_rep_1,imposta_rep_2,imposta_rep_3,imposta_rep_4,imposta_rep_5,numero_servizio,LIB4,data_servizio,data_comanda,ora_comanda,riepilogo,prezzo_maxi_prima,prezzo_varianti_maxi,prezzo_varianti_aggiuntive,tasto_segue,spazio_forno,cod_promo,QTAP,giorno,nome_pc,stampata_sn,terminale,perc_iva,id,cod_articolo,ordinamento,peso_ums,ultima_portata,numero_conto,nodo,cat_variante,dest_stampa,dest_stampa_2,portata,desc_art,quantita,prezzo_un,categoria,ntav_comanda,operatore,tipo_record,stato_record,prog_inser,posizione,BIS,prezzo_maxi) VALUES ("' + id_pony + '","' + comanda.progr_gg_uni + '","' + comanda.centro + '","","0.00","","' + reparto + '","' + costo + '","' + GIACENZA + '","","' + comanda.TID + '","' + comanda.parcheggio + '","","' + numero_layout + '","' + id_gruppo_statistico + '","0","0","0","0","0","0","0","0","0","0","0","' + comanda.folder_number + '","' + LIB4 + '","' + comanda.data_servizio + '","' + comanda.data_comanda + '","' + comanda.ora_comanda + '","' + abilita_riepilogo + '","' + prezzo_maxi_prima + '","' + prezzo_varianti_maxi + '","' + prezzo_varianti_aggiuntive + '","' + progressivo_segue + '","' + spazio_forno + '","' + cod_promo + '","0","' + comanda.giorno + '","' + comanda.nome_pc + '","' + stampata_sn + '","' + comanda.terminale + '","' + perc_iva + '","' + id + '","' + cod_articolo + '","' + ordinamento + '","' + peso_ums + '","' + ultima_portata + '","' + numero_conto + '","' + nodo + '","' + cat_varianti + '","' + dest_stampa + '","' + dest_stampa_2 + '","' + portata + '","' + descrizione_articolo + '","' + quantita + '","' + prezzo + '","' + categoria + '","' + comanda.tavolo + '","' + comanda.operatore + '","CORPO","' + stato_record + '","' + prog_inser + '","' + pos + '","' + BIS + '","' + prezzo_maxi + '");';
                    if (comanda.compatibile_xml === true && comanda.tipo_ricerca_articolo === "NUMERICA") {
                        comanda.ultima_cat_variante = cat_varianti;
                    }

                }
            }

            var time2 = new Date().getTime();
            console.log("TIME_SB2", time2 - time1);
            comanda.stampata_n = true;
            $("#tab_sx a.pizza_maxi").removeClass("cat_accesa");
            //$("#tab_sx a.pizza_due_gusti").removeClass("cat_accesa");

            /* INIZIO PIZZA MAXI */
            bool_pizza_maxi = false;
            comanda.specifica_maxi = '';
            /* FINE PIZZA MAXI */

            $('#qta_menu_fisso').hide();
            salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + aggZero(new Date().getMilliseconds(), 3) + " # AGGIUNGI ARTICOLO # QUERY: " + testo_query.replace(/[\n\r]+/g, '') + " # ");
            //Resetto l'ulteriore specifica
            $('.tasto_specifica_sotto').css('background-color', '');



            /* tolta sta parte il 23/06/2021 altrimenti se ad esempio batti GUSTO ti viene fuori solo alla prima variante che batti */
            /* comanda.specifica_tasti_sotto = ''; */


            comanda.prezzo_tasti_sotto = '0.00';
            console.log("QUERY AGGIUNTA ARTICOLO", testo_query);
            comanda.sock.send({ tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
            //16-12-2016 ALTRIMENTI FA CASINO CON FAST ORDER
            //14-09-2021 TOLTA PERCHE' SENNO PUO METTERE LO STESSO ARTICOLO 2 VOLTE NEL DATABASE
            /*if (typeof comanda.terminale === 'string' && comanda.terminale.substr(0, 6) !== 'TAVOLO') {
                comanda.sincro.query_cassa();
            }*/

            //$('#popup_richiesta_aggiunta_fastorder').modal('show');

            log_velocita(false, "secondo blocco pre-inserimento");
            /*if (ultimo_elemento !== cod_articolo) {
             trainingSet = [];
             trainingSet.push({
             input: [ultimo_elemento / NORMALIZER],
             output: [parseInt(cod_articolo) / NORMALIZER]
             });
             
             network.train(trainingSet, TRAIN_OPTIONS);
             ultimo_elemento = cod_articolo;
             }
             
             $(".elenco_prodotti li:visible").css("filter", "initial");
             $(".elenco_prodotti li[value='" + (network.activate([cod_articolo / NORMALIZER])[0]*NORMALIZER).toFixed(0) + "']:visible").css("filter", "brightness(3)");*/

            comanda.sincro.query(testo_query, function () {





                if (comanda.mantieni_lettera !== "S") {
                    $("#atp_testo").val("");
                }
                console.log("BODY POINTER EVENTS RESET");
                $('body').css('pointer-events', '');
                console.log("BODY POINTER EVENTS FREE C5");
                if (comanda.filtro_lettera_bloccato !== "S" && $('div.filtro:visible').length === 1) {
                    if (comanda.variante_aperta !== "S") {
                        if (comanda.mantieni_lettera !== "S") {
                            mod_categoria(comanda.categoria);
                        }
                    }
                }

                if (/*(*/pizza_due_gusti === true /*|| boolean_schiacciata === true)*/ && comanda.variante_aperta != "S") {
                    comanda.ultima_opz_variante = '=';
                    operazione = '=';
                    comanda.specifica_tasti_sotto = '';
                }

                log_velocita(false, "secondo blocco post-inserimento");
                //IL PROBLEMA E' QUI DELLO SDOPPIAMENTO
                //PER RISOLVERLO BASTA RIAVVIARE IL SOCKET (????)
                if (prechiuso !== true) {
                    cb(true);
                }



                console.trace();
                console.log("SECONDO BLOCCO");
                //sblocco_tasti = true;


                if (prodotto.automodifica_prezzo !== undefined && prodotto.automodifica_prezzo === "S") {
                    comanda.automodifica = true;
                    console.log("automodifica_prezzo attiva");
                    popup_mod_art(event, prog_inser, nodo);
                    $("#popup_mod_art [name='prezzo_1']").trigger("touchend");
                }

            });
        });
    });
}


var fine_conto_attivo = function (callBACCO, salta_colore) {

    log_velocita(false, "inizio fine conto attivo");

    var time = new Date().getTime();
    console.log("TIME_FCA", 0);

    $('#intestazioni_conto_separato_grande').show();
    //console.log(indice, array_numero_righe, indice === array_numero_righe);
    $('#intestazioni_conto_separato_2_grande').show();
    //console.log("TOTALE REINSERITO");

    calcola_totale();
    // if (comanda.mobile === true || comanda.pizzeria_asporto === true) {

    var time2 = new Date().getTime();
    console.log("TIME_FCA2", time2 - time);

    if (comanda.fastorder !== true) {

        ultime_battiture(salta_colore);
    }

    var time = new Date().getTime();
    console.log("TIME_FCA", 0);

    if (comanda.mobile === true) {
        if ($('#prodotti_conto_separato_grande tr').length === 0) {
            $('.totale_conto_separato_grande_barra').hide();
        }
        if ($('#prodotti_conto_separato_2_grande tr').length === 0) {
            $('.totale_conto_separato_2_grande_barra').hide();
        }
        if ($('#conto tr').length === 0) {
            $('#tasti_incasso_nascosti_palmare').hide();
            $('#tab_dx').removeClass('tasti_incasso_visibili');
            $('#tab_dx').css('height', '124vh');
        }

    }

    //Sblocca i tasti di incasso e conto
    $('.tasto_konto').css('pointer-events', '');
    $('.tasto_quittung').css('pointer-events', '');
    $('.tasto_rechnung').css('pointer-events', '');
    $('.tasto_bargeld').css('pointer-events', '');
    sblocco_tasti = true;

    var time2 = new Date().getTime();
    console.log("TIME_FCA2", time2 - time);

    log_velocita(false, "fine fine conto attivo");

    if (typeof (callBACCO) === "function") {
        console.log("CALLBACK CONTO ATTIVO");

        callBACCO(true);
    }

}
    ;
function riga_conto_ricostruito(prezzo_maxi, tasto_segue, ord, cod_promo, fiscalizzata_sn, ora_, rag_soc_cliente, perc_iva, reparto,
    nome_comanda, stampata_sn, numero_conto, dest_stampa, portata, categoria, prog_inser, nodo, desc_art, prezzo_un,
    prezzo_varianti_aggiuntive, prezzo_varianti_maxi, prezzo_maxi_prima, quantita, contiene_variante, tipo_impasto, numero_paganti, BIS, QTA) {

    this.prezzo_maxi = prezzo_maxi;
    this.tasto_segue = tasto_segue;
    this.ord = ord;
    this.cod_promo = cod_promo;
    this.fiscalizzata_sn = fiscalizzata_sn;
    this.ora_ = ora_;
    this.rag_soc_cliente = rag_soc_cliente;
    this.perc_iva = perc_iva;
    this.reparto = reparto;
    this.nome_comanda = nome_comanda;
    this.stampata_sn = stampata_sn;
    this.numero_conto = numero_conto;
    this.dest_stampa = dest_stampa;
    this.portata = portata;
    this.categoria = categoria;
    this.prog_inser = prog_inser;
    this.nodo = nodo;
    this.desc_art = desc_art;
    this.prezzo_un = prezzo_un;
    this.prezzo_varianti_aggiuntive = prezzo_varianti_aggiuntive;
    this.prezzo_varianti_maxi = prezzo_varianti_maxi;
    this.prezzo_maxi_prima = prezzo_maxi_prima;
    this.quantita = quantita;
    this.contiene_variante = contiene_variante;
    this.tipo_impasto = tipo_impasto;
    this.numero_paganti = numero_paganti;
    this.BIS = BIS;
    this.QTA = QTA;
}

function ultime_battiture(salta_colore) {

    log_velocita(false, "inizio ultime battiture");

    if (salta_colore === true) {
        $('.table.ultime_battiture tr').css('background-color', '');
        $('.table.ultime_battiture tr#art_' + comanda.ultimo_progressivo_articolo).css('background-color', '#3498db');
    } else {
        var time = new Date().getTime();
        console.log("TIME_UB", 0);
        //var testo_query = "SELECT nodo,prog_inser FROM oggetto_conto WHERE substr(nodo, 1,3)=(select substr(nodo,1,3) as nodo from oggetto_conto where   desc_art NOT LIKE 'RECORD TESTA%'   and fiscale_sospeso!='STAMPAFISCALESOSPESO' order by cast(prog_inser as int) desc limit 1)  and   fiscale_sospeso!='STAMPAFISCALESOSPESO' and desc_art NOT LIKE 'RECORD TESTA%' ORDER BY nodo asc limit 1;";
        var testo_query = "SELECT nodo,prog_inser FROM oggetto_conto WHERE substr(nodo, 1,3)=(select substr(nodo,1,3) as nodo from oggetto_conto where   desc_art NOT LIKE 'RECORD TESTA%'  order by cast(prog_inser as int) desc limit 1)   and desc_art NOT LIKE 'RECORD TESTA%' ORDER BY nodo asc;";

        alasql(testo_query, function (result) {
            var time2 = new Date().getTime();
            console.log("TIME_UB2", time2 - time);

            console.log("DA COLORARE 1", result);
            if (result !== undefined && result[0] !== undefined && result[0].nodo !== undefined && result[0].prog_inser !== undefined) {

                result.forEach(function (element) {
                    console.log("DA COLORARE 2", element.prog_inser, element.nodo);
                    if (comanda.mobile === true || comanda.pizzeria_asporto === true) {
                        $('.ultime_battiture tr#art_' + element.prog_inser).css('background-color', '#3498db');
                    } else {
                        $('#conto tr#art_' + element.prog_inser).css('background-color', '#3498db');
                    }
                });
                if (comanda.mobile === true || comanda.pizzeria_asporto === true) {
                    var altezza_riga = $('.ultime_battiture tr:first-child()').height();
                } else {
                    var altezza_riga = $('#conto tr:first-child()').height();
                }

                var i = 0;
                if (comanda.mobile === true || comanda.pizzeria_asporto === true) {
                    $('.ultime_battiture tr:visible').each(function (key, element) {

                        if ($(element).attr('id') === 'art_' + result[result.length - 1].prog_inser) {

                            var valore_scroll = altezza_riga * i;
                            $('.ultime_battiture').closest('div')[0].scrollTop = valore_scroll;
                            return false;
                        }
                        if ($(element).height() !== 0) {
                            i++;
                        }
                    });
                } else {
                    $('#conto tr:visible').each(function (key, element) {

                        if ($(element).attr('id') === 'art_' + result[0].prog_inser) {

                            var valore_scroll = altezza_riga * i;
                            $('#conto').closest('div')[0].scrollTop = valore_scroll;
                            return false;
                        }
                        if ($(element).height() !== 0) {
                            i++;
                        }
                    });
                }
            }
            beep();
            $('#prodotti', 'body #creazione_comanda').css('opacity', '');
            $('#prodotti', 'body #creazione_comanda').css('pointer-events', '');
            $('#tasti_fiscali_da_nascondere', 'body #creazione_comanda').css('pointer-events', '');
            $('#tasti_fiscali_da_nascondere', 'body #creazione_comanda').css('opacity', '');

            log_velocita(false, "fine ultime battiture");
        });
    }


}


function dati_comanda(varianti_unite, CallBack, id_scontrino) {

    let testo_query = "";

    if (varianti_unite === true) {
        let field_prezzo_un = "prezzo_vero";
        if (comanda.pizzeria_asporto === true) {
            field_prezzo_un += " as prezzo_un";
        }

        if (checkEmptyVariable(id_scontrino) === false) {
            testo_query = "select cod_articolo,desc_art,quantita," + field_prezzo_un + ",nodo,prog_inser,contiene_variante,categoria,dest_stampa,dest_stampa_2,portata,(select desc_art from comanda as c2 where stato_record='ATTIVO' and ntav_comanda='" + comanda.tavolo + "'  and c2.nodo=substr(c1.nodo,1,3)) as articolo_main from comanda as c1 where stato_record='ATTIVO' and ntav_comanda='" + comanda.tavolo + "' order by articolo_main,nodo asc;";
        } else {
            testo_query = "select cod_articolo,desc_art,quantita," + field_prezzo_un + ",nodo,prog_inser,contiene_variante,categoria,dest_stampa,dest_stampa_2,portata,(select desc_art from comanda as c2 where id='" + id_scontrino + "'  and c2.nodo=substr(c1.nodo,1,3)) as articolo_main from comanda as c1 where id='" + id_scontrino + "' order by articolo_main,nodo asc;";
        }

        comanda.sincro.query(testo_query, function (risultato) {

            var conto = new Array();
            var buffer_varianti = new Array();
            risultato.forEach(function (obj) {


                var posizione = -1; //INDEX OF FOUND ELEMENT IN CONTO


                //Solo per gli articoli principali
                if (obj.desc_art[0] !== '+' && obj.desc_art[0] !== '-' && obj.desc_art[0] !== '(' && obj.desc_art[0] !== '*' && obj.desc_art[0] !== '=') {

                    //Iteratore per vedere se ci sono articoli uguali
                    conto.some(function (iterator, index, _ary) {

                        console.log("CONFRONTO ARTICOLI UGUALI", iterator.desc_art, obj.desc_art);
                        if (iterator.desc_art === obj.desc_art) {
                            posizione = index;
                            return true;
                        }

                    });
                    console.log("POSIZIONE TROVATA: ", posizione);
                    //FINE ITERATORE

                    console.log(posizione);
                    if (posizione !== -1) {
                        conto[posizione].quantita = (parseInt(conto[posizione].quantita) + parseInt(obj.quantita)).toString();
                    } else {
                        conto.push(obj);
                    }
                }
                //Solo per le varianti
                else {


                    //Se la variante appartiene allo stesso articolo di quella di prima la unisce nella stessa riga
                    //NB Quella di prima però dev'essere una variante
                    if (conto[conto.length - 1] !== undefined && conto[conto.length - 1].nodo !== undefined && conto[conto.length - 1].nodo.length > 3 && obj.nodo.substr(0, 3) === conto[conto.length - 1].nodo.substr(0, 3)) {
                        conto[conto.length - 1].desc_art += ' ' + obj.desc_art;
                        conto[conto.length - 1].prezzo_un = parseFloat(conto[conto.length - 1].prezzo_un) + parseFloat(obj.prezzo_un);
                    } else {
                        conto.push(obj);
                    }
                }

            });
            var conto_varianti_perfezionate = new Array();
            conto.forEach(function (obj) {

                console.log(obj);
                var posizione = -1; //INDEX OF FOUND ELEMENT IN CONTO

                //Solo per gli articoli principali
                if (obj.desc_art[0] !== '+' && obj.desc_art[0] !== '-' && obj.desc_art[0] !== '(' && obj.desc_art[0] !== '*' && obj.desc_art[0] !== '=') {

                    //Iteratore per vedere se ci sono articoli uguali
                    conto_varianti_perfezionate.some(function (iterator, index, _ary) {

                        console.log(iterator.desc_art, obj.desc_art);
                        if (iterator.desc_art === obj.desc_art) {

                            posizione = index;
                            return true;
                        }

                    });
                    console.log("POSIZIONE TROVATA: ", posizione);
                    //FINE ITERATORE

                    console.log(posizione);
                    if (posizione !== -1) {
                        conto_varianti_perfezionate[posizione].quantita = (parseInt(conto_varianti_perfezionate[posizione].quantita) + parseInt(obj.quantita)).toString();
                    } else {
                        conto_varianti_perfezionate.push(obj);
                    }
                } else {
                    //Se la variante ha lo stesso nome dell' articolo precedente aggiunge solo la quantita

                    var quantita_aumentata = false;

                    conto_varianti_perfezionate.forEach(function (a, b) {

                        if (a.desc_art === obj.desc_art) {
                            conto_varianti_perfezionate[b].quantita = (parseInt(conto_varianti_perfezionate[b].quantita) + parseInt(obj.quantita)).toString();

                            quantita_aumentata = true;
                        }

                    });

                    if (quantita_aumentata === false) {
                        conto_varianti_perfezionate.push(obj);
                    }
                }

            });
            console.log(conto_varianti_perfezionate);
            if (typeof CallBack === "function") {
                CallBack(conto_varianti_perfezionate);
            }

        });
    } else {

        alasql("DELETE FROM oggetto_comanda");
        let field_prezzo_un = "prezzo_vero";
        if (comanda.pizzeria_asporto === true) {
            field_prezzo_un += " as prezzo_un";
        }

        if (checkEmptyVariable(id_scontrino) === false) {

            testo_query = "select cod_articolo,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,dest_stampa_2,portata,categoria,prog_inser,nodo,desc_art," + field_prezzo_un + ",quantita,contiene_variante from comanda where ntav_comanda='" + comanda.tavolo + "'  and posizione='CONTO' and stato_record='ATTIVO';";
        } else {
            testo_query = "select cod_articolo,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,dest_stampa_2,portata,categoria,prog_inser,nodo,desc_art," + field_prezzo_un + ",quantita,contiene_variante from comanda where id='" + id_scontrino + "' and posizione='CONTO';";

        }

        var r1 = alasql(testo_query);
        var ord = "";
        var raggruppamento = new Object();
        r1.forEach(function (e) {
            if (e.contiene_variante === "1") {
                ord = e.desc_art.substr(0, 3) + "M" + e.nodo;
            } else if (e.nodo.length === 7) {

                if (checkEmptyVariable(id_scontrino) === false) {
                    testo_query = "select substr(desc_art,1,3) as desc_art from comanda where nodo='" + e.nodo.substr(0, 3) + "' and stato_record='ATTIVO'  and ntav_comanda='" + comanda.tavolo + "' limit 1;"
                } else {
                    testo_query = "select substr(desc_art,1,3) as desc_art from comanda where nodo='" + e.nodo.substr(0, 3) + "' and  id='" + id_scontrino + "' limit 1;"
                }

                var r2 = alasql(testo_query);
                ord = r2[0].desc_art + "M" + e.nodo;
            } else if (e.contiene_variante !== "1") {
                ord = e.desc_art.substr(0, 3) + "M" + e.nodo;
            }


            alasql.tables.oggetto_comanda.data.push({ 'ord': ord, 'cod_articolo': e.cod_articolo, 'perc_iva': e.perc_iva, 'nome_comanda': e.nome_comanda, 'stampata_sn': e.stampata_sn, 'numero_conto': e.numero_conto, 'dest_stampa': e.dest_stampa, 'dest_stampa_2': e.dest_stampa_2, 'portata': e.portata, 'categoria': e.categoria, 'prog_inser': e.prog_inser, 'nodo': e.nodo, 'desc_art': e.desc_art, 'prezzo_un': e.prezzo_un, 'quantita': e.quantita, 'contiene_variante': e.contiene_variante });
        });

        alasql("select * from oggetto_comanda where contiene_variante!='1' and  length(nodo)=3;").forEach(function (e) {
            if (raggruppamento[e.desc_art] === undefined) {
                raggruppamento[e.desc_art] = new Object();
            }

            if (raggruppamento[e.desc_art][e.nome_comanda] === undefined) {
                e.quantita = parseInt(e.quantita);
                raggruppamento[e.desc_art][e.nome_comanda] = e;
            } else {
                raggruppamento[e.desc_art][e.nome_comanda].quantita += parseInt(e.quantita);
            }
        });

        alasql("delete from oggetto_comanda where contiene_variante!='1' and  length(nodo)=3;")

        //FOR RAGGRUPPAMENTO

        for (var desc_art in raggruppamento) {
            for (var nome_comanda in raggruppamento[desc_art]) {
                alasql.tables.oggetto_comanda.data.push(raggruppamento[desc_art][nome_comanda]);
            }
        }

        raggruppamento = {};
        r1 = {};

        raggruppamento = null;
        r1 = null;
        //delete raggruppamento;
        //delete r1;

        var res = alasql("select * from oggetto_comanda order by nome_comanda DESC, ord ASC;");

        //QUESTA E' LA COMANDA DIVISA PER NODI
        var comanda_riunita_diviggiano = new Object();

        res.forEach(function (el, key) {
            if (comanda_riunita_diviggiano[el.ord] === undefined) {
                comanda_riunita_diviggiano[el.ord] = new Array();
            }

            comanda_riunita_diviggiano[el.ord].push(el);
        });

        //QUESTA E' LA COMANDA RIUNITA IN UN OGGETTO
        var comanda_finale = new Object();

        for (var ord in comanda_riunita_diviggiano) {

            //Slice serve a copiare l'array senza puntarci dentro
            var array_vecchio = JSON.parse(JSON.stringify(comanda_riunita_diviggiano[ord]));

            array_vecchio.forEach(function (el, key, arr) {
                arr[key].ord = "";
                arr[key].prog_inser = "";
                arr[key].nodo = "";
                arr[key].quantita = "";
            });


            var bool = false;
            for (var ord2 in comanda_finale) {

                //Slice serve a copiare l'array senza puntarci dentro
                var array_comanda = JSON.parse(JSON.stringify(comanda_finale[ord2]));

                array_comanda.forEach(function (el, key, arr) {
                    arr[key].ord = "";
                    arr[key].prog_inser = "";
                    arr[key].nodo = "";
                    arr[key].quantita = "";
                });

                //Bisogna confrontare l'array diviggiano con tutti i comanda

                //Se in comanda non ce n'è uno con lunghezza uguale e stessi elementi lo si aggiunge a comanda stesso


                //condizione 1 : controllo che la lunghezza dell'array sia la stessa in entrambi gli oggetti
                if (array_vecchio.length === array_comanda.length) {

                    if (JSON.stringify(array_vecchio.sort(mySorter)) == JSON.stringify(array_comanda.sort(mySorter))) {

                        if (comanda_finale[ord2][0].contiene_variante !== "1" && comanda_finale[ord2][0].desc_art[0] !== "-" && comanda_finale[ord2][0].desc_art[0] !== "+" && comanda_finale[ord2][0].desc_art[0] !== "x" && comanda_finale[ord2][0].desc_art[0] !== "*" && comanda_finale[ord2][0].desc_art[0] !== "(" && comanda_finale[ord2][0].desc_art[0] !== "=") {

                            comanda_finale[ord2][0].quantita = parseInt(comanda_finale[ord2][0].quantita) + 1;

                            bool = true;

                        }

                    }

                }

            }

            if (bool !== true) {

                comanda_finale[ord] = comanda_riunita_diviggiano[ord];

            }

        }

        //QUESTO E' L'OGGETTO FINALE TRASFORMATO IN ARRAY
        var oggetto_finale_comanda = new Array();
        for (var key in comanda_finale) {
            comanda_finale[key].forEach(function (el, key) {
                oggetto_finale_comanda.push(el);
            });
        }


        console.log("COMANDA RIUNITA DIVIGGIANO", res, comanda_riunita_diviggiano, comanda_finale, oggetto_finale_comanda);

        if (typeof CallBack === "function") {
            CallBack(oggetto_finale_comanda);
        }

    }
}


function calcolo_prezzo_prodotto_peso_postumo() {
    var peso = $('.modifica_postuma_peso [name="peso_etti"]').val().replace(",", ".");
    var prezzo = $(".modifica_postuma_peso [name='prezzo_um']").val();

    var prezzo_nuovo = parseFloat(prezzo) * parseFloat(peso);
    $(".modifica_postuma_peso [name='prezzo_1']").val(prezzo_nuovo.toFixed(2));

    var query = "update comanda set desc_art='" + comanda.desc_art_partenza + " " + peso + " hg.', prezzo_un='" + parseFloat(prezzo_nuovo).toFixed(2) + "' where prog_inser='" + comanda.ultimo_progressivo_articolo + "' and stato_record='ATTIVO';";
    comanda.sock.send({ tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: query, terminale: comanda.terminale, ip: comanda.ip_address });
    alasql(query);

    comanda.funzionidb.conto_attivo();

    $("#popup_sposta_articolo").modal("hide");


}

function calcolo_prezzo_prodotto() {
    var peso = $('#popup_prezzo_prodotto [name="peso_etti"]').val().replace(",", ".");
    var prezzo = $("#popup_prezzo_prodotto [name='prezzo_um']").val();

    var prezzo_nuovo = parseFloat(prezzo) * parseFloat(peso);

    $("#popup_prezzo_prodotto [name='prezzo_1']").val(prezzo_nuovo.toFixed(2));
}


function apri_popup_prezzo_prodotto(cb) {
    $('#popup_prezzo_prodotto [name="quantita"]').val("1");
    $('#popup_prezzo_prodotto [name="peso_etti"]').val("1");

    $('#popup_prezzo_prodotto').modal('show');

    $("#popup_prezzo_prodotto [name='peso_etti']").trigger('touchend');

    $('#conferma_popup_prezzo_prodotto').on('click', function () {
        $('#popup_prezzo_prodotto').modal('hide');
        $('#conferma_popup_prezzo_prodotto').off();
        cb(true);
    });
}

function aggiungi_quantita_box() {


    var qt = $('#popup_prezzo_prodotto [name="quantita"]').val();
    qt = parseInt(qt);
    var nuova_qt = qt + 1;
    $('#popup_prezzo_prodotto [name="quantita"]').val(nuova_qt);

}

function diminuisci_quantita_box() {


    var qt = $('#popup_prezzo_prodotto [name="quantita"]').val();
    qt = parseInt(qt);
    if (qt > 0) {
        var nuova_qt = qt - 1;
        $('#popup_prezzo_prodotto [name="quantita"]').val(nuova_qt);

    }
}

function doppio_mod_art() {

    var prezzo = $('#popup_mod_art [name="prezzo_1"]').val();
    var descrizione = $('#popup_mod_art [name="desc_art"]').val();

    if (descrizione.indexOf(" - DOPPIO/A") !== -1) {
        $('#popup_mod_art [name="desc_art"]').val(descrizione.split(" - DOPPIO/A")[0]);
        $('#popup_mod_art [name="prezzo_1"]').val((parseFloat(prezzo) / 2).toFixed(2));
    } else {
        $('#popup_mod_art [name="desc_art"]').val(descrizione + " - DOPPIO/A");
        $('#popup_mod_art [name="prezzo_1"]').val((parseFloat(prezzo) * 2).toFixed(2));
    }



}

function calcola_giorni_mese(mese, anno) {

    //Se il mese ï¿½ Febbraio bisogna vedere se l'anno ï¿½ bisestile oppure no

    if (mese === 1) {
        //Se l'anno ï¿½ bisestile febbraio ï¿½ di 29 giorni, altrimenti 28

        //Per calcolare l'anno bisestile

        if (anno % 4 === 0) {
            if (anno % 100 === 0) {

                if (anno % 400 === 0) {
                    return 29;
                    //ANNO BISESTILE   
                } else {
                    return 28;
                    //ANNO NON BISESTILE
                }

            } else {
                return 29;
                //ANNO BISESTILE
            }

        } else {
            return 28;
            //ANNO NON BISESTILE
        }

    }
    //Se il mese+1 ï¿½ pari ï¿½ sempre di 31 giorni
    else if (mese === 0 || mese === 2 || mese === 7) {
        return 31;
    }
    //Se il mese ï¿½ un altro ha 30 giorni
    else {
        return 30;
    }

}

function stampa_movimenti_A4() {

    $(".print").html("");

    var data = new Date();

    var giorno = data.format("dd/mm/yyyy");
    var orario = data.format("HH:MM");

    var dataUrl_1 = document.getElementById('canvas').toDataURL();
    var dataUrl_2 = document.getElementById('canvas2').toDataURL();

    var dataUrl = document.getElementById('canvas3').toDataURL();
    var dataUrl2 = document.getElementById('canvas4').toDataURL();

    var windowContent = "";



    //------------------------------------------------------------------//

    windowContent += '<div style="margin-left:5%;width:90%">';

    windowContent += '<div class="row" style="font-size:1.5vh;border:1px solid black;overflow:auto;padding:5px;">';
    windowContent += '<div style="float:left;width:50%; text-align:left;">';
    windowContent += '<strong>' + comanda.profilo_aziendale.ragione_sociale + '</strong><br>Stampa effettuata il: <strong>' + giorno + '</strong>';
    windowContent += '</div>';
    windowContent += '<div style="float:left;width:50%; text-align:right;">';
    windowContent += '<strong>' + comanda.locale + '</strong><br>Alle ore: <strong>' + orario + '</strong>';
    windowContent += '</div>';
    windowContent += '</div>';

    windowContent += '<div class="row">';
    windowContent += '<div style="width:100%;">';
    windowContent += '<div style="font-size:1.8vh;text-decoration: underline;text-align:center;padding-top:0.25cm;">STAMPA MOVIMENTI</div>';
    windowContent += '</div>';
    windowContent += '</div>';

    var anno_corrente = $('#gm_anno_corrente').html();
    var mese_corrente = $('#gm_mese_corrente').html();


    windowContent += '<div class="row">';
    windowContent += '<div>';
    windowContent += '<div style="font-weight:bold;font-size:1.8vh;text-align:center;padding-top:0.25cm;">Anno Corrente: ' + anno_corrente + '</div>';
    windowContent += '</div>';
    windowContent += '</div>';




    windowContent += '<div class="row">';
    windowContent += '<div style="width:100%;">';
    windowContent += '<img src="' + dataUrl_1 + '" style="filter: invert(100%);">';
    windowContent += '</div>';
    windowContent += '</div>';


    windowContent += tabella_anno_corrente_movimenti;



    windowContent += '<div class="row">';
    windowContent += '<div>';
    windowContent += '<div style="font-weight:bold;font-size:1.8vh;text-align:center;padding-top:0.4cm;">Mese Corrente: ' + mese_corrente + '</div>';
    windowContent += '</div>';
    windowContent += '</div>';





    windowContent += '<div class="row">';
    windowContent += '<div style="width:100%;">';
    windowContent += '<img src="' + dataUrl_2 + '" style="filter: invert(100%);">';
    windowContent += '</div>';
    windowContent += '</div>';


    windowContent += tabella_mese_corrente_movimenti;



    windowContent += '</div>';

    //--------------------------------------------------------------------------------------

    windowContent += '<div style="margin-top:3cm;margin-left:5%;width:90%">';

    /*windowContent += '<div class="row" style="font-size:1.5vh;border:1px solid black;overflow:auto;padding:5px;">';
     windowContent += '<div style="float:left;width:50%; text-align:left;">';
     windowContent += '<strong>' + comanda.profilo_aziendale.ragione_sociale + '</strong><br>Stampa effettuata il: <strong>' + giorno + '</strong>';
     windowContent += '</div>';
     windowContent += '<div style="float:left;width:50%; text-align:right;">';
     windowContent += '<strong>' + comanda.locale + '</strong><br>Alle ore: <strong>' + orario + '</strong>';
     windowContent += '</div>';
     windowContent += '</div>';
     
     windowContent += '<div class="row">';
     windowContent += '<div style="width:100%;">';
     windowContent += '<div style="font-size:1.8vh;text-decoration: underline;text-align:center;padding-top:1cm;">STAMPA MOVIMENTI</div>';
     windowContent += '</div>';
     windowContent += '</div>';*/

    var giorno_1 = $("[name='giorno_1']").val();
    if (giorno_1 === "Giorno") {
        giorno_1 = "";
    }
    var mese_1 = $("[name='mese_1'] option:selected").text();

    if (mese_1 === "Mese") {
        mese_1 = "";
    }
    var anno_1 = $("[name='anno_1'] option:selected").text();
    if (anno_1 === "Anno") {
        anno_1 = "";
    }

    var data_1 = giorno_1 + " " + mese_1 + " " + anno_1;

    windowContent += '<div class="row">';
    windowContent += '<div>';
    windowContent += '<div style="font-size:1.8vh;text-align:center;">' + data_1 + '</div>';
    windowContent += '</div>';
    windowContent += '</div>';



    windowContent += '<div class="row">';
    windowContent += '<div style="width:100%;">';
    windowContent += '<img src="' + dataUrl + '" style="filter: invert(100%);">';
    windowContent += '</div>';
    windowContent += '</div>';

    if (tabella_confronto_movimenti !== undefined && tabella_confronto_movimenti[1] !== undefined) {
        windowContent += tabella_confronto_movimenti[1];
    }

    var giorno_2 = $("[name='giorno_2']").val();
    if (giorno_2 === "Giorno") {
        giorno_2 = "";
    }
    var mese_2 = $("[name='mese_2'] option:selected").text();
    if (mese_2 === "Mese") {
        mese_2 = "";
    }
    var anno_2 = $("[name='anno_2'] option:selected").text();
    if (anno_2 === "Anno") {
        anno_2 = "";
    }

    var data_2 = giorno_2 + " " + mese_2 + " " + anno_2;


    windowContent += '<div class="row">';
    windowContent += '<div>';
    windowContent += '<div style="font-size:1.8vh;text-align:center;padding-top:1cm;">' + data_2 + '</div>';
    windowContent += '</div>';
    windowContent += '</div>';



    windowContent += '<div class="row">';
    windowContent += '<div style="width:100%;">';
    windowContent += '<img src="' + dataUrl2 + '" style="filter: invert(100%);">';
    windowContent += '</div>';
    windowContent += '</div>';

    if (tabella_confronto_movimenti !== undefined && tabella_confronto_movimenti[2] !== undefined) {
        windowContent += tabella_confronto_movimenti[2];
    }

    windowContent += '</div>';

    $(".print").html(windowContent);

    //$(".print").printArea();
    setTimeout(function () {
        window.print();
    }, 500);

    /*var width = screen.availWidth;
     var height = screen.availHeight;
     
     var data = new Date();
     
     var giorno = data.format("dd/mm/yyyy");
     var orario = data.format("HH:MM");
     
     var windowContent = '<!DOCTYPE html>';
     windowContent += '<html>';
     windowContent += '<head>';
     windowContent += '<title>Stampa Movimenti</title>';
     windowContent += '<meta charset="UTF-8">';
     windowContent += '<style>';
     windowContent += '@font-face { font-family: Personalizzato; src: url(\'http://localhost/fast.intellinet/bootstrap/css/1YwB1sO8YE1Lyjf12WNiUA.woff2\'); } ';
     windowContent += '</style>';
     windowContent += '</head>';
     windowContent += '<body style=\'font-size:15px;font-family: Personalizzato\'>';
     
     windowContent += '<div class="container">';
     
     windowContent += '<div class="row">';
     
     
     
     windowContent += '<div class="row" style="border:1px solid black;overflow:auto;padding:5px;">';
     windowContent += '<div style="float:left;width:50%; text-align:left;">';
     windowContent += '<strong>'+comanda.profilo_aziendale.ragione_sociale+'</strong><br>Stampa effettuata il: <strong>'+giorno+'</strong>';
     windowContent += '</div>';
     windowContent += '<div style="float:left;width:50%; text-align:right;">';
     windowContent += '<strong>'+comanda.locale+'</strong><br>Alle ore: <strong>'+orario+'</strong>';
     windowContent += '</div>';
     windowContent += '</div>';
     
     
     windowContent += '<div class="row" style="margin-top:2cm">';
     windowContent += '<div style="width:100%;">';
     windowContent += '<h1>Stampa Movimenti</h1>';
     windowContent += '</div>';
     windowContent += '</div>';
     
     
     windowContent += '<div class="row">';
     windowContent += '<div style="width:100%;">';
     windowContent += '<img src="' + dataUrl + '" style="filter: invert(100%);">';
     windowContent += '</div>';
     windowContent += '</div>';
     
     
     
     windowContent += '<div class="row">';
     windowContent += '<div text-align:right;">';
     windowContent += '<h3>Prova 2</h3>';
     windowContent += '</div>';
     windowContent += '</div>';
     
     windowContent += '<div class="row">';
     windowContent += '<div style="width:100%;">';
     windowContent += '<img src="' + dataUrl2 + '" style="filter: invert(100%);">';
     windowContent += '</div>';
     windowContent += '</div>';
     
     windowContent += '</div>';
     
     
     windowContent += '</div>';
     
     windowContent += '</body>';
     windowContent += '</html>';
     
     var printWin = window.open('', '', 'width=' + width + ',height=' + height);
     printWin.document.open();
     printWin.document.write(windowContent);
     
     printWin.document.addEventListener('load', function () {
     printWin.focus();
     printWin.print();
     printWin.document.close();
     printWin.close();
     }, true);*/
}


/* Get the documentElement (<html>) to display the page in fullscreen */
var elem = document.documentElement;

/* View in fullscreen */
function openFullscreen() {
    if (elem.requestFullscreen) {
        elem.requestFullscreen();
    } else if (elem.mozRequestFullScreen) { /* Firefox */
        elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
        elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) { /* IE/Edge */
        elem.msRequestFullscreen();
    }
}

/* Close fullscreen */
function closeFullscreen() {
    if (document.exitFullscreen) {
        document.exitFullscreen();
    } else if (document.mozCancelFullScreen) { /* Firefox */
        document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
        document.webkitExitFullscreen();
    } else if (document.msExitFullscreen) { /* IE/Edge */
        document.msExitFullscreen();
    }
}