/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global alasql, comanda, bootbox */

function FastorderApiClient() {

    this.export_products_fastorder = function () {

        let url_fastorder = $('#form_settaggi input[name="url_fastorder"]:visible').val();
        let categorie_come_intellinet = $('#form_settaggi input[name="categorie_come_intellinet"]:visible').is(':checked');

        if (categorie_come_intellinet === true) {
            this.export_categories_fastorder();
        }

        if (comanda.pizzeria_asporto === true) {
            this.export_settings_fastorder();
        }

        if (checkEmptyVariable(url_fastorder, "string") === true) {


            let datiTabellaGrezzi = JSON.parse(JSON.stringify(alasql.tables.prodotti.data));
            datiTabellaGrezzi.filter((v) => {

                if (comanda.nome_categoria[v.categoria] !== undefined && v.categoria !== "XXX" && v.esportazione_fastorder_abilitata === "true") {
                    return v;
                }
            }).forEach((v) => {

                Object.keys(v).forEach((index) => {
                    if (v[index] === null || v[index] === "NaN" || v[index] === "undefined" || v[index] === "NULL" || v[index] === "null") {
                        v[index] = "";
                    }
                });

                if (categorie_come_intellinet === true) {
                    v.cat_varianti = v.cat_varianti.split("/")[0];
                } else {
                    if (comanda.nome_categoria[v.categoria].substr(0, 2) === "V.") {
                        v.cat_varianti = "";
                    } else {
                        v.categoria = "999";
                        v.cat_varianti = v.cat_varianti.split("/")[0];
                    }
                }

                v.descrizione = v.descrizione_fastorder;
                v.ricetta = v.ricetta_fastorder;
            });
            let datiTabella = JSON.stringify(datiTabellaGrezzi);


            let modifica_prezzi_fastorder = $('#form_settaggi input[name="modifica_prezzi_fastorder"]:visible').is(':checked');


            $.ajax({
                dataType: 'json',
                type: 'POST',
                url: url_fastorder + "classi_php/importTable.php",
                timeout: 3000,
                data: {
                    'modifica_prezzi_fastorder': modifica_prezzi_fastorder,
                    'nome_tabella': 'prodotti',
                    'dati_tabella': datiTabella
                },
                success: function (dati) {

                    /*se chiami l'api come json è true, altrimenti "true"*/
                    if (dati === true) {
                        bootbox.alert("Esportazione Completata");
                    } else {
                        bootbox.alert("Esportazione Errata - 1");
                    }

                },
                error: function () {

                    bootbox.alert("Esportazione Errata - 2");
                }

            });
        }
    };



    this.export_categories_fastorder = function () {

        let url_fastorder = $('#form_settaggi input[name="url_fastorder"]:visible').val();

        if (checkEmptyVariable(url_fastorder, "string") === true) {


            let datiTabellaGrezzi = JSON.parse(JSON.stringify(alasql.tables.categorie.data));
            datiTabellaGrezzi.filter((v) => {

                /*if (comanda.nome_categoria[v.categoria] !== undefined && v.categoria !== "XXX") {
                 return v;
                 }*/
            }).forEach((v) => {

                Object.keys(v).forEach((index) => {
                    if (v[index] === null || v[index] === "NaN" || v[index] === "undefined" || v[index] === "NULL" || v[index] === "null") {
                        v[index] = "";
                    }
                });
                /*if (comanda.nome_categoria[v.categoria].substr(0, 2) === "V.") {
                 v.cat_varianti = "";
                 } else {
                 v.categoria = "999";
                 v.cat_varianti = v.cat_varianti.split("/")[0];
                 }*/
            });
            let datiTabella = JSON.stringify(datiTabellaGrezzi);
            $.ajax({
                dataType: 'json',
                type: 'POST',
                url: url_fastorder + "classi_php/importTableCategorie.php",
                timeout: 3000,
                data: {
                    'nome_tabella': 'categorie',
                    'dati_tabella': datiTabella
                },
                success: function (dati) {

                    /*se chiami l'api come json è true, altrimenti "true"*/
                    if (dati === true) {
                        bootbox.alert("Esportazione Completata");
                    } else {
                        bootbox.alert("Esportazione Errata - 1");
                    }

                },
                error: function () {

                    bootbox.alert("Esportazione Errata - 2");
                }

            });
        }
    };

    this.export_settings_fastorder = function () {

        let url_fastorder = $('#form_settaggi input[name="url_fastorder"]:visible').val();

        if (checkEmptyVariable(url_fastorder, "string") === true) {


            let datiTabellaGrezzi = JSON.parse(JSON.stringify(alasql.tables.impostazioni_fiscali.data));
            datiTabellaGrezzi.filter((v) => {


            }).forEach((v) => {

                Object.keys(v).forEach((index) => {
                    if (v[index] === null || v[index] === "NaN" || v[index] === "undefined" || v[index] === "NULL" || v[index] === "null") {
                        v[index] = "";
                    }
                });

            });
            let datiTabella = JSON.stringify(datiTabellaGrezzi);
            $.ajax({
                dataType: 'json',
                type: 'POST',
                url: url_fastorder + "classi_php/importTableSettaggiProfili.php",
                timeout: 3000,
                data: {
                    'nome_tabella': 'impostazioni_fiscali',
                    'dati_tabella': datiTabella
                },
                success: function (dati) {

                    /*se chiami l'api come json è true, altrimenti "true"*/
                    if (dati === true) {
                        bootbox.alert("Esportazione Completata");
                    } else {
                        bootbox.alert("Esportazione Errata - 1");
                    }

                },
                error: function () {

                    bootbox.alert("Esportazione Errata - 2");
                }

            });
        }
    };

    this.api_request = async function (objJsonStr) {

        return new Promise(resolve => {

            if (checkEmptyVariable(comanda.url_fastorder, "string") === true) {

                var objJsonB64 = btoa(unescape(encodeURIComponent(JSON.stringify(objJsonStr))));
                var dati_da_inviare = {
                    dati: objJsonB64
                };
                if (objJsonStr === undefined) {
                    dati_da_inviare = null;
                }

                $.post(comanda.url_fastorder + "classi_php/api_fastorder.php", dati_da_inviare, function (data, status) {
                    resolve(JSON.parse(data));
                });
            }
        });
    };


    this.ricevi_ordini_inviati = async function () {
        
        let oggetto_id_ordini_ricevuti = new Object();
        let ordini_ricevuti = await fastorder.api_request({ tipo: 'ricevi_ordini_inviati' });
        if (ordini_ricevuti !== null && ordini_ricevuti.length > 0) {

            let counter = 0;
            let id_cliente = "";

            for (let ordine of ordini_ricevuti) {

                Object.keys(ordine).forEach((index) => {
                    if (ordine[index] === null || ordine[index] === "NaN" || ordine[index] === "undefined" || ordine[index] === "NULL" || ordine[index] === "null") {
                        ordine[index] = "";
                    }
                });


                if (oggetto_id_ordini_ricevuti[ordine.id] === undefined) {
                    oggetto_id_ordini_ricevuti[ordine.id] = ordine.id;
                }

                ordine.prezzo_vero = ordine.prezzo_un;

                if (comanda.pizzeria_asporto === true) {

                    ordine.ora_consegna = ordine.ora_consegna.slice(-5);
                    ordine.orario_preparazione = ordine.ora_consegna;

                    if (comanda.orario_preparazione === "1") {

                        if (ordine.ora_consegna.length === 5) {
                            let a = new Date("01 Jan 2000 " + ordine.ora_consegna + ":00");
                            if (ordine.tipo_consegna === "DOMICILIO") {
                                a.setMinutes(a.getMinutes() + parseInt("-" + comanda.tempo_preparazione_default) + parseInt("-" + comanda.tempo_preparazione_default_domicilio));
                            } else {
                                a.setMinutes(a.getMinutes() + parseInt("-" + comanda.tempo_preparazione_default));
                            }

                            let b = aggZero(a.getHours(), 2) + ":" + aggZero(a.getMinutes(), 2);
                            ordine.orario_preparazione = b;

                        }


                    }

                    ordine.stato_record = "FORNO";
                    if (ordine.ntav_comanda === "TAKE AWAY") {

                        ordine.ntav_comanda = "ASPORTO99999" + ordine.id;

                        ordine.cod_cliente = (parseInt(ordine.cod_cliente) + 3000000).toString();

                        let nome_cliente = alasql("select * from clienti where id=" + ordine.cod_cliente + ";");
                        if (nome_cliente.length > 0) {
                            ordine.nome_comanda = nome_cliente[0].nome + " " + nome_cliente[0].cognome;
                            ordine.rag_soc_cliente = ordine.cod_cliente;
                        } else {
                            ordine.nome_comanda = ordine.rag_soc_cliente.split(",")[0];
                        }

                    } else {

                        ordine.cod_cliente = "";

                        ordine.ntav_comanda = "ASPORTO TAV " + ordine.ntav_comanda;

                        ordine.nome_comanda = ordine.ntav_comanda;

                        if (counter === 0) {

                            var testo_query = "select id from clienti WHERE id<3000000 order by cast(id as int) desc limit 1;";
                            let risultato = alasql(testo_query);


                            if (risultato[0] !== undefined && risultato[0].id !== undefined) {
                                id_cliente = parseInt(risultato[0].id) + 1;
                            }

                            let query_inserimento_cliente_temp = "INSERT INTO clienti (id,ragione_sociale,nome,cognome,tipo_cliente,stato_sincro,formato_trasmissione,regime_fiscale) VALUES (" + id_cliente + ",'','" + ordine.ntav_comanda + "','','ap_privato','0','false','RF01');";
                            alasql(query_inserimento_cliente_temp);
                            comanda.sock.send({ tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: query_inserimento_cliente_temp, terminale: comanda.terminale, ip: comanda.ip_address });

                        }



                        ordine.rag_soc_cliente = id_cliente;
                        ordine.tipo_consegna = "PIZZERIA";

                    }
                    ordine.progr_gg_uni = ordine.ntav_comanda;
                    ordine.tipologia_fattorino = "0";
                } else {


                    if (ordine.ntav_comanda === "TAKE AWAY") {

                        ordine.ntav_comanda = "ASPORTO99999" + ordine.id;

                        ordine.cod_cliente = (parseInt(ordine.cod_cliente) + 3000000).toString();

                        let nome_cliente = alasql("select * from clienti where id=" + ordine.cod_cliente + ";");
                        if (nome_cliente.length > 0) {
                            ordine.nome_comanda = nome_cliente[0].nome + " " + nome_cliente[0].cognome;
                            ordine.rag_soc_cliente = ordine.cod_cliente;
                        } else {
                            ordine.nome_comanda = ordine.rag_soc_cliente.split(",")[0];
                        }

                    } else {


                        if (counter === 0) {

                            /*var testo_query = "select id from clienti WHERE id<3000000 order by cast(id as int) desc limit 1;";
                             let risultato = alasql(testo_query);
                             
                             
                             if (risultato[0] !== undefined && risultato[0].id !== undefined) {
                             id_cliente = parseInt(risultato[0].id) + 1;
                             }
                             
                             let query_inserimento_cliente_temp = "INSERT INTO clienti (id,ragione_sociale,nome,cognome,tipo_cliente,stato_sincro,formato_trasmissione,regime_fiscale) VALUES (" + id_cliente + ",'','" + ordine.ntav_comanda + "','','ap_privato','0','false','RF01');";
                             alasql(query_inserimento_cliente_temp);
                             comanda.sock.send({tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: query_inserimento_cliente_temp, terminale: comanda.terminale, ip: comanda.ip_address});
                             */

                            let query_tavolo_rosso = "UPDATE tavoli SET colore='green' WHERE numero='" + ordine.ntav_comanda + "';";
                            alasql(query_tavolo_rosso);
                            comanda.sock.send({ tipo: "aggiornamento_tavoli", operatore: comanda.operatore, query: query_tavolo_rosso, terminale: comanda.terminale, ip: comanda.ip_address });

                            comanda.funzionidb.conto_attivo();
                            disegna_tavoli_salvati();


                        }



                    }

                    /*ordine.rag_soc_cliente = id_cliente;
                     ordine.tipo_consegna = "PIZZERIA";*/

                    ordine.stato_record = "ATTIVO";

                   

                }

                if (ordine.contiene_variante === "S") {
                    ordine.contiene_variante = "1";
                }

                ordine.progr_gg_uni = ordine.ntav_comanda;
                ordine.tipologia_fattorino = "0";


                ordine.numero_servizio = comanda.nome_servizio;
                ordine.data_servizio = comanda.data_servizio;



                ordine.perc_iva = comanda.percentuale_iva_takeaway;
                ordine.reparto = comanda.reparto_beni_default;

                ordine.stampata_sn="N";
                ordine.data_comanda=new Date().format("yyyy-mm-dd");

                //BISOGNA METTERE LA DATA COMANDA IN FORMATO 2021-12-23 ad esempio

                let query_insert = "INSERT INTO comanda (" + Object.keys(ordine).join() + ") VALUES (" + Object.values(ordine).map((v) => {
                    return "'" + v + "'";
                }).join() + ");";
                alasql(query_insert);
                comanda.sock.send({ tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: query_insert, terminale: comanda.terminale, ip: comanda.ip_address });

                counter++;
            }

            comanda.sincro.query_cassa();


            for (let key in oggetto_id_ordini_ricevuti) {
                await fastorder.api_request({ tipo: 'segna_ordine_stampato', id: key });
            }

            righe_forno();
            comanda.sincro.query_cassa();
        }
    };

    this.ricevi_clienti_aggiornati = async function () {
        let oggetto_clienti_ricevuti = new Object();
        let ordini_ricevuti = await fastorder.api_request({ tipo: 'ricevi_clienti_aggiornati' });
        if (ordini_ricevuti.length > 0) {

            for (let cliente of ordini_ricevuti) {



                Object.keys(cliente).forEach((index) => {
                    if (cliente[index] === null || cliente[index] === "NaN" || cliente[index] === "undefined" || cliente[index] === "NULL" || cliente[index] === "null") {
                        cliente[index] = "";
                    }

                    if (index === "partita_iva" || index === "codice_fiscale" || index === "nome" || index === "cognome" || index === "ragione_sociale" || index === "indirizzo" || index === "citta" || index === "comune" || index === "numero" || index === "note") {
                        cliente[index] = cliente[index].toUpperCase();
                    }

                });


                if (oggetto_clienti_ricevuti[cliente.id] === undefined) {
                    oggetto_clienti_ricevuti[cliente.id] = cliente.id;
                }

                cliente.id = (parseInt(cliente.id) + 3000000).toString();
                /*cliente.stato_sincro = "1";*/

                let query_select = "SELECT * FROM clienti WHERE id=" + cliente.id + ";";
                let query_exec = "";
                let elenco_clienti_compatibili = alasql(query_select);
                if (elenco_clienti_compatibili.length === 0) {
                    query_exec = "INSERT INTO clienti (" + Object.keys(cliente).join() + ") VALUES (" + Object.entries(cliente).map((v) => {
                        if (v[0] === "id") {
                            return v[1];
                        } else {
                            if (v[1] === null || v[1] === "NaN" || v[1] === "undefined" || v[1] === "null" || v[1] === "NULL" || v[1] === "UNDEFINED") {
                                return "''";
                            } else {
                                return "'" + v[1] + "'";
                            }
                        }
                    }).join() + ");";
                    alasql(query_exec);
                } else {
                    let id = cliente.id;
                    delete cliente.id;
                    query_exec = "UPDATE clienti SET " + Object.entries(cliente).map((v) => {
                        return v[0] + "='" + v[1] + "'";
                    }).join() + " WHERE id=" + id + ";";
                    alasql(query_exec);
                }

                comanda.sock.send({ tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: query_exec, terminale: comanda.terminale, ip: comanda.ip_address });
            }

            for (let key in oggetto_clienti_ricevuti) {
                await fastorder.api_request({ tipo: 'segna_cliente_sincronizzato', id: key });
            }

            /*righe_forno();*/
            comanda.sincro.query_cassa();
        }
    };

    this.intervallo = function () {

        fastorder.ricevi_clienti_aggiornati();

        fastorder.ricevi_ordini_inviati();

    };
    /* ogni 20 secondi si scarica i nuovi dati */
    setInterval(this.intervallo, 5000);

    this.invia_ordini_forno = async function () {

        await fastorder.api_request({ tipo: 'resetta_stato_forno_locale' });
        /*desc_art,portata,quantita,spazio_forno,tipo_ricevuta,stampata_sn,ora_consegna,orario_preparazione,portata,id,stato_record,tipo_consegna*/
        let testo_query = 'select *  from comanda where data_servizio="' + comanda.data_servizio + '" and (stato_record="FORNO" or stato_record="ATTIVO") and tipo_consegna!="NIENTE" and tipo_consegna!="";';
        let risultato = alasql(testo_query);
        for (let dati_articolo of risultato) {

            dati_articolo.tipo = 'invia_articolo';
            dati_articolo.id = "SERVER_LOCALE";
            dati_articolo.stato_record = "STAMPATO";
            await fastorder.api_request(dati_articolo);
        }

    };
}

let fastorder = new FastorderApiClient();