/* global comanda, bootbox */

function Calendario(mese, anno) {

    var data_oggi = new Date();
    if (mese !== undefined) {
        this.mese = mese;
    } else
    {
        this.mese = data_oggi.getMonth();
    }
    if (anno !== undefined) {
        this.anno = anno;
    } else
    {
        this.anno = data_oggi.getFullYear();
    }

    this.mesi = new Array();
    this.mesi.push("Gennaio");
    this.mesi.push("Febbraio");
    this.mesi.push("Marzo");
    this.mesi.push("Aprile");
    this.mesi.push("Maggio");
    this.mesi.push("Giugno");
    this.mesi.push("Luglio");
    this.mesi.push("Agosto");
    this.mesi.push("Settembre");
    this.mesi.push("Ottobre");
    this.mesi.push("Novembre");
    this.mesi.push("Dicembre");

    this.riempi_tendina_ordinazioni = function () {

        $("#selezione_gruppo_ordinazione option").remove();

        $('#selezione_gruppo_ordinazione').append($('<option>', {
            value: "",
            text: "Seleziona Gruppo"
        }));

        $('#selezione_gruppo_ordinazione').append($('<option>', {
            value: 1,
            text: this.gruppo1
        }));

        $('#selezione_gruppo_ordinazione').append($('<option>', {
            value: 2,
            text: this.gruppo2
        }));

        $('#selezione_gruppo_ordinazione').append($('<option>', {
            value: 3,
            text: this.gruppo3
        }));

        $('#selezione_gruppo_ordinazione').append($('<option>', {
            value: 4,
            text: this.gruppo4
        }));

    };


    this.rinomina_gruppi = function () {

        var query = "select * from gruppi_calendario where id='0';";
        comanda.sincro.query(query, function (risultato) {

            calendario.array_gruppo = new Array();

            calendario.gruppo1 = risultato[0].gruppo1;
            calendario.array_gruppo[1] = '<span style="color:yellow;font-size: 3vh;line-height: 0;">&#9679;</span>';
            calendario.gruppo2 = risultato[0].gruppo2;
            calendario.array_gruppo[2] = '<span style="color:red;font-size: 3vh;line-height: 0;">&#9679;</span>';
            calendario.gruppo3 = risultato[0].gruppo3;
            calendario.array_gruppo[3] = '<span style="color:green;font-size: 3vh;line-height: 0;">&#9679;</span>';
            calendario.gruppo4 = risultato[0].gruppo4;
            calendario.array_gruppo[4] = '<span style="color:blue;font-size: 3vh;line-height: 0;">&#9679;</span>';

            $("#calendario_nome_gruppo1").html(calendario.gruppo1);
            $("#calendario_nome_gruppo2").html(calendario.gruppo2);
            $("#calendario_nome_gruppo3").html(calendario.gruppo3);
            $("#calendario_nome_gruppo4").html(calendario.gruppo4);

            calendario.riempi_tendina_ordinazioni();

        });
    };



    this.calcola_giorni_mese = function () {

        //Se il mese � Febbraio bisogna vedere se l'anno � bisestile oppure no

        if (this.mese === 1) {
            //Se l'anno � bisestile febbraio � di 29 giorni, altrimenti 28
            var anno_bisestile = false;
            //Per calcolare l'anno bisestile

            if (this.anno % 4 === 0)
            {
                if (this.anno % 100 === 0) {

                    if (this.anno % 400 === 0) {
                        return 29;
                        //ANNO BISESTILE   
                    } else
                    {
                        return 28;
                        //ANNO NON BISESTILE
                    }

                } else
                {
                    return 29;
                    //ANNO BISESTILE
                }

            } else
            {
                return 28;
                //ANNO NON BISESTILE
            }

        }
        //Se il mese+1 � pari � sempre di 31 giorni
        else if (this.mese === 0 || this.mese === 2 || this.mese === 4 || this.mese === 6 || this.mese === 7 || this.mese === 9 || this.mese === 11) {
            return 31;
        }
        //Se il mese � un altro ha 30 giorni
        else {
            return 30;
        }

    };
    this.giorno_settimanale = new Array(7);
    this.giorno_settimanale[0] = "lu";
    this.giorno_settimanale[1] = "ma";
    this.giorno_settimanale[2] = "me";
    this.giorno_settimanale[3] = "gi";
    this.giorno_settimanale[4] = "ve";
    this.giorno_settimanale[5] = "sa";
    this.giorno_settimanale[6] = "do";

    this.calcola_primo_giorno_mese = function () {
        var data = new Date();
        data.setDate(1);
        data.setMonth(this.mese);
        data.setYear(this.anno);
        //0 � domenica
        var numero_giorno = ((data.getDay() + 6) % 7);
        var nome_giorno = this.giorno_settimanale[((data.getDay() + 6) % 7)];
        return numero_giorno;
    };

    this.mese_precedente = function () {

        if (this.mese === 0) {
            this.mese = 11;
            this.anno--;
        } else {
            this.mese--;
        }

        this.crea();
    };

    this.mese_successivo = function () {

        if (this.mese === 11) {
            this.mese = 0;
            this.anno++;
        } else {
            this.mese++;
        }

        this.crea();
    };

    this.crea = function (salta_ricreazione_anteprima) {

        this.rinomina_gruppi();

        if (salta_ricreazione_anteprima !== true) {
            $("#calendario_ordinazioni td>.numero_giorno").html("");
            $("#calendario_ordinazioni td>.anteprima").hide();
        }
        $("#calendario_ordinazioni td>.numero_giorno").css("color", "white");

        $("#calendario_ordinazioni_mese").html(this.mesi[this.mese]);
        $("#calendario_ordinazioni_anno").html(this.anno);
        var numero_giorni = this.calcola_giorni_mese();
        var giorno_partenza = this.calcola_primo_giorno_mese();
        var layout = "";
        this.giorno_settimanale.forEach(function (v) {
            layout += v + " ";
        });
        layout += "\n\n";
        var settimana_calendario = 1;
        var giorno_calendario = 1;
        var i = 1;
        for (; i <= giorno_partenza; i++) {
            //PARTENZA DA GIORNO SUCCESSIVO
            layout += "___";
            giorno_calendario++;
        }

        var sql = "select * from appuntamenti_calendario where substr(da_data,1,4)='" + calendario.anno + "' and substr(da_data,6,2)='" + aggZero(calendario.mese + 1, 2) + "' order by da_data asc,cast(da_ora as int) asc,cast(da_minuti as int) asc;";

        comanda.sincro.query(sql, function (impegni) {

            var sql_2 = "select * from comanda where (stato_record='APERTO' or stato_record='ATTIVO') and substr(nome_comanda,1,2)='C_' and length(nodo)=3  group by nome_comanda  order by nome_comanda asc,prog_inser asc; ";
            comanda.sincro.query(sql_2, function (articoli) {

                var data_odierna = new Date();
                var giorno_odierno = data_odierna.getDate();
                var mese_odierno = data_odierna.getMonth();
                var anno_odierno = data_odierna.getFullYear();

                var g = 1;
                for (; g <= numero_giorni; g++) {
                    //NUMERO GIORNO IN i GIORNO SETTIMANA
                    layout += g + "__";


                    if (anno_odierno === calendario.anno && mese_odierno === calendario.mese && g === giorno_odierno) {
                        $("#cal_s" + settimana_calendario + "_g" + giorno_calendario + ">.numero_giorno").css("color", "red");
                    }


                    $("#cal_s" + settimana_calendario + "_g" + giorno_calendario + ">.numero_giorno").html(g);
                    $("#cal_s" + settimana_calendario + "_g" + giorno_calendario + ">.numero_giorno").attr(comanda.evento, 'prendi_ordine("' + g + '")');


                    $("#cal_s" + settimana_calendario + "_g" + giorno_calendario + ">.anteprima").html("");

                    impegni.forEach(function (a) {

                        a.desc_art = "VUOTO";

                        articoli.forEach(function (b) {
                            if (b.nome_comanda === "C_" + a.id) {
                                a.desc_art = b.desc_art;
                            }
                        });

                        var giorno_confronto = calendario.anno + "-" + aggZero(calendario.mese + 1, 2) + "-" + aggZero(g, 2);
                        if ((a.da_data === giorno_confronto) || (a.ordinazione_pertinenza === "F" && (a.da_data <= giorno_confronto && a.a_data >= giorno_confronto))) {

                            if (a.selezione_gruppo_ordinazione === "") {

                                var gruppo = '<span style="color:black;font-size: 3vh;line-height: 0;">&#9679;</span>';
                            } else {
                                var gruppo = calendario.array_gruppo[a.selezione_gruppo_ordinazione];
                            }

                            if (a.da_minuti === "") {
                                a.da_minuti = "00";
                            }
                            var da_orario = "<span style='margin-right:9px;' class='pull-right'>" + a.da_ora + ":" + a.da_minuti + "</span>";

                            if (a.da_ora === "")
                            {
                                da_orario = "";
                            }



                            if (a.a_minuti === "") {
                                a.a_minuti = "00";
                            }
                            var a_orario = "<span style='margin-right:9px;' class='pull-right'>" + a.a_ora + ":" + a.a_minuti + "</span>";

                            if (a.a_ora === "")
                            {
                                a_orario = "";
                            }



                            if (a.ordinazione_pertinenza === "L") {
                                var per_persone = "";

                                var descrizione_articolo = a.desc_art;
                                if (a.qta_persone !== "") {
                                    per_persone = " x " + a.qta_persone;
                                    if (descrizione_articolo.length > 10) {
                                        descrizione_articolo = descrizione_articolo.substr(0, 10) + ".";
                                    }
                                } else
                                {
                                    if (descrizione_articolo.length > 13) {
                                        descrizione_articolo = descrizione_articolo.substr(0, 10) + ".";
                                    }
                                }

                                var colore = "";
                                if (a.ordine_evaso === "S") {
                                    colore = "color:#525252;";
                                }



                                var anteprima = "<div style='" + colore + "' class='appuntamento_cliccato' "+comanda.evento+"='riprendi_ordine_calendario(\"" + a.id + "\")'><strong>" + gruppo + "<span>[" + a.ordinazione_pertinenza + "] " + descrizione_articolo + per_persone + " " + da_orario + "</span></strong></div>";
                            } else if (a.ordinazione_pertinenza === "P") {
                                var anteprima = "<div style='" + colore + "'class='appuntamento_cliccato' "+comanda.evento+"='riprendi_ordine_calendario(\"" + a.id + "\")'><strong>" + gruppo + "<span>[" + a.ordinazione_pertinenza + "] " + a.titolo_personale.toUpperCase() + " " + da_orario + "</strong></div>";
                            } else if (a.ordinazione_pertinenza === "F") {

                                var dalle_alle = "";
                                if (giorno_confronto === a.da_data) {
                                    dalle_alle += "dalle " + a.da_ora + ":" + a.da_minuti;
                                }
                                if (giorno_confronto === a.a_data) {
                                    dalle_alle += " alle " + a.a_ora + ":" + a.a_minuti;
                                }
                                var anteprima = "<div style='" + colore + "'class='appuntamento_cliccato' "+comanda.evento+"='riprendi_ordine_calendario(\"" + a.id + "\")'><strong>" + gruppo + "<span>[" + a.ordinazione_pertinenza + "] " + a.nome.toUpperCase() + " " + dalle_alle + "</strong></div>";
                            }

                            $("#cal_s" + settimana_calendario + "_g" + giorno_calendario + ">.anteprima").append(anteprima);
                        }

                    });

                    $("#cal_s" + settimana_calendario + "_g" + giorno_calendario + ">.anteprima").show();

                    giorno_calendario++;
                    if (i % 7 === 0) {
                        //SETTIMANA SUCCESSIVA
                        layout += "\n";
                        giorno_calendario = 1;
                        settimana_calendario++;
                    }
                    i++;
                }

                $('#popup_calendario_ordinazioni').modal('show');
            });
        });
    };
    this.gruppo_da_modificare = "";
}

function popola_caselle(a, settimana_calendario, giorno_calendario, giorno_confronto) {
    var sql_2 = "select * from comanda where nome_comanda='C_" + a.id + "' limit 1; ";
    comanda.sincro.query(sql_2, function (articolo) {

        if (a.selezione_gruppo_ordinazione === "") {
            var gruppo = '<span style="color:black;font-size: 3vh;line-height: 0;">&#9679;</span>';
        } else {
            var gruppo = calendario.array_gruppo[a.selezione_gruppo_ordinazione];
        }


        if (a.ordinazione_pertinenza === "L") {
            var desc_art = "VUOTO";
            if (articolo !== undefined && articolo[0] !== undefined && articolo[0].desc_art !== undefined) {
                desc_art = articolo[0].desc_art;
            }
            var anteprima = "<div "+comanda.evento+"='riprendi_ordine_calendario(\'" + a.id + "\')'><strong>" + gruppo + "<span>[" + a.ordinazione_pertinenza + "] " + desc_art + " x " + a.qta_persone + " " + a.da_ora + ":" + a.da_minuti + "</span></strong></div>";
        } else if (a.ordinazione_pertinenza === "P") {
            var anteprima = "<div "+comanda.evento+"='riprendi_ordine_calendario(\'" + a.id + "\')'><strong>" + gruppo + "<span>[" + a.ordinazione_pertinenza + "] " + a.titolo_personale.toUpperCase() + " " + a.da_ora + ":" + a.da_minuti + "</strong></div>";
        } else if (a.ordinazione_pertinenza === "F") {

            var dalle_alle = "";
            if (giorno_confronto === a.da_data) {
                dalle_alle += "dalle " + a.da_ora + ":" + a.da_minuti;
            }
            if (giorno_confronto === a.a_data) {
                dalle_alle += " alle " + a.a_ora + ":" + a.a_minuti;
            }
            var anteprima = "<div "+comanda.evento+"='riprendi_ordine_calendario(\'" + a.id + "\')'><strong>" + gruppo + "<span>[" + a.ordinazione_pertinenza + "] " + a.nome.toUpperCase() + " " + dalle_alle + "</strong></div>";
        }

        $("#cal_s" + settimana_calendario + "_g" + giorno_calendario + ">.anteprima").append(anteprima);

    });
}



calendario = new Calendario();

function apri_calendario_ordinazioni() {

    calendario = new Calendario();
    calendario.crea();
}

function calendario_mese_precedente() {
    calendario.mese_precedente();
}

function calendario_mese_successivo() {
    calendario.mese_successivo();
}


$(document).on('focus', '[contenteditable]', function () {
    var $this = $(this);
    $this.data('before', $this.html());
    return $this;
}).on('blur keyup paste input', '[contenteditable]', function () {
    var $this = $(this);
    if ($this.data('before') !== $this.html()) {
        $this.data('before', $this.html());
        $this.trigger('change');
    }
    return $this;
});

$(document).on('change', '#ordinazione_pertinenza', function () {

    var valore_campo = $("#ordinazione_pertinenza option:selected").val();

    switch (valore_campo) {
        case "L":
            $("#calendario_prendi_ordine .tasto_ordinazione_fatta").show();
            $("#calendario_prendi_ordine .titolo").html("ORDINE");
            $("#calendario_prendi_ordine .nome_data").html("Data:");

            $("#calendario_prendi_ordine .ferie").hide();
            $("#calendario_prendi_ordine .scegli_prodotti").show();
            $("#calendario_prendi_ordine .riquadro_prodotti").show();
            $("#calendario_prendi_ordine .qta_persone").show();
            $("#calendario_prendi_ordine .fontana").show();
            $("#calendario_prendi_ordine .candeline").show();
            $("#calendario_prendi_ordine .candeline_anni").show();
            $("#calendario_prendi_ordine .titolo_personale").hide();
            $("#calendario_prendi_ordine .testo").removeClass("col-xs-11");
            $("#calendario_prendi_ordine .testo").addClass("col-xs-5");

            $("#calendario_prendi_ordine .tasto_stampa_ordinazione").show();
            break;
        case "P":
            $("#calendario_prendi_ordine .tasto_ordinazione_fatta").hide();
            $("#calendario_prendi_ordine .titolo").html("APPUNTAMENTO");
            $("#calendario_prendi_ordine .nome_data").html("Da:");

            $("#calendario_prendi_ordine .ferie").hide();
            $("#calendario_prendi_ordine .scegli_prodotti").hide();
            $("#calendario_prendi_ordine .riquadro_prodotti").hide();
            $("#calendario_prendi_ordine .qta_persone").hide();
            $("#calendario_prendi_ordine .fontana").hide();
            $("#calendario_prendi_ordine .candeline").hide();
            $("#calendario_prendi_ordine .candeline_anni").hide();
            $("#calendario_prendi_ordine .titolo_personale").show();
            $("#calendario_prendi_ordine .testo").removeClass("col-xs-5");
            $("#calendario_prendi_ordine .testo").addClass("col-xs-11");

            $("#calendario_prendi_ordine .tasto_stampa_ordinazione").show();
            break;
        case "F":
            $("#calendario_prendi_ordine .tasto_ordinazione_fatta").hide();
            $("#calendario_prendi_ordine .titolo").html("FERIE");
            $("#calendario_prendi_ordine .nome_data").html("Da:");

            $("#calendario_prendi_ordine .ferie").show();
            $("#calendario_prendi_ordine .scegli_prodotti").hide();
            $("#calendario_prendi_ordine .riquadro_prodotti").hide();
            $("#calendario_prendi_ordine .qta_persone").hide();
            $("#calendario_prendi_ordine .fontana").hide();
            $("#calendario_prendi_ordine .candeline").hide();
            $("#calendario_prendi_ordine .candeline_anni").hide();
            $("#calendario_prendi_ordine .titolo_personale").hide();
            $("#calendario_prendi_ordine .testo").removeClass("col-xs-5");
            $("#calendario_prendi_ordine .testo").addClass("col-xs-11");

            $("#calendario_prendi_ordine .tasto_stampa_ordinazione").hide();

            break;
    }

});

$(document).on('change', '.gruppi_modificabili', function () {

    var id_gruppo_modificabile = this.id;

    if (this.innerText.length < 1) {
        this.innerText = "XXX";
    }

    var nome_modificato = this.innerText;


    var nome_gruppo_db = "";
    switch (id_gruppo_modificabile) {

        case "calendario_nome_gruppo1":
            nome_gruppo_db = "gruppo1";
            calendario.gruppo1 = nome_modificato;
            break;
        case "calendario_nome_gruppo2":
            nome_gruppo_db = "gruppo2";
            calendario.gruppo2 = nome_modificato;

            break;
        case "calendario_nome_gruppo3":
            nome_gruppo_db = "gruppo3";
            calendario.gruppo3 = nome_modificato;

            break;
        case "calendario_nome_gruppo4":
            nome_gruppo_db = "gruppo4";
            calendario.gruppo4 = nome_modificato;

            break;
    }

    var sql = "update gruppi_calendario set " + nome_gruppo_db + " = '" + nome_modificato + "';";

    comanda.sock.send({tipo: "aggiornamento_gruppi_calendario", operatore: comanda.operatore, query: sql, terminale: comanda.terminale, ip: comanda.ip_address});

    comanda.sincro.query(sql, function () {

        calendario.riempi_tendina_ordinazioni();

        comanda.sincro.query_cassa();

    });
});


function modifica_gruppo_calendario(numero_gruppo_da_modificare, nome_gruppo_modificato) {

    var sql = "update gruppi_calendario set " + calendario.gruppo_da_modificare + "='" + nome_gruppo_modificato + "' where id='0';";
    comanda.sincro.query(sql, function () {

        $("#popup_modifica_gruppo_calendario").modal("hide");
        calendario.crea();
    });
}

function prendi_ordine(giorno_facoltativo) {

    $('#calendario_prendi_ordine #id_cestino').hide();

    var sql_2 = "update comanda set stato_record='CANCELLATO DA CALENDARIO' where stato_record='ATTIVO' and nome_comanda=''  and ntav_comanda='CALENDARIO';";
    comanda.sock.send({tipo: "aggiornamento_calendario", operatore: comanda.operatore, query: sql_2, terminale: comanda.terminale, ip: comanda.ip_address});

    comanda.sincro.query(sql_2, function () {

        var sql_3 = "update comanda set stato_record='APERTO' where stato_record='ATTIVO' and nome_comanda!=''  and ntav_comanda='CALENDARIO';";
        comanda.sock.send({tipo: "aggiornamento_calendario", operatore: comanda.operatore, query: sql_3, terminale: comanda.terminale, ip: comanda.ip_address});

        comanda.sincro.query(sql_3, function () {


            comanda.sincro.query_cassa();

            var sql = "select id from appuntamenti_calendario order by cast(id as int) desc limit 1";

            comanda.sincro.query(sql, function (r) {

                var progressivo = "000000001";

                if (r[0] !== undefined)
                {
                    progressivo = parseInt(r[0].id) + 1;
                    progressivo = progressivo.toString();
                }

                calendario.progressivo_appuntamenti = aggZero(progressivo, 9);

                anteprima_articoli(calendario.progressivo_appuntamenti,function(){});

            });

            $('#calendario_prendi_ordine input[type="text"]').val("");

            $('#calendario_prendi_ordine').modal('show');

            if (giorno_facoltativo !== undefined) {
                $('#calendario_prendi_ordine .data_da_ordine').val(calendario.anno + "-" + aggZero(calendario.mese + 1, 2) + "-" + aggZero(giorno_facoltativo, 2));
            } else
            {
                $('#calendario_prendi_ordine .data_da_ordine').val("");
            }
            
            $("#calendario_prendi_ordine .tasto_ordinazione_fatta").show();

            $("#calendario_prendi_ordine .titolo").html("ORDINE");
            $("#calendario_prendi_ordine .nome_data").html("Data:");

            $("#calendario_prendi_ordine .ferie").hide();
            $("#calendario_prendi_ordine .scegli_prodotti").show();
            $("#calendario_prendi_ordine .riquadro_prodotti").show();
            $("#calendario_prendi_ordine .qta_persone").show();

            $("#calendario_prendi_ordine .fontana input").prop('checked', false);
            $("#calendario_prendi_ordine .candeline input").prop('checked', false);

            $("#calendario_prendi_ordine .fontana").show();
            $("#calendario_prendi_ordine .candeline").show();

            $("#calendario_prendi_ordine .candeline_anni").show();

            $("#calendario_prendi_ordine .titolo_personale").val("");
            $("#calendario_prendi_ordine .titolo_personale").hide();

            $("#calendario_prendi_ordine .testo").removeClass("col-xs-11");
            $("#calendario_prendi_ordine .testo").addClass("col-xs-5");

            $("#calendario_prendi_ordine select").prop("selectedIndex", 0);

            $("#calendario_prendi_ordine .tasto_stampa_ordinazione").show();
        });
    });
}


function riprendi_ordine_calendario(id) {

    calendario.modifica_in_corso = true;
    calendario.progressivo_appuntamenti = id;

    var sql = "select * from appuntamenti_calendario where id='" + id + "' limit 1";
    comanda.sincro.query(sql, function (r) {

        var sql_2 = "update comanda set stato_record='CANCELLATO DA CALENDARIO' where stato_record='ATTIVO'  and nome_comanda=''  and ntav_comanda='CALENDARIO';";
        comanda.sock.send({tipo: "aggiornamento_calendario", operatore: comanda.operatore, query: sql_2, terminale: comanda.terminale, ip: comanda.ip_address});

        comanda.sincro.query(sql_2, function () {

            var sql_3 = "update comanda set stato_record='APERTO' where stato_record='ATTIVO' and nome_comanda!=''  and ntav_comanda='CALENDARIO';";
            comanda.sock.send({tipo: "aggiornamento_calendario", operatore: comanda.operatore, query: sql_3, terminale: comanda.terminale, ip: comanda.ip_address});

            comanda.sincro.query(sql_3, function () {

                var sql_4 = "update comanda set stato_record='ATTIVO' where nome_comanda='C_" + id + "' and stato_record='APERTO'  and ntav_comanda='CALENDARIO';";
                comanda.sock.send({tipo: "aggiornamento_calendario", operatore: comanda.operatore, query: sql_4, terminale: comanda.terminale, ip: comanda.ip_address});

                comanda.sincro.query(sql_4, function () {

                    comanda.sincro.query_cassa();

                    $('#calendario_prendi_ordine input[type="text"]').val("");
                    $('#calendario_prendi_ordine .data_da_ordine').val(r[0].da_data);
                    $('#calendario_prendi_ordine #id_cestino').attr(comanda.evento, "elimina_ordine('" + id + "')");
                    $('#calendario_prendi_ordine #id_cestino').show();
                    $('#calendario_prendi_ordine .ora_da_ordine').val(r[0].da_ora);
                    $('#calendario_prendi_ordine .minuti_da_ordine').val(r[0].da_minuti);
                    $('#calendario_prendi_ordine .data_a_ordine').val(r[0].a_data);
                    $('#calendario_prendi_ordine .ora_a_ordine').val(r[0].a_ora);
                    $('#calendario_prendi_ordine .minuti_a_ordine').val(r[0].a_minuti);
                    $('#calendario_prendi_ordine .nome_ordinante').val(r[0].nome);
                    $('#calendario_prendi_ordine .cognome_ordinante').val(r[0].cognome);
                    $('#calendario_prendi_ordine .telefono').val(r[0].telefono);
                    $('#calendario_prendi_ordine .qta_persone').val(r[0].qta_persone);
                    $('#calendario_prendi_ordine .testo').val(r[0].testo);
                    $('#calendario_prendi_ordine .candeline_anni input').val(r[0].candeline_anni);
                    $("#selezione_gruppo_ordinazione option[value='" + r[0].selezione_gruppo_ordinazione + "']").prop('selected', true);
                    $("#ordinazione_pertinenza option[value='" + r[0].ordinazione_pertinenza + "']").prop('selected', true);
                    $("#avviso_ordinazione option[value='" + r[0].avviso + "']").prop('selected', true);
                    if (r[0].fontana === 'S') {
                        $("#calendario_prendi_ordine .fontana input").prop('checked', true);
                    } else {
                        $("#calendario_prendi_ordine .fontana input").prop('checked', false);
                    }

                    if (r[0].candeline === 'S') {
                        $("#calendario_prendi_ordine .candeline input").prop('checked', true);
                    } else {
                        $("#calendario_prendi_ordine .candeline input").prop('checked', false);
                    }
                    $("#calendario_prendi_ordine .titolo_personale").val(r[0].titolo_personale);
                    anteprima_articoli(calendario.progressivo_appuntamenti,function(){});
                    switch (r[0].ordinazione_pertinenza) {

                        case "F":
                            $("#calendario_prendi_ordine .tasto_ordinazione_fatta").hide();
                            $("#calendario_prendi_ordine .titolo").html("FERIE");
                            $("#calendario_prendi_ordine .nome_data").html("Da:");
                            $("#calendario_prendi_ordine .ferie").show();
                            $("#calendario_prendi_ordine .scegli_prodotti").hide();
                            $("#calendario_prendi_ordine .riquadro_prodotti").hide();
                            $("#calendario_prendi_ordine .qta_persone").hide();
                            $("#calendario_prendi_ordine .fontana").hide();
                            $("#calendario_prendi_ordine .candeline").hide();
                            $("#calendario_prendi_ordine .candeline_anni").hide();
                            $("#calendario_prendi_ordine .titolo_personale").hide();
                            $("#calendario_prendi_ordine .testo").removeClass("col-xs-5");
                            $("#calendario_prendi_ordine .testo").addClass("col-xs-11");

                            $("#calendario_prendi_ordine .tasto_stampa_ordinazione").hide();
                            break;
                        case "P":
                            
                           $("#calendario_prendi_ordine .tasto_ordinazione_fatta").hide();
                            $("#calendario_prendi_ordine .titolo").html("APPUNTAMENTO");
                            $("#calendario_prendi_ordine .nome_data").html("Da:");
                            $("#calendario_prendi_ordine .ferie").hide();
                            $("#calendario_prendi_ordine .scegli_prodotti").hide();
                            $("#calendario_prendi_ordine .riquadro_prodotti").hide();
                            $("#calendario_prendi_ordine .qta_persone").hide();
                            $("#calendario_prendi_ordine .fontana").hide();
                            $("#calendario_prendi_ordine .candeline").hide();
                            $("#calendario_prendi_ordine .candeline_anni").hide();
                            $("#calendario_prendi_ordine .titolo_personale").show();
                            $("#calendario_prendi_ordine .testo").removeClass("col-xs-5");
                            $("#calendario_prendi_ordine .testo").addClass("col-xs-11");

                            $("#calendario_prendi_ordine .tasto_stampa_ordinazione").show();
                            break;
                        case "L":
                        default:
                            $("#calendario_prendi_ordine .tasto_ordinazione_fatta").show();
                            $("#calendario_prendi_ordine .titolo").html("ORDINE");
                            $("#calendario_prendi_ordine .nome_data").html("Data:");
                            $("#calendario_prendi_ordine .ferie").hide();
                            $("#calendario_prendi_ordine .scegli_prodotti").show();
                            $("#calendario_prendi_ordine .riquadro_prodotti").show();
                            $("#calendario_prendi_ordine .qta_persone").show();
                            $("#calendario_prendi_ordine .fontana").show();
                            $("#calendario_prendi_ordine .candeline").show();
                            $("#calendario_prendi_ordine .candeline_anni").show();
                            $("#calendario_prendi_ordine .titolo_personale").hide();
                            $("#calendario_prendi_ordine .testo").removeClass("col-xs-11");
                            $("#calendario_prendi_ordine .testo").addClass("col-xs-5");

                            $("#calendario_prendi_ordine .tasto_stampa_ordinazione").show();
                    }


                    $('#calendario_prendi_ordine').modal('show');
                });
            });
        });
    });
}

function esci_ordine() {

    var campi_pieni = false;
    $('#calendario_prendi_ordine input,#calendario_prendi_ordine select,#calendario_prendi_ordine #campo_ordinazione .anteprima').each(function (a, b) {

        if ($(b).html() !== "" && b.value !== "" && b.value !== "L") {
            campi_pieni = true;
        }

    });
    if (campi_pieni === true && calendario.modifica_in_corso !== true) {
        bootbox.confirm("Sei sicuro di voler cancellare definitivamente tutti i dati inseriti?", function (conferma) {

            if (conferma === true) {
                $('#calendario_prendi_ordine').modal('hide');
                var obj = {"intestazione": "", "conto": ""};
                socket_fast_split.send(JSON.stringify(obj));

                var obj = {"totale": "0.00", "residuo": "Totale"};
                socket_fast_split.send(JSON.stringify(obj));
                calendario.tasti_asporto_nascosti = false;
            }

        });
    } else
    {
        $('#calendario_prendi_ordine').modal('hide');
        var obj = {"intestazione": "", "conto": ""};
        socket_fast_split.send(JSON.stringify(obj));

        var obj = {"totale": "0.00", "residuo": "Totale"};
        socket_fast_split.send(JSON.stringify(obj));
        calendario.tasti_asporto_nascosti = false;
        calendario.modifica_in_corso = undefined;
    }

}

function salva_ordine() {

    var id = calendario.progressivo_appuntamenti;
    var data_da_ordine = $("#calendario_prendi_ordine .data_da_ordine").val();
    var ora_da_ordine = $("#calendario_prendi_ordine .ora_da_ordine").val();
    var minuti_da_ordine = $("#calendario_prendi_ordine .minuti_da_ordine").val();
    var data_a_ordine = $("#calendario_prendi_ordine .data_a_ordine").val();
    var ora_a_ordine = $("#calendario_prendi_ordine .ora_a_ordine").val();
    var minuti_a_ordine = $("#calendario_prendi_ordine .minuti_a_ordine").val();
    var selezione_gruppo_ordinazione = $("#selezione_gruppo_ordinazione option:selected").val();
    var ordinazione_pertinenza = $("#ordinazione_pertinenza option:selected").val();
    var nome_ordinante = $("#calendario_prendi_ordine .nome_ordinante").val();
    var cognome_ordinante = $("#calendario_prendi_ordine .cognome_ordinante").val();
    var telefono = $("#calendario_prendi_ordine .telefono").val();
    var qta_persone = $("#calendario_prendi_ordine .qta_persone input").val();
    var titolo_personale = $("#calendario_prendi_ordine .titolo_personale input").val();
    var testo = $("#calendario_prendi_ordine .testo input").val();
    //CHECKBOX
    var fontana = $("#calendario_prendi_ordine .fontana input[type='checkbox']").is(':checked') ? 'S' : 'N';
    var candeline = $("#calendario_prendi_ordine .candeline input[type='checkbox']").is(':checked') ? 'S' : 'N';
    var candeline_anni = $("#calendario_prendi_ordine .candeline_anni input").val();
    var avviso_con_stampa = $("#calendario_prendi_ordine .avviso_con_stampa option:selected").val();
    $('#calendario_prendi_ordine').modal('hide');
    calendario.tasti_asporto_nascosti = false;
    var chiusura_conto = "update comanda set stato_record='APERTO',nome_comanda='C_" + id + "' where stato_record='ATTIVO' and ntav_comanda='CALENDARIO';";
    comanda.sock.send({tipo: "aggiornamento_calendario", operatore: comanda.operatore, query: chiusura_conto, terminale: comanda.terminale, ip: comanda.ip_address});
    comanda.sincro.query(chiusura_conto, function (res) {

        var sql = '';
        if (calendario.modifica_in_corso === true) {
            sql = 'update appuntamenti_calendario set da_data="' + data_da_ordine + '",da_ora="' + ora_da_ordine + '",da_minuti="' + minuti_da_ordine + '",a_data="' + data_a_ordine + '",a_ora="' + ora_a_ordine + '",a_minuti="' + minuti_a_ordine + '",selezione_gruppo_ordinazione="' + selezione_gruppo_ordinazione + '",ordinazione_pertinenza="' + ordinazione_pertinenza + '",nome="' + nome_ordinante + '",cognome="' + cognome_ordinante + '",telefono="' + telefono + '",qta_persone="' + qta_persone + '",titolo_personale="' + titolo_personale + '",testo="' + testo + '",fontana="' + fontana + '",candeline="' + candeline + '",candeline_anni="' + candeline_anni + '",avviso="' + avviso_con_stampa + '" WHERE id="' + id + '";';
            calendario.modifica_in_corso = undefined;
        } else
        {
            sql = "insert into appuntamenti_calendario \n\
                (id,da_data,da_ora,da_minuti,a_data,a_ora,a_minuti,selezione_gruppo_ordinazione,ordinazione_pertinenza,nome,cognome,telefono,qta_persone,titolo_personale,testo,fontana,candeline,candeline_anni,avviso) \n\
                values \n\
                ('" + id + "','" + data_da_ordine + "','" + ora_da_ordine + "','" + minuti_da_ordine + "','" + data_a_ordine + "','" + ora_a_ordine + "','" + minuti_a_ordine + "','" + selezione_gruppo_ordinazione + "','" + ordinazione_pertinenza + "','" + nome_ordinante + "','" + cognome_ordinante + "','" + telefono + "','" + qta_persone + "','" + titolo_personale + "','" + testo + "','" + fontana + "','" + candeline + "','" + candeline_anni + "','" + avviso_con_stampa + "');";
        }

        comanda.sock.send({tipo: "aggiornamento_calendario", operatore: comanda.operatore, query: sql, terminale: comanda.terminale, ip: comanda.ip_address});
        comanda.sincro.query(sql, function () {

            stampa_avvisi_programmati();


            calendario.crea(true);
            var obj = {"intestazione": "", "conto": ""};
            socket_fast_split.send(JSON.stringify(obj));

            var obj = {"totale": "0.00", "residuo": "Totale"};
            socket_fast_split.send(JSON.stringify(obj));
            comanda.sincro.query_cassa();
        });
    });
}

function elimina_ordine(id) {

    bootbox.confirm("Sei sicuro di voler cancellare questo ordine?", function (a) {

        if (a !== true) {

        } else {
            $('#calendario_prendi_ordine').modal('hide');

            var chiusura_conto = "update comanda set stato_record='ELIMINATO DA CALENDARIO' where id='" + id + "';";
            comanda.sock.send({tipo: "aggiornamento_calendario", operatore: comanda.operatore, query: chiusura_conto, terminale: comanda.terminale, ip: comanda.ip_address});
            comanda.sincro.query(chiusura_conto, function () {

                var sql = 'delete from appuntamenti_calendario WHERE id="' + id + '";';

                comanda.sock.send({tipo: "aggiornamento_calendario", operatore: comanda.operatore, query: sql, terminale: comanda.terminale, ip: comanda.ip_address});
                comanda.sincro.query(sql, function () {

                    stampa_avvisi_programmati();

                    calendario.crea(true);
                    comanda.sincro.query_cassa();
                });
            });
        }
    });


}

function salva_ordinazione() {

    comanda.stampa_comanda = false;
    comanda.tasto_indietro_parcheggio = true;
    anteprima_articoli(calendario.progressivo_appuntamenti,function(){
        
      btn_tavoli();
    $('#popup_calendario_ordinazioni').modal('show');
    $('#calendario_prendi_ordine').modal('show');  
        
    });
    
}

function anteprima_articoli(progressivo_appuntamenti,cb) {

    comanda.tavolo = "CALENDARIO";
    comanda.funzionidb.conto_attivo(function () {
        comanda.tavolo = "0";
        cb(true);
    });
}

function ordinazione_evasa() {

    var id = calendario.progressivo_appuntamenti;

var vedi_se_esiste="select id from appuntamenti_calendario where id='"+calendario.progressivo_appuntamenti+"' limit 1;";

 comanda.sincro.query(vedi_se_esiste, function (risultato) {
     
     if(risultato!==undefined&&risultato[0]!==undefined&&risultato[0].id!==undefined&&risultato[0].id===id){

    var sql = "update appuntamenti_calendario set ordine_evaso='S' where id='" + id + "' ;";

    comanda.sock.send({tipo: "aggiornamento_calendario", operatore: comanda.operatore, query: sql, terminale: comanda.terminale, ip: comanda.ip_address});

    comanda.sincro.query(sql, function () {

        var obj = {"intestazione": "", "conto": ""};
        socket_fast_split.send(JSON.stringify(obj));

        var obj = {"totale": "0.00", "residuo": "Totale"};
        socket_fast_split.send(JSON.stringify(obj));
        calendario.crea(true);

        comanda.sincro.query_cassa();

    });
}else
{
    bootbox.alert("Non puoi evadere un ordine NON salvato.");
}
 });
}

function scegli_tipologia_prodotti_ordinazione() {

    var nome_ordinante = $('#calendario_prendi_ordine .nome_ordinante').val();
    var cognome_ordinante = $('#calendario_prendi_ordine .cognome_ordinante').val();
    var ora_ordine = $('#calendario_prendi_ordine .ora_ordine').val();
    var minuti_ordine = $('#calendario_prendi_ordine .minuti_ordine').val();
    var data_ordine = $('#calendario_prendi_ordine .data_da_ordine').val();
    //Es. 2017-09-21

    var anno_ordine = data_ordine.substr(0, 4);
    var mese_ordine = data_ordine.substr(5, 2);
    var giorno_ordine = data_ordine.substr(8, 2);
    comanda.parcheggio = "C_" + calendario.progressivo_appuntamenti;
    comanda.ora_consegna = ora_ordine;
    comanda.minuti_consegna = minuti_ordine;
    //CONTINUA CASSA5886

    calendario.tasti_asporto_nascosti = true;
    comanda.categoria = comanda.categoria_calendario;
    blocco_concreto("CALENDARIO");
    $('#calendario_prendi_ordine').modal('hide');
    $('#popup_calendario_ordinazioni').modal('hide');
}

function stampa_ordinazione() {

    var data_da_ordine = $("#calendario_prendi_ordine .data_da_ordine").val();
    var ora_da_ordine = $("#calendario_prendi_ordine .ora_da_ordine").val();
    var minuti_da_ordine = $("#calendario_prendi_ordine .minuti_da_ordine").val();
    var data_a_ordine = $("#calendario_prendi_ordine .data_a_ordine").val();
    var ora_a_ordine = $("#calendario_prendi_ordine .ora_a_ordine").val();
    var minuti_a_ordine = $("#calendario_prendi_ordine .minuti_a_ordine").val();
    var selezione_gruppo_ordinazione = $("#selezione_gruppo_ordinazione option:selected").val();
    var ordinazione_pertinenza = $("#ordinazione_pertinenza option:selected").val();
    var nome_ordinante = $("#calendario_prendi_ordine .nome_ordinante").val();
    var cognome_ordinante = $("#calendario_prendi_ordine .cognome_ordinante").val();
    var telefono = $("#calendario_prendi_ordine .telefono").val();
    var qta_persone = $("#calendario_prendi_ordine .qta_persone input").val();
    var titolo_personale = $("#calendario_prendi_ordine .titolo_personale input").val();
    var testo = $("#calendario_prendi_ordine .testo input").val();
    //CHECKBOX
    var fontana = $("#calendario_prendi_ordine .fontana input[type='checkbox']").is(':checked') ? 'S' : 'N';
    var candeline = $("#calendario_prendi_ordine .candeline input[type='checkbox']").is(':checked') ? 'S' : 'N';
    var candeline_anni = $("#calendario_prendi_ordine .candeline_anni input").val();
    var avviso_con_stampa = $("#calendario_prendi_ordine .avviso_con_stampa option:selected").val();


    switch (ordinazione_pertinenza) {
        case "L":
            var data = comanda.funzionidb.data_attuale();
            var builder = new epson.ePOSBuilder();
            builder.addTextFont(builder.FONT_A);
            builder.addTextAlign(builder.ALIGN_CENTER);

            builder.addFeedLine(2);

            builder.addText("***********************************************\n");


            builder.addTextSize(2, 2);

            var a = new Date(data_da_ordine);

            var mese = calendario.mesi[a.getMonth()];
            var numero_giorno = a.getDate();
            var giorno = calendario.giorno_settimanale[((a.getDay() + 6) % 7)].capitalize();

            builder.addText(giorno + " " + numero_giorno + " " + mese + "\n");

            if (ora_da_ordine !== "") {
                if (minuti_da_ordine === "") {
                    minuti_da_ordine = "00";
                }
                builder.addText(ora_da_ordine + ":" + minuti_da_ordine + "\n");
            }

            if (qta_persone !== "") {
                builder.addText("x " + qta_persone + " PERSONE\n");
            }

            builder.addTextSize(1, 1);


            builder.addText("***********************************************\n");

            builder.addFeedLine(1);

            builder.addTextSize(2, 2);

            builder.addTextAlign(builder.ALIGN_LEFT);


            var articoli = $('#campo_ordinazione > table > tbody')[0].innerText;

            var righe = "";

            articoli.split("\n").forEach(function (r) {

                if (r.search(/\s\s\s\s\s\s\s\s\s/gi) !== -1) {
                    righe += r.replace(/\d+/, " ").replace(/\s\s\s\s\s\s\s\s\s/gi, "     ") + "\n";
                } else
                {
                    righe += r + "\n";
                }

            });



            builder.addText(righe + "\n");

            builder.addTextAlign(builder.ALIGN_CENTER);

            builder.addFeedLine(1);

            builder.addTextSize(1, 1);

            builder.addTextAlign(builder.ALIGN_LEFT);

            builder.addTextStyle(false, false, true, builder.COLOR_1);

            builder.addText("TESTO: ");

            builder.addTextStyle(false, false, false, builder.COLOR_1);

            builder.addText(testo.toUpperCase() + "\n");

            builder.addTextAlign(builder.ALIGN_LEFT);

            if (candeline === "S") {
                builder.addText("CON CANDELINE\n");
            }
            if (fontana === "S") {
                builder.addText("CON FONTANA\n");
            }
            if (candeline_anni !== "") {
                builder.addText("CON CANDELINE " + candeline_anni + " ANNI\n");
            }

            builder.addFeedLine(1);

            builder.addTextAlign(builder.ALIGN_CENTER);

            builder.addText("***********************************************\n");

            builder.addTextStyle(false, false, true, builder.COLOR_1);

            builder.addText("Ordine di:\n");

            builder.addTextStyle(false, false, false, builder.COLOR_1);

            builder.addTextSize(2, 2);

            builder.addText(nome_ordinante.toUpperCase() + " " + cognome_ordinante.toUpperCase() + "\n");

            if (telefono !== "") {
                builder.addText(telefono + "\n");
            }

            builder.addTextSize(1, 1);

            builder.addText("***********************************************\n");


            builder.addCut(builder.CUT_FEED);
            var request = builder.toString();
            //CONTENUTO CONTO
            //console.log(request);

            //Create a SOAP envelop
            var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
            //Create an XMLHttpRequest object
            var xhr = new XMLHttpRequest();
            //Set the end point address

            //var url = 'http://' + comanda.intelligent_non_fiscale + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
            var url = 'http://' + comanda.intelligent_non_fiscale + ':8000/SPOOLER?stampante=BAR';

            //Open an XMLHttpRequest object
            xhr.open('POST', url, true);
            //<Header settings>
            xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
            xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
            xhr.setRequestHeader('SOAPAction', '""');
            // Send print document
            xhr.send(soap);
            break;

        case "P":
            var data = comanda.funzionidb.data_attuale();
            var builder = new epson.ePOSBuilder();
            builder.addTextFont(builder.FONT_A);
            builder.addTextAlign(builder.ALIGN_CENTER);

            builder.addTextSize(2, 2);

            builder.addText("APPUNTAMENTO");

            builder.addTextSize(1, 1);

            builder.addFeedLine(1);

            builder.addText("***********************************************\n");


            builder.addTextSize(2, 2);

            var a = new Date(data_da_ordine);

            var mese = calendario.mesi[a.getMonth()];
            var numero_giorno = a.getDate();
            var giorno = calendario.giorno_settimanale[((a.getDay() + 6) % 7)].capitalize();

            builder.addText(giorno + " " + numero_giorno + " " + mese + "\n");

            builder.addText(ora_da_ordine + ":" + minuti_da_ordine + "\n");


            builder.addTextSize(1, 1);

            builder.addText("***********************************************\n");

            builder.addFeedLine(1);

            builder.addTextSize(2, 2);


            builder.addText(titolo_personale.toUpperCase() + "\n");

            builder.addText(telefono + "\n");

            builder.addText(nome_ordinante.toUpperCase() + " " + cognome_ordinante.toUpperCase() + "\n");

            builder.addFeedLine(1);

            builder.addTextSize(1, 1);

            builder.addText("***********************************************\n");

            builder.addText("Testo:\n");

            builder.addTextSize(2, 2);

            builder.addText(testo.toUpperCase() + "\n");

            builder.addTextSize(1, 1);

            builder.addText("***********************************************\n");


            builder.addCut(builder.CUT_FEED);
            var request = builder.toString();
            //CONTENUTO CONTO
            //console.log(request);

            //Create a SOAP envelop
            var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
            //Create an XMLHttpRequest object
            var xhr = new XMLHttpRequest();
            //Set the end point address

            //var url = 'http://' + comanda.intelligent_non_fiscale + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
            var url = 'http://' + comanda.intelligent_non_fiscale + ':8000/SPOOLER?stampante=BAR';

            //Open an XMLHttpRequest object
            xhr.open('POST', url, true);
            //<Header settings>
            xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
            xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
            xhr.setRequestHeader('SOAPAction', '""');
            // Send print document
            xhr.send(soap);
            break;

        case "F":

            break;
    }
}

function stampa_avviso(o) {

    var testo_query = "select  substr(desc_art,1,3)||'M'|| substr(nodo,1,3) as ord,c1.cod_promo,cod_articolo,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita from comanda as c1 where contiene_variante='1' and ntav_comanda='CALENDARIO'  and posizione='CONTO' and (stampata_sn is null or stampata_sn='null' or stampata_sn='n' or stampata_sn='')  and stato_record='APERTO' and nome_comanda='C_" + o.id + "' \n\
union all \n\
select  substr(desc_art,1,3)||'M'||substr(nodo,1,3) as ord,c1.cod_promo,cod_articolo,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,sum(quantita) as quantita from comanda as c1 where (contiene_variante !='1' or contiene_variante is null or contiene_variante='null') and length(nodo)=3 and ntav_comanda='CALENDARIO' and posizione='CONTO' and (stampata_sn is null or stampata_sn='null' or stampata_sn='n' or stampata_sn='')  and stato_record='APERTO' and nome_comanda='C_" + o.id + "' group by desc_art,nome_comanda \n\
union all \n\
select  (select substr(desc_art,1,3) from comanda where nodo=substr(c1.nodo,1,3) and stato_record='APERTO' and nome_comanda='C_" + o.id + "'  and ntav_comanda='CALENDARIO')||'M' ||substr(nodo,1,3) as ord,c1.cod_promo,cod_articolo,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita from comanda as c1 where length(nodo)=7 and substr(desc_art,1,1)='-' and ntav_comanda='CALENDARIO'   and posizione='CONTO' and (stampata_sn is null or stampata_sn='null' or stampata_sn='n' or stampata_sn='')  and stato_record='APERTO' and nome_comanda='C_" + o.id + "' \n\
union all \n\
select (select substr(desc_art,1,3) from comanda where nodo=substr(c1.nodo,1,3) and stato_record='APERTO' and nome_comanda='C_" + o.id + "'  and ntav_comanda='CALENDARIO')||'M'||substr(nodo,1,3) as ord,c1.cod_promo,cod_articolo,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita from comanda as c1 where length(nodo)=7 and substr(desc_art,1,1)!='-' and ntav_comanda='CALENDARIO'  and posizione='CONTO' and (stampata_sn is null or stampata_sn='null' or stampata_sn='n' or stampata_sn='')  and stato_record='APERTO' and nome_comanda='C_" + o.id + "' \n\
order by ord ASC;";
    comanda.sincro.query(testo_query, function (com) {

        var data_da_ordine = o.da_data;
        var ora_da_ordine = o.da_ora;
        var minuti_da_ordine = o.da_minuti;
        var data_a_ordine = o.a_data;
        var ora_a_ordine = o.a_ora;
        var minuti_a_ordine = o.a_minuti;
        var selezione_gruppo_ordinazione = o.selezione_gruppo_ordinazione;
        var ordinazione_pertinenza = o.ordinazione_pertinenza;
        var nome_ordinante = o.nome;
        var cognome_ordinante = o.cognome;
        var telefono = o.telefono;
        var qta_persone = o.qta_persone;
        var titolo_personale = o.titolo_personale;
        var testo = o.testo;
        //CHECKBOX
        var fontana = o.fontana;
        var candeline = o.candeline;
        var candeline_anni = o.candeline_anni;
        var avviso_con_stampa = o.avviso;
        switch (ordinazione_pertinenza) {
            case "L":
                var data = comanda.funzionidb.data_attuale();
                var builder = new epson.ePOSBuilder();
                builder.addTextFont(builder.FONT_A);
                builder.addTextAlign(builder.ALIGN_CENTER);
                builder.addTextSize(2, 2);
                builder.addText("AVVISO");
                builder.addTextSize(1, 1);
                builder.addFeedLine(1);
                builder.addText("***********************************************\n");
                builder.addTextSize(2, 2);
                var a = new Date(data_da_ordine);
                var mese = calendario.mesi[a.getMonth()];
                var numero_giorno = a.getDate();
                var giorno = calendario.giorno_settimanale[((a.getDay() + 6) % 7)].capitalize();
                builder.addText(giorno + " " + numero_giorno + " " + mese + "\n");
                if (ora_da_ordine !== "") {
                    if (minuti_da_ordine === "") {
                        minuti_da_ordine = "00";
                    }
                    builder.addText(ora_da_ordine + ":" + minuti_da_ordine + "\n");
                }

                if (qta_persone !== "") {
                    builder.addText("x " + qta_persone + " PERSONE\n");
                }

                builder.addTextSize(1, 1);
                builder.addText("***********************************************\n");
                builder.addFeedLine(1);
                builder.addTextSize(2, 2);
                builder.addTextAlign(builder.ALIGN_LEFT);
                var righe = "";
                com.forEach(function (a) {

                    if (a.nodo.length > 3) {
                        righe += "     ";
                    } else
                    {
                        righe += a.quantita;
                        righe += "  ";
                    }
                    righe += a.desc_art;
                    righe += "\n";
                });
                builder.addText(righe + "\n");
                builder.addTextAlign(builder.ALIGN_CENTER);
                builder.addFeedLine(1);
                builder.addTextSize(1, 1);
                builder.addTextAlign(builder.ALIGN_LEFT);
                builder.addTextStyle(false, false, true, builder.COLOR_1);
                builder.addText("TESTO: ");
                builder.addTextStyle(false, false, false, builder.COLOR_1);
                builder.addText(testo.toUpperCase() + "\n");
                builder.addTextAlign(builder.ALIGN_LEFT);
                if (candeline === "S") {
                    builder.addText("CON CANDELINE\n");
                }
                if (fontana === "S") {
                    builder.addText("CON FONTANA\n");
                }
                if (candeline_anni !== "") {
                    builder.addText("CON CANDELINE " + candeline_anni + " ANNI\n");
                }

                builder.addFeedLine(1);
                builder.addTextAlign(builder.ALIGN_CENTER);
                builder.addText("***********************************************\n");
                builder.addTextStyle(false, false, true, builder.COLOR_1);
                builder.addText("Ordine di:\n");
                builder.addTextStyle(false, false, false, builder.COLOR_1);
                builder.addTextSize(2, 2);
                builder.addText(nome_ordinante.toUpperCase() + " " + cognome_ordinante.toUpperCase() + "\n");
                if (telefono !== "") {
                    builder.addText(telefono + "\n");
                }

                builder.addTextSize(1, 1);
                builder.addText("***********************************************\n");
                builder.addCut(builder.CUT_FEED);
                var request = builder.toString();
                //CONTENUTO CONTO
                //console.log(request);

                //Create a SOAP envelop
                var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
                //Create an XMLHttpRequest object
                var xhr = new XMLHttpRequest();
                //Set the end point address

                //var url = 'http://' + comanda.intelligent_non_fiscale + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                var url = 'http://' + comanda.intelligent_non_fiscale + ':8000/SPOOLER?stampante=BAR';
                //Open an XMLHttpRequest object
                xhr.open('POST', url, true);
                //<Header settings>
                xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
                xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
                xhr.setRequestHeader('SOAPAction', '""');
                // Send print document
                xhr.send(soap);
                var sql = 'update appuntamenti_calendario set avviso_stampato="S" WHERE id="' + o.id + '";';
                comanda.sock.send({tipo: "aggiornamento_calendario", operatore: comanda.operatore, query: sql, terminale: comanda.terminale, ip: comanda.ip_address});
                comanda.sincro.query(sql, function () {
                    comanda.sincro.query_cassa();
                });
                break;
            case "P":
                var data = comanda.funzionidb.data_attuale();
                var builder = new epson.ePOSBuilder();
                builder.addTextFont(builder.FONT_A);
                builder.addTextAlign(builder.ALIGN_CENTER);
                builder.addTextSize(2, 2);
                builder.addText("AVVISO APPUNTAMENTO");
                builder.addTextSize(1, 1);
                builder.addFeedLine(1);
                builder.addText("***********************************************\n");
                builder.addTextSize(2, 2);
                var a = new Date(data_da_ordine);
                var mese = calendario.mesi[a.getMonth()];
                var numero_giorno = a.getDate();
                var giorno = calendario.giorno_settimanale[((a.getDay() + 6) % 7)].capitalize();
                builder.addText(giorno + " " + numero_giorno + " " + mese + "\n");
                builder.addText(ora_da_ordine + ":" + minuti_da_ordine + "\n");
                builder.addTextSize(1, 1);
                builder.addText("***********************************************\n");
                builder.addFeedLine(1);
                builder.addTextSize(2, 2);
                builder.addText(titolo_personale.toUpperCase() + "\n");
                builder.addText(telefono + "\n");
                builder.addText(nome_ordinante.toUpperCase() + " " + cognome_ordinante.toUpperCase() + "\n");
                builder.addFeedLine(1);
                builder.addTextSize(1, 1);
                builder.addText("***********************************************\n");
                builder.addText("Testo:\n");
                builder.addTextSize(2, 2);
                builder.addText(testo.toUpperCase() + "\n");
                builder.addTextSize(1, 1);
                builder.addText("***********************************************\n");
                builder.addCut(builder.CUT_FEED);
                var request = builder.toString();
                //CONTENUTO CONTO
                //console.log(request);

                //Create a SOAP envelop
                var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
                //Create an XMLHttpRequest object
                var xhr = new XMLHttpRequest();
                //Set the end point address

                //var url = 'http://' + comanda.intelligent_non_fiscale + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                var url = 'http://' + comanda.intelligent_non_fiscale + ':8000/SPOOLER?stampante=BAR';
                //Open an XMLHttpRequest object
                xhr.open('POST', url, true);
                //<Header settings>
                xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
                xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
                xhr.setRequestHeader('SOAPAction', '""');
                // Send print document
                xhr.send(soap);
                var sql = 'update appuntamenti_calendario set avviso_stampato="S" WHERE id="' + o.id + '";';
                comanda.sock.send({tipo: "aggiornamento_calendario", operatore: comanda.operatore, query: sql, terminale: comanda.terminale, ip: comanda.ip_address});
                comanda.sincro.query(sql, function () {
                    comanda.sincro.query_cassa();
                });
                break;
            case "F":

                break;
        }
    });
}

var array_timeouts_calendario = new Array();
function stampa_avvisi_programmati() {

    for (var i in array_timeouts_calendario) {
        clearTimeout(array_timeouts_calendario[i]);
    }

    var sql = "select * from appuntamenti_calendario where avviso!='' and ordine_evaso='' and avviso_stampato='';";
    comanda.sincro.query(sql, function (r) {



        var data_adesso = new Date();
        r.forEach(function (o) {

            o.da_ora === "" ? "00" : o.da_ora;
            o.da_minuti === "" ? "00" : o.da_minuti;
            var data_avviso = new Date(o.da_data + " " + o.da_ora + ":" + o.da_minuti);
            switch (o.avviso) {
                case "1":
                    data_avviso.setMinutes(data_avviso.getMinutes() - 30);
                    break;
                case "2":
                    data_avviso.setHours(data_avviso.getHours() - 1);
                    break;
                case "3":
                    data_avviso.setHours(data_avviso.getHours() - 2);
                    break;
                case "4":
                    data_avviso.setHours(data_avviso.getHours() - 3);
                    break;
                case "5":
                    data_avviso.setHours(data_avviso.getHours() - 6);
                    break;
                case "6":
                    data_avviso.setDate(data_avviso.getDate() - 1);
                    break;
                case "7":
                    data_avviso.setDate(data_avviso.getDate() - 2);
                    break;
            }

            if (data_adesso >= data_avviso) {

                stampa_avviso(o);
            } else if (data_adesso < data_avviso) {

                var differenza = data_avviso.getTime() - data_adesso.getTime();
                //Se sono meno di 24 ore di differenza
                if (differenza <= 8, 64e+7) {

                    array_timeouts_calendario.push(setTimeout(function () {

                        stampa_avviso(o);
                    }, differenza));
                }

            }

        });
    });
}