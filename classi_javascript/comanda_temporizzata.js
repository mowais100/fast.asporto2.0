
/* global alasql, comanda, parseFloat */

let secondi_stampa_comanda_temporizzata = 0;
let secondi_stampa_comanda_temporizzata_domicilio = 0;

function dati_comanda_temporizzata(varianti_unite, CallBack, avvio_software) {



    this.funzione_interna = function (id_comanda) {
        if (varianti_unite === true) {

            let field_prezzo_un = "prezzo_vero";

            if (comanda.pizzeria_asporto === true) {
                field_prezzo_un += " as prezzo_un";
            }

            var testo_query = "select cod_articolo,desc_art,quantita," + field_prezzo_un + ",nodo,prog_inser,contiene_variante,categoria,dest_stampa,dest_stampa_2,portata,(select desc_art from comanda as c2 where id='" + id_comanda + "' and stato_record='FORNO'   and c2.nodo=substr(c1.nodo,1,3)) as articolo_main from comanda as c1 where id='" + id_comanda + "' and stato_record='FORNO' order by articolo_main,nodo asc;";

            let risultato = alasql(testo_query);

            var conto = new Array();

            risultato.forEach(function (obj) {

                var posizione = -1; //INDEX OF FOUND ELEMENT IN CONTO

                //Solo per gli articoli principali
                if (obj.desc_art[0] !== '+' && obj.desc_art[0] !== '-' && obj.desc_art[0] !== '(' && obj.desc_art[0] !== '*' && obj.desc_art[0] !== '=') {

                    //Iteratore per vedere se ci sono articoli uguali
                    conto.some(function (iterator, index, _ary) {

                        console.log("CONFRONTO ARTICOLI UGUALI", iterator.desc_art, obj.desc_art);
                        if (iterator.desc_art === obj.desc_art) {
                            posizione = index;
                            return true;
                        }

                    });
                    console.log("POSIZIONE TROVATA: ", posizione);
                    //FINE ITERATORE

                    console.log(posizione);
                    if (posizione !== -1) {
                        conto[posizione].quantita = (parseInt(conto[posizione].quantita) + parseInt(obj.quantita)).toString();
                    } else {
                        conto.push(obj);
                    }
                }
                //Solo per le varianti
                else {


                    //Se la variante appartiene allo stesso articolo di quella di prima la unisce nella stessa riga
                    //NB Quella di prima però dev'essere una variante
                    if (conto[conto.length - 1] !== undefined && conto[conto.length - 1].nodo !== undefined && conto[conto.length - 1].nodo.length > 3 && obj.nodo.substr(0, 3) === conto[conto.length - 1].nodo.substr(0, 3)) {
                        conto[conto.length - 1].desc_art += ' ' + obj.desc_art;
                        conto[conto.length - 1].prezzo_un = parseFloat(conto[conto.length - 1].prezzo_un) + parseFloat(obj.prezzo_un);
                    } else {
                        conto.push(obj);
                    }
                }

            });
            var conto_varianti_perfezionate = new Array();
            conto.forEach(function (obj) {

                console.log(obj);
                var posizione = -1; //INDEX OF FOUND ELEMENT IN CONTO

                //Solo per gli articoli principali
                if (obj.desc_art[0] !== '+' && obj.desc_art[0] !== '-' && obj.desc_art[0] !== '(' && obj.desc_art[0] !== '*' && obj.desc_art[0] !== '=') {

                    //Iteratore per vedere se ci sono articoli uguali
                    conto_varianti_perfezionate.some(function (iterator, index, _ary) {

                        console.log(iterator.desc_art, obj.desc_art);
                        if (iterator.desc_art === obj.desc_art) {

                            posizione = index;
                            return true;
                        }

                    });
                    console.log("POSIZIONE TROVATA: ", posizione);
                    //FINE ITERATORE

                    console.log(posizione);
                    if (posizione !== -1) {
                        conto_varianti_perfezionate[posizione].quantita = (parseInt(conto_varianti_perfezionate[posizione].quantita) + parseInt(obj.quantita)).toString();
                    } else {
                        conto_varianti_perfezionate.push(obj);
                    }
                } else {
                    //Se la variante ha lo stesso nome dell' articolo precedente aggiunge solo la quantita

                    var quantita_aumentata = false;

                    conto_varianti_perfezionate.forEach(function (a, b) {

                        if (a.desc_art === obj.desc_art) {
                            conto_varianti_perfezionate[b].quantita = (parseInt(conto_varianti_perfezionate[b].quantita) + parseInt(obj.quantita)).toString();

                            quantita_aumentata = true;
                        }

                    });

                    if (quantita_aumentata === false) {
                        conto_varianti_perfezionate.push(obj);
                    }
                }

            });
            console.log(conto_varianti_perfezionate);

            if (typeof CallBack === "function") {
                CallBack(conto_varianti_perfezionate, id_comanda);
            }


        } else {

            alasql("DELETE FROM oggetto_comanda");
            let field_prezzo_un = "prezzo_vero";
            if (comanda.pizzeria_asporto === true) {
                field_prezzo_un += " as prezzo_un";
            }

            var r1 = alasql("select cod_articolo,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,dest_stampa_2,portata,categoria,prog_inser,nodo,desc_art," + field_prezzo_un + ",quantita,contiene_variante from comanda where id='" + id_comanda + "'   and stato_record='FORNO'  and posizione='CONTO' ;");

            var ord = "";
            var raggruppamento = new Object();
            r1.forEach(function (e) {
                if (e.contiene_variante === "1") {
                    ord = e.desc_art.substr(0, 3) + "M" + e.nodo;
                } else if (e.nodo.length === 7) {
                    var r2 = alasql("select substr(desc_art,1,3) as desc_art from comanda where nodo='" + e.nodo.substr(0, 3) + "' and id='" + id_comanda + "'  and stato_record='FORNO'  limit 1;");
                    ord = r2[0].desc_art + "M" + e.nodo;
                } else if (e.contiene_variante !== "1") {
                    ord = e.desc_art.substr(0, 3) + "M" + e.nodo;
                }


                alasql.tables.oggetto_comanda.data.push({ 'ord': ord, 'cod_articolo': e.cod_articolo, 'perc_iva': e.perc_iva, 'nome_comanda': e.nome_comanda, 'stampata_sn': e.stampata_sn, 'numero_conto': e.numero_conto, 'dest_stampa': e.dest_stampa, 'dest_stampa_2': e.dest_stampa_2, 'portata': e.portata, 'categoria': e.categoria, 'prog_inser': e.prog_inser, 'nodo': e.nodo, 'desc_art': e.desc_art, 'prezzo_un': e.prezzo_un, 'quantita': e.quantita, 'contiene_variante': e.contiene_variante });
            });

            alasql("select * from oggetto_comanda where contiene_variante!='1' and stato_record='FORNO'  and  length(nodo)=3;").forEach(function (e) {
                if (raggruppamento[e.desc_art] === undefined) {
                    raggruppamento[e.desc_art] = new Object();
                }

                if (raggruppamento[e.desc_art][e.nome_comanda] === undefined) {
                    e.quantita = parseInt(e.quantita);
                    raggruppamento[e.desc_art][e.nome_comanda] = e;
                } else {
                    raggruppamento[e.desc_art][e.nome_comanda].quantita += parseInt(e.quantita);
                }
            });

            alasql("delete from oggetto_comanda where contiene_variante!='1'  and stato_record='FORNO' and  length(nodo)=3;");

            //FOR RAGGRUPPAMENTO

            for (var desc_art in raggruppamento) {
                for (var nome_comanda in raggruppamento[desc_art]) {
                    alasql.tables.oggetto_comanda.data.push(raggruppamento[desc_art][nome_comanda]);
                }
            }

            raggruppamento = {};
            r1 = {};

            raggruppamento = null;
            r1 = null;
            //delete raggruppamento;
            //delete r1;

            var res = alasql("select * from oggetto_comanda order by nome_comanda DESC, ord ASC;");

            //QUESTA E' LA COMANDA DIVISA PER NODI
            var comanda_riunita_diviggiano = new Object();

            res.forEach(function (el, key) {
                if (comanda_riunita_diviggiano[el.ord] === undefined) {
                    comanda_riunita_diviggiano[el.ord] = new Array();
                }

                comanda_riunita_diviggiano[el.ord].push(el);
            });

            //QUESTA E' LA COMANDA RIUNITA IN UN OGGETTO
            var comanda_finale = new Object();

            for (var ord in comanda_riunita_diviggiano) {

                //Slice serve a copiare l'array senza puntarci dentro
                var array_vecchio = JSON.parse(JSON.stringify(comanda_riunita_diviggiano[ord]));

                array_vecchio.forEach(function (el, key, arr) {
                    arr[key].ord = "";
                    arr[key].prog_inser = "";
                    arr[key].nodo = "";
                    arr[key].quantita = "";
                });


                var bool = false;
                for (var ord2 in comanda_finale) {

                    //Slice serve a copiare l'array senza puntarci dentro
                    var array_comanda = JSON.parse(JSON.stringify(comanda_finale[ord2]));

                    array_comanda.forEach(function (el, key, arr) {
                        arr[key].ord = "";
                        arr[key].prog_inser = "";
                        arr[key].nodo = "";
                        arr[key].quantita = "";
                    });

                    //Bisogna confrontare l'array diviggiano con tutti i comanda

                    //Se in comanda non ce n'è uno con lunghezza uguale e stessi elementi lo si aggiunge a comanda stesso


                    //condizione 1 : controllo che la lunghezza dell'array sia la stessa in entrambi gli oggetti
                    if (array_vecchio.length === array_comanda.length) {

                        if (JSON.stringify(array_vecchio.sort(mySorter)) == JSON.stringify(array_comanda.sort(mySorter))) {

                            if (comanda_finale[ord2][0].contiene_variante !== "1" && comanda_finale[ord2][0].desc_art[0] !== "-" && comanda_finale[ord2][0].desc_art[0] !== "+" && comanda_finale[ord2][0].desc_art[0] !== "x" && comanda_finale[ord2][0].desc_art[0] !== "*" && comanda_finale[ord2][0].desc_art[0] !== "(" && comanda_finale[ord2][0].desc_art[0] !== "=") {

                                comanda_finale[ord2][0].quantita = parseInt(comanda_finale[ord2][0].quantita) + 1;

                                bool = true;

                            }

                        }

                    }

                }

                if (bool !== true) {

                    comanda_finale[ord] = comanda_riunita_diviggiano[ord];

                }

            }

            //QUESTO E' L'OGGETTO FINALE TRASFORMATO IN ARRAY
            var oggetto_finale_comanda = new Array();
            for (var key in comanda_finale) {
                comanda_finale[key].forEach(function (el, key) {
                    oggetto_finale_comanda.push(el);
                });
            }


            console.log("COMANDA RIUNITA DIVIGGIANO", res, comanda_riunita_diviggiano, comanda_finale, oggetto_finale_comanda);

            if (typeof CallBack === "function") {
                CallBack(oggetto_finale_comanda, id_comanda);
            }

        }
    };


    if (avvio_software !== true) {

        let data = new Date();
        let data_domicilio = new Date();

        /* 300 vuol dire 5 minuti */
        data.setSeconds(data.getSeconds() + secondi_stampa_comanda_temporizzata);

        data_domicilio.setSeconds(data_domicilio.getSeconds() + secondi_stampa_comanda_temporizzata_domicilio);

        let data_attuale = data.format("yyyy-mm-dd");

        let ora_consegne_incluse = data.format("HH:MM");

        let ora_consegne_incluse_domicilio = data_domicilio.format("HH:MM");

        let query_comanda_temp = "select distinct id,ora_consegna from comanda  where ((ora_consegna<='" + ora_consegne_incluse + "' and tipo_consegna='RITIRO') OR (ora_consegna<='" + ora_consegne_incluse_domicilio + "' and tipo_consegna='DOMICILIO')) and stampata_sn!='S' and stato_record='FORNO' and ntav_comanda LIKE 'ASPORTO%' and data_comanda='" + data_attuale + "' and ora_consegna!='' order by ora_consegna asc;";

        let id_comande_temporizzate = alasql(query_comanda_temp);

        id_comande_temporizzate.forEach(ict => {

            let query_verifica_comanda_gia_stampata_anche_parzialmente = "select id from comanda  where id='" + ict.id + "' and stampata_sn='S' and stato_record='FORNO' order by stampata_sn desc limit 1;";

            let verifica_comanda_gia_stampata_anche_parzialmente = alasql(query_verifica_comanda_gia_stampata_anche_parzialmente);

            /*solo se non è stata stampata neanche parzialmente la comanda la stampa temporizzata altrimenti resta così com è*/
            /*nel secondo caso solo a mano si può integrare o modificare la comanda*/
            if (verifica_comanda_gia_stampata_anche_parzialmente.length === 0) {
                this.funzione_interna(ict.id);
            }
        });

    }
}

async function stampa_comanda_temporizzata() {

    test_copertura(function () {

        if (comanda.pizzeria_asporto !== true && comanda.tavolo.left(7) === "ASPORTO") {
            funzione_interna_comanda_temporizzata();
        } else if (comanda.pizzeria_asporto !== true && (comanda.tavolo === "BANCO" || comanda.tavolo === "BAR" || comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === "TAKEAWAY" || comanda.tavolo === "TAKE AWAY")) {
            comanda.stampato = false;
            btn_parcheggia();
        } else {
            funzione_interna_comanda_temporizzata();
        }



    });


}


function funzione_interna_comanda_temporizzata() {

    var query = "select * from settaggi_profili where id=" + comanda.folder_number + " ;";
    let settaggi_profili = alasql(query);
    settaggi_profili = settaggi_profili[0];



    var varianti_unite = false;

    if (settaggi_profili.Field135 === 'true') {
        varianti_unite = true;
    }



    dati_comanda_temporizzata(varianti_unite, function (result, id_comanda) {

        funzione_interna_comanda_temporizzata_2(result, id_comanda);

    });

}

function funzione_interna_comanda_temporizzata_2(result, id_comanda) {

    var testo_query = "select id,ricetta from prodotti;";
    let result_prodotti = alasql(testo_query);

    var corrispondenza_id_prodotti = new Object();

    result_prodotti.forEach(function (element) {

        corrispondenza_id_prodotti[element.id] = element.ricetta;

    });

    let numero_consegna_domicilio = "";

    comanda.stampato = true;


    var query = "select * from settaggi_profili where id=" + comanda.folder_number + " ;";
    let settaggi_profili = alasql(query);
    settaggi_profili = settaggi_profili[0];


    var array_comanda = new Array();
    var array_dest_stampa = new Array();
    var array_portata = new Array();

    let query_dati_comanda = "select id,consegna_gap,ora_consegna,tipo_consegna,orario_preparazione,nome_comanda,jolly,rag_soc_cliente from comanda  where stato_record='FORNO' and ntav_comanda LIKE 'ASPORTO%' and id='" + id_comanda + "' order by nome_comanda desc,tipo_consegna desc, ora_consegna desc,ora_preparazione desc,jolly desc,rag_soc_cliente desc limit 1;";

    let corpi_comanda = alasql(query_dati_comanda);
    if(corpi_comanda[0].rag_soc_cliente >0)
    {
        let note_check = "select note from clienti where id=" + corpi_comanda[0].rag_soc_cliente + ";";
        let note_query = alasql(note_check);
        let note = note_query[0].note;
    }
    
  

    let dati_estratti = new Object();
    dati_estratti['rag_soc_cliente'] = "";
    dati_estratti['ora_consegna'] = "";
    dati_estratti['tipo_consegna'] = "";
    dati_estratti['orario_preparazione'] = "";
    dati_estratti['nome_cliente'] = "";
    dati_estratti['esecuzione_comanda_immediata'] = "";
    dati_estratti['consegna_gap'] = "";

    corpi_comanda.forEach(v => {
        dati_estratti['rag_soc_cliente'] = v.rag_soc_cliente;
        dati_estratti['tipo_consegna'] = v.tipo_consegna;
        dati_estratti['ora_consegna'] = v.ora_consegna;
        dati_estratti['orario_preparazione'] = v.orario_preparazione;
        dati_estratti['nome_cliente'] = v.nome_comanda;
        dati_estratti['consegna_gap'] = v.consegna_gap;
        if (v.jolly === "SUBITO") {
            dati_estratti['esecuzione_comanda_immediata'] = "SUBITO";
        }
    });


    let nome = "", cognome = "", ragione_sociale = "", indirizzo = "", numero = "", cap = "", citta = "", comune = "", provincia = "", cellulare = "", telefono = "", destinazione = "";

    if (dati_estratti['tipo_consegna'] === "DOMICILIO") {

        if (comanda.numero_licenza_cliente === "0629") {

            /*fa_richiesta_progressivo_domicilio() può essere anche non asincrona dato che dentro non contiene funzioni asincrone*/
            numero_consegna_domicilio = fa_richiesta_progressivo_domicilio(id_comanda);

        }

        if (settaggi_profili.Field252 === 'true' || settaggi_profili.Field253 === 'true') {

            var cliente = alasql("select * from clienti where id=" + dati_estratti['rag_soc_cliente'] + ";");

            if (cliente.length > 0) {
                nome = cliente[0].nome;
                cognome = cliente[0].cognome;
                ragione_sociale = cliente[0].ragione_sociale;
                indirizzo = cliente[0].indirizzo;
                numero = cliente[0].numero;
                cap = cliente[0].cap;
                citta = cliente[0].citta;
                comune = cliente[0].comune;
                provincia = cliente[0].provincia;
                cellulare = cliente[0].cellulare;
                telefono = cliente[0].telefono;
                destinazione = indirizzo + ", " + numero + " " + comune + " " + " " + cap + " " + provincia;
            }
        }

    }

    console.log(result);

    let totale_ordine = 0.00;
    let totale_consegna = 0.00;

    let field_prezzo_un = "prezzo_vero";

    if (comanda.pizzeria_asporto === true) {
        field_prezzo_un += " as prezzo_un";
    }

    let query_prezzi_comanda = "select " + field_prezzo_un + ",QTA,quantita,sconto_perc from comanda  where stato_record='FORNO' and ntav_comanda LIKE 'ASPORTO%' and id='" + id_comanda + "';";

    let prezzi_comanda = alasql(query_prezzi_comanda);

    totale_ordine = prezzi_comanda.reduce((accumulator, obj) => {
        if (obj.prezzo_un === "" || isNaN(obj.prezzo_un)) {
            obj.prezzo_un = "0";
        }
        if (obj.quantita === "" || isNaN(obj.quantita)) {
            obj.quantita = "0";
        }
        if (obj.QTA === "" || isNaN(obj.QTA)) {
            obj.QTA = "0";
        }
        return accumulator + ((parseFloat(obj.prezzo_un) + parseFloat(obj.QTA)) * parseInt(obj.quantita));
    }, 0);
    totale_ordine = totale_ordine.toFixed(2);

    totale_consegna = prezzi_comanda.reduce((accumulator, obj) => {
        if (obj.prezzo_un === "" || isNaN(obj.prezzo_un)) {
            obj.prezzo_un = "0";
        }
        if (obj.quantita === "" || isNaN(obj.quantita)) {
            obj.quantita = "0";
        }
        if (obj.QTA === "" || isNaN(obj.QTA)) {
            obj.QTA = "0";
        }
        return accumulator + (parseFloat(obj.QTA) * parseInt(obj.quantita));
    }, 0);
    totale_consegna = totale_consegna.toFixed(2);

    let nome_cliente = dati_estratti['nome_cliente'];
    let orario_preparazione = dati_estratti['orario_preparazione'];

    var progressivo = 0;
    result.forEach(function (obj) {

        try {
            var descrizione = obj['desc_art'];
            var prezzo = "0.00";
            if (!isNaN(obj['prezzo_un'])) {
                prezzo = obj['prezzo_un'];
            }

            var nodo = obj['nodo'];
            var prog_inser= obj['prog_inser'];
            var quantita = obj['quantita'];


            var categoria = obj['categoria'];
            var dest_stampa = obj['dest_stampa'];
            var portata = obj['portata'];
            var cod_articolo = obj['cod_articolo'];
            if (cod_articolo !== undefined && cod_articolo !== 'null' && cod_articolo.length > 0) {
                console.log("ricette", corrispondenza_id_prodotti, cod_articolo);
                var ricetta = corrispondenza_id_prodotti[cod_articolo];
            }

            if (array_comanda[dest_stampa] === undefined) {
                array_comanda[dest_stampa] = new Object();
            }

            if (array_dest_stampa[dest_stampa] === undefined) {
                array_dest_stampa[dest_stampa] = new Object();
                array_dest_stampa[dest_stampa].val = comanda.nome_stampante[dest_stampa].nome;
                array_dest_stampa[dest_stampa].dimensione = comanda.nome_stampante[dest_stampa].dimensione;
                array_dest_stampa[dest_stampa].ip = comanda.nome_stampante[dest_stampa].ip;
                array_dest_stampa[dest_stampa].intelligent = comanda.nome_stampante[dest_stampa].intelligent;
                array_dest_stampa[dest_stampa].fiscale = comanda.nome_stampante[dest_stampa].fiscale;
                array_dest_stampa[dest_stampa].nome_umano = comanda.nome_stampante[dest_stampa].nome_umano;
                array_dest_stampa[dest_stampa].devid_nf = comanda.nome_stampante[dest_stampa].devid_nf;
                array_dest_stampa[dest_stampa].devicetype = comanda.nome_stampante[dest_stampa].devicetype;
                array_dest_stampa[dest_stampa].devicemodel = comanda.nome_stampante[dest_stampa].devicemodel;
            }

            if (array_comanda[dest_stampa][portata] === undefined) {
                array_comanda[dest_stampa][portata] = new Object();
                array_comanda[dest_stampa][portata].val = comanda.nome_portata[portata];
                array_comanda[dest_stampa][portata].portata = portata;
            }

            if (array_comanda[dest_stampa][portata][(progressivo + 1)] === undefined) {
                array_comanda[dest_stampa][portata][(progressivo + 1)] = [quantita, descrizione, prezzo, ricetta, categoria,"N",cod_articolo,prog_inser];
            }

            progressivo++;


            //DESTINAZIONE STAMPA 2

            if (obj['cod_promo'] === "V_1") {
                var descrizione = obj['desc_art'].replace(/&#37;/gi, "%").replace(/&#8364;/gi, "�").replace(/=/gi, "...");
            } else {
                var descrizione = obj['desc_art'].replace(/&#37;/gi, "%").replace(/&#8364;/gi, "�");
            }
            var prezzo = "0.00";
            if (!isNaN(obj['prezzo_un'])) {
                prezzo = obj['prezzo_un'];
            }
            var nodo = obj['nodo'];
            var prog_inser= obj['prog_inser'];
            var quantita = obj['quantita'];

            var cod_articolo = obj['cod_articolo'];
            if (cod_articolo !== undefined && cod_articolo !== 'null' && cod_articolo.length > 0) {
                console.log("ricette", corrispondenza_id_prodotti, cod_articolo);
                var ricetta = corrispondenza_id_prodotti[cod_articolo];
            }

            var dest_stampa = obj['dest_stampa_2'];
            var portata = obj['portata'];
            var tasto_segue = obj['tasto_segue'];
            var categoria = obj['categoria'];

            //Aggiunto !==undefined 25/09/2019
            if (dest_stampa !== undefined && dest_stampa !== null && dest_stampa !== "" && dest_stampa !== "undefined" && dest_stampa !== "null" && dest_stampa !== obj['dest_stampa'] && obj['dest_stampa'] !== "T") {


                if (array_comanda[dest_stampa] === undefined) {
                    array_comanda[dest_stampa] = new Object();
                }

                if (array_dest_stampa[dest_stampa] === undefined) {
                    array_dest_stampa[dest_stampa] = new Object();
                    array_dest_stampa[dest_stampa].val = comanda.nome_stampante[dest_stampa].nome;
                    array_dest_stampa[dest_stampa].dimensione = comanda.nome_stampante[dest_stampa].dimensione;
                    array_dest_stampa[dest_stampa].ip = comanda.nome_stampante[dest_stampa].ip;
                    array_dest_stampa[dest_stampa].intelligent = comanda.nome_stampante[dest_stampa].intelligent;
                    array_dest_stampa[dest_stampa].fiscale = comanda.nome_stampante[dest_stampa].fiscale;
                    array_dest_stampa[dest_stampa].nome_umano = comanda.nome_stampante[dest_stampa].nome_umano;
                    array_dest_stampa[dest_stampa].devid_nf = comanda.nome_stampante[dest_stampa].devid_nf;
                    array_dest_stampa[dest_stampa].devicetype = comanda.nome_stampante[dest_stampa].devicetype;
                    array_dest_stampa[dest_stampa].devicemodel = comanda.nome_stampante[dest_stampa].devicemodel;
                }

                if (array_comanda[dest_stampa][portata] === undefined) {
                    array_comanda[dest_stampa][portata] = new Object();
                    array_comanda[dest_stampa][portata].val = comanda.nome_portata[portata];
                    array_comanda[dest_stampa][portata].portata = portata;
                }

                if (array_comanda[dest_stampa][portata][(progressivo + 1)] === undefined) {
                    array_comanda[dest_stampa][portata][(progressivo + 1)] = [quantita, descrizione, prezzo, ricetta, categoria,"D",cod_articolo,prog_inser];
                }

                progressivo++;

            }



        } catch (e) {

            throw descrizione;


        }
    });
    //---COMANDA INTELLIGENT---//

    for (var dest_stampa in array_comanda) {

        console.log("NOME STAMPANTI", comanda.nome_stampante[dest_stampa]);
        switch (array_dest_stampa[dest_stampa].intelligent) {
            case "s":
            case "SDS":


                if (array_dest_stampa[dest_stampa].fiscale === 'n') {

                    var concomitanze_presenti = true;
                    console.log("STAMPA COMANDA INTELLIGENT", array_dest_stampa[dest_stampa].ip);
                    var com = '';
                    var data = comanda.funzionidb.data_attuale();
                    var builder = new epson.ePOSBuilder();
                   
                    var messaggio_finale = new epson.ePOSBuilder();
                    var parola_conto = comanda.lang_stampa[11];
                    builder.addTextFont(builder.FONT_A);
                    builder.addTextAlign(builder.ALIGN_CENTER);
                    builder.addTextSize(1, 1);
                    if (settaggi_profili.Field111.trim().length > 0) {
                        switch (settaggi_profili.Field112) {
                            case "G":
                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                break;
                            case "N":
                            default:
                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                        }
                        builder.addText(settaggi_profili.Field111 + '\n');
                        if (settaggi_profili.Field113 === 'true') {
                            builder.addFeedLine(1);
                        }
                    }


                    if (settaggi_profili.Field114.trim().length > 0) {
                        switch (settaggi_profili.Field115) {
                            case "G":
                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                break;
                            case "N":
                            default:
                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                        }
                        builder.addText(settaggi_profili.Field114 + '\n');
                        if (settaggi_profili.Field116 === 'true') {
                            builder.addFeedLine(1);
                        }
                    }


                    if (settaggi_profili.Field117.trim().length > 0) {
                        switch (settaggi_profili.Field118) {
                            case "G":
                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                break;
                            case "N":
                            default:
                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                        }
                        builder.addText(settaggi_profili.Field117 + '\n');
                        if (settaggi_profili.Field119 === 'true') {
                            builder.addFeedLine(1);
                        }
                    }


                    if (settaggi_profili.Field120.trim().length > 0) {
                        switch (settaggi_profili.Field121) {
                            case "G":
                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                break;
                            case "N":
                            default:
                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                        }
                        builder.addText(settaggi_profili.Field120 + '\n');
                    }

                    builder.addText('\n\n');
                    builder.addTextSize(2, 2);
                    if (settaggi_profili.Field122 === 'true') {
                        builder.addText(parola_conto + '\n');
                    }

                    builder.addTextFont(builder.FONT_A);
                    builder.addFeedLine(1);
                    if (settaggi_profili.Field123 === 'true') {
                        builder.addText('' + array_dest_stampa[dest_stampa].nome_umano + '\n');
                        builder.addFeedLine(1);
                    }

                    builder.addFeedLine(1);
                    builder.addTextSize(2, 2);
                    builder.addTextAlign(builder.ALIGN_LEFT);

                    builder.addTextAlign(builder.ALIGN_CENTER);
                    builder.addText(nome_cliente + '\n');
                    builder.addTextAlign(builder.ALIGN_LEFT);



                    builder.addTextSize(1, 1);
                    builder.addFeedLine(1);
                    builder.addTextStyle(false, false, true, builder.COLOR_1);

                    builder.addTextAlign(builder.ALIGN_CENTER);
                    builder.addTextSize(2, 2);



                    if (dati_estratti['esecuzione_comanda_immediata'] === true) {

                        builder.addText('FARE SUBITO !\n');
                    } else {

                        /*bool_orario_comanda_visu_alta*/
                        if (settaggi_profili.Field469 === "true") {

                            /*tipo_orario_comanda_visu_alta */
                            if (settaggi_profili.Field471 === "P") {
                                if (orario_preparazione.length === 5) {
                                    builder.addText('PREPARAZIONE ' + orario_preparazione + '\n');
                                }
                            }

                            /*tipo_orario_comanda_visu_alta */
                            if (settaggi_profili.Field471 === "C") {
                                var query_gap_consegna = "select time_gap_consegna from impostazioni_fiscali where id=" + comanda.folder_number + ";"
                                var result = alasql(query_gap_consegna);
                                if (result[0].time_gap_consegna == "1" &&  dati_estratti['consegna_gap'] != "" &&  dati_estratti['consegna_gap'] != "undefined") {
                                    var ora_consegna =dati_estratti['ora_consegna']
                                    var minuti_somma = ora_consegna.substring(ora_consegna.indexOf(':') + 1)
                                    var int_miunti = parseInt(minuti_somma);
                                    var int_gap_miunti = parseInt(dati_estratti['consegna_gap']);;
                                    var min_somma = int_miunti + int_gap_miunti;
                                    var ora_gap = ora_consegna.substring(0, ora_consegna.indexOf(':'))
                                    var hours = Math.floor(min_somma / 60);
                                    var minutes = min_somma % 60;
                                    var ora = parseInt(ora_gap);

                                    if (minutes < 10) {
                                        if (ora < 10) {
                                            var oraa = ora + hours
                                            var totale_ora = "0" + oraa + ":" + "0" + minutes;
                                        }
                                        else {
                                            var totale_ora = ora + hours + ":" + "0" + minutes;

                                        }

                                    }
                                    else {
                                        if (ora < 10) {
                                            var oraa = ora + hours
                                            var totale_ora = "0" + oraa + ":" + minutes;
                                        }
                                        else {

                                            var totale_ora = ora + hours + ":" + minutes;
                                        }
                                        if (totale_ora === "NaN:NaN") {
                                            totale_ora = "";
                                        }
                                    }


                                    comanda.ora_consegna_gap = totale_ora;
                                    builder.addText('CONSEGNA ' + ' ' + dati_estratti['ora_consegna'] + '\n');
                                    builder.addText('         ' + ' ' +   totale_ora + '\n');

                                }
                                else{  builder.addText('CONSEGNA ' + ' ' + dati_estratti['ora_consegna'] + '\n');

                                }
                               
                            }
                        }

                        /*if (comanda.orario_preparazione === "1") {
                         if (orario_preparazione.length === 5) {
                         builder.addText('PREPARAZIONE ' + orario_preparazione + '\n');
                         }
                         } else {
                         builder.addText('CONSEGNA ' + ' ' + dati_estratti['ora_consegna'] + '\n');
                         }*/
                    }

                    builder.addTextSize(1, 1);
                    builder.addFeedLine(1);
                    builder.addTextAlign(builder.ALIGN_LEFT);


                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                    builder.addTextSize(1, 1);
                    var totale_quantita = 0;
                    var totale_quantita_pizze = 0;
                    var totale_quantita_bibite = 0;

                    var totale_prezzo = 0.00;
                    for (var portata in array_comanda[dest_stampa]) {
                        if (array_comanda[dest_stampa][portata].val !== undefined) {

                            builder.addFeedLine(1);
                            builder.addTextAlign(builder.ALIGN_CENTER);
                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                            builder.addTextSize(1, 1);
                            if (settaggi_profili.Field130 === 'true') {
                                switch (settaggi_profili.Field131) {
                                    case "G":
                                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                                        break;
                                    case "N":
                                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                                        builder.addTextSize(1, 1);
                                        break;
                                    case "D":
                                        builder.addTextSize(2, 2);
                                        break;
                                    default:
                                        builder.addTextStyle(false, false, false, builder.COLOR_1);

                                }

                                if (settaggi_profili.Field131 === "D") {
                                    builder.addTextAlign(builder.ALIGN_CENTER);

                                    builder.addText(array_comanda[dest_stampa][portata].val + '\n');
                                    builder.addTextSize(1, 1);
                                    builder.addText("-----------------------------------------\n");


                                    builder.addTextAlign(builder.ALIGN_LEFT);

                                } else {
                                    builder.addText(trattini(array_comanda[dest_stampa][portata].val));
                                }
                            } else {
                                builder.addText("-----------------------------------------\n");
                            }
                            builder.addTextSize(1, 1);

                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                            builder.addTextAlign(builder.ALIGN_LEFT);
                            builder.addTextSize(2, 2);

                            var i = 0;
                            for (var node in array_comanda[dest_stampa][portata]) {

                                var nodo = array_comanda[dest_stampa][portata][node];
                                if (nodo !== undefined && nodo[1] !== undefined && nodo[1].length > 1) {

                                    var articolo = filtra_accenti(nodo[1]);
                                    var quantita = nodo[0];

                                    var prezzo = "0.00";
                                    if (!isNaN(parseFloat(nodo[2]).toFixed(2))) {
                                        prezzo = parseFloat(nodo[2]).toFixed(2);
                                    }
                                    var categoria = nodo[4];
                                    if (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(" && articolo[0] !== "*" && articolo[0] !== "x" && articolo[0] !== "=") {

                                        switch (settaggi_profili.Field133) {
                                            case "D":
                                                builder.addTextSize(2, 2);
                                                builder.addTextStyle(false, false, true, builder.COLOR_1);

                                                break;
                                            case "G":
                                                builder.addTextSize(1, 1);
                                                builder.addTextStyle(false, false, true, builder.COLOR_1);

                                                break;
                                            case "N":
                                            default:
                                                builder.addTextSize(1, 1);
                                                builder.addTextStyle(false, false, false, builder.COLOR_1);

                                        }

                                        /*if (i !== 0) {
                                         builder.addFeedLine(1);
                                         }*/
                                        i++;

                                        if (portata === "ZZZ") {
                                            builder.addTextAlign(builder.ALIGN_CENTER);
                                        } else {
                                            builder.addText(quantita);
                                            builder.addTextPosition(50);
                                            totale_quantita += parseInt(quantita);
                                        }

                                        switch (settaggi_profili.Field133) {
                                            case "D":

                                                if (settaggi_profili.Field383 === categoria) {
                                                    builder.addTextStyle(true, false, true, builder.COLOR_1);

                                                }
                                                if (settaggi_profili.Field384 === categoria) {
                                                    builder.addTextStyle(true, false, true, builder.COLOR_1);

                                                }
                                                break;
                                            case "G":

                                                if (settaggi_profili.Field383 === categoria) {
                                                    builder.addTextStyle(true, false, true, builder.COLOR_1);
                                                }
                                                if (settaggi_profili.Field384 === categoria) {
                                                    builder.addTextStyle(true, false, true, builder.COLOR_1);
                                                }
                                                break;
                                            case "N":
                                            default:

                                                if (settaggi_profili.Field383 === categoria) {
                                                    builder.addTextStyle(true, false, false, builder.COLOR_1);
                                                }
                                                if (settaggi_profili.Field384 === categoria) {
                                                    builder.addTextStyle(true, false, false, builder.COLOR_1);
                                                }
                                        }

                                        if (portata.indexOf("P") !== -1) {
                                            totale_quantita_pizze += parseInt(quantita);
                                        }

                                        if (portata === "B") {
                                            totale_quantita_bibite += parseInt(quantita);
                                        }

                                        if (portata === "ZZZ") {
                                            builder.addTextPosition(0);
                                            builder.addTextAlign(builder.ALIGN_LEFT);
                                        } else {
                                            builder.addTextPosition(50);
                                        }

                                        builder.addText(articolo.slice(0, 40));

                                        if (settaggi_profili.Field132 === 'true') {

                                            if (comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].nome === 'CUBO') {
                                                if (prezzo.length === 4) {
                                                    builder.addTextPosition(500 - 26);
                                                } else if (prezzo.length === 5) {
                                                    builder.addTextPosition(487 - 26);
                                                } else if (prezzo.length === 6) {
                                                    builder.addTextPosition(474 - 26);
                                                } else {
                                                    builder.addTextPosition(461 - 26);
                                                }
                                            } else {
                                                builder.addTextPosition(415);
                                            }


                                            builder.addText('€ ' + prezzo);




                                        }

                                        builder.addText('\n');

                                        builder.addTextSize(1, 1);
                                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                                        if (nodo[3] !== undefined && nodo[3] !== null && settaggi_profili.Field136 === 'true' && (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(" && articolo[0] !== "=")) {

                                            var ricetta = filtra_accenti(nodo[3]);

                                            if (ricetta.length > 1) {

                                                var stringa = ricetta;
                                                var lettere = 40;
                                                var counter = 0;



                                                while (stringa.length > 0) {



                                                    builder.addTextPosition(50);

                                                    if (settaggi_profili.Field380 === "G") {
                                                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                    } else if (settaggi_profili.Field380 === "D") {
                                                        builder.addTextSize(2, 2);
                                                    } else {
                                                    }

                                                    if (settaggi_profili.Field379 === "true") {
                                                        stringa = stringa.toUpperCase();
                                                    }

                                                    if (counter === 0) {
                                                        builder.addText('(');
                                                    }

                                                    counter++;



                                                    builder.addText(stringa.substring(0, lettere));



                                                    if (stringa.length <= lettere) {
                                                        builder.addText(')');
                                                    }

                                                    builder.addText('\n');

                                                    stringa = stringa.substring(lettere);

                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                    builder.addTextSize(1, 1);

                                                }


                                            }

                                        }

                                        builder.addTextAlign(builder.ALIGN_LEFT);
                                    } else {

                                        var variante_meno = false;
                                        var variante_piu = false;
                                        var specifica = false;

                                        if (articolo[0] === "-") {
                                            variante_meno = true;
                                        } else if (articolo[0] === "+") {
                                            variante_piu = true;
                                        } else if (articolo[0] === "(") {
                                            specifica = true;
                                        }

                                        switch (settaggi_profili.Field134) {
                                            case "D":
                                                builder.addTextSize(2, 2);


                                                //RIPASSAVA "+" CON UN A CAPO PRECEDENTE

                                                //articolo = articolo.replace(/\+/gi, ',+').replace(/\*/gi, ',').replace(/\-/gi, ',-').replace(/\(/gi, ',(').replace(/\=/gi, ',=').replace('\,', '').split(',');


                                                var dio_re = /\(|[\=\+\-\*\x]+(?![^(]*\))/gi,
                                                    str = articolo;

                                                var c = 0;
                                                while ((match = dio_re.exec(str)) != null) {

                                                    articolo = [articolo.slice(0, match.index + c), ',', articolo.slice(match.index + c)].join('');
                                                    c++;
                                                }

                                                articolo = articolo.replace('\,', '').split(',');


                                                builder.addTextPosition(50);


                                                if (settaggi_profili.Field135 === 'true') {
                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                    builder.addText(quantita);
                                                }

                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                if (settaggi_profili.Field310 === 'true' && settaggi_profili.Field135 !== 'true' && variante_meno === true) {
                                                    builder.addTextStyle(true, false, false, builder.COLOR_1);
                                                } else if (settaggi_profili.Field410 === 'true' && settaggi_profili.Field135 !== 'true' && variante_piu === true) {
                                                    builder.addTextStyle(true, false, false, builder.COLOR_1);
                                                } else if (settaggi_profili.Field424 === 'true' && settaggi_profili.Field135 !== 'true' && specifica === true) {
                                                    builder.addTextStyle(true, false, false, builder.COLOR_1);
                                                }


                                                for (var art in articolo) {
                                                    builder.addTextPosition(90);

                                                    builder.addText(articolo[art].replace(/\=/gi, ' ').trim());

                                                    if (settaggi_profili.Field135 === "true") {
                                                        builder.addText('\n');
                                                    }
                                                }


                                                if (settaggi_profili.Field132 === 'true') {

                                                    if (comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].nome === 'CUBO') {
                                                        if (prezzo.length === 4) {
                                                            builder.addTextPosition(500 - 26);
                                                        } else if (prezzo.length === 5) {
                                                            builder.addTextPosition(487 - 26);
                                                        } else if (prezzo.length === 6) {
                                                            builder.addTextPosition(474 - 26);
                                                        } else {
                                                            builder.addTextPosition(461 - 26);
                                                        }
                                                    } else {
                                                        builder.addTextPosition(415);
                                                    }

                                                    builder.addText('€ ' + prezzo);
                                                }
                                                builder.addText('\n');
                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                break;
                                            case "G":
                                                builder.addTextSize(1, 1);
                                                //articolo = articolo.replace(/\+/gi, ',+').replace(/\*/gi, ',').replace(/\-/gi, ',-').replace(/\(/gi, ',(').replace(/\=/gi, ',=').replace(/FINE COT./gi, 'F.C.').replace(/IN COT./gi, 'I.C.').replace('\,', '').split(',');
                                                var dio_re = /\(|[\=\+\-\*\x]+(?![^(]*\))/gi,
                                                    str = articolo;

                                                var c = 0;
                                                while ((match = dio_re.exec(str)) != null) {

                                                    articolo = [articolo.slice(0, match.index + c), ',', articolo.slice(match.index + c)].join('');
                                                    c++;
                                                }

                                                articolo = articolo.replace('\,', '').split(',');

                                                builder.addTextPosition(50);

                                                if (settaggi_profili.Field135 === 'true') {
                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                    builder.addText(quantita);
                                                }

                                                builder.addTextStyle(false, false, true, builder.COLOR_1);

                                                if (settaggi_profili.Field310 === 'true' && variante_meno === true) {
                                                    builder.addTextStyle(true, false, true, builder.COLOR_1);
                                                } else if (settaggi_profili.Field410 === 'true' && variante_piu === true) {
                                                    builder.addTextStyle(true, false, true, builder.COLOR_1);
                                                } else if (settaggi_profili.Field424 === 'true' && settaggi_profili.Field135 !== 'true' && specifica === true) {
                                                    builder.addTextStyle(true, false, false, builder.COLOR_1);
                                                }

                                                for (var art in articolo) {
                                                    builder.addTextPosition(90);

                                                    builder.addText(articolo[art].replace(/\=/gi, ' ').trim() + '\n');
                                                }
                                                if (settaggi_profili.Field132 === 'true') {

                                                    if (comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].nome === 'CUBO') {
                                                        if (prezzo.length === 4) {
                                                            builder.addTextPosition(500 - 26);
                                                        } else if (prezzo.length === 5) {
                                                            builder.addTextPosition(487 - 26);
                                                        } else if (prezzo.length === 6) {
                                                            builder.addTextPosition(474 - 26);
                                                        } else {
                                                            builder.addTextPosition(461 - 26);
                                                        }
                                                    } else {
                                                        builder.addTextPosition(415);
                                                    }

                                                    builder.addText('€ ' + prezzo);

                                                }
                                                builder.addText('\n');
                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                break;
                                            case "N":
                                            default:
                                                builder.addTextSize(1, 1);
                                                //articolo = articolo.replace(/\+/gi, ',+').replace(/\*/gi, ',').replace(/\-/gi, ',-').replace(/\(/gi, ',(').replace(/\=/gi, ',=').replace(/FINE COT./gi, 'F.C.').replace(/IN COT./gi, 'I.C.').replace('\,', '').split(',');
                                                var dio_re = /\(|[\=\+\-\*\x]+(?![^(]*\))/gi,
                                                    str = articolo;

                                                var c = 0;
                                                while ((match = dio_re.exec(str)) != null) {

                                                    articolo = [articolo.slice(0, match.index + c), ',', articolo.slice(match.index + c)].join('');
                                                    c++;
                                                }

                                                articolo = articolo.replace('\,', '').split(',');

                                                builder.addTextPosition(50);

                                                if (settaggi_profili.Field135 === 'true') {
                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                    builder.addText(quantita);
                                                }

                                                builder.addTextStyle(false, false, false, builder.COLOR_1);

                                                if (settaggi_profili.Field310 === 'true' && variante_meno === true) {
                                                    builder.addTextStyle(true, false, true, builder.COLOR_1);
                                                } else if (settaggi_profili.Field410 === 'true' && variante_piu === true) {
                                                    builder.addTextStyle(true, false, true, builder.COLOR_1);
                                                } else if (settaggi_profili.Field424 === 'true' && settaggi_profili.Field135 !== 'true' && specifica === true) {
                                                    builder.addTextStyle(true, false, false, builder.COLOR_1);
                                                }

                                                var indice_articolo = 0;
                                                for (var art in articolo) {
                                                    builder.addTextPosition(90);

                                                    builder.addText(articolo[art].replace(/\=/gi, ' ').trim());

                                                    indice_articolo++;

                                                    if (indice_articolo < articolo.length) {
                                                        builder.addText('\n');
                                                    }
                                                }

                                                if (settaggi_profili.Field132 === 'true') {

                                                    if (comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].nome === 'CUBO') {
                                                        if (prezzo.length === 4) {
                                                            builder.addTextPosition(500 - 26);
                                                        } else if (prezzo.length === 5) {
                                                            builder.addTextPosition(487 - 26);
                                                        } else if (prezzo.length === 6) {
                                                            builder.addTextPosition(474 - 26);
                                                        } else {
                                                            builder.addTextPosition(461 - 26);
                                                        }
                                                    } else {
                                                        builder.addTextPosition(415);
                                                    }

                                                    builder.addText('€ ' + prezzo);
                                                }
                                                builder.addText('\n');

                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                        }

                                    }

                                    totale_prezzo += parseFloat(prezzo) * parseInt(quantita);
                                }
                            }
                        }

                        builder.addTextAlign(builder.ALIGN_CENTER);
                        builder.addTextSize(1, 1);
                        builder.addText("------------------------------------------\n");
                        builder.addTextAlign(builder.ALIGN_LEFT);
                    }


                    if (settaggi_profili.Field189 === 'true') {

                        builder.addTextStyle(false, false, true, builder.COLOR_1);

                        if (!isNaN(parseFloat(totale_consegna)) && parseFloat(totale_consegna) > 0) {

                            builder.addTextPosition(50);

                            builder.addText("CONSEGNA");

                            indice_articolo++;

                            prezzo = totale_consegna;

                            if (comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].nome === 'CUBO') {
                                if (prezzo.length === 4) {
                                    builder.addTextPosition(500 - 26);
                                } else if (prezzo.length === 5) {
                                    builder.addTextPosition(487 - 26);
                                } else if (prezzo.length === 6) {
                                    builder.addTextPosition(474 - 26);
                                } else {
                                    builder.addTextPosition(461 - 26);
                                }
                            } else {
                                builder.addTextPosition(415);
                            }

                            builder.addText('€ ' + prezzo + '\n');

                            totale_prezzo += parseFloat(totale_consegna);
                        }
                    }



                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                    builder.addFeedLine(1);

                    if (settaggi_profili.Field381 === 'true' && settaggi_profili.Field382 === 'true' && settaggi_profili.Field137 === 'true') {

                        builder.addText('\n');
                        builder.addText(totale_quantita);
                        builder.addTextPosition(50);
                        builder.addText('prodotti' + '               '); //quantita

                        if (parseInt(totale_quantita_pizze) > 0) {

                            builder.addTextPosition(400);
                            builder.addText(totale_quantita_pizze);
                            builder.addTextPosition(450);
                            builder.addText('pizze' + '               '); //quantita
                        }

                        if (parseInt(totale_quantita_bibite) > 0) {
                            builder.addTextPosition(400);
                            builder.addText(totale_quantita_bibite);
                            builder.addTextPosition(450);
                            builder.addText('bibite' + '               '); //quantita
                        }


                    } else {
                        if (parseInt(totale_quantita_pizze) > 0) {
                            if (settaggi_profili.Field381 === 'true') {
                                builder.addText(totale_quantita_pizze);
                                builder.addTextPosition(50);
                                builder.addText('pizze' + '               '); //quantita
                            }
                        }

                        if (parseInt(totale_quantita_bibite) > 0) {
                            if (settaggi_profili.Field382 === 'true') {
                                builder.addText('\n');
                                builder.addText(totale_quantita_bibite);
                                builder.addTextPosition(50);
                                builder.addText('bibite' + '               '); //quantita
                            }
                        }

                        if (settaggi_profili.Field137 === 'true') {
                            builder.addText('\n');
                            builder.addText(totale_quantita);
                            builder.addTextPosition(50);
                            builder.addText('prodotti' + '               '); //quantita
                        }
                    }




                    if (settaggi_profili.Field189 === 'true') {

                        if (parseFloat(totale_ordine).toFixed(2) !== parseFloat(totale_prezzo).toFixed(2)) {


                            builder.addTextPosition(0);
                            builder.addText("\n\nTotale ORDINE: ");

                            var prz_t = parseFloat(totale_ordine).toFixed(2);

                            if (comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].nome === 'CUBO') {
                                if (prz_t.length === 4) {
                                    builder.addTextPosition(500 - 26);
                                } else if (prz_t.length === 5) {
                                    builder.addTextPosition(487 - 26);
                                } else if (prz_t.length === 6) {
                                    builder.addTextPosition(474 - 26);
                                } else {
                                    builder.addTextPosition(461 - 26);
                                }
                            } else {
                                builder.addTextPosition(415);
                            }



                            builder.addText('€ ' + prz_t); //quantita

                        } else {

                            builder.addTextPosition(0);
                            builder.addText("\n\nTotale: ");

                            var prz_t = parseFloat(totale_prezzo).toFixed(2);

                            if (comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].nome === 'CUBO') {
                                if (prz_t.length === 4) {
                                    builder.addTextPosition(500 - 26);
                                } else if (prz_t.length === 5) {
                                    builder.addTextPosition(487 - 26);
                                } else if (prz_t.length === 6) {
                                    builder.addTextPosition(474 - 26);
                                } else {
                                    builder.addTextPosition(461 - 26);
                                }
                            } else {
                                builder.addTextPosition(415);
                            }



                            builder.addText('€ ' + prz_t); //quantita
                        }
                    }

                    builder.addText('\n');
                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                    builder.addTextSize(1, 1);
                    if (comanda.tavolo === "BANCO" || comanda.tavolo == "BAR" || comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === "TAKE AWAY") {

                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                        if (settaggi_profili.Field124 === 'true') {
                            switch (settaggi_profili.Field125) {
                                case "G":
                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                    break;
                                case "N":
                                default:
                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                            }
                            builder.addText('\nOperatore: ' + comanda.operatore);
                        }

                        if (settaggi_profili.Field126 === 'true') {
                            switch (settaggi_profili.Field127) {
                                case "G":
                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                    break;
                                case "N":
                                default:
                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                            }
                            builder.addText('\nData: ' + data.substr(0, 8).replace(/-/gi, '/'));
                        }

                        if (settaggi_profili.Field128 === 'true') {
                            switch (settaggi_profili.Field129) {
                                case "G":
                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                    break;
                                case "N":
                                default:
                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                            }
                            builder.addText('\nOra:  ' + data.slice(-5));
                        }


                        builder.addTextSize(2, 2);



                        if (dati_estratti['tipo_consegna'] === "DOMICILIO" && settaggi_profili.Field252 !== 'true' && settaggi_profili.Field253 !== 'true') {
                            if (settaggi_profili.Field375.trim().length > 0) {
                                builder.addFeedLine(2);
                                builder.addTextAlign(builder.ALIGN_CENTER);
                                builder.addText(settaggi_profili.Field375 + "\n");
                            }
                        } else if (dati_estratti['tipo_consegna'] === "RITIRO") {
                            if (settaggi_profili.Field376.trim().length > 0) {
                                builder.addFeedLine(2);
                                builder.addTextAlign(builder.ALIGN_CENTER);
                                builder.addText(settaggi_profili.Field376 + "\n");
                            }
                        } else if (dati_estratti['tipo_consegna'] === "PIZZERIA") {
                            if (settaggi_profili.Field377.trim().length > 0) {
                                builder.addFeedLine(2);
                                builder.addTextAlign(builder.ALIGN_CENTER);
                                builder.addText(settaggi_profili.Field377 + "\n");
                            }
                        }

                        builder.addTextSize(1, 1);


                        if (dati_estratti['tipo_consegna'] === "DOMICILIO") {




                            if (settaggi_profili.Field252 === 'true' || settaggi_profili.Field253 === 'true') {

                                builder.addFeedLine(1);

                                builder.addTextAlign(builder.ALIGN_CENTER);




                                if (comanda.stampante_piccola_conto === 'N') {
                                    builder.addText("-----------------------------------------\n");
                                } else {
                                    builder.addText("--------------------------------\n");
                                }


                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                builder.addText('DATI CLIENTE:\n');
                                builder.addTextStyle(false, false, false, builder.COLOR_1);

                                if (settaggi_profili.Field252 === 'true' && settaggi_profili.Field253 === 'true') {
                                    builder.addPageBegin();
                                    builder.addPageArea(40, 0, 330, 200);
                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                    builder.addText(nome + " " + cognome + '\n');
                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                    if (ragione_sociale.length >= 3) {
                                        builder.addText(ragione_sociale + '\n');
                                    }
                                    builder.addText(indirizzo + "," + numero + '\n');
                                    if (cap) {
                                        builder.addText(cap + " " + comune + " " + provincia + '\n');
                                    } else {
                                        builder.addText(comune + " " + provincia + '\n');
                                    }
                                    if (cellulare) {
                                        builder.addText(cellulare);
                                    }
                                    if (cellulare && telefono) {
                                        builder.addText(" - ");
                                    }
                                    if (telefono) {
                                        builder.addText(telefono);
                                    }
                                    if (cellulare || telefono) {
                                        builder.addText("\n");
                                    }
                                    if (comanda.tipo_consegna !== "RITIRO" && note.length > 0) {
                                        //builder.addFeedLine(1);
                                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                                        builder.addText("Note: ");
                                        if (note.length > 20) {
                                            builder.addText(note.substr(0, 30));
                                        } else {
                                            builder.addText(note);
                                        }
                                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                                        builder.addText('\n');
                                    }

                                    builder.addPageArea(370, 0, 300, 200);
                                    builder.addSymbol("https://www.google.com/maps/dir/?api=1&destination=" + destinazione.replace(/\s+/g, "+") + "&travelmode=DRIVING", builder.SYMBOL_QRCODE_MODEL_2, builder.LEVEL_DEFAULT, 4);
                                    builder.addPageEnd();
                                } else if (settaggi_profili.Field252 === 'true') {
                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                    builder.addFeedLine(1);
                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                    builder.addText(nome + " " + cognome + '\n');
                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                    if (ragione_sociale.length >= 3) {
                                        builder.addText(ragione_sociale + '\n');
                                    }
                                    builder.addText(indirizzo + "," + numero + '\n');
                                    if (cap) {
                                        builder.addText(cap + " " + comune + " " + provincia + '\n');
                                    } else {
                                        builder.addText(comune + " " + provincia + '\n');
                                    }
                                    if (cellulare) {
                                        builder.addText(cellulare);
                                    }
                                    if (cellulare && telefono) {
                                        builder.addText(" - ");
                                    }
                                    if (telefono) {
                                        builder.addText(telefono);
                                    }
                                    if (cellulare || telefono) {
                                        builder.addText("\n");
                                    }
                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                } else if (settaggi_profili.Field253 === 'true') {
                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                    builder.addFeedLine(1);

                                    builder.addSymbol("https://www.google.com/maps/dir/?api=1&destination=" + destinazione.replace(/\s+/g, "+") + "&travelmode=DRIVING", builder.SYMBOL_QRCODE_MODEL_2, builder.LEVEL_DEFAULT, 4);

                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                }
                                if (comanda.stampante_piccola_conto === 'N') {
                                    builder.addText("-----------------------------------------\n");
                                } else {
                                    builder.addText("--------------------------------\n");
                                }
                            }
                        }

                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                    }

                    if (dati_estratti['esecuzione_comanda_immediata'] !== true /*&& comanda.orario_preparazione === "1"*/) {
                        builder.addTextSize(2, 2);

                        /*bool_orario_comanda_visu_alta*/
                        if (settaggi_profili.Field470 === "true") {

                            /*tipo_orario_comanda_visu_alta */
                            if (settaggi_profili.Field472 === "P") {
                                if (orario_preparazione.length === 5) {
                                    builder.addText('PREPARAZIONE ' + orario_preparazione + '\n');
                                }
                            }

                            /*tipo_orario_comanda_visu_alta */
                            if (settaggi_profili.Field472 === "C") {
                                var query_gap_consegna = "select time_gap_consegna from impostazioni_fiscali where id=" + comanda.folder_number + ";"
                                var result = alasql(query_gap_consegna);
                                if (result[0].time_gap_consegna == "1" &&  dati_estratti['consegna_gap'] != "" &&  dati_estratti['consegna_gap'] != "undefined") {
                                    var ora_consegna =dati_estratti['ora_consegna']
                                    var minuti_somma = ora_consegna.substring(ora_consegna.indexOf(':') + 1)
                                    var int_miunti = parseInt(minuti_somma);
                                    var int_gap_miunti = parseInt(dati_estratti['consegna_gap']);;
                                    var min_somma = int_miunti + int_gap_miunti;
                                    var ora_gap = ora_consegna.substring(0, ora_consegna.indexOf(':'))
                                    var hours = Math.floor(min_somma / 60);
                                    var minutes = min_somma % 60;
                                    var ora = parseInt(ora_gap);

                                    if (minutes < 10) {
                                        if (ora < 10) {
                                            var oraa = ora + hours
                                            var totale_ora = "0" + oraa + ":" + "0" + minutes;
                                        }
                                        else {
                                            var totale_ora = ora + hours + ":" + "0" + minutes;

                                        }

                                    }
                                    else {
                                        if (ora < 10) {
                                            var oraa = ora + hours
                                            var totale_ora = "0" + oraa + ":" + minutes;
                                        }
                                        else {

                                            var totale_ora = ora + hours + ":" + minutes;
                                        }
                                        if (totale_ora === "NaN:NaN") {
                                            totale_ora = "";
                                        }
                                    }


                                    comanda.ora_consegna_gap = totale_ora;
                                    builder.addText('CONSEGNA ' + ' ' + dati_estratti['ora_consegna'] + '\n');
                                    builder.addText('         ' + ' ' +   totale_ora + '\n');

                                }
                                else{
                                    builder.addText('CONSEGNA ' + ' ' + dati_estratti['ora_consegna'] + '\n');
                                }
                               
                            }
                        }
                        /*builder.addText('CONSEGNA ' + ' ' + dati_estratti['ora_consegna'] + '\n');*/
                        builder.addTextSize(1, 1);
                    }

                    if (settaggi_profili.Field138.trim().length > 0 || settaggi_profili.Field142.trim().length > 0 || settaggi_profili.Field146.trim().length > 0) {
                        builder.addFeedLine(2);
                    }
                    if (settaggi_profili.Field139 === 'G')
                        messaggio_finale.addTextStyle(false, false, true, builder.COLOR_1);
                    else if (settaggi_profili.Field139 === 'N')
                        messaggio_finale.addTextStyle(false, false, false, builder.COLOR_1);
                    if (settaggi_profili.Field140 === 'x--')
                        messaggio_finale.addTextAlign(builder.ALIGN_LEFT);
                    else if (settaggi_profili.Field140 === '-x-')
                        messaggio_finale.addTextAlign(builder.ALIGN_CENTER);
                    else if (settaggi_profili.Field140 === '--x')
                        messaggio_finale.addTextAlign(builder.ALIGN_RIGHT);
                    if (settaggi_profili.Field138.trim().length > 0)
                        messaggio_finale.addText(settaggi_profili.Field138 + '\n');
                    if (settaggi_profili.Field141 === 'true')
                        messaggio_finale.addFeedLine(1);
                    if (settaggi_profili.Field143 === 'G')
                        messaggio_finale.addTextStyle(false, false, true, builder.COLOR_1);
                    else if (settaggi_profili.Field143 === 'N')
                        messaggio_finale.addTextStyle(false, false, false, builder.COLOR_1);
                    if (settaggi_profili.Field144 === 'x--')
                        messaggio_finale.addTextAlign(builder.ALIGN_LEFT);
                    else if (settaggi_profili.Field144 === '-x-')
                        messaggio_finale.addTextAlign(builder.ALIGN_CENTER);
                    else if (settaggi_profili.Field144 === '--x')
                        messaggio_finale.addTextAlign(builder.ALIGN_RIGHT);
                    if (settaggi_profili.Field142.trim().length > 0)
                        messaggio_finale.addText(settaggi_profili.Field142 + '\n');
                    if (settaggi_profili.Field145 === 'true')
                        messaggio_finale.addFeedLine(1);
                    if (settaggi_profili.Field147 === 'G')
                        messaggio_finale.addTextStyle(false, false, true, builder.COLOR_1);
                    else if (settaggi_profili.Field147 === 'N')
                        messaggio_finale.addTextStyle(false, false, false, builder.COLOR_1);
                    if (settaggi_profili.Field148 === 'x--')
                        messaggio_finale.addTextAlign(builder.ALIGN_LEFT);
                    else if (settaggi_profili.Field148 === '-x-')
                        messaggio_finale.addTextAlign(builder.ALIGN_CENTER);
                    else if (settaggi_profili.Field148 === '--x')
                        messaggio_finale.addTextAlign(builder.ALIGN_RIGHT);
                    if (settaggi_profili.Field146.trim().length > 0)
                        messaggio_finale.addText(settaggi_profili.Field146 + '\n');
                    //FINE SETTAGGI MESSAGGIO FINALE



                    if (comanda.numero_licenza_cliente === "0629") {
                        if (dati_estratti['tipo_consegna'] === "DOMICILIO") {
                            builder.addTextSize(2, 2);
                            builder.addText("Num. Consegna: " + numero_consegna_domicilio + "\n");
                            builder.addTextSize(1, 1);
                        }
                    }

                    builder.addFeedLine(2);

                    if (settaggi_profili.Field498 === 'true') {
                        builder.addBarcode('{A#' + id_comanda, builder.BARCODE_CODE128, builder.HRI_NONE, builder.FONT_A, 2, 100);
                    }

                    let oggetto_concomitanze = null;
                    let testo_concomitanze = '';
                    let concomitanze = '';
                    if (concomitanze_presenti === true) {
                        if (settaggi_profili.Field149 === 'true') {
                            builder.addTextAlign(builder.ALIGN_CENTER);

                            oggetto_concomitanze = funzione_concomitanze(array_comanda, dest_stampa, array_dest_stampa);

                            concomitanze = oggetto_concomitanze['concomitanze'];
                            testo_concomitanze = oggetto_concomitanze['testo_concomitanze'];

                        }
                    }

                    var messaggio_finale = '<feed line="1"/>' + messaggio_finale.toString().substr(72).slice(0, -13);
                    var request = builder.toString().slice(0, -13) + testo_concomitanze + messaggio_finale + '<feed line="2"/><cut type="feed"/></epos-print>';
                    //var request = builder.toString().slice(0, -13) + testo_concomitanze + '<feed line="3"/><cut type="feed"/></epos-print>';
                    console.log("REQUEST", request);
                    var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
                    //CAMBIANDO QUESTO PARAMETRO CAMBIA L'IP DI STAMPA (quindi si potrebbe prendere da comanda.nome_stampante[num].ip
                    //SI POTREBBE TESTARE LA DIMENSIONE CON comanda.nome_stampante[num].dimensione
                    //E SE E' O NO FISCALE CON IL PARAMETRO comanda.nome_stampante[num].fiscale

                    var url = 'http://' + array_dest_stampa[dest_stampa].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';

                    if (array_dest_stampa[dest_stampa].intelligent === "SDS") {
                        url = 'http://' + array_dest_stampa[dest_stampa].ip + ':8000/SPOOLER?stampante=' + array_dest_stampa[dest_stampa].devid_nf;
                    }

                    if (settaggi_profili.Field109 === 'true') {
                        setTimeout(function () {
                            aggiungi_spool_stampa(url, soap, array_dest_stampa[dest_stampa].nome_umano);
                        }, 100);
                    }

                    aggiungi_spool_stampa(url, soap, array_dest_stampa[dest_stampa].nome_umano);
                }
        }
    }


    var data = comanda.funzionidb.data_attuale().substr(0, 8);
    var ora = comanda.funzionidb.data_attuale().substr(9, 8);
    var testo_query = "update comanda set stampata_sn='S',data_stampa='" + data + "',ora_stampa='" + ora + "' where id='" + id_comanda + "'  and stato_record='FORNO' ;";
    comanda.sock.send({ tipo: "comanda_stampata", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
    alasql(testo_query);

    //comanda.funzionidb.conto_attivo();
    righe_forno();

}

