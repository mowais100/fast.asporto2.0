/* global parseFloat, comanda, epson, ive_misuratore_controller */

﻿var buoni_memorizzati = new Array();
function importa_dati_circuiti(callback) {
    comanda.sincro.query("select * from circuiti;", function (elenco_circuiti) {

        comanda.circuito = new Object();

        elenco_circuiti.forEach(function (c) {

            comanda.circuito[c.id] = new Object();

            comanda.circuito[c.id].ragione_sociale = c.ragione_sociale;
            comanda.circuito[c.id].indirizzo = c.indirizzo;
            comanda.circuito[c.id].numero = c.numero;
            comanda.circuito[c.id].citta = c.citta;
            comanda.circuito[c.id].cap = c.cap;
            comanda.circuito[c.id].provincia = c.provincia;
            comanda.circuito[c.id].paese = c.paese;
            comanda.circuito[c.id].telefono = c.telefono;
            comanda.circuito[c.id].partita_iva = c.partita_iva;
            comanda.circuito[c.id].codice_fiscale = c.codice_fiscale;
            comanda.circuito[c.id].email = c.email;
            comanda.circuito[c.id].adeguamento_perc = c.adeguamento_perc;

        });

        callback(true);

    });
}

function fatture_buoni_a4() {

    importa_dati_circuiti(function () {

        var array_data_partenza = $('#data_incassi_partenza:visible').multiDatesPicker('getDates');
        var array_data_arrivo = $('#data_incassi_arrivo:visible').multiDatesPicker('getDates');

        if (array_data_partenza.length === 1 && array_data_arrivo.length === 1) {

            var yY = array_data_partenza[0].substr(6, 4);
            var yD = array_data_partenza[0].substr(0, 2);
            var yM = array_data_partenza[0].substr(3, 2);

            var h = "00";
            var m = "00";
            var s = "00";
            var ms = "000";
            var data_ieri = yY + '' + yM + '' + yD + '00';


            var Y = array_data_arrivo[0].substr(6, 4);
            var D = array_data_arrivo[0].substr(0, 2);
            var M = array_data_arrivo[0].substr(3, 2);

            var h = "00";
            var m = "00";
            var s = "00";
            var ms = "000";
            var data_oggi = Y + '' + M + '' + D + '24';

        } else {

            var d = new Date();
            d.setDate(d.getDate());
            var Y = d.getFullYear();
            var M = addZero((d.getMonth() + 1), 2);
            var D = addZero(d.getDate(), 2);
            var h = addZero(d.getHours(), 2);
            var m = addZero(d.getMinutes(), 2);
            var s = addZero(d.getSeconds(), 2);
            var ms = addZero(d.getMilliseconds(), 3);
            var data_oggi = Y + '' + M + '' + D + '24';
            var y = new Date();
            y.setDate(y.getDate());
            var yY = y.getFullYear();
            var yM = addZero((y.getMonth() + 1), 2);
            var yD = addZero(y.getDate(), 2);
            var yh = addZero(y.getHours(), 2);
            var ym = addZero(y.getMinutes(), 2);
            var ys = addZero(y.getSeconds(), 2);
            var yms = addZero(y.getMilliseconds(), 3);
            var data_ieri = yY + '' + yM + '' + yD + '00';

        }

        var array_pc = new Array();

        $('#selezione_pc_incassi input:checked').each(function (i, v) {
            //MOD 16/06/2017 PRIMA ERA v.name
            array_pc.push(v.value);
        });

        var aggiunta_query_nome_pc = '';

        if (array_pc.length >= 1) {
            aggiunta_query_nome_pc += ' and nome_pc IN (' + array_pc.map(function (date) {
                return "'" + date + "'";
            }).join() + ') ';
        }

        var testo_query_circuiti = "select id,ragione_sociale from circuiti;";
        comanda.sincro.query_centrale(testo_query_circuiti, function (rc) {
            var testo_query_teste = " select '1' as n_estrazione,nome_buono ,sum(numero_buoni_pasto) as quantita,sum(buoni_pasto*numero_buoni_pasto) as importo,buoni_pasto as importo_singolo,nome_pc,tavolo from record_teste where cast(substr(progressivo_comanda,5,10) as int)>='" + data_ieri + "' AND cast(substr(progressivo_comanda,5,10) as int)<='" + data_oggi + "'  " + aggiunta_query_nome_pc + "  group by nome_buono, buoni_pasto \n\
 union all \n\
select '2' as n_estrazione,nome_buono_2  as nome_buono,sum(numero_buoni_pasto_2) as quantita,sum(buoni_pasto_2*numero_buoni_pasto_2) as importo,buoni_pasto_2 as importo_singolo,nome_pc,tavolo from record_teste  where cast(substr(progressivo_comanda,5,10) as int)>='" + data_ieri + "' AND cast(substr(progressivo_comanda,5,10) as int)<='" + data_oggi + "'  " + aggiunta_query_nome_pc + "  group by  nome_buono_2, buoni_pasto_2\n\
 union all \n\
select '3' as n_estrazione,nome_buono_3  as nome_buono,sum(numero_buoni_pasto_3),sum(buoni_pasto_3*numero_buoni_pasto_3) as importo,buoni_pasto_3 as importo_singolo,nome_pc,tavolo from record_teste  where cast(substr(progressivo_comanda,5,10) as int)>='" + data_ieri + "' AND cast(substr(progressivo_comanda,5,10) as int)<='" + data_oggi + "'   " + aggiunta_query_nome_pc + " group by nome_buono_3, buoni_pasto_3\n\
 union all \n\
select '4' as n_estrazione,nome_buono_4  as nome_buono,sum(numero_buoni_pasto_4) as quantita,sum(buoni_pasto_4*numero_buoni_pasto_4) as importo,buoni_pasto_4 as importo_singolo,nome_pc,tavolo from record_teste  where cast(substr(progressivo_comanda,5,10) as int)>='" + data_ieri + "' AND cast(substr(progressivo_comanda,5,10) as int)<='" + data_oggi + "'  " + aggiunta_query_nome_pc + "  group by  nome_buono_4, buoni_pasto_4\n\
 union all \n\
select '5' as n_estrazione, nome_buono_5 as nome_buono ,sum(numero_buoni_pasto_5) as quantita,sum(buoni_pasto_5*numero_buoni_pasto_5) as importo,buoni_pasto_5 as importo_singolo,nome_pc,tavolo from record_teste  where cast(substr(progressivo_comanda,5,10) as int)>='" + data_ieri + "' AND cast(substr(progressivo_comanda,5,10) as int)<='" + data_oggi + "'  " + aggiunta_query_nome_pc + " group by  nome_buono_5, buoni_pasto_5;";
            comanda.sincro.query_centrale(testo_query_teste, function (rt) {

                var totale_buoni = new Object();

                var oggetto_circuiti = new Object();

                rc.forEach(function (el) {
                    oggetto_circuiti[el.id] = el.ragione_sociale;
                });


                var oggetto_buono = new Object();
                rt.forEach(function (el) {
                    if (el.nome_buono !== "") {

                        if (oggetto_buono[el.nome_buono] === undefined) {
                            oggetto_buono[el.nome_buono] = new Object();
                        }
                        if (totale_buoni[el.nome_buono] === undefined) {
                            totale_buoni[el.nome_buono] = 0;
                        }
                        if (oggetto_buono[el.nome_buono][el.nome_buono] !== undefined) {
                            if (oggetto_buono[el.nome_buono][el.nome_buono]["TAGLI"][el.importo_singolo] === undefined) {
                                oggetto_buono[el.nome_buono][el.nome_buono]["TAGLI"][el.importo_singolo] = new Object();
                                oggetto_buono[el.nome_buono][el.nome_buono]["TAGLI"][el.importo_singolo].quantita = parseInt(el.quantita);
                            } else
                            {
                                oggetto_buono[el.nome_buono][el.nome_buono]["TAGLI"][el.importo_singolo].quantita += parseInt(el.quantita);
                            }
                            oggetto_buono[el.nome_buono][el.nome_buono].quantita += parseInt(el.quantita);
                            oggetto_buono[el.nome_buono][el.nome_buono].importo += parseFloat(el.importo);
                            totale_buoni[el.nome_buono] += parseFloat(el.importo);
                        } else
                        {
                            oggetto_buono[el.nome_buono][el.nome_buono] = new Object();
                            oggetto_buono[el.nome_buono][el.nome_buono]["TAGLI"] = new Object();

                            if (oggetto_buono[el.nome_buono][el.nome_buono]["TAGLI"][el.importo_singolo] === undefined) {
                                oggetto_buono[el.nome_buono][el.nome_buono]["TAGLI"][el.importo_singolo] = new Object();
                                oggetto_buono[el.nome_buono][el.nome_buono]["TAGLI"][el.importo_singolo].quantita = parseInt(el.quantita);
                            } else
                            {
                                oggetto_buono[el.nome_buono][el.nome_buono]["TAGLI"][el.importo_singolo].quantita += parseInt(el.quantita);
                            }
                            oggetto_buono[el.nome_buono][el.nome_buono].ragione_sociale = oggetto_circuiti[el.nome_buono];
                            oggetto_buono[el.nome_buono][el.nome_buono].quantita = parseInt(el.quantita);
                            oggetto_buono[el.nome_buono][el.nome_buono].importo = parseFloat(el.importo);
                            totale_buoni[el.nome_buono] += parseFloat(el.importo);
                        }
                    }
                });
                console.log("RIEPILOGO BUONI", oggetto_buono);

                var data = comanda.funzionidb.data_attuale();
                $('.print').html('');
                var print_html = '';
                var pagina = 0;
                for (var nome_buono in  oggetto_buono) {
                    if (comanda.circuito[nome_buono] !== undefined) {
                        pagina++;
                        print_html += '<div class="contenitore_fatture_buoni" style="overflow:auto;height:100vh;font-size: 1rem;">';

                        print_html += '<div style="overflow:auto">';

                        print_html += '<div style="float:left;"><h5><strong>' + comanda.profilo_aziendale.ragione_sociale + '</strong></h5><br/>';
                        print_html += comanda.profilo_aziendale.indirizzo + ', ' + comanda.profilo_aziendale.numero + '<br/>';
                        print_html += comanda.profilo_aziendale.cap + ' - ' + comanda.profilo_aziendale.citta + ' (' + comanda.profilo_aziendale.provincia + ')<br/>';
                        print_html += 'P.IVA ' + comanda.profilo_aziendale.partita_iva + '<br/>';
                        print_html += 'C.F. ' + comanda.profilo_aziendale.codice_fiscale + '<br/>';
                        print_html += 'Tel. ' + comanda.profilo_aziendale.telefono;
                        print_html += '</div>';


                        print_html += '<div style="float: right;margin-top: 2cm;margin-right: 2cm;">';
                        print_html += '<span style="font-size:0.7rem">DESTINAZIONE</span>' + '<br/>';
                        print_html += 'Spett.le' + '<br/>';
                        print_html += '<strong><span style="font-size:1.5rem;">' + oggetto_circuiti[nome_buono] + '</span><br/>';
                        print_html += comanda.circuito[nome_buono].indirizzo + ', ' + comanda.circuito[nome_buono].numero + '<br/>';
                        print_html += comanda.circuito[nome_buono].cap + ' - ' + comanda.circuito[nome_buono].citta + ' (' + comanda.circuito[nome_buono].provincia + ')<br/>';
                        print_html += 'Tel. ' + comanda.circuito[nome_buono].telefono + '</strong>';
                        print_html += '</div>';


                        print_html += '</div>';



                        print_html += '<div style="overflow:auto;margin-top:0.5cm;">';



                        print_html += '</div>';


                        print_html += '<div style="clear:both;margin-top:0.5cm;font-weight: bold;"><span style="font-size:1.5rem;">ESTRATTO CONTO</span>';

                        if (array_data_partenza.length === 1 && array_data_arrivo.length === 1) {
                            print_html += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERIODO ' + array_data_partenza[0] + ' / ' + array_data_arrivo[0] + '&nbsp;';
                        } else
                        {
                            print_html += '&nbsp;' + data.replace(/-/gi, '/') + '&nbsp;';
                        }

                        print_html += '<div style="overflow:auto;margin-top:0.3cm;">';

                        print_html += '<div style="float: left;">';
                        print_html += '<span>----</span>';
                        print_html += '</div>';

                        print_html += '<div style="float: right;">';
                        print_html += '<span>----</span>';
                        print_html += '</div>';

                        print_html += '</div>';


                        print_html += '</div>';

                        print_html += '<div>';

                        print_html += '<table class="table table-responsive table-hover table-striped table-bordered table-condensed">';



                        print_html += '<tr>';

                        print_html += '<td style="padding:1px;"><span style="font-size:0.7rem">DATA</span><br/><strong>' + data + '</strong></td>';
                        print_html += '<td style="padding:1px;"><span style="font-size:0.7rem">SEZ./NUMERO</span></td>';
                        print_html += '<td style="padding:1px;"><span style="font-size:0.7rem">CAUSALE</span><br/><strong>RIMBORSO BUONI PASTO</strong></td>';
                        print_html += '<td style="padding:1px;"><span style="font-size:0.7rem">CODICE ESERCIZIO</span></td>';
                        print_html += '</tr>';

                        print_html += '<tr>';
                        print_html += '<td style="padding:1px;"><span style="font-size:0.7rem">COD.CLIENTE</span><br/><strong>' + aggZero(nome_buono, 6) + '</strong></td>';
                        print_html += '<td style="padding:1px;"><span style="font-size:0.7rem">PARTITA IVA</span><br/><strong>' + comanda.circuito[nome_buono].partita_iva + '</strong></td>';
                        print_html += '<td style="padding:1px;"><span style="font-size:0.7rem">CODICE FISCALE</span><br/><strong>' + comanda.circuito[nome_buono].codice_fiscale + '</strong></td>';
                        print_html += '<td style="padding:1px;"><span style="font-size:0.7rem">CODICE PAGAMENTO</span></td>';
                        print_html += '<td style="padding:1px;"><span style="font-size:0.7rem">PAG</span><br/><strong>' + pagina + '</strong>';
                        print_html += '</tr>';

                        print_html += '<tr>';
                        print_html += '<td style="padding:1px;"><span style="font-size:0.7rem">BANCA DI APPOGGIO</span><br/>&nbsp;</td>';
                        print_html += '<td style="padding:1px;"><span style="font-size:0.7rem">ABI</span><br/>&nbsp;</td>';
                        print_html += '<td style="padding:1px;"><span style="font-size:0.7rem">CAB</span><br/>&nbsp;</td>';
                        print_html += '<td style="padding:1px;"><span style="font-size:0.7rem">C/C</span><br/>&nbsp;</td>';
                        print_html += '<td style="padding:1px;"><span style="font-size:0.7rem">CIN</span><br/>&nbsp;</strong>';
                        print_html += '</tr>';



                        print_html += '</table>';

                        print_html += '</div>';




                        print_html += '<div style="margin-top: 20px;height: 49vh;">';

                        print_html += '<table class="table table-responsive table-hover table-striped table-bordered table-condensed">';

                        for (var key in oggetto_buono[nome_buono]) {

                            print_html += '<tr style="font-weight: bold;font-size:0.7rem;">';

                            print_html += '<td style="background-color: rgb(236, 234, 234) !important;padding:1px;">DESCRIZIONE</td>';
                            print_html += '<td style="background-color: rgb(236, 234, 234) !important;padding:1px;">U.M</td>';
                            print_html += '<td style="background-color: rgb(236, 234, 234) !important;padding:1px;">QUANTITA</td>';
                            print_html += '<td style="background-color: rgb(236, 234, 234) !important;padding:1px;">PRZ.UNIT.</td>';
                            print_html += '<td style="background-color: rgb(236, 234, 234) !important;padding:1px;">TOTALE IVATO</td>';
                            print_html += '<td style="background-color: rgb(236, 234, 234) !important;padding:1px;">IVA</td>';

                            print_html += '</tr>';

                            var iva = 10;

                            for (var importo in oggetto_buono[nome_buono][key]["TAGLI"]) {

                                print_html += '<tr>';

                                print_html += '<td style="padding:0px;">BUONI PASTO da Euro ' + parseFloat(importo).toFixed(2) + '</td>';

                                print_html += '<td style="padding:0px;">NR.' + '</td>';

                                print_html += '<td style="padding:0px;">' + parseInt(oggetto_buono[nome_buono][key]["TAGLI"][importo].quantita) + '</td>';

                                print_html += '<td style="padding:0px;">' + parseFloat(importo).toFixed(2) + '</td>';

                                print_html += '<td style="padding:0px;">' + parseFloat(parseFloat(importo).toFixed(2) * parseInt(oggetto_buono[nome_buono][key]["TAGLI"][importo].quantita)).toFixed(2) + '</td>';

                                print_html += '<td style="padding:0px;">' + iva + '</td>';

                                print_html += '</tr>';

                            }


                        }

                        print_html += '</table>';

                        print_html += '</div>';



                        var totale_lordo_buoni_senza_arrotondamento = parseFloat(totale_buoni[nome_buono]);
                        var totale_lordo_buoni_arrotondato = totale_lordo_buoni_senza_arrotondamento.toFixed(2);


                        var sconto = 0;

                        if (comanda.circuito[nome_buono].adeguamento_perc !== null && !isNaN(comanda.circuito[nome_buono].adeguamento_perc)) {
                            sconto = comanda.circuito[nome_buono].adeguamento_perc;
                        }


                        var totale_buoni_ivato_netto = totale_lordo_buoni_senza_arrotondamento / 100 * (100 - sconto);

                        var totale_buoni_ivato_netto_arrotondato = parseFloat(totale_buoni_ivato_netto).toFixed(2);

                        var aliquota_iva = parseFloat(totale_buoni_ivato_netto * iva / (100 + iva)).toFixed(2);

                        var totale_imponibile = parseFloat(parseFloat(totale_buoni_ivato_netto) - parseFloat(aliquota_iva)).toFixed(2);

                        if (sconto !== 0) {
                            sconto += ' %'
                        }

                        print_html += '<div>';

                        print_html += '<table class="table table-responsive table-hover table-striped table-bordered table-condensed">';

                        print_html += '<tr>';
                        print_html += '<td style="padding:1px;"><span style="font-size:0.7rem">TOTALE BUONI IVATO LORDO</span><br/><strong>' + totale_lordo_buoni_arrotondato + '</strong></td>';
                        print_html += '<td rowspan="2" style="padding:1px;"><span style="font-size:0.7rem">IMPONIBILE</span><br/><strong>' + totale_imponibile + '</strong></td>';
                        print_html += '<td rowspan="2" style="padding:1px;"><span style="font-size:0.7rem">% IVA</span><br/><strong>' + iva + '</strong></td>';
                        print_html += '<td rowspan="2" style="padding:1px;"><span style="font-size:0.7rem">Importo IVA</span><br/><strong>' + aliquota_iva + '</strong></td>';
                        print_html += '<td rowspan="2" style="padding:1px;"><span style="font-size:0.7rem">TOTALE FATTURA</span><br/><strong>' + totale_buoni_ivato_netto_arrotondato + '</strong></td>';


                        print_html += '<tr>';
                        print_html += '<td style="padding:1px;"><span style="font-size:0.7rem">SCONTO</span><br/><strong>' + sconto + '</strong></td>';
                        print_html += '</tr>';


                        print_html += '<tr>';
                        print_html += '<td style="padding:1px;"><span style="font-size:0.7rem">TOTALE BUONI IVATO NETTO</span><br/><strong>' + totale_buoni_ivato_netto_arrotondato + '</strong></td>';
                        print_html += '<td style="padding:1px;"><span style="font-size:0.7rem">TOTALE IMPONIBILE</span><br/><strong>' + totale_imponibile + '</strong></td>';
                        print_html += '<td style="padding:1px;"><br/></td>';
                        print_html += '<td style="padding:1px;"><span style="font-size:0.7rem">TOTALE IMPOSTA</span><br/><strong>' + aliquota_iva + '</strong></td>';
                        print_html += '<td style="padding:1px;"><span style="font-size:0.7rem">NETTO A PAGARE</span><br/><strong>' + totale_buoni_ivato_netto_arrotondato + '</strong></td>';




                        print_html += '</table>';

                        print_html += '</div>';

                        print_html += '</div>';

                    }
                }

                $('.print').html(print_html);

                window.print();
            });
        });
    });
}




function riepilogo_buoni_pasto_2() {


    var array_data_partenza = $('#data_incassi_partenza:visible').multiDatesPicker('getDates');
    var array_data_arrivo = $('#data_incassi_arrivo:visible').multiDatesPicker('getDates');

    if (array_data_partenza.length === 1 && array_data_arrivo.length === 1) {

        var yY = array_data_partenza[0].substr(6, 4);
        var yD = array_data_partenza[0].substr(0, 2);
        var yM = array_data_partenza[0].substr(3, 2);

        var h = "00";
        var m = "00";
        var s = "00";
        var ms = "000";
        var data_ieri = yY + '' + yM + '' + yD + '00';


        var Y = array_data_arrivo[0].substr(6, 4);
        var D = array_data_arrivo[0].substr(0, 2);
        var M = array_data_arrivo[0].substr(3, 2);

        var h = "00";
        var m = "00";
        var s = "00";
        var ms = "000";
        var data_oggi = Y + '' + M + '' + D + '24';

    } else {

        var d = new Date();
        d.setDate(d.getDate() + 1);
        var Y = d.getFullYear();
        var M = addZero((d.getMonth() + 1), 2);
        var D = addZero(d.getDate(), 2);
        var h = addZero(d.getHours(), 2);
        var m = addZero(d.getMinutes(), 2);
        var s = addZero(d.getSeconds(), 2);
        var ms = addZero(d.getMilliseconds(), 3);
        var data_oggi = Y + '' + M + '' + D + '24';

        var y = new Date();
        y.setDate(y.getDate());
        var yY = y.getFullYear();
        var yM = addZero((y.getMonth() + 1), 2);
        var yD = addZero(y.getDate(), 2);
        var yh = addZero(y.getHours(), 2);
        var ym = addZero(y.getMinutes(), 2);
        var ys = addZero(y.getSeconds(), 2);
        var yms = addZero(y.getMilliseconds(), 3);
        var data_ieri = yY + '' + yM + '' + yD + '00';

    }

    var array_pc = new Array();

    $('#selezione_pc_incassi input:checked').each(function (i, v) {
        //MOD 16/06/2017 PRIMA ERA v.name
        array_pc.push(v.value);
    });


    if (array_pc.length === 0 || array_pc.length >= 2) {
        var aggiunta_query_nome_pc = '';

        if (array_pc.length >= 1) {
            aggiunta_query_nome_pc += ' and nome_pc IN (' + array_pc.map(function (date) {
                return "'" + date + "'";
            }).join() + ') ';
        }



        var testo_query_circuiti = "select id,ragione_sociale from circuiti;";
        comanda.sincro.query_centrale(testo_query_circuiti, function (rc) {

            var testo_query_teste = " select '1' as n_estrazione,nome_buono ,sum(numero_buoni_pasto) as quantita,sum(buoni_pasto*numero_buoni_pasto) as importo,buoni_pasto as importo_singolo,nome_pc,tavolo from record_teste where cast(substr(progressivo_comanda,5,10) as int)>='" + data_ieri + "' AND cast(substr(progressivo_comanda,5,10) as int)<='" + data_oggi + "' " + aggiunta_query_nome_pc + "  group by nome_buono,buoni_pasto \n\
 union all \n\
select '2' as n_estrazione,nome_buono_2  as nome_buono,sum(numero_buoni_pasto_2) as quantita,sum(buoni_pasto_2*numero_buoni_pasto_2) as importo,buoni_pasto_2 as importo_singolo,nome_pc,tavolo from record_teste  where cast(substr(progressivo_comanda,5,10) as int)>='" + data_ieri + "' AND cast(substr(progressivo_comanda,5,10) as int)<='" + data_oggi + "'  " + aggiunta_query_nome_pc + " group by  nome_buono_2,buoni_pasto_2\n\
 union all \n\
select '3' as n_estrazione,nome_buono_3  as nome_buono,sum(numero_buoni_pasto_3),sum(buoni_pasto_3*numero_buoni_pasto_3) as importo,buoni_pasto_3 as importo_singolo,nome_pc,tavolo from record_teste  where cast(substr(progressivo_comanda,5,10) as int)>='" + data_ieri + "' AND cast(substr(progressivo_comanda,5,10) as int)<='" + data_oggi + "' " + aggiunta_query_nome_pc + "  group by  nome_buono_3,buoni_pasto_3\n\
 union all \n\
select '4' as n_estrazione,nome_buono_4  as nome_buono,sum(numero_buoni_pasto_4) as quantita,sum(buoni_pasto_4*numero_buoni_pasto_4) as importo,buoni_pasto_4 as importo_singolo,nome_pc,tavolo from record_teste  where cast(substr(progressivo_comanda,5,10) as int)>='" + data_ieri + "' AND cast(substr(progressivo_comanda,5,10) as int)<='" + data_oggi + "'  " + aggiunta_query_nome_pc + " group by  nome_buono_4,buoni_pasto_4\n\
 union all \n\
select '5' as n_estrazione, nome_buono_5 as nome_buono ,sum(numero_buoni_pasto_5) as quantita,sum(buoni_pasto_5*numero_buoni_pasto_5) as importo,buoni_pasto_5 as importo_singolo,nome_pc,tavolo from record_teste  where cast(substr(progressivo_comanda,5,10) as int)>='" + data_ieri + "' AND cast(substr(progressivo_comanda,5,10) as int)<='" + data_oggi + "' " + aggiunta_query_nome_pc + " group by  nome_buono_5,buoni_pasto_5;";
            comanda.sincro.query_centrale(testo_query_teste, function (rt) {

                var totale_buoni = 0;

                var oggetto_circuiti = new Object();
                rc.forEach(function (el) {
                    oggetto_circuiti[el.id] = el.ragione_sociale;
                });
                var oggetto_buono = new Object();
                rt.forEach(function (el) {
                    if (el.nome_buono !== "") {
                        if (oggetto_buono[el.nome_buono] !== undefined) {

                            if (oggetto_buono[el.nome_buono]["TAGLI"][el.importo_singolo] === undefined) {
                                oggetto_buono[el.nome_buono]["TAGLI"][el.importo_singolo] = new Object();
                                oggetto_buono[el.nome_buono]["TAGLI"][el.importo_singolo].quantita = parseInt(el.quantita);
                            } else
                            {
                                oggetto_buono[el.nome_buono]["TAGLI"][el.importo_singolo].quantita += parseInt(el.quantita);
                            }

                            oggetto_buono[el.nome_buono].quantita += parseInt(el.quantita);
                            oggetto_buono[el.nome_buono].importo += parseFloat(el.importo);
                            totale_buoni += parseFloat(el.importo);
                        } else
                        {
                            oggetto_buono[el.nome_buono] = new Object();

                            oggetto_buono[el.nome_buono]["TAGLI"] = new Object();

                            if (oggetto_buono[el.nome_buono]["TAGLI"][el.importo_singolo] === undefined) {
                                oggetto_buono[el.nome_buono]["TAGLI"][el.importo_singolo] = new Object();
                                oggetto_buono[el.nome_buono]["TAGLI"][el.importo_singolo].quantita = parseInt(el.quantita);
                            } else
                            {
                                oggetto_buono[el.nome_buono]["TAGLI"][el.importo_singolo].quantita += parseInt(el.quantita);
                            }


                            oggetto_buono[el.nome_buono].ragione_sociale = oggetto_circuiti[el.nome_buono];
                            oggetto_buono[el.nome_buono].quantita = parseInt(el.quantita);
                            oggetto_buono[el.nome_buono].importo = parseFloat(el.importo);
                            totale_buoni += parseFloat(el.importo);
                        }
                    }
                });
                console.log("RIEPILOGO BUONI", oggetto_buono);

                var data = comanda.funzionidb.data_attuale();
                var builder = new epson.ePOSBuilder();
                var print_xml = "<div>";

                builder.addTextFont(builder.FONT_A);
                builder.addTextAlign(builder.ALIGN_CENTER);
                builder.addTextSize(1, 1);
                builder.addText("-----------------------------------------\n");

                builder.addTextSize(2, 2);

                print_xml += "<h3>RIEPILOGO BUONI PASTO</h3>";
                builder.addText('RIEPILOGO BUONI PASTO\n');

                var now = new Date();
                var data_stampa = now.format("dd-mm-yyyy H:MM");

                print_xml += "<h3>DATA STAMPA:&nbsp;" + data_stampa + "</h3>";

                print_xml += "<h4>TOTALE</h4>";
                builder.addText('TOTALE\n');

                builder.addTextSize(1, 1);
                builder.addText("-----------------------------------------\n");

                if (array_data_partenza.length === 1 && array_data_arrivo.length === 1) {
                    print_xml += "<h5>" + 'DA: ' + array_data_partenza[0] + ' A: ' + array_data_arrivo[0] + "</h5>";
                    builder.addText('DA: ' + array_data_partenza[0] + ' A: ' + array_data_arrivo[0]);
                } else
                {
                    print_xml += "<h5>" + data.replace(/-/gi, '/') + "</h5>";
                    builder.addText(data.replace(/-/gi, '/') + '\n');
                }
                builder.addTextAlign(builder.ALIGN_LEFT);
                builder.addTextSize(2, 2);
                builder.addFeedLine(1);
                builder.addTextSize(1, 1);
                builder.addTextStyle(false, false, true, builder.COLOR_1);

                print_xml += "<h5>" + 'OPERATORE: ' + comanda.operatore + "</h5>";
                builder.addText('OPERATORE: ' + comanda.operatore + '\n');

                //builder.addText(data.replace(/-/gi, '/') + '\n');
                builder.addFeedLine(1);
                builder.addTextSize(1, 1);
                for (var key in oggetto_buono) {
                    if (oggetto_buono[key].ragione_sociale === undefined) {
                        oggetto_buono[key].ragione_sociale = "CIRCUITO ELIMINATO. ID: " + key;
                    }
                    builder.addText("------------------------------------------------\n");

                    print_xml += "<h5>" + 'RAGIONE SOCIALE: ' + oggetto_buono[key].ragione_sociale + "</h5>";
                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                    builder.addText('RAGIONE SOCIALE: ' + oggetto_buono[key].ragione_sociale + '\n');
                    builder.addTextStyle(false, false, false, builder.COLOR_1);

                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                    builder.addFeedLine(1);

                    print_xml += '<table class="table table-responsive table-hover table-striped table-bordered table-condensed">';

                    print_xml += "<tr>";
                    print_xml += "<td style='padding:0;'>";
                    print_xml += "TAGLIO EURO";
                    builder.addText("TAGLIO EURO");
                    print_xml += "</td>";
                    builder.addTextPosition(250);
                    print_xml += "<td style='padding:0;'>";
                    print_xml += "QTA";
                    builder.addText("QTA");
                    print_xml += "</td>";
                    builder.addTextPosition(420);
                    print_xml += "<td style='padding:0;'>";
                    print_xml += "TOTALE EURO";
                    builder.addText("TOTALE EURO\n");
                    print_xml += "</td>";

                    print_xml += "</tr>";
                    builder.addTextStyle(false, false, false, builder.COLOR_1);

                    for (var importo in oggetto_buono[key]["TAGLI"]) {
                        print_xml += "<tr>";
                        print_xml += "<td style='padding:0;'>";
                        print_xml += parseFloat(importo).toFixed(2);
                        builder.addTextPosition(0);
                        builder.addText(parseFloat(importo).toFixed(2));
                        print_xml += "</td>";
                        builder.addTextPosition(250);
                        print_xml += "<td style='padding:0;'>";
                        print_xml += parseInt(oggetto_buono[key]["TAGLI"][importo].quantita);
                        builder.addText(parseInt(oggetto_buono[key]["TAGLI"][importo].quantita));
                        print_xml += "</td>";
                        builder.addTextPosition(420);
                        print_xml += "<td style='padding:0;'>";
                        print_xml += parseFloat(parseFloat(importo).toFixed(2) * parseInt(oggetto_buono[key]["TAGLI"][importo].quantita)).toFixed(2);
                        builder.addText(parseFloat(parseFloat(importo).toFixed(2) * parseInt(oggetto_buono[key]["TAGLI"][importo].quantita)).toFixed(2) + "\n");
                        print_xml += "</td>";
                        print_xml += "</tr>";
                    }

                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                    print_xml += "<tr>";
                    print_xml += "<td style='padding:0;'>";
                    print_xml += 'TOTALE';
                    builder.addText('TOTALE');
                    print_xml += "</td>";
                    builder.addTextPosition(250);
                    print_xml += "<td style='padding:0;'>";
                    print_xml += oggetto_buono[key].quantita;
                    builder.addText(oggetto_buono[key].quantita);
                    print_xml += "</td>";
                    builder.addTextPosition(420);
                    print_xml += "<td style='padding:0;'>";
                    print_xml += parseFloat(oggetto_buono[key].importo).toFixed(2);
                    builder.addText(parseFloat(oggetto_buono[key].importo).toFixed(2) + '\n');
                    print_xml += "</td>";
                    print_xml += "</tr>";
                    builder.addTextStyle(false, false, false, builder.COLOR_1);

                    print_xml += "</table>";

                }

                builder.addText("------------------------------------------------\n");

                builder.addTextStyle(false, false, true, builder.COLOR_1);
                print_xml += "<h5>" + 'TOTALE BUONI: Euro ' + parseFloat(totale_buoni).toFixed(2) + "</h5>";
                builder.addText('TOTALE BUONI: Euro ' + parseFloat(totale_buoni).toFixed(2) + '\n');

                print_xml += "</div>";

                $('.print').html(print_xml);

                window.print();


                builder.addTextSize(1, 1);
                builder.addCut(builder.CUT_FEED);
                var request = builder.toString();
                //CONTENUTO CONTO
                //console.log(request);

                //Create a SOAP envelop
                var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
                //Create an XMLHttpRequest object
                var xhr = new XMLHttpRequest();
                //Set the end point address
                var url = 'http://' + comanda.intelligent_non_fiscale + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                //Open an XMLHttpRequest object
                xhr.open('POST', url, true);
                //<Header settings>
                xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
                xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
                xhr.setRequestHeader('SOAPAction', '""');
                // Send print document
                xhr.send(soap);
            });
        });
    }
}



function riepilogo_buoni_pasto() {
    print_xml = '';

    riepilogo_buoni_pasto_2();


    var array_data_partenza = $('#data_incassi_partenza:visible').multiDatesPicker('getDates');
    var array_data_arrivo = $('#data_incassi_arrivo:visible').multiDatesPicker('getDates');

    if (array_data_partenza.length === 1 && array_data_arrivo.length === 1) {

        var yY = array_data_partenza[0].substr(6, 4);
        var yD = array_data_partenza[0].substr(0, 2);
        var yM = array_data_partenza[0].substr(3, 2);

        var h = "00";
        var m = "00";
        var s = "00";
        var ms = "000";
        var data_ieri = yY + '' + yM + '' + yD + '00';


        var Y = array_data_arrivo[0].substr(6, 4);
        var D = array_data_arrivo[0].substr(0, 2);
        var M = array_data_arrivo[0].substr(3, 2);

        var h = "00";
        var m = "00";
        var s = "00";
        var ms = "000";
        var data_oggi = Y + '' + M + '' + D + '24';

    } else {

        var d = new Date();
        d.setDate(d.getDate());
        var Y = d.getFullYear();
        var M = addZero((d.getMonth() + 1), 2);
        var D = addZero(d.getDate(), 2);
        var h = addZero(d.getHours(), 2);
        var m = addZero(d.getMinutes(), 2);
        var s = addZero(d.getSeconds(), 2);
        var ms = addZero(d.getMilliseconds(), 3);
        var data_oggi = Y + '' + M + '' + D + '24';
        var y = new Date();
        y.setDate(y.getDate());
        var yY = y.getFullYear();
        var yM = addZero((y.getMonth() + 1), 2);
        var yD = addZero(y.getDate(), 2);
        var yh = addZero(y.getHours(), 2);
        var ym = addZero(y.getMinutes(), 2);
        var ys = addZero(y.getSeconds(), 2);
        var yms = addZero(y.getMilliseconds(), 3);
        var data_ieri = yY + '' + yM + '' + yD + '00';

    }

    var array_pc = new Array();

    $('#selezione_pc_incassi input:checked').each(function (i, v) {
        //MOD 16/06/2017 PRIMA ERA v.name
        array_pc.push(v.value);
    });

    var aggiunta_query_nome_pc = '';

    if (array_pc.length >= 1) {
        aggiunta_query_nome_pc += ' and nome_pc IN (' + array_pc.map(function (date) {
            return "'" + date + "'";
        }).join() + ') ';
    }

    var testo_query_circuiti = "select id,ragione_sociale from circuiti;";
    comanda.sincro.query_centrale(testo_query_circuiti, function (rc) {
        var testo_query_teste = " select '1' as n_estrazione,nome_buono ,sum(numero_buoni_pasto) as quantita,sum(buoni_pasto*numero_buoni_pasto) as importo,buoni_pasto as importo_singolo,nome_pc,tavolo from record_teste where cast(substr(progressivo_comanda,5,10) as int)>='" + data_ieri + "' AND cast(substr(progressivo_comanda,5,10) as int)<='" + data_oggi + "'  " + aggiunta_query_nome_pc + "  group by tavolo,nome_buono, buoni_pasto \n\
 union all \n\
select '2' as n_estrazione,nome_buono_2  as nome_buono,sum(numero_buoni_pasto_2) as quantita,sum(buoni_pasto_2*numero_buoni_pasto_2) as importo,buoni_pasto_2 as importo_singolo,nome_pc,tavolo from record_teste  where cast(substr(progressivo_comanda,5,10) as int)>='" + data_ieri + "' AND cast(substr(progressivo_comanda,5,10) as int)<='" + data_oggi + "'  " + aggiunta_query_nome_pc + "  group by  tavolo,nome_buono_2, buoni_pasto_2\n\
 union all \n\
select '3' as n_estrazione,nome_buono_3  as nome_buono,sum(numero_buoni_pasto_3),sum(buoni_pasto_3*numero_buoni_pasto_3) as importo,buoni_pasto_3 as importo_singolo,nome_pc,tavolo from record_teste  where cast(substr(progressivo_comanda,5,10) as int)>='" + data_ieri + "' AND cast(substr(progressivo_comanda,5,10) as int)<='" + data_oggi + "'   " + aggiunta_query_nome_pc + " group by  tavolo,nome_buono_3, buoni_pasto_3\n\
 union all \n\
select '4' as n_estrazione,nome_buono_4  as nome_buono,sum(numero_buoni_pasto_4) as quantita,sum(buoni_pasto_4*numero_buoni_pasto_4) as importo,buoni_pasto_4 as importo_singolo,nome_pc,tavolo from record_teste  where cast(substr(progressivo_comanda,5,10) as int)>='" + data_ieri + "' AND cast(substr(progressivo_comanda,5,10) as int)<='" + data_oggi + "'  " + aggiunta_query_nome_pc + "  group by  tavolo,nome_buono_4, buoni_pasto_4\n\
 union all \n\
select '5' as n_estrazione, nome_buono_5 as nome_buono ,sum(numero_buoni_pasto_5) as quantita,sum(buoni_pasto_5*numero_buoni_pasto_5) as importo,buoni_pasto_5 as importo_singolo,nome_pc,tavolo from record_teste  where cast(substr(progressivo_comanda,5,10) as int)>='" + data_ieri + "' AND cast(substr(progressivo_comanda,5,10) as int)<='" + data_oggi + "'  " + aggiunta_query_nome_pc + " group by  tavolo,nome_buono_5, buoni_pasto_5;";
        comanda.sincro.query_centrale(testo_query_teste, function (rt) {

            var totale_buoni = new Object();

            var oggetto_circuiti = new Object();

            rc.forEach(function (el) {
                oggetto_circuiti[el.id] = el.ragione_sociale;
            });


            var oggetto_buono = new Object();
            rt.forEach(function (el) {
                if (el.nome_buono !== "") {

                    if (oggetto_buono[el.tavolo] === undefined) {
                        oggetto_buono[el.tavolo] = new Object();
                    }
                    if (totale_buoni[el.tavolo] === undefined) {
                        totale_buoni[el.tavolo] = 0;
                    }
                    if (oggetto_buono[el.tavolo][el.nome_buono] !== undefined) {
                        if (oggetto_buono[el.tavolo][el.nome_buono]["TAGLI"][el.importo_singolo] === undefined) {
                            oggetto_buono[el.tavolo][el.nome_buono]["TAGLI"][el.importo_singolo] = new Object();
                            oggetto_buono[el.tavolo][el.nome_buono]["TAGLI"][el.importo_singolo].quantita = parseInt(el.quantita);
                        } else
                        {
                            oggetto_buono[el.tavolo][el.nome_buono]["TAGLI"][el.importo_singolo].quantita += parseInt(el.quantita);
                        }
                        oggetto_buono[el.tavolo][el.nome_buono].quantita += parseInt(el.quantita);
                        oggetto_buono[el.tavolo][el.nome_buono].importo += parseFloat(el.importo);
                        totale_buoni[el.tavolo] += parseFloat(el.importo);
                    } else
                    {
                        oggetto_buono[el.tavolo][el.nome_buono] = new Object();
                        oggetto_buono[el.tavolo][el.nome_buono]["TAGLI"] = new Object();

                        if (oggetto_buono[el.tavolo][el.nome_buono]["TAGLI"][el.importo_singolo] === undefined) {
                            oggetto_buono[el.tavolo][el.nome_buono]["TAGLI"][el.importo_singolo] = new Object();
                            oggetto_buono[el.tavolo][el.nome_buono]["TAGLI"][el.importo_singolo].quantita = parseInt(el.quantita);
                        } else
                        {
                            oggetto_buono[el.tavolo][el.nome_buono]["TAGLI"][el.importo_singolo].quantita += parseInt(el.quantita);
                        }
                        oggetto_buono[el.tavolo][el.nome_buono].ragione_sociale = oggetto_circuiti[el.nome_buono];
                        oggetto_buono[el.tavolo][el.nome_buono].quantita = parseInt(el.quantita);
                        oggetto_buono[el.tavolo][el.nome_buono].importo = parseFloat(el.importo);
                        totale_buoni[el.tavolo] += parseFloat(el.importo);
                    }
                }
            });
            console.log("RIEPILOGO BUONI", oggetto_buono);

            var data = comanda.funzionidb.data_attuale();
            var builder = new epson.ePOSBuilder();

            var print_xml = "<div>";

            for (var tavolo in  oggetto_buono) {
                builder.addTextFont(builder.FONT_A);
                builder.addTextAlign(builder.ALIGN_CENTER);
                builder.addTextSize(1, 1);
                builder.addText("-----------------------------------------\n");
                builder.addTextSize(2, 2);
                print_xml += "<h3>" + 'RIEPILOGO BUONI PASTO' + "</h3>";
                builder.addText('RIEPILOGO BUONI PASTO\n');

                //Temporaneo per Brovedani
                var tavolo_rinominato = '';
                switch (tavolo) {
                    case "CONTINUO_1":
                        tavolo_rinominato = "GREEN CORNER";
                        break;
                    case "CONTINUO_2":
                        tavolo_rinominato = "FOCACCERIA";
                        break;
                    case "CONTINUO_3":
                        tavolo_rinominato = "TRAMEZZINI";
                        break;
                }

                print_xml += "<h4>" + 'CASSA: ' + tavolo_rinominato + "</h4>";

                builder.addText('CASSA: ' + tavolo_rinominato + '\n');
                builder.addTextSize(1, 1);
                builder.addText("-----------------------------------------\n");
                if (array_data_partenza.length === 1 && array_data_arrivo.length === 1) {
                    print_xml += "<h5" + 'DA: ' + array_data_partenza[0] + ' A: ' + array_data_arrivo[0] + "</h5>";
                    builder.addText('DA: ' + array_data_partenza[0] + ' A: ' + array_data_arrivo[0]);
                } else
                {
                    print_xml += "<h5>" + data.replace(/-/gi, '/') + "</h5>";
                    builder.addText(data.replace(/-/gi, '/') + '\n');
                }
                builder.addTextAlign(builder.ALIGN_LEFT);
                builder.addTextSize(2, 2);
                builder.addFeedLine(1);
                builder.addTextSize(1, 1);
                builder.addTextStyle(false, false, true, builder.COLOR_1);
                print_xml += "<h5>" + 'OPERATORE: ' + comanda.operatore + "</h5>";
                builder.addText('OPERATORE: ' + comanda.operatore + '\n');

                builder.addFeedLine(1);
                builder.addTextSize(1, 1);



                for (var key in oggetto_buono[tavolo]) {
                    if (oggetto_buono[tavolo][key].ragione_sociale === undefined) {
                        oggetto_buono[tavolo][key].ragione_sociale = "CIRCUITO ELIMINATO. ID: " + key;
                    }


                    builder.addText("------------------------------------------------\n");
                    builder.addTextStyle(false, false, true, builder.COLOR_1);

                    print_xml += "<h5>" + 'RAGIONE SOCIALE: ' + oggetto_buono[tavolo][key].ragione_sociale + "</h5>";

                    builder.addText('RAGIONE SOCIALE: ' + oggetto_buono[tavolo][key].ragione_sociale + '\n');
                    builder.addTextStyle(false, false, false, builder.COLOR_1);

                    print_xml += '<table class="table table-responsive table-hover table-striped table-bordered table-condensed">';

                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                    builder.addFeedLine(1);
                    print_xml += "<tr>";
                    print_xml += "<td>";
                    print_xml += "TAGLIO EURO";
                    builder.addText("TAGLIO EURO");
                    print_xml += "</td>";

                    builder.addTextPosition(250);
                    print_xml += "<td>";
                    print_xml += "QTA";
                    builder.addText("QTA");
                    print_xml += "</td>";
                    builder.addTextPosition(420);
                    print_xml += "<td>";
                    print_xml += "TOTALE EURO";
                    builder.addText("TOTALE EURO\n");
                    print_xml += "</td>";
                    print_xml += "</tr>";
                    builder.addTextStyle(false, false, false, builder.COLOR_1);


                    for (var importo in oggetto_buono[tavolo][key]["TAGLI"]) {

                        print_xml += "<tr>";
                        print_xml += "<td>";
                        print_xml += parseFloat(importo).toFixed(2);
                        builder.addTextPosition(0);
                        builder.addText(parseFloat(importo).toFixed(2));
                        print_xml += "</td>";
                        builder.addTextPosition(250);
                        print_xml += "<td>";
                        print_xml += parseInt(oggetto_buono[tavolo][key]["TAGLI"][importo].quantita);
                        builder.addText(parseInt(oggetto_buono[tavolo][key]["TAGLI"][importo].quantita));
                        print_xml += "</td>";
                        builder.addTextPosition(420);
                        print_xml += "<td>";
                        print_xml += parseFloat(parseFloat(importo).toFixed(2) * parseInt(oggetto_buono[tavolo][key]["TAGLI"][importo].quantita)).toFixed(2);
                        builder.addText(parseFloat(parseFloat(importo).toFixed(2) * parseInt(oggetto_buono[tavolo][key]["TAGLI"][importo].quantita)).toFixed(2) + "\n");
                        print_xml += "</td>";
                        print_xml += "</tr>";


                    }
                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                    print_xml += "<tr>";
                    print_xml += "<td>";
                    print_xml += 'TOTALE';
                    builder.addText('TOTALE');
                    print_xml += "</td>";
                    builder.addTextPosition(250);
                    print_xml += "<td>";
                    print_xml += oggetto_buono[tavolo][key].quantita;
                    builder.addText(oggetto_buono[tavolo][key].quantita);
                    print_xml += "</td>";
                    builder.addTextPosition(420);
                    print_xml += "<td>";
                    print_xml += parseFloat(oggetto_buono[tavolo][key].importo).toFixed(2);
                    builder.addText(parseFloat(oggetto_buono[tavolo][key].importo).toFixed(2) + '\n');
                    print_xml += "</td>";
                    print_xml += "</tr>";
                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                    //builder.addFeedLine(2);
                    print_xml += "</table>";
                }
                builder.addText("------------------------------------------------\n");

                builder.addTextStyle(false, false, true, builder.COLOR_1);
                print_xml += "<h5>" + 'TOTALE BUONI: Euro ' + parseFloat(totale_buoni[tavolo]).toFixed(2) + "</h5>";
                builder.addText('TOTALE BUONI: Euro ' + parseFloat(totale_buoni[tavolo]).toFixed(2) + '\n');



                builder.addTextSize(1, 1);
                builder.addCut(builder.CUT_FEED);
            }

            print_xml += "</div>";

            //$('.print').html(print_xml);

            //window.print();

            var request = builder.toString();
            //CONTENUTO CONTO
            //console.log(request);

            //Create a SOAP envelop
            var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
            //Create an XMLHttpRequest object
            var xhr = new XMLHttpRequest();
            //Set the end point address
            var url = 'http://' + comanda.intelligent_non_fiscale + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
            //Open an XMLHttpRequest object
            xhr.open('POST', url, true);
            //<Header settings>
            xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
            xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
            xhr.setRequestHeader('SOAPAction', '""');
            // Send print document
            xhr.send(soap);
        });
    });
}

function nuovo_circuito_onfly() {


    bootbox.prompt({
        size: "medium",
        title: "Indica il nome del nuovo circuito",
        value: "",
        callback: function (ragione_sociale) {
            if (ragione_sociale !== null) {
                var testo_query = "select id from circuiti order by cast(id as int) desc limit 1;";
                comanda.sincro.query(testo_query, function (risultato) {

                    var id = 0;
                    if (risultato[0] !== undefined && risultato[0].id !== undefined) {
                        id = parseInt(risultato[0].id) + 1;
                    }
                    testo_query = "insert into circuiti (id,ragione_sociale) VALUES (" + id + ",'" + ragione_sociale + "');";
                    comanda.sock.send({tipo: "nuovo_circuito_onfly", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                    //comanda.array_dbcentrale_mancanti.push(testo_query);
                    comanda.sincro.query(testo_query, function () {
                        apri_popup_buoni(id, $('[name="scelta_buoni__importo_taglio"]').val());
                    });
                    comanda.sincro.query_cassa();
                });
            }
        }});
    setTimeout(function () {
        $('.bootbox-input-text:visible').trigger('touchend');
    }, 500);
}

function nuovo_taglio_onfly() {

    bootbox.prompt({

        size: "medium",
        title: "Precisa l'importo",
        value: "",
        className: 'kb_num',
        callback: function (importo) {

            if (importo !== null) {
                var testo_query = "select importo from tagli_buoni where importo='" + parseFloat(importo).toFixed(2) + "' and id_buono='" + $('[name="scelta_buoni__nome_circuito"]').val() + "' order by cast(id as int) desc limit 1;";
                var check = alasql(testo_query);

                if (check[0] !== undefined) {
                    bootbox.alert("L'importo del taglio &egrave; gi&agrave; stato inserito.");
                    return false;
                }

                var testo_query = "select id from tagli_buoni order by cast(id as int) desc limit 1;";
                comanda.sincro.query(testo_query, function (risultato) {

                    var id = 0;
                    if (risultato[0] !== undefined && risultato[0].id !== undefined) {
                        id = parseInt(risultato[0].id) + 1;
                    }

                    testo_query = "insert into tagli_buoni (id,id_buono,importo) VALUES (" + id + ",'" + $('[name="scelta_buoni__nome_circuito"]').val() + "','" + parseFloat(importo.replace(/,/g, '.')).toFixed(2) + "');";
                    console.log("QUERY BUONO FLY", testo_query);
                    comanda.sock.send({tipo: "nuovo_taglio", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                    //comanda.array_dbcentrale_mancanti.push(testo_query);
                    comanda.sincro.query(testo_query, function () {
                        apri_popup_buoni($('[name="scelta_buoni__nome_circuito"]').val(), parseFloat(importo.replace(/,/g, '.')).toFixed(2));
                    });
                    comanda.sincro.query_cassa();
                });
            }
        }});
    setTimeout(function () {
        $('.bootbox-input-text:visible').trigger('touchend');
    }, 500);
}

function elimina_taglio_onfly() {

    var id_buono = $('[name="scelta_buoni__nome_circuito"]').val();
    var importo = $('[name="scelta_buoni__importo_taglio"]').val();
    var testo_query = "DELETE FROM tagli_buoni WHERE importo='" + importo + "' and id_buono='" + id_buono + "';";
    //comanda.array_dbcentrale_mancanti.push(testo_query);
    comanda.sock.send({tipo: "eliminazione_tagli", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
    comanda.sincro.query(testo_query, function () {
        apri_popup_buoni($('[name="scelta_buoni__nome_circuito"]').val(), $('[name="scelta_buoni__importo_taglio"]').val());
    });
    comanda.sincro.query_cassa();
}

function elimina_circuito_onfly() {

    var id_circuito = $('[name="scelta_buoni__nome_circuito"]').val();
    var testo_query = "DELETE FROM circuiti WHERE id=" + id_circuito + ";";
    //comanda.array_dbcentrale_mancanti.push(testo_query);
    comanda.sock.send({tipo: "eliminazione_circuiti", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
    comanda.sincro.query(testo_query, function () {
        apri_popup_buoni($('[name="scelta_buoni__nome_circuito"]').val(), $('[name="scelta_buoni__importo_taglio"]').val());
    });
    comanda.sincro.query_cassa();
}

function autoQTA_buoni() {
    if ($('[name="scelta_buoni__importo_taglio"]').val() !== null && $('[name="scelta_buoni__importo_taglio"]').val().length > 0) {

        var memorizzati = 0;
        //Calcola anche i buoni memorizzati
        if (buoni_memorizzati.length > 0) {
            buoni_memorizzati.forEach(function (val, index) {
                memorizzati += parseInt(val.quantita) * parseFloat(val.importo_taglio);
            });
        }

        var buoni_acquisto_incassati = parseFloat($('[name="scelta_buoni__buono_spesa"]').val());

        var ammontare = parseFloat($('[name="scelta_buoni__importo_taglio"]').val());
        var totale = parseFloat($('.scelta_buoni__totale_conto').html()) - buoni_acquisto_incassati;
        var restante = totale - memorizzati;
        var quantita = parseInt(restante / ammontare);
        $('[name="scelta_buoni__quantita_buoni"]').val(quantita);
        $('[name="scelta_buoni__quantita_buoni"]').trigger('change');
    } else {
        alert("Devi scegliere un taglio - ed un eventuale buono acquisto da inserire - per fare il calcolo automatico.");
    }
}

async function apri_popup_buoni(default_buono, default_importo) {

    await seleziona_metodo_pagamento('SCONTRINO FISCALE', undefined, true);


    function calcolo_restante_interna() {
        /*if ($('[name="scelta_buoni__importo_taglio"]').val() !== null && $('[name="scelta_buoni__importo_taglio"]').val().length > 0) {*/

        /* MODIFICATO 02 12 2020 ALTRIMENTI NON VEDE IL RESTANTE SE MTTE SOLO IL BUONO ACQUISTO E NON I BUONI */

        var ammontare = 0;
        //Calcola anche i buoni memorizzati
        if (buoni_memorizzati.length > 0) {
            buoni_memorizzati.forEach(function (val, index) {
                ammontare += parseInt(val.quantita) * parseFloat(val.importo_taglio);
            });
        }

        $('span.scelta_buoni__ammontare_buono').html(ammontare.toFixed(2));
        var restante = parseFloat($(comanda.totale_scontrino).html()) - parseFloat(ammontare) - parseFloat($('[name="scelta_buoni__buono_spesa"]').val());
        $('span.scelta_buoni__restante').html(restante.toFixed(2));
        /*}*/
    }



    console.log("ID BUONO: " + default_buono + " - ID IMPORTO: " + default_importo);
    var testo_query = "SELECT id,ragione_sociale FROM circuiti ORDER BY ordinamento asc,ragione_sociale ASC;";
    let risultato_circuiti = alasql(testo_query);

    //ALTRIMENTI SI CREANO 1000 LISTENERS SOVRAPPOSTI
    $('[name="scelta_buoni__nome_circuito"]').off();
    //SVUOTA HTML TENDINA
    $('[name="scelta_buoni__nome_circuito"]').html('');
    $('span.scelta_buoni__ammontare_buono').html('');
    $('span.scelta_buoni__restante').html('');
    $('[name="scelta_buoni__buono_spesa"]').val('0.00');
    $('[name="scelta_buoni__quantita_buoni"]').val('1');
    //RIEMPIO LA TENDINA CON I VALORI NUOVI (SE CI FOSSERO)
    risultato_circuiti.forEach(function (dati_buono) {

        $('[name="scelta_buoni__nome_circuito"]').append('<option value="' + dati_buono.id + '">' + dati_buono.ragione_sociale + '</option>');
    });
    //LISTENER SCELTA TAGLI IN BASE AL CIRCUITO
    $('[name="scelta_buoni__nome_circuito"]').change(function () {

        //ALTRIMENTI SI CREANO 1000 LISTENERS SOVRAPPOSTI (2°)
        $('[name="scelta_buoni__quantita_buoni"],[name="scelta_buoni__importo_taglio"],[name="scelta_buoni__buono_spesa"]').off();
        var id_buono = $('[name="scelta_buoni__nome_circuito"]').val();
        var testo_query = "SELECT id,importo FROM tagli_buoni WHERE id_buono='" + id_buono + "' ORDER BY cast(importo as float) ASC;";
        console.log("QUERY TAGLI", testo_query);
        /* comanda.sincro.query(testo_query, function (risultato_tagli) {*/
        let risultato_tagli = alasql(testo_query);

        $('[name="scelta_buoni__importo_taglio"]').html('');
        //APPENDO UN TAGLIO VUOTO, ALTRIMENTI MI INCASINO PER SELEZIONARE
        $('[name="scelta_buoni__importo_taglio"]').append('<option></option>');
        risultato_tagli.forEach(function (dati_taglio) {

            $('[name="scelta_buoni__importo_taglio"]').append('<option value="' + dati_taglio.importo + '">' + dati_taglio.importo + '</option>');
        });
        //Visualizzo buoni memorizzati
        var opzioni_buoni_memorizzati = '';
        buoni_memorizzati.forEach(function (val, index) {
            opzioni_buoni_memorizzati += '<option value="' + index + '">' + val.quantita + 'x' + val.importo_taglio + 'x' + val.nome_circuito + '</option>';
        });
        $('[name="scelta_buoni__buoni_memorizzati"]').html(opzioni_buoni_memorizzati);
        //SE CAMBIO TAGLIO O QUANTITA RICALCOLA L'AMMONTARE DEL BUONO E LE VARIE ROBE



        $('[name="scelta_buoni__quantita_buoni"],[name="scelta_buoni__importo_taglio"]').change(function () {

            calcolo_restante_interna();

        });

        $('[name="scelta_buoni__buono_spesa"]').on('input', function () {

            calcolo_restante_interna();

        });


        /*});*/

    });
    //IN QUESTO PUNTO ALTRIMENTI QUELLI SCELTI DI DEFAULT LI SBAGLIA
    $('[name="scelta_buoni__nome_circuito"]').trigger('change');
    if (default_buono !== undefined)
    {
        $('[name="scelta_buoni__nome_circuito"] option[value="' + default_buono + '"]').attr("selected", true);
        if (default_importo !== undefined)
        {
            $('[name="scelta_buoni__nome_circuito"]').trigger('change');
            setTimeout(function () {
                $('[name="scelta_buoni__importo_taglio"] option[value="' + default_importo + '"]').attr("selected", true);
                $('[name="scelta_buoni__importo_taglio"]').trigger('change');
            }, 250);
        }
    }

    $('span.scelta_buoni__totale_conto').html($('#popup_metodo_pagamento_da_pagare').html());
    //orginale_righa_comentato//$('span.scelta_buoni__totale_conto').html($(comanda.totale_scontrino).html());
    var ammontare = 0;
    //Calcola anche i buoni memorizzati
    if (buoni_memorizzati.length > 0) {
        buoni_memorizzati.forEach(function (val, index) {
            ammontare += parseInt(val.quantita) * parseFloat(val.importo_taglio);
        });
    }

    $('span.scelta_buoni__ammontare_buono').html(ammontare.toFixed(2));
    //var restante = parseFloat($(comanda.totale_scontrino).html()) - parseFloat(ammontare) - parseFloat($('[name="scelta_buoni__buono_spesa"]').val());
    var restante = parseFloat($('#popup_metodo_pagamento_da_pagare').html()) - parseFloat(ammontare) - parseFloat($('[name="scelta_buoni__buono_spesa"]').val());
    $('span.scelta_buoni__restante').html(restante.toFixed(2));
    $('#scelta_buoni').modal('show');
    $("[name='codice_lotteria']").val(await fa_richiesta_codice_lotteria());
}



function aggiungi_buono_memorizzato() {
    if ($('[name="scelta_buoni__buoni_memorizzati"] option').length < 5) {
        var obj_buono = new Object();
        obj_buono.id_circuito = $('[name="scelta_buoni__nome_circuito"]').val();
        obj_buono.nome_circuito = $('[name="scelta_buoni__nome_circuito"] option:selected').text();
        obj_buono.importo_taglio = $('[name="scelta_buoni__importo_taglio"]').val();
        obj_buono.quantita = $('[name="scelta_buoni__quantita_buoni"]').val();
        if (obj_buono.nome_circuito.length > 0 && obj_buono.importo_taglio > 0 && obj_buono.quantita !== '' && obj_buono.quantita > 0) {
            buoni_memorizzati.push(obj_buono);
            var opzioni_buoni_memorizzati = '';
            buoni_memorizzati.forEach(function (val, index) {
                opzioni_buoni_memorizzati += '<option value="' + index + '">' + val.quantita + 'x' + val.importo_taglio + 'x' + val.nome_circuito + '</option>';
            });
            $('[name="scelta_buoni__buoni_memorizzati"]').html(opzioni_buoni_memorizzati);
            var ammontare = 0;
            //Calcola anche i buoni memorizzati
            if (buoni_memorizzati.length > 0) {
                buoni_memorizzati.forEach(function (val, index) {
                    ammontare += parseInt(val.quantita) * parseFloat(val.importo_taglio);
                });
            }

            $('span.scelta_buoni__ammontare_buono').html(ammontare.toFixed(2));
            var restante = parseFloat($(comanda.totale_scontrino).html()) - parseFloat(ammontare) - parseFloat($('[name="scelta_buoni__buono_spesa"]').val());
            $('span.scelta_buoni__restante').html(restante.toFixed(2));
            $('[name="scelta_buoni__quantita_buoni"]').val('1');
        } else {
            bootbox.alert("Bisogna selezionare il circuito, o il taglio, o la quantita' del buono");
        }
    } else
    {
        bootbox.alert("Il numero massimo di buoni pasto e' di CINQUE unita'.");
    }
}

function elimina_buono_memorizzato() {
    var index = $('[name="scelta_buoni__buoni_memorizzati"').val();
    buoni_memorizzati.splice(index, 1);
    var opzioni_buoni_memorizzati = '';
    buoni_memorizzati.forEach(function (val, index) {
        opzioni_buoni_memorizzati += '<option value="' + index + '">' + val.quantita + 'x' + val.importo_taglio + 'x' + val.nome_circuito + '</option>';
    });
    $('[name="scelta_buoni__buoni_memorizzati"').html(opzioni_buoni_memorizzati);
    if ($('[name="scelta_buoni__importo_taglio"]').val() !== null && $('[name="scelta_buoni__importo_taglio"]').val().length > 0) {

        var ammontare = 0;
        //Calcola anche i buoni memorizzati
        if (buoni_memorizzati.length > 0) {
            buoni_memorizzati.forEach(function (val, index) {
                ammontare += parseInt(val.quantita) * parseFloat(val.importo_taglio);
            });
        }

        $('span.scelta_buoni__ammontare_buono').html(ammontare.toFixed(2));
        var restante = parseFloat($(comanda.totale_scontrino).html()) - parseFloat(ammontare) - parseFloat($('[name="scelta_buoni__buono_spesa"]').val());
        $('span.scelta_buoni__restante').html(restante.toFixed(2));
    }
}


function stampa_fattura_circuito_BACKUP_DA_TENERE() {

    var fattura_elettronica = new Object();

    /* Definizione Struttura Oggetti */
    var testa = new Array();
    var corpo = new Array();
    var totali = new Array();
    var pagamento = new Array();

    var t = new Testa();

    var intestatario = alasql("select * from circuiti where id=" + comanda.id_circuito + " limit 1;");
    intestatario = intestatario[0];

    /* Intermediario Trasmissione Fattura */
    t.C1111; /* Id Paese + */
    t.C1112; /* Id Codice + */
    t.C112 = " "; /* Progressivo Invio + */

    /* FPA12 per pubblica amministrazione */
    if (intestatario.formato_trasmissione === "true") {
        t.C113 = "FPA12";
    } else
    {
        t.C113 = "FPR12"; /* Formato Trasmissione + */
    }

    /* ANAGRAFICA DA COMPILARE */
    if (intestatario.paese !== "IT" && intestatario.partita_iva_estera !== null && intestatario.partita_iva_estera.length > 0) {
        t.C114 = "XXXXXXX";
    } else if (intestatario.codice_destinatario === "") {
        t.C114 = "0000000"; /* Codice Destinatario + */
    } else {
        t.C114 = intestatario.codice_destinatario; /* Codice Destinatario + */
    }

    t.C116 = intestatario.pec; /* PEC Destinatario + */

    /* Ristoratore */
    t.C12111 = comanda.profilo_aziendale.id_nazione; /* Id Paese + */

    if (comanda.profilo_aziendale.partita_iva.length === 11) {
        t.C12112 = comanda.profilo_aziendale.partita_iva; /* Id Codice + */
    } else
    {
        t.C12112 = comanda.profilo_aziendale.codice_fiscale;
    }
    t.C12131 = comanda.profilo_aziendale.ragione_sociale; /* Denominazione + */
    if (intestatario.regime_fiscale !== undefined) {
        t.C1218 = intestatario.regime_fiscale; /* Regime Fiscale + */
    } else
    {
        t.C1218 = "RF01";
    }
    t.C1221 = comanda.profilo_aziendale.indirizzo + ", " + comanda.profilo_aziendale.numero; /* Indirizzo + */
    t.C1223 = comanda.profilo_aziendale.cap; /* CAP + */
    t.C1224 = comanda.profilo_aziendale.comune; /* Comune + */
    t.C1225 = comanda.profilo_aziendale.provincia; /* Provincia + */
    t.C1226 = comanda.profilo_aziendale.nazione; /* Nazione + */

    /* Consumatore Finale (cliente del cliente) */

    if (intestatario.nazione !== "") {
        t.C14111 = intestatario.nazione;
    } else
    {
        t.C14111 = "IT";
    }

    if (intestatario.partita_iva.length === 11 || intestatario.partita_iva.length === 16) {
        t.C14112 = intestatario.partita_iva; /* Id Codice + */
    } else if (intestatario.partita_iva_estera !== null && intestatario.partita_iva_estera.length > 0) {
        t.C14112 = intestatario.partita_iva_estera;
    } else
    {
        t.C14112 = intestatario.codice_fiscale;
    }


    t.C14131 = intestatario.ragione_sociale; /* Denominazione + */
    t.C1421 = intestatario.indirizzo + ", " + intestatario.numero; /* Indirizzo + */
    t.C1423 = intestatario.cap; /* CAP + */
    t.C1424 = intestatario.comune; /* Comune + */
    t.C1425 = intestatario.provincia; /* Provincia + */
    t.C1426 = intestatario.nazione; /* Nazione + */

    /* Per fatture nelle fatture (non utilizzato nella ristorazione) */
    t.C15111 = intestatario.nazione;/* Id Paese */
    t.C15112; /* Id Codice */
    t.C15131; /* Denominazione */

    /* Dati fiscali */
    t.C2111 = "TD01"; /* Tipo Documento + */
    t.C2112 = "EUR"; /* Divisa + */
    var a = new Date();
    t.C2113 = a.format("yyyy-mm-dd"); /* Data + */

    /* NUMERO FISCALE */
    t.C2114 = ""; /* Numero + */

    /* Per ritenute */
    t.C21151; /* Tipo Ritenuta */
    t.C21152; /* Importo Ritenuta */
    t.C21153; /* Aliquota Ritenuta */
    t.C21154; /* Causale Pagamento */

    /* SC SE CE SCONTO */

    t.C21181 = ""; /* Tipo */
    t.C21182; /* Percentuale */

    /* IMPORTO SCONTO SE CE */
    t.C21183 = ""; /* Importo */
    t.C21182 = ""; /* Ripartizione Sconto */


    t.C21111 = "VENDITA FISSA"; /* Causale */
    t.SPED; /*  */
    t.DATAS; /*  */
    t.INDIRIZZO; /*  */

    var container = $('#fattura_circuito');
    var data = comanda.funzionidb.data_attuale();
    var builder = new epson.ePOSBuilder();
    builder.addTextFont(builder.FONT_A);
    builder.addTextAlign(builder.ALIGN_LEFT);
    builder.addTextSize(2, 2);
    builder.addText("DATA: ");
    builder.addText(container.find('.data_fattura_circuito').html() + "\n");
    builder.addText("\n");
    builder.addText("FATTURA NÃ‚Â°");
    builder.addText(container.find('.numero_fattura_circuito').html() + " ");
    builder.addText("\n");
    builder.addFeedLine(1);
    builder.addTextSize(1, 1);
    builder.addTextStyle(false, false, true, builder.COLOR_1);
    builder.addText("EMITTENTE\n\n");
    builder.addTextStyle(false, false, false, builder.COLOR_1);
    builder.addText(container.find('.emit_ragione_sociale').html() + "\n\n");
    builder.addText(container.find('.emit_indirizzo').html() + "\n");
    //fattura += container.find('.emit_contatti').html() + "\n";
    builder.addText(container.find('.emit_info_fiscali').html() + "\n");
    builder.addText("\n");
    builder.addTextStyle(false, false, true, builder.COLOR_1);
    builder.addText("INTESTATARIO \n\n");
    builder.addTextStyle(false, false, false, builder.COLOR_1);
    builder.addText(container.find('.intest_ragione_sociale').html() + "\n\n");
    builder.addText(container.find('.intest_indirizzo').html() + "\n");
    //fattura += container.find('.intest_contatti').html() + "\n";
    builder.addText(container.find('.intest_info_fiscali').html() + "\n");
    builder.addText("\n");
    builder.addTextStyle(false, false, true, builder.COLOR_1);
    builder.addText("DATI SUDDIVISI PER TAGLIO\n\n");
    builder.addTextStyle(false, false, false, builder.COLOR_1);
    builder.addText("QuantitÃƒÂ \tUnitario\tTotale\n");

    var contatore_linea = 0;

    var aliquota_temp = ive_misuratore_controller.iva_reparto(1);
    var quoziente_scorporo = parseInt(ive_misuratore_controller.iva_reparto(1)) / 100 + 1;

    var imponibile = new Object();
    imponibile[ive_misuratore_controller.iva_reparto(1)] = 0.00;
    imponibile[ive_misuratore_controller.iva_reparto(2)] = 0.00;
    imponibile[ive_misuratore_controller.iva_reparto(3)] = 0.00;
    var imposta = new Object();
    imposta[ive_misuratore_controller.iva_reparto(1)] = 0.00;
    imposta[ive_misuratore_controller.iva_reparto(2)] = 0.00;
    imposta[ive_misuratore_controller.iva_reparto(3)] = 0.00;

    container.find('.riga').each(function (a, b) {

        var c = new Corpo();

        var quantita = $(b).find('.quantita').html();
        var prezzo_un = $(b).find('.valore').html();


        switch (container.find('.iva').html()) {
            case ive_misuratore_controller.iva_reparto(1):
                quoziente_scorporo = ive_misuratore_controller.iva_reparto(1) / 100 + 1;
                imponibile[ive_misuratore_controller.iva_reparto(1)] += (100 * (parseFloat(prezzo_un) * quantita)) / (100 + parseInt(ive_misuratore_controller.iva_reparto(1)));
                imposta[ive_misuratore_controller.iva_reparto(1)] += (parseFloat(prezzo_un) * quantita) - ((100 * (parseFloat(prezzo_un) * quantita)) / (100 + parseInt(ive_misuratore_controller.iva_reparto(1))));
                aliquota_temp = ive_misuratore_controller.iva_reparto(1);
                break;
            case ive_misuratore_controller.iva_reparto(2):
                quoziente_scorporo = ive_misuratore_controller.iva_reparto(2) / 100 + 1;
                imponibile[ive_misuratore_controller.iva_reparto(2)] += (100 * (parseFloat(prezzo_un) * quantita)) / (100 + parseInt(ive_misuratore_controller.iva_reparto(2)));
                imposta[ive_misuratore_controller.iva_reparto(2)] += (parseFloat(prezzo_un) * quantita) - ((100 * (parseFloat(prezzo_un) * quantita)) / (100 + parseInt(ive_misuratore_controller.iva_reparto(2))));
                aliquota_temp = ive_misuratore_controller.iva_reparto(2);
                break;
            case ive_misuratore_controller.iva_reparto(3):
                quoziente_scorporo = ive_misuratore_controller.iva_reparto(3) / 100 + 1;
                imponibile[ive_misuratore_controller.iva_reparto(3)] += (100 * (parseFloat(prezzo_un) * quantita)) / (100 + parseInt(ive_misuratore_controller.iva_reparto(3)));
                imposta[ive_misuratore_controller.iva_reparto(3)] += (parseFloat(prezzo_un) * quantita) - ((100 * (parseFloat(prezzo_un) * quantita)) / (100 + parseInt(ive_misuratore_controller.iva_reparto(3))));
                aliquota_temp = ive_misuratore_controller.iva_reparto(3);
                break;
        }


        c.C2211 = contatore_linea.toString(); /* Numero Linea + */
        c.C2212; /* Tipo Cessione Prestazione */
        c.C22131 = "Art. Cliente"; /* Codice Tipo + */
        /* CODICE ARTICOLO */
        c.C22132 = ""; /* Codice Valore + */
        c.C2214 = quantita + " x " + prezzo_un; /* Descrizione + */
        c.C2215 = "1"; /* Quantità + */
        c.C2216; /* Unità Misura + */
        c.C2219 = (parseFloat(prezzo_un) / quoziente_scorporo).toFixed(2); /* Prezzo Unitario + */
        c.C221101; /* Tipo (Sconto-Magg-iorazione) */
        c.C221103; /* Importo Sconto */
        c.C22111 = (parseFloat(prezzo_un) / quoziente_scorporo * quantita).toFixed(2); /* Prezzo Totale + */
        c.C22112 = aliquota_temp; /* Aliquota IVA + */

        corpo.push(c);

        contatore_linea++;

        builder.addText($(b).find('.quantita').html() + "\t\t" + $(b).find('.valore').html() + "\t\t" + $(b).find('.totale').html() + "\t\n");
    });
    builder.addText("\n");
    builder.addTextStyle(false, false, true, builder.COLOR_1);
    builder.addText("DATI FISCALI FATTURA\n\n");
    builder.addTextStyle(false, false, false, builder.COLOR_1);
    builder.addText("Imponibile\tIVA\tImposta\tTotale\n");
    builder.addText(container.find('.imponibile').html() + "\t\t");
    builder.addText(container.find('.iva').html() + "\t");
    builder.addText(container.find('.imposta').html() + "\t");
    builder.addText(container.find('.totale').html() + "\t");

    t.C2119 = container.find('.totale').html(); /* Importo Totale Documento + */
    testa.push(t);

    /* Tabella Totali OK */
    if (imponibile[ive_misuratore_controller.iva_reparto(1)] > 0) {
        var o = new Totali();
        o.C2221 = ive_misuratore_controller.iva_reparto(1); /* Aliquota IVA + */
        o.C2222; /* Natura */
        o.C2225 = parseFloat(imponibile[ive_misuratore_controller.iva_reparto(1)]).toFixed(2); /* Imponibile Importo + */
        o.C2226 = parseFloat(imposta[ive_misuratore_controller.iva_reparto(1)]).toFixed(2); /* Imposta + */
        o.C2227 = "I"; /* Esigibilità IVA */
        totali.push(o);
    }

    if (imponibile[ive_misuratore_controller.iva_reparto(2)] > 0) {
        var o = new Totali();
        o.C2221 = ive_misuratore_controller.iva_reparto(2); /* Aliquota IVA + */
        o.C2222; /* Natura */
        o.C2225 = parseFloat(imponibile[ive_misuratore_controller.iva_reparto(2)]).toFixed(2); /* Imponibile Importo + */
        o.C2226 = parseFloat(imposta[ive_misuratore_controller.iva_reparto(2)]).toFixed(2); /* Imposta + */
        o.C2227 = "I"; /* Esigibilità IVA */
        totali.push(o);
    }

    if (imponibile[ive_misuratore_controller.iva_reparto(3)] > 0) {
        var o = new Totali();
        o.C2221 = ive_misuratore_controller.iva_reparto(3); /* Aliquota IVA + */
        o.C2222; /* Natura */
        o.C2225 = parseFloat(imponibile[ive_misuratore_controller.iva_reparto(3)]).toFixed(2); /* Imponibile Importo + */
        o.C2226 = parseFloat(imposta[ive_misuratore_controller.iva_reparto(3)]).toFixed(2); /* Imposta + */
        o.C2227 = "I"; /* Esigibilità IVA */
        totali.push(o);
    }

    var p = new Pagamento();
    p.C241 = "TP02"; /* Condizioni Pagamento */
    p.C2422 = "MP01"; /* Modalità Pagamento */
    p.C2426 = container.find('.totale').html(); /* Importo Pagamento */
    pagamento.push(p);

    builder.addText("\n");
    builder.addFeedLine(1);
    builder.addCut(builder.CUT_FEED);


    var request = builder.toString();
    var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
    var xhr = new XMLHttpRequest();
    var url = 'http://' + comanda.intelligent_non_fiscale + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
    xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
    xhr.setRequestHeader('SOAPAction', '""');
    xhr.send(soap);

    fattura_elettronica["testa"] = testa;
    fattura_elettronica["corpo"] = corpo;
    fattura_elettronica["totali"] = totali;
    fattura_elettronica["pagamento"] = pagamento;

    invia_fattura_elettronica(testa, corpo, totali, pagamento);



}

function estratto_conto_buoni() {

    var numero_mese = 0;

    var numero_mese = new Date().getMonth() + 1;
    var numero_anno = new Date().getFullYear();

    let mese = "";
    switch (numero_mese) {
        case 1:
            mese = "Gennaio";
            break;
        case 2:
            mese = "Febbraio";
            break;
        case 3:
            mese = "Marzo";
            break;
        case 4:
            mese = "Aprile";
            break;
        case 5:
            mese = "Maggio";
            break;
        case 6:
            mese = "Giugno";
            break;
        case 7:
            mese = "Luglio";
            break;
        case 8:
            mese = "Agosto";
            break;
        case 9:
            mese = "Settembre";
            break;
        case 10:
            mese = "Ottobre";
            break;
        case 11:
            mese = "Novembre";
            break;
        case 12:
            mese = "Dicembre";
            break;
    }
    numero_mese = aggZero(numero_mese, 2);



    var intestatario = alasql("select * from circuiti where id=" + comanda.id_circuito + " limit 1;");
    intestatario = intestatario[0];

    if (intestatario.riepilogo_scontrini === "true") {

        for (var key in intestatario[0]) {
            if (intestatario[0][key] === null) {
                intestatario[0][key] = '';
            }
        }


        var data = comanda.funzionidb.data_attuale();
        var builder = new epson.ePOSBuilder();
        builder.addTextFont(builder.FONT_A);
        builder.addTextAlign(builder.ALIGN_CENTER);
        builder.addTextStyle(false, false, false, builder.COLOR_1);
        builder.addTextSize(1, 1);
        builder.addText("-----------------------------------------\n");

        builder.addFeedLine(1);
        builder.addTextSize(2, 2);
        builder.addText('ESTRATTO CONTO\n');
        builder.addText('' + mese + ' ' + numero_anno + '\n');
        builder.addTextSize(1, 1);
        builder.addFeedLine(1);
        builder.addText("-----------------------------------------\n");
        builder.addFeedLine(1);
        builder.addTextAlign(builder.ALIGN_LEFT);
        builder.addTextSize(1, 1);
        builder.addTextStyle(false, false, false, builder.COLOR_1);
        builder.addText('EMITTENTE: \n');
        builder.addTextStyle(false, false, true, builder.COLOR_1);
        builder.addText(comanda.profilo_aziendale.ragione_sociale + '\n');
        builder.addTextStyle(false, false, false, builder.COLOR_1);
        builder.addText(comanda.profilo_aziendale.indirizzo + ',' + comanda.profilo_aziendale.numero + '\n');
        builder.addText(comanda.profilo_aziendale.cap + ' - ' + comanda.profilo_aziendale.citta + ' (' + comanda.profilo_aziendale.provincia + ') ' + comanda.profilo_aziendale.paese + ' \n');
        builder.addText('Tel : ' + comanda.profilo_aziendale.telefono + '\n');
        if (comanda.profilo_aziendale.email.length >= 5) {
            builder.addText('Email : ' + comanda.profilo_aziendale.email + '\n');
        }
        builder.addText('P.I. : ' + comanda.profilo_aziendale.partita_iva + '\n');
        if (comanda.profilo_aziendale.codice_fiscale.length >= 5) {
            builder.addText('C.F. : ' + comanda.profilo_aziendale.codice_fiscale + '\n');
        }

        builder.addFeedLine(2);
        builder.addTextAlign(builder.ALIGN_RIGHT);
        builder.addTextSize(1, 1);
        builder.addTextStyle(false, false, false, builder.COLOR_1);
        builder.addText('CESSIONARIO:\n');
        builder.addTextStyle(false, false, true, builder.COLOR_1);
        builder.addText(intestatario.ragione_sociale + '\n');
        builder.addTextStyle(false, false, false, builder.COLOR_1);
        builder.addText(intestatario.indirizzo + ',' + intestatario.numero + '\n');
        builder.addText(intestatario.cap + ' - ' + intestatario.citta + ' (' + intestatario.provincia + ') ' + intestatario.paese + ' \n');
        if (intestatario.telefono.trim().length > 0) {
            builder.addText('Tel : ' + intestatario.telefono + '\n');
        }
        if (intestatario.email.trim().length > 0) {
            builder.addText(intestatario.email + '\n');
        }
        builder.addText('P.I. : ' + intestatario.partita_iva + '\n');
        if (intestatario.codice_fiscale.length >= 5) {
            builder.addText('C.F. : ' + intestatario.codice_fiscale + '\n');
        }

        builder.addFeedLine(1);
        builder.addTextAlign(builder.ALIGN_CENTER);
        builder.addText("-----------------------------------------\n");
        builder.addFeedLine(1);
        builder.addTextAlign(builder.ALIGN_CENTER);
        builder.addTextStyle(false, false, true, builder.COLOR_1);

        builder.addText('DISTINTA SCONTRINI\n');
        builder.addFeedLine(1);
        builder.addTextAlign(builder.ALIGN_LEFT);
        builder.addTextStyle(false, false, true, builder.COLOR_1);
        builder.addTextPosition(0);
        builder.addText('DATA');
        builder.addTextPosition(130);
        builder.addText('ORA');
        builder.addTextPosition(220);
        builder.addText('N.');
        builder.addTextPosition(290);
        builder.addText('A.IVA');
        builder.addTextPosition(380);
        builder.addText('TOTALE\n');
        builder.addTextStyle(false, false, false, builder.COLOR_1);
        builder.addFeedLine(1);
        var totale_generale = 0;
        var distinta_iva = new Object();
        distinta_iva[ive_misuratore_controller.iva_reparto(3)] = new Object();
        distinta_iva[ive_misuratore_controller.iva_reparto(3)].netto = 0;
        distinta_iva[ive_misuratore_controller.iva_reparto(3)].imposta = 0;
        distinta_iva[ive_misuratore_controller.iva_reparto(3)].imponibile = 0;
        distinta_iva[ive_misuratore_controller.iva_reparto(2)] = new Object();
        distinta_iva[ive_misuratore_controller.iva_reparto(2)].netto = 0;
        distinta_iva[ive_misuratore_controller.iva_reparto(2)].imposta = 0;
        distinta_iva[ive_misuratore_controller.iva_reparto(2)].imponibile = 0;
        distinta_iva[ive_misuratore_controller.iva_reparto(1)] = new Object();
        distinta_iva[ive_misuratore_controller.iva_reparto(1)].netto = 0;
        distinta_iva[ive_misuratore_controller.iva_reparto(1)].imposta = 0;
        distinta_iva[ive_misuratore_controller.iva_reparto(1)].imponibile = 0;


        $('.tabella_movimenti_mese_corrente tr').not(':nth-child(1)').each(function (a, b) {

            let data = $(b).find('td:nth-child(1)').html();
            let ora = $(b).find('td:nth-child(2)').html();
            let tipo = $(b).find('td:nth-child(3)').html();
            let nF = $(b).find('td:nth-child(4)').html();
            let nBuoni = $(b).find('td:nth-child(5)').html();
            let taglio = $(b).find('td:nth-child(6)').html();
            let tBuoni = $(b).find('td:nth-child(7)').html();
            let locale = $(b).find('td:nth-child(8)').html();
            let tavolo = $(b).find('td:nth-child(9)').html();
            let operatore = $(b).find('td:nth-child(10)').html();
            let totale = $(b).find('td:nth-child(11)').html();

            builder.addTextPosition(0);
            builder.addText(data.replace(/-/gi, '/'));
            builder.addTextPosition(130);
            builder.addText(ora.substr(0, 5));
            builder.addTextPosition(220);
            builder.addText(nF);
            builder.addTextPosition(290);
            builder.addText(intestatario.iva_non_pagati + '%');
            builder.addTextPosition(380);
            builder.addText('€ ' + (parseFloat(nBuoni) * parseFloat(taglio)).toFixed(2));
            builder.addText('\n');
            totale_generale += parseFloat(nBuoni) * parseFloat(taglio);
            if (intestatario.iva_non_pagati === "") {
                intestatario.iva_non_pagati = "10";
            }
            distinta_iva[intestatario.iva_non_pagati].netto += scorpora(parseFloat(nBuoni) * parseFloat(taglio), parseFloat(intestatario.iva_non_pagati))[0];
            distinta_iva[intestatario.iva_non_pagati].imposta += scorpora(parseFloat(nBuoni) * parseFloat(taglio), parseFloat(intestatario.iva_non_pagati))[1];
            distinta_iva[intestatario.iva_non_pagati].imponibile += parseFloat(nBuoni) * parseFloat(taglio)
        });

        builder.addFeedLine(1);
        builder.addTextStyle(false, false, true, builder.COLOR_1);
        builder.addText('TOTALE SCONTRINI: ');
        builder.addTextPosition(380);
        builder.addText('€ ' + parseFloat(totale_generale).toFixed(2));
        builder.addTextStyle(false, false, false, builder.COLOR_1);
        builder.addFeedLine(1);
        builder.addTextAlign(builder.ALIGN_CENTER);
        builder.addText("-----------------------------------------\n");
        builder.addTextAlign(builder.ALIGN_LEFT);
        builder.addFeedLine(2);
        builder.addTextStyle(false, false, true, builder.COLOR_1);
        builder.addTextAlign(builder.ALIGN_CENTER);
        builder.addText('DISTINTA IVA\n');
        builder.addTextStyle(false, false, true, builder.COLOR_1);
        builder.addTextAlign(builder.ALIGN_LEFT);
        builder.addFeedLine(2);
        builder.addTextStyle(false, false, true, builder.COLOR_1);
        builder.addText('IMPONIBILE');
        builder.addTextPosition(130);
        builder.addText('PERC.');
        builder.addTextPosition(220);
        builder.addText('IMPOSTA');
        builder.addTextPosition(380);
        builder.addText('TOTALE\n');
        builder.addTextStyle(false, false, false, builder.COLOR_1);
        builder.addFeedLine(1);
        var totale_fattura = 0;


        for (var key in distinta_iva) {

            var obj = distinta_iva[key];

            if (parseFloat(obj.imponibile) > 0) {
                builder.addTextPosition(0);
                builder.addText('€ ' + (parseFloat(obj.imponibile) - scorpora(parseFloat(obj.imponibile), key)[1]).toFixed(2));
                builder.addTextPosition(130);
                builder.addText(key + ' %');
                builder.addTextPosition(220);
                builder.addText('€ ' + scorpora(parseFloat(obj.imponibile), key)[1].toFixed(2));
                builder.addTextPosition(380);
                builder.addText('€ ' + parseFloat(obj.imponibile).toFixed(2));
                totale_fattura += parseFloat(obj.imponibile);
                builder.addText('\n');
            }
        }

        builder.addFeedLine(1);
        builder.addTextStyle(false, false, true, builder.COLOR_1);

        builder.addText('TOTALE:');
        builder.addTextPosition(380);
        builder.addText('€ ' + parseFloat(totale_fattura).toFixed(2) + '\n');
        builder.addTextStyle(false, false, false, builder.COLOR_1);
        builder.addFeedLine(1);
        builder.addTextAlign(builder.ALIGN_CENTER);
        builder.addText("-----------------------------------------\n");
        builder.addCut(builder.CUT_FEED);
        var request = builder.toString();
        var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
        var url = 'http://' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';

        var xhr = new XMLHttpRequest();

        xhr.open('POST', url, true);

        xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
        xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
        xhr.setRequestHeader('SOAPAction', '""');

        xhr.send(soap);


    }

}

function stampa_fattura_circuito() {

    var p1 = new Promise(function (resolve, reject) {
        if (comanda.progressivo_unico_fatture === true) {
            comanda.funzionidb.richiesta_progressivo_ita("FATTURA", function (numero_progressivo_scontrino) {
                resolve(numero_progressivo_scontrino);
            });
        } else
        {
            resolve("0");
        }
    });
    p1.then(function (numero_fattura) {

        var fattura_elettronica = new Object();

        /* Definizione Struttura Oggetti */
        var testa = new Array();
        var corpo = new Array();
        var totali = new Array();
        var pagamento = new Array();

        var t = new Testa();

        var intestatario = alasql("select * from circuiti where id=" + comanda.id_circuito + " limit 1;");
        intestatario = intestatario[0];

        /* Intermediario Trasmissione Fattura */
        t.C1111; /* Id Paese + */
        t.C1112; /* Id Codice + */
        t.C112 = " "; /* Progressivo Invio + */

        /* FPA12 per pubblica amministrazione */
        if (intestatario.formato_trasmissione === "true") {
            t.C113 = "FPA12";
        } else
        {
            t.C113 = "FPR12"; /* Formato Trasmissione + */
        }

        /* ANAGRAFICA DA COMPILARE */
        if (intestatario.paese !== "IT" && intestatario.partita_iva_estera !== null && intestatario.partita_iva_estera.length > 0) {
            t.C114 = "XXXXXXX";
        } else if (intestatario.codice_destinatario === "") {
            t.C114 = "0000000"; /* Codice Destinatario + */
        } else {
            t.C114 = intestatario.codice_destinatario; /* Codice Destinatario + */
        }

        t.C116 = intestatario.pec; /* PEC Destinatario + */

        /* Ristoratore */
        t.C12111 = comanda.profilo_aziendale.id_nazione; /* Id Paese + */

        if (comanda.profilo_aziendale.partita_iva.length === 11) {
            t.C12112 = comanda.profilo_aziendale.partita_iva; /* Id Codice + */
        } else
        {
            t.C12112 = comanda.profilo_aziendale.codice_fiscale;
        }
        t.C12131 = comanda.profilo_aziendale.ragione_sociale; /* Denominazione + */
        if (intestatario.regime_fiscale !== undefined) {
            t.C1218 = intestatario.regime_fiscale; /* Regime Fiscale + */
        } else
        {
            t.C1218 = "RF01";
        }
        t.C1221 = comanda.profilo_aziendale.indirizzo + ", " + comanda.profilo_aziendale.numero; /* Indirizzo + */
        t.C1223 = comanda.profilo_aziendale.cap; /* CAP + */
        t.C1224 = comanda.profilo_aziendale.comune; /* Comune + */
        t.C1225 = comanda.profilo_aziendale.provincia; /* Provincia + */
        t.C1226 = comanda.profilo_aziendale.nazione; /* Nazione + */

        /* Consumatore Finale (cliente del cliente) */

        if (intestatario.nazione !== "") {
            t.C14111 = intestatario.nazione;
        } else
        {
            t.C14111 = "IT";
        }

        if (intestatario.partita_iva.length === 11 || intestatario.partita_iva.length === 16) {
            t.C14112 = intestatario.partita_iva; /* Id Codice + */
        } else if (intestatario.partita_iva_estera !== null && intestatario.partita_iva_estera.length > 0) {
            t.C14112 = intestatario.partita_iva_estera;
        } else
        {
            t.C14112 = intestatario.codice_fiscale;
        }


        t.C14131 = intestatario.ragione_sociale; /* Denominazione + */
        t.C1421 = intestatario.indirizzo + ", " + intestatario.numero; /* Indirizzo + */
        t.C1423 = intestatario.cap; /* CAP + */
        t.C1424 = intestatario.comune; /* Comune + */
        t.C1425 = intestatario.provincia; /* Provincia + */
        t.C1426 = intestatario.nazione; /* Nazione + */

        /* Per fatture nelle fatture (non utilizzato nella ristorazione) */
        t.C15111 = intestatario.nazione;/* Id Paese */
        t.C15112; /* Id Codice */
        t.C15131; /* Denominazione */

        /* Dati fiscali */
        t.C2111 = "TD24"; /* Tipo Documento + */
        t.C2112 = "EUR"; /* Divisa + */
        var a = new Date();
        t.C2113 = a.format("yyyy-mm-dd"); /* Data + */

        /* NUMERO FISCALE */
        t.C2114 = ""; /* Numero + */

        /* Per ritenute */
        t.C21151; /* Tipo Ritenuta */
        t.C21152; /* Importo Ritenuta */
        t.C21153; /* Aliquota Ritenuta */
        t.C21154; /* Causale Pagamento */

        /* SC SE CE SCONTO */

        t.C21181 = ""; /* Tipo */
        t.C21182; /* Percentuale */

        /* IMPORTO SCONTO SE CE */
        t.C21183 = ""; /* Importo */
        t.C21182 = ""; /* Ripartizione Sconto */


        t.C21111 = "VENDITA FISSA"; /* Causale */
        t.SPED; /*  */
        t.DATAS; /*  */
        t.INDIRIZZO; /*  */

        var container = $('#fattura_circuito');
        var data = comanda.funzionidb.data_attuale();
        var builder = new epson.ePOSBuilder();

        var xml = '<printerFiscalDocument>';


        /*builder.addTextFont(builder.FONT_A);
         builder.addTextAlign(builder.ALIGN_LEFT);
         builder.addTextSize(2, 2);
         builder.addText("DATA: ");
         builder.addText(container.find('.data_fattura_circuito').html() + "\n");
         builder.addText("\n");
         builder.addText("FATTURA NÃ‚Â°");
         builder.addText(container.find('.numero_fattura_circuito').html() + " ");
         builder.addText("\n");
         builder.addFeedLine(1);
         builder.addTextSize(1, 1);
         builder.addTextStyle(false, false, true, builder.COLOR_1);
         builder.addText("EMITTENTE\n\n");
         builder.addTextStyle(false, false, false, builder.COLOR_1);*/
        /*builder.addText(container.find('.emit_ragione_sociale').html() + "\n\n");
         builder.addText(container.find('.emit_indirizzo').html() + "\n");
         //fattura += container.find('.emit_contatti').html() + "\n";
         builder.addText(container.find('.emit_info_fiscali').html() + "\n");
         builder.addText("\n");
         builder.addTextStyle(false, false, true, builder.COLOR_1);*/

        xml += '<printRecMessage operator="1" index="1" messageType="6" message="' + intestatario.ragione_sociale.substr(0, 50) + '" />';
        xml += '<printRecMessage operator="1" index="2" messageType="6" message="' + intestatario.indirizzo + ',' + intestatario.numero + '" />';
        xml += '<printRecMessage operator="1" index="3" messageType="6" message="' + intestatario.cap + ' - ' + intestatario.comune + ' (' + intestatario.provincia + ') ' + intestatario.nazione + '" />';
        xml += '<printRecMessage operator="1" index="4" messageType="6" message="' + 'P.I. : ' + intestatario.partita_iva + '" />';
        xml += '<printRecMessage operator="1" index="5" messageType="6" message="------------ DOCUMENTO NON FISCALE ------------" />';



        /*builder.addText("INTESTATARIO \n\n");
         builder.addTextStyle(false, false, false, builder.COLOR_1);
         builder.addText(container.find('.intest_ragione_sociale').html() + "\n\n");
         builder.addText(container.find('.intest_indirizzo').html() + "\n");
         //fattura += container.find('.intest_contatti').html() + "\n";
         builder.addText(container.find('.intest_info_fiscali').html() + "\n");
         builder.addText("\n");
         builder.addTextStyle(false, false, true, builder.COLOR_1);*/
        /*builder.addText("DATI SUDDIVISI PER TAGLIO\n\n");
         builder.addTextStyle(false, false, false, builder.COLOR_1);
         builder.addText("QuantitÃƒÂ \tUnitario\tTotale\n");*/

        var contatore_linea = 0;

        var aliquota_temp = ive_misuratore_controller.iva_reparto(1);
        var quoziente_scorporo = parseInt(ive_misuratore_controller.iva_reparto(1)) / 100 + 1;

        var imponibile = new Object();
        imponibile[ive_misuratore_controller.iva_reparto(1)] = 0.00;
        imponibile[ive_misuratore_controller.iva_reparto(2)] = 0.00;
        imponibile[ive_misuratore_controller.iva_reparto(3)] = 0.00;
        var imposta = new Object();
        imposta[ive_misuratore_controller.iva_reparto(1)] = 0.00;
        imposta[ive_misuratore_controller.iva_reparto(2)] = 0.00;
        imposta[ive_misuratore_controller.iva_reparto(3)] = 0.00;



        if (intestatario.riepilogo_scontrini === "true") {


            let totale_fattura_emessa = 0;

            let xml_temp = "";

            $('.tabella_movimenti_mese_corrente tr').not(':nth-child(1)').each(function (a, b) {

                var c = new Corpo();

                let data = $(b).find('td:nth-child(1)').html();
                let ora = $(b).find('td:nth-child(2)').html();
                let tipo = $(b).find('td:nth-child(3)').html();
                let nF = $(b).find('td:nth-child(4)').html();
                let nBuoni = $(b).find('td:nth-child(5)').html();
                let taglio = $(b).find('td:nth-child(6)').html();
                let tBuoni = $(b).find('td:nth-child(7)').html();
                let locale = $(b).find('td:nth-child(8)').html();
                let tavolo = $(b).find('td:nth-child(9)').html();
                let operatore = $(b).find('td:nth-child(10)').html();
                let totale = $(b).find('td:nth-child(11)').html();

                let quantita = 1;
                let prezzo_un = parseFloat(nBuoni) * parseFloat(taglio);
                totale_fattura_emessa += parseFloat(nBuoni) * parseFloat(taglio);

                var reparto = "1";


                switch (parseFloat(container.find('.iva').html().replace("%", ""))) {
                    case parseFloat(ive_misuratore_controller.iva_reparto(1)):
                        quoziente_scorporo = ive_misuratore_controller.iva_reparto(1) / 100 + 1;
                        imponibile[ive_misuratore_controller.iva_reparto(1)] += (100 * (parseFloat(prezzo_un) * quantita)) / (100 + parseInt(ive_misuratore_controller.iva_reparto(1)));
                        imposta[ive_misuratore_controller.iva_reparto(1)] += (parseFloat(prezzo_un) * quantita) - ((100 * (parseFloat(prezzo_un) * quantita)) / (100 + parseInt(ive_misuratore_controller.iva_reparto(1))));
                        aliquota_temp = ive_misuratore_controller.iva_reparto(1);
                        reparto = "1";
                        break;
                    case parseFloat(ive_misuratore_controller.iva_reparto(2)):
                        quoziente_scorporo = ive_misuratore_controller.iva_reparto(2) / 100 + 1;
                        imponibile[ive_misuratore_controller.iva_reparto(2)] += (100 * (parseFloat(prezzo_un) * quantita)) / (100 + parseInt(ive_misuratore_controller.iva_reparto(2)));
                        imposta[ive_misuratore_controller.iva_reparto(2)] += (parseFloat(prezzo_un) * quantita) - ((100 * (parseFloat(prezzo_un) * quantita)) / (100 + parseInt(ive_misuratore_controller.iva_reparto(2))));
                        aliquota_temp = ive_misuratore_controller.iva_reparto(2);

                        reparto = "2";
                        break;
                    case parseFloat(ive_misuratore_controller.iva_reparto(3)):
                        quoziente_scorporo = ive_misuratore_controller.iva_reparto(3) / 100 + 1;
                        imponibile[ive_misuratore_controller.iva_reparto(3)] += (100 * (parseFloat(prezzo_un) * quantita)) / (100 + parseInt(ive_misuratore_controller.iva_reparto(3)));
                        imposta[ive_misuratore_controller.iva_reparto(3)] += (parseFloat(prezzo_un) * quantita) - ((100 * (parseFloat(prezzo_un) * quantita)) / (100 + parseInt(ive_misuratore_controller.iva_reparto(3))));
                        aliquota_temp = ive_misuratore_controller.iva_reparto(3);

                        reparto = "3";
                        break;
                }


                c.C2211 = contatore_linea.toString(); /* Numero Linea + */
                c.C2212; /* Tipo Cessione Prestazione */
                c.C22131 = "Art. Cliente"; /* Codice Tipo + */
                /* CODICE ARTICOLO */
                c.C22132 = ""; /* Codice Valore + */
                c.C2214 = quantita + " x " + prezzo_un; /* Descrizione + */
                c.C2215 = "1"; /* Quantità + */
                c.C2216; /* Unità Misura + */
                c.C2219 = (parseFloat(prezzo_un) / quoziente_scorporo).toFixed(2); /* Prezzo Unitario + */
                c.C221101; /* Tipo (Sconto-Magg-iorazione) */
                c.C221103; /* Importo Sconto */
                c.C22111 = (parseFloat(prezzo_un) / quoziente_scorporo * quantita).toFixed(2); /* Prezzo Totale + */
                c.C22112 = aliquota_temp; /* Aliquota IVA + */

                corpo.push(c);

                contatore_linea++;

                xml_temp += '<printRecItem  operator="1" description="' + tipo + ' n.' + nF + ' del ' + data + '"  quantity="' + quantita + '" unitPrice="' + parseFloat(prezzo_un).toFixed(2) + '" department="' + reparto + '" justification="1" />';


                /*builder.addText($(b).find('.quantita').html() + "\t\t" + $(b).find('.valore').html() + "\t\t" + $(b).find('.totale').html() + "\t\n");*/
            });


            var totale_generale = totale_fattura_emessa.toFixed(2);
            xml += '<beginFiscalDocument  operator="1" documentType="directInvoice" documentAmount="' + totale_generale + '" documentNumber="' + numero_fattura + '" /><openDrawer operator="1" />';

            xml += xml_temp;

            xml += '<printRecTotal  operator="1" description="CONTANTI" payment="' + totale_generale + '" paymentType="0" index="1" justification="1" />';




        } else {

            xml += '<beginFiscalDocument  operator="1" documentType="directInvoice" documentAmount="' + container.find('.totale').html() + '" documentNumber="' + numero_fattura + '" />';
            xml += '<openDrawer operator="1" />';

            container.find('.riga').each(function (a, b) {

                var c = new Corpo();

                var quantita = $(b).find('.quantita').html();
                var prezzo_un = $(b).find('.valore').html();

                var reparto = "1";


                switch (parseFloat(container.find('.iva').html().replace("%", ""))) {
                    case parseFloat(ive_misuratore_controller.iva_reparto(1)):
                        quoziente_scorporo = ive_misuratore_controller.iva_reparto(1) / 100 + 1;
                        imponibile[ive_misuratore_controller.iva_reparto(1)] += (100 * (parseFloat(prezzo_un) * quantita)) / (100 + parseInt(ive_misuratore_controller.iva_reparto(1)));
                        imposta[ive_misuratore_controller.iva_reparto(1)] += (parseFloat(prezzo_un) * quantita) - ((100 * (parseFloat(prezzo_un) * quantita)) / (100 + parseInt(ive_misuratore_controller.iva_reparto(1))));
                        aliquota_temp = ive_misuratore_controller.iva_reparto(1);
                        reparto = "1";
                        break;
                    case parseFloat(ive_misuratore_controller.iva_reparto(2)):
                        quoziente_scorporo = ive_misuratore_controller.iva_reparto(2) / 100 + 1;
                        imponibile[ive_misuratore_controller.iva_reparto(2)] += (100 * (parseFloat(prezzo_un) * quantita)) / (100 + parseInt(ive_misuratore_controller.iva_reparto(2)));
                        imposta[ive_misuratore_controller.iva_reparto(2)] += (parseFloat(prezzo_un) * quantita) - ((100 * (parseFloat(prezzo_un) * quantita)) / (100 + parseInt(ive_misuratore_controller.iva_reparto(2))));
                        aliquota_temp = ive_misuratore_controller.iva_reparto(2);

                        reparto = "2";
                        break;
                    case parseFloat(ive_misuratore_controller.iva_reparto(3)):
                        quoziente_scorporo = ive_misuratore_controller.iva_reparto(3) / 100 + 1;
                        imponibile[ive_misuratore_controller.iva_reparto(3)] += (100 * (parseFloat(prezzo_un) * quantita)) / (100 + parseInt(ive_misuratore_controller.iva_reparto(3)));
                        imposta[ive_misuratore_controller.iva_reparto(3)] += (parseFloat(prezzo_un) * quantita) - ((100 * (parseFloat(prezzo_un) * quantita)) / (100 + parseInt(ive_misuratore_controller.iva_reparto(3))));
                        aliquota_temp = ive_misuratore_controller.iva_reparto(3);

                        reparto = "3";
                        break;
                }


                c.C2211 = contatore_linea.toString(); /* Numero Linea + */
                c.C2212; /* Tipo Cessione Prestazione */
                c.C22131 = "Art. Cliente"; /* Codice Tipo + */
                /* CODICE ARTICOLO */
                c.C22132 = ""; /* Codice Valore + */
                c.C2214 = quantita + " x " + prezzo_un; /* Descrizione + */
                c.C2215 = "1"; /* Quantità + */
                c.C2216; /* Unità Misura + */
                c.C2219 = (parseFloat(prezzo_un) / quoziente_scorporo).toFixed(2); /* Prezzo Unitario + */
                c.C221101; /* Tipo (Sconto-Magg-iorazione) */
                c.C221103; /* Importo Sconto */
                c.C22111 = (parseFloat(prezzo_un) / quoziente_scorporo * quantita).toFixed(2); /* Prezzo Totale + */
                c.C22112 = aliquota_temp; /* Aliquota IVA + */

                corpo.push(c);

                contatore_linea++;

                xml += '<printRecItem  operator="1" description="buono/i da: ' + $(b).find('.valore').html() + ' euro" quantity="' + quantita + '" unitPrice="' + parseFloat(prezzo_un).toFixed(2) + '" department="' + reparto + '" justification="1" />';


                /*builder.addText($(b).find('.quantita').html() + "\t\t" + $(b).find('.valore').html() + "\t\t" + $(b).find('.totale').html() + "\t\n");*/
            });


            container.find('.riga_adeguamento').each(function (a, b) {

                var c = new Corpo();

                var quantita = "1";
                var prezzo_un = $(b).find('.importo_adeguamento').html();

                var percentuale_adeguamento = $(b).find('.percentuale_adeguamento').html();

                var reparto = "1";


                switch (parseFloat(container.find('.iva').html().replace("%", ""))) {
                    case parseFloat(ive_misuratore_controller.iva_reparto(1)):
                        quoziente_scorporo = ive_misuratore_controller.iva_reparto(1) / 100 + 1;
                        imponibile[ive_misuratore_controller.iva_reparto(1)] -= (100 * (parseFloat(prezzo_un) * quantita)) / (100 + parseInt(ive_misuratore_controller.iva_reparto(1)));
                        imposta[ive_misuratore_controller.iva_reparto(1)] -= (parseFloat(prezzo_un) * quantita) - ((100 * (parseFloat(prezzo_un) * quantita)) / (100 + parseInt(ive_misuratore_controller.iva_reparto(1))));
                        aliquota_temp = ive_misuratore_controller.iva_reparto(1);
                        reparto = "1";
                        break;
                    case parseFloat(ive_misuratore_controller.iva_reparto(2)):
                        quoziente_scorporo = ive_misuratore_controller.iva_reparto(2) / 100 + 1;
                        imponibile[ive_misuratore_controller.iva_reparto(2)] -= (100 * (parseFloat(prezzo_un) * quantita)) / (100 + parseInt(ive_misuratore_controller.iva_reparto(2)));
                        imposta[ive_misuratore_controller.iva_reparto(2)] -= (parseFloat(prezzo_un) * quantita) - ((100 * (parseFloat(prezzo_un) * quantita)) / (100 + parseInt(ive_misuratore_controller.iva_reparto(2))));
                        aliquota_temp = ive_misuratore_controller.iva_reparto(2);

                        reparto = "2";
                        break;
                    case parseFloat(ive_misuratore_controller.iva_reparto(3)):
                        quoziente_scorporo = ive_misuratore_controller.iva_reparto(3) / 100 + 1;
                        imponibile[ive_misuratore_controller.iva_reparto(3)] -= (100 * (parseFloat(prezzo_un) * quantita)) / (100 + parseInt(ive_misuratore_controller.iva_reparto(3)));
                        imposta[ive_misuratore_controller.iva_reparto(3)] -= (parseFloat(prezzo_un) * quantita) - ((100 * (parseFloat(prezzo_un) * quantita)) / (100 + parseInt(ive_misuratore_controller.iva_reparto(3))));
                        aliquota_temp = ive_misuratore_controller.iva_reparto(3);

                        reparto = "3";
                        break;
                }


                c.C2211 = contatore_linea.toString(); /* Numero Linea + */
                c.C2212; /* Tipo Cessione Prestazione */
                c.C22131 = "Art. Cliente"; /* Codice Tipo + */
                /* CODICE ARTICOLO */
                c.C22132 = ""; /* Codice Valore + */
                c.C2214 = 'Adeguamento Percentuale ' + percentuale_adeguamento + ''; /* Descrizione + */
                c.C2215 = "1"; /* Quantità + */
                c.C2216; /* Unità Misura + */
                c.C2219 = "-" + (parseFloat(prezzo_un) / quoziente_scorporo).toFixed(2); /* Prezzo Unitario + */
                c.C221101; /* Tipo (Sconto-Magg-iorazione) */
                c.C221103; /* Importo Sconto */
                c.C22111 = "-" + (parseFloat(prezzo_un) / quoziente_scorporo * quantita).toFixed(2); /* Prezzo Totale + */
                c.C22112 = aliquota_temp; /* Aliquota IVA + */

                corpo.push(c);

                contatore_linea++;

                xml += '<printRecItemAdjustment operator="1" adjustmentType="3" description="Adeguamento Percentuale ' + percentuale_adeguamento + '" amount="' + parseFloat(prezzo_un).toFixed(2) + '" department="' + reparto + '" justification="1" />';


                /*builder.addText($(b).find('.quantita').html() + "\t\t" + $(b).find('.valore').html() + "\t\t" + $(b).find('.totale').html() + "\t\n");*/
            });

            var totale_generale = $('#totale_generale_fattura_circuito').html().replace("€", "").trim();

            xml += '<printRecTotal  operator="1" description="CONTANTI" payment="' + totale_generale + '" paymentType="0" index="1" justification="1" />';

        }

        xml += '<endFiscalReceipt  operator="1" />';
        xml += '</printerFiscalDocument>';


        /*builder.addText("\n");
         builder.addTextStyle(false, false, true, builder.COLOR_1);
         builder.addText("DATI FISCALI FATTURA\n\n");
         builder.addTextStyle(false, false, false, builder.COLOR_1);
         builder.addText("Imponibile\tIVA\tImposta\tTotale\n");
         builder.addText(container.find('.imponibile').html() + "\t\t");
         builder.addText(container.find('.iva').html() + "\t");
         builder.addText(container.find('.imposta').html() + "\t");
         builder.addText(container.find('.totale').html() + "\t");*/


        t.C2119 = totale_generale; /* Importo Totale Documento + */
        testa.push(t);

        /* Tabella Totali OK */
        if (imponibile[ive_misuratore_controller.iva_reparto(1)] > 0) {
            var o = new Totali();
            o.C2221 = ive_misuratore_controller.iva_reparto(1); /* Aliquota IVA + */
            o.C2222; /* Natura */
            o.C2225 = parseFloat(imponibile[ive_misuratore_controller.iva_reparto(1)]).toFixed(2); /* Imponibile Importo + */
            o.C2226 = parseFloat(imposta[ive_misuratore_controller.iva_reparto(1)]).toFixed(2); /* Imposta + */
            o.C2227 = "I"; /* Esigibilità IVA */
            totali.push(o);
        }

        if (imponibile[ive_misuratore_controller.iva_reparto(2)] > 0) {
            var o = new Totali();
            o.C2221 = ive_misuratore_controller.iva_reparto(2); /* Aliquota IVA + */
            o.C2222; /* Natura */
            o.C2225 = parseFloat(imponibile[ive_misuratore_controller.iva_reparto(2)]).toFixed(2); /* Imponibile Importo + */
            o.C2226 = parseFloat(imposta[ive_misuratore_controller.iva_reparto(2)]).toFixed(2); /* Imposta + */
            o.C2227 = "I"; /* Esigibilità IVA */
            totali.push(o);
        }

        if (imponibile[ive_misuratore_controller.iva_reparto(3)] > 0) {
            var o = new Totali();
            o.C2221 = ive_misuratore_controller.iva_reparto(3); /* Aliquota IVA + */
            o.C2222; /* Natura */
            o.C2225 = parseFloat(imponibile[ive_misuratore_controller.iva_reparto(3)]).toFixed(2); /* Imponibile Importo + */
            o.C2226 = parseFloat(imposta[ive_misuratore_controller.iva_reparto(3)]).toFixed(2); /* Imposta + */
            o.C2227 = "I"; /* Esigibilità IVA */
            totali.push(o);
        }

        var p = new Pagamento();
        p.C241 = "TP02"; /* Condizioni Pagamento */
        p.C2422 = "MP01"; /* Modalità Pagamento */
        p.C2426 = totale_generale; /* Importo Pagamento */
        pagamento.push(p);

        /*builder.addText("\n");
         builder.addFeedLine(1);
         builder.addCut(builder.CUT_FEED);*/


        /*var request = builder.toString();
         var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
         var xhr = new XMLHttpRequest();
         var url = 'http://' + comanda.intelligent_non_fiscale + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
         xhr.open('POST', url, true);
         xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
         xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
         xhr.setRequestHeader('SOAPAction', '""');
         xhr.send(soap);
         
         fattura_elettronica["testa"] = testa;
         fattura_elettronica["corpo"] = corpo;
         fattura_elettronica["totali"] = totali;
         fattura_elettronica["pagamento"] = pagamento;
         
         invia_fattura_elettronica(testa, corpo, totali, pagamento);*/

        var epos = new epson.fiscalPrint();
        epos.send("http://" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=local_printer&timeout=30000", xml, 30000);

        var xml = '<printerCommand><directIO  command="4225" data="" /></printerCommand>';
        var epos = new epson.fiscalPrint();
        epos.send("http://" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=local_printer&timeout=30000", xml, 30000);
        epos.onreceive = function (result, tag_names_array, add_info, res_add) {

            var numero_fattura = (parseInt(add_info.responseData.substr(0, 4)) - 1);

            t.C2114 = numero_fattura;

            testa.push(t);

            fattura_elettronica["testa"] = testa;
            fattura_elettronica["corpo"] = corpo;
            fattura_elettronica["totali"] = totali;
            fattura_elettronica["pagamento"] = pagamento;


            invia_fattura_elettronica(testa, corpo, totali, pagamento);

        };
    });
}

function fattura_circuito(tipo, id) {

    console.log("CHIAMATA FATTURA CIRCUITO");
    //ANTEPRIMA DI STAMPA
    if (tipo === "salva") {
        //ATTUALMENTE NON SERVE

        /* $('#fattura_circuito').css("overflow", "hidden");
         $('#fattura_circuito').printArea();
         $('#fattura_circuito').css("overflow", "auto");*/

        stampa_fattura_circuito();
    }

    //FATTURE EMESSE
    var testo_query = "select numero_fattura FROM record_teste ORDER BY cast(numero_fattura as int) DESC LIMIT 1;";
    comanda.sincro.query_cassa(testo_query, 30000, function (result) {

        var numero_fattura = 0;
        console.log("RISULTATO FATTURA CIRCUITO", result);
        // Non so perchÃƒÂ¨, ma nella query remota parte dall'1
        if (!isNaN(parseInt(result[1].numero_fattura))) {
            //CALCOLA IL PROSSIMO NUMERO FATTURA
            numero_fattura = parseInt(result[1].numero_fattura) + 1;
        } else
        {
            numero_fattura = 1;
        }

        if (tipo === "salva") {
            //SALVATAGGIO NUOVA FATTURA
            testo_query = "update record_teste numero_fattura='" + numero_fattura + "' where nome_buono='" + comanda.id_buono + "';";
            comanda.sincro.query_cassa(testo_query, function () {

            });
        } else
        {
            console.log("NUMERO FATTURA EMESSA", numero_fattura);
            $('.numero_fattura_circuito').html(numero_fattura);
        }

    });
}