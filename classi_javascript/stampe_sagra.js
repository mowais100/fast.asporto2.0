

function sagra_comanda_conto_parcheggia() {

    fa_richiesta_progressivo((numero) => {

        let numero_progressivo = numero.toString();

        var salta_parcheggio = false;

        if (comanda.tavolo.indexOf("CONTINUO") !== -1) {
            salta_parcheggio = true;
        }
        comandapiuconto(salta_parcheggio, undefined, undefined, numero_progressivo);

        salva_comanda(numero_progressivo, true);

    });

}


function sagra_comanda_conto_scontrino() {
    fa_richiesta_progressivo((numero) => {

        let numero_progressivo = numero.toString();

        var salta_parcheggio = false;
        if (comanda.tavolo.indexOf("CONTINUO") !== -1) {
            salta_parcheggio = true;
        }
        comandapiuconto(salta_parcheggio, undefined, undefined, numero_progressivo);
    
        stampa_scontrino('SCONTRINO FISCALE', undefined, undefined);
    });
}