/* global comanda, alasql, REPARTO_controller, parseFloat */

$(document).on(comanda.eventino, function (event) {

    if ($(event.target).hasClass("campo_importo_metodo_pagamento") === true) {

        if ($(event.target).val() === "0.00") {
            $(event.target).val("");
        }

    }


    //questo va fatto a prescindere
    $(".campo_importo_metodo_pagamento").not(event.target).each((index, element) => {

        if ($(element).val() === "") {
            $(element).val("0.00");
        }

    });



});
$(document).on(comanda.eventino, '.forno_pag table tbody tr td:not(.escludi-listener)', function (event) {
    $('#input_ricerca_righe_forno').val("");
    event.stopPropagation();
    event.preventDefault();
    if (event.handled !== true) {

        console.log("LISTENER FORNO", event);
        console.log("MENU FORNO", event.clientX, event.clientY);
        console.log($(this).closest('tr').attr('class'));

        let id_comanda = "";
        let id_comanda_temp = id_comanda_attuale_alasql();

        if (id_comanda_temp !== false) {

            id_comanda = id_comanda_temp;

        }


        if (id_comanda !== "" && (comanda.tipo_consegna === "DOMICILIO" || comanda.tipo_consegna === "RITIRO" || comanda.tipo_consegna === "PIZZERIA")) {

            inforna(true);


            comanda.ultimo_id_asporto = $(this).closest('tr').attr('class');
            comanda.ultimo_nome_parcheggio = $(this).closest('tr').find('.nome_comanda').html();

            /*comanda.progr_gg_uni = 'ASPORTO' + comanda.centro + aggZero(comanda.terminale, 2) + comanda.ultimo_id_asporto;
            comanda.tavolo = comanda.progr_gg_uni;*/

            comanda.progr_gg_uni = 'ASPORTO' + comanda.centro + aggZero(comanda.terminale, 2);
            comanda.tavolo = comanda.progr_gg_uni;

            riapri_comanda_forno(comanda.ultimo_id_asporto, comanda.ultimo_nome_parcheggio, id_comanda);

        } else if (id_comanda === "") {

            comanda.ultimo_id_asporto = $(this).closest('tr').attr('class');
            comanda.ultimo_nome_parcheggio = $(this).closest('tr').find('.nome_comanda').html();

            /*comanda.progr_gg_uni = 'ASPORTO' + comanda.centro + aggZero(comanda.terminale, 2) + comanda.ultimo_id_asporto;
            comanda.tavolo = comanda.progr_gg_uni;*/

            comanda.progr_gg_uni = 'ASPORTO' + comanda.centro + aggZero(comanda.terminale, 2);
            comanda.tavolo = comanda.progr_gg_uni;

            riapri_comanda_forno(comanda.ultimo_id_asporto, comanda.ultimo_nome_parcheggio, id_comanda);

        } else {
            bootbox.alert("Non puoi fare questa operazione finchè non avrai selezionato il tipo di consegna");
        }

        event.handled = true;
    } else {
        return false;
    }

});
if (comanda.pizzeria_asporto !== true) {

    $(document).on('dblclick', '.forno_popup table tbody tr td:not(.escludi-listener)', function (event) {

        event.stopPropagation();
        event.preventDefault();
        if (event.handled !== true) {

            if (id_comanda !== "" && (comanda.tipo_consegna === "DOMICILIO" || comanda.tipo_consegna === "RITIRO" || comanda.tipo_consegna === "PIZZERIA")) {

                inforna(true);

                console.log("LISTENER FORNO", event);
                console.log("MENU FORNO", event.clientX, event.clientY);
                console.log($(this).closest('tr').attr('class'));


                let id_comanda = "";
                let id_comanda_temp = id_comanda_attuale_alasql();

                if (id_comanda_temp !== false) {

                    id_comanda = id_comanda_temp;

                }

                comanda.ultimo_id_asporto = $(this).closest('tr').attr('class');
                comanda.ultimo_nome_parcheggio = $(this).closest('tr').find('.nome_comanda').html();

                comanda.progr_gg_uni = 'ASPORTO' + comanda.centro + aggZero(comanda.terminale, 2) + comanda.ultimo_id_asporto;
                comanda.tavolo = comanda.progr_gg_uni;

                riapri_comanda_forno(comanda.ultimo_id_asporto, comanda.ultimo_nome_parcheggio, id_comanda);

                $('#popup_scelta_cliente').modal('hide');

            } else if (id_comanda === "") {

                console.log("LISTENER FORNO", event);
                console.log("MENU FORNO", event.clientX, event.clientY);
                console.log($(this).closest('tr').attr('class'));


                let id_comanda = "";
                let id_comanda_temp = id_comanda_attuale_alasql();

                if (id_comanda_temp !== false) {

                    id_comanda = id_comanda_temp;

                }

                comanda.ultimo_id_asporto = $(this).closest('tr').attr('class');
                comanda.ultimo_nome_parcheggio = $(this).closest('tr').find('.nome_comanda').html();

                comanda.progr_gg_uni = 'ASPORTO' + comanda.centro + aggZero(comanda.terminale, 2) + comanda.ultimo_id_asporto;
                comanda.tavolo = comanda.progr_gg_uni;

                riapri_comanda_forno(comanda.ultimo_id_asporto, comanda.ultimo_nome_parcheggio, id_comanda);

                $('#popup_scelta_cliente').modal('hide');

            } else {
                bootbox.alert("Non puoi fare questa operazione finchè non avrai selezionato il tipo di consegna");
            }

            event.handled = true;

        } else {
            return false;
        }

    });
}

async function cambia_ive_asporto() {
    /* id_comanda_attuale_alasql()
     * 
     * se id comanda attuale è diverso da false
     
     seleziono where id comanda attuale e stato rec attivo e nodo 3 cifre
     
     modifico sempre nell id attuale e stato rec attivo sui nodi a 3 cifre con like 3 cifre%*/
    /* global comanda */

    let id_comanda = id_comanda_attuale_alasql();
    if (id_comanda !== false) {

        let query_select = "select * from comanda where length(nodo)=3 and stato_record='ATTIVO' and id='" + id_comanda + "';";
        let result = alasql(query_select);
        result.forEach((v) => {

            let query_select_prodotti = "select * from prodotti where id='" + v.cod_articolo + "' and categoria!='XXX' limit 1;";
            let result_prodotti = alasql(query_select_prodotti);
            let reparto = "";
            let perc_iva = "";
            if (result_prodotti.length > 0) {

                result_prodotti.forEach((p) => {


                    /*CONDIZIONI
                     * 
                     * tavolo asporto (che può essere anche in pizzeria
                     * 
                     *      non xml
                     * 
                     *      tipo consegna pizzeria in pizzeria da asporto
                     *      
                     *      comanda.tavolo === 'TAKE AWAY' || comanda.tavolo === '*TAKEAWAY*' || comanda.tavolotxt === '*TAKEAWAY*'
                     *      
                     *      altri casi
                     *      
                     * */

                    if (comanda.compatibile_xml !== true) {

                        let reparti = alasql('SELECT aliquota_default,aliquota_takeaway,reparto_beni_default,reparto_servizi_default,reparto_mangiaqui,reparto_ritiro,reparto_domicilio from impostazioni_fiscali LIMIT 1;');

                        if (comanda.tipo_consegna === "PIZZERIA") {

                            if (reparti[0].reparto_mangiaqui === "0") {
                                if (p.perc_iva_base !== undefined && p.perc_iva_base !== null && p.perc_iva_base !== '') {
                                    perc_iva = p.perc_iva_base;
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_servizi;
                                    }
                                } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                                    perc_iva = comanda.perc_iva_default;
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_servizi;
                                    }
                                } else {
                                    perc_iva = "10";
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_servizi;
                                    }
                                }
                            } else if (reparti[0].reparto_mangiaqui === "1") {
                                if (p.perc_iva_takeaway !== undefined && p.perc_iva_takeaway !== null && !isNaN(p.perc_iva_takeaway) && p.perc_iva_takeaway !== '') {
                                    perc_iva = p.perc_iva_takeaway;
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_beni;
                                    }
                                } else if (comanda.percentuale_iva_takeaway !== undefined && comanda.percentuale_iva_takeaway !== null && !isNaN(comanda.percentuale_iva_takeaway) && comanda.percentuale_iva_takeaway !== '') {
                                    perc_iva = comanda.percentuale_iva_takeaway;
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_beni;
                                    }
                                } else if (p.perc_iva_base !== undefined && p.perc_iva_base !== null && p.perc_iva_base !== '') {
                                    perc_iva = p.perc_iva_base;
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_servizi;
                                    }
                                } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                                    perc_iva = comanda.perc_iva_default;
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_servizi;
                                    }
                                } else {
                                    perc_iva = "22";
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_beni;
                                    }
                                }
                            } else {
                                if (p.perc_iva_base !== undefined && p.perc_iva_base !== null && p.perc_iva_base !== '') {
                                    perc_iva = p.perc_iva_base;
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_servizi;
                                    }
                                } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                                    perc_iva = comanda.perc_iva_default;
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_servizi;
                                    }
                                } else {
                                    perc_iva = "10";
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_servizi;
                                    }
                                }
                            }



                        } else if (comanda.tipo_consegna === "RITIRO") {

                            if (reparti[0].reparto_ritiro === "0") {
                                if (p.perc_iva_base !== undefined && p.perc_iva_base !== null && p.perc_iva_base !== '') {
                                    perc_iva = p.perc_iva_base;
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_servizi;
                                    }
                                } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                                    perc_iva = comanda.perc_iva_default;
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_servizi;
                                    }
                                } else {
                                    perc_iva = "10";
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_servizi;
                                    }
                                }
                            } else if (reparti[0].reparto_ritiro === "1") {
                                if (p.perc_iva_takeaway !== undefined && p.perc_iva_takeaway !== null && !isNaN(p.perc_iva_takeaway) && p.perc_iva_takeaway !== '') {
                                    perc_iva = p.perc_iva_takeaway;
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_beni;
                                    }
                                } else if (comanda.percentuale_iva_takeaway !== undefined && comanda.percentuale_iva_takeaway !== null && !isNaN(comanda.percentuale_iva_takeaway) && comanda.percentuale_iva_takeaway !== '') {
                                    perc_iva = comanda.percentuale_iva_takeaway;
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_beni;
                                    }
                                } else if (p.perc_iva_base !== undefined && p.perc_iva_base !== null && p.perc_iva_base !== '') {
                                    perc_iva = p.perc_iva_base;
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_servizi;
                                    }
                                } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                                    perc_iva = comanda.perc_iva_default;
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_servizi;
                                    }
                                } else {
                                    perc_iva = "22";
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_beni;
                                    }
                                }
                            } else {
                                if (p.perc_iva_takeaway !== undefined && p.perc_iva_takeaway !== null && !isNaN(p.perc_iva_takeaway) && p.perc_iva_takeaway !== '') {
                                    perc_iva = p.perc_iva_takeaway;
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_beni;
                                    }
                                } else if (comanda.percentuale_iva_takeaway !== undefined && comanda.percentuale_iva_takeaway !== null && !isNaN(comanda.percentuale_iva_takeaway) && comanda.percentuale_iva_takeaway !== '') {
                                    perc_iva = comanda.percentuale_iva_takeaway;
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_beni;
                                    }
                                } else if (p.perc_iva_base !== undefined && p.perc_iva_base !== null && p.perc_iva_base !== '') {
                                    perc_iva = p.perc_iva_base;
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_servizi;
                                    }
                                } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                                    perc_iva = comanda.perc_iva_default;
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_servizi;
                                    }
                                } else {
                                    perc_iva = "22";
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_beni;
                                    }
                                }
                            }
                        } else if (comanda.tipo_consegna === "DOMICILIO") {

                            if (reparti[0].reparto_domicilio === "0") {
                                if (p.perc_iva_base !== undefined && p.perc_iva_base !== null && p.perc_iva_base !== '') {
                                    perc_iva = p.perc_iva_base;
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_servizi;
                                    }
                                } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                                    perc_iva = comanda.perc_iva_default;
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_servizi;
                                    }
                                } else {
                                    perc_iva = "10";
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_servizi;
                                    }
                                }
                            } else if (reparti[0].reparto_domicilio === "1") {
                                if (p.perc_iva_takeaway !== undefined && p.perc_iva_takeaway !== null && !isNaN(p.perc_iva_takeaway) && p.perc_iva_takeaway !== '') {
                                    perc_iva = p.perc_iva_takeaway;
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_beni;
                                    }
                                } else if (comanda.percentuale_iva_takeaway !== undefined && comanda.percentuale_iva_takeaway !== null && !isNaN(comanda.percentuale_iva_takeaway) && comanda.percentuale_iva_takeaway !== '') {
                                    perc_iva = comanda.percentuale_iva_takeaway;
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_beni;
                                    }
                                } else if (p.perc_iva_base !== undefined && p.perc_iva_base !== null && p.perc_iva_base !== '') {
                                    perc_iva = p.perc_iva_base;
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_servizi;
                                    }
                                } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                                    perc_iva = comanda.perc_iva_default;
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_servizi;
                                    }
                                } else {
                                    perc_iva = "22";
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_beni;
                                    }
                                }
                            } else {
                                if (p.perc_iva_takeaway !== undefined && p.perc_iva_takeaway !== null && !isNaN(p.perc_iva_takeaway) && p.perc_iva_takeaway !== '') {
                                    perc_iva = p.perc_iva_takeaway;
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_beni;
                                    }
                                } else if (comanda.percentuale_iva_takeaway !== undefined && comanda.percentuale_iva_takeaway !== null && !isNaN(comanda.percentuale_iva_takeaway) && comanda.percentuale_iva_takeaway !== '') {
                                    perc_iva = comanda.percentuale_iva_takeaway;
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_beni;
                                    }
                                } else if (p.perc_iva_base !== undefined && p.perc_iva_base !== null && p.perc_iva_base !== '') {
                                    perc_iva = p.perc_iva_base;
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_servizi;
                                    }
                                } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                                    perc_iva = comanda.perc_iva_default;
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_servizi;
                                    }
                                } else {
                                    perc_iva = "22";
                                    if (comanda.compatibile_xml !== true) {
                                        reparto = p.reparto_beni;
                                    }
                                }
                            }
                        } else if (comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === 'TAKE AWAY' || comanda.tavolo === '*TAKEAWAY*' || comanda.tavolotxt === '*TAKEAWAY*') {

                            if (p.perc_iva_takeaway !== undefined && p.perc_iva_takeaway !== null && !isNaN(p.perc_iva_takeaway) && p.perc_iva_takeaway !== '') {
                                perc_iva = p.perc_iva_takeaway;
                                if (comanda.compatibile_xml !== true) {
                                    reparto = p.reparto_beni;
                                }
                            } else if (comanda.percentuale_iva_takeaway !== undefined && comanda.percentuale_iva_takeaway !== null && !isNaN(comanda.percentuale_iva_takeaway) && comanda.percentuale_iva_takeaway !== '') {
                                perc_iva = comanda.percentuale_iva_takeaway;
                                if (comanda.compatibile_xml !== true) {
                                    reparto = p.reparto_beni;
                                }
                            } else if (p.perc_iva_base !== undefined && p.perc_iva_base !== null && p.perc_iva_base !== '') {
                                perc_iva = p.perc_iva_base;
                                if (comanda.compatibile_xml !== true) {
                                    reparto = p.reparto_servizi;
                                }
                            } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                                perc_iva = comanda.perc_iva_default;
                                if (comanda.compatibile_xml !== true) {
                                    reparto = p.reparto_servizi;
                                }
                            } else {
                                perc_iva = "22";
                                if (comanda.compatibile_xml !== true) {
                                    reparto = p.reparto_beni;
                                }
                            }

                        } else {

                            if (p.perc_iva_base !== undefined && p.perc_iva_base !== null && p.perc_iva_base !== '') {
                                perc_iva = p.perc_iva_base;
                                if (comanda.compatibile_xml !== true) {
                                    reparto = p.reparto_servizi;
                                }
                            } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                                perc_iva = comanda.perc_iva_default;
                                if (comanda.compatibile_xml !== true) {
                                    reparto = p.reparto_servizi;
                                }
                            } else {
                                perc_iva = "10";
                                if (comanda.compatibile_xml !== true) {
                                    reparto = p.reparto_servizi;
                                }
                            }

                        }

                    } else {

                        if (p.perc_iva_base !== undefined && p.perc_iva_base !== null && p.perc_iva_base !== '') {
                            perc_iva = p.perc_iva_base;
                            if (comanda.compatibile_xml !== true) {
                                reparto = p.reparto_servizi;
                            }
                        } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                            perc_iva = comanda.perc_iva_default;
                            if (comanda.compatibile_xml !== true) {
                                reparto = p.reparto_servizi;
                            }
                        } else {
                            perc_iva = "10";
                            if (comanda.compatibile_xml !== true) {
                                reparto = p.reparto_servizi;
                            }
                        }

                    }

                    let query_update = "update comanda set reparto='" + reparto + "',perc_iva='" + perc_iva + "' where nodo LIKE '" + v.nodo + "%' and stato_record='ATTIVO' and id='" + id_comanda + "';";
                    alasql(query_update);

                    comanda.sock.send({
                        tipo: "aggiornamento_tavolo",
                        operatore: comanda.operatore,
                        query: query_update,
                        terminale: comanda.terminale,
                        ip: comanda.ip_address
                    });

                    comanda.funzionidb.conto_attivo();
                });
                return true;
            } else {
                return true;
                /* lasci le ive come sono se non hai corrispondenze */
            }
        });
    }



}

async function toggle_scooter() {


    /* let r = await comanda.sincro.query_locale_sincrona("select valore from settaggi where nome='altezza_barra_scooter' limit 1;")
     if(r.length>0){
     
     }*/

    let altezza_consegna = $(".consegna").height();
    altezza_consegna += parseInt($(".consegna  h3").css("margin-top").replace("px", ""));

    if ($(".consegna").is(":visible") === true) {
        $(".forno_pag").height($(".forno_pag").height() + altezza_consegna);
        $(".consegna").hide();
        $("#btn_domicilio_visibile").hide();
        $("#btn_domicilio_non_visibile").show();
        comanda.scooter_visible = false;
    } else {
        $(".forno_pag").height("550px");
        $(".consegna").show();
        $("#btn_domicilio_non_visibile").hide();
        $("#btn_domicilio_visibile").show();
        comanda.scooter_visible = true;
    }




}

function codice_ordinamento_stampa(codice_portata) {
    let r = alasql("select ordinamento_stampa from nomi_portate where numero='" + codice_portata + "' limit 1;");
    if (r.length === 1) {
        return r[0].ordinamento_stampa.trim();
    }
    return "";
}

function codice_portata(ordinamento_stampa) {
    let r = alasql("select codice_portata from nomi_portate where numero='" + ordinamento_stampa + "' limit 1;");
    if (r.length === 1) {
        return r[0].ordinamento_stampa.trim();
    }
    return "";
}

function calcola_importi_fiscali() {

    var prezzo_vero = "prezzo_un";
    if (comanda.pizzeria_asporto === true) {
        prezzo_vero = "prezzo_vero";
    }

    let testo_query = "";

    testo_query = "UPDATE comanda SET totale_scontato_ivato =ROUND(((" + prezzo_vero + "*quantita)-((prezzo_un*quantita)*sconto_perc/100)),2)  where (sconto_perc!='' and sconto_perc!='0' and sconto_perc!='0.00');";
    comanda.sock.send({
        tipo: "calcola_importi_fiscali",
        operatore: comanda.operatore,
        query: testo_query,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    alasql(testo_query);

    testo_query = "UPDATE comanda SET totale_scontato_ivato =ROUND(((" + prezzo_vero + "*quantita)),2)  where (sconto_perc='' or sconto_perc='0' or sconto_perc='0.00');";
    comanda.sock.send({
        tipo: "calcola_importi_fiscali",
        operatore: comanda.operatore,
        query: testo_query,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    alasql(testo_query);

    let iva_rep = new Object();

    for (let department = 1; department <= 5; department++) {

        iva_rep[department] = parseInt(REPARTO_controller.iva_reparto(parseInt(REPARTO_controller.indice(parseInt(department)))));

        if (isNaN(iva_rep[department])) {
            iva_rep[department] = 0;
        }

        testo_query = "UPDATE comanda SET imponibile_rep_" + department + "=ROUND((totale_scontato_ivato/" + (iva_rep[department] / 100 + 1) + "),8)  where reparto='" + department + "'";
        comanda.sock.send({
            tipo: "calcola_importi_fiscali",
            operatore: comanda.operatore,
            query: testo_query,
            terminale: comanda.terminale,
            ip: comanda.ip_address
        });
        alasql(testo_query);

        testo_query = "UPDATE comanda SET imposta_rep_" + department + "=ROUND((totale_scontato_ivato-imponibile_rep_" + department + "),8) where reparto='" + department + "';";
        comanda.sock.send({
            tipo: "calcola_importi_fiscali",
            operatore: comanda.operatore,
            query: testo_query,
            terminale: comanda.terminale,
            ip: comanda.ip_address
        });
        alasql(testo_query);

        testo_query = "UPDATE comanda SET importo_totale_fiscale=(imponibile_rep_1+imponibile_rep_2+imponibile_rep_3+imponibile_rep_4+imponibile_rep_5+imposta_rep_1+imposta_rep_2+imposta_rep_3+imposta_rep_4+imposta_rep_5);";
        comanda.sock.send({
            tipo: "calcola_importi_fiscali",
            operatore: comanda.operatore,
            query: testo_query,
            terminale: comanda.terminale,
            ip: comanda.ip_address
        });
        alasql(testo_query);

    }

    comanda.sincro.query_cassa();

}

function log_query_db_centrale(testo) {

    /*salva log db centrale solo se è cassa singola*/

    var counter = 0;

    function interna() {
        counter++;
        $.ajax({
            type: "POST",
            /*deve salvare il log sulla sua stessa macchina. poi semmai li unisci con excel altrimenti rischi che non salvi i log*/
            url: 'http://localhost:' + comanda.porta_server_remoto + '/fast.intellinet/classi_php/salva_log_query_db_centrale.php',
            data: {
                testo_log: Date.now() + "#" + testo.replace(/\s\s+/g, ' ').replace(/[\n\r]+/g, '') + "#TENTATIVO " + counter
            },
            success: function (data) {
                if (data === "FALSE" || data === "false") {
                    if (counter === 10) {

                        //bootbox.alert("ERRORE DI RETE - SALVATAGGIO LOG FALLITO DOPO 10 TENTATIVI (1 " + data + "): " + testo);
                    } else {
                        interna();
                    }
                }
            },
            error: function () {
                if (counter === 10) {
                    //bootbox.alert("FUORI COPERTURA - SALVATAGGIO LOG FALLITO DOPO 10 TENTATIVI(2): " + testo);
                } else {
                    interna();
                }
            },
            timeout: 1000
        });
    }

    /*if (comanda.compatibile_xml !== true) {
     if (comanda.mobile === true) {
     appendAndroidLog(Date.now() + "#" + testo.replace(/\s\s+/g, ' ').replace(/[\n\r]+/g, '') + "#");
     } else {
     interna();
     }
     }*/
}

async function intesta_cliente(visualizza_popup, reset) {

    if (comanda.partita_iva_estera === true) {
        $('.campo_partita_iva_estera').show();
    } else {
        $('.campo_partita_iva_estera').hide();
    }

    /*SCROLLA A 0 LA TENDINA DI SCELTA CLIENTE
     $('select#popup_scelta_cliente_elenco_clienti').scrollTop(0);*/

    if ($('#popup_scelta_cliente:visible').length === 0 || reset === true) {

        if (reset === true) {

            let id_comanda = "";
            let id_comanda_temp = id_comanda_attuale_alasql();

            if (id_comanda_temp !== false) {

                id_comanda = id_comanda_temp;

            }

            /* aggiunto il 11/05/2021 per evitare errore di sincronia trovato da Linda e Giancarlo, che quando aprivi l'ultimo tavolo sull'altro pc, 
             * l'id di comanda del primo diventava come l'ultima comanda battuta causando errori di battitura e lettura a video
             */
            comanda.progr_gg_uni = 'ASPORTO' + comanda.centro + aggZero(comanda.terminale, 2) + id_comanda;
            comanda.tavolo = comanda.progr_gg_uni;


            comanda.ultimo_id_cliente = undefined;
            comanda.ultimo_id_asporto = undefined;
            comanda.dati_temporanei_cliente = undefined;
            non_fare_subito();
        }

        if (comanda.consumazione_in_pizzeria === true || comanda.consumazione_in_pizzeria === "true") {
            $(".consegna_pizzeria").show();
        } else {
            $(".consegna_pizzeria").hide();
        }

        console.log("** LOG ORDINE", "TASTO AGGIUNTA CLIENTE");
        $('#popup_scelta_cliente input').not('[name="numeretto"]').not('#data_fattura').val('');
        $('#popup_scelta_cliente select[name="nazione"] option[value="IT"]').prop("selected", "selected");
        $('#popup_scelta_cliente select[name="regime_fiscale"] option[value="RF01"]').prop("selected", "selected");
        $('#popup_scelta_cliente input[name="formato_trasmissione"]').prop("checked", false);
        $('#popup_scelta_cliente input[name="azienda_privato"]#ap_privato').prop("checked", "checked");
        $('#popup_scelta_cliente form select#popup_scelta_cliente_elenco_clienti option').remove();
        var prog_result = alasql('SELECT numero FROM progressivo_asporto limit 1;');
        var results = alasql('SELECT id,nome,cognome,ragione_sociale FROM clienti ORDER BY nome ASC,cognome ASC,ragione_sociale ASC;');
        var progressivo_asporto = '0';
        if (comanda.ultimo_id_cliente !== undefined && comanda.ultimo_id_cliente !== null && typeof (comanda.ultimo_id_cliente) === 'string' && comanda.ultimo_id_cliente.substr(0, 1) === 'P') {
            progressivo_asporto = comanda.ultimo_id_cliente.substr(1);
        } else {
            progressivo_asporto = prog_result[0].numero;
            //SOLO QUANDO INFORNO UN CLIENTE COL PROGRESSIVO VA AVANTI, ALTRIMENTI RESTA FERMO
        }

        var opzione = "";
        results.forEach(function (row) {

            let style = "";
            let pallina = "";

            if (parseInt(row.id) > 3000000) {
                //style = " style='background-color:yellow; ' ";
                pallina = " &bull;";
            }

            opzione += "<option value=\"" + row.id + "\" " + style + ">" + row.nome + " " + row.cognome + pallina;
            if (row.ragione_sociale) {
                opzione += " - " + row.ragione_sociale;
            }
            opzione += "</option>";
        });
        $('#popup_scelta_cliente_elenco_clienti').html(opzione);
        if (comanda.ultimo_id_cliente !== undefined && comanda.ultimo_id_cliente !== null && comanda.ultimo_id_cliente !== 'null' && comanda.ultimo_id_cliente.length > 0) {

            if (comanda.ultimo_id_asporto !== undefined) {

                var testo_query = "select jolly,ora_consegna,orario_preparazione,tipo_consegna,tipologia_fattorino,consegna_gap from comanda where ntav_comanda='" + comanda.tavolo + "' and id='" + comanda.ultimo_id_asporto + "' order by ora_consegna desc, tipo_consegna desc, tipologia_fattorino desc, jolly desc limit 1;";
                var dati_cliente = alasql(testo_query);
                if (visualizza_popup !== false) {
                    $('#popup_scelta_cliente').modal('show');
                    $('#popup_scelta_cliente_elenco_clienti').focus();
                }

                //  12/08/1920


                $('#popup_scelta_cliente [name="giorno_nascita"] option').prop('selected', false);
                $('#popup_scelta_cliente [name="mese_nascita"] option').prop('selected', false);
                $('#popup_scelta_cliente [name="anno_nascita"] option').prop('selected', false);
                if (results !== undefined && results[0] !== undefined && results[0].data_nascita !== undefined && results[0].data_nascita !== null && results[0].data_nascita.length === 10) {


                    var giorno_nascita = results[0].data_nascita.substr(0, 2);
                    var mese_nascita = results[0].data_nascita.substr(3, 2);
                    var anno_nascita = results[0].data_nascita.substr(6, 4);
                    $('#popup_scelta_cliente [name="giorno_nascita"] option:containsExact(' + giorno_nascita + ')').prop('selected', true);
                    $('#popup_scelta_cliente [name="mese_nascita"] option[value="' + mese_nascita + '"]').prop('selected', true);
                    if (anno_nascita !== '0000') {
                        $('#popup_scelta_cliente [name="anno_nascita"] option:containsExact(' + anno_nascita + ')').prop('selected', true);
                    }
                } else {
                    $('#popup_scelta_cliente [name="giorno_nascita"] option:first-child').prop('selected', true);
                    $('#popup_scelta_cliente [name="mese_nascita"] option:first-child').prop('selected', true);
                    $('#popup_scelta_cliente [name="anno_nascita"] option:first-child').prop('selected', true);
                }

                //SE ULTIMO ID E' UN NUMERO
                if (!isNaN(comanda.ultimo_id_cliente)) {
                    $('#popup_scelta_cliente_elenco_clienti option[value="' + comanda.ultimo_id_cliente + '"]').attr("selected", true);
                    $('#popup_scelta_cliente_elenco_clienti').trigger('change');
                } else if (comanda.parcheggio.trim() === comanda.ultimo_id_cliente.trim()) {
                    /*AGGIUNTO TRIM A comanda.parcheggio l'1/12/2020 perchè altrimenti riprendendo un nome non memorizzato mi riassegnava il progressivo
                     * quando andavo a re-intestare, ad esempio se andavo a cambiare l'ora
                     */
                    $('#popup_scelta_cliente [name="nome"]').val(comanda.ultimo_id_cliente.trim());
                }


                $('.tipo_consegna').removeClass('tipo_consegna_selezionata');
                if (dati_cliente !== undefined && dati_cliente[0] !== undefined && dati_cliente[0].ora_consegna !== undefined && dati_cliente[0].tipo_consegna !== undefined) {

                    comanda.tipologia_fattorino = dati_cliente[0].tipologia_fattorino;
                    /* studiare come riprendere consegna */
                    if (comanda.tipologia_fattorino !== null && comanda.tipologia_fattorino !== "0") {

                        var totale_consegne = alasql("select QTA from comanda where ntav_comanda='" + comanda.tavolo + "' and id='" + comanda.ultimo_id_asporto + "' order by ora_consegna desc, tipo_consegna desc, tipologia_fattorino desc, jolly desc;");
                        var pr_tot_consegne = 0;
                        totale_consegne.forEach(v => {
                            pr_tot_consegne += parseFloat(v.QTA);
                        });
                        comanda.consegna_fattorino_extra = pr_tot_consegne;
                    } else {
                        comanda.consegna_fattorino_extra = "0.00";
                    }

                    $("select#tipologia_fattorino option[value=" + dati_cliente[0].tipologia_fattorino + "]").prop("selected", "selected");
                    $("select#tipologia_fattorino").trigger("change");
                    var ora = dati_cliente[0].ora_consegna;
                    var consegna_gap = dati_cliente[0].consegna_gap;
                    var orario_preparazione = dati_cliente[0].orario_preparazione;
                    //var ora = $('.' + comanda.ultimo_id_asporto + ' td:first-child').html();

                    comanda.ora_consegna = ora.substr(0, 2);
                    comanda.minuti_consegna = ora.substr(3, 2);

                    let scritta_consegna_ordine = '';



                    $('[name="ora_consegna"]').val(ora);
                    $("[name='consegna_input_gap']").val(consegna_gap);
                    comanda.ultima_ora_consegna = ora;
                    comanda.ultima_ora_consegna_gap = consegna_gap;
                    var tipo_consegna = dati_cliente[0].tipo_consegna;
                    //var tipo_consegna = $('.forno_pag .' + comanda.ultimo_id_asporto + ' td:nth-child(4)').html();

                    $('[name="orario_preparazione"]').val(orario_preparazione);
                    if (tipo_consegna === undefined) {
                        //NIENTE
                    } else if (tipo_consegna === "RITIRO") {
                        comanda.tipo_consegna = "RITIRO";
                        $('.tipo_consegna').removeClass('tipo_consegna_selezionata');
                        $('.consegna_ritiro').addClass('tipo_consegna_selezionata');
                        $('#popup_scelta_cliente input[name="numeretto"]').val(progressivo_asporto);
                    } else if (tipo_consegna === "DOMICILIO") {
                        comanda.tipo_consegna = "DOMICILIO";
                        $('.tipo_consegna').removeClass('tipo_consegna_selezionata');
                        $('.consegna_domicilio').addClass('tipo_consegna_selezionata');
                        $('#popup_scelta_cliente input[name="numeretto"]').val(progressivo_asporto);
                    } else {
                        comanda.tipo_consegna = "PIZZERIA";
                        $('.tipo_consegna').removeClass('tipo_consegna_selezionata');
                        $('.consegna_pizzeria').addClass('tipo_consegna_selezionata');
                        $('#popup_scelta_cliente input[name="numeretto"]').val(progressivo_asporto);
                    }
                    if (consegna_gap === "15") {
                        $('.consegna_gap_15').addClass('tipo_consegna_selezionata_gap');
                        $('#consegna_input_gap').val('15');
                        comanda.testo_consegna_15 = true;
                    }
                    else if(consegna_gap ==="30") {
                        $('.consegna_gap_30').addClass('tipo_consegna_selezionata_gap');
                        $('#consegna_input_gap').val('30');
                       comanda.testo_consegna_30 = true;
                    }
                    else{
                        $('#consegna_input_gap').val("");
                    }
                }

                if (dati_cliente !== undefined && dati_cliente[0] !== undefined && dati_cliente[0].jolly !== undefined && dati_cliente[0].jolly === "SUBITO") {

                    $('[name="codice_lotteria"]').val(await fa_richiesta_codice_lotteria());
                    comanda.esecuzione_comanda_immediata = true;
                    var data_js = new Date();
                    var ora_adesso = addZero(data_js.getHours(), 2) + ':' + addZero(data_js.getMinutes(), 2);
                    $('#popup_scelta_cliente input[name="ora_consegna"]:visible').val(ora_adesso);
                    comanda.ultima_ora_consegna = ora_adesso;
                    comanda.consegna_gap = consegna_gap;


                    /* cambiata il 20/11/2020 perchè se aprivi una consegna a domicilio da fare subito la convertiva in ritiro sto merda*/
                    if (comanda.tipo_consegna === "DOMICILIO") {
                        $('.tipo_consegna').removeClass('tipo_consegna_selezionata');
                        $('.consegna_gap_15').removeClass('tipo_consegna_selezionata_gap');
                        $('.consegna_gap_30').removeClass('tipo_consegna_selezionata_gap');
                        $('.consegna_domicilio').addClass('tipo_consegna_selezionata');
                    } else if (comanda.tipo_consegna === "PIZZERIA") {
                        //    comanda.tipo_consegna = "PIZZERIA";
                        $('.tipo_consegna').removeClass('tipo_consegna_selezionata');
                        $('.consegna_gap_15').removeClass('tipo_consegna_selezionata_gap');
                        $('.consegna_gap_30').removeClass('tipo_consegna_selezionata_gap');
                        $('.consegna_pizzeria').addClass('tipo_consegna_selezionata');
                    } else {
                        comanda.tipo_consegna = 'RITIRO';
                        $('.tipo_consegna').removeClass('tipo_consegna_selezionata');
                        $('.consegna_gap_15').removeClass('tipo_consegna_selezionata_gap');
                        $('.consegna_gap_30').removeClass('tipo_consegna_selezionata_gap');
                        $('.consegna_ritiro').addClass('tipo_consegna_selezionata');
                    }
                    if (consegna_gap === "15") {
                        $('.consegna_gap_15').addClass('tipo_consegna_selezionata_gap');
                        $('#consegna_input_gap').val('15');
                        comanda.testo_consegna_15 = true;
                    }
                    else if(consegna_gap ==="30") {
                        $('.consegna_gap_30').addClass('tipo_consegna_selezionata_gap');
                        $('#consegna_input_gap').val('30');
                        comanda.testo_consegna_30 = true;
                    }
                    else{
                        $('#consegna_input_gap').val("");
                    }

                    $('#switch_esecuzione_comanda_immediata').css('background-image', '-webkit-linear-gradient(top, grey,#4d4d4d)');
                } else {

                    $('[name="codice_lotteria"]').val(await fa_richiesta_codice_lotteria());
                    comanda.esecuzione_comanda_immediata = false;
                    $('#switch_esecuzione_comanda_immediata').css('background-image', '');
                }


            } else if (comanda.dati_temporanei_cliente !== undefined) {

                //dati_cliente

                if (visualizza_popup !== false) {
                    $('#popup_scelta_cliente').modal('show');
                    $('#popup_scelta_cliente_elenco_clienti').focus();
                }

                $('#popup_scelta_cliente [name="giorno_nascita"] option').prop('selected', false);
                $('#popup_scelta_cliente [name="mese_nascita"] option').prop('selected', false);
                $('#popup_scelta_cliente [name="anno_nascita"] option').prop('selected', false);
                if (!isNaN(comanda.ultimo_id_cliente)) {
                    $('#popup_scelta_cliente_elenco_clienti option[value="' + comanda.ultimo_id_cliente + '"]').attr("selected", true);
                    $('#popup_scelta_cliente_elenco_clienti').trigger('change');
                } else if (comanda.parcheggio === comanda.ultimo_id_cliente.trim()) {
                    $('#popup_scelta_cliente [name="nome"]').val(comanda.ultimo_id_cliente.trim());
                }


                $('.tipo_consegna').removeClass('tipo_consegna_selezionata');
                $('.consegna_gap_15').removeClass('tipo_consegna_selezionata_gap');
                $('.consegna_gap_30').removeClass('tipo_consegna_selezionata_gap');
                if (comanda.dati_temporanei_cliente !== undefined && comanda.dati_temporanei_cliente.ora_consegna !== undefined && comanda.dati_temporanei_cliente.tipo_consegna !== undefined) {

                    comanda.tipologia_fattorino = comanda.dati_temporanei_cliente.tipologia_fattorino;
                    //QUA ANDAVA IL PREZZO DI CONSEGNA EXTRA EVENTUALMENTE

                    $("select#tipologia_fattorino option[value=" + comanda.dati_temporanei_cliente.tipologia_fattorino + "]").prop("selected", "selected");
                    $("select#tipologia_fattorino").trigger("change");
                    var ora = comanda.dati_temporanei_cliente.ora_consegna;
                    var orario_preparazione = comanda.dati_temporanei_cliente.orario_preparazione;
                    var codice_lotteria = comanda.dati_temporanei_cliente.codice_lotteria;
                    comanda.ora_consegna = ora.substr(0, 2);
                    comanda.minuti_consegna = ora.substr(3, 2);

                    let scritta_consegna_ordine = '';
                    switch (comanda.tipo_consegna) {
                        case 'DOMICILIO':
                            scritta_consegna_ordine = comanda.scritta_consegna_domicilio;
                            if (scritta_consegna_ordine == "") {
                                scritta_consegna_ordine = 'DOMICILIO';

                            }
                            break;

                        case 'RITIRO':
                            scritta_consegna_ordine = comanda.scritta_consegna_ritiro;
                            break;

                        case 'PIZZERIA':
                            scritta_consegna_ordine = comanda.scritta_consegna_pizzeria;
                            break;

                    }
                    var query_gap_consegna = "select time_gap_consegna from impostazioni_fiscali where id=" + comanda.folder_number + ";"
                    var result = alasql(query_gap_consegna);
                    if (result[0].time_gap_consegna == "1" && comanda.consegna_gap != "") {
                       // comanda.consegna_gap = consegna_gap;
                        var minuti_somma = ora.substring(ora.indexOf(':') + 1)
                        var int_miunti = parseInt(minuti_somma);
                        var int_gap_miunti = parseInt(comanda.consegna_gap);;
                        var min_somma = int_miunti + int_gap_miunti;
                        var ora_gap = ora.substring(0, ora.indexOf(':'))
                        var hours = Math.floor(min_somma / 60);
                        var minutes = min_somma % 60;
                        var ore = parseInt(ora_gap);

                        if (minutes < 10) {
                            if (ore < 10) {
                                var oraa = ore + hours
                                if(oraa===10)
                                { var totale_ora = oraa + ":" + minutes;
        
                                }
                                else{
                                    var totale_ora = "0" + oraa + ":" + minutes;
                                }
                            }
                            else {
                                var totale_ora = ore + hours + ":" + "0" + minutes;

                            }

                        }
                        else {
                            if (ore < 10) {
                                var oraa = ore + hours
                                if(oraa===10)
                                { var totale_ora = oraa + ":" + minutes;
        
                                }
                                else{
                                    var totale_ora = "0" + oraa + ":" + minutes;
                                }
                            }
                            else {

                                var totale_ora = ore + hours + ":" + minutes;
                            }
                        }


                        $('#tipo_consegna_modal_tipo').html(scritta_consegna_ordine + ": ");
                        $('#tipo_consegna_modal').html(ora);
                        $('#tipo_consegna_modal_gap_scritta').html("Gap:");
                        $('#tipo_consegna_gap_modal').html(totale_ora);

                    }
                    else {
                        $('#tipo_consegna_modal_tipo').html(scritta_consegna_ordine + ": ");
                        $('#tipo_consegna_modal').html(ora);

                    }

                    $('[name="ora_consegna"]').val(ora);
                    $("[name='consegna_input_gap']").val(comanda.consegna_gap);

                    comanda.ultima_ora_consegna = ora;
                   // comanda.consegna_gap = consegna_gap;
                    var tipo_consegna = comanda.dati_temporanei_cliente.tipo_consegna;
                    //var tipo_consegna = $('.forno_pag .' + comanda.ultimo_id_asporto + ' td:nth-child(4)').html();

                    $('[name="orario_preparazione"]').val(orario_preparazione);
                    $('[name="codice_lotteria"]').val(codice_lotteria);
                    if (tipo_consegna === undefined) {
                        //NIENTE
                    } else if (tipo_consegna === "RITIRO") {
                        comanda.tipo_consegna = "RITIRO";
                        $('.tipo_consegna').removeClass('tipo_consegna_selezionata');
                        $('.consegna_gap_15').removeClass('tipo_consegna_selezionata_gap');
                        $('.consegna_gap_30').removeClass('tipo_consegna_selezionata_gap');
                        $('.consegna_ritiro').addClass('tipo_consegna_selezionata');
                        $('#popup_scelta_cliente input[name="numeretto"]').val(progressivo_asporto);
                    } else if (tipo_consegna === "DOMICILIO") {
                        comanda.tipo_consegna = "DOMICILIO";
                        $('.tipo_consegna').removeClass('tipo_consegna_selezionata');
                        $('.consegna_gap_15').removeClass('tipo_consegna_selezionata_gap');
                        $('.consegna_gap_30').removeClass('tipo_consegna_selezionata_gap');
                        $('.consegna_domicilio').addClass('tipo_consegna_selezionata');
                        $('#popup_scelta_cliente input[name="numeretto"]').val(progressivo_asporto);
                    } else {
                        comanda.tipo_consegna = "PIZZERIA";
                        $('.tipo_consegna').removeClass('tipo_consegna_selezionata');
                        $('.consegna_gap_15').removeClass('tipo_consegna_selezionata_gap');
                        $('.consegna_gap_30').removeClass('tipo_consegna_selezionata_gap');
                        $('.consegna_pizzeria').addClass('tipo_consegna_selezionata');
                        $('#popup_scelta_cliente input[name="numeretto"]').val(progressivo_asporto);
                    }
                }

                if (comanda.dati_temporanei_cliente !== undefined && comanda.dati_temporanei_cliente.jolly !== undefined && comanda.dati_temporanei_cliente.jolly === "SUBITO") {

                    comanda.esecuzione_comanda_immediata = true;
                    var data_js = new Date();
                    var ora_adesso = addZero(data_js.getHours(), 2) + ':' + addZero(data_js.getMinutes(), 2);
                    $('#popup_scelta_cliente input[name="ora_consegna"]:visible').val(ora_adesso);
                    $("[name='consegna_input_gap']").val(comanda.consegna_gap);
                   // comanda.consegna_gap = consegna_gap;
                    comanda.ultima_ora_consegna = ora_adesso;
                    if (comanda.tipo_consegna === "PIZZERIA") {
                        comanda.tipo_consegna = "PIZZERIA";
                    } else {
                        comanda.tipo_consegna = 'RITIRO';
                    }
                    $('.tipo_consegna').removeClass('tipo_consegna_selezionata');
                    if(comanda.consegna_gap==="15")
                    {     $('.consegna_gap_15').addClass('tipo_consegna_selezionata_gap');

                    }
                    if(comanda.consegna_gap==="30")
                    { $('.consegna_gap_30').addClass('tipo_consegna_selezionata_gap');

                    }
               
                   
                    $('.consegna_ritiro').addClass('tipo_consegna_selezionata');
                   
                    $('#switch_esecuzione_comanda_immediata').css('background-image', '-webkit-linear-gradient(top, grey,#4d4d4d)');
                } else {
                    comanda.esecuzione_comanda_immediata = false;
                    $('#switch_esecuzione_comanda_immediata').css('background-image', '');
                }


            } else {
                comanda.tipo_consegna = "NIENTE";
                $('.tipo_consegna').removeClass('tipo_consegna_selezionata');
                $('.consegna_gap_15').removeClass('tipo_consegna_selezionata_gap');
                $('.consegna_gap_30').removeClass('tipo_consegna_selezionata_gap');
                $('#popup_scelta_cliente input[name="numeretto"]').val(progressivo_asporto);
                if (visualizza_popup !== false) {
                    $('#popup_scelta_cliente').modal('show');
                    $('#popup_scelta_cliente_elenco_clienti').scrollTop(0);
                }
            }

        } else {
            comanda.tipo_consegna = "NIENTE";
            $('.tipo_consegna').removeClass('tipo_consegna_selezionata');
            $('.consegna_gap_15').removeClass('tipo_consegna_selezionata_gap');
            $('.consegna_gap_30').removeClass('tipo_consegna_selezionata_gap');
            $('#popup_scelta_cliente input[name="numeretto"]').val(progressivo_asporto);
            if (visualizza_popup !== false) {
                $('#popup_scelta_cliente').modal('show');
                $('#popup_scelta_cliente_elenco_clienti').scrollTop(0);
            }
        }

        let scritta_consegna_ordine = comanda.tipo_consegna;

        switch (comanda.tipo_consegna) {
            case 'DOMICILIO':
                if (scritta_consegna_ordine.length > 0) {
                    scritta_consegna_ordine = 'DOMICILIO';
                }

                break;

            case 'RITIRO':
                if (scritta_consegna_ordine.length > 0) {
                    scritta_consegna_ordine = comanda.scritta_consegna_ritiro;
                }

                break;

            case 'PIZZERIA':
                if (scritta_consegna_ordine.length > 0) {
                    scritta_consegna_ordine = comanda.scritta_consegna_pizzeria;
                }
                break;

        }
        if (scritta_consegna_ordine === "NIENTE") {
            $('#tipo_consegna_modal_tipo').html("" + " ");
            $('#tipo_consegna_modal').html("");
            $('#tipo_consegna_modal_gap_scritta').html("" + " ");
            $('#tipo_consegna_gap_modal').html("");
        } else {
            var query_gap_consegna = "select time_gap_consegna from impostazioni_fiscali where id=" + comanda.folder_number + ";"
            var result = alasql(query_gap_consegna);
            comanda.consegna_gap = consegna_gap;
            if (result[0].time_gap_consegna == "1" && consegna_gap != "") {
                var minuti_somma = ora.substring(ora.indexOf(':') + 1)
                var int_miunti = parseInt(minuti_somma);
                var int_gap_miunti = parseInt(consegna_gap);
                var min_somma = int_miunti + int_gap_miunti;
                var ora_gap = ora.substring(0, ora.indexOf(':'))
                var hours = Math.floor(min_somma / 60);
                var minutes = min_somma % 60;
                var ore = parseInt(ora_gap);

                if (minutes < 10) {
                    if (ore < 10) {
                        var oraa = ore + hours
                        if(oraa===10)
                        { var totale_ora = oraa + ":" + minutes;

                        }
                        else{
                            var totale_ora = "0" + oraa + ":" + minutes;
                        }
                    }
                    else {
                        var totale_ora = ore + hours + ":" + "0" + minutes;

                    }

                }
                else {
                    if (ore < 10) {
                        var oraa = ore + hours
                        if(oraa===10)
                        { var totale_ora = oraa + ":" + minutes;

                        }
                        else{
                            var totale_ora = "0" + oraa + ":"+ "0" + minutes;
                        }
                    }
                    else {

                        var totale_ora = ore + hours + ":" + minutes;
                    }
                }



                $('#tipo_consegna_modal_tipo').html(scritta_consegna_ordine + ": ");
                $('#tipo_consegna_modal').html(ora);
                $('#tipo_consegna_modal_gap_scritta').html("Gap:");
                $('#tipo_consegna_gap_modal').html(totale_ora);

            }
            else {
                $('#tipo_consegna_modal_tipo').html(scritta_consegna_ordine + ": ");
                $('#tipo_consegna_modal').html(ora);

            }
        }
    }
}

function consegna_gap_classe_ativo(id) {
    var testo_clicato_15 = comanda.testo_consegna_15;
    var testo_clicato_30 = comanda.testo_consegna_30;
   if($('.consegna_gap_15').hasClass('tipo_consegna_selezionata_gap')===false&& comanda.consegna_gap==="15")
   {
    comanda.testo_consegna_15= true;
    $('.consegna_gap_15').addClass('tipo_consegna_selezionata_gap');
    $('#consegna_input_gap').val("15");

   }
   else if($('.consegna_gap_30').hasClass('tipo_consegna_selezionata_gap')===false && comanda.consegna_gap==="30")
   {comanda.testo_consegna_30= true;
       $('.consegna_gap_30').addClass('tipo_consegna_selezionata_gap');
   $('#consegna_input_gap').val("30");

   }
    if( testo_clicato_15===true && id==="15")
    {  
        comanda.testo_consegna_15= false;
        comanda.testo_consegna_30= false;
     $('.consegna_gap_15').removeClass('tipo_consegna_selezionata_gap');
        $('#consegna_input_gap').val("");
    }
    else if(testo_clicato_30===true && id==="30"){
        comanda.testo_consegna_30 =false;
        comanda.testo_consegna_15 =false;
        $('.consegna_gap_30').removeClass('tipo_consegna_selezionata_gap');
        $('#consegna_input_gap').val("");
    }
    
            
    
    
    
    
    
}

function salva_cliente_asporto() {

    let testo_query = "";
    var tipo_fattorino = $("select#tipologia_fattorino option:selected").val();
    if (tipo_fattorino !== "0") {

        let box = bootbox.prompt({

            title: 'Inserisci Prezzo di Consegna',
            placeholder: 'Es. 10.50 (ATTENZIONE: NON USARE LA VIRGOLA, MA USA IL PUNTO"',
            className: 'only-number',
            inputType: 'text',
            buttons: {
                confirm: {
                    label: 'OK'
                }
            },
            callback: function (value) {

                comanda.consegna_fattorino_extra = value;
                funzione_interna();
                comanda.funzionidb.conto_attivo();
            }

        });
        box.on("shown.bs.modal", function () {
            $(".bootbox-input-text:visible").addClass("kb_num");
        });
    } else {

        comanda.consegna_fattorino_extra = "0.00";


        funzione_interna();
    }

    function funzione_interna() {
        $('#atp_testo').val("");
        var nome = $('#popup_scelta_cliente input[name="nome"]').val().toUpperCase().replace(/'/g, '');
        var cognome = $('#popup_scelta_cliente input[name="cognome"]').val().toUpperCase().replace(/'/g, '');
        var ragione_sociale = $('#popup_scelta_cliente input[name="ragione_sociale"]').val().toUpperCase().replace(/'/g, '');
        var indirizzo = $('#popup_scelta_cliente input[name="indirizzo"]').val().replace(/'/g, '').toUpperCase();
        var numero = $('#popup_scelta_cliente input[name="numero"]').val().toUpperCase();
        var cap = $('#popup_scelta_cliente input[name="cap"]').val();
        var provincia = $('#popup_scelta_cliente input[name="provincia"]').val().toUpperCase();

        var email = $('#popup_scelta_cliente input[name="email"]').val().toUpperCase();


        var cellulare = $('#popup_scelta_cliente input[name="cellulare"]').val();
        var telefono = $('#popup_scelta_cliente input[name="telefono"]').val();
        var telefono_3 = $('#popup_scelta_cliente input[name="telefono_3"]').val();
        if ($('#popup_scelta_cliente input[name="telefono_4"]').val() === undefined) {
            var telefono_4 = "";
        } else {
            var telefono_4 = $('#popup_scelta_cliente input[name="telefono_4"]').val();
        }
        if ($('#popup_scelta_cliente input[name="telefono_5"]').val() === undefined) {
            var telefono_5 = "";
        } else {
            var telefono_5 = $('#popup_scelta_cliente input[name="telefono_5"]').val();
        }
        if ($('#popup_scelta_cliente input[name="telefono_6"]').val() === undefined) {
            var telefono_6 = "";
        } else {
            var telefono_6 = $('#popup_scelta_cliente input[name="telefono_6"]').val();
        }



        //var fax = $('#popup_scelta_cliente input[name="fax"]').val();

        var note = $('#popup_scelta_cliente input[name="note"]').val().toUpperCase();
        var codice_lotteria = $('#popup_scelta_cliente input[name="codice_lotteria"]').val().toUpperCase();
        fa_set_codice_lotteria(codice_lotteria);
        var codice_fiscale = $('#popup_scelta_cliente input[name="codice_fiscale"]').val().toUpperCase();
        var partita_iva = $('#popup_scelta_cliente input[name="partita_iva"]').val();
        var giorno_nascita = $('#popup_scelta_cliente [name="giorno_nascita"] option:selected').val();
        var mese_nascita = $('#popup_scelta_cliente [name="mese_nascita"] option:selected').val();
        var anno_nascita = $('#popup_scelta_cliente [name="anno_nascita"] option:selected').val();
        var numeretto_asp = $('#popup_scelta_cliente input[name="numeretto"]').val();
        var ora_consegna = $('#popup_scelta_cliente input[name="ora_consegna"]').val();
        var orario_preparazione = $('#popup_scelta_cliente input[name="orario_preparazione"]').val();
        var formato_trasmissione = $('#popup_scelta_cliente [name="formato_trasmissione"]').prop("checked");
        var tipo_cliente = $('[name="azienda_privato"]:checked').attr("id");
        var codice_destinatario = $('#popup_scelta_cliente [name="codice_destinatario"]').val().toUpperCase();
        /* non serve metterlo obbligatoriamente. Se non c'è diventa privato con 7 zeri*/
        var regime_fiscale = $('#popup_scelta_cliente [name="regime_fiscale"]').val();
        var comune = $('#popup_scelta_cliente [name="comune"]').val().toUpperCase();
        var nazione = $('#popup_scelta_cliente [name="nazione"]').val().toUpperCase();
        var partita_iva_estera = $('#popup_scelta_cliente [name="partita_iva_estera"]').val();
        var pec = $('#popup_scelta_cliente [name="pec"]').val();
        var codice_tessera = $('#popup_scelta_cliente input[name="codice_tessera"]').val().toUpperCase();
        var tipo_consegna = comanda.tipo_consegna;
        var consegna_gap = $("[name='consegna_input_gap']").val();
        comanda.consegna_gap = consegna_gap;

        let scritta_consegna_ordine = '';
        switch (comanda.tipo_consegna) {
            case 'DOMICILIO':
                scritta_consegna_ordine = comanda.scritta_consegna_domicilio;
                if (scritta_consegna_ordine == "") {
                    scritta_consegna_ordine = 'DOMICILIO';

                }
                break;

            case 'RITIRO':
                scritta_consegna_ordine = comanda.scritta_consegna_ritiro;
                break;

            case 'PIZZERIA':
                scritta_consegna_ordine = comanda.scritta_consegna_pizzeria;
                break;

        }
        //nome tendina
        var tendina = $('#popup_scelta_cliente_elenco_clienti option:selected').val();
        if (ora_consegna.length === 0) {

            /* var cliente_sul_posto = confirm("Se il cliente e' venuto personalmente a ritirare la pizza, clicca OK!");
             
             if (cliente_sul_posto === true) {*/

            var data_js = new Date();
            var ora_adesso = addZero(data_js.getHours(), 2) + ':' + addZero(data_js.getMinutes(), 2);
            ora_consegna = ora_adesso;

            var query_gap_consegna = "select time_gap_consegna from impostazioni_fiscali where id=" + comanda.folder_number + ";"
            var result = alasql(query_gap_consegna);
            if (result[0].time_gap_consegna == "1" && comanda.consegna_gap != "" && comanda.consegna_gap != "undefined") {
                var minuti_somma = ora_consegna.substring(ora_consegna.indexOf(':') + 1)
                var int_miunti = parseInt(minuti_somma);
                var int_gap_miunti = parseInt(comanda.consegna_gap);;
                var min_somma = int_miunti + int_gap_miunti;
                var ora_gap = ora_consegna.substring(0, ora_consegna.indexOf(':'))
                var hours = Math.floor(min_somma / 60);
                var minutes = min_somma % 60;
                var ora = parseInt(ora_gap);

                if (minutes < 10) {
                    if (ora < 10) {
                        var oraa = ora + hours
                        if(oraa===10)
                        { var totale_ora = oraa + ":" + minutes;

                        }
                        else{
                            var totale_ora = "0" + oraa + ":"+ "0" + minutes;
                        }
                    }
                    else {
                        var totale_ora = ora + hours + ":" + "0" + minutes;

                    }

                }
                else {
                    if (ora < 10) {
                        var oraa = ora + hours
                        if(oraa===10)
                        { var totale_ora = oraa + ":" + minutes;

                        }
                        else{
                            var totale_ora = "0" + oraa + ":" + minutes;
                        }
                    }
                    else {

                        var totale_ora = ora + hours + ":" + minutes;
                    }
                }


                // comanda.ora_consegna_gap = totale_ora;

                $('#tipo_consegna_modal_tipo').html(scritta_consegna_ordine + ": ");
                $('#tipo_consegna_modal').html(ora_consegna);
                $('#tipo_consegna_modal_gap_scritta').html("Gap:");
                $('#tipo_consegna_gap_modal').html(totale_ora);

            }
            else {
                $('#tipo_consegna_modal_tipo').html(scritta_consegna_ordine + ": ");
                $('#tipo_consegna_modal').html(ora_consegna);

            }

            $('#popup_scelta_cliente input[name="ora_consegna"]:visible').val(ora_adesso);
            $("[name = 'consegna_inpuit_gap']").val(comanda.consegna_gap);
            comanda.ultima_ora_consegna = ora_adesso;
            //}
        }



        if (ora_consegna.length > 0) {

            var query_gap_consegna = "select time_gap_consegna from impostazioni_fiscali where id=" + comanda.folder_number + ";"
            var result = alasql(query_gap_consegna);
            if (result[0].time_gap_consegna == "1" && comanda.consegna_gap != "" && comanda.consegna_gap != "undefined") {
                var minuti_somma = ora_consegna.substring(ora_consegna.indexOf(':') + 1)
                var int_miunti = parseInt(minuti_somma);
                var int_gap_miunti = parseInt(comanda.consegna_gap);;
                var min_somma = int_miunti + int_gap_miunti;
                var ora_gap = ora_consegna.substring(0, ora_consegna.indexOf(':'))
                var hours = Math.floor(min_somma / 60);
                var minutes = min_somma % 60;
                var ora = parseInt(ora_gap);

                if (minutes < 10) {
                    if (ora < 10) {
                        var oraa = ora + hours
                        if(oraa===10)
                        { var totale_ora = oraa + ":" + minutes;

                        }
                        else{
                            var totale_ora = "0" + oraa + ":" + minutes;
                        }
                    }
                    else {
                        var totale_ora = ora + hours + ":" + "0" + minutes;

                    }

                }
                else {
                    if (ora < 10) {
                        var oraa = ora + hours
                        if(oraa===10)
                        { var totale_ora = oraa + ":" + minutes;

                        }
                        else{
                            var totale_ora = "0" + oraa + ":" + minutes;
                        }
                       
                    }
                    else {

                        var totale_ora = ora + hours + ":" + minutes;
                    }
                }

                comanda.ora_consegna_gap = totale_ora;

                $('#tipo_consegna_modal_tipo').html(scritta_consegna_ordine + ": ");
                $('#tipo_consegna_modal').html(ora_consegna);
                $('#tipo_consegna_modal_gap_scritta').html("Gap:");
                $('#tipo_consegna_gap_modal').html(totale_ora);

            }
            else {
                $('#tipo_consegna_modal_tipo').html(scritta_consegna_ordine + ": ");
                $('#tipo_consegna_modal').html(ora_consegna);
                $('#tipo_consegna_modal_gap_scritta').html("");
                $('#tipo_consegna_gap_modal').html("");

            }


            var query = "select * from settaggi_profili where id=" + comanda.folder_number + " ;";
            comanda.sincro.query(query, function (settaggi_profili) {
                settaggi_profili = settaggi_profili[0];
                if ((comanda.tipo_consegna.length > 0 && comanda.tipo_consegna !== 'NIENTE') || settaggi_profili.Field182 !== 'true') {

                    /* Commentato il 21 agosto 2019 ... speriamo bene, perchè non so per quale motivo sia stato messo */
                    /*if (settaggi_profili.Field182 !== 'true') {
                     comanda.tipo_consegna = "RITIRO";
                     tipo_consegna = "RITIRO";
                     }*/

                    if ((tipo_consegna === "RITIRO" || tipo_consegna === "PIZZERIA") && nome.length === 0 && cognome.length === 0 && ragione_sociale.length === 0 && ora_consegna.length > 0) {

                        comanda.dati_temporanei_cliente = new Object();
                        comanda.dati_temporanei_cliente.ora_consegna = ora_consegna;
                        comanda.dati_temporanei_cliente.tipo_consegna = tipo_consegna;
                        comanda.dati_temporanei_cliente.tipologia_fattorino = $("select#tipologia_fattorino").children(":selected").val();
                        comanda.dati_temporanei_cliente.orario_preparazione = orario_preparazione;
                        comanda.dati_temporanei_cliente.codice_lotteria = codice_lotteria;
                        if (comanda.esecuzione_comanda_immediata === true) {
                            comanda.dati_temporanei_cliente.jolly = "SUBITO";
                        } else {
                            comanda.dati_temporanei_cliente.jolly = "";
                        }


                        comanda.parcheggio = 'Prog: ' + numeretto_asp + "-" + aggZero(comanda.centro, 3) + "-" + aggZero(comanda.terminale, 2);
                        comanda.ultimo_id_cliente = 'P' + numeretto_asp + "-" + aggZero(comanda.centro, 3) + "-" + aggZero(comanda.terminale, 2);
                        testo_query = "update comanda set nome_comanda='" + comanda.parcheggio + "' where ntav_comanda='" + comanda.tavolo + "' and stato_record='ATTIVO';";
                        alasql(testo_query);
                        comanda.sock.send({
                            tipo: "aggiornamento_tavolo",
                            operatore: comanda.operatore,
                            query: testo_query,
                            terminale: comanda.terminale,
                            ip: comanda.ip_address
                        });
                        $('.nome_parcheggio').html(comanda.parcheggio);
                        $('#popup_scelta_cliente').modal('hide');
                    } else
                        /* modificato il 16 11 2020 perchè Canton voleva solo cognome obbligatorio
                         * l'originale e questo:
                         * if ((tendina === undefined || tendina === "") && ((tipo_consegna === "PIZZERIA" && (ragione_sociale.length > 0 || nome.length > 0)) || (tipo_consegna === "DOMICILIO" && indirizzo.length > 0 && (ragione_sociale.length > 0 || (nome.length > 0 && cognome.length > 0)))))*/

                        if ((tendina === undefined || tendina === "") && ((tipo_consegna === "PIZZERIA" && indirizzo.length > 0 && (ragione_sociale.length > 0 || (nome.length > 0 || cognome.length > 0)) /*&& (ragione_sociale.length > 0 || nome.length > 0 || cognome.length > 0)*/) || (tipo_consegna === "DOMICILIO" && indirizzo.length > 0 && (ragione_sociale.length > 0 || (nome.length > 0 || cognome.length > 0))) || (tipo_consegna === "RITIRO" && indirizzo.length > 0 && (ragione_sociale.length > 0 || (nome.length > 0 || cognome.length > 0))))) {


                            testo_query = "SELECT id  FROM clienti WHERE id<3000000 ORDER BY cast(id as int)  DESC LIMIT 1;";
                            let result = alasql(testo_query);
                            var id = '1';
                            if (result !== undefined && result[0] !== undefined && result[0].id !== undefined) {
                                id = parseInt(result[0].id) + 1;
                                id = id.toString();
                            }


                            testo_query = "INSERT INTO clienti (email,tipo_cliente,codice_lotteria,partita_iva_estera,formato_trasmissione,codice_destinatario,regime_fiscale,pec,comune,id_nazione,nazione,id,nome,cognome,ragione_sociale,indirizzo,numero,cap,provincia,cellulare,telefono,telefono_3,telefono_4,telefono_5,telefono_6,note,codice_fiscale,partita_iva,codice_tessera) VALUES ('" + email + "','" + tipo_cliente + "','" + codice_lotteria + "','" + partita_iva_estera + "','" + formato_trasmissione + "','" + codice_destinatario + "','" + regime_fiscale + "','" + pec + "','" + comune + "','" + nazione + "','" + nazione + "'," + id + ",'" + nome + "','" + cognome + "','" + ragione_sociale + "','" + indirizzo + "','" + numero + "','" + cap + "','" + provincia + "','" + cellulare + "','" + telefono + "','" + telefono_3 + "','" + telefono_4 + "','" + telefono_5 + "','" + telefono_6 + "','" + note + "','" + codice_fiscale + "','" + partita_iva + "','" + codice_tessera + "')";
                            console.log("SALVATAGGIO CLIENTE", testo_query);
                            alasql(testo_query);
                            comanda.sock.send({
                                tipo: "aggiornamento_clienti",
                                operatore: comanda.operatore,
                                query: testo_query,
                                terminale: comanda.terminale,
                                ip: comanda.ip_address
                            });
                            comanda.dati_temporanei_cliente = new Object();
                            comanda.dati_temporanei_cliente.ora_consegna = ora_consegna;
                            comanda.dati_temporanei_cliente.tipo_consegna = tipo_consegna;
                            comanda.dati_temporanei_cliente.tipologia_fattorino = $("select#tipologia_fattorino").children(":selected").val();
                            comanda.dati_temporanei_cliente.orario_preparazione = orario_preparazione;
                            comanda.dati_temporanei_cliente.codice_lotteria = codice_lotteria;
                            if (comanda.esecuzione_comanda_immediata === true) {
                                comanda.dati_temporanei_cliente.jolly = "SUBITO";
                            } else {
                                comanda.dati_temporanei_cliente.jolly = "";
                            }

                            comanda.ultimo_id_cliente = id;
                            //comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                            comanda.sincro.query_cassa();
                            console.log("ULTIMO ID CLIENTE", comanda.ultimo_id_cliente);
                            comanda.parcheggio = nome + " " + cognome + " " + ragione_sociale;
                            if (ragione_sociale.length >= 3) {
                                comanda.parcheggio = ragione_sociale;
                            } else {
                                comanda.parcheggio = nome + " " + cognome;
                            }

                            testo_query = "update comanda set nome_comanda='" + comanda.parcheggio + "' where ntav_comanda='" + comanda.tavolo + "' and  stato_record='ATTIVO';";
                            alasql(testo_query);
                            comanda.sock.send({
                                tipo: "aggiornamento_tavolo",
                                operatore: comanda.operatore,
                                query: testo_query,
                                terminale: comanda.terminale,
                                ip: comanda.ip_address
                            });
                            $('.nome_parcheggio').html(comanda.parcheggio);
                            $('#popup_scelta_cliente').modal('hide');
                            //MODIFICATO. PRIMA ERA tendina !== undefined && tendina !== "", ma essendo diverso da "" poteva anche essere undefined 
                            //e così fa la query con WHERE id=undefined e si impalla tutto

                            /*tolto indirizzo il 17-12-2020 nel senso che anche se manca l indirizzo in pizzeria comunque deve aggiornare i dati del cliente, ad esempio */
                        } else if (tendina !== undefined && tendina !== "" /*&& indirizzo.length > 0*/ && (nome.length > 0 || cognome.length > 0)) {

                            testo_query = "UPDATE clienti SET email='" + email + "',tipo_cliente='" + tipo_cliente + "',codice_lotteria='" + codice_lotteria + "',partita_iva_estera='" + partita_iva_estera + "',formato_trasmissione='" + formato_trasmissione + "',codice_destinatario='" + codice_destinatario + "',regime_fiscale='" + regime_fiscale + "',pec='" + pec + "',comune='" + comune + "',id_nazione='" + nazione + "',nazione='" + nazione + "',nome='" + nome + "',cognome='" + cognome + "',ragione_sociale='" + ragione_sociale + "',indirizzo='" + indirizzo + "',numero='" + numero + "',cap='" + cap + "',provincia='" + provincia + "',cellulare='" + cellulare + "',telefono='" + telefono + "',telefono_3='" + telefono_3 + "',telefono_4='" + telefono_4 + "',telefono_5='" + telefono_5 + "',telefono_6='" + telefono_6 + "',note='" + note + "',codice_fiscale='" + codice_fiscale + "',partita_iva='" + partita_iva + "',codice_tessera='" + codice_tessera + "' WHERE id=" + tendina + ";";
                            console.log("MODIFICA UTENTE", testo_query);
                            alasql(testo_query);
                            comanda.sock.send({
                                tipo: "aggiornamento",
                                operatore: comanda.operatore,
                                query: testo_query,
                                terminale: comanda.terminale,
                                ip: comanda.ip_address
                            });
                            comanda.sincro.query_cassa();
                            comanda.parcheggio = nome + " " + cognome + " " + ragione_sociale;
                            if (ragione_sociale.length >= 3) {
                                comanda.parcheggio = ragione_sociale;
                            } else {
                                comanda.parcheggio = nome + " " + cognome;
                            }

                            testo_query = "update comanda set nome_comanda='" + comanda.parcheggio + "' where ntav_comanda='" + comanda.tavolo + "' and stato_record='ATTIVO';";
                            alasql(testo_query);
                            comanda.sock.send({
                                tipo: "aggiornamento",
                                operatore: comanda.operatore,
                                query: testo_query,
                                terminale: comanda.terminale,
                                ip: comanda.ip_address
                            });
                            $('.nome_parcheggio').html(comanda.parcheggio);
                            $('#popup_scelta_cliente').modal('hide');
                            comanda.dati_temporanei_cliente = new Object();
                            comanda.dati_temporanei_cliente.ora_consegna = ora_consegna;
                            comanda.dati_temporanei_cliente.tipo_consegna = tipo_consegna;
                            comanda.dati_temporanei_cliente.tipologia_fattorino = $("select#tipologia_fattorino").children(":selected").val();
                            comanda.dati_temporanei_cliente.orario_preparazione = orario_preparazione;
                            comanda.dati_temporanei_cliente.codice_lotteria = codice_lotteria;
                            if (comanda.esecuzione_comanda_immediata === true) {
                                comanda.dati_temporanei_cliente.jolly = "SUBITO";
                            } else {
                                comanda.dati_temporanei_cliente.jolly = "";
                            }

                            comanda.ultimo_id_cliente = tendina;
                        } else if ((tipo_consegna === "RITIRO" || tipo_consegna === "PIZZERIA") && (nome.length > 0 || cognome.length > 0)) {
                            comanda.parcheggio = (nome + " " + cognome + " " + ragione_sociale).trim();
                            if (ragione_sociale.length >= 3) {
                                comanda.parcheggio = (ragione_sociale).trim();
                            } else {
                                comanda.parcheggio = (nome + " " + cognome).trim();
                            }

                            comanda.dati_temporanei_cliente = new Object();
                            comanda.dati_temporanei_cliente.ora_consegna = ora_consegna;
                            comanda.dati_temporanei_cliente.tipo_consegna = tipo_consegna;
                            comanda.dati_temporanei_cliente.tipologia_fattorino = $("select#tipologia_fattorino").children(":selected").val();
                            comanda.dati_temporanei_cliente.orario_preparazione = orario_preparazione;
                            comanda.dati_temporanei_cliente.codice_lotteria = codice_lotteria;
                            if (comanda.esecuzione_comanda_immediata === true) {
                                comanda.dati_temporanei_cliente.jolly = "SUBITO";
                            } else {
                                comanda.dati_temporanei_cliente.jolly = "";
                            }

                            comanda.ultimo_id_cliente = (nome + " " + cognome + " " + ragione_sociale).trim();
                            testo_query = "update comanda set nome_comanda='" + comanda.parcheggio + "' where ntav_comanda='" + comanda.tavolo + "' and stato_record='ATTIVO';";
                            alasql(testo_query);
                            comanda.sock.send({
                                tipo: "aggiornamento",
                                operatore: comanda.operatore,
                                query: testo_query,
                                terminale: comanda.terminale,
                                ip: comanda.ip_address
                            });
                            $('.nome_parcheggio').html(comanda.parcheggio);
                            $('#popup_scelta_cliente').modal('hide');
                        } else {
                            intesta_cliente();
                        }
                } else {
                    intesta_cliente();
                }
            });
        } else {
            intesta_cliente();
        }
    }
}


comanda.ultima_ora_consegna = "";

function inforna(conto_finale, scontrino) {



    let id_comanda = id_comanda_attuale_alasql();

    let tavolo = comanda.tavolo;



    comanda.ultima_comanda_forno_aperta_stampata = false;
    let r = alasql("select stato_record from comanda where stato_record ='ATTIVO' and id='" + id_comanda + "' limit 1;");
    if (r.length === 0) {

        let c = alasql("select desc_art,stato_record from comanda where stampata_sn='S' and stato_record LIKE '%CANCELLATO%' and id='" + comanda.ultimo_id + "' and nome_comanda='" + comanda.parcheggio + "' limit 1;");
        if (c.length > 0) {

            var data = comanda.funzionidb.data_attuale();
            var builder = new epson.ePOSBuilder();
            builder.addTextFont(builder.FONT_A);
            builder.addTextAlign(builder.ALIGN_CENTER);
            builder.addTextSize(2, 2);
            //OVVERO SE L'ARTICOLO E' UNA VARIANTE
            builder.addFeedLine(1);
            builder.addFeedLine(1);
            builder.addText(comanda.parcheggio + '\n');
            builder.addFeedLine(1);
            builder.addText('CONSEGNA: ' + comanda.ora_consegna + ':' + comanda.minuti_consegna + '\n');
            builder.addFeedLine(1);
            builder.addTextSize(1, 1);
            builder.addText("-----------------------------------------\n");
            builder.addTextSize(2, 2);
            builder.addTextStyle(true, false, true, builder.COLOR_1);
            builder.addText('ORDINE CANCELLATO\n');
            builder.addTextStyle(false, false, true, builder.COLOR_1);
            builder.addTextSize(1, 1);
            builder.addText("-----------------------------------------\n");
            builder.addTextSize(2, 2);
            builder.addFeedLine(1);
            builder.addText(comanda.tipo_consegna + '\n');
            if (comanda.numero_licenza_cliente === "0629") {
                builder.addFeedLine(1);
                let testo_query = "select * from elenco_progressivi_asporto_domicilio where progressivo_comanda='" + comanda.ultimo_id + "' limit 1;";
                let progressivo_attuale = alasql(testo_query);
                if (progressivo_attuale.length > 0) {

                    builder.addText("Num. Consegna: " + progressivo_attuale[0].numero + '\n');
                }
            }

            builder.addFeedLine(1);
            builder.addTextSize(1, 1);
            builder.addTextAlign(builder.ALIGN_LEFT);
            builder.addText('Cancellato alle ' + data.replace(/-/gi, '/').substr(9) + '\n');
            builder.addFeedLine(1);
            builder.addFeedLine(1);
            builder.addCut(builder.CUT_FEED);
            var request = builder.toString();
            //CONTENUTO CONTO
            //console.log(request);

            //Create a SOAP envelop
            var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
            //Create an XMLHttpRequest object
            var xhr = new XMLHttpRequest();
            //Set the end point address

            let d = alasql("select distinct dest_stampa from comanda where id='" + comanda.ultimo_id + "';");
            let url = "";
            d.forEach(v => {
                url = 'http://' + comanda.nome_stampante[v.dest_stampa].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                if (comanda.nome_stampante[v.dest_stampa].intelligent === "SDS") {
                    url = 'http://' + comanda.nome_stampante[v.dest_stampa].ip + ':8000/SPOOLER?stampante=' + comanda.nome_stampante[v.dest_stampa].devid_nf;
                }

                //Open an XMLHttpRequest object
                xhr.open('POST', url, true);
                //<Header settings>
                xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
                xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
                xhr.setRequestHeader('SOAPAction', '""');
                // Send print document
                xhr.send(soap);
            });
        }
    }




    //quando apri un ordine salvato elimina i resti precedenti
    $('[name=\'soldi_ricevuti\']').val("");
    $('[name=\'soldi_resto\']').val("");
    if (pizza_due_gusti === true) {

        $('.pizza_due_gusti').removeClass('cat_accesa');

        pizza_duegusti();
    }
    /*if (boolean_schiacciata === true) {
        $('.tasto_schiacciata').removeClass('cat_accesa');
        boolean_schiacciata();
    }*/

    query_consegna(function () {

        console.log("** LOG ORDINE", "MEMO");
        comanda.popup_modifica_articolo = false;
        console.log("ULTIMO ID CLIENTE", comanda.ultimo_id_cliente);
        var id_cliente = $('#popup_scelta_cliente_elenco_clienti').val();
        if (id_cliente !== undefined && (id_cliente === 'null' || id_cliente === null || id_cliente.length === 0)) {
            id_cliente = comanda.ultimo_id_cliente;
        }

        var nome = $('#popup_scelta_cliente input[name="nome"]').val().toUpperCase().replace(/'/g, '');
        var cognome = $('#popup_scelta_cliente input[name="cognome"]').val().toUpperCase().replace(/'/g, '');
        var ragione_sociale = $('#popup_scelta_cliente input[name="ragione_sociale"]').val().toUpperCase().replace(/'/g, '');
        var numeretto = $('#popup_scelta_cliente input[name="numeretto"]').val();
        var ora_consegna = $('#popup_scelta_cliente input[name="ora_consegna"]').val();
        var orario_preparazione = $('#popup_scelta_cliente input[name="orario_preparazione"]').val();
        var tipologia_consegna = comanda.tipo_consegna;
        var tipologia_fattorino = $("select#tipologia_fattorino").children(":selected").val();
        var codice_lotteria = $("[name='codice_lotteria']").val();
        comanda.consegna_gap = $("[name='consegna_input_gap']").val();
        var query_gap_consegna = "select time_gap_consegna from impostazioni_fiscali where id=" + comanda.folder_number + ";"
        var result = alasql(query_gap_consegna);
        if (result[0].time_gap_consegna == "1" && comanda.consegna_gap != "" && comanda.consegna_gap != "undefined") {
            var minuti_somma = ora_consegna.substring(ora_consegna.indexOf(':') + 1)
            var int_miunti = parseInt(minuti_somma);
            var int_gap_miunti = parseInt(comanda.consegna_gap);;
            var min_somma = int_miunti + int_gap_miunti;
            var ora_gap = ora_consegna.substring(0, ora_consegna.indexOf(':'))
            var hours = Math.floor(min_somma / 60);
            var minutes = min_somma % 60;
            var ora = parseInt(ora_gap);

            if (minutes < 10) {
                if (ora < 10) {
                    var oraa = ora + hours
                    var totale_ora = "0" + oraa + ":" + "0" + minutes;
                }
                else {
                    var totale_ora = ora + hours + ":" + "0" + minutes;

                }

            }
            else {
                if (ora < 10) {
                    var oraa = ora + hours
                    var totale_ora = "0" + oraa + ":" + minutes;
                }
                else {

                    var totale_ora = ora + hours + ":" + minutes;
                }
            }

        }
        fa_set_codice_lotteria(codice_lotteria);
        if ($('.nome_parcheggio').html().length > 0 || comanda.parcheggio.length > 0) {
            if ($('#popup_scelta_cliente input[name="ora_consegna"]').val().length > 0) {


                //let differenza = calcolo_differenza_consegna();
                //bootbox.alert("Differenza: " + differenza);


                if (comanda.tipo_consegna.length > 0 && comanda.tipo_consegna !== 'NIENTE') {

                    //SE IL CLIENTE E' UN PROGRESSIVO ALLORA AUMENTA ALTRIMENTI RESTA FERMO
                    if (id_cliente !== undefined && typeof id_cliente === 'string' && id_cliente[0] === 'P' && comanda.blocca_progressivo !== true) {
                        var query_progressivo = 'UPDATE progressivo_asporto SET numero=cast(numero as int)+1;';
                        comanda.sock.send({
                            tipo: "aggiornamento",
                            operatore: comanda.operatore,
                            query: query_progressivo,
                            terminale: comanda.terminale,
                            ip: comanda.ip_address
                        });
                        comanda.sincro.query(query_progressivo, function () { });
                    }

                    comanda.blocca_progressivo = false;
                    var subito_sn = '';
                    if (comanda.esecuzione_comanda_immediata === true) {
                        subito_sn = "SUBITO";
                    }

                    var nome_comanda = nome + " " + cognome + " " + ragione_sociale;
                    if (ragione_sociale.length >= 3) {
                        nome_comanda = ragione_sociale;
                    } else {
                        nome_comanda = nome + " " + cognome;
                    }

                    if (id_cliente !== undefined && typeof id_cliente === 'string' && id_cliente[0] === 'P') {
                        nome_comanda = id_cliente;
                    } else if (nome_comanda.trim().length === 0) {
                        nome_comanda = id_cliente;
                    }


                    var query_salva_comanda = "UPDATE comanda SET tipologia_fattorino='" + tipologia_fattorino + "',stato_record='FORNO',rag_soc_cliente='" + id_cliente + "',nome_comanda='" + nome_comanda + "',tipo_consegna='" + tipologia_consegna + "',ora_consegna='" + ora_consegna + "',orario_preparazione='" + orario_preparazione + "',numeretto_asp='" + numeretto + "',jolly='" + subito_sn + "',consegna_gap='" + comanda.consegna_gap + "' WHERE ntav_comanda='" + tavolo + "' and  stato_record='ATTIVO';";
                    //fino al 30/04/2021 era qui che metteva prog_gg_uni a tavolo                    
                    testo_query = "DELETE FROM id_occupati_forno WHERE id_terminale='" + comanda.progr_gg_uni + "';";
                    alasql(testo_query);
                    comanda.sock.send({
                        tipo: "aggiornamento_tavolo",
                        operatore: comanda.operatore,
                        query: testo_query,
                        terminale: comanda.terminale,
                        ip: comanda.ip_address
                    });

                    /* aggiunto il 11/05/2021 per evitare errore di sincronia trovato da Linda e Giancarlo, che quando aprivi l'ultimo tavolo sull'altro pc, 
                     * l'id di comanda del primo diventava come l'ultima comanda battuta causando errori di battitura e lettura a video
                     */
                    comanda.progr_gg_uni = 'ASPORTO' + comanda.centro + aggZero(comanda.terminale, 2);
                    comanda.tavolo = comanda.progr_gg_uni;

                    comanda.ultimo_id_cliente = undefined;
                    comanda.ultimo_id_asporto = undefined;
                    comanda.dati_temporanei_cliente = undefined;
                    console.log("INFORNA", query_salva_comanda);



                    comanda.sock.send({
                        tipo: "aggiornamento_tavolo",
                        operatore: comanda.operatore,
                        query: query_salva_comanda,
                        terminale: comanda.terminale,
                        ip: comanda.ip_address
                    });
                    comanda.sincro.query(query_salva_comanda, function () {




                        var spazio_forno = "select portata,spazio_forno ,ora_consegna from comanda where ora_consegna= '" + ora_consegna + "'and  (stato_record='ATTIVO' OR stato_record= 'FORNO')";
                        var spazio_forno_ora = alasql(spazio_forno);
                        var settagio_spazio = "select Field185,Field186 from settaggi_profili "
                        var query = alasql(settagio_spazio);
                        if (query[0].Field186 === 'true') {
                            if (spazio_forno_ora.length > 0) {
                                var sum = 0;
                                spazio_forno_ora.forEach(function (set) {
                                    if(set.portata==="P"){
                                        sum = sum + parseInt(set.spazio_forno);
                                     
                                    }

                                  

                                });
                                if (sum >= parseInt(query[0].Field185)) {
                                    bootbox.alert("ATTENZIONE! Superata capienza forno nella fascia oraria: " + ora_consegna + "");
                                }
                            }
                        }
                        $('.nome_parcheggio').html('');
                        comanda.tipologia_fattorino = "0";
                        comanda.consegna_fattorino_extra = "0.00";
                        $("select#tipologia_fattorino option[value=" + comanda.tipologia_fattorino + "]").prop("selected", "selected");
                        righe_forno();
                        $('#conto_grande').modal('hide');
                        comanda.esecuzione_comanda_immediata = false;
                        $('#popup_scelta_cliente input[name="ora_consegna"]:visible').val("");
                        comanda.ultima_ora_consegna = "";
                        comanda.tipo_consegna = "NIENTE";
                        $('.tipo_consegna').removeClass('tipo_consegna_selezionata');
                        $('.consegna_gap_15').removeClass('tipo_consegna_selezionata_gap');
                        $('.consegna_gap_30').removeClass('tipo_consegna_selezionata_gap');
                        $('#popup_scelta_cliente input[name="orario_preparazione"]:visible').val("");
                        $('#switch_esecuzione_comanda_immediata').css('background-image', '');
                        if (conto_finale === true) {
                            comanda.funzionidb.conto_attivo();
                            comanda.sincro.query_cassa();
                            comanda.parcheggio = "";
                            reset_progressivo_asporto();
                        }

                    });
                    mod_categoria(comanda.categoria_predefinita);
                    mod_pagina(1);
                } else {
                    if (comanda.parcheggio !== "") {
                        intesta_cliente();
                    }



                }
            } else {
                if (comanda.parcheggio !== "") {
                    intesta_cliente();
                }
            }
        } else if (scontrino === true) {

            comanda.blocca_progressivo = false;
            var id_cliente = "SCONTRINO DIRETTO";
            var nome_comanda = "SCONTRINO DIRETTO";
            var tipologia_consegna = "RITIRO";
            var ora_consegna = "";
            var numeretto = "";
            var subito_sn = "SUBITO";

            var query_salva_comanda = "UPDATE comanda SET  tipologia_fattorino='" + tipologia_fattorino + "',stato_record='CHIUSO',rag_soc_cliente='" + id_cliente + "',nome_comanda='" + nome_comanda + "',tipo_consegna='" + tipologia_consegna + "',ora_consegna='" + ora_consegna + "',numeretto_asp='" + numeretto + "',jolly='" + subito_sn + "' WHERE ntav_comanda='" + tavolo + "' and  stato_record='ATTIVO';";

            /* aggiunto il 11/05/2021 per evitare errore di sincronia trovato da Linda e Giancarlo, che quando aprivi l'ultimo tavolo sull'altro pc, 
             * l'id di comanda del primo diventava come l'ultima comanda battuta causando errori di battitura e lettura a video
             */
            comanda.progr_gg_uni = 'ASPORTO' + comanda.centro + aggZero(comanda.terminale, 2);
            comanda.tavolo = comanda.progr_gg_uni;

            comanda.ultimo_id_cliente = undefined;
            comanda.ultimo_id_asporto = undefined;
            comanda.dati_temporanei_cliente = undefined;
            console.log("INFORNA", query_salva_comanda);




            comanda.sock.send({
                tipo: "aggiornamento_tavolo",
                operatore: comanda.operatore,
                query: query_salva_comanda,
                terminale: comanda.terminale,
                ip: comanda.ip_address
            });
            comanda.sincro.query(query_salva_comanda, function () {



                $('.nome_parcheggio').html('');
                comanda.tipologia_fattorino = "0";
                comanda.consegna_fattorino_extra = "0.00";
                $("select#tipologia_fattorino option[value=" + comanda.tipologia_fattorino + "]").prop("selected", "selected");
                righe_forno();
                $('#conto_grande').modal('hide');
                comanda.esecuzione_comanda_immediata = false;
                $('#popup_scelta_cliente input[name="ora_consegna"]:visible').val("");
                comanda.ultima_ora_consegna = "";
                comanda.tipo_consegna = "NIENTE";
                $('.tipo_consegna').removeClass('tipo_consegna_selezionata');

                $('#popup_scelta_cliente input[name="orario_preparazione"]:visible').val("");
                $('#switch_esecuzione_comanda_immediata').css('background-image', '');
                if (conto_finale === true) {
                    comanda.funzionidb.conto_attivo();
                    comanda.sincro.query_cassa();
                    comanda.parcheggio = "";
                    reset_progressivo_asporto();
                }

            });
            mod_categoria(comanda.categoria_predefinita);
            mod_pagina(1);
        } else {
            intesta_cliente();
        }

    }, comanda.id_comanda_attuale);
    $('#tipo_consegna_modal').html("");
    $('#tipo_consegna_modal_tipo').html("" + "");
    $('#tipo_consegna_modal_gap_scritta').html("");
    $('#tipo_consegna_gap_modal').html("" + "");
}



/*Function,String (id comanda)*/
function query_consegna(callBACK, idc) {
    if (comanda.consegna_presente === true) {



        var categorie_intaccate = comanda.categorie_con_consegna_abilitata.map(function (o) {
            return '"' + o + '"';
        }).join();
        //MODIFICA LE CATEGORIE VANNO RACCHIUSE CON GLI APICI ALTRIMENTI VA NEL DB REMOTO MA NON NEL LOCALE. SONO STRINGHE.

        var query_update_iniziale = 'UPDATE comanda SET ';
        var query_where_categorie = ' WHERE QTA!="00000" AND categoria IN (' + categorie_intaccate + ') ';
        var query_where_categorie_extra_fattorini = ' WHERE QTA!="00000"  ';

        var query_where_finale = ' AND substr(desc_art,1,1)!="+" and  substr(desc_art,1,1)!="-"  and  substr(desc_art,1,1)!="="  and  substr(desc_art,1,1)!="*"   and  substr(desc_art,1,1)!="("   AND id="' + idc + '" /*AND stato_record="ATTIVO"*/ AND stato_record NOT LIKE "%CANCELLATO%" and ntav_comanda="' + comanda.tavolo + '"  and posizione="CONTO" ; ';
        var query_where_finale_extra_fattrini = ' AND  id="' + idc + '" /*AND stato_record="ATTIVO"*/ AND stato_record NOT LIKE "%CANCELLATO%" and ntav_comanda="' + comanda.tavolo + '"  and posizione="CONTO" ; ';
        var interna = function () {


            var query = query_update_iniziale;
            query += ' QTA="" ';
            query += query_where_categorie;
            query += query_where_finale;
            comanda.sock.send({
                tipo: "aggiornamento",
                operatore: comanda.operatore,
                query: query,
                terminale: comanda.terminale,
                ip: comanda.ip_address
            });
            comanda.sincro.query(query, function () {
                interna1();
            });
        };
        var interna1 = function () {
            if (comanda.quantita_pizze_totali > 0) {
                if (comanda.consegna_totale > 0) {
                    if (parseInt(comanda.consegna_fattorino_extra) !== 0) {
                        var calcolo = parseFloat(comanda.consegna_totale) / parseInt(comanda.quantita_pizze_totali);
                        var calcolo_2 = calcolo.toFixed(8);
                        var query = query_update_iniziale;
                        query += ' QTA="' + calcolo_2 + '" ';
                        query += query_where_categorie_extra_fattorini;
                        query += query_where_finale_extra_fattrini;
                        comanda.sock.send({
                            tipo: "aggiornamento",
                            operatore: comanda.operatore,
                            query: query,
                            terminale: comanda.terminale,
                            ip: comanda.ip_address
                        });
                        comanda.sincro.query(query, function () {
                            interna2();
                        });

                    } else {
                        var calcolo = parseFloat(comanda.consegna_totale) / parseInt(comanda.quantita_pizze_totali);
                        var calcolo_2 = calcolo.toFixed(8);
                        var query = query_update_iniziale;

                        query += ' QTA="' + calcolo_2 + '" ';
                        query += query_where_categorie;
                        query += query_where_finale;
                        comanda.sock.send({
                            tipo: "aggiornamento",
                            operatore: comanda.operatore,
                            query: query,
                            terminale: comanda.terminale,
                            ip: comanda.ip_address
                        });
                        comanda.sincro.query(query, function () {
                            interna2();
                        });

                    }

                } else {
                    interna2();
                }
            } else {
                interna2();
            }
        };
        var interna2 = function () {


            if (comanda.consegna_una_pizza > 0) {

                var query = query_update_iniziale;
                query += ' QTA="' + comanda.consegna_una_pizza + '" ';
                query += query_where_categorie;
                query += query_where_finale;
                comanda.sock.send({
                    tipo: "aggiornamento",
                    operatore: comanda.operatore,
                    query: query,
                    terminale: comanda.terminale,
                    ip: comanda.ip_address
                });
                comanda.sincro.query(query, function () {
                    interna3();
                });
            } else {
                interna3();
            }

        };
        var interna3 = function () {


            if (comanda.consegna_piu_di_una_pizza > 0) {

                var query = query_update_iniziale;
                query += ' QTA="' + comanda.consegna_piu_di_una_pizza + '" ';
                query += query_where_categorie;
                query += query_where_finale;
                comanda.sock.send({
                    tipo: "aggiornamento",
                    operatore: comanda.operatore,
                    query: query,
                    terminale: comanda.terminale,
                    ip: comanda.ip_address
                });
                comanda.sincro.query(query, function () {
                    interna4();
                });
            } else {
                interna4();
            }
        };
        var interna4 = function () {


            if (comanda.consegna_unitario_a_pizza > 0) {

                var query = query_update_iniziale;
                query += ' QTA="' + comanda.consegna_unitario_a_pizza + '" ';
                query += query_where_categorie;
                query += query_where_finale;
                comanda.sock.send({
                    tipo: "aggiornamento",
                    operatore: comanda.operatore,
                    query: query,
                    terminale: comanda.terminale,
                    ip: comanda.ip_address
                });
                comanda.sincro.query(query, function () {
                    interna5();
                });
            } else {
                interna5();
            }
        };
        var interna5 = function () {



            if (comanda.consegna_unitario_metro > 0) {

                var query = query_update_iniziale;
                query += ' QTA="' + comanda.consegna_unitario_metro + '" ';
                query += query_where_categorie;
                query += ' AND desc_art LIKE "%UN METRO%" ';
                query += query_where_finale;
                comanda.sock.send({
                    tipo: "aggiornamento",
                    operatore: comanda.operatore,
                    query: query,
                    terminale: comanda.terminale,
                    ip: comanda.ip_address
                });
                comanda.sincro.query(query, function () {
                    interna6();
                });
            } else {
                interna6();
            }
        };
        var interna6 = function () {

            if (comanda.consegna_unitario_mezzo_metro > 0) {

                var query = query_update_iniziale;
                query += ' QTA="' + comanda.consegna_unitario_mezzo_metro + '" ';
                query += query_where_categorie;
                query += ' AND desc_art LIKE "%MEZZO METRO%" ';
                query += query_where_finale;
                comanda.sock.send({
                    tipo: "aggiornamento",
                    operatore: comanda.operatore,
                    query: query,
                    terminale: comanda.terminale,
                    ip: comanda.ip_address
                });
                comanda.sincro.query(query, function () {
                    interna7();
                });
            } else {
                interna7();
            }



        };
        var interna7 = function () {

            if (comanda.consegna_maxi > 0) {

                var query = query_update_iniziale;
                query += ' QTA="' + comanda.consegna_maxi + '" ';
                query += query_where_categorie;
                query += ' AND desc_art LIKE "%MAXI%" ';
                query += query_where_finale;
                comanda.sock.send({
                    tipo: "aggiornamento",
                    operatore: comanda.operatore,
                    query: query,
                    terminale: comanda.terminale,
                    ip: comanda.ip_address
                });
                comanda.sincro.query(query, function () {
                    callBACK(true);
                });
            } else {
                callBACK(true);
            }



        };
        //START
        interna();
        comanda.sincro.query_cassa();
    } else {
        callBACK(true);
    }
}

var schermo_consegne = new WebSocket("ws://localhost:7777");
schermo_consegne.onmessage = function (event) {

    console.log("schermo_consegne - messaggio ricevuto");
    let msg = JSON.parse(event.data);
    switch (msg.tipo) {
        case "accensione":
            righe_forno();
            break;
        case "segna_pony":
            segna_pony(msg.contenuto.id_pony, msg.contenuto.id_comanda);
            righe_forno();
            break;
        case "cancella_riga_forno":
            cancella_riga_forno(msg.contenuto);
            righe_forno();
            break;
        case "stampa_qr_cliente":
            stampa_qr_cliente(msg.contenuto.ora_consegna, msg.contenuto.id_pony, false);
            righe_forno();
            break;
        default:

    }

};
schermo_consegne.onopen = function () {
    console.log("schermo_consegne - connessione effettuata");
};
schermo_consegne.onclose = function () {
    console.log("schermo_consegne - connessione chiusa");
};
schermo_consegne.onerror = function () {
    console.log("schermo_consegne - errore nella connessione");
};

$(document).on('change', '#input_ricerca_righe_forno', () => {

    let testo_da_cercare = $('#input_ricerca_righe_forno').val();

    if (testo_da_cercare.length > 0) {
        righe_forno(false, testo_da_cercare);
    } else {
        righe_forno(false);
    }

});


function righe_forno(bool_provenienza_socket, string_testo_ricerca_opz) {

    fastorder.invia_ordini_forno();

    let tavolo = comanda.tavolo;
    if (comanda.pizzeria_asporto !== true) {
        tavolo = "ASPORTO";
    }

    var d = new Date();
    var Y = d.getFullYear();
    var M = addZero((d.getMonth() + 1), 2);
    var D = addZero(d.getDate(), 2);
    var h = addZero(d.getHours(), 2);
    var m = addZero(d.getMinutes(), 2);
    var s = addZero(d.getSeconds(), 2);
    var ms = addZero(d.getMilliseconds(), 3);
    var data_oggi = Y + '' + M + '' + D;
    var query_tot_pizze_da_fare = "select tipo_impasto,quantita,spazio_forno,ora_consegna,id from comanda where /*ntav_comanda='" + tavolo + "' and*/ data_servizio='" + comanda.data_servizio + "' and portata LIKE '%P%' and (substr(desc_art,1,1)!='+' and substr(desc_art,1,1)!='-' and substr(desc_art,1,1)!='=' and substr(desc_art,1,1)!='*' and substr(desc_art,1,1)!='x' and substr(desc_art,1,1)!='(') and (stato_record='FORNO' or stato_record='ATTIVO') and tipo_consegna!='NIENTE' and tipo_consegna!='';";
    //SUM QUANTITA
    //FROM comanda E BASTA
    //GROUPBY
    //comanda.portata,comanda.tipo_impasto
    //sum=0;
    //          var spazio_forno_per_una_pizza = "select spazio_forno from comanda where /*ntav_comanda='" + set.ora_consegna + "' and*/ data_servizio='" + comanda.data_servizio + "' and ora_consegna='"+set.ora_consegna +"' and portata LIKE '%P%' and (substr(desc_art,1,1)!='+' and substr(desc_art,1,1)!='-' and substr(desc_art,1,1)!='=' and substr(desc_art,1,1)!='*' and substr(desc_art,1,1)!='x' and substr(desc_art,1,1)!='(') and (stato_record='FORNO' or stato_record='ATTIVO') and tipo_consegna!='NIENTE' and tipo_consegna!='';";
    //        var spazio_forno_pizza = alasql(spazio_forno_per_una_pizza);
    //      spazio_forno_pizza.forEach((obj) => {

    //       sum = sum + parseInt( obj.spazio_forno);


    //  });




    alasql(query_tot_pizze_da_fare, function (risultato) {

        var quantita_pizze_per_impasto = new Object();
        var quantita_pizze_per_impasto_normali = 0;
        risultato.forEach(function (set) {
            if (set.tipo_impasto === '') {

                quantita_pizze_per_impasto_normali += parseFloat(set.quantita);
            } else {
                if (quantita_pizze_per_impasto[set.tipo_impasto] === undefined) {
                    quantita_pizze_per_impasto[set.tipo_impasto] = 0;
                }
                quantita_pizze_per_impasto[set.tipo_impasto] += parseFloat(set.quantita);
            }
        });
        $('#tot_pizze_da_fare').html('0');
        var testo_forno = "N: " + quantita_pizze_per_impasto_normali;
        var acapo = 2;
        for (var index in quantita_pizze_per_impasto) {
            var nome_impasto = "";
            switch (index) {
                case "K":
                    nome_impasto = "K";
                    break;
                case "I":
                    nome_impasto = "I";
                    break;
                case "F":
                    nome_impasto = "F";
                    break;
                case "PM":
                    nome_impasto = "PM";
                    break;
                case "5":
                    nome_impasto = "5";
                    break;
                case "7":
                    nome_impasto = "7";
                    break;
                case "S":
                    nome_impasto = "S";
                    break;
                case "G":
                    nome_impasto = "G";
                    break;
                case "C":
                    nome_impasto = "C";
                    break;
                case "N":
                    nome_impasto = "N";
                    break;
                case "DP":
                    nome_impasto = "DP";
                    break;
                case "B":
                    nome_impasto = "B";
                    break;
                case "BF":
                    nome_impasto = "BF";
                    break;
                case "E":
                    nome_impasto = "E";
                    break;
                case "CP":
                    nome_impasto = "CP";
                    break;
                case "GA":
                    nome_impasto = "GA";
                    break;
                case "M":
                    nome_impasto = "M";
                    break;
                case "P":
                    nome_impasto = "P";
                    break;
                case "PR":
                    nome_impasto = "PR";
                    break;
                case "FG":
                    nome_impasto = "FG";
                    break;
                case "BU":
                    nome_impasto = "BU";
                    break;

                case "P1":
                    nome_impasto = comanda.abbreviazione_pasta_personalizzata_1;
                    break;

                case "P2":
                    nome_impasto = comanda.abbreviazione_pasta_personalizzata_2;
                    break;

                case "P3":
                    nome_impasto = comanda.abbreviazione_pasta_personalizzata_3;
                    break;

                case "P4":
                    nome_impasto = comanda.abbreviazione_pasta_personalizzata_4;
                    break;

                case "P5":
                    nome_impasto = comanda.abbreviazione_pasta_personalizzata_5;
                    break;

                case "P6":
                    nome_impasto = comanda.abbreviazione_pasta_personalizzata_6;
                    break;


            }

            testo_forno += "&nbsp;&nbsp;&nbsp;" + nome_impasto + ": " + quantita_pizze_per_impasto[index];
            acapo++;
        }

        $('#tot_pizze_da_fare').html(testo_forno);
    });
    //con che criterio ordinare le tighe del forno

    let ordinamento_righe_forno = "ora_consegna";
    let ordinamento_righe_forno_gap = "consegna_gap"
    if (comanda.tipo_orario_forno === "C") {
        ordinamento_righe_forno = "ora_consegna";
        ordinamento_righe_forno_gap = "consegna_gap"
    } else if (comanda.tipo_orario_forno === "P") {
        ordinamento_righe_forno = "orario_preparazione";
        ordinamento_righe_forno_gap = "consegna_gap"
    }


    var query_comanda = "select id,id_pony,desc_art,portata,'' as citta,'' as comune,'' as indirizzo,'' as numero,tipo_consegna,ora_consegna,orario_preparazione,nome_comanda,quantita,rag_soc_cliente from comanda where /*ntav_comanda='" + tavolo + "' and*/ data_servizio='" + comanda.data_servizio + "'  and (stato_record='FORNO' or stato_record='ATTIVO') and tipo_consegna!='NIENTE' and tipo_consegna!='';";
    var a = alasql(query_comanda);
    var nome_cliente = "";
    var citta = "";
    var comune = "";
    var indirizzo = "";
    var numero = "";
    var cellulare = "";
    var telefono = "";
    var telefono_3 = "";
    var telefono_4 = "";
    var telefono_5 = "";
    var telefono_6 = "";

    var risultato = new Array();
    var risultato_raggruppato = new Object();
    a.forEach(function (e) {

        try {
            var b = alasql("select comune,citta,comune,indirizzo,numero,cellulare,telefono,telefono_3,telefono_4,telefono_5,telefono_6 from clienti where id=" + e.rag_soc_cliente + " limit 1;");
            if (b !== undefined && b[0] !== undefined) {
                nome_cliente = b[0].rag_soc_cliente;
                comune = b[0].comune;
                indirizzo = b[0].indirizzo;
                numero = b[0].numero;
                cellulare = b[0].cellulare;
                telefono = b[0].telefono;
                telefono_3 = b[0].telefono_3;
                telefono_4 = b[0].telefono_4;
                telefono_5 = b[0].telefono_5;
                telefono_6 = b[0].telefono_6;
            } else if (e.nome_comanda.trim() === e.rag_soc_cliente) {
                nome_cliente = e.nome_comanda.trim();
                comune = "";
                citta = "";
                indirizzo = "";
                numero = "";
            }
        } catch (err) {
            console.log("ERRORE GESTITO" + err);
            /*PUO' ESSERE UNA CASISTICA DI ERRORE QUINDI VA RIPETUTO IL CODICE*/
            if (e.nome_comanda.trim() === e.rag_soc_cliente) {
                nome_cliente = e.nome_comanda.trim();
                comune = "";
                citta = "";
                indirizzo = "";
                numero = "";
            }
        }

        if (risultato_raggruppato[e.id] === undefined) {
            risultato_raggruppato[e.id] = new Object();
        }
        if (risultato_raggruppato[e.id][e.nome_comanda] === undefined) {
            risultato_raggruppato[e.id][e.nome_comanda] = new Object();
        }
        if (risultato_raggruppato[e.id][e.nome_comanda][e[ordinamento_righe_forno]] === undefined) {
            e.quantita = parseInt(e.quantita);
            risultato_raggruppato[e.id][e.nome_comanda][e[ordinamento_righe_forno]] = e;
            risultato_raggruppato[e.id][e.nome_comanda][e[ordinamento_righe_forno]].citta = citta;
            risultato_raggruppato[e.id][e.nome_comanda][e[ordinamento_righe_forno]].comune = comune;
            risultato_raggruppato[e.id][e.nome_comanda][e[ordinamento_righe_forno]].indirizzo = indirizzo;
            risultato_raggruppato[e.id][e.nome_comanda][e[ordinamento_righe_forno]].numero = numero;
            risultato_raggruppato[e.id][e.nome_comanda][e[ordinamento_righe_forno]].cellulare = cellulare;
            risultato_raggruppato[e.id][e.nome_comanda][e[ordinamento_righe_forno]].telefono = telefono;
            risultato_raggruppato[e.id][e.nome_comanda][e[ordinamento_righe_forno]].telefono_3 = telefono_3;
            risultato_raggruppato[e.id][e.nome_comanda][e[ordinamento_righe_forno]].telefono_4 = telefono_4;
            risultato_raggruppato[e.id][e.nome_comanda][e[ordinamento_righe_forno]].telefono_5 = telefono_5;
            risultato_raggruppato[e.id][e.nome_comanda][e[ordinamento_righe_forno]].telefono_6 = telefono_6;
        } else {
            risultato_raggruppato[e.id][e.nome_comanda][e[ordinamento_righe_forno]].quantita += parseInt(e.quantita);
        }

        b = {};
        delete b;
    });
    for (let id in risultato_raggruppato) {
        for (let nome_comanda in risultato_raggruppato[id]) {
            for (let ord_righe_forno in risultato_raggruppato[id][nome_comanda]) {

                risultato.push(risultato_raggruppato[id][nome_comanda][ord_righe_forno]);
            }
        }
    }

    a = {};
    delete a;
    risultato_raggruppato = {};
    delete risultato_raggruppato;
    console.log("RISULTATO_PARZIALE", risultato);
    $(".forno_pag table tbody tr").remove();
    $(".forno_popup table tbody tr").remove();
    let info_scooter = "";
    $(".consegna table tbody tr").remove();
    var ultima_ora_consegna = '';
    var righe_forno_html = '';
    var id_comanda = new Object();
    var risultato_raggruppamento_quantita_orario = new Object();
    var query_conti = 'select desc_art,portata,quantita,spazio_forno,tipo_ricevuta,stampata_sn,ora_consegna,orario_preparazione,portata,id,stato_record,tipo_consegna,consegna_gap from comanda where   /*ntav_comanda="' + tavolo + '" and */ data_servizio="' + comanda.data_servizio + '" and (stato_record="FORNO" or stato_record="ATTIVO") and tipo_consegna!="NIENTE" and tipo_consegna!="" ;';
    //console.log("QUERY RIGHE FORNO 2");
    alasql(query_conti, function (conti) {

        conti.forEach(function (articolo) {


            /* Tutte le portate con la sigla P all'interno, quindi Pizze ma anche altro volendo, vengono conteggiate */
            if (articolo.portata.indexOf("P") !== -1 && (articolo.desc_art.substr(0, 1) !== '+' && articolo.desc_art.substr(0, 1) !== '-' && articolo.desc_art.substr(0, 1) !== '*' && articolo.desc_art.substr(0, 1) !== '=' && articolo.desc_art.substr(0, 1) !== 'x' && articolo.desc_art.substr(0, 1) !== '(')) {

                if (risultato_raggruppamento_quantita_orario[articolo[ordinamento_righe_forno]] === undefined) {
                    risultato_raggruppamento_quantita_orario[articolo[ordinamento_righe_forno]] = parseInt(articolo.quantita);
                    risultato_raggruppamento_quantita_orario[articolo[ordinamento_righe_forno_gap]] = parseInt(articolo.quantita);
                } else {
                    risultato_raggruppamento_quantita_orario[articolo[ordinamento_righe_forno]] += parseInt(articolo.quantita);
                    risultato_raggruppamento_quantita_orario[articolo[ordinamento_righe_forno_gap]] += parseInt(articolo.quantita);
                }

            }


            if (id_comanda[articolo.id] === undefined) {

                id_comanda[articolo.id] = new Object();
                id_comanda[articolo.id].pizze = 0;
                id_comanda[articolo.id].spazio_forno = 0;
                id_comanda[articolo.id].altri_articoli = 0;
                id_comanda[articolo.id].tipo_ricevuta = '';

            }

            if (articolo.stampata_sn === "S") {
                if (id_comanda[articolo.id].tipo_ricevuta.indexOf('<span style="font-size:14px;"><strong>C</strong></span>') === -1) {
                    id_comanda[articolo.id].tipo_ricevuta += '<span style="font-size:14px;"><strong>C</strong></span>';
                }
            }

            if (articolo.tipo_ricevuta.indexOf("STORICIZZAZIONE") !== -1) {
                if (id_comanda[articolo.id].tipo_ricevuta.indexOf('<span style="font-size:14px;"><strong>S</strong></span>') === -1) {
                    id_comanda[articolo.id].tipo_ricevuta += '<span style="font-size:14px;"><strong>S</strong></span>';
                }
            }


            switch (articolo.tipo_ricevuta) {
                case "conto":
                case "CONTO":
                    if (id_comanda[articolo.id].tipo_ricevuta.indexOf("<img style='width: 20%;height:auto;' src='./img/BLU_51x51.png' />") === -1) {
                        id_comanda[articolo.id].tipo_ricevuta += "<img style='width: 20%;height:auto;' src='./img/BLU_51x51.png' />";
                    }
                    break;
                case "scontrino":
                case "fattura":
                case "SCONTRINO":
                case "FATTURA":
                    if (id_comanda[articolo.id].tipo_ricevuta.indexOf("<img style='width: 20%;height:auto;' src='./img/ROSSO_51x51.png' />") === -1) {
                        id_comanda[articolo.id].tipo_ricevuta += "<img style='width: 20%;height:auto;' src='./img/ROSSO_51x51.png' />";
                    }
                    break;
            }


            if (comanda.pizzeria_asporto !== true && tavolo.left(7) === "ASPORTO") {
                if (articolo.desc_art.substr(0, 1) === '+' || articolo.desc_art.substr(0, 1) === '-' || articolo.desc_art.substr(0, 1) === '*' || articolo.desc_art.substr(0, 1) === '=' || articolo.desc_art.substr(0, 1) === 'x' || articolo.desc_art.substr(0, 1) === '(') {


                } else if (articolo.portata.indexOf("B") === 0) {
                    id_comanda[articolo.id].altri_articoli += parseInt(articolo.quantita);
                } else {
                    id_comanda[articolo.id].pizze += parseInt(articolo.quantita);
                    if (isNaN(parseInt(articolo.spazio_forno))) {
                        id_comanda[articolo.id].spazio_forno += 1 * parseInt(articolo.quantita);
                    } else {
                        id_comanda[articolo.id].spazio_forno += parseInt(articolo.spazio_forno) * parseInt(articolo.quantita);
                    }
                }

            } else {
                if (articolo.desc_art.substr(0, 1) === '+' || articolo.desc_art.substr(0, 1) === '-' || articolo.desc_art.substr(0, 1) === '*' || articolo.desc_art.substr(0, 1) === '=' || articolo.desc_art.substr(0, 1) === 'x' || articolo.desc_art.substr(0, 1) === '(') {


                } else if (articolo.portata.indexOf("P") === -1) {
                    id_comanda[articolo.id].altri_articoli += parseInt(articolo.quantita);
                } else {
                    id_comanda[articolo.id].pizze += parseInt(articolo.quantita);
                    id_comanda[articolo.id].gap = articolo.consegna_gap;
                    if (isNaN(parseInt(articolo.spazio_forno))) {
                        id_comanda[articolo.id].spazio_forno += 1 * parseInt(articolo.quantita);
                    } else {
                        id_comanda[articolo.id].spazio_forno += parseInt(articolo.spazio_forno) * parseInt(articolo.quantita);
                    }
                }
            }
        });
        risultato = alasql("select * from ? order by " + ordinamento_righe_forno + " asc, id_pony asc;", [risultato]);

        if (checkEmptyVariable(string_testo_ricerca_opz) === true) {


            risultato = risultato.filter((value) => {
                if (Object.values(value).join(" ").indexOf(string_testo_ricerca_opz) !== -1) {
                    return value;
                }
            });

            console.log(risultato)

        }


        risultato.forEach(function (obj) {





            var img_consegna;
            if (obj['tipo_consegna'] === "DOMICILIO") {
                img_consegna = "<span style='font-size:4vh;margin:10px;' class='glyphicon glyphicon-road'></span>";
            } else if (obj['tipo_consegna'] === "RITIRO") {
                img_consegna = "<span style='font-size:4vh;margin:10px;'  class='glyphicon glyphicon-home'></span>";
            } else {
                img_consegna = "<span style='font-size:4vh;margin:10px;'  class='glyphicon glyphicon-cutlery'></span>";
            }



            if (obj[ordinamento_righe_forno] !== ultima_ora_consegna) {
                righe_forno_html += "<tr style='height:1mm;'></tr>";
                $(".forno_pag table tbody").append("<tr style='height:1mm;'></tr>");
                $(".forno_popup table tbody").append("<tr style='height:1mm;'></tr>");
                if (obj['tipo_consegna'] === "DOMICILIO") {
                    info_scooter += "<tr style='height:1mm;'></tr>";
                }
            }




            var qnt_ragg_ora = 0;
            if (risultato_raggruppamento_quantita_orario[obj[ordinamento_righe_forno]] !== undefined) {
                qnt_ragg_ora = risultato_raggruppamento_quantita_orario[obj[ordinamento_righe_forno]];
            }

            //183 rosso
            //184 giallo



            var colore_riga = '';
            switch (true) {

                case (qnt_ragg_ora >= 1 && qnt_ragg_ora < comanda.inizio_fascia_gialla):
                    colore_riga = "#008966";
                    break;
                case (qnt_ragg_ora >= comanda.inizio_fascia_gialla && qnt_ragg_ora < comanda.inizio_fascia_rossa):
                    colore_riga = "#c87f0a";
                    break;
                case (qnt_ragg_ora >= comanda.inizio_fascia_rossa):
                    colore_riga = "#d62c1a";
                    break;
                default:
                    colore_riga = "initial";
            }




            var L_0629 = "";
            if (comanda.numero_licenza_cliente === "0629" && obj['tipo_consegna'] === "DOMICILIO") {
                let testo_query = "select * from elenco_progressivi_asporto_domicilio where progressivo_comanda='" + obj['id'] + "' limit 1;";
                let progressivo_attuale = alasql(testo_query);
                if (progressivo_attuale.length > 0) {

                    L_0629 = "<br>(" + progressivo_attuale[0].numero + ")";
                }

            }

            var query_gap_consegna = "select time_gap_consegna from impostazioni_fiscali where id=" + comanda.folder_number + ";"
            var result = alasql(query_gap_consegna);
            if (result[0].time_gap_consegna == "1" && id_comanda[obj['id']].gap != "undefined" && id_comanda[obj['id']].gap != "" && ora_consegna != "") {
                var ora_consegna = obj[ordinamento_righe_forno];
                var minuti_somma = ora_consegna.substring(ora_consegna.indexOf(':') + 1)
                var int_miunti = parseInt(minuti_somma);
                var int_gap_miunti = parseInt(id_comanda[obj['id']].gap);;
                var min_somma = int_miunti + int_gap_miunti;
                var ora_gap = ora_consegna.substring(0, ora_consegna.indexOf(':'))
                var hours = Math.floor(min_somma / 60);
                var minutes = min_somma % 60;
                var ora = parseInt(ora_gap);

                if (minutes < 10) {
                    if (ora < 10) {
                        var oraa = ora + hours
                        if(oraa===10)
                        { var totale_ora = oraa + ":" + minutes;

                        }
                        else{
                            var totale_ora = "0" + oraa + ":" + minutes;
                        }
                    }
                    else {
                        var totale_ora = ora + hours + ":" + "0" + minutes;

                    }

                }
                else {
                    if (ora < 10) {
                        var oraa = ora + hours
                        if(oraa===10)
                        { var totale_ora = oraa + ":" + minutes;

                        }
                        else{
                            var totale_ora = "0" + oraa + ":" + minutes;
                        }
                    }
                    else {

                        var totale_ora = ora + hours + ":" + minutes;
                    }
                }
                if (totale_ora === "NaN:NaN") {
                    totale_ora = ""
                }




                righe_forno_html += "<tr  style='background-color:" + colore_riga + "' class='" + obj['id'] + "'><td class='text-center'>" + obj[ordinamento_righe_forno] + "\n " + totale_ora + "</td><td class='text-center'>" + id_comanda[obj['id']].pizze + " (+" + id_comanda[obj['id']].altri_articoli + ")</td><td class='text-center'>" + id_comanda[obj['id']].spazio_forno + "</td><td class='nome_comanda text-center'>" + obj['nome_comanda'].toUpperCase() + L_0629 + "</td><td class='text-center' style='padding: 0;'>" + img_consegna + "</td ><td class='tipo_ricevuta text-center'>" + id_comanda[obj['id']].tipo_ricevuta + "</td><td class='escludi-listener'><input class='form-control' type='checkbox' style='margin:0;padding:0;' " + comanda.evento + "='cancella_riga_forno(\"" + obj['id'] + "\")' /></td></tr>";
                $(".forno_pag table tbody").append("<tr  style='background-color:" + colore_riga + "' class='" + obj['id'] + "'><td class='text-center'>" + obj[ordinamento_righe_forno] + "\n " + totale_ora + "</td><td class='text-center'>" + id_comanda[obj['id']].pizze + " (+" + id_comanda[obj['id']].altri_articoli + ")</td><td class='text-center'>" + id_comanda[obj['id']].spazio_forno + "</td><td class='nome_comanda text-center'>" + obj['nome_comanda'].toUpperCase() + L_0629 + "</td><td class='text-center' style='padding: 0;'>" + img_consegna + "</td ><td class='tipo_ricevuta text-center'>" + id_comanda[obj['id']].tipo_ricevuta + "</td><td class='escludi-listener'><input class='form-control' type='checkbox' style='margin:0;padding:0;' " + comanda.evento + "='cancella_riga_forno(\"" + obj['id'] + "\")' /></td></tr>");
                if (obj['tipo_consegna'] !== "RITIRO" && obj['indirizzo'] !== undefined && obj['indirizzo'] !== null && obj['indirizzo'].length > 0) {
                    righe_forno_html += "<tr style='background-color:" + colore_riga + "' class='" + obj['id'] + "'><td class='text-center'>" + obj[ordinamento_righe_forno] + "\n " + totale_ora + "</td><td class='text-center'>" + id_comanda[obj['id']].pizze + " (+" + id_comanda[obj['id']].altri_articoli + ")</td><td class='text-center'>" + id_comanda[obj['id']].spazio_forno + "</td><td class='text-center' style='padding: 0;'>" + img_consegna + "</td><td class='text-center'>" + obj['indirizzo'] + ", " + obj['numero'] + " - " + obj['comune'] + "</td><td class='nome_comanda text-center'>" + obj['nome_comanda'].toUpperCase() + L_0629 + "</td><td class='tipo_ricevuta text-center'>" + id_comanda[obj['id']].tipo_ricevuta + "</td><td  class='escludi-listener'><input class='form-control' type='checkbox' style='margin:0;padding:0;' " + comanda.evento + "='cancella_riga_forno(\"" + obj['id'] + "\")' /></td></tr>";
                    if (comanda.pizzeria_asporto !== true && tavolo.left(7) === "ASPORTO") {
                        $(".forno_popup table tbody").append("<tr style='background-color:" + colore_riga + "' class='" + obj['id'] + "'><td class='text-center'>" + obj[ordinamento_righe_forno] + "\n " + totale_ora + "</td><td class='text-center'>" + id_comanda[obj['id']].pizze + " (+" + id_comanda[obj['id']].altri_articoli + ")</td><td class='text-center' style='padding: 0;'>" + img_consegna + "</td><td class='text-center'>" + obj['indirizzo'] + ", " + obj['numero'] + " - " + obj['comune'] + "</td><td class='nome_comanda text-center'>" + obj['nome_comanda'].toUpperCase() + L_0629 + "</td><td class='tipo_ricevuta text-center'>" + id_comanda[obj['id']].tipo_ricevuta + "</td><td  class='escludi-listener'><input class='form-control' type='checkbox' style='margin:0;padding:0;height:5vh;' " + comanda.evento + "='cancella_riga_forno(\"" + obj['id'] + "\")' /></td></tr>");
                    } else {
                        $(".forno_popup table tbody").append("<tr style='background-color:" + colore_riga + "' class='" + obj['id'] + "'><td class='text-center'>" + obj[ordinamento_righe_forno] + "\n " + totale_ora + "</td><td class='text-center'>" + id_comanda[obj['id']].pizze + " (+" + id_comanda[obj['id']].altri_articoli + ")</td><td class='text-center'>" + id_comanda[obj['id']].spazio_forno + "</td><td class='text-center' style='padding: 0;'>" + img_consegna + "</td><td class='text-center'>" + obj['indirizzo'] + ", " + obj['numero'] + " - " + obj['comune'] + "</td><td class='nome_comanda text-center'>" + obj['nome_comanda'].toUpperCase() + L_0629 + "</td><td class='tipo_ricevuta text-center'>" + id_comanda[obj['id']].tipo_ricevuta + "</td><td  class='escludi-listener'></td></tr>");
                    }
                } else {
                    righe_forno_html += "<tr style='background-color:" + colore_riga + "' class='" + obj['id'] + "'><td class='text-center'>" + obj[ordinamento_righe_forno] + "\n " + totale_ora + "</td><td class='text-center'>" + id_comanda[obj['id']].pizze + " (+" + id_comanda[obj['id']].altri_articoli + ")</td><td class='text-center'>" + id_comanda[obj['id']].spazio_forno + "</td><td class='text-center' style='padding: 0;'>" + img_consegna + "</td><td class='text-center'>RITIRO</td><td class='nome_comanda text-center'>" + obj['nome_comanda'].toUpperCase() + L_0629 + "</td><td class='tipo_ricevuta text-center'>" + id_comanda[obj['id']].tipo_ricevuta + "</td><td class='escludi-listener' ><input class='form-control' type='checkbox' style='margin:0;padding:0;' " + comanda.evento + "='cancella_riga_forno(\"" + obj['id'] + "\")' /></td></tr>";
                    if (comanda.pizzeria_asporto !== true && tavolo.left(7) === "ASPORTO") {
                        $(".forno_popup table tbody").append("<tr style='background-color:" + colore_riga + "' class='" + obj['id'] + "'><td class='text-center'>" + obj[ordinamento_righe_forno] + "\n " + totale_ora + "</td><td class='text-center'>" + id_comanda[obj['id']].pizze + " (+" + id_comanda[obj['id']].altri_articoli + ")</td><td class='text-center' style='padding: 0;'>" + img_consegna + "</td><td class='text-center'>RITIRO</td><td class='nome_comanda text-center'>" + obj['nome_comanda'].toUpperCase() + L_0629 + "</td><td class='tipo_ricevuta text-center'>" + id_comanda[obj['id']].tipo_ricevuta + "</td><td class='escludi-listener' ><input class='form-control' type='checkbox' style='margin:0;padding:0;height:5vh;' " + comanda.evento + "='cancella_riga_forno(\"" + obj['id'] + "\")' /></td></tr>");
                    } else {
                        $(".forno_popup table tbody").append("<tr style='background-color:" + colore_riga + "' class='" + obj['id'] + "'><td class='text-center'>" + obj[ordinamento_righe_forno] + "\n " + totale_ora + "</td><td class='text-center'>" + id_comanda[obj['id']].pizze + " (+" + id_comanda[obj['id']].altri_articoli + ")</td><td class='text-center'>" + id_comanda[obj['id']].spazio_forno + "</td><td class='text-center' style='padding: 0;'>" + img_consegna + "</td><td class='text-center'>RITIRO</td><td class='nome_comanda text-center'>" + obj['nome_comanda'].toUpperCase() + L_0629 + "</td><td class='tipo_ricevuta text-center'>" + id_comanda[obj['id']].tipo_ricevuta + "</td><td class='escludi-listener' ></td></tr>");
                    }
                }

            }
            else {
                righe_forno_html += "<tr  style='background-color:" + colore_riga + "' class='" + obj['id'] + "'><td class='text-center'>" + obj[ordinamento_righe_forno] + "</td><td class='text-center'>" + id_comanda[obj['id']].pizze + " (+" + id_comanda[obj['id']].altri_articoli + ")</td><td class='text-center'>" + id_comanda[obj['id']].spazio_forno + "</td><td class='nome_comanda text-center'>" + obj['nome_comanda'].toUpperCase() + L_0629 + "</td><td class='text-center' style='padding: 0;'>" + img_consegna + "</td ><td class='tipo_ricevuta text-center'>" + id_comanda[obj['id']].tipo_ricevuta + "</td><td class='escludi-listener'><input class='form-control' type='checkbox' style='margin:0;padding:0;' " + comanda.evento + "='cancella_riga_forno(\"" + obj['id'] + "\")' /></td></tr>";
                $(".forno_pag table tbody").append("<tr  style='background-color:" + colore_riga + "' class='" + obj['id'] + "'><td class='text-center'>" + obj[ordinamento_righe_forno] + "</td><td class='text-center'>" + id_comanda[obj['id']].pizze + " (+" + id_comanda[obj['id']].altri_articoli + ")</td><td class='text-center'>" + id_comanda[obj['id']].spazio_forno + "</td><td class='nome_comanda text-center'>" + obj['nome_comanda'].toUpperCase() + L_0629 + "</td><td class='text-center' style='padding: 0;'>" + img_consegna + "</td ><td class='tipo_ricevuta text-center'>" + id_comanda[obj['id']].tipo_ricevuta + "</td><td class='escludi-listener'><input class='form-control' type='checkbox' style='margin:0;padding:0;' " + comanda.evento + "='cancella_riga_forno(\"" + obj['id'] + "\")' /></td></tr>");
                if (obj['tipo_consegna'] !== "RITIRO" && obj['indirizzo'] !== undefined && obj['indirizzo'] !== null && obj['indirizzo'].length > 0) {
                    righe_forno_html += "<tr style='background-color:" + colore_riga + "' class='" + obj['id'] + "'><td class='text-center'>" + obj[ordinamento_righe_forno] + "</td><td class='text-center'>" + id_comanda[obj['id']].pizze + " (+" + id_comanda[obj['id']].altri_articoli + ")</td><td class='text-center'>" + id_comanda[obj['id']].spazio_forno + "</td><td class='text-center' style='padding: 0;'>" + img_consegna + "</td><td class='text-center'>" + obj['indirizzo'] + ", " + obj['numero'] + " - " + obj['comune'] + "</td><td class='nome_comanda text-center'>" + obj['nome_comanda'].toUpperCase() + L_0629 + "</td><td class='tipo_ricevuta text-center'>" + id_comanda[obj['id']].tipo_ricevuta + "</td><td  class='escludi-listener'><input class='form-control' type='checkbox' style='margin:0;padding:0;' " + comanda.evento + "='cancella_riga_forno(\"" + obj['id'] + "\")' /></td></tr>";
                    if (comanda.pizzeria_asporto !== true && tavolo.left(7) === "ASPORTO") {
                        $(".forno_popup table tbody").append("<tr style='background-color:" + colore_riga + "' class='" + obj['id'] + "'><td class='text-center'>" + obj[ordinamento_righe_forno] + "</td><td class='text-center'>" + id_comanda[obj['id']].pizze + " (+" + id_comanda[obj['id']].altri_articoli + ")</td><td class='text-center' style='padding: 0;'>" + img_consegna + "</td><td class='text-center'>" + obj['indirizzo'] + ", " + obj['numero'] + " - " + obj['comune'] + "</td><td class='nome_comanda text-center'>" + obj['nome_comanda'].toUpperCase() + L_0629 + "</td><td class='tipo_ricevuta text-center'>" + id_comanda[obj['id']].tipo_ricevuta + "</td><td  class='escludi-listener'><input class='form-control' type='checkbox' style='margin:0;padding:0;height:5vh;' " + comanda.evento + "='cancella_riga_forno(\"" + obj['id'] + "\")' /></td></tr>");
                    } else {
                        $(".forno_popup table tbody").append("<tr style='background-color:" + colore_riga + "' class='" + obj['id'] + "'><td class='text-center'>" + obj[ordinamento_righe_forno] + "</td><td class='text-center'>" + id_comanda[obj['id']].pizze + " (+" + id_comanda[obj['id']].altri_articoli + ")</td><td class='text-center'>" + id_comanda[obj['id']].spazio_forno + "</td><td class='text-center' style='padding: 0;'>" + img_consegna + "</td><td class='text-center'>" + obj['indirizzo'] + ", " + obj['numero'] + " - " + obj['comune'] + "</td><td class='nome_comanda text-center'>" + obj['nome_comanda'].toUpperCase() + L_0629 + "</td><td class='tipo_ricevuta text-center'>" + id_comanda[obj['id']].tipo_ricevuta + "</td><td  class='escludi-listener'></td></tr>");
                    }
                } else {
                    righe_forno_html += "<tr style='background-color:" + colore_riga + "' class='" + obj['id'] + "'><td class='text-center'>" + obj[ordinamento_righe_forno] + "</td><td class='text-center'>" + id_comanda[obj['id']].pizze + " (+" + id_comanda[obj['id']].altri_articoli + ")</td><td class='text-center'>" + id_comanda[obj['id']].spazio_forno + "</td><td class='text-center' style='padding: 0;'>" + img_consegna + "</td><td class='text-center'>RITIRO</td><td class='nome_comanda text-center'>" + obj['nome_comanda'].toUpperCase() + L_0629 + "</td><td class='tipo_ricevuta text-center'>" + id_comanda[obj['id']].tipo_ricevuta + "</td><td class='escludi-listener' ><input class='form-control' type='checkbox' style='margin:0;padding:0;' " + comanda.evento + "='cancella_riga_forno(\"" + obj['id'] + "\")' /></td></tr>";
                    if (comanda.pizzeria_asporto !== true && tavolo.left(7) === "ASPORTO") {
                        $(".forno_popup table tbody").append("<tr style='background-color:" + colore_riga + "' class='" + obj['id'] + "'><td class='text-center'>" + obj[ordinamento_righe_forno] + "</td><td class='text-center'>" + id_comanda[obj['id']].pizze + " (+" + id_comanda[obj['id']].altri_articoli + ")</td><td class='text-center' style='padding: 0;'>" + img_consegna + "</td><td class='text-center'>RITIRO</td><td class='nome_comanda text-center'>" + obj['nome_comanda'].toUpperCase() + L_0629 + "</td><td class='tipo_ricevuta text-center'>" + id_comanda[obj['id']].tipo_ricevuta + "</td><td class='escludi-listener' ><input class='form-control' type='checkbox' style='margin:0;padding:0;height:5vh;' " + comanda.evento + "='cancella_riga_forno(\"" + obj['id'] + "\")' /></td></tr>");
                    } else {
                        $(".forno_popup table tbody").append("<tr style='background-color:" + colore_riga + "' class='" + obj['id'] + "'><td class='text-center'>" + obj[ordinamento_righe_forno] + "</td><td class='text-center'>" + id_comanda[obj['id']].pizze + " (+" + id_comanda[obj['id']].altri_articoli + ")</td><td class='text-center'>" + id_comanda[obj['id']].spazio_forno + "</td><td class='text-center' style='padding: 0;'>" + img_consegna + "</td><td class='text-center'>RITIRO</td><td class='nome_comanda text-center'>" + obj['nome_comanda'].toUpperCase() + L_0629 + "</td><td class='tipo_ricevuta text-center'>" + id_comanda[obj['id']].tipo_ricevuta + "</td><td class='escludi-listener' ></td></tr>");
                    }
                }

            }




            if (obj['tipo_consegna'] === "DOMICILIO") {

                var nome_pony = "";
                if (oggetto_pony[obj['id_pony']] !== undefined) {
                    nome_pony = oggetto_pony[obj['id_pony']];
                }
                var query_gap_consegna = "select time_gap_consegna from impostazioni_fiscali where id=" + comanda.folder_number + ";"
                var result = alasql(query_gap_consegna);
                if (result[0].time_gap_consegna == "1" && id_comanda[obj['id']].gap != "undefined" && id_comanda[obj['id']].gap != "" && ora_consegna != "") {
                    var ora_consegna = obj[ordinamento_righe_forno];
                    var minuti_somma = ora_consegna.substring(ora_consegna.indexOf(':') + 1)
                    var int_miunti = parseInt(minuti_somma);
                    var int_gap_miunti = parseInt(id_comanda[obj['id']].gap);;
                    var min_somma = int_miunti + int_gap_miunti;
                    var ora_gap = ora_consegna.substring(0, ora_consegna.indexOf(':'))
                    var hours = Math.floor(min_somma / 60);
                    var minutes = min_somma % 60;
                    var ora = parseInt(ora_gap);

                    if (minutes < 10) {
                        if (ora < 10) {
                            var oraa = ora + hours
                            var totale_ora = "0" + oraa + ":" + "0" + minutes;
                        }
                        else {
                            var totale_ora = ora + hours + ":" + "0" + minutes;

                        }

                    }
                    else {
                        if (ora < 10) {
                            var oraa = ora + hours
                            var totale_ora = "0" + oraa + ":" + minutes;
                        }
                        else {

                            var totale_ora = ora + hours + ":" + minutes;
                        }
                    }


                    info_scooter += "<tr class='" + obj['id'] + "'><td class='text-center'><a " + comanda.evento + "='stampa_qr_cliente(\"" + obj['ora_consegna'] + "\",\"" + obj['id_pony'] + "\",\"" + totale_ora + "\");'>" + obj['ora_consegna'] + " \ " + " " + totale_ora + "</a><br/>" + nome_pony + "</td><td class='text-center'>" + id_comanda[obj['id']].pizze + " (+" + id_comanda[obj['id']].altri_articoli + ")</td><td class='nome_comanda text-center'><a onclick='assegna_pony(\"" + obj['id'] + "\")'>" + obj['nome_comanda'].toUpperCase() + "</a>";
                }
                else {
                    info_scooter += "<tr class='" + obj['id'] + "'><td class='text-center'><a " + comanda.evento + "='stampa_qr_cliente(\"" + obj['ora_consegna'] + "\",\"" + obj['id_pony'] + "\");'>" + obj['ora_consegna'] + "</a><br/>" + nome_pony + "</td><td class='text-center'>" + id_comanda[obj['id']].pizze + " (+" + id_comanda[obj['id']].altri_articoli + ")</td><td class='nome_comanda text-center'><a onclick='assegna_pony(\"" + obj['id'] + "\")'>" + obj['nome_comanda'].toUpperCase() + "</a>";
                }

                if (comanda.numero_licenza_cliente === "0629") {
                    let testo_query = "select * from elenco_progressivi_asporto_domicilio where progressivo_comanda='" + obj['id'] + "' limit 1;";
                    let progressivo_attuale = alasql(testo_query);
                    if (progressivo_attuale.length > 0) {

                        info_scooter += "<br>(" + progressivo_attuale[0].numero + ")";
                    }

                }


                if (comanda.abilita_chiusura_ordine === "true" || comanda.abilita_chiusura_ordine === true) {
                    info_scooter += "</td><td class='escludi-listener text-center' >" + obj['indirizzo'] + ", " + obj['numero'] + "<br/>" + obj['comune'] + "</td><td class='escludi-listener'><input class='form-control' type='checkbox' style='margin:0;padding:0;' " + comanda.evento + "='cancella_riga_forno(\"" + obj['id'] + "\")' /></td></tr>";
                } else {
                    info_scooter += "</td><td class='escludi-listener text-center' >" + obj['indirizzo'] + ", " + obj['numero'] + "<br/>" + obj['comune'] + "</td><td class='escludi-listener'></td></tr>";
                }
            }

            ultima_ora_consegna = obj[ordinamento_righe_forno];
        });
    });
    var query_selezione_pony = 'SELECT * FROM pony_express ORDER BY nome ASC;';
    let array_risultato = alasql(query_selezione_pony);
    schermo_consegne.send(JSON.stringify({
        tipo: "dati_pony",
        contenuto: array_risultato
    }));
    schermo_consegne.send(JSON.stringify({
        tipo: "righe_forno",
        contenuto: info_scooter
    }));

    $(".consegna table tbody").html(info_scooter);
    if (bool_provenienza_socket !== true) {

        comanda.sock.send({
            tipo: "aggiornamento_righe_forno",
            operatore: comanda.operatore,
            query: "select null",
            terminale: comanda.terminale,
            ip: comanda.ip_address
        });
    }


}

function tasto_tavoli() {
    if (comanda.pizzeria_asporto !== true && comanda.tavolo.left(7) === "ASPORTO") {
        if ($("#conto tr").not(".non-contare-riga").length > 0 || $(".bottoni_asporto.nome_parcheggio").html().trim().length > 1) {
            inforna(true);
        } else {
            comanda.stampa_comanda = false;
            comanda.tasto_indietro_parcheggio = true;
            btn_tavoli();
        }
    } else {
        comanda.stampa_comanda = false;
        comanda.tasto_indietro_parcheggio = true;
        btn_tavoli();
    }
}

function switch_esecuzione_comanda_immediata() {

    console.log("** LOG ORDINE", "TASTO SUBITO");
    if (comanda.esecuzione_comanda_immediata === false) {
        comanda.esecuzione_comanda_immediata = true;
        var data_js = new Date();
        var ora_adesso = addZero(data_js.getHours(), 2) + ':' + addZero(data_js.getMinutes(), 2);
        $('#popup_scelta_cliente input[name="ora_consegna"]:visible').val(ora_adesso);
        //NON DEVE ESSERCI. ANCHE SE VIENE CAMBIATA ORA A FARE SUBITO DEVE AVVISARE!
        //comanda.ultima_ora_consegna = ora_adesso;
        comanda.tipo_consegna = 'RITIRO';
        $('.tipo_consegna').removeClass('tipo_consegna_selezionata');
        $('.consegna_gap_15').removeClass('tipo_consegna_selezionata_gap');
        $('.consegna_gap_30').removeClass('tipo_consegna_selezionata_gap');
        $('#consegna_input_gap').val("");
        $('.consegna_ritiro').addClass('tipo_consegna_selezionata');
        $('#switch_esecuzione_comanda_immediata').css('background-image', '-webkit-linear-gradient(top, grey,#4d4d4d)');
        if (comanda.orario_preparazione === "1") {

            if ($('[name="ora_consegna"]').val().length === 5) {

                let a = new Date("01 Jan 2000 " + $('[name="ora_consegna"]').val() + ":00");
                let b = aggZero(a.getHours(), 2) + ":" + aggZero(a.getMinutes(), 2);
                $('[name="orario_preparazione"]').val(b);
            }
        }
    } else {
        comanda.esecuzione_comanda_immediata = false;
        $('#switch_esecuzione_comanda_immediata').css('background-image', '');
        if (comanda.orario_preparazione === "1") {

            if ($('[name="ora_consegna"]').val().length === 5) {

                let a = new Date("01 Jan 2000 " + $('[name="ora_consegna"]').val() + ":00");
                if (comanda.tipo_consegna === "DOMICILIO") {
                    a.setMinutes(a.getMinutes() + parseInt("-" + comanda.tempo_preparazione_default) + parseInt("-" + comanda.tempo_preparazione_default_domicilio));
                } else {
                    a.setMinutes(a.getMinutes() + parseInt("-" + comanda.tempo_preparazione_default));
                }

                let b = aggZero(a.getHours(), 2) + ":" + aggZero(a.getMinutes(), 2);
                $('[name="orario_preparazione"]').val(b);
            }
        }
    }

}

/*class ING {
 
 abilita_stampa_ECR(dati_testa_pos, dati_pos) {
 dati_pos("");
 }
 
 }*/



var riapri_comanda_forno = function (id, nome_parcheggio, id_comanda_vecchia) {


    let tavolo_di_partenza = comanda.tavolo;

    let testo_query = "SELECT ntav_comanda FROM comanda WHERE id='" + id + "' order by ntav_comanda desc limit 1;";
    let tavolo = alasql(testo_query);
    comanda.tavolo = tavolo[0].ntav_comanda;
    testo_query = "SELECT * FROM id_occupati_forno WHERE id_comanda='" + id + "' limit 1;";
    let id_occupati_forno = alasql(testo_query);
    if (id_occupati_forno.length === 0) {

        testo_query = "INSERT INTO id_occupati_forno (id_terminale,id_comanda,operatore) VALUES ('" + comanda.progr_gg_uni + "','" + id + "','" + comanda.operatore + "');";
        alasql(testo_query);
        comanda.sock.send({
            tipo: "aggiornamento_tavolo",
            operatore: comanda.operatore,
            query: testo_query,
            terminale: comanda.terminale,
            ip: comanda.ip_address
        });
    } else {

        if (id_occupati_forno[0].id_terminale === comanda.progr_gg_uni) {

        } else {

            bootbox.alert("Ordine in uso dall'utente: " + id_occupati_forno[0].operatore + "- Terminale n. " + id_occupati_forno[0].id_terminale.substr(-2));
            return false;
        }

    }



    comanda.ultimo_id = id;
    nome_parcheggio = nome_parcheggio.replace(/\<br\>\(\d+\)/g, "");
    //quando apri un ordine salvato elimina i resti precedenti
    $('[name=\'soldi_ricevuti\']').val("");
    $('[name=\'soldi_resto\']').val("");
    console.log("** LOG ORDINE", "RIAPRI COMANDA FORNO", id, nome_parcheggio);
    $('.nome_parcheggio').html(nome_parcheggio);
    var subito_sn = '';
    if (comanda.esecuzione_comanda_immediata === true) {
        subito_sn = "SUBITO";
    }

    testo_query = "UPDATE comanda SET jolly='" + subito_sn + "',stato_record='FORNO',ora_consegna='" + $('#popup_scelta_cliente input[name="ora_consegna"]').val() + "',tipo_consegna='" + comanda.tipo_consegna + "',nome_comanda='" + comanda.parcheggio + "' WHERE stato_record='ATTIVO' and id='" + id_comanda_vecchia + "';";

    comanda.sock.send({
        tipo: "tavolo_occupato",
        operatore: comanda.operatore,
        query: testo_query,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });


    comanda.esecuzione_comanda_immediata = false;
    $('#popup_scelta_cliente input[name="ora_consegna"]:visible').val("");
    comanda.tipo_consegna = "NIENTE";
    $('.tipo_consegna').removeClass('tipo_consegna_selezionata');
    $('.consegna_gap_15').removeClass('tipo_consegna_selezionata_gap');
    $('.consegna_gap_30').removeClass('tipo_consegna_selezionata_gap');
    $('#switch_esecuzione_comanda_immediata').css('background-color', '');
    alasql(testo_query);

    comanda.sock.send({
        tipo: "aggiornamento_tavolo",
        operatore: comanda.operatore,
        query: testo_query,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    testo_query = "SELECT stampata_sn FROM comanda WHERE stato_record='FORNO' and id='" + id + "' /*and ntav_comanda='" + comanda.tavolo + "'*/ order by stampata_sn desc limit 1;";
    let stampata = alasql(testo_query);
    comanda.ultima_comanda_forno_aperta_stampata = false;
    if (stampata.length === 1 && stampata[0].stampata_sn === "S") {
        comanda.ultima_comanda_forno_aperta_stampata = true;
    }

    testo_query = "UPDATE comanda SET stato_record='ATTIVO' WHERE stato_record='FORNO' and id='" + id + "' /*and ntav_comanda='" + comanda.tavolo + "'*/;";
    console.log("RIAPRI COMANDA FORNO", testo_query);
    comanda.sock.send({
        tipo: "tavolo_occupato",
        operatore: comanda.operatore,
        query: testo_query,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    alasql(testo_query);
    comanda.sock.send({
        tipo: "aggiornamento_tavolo",
        operatore: comanda.operatore,
        query: testo_query,
        terminale: comanda.terminale,
        ip: comanda.ip_address
    });
    //Prede il nome parcheggio all'apertura
    comanda.parcheggio = nome_parcheggio;
    comanda.funzionidb.conto_attivo(function () {

        intesta_cliente(false);
        $('.tasto_fiscale_bloccabile').css('pointer-events', '');
        $('.tasto_fiscale_bloccabile').css('opacity', '');
        comanda.funzionidb.conto_attivo(function () {

            righe_forno();
        });
        comanda.stampata_n = false;
    });
};
async function conto_asporto() {

    let cache_id_comanda = id_comanda_attuale_alasql();

    let numero_consegna_domicilio = "";
    if (comanda.numero_licenza_cliente === "0629") {
        if (comanda.tipo_consegna === "DOMICILIO") {

            numero_consegna_domicilio = fa_richiesta_progressivo_domicilio();
        }
    }

    var nome_parcheggio = comanda.parcheggio;
    var nome = $('#popup_scelta_cliente input[name="nome"]').val().toUpperCase();
    var cognome = $('#popup_scelta_cliente input[name="cognome"]').val().toUpperCase();
    var ragione_sociale = $('#popup_scelta_cliente input[name="ragione_sociale"]').val().toUpperCase();
    var note = $('#popup_scelta_cliente input[name="note"]').val().toUpperCase();
    test_copertura(function (stato_copertura) {
        if (stato_copertura === true) {



            var promise_progressivo = new Promise(function (resolve, reject) {

                if (comanda.persona === null && comanda.tavolo === '*ASPORTO*') {

                    var query_prog = "select numero from progressivi_asporto limit 1;";
                    comanda.sincro.query(query_prog, function (n_prog) {

                        var progressivo_attuale = parseInt(n_prog[0].numero) + 1;
                        comanda.persona = progressivo_attuale;
                        var query_nuovo_prog = "update progressivi_asporto set numero='" + progressivo_attuale + "';";
                        comanda.sincro.query(query_nuovo_prog, function () {

                            resolve(1);
                        });
                        comanda.sock.send({
                            tipo: "aggiornamento",
                            operatore: comanda.operatore,
                            query: query_nuovo_prog,
                            terminale: comanda.terminale,
                            ip: comanda.ip_address
                        });
                        comanda.sincro.query_cassa();
                    });
                } else if (comanda.persona !== null && comanda.tavolo === '*ASPORTO*') {
                    resolve(2);
                } else {
                    comanda.persona = '';
                    resolve(3);
                }
            });
            promise_progressivo.then(function () {
                var query = "select * from settaggi_profili  where id=" + comanda.folder_number + " ;";
                comanda.sincro.query(query, function (settaggi_profili) {
                    settaggi_profili = settaggi_profili[0];
                    if (comanda.pizzeria_asporto !== true && comanda.tavolo.left(7) === "ASPORTO") {
                        //CONTO DOPPIO
                        settaggi_profili.Field110 = "false";
                        //INFORNA DOPO CONTO
                        settaggi_profili.Field188 = "true";
                        settaggi_profili.Field393 = "";
                        settaggi_profili.Field394 = "";
                        settaggi_profili.Field150 = comanda.profilo_aziendale.ragione_sociale;
                        settaggi_profili.Field151 = "G";
                        settaggi_profili.Field152 = "false";
                        settaggi_profili.Field153 = comanda.profilo_aziendale.indirizzo + ", " + comanda.profilo_aziendale.numero;
                        settaggi_profili.Field154 = "N";
                        settaggi_profili.Field155 = "false";
                        settaggi_profili.Field156 = comanda.profilo_aziendale.cap + " - " + comanda.profilo_aziendale.comune + " (" + comanda.profilo_aziendale.provincia + ")";
                        settaggi_profili.Field157 = "N";
                        settaggi_profili.Field158 = "false";
                        settaggi_profili.Field159 = comanda.profilo_aziendale.telefono + " - " + comanda.profilo_aziendale.email;
                        settaggi_profili.Field160 = "N";
                        settaggi_profili.Field161 = "true";
                        settaggi_profili.Field162 = "true";
                        settaggi_profili.Field163 = "N";
                        settaggi_profili.Field164 = "true";
                        settaggi_profili.Field165 = "G";
                        settaggi_profili.Field166 = "true";
                        settaggi_profili.Field167 = "G";
                        settaggi_profili.Field168 = "G";
                        settaggi_profili.Field169 = "N";
                        settaggi_profili.Field170 = "true";
                        settaggi_profili.Field171 = "Grazie e Arrivederci";
                        settaggi_profili.Field172 = "N";
                        settaggi_profili.Field173 = "-x-";
                        settaggi_profili.Field174 = "false";
                        settaggi_profili.Field175 = "";
                        settaggi_profili.Field176 = "N";
                        settaggi_profili.Field177 = "-x-";
                        settaggi_profili.Field178 = "false";
                        settaggi_profili.Field179 = "";
                        settaggi_profili.Field180 = "N";
                        settaggi_profili.Field181 = "-x-";
                        settaggi_profili.Field230 = "true";
                        settaggi_profili.Field323 = "true";
                        settaggi_profili.Field324 = "true";
                        settaggi_profili.Field325 = "true";
                    }



                    if (comanda.persona !== null) {

                        var data = comanda.funzionidb.data_attuale();
                        var builder = new epson.ePOSBuilder();
                        var parola_conto = comanda.lang_stampa[60].replace(/\<br\/>/gi, "");
                        builder.addTextFont(builder.FONT_A);
                        builder.addTextAlign(builder.ALIGN_CENTER);
                        if (settaggi_profili.Field150.length > 0) {
                            switch (settaggi_profili.Field151) {
                                case "G":
                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                    break;
                                case "N":
                                default:
                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                            }
                            builder.addText(settaggi_profili.Field150 + '\n');
                            if (settaggi_profili.Field152 === 'true') {
                                builder.addFeedLine(1);
                            }
                        }


                        if (settaggi_profili.Field153.length > 0) {
                            switch (settaggi_profili.Field154) {
                                case "G":
                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                    break;
                                case "N":
                                default:
                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                            }
                            builder.addText(settaggi_profili.Field153 + '\n');
                            if (settaggi_profili.Field155 === 'true') {
                                builder.addFeedLine(1);
                            }
                        }


                        if (settaggi_profili.Field156.length > 0) {
                            switch (settaggi_profili.Field157) {
                                case "G":
                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                    break;
                                case "N":
                                default:
                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                            }
                            builder.addText(settaggi_profili.Field156 + '\n');
                            if (settaggi_profili.Field158 === 'true') {
                                builder.addFeedLine(1);
                            }
                        }


                        if (settaggi_profili.Field159.length > 0) {
                            switch (settaggi_profili.Field160) {
                                case "G":
                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                    break;
                                case "N":
                                default:
                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                            }
                            builder.addText(settaggi_profili.Field159 + '\n');
                        }


                        if (settaggi_profili.Field150.length > 0 || settaggi_profili.Field153.length > 0 || settaggi_profili.Field156.length > 0 || settaggi_profili.Field159.length > 0) {
                            builder.addText('\n\n');
                        }

                        builder.addTextSize(2, 2);
                        if (comanda.tavolo === '*ASPORTO*' || comanda.tavolo === '*BAR*') {
                            builder.addText(parola_conto + ' ' + comanda.tavolo + '\n');
                            builder.addFeedLine(1);
                            if (comanda.tavolo === '*ASPORTO*') {
                                builder.addText(comanda.persona + '\n');
                            }
                        } else {
                            if (settaggi_profili.Field161 === 'true') {
                                builder.addText(parola_conto);
                            }


                            if (comanda.tavolo !== 'BAR' && comanda.tavolo.left(7) !== "ASPORTO" && comanda.tavolo !== 'TAKEAWAY' && comanda.tavolo !== 'TAKE AWAY') {
                                builder.addText(' - n° ' + comanda.tavolo + '\n');
                            } else {
                                builder.addText(' \n');
                            }
                        }

                        if (settaggi_profili.Field323 === "true") {
                            builder.addText(nome + " " + cognome + '\n');
                        }


                        builder.addTextFont(builder.FONT_A);
                        builder.addTextAlign(builder.ALIGN_LEFT);
                        builder.addTextSize(1, 1);
                        builder.addFeedLine(2);
                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                        if (settaggi_profili.Field162 === 'true') {
                            switch (settaggi_profili.Field163) {
                                case "G":
                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                    break;
                                case "N":
                                default:
                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                            }
                            builder.addText(comanda.lang_stampa[109].toLowerCase().capitalize() + ': ' + comanda.operatore + '\n');
                        }

                        if (settaggi_profili.Field164 === 'true' || settaggi_profili.Field166 === 'true') {

                            if (settaggi_profili.Field164 === 'true') {
                                switch (settaggi_profili.Field165) {
                                    case "G":
                                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                                        break;
                                    case "N":
                                    default:
                                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                                }
                                builder.addText(data.substr(0, 9).replace(/-/gi, '/') + '   ');
                            }

                            if (settaggi_profili.Field166 === 'true') {
                                switch (settaggi_profili.Field167) {
                                    case "G":
                                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                                        break;
                                    case "N":
                                    default:
                                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                                }
                                builder.addText(data.substr(9, 5));
                            }

                            builder.addText('\n');
                        }
                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                        builder.addFeedLine(1);
                        builder.addTextSize(1, 1);
                        builder.addTextAlign(builder.ALIGN_CENTER);
                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                        builder.addTextSize(1, 1);
                        builder.addTextAlign(builder.ALIGN_CENTER);
                        if (comanda.stampante_piccola_conto === 'N') {
                            builder.addText("-----------------------------------------\n");
                        } else {
                            builder.addText("--------------------------------\n");
                        }
                        builder.addTextAlign(builder.ALIGN_LEFT);
                        var quantita_totale = 0;
                        var prezzo_righe = 0;
                        var pizza_metro = false;
                        $('#conto tr').not('.non-contare-riga').each(function () {

                            var portata = $(this).attr("portata");
                            if (portata !== undefined && portata === "ZZZ") {

                                /*NIENTE*/
                            } else {

                                var articolo = $(this).find('td:nth-child(2)').html().replace(/&nbsp;/g, '');
                                var quantita = $(this).find('td:nth-child(1)').html();
                                var prezzo = $(this).find('td:nth-child(4)').html().replace(/&euro;/g, '');
                                var categoria = $(this).attr("categoria");
                                prezzo = prezzo.replace(/€/g, '');
                                if (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(" && articolo[0] !== "*" && articolo[0] !== "x" && articolo[0] !== "=") {

                                    switch (settaggi_profili.Field168) {
                                        case "D":

                                            builder.addTextSize(2, 2);
                                            break;
                                        case "G":

                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                            break;
                                        case "N":

                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                            break;
                                        default:

                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                    }
                                    if (articolo.indexOf('SERVIZIO') === -1 && articolo.indexOf('CONSEGNA') === -1) {
                                        //builder.addText(quantita);
                                        quantita_totale += parseInt(quantita);
                                    }
                                    builder.addTextPosition(0);
                                    switch (settaggi_profili.Field168) {
                                        case "D":


                                            if (categoria === settaggi_profili.Field393 || categoria === settaggi_profili.Field394) {
                                                builder.addTextStyle(true, false, false, builder.COLOR_1);
                                            }
                                            break;
                                        case "G":


                                            if (categoria === settaggi_profili.Field393 || categoria === settaggi_profili.Field394) {
                                                builder.addTextStyle(true, false, true, builder.COLOR_1);
                                            }
                                            break;
                                        case "N":


                                            if (categoria === settaggi_profili.Field393 || categoria === settaggi_profili.Field394) {
                                                builder.addTextStyle(true, false, false, builder.COLOR_1);
                                            }
                                            break;
                                        default:


                                            if (categoria === settaggi_profili.Field393 || categoria === settaggi_profili.Field394) {
                                                builder.addTextStyle(true, false, false, builder.COLOR_1);
                                            }
                                    }
                                } else {
                                    builder.addTextPosition(0);
                                    switch (settaggi_profili.Field169) {
                                        case "D":
                                            builder.addTextSize(2, 2);
                                            break;
                                        case "G":
                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                            break;
                                        case "N":
                                        default:
                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                    }

                                    articolo = articolo.replace(/\*/gi, '');
                                    articolo = articolo.replace(/\=/gi, '');
                                    articolo = '   ' + articolo;
                                }



                                builder.addTextAlign(builder.ALIGN_RIGHT);
                                var rf_prezzo_un = "0.00";
                                var rf_prezzo_tot = "0.00";
                                if (prezzo.substr(0, 1) !== "-") {
                                    //builder.addText('€ ' + (parseFloat(prezzo) * parseInt(quantita)).toFixed(2) + '\n');
                                    rf_prezzo_un = parseFloat(prezzo).toFixed(2);
                                    rf_prezzo_tot = (parseFloat(prezzo) * parseInt(quantita)).toFixed(2);
                                    prezzo_righe += prezzo * quantita;
                                } else {
                                    //builder.addText(prezzo + '\n');
                                    rf_prezzo_un = parseFloat(prezzo).toFixed(2);
                                    rf_prezzo_tot = parseFloat(prezzo).toFixed(2);
                                }

                                /* 15-10-2019 VA BENE SULLA PIZZA DUE GUSTI */
                                /* MA NON SU QUELLA AL METRO PERCHE TOGLIE IL PREZZO */
                                if (pizza_metro === false && articolo.trim().substr(0, 3) === "1/2") {
                                    quantita = "";
                                    rf_prezzo_un = "";
                                    rf_prezzo_tot = "";
                                } else if (articolo.trim()[0] !== "+" && articolo.trim()[0] !== "-" && articolo.trim()[0] !== "(" && articolo.trim()[0] !== "*" && articolo.trim()[0] !== "x" && articolo.trim()[0] !== "=" && articolo.trim() !== "CONSEGNA") {

                                    if (articolo.trim().substr(0, 3) !== "1/2" && articolo.trim().substr(0, 3) !== "1/4") {
                                        if (articolo.indexOf("METRO") !== -1) {
                                            pizza_metro = true;
                                        } else {
                                            pizza_metro = false;
                                        }
                                    }


                                } else {
                                    quantita = "";
                                    rf_prezzo_un = "";
                                }

                                numero_caratteri = 42;
                                if (comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].nome === 'CUBO') {
                                    numero_caratteri = 48;
                                }

                                builder.addText(riga_conto_formattata(articolo, quantita, rf_prezzo_un, rf_prezzo_tot, numero_caratteri));
                                builder.addTextAlign(builder.ALIGN_LEFT);
                            }
                        });
                        var cifra_sconto = 0;
                        $('#intestazioni_conto tr').not('.non-contare-riga').each(function () {

                            var quantita = $(this).find('td:nth-child(1)').html();
                            var prezzo = $(this).find('td:nth-child(4)').html().replace(/&euro;/g, '');
                            var articolo = $(this).find('td:nth-child(2)').html().replace(/&nbsp;/g, '');
                            if (articolo !== comanda.lang_stampa[15].toUpperCase()) {
                                articolo = comanda.lang_stampa[58].toUpperCase();
                                builder.addTextAlign(builder.ALIGN_RIGHT);
                                if (prezzo.slice(-1) === '%') {
                                    articolo += ' ' + parseInt(prezzo.slice(1, -1)) + '%';
                                } else {
                                    articolo += ' € ' + parseFloat(prezzo.substr(1)).toFixed(2);
                                }
                                builder.addTextAlign(builder.ALIGN_LEFT);
                            } else {
                                //CALCOLO COPERTI DA TOGLIERE DAL TOTALE PER SERVIZIO
                                prezzo_righe += prezzo * quantita;
                            }

                            console.log("PREZZO SCONTO", prezzo);
                            prezzo = prezzo.replace(/€/g, '');
                            if (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(" && articolo[0] !== "*" && articolo[0] !== "x" && articolo[0] !== "=") {

                                switch (settaggi_profili.Field168) {
                                    case "D":
                                        builder.addTextSize(2, 2);
                                        break;
                                    case "G":
                                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                                        break;
                                    case "N":
                                    default:
                                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                                }
                                //quantita_totale += parseInt(quantita);
                                if (articolo.indexOf(comanda.lang_stampa[15]) !== -1) {
                                    builder.addText(quantita);
                                    builder.addTextPosition(35);
                                }

                            } else {
                                switch (settaggi_profili.Field169) {
                                    case "D":
                                        builder.addTextSize(2, 2);
                                        break;
                                    case "G":
                                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                                        break;
                                    case "N":
                                    default:
                                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                                }

                                if (articolo[0] === "=") {
                                    articolo = '\t' + articolo.substr(1);
                                } else {
                                    articolo = '\t' + articolo;
                                }
                            }

                            builder.addText(articolo.slice(0, 27));
                            builder.addTextAlign(builder.ALIGN_RIGHT);
                            if (prezzo.substr(0, 1) !== "-") {
                                cifra_sconto = prezzo;
                                builder.addTextPosition(468);
                                builder.addText('€');
                                if (comanda.stampante_piccola_conto === 'N') {

                                    if (cifra_sconto.length === 4) {
                                        builder.addTextPosition(528);
                                    } else {
                                        builder.addTextPosition(515);
                                    }
                                } else {
                                    builder.addTextPosition(276);
                                }

                                builder.addText(cifra_sconto + '\n');
                            } else if (prezzo.slice(-1) === '%') {
                                console.log("CALCOLO SCONTO PERC", prezzo_righe, prezzo.slice(1, -1), prezzo_righe / 100 * parseInt(prezzo.slice(1, -1)));
                                cifra_sconto = (prezzo_righe / 100 * parseInt(prezzo.slice(1, -1))).toFixed(2);
                                builder.addTextPosition(468);
                                builder.addText('€');
                                if (comanda.stampante_piccola_conto === 'N') {
                                    if (cifra_sconto.length === 4) {
                                        builder.addTextPosition(528);
                                    } else {
                                        builder.addTextPosition(515);
                                    }
                                } else {
                                    builder.addTextPosition(276);
                                }

                                builder.addText(cifra_sconto + '\n');
                            } else {
                                cifra_sconto = prezzo.substr(1);
                                builder.addTextPosition(468);
                                builder.addText('€');
                                if (comanda.stampante_piccola_conto === 'N') {
                                    if (cifra_sconto.length === 4) {
                                        builder.addTextPosition(528);
                                    } else {
                                        builder.addTextPosition(515);
                                    }
                                } else {
                                    builder.addTextPosition(276);
                                }

                                builder.addText(cifra_sconto + '\n');
                            }

                            builder.addTextAlign(builder.ALIGN_LEFT);
                        });
                        builder.addTextAlign(builder.ALIGN_CENTER);
                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                        builder.addTextSize(1, 1);
                        builder.addTextAlign(builder.ALIGN_CENTER);
                        if (comanda.stampante_piccola_conto === 'N') {
                            builder.addText("-----------------------------------------\n");
                        } else {
                            builder.addText("--------------------------------\n");
                        }
                        builder.addTextAlign(builder.ALIGN_LEFT);
                        //totale
                        //tasse
                        builder.addFeedLine(1);
                        var totale = $('#totale_scontrino').html();
                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                        //QUANTITA TOTALE
                        if (settaggi_profili.Field170 === 'true') {
                            builder.addText(quantita_totale);
                            builder.addTextPosition(35);
                            builder.addText(comanda.lang_stampa[53]); //quantita
                        }
                        //TOTALE

                        if (comanda.lingua_stampa === 'deutsch') {
                            if (comanda.stampante_piccola_conto === 'N') {
                                builder.addTextPosition(310);
                            } else {
                                builder.addTextPosition(180);
                            }
                            builder.addText('Totale: ');
                            builder.addText('€ ' + totale + '\n');
                        } else if (comanda.lingua_stampa === 'italiano') {
                            /*if (comanda.stampante_piccola_conto === 'N') {
                             builder.addTextPosition(310);
                             } else {
                             builder.addTextPosition(180);
                             }*/
                            //builder.addText(comanda.lang_stampa[70].capitalize() + ': ');
                            //builder.addText('€ ' + totale + '\n');

                            builder.addTextPosition(160);
                            if (comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].nome === 'TMT88-Vi') {
                                builder.addTextPosition(280);
                            } else if (comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].nome === 'CUBO') {
                                builder.addTextPosition(360);
                            }
                            builder.addText('Totale: ');
                            builder.addText('€ ' + riga_conto_totale(totale) + '\n');
                        }


                        //A PERSONA
                        if (parseInt($('#numero_coperti_effettivi').html()) > 1) {
                            if (comanda.stampante_piccola_conto === 'N') {

                                builder.addTextPosition(274);
                            } else {
                                builder.addTextPosition(144);
                            }
                            builder.addText('A persona: ');
                            builder.addText('€ ' + parseFloat(parseFloat(totale) / parseInt($('#numero_coperti_effettivi').html())).toFixed(2) + '\n');
                        }



                        //SETTAGGI DEL MESSAGGIO FINALE
                        if (settaggi_profili.Field171.trim().length > 0 || settaggi_profili.Field175.trim().length > 0 || settaggi_profili.Field179.trim().length > 0) {
                            builder.addFeedLine(2);
                        }

                        if (settaggi_profili.Field172 === 'G')
                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                        else if (settaggi_profili.Field172 === 'N')
                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                        if (settaggi_profili.Field173 === 'x--')
                            builder.addTextAlign(builder.ALIGN_LEFT);
                        else if (settaggi_profili.Field173 === '-x-')
                            builder.addTextAlign(builder.ALIGN_CENTER);
                        else if (settaggi_profili.Field173 === '--x')
                            builder.addTextAlign(builder.ALIGN_RIGHT);
                        if (settaggi_profili.Field171.trim().length > 0)
                            builder.addText(settaggi_profili.Field171 + '\n');
                        if (settaggi_profili.Field174 === 'true')
                            builder.addFeedLine(1);
                        if (settaggi_profili.Field176 === 'G')
                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                        else if (settaggi_profili.Field176 === 'N')
                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                        if (settaggi_profili.Field177 === 'x--')
                            builder.addTextAlign(builder.ALIGN_LEFT);
                        else if (settaggi_profili.Field177 === '-x-')
                            builder.addTextAlign(builder.ALIGN_CENTER);
                        else if (settaggi_profili.Field177 === '--x')
                            builder.addTextAlign(builder.ALIGN_RIGHT);
                        if (settaggi_profili.Field175.trim().length > 0)
                            builder.addText(settaggi_profili.Field175 + '\n');
                        if (settaggi_profili.Field178 === 'true')
                            builder.addFeedLine(1);
                        if (settaggi_profili.Field180 === 'G')
                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                        else if (settaggi_profili.Field180 === 'N')
                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                        if (settaggi_profili.Field181 === 'x--')
                            builder.addTextAlign(builder.ALIGN_LEFT);
                        else if (settaggi_profili.Field181 === '-x-')
                            builder.addTextAlign(builder.ALIGN_CENTER);
                        else if (settaggi_profili.Field181 === '--x')
                            builder.addTextAlign(builder.ALIGN_RIGHT);
                        if (settaggi_profili.Field179.trim().length > 0)
                            builder.addText(settaggi_profili.Field179 + '\n');
                        //FINE SETTAGGI MESSAGGIO FINALE

                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                        builder.addFeedLine(2);
                        if (comanda.tipo_consegna === "DOMICILIO") {
                            builder.addTextAlign(builder.ALIGN_CENTER);
                            var indirizzo = $('#popup_scelta_cliente input[name="indirizzo"]').val().toUpperCase();
                            var numero = $('#popup_scelta_cliente input[name="numero"]').val().toUpperCase();
                            var cap = $('#popup_scelta_cliente input[name="cap"]').val();
                            var citta = $('#popup_scelta_cliente input[name="comune"]').val().toUpperCase();
                            var provincia = $('#popup_scelta_cliente input[name="provincia"]').val().toUpperCase();

                            var email = $('#popup_scelta_cliente input[name="email"]').val().toUpperCase();

                            var cellulare = $('#popup_scelta_cliente input[name="cellulare"]').val();
                            var telefono = $('#popup_scelta_cliente input[name="telefono"]').val();
                            var telefono_3 = $('#popup_scelta_cliente input[name="telefono_3"]').val();
                            var telefono_4 = $('#popup_scelta_cliente input[name="telefono_4"]').val();
                            var telefono_5 = $('#popup_scelta_cliente input[name="telefono_5"]').val();
                            var telefono_6 = $('#popup_scelta_cliente input[name="telefono_6"]').val();
                            var destinazione = indirizzo + " ," + numero + " " + citta + " " + " " + cap + " " + provincia;
                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                            builder.addTextSize(1, 1);
                            builder.addTextAlign(builder.ALIGN_CENTER);
                            if (comanda.stampante_piccola_conto === 'N') {
                                builder.addText("-----------------------------------------\n");
                            } else {
                                builder.addText("--------------------------------\n");
                            }

                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                            builder.addText('DATI CLIENTE:\n');
                            
                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                            if (settaggi_profili.Field230 === 'true' && settaggi_profili.Field513==="D") {
                             
                                if (comanda.tipo_consegna !== "RITIRO" && note.length > 0) {
                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                } else {
                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                }

                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                if(settaggi_profili.Field513==="N"){
                                    builder.addTextSize(1, 1);
                                }
                                if(settaggi_profili.Field513==="D")
                                {
                                    builder.addTextSize(2, 2);
                                }
                                if (nome === "" && cognome === "") {
                                    nome = nome_parcheggio;
                                }
                                builder.addText(nome + " " + cognome + '\n');
                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                if (ragione_sociale.length >= 3) {
                                    builder.addText(ragione_sociale + '\n');
                                }
                                builder.addText(indirizzo + "," + numero + '\n');
                                if (cap) {
                                    builder.addText(cap + " " + citta + " " + provincia + '\n');
                                } else {
                                    builder.addText(citta + " " + provincia + '\n');
                                }



                                if (cellulare) {
                                    builder.addText(cellulare);
                                }
                                if (cellulare && telefono) {
                                    builder.addText(" - ");
                                }
                                if (telefono) {
                                    builder.addText(telefono);
                                }
                                if (cellulare || telefono) {
                                    builder.addText("\n");
                                }

                                if (comanda.tipo_consegna !== "RITIRO" && note.length > 0) {
                                    //builder.addFeedLine(1);
                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                    /*builder.addText("Note: ");*/

                                    builder.addText("Note: " + note.substr(0, 30) + "\n");
                                    /*if (note.length > 20) {
                                        builder.addText(note.substr(0, 30));
                                    } else {
                                        builder.addText(note);
                                    }*/
                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                    /* builder.addText('\n');*/
                                }

                                if (settaggi_profili.Field325 === "true") {
                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                    builder.addText('CONSEGNA' + ' ' + $('#popup_scelta_cliente input[name="ora_consegna"]').val() + '\n');
                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                }

                                builder.addTextAlign(builder.ALIGN_CENTER);
                                builder.addSymbol("https://www.google.com/maps/dir/?api=1&destination=" + destinazione.replace(/\s+/g, "+") + "&travelmode=DRIVING", builder.SYMBOL_QRCODE_MODEL_2, builder.LEVEL_DEFAULT, 4);
                            
                            }
                           else if (settaggi_profili.Field230 === 'true' && settaggi_profili.Field513==="N" ) {
                                builder.addPageBegin();
                                if (comanda.tipo_consegna !== "RITIRO" && note.length > 0) {
                                    builder.addPageArea(40, 0, 330, 230);
                                } else {
                                    builder.addPageArea(40, 0, 330, 200);
                                }

                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                if(settaggi_profili.Field513==="N"){
                                    builder.addTextSize(1, 1);
                                }
                                if(settaggi_profili.Field513==="D")
                                {
                                    builder.addTextSize(2, 2);
                                }
                                if (nome === "" && cognome === "") {
                                    nome = nome_parcheggio;
                                }
                                builder.addText(nome + " " + cognome + '\n');
                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                if (ragione_sociale.length >= 3) {
                                    builder.addText(ragione_sociale + '\n');
                                }
                                builder.addText(indirizzo + "," + numero + '\n');
                                if (cap) {
                                    builder.addText(cap + " " + citta + " " + provincia + '\n');
                                } else {
                                    builder.addText(citta + " " + provincia + '\n');
                                }



                                if (cellulare) {
                                    builder.addText(cellulare);
                                }
                                if (cellulare && telefono) {
                                    builder.addText(" - ");
                                }
                                if (telefono) {
                                    builder.addText(telefono);
                                }
                                if (cellulare || telefono) {
                                    builder.addText("\n");
                                }

                                if (comanda.tipo_consegna !== "RITIRO" && note.length > 0) {
                                    //builder.addFeedLine(1);
                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                    /*builder.addText("Note: ");*/

                                    builder.addText("Note: " + note.substr(0, 30) + "\n");
                                    /*if (note.length > 20) {
                                        builder.addText(note.substr(0, 30));
                                    } else {
                                        builder.addText(note);
                                    }*/
                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                    /* builder.addText('\n');*/
                                }

                                if (settaggi_profili.Field325 === "true") {
                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                    builder.addText('CONSEGNA' + ' ' + $('#popup_scelta_cliente input[name="ora_consegna"]').val() + '\n');
                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                }

                                builder.addPageArea(370, 0, 300, 200);
                                builder.addSymbol("https://www.google.com/maps/dir/?api=1&destination=" + destinazione.replace(/\s+/g, "+") + "&travelmode=DRIVING", builder.SYMBOL_QRCODE_MODEL_2, builder.LEVEL_DEFAULT, 4);
                                builder.addPageEnd();
                            } 
                            
                            
                            
                          else  if(settaggi_profili.Field513==="N" ) {
                                builder.addTextAlign(builder.ALIGN_CENTER);
                                builder.addFeedLine(1);
                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                builder.addTextSize(1, 1);
                                builder.addText(nome + " " + cognome + '\n');
                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                if (ragione_sociale.length >= 3) {
                                    builder.addText(ragione_sociale + '\n');
                                }
                                builder.addText(indirizzo + "," + numero + '\n');
                                if (cap) {
                                    builder.addText(cap + " " + citta + " " + provincia + '\n');
                                } else {
                                    builder.addText(citta + " " + provincia + '\n');
                                }


                                if (cellulare) {
                                    builder.addText(cellulare);
                                }
                                if (cellulare && telefono) {
                                    builder.addText(" - ");
                                }
                                if (telefono) {
                                    builder.addText(telefono);
                                }
                                if (cellulare || telefono) {
                                    builder.addText("\n");
                                }

                                if (comanda.tipo_consegna !== "RITIRO" && note.length > 0) {
                                    //builder.addFeedLine(1);
                                    builder.addTextStyle(false, false, true, builder.COLOR_1);

                                    builder.addText("Note: " + note.substr(0, 30) + "\n");

                                    /*builder.addText("Note: ");*/
                                    /*if (note.length > 20) {
                                        builder.addText(note.substr(0, 30));
                                    } else {
                                        builder.addText(note);
                                    }*/

                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                    /*builder.addText('\n');*/
                                }

                                if (settaggi_profili.Field325 === "true") {
                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                    builder.addText('CONSEGNA' + ' ' + $('#popup_scelta_cliente input[name="ora_consegna"]').val() + '\n');
                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                }

                                builder.addTextAlign(builder.ALIGN_CENTER);
                            }
                           else if(settaggi_profili.Field513==="D" ) {
                                builder.addTextAlign(builder.ALIGN_CENTER);
                                builder.addFeedLine(1);
                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                builder.addTextSize(2, 2);
                                builder.addText(nome + " " + cognome + '\n');
                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                if (ragione_sociale.length >= 3) {
                                    builder.addText(ragione_sociale + '\n');
                                }
                                builder.addText(indirizzo + "," + numero + '\n');
                                if (cap) {
                                    builder.addText(cap + " " + citta + " " + provincia + '\n');
                                } else {
                                    builder.addText(citta + " " + provincia + '\n');
                                }


                                if (cellulare) {
                                    builder.addText(cellulare);
                                }
                                if (cellulare && telefono) {
                                    builder.addText(" - ");
                                }
                                if (telefono) {
                                    builder.addText(telefono);
                                }
                                if (cellulare || telefono) {
                                    builder.addText("\n");
                                }

                                if (comanda.tipo_consegna !== "RITIRO" && note.length > 0) {
                                    //builder.addFeedLine(1);
                                    builder.addTextStyle(false, false, true, builder.COLOR_1);

                                    builder.addText("Note: " + note.substr(0, 30) + "\n");

                                    /*builder.addText("Note: ");*/
                                    /*if (note.length > 20) {
                                        builder.addText(note.substr(0, 30));
                                    } else {
                                        builder.addText(note);
                                    }*/

                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                    /*builder.addText('\n');*/
                                }

                                if (settaggi_profili.Field325 === "true") {
                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                    builder.addText('CONSEGNA' + ' ' + $('#popup_scelta_cliente input[name="ora_consegna"]').val() + '\n');
                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                }

                                builder.addTextAlign(builder.ALIGN_CENTER);
                            }
                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                            builder.addTextSize(1, 1);
                            builder.addTextAlign(builder.ALIGN_CENTER);
                            if (comanda.stampante_piccola_conto === 'N') {
                                builder.addText("-----------------------------------------\n");
                            } else {
                                builder.addText("--------------------------------\n");
                            }
                        } else if (settaggi_profili.Field324 === "true") {

                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                            builder.addTextSize(1, 1);
                            builder.addTextAlign(builder.ALIGN_CENTER);
                            builder.addTextAlign(builder.ALIGN_CENTER);
                            if (comanda.stampante_piccola_conto === 'N') {
                                builder.addText("-----------------------------------------\n");
                            } else {
                                builder.addText("--------------------------------\n");
                            }


                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                            builder.addText('DATI CLIENTE:\n');
                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                            if(settaggi_profili.Field513==="N"){
                                builder.addTextSize(1, 1);
                            }
                            if(settaggi_profili.Field513==="D")
                            {
                                builder.addTextSize(2, 2);
                            }
                            if (nome === "" && cognome === "") {
                                nome = nome_parcheggio;
                            }
                            builder.addText(nome + " " + cognome + '\n');
                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                            if (ragione_sociale.length >= 3) {
                                builder.addText(ragione_sociale + '\n');
                            }
                            builder.addFeedLine(1);
                            if (comanda.tipo_consegna !== "RITIRO" && note.length > 0) {
                                //builder.addFeedLine(1);
                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                /*builder.addText("Note: ");*/
                                /*if (note.length > 20) {
                                    builder.addText(note.substr(0, 30));
                                } else {
                                    builder.addText(note);
                                }*/
                                builder.addText("Note: " + note.substr(0, 30) + "\n");

                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                /*builder.addText('\n');*/
                            }

                            if (settaggi_profili.Field325 === "true") {
                                builder.addText('CONSEGNA' + ' ' + $('#popup_scelta_cliente input[name="ora_consegna"]').val() + '\n');
                            }

                            builder.addTextAlign(builder.ALIGN_CENTER);
                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                            builder.addTextSize(1, 1);
                            builder.addTextAlign(builder.ALIGN_CENTER);
                            if (comanda.stampante_piccola_conto === 'N') {
                                builder.addText("-----------------------------------------\n");
                            } else {
                                builder.addText("--------------------------------\n");
                            }

                        }



                        if (comanda.numero_licenza_cliente === "0629") {
                            if (comanda.tipo_consegna === "DOMICILIO") {
                                builder.addTextSize(2, 2);
                                builder.addText("Num. Consegna: " + numero_consegna_domicilio + "\n");
                                builder.addTextSize(1, 1);
                            }
                        }

                        builder.addFeedLine(2);

                        if (settaggi_profili.Field499 === 'true') {
                            builder.addBarcode('{A#' + cache_id_comanda, builder.BARCODE_CODE128, builder.HRI_NONE, builder.FONT_A, 2, 100);
                        }

                        builder.addCut(builder.CUT_FEED);
                        var request = builder.toString();
                        //CONTENUTO CONTO

                        //Create a SOAP envelop
                        var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
                        var url = 'http://' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                        if (comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].intelligent === "SDS") {
                            url = 'http://' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "") + numero_layout].ip + ':8000/SPOOLER?stampante=' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].devid_nf;
                        }
                        var sconto_perc = 0;
                        var sconto_imp = 0;
                        var tipo_sconto = 'N';
                        //SCONTI PERCENTUALI
                        $('#intestazioni_conto tr:contains(%)').not(':contains(' + comanda.lang[15] + ')').each(function (index, element) {
                            sconto_perc += parseFloat($(element).find('td:nth-child(4)').html().slice(1, -1).replace(',', '.'));
                            console.log("CALCOLO SCONTISTICA TOTALI", $(element).find('td:nth-child(4)').html().slice(1, -1), typeof ($(element).find('td:nth-child(4)').html().slice(1, -1)));
                            tipo_sconto = 'P';
                        });
                        //SCONTI IMPONIBILI
                        $('#intestazioni_conto tr').not(':contains(%)').not(':contains(' + comanda.lang[15] + ')').each(function (index, element) {
                            sconto_imp += parseFloat($(element).find('td:nth-child(4)').html().substr(1).replace(',', '.'));
                            console.log("CALCOLO SCONTISTICA TOTALI", $(element).find('td:nth-child(4)').html(), typeof ($(element).find('td:nth-child(4)').html()));
                            tipo_sconto = 'F';
                        });
                        //CALCOLO PERCENTUALE IN BASE ALLO SCONTO FISSO
                        var netto_dopo_sconto = parseFloat($('#totale_scontrino').html().replace(',', '.'));
                        var totale = netto_dopo_sconto + sconto_imp;
                        //Lo sommo alla percentuale che c'era già quindi 0
                        sconto_perc += 100 / (totale / sconto_imp);
                        if (tipo_sconto === 'P' && isNaN(sconto_perc) === true) {
                            sconto_perc = 100;
                        } else if (tipo_sconto === 'N') {
                            sconto_perc = 0;
                        }

                        console.log("CALCOLO SCONTISTICA TOTALI", totale, sconto_imp, sconto_perc, 100 / (totale / sconto_imp));
                        var data = comanda.funzionidb.data_attuale().substr(0, 8);
                        var ora = comanda.funzionidb.data_attuale().substr(9, 8);
                        var testo_query = "update comanda set tipo_ricevuta='conto', sconto_perc='" + sconto_perc.toFixed(2) + "',sconto_imp='" + tipo_sconto + "',netto='" + cifra_sconto + "',data_stampa='" + data + "',ora_stampa='" + ora + "' where desc_art!='EXTRA' and ntav_comanda='" + comanda.tavolo + "' and stato_record='ATTIVO'; ";
                        comanda.sock.send({
                            tipo: "tavolo_occupato",
                            operatore: comanda.operatore,
                            query: testo_query,
                            terminale: comanda.terminale,
                            ip: comanda.ip_address
                        });
                        alasql(testo_query);


                        calcola_importi_fiscali();

                        let id_stampante_di_conto = alasql("select id from nomi_stampanti where ip= '" + comanda.intelligent_non_fiscale + "' and italiano ='" + comanda.lang_stampa[60] + "' OR english = '" + comanda.lang_stampa[60] + "' OR deutsch= '" + comanda.lang_stampa[60] + "' limit 1;");
                        let controllo_doppia_conto = alasql("select doppia from nomi_stampanti where id='" + id_stampante_di_conto[0].id + "' limit 1;");


                        if (settaggi_profili.Field110 === 'true' || controllo_doppia_conto[0].doppia !== null && controllo_doppia_conto[0].doppia === "true") {
                            setTimeout(function () {
                                aggiungi_spool_stampa(url, soap, "CONTO");
                            }, 100);
                        }

                        aggiungi_spool_stampa(url, soap, "CONTO");
                        if (comanda.apertura_conto === "true") {
                            apri_cassetto();
                        }


                    }
                });
            });
        }
    });
}

let timeout_inforna_conto_asporto = function () { };

function stampa(operazione) {
    $(".css_tasti_incasso").css("pointer-events", "none");
    $(".css_tasti_incasso").css("opacity", "0.8");
    //Il tipo di operazione dev'essere Array

    //Esempio: new Array("comanda","conto")
    //OPPURE
    //("comanda","scontrino")

    if (operazione.constructor === Array) {

        if (operazione.indexOf("fattura") !== -1 || operazione.indexOf("scontrino") !== -1 || operazione.indexOf("conferma_metodo_pagamento") !== -1) {
            if (alert_aperto_misuratore === true) {
                bootbox.alert("La cassa non ha letto i reparti dal misuratore e può lavorare solo in modalità provvisoria NON fiscale!<br><br> Se non ti va bene, accendi il misuratore e riavvia fast.intellinet");
                return false;
            }
        }

        //O NON E' FATTURA E SCONTRINO OPPURE DEVONO ESSERCI DELLE RIGHE DA STAMPARE
        if ((operazione.indexOf("fattura") === -1 && operazione.indexOf("scontrino") === -1 && operazione.indexOf("conferma_metodo_pagamento") === -1) ||
            $(comanda.conto).not(comanda.intestazione_conto).find('tr[id^="art_"]:contains("€")').not('.fiscalizzata').not('.non-contare-riga').length > 0) {

            //intesta cliente solo se non è uno scontrino
            //nb se è scontrino piu comanda però va intestato
            if (operazione.indexOf("conferma_metodo_pagamento") !== -1 && operazione.indexOf("scontrino") !== -1 && operazione.indexOf("comanda") !== -1) {
                if (comanda.tipo_consegna === "NIENTE") {
                    intesta_cliente();
                    $(".css_tasti_incasso").css("pointer-events", "");
                    $(".css_tasti_incasso").css("opacity", "");
                    return false;
                }

            } else if (operazione.indexOf("scontrino") === -1) {

                var partita_iva = $('#popup_scelta_cliente form [name="partita_iva"]').val();
                var partita_iva_estera = $('#popup_scelta_cliente form [name="partita_iva_estera"]').val();
                if (operazione.indexOf("fattura") !== -1 || operazione.indexOf("FATTURA") !== -1) {
                    if (partita_iva.length === 11) {
                        var a = ControllaPIVA(partita_iva);
                        if (a !== true) {
                            alert(a);
                            intesta_cliente();
                            $(".css_tasti_incasso").css("pointer-events", "");
                            $(".css_tasti_incasso").css("opacity", "");
                            return false;
                        }
                    } else if (partita_iva_estera.length > 3) {
                        /*va vanti*/
                    } else if (partita_iva === '') {
                        alert("Bisogna inserire OBBLIGATORIAMENTE la Partita IVA per emettere fattura.");
                        intesta_cliente();
                        $(".css_tasti_incasso").css("pointer-events", "");
                        $(".css_tasti_incasso").css("opacity", "");
                        return false;
                    } else {
                        alert("Errore: il formato della Partita IVA introdotta non � valido.");
                        intesta_cliente();
                        $(".css_tasti_incasso").css("pointer-events", "");
                        $(".css_tasti_incasso").css("opacity", "");
                        return false;
                    }
                }

                if (comanda.tipo_consegna === "NIENTE") {
                    intesta_cliente();
                    $(".css_tasti_incasso").css("pointer-events", "");
                    $(".css_tasti_incasso").css("opacity", "");
                    return false;
                }

            }



            var query = "select * from settaggi_profili where id=" + comanda.folder_number + " ;";
            comanda.sincro.query(query, function (settaggi_profili) {
                settaggi_profili = settaggi_profili[0];
                if (comanda.pizzeria_asporto !== true && comanda.tavolo.left(7) === "ASPORTO" && operazione.indexOf('conto') !== -1) {
                    //CONTO DOPPIO
                    settaggi_profili.Field110 = "false";
                    //INFORNA DOPO CONTO
                    settaggi_profili.Field188 = "true";
                }

                /*if (comanda.pizzeria_asporto !== true && comanda.tavolo.left(7) === "ASPORTO" && operazione.indexOf('comanda') !== -1) {
                 //+conto                    
                 settaggi_profili.Field108 = "false";
                 //doppia              
                 settaggi_profili.Field109 = "false";
                 //INFORNA DOPO comanda
                 settaggi_profili.Field187 = "true";
                 }*/

                operazione.forEach(function (obj) {

                    switch (obj) {

                        case "comanda":
                            console.log("** LOG ORDINE", "STAMPA COMANDA");
                            $('#tipo_consegna_modal').html("");
                            $('#tipo_consegna_modal_tipo').html("" + "");
                            stampa_comanda_test();
                            break;
                        case "conto":
                            console.log("** LOG ORDINE", "STAMPA CONTO");
                            $('#tipo_consegna_modal').html("");
                            $('#tipo_consegna_modal_tipo').html("" + "");
                            conto_asporto();
                            break;
                        case "scontrino":


                            console.log("** LOG ORDINE", "STAMPA SCONTRINO");
                            $('.tasto_fiscale_bloccabile').css('pointer-events', 'none');
                            $('.tasto_fiscale_bloccabile').css('opacity', '0.6');
                            /*stampa_scontrino_asporto('SCONTRINO FISCALE');*/
                            stampa_scontrino('SCONTRINO FISCALE');
                            break;
                        case "fattura":

                            console.log("** LOG ORDINE", "STAMPA FATTURA");
                            $('.tasto_fiscale_bloccabile').css('pointer-events', 'none');
                            $('.tasto_fiscale_bloccabile').css('opacity', '0.6');
                            /* stampa_scontrino_asporto('FATTURA');*/
                            stampa_scontrino('FATTURA');
                            break;
                        case "pagamento":
                            console.log("** LOG ORDINE", "CONFERMA_PAGAMENTO");
                            stampa_comanda_test();
                            break;
                        default:

                        case "conferma_metodo_pagamento":
                            conferma_metodo_pagamento(false, true);
                            break;
                    }


                });
                timeout_inforna_conto_asporto = setTimeout(function () {

                    if (operazione.indexOf('conferma_metodo_pagamento') !== -1 || operazione.indexOf('scontrino') !== -1 || operazione.indexOf('fattura') !== -1) {
                        if (operazione.indexOf('scontrino') !== -1 || operazione.indexOf('conferma_metodo_pagamento') !== -1) {
                            inforna(true, true);
                            $(".css_tasti_incasso").css("pointer-events", "");
                            $(".css_tasti_incasso").css("opacity", "");
                        } else {
                            inforna(true);
                            $(".css_tasti_incasso").css("pointer-events", "");
                            $(".css_tasti_incasso").css("opacity", "");
                        }
                    } else if (operazione.indexOf('conto') !== -1) {
                        if (settaggi_profili.Field188 === 'true') {
                            inforna(true);
                            $(".css_tasti_incasso").css("pointer-events", "");
                            $(".css_tasti_incasso").css("opacity", "");
                        } else {
                            $(".css_tasti_incasso").css("pointer-events", "");
                            $(".css_tasti_incasso").css("opacity", "");
                        }
                    }
                    //è un elseif perchè o fa un inforna o l'altro e quello del conto ovviamente è il più importante dei due
                    else if (operazione.indexOf('comanda') !== -1) {
                        if (settaggi_profili.Field187 === 'true') {
                            inforna(true);
                            $(".css_tasti_incasso").css("pointer-events", "");
                            $(".css_tasti_incasso").css("opacity", "");
                        } else {
                            $(".css_tasti_incasso").css("pointer-events", "");
                            $(".css_tasti_incasso").css("opacity", "");
                        }
                    }
                    //a volte può impedire la stampa di scontrini se inforna prima che legga le righe
                }, 2000);
            });
        } else {
            bootbox.alert("Non è possibile procedere. E' già stato emesso un documento fiscale per questo ordine. (indice 2)")
            $(".css_tasti_incasso").css("pointer-events", "");
            $(".css_tasti_incasso").css("opacity", "");
        }

    } else {
        alert("ERRORE: operazioni_stampa.js> stampa(operazione)-- operazione non è un Array!");
        $(".css_tasti_incasso").css("pointer-events", "");
        $(".css_tasti_incasso").css("opacity", "");
    }

}




async function stampa_comanda_test(saltafaseparcheggio) {

    let cache_id_comanda = id_comanda_attuale_alasql();
    let note = $('#popup_scelta_cliente input[name="note"]').val().toUpperCase();

    let numero_consegna_domicilio = "";
    if (comanda.numero_licenza_cliente === "0629") {
        if (comanda.tipo_consegna === "DOMICILIO") {

            numero_consegna_domicilio = fa_richiesta_progressivo_domicilio();
        }
    }

    let totale_ordine = $(comanda.totale_scontrino).html();
    let orario_preparazione = $('#popup_scelta_cliente input[name="orario_preparazione"]').val();
    if ($('.nome_parcheggio').html().length > 0) {
        if ($('#popup_scelta_cliente input[name="ora_consegna"]').val().length > 0) {
            if (comanda.tipo_consegna.length > 0 && comanda.tipo_consegna !== 'NIENTE') {

                test_copertura(function () {

                    var query = "select * from settaggi_profili where id=" + comanda.folder_number + " ;";
                    comanda.sincro.query(query, function (settaggi_profili) {
                        settaggi_profili = settaggi_profili[0];
                        if (comanda.pizzeria_asporto !== true && comanda.tavolo.left(7) === "ASPORTO") {
                            funzione_interna();
                        } else if (comanda.pizzeria_asporto !== true && saltafaseparcheggio !== true && (comanda.tavolo === "BANCO" || comanda.tavolo === "BAR" || comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === "TAKEAWAY" || comanda.tavolo === "TAKE AWAY")) {
                            comanda.stampato = false;
                            btn_parcheggia();
                        } else {
                            funzione_interna();
                        }


                        function funzione_interna() {

                            comanda.stampato = true;
                            var nome_cliente = comanda.parcheggio;
                            var array_comanda = new Array();
                            var array_dest_stampa = new Array();
                            var array_portata = new Array();
                            var varianti_unite = false;
                            if (settaggi_profili.Field135 === 'true') {
                                varianti_unite = true;
                            }

                            var testo_query = "select id,ricetta from prodotti;";
                            comanda.sincro.query(testo_query, function (result_prodotti) {

                                var corrispondenza_id_prodotti = new Object();
                                result_prodotti.forEach(function (element) {

                                    corrispondenza_id_prodotti[element.id] = element.ricetta;
                                });
                                dati_comanda(varianti_unite, function (result) {

                                    var progressivo = 0;
                                    result.forEach(function (obj) {

                                        try {
                                            var descrizione = obj['desc_art'];
                                            var prezzo = "0.00";
                                            if (!isNaN(obj['prezzo_un'])) {
                                                prezzo = obj['prezzo_un'];
                                            }
                                            var nodo = obj['nodo'];
                                            var prog_inser = obj['prog_inser'];
                                            var quantita = obj['quantita'];
                                            var categoria = obj['categoria'];
                                            var dest_stampa = obj['dest_stampa'];

                                            var portata = obj['portata'];

                                            var codice_ordinamento = portata;
                                            if (codice_ordinamento_stampa(portata) !== "") {
                                                codice_ordinamento = codice_ordinamento_stampa(portata);
                                            }

                                            var cod_articolo = obj['cod_articolo'];
                                            if (cod_articolo !== undefined && cod_articolo !== 'null' && cod_articolo.length > 0) {
                                                console.log("ricette", corrispondenza_id_prodotti, cod_articolo);
                                                var ricetta = corrispondenza_id_prodotti[cod_articolo];
                                            }

                                            if (array_comanda[dest_stampa] === undefined) {
                                                array_comanda[dest_stampa] = new Object();
                                            }

                                            if (array_dest_stampa[dest_stampa] === undefined) {
                                                array_dest_stampa[dest_stampa] = new Object();

                                                array_dest_stampa[dest_stampa].val = comanda.nome_stampante[dest_stampa].nome;
                                                array_dest_stampa[dest_stampa].dimensione = comanda.nome_stampante[dest_stampa].dimensione;
                                                array_dest_stampa[dest_stampa].ip = comanda.nome_stampante[dest_stampa].ip;
                                                array_dest_stampa[dest_stampa].intelligent = comanda.nome_stampante[dest_stampa].intelligent;
                                                array_dest_stampa[dest_stampa].fiscale = comanda.nome_stampante[dest_stampa].fiscale;
                                                array_dest_stampa[dest_stampa].nome_umano = comanda.nome_stampante[dest_stampa].nome_umano;
                                                array_dest_stampa[dest_stampa].devid_nf = comanda.nome_stampante[dest_stampa].devid_nf;
                                                array_dest_stampa[dest_stampa].devicetype = comanda.nome_stampante[dest_stampa].devicetype;
                                                array_dest_stampa[dest_stampa].devicemodel = comanda.nome_stampante[dest_stampa].devicemodel;
                                            }

                                            if (array_comanda[dest_stampa][codice_ordinamento] === undefined) {
                                                array_comanda[dest_stampa][codice_ordinamento] = new Object();
                                                array_comanda[dest_stampa][codice_ordinamento].val = comanda.nome_portata[obj['portata']];
                                                array_comanda[dest_stampa][codice_ordinamento].portata = portata;
                                            }

                                            if (array_comanda[dest_stampa][codice_ordinamento][(progressivo + 1)] === undefined) {
                                                array_comanda[dest_stampa][codice_ordinamento][(progressivo + 1)] = [quantita, descrizione, prezzo, ricetta, categoria, "N", cod_articolo, prog_inser];
                                            }

                                            progressivo++;
                                            //DESTINAZIONE STAMPA 2

                                            if (obj['cod_promo'] === "V_1") {
                                                var descrizione = obj['desc_art'].replace(/&#37;/gi, "%").replace(/&#8364;/gi, "�").replace(/=/gi, "...");
                                            } else {
                                                var descrizione = obj['desc_art'].replace(/&#37;/gi, "%").replace(/&#8364;/gi, "�");
                                            }
                                            var prezzo = "0.00";
                                            if (!isNaN(obj['prezzo_un'])) {
                                                prezzo = obj['prezzo_un'];
                                            }
                                            var nodo = obj['nodo'];
                                            var quantita = obj['quantita'];
                                            var cod_articolo = obj['cod_articolo'];

                                            var prog_inser = obj['prog_inser'];

                                            if (cod_articolo !== undefined && cod_articolo !== 'null' && cod_articolo.length > 0) {
                                                console.log("ricette", corrispondenza_id_prodotti, cod_articolo);
                                                var ricetta = corrispondenza_id_prodotti[cod_articolo];
                                            }

                                            var dest_stampa = obj['dest_stampa_2'];
                                            var portata = obj['portata'];

                                            var codice_ordinamento = portata;
                                            if (codice_ordinamento_stampa(portata) !== "") {
                                                codice_ordinamento = codice_ordinamento_stampa(portata);
                                            }

                                            var tasto_segue = obj['tasto_segue'];
                                            var categoria = obj['categoria'];
                                            //Aggiunto !==undefined 25/09/2019
                                            if (dest_stampa !== undefined && dest_stampa !== null && dest_stampa !== "" && dest_stampa !== "undefined" && dest_stampa !== "null" && dest_stampa !== obj['dest_stampa'] && obj['dest_stampa'] !== "T") {


                                                if (array_comanda[dest_stampa] === undefined) {
                                                    array_comanda[dest_stampa] = new Object();
                                                }

                                                if (array_dest_stampa[dest_stampa] === undefined) {
                                                    array_dest_stampa[dest_stampa] = new Object();
                                                    array_dest_stampa[dest_stampa].val = comanda.nome_stampante[dest_stampa].nome;
                                                    array_dest_stampa[dest_stampa].dimensione = comanda.nome_stampante[dest_stampa].dimensione;
                                                    array_dest_stampa[dest_stampa].ip = comanda.nome_stampante[dest_stampa].ip;
                                                    array_dest_stampa[dest_stampa].intelligent = comanda.nome_stampante[dest_stampa].intelligent;
                                                    array_dest_stampa[dest_stampa].fiscale = comanda.nome_stampante[dest_stampa].fiscale;
                                                    array_dest_stampa[dest_stampa].nome_umano = comanda.nome_stampante[dest_stampa].nome_umano;
                                                    array_dest_stampa[dest_stampa].devid_nf = comanda.nome_stampante[dest_stampa].devid_nf;
                                                    array_dest_stampa[dest_stampa].devicetype = comanda.nome_stampante[dest_stampa].devicetype;
                                                    array_dest_stampa[dest_stampa].devicemodel = comanda.nome_stampante[dest_stampa].devicemodel;
                                                }

                                                if (array_comanda[dest_stampa][codice_ordinamento] === undefined) {
                                                    array_comanda[dest_stampa][codice_ordinamento] = new Object();
                                                    array_comanda[dest_stampa][codice_ordinamento].val = comanda.nome_portata[obj['portata']];
                                                    array_comanda[dest_stampa][codice_ordinamento].portata = obj['portata'];
                                                }

                                                if (array_comanda[dest_stampa][codice_ordinamento][(progressivo + 1)] === undefined) {
                                                    array_comanda[dest_stampa][codice_ordinamento][(progressivo + 1)] = [quantita, descrizione, prezzo, ricetta, categoria, "D", cod_articolo, prog_inser];
                                                }

                                                progressivo++;
                                            }



                                        } catch (e) {

                                            throw descrizione;
                                        }
                                    });

                                    array_comanda = Object.keys(array_comanda).sort().reduce(
                                        (obj, key) => {
                                            obj[key] = array_comanda[key];
                                            return obj;
                                        }, {}
                                    );

                                    //---COMANDA INTELLIGENT---//

                                    for (var dest_stampa in array_comanda) {

                                        console.log("NOME STAMPANTI", comanda.nome_stampante[dest_stampa]);
                                        switch (array_dest_stampa[dest_stampa].intelligent) {
                                            case "s":
                                            case "SDS":
                                                var numeretto = $('#popup_scelta_cliente input[name="numeretto"]').val();
                                                if (array_dest_stampa[dest_stampa].fiscale === 'n') {

                                                    var concomitanze_presenti = true;
                                                    console.log("STAMPA COMANDA INTELLIGENT", array_dest_stampa[dest_stampa].ip);
                                                    var com = '';
                                                    var data = comanda.funzionidb.data_attuale();
                                                    var builder = new epson.ePOSBuilder();

                                                    var messaggio_finale = new epson.ePOSBuilder();
                                                    var parola_conto = comanda.lang_stampa[11];
                                                    builder.addTextFont(builder.FONT_A);
                                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                                    builder.addTextSize(1, 1);
                                                    if (settaggi_profili.Field111.trim().length > 0) {
                                                        switch (settaggi_profili.Field112) {
                                                            case "G":
                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                break;
                                                            case "N":
                                                            default:
                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                        }
                                                        builder.addText(settaggi_profili.Field111 + '\n');
                                                        if (settaggi_profili.Field113 === 'true') {
                                                            builder.addFeedLine(1);
                                                        }
                                                    }


                                                    if (settaggi_profili.Field114.trim().length > 0) {
                                                        switch (settaggi_profili.Field115) {
                                                            case "G":
                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                break;
                                                            case "N":
                                                            default:
                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                        }
                                                        builder.addText(settaggi_profili.Field114 + '\n');
                                                        if (settaggi_profili.Field116 === 'true') {
                                                            builder.addFeedLine(1);
                                                        }
                                                    }


                                                    if (settaggi_profili.Field117.trim().length > 0) {
                                                        switch (settaggi_profili.Field118) {
                                                            case "G":
                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                break;
                                                            case "N":
                                                            default:
                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                        }
                                                        builder.addText(settaggi_profili.Field117 + '\n');
                                                        if (settaggi_profili.Field119 === 'true') {
                                                            builder.addFeedLine(1);
                                                        }
                                                    }


                                                    if (settaggi_profili.Field120.trim().length > 0) {
                                                        switch (settaggi_profili.Field121) {
                                                            case "G":
                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                break;
                                                            case "N":
                                                            default:
                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                        }
                                                        builder.addText(settaggi_profili.Field120 + '\n');
                                                    }

                                                    builder.addText('\n\n');
                                                    builder.addTextSize(2, 2);
                                                    if (settaggi_profili.Field122 === 'true') {
                                                        builder.addText(parola_conto + '\n');
                                                    }

                                                    builder.addTextFont(builder.FONT_A);
                                                    builder.addFeedLine(1);
                                                    if (settaggi_profili.Field123 === 'true') {
                                                        builder.addText('' + array_dest_stampa[dest_stampa].nome_umano + '\n');
                                                        builder.addFeedLine(1);
                                                    }

                                                    builder.addFeedLine(1);
                                                    builder.addTextSize(2, 2);
                                                    builder.addTextAlign(builder.ALIGN_LEFT);
                                                    if (comanda.tavolo !== "BANCO" && comanda.tavolo !== "BAR" && comanda.tavolo.left(7) !== "ASPORTO" && comanda.tavolo !== "TAKE AWAY") {
                                                        builder.addText(comanda.lang_stampa[68].toLowerCase().capitalize() + ' N°' + comanda.tavolo + '\n');
                                                    } else if (nome_cliente !== 0) {
                                                        builder.addTextAlign(builder.ALIGN_CENTER);
                                                        nome_cliente = nome_cliente.replace('Prog:', '');
                                                        builder.addText(nome_cliente + /*' (' + numeretto + ')'*/ '\n');
                                                        builder.addTextAlign(builder.ALIGN_LEFT);
                                                    } else {
                                                        builder.addTextAlign(builder.ALIGN_CENTER);
                                                        builder.addText('P: ' + numeretto + '\n');
                                                        builder.addTextAlign(builder.ALIGN_LEFT);
                                                    }


                                                    builder.addTextSize(1, 1);
                                                    builder.addFeedLine(1);
                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                    if (comanda.tavolo !== "BANCO" && comanda.tavolo !== "BAR" && comanda.tavolo.left(7) !== "ASPORTO" && comanda.tavolo !== "TAKE AWAY") {
                                                        builder.addText(comanda.lang_stampa[109].toLowerCase().capitalize() + ': ' + comanda.operatore + '\n');
                                                        builder.addText(data + '\n');
                                                    } else {
                                                        builder.addTextAlign(builder.ALIGN_CENTER);
                                                        builder.addTextSize(2, 2);
                                                        /*if (comanda.stampata_s === true && comanda.stampata_n === true) {*/

                                                        if (comanda.ultima_comanda_forno_aperta_stampata === true && comanda.stampata_n === true) {
                                                            builder.addText('CAMBIO ORDINAZIONE !\n\n');
                                                        }

                                                        if (comanda.esecuzione_comanda_immediata === true) {
                                                            /*if (comanda.ultima_ora_consegna !== "" && comanda.ultima_ora_consegna !== $('#popup_scelta_cliente input[name="ora_consegna"]').val()) {
                                                             builder.addFeedLine(1);
                                                             builder.addText('CAMBIO ORA CONSEGNA!\n');
                                                             builder.addFeedLine(1);
                                                             }*/
                                                            builder.addText('FARE SUBITO !\n');
                                                        } else {
                                                            if (comanda.ultima_comanda_forno_aperta_stampata === true && comanda.ultima_ora_consegna !== "" && comanda.ultima_ora_consegna !== $('#popup_scelta_cliente input[name="ora_consegna"]').val()) {
                                                                builder.addFeedLine(1);
                                                                builder.addText('CAMBIO ORA CONSEGNA!\n');
                                                                builder.addFeedLine(1);
                                                            }

                                                            /*bool_orario_comanda_visu_alta*/
                                                            if (settaggi_profili.Field469 === "true") {

                                                                /*tipo_orario_comanda_visu_alta */
                                                                if (settaggi_profili.Field471 === "P") {
                                                                    if (orario_preparazione.length === 5) {
                                                                        builder.addText('PREPARAZIONE ' + orario_preparazione + '\n');
                                                                    }
                                                                }

                                                                /*tipo_orario_comanda_visu_alta */
                                                                if (settaggi_profili.Field471 === "C") {
                                                                    var query_gap_consegna = "select time_gap_consegna from impostazioni_fiscali where id=" + comanda.folder_number + ";"
                                                                    var result = alasql(query_gap_consegna);
                                                                    if (result[0].time_gap_consegna == "1" && comanda.consegna_gap != "" && comanda.consegna_gap != "undefined") {
                                                                        var ora_consegna = $('#popup_scelta_cliente input[name="ora_consegna"]').val()
                                                                        var minuti_somma = ora_consegna.substring(ora_consegna.indexOf(':') + 1)
                                                                        var int_miunti = parseInt(minuti_somma);
                                                                        var int_gap_miunti = parseInt(comanda.consegna_gap);;
                                                                        var min_somma = int_miunti + int_gap_miunti;
                                                                        var ora_gap = ora_consegna.substring(0, ora_consegna.indexOf(':'))
                                                                        var hours = Math.floor(min_somma / 60);
                                                                        var minutes = min_somma % 60;
                                                                        var ora = parseInt(ora_gap);

                                                                        if (minutes < 10) {
                                                                            if (ora < 10) {
                                                                                var oraa = ora + hours
                                                                                var totale_ora = "0" + oraa + ":" + "0" + minutes;
                                                                            }
                                                                            else {
                                                                                var totale_ora = ora + hours + ":" + "0" + minutes;

                                                                            }

                                                                        }
                                                                        else {
                                                                            if (ora < 10) {
                                                                                var oraa = ora + hours
                                                                                var totale_ora = "0" + oraa + ":" + minutes;
                                                                            }
                                                                            else {

                                                                                var totale_ora = ora + hours + ":" + minutes;
                                                                            }
                                                                            if (totale_ora === "NaN:NaN") {
                                                                                totale_ora = "";
                                                                            }
                                                                        }


                                                                        comanda.ora_consegna_gap = totale_ora;
                                                                        builder.addText('CONSEGNA ' + ' ' + $('#popup_scelta_cliente input[name="ora_consegna"]').val() + '\n');
                                                                        builder.addText('         ' + ' ' + totale_ora + '\n');

                                                                    }
                                                                    else {
                                                                        builder.addText('CONSEGNA ' + ' ' + $('#popup_scelta_cliente input[name="ora_consegna"]').val() + '\n');
                                                                    }

                                                                }
                                                            }
                                                        }

                                                        builder.addTextSize(1, 1);
                                                        builder.addFeedLine(1);
                                                        builder.addTextAlign(builder.ALIGN_LEFT);
                                                    }

                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                    builder.addTextSize(1, 1);
                                                    var totale_quantita = 0;
                                                    var totale_quantita_pizze = 0;
                                                    var totale_quantita_bibite = 0;
                                                    var totale_prezzo = 0.00;

                                                    array_comanda[dest_stampa] = Object.keys(array_comanda[dest_stampa]).sort().reduce(
                                                        (obj, key) => {
                                                            obj[key] = array_comanda[dest_stampa][key];
                                                            return obj;
                                                        }, {}
                                                    );

                                                    for (let codice_ordinamento of Object.keys(array_comanda[dest_stampa]).sort()) {
                                                        if (array_comanda[dest_stampa][codice_ordinamento].val !== undefined) {

                                                            builder.addFeedLine(1);
                                                            builder.addTextAlign(builder.ALIGN_CENTER);
                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                            builder.addTextSize(1, 1);
                                                            if (settaggi_profili.Field130 === 'true') {
                                                                switch (settaggi_profili.Field131) {
                                                                    case "G":
                                                                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                        break;
                                                                    case "N":
                                                                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                        builder.addTextSize(1, 1);
                                                                        break;
                                                                    case "D":
                                                                        builder.addTextSize(2, 2);
                                                                        break;
                                                                    default:
                                                                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                }

                                                                if (settaggi_profili.Field131 === "D") {
                                                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                                                    builder.addText(array_comanda[dest_stampa][codice_ordinamento].val + '\n');
                                                                    builder.addTextSize(1, 1);
                                                                    builder.addText("-----------------------------------------\n");
                                                                    builder.addTextAlign(builder.ALIGN_LEFT);
                                                                } else {
                                                                    builder.addText(trattini(array_comanda[dest_stampa][codice_ordinamento].val));
                                                                }
                                                            } else {
                                                                builder.addText("-----------------------------------------\n");
                                                            }
                                                            builder.addTextSize(1, 1);
                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                            builder.addTextAlign(builder.ALIGN_LEFT);
                                                            builder.addTextSize(2, 2);
                                                            var i = 0;
                                                            for (var node in array_comanda[dest_stampa][codice_ordinamento]) {

                                                                var nodo = array_comanda[dest_stampa][codice_ordinamento][node];
                                                                if (nodo !== undefined && nodo[1] !== undefined && nodo[1].length > 1) {

                                                                    var articolo = filtra_accenti(nodo[1]);
                                                                    var quantita = nodo[0];
                                                                    var prezzo = "0.00";
                                                                    if (!isNaN(parseFloat(nodo[2]).toFixed(2))) {
                                                                        prezzo = parseFloat(nodo[2]).toFixed(2);
                                                                    }
                                                                    var categoria = nodo[4];
                                                                    if (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(" && articolo[0] !== "*" && articolo[0] !== "x" && articolo[0] !== "=") {

                                                                        switch (settaggi_profili.Field133) {
                                                                            case "D":
                                                                                builder.addTextSize(2, 2);
                                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                break;
                                                                            case "G":
                                                                                builder.addTextSize(1, 1);
                                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                break;
                                                                            case "N":
                                                                            default:
                                                                                builder.addTextSize(1, 1);
                                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                        }

                                                                        /*if (i !== 0) {
                                                                         builder.addFeedLine(1);
                                                                         }*/
                                                                        i++;

                                                                        let portata = array_comanda[dest_stampa][codice_ordinamento].portata;

                                                                        if (portata === "ZZZ") {
                                                                            builder.addTextAlign(builder.ALIGN_CENTER);
                                                                        } else {
                                                                            builder.addText(quantita);
                                                                            builder.addTextPosition(50);
                                                                            totale_quantita += parseInt(quantita);
                                                                        }

                                                                        switch (settaggi_profili.Field133) {
                                                                            case "D":

                                                                                if (settaggi_profili.Field383 === categoria) {
                                                                                    builder.addTextStyle(true, false, true, builder.COLOR_1);
                                                                                }
                                                                                if (settaggi_profili.Field384 === categoria) {
                                                                                    builder.addTextStyle(true, false, true, builder.COLOR_1);
                                                                                }
                                                                                break;
                                                                            case "G":

                                                                                if (settaggi_profili.Field383 === categoria) {
                                                                                    builder.addTextStyle(true, false, true, builder.COLOR_1);
                                                                                }
                                                                                if (settaggi_profili.Field384 === categoria) {
                                                                                    builder.addTextStyle(true, false, true, builder.COLOR_1);
                                                                                }
                                                                                break;
                                                                            case "N":
                                                                            default:

                                                                                if (settaggi_profili.Field383 === categoria) {
                                                                                    builder.addTextStyle(true, false, false, builder.COLOR_1);
                                                                                }
                                                                                if (settaggi_profili.Field384 === categoria) {
                                                                                    builder.addTextStyle(true, false, false, builder.COLOR_1);
                                                                                }
                                                                        }

                                                                        if (portata.indexOf("P") !== -1) {
                                                                            totale_quantita_pizze += parseInt(quantita);

                                                                        }

                                                                        if (portata === "B") {
                                                                            totale_quantita_bibite += parseInt(quantita);
                                                                        }

                                                                        if (portata === "ZZZ") {
                                                                            builder.addTextPosition(0);
                                                                            builder.addTextAlign(builder.ALIGN_LEFT);
                                                                        } else {
                                                                            builder.addTextPosition(50);
                                                                        }

                                                                        //builder.addText(articolo.slice(0, 40));

                                                                        var counter = 0;
                                                                        var stringa = articolo;
                                                                        var lettere = 40;
                                                                        while (stringa.length > 0) {
                                                                            if (counter > 0) {
                                                                                builder.addText('\n');
                                                                            }
                                                                            builder.addText(stringa.substring(0, lettere));
                                                                            stringa = stringa.substring(lettere);
                                                                            counter++
                                                                        }


                                                                        if (settaggi_profili.Field132 === 'true') {

                                                                            if (comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].nome === 'CUBO') {
                                                                                if (prezzo.length === 4) {
                                                                                    builder.addTextPosition(500 - 26);
                                                                                } else if (prezzo.length === 5) {
                                                                                    builder.addTextPosition(487 - 26);
                                                                                } else if (prezzo.length === 6) {
                                                                                    builder.addTextPosition(474 - 26);
                                                                                } else {
                                                                                    builder.addTextPosition(461 - 26);
                                                                                }
                                                                            } else {
                                                                                builder.addTextPosition(415);
                                                                            }


                                                                            builder.addText('€ ' + prezzo);
                                                                        }

                                                                        builder.addText('\n');
                                                                        builder.addTextSize(1, 1);
                                                                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                        if (nodo[3] !== undefined && nodo[3] !== null && settaggi_profili.Field136 === 'true' && (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(" && articolo[0] !== "=")) {

                                                                            var ricetta = filtra_accenti(nodo[3]);
                                                                            if (ricetta.length > 1) {
                                                                                var result = '';
                                                                                var stringa = ricetta;
                                                                                var lettere = 40;
                                                                                var counter = 0;
                                                                                while (stringa.length > 0) {



                                                                                    builder.addTextPosition(50);
                                                                                    if (settaggi_profili.Field380 === "G") {
                                                                                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                    } else if (settaggi_profili.Field380 === "D") {
                                                                                        builder.addTextSize(2, 2);
                                                                                    } else { }

                                                                                    if (settaggi_profili.Field379 === "true") {
                                                                                        stringa = stringa.toUpperCase();
                                                                                    }

                                                                                    if (counter === 0) {
                                                                                        builder.addText('(');
                                                                                    }

                                                                                    counter++;
                                                                                    builder.addText(stringa.substring(0, lettere));
                                                                                    if (stringa.length <= lettere) {
                                                                                        builder.addText(')');
                                                                                    }

                                                                                    builder.addText('\n');
                                                                                    stringa = stringa.substring(lettere);
                                                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                                    builder.addTextSize(1, 1);
                                                                                }


                                                                            }

                                                                        }

                                                                        builder.addTextAlign(builder.ALIGN_LEFT);
                                                                    } else {

                                                                        var variante_meno = false;
                                                                        var variante_piu = false;
                                                                        var specifica = false;
                                                                        if (articolo[0] === "-") {
                                                                            variante_meno = true;
                                                                        } else if (articolo[0] === "+") {
                                                                            variante_piu = true;
                                                                        } else if (articolo[0] === "(") {
                                                                            specifica = true;
                                                                        }

                                                                        switch (settaggi_profili.Field134) {
                                                                            case "D":
                                                                                builder.addTextSize(2, 2);
                                                                                //RIPASSAVA "+" CON UN A CAPO PRECEDENTE

                                                                                //articolo = articolo.replace(/\+/gi, ',+').replace(/\*/gi, ',').replace(/\-/gi, ',-').replace(/\(/gi, ',(').replace(/\=/gi, ',=').replace('\,', '').split(',');


                                                                                var dio_re = /\(|[\=\+\-\*\x]+(?![^(]*\))/gi,
                                                                                    str = articolo;
                                                                                var c = 0;
                                                                                while ((match = dio_re.exec(str)) != null) {

                                                                                    articolo = [articolo.slice(0, match.index + c), ',', articolo.slice(match.index + c)].join('');
                                                                                    c++;
                                                                                }

                                                                                articolo = articolo.replace('\,', '').split(',');
                                                                                builder.addTextPosition(50);
                                                                                if (settaggi_profili.Field135 === 'true') {
                                                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                    builder.addText(quantita);
                                                                                }

                                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                                if (settaggi_profili.Field310 === 'true' && settaggi_profili.Field135 !== 'true' && variante_meno === true) {
                                                                                    builder.addTextStyle(true, false, false, builder.COLOR_1);
                                                                                } else if (settaggi_profili.Field410 === 'true' && settaggi_profili.Field135 !== 'true' && variante_piu === true) {
                                                                                    builder.addTextStyle(true, false, false, builder.COLOR_1);
                                                                                } else if (settaggi_profili.Field424 === 'true' && settaggi_profili.Field135 !== 'true' && specifica === true) {
                                                                                    builder.addTextStyle(true, false, false, builder.COLOR_1);
                                                                                }


                                                                                for (var art in articolo) {
                                                                                    builder.addTextPosition(90);

                                                                                    var counter = 0;
                                                                                    var stringa = articolo[art].replace(/\=/gi, ' ').trim();
                                                                                    var lettere = 40;
                                                                                    while (stringa.length > 0) {
                                                                                        if (counter > 0) {
                                                                                            builder.addText('\n');
                                                                                        }
                                                                                        builder.addText(stringa.substring(0, lettere));
                                                                                        stringa = stringa.substring(lettere);
                                                                                        counter++;
                                                                                    }

                                                                                    if (settaggi_profili.Field135 === "true") {
                                                                                        builder.addText('\n');
                                                                                    }
                                                                                }


                                                                                if (settaggi_profili.Field132 === 'true') {

                                                                                    if (comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].nome === 'CUBO') {
                                                                                        if (prezzo.length === 4) {
                                                                                            builder.addTextPosition(500 - 26);
                                                                                        } else if (prezzo.length === 5) {
                                                                                            builder.addTextPosition(487 - 26);
                                                                                        } else if (prezzo.length === 6) {
                                                                                            builder.addTextPosition(474 - 26);
                                                                                        } else {
                                                                                            builder.addTextPosition(461 - 26);
                                                                                        }
                                                                                    } else {
                                                                                        builder.addTextPosition(415);
                                                                                    }

                                                                                    builder.addText('€ ' + prezzo);
                                                                                }
                                                                                builder.addText('\n');
                                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                                break;
                                                                            case "G":
                                                                                builder.addTextSize(1, 1);
                                                                                //articolo = articolo.replace(/\+/gi, ',+').replace(/\*/gi, ',').replace(/\-/gi, ',-').replace(/\(/gi, ',(').replace(/\=/gi, ',=').replace(/FINE COT./gi, 'F.C.').replace(/IN COT./gi, 'I.C.').replace('\,', '').split(',');
                                                                                var dio_re = /\(|[\=\+\-\*\x]+(?![^(]*\))/gi,
                                                                                    str = articolo;
                                                                                var c = 0;
                                                                                while ((match = dio_re.exec(str)) != null) {

                                                                                    articolo = [articolo.slice(0, match.index + c), ',', articolo.slice(match.index + c)].join('');
                                                                                    c++;
                                                                                }

                                                                                articolo = articolo.replace('\,', '').split(',');
                                                                                builder.addTextPosition(50);
                                                                                if (settaggi_profili.Field135 === 'true') {
                                                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                    builder.addText(quantita);
                                                                                }

                                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                if (settaggi_profili.Field310 === 'true' && variante_meno === true) {
                                                                                    builder.addTextStyle(true, false, true, builder.COLOR_1);
                                                                                } else if (settaggi_profili.Field410 === 'true' && variante_piu === true) {
                                                                                    builder.addTextStyle(true, false, true, builder.COLOR_1);
                                                                                } else if (settaggi_profili.Field424 === 'true' && settaggi_profili.Field135 !== 'true' && specifica === true) {
                                                                                    builder.addTextStyle(true, false, false, builder.COLOR_1);
                                                                                }

                                                                                for (var art in articolo) {
                                                                                    builder.addTextPosition(90);

                                                                                    var counter = 0;
                                                                                    var stringa = articolo[art].replace(/\=/gi, ' ').trim();
                                                                                    var lettere = 40;
                                                                                    while (stringa.length > 0) {
                                                                                        if (counter > 0) {
                                                                                            builder.addText('\n');
                                                                                        }
                                                                                        builder.addText(stringa.substring(0, lettere));
                                                                                        stringa = stringa.substring(lettere);
                                                                                        counter++;
                                                                                    }
                                                                                }
                                                                                if (settaggi_profili.Field132 === 'true') {

                                                                                    if (comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].nome === 'CUBO') {
                                                                                        if (prezzo.length === 4) {
                                                                                            builder.addTextPosition(500 - 26);
                                                                                        } else if (prezzo.length === 5) {
                                                                                            builder.addTextPosition(487 - 26);
                                                                                        } else if (prezzo.length === 6) {
                                                                                            builder.addTextPosition(474 - 26);
                                                                                        } else {
                                                                                            builder.addTextPosition(461 - 26);
                                                                                        }
                                                                                    } else {
                                                                                        builder.addTextPosition(415);
                                                                                    }

                                                                                    builder.addText('€ ' + prezzo);
                                                                                }
                                                                                builder.addText('\n');
                                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                                break;
                                                                            case "N":
                                                                            default:
                                                                                builder.addTextSize(1, 1);
                                                                                //articolo = articolo.replace(/\+/gi, ',+').replace(/\*/gi, ',').replace(/\-/gi, ',-').replace(/\(/gi, ',(').replace(/\=/gi, ',=').replace(/FINE COT./gi, 'F.C.').replace(/IN COT./gi, 'I.C.').replace('\,', '').split(',');
                                                                                var dio_re = /\(|[\=\+\-\*\x]+(?![^(]*\))/gi,
                                                                                    str = articolo;
                                                                                var c = 0;
                                                                                while ((match = dio_re.exec(str)) != null) {

                                                                                    articolo = [articolo.slice(0, match.index + c), ',', articolo.slice(match.index + c)].join('');
                                                                                    c++;
                                                                                }

                                                                                articolo = articolo.replace('\,', '').split(',');
                                                                                builder.addTextPosition(50);
                                                                                if (settaggi_profili.Field135 === 'true') {
                                                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                                    builder.addText(quantita);
                                                                                }

                                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                                if (settaggi_profili.Field310 === 'true' && variante_meno === true) {
                                                                                    builder.addTextStyle(true, false, true, builder.COLOR_1);
                                                                                } else if (settaggi_profili.Field410 === 'true' && variante_piu === true) {
                                                                                    builder.addTextStyle(true, false, true, builder.COLOR_1);
                                                                                } else if (settaggi_profili.Field424 === 'true' && settaggi_profili.Field135 !== 'true' && specifica === true) {
                                                                                    builder.addTextStyle(true, false, false, builder.COLOR_1);
                                                                                }

                                                                                var indice_articolo = 0;
                                                                                for (var art in articolo) {
                                                                                    builder.addTextPosition(90);
                                                                                    //builder.addText(articolo[art].replace(/\=/gi, ' ').trim());

                                                                                    var counter = 0;
                                                                                    var stringa = articolo[art].replace(/\=/gi, ' ').trim();
                                                                                    var lettere = 40;
                                                                                    while (stringa.length > 0) {
                                                                                        if (counter > 0) {
                                                                                            builder.addText('\n');
                                                                                        }
                                                                                        builder.addText(stringa.substring(0, lettere));
                                                                                        stringa = stringa.substring(lettere);
                                                                                        counter++
                                                                                    }
                                                                                    indice_articolo++;
                                                                                    if (indice_articolo < articolo.length) {
                                                                                        builder.addText('\n');
                                                                                    }
                                                                                }

                                                                                if (settaggi_profili.Field132 === 'true') {

                                                                                    if (comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].nome === 'CUBO') {
                                                                                        if (prezzo.length === 4) {
                                                                                            builder.addTextPosition(500 - 26);
                                                                                        } else if (prezzo.length === 5) {
                                                                                            builder.addTextPosition(487 - 26);
                                                                                        } else if (prezzo.length === 6) {
                                                                                            builder.addTextPosition(474 - 26);
                                                                                        } else {
                                                                                            builder.addTextPosition(461 - 26);
                                                                                        }
                                                                                    } else {
                                                                                        builder.addTextPosition(415);
                                                                                    }

                                                                                    builder.addText('€ ' + prezzo);
                                                                                }
                                                                                builder.addText('\n');
                                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                        }

                                                                    }

                                                                    totale_prezzo += parseFloat(prezzo) * parseInt(quantita);
                                                                }
                                                            }
                                                        }

                                                        builder.addTextAlign(builder.ALIGN_CENTER);
                                                        builder.addTextSize(1, 1);
                                                        builder.addText("------------------------------------------\n");
                                                        builder.addTextAlign(builder.ALIGN_LEFT);
                                                    }


                                                    if (settaggi_profili.Field189 === 'true') {

                                                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                        if (!isNaN(parseFloat(comanda.totale_consegna)) && parseFloat(comanda.totale_consegna) > 0) {

                                                            builder.addTextPosition(50);
                                                            builder.addText("CONSEGNA");
                                                            indice_articolo++;
                                                            prezzo = comanda.totale_consegna;
                                                            if (comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].nome === 'CUBO') {
                                                                if (prezzo.length === 4) {
                                                                    builder.addTextPosition(500 - 26);
                                                                } else if (prezzo.length === 5) {
                                                                    builder.addTextPosition(487 - 26);
                                                                } else if (prezzo.length === 6) {
                                                                    builder.addTextPosition(474 - 26);
                                                                } else {
                                                                    builder.addTextPosition(461 - 26);
                                                                }
                                                            } else {
                                                                builder.addTextPosition(415);
                                                            }

                                                            builder.addText('€ ' + prezzo + '\n');
                                                            totale_prezzo += parseFloat(comanda.totale_consegna);
                                                        }
                                                    }



                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                    builder.addFeedLine(1);
                                                    if (settaggi_profili.Field381 === 'true' && settaggi_profili.Field382 === 'true' && settaggi_profili.Field137 === 'true') {

                                                        builder.addText('\n');
                                                        builder.addText(totale_quantita);
                                                        builder.addTextPosition(50);
                                                        builder.addText('prodotti' + '               '); //quantita

                                                        if (parseInt(totale_quantita_pizze) > 0) {

                                                            builder.addTextPosition(400);
                                                            builder.addText(totale_quantita_pizze);
                                                            builder.addTextPosition(450);
                                                            builder.addText('pizze' + '               '); //quantita
                                                        }

                                                        if (parseInt(totale_quantita_bibite) > 0) {
                                                            builder.addTextPosition(400);
                                                            builder.addText(totale_quantita_bibite);
                                                            builder.addTextPosition(450);
                                                            builder.addText('bibite' + '               '); //quantita
                                                        }


                                                    } else {
                                                        if (parseInt(totale_quantita_pizze) > 0) {
                                                            if (settaggi_profili.Field381 === 'true') {
                                                                builder.addText(totale_quantita_pizze);
                                                                builder.addTextPosition(50);
                                                                builder.addText('pizze' + '               '); //quantita
                                                            }
                                                        }

                                                        if (parseInt(totale_quantita_bibite) > 0) {
                                                            if (settaggi_profili.Field382 === 'true') {
                                                                builder.addText('\n');
                                                                builder.addText(totale_quantita_bibite);
                                                                builder.addTextPosition(50);
                                                                builder.addText('bibite' + '               '); //quantita
                                                            }
                                                        }

                                                        if (settaggi_profili.Field137 === 'true') {
                                                            builder.addText('\n');
                                                            builder.addText(totale_quantita);
                                                            builder.addTextPosition(50);
                                                            builder.addText('prodotti' + '               '); //quantita
                                                        }
                                                    }




                                                    if (settaggi_profili.Field189 === 'true') {

                                                        if (parseFloat(totale_ordine).toFixed(2) !== parseFloat(totale_prezzo).toFixed(2)) {


                                                            builder.addTextPosition(0);
                                                            builder.addText("\n\nTotale ORDINE: ");
                                                            var prz_t = parseFloat(totale_ordine).toFixed(2);
                                                            if (comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].nome === 'CUBO') {
                                                                if (prz_t.length === 4) {
                                                                    builder.addTextPosition(500 - 26);
                                                                } else if (prz_t.length === 5) {
                                                                    builder.addTextPosition(487 - 26);
                                                                } else if (prz_t.length === 6) {
                                                                    builder.addTextPosition(474 - 26);
                                                                } else {
                                                                    builder.addTextPosition(461 - 26);
                                                                }
                                                            } else {
                                                                builder.addTextPosition(415);
                                                            }



                                                            builder.addText('€ ' + prz_t); //quantita

                                                        } else {

                                                            builder.addTextPosition(0);
                                                            builder.addText("\n\nTotale: ");
                                                            var prz_t = parseFloat(totale_prezzo).toFixed(2);
                                                            if (comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].nome === 'CUBO') {
                                                                if (prz_t.length === 4) {
                                                                    builder.addTextPosition(500 - 26);
                                                                } else if (prz_t.length === 5) {
                                                                    builder.addTextPosition(487 - 26);
                                                                } else if (prz_t.length === 6) {
                                                                    builder.addTextPosition(474 - 26);
                                                                } else {
                                                                    builder.addTextPosition(461 - 26);
                                                                }
                                                            } else {
                                                                builder.addTextPosition(415);
                                                            }



                                                            builder.addText('€ ' + prz_t); //quantita
                                                        }
                                                    }

                                                    builder.addText('\n');
                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                    builder.addTextSize(1, 1);
                                                    if (comanda.tavolo === "BANCO" || comanda.tavolo == "BAR" || comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === "TAKE AWAY") {

                                                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                        if (settaggi_profili.Field124 === 'true') {
                                                            switch (settaggi_profili.Field125) {
                                                                case "G":
                                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                    break;
                                                                case "N":
                                                                default:
                                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                            }
                                                            builder.addText('\nOperatore: ' + comanda.operatore);
                                                        }

                                                        if (settaggi_profili.Field126 === 'true') {
                                                            switch (settaggi_profili.Field127) {
                                                                case "G":
                                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                    break;
                                                                case "N":
                                                                default:
                                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                            }
                                                            builder.addText('\nData: ' + data.substr(0, 8).replace(/-/gi, '/'));
                                                        }

                                                        if (settaggi_profili.Field128 === 'true') {
                                                            switch (settaggi_profili.Field129) {
                                                                case "G":
                                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                    break;
                                                                case "N":
                                                                default:
                                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                            }
                                                            builder.addText('\nOra:  ' + data.slice(-5));
                                                        }


                                                        builder.addTextSize(2, 2);
                                                        if (nome_cliente.length === 0) {
                                                            builder.addText('\n' + numeretto + '\n');
                                                        }


                                                        if (comanda.tipo_consegna === "DOMICILIO" && settaggi_profili.Field252 !== 'true' && settaggi_profili.Field253 !== 'true') {
                                                            if (settaggi_profili.Field375.trim().length > 0) {
                                                                builder.addFeedLine(2);
                                                                builder.addTextAlign(builder.ALIGN_CENTER);
                                                                //builder.addText(comanda.tipo_consegna + "\n");
                                                                builder.addText(settaggi_profili.Field375 + "\n");
                                                            }
                                                        } else if (comanda.tipo_consegna === "RITIRO") {
                                                            if (settaggi_profili.Field376.trim().length > 0) {
                                                                builder.addFeedLine(2);
                                                                builder.addTextAlign(builder.ALIGN_CENTER);
                                                                //builder.addText(comanda.tipo_consegna + "\n");
                                                                builder.addText(settaggi_profili.Field376 + "\n");
                                                            }
                                                        } else if (comanda.tipo_consegna === "PIZZERIA") {
                                                            if (settaggi_profili.Field377.trim().length > 0) {
                                                                builder.addFeedLine(2);
                                                                builder.addTextAlign(builder.ALIGN_CENTER);
                                                                //builder.addText(comanda.tipo_consegna + "\n");
                                                                builder.addText(settaggi_profili.Field377 + "\n");
                                                            }
                                                        }





                                                        builder.addTextSize(1, 1);
                                                        if (comanda.tipo_consegna === "DOMICILIO") {




                                                            if (settaggi_profili.Field252 === 'true' || settaggi_profili.Field253 === 'true') {

                                                                builder.addFeedLine(1);
                                                                builder.addTextAlign(builder.ALIGN_CENTER);
                                                                var nome = $('#popup_scelta_cliente input[name="nome"]').val().toUpperCase();
                                                                var cognome = $('#popup_scelta_cliente input[name="cognome"]').val().toUpperCase();
                                                                var ragione_sociale = $('#popup_scelta_cliente input[name="ragione_sociale"]').val().toUpperCase();
                                                                var indirizzo = $('#popup_scelta_cliente input[name="indirizzo"]').val().toUpperCase();
                                                                var numero = $('#popup_scelta_cliente input[name="numero"]').val().toUpperCase();
                                                                var cap = $('#popup_scelta_cliente input[name="cap"]').val();
                                                                var citta = $('#popup_scelta_cliente input[name="citta"]').val().toUpperCase();
                                                                var comune = $('#popup_scelta_cliente input[name="comune"]').val().toUpperCase();
                                                                var provincia = $('#popup_scelta_cliente input[name="provincia"]').val().toUpperCase();
                                                                var cellulare = $('#popup_scelta_cliente input[name="cellulare"]').val();
                                                                var telefono = $('#popup_scelta_cliente input[name="telefono"]').val();
                                                                var destinazione = indirizzo + ", " + numero + " " + comune + " " + " " + cap + " " + provincia;
                                                                if (comanda.stampante_piccola_conto === 'N') {
                                                                    builder.addText("-----------------------------------------\n");
                                                                } else {
                                                                    builder.addText("--------------------------------\n");
                                                                }
                                                                var font_size_cliente; 
                                                                if(settaggi_profili.Field512==="D")
                                                                {
                                                                    font_size_cliente=2;
                                                                } 
                                                                if(settaggi_profili.Field512==="N")
                                                                { font_size_cliente=1;

                                                                }


                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                builder.addText('DATI CLIENTE:\n');
                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                              
                                                                if (settaggi_profili.Field252 === 'true' && settaggi_profili.Field253 === 'true' && settaggi_profili.Field512==="D") {
                                                                   // builder.addPageBegin();
                                                                   // builder.addPageArea(40, 0, 330, 200);
                                                                   builder.addTextAlign(builder.ALIGN_CENTER);
                                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                    builder.addTextSize(font_size_cliente, font_size_cliente);
                                                                    builder.addText(nome + " " + cognome + '\n');
                                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                    if (ragione_sociale.length >= 3) {
                                                                        builder.addText(ragione_sociale + '\n');
                                                                    }
                                                                    builder.addText(indirizzo + "," + numero + '\n');
                                                                    if (cap) {
                                                                        builder.addText(cap + " " + comune + " " + provincia + '\n');
                                                                    } else {
                                                                        builder.addText(comune + " " + provincia + '\n');
                                                                    }
                                                                    if (cellulare) {
                                                                        builder.addText(cellulare);
                                                                    }
                                                                    if (cellulare && telefono) {
                                                                        builder.addText(" - ");
                                                                    }
                                                                    if (telefono) {
                                                                        builder.addText(telefono);
                                                                    }
                                                                    if (cellulare || telefono) {
                                                                        builder.addText("\n");
                                                                    }

                                                                    if (comanda.tipo_consegna !== "RITIRO" && note.length > 0) {
                                                                        //builder.addFeedLine(1);
                                                                        builder.addTextStyle(false, false, true, builder.COLOR_1);

                                                                        builder.addText("Note: " + note.substr(0, 30) + "\n");


                                                                        /*builder.addText("Note: ");*/
                                                                        /*if (note.length > 20) {
                                                                            builder.addText(note.substr(0, 30));
                                                                        } else {
                                                                            builder.addText(note);
                                                                        }*/
                                                                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                        /*builder.addText('\n');*/
                                                                    }

                                                                   // builder.addPageArea(370, 0, 300, 200);
                                                                   builder.addTextAlign(builder.ALIGN_CENTER);
                                                                   builder.addSymbol("https://www.google.com/maps/dir/?api=1&destination=" + destinazione.replace(/\s+/g, "+") + "&travelmode=DRIVING", builder.SYMBOL_QRCODE_MODEL_2, builder.LEVEL_DEFAULT, 4);
                                                                  //  builder.addPageEnd();}
                                                                }
                                                                 else if (settaggi_profili.Field252 === 'true' && settaggi_profili.Field253 === 'true' && settaggi_profili.Field512==="N") {
                                                                     builder.addPageBegin();
                                                                     builder.addPageArea(40, 0, 330, 200);
                                                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                                                     builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                     builder.addTextSize(font_size_cliente, font_size_cliente);
                                                                     builder.addText(nome + " " + cognome + '\n');
                                                                     builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                     if (ragione_sociale.length >= 3) {
                                                                         builder.addText(ragione_sociale + '\n');
                                                                     }
                                                                     builder.addText(indirizzo + "," + numero + '\n');
                                                                     if (cap) {
                                                                         builder.addText(cap + " " + comune + " " + provincia + '\n');
                                                                     } else {
                                                                         builder.addText(comune + " " + provincia + '\n');
                                                                     }
                                                                     if (cellulare) {
                                                                         builder.addText(cellulare);
                                                                     }
                                                                     if (cellulare && telefono) {
                                                                         builder.addText(" - ");
                                                                     }
                                                                     if (telefono) {
                                                                         builder.addText(telefono);
                                                                     }
                                                                     if (cellulare || telefono) {
                                                                         builder.addText("\n");
                                                                     }
 
                                                                     if (comanda.tipo_consegna !== "RITIRO" && note.length > 0) {
                                                                         //builder.addFeedLine(1);
                                                                         builder.addTextStyle(false, false, true, builder.COLOR_1);
 
                                                                         builder.addText("Note: " + note.substr(0, 30) + "\n");
 
 
                                                                         /*builder.addText("Note: ");*/
                                                                         /*if (note.length > 20) {
                                                                             builder.addText(note.substr(0, 30));
                                                                         } else {
                                                                             builder.addText(note);
                                                                         }*/
                                                                         builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                         /*builder.addText('\n');*/
                                                                     }
 
                                                                     builder.addPageArea(370, 0, 300, 200);
                                                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                                                    builder.addSymbol("https://www.google.com/maps/dir/?api=1&destination=" + destinazione.replace(/\s+/g, "+") + "&travelmode=DRIVING", builder.SYMBOL_QRCODE_MODEL_2, builder.LEVEL_DEFAULT, 4);
                                                                     builder.addPageEnd();
                                                                 }
                                                                 else if (settaggi_profili.Field252 === 'true') {
                                                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                                                    builder.addFeedLine(1);
                                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                    builder.addTextSize(font_size_cliente, font_size_cliente);
                                                                    builder.addText(nome + " " + cognome + '\n');
                                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                    if (ragione_sociale.length >= 3) {
                                                                        builder.addText(ragione_sociale + '\n');
                                                                    }
                                                                    builder.addText(indirizzo + "," + numero + '\n');
                                                                    if (cap) {
                                                                        builder.addText(cap + " " + comune + " " + provincia + '\n');
                                                                    } else {
                                                                        builder.addText(comune + " " + provincia + '\n');
                                                                    }
                                                                    if (cellulare) {
                                                                        builder.addText(cellulare);
                                                                    }
                                                                    if (cellulare && telefono) {
                                                                        builder.addText(" - ");
                                                                    }
                                                                    if (telefono) {
                                                                        builder.addText(telefono);
                                                                    }
                                                                    if (cellulare || telefono) {
                                                                        builder.addText("\n");
                                                                    }

                                                                    if (comanda.tipo_consegna !== "RITIRO" && note.length > 0) {
                                                                        //builder.addFeedLine(1);
                                                                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                        /*builder.addText("Note: ");
                                                                        if (note.length > 20) {
                                                                            builder.addText(note.substr(0, 30));
                                                                        } else {
                                                                            builder.addText(note);
                                                                        }*/

                                                                        builder.addText("Note: " + note.substr(0, 30) + "\n");

                                                                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                        /*builder.addText('\n');*/
                                                                    }
                                                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                                                }else if (settaggi_profili.Field253 === 'true') {
                                                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                                                    builder.addFeedLine(1);
                                                                    builder.addSymbol("https://www.google.com/maps/dir/?api=1&destination=" + destinazione.replace(/\s+/g, "+") + "&travelmode=DRIVING", builder.SYMBOL_QRCODE_MODEL_2, builder.LEVEL_DEFAULT, 4);
                                                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                                                }builder.addTextSize(1, 1);
                                                                if (comanda.stampante_piccola_conto === 'N') {
                                                                    builder.addText("-----------------------------------------\n");
                                                                } else {
                                                                    builder.addText("--------------------------------\n");
                                                                }
                                                            }
                                                        }

                                                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                    }

                                                    if (comanda.esecuzione_comanda_immediata !== true /*&& comanda.orario_preparazione === "1"*/) {

                                                        builder.addTextSize(2, 2);

                                                        /*bool_orario_comanda_visu_alta*/
                                                        if (settaggi_profili.Field470 === "true") {

                                                            /*tipo_orario_comanda_visu_alta */
                                                            if (settaggi_profili.Field472 === "P") {
                                                                if (orario_preparazione.length === 5) {
                                                                    builder.addText('PREPARAZIONE ' + orario_preparazione + '\n');
                                                                }
                                                            }

                                                            /*tipo_orario_comanda_visu_alta */
                                                            if (settaggi_profili.Field472 === "C") {
                                                                var query_gap_consegna = "select time_gap_consegna from impostazioni_fiscali where id=" + comanda.folder_number + ";"
                                                                var result = alasql(query_gap_consegna);
                                                                if (result[0].time_gap_consegna == "1" && comanda.consegna_gap != "" && comanda.consegna_gap != "undefined") {
                                                                    var ora_consegna = $('#popup_scelta_cliente input[name="ora_consegna"]').val()
                                                                    var minuti_somma = ora_consegna.substring(ora_consegna.indexOf(':') + 1)
                                                                    var int_miunti = parseInt(minuti_somma);
                                                                    var int_gap_miunti = parseInt(comanda.consegna_gap);;
                                                                    var min_somma = int_miunti + int_gap_miunti;
                                                                    var ora_gap = ora_consegna.substring(0, ora_consegna.indexOf(':'))
                                                                    var hours = Math.floor(min_somma / 60);
                                                                    var minutes = min_somma % 60;
                                                                    var ora = parseInt(ora_gap);

                                                                    if (minutes < 10) {
                                                                        if (ora < 10) {
                                                                            var oraa = ora + hours
                                                                            var totale_ora = "0" + oraa + ":" + "0" + minutes;
                                                                        }
                                                                        else {
                                                                            var totale_ora = ora + hours + ":" + "0" + minutes;

                                                                        }

                                                                    }
                                                                    else {
                                                                        if (ora < 10) {
                                                                            var oraa = ora + hours
                                                                            var totale_ora = "0" + oraa + ":" + minutes;
                                                                        }
                                                                        else {

                                                                            var totale_ora = ora + hours + ":" + minutes;
                                                                        }
                                                                        if (totale_ora === "NaN:NaN") {
                                                                            totale_ora = "";
                                                                        }
                                                                    }


                                                                    comanda.ora_consegna_gap = totale_ora;
                                                                    builder.addText('CONSEGNA ' + ' ' + $('#popup_scelta_cliente input[name="ora_consegna"]').val() + '\n');
                                                                    builder.addText('         ' + ' ' + totale_ora + '\n');

                                                                } else {
                                                                    builder.addText('CONSEGNA ' + ' ' + $('#popup_scelta_cliente input[name="ora_consegna"]').val() + '\n');
                                                                }

                                                            }
                                                        }
                                                        builder.addTextSize(1, 1);
                                                    }

                                                    if (settaggi_profili.Field138.trim().length > 0 || settaggi_profili.Field142.trim().length > 0 || settaggi_profili.Field146.trim().length > 0) {
                                                        builder.addFeedLine(2);
                                                    }
                                                    if (settaggi_profili.Field139 === 'G')
                                                        messaggio_finale.addTextStyle(false, false, true, builder.COLOR_1);
                                                    else if (settaggi_profili.Field139 === 'N')
                                                        messaggio_finale.addTextStyle(false, false, false, builder.COLOR_1);
                                                    if (settaggi_profili.Field140 === 'x--')
                                                        messaggio_finale.addTextAlign(builder.ALIGN_LEFT);
                                                    else if (settaggi_profili.Field140 === '-x-')
                                                        messaggio_finale.addTextAlign(builder.ALIGN_CENTER);
                                                    else if (settaggi_profili.Field140 === '--x')
                                                        messaggio_finale.addTextAlign(builder.ALIGN_RIGHT);
                                                    if (settaggi_profili.Field138.trim().length > 0)
                                                        messaggio_finale.addText(settaggi_profili.Field138 + '\n');
                                                    if (settaggi_profili.Field141 === 'true')
                                                        messaggio_finale.addFeedLine(1);
                                                    if (settaggi_profili.Field143 === 'G')
                                                        messaggio_finale.addTextStyle(false, false, true, builder.COLOR_1);
                                                    else if (settaggi_profili.Field143 === 'N')
                                                        messaggio_finale.addTextStyle(false, false, false, builder.COLOR_1);
                                                    if (settaggi_profili.Field144 === 'x--')
                                                        messaggio_finale.addTextAlign(builder.ALIGN_LEFT);
                                                    else if (settaggi_profili.Field144 === '-x-')
                                                        messaggio_finale.addTextAlign(builder.ALIGN_CENTER);
                                                    else if (settaggi_profili.Field144 === '--x')
                                                        messaggio_finale.addTextAlign(builder.ALIGN_RIGHT);
                                                    if (settaggi_profili.Field142.trim().length > 0)
                                                        messaggio_finale.addText(settaggi_profili.Field142 + '\n');
                                                    if (settaggi_profili.Field145 === 'true')
                                                        messaggio_finale.addFeedLine(1);
                                                    if (settaggi_profili.Field147 === 'G')
                                                        messaggio_finale.addTextStyle(false, false, true, builder.COLOR_1);
                                                    else if (settaggi_profili.Field147 === 'N')
                                                        messaggio_finale.addTextStyle(false, false, false, builder.COLOR_1);
                                                    if (settaggi_profili.Field148 === 'x--')
                                                        messaggio_finale.addTextAlign(builder.ALIGN_LEFT);
                                                    else if (settaggi_profili.Field148 === '-x-')
                                                        messaggio_finale.addTextAlign(builder.ALIGN_CENTER);
                                                    else if (settaggi_profili.Field148 === '--x')
                                                        messaggio_finale.addTextAlign(builder.ALIGN_RIGHT);
                                                    if (settaggi_profili.Field146.trim().length > 0)
                                                        messaggio_finale.addText(settaggi_profili.Field146 + '\n');
                                                    //FINE SETTAGGI MESSAGGIO FINALE





                                                    if (comanda.numero_licenza_cliente === "0629") {
                                                        if (comanda.tipo_consegna === "DOMICILIO") {
                                                            builder.addTextSize(2, 2);
                                                            builder.addText("Num. Consegna: " + numero_consegna_domicilio + "\n");
                                                            builder.addTextSize(1, 1);
                                                        }
                                                    }

                                                    builder.addFeedLine(2);

                                                    if (settaggi_profili.Field498 === 'true') {
                                                        builder.addBarcode('{A#' + cache_id_comanda, builder.BARCODE_CODE128, builder.HRI_NONE, builder.FONT_A, 2, 100);
                                                    }

                                                    let oggetto_concomitanze = null;
                                                    let testo_concomitanze = '';
                                                    let concomitanze = '';
                                                    if (concomitanze_presenti === true) {
                                                        if (settaggi_profili.Field149 === 'true') {
                                                            builder.addTextAlign(builder.ALIGN_CENTER);

                                                            oggetto_concomitanze = funzione_concomitanze(array_comanda, dest_stampa, array_dest_stampa);

                                                            concomitanze = oggetto_concomitanze['concomitanze'];
                                                            testo_concomitanze = oggetto_concomitanze['testo_concomitanze'];

                                                        }
                                                    }

                                                    var messaggio_finale = '<feed line="1"/>' + messaggio_finale.toString().substr(72).slice(0, -13);
                                                    var request = builder.toString().slice(0, -13) + testo_concomitanze + messaggio_finale + '<feed line="2"/><cut type="feed"/></epos-print>';
                                                    //var request = builder.toString().slice(0, -13) + testo_concomitanze + '<feed line="3"/><cut type="feed"/></epos-print>';
                                                    console.log("REQUEST", request);
                                                    var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
                                                    //CAMBIANDO QUESTO PARAMETRO CAMBIA L'IP DI STAMPA (quindi si potrebbe prendere da comanda.nome_stampante[num].ip
                                                    //SI POTREBBE TESTARE LA DIMENSIONE CON comanda.nome_stampante[num].dimensione
                                                    //E SE E' O NO FISCALE CON IL PARAMETRO comanda.nome_stampante[num].fiscale

                                                    var url = 'http://' + array_dest_stampa[dest_stampa].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                                                    if (array_dest_stampa[dest_stampa].intelligent === "SDS") {
                                                        url = 'http://' + array_dest_stampa[dest_stampa].ip + ':8000/SPOOLER?stampante=' + array_dest_stampa[dest_stampa].devid_nf;
                                                    }
                                                    //per stampare 2 sulla stessa stampante
                                                    let dc = alasql("select doppia from nomi_stampanti where id='" + dest_stampa + "' limit 1;");

                                                    if (dc.length > 0) {
                                                        if (dc[0].doppia !== null && dc[0].doppia === "true") {
                                                            /*if (settaggi_profili.Field109 === 'true') {*/

                                                            aggiungi_spool_stampa(url, soap + " ", array_dest_stampa[dest_stampa].nome_umano);

                                                        }
                                                    }

                                                    aggiungi_spool_stampa(url, soap, array_dest_stampa[dest_stampa].nome_umano);
                                                }
                                        }
                                    }


                                    var data = comanda.funzionidb.data_attuale().substr(0, 8);
                                    var ora = comanda.funzionidb.data_attuale().substr(9, 8);
                                    var testo_query = "update comanda set stampata_sn='S',data_stampa='" + data + "',ora_stampa='" + ora + "' where stato_record='ATTIVO' and ntav_comanda='" + comanda.tavolo + "'";
                                    comanda.sock.send({
                                        tipo: "comanda_stampata",
                                        operatore: comanda.operatore,
                                        query: testo_query,
                                        terminale: comanda.terminale,
                                        ip: comanda.ip_address
                                    });
                                    comanda.sincro.query(testo_query, function () {

                                    });
                                });
                            });
                        }
                    });
                });
            } else {
                intesta_cliente();
            }
        } else {
            intesta_cliente();
        }
    } else {
        intesta_cliente();
    }
}


function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

function cerca_in_comanda_corrente(comanda_attuale, node6, node7) {

    let ritorno = false;

    Object.values(comanda_attuale).forEach((a) => {

        Object.values(a).forEach((b) => {

            if (Array.isArray(b)) {

                if (b[6] === node6 && b[7] === node7) {
                    ritorno = true;
                }

            }

        });

    });

    return ritorno;

}


//dest_stampa e destiozione di stampa atuale 
function funzione_concomitanze(array_comanda, dest_stampa, array_dest_stampa) {

    /* INIZIO TEST CONCOMITANZA */

    var concomitanze = new epson.ePOSBuilder();

    //l'array delle comande suddivise per destinazione di stampa le copia anche sull'array per stampare le concomitanze
    let array_concomitanze = new Object();
    Object.assign(array_concomitanze, array_comanda);


    //funzione per sapere le portate corrispondenti alla comanda corrente
    //queste sono le portate della destinazione di stampa corrente, ovvero le chiavi dell'oggetto
    let portate_comanda_corrente = [];

    let portate_corrispondenti = [];

    Object.values(array_concomitanze[dest_stampa]).forEach((v) => {

        portate_comanda_corrente.push(v.portata);
        //portate_corrispondenti = portate_corrispondenti.concat(v.portata);

    });



    portate_comanda_corrente.forEach((v) => {

        let temp_concomitanze = alasql("select concomitanze from nomi_portate where numero='" + v + "'");

        portate_corrispondenti = portate_corrispondenti.concat(JSON.parse(temp_concomitanze[0].concomitanze));

    });



    portate_corrispondenti = portate_corrispondenti.filter(onlyUnique);

    let comanda_attuale = Object.assign({}, array_concomitanze[dest_stampa]);

    //quanto è copiato cancella la destinazione corrente dalle concomitanze
    delete array_concomitanze[dest_stampa];

    //se l'array delle concomitanze non è vuoto va avanti
    if (Object.keys(array_concomitanze).length > 0) {

        array_concomitanze = Object.keys(array_concomitanze).sort().reduce(
            (obj, key) => {
                obj[key] = array_concomitanze[key];
                return obj;
            }, {}
        );

        concomitanze.addFeedLine(2);
        concomitanze.addTextAlign(concomitanze.ALIGN_CENTER);
        concomitanze.addTextSize(1, 1);
        concomitanze.addTextStyle(false, false, true, concomitanze.COLOR_1);

        let titolo_presente = false;

        loop1:
        for (var dest_stampa_concomitanza in array_concomitanze) {
            if (array_dest_stampa[dest_stampa_concomitanza].nome_umano !== undefined && dest_stampa_concomitanza !== dest_stampa) {
                for (let codice_ordinamento_concomitanza in array_concomitanze[dest_stampa_concomitanza]) {
                    if (array_concomitanze[dest_stampa_concomitanza][codice_ordinamento_concomitanza] !== undefined && array_concomitanze[dest_stampa_concomitanza][codice_ordinamento_concomitanza] !== null && typeof (array_concomitanze[dest_stampa_concomitanza][codice_ordinamento_concomitanza]) === "object" && Object.keys(array_concomitanze[dest_stampa_concomitanza][codice_ordinamento_concomitanza]).length > 0) {
                        if (portate_corrispondenti.indexOf(array_concomitanze[dest_stampa_concomitanza][codice_ordinamento_concomitanza].portata) !== -1) {

                            Object.values(array_concomitanze[dest_stampa_concomitanza][codice_ordinamento_concomitanza]).forEach((v) => {
                                if (Array.isArray(v) && v[5] === "N" && cerca_in_comanda_corrente(comanda_attuale, v[6], v[7]) === false) {
                                    titolo_presente = true;
                                }
                            });


                        }
                    }
                }
            }
        }

        if (titolo_presente === true) {
            //intestazione con scritta concomitanze
            concomitanze.addText('_______________________________\n');
            concomitanze.addText('CONCOMITANZE\n');
            concomitanze.addTextStyle(false, false, false, concomitanze.COLOR_1);
        }

        for (var dest_stampa_concomitanza in array_concomitanze) {


            if (array_dest_stampa[dest_stampa_concomitanza].nome_umano !== undefined && dest_stampa_concomitanza !== dest_stampa) {

                array_concomitanze[dest_stampa_concomitanza] = Object.keys(array_concomitanze[dest_stampa_concomitanza]).sort().reduce(
                    (obj, key) => {
                        obj[key] = array_concomitanze[dest_stampa_concomitanza][key];
                        return obj;
                    }, {}
                );


                for (let codice_ordinamento_concomitanza in array_concomitanze[dest_stampa_concomitanza]) {

                    if (array_concomitanze[dest_stampa_concomitanza][codice_ordinamento_concomitanza] !== undefined && array_concomitanze[dest_stampa_concomitanza][codice_ordinamento_concomitanza] !== null && typeof (array_concomitanze[dest_stampa_concomitanza][codice_ordinamento_concomitanza]) === "object" && Object.keys(array_concomitanze[dest_stampa_concomitanza][codice_ordinamento_concomitanza]).length > 0) {

                        if (portate_corrispondenti.indexOf(array_concomitanze[dest_stampa_concomitanza][codice_ordinamento_concomitanza].portata) !== -1) {

                            let conteggio_articoli_prima_destinazione_in_portata = Object.values(array_concomitanze[dest_stampa_concomitanza][codice_ordinamento_concomitanza]).filter((v) => { if (v !== null && v[5] === "N" && cerca_in_comanda_corrente(comanda_attuale, v[6], v[7]) === false) { return v; } });

                            if (conteggio_articoli_prima_destinazione_in_portata.length > 0) {
                                concomitanze.addTextAlign(concomitanze.ALIGN_CENTER);
                                concomitanze.addTextSize(1, 1);
                                concomitanze.addTextStyle(false, false, true, concomitanze.COLOR_1);

                                concomitanze.addText('Destinazione: ' + array_dest_stampa[dest_stampa_concomitanza].nome_umano + ' - ' + 'Portata: ' + array_concomitanze[dest_stampa_concomitanza][codice_ordinamento_concomitanza].val + '\n');

                                concomitanze.addTextStyle(false, false, false, concomitanze.COLOR_1);
                                concomitanze.addTextAlign(concomitanze.ALIGN_LEFT);

                                var caratteristiche_concomitanza = alasql("select * from nomi_portate where " + comanda.lingua + "='" + array_concomitanze[dest_stampa_concomitanza][codice_ordinamento_concomitanza].val + "' limit 1;");

                                if (caratteristiche_concomitanza.length > 0) {

                                    if (caratteristiche_concomitanza[0].visu_articoli_concomitanze === "true") {

                                        switch (caratteristiche_concomitanza[0].attributi_font_concomitanze) {
                                            case "D":
                                                concomitanze.addTextSize(2, 2);
                                                break;
                                            case "G":
                                                concomitanze.addTextSize(1, 1);
                                                concomitanze.addTextStyle(false, false, true, concomitanze.COLOR_1);
                                                break;
                                            case "N":
                                            default:
                                                concomitanze.addTextSize(1, 1);
                                                concomitanze.addTextStyle(false, false, false, concomitanze.COLOR_1);
                                        }


                                        for (var node_concomitanza in array_concomitanze[dest_stampa_concomitanza][codice_ordinamento_concomitanza]) {

                                            var nodo = array_concomitanze[dest_stampa_concomitanza][codice_ordinamento_concomitanza][node_concomitanza];

                                            if (nodo !== undefined && nodo[1] !== undefined && nodo[1].length > 1) {

                                                if (nodo[5] !== undefined && nodo[6] !== undefined && nodo[7] !== undefined && nodo[5] === "N" && cerca_in_comanda_corrente(comanda_attuale, nodo[6], nodo[7]) === false) {

                                                    var articolo = filtra_accenti(nodo[1]);
                                                    if (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(" && articolo[0] !== "=" && articolo[0] !== "." && articolo[0] !== "*") {

                                                        let quantita = nodo[0];
                                                        concomitanze.addText(quantita);
                                                        concomitanze.addTextPosition(50);

                                                    } else {

                                                        articolo = ' \t' + articolo;
                                                    }

                                                    concomitanze.addTextPosition(50);

                                                    var counter = 0;
                                                    var stringa = articolo;
                                                    var lettere = 40;
                                                    while (stringa.length > 0) {
                                                        if (counter > 0) {
                                                            concomitanze.addText('\n');
                                                        }
                                                        concomitanze.addText(stringa.substring(0, lettere));
                                                        stringa = stringa.substring(lettere);
                                                        counter++;
                                                    }
                                                }
                                            }
                                            concomitanze.addText('\n');
                                        }

                                    }
                                }
                                concomitanze.addText('\n');
                                concomitanze.addTextAlign(concomitanze.ALIGN_LEFT);
                            }
                        }
                    }

                }
            }
        }
    }

    /* FINE TEST CONCOMITANZA */

    let testo_concomitanze = concomitanze.toString().substr(72).slice(0, -13);

    //ritorna un oggetto con questi 2 parametri
    return { 'concomitanze': concomitanze, 'testo_concomitanze': testo_concomitanze };

}

function non_fare_subito() {

    console.log("** LOG ORDINE", "TASTO SUBITO");
    comanda.esecuzione_comanda_immediata = false;
    $('#switch_esecuzione_comanda_immediata').css('background-image', '');
}

$(document).on('change', '[name="ora_consegna"]', function () {
    non_fare_subito();
    if (comanda.orario_preparazione === "1") {

        if ($('[name="ora_consegna"]').val().length === 5) {

            let a = new Date("01 Jan 2000 " + $('[name="ora_consegna"]').val() + ":00");
            if (comanda.tipo_consegna === "DOMICILIO") {
                a.setMinutes(a.getMinutes() + parseInt("-" + comanda.tempo_preparazione_default) + parseInt("-" + comanda.tempo_preparazione_default_domicilio));
            } else {
                a.setMinutes(a.getMinutes() + parseInt("-" + comanda.tempo_preparazione_default));
            }

            let b = aggZero(a.getHours(), 2) + ":" + aggZero(a.getMinutes(), 2);
            $('[name="orario_preparazione"]').val(b);
        }
    }
});

function selezione_tipo_consegna(stringa_tipo) {

    if (stringa_tipo === "DOMICILIO") {
        comanda.tipo_consegna = 'DOMICILIO';
        comanda.funzionidb.conto_attivo();
        $('.tipo_consegna').removeClass('tipo_consegna_selezionata');
        $('.tipo_consegna.consegna_domicilio').addClass('tipo_consegna_selezionata');
    } else if (stringa_tipo === "RITIRO") {
        comanda.tipo_consegna = 'RITIRO';
        comanda.funzionidb.conto_attivo();
        $('.tipo_consegna').removeClass('tipo_consegna_selezionata');
        $('.tipo_consegna.consegna_ritiro').addClass('tipo_consegna_selezionata');
    }

    if (comanda.orario_preparazione === "1") {

        if ($('[name="ora_consegna"]').val().length === 5) {

            let a = new Date("01 Jan 2000 " + $('[name="ora_consegna"]').val() + ":00");
            if (comanda.tipo_consegna === "DOMICILIO") {
                a.setMinutes(a.getMinutes() + parseInt("-" + comanda.tempo_preparazione_default) + parseInt("-" + comanda.tempo_preparazione_default_domicilio));
            } else {
                a.setMinutes(a.getMinutes() + parseInt("-" + comanda.tempo_preparazione_default));
            }

            let b = aggZero(a.getHours(), 2) + ":" + aggZero(a.getMinutes(), 2);
            $('[name="orario_preparazione"]').val(b);
        }
    }
}


function popup_ordine_disdetto() {
    bootbox.confirm("Sei sicuro di disdire l'ordine corrente?", function (a) {

        if (a === true) {

            comanda.consegna_fattorino_extra = "0.00";

            $("#parcheggia_comanda").modal("hide");
            let c = alasql("select desc_art,stato_record from comanda where stampata_sn='S' and stato_record='ATTIVO'  and fiscalizzata_sn!='S' limit 1;");
            if (c.length > 0) {


                var data = comanda.funzionidb.data_attuale();
                var builder = new epson.ePOSBuilder();
                builder.addTextFont(builder.FONT_A);
                builder.addTextAlign(builder.ALIGN_CENTER);
                builder.addTextSize(2, 2);
                //OVVERO SE L'ARTICOLO E' UNA VARIANTE
                builder.addFeedLine(1);
                builder.addFeedLine(1);
                builder.addText(comanda.parcheggio + '\n');
                builder.addFeedLine(1);
                builder.addText('CONSEGNA: ' + comanda.ora_consegna + ':' + comanda.minuti_consegna + '\n');
                builder.addFeedLine(1);
                builder.addTextSize(1, 1);
                builder.addText("-----------------------------------------\n");
                builder.addTextSize(2, 2);
                builder.addTextStyle(true, false, true, builder.COLOR_1);
                builder.addText('ORDINE DISDETTO\n');
                builder.addTextStyle(false, false, true, builder.COLOR_1);
                builder.addTextSize(1, 1);
                builder.addText("-----------------------------------------\n");
                builder.addTextSize(2, 2);
                builder.addFeedLine(1);
                builder.addText(comanda.tipo_consegna + '\n');
                if (comanda.numero_licenza_cliente === "0629") {
                    builder.addFeedLine(1);
                    let testo_query = "select * from elenco_progressivi_asporto_domicilio where progressivo_comanda='" + id_comanda_attuale_alasql() + "' and fiscalizzata_sn!='S' limit 1;";
                    let progressivo_attuale = alasql(testo_query);
                    if (progressivo_attuale.length > 0) {

                        builder.addText("Num. Consegna: " + progressivo_attuale[0].numero + '\n');
                    }
                }

                builder.addFeedLine(1);
                builder.addTextSize(1, 1);
                builder.addTextAlign(builder.ALIGN_LEFT);
                builder.addText('Disdetto alle ' + data.replace(/-/gi, '/').substr(9) + '\n');
                builder.addFeedLine(1);
                builder.addFeedLine(1);
                builder.addCut(builder.CUT_FEED);
                var request = builder.toString();
                //CONTENUTO CONTO
                //console.log(request);

                //Create a SOAP envelop
                var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
                //Create an XMLHttpRequest object
                var xhr = new XMLHttpRequest();
                //Set the end point address

                let d = alasql("select distinct dest_stampa from comanda where id='" + id_comanda_attuale_alasql() + "' and fiscalizzata_sn!='S';");
                let url = "";
                d.forEach(v => {
                    url = 'http://' + comanda.nome_stampante[v.dest_stampa].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                    if (comanda.nome_stampante[v.dest_stampa].intelligent === "SDS") {
                        url = 'http://' + comanda.nome_stampante[v.dest_stampa].ip + ':8000/SPOOLER?stampante=' + comanda.nome_stampante[v.dest_stampa].devid_nf;
                    }

                    //Open an XMLHttpRequest object
                    xhr.open('POST', url, true);
                    //<Header settings>
                    xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
                    xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
                    xhr.setRequestHeader('SOAPAction', '""');
                    // Send print document
                    xhr.send(soap);
                });
            }







            var data = comanda.funzionidb.data_attuale().substr(0, 8);
            var ora = comanda.funzionidb.data_attuale().substr(9, 8).replace(/-/gi, '/');
            var testo_query = "update comanda set stato_record='CANCELLATO ORDINE DISDETTO DAL CLIENTE " + data + " " + ora + "' WHERE stato_record='ATTIVO' AND ntav_comanda='" + comanda.tavolo + "'  and fiscalizzata_sn!='S' ;";
            comanda.sock.send({
                tipo: "aggiornamento_tavolo",
                operatore: comanda.operatore,
                query: testo_query,
                terminale: comanda.terminale,
                ip: comanda.ip_address
            });
            alasql(testo_query);
            let conta_articoli_rimanenti = alasql("select desc_art FROM comanda WHERE stato_record='ATTIVO' AND ntav_comanda='" + comanda.tavolo + "';");
            /* aggiunto if il 14/12/2020 perchè sennò toglieva il parcheggio anche se restano articoli nell ordine*/
            if (conta_articoli_rimanenti.length === 0) {
                $(".nome_parcheggio").html("");
                righe_forno();
                intesta_cliente(false, true);
                comanda.esecuzione_comanda_immediata = false;
                $('#switch_esecuzione_comanda_immediata').css('background-image', '');
                reset_progressivo_asporto();
            }

            comanda.funzionidb.conto_attivo();
        }

    });
}

async function vedi_consegne_giornata_pony(id_pony) {

    let data_partenza = $('#form_gestione_pony #data_incassi_partenza').multiDatesPicker('getDates')[0];
    let data_arrivo = $('#form_gestione_pony #data_incassi_arrivo').multiDatesPicker('getDates')[0];
    if (data_partenza !== undefined && data_arrivo !== undefined) {

        await comanda.sincro.upload_mysql_temp();
        var query_forno = " select \n\
                        id,id_pony,rag_soc_cliente,desc_art,portata,tipo_consegna,ora_consegna,nome_comanda,\n\
                        cast((((cast(prezzo_vero as float)+cast(QTA as float))*quantita)/100*(100-cast(sconto_perc as float))) as float) as totale_prezzo,\n\
                        quantita as quantita from comanda\n\
                        where \n\
                        id_pony='" + id_pony + "'  and data_servizio>='" + data_partenza + "' and data_servizio<='" + data_arrivo + "' \n\
                        and (stato_record='FORNO' or stato_record='CHIUSO') and tipo_consegna='DOMICILIO' union all select\n\
                        id,id_pony,rag_soc_cliente,desc_art,portata,tipo_consegna,ora_consegna,nome_comanda,\n\
                        cast((((cast(prezzo_vero as float)+cast(QTA as float))*quantita)/100*(100-cast(sconto_perc as float))) as float) as totale_prezzo,\n\
                        quantita as quantita from comanda_temp\n\
                        where \n\
                        id_pony='" + id_pony + "'  and data_servizio>='" + data_partenza + "' and data_servizio<='" + data_arrivo + "' \n\
                        and (stato_record='FORNO' or stato_record='CHIUSO') and tipo_consegna='DOMICILIO'  order by ora_consegna asc,id_pony asc,id asc;";
        var r1 = await comanda.sincro.query_incassi_async(query_forno);
    } else if (data_partenza !== undefined) {
        await comanda.sincro.upload_mysql_temp();
        var query_forno = " select \n\
                        id,id_pony,rag_soc_cliente,desc_art,portata,tipo_consegna,ora_consegna,nome_comanda,\n\
                        cast((((cast(prezzo_vero as float)+cast(QTA as float))*quantita)/100*(100-cast(sconto_perc as float))) as float) as totale_prezzo,\n\
                        quantita as quantita from comanda\n\
                        where \n\
                        id_pony='" + id_pony + "'  and data_servizio='" + data_partenza + "' \n\
                        and (stato_record='FORNO' or stato_record='CHIUSO') and tipo_consegna='DOMICILIO'   union all select\n\
                        id,id_pony,rag_soc_cliente,desc_art,portata,tipo_consegna,ora_consegna,nome_comanda,\n\
                        cast((((cast(prezzo_vero as float)+cast(QTA as float))*quantita)/100*(100-cast(sconto_perc as float))) as float) as totale_prezzo,\n\
                        quantita as quantita from comanda_temp\n\
                        where \n\
                        id_pony='" + id_pony + "'  and data_servizio='" + data_partenza + "' \n\
                        and (stato_record='FORNO' or stato_record='CHIUSO') and tipo_consegna='DOMICILIO'  order by ora_consegna asc,id_pony asc,id asc;";
        var r1 = await comanda.sincro.query_incassi_async(query_forno);
    } else if (data_arrivo !== undefined) {
        await comanda.sincro.upload_mysql_temp();
        var query_forno = " select \n\
                        id,id_pony,rag_soc_cliente,desc_art,portata,tipo_consegna,ora_consegna,nome_comanda,\n\
                        cast((((cast(prezzo_vero as float)+cast(QTA as float))*quantita)/100*(100-cast(sconto_perc as float))) as float) as totale_prezzo,\n\
                        quantita as quantita from comanda\n\
                        where \n\
                        id_pony='" + id_pony + "'  and data_servizio='" + data_arrivo + "' \n\
                        and (stato_record='FORNO' or stato_record='CHIUSO') and tipo_consegna='DOMICILIO'  union all select\n\
                        id,id_pony,rag_soc_cliente,desc_art,portata,tipo_consegna,ora_consegna,nome_comanda,\n\
                        cast((((cast(prezzo_vero as float)+cast(QTA as float))*quantita)/100*(100-cast(sconto_perc as float))) as float) as totale_prezzo,\n\
                        quantita as quantita from comanda_temp\n\
                        where \n\
                        id_pony='" + id_pony + "'  and data_servizio='" + data_arrivo + "' \n\
                        and (stato_record='FORNO' or stato_record='CHIUSO') and tipo_consegna='DOMICILIO'  order by ora_consegna asc,id_pony asc,id asc;";
        var r1 = await comanda.sincro.query_incassi_async(query_forno);
    } else {

        var query_forno = " select \n\
                        id,id_pony,rag_soc_cliente,desc_art,portata,tipo_consegna,ora_consegna,nome_comanda,\n\
                        cast((((cast(prezzo_vero as float)+cast(QTA as float))*quantita)/100*(100-cast(sconto_perc as float))) as float) as totale_prezzo,\n\
                        quantita as quantita from comanda\n\
                        where \n\
                        id_pony='" + id_pony + "'  and data_servizio='" + comanda.data_servizio + "' \n\
                        and (stato_record='FORNO' or stato_record='CHIUSO') and tipo_consegna='DOMICILIO'  order by ora_consegna asc,id_pony asc,id asc";
        var r1 = alasql(query_forno);
    }




    var nome_pony = "";
    if (oggetto_pony[id_pony] !== undefined) {
        nome_pony = oggetto_pony[id_pony];
    }

    var risultato = new Array();
    r1.forEach(function (e) {

        var query_forno_2 = "select \n\
                            telefono,cellulare,citta,comune,indirizzo,numero,provincia,cap\n\
                            from clienti \n\
                            where \n\
                            id=" + e.rag_soc_cliente + " \n\
                            limit 1;";
        var r2 = alasql(query_forno_2);
        e.totale_prezzo = parseFloat(e.totale_prezzo).toFixed(8);

        if (r2.length > 0) {
            risultato.push({
                id: e.id,
                id_pony: e.id_pony,
                desc_art: e.desc_art,
                portata: e.portata,
                tipo_consegna: e.tipo_consegna,
                ora_consegna: e.ora_consegna,
                nome_comanda: e.nome_comanda,
                totale_prezzo: e.totale_prezzo,
                quantita: e.quantita,
                telefono: r2[0].telefono,
                cellulare: r2[0].cellulare,
                citta: r2[0].citta,
                comune: r2[0].comune,
                indirizzo: r2[0].indirizzo,
                numero: r2[0].numero,
                provincia: r2[0].provincia,
                cap: r2[0].cap
            });
        } else {
            risultato.push({
                id: e.id,
                id_pony: e.id_pony,
                desc_art: e.desc_art,
                portata: e.portata,
                tipo_consegna: e.tipo_consegna,
                ora_consegna: e.ora_consegna,
                nome_comanda: e.nome_comanda,
                totale_prezzo: e.totale_prezzo,
                quantita: e.quantita,
                telefono: "",
                cellulare: "",
                citta: "",
                comune: "",
                indirizzo: "",
                numero: "",
                provincia: "",
                cap: ""
            });
        }
    });
    var oggetto_consegne = new Object();
    var totale_prezzo = 0;
    var totale_pizze = 0;
    var totale_bibite = 0;

    let html_page = "";

    html_page += "-----------------------------------------<br>";
    html_page += "CONSEGNE DI " + nome_pony + "<br>";
    html_page += new Date().format("dd/mm/yyyy") + "<br>";
    html_page += "-----------------------------------------<br>";

    risultato.forEach(function (obj) {



        if (oggetto_consegne[obj.ora_consegna] === undefined) {
            oggetto_consegne[obj.ora_consegna] = new Object();
        }

        if (oggetto_consegne[obj.ora_consegna][obj.id_pony] === undefined) {
            oggetto_consegne[obj.ora_consegna][obj.id_pony] = new Object();
        }


        if (oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id] === undefined) {
            oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id] = new Object();
            oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].qta_pizze = 0;
            oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].qta_bibite = 0;
            oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].prezzo = 0;
            oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].nome_comanda = obj.nome_comanda;
        }




        if (obj.desc_art[0] !== "+" && obj.desc_art[0] !== "-" && obj.desc_art[0] !== "=" && obj.desc_art[0] !== "*" && obj.desc_art[0] !== "x" && obj.desc_art[0] !== "(") {

            if (obj.portata.indexOf("P") !== -1) {
                totale_pizze += parseInt(obj.quantita);
                oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].qta_pizze += parseInt(obj.quantita);
            } else if (obj.portata === "B") {
                totale_bibite += parseInt(obj.quantita);
                oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].qta_bibite += parseInt(obj.quantita);
            }


            oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].prezzo += parseFloat(obj.totale_prezzo);
        } else {
            oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].prezzo += parseFloat(obj.totale_prezzo);
        }


        totale_prezzo += parseFloat(obj.totale_prezzo);

    });
    for (var ora_consegna in oggetto_consegne) {

        html_page += "ORE: " + ora_consegna + "<br>";

        for (var id_pony in oggetto_consegne[ora_consegna]) {


            for (var id in oggetto_consegne[ora_consegna][id_pony]) {

                //console.log("STAMPA_CONSEGNE - DESTINATARIO ", oggetto_consegne[ora_consegna][id_pony][id].nome_comanda);

                //console.log("STAMPA_CONSEGNE - NUM. PIZZE ", oggetto_consegne[ora_consegna][id_pony][id].qta);

                //console.log("STAMPA_CONSEGNE - PREZZO: € ", arrotonda_prezzo(oggetto_consegne[ora_consegna][id_pony][id].prezzo));

                html_page += oggetto_consegne[ora_consegna][id_pony][id].nome_comanda.trim();
                if (comanda.numero_licenza_cliente === "0629") {
                    let testo_query = "select * from elenco_progressivi_asporto_domicilio where progressivo_comanda='" + id + "' limit 1;";
                    let progressivo_attuale = alasql(testo_query);
                    if (progressivo_attuale.length > 0) {

                        html_page += " (" + progressivo_attuale[0].numero + ")";
                    }

                }

                html_page += ": ";
                if (oggetto_consegne[ora_consegna][id_pony][id].qta_pizze === "1" || oggetto_consegne[ora_consegna][id_pony][id].qta_pizze === 1) {
                    html_page += oggetto_consegne[ora_consegna][id_pony][id].qta_pizze + " pizza";

                } else {
                    html_page += oggetto_consegne[ora_consegna][id_pony][id].qta_pizze + " pizze";
                }

                if (oggetto_consegne[ora_consegna][id_pony][id].qta_bibite === "1" || oggetto_consegne[ora_consegna][id_pony][id].qta_bibite === 1) {
                    html_page += " " + oggetto_consegne[ora_consegna][id_pony][id].qta_bibite + " bibita";
                } else if (oggetto_consegne[ora_consegna][id_pony][id].qta_bibite > 0) {
                    html_page += " " + oggetto_consegne[ora_consegna][id_pony][id].qta_bibite + " bibite";
                }
                let costo_totale_qta = arrotonda_prezzo(oggetto_consegne[ora_consegna][id_pony][id].prezzo);
                html_page += " € " + parseFloat(oggetto_consegne[ora_consegna][id_pony][id].prezzo).toFixed(2);
                html_page += "<br>";
            }

        }

        html_page += "-----------------------------------------<br>";
    }
    totale_costo_stampa = arrotondamento_pagamento(totale_prezzo);
    html_page += "TOTALE: € " + parseFloat(totale_costo_stampa).toFixed(2) + "<br>";
    html_page += "-----------------------------------------<br>";
    html_page += "Pizze Consegnate: " + totale_pizze + "<br>";
    html_page += "Bibite Consegnate: " + totale_bibite + "<br>";


    $("#popup_generico .contenuto").html(html_page);
    $("#popup_generico").modal("show");
}

async function stampa_consegne_giornata_pony(id_pony) {

    let data_partenza = $('#form_gestione_pony #data_incassi_partenza').multiDatesPicker('getDates')[0];
    let data_arrivo = $('#form_gestione_pony #data_incassi_arrivo').multiDatesPicker('getDates')[0];
    if (data_partenza !== undefined && data_arrivo !== undefined) {

        await comanda.sincro.upload_mysql_temp();
        var query_forno = " select \n\
        id,id_pony,rag_soc_cliente,desc_art,portata,tipo_consegna,ora_consegna,nome_comanda,\n\
        cast((((cast(prezzo_vero as float)+cast(QTA as float))*quantita)/100*(100-cast(sconto_perc as float))) as float) as totale_prezzo,\n\
        quantita as quantita from comanda\n\
        where \n\
        id_pony='" + id_pony + "'  and data_servizio='" + data_partenza + "' \n\
        and (stato_record='FORNO' or stato_record='CHIUSO') and tipo_consegna='DOMICILIO'   union all select\n\
        id,id_pony,rag_soc_cliente,desc_art,portata,tipo_consegna,ora_consegna,nome_comanda,\n\
        cast((((cast(prezzo_vero as float)+cast(QTA as float))*quantita)/100*(100-cast(sconto_perc as float))) as float) as totale_prezzo,\n\
        quantita as quantita from comanda_temp\n\
        where \n\
        id_pony='" + id_pony + "'  and data_servizio='" + data_partenza + "' \n\
        and (stato_record='FORNO' or stato_record='CHIUSO') and tipo_consegna='DOMICILIO'  order by ora_consegna asc,id_pony asc,id asc;";
        var r1 = await comanda.sincro.query_incassi_async(query_forno);
    } else if (data_arrivo !== undefined) {
        await comanda.sincro.upload_mysql_temp();
        var query_forno = " select \n\
        id,id_pony,rag_soc_cliente,desc_art,portata,tipo_consegna,ora_consegna,nome_comanda,\n\
        cast((((cast(prezzo_vero as float)+cast(QTA as float))*quantita)/100*(100-cast(sconto_perc as float))) as float) as totale_prezzo,\n\
        quantita as quantita from comanda\n\
        where \n\
        id_pony='" + id_pony + "'  and data_servizio='" + data_arrivo + "' \n\
        and (stato_record='FORNO' or stato_record='CHIUSO') and tipo_consegna='DOMICILIO'  union all select\n\
        id,id_pony,rag_soc_cliente,desc_art,portata,tipo_consegna,ora_consegna,nome_comanda,\n\
        cast((((cast(prezzo_vero as float)+cast(QTA as float))*quantita)/100*(100-cast(sconto_perc as float))) as float) as totale_prezzo,\n\
        quantita as quantita from comanda_temp\n\
        where \n\
        id_pony='" + id_pony + "'  and data_servizio='" + data_arrivo + "' \n\
        and (stato_record='FORNO' or stato_record='CHIUSO') and tipo_consegna='DOMICILIO'  order by ora_consegna asc,id_pony asc,id asc;";
        var r1 = await comanda.sincro.query_incassi_async(query_forno);
    } else {

        var query_forno = " select \n\
        id,id_pony,rag_soc_cliente,desc_art,portata,tipo_consegna,ora_consegna,nome_comanda,\n\
        cast((((cast(prezzo_vero as float)+cast(QTA as float))*quantita)/100*(100-cast(sconto_perc as float))) as float) as totale_prezzo,\n\
        quantita as quantita from comanda\n\
        where \n\
        id_pony='" + id_pony + "'  and data_servizio='" + comanda.data_servizio + "' \n\
        and (stato_record='FORNO' or stato_record='CHIUSO') and tipo_consegna='DOMICILIO'  order by ora_consegna asc,id_pony asc,id asc";
        var r1 = alasql(query_forno);
    }




    bootbox.alert("STAMPA IN CORSO...");
    var nome_pony = "";
    if (oggetto_pony[id_pony] !== undefined) {
        nome_pony = oggetto_pony[id_pony];
    }

    var risultato = new Array();
    r1.forEach(function (e) {

        var query_forno_2 = "select \n\
                            telefono,cellulare,citta,comune,indirizzo,numero,provincia,cap\n\
                            from clienti \n\
                            where \n\
                            id=" + e.rag_soc_cliente + " \n\
                            limit 1;";
        var r2 = alasql(query_forno_2);
        e.totale_prezzo = parseFloat(e.totale_prezzo).toFixed(8);

        if (r2.length > 0) {
            risultato.push({
                id: e.id,
                id_pony: e.id_pony,
                desc_art: e.desc_art,
                portata: e.portata,
                tipo_consegna: e.tipo_consegna,
                ora_consegna: e.ora_consegna,
                nome_comanda: e.nome_comanda,
                totale_prezzo: e.totale_prezzo,
                quantita: e.quantita,
                telefono: r2[0].telefono,
                cellulare: r2[0].cellulare,
                citta: r2[0].citta,
                comune: r2[0].comune,
                indirizzo: r2[0].indirizzo,
                numero: r2[0].numero,
                provincia: r2[0].provincia,
                cap: r2[0].cap
            });
        } else {
            risultato.push({
                id: e.id,
                id_pony: e.id_pony,
                desc_art: e.desc_art,
                portata: e.portata,
                tipo_consegna: e.tipo_consegna,
                ora_consegna: e.ora_consegna,
                nome_comanda: e.nome_comanda,
                totale_prezzo: e.totale_prezzo,
                quantita: e.quantita,
                telefono: "",
                cellulare: "",
                citta: "",
                comune: "",
                indirizzo: "",
                numero: "",
                provincia: "",
                cap: ""
            });
        }
    });
    var oggetto_consegne = new Object();
    var totale_prezzo = 0;
    var totale_pizze = 0;
    var totale_bibite = 0;
    var builder = new epson.ePOSBuilder();
    builder.addTextFont(builder.FONT_A);
    builder.addTextAlign(builder.ALIGN_CENTER);
    builder.addTextSize(1, 1);
    builder.addText("-----------------------------------------\n");
    builder.addTextSize(2, 2);
    builder.addText('CONSEGNE DI ' + nome_pony + '\n');
    builder.addTextSize(1, 1);
    builder.addFeedLine(1);
    builder.addText(new Date().format("dd/mm/yyyy") + '\n');
    builder.addText("-----------------------------------------\n");
    risultato.forEach(function (obj) {



        if (oggetto_consegne[obj.ora_consegna] === undefined) {
            oggetto_consegne[obj.ora_consegna] = new Object();
        }

        if (oggetto_consegne[obj.ora_consegna][obj.id_pony] === undefined) {
            oggetto_consegne[obj.ora_consegna][obj.id_pony] = new Object();
        }


        if (oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id] === undefined) {
            oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id] = new Object();
            oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].qta_pizze = 0;
            oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].qta_bibite = 0;
            oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].prezzo = 0;
            oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].nome_comanda = obj.nome_comanda;
        }





        if (obj.desc_art[0] !== "+" && obj.desc_art[0] !== "-" && obj.desc_art[0] !== "=" && obj.desc_art[0] !== "*" && obj.desc_art[0] !== "x" && obj.desc_art[0] !== "(") {

            if (obj.portata.indexOf("P") !== -1) {
                totale_pizze += parseInt(obj.quantita);
                oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].qta_pizze += parseInt(obj.quantita);
            } else if (obj.portata === "B") {
                totale_bibite += parseInt(obj.quantita);
                oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].qta_bibite += parseInt(obj.quantita);
            }


            oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].prezzo += parseFloat(obj.totale_prezzo);
        } else {
            oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].prezzo += parseFloat(obj.totale_prezzo);
        }


        totale_prezzo += parseFloat(obj.totale_prezzo);
    });
    for (var ora_consegna in oggetto_consegne) {

        builder.addTextAlign(builder.ALIGN_CENTER);
        builder.addTextStyle(false, false, true, builder.COLOR_1);
        //console.log("STAMPA_CONSEGNE - ORA CONSEGNA ", ora_consegna);

        builder.addText('ORE: ' + ora_consegna + '\n');
        builder.addFeedLine(1);
        for (var id_pony in oggetto_consegne[ora_consegna]) {


            for (var id in oggetto_consegne[ora_consegna][id_pony]) {

                //console.log("STAMPA_CONSEGNE - DESTINATARIO ", oggetto_consegne[ora_consegna][id_pony][id].nome_comanda);

                //console.log("STAMPA_CONSEGNE - NUM. PIZZE ", oggetto_consegne[ora_consegna][id_pony][id].qta);

                //console.log("STAMPA_CONSEGNE - PREZZO: € ", arrotonda_prezzo(oggetto_consegne[ora_consegna][id_pony][id].prezzo));

                builder.addTextAlign(builder.ALIGN_LEFT);
                builder.addTextPosition(35);
                builder.addTextStyle(false, false, true, builder.COLOR_1);
                builder.addText(oggetto_consegne[ora_consegna][id_pony][id].nome_comanda.trim());
                if (comanda.numero_licenza_cliente === "0629") {
                    let testo_query = "select * from elenco_progressivi_asporto_domicilio where progressivo_comanda='" + id + "' limit 1;";
                    let progressivo_attuale = alasql(testo_query);
                    if (progressivo_attuale.length > 0) {

                        builder.addText(" (" + progressivo_attuale[0].numero + ")");
                    }

                }

                builder.addText(': ');
                builder.addTextStyle(false, false, false, builder.COLOR_1);
                if (oggetto_consegne[ora_consegna][id_pony][id].qta_pizze === "1" || oggetto_consegne[ora_consegna][id_pony][id].qta_pizze === 1) {
                    builder.addText(oggetto_consegne[ora_consegna][id_pony][id].qta_pizze + ' pizza');
                } else {
                    builder.addText(oggetto_consegne[ora_consegna][id_pony][id].qta_pizze + ' pizze');
                }

                if (oggetto_consegne[ora_consegna][id_pony][id].qta_bibite === "1" || oggetto_consegne[ora_consegna][id_pony][id].qta_bibite === 1) {
                    builder.addText(' ' + oggetto_consegne[ora_consegna][id_pony][id].qta_bibite + ' bibita');
                } else if (oggetto_consegne[ora_consegna][id_pony][id].qta_bibite > 0) {
                    builder.addText(' ' + oggetto_consegne[ora_consegna][id_pony][id].qta_bibite + ' bibite');
                }

                builder.addText(' € ' + arrotondamento_pagamento(parseFloat(oggetto_consegne[ora_consegna][id_pony][id].prezzo)).toFixed(2));

                builder.addText('\n');
            }

        }

        builder.addTextAlign(builder.ALIGN_CENTER);
        builder.addText("-----------------------------------------\n");
    }

    builder.addTextAlign(builder.ALIGN_CENTER);
    builder.addTextStyle(false, false, false, builder.COLOR_1);
    builder.addTextSize(2, 2);
    //console.log("STAMPA_CONSEGNE", "TOTALE: € " + arrotonda_prezzo(parseFloat(totale_prezzo)));

    let costo_totale_stampa = arrotondamento_pagamento(totale_prezzo);
    builder.addText("TOTALE: € " + parseFloat(costo_totale_stampa).toFixed(2) + "\n");
    builder.addTextStyle(false, false, false, builder.COLOR_1);
    builder.addTextSize(1, 1);
    builder.addText("-----------------------------------------\n");
    //console.log("STAMPA_CONSEGNE", "Pizze Consegnate: " + totale_pizze);

    builder.addText("Pizze Consegnate: " + totale_pizze + "\n");
    builder.addText("Bibite Consegnate: " + totale_bibite + "\n");
    builder.addCut(builder.CUT_FEED);
    var request = builder.toString();
    var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
    var url = "";
    if (comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].intelligent === "SDS") {
        url = 'http://' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "") + numero_layout].ip + ':8000/SPOOLER?stampante=' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "") + numero_layout].devid_nf;
    } else {
        url = 'http://' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
    }

    var xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    //<Header settings>
    xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
    xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
    xhr.setRequestHeader('SOAPAction', '""');
    // Send print document
    xhr.send(soap);
    xhr.onreadystatechange = function () {
        bootbox.hideAll();
    };
}

function stampa_qr_cliente(ora_consegna, id_pony, consegna_gap, avviso_stampa_in_corso) {
    console.log("STAMPA QR CLIENTE", ora_consegna);
    if (avviso_stampa_in_corso !== false) {
        bootbox.alert("STAMPA IN CORSO...");
    }

    var d = new Date();
    var Y = d.getFullYear();
    var M = addZero((d.getMonth() + 1), 2);
    var D = addZero(d.getDate(), 2);
    var h = addZero(d.getHours(), 2);
    var m = addZero(d.getMinutes(), 2);
    var s = addZero(d.getSeconds(), 2);
    var ms = addZero(d.getMilliseconds(), 3);
    var data_oggi = Y + '' + M + '' + D;
    var oggetto_raggruppamento = new Object();
    var q1 = "select id,id_pony,desc_art,portata,tipo_consegna,ora_consegna,consegna_gap,rag_soc_cliente,nome_comanda,quantita,prezzo_vero,QTA,sconto_perc\n\
                from comanda\n\
                where\n\
                id_pony='" + id_pony + "' and ora_consegna='" + ora_consegna + "' and data_servizio='" + comanda.data_servizio + "' \n\
                and (stato_record='FORNO' or stato_record='ATTIVO') and tipo_consegna='DOMICILIO' ;";
    var r1 = alasql(q1);
    r1.forEach(function (e) {



        if (e.QTA === "") {
            e.QTA = "0";
        }

        if (e.prezzo_vero === "") {
            e.prezzo_vero = "0";
        }

        if (e.sconto_perc === "") {
            e.sconto_perc = "0";
        }



        if (e.rag_soc_cliente.substr(0, 1) === 'P') {
            e.telefono = "";
            e.cellulare = "";
            e.citta = "";
            e.comune = "";
            e.indirizzo = "";
            e.numero = "";
            e.provincia = "";
            e.cap = "";
            //array_risultati.push(e);
            if (oggetto_raggruppamento[e.id] === undefined) {
                oggetto_raggruppamento[e.id] = new Object();
            }
            if (oggetto_raggruppamento[e.id][e.nome_comanda] === undefined) {
                oggetto_raggruppamento[e.id][e.nome_comanda] = new Object();
            }
            if (oggetto_raggruppamento[e.id][e.nome_comanda][e.ora_consegna] === undefined) {
                oggetto_raggruppamento[e.id][e.nome_comanda][e.ora_consegna] = new Object();
            }

            /* tutte le portate che si conteggiano come pizze devono essere raggruppate con il simbolo P, altrimenti il conteggio nella stampa consegne fa casino */
            /* modificato 23/07/2019 */
            if (e.portata.indexOf("P") !== -1) {
                e.portata = "P";
            }

            if (oggetto_raggruppamento[e.id][e.nome_comanda][e.ora_consegna][e.portata] === undefined) {
                if (e.desc_art[0] !== "+" && e.desc_art[0] !== "-" && e.desc_art[0] !== "=" && e.desc_art[0] !== "*" && e.desc_art[0] !== "x" && e.desc_art[0] !== "(") {
                    e.quantita = parseInt(e.quantita);
                }
                e.prezzo_totale = ((parseFloat(e.prezzo_vero) + parseFloat(e.QTA)) * parseInt(e.quantita)) - (((parseFloat(e.prezzo_vero) + parseFloat(e.QTA)) * parseInt(e.quantita)) / 100 * parseFloat(e.sconto_perc));
                e.nome_portata = comanda.nome_portata[e.portata];
                oggetto_raggruppamento[e.id][e.nome_comanda][e.ora_consegna][e.portata] = e;
            } else {
                if (e.desc_art[0] !== "+" && e.desc_art[0] !== "-" && e.desc_art[0] !== "=" && e.desc_art[0] !== "*" && e.desc_art[0] !== "x" && e.desc_art[0] !== "(") {
                    oggetto_raggruppamento[e.id][e.nome_comanda][e.ora_consegna][e.portata].quantita += parseInt(e.quantita);
                }
                oggetto_raggruppamento[e.id][e.nome_comanda][e.ora_consegna][e.portata].prezzo_totale += ((parseFloat(e.prezzo_vero) + parseFloat(e.QTA)) * parseInt(e.quantita)) - (((parseFloat(e.prezzo_vero) + parseFloat(e.QTA)) * parseInt(e.quantita)) / 100 * parseFloat(e.sconto_perc));
            }
        } else {
            try {
                var q2 = "select note,telefono,cellulare,citta,comune,indirizzo,numero,provincia,cap from clienti where id=" + e.rag_soc_cliente + " limit 1;";
                var r2 = alasql(q2);
                if (r2[0] !== undefined) {
                    e.note = r2[0].note;
                    e.telefono = r2[0].telefono;
                    e.cellulare = r2[0].cellulare;
                    e.citta = r2[0].citta;
                    e.comune = r2[0].comune;
                    e.indirizzo = r2[0].indirizzo;
                    e.numero = r2[0].numero;
                    e.provincia = r2[0].provincia;
                    e.cap = r2[0].cap;
                    //array_risultati.push(e);
                    if (oggetto_raggruppamento[e.id] === undefined) {
                        oggetto_raggruppamento[e.id] = new Object();
                    }
                    if (oggetto_raggruppamento[e.id][e.nome_comanda] === undefined) {
                        oggetto_raggruppamento[e.id][e.nome_comanda] = new Object();
                    }
                    if (oggetto_raggruppamento[e.id][e.nome_comanda][e.ora_consegna] === undefined) {
                        oggetto_raggruppamento[e.id][e.nome_comanda][e.ora_consegna] = new Object();
                    }

                    /* tutte le portate che si conteggiano come pizze devono essere raggruppate con il simbolo P, altrimenti il conteggio nella stampa consegne fa casino */
                    /* modificato 23/07/2019 */
                    if (e.portata.indexOf("P") !== -1) {
                        e.portata = "P";
                    }

                    if (oggetto_raggruppamento[e.id][e.nome_comanda][e.ora_consegna][e.portata] === undefined) {
                        if (e.desc_art[0] !== "+" && e.desc_art[0] !== "-" && e.desc_art[0] !== "=" && e.desc_art[0] !== "*" && e.desc_art[0] !== "x" && e.desc_art[0] !== "(") {
                            e.quantita = parseInt(e.quantita);
                        }
                        e.prezzo_totale = ((parseFloat(e.prezzo_vero) + parseFloat(e.QTA)) * parseInt(e.quantita)) - (((parseFloat(e.prezzo_vero) + parseFloat(e.QTA)) * parseInt(e.quantita)) / 100 * parseFloat(e.sconto_perc));
                        e.nome_portata = comanda.nome_portata[e.portata];
                        oggetto_raggruppamento[e.id][e.nome_comanda][e.ora_consegna][e.portata] = e;
                    } else {
                        if (e.desc_art[0] !== "+" && e.desc_art[0] !== "-" && e.desc_art[0] !== "=" && e.desc_art[0] !== "*" && e.desc_art[0] !== "x" && e.desc_art[0] !== "(") {
                            oggetto_raggruppamento[e.id][e.nome_comanda][e.ora_consegna][e.portata].quantita += parseInt(e.quantita);
                        }
                        oggetto_raggruppamento[e.id][e.nome_comanda][e.ora_consegna][e.portata].prezzo_totale += ((parseFloat(e.prezzo_vero) + parseFloat(e.QTA)) * parseInt(e.quantita)) - (((parseFloat(e.prezzo_vero) + parseFloat(e.QTA)) * parseInt(e.quantita)) / 100 * parseFloat(e.sconto_perc));
                    }
                }
            } catch (err) {
                console.log("ERRORE GESTITO" + err);
            }
        }
    });
    var array_risultati = new Array();
    for (var id in oggetto_raggruppamento) {
        for (var nome_comanda in oggetto_raggruppamento[id]) {
            for (var ora_consegna in oggetto_raggruppamento[id][nome_comanda]) {
                //in ora consegna c'è tutto
                array_risultati.push(oggetto_raggruppamento[id][nome_comanda][ora_consegna]);
            }
        }
    }

    var q3 = "select * from ? order by ora_consegna asc;";
    var risultato = alasql(q3, [array_risultati]);
    var builder = new epson.ePOSBuilder();
    builder.addTextFont(builder.FONT_A);
    builder.addTextAlign(builder.ALIGN_CENTER);
    builder.addTextSize(1, 1);
    builder.addText("-----------------------------------------\n");
    builder.addTextSize(2, 2);
    var query_gap_consegna = "select time_gap_consegna from impostazioni_fiscali where id=" + comanda.folder_number + ";"
    var result = alasql(query_gap_consegna);
    if (result[0].time_gap_consegna == "1" && consegna_gap != "undefined" && consegna_gap != "" && consegna_gap!=undefined && ora_consegna != "") {
        builder.addText('CONSEGNA DELLE ' + ora_consegna + '\n');
        builder.addText('               ' + consegna_gap + '\n');
    }
    else {
        builder.addText('CONSEGNA DELLE ' + ora_consegna + '\n');
    }

    builder.addTextSize(1, 1);
    builder.addFeedLine(1);
    builder.addText(new Date().format("dd/mm/yyyy") + '\n');
    builder.addText("-----------------------------------------\n");
    var counter = 0;
    var totale_pizze = 0;
    var totale_bibite = 0;
    risultato.forEach(function (obj) {

        var obj0 = Object.values(obj);
        var nome = obj0[0]['nome_comanda'];
        var indirizzo = obj0[0]['indirizzo'];
        var numero = obj0[0]['numero'];
        var cap = obj0[0]['cap'];
        var citta = obj0[0]['citta'];
        var comune = obj0[0]['comune'];
        var provincia = obj0[0]['provincia'];
        var cellulare = obj0[0]['cellulare'];
        var telefono = obj0[0]['telefono'];
        var note = obj0[0]['note'];
        var destinazione = indirizzo + ", " + numero + " " + comune + " " + " " + cap + " " + provincia;
        builder.addTextStyle(false, false, true, builder.COLOR_1);
        builder.addFeedLine(1);
        builder.addTextAlign(builder.ALIGN_CENTER);
        builder.addText('DATI CLIENTE:\n');
        builder.addTextStyle(false, false, true, builder.COLOR_1);
        builder.addFeedLine(1);
        //if (counter % 2 === 0) {
        builder.addPageBegin();
        if (note !== null && note.length > 0) {
            builder.addPageArea(40, 0, 330, 280);
        } else {
            builder.addPageArea(40, 0, 330, 220);
        }
        builder.addTextAlign(builder.ALIGN_CENTER);
        builder.addText(nome.substr(0, 26) + '\n');
        builder.addTextStyle(false, false, false, builder.COLOR_1);
        builder.addText(indirizzo + "," + numero + '\n');
        builder.addText(cap + " " + comune + " " + provincia + '\n');
        if (cellulare.length !== 0 || telefono.length !== 0) {
            builder.addText(cellulare + " " + " " + telefono + '\n');
        }
        if (note !== null && note.length > 0) {
            builder.addTextStyle(false, false, true, builder.COLOR_1);
            builder.addText('Note: ' + note.substr(0, 30) + '\n');
            builder.addTextStyle(false, false, false, builder.COLOR_1);
        }
        builder.addTextStyle(false, false, true, builder.COLOR_1);
        var totale_prezzo = 0.00;
        for (let obj_portata in obj) {

            if (obj[obj_portata].desc_art[0] !== "+" && obj[obj_portata].desc_art[0] !== "-" && obj[obj_portata].desc_art[0] !== "=" && obj[obj_portata].desc_art[0] !== "*" && obj[obj_portata].desc_art[0] !== "x" && obj[obj_portata].desc_art[0] !== "(") {


                var portata = obj[obj_portata]['nome_portata'];
                var numero_portata = obj[obj_portata]['portata'];
                var nome_portata = "";
                totale_prezzo += parseFloat(obj[obj_portata]['prezzo_totale']);
                if (numero_portata === "P") {
                    totale_pizze += obj[obj_portata]['quantita'];
                    if (obj[obj_portata]['quantita'] === "1" || obj[obj_portata]['quantita'] === 1) {
                        nome_portata = "PIZZA";
                    } else {
                        nome_portata = "PIZZE";
                    }

                    builder.addText("- " + obj[obj_portata]['quantita'] + ' ' + nome_portata + ' -\n');
                } else if (numero_portata === "B") {
                    totale_bibite += obj[obj_portata]['quantita'];
                    if (obj[obj_portata]['quantita'] === "1" || obj[obj_portata]['quantita'] === 1) {
                        nome_portata = "BIBITA";
                    } else {
                        nome_portata = "BIBITE";
                    }
                    builder.addText("- " + obj[obj_portata]['quantita'] + ' ' + nome_portata + ' -\n');
                    if (comanda.numero_licenza_cliente === "0629") {
                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                        builder.addTextAlign(builder.ALIGN_LEFT);
                        alasql(q1).filter(v => {
                            if (v.portata === "B" && obj["B"].id === v.id) {
                                return v;
                            }
                        }).forEach((v) => {
                            builder.addText("              " + v.quantita + " " + v.desc_art + '\n');
                        });
                        builder.addTextAlign(builder.ALIGN_CENTER);
                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                    }

                } else if (portata !== undefined && portata !== "undefined") {
                    nome_portata = portata;
                    builder.addText(obj[obj_portata]['quantita'] + ' ' + nome_portata + ' \n');
                }


            }
        }
        builder.addTextStyle(false, false, true, builder.COLOR_1);
        builder.addText('€ ' + totale_prezzo.toFixed(2) + ' \n');
        builder.addTextStyle(false, false, false, builder.COLOR_1);
        var L_0629 = "";
        if (comanda.numero_licenza_cliente === "0629" && obj0[0]['tipo_consegna'] === "DOMICILIO") {
            let testo_query = "select * from elenco_progressivi_asporto_domicilio where progressivo_comanda='" + obj0[0]['id'] + "' limit 1;";
            let progressivo_attuale = alasql(testo_query);
            if (progressivo_attuale.length > 0) {

                L_0629 = "\nNumero Consegna: " + progressivo_attuale[0].numero + "\n";
            }

            builder.addText(L_0629);
        }

        builder.addPageArea(370, 0, 300, 200);
        builder.addSymbol("https://www.google.com/maps/dir/?api=1&destination=" + destinazione.replace(/\s+/g, "+") + "&travelmode=DRIVING", builder.SYMBOL_QRCODE_MODEL_2, builder.LEVEL_DEFAULT, 4);
        builder.addPageEnd();
        counter++;
        builder.addTextAlign(builder.ALIGN_CENTER);
        builder.addText("-----------------------------------------\n");
    });
    builder.addTextStyle(false, false, true, builder.COLOR_1);
    builder.addText("TOTALE PIZZE DA CONSEGNARE: " + totale_pizze + "\n");
    builder.addText("TOTALE BIBITE DA CONSEGNARE: " + totale_bibite + "\n");
    builder.addTextStyle(false, false, false, builder.COLOR_1);
    builder.addText("-----------------------------------------\n");
    builder.addCut(builder.CUT_FEED);
    var request = builder.toString();
    var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
    var url = "";
    if (comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].intelligent === "SDS") {
        url = 'http://' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "") + numero_layout].ip + ':8000/SPOOLER?stampante=' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "") + numero_layout].devid_nf;
    } else {
        url = 'http://' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
    }
    aggiungi_spool_stampa(url, soap, "CONTO", undefined, undefined, undefined, false);
    setTimeout(function () {
        bootbox.hideAll();
    }, 1500);
}

let qrcode_fattorino = undefined;

function scarica_qr_pony(id) {

    let dati_fattorino = alasql("select * from pony_express where id=" + id + ";");

    if (dati_fattorino.length > 0) {

        /*builder.addText("-----------------------------------------\n");
        builder.addText('QR FATTORINO\n');
        builder.addText(dati_fattorino[0].nome + '\n');
       
        builder.addText("-----------------------------------------\n");
        builder.addSymbol(id, builder.SYMBOL_QRCODE_MODEL_2, builder.LEVEL_DEFAULT, 4);
        builder.addText("-----------------------------------------\n");
        */

        $("#scritte_qrcode_fattorino").html("<h1>" + dati_fattorino[0].nome + "</h1>");


        if (qrcode_fattorino === undefined) {
            qrcode_fattorino = new QRCode(document.getElementById("qrcode_fattorino"), {
                text: id,
                width: 200,
                height: 200
            });
        } else {
            qrcode_fattorino.clear();
            qrcode_fattorino.makeCode(id);
        }

        setTimeout(() => {

            var canvas = document.createElement("canvas");

            // set desired size of transparent image
            canvas.width = 220;
            canvas.height = 300;

            var ctx = canvas.getContext("2d");
            ctx.font = "30px Arial";
            ctx.fillText(dati_fattorino[0].nome, 10, 50);

            var img = $("#qrcode_fattorino img")[0];;
            ctx.drawImage(img, 10, 70);

            /* scarica immagine */
            var a = $("<a>")
                .attr("href", canvas.toDataURL())
                .attr("download", dati_fattorino[0].nome + ".png")
                .appendTo("body");

            a[0].click();

            a.remove();

            canvas.remove();
        }, 1000);



    }
}

function vedi_consegne_totali_pony() {


    var d = new Date();
    var Y = d.getFullYear();
    var M = addZero((d.getMonth() + 1), 2);
    var D = addZero(d.getDate(), 2);
    var data_oggi = Y + '' + M + '' + D;
    var risultato = new Array();
    //group by comanda.id,comanda.nome_comanda,comanda.ora_consegna


    var query_forno = " select id,id_pony,desc_art,portata,tipo_consegna,ora_consegna,nome_comanda,cast((((cast(prezzo_vero as float)+cast(QTA as float))*quantita)/100*(100-cast(sconto_perc as float))) as float) as totale_prezzo,quantita,rag_soc_cliente\n\
                        from comanda\n\
                        where data_servizio='" + comanda.data_servizio + "'  and (stato_record='FORNO' or stato_record='CHIUSO') \n\
                        and tipo_consegna='DOMICILIO'  order by ora_consegna asc,id_pony asc,id asc;";
    var r1 = alasql(query_forno);
    r1.forEach(function (e) {

        var query_forno_2 = "select telefono,cellulare,citta,comune,indirizzo,numero,provincia,cap\n\
                        from clienti \n\
                        where id=" + e.rag_soc_cliente + " limit 1;";
        var r2 = alasql(query_forno_2);
        e.totale_prezzo = e.totale_prezzo.toFixed(8);
        risultato.push({
            id: e.id,
            id_pony: e.id_pony,
            desc_art: e.desc_art,
            portata: e.portata,
            tipo_consegna: e.tipo_consegna,
            ora_consegna: e.ora_consegna,
            nome_comanda: e.nome_comanda,
            totale_prezzo: e.totale_prezzo,
            quantita: e.quantita,
            telefono: r2[0].telefono,
            cellulare: r2[0].cellulare,
            citta: r2[0].citta,
            comune: r2[0].comune,
            indirizzo: r2[0].indirizzo,
            numero: r2[0].numero,
            provincia: r2[0].provincia,
            cap: r2[0].cap
        });
    });
    var oggetto_consegne = new Object();
    var totale_prezzo = 0;
    var totale_pizze = 0;
    var totale_bibite = 0;

    let html_page = "";
    html_page += "-----------------------------------------<br>";
    html_page += "ELENCO CONSEGNE<br>";
    html_page += new Date().format("dd/mm/yyyy") + "<br>";
    html_page += "-----------------------------------------<br>";
    risultato.forEach(function (obj) {



        if (oggetto_consegne[obj.ora_consegna] === undefined) {
            oggetto_consegne[obj.ora_consegna] = new Object();
        }

        if (oggetto_consegne[obj.ora_consegna][obj.id_pony] === undefined) {
            oggetto_consegne[obj.ora_consegna][obj.id_pony] = new Object();
        }


        if (oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id] === undefined) {
            oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id] = new Object();
            oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].qta_pizze = 0;
            oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].qta_bibite = 0;
            oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].prezzo = 0;
            oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].nome_comanda = obj.nome_comanda;
        }




        if (obj.desc_art[0] !== "+" && obj.desc_art[0] !== "-" && obj.desc_art[0] !== "=" && obj.desc_art[0] !== "*" && obj.desc_art[0] !== "x" && obj.desc_art[0] !== "(") {

            if (obj.portata.indexOf("P") !== -1) {
                totale_pizze += parseInt(obj.quantita);
                oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].qta_pizze += parseInt(obj.quantita);
            } else if (obj.portata === "B") {
                totale_bibite += parseInt(obj.quantita);
                oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].qta_bibite += parseInt(obj.quantita);
            }


            oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].prezzo += parseFloat(obj.totale_prezzo);
        } else {
            oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].prezzo += parseFloat(obj.totale_prezzo);
        }


        totale_prezzo += parseFloat(obj.totale_prezzo);
    });
    for (var ora_consegna in oggetto_consegne) {

        html_page += "ORE: " + ora_consegna + "<br>";

        for (var id_pony in oggetto_consegne[ora_consegna]) {

            var nome_pony = "";
            if (oggetto_pony[id_pony] !== undefined) {
                nome_pony = oggetto_pony[id_pony];
            }

            console.log("STAMPA_CONSEGNE - NOME PONY ", nome_pony);
            for (var id in oggetto_consegne[ora_consegna][id_pony]) {

                //console.log("STAMPA_CONSEGNE - DESTINATARIO ", oggetto_consegne[ora_consegna][id_pony][id].nome_comanda);

                //console.log("STAMPA_CONSEGNE - NUM. PIZZE ", oggetto_consegne[ora_consegna][id_pony][id].qta);

                //console.log("STAMPA_CONSEGNE - PREZZO: € ", arrotonda_prezzo(oggetto_consegne[ora_consegna][id_pony][id].prezzo));


                var nome_persona = "";
                if (nome_pony.length > 0) {
                    var nome_persona_diviso = oggetto_consegne[ora_consegna][id_pony][id].nome_comanda.trim().split(/\W+/gi);
                    if (nome_persona_diviso[1] !== undefined) {

                        nome_persona = nome_persona_diviso[0].trim().substr(0, 1) + ".";
                        nome_persona += " " + nome_persona_diviso[1].trim();
                    } else {
                        nome_persona = nome_persona_diviso[0].trim();
                    }
                } else {
                    nome_persona = oggetto_consegne[ora_consegna][id_pony][id].nome_comanda.trim();
                }

                html_page += nome_persona;

                if (comanda.numero_licenza_cliente === "0629") {
                    let testo_query = "select * from elenco_progressivi_asporto_domicilio where progressivo_comanda='" + id + "' limit 1;";
                    let progressivo_attuale = alasql(testo_query);
                    if (progressivo_attuale.length > 0) {

                        html_page += " (" + progressivo_attuale[0].numero + ")";
                    }

                }
                html_page += ": ";

                if (oggetto_consegne[ora_consegna][id_pony][id].qta_pizze === "1" || oggetto_consegne[ora_consegna][id_pony][id].qta_pizze === 1) {
                    html_page += oggetto_consegne[ora_consegna][id_pony][id].qta_pizze + ' pizza';
                } else {
                    html_page += oggetto_consegne[ora_consegna][id_pony][id].qta_pizze + ' pizze';
                }

                if (oggetto_consegne[ora_consegna][id_pony][id].qta_bibite === "1" || oggetto_consegne[ora_consegna][id_pony][id].qta_bibite === 1) {
                    html_page += " " + oggetto_consegne[ora_consegna][id_pony][id].qta_bibite + " bibita";
                } else if (oggetto_consegne[ora_consegna][id_pony][id].qta_bibite > 0) {
                    html_page += " " + oggetto_consegne[ora_consegna][id_pony][id].qta_bibite + " bibite";
                }

                html_page += " € " + arrotonda_prezzo(oggetto_consegne[ora_consegna][id_pony][id].prezzo);
                if (nome_pony.length > 0) {
                    html_page += " - " + nome_pony;
                }

                html_page += "<br>";
            }

        }

        html_page += "-----------------------------------------<br>";
    }

    html_page += "TOTALE: € " + arrotonda_prezzo(parseFloat(totale_prezzo)) + "<br>";
    html_page += "-----------------------------------------<br>";
    html_page += "Pizze Consegnate: " + totale_pizze + "<br>";
    html_page += "Bibite Consegnate: " + totale_bibite + "<br>";


    $("#popup_generico .contenuto").html(html_page);
    $("#popup_generico").modal("show");
}

function stampa_consegne_totali_pony() {



    bootbox.alert("STAMPA IN CORSO...");
    var d = new Date();
    var Y = d.getFullYear();
    var M = addZero((d.getMonth() + 1), 2);
    var D = addZero(d.getDate(), 2);
    var data_oggi = Y + '' + M + '' + D;
    var risultato = new Array();
    //group by comanda.id,comanda.nome_comanda,comanda.ora_consegna


    var query_forno = " select id,id_pony,desc_art,portata,tipo_consegna,ora_consegna,nome_comanda,cast((((cast(prezzo_vero as float)+cast(QTA as float))*quantita)/100*(100-cast(sconto_perc as float))) as float) as totale_prezzo,quantita,rag_soc_cliente\n\
                        from comanda\n\
                        where data_servizio='" + comanda.data_servizio + "'  and (stato_record='FORNO' or stato_record='CHIUSO') \n\
                        and tipo_consegna='DOMICILIO'  order by ora_consegna asc,id_pony asc,id asc;";
    var r1 = alasql(query_forno);
    r1.forEach(function (e) {

        var query_forno_2 = "select telefono,cellulare,citta,comune,indirizzo,numero,provincia,cap\n\
                        from clienti \n\
                        where id=" + e.rag_soc_cliente + " limit 1;";
        var r2 = alasql(query_forno_2);
        e.totale_prezzo = e.totale_prezzo.toFixed(8);
        risultato.push({
            id: e.id,
            id_pony: e.id_pony,
            desc_art: e.desc_art,
            portata: e.portata,
            tipo_consegna: e.tipo_consegna,
            ora_consegna: e.ora_consegna,
            nome_comanda: e.nome_comanda,
            totale_prezzo: e.totale_prezzo,
            quantita: e.quantita,
            telefono: r2[0].telefono,
            cellulare: r2[0].cellulare,
            citta: r2[0].citta,
            comune: r2[0].comune,
            indirizzo: r2[0].indirizzo,
            numero: r2[0].numero,
            provincia: r2[0].provincia,
            cap: r2[0].cap
        });
    });
    var oggetto_consegne = new Object();
    var totale_prezzo = 0;
    var totale_pizze = 0;
    var totale_bibite = 0;
    var builder = new epson.ePOSBuilder();
    builder.addTextFont(builder.FONT_A);
    builder.addTextAlign(builder.ALIGN_CENTER);
    builder.addTextSize(1, 1);
    builder.addText("-----------------------------------------\n");
    builder.addTextSize(2, 2);
    builder.addText('ELENCO CONSEGNE\n');
    builder.addTextSize(1, 1);
    builder.addFeedLine(1);
    builder.addText(new Date().format("dd/mm/yyyy") + '\n');
    builder.addText("-----------------------------------------\n");
    risultato.forEach(function (obj) {



        if (oggetto_consegne[obj.ora_consegna] === undefined) {
            oggetto_consegne[obj.ora_consegna] = new Object();
        }

        if (oggetto_consegne[obj.ora_consegna][obj.id_pony] === undefined) {
            oggetto_consegne[obj.ora_consegna][obj.id_pony] = new Object();
        }


        if (oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id] === undefined) {
            oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id] = new Object();
            oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].qta_pizze = 0;
            oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].qta_bibite = 0;
            oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].prezzo = 0;
            oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].nome_comanda = obj.nome_comanda;
        }




        if (obj.desc_art[0] !== "+" && obj.desc_art[0] !== "-" && obj.desc_art[0] !== "=" && obj.desc_art[0] !== "*" && obj.desc_art[0] !== "x" && obj.desc_art[0] !== "(") {

            if (obj.portata.indexOf("P") !== -1) {
                totale_pizze += parseInt(obj.quantita);
                oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].qta_pizze += parseInt(obj.quantita);
            } else if (obj.portata === "B") {
                totale_bibite += parseInt(obj.quantita);
                oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].qta_bibite += parseInt(obj.quantita);
            }


            oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].prezzo += parseFloat(obj.totale_prezzo);
        } else {
            oggetto_consegne[obj.ora_consegna][obj.id_pony][obj.id].prezzo += parseFloat(obj.totale_prezzo);
        }


        totale_prezzo += parseFloat(obj.totale_prezzo);
    });
    for (var ora_consegna in oggetto_consegne) {

        builder.addTextAlign(builder.ALIGN_CENTER);
        builder.addTextStyle(false, false, true, builder.COLOR_1);
        //console.log("STAMPA_CONSEGNE - ORA CONSEGNA ", ora_consegna);

        builder.addText('ORE: ' + ora_consegna + '\n');
        builder.addFeedLine(1);
        for (var id_pony in oggetto_consegne[ora_consegna]) {

            var nome_pony = "";
            if (oggetto_pony[id_pony] !== undefined) {
                nome_pony = oggetto_pony[id_pony];
            }

            console.log("STAMPA_CONSEGNE - NOME PONY ", nome_pony);
            for (var id in oggetto_consegne[ora_consegna][id_pony]) {

                //console.log("STAMPA_CONSEGNE - DESTINATARIO ", oggetto_consegne[ora_consegna][id_pony][id].nome_comanda);

                //console.log("STAMPA_CONSEGNE - NUM. PIZZE ", oggetto_consegne[ora_consegna][id_pony][id].qta);

                //console.log("STAMPA_CONSEGNE - PREZZO: € ", arrotonda_prezzo(oggetto_consegne[ora_consegna][id_pony][id].prezzo));

                builder.addTextAlign(builder.ALIGN_LEFT);
                builder.addTextPosition(0);
                builder.addTextStyle(false, false, true, builder.COLOR_1);
                var nome_persona = "";
                if (nome_pony.length > 0) {
                    var nome_persona_diviso = oggetto_consegne[ora_consegna][id_pony][id].nome_comanda.trim().split(/\W+/gi);
                    if (nome_persona_diviso[1] !== undefined) {

                        nome_persona = nome_persona_diviso[0].trim().substr(0, 1) + ".";
                        nome_persona += " " + nome_persona_diviso[1].trim();
                    } else {
                        nome_persona = nome_persona_diviso[0].trim();
                    }
                } else {
                    nome_persona = oggetto_consegne[ora_consegna][id_pony][id].nome_comanda.trim();
                }

                builder.addText(nome_persona);
                if (comanda.numero_licenza_cliente === "0629") {
                    let testo_query = "select * from elenco_progressivi_asporto_domicilio where progressivo_comanda='" + id + "' limit 1;";
                    let progressivo_attuale = alasql(testo_query);
                    if (progressivo_attuale.length > 0) {

                        builder.addText(" (" + progressivo_attuale[0].numero + ")");
                    }

                }
                builder.addText(': ');
                builder.addTextStyle(false, false, false, builder.COLOR_1);
                if (oggetto_consegne[ora_consegna][id_pony][id].qta_pizze === "1" || oggetto_consegne[ora_consegna][id_pony][id].qta_pizze === 1) {
                    builder.addText(oggetto_consegne[ora_consegna][id_pony][id].qta_pizze + ' pizza');
                } else {
                    builder.addText(oggetto_consegne[ora_consegna][id_pony][id].qta_pizze + ' pizze');
                }

                if (oggetto_consegne[ora_consegna][id_pony][id].qta_bibite === "1" || oggetto_consegne[ora_consegna][id_pony][id].qta_bibite === 1) {
                    builder.addText(' ' + oggetto_consegne[ora_consegna][id_pony][id].qta_bibite + ' bibita');
                } else if (oggetto_consegne[ora_consegna][id_pony][id].qta_bibite > 0) {
                    builder.addText(' ' + oggetto_consegne[ora_consegna][id_pony][id].qta_bibite + ' bibite');
                }

                builder.addText(' € ' + arrotonda_prezzo(oggetto_consegne[ora_consegna][id_pony][id].prezzo));
                if (nome_pony.length > 0) {
                    builder.addText(' - ' + nome_pony);
                }

                builder.addText('\n');
            }

        }

        builder.addTextAlign(builder.ALIGN_CENTER);
        builder.addText("-----------------------------------------\n");
    }

    builder.addTextAlign(builder.ALIGN_CENTER);
    builder.addTextStyle(false, false, false, builder.COLOR_1);
    builder.addTextSize(2, 2);
    //console.log("STAMPA_CONSEGNE", "TOTALE: € " + arrotonda_prezzo(parseFloat(totale_prezzo)));


    builder.addText("TOTALE: € " + arrotonda_prezzo(parseFloat(totale_prezzo)) + "\n");
    builder.addTextStyle(false, false, false, builder.COLOR_1);
    builder.addTextSize(1, 1);
    builder.addText("-----------------------------------------\n");
    //console.log("STAMPA_CONSEGNE", "Pizze Consegnate: " + totale_pizze);

    builder.addText("Pizze Consegnate: " + totale_pizze + "\n");
    builder.addText("Bibite Consegnate: " + totale_bibite + "\n");
    builder.addCut(builder.CUT_FEED);
    var request = builder.toString();
    var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
    var url = "";
    if (comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].intelligent === "SDS") {
        url = 'http://' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "") + numero_layout].ip + ':8000/SPOOLER?stampante=' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "") + numero_layout].devid_nf;
    } else {
        url = 'http://' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
    }
    var xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    //<Header settings>
    xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
    xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
    xhr.setRequestHeader('SOAPAction', '""');
    // Send print document
    xhr.send(soap);
    xhr.onreadystatechange = function () {
        bootbox.hideAll();
    };
}

function fa_richiesta_progressivo_domicilio(id_comanda_definito) {

    if (id_comanda_definito === undefined) {
        id_comanda_definito = id_comanda_attuale_alasql();
    }

    let testo_query = "select * from elenco_progressivi_asporto_domicilio where progressivo_comanda='" + id_comanda_definito + "' limit 1;";
    let progressivo_attuale = alasql(testo_query);
    if (progressivo_attuale.length > 0) {

        return progressivo_attuale[0].numero;
    } else {


        testo_query = "select numero from progressivo_asporto_domicilio;";
        let n = alasql(testo_query);
        //MEGLIO UN PROGRESSIVO IN PIU' CHE UN PROGRESSIVO DOPPIO

        testo_query = "update progressivo_asporto_domicilio set numero=cast(numero as int)+1;";
        comanda.sock.send({
            tipo: "aggiornamento_progressivo_asporto_domicilio",
            operatore: comanda.operatore,
            query: testo_query,
            terminale: comanda.terminale,
            ip: comanda.ip_address
        });
        alasql(testo_query);
        let numero_nuovo_progressivo = n[0].numero;
        testo_query = "insert into elenco_progressivi_asporto_domicilio (progressivo_comanda,numero) VALUES ( '" + id_comanda_definito + "','" + numero_nuovo_progressivo + "');";
        comanda.sock.send({
            tipo: "aggiornamento_progressivo_asporto_domicilio",
            operatore: comanda.operatore,
            query: testo_query,
            terminale: comanda.terminale,
            ip: comanda.ip_address
        });
        alasql(testo_query);
        return numero_nuovo_progressivo;
    }
}

function reset_progressivo_asporto() {
    var prog_result = alasql('SELECT numero FROM progressivo_asporto limit 1;');
    comanda.blocca_progressivo = false;

    /* aggiunto il 11/05/2021 per evitare errore di sincronia trovato da Linda e Giancarlo, che quando aprivi l'ultimo tavolo sull'altro pc, 
     * l'id di comanda del primo diventava come l'ultima comanda battuta causando errori di battitura e lettura a video
     */
    if (comanda.pizzeria_asporto !== true && comanda.tavolo.left(7) === "ASPORTO") {
        comanda.progr_gg_uni = 'ASPORTO' + comanda.centro + aggZero(comanda.terminale, 2);
        comanda.tavolo = comanda.progr_gg_uni;
    }

    comanda.ultimo_id_cliente = undefined;
    comanda.ultimo_id_asporto = undefined;
    comanda.dati_temporanei_cliente = undefined;



    non_fare_subito();
    comanda.tipo_consegna = "NIENTE";
    $('.tipo_consegna').removeClass('tipo_consegna_selezionata');
    $('#popup_scelta_cliente input[name="numeretto"]').val(prog_result[0].numero);
}

function ripristina_riga(id_scontrino) {
    testo_queryy = "select stato_record from comanda where id = '" + id_scontrino + "' ";
    let check = alasql(testo_queryy);
    check.forEach(function (obj) {
        if (obj.stato_record === 'CHIUSO') {


            testo_query = "update comanda set stato_record = 'FORNO' where id = '" + id_scontrino + "' AND stato_record = 'CHIUSO' ";

            comanda.sock.send({
                tipo: "aggiornamento_progressivo_asporto_domicilio",
                operatore: comanda.operatore,
                query: testo_query,
                terminale: comanda.terminale,
                ip: comanda.ip_address
            });
            alasql(testo_query);
        }
    });



    righe_forno();
}


function ristampa_comanda_fiscale(id_scontrino) {
    let controlla = controlla_stato_record(id_scontrino);
    if (controlla.toString() === "true") {


        testo_query = "update comanda set stato_record = 'FORNO' where id = '" + id_scontrino + "'AND stato_record = 'CHIUSO' ";

        alasql(testo_query);


        dct.funzione_interna(id_scontrino);
        testo_queryy = "update comanda set stato_record = 'CHIUSO' where id = '" + id_scontrino + "'AND stato_record = 'FORNO'  ";

        alasql(testo_queryy);
        righe_forno();
    } else {
        dct.funzione_interna(id_scontrino);
        righe_forno();
    }

}
function controlla_stato_record(id_scontrino) {
    testo_queryy = "select stato_record from comanda where id = '" + id_scontrino + "' ";
    let check = alasql(testo_queryy);
    check.forEach(function (obj) {
        if (obj.stato_record === 'CHIUSO') {
            ritorno = true;
        }
        else {
            ritorno = false;
        }

    });
    return ritorno;
}