var ClasseXML = function (nome_file, tag_da_importare, nome_tabella_db, array_corrispondenze_colonne_valori, array_campi_aggiuntivi_fissi, bool_elimina_tabella_prima, bool_crea_nuova_tabella, bool_colonne_db_minuscole, bool_record_maiuscoli) {

    // VEDE SE UN ELEMENTO ESISTE NELL'ARRAY
    Array.prototype.inArray = function (comparer) {
        for (var i = 0; i < this.length; i++) {
            if (comparer(this[i]))
                return true;
        }
        return false;
    };

    // AGGIUNGE UN ELEMENTO ALL'ARRAY, SE QUESTO NON ESISTE
    // UTILIZZANDO IL METODO INARRAY
    Array.prototype.pushIfNotExist = function (element, comparer) {
        if (!this.inArray(comparer)) {
            this.push(element);
        }
    };

    //AGGIUNGE GLI SLASH AI CARATTERI SPECIALI, PER EVITARE ERRORI NELLE QUERY
    String.prototype.addslashes = function () {
        return this;
        (this + '');
        //AL MOMENTO NON OCCORRONO GLI SLASH
        //.replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');

    };
    //-------------------- FINE DEI PROTOTIPI ----------------------


    //LEGGE IL FILE XML E LO ELABORA CON LA FUNZIONE LISTA_ELEMENTI_IMPORTABILI
    this.importa = function (callBack) {
        console.log("call funzione importazione");

        //console.log(nome_file);

        ClasseXML.xhttp = new XMLHttpRequest();

        ClasseXML.xhttp.timeout = 5000;

        ClasseXML.xhttp.onreadystatechange = function () {
            if (ClasseXML.xhttp.readyState === 4 && ClasseXML.xhttp.status === 200) {

                lista_elementi_importabili(function () {
                    salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # FINE IMPORTA FILE # NOME: " + nome_file + " # ");
                    callBack(true);
                    console.log("callback vero importazione");
                });
            } else if (ClasseXML.xhttp.readyState === 4 && ClasseXML.xhttp.status === 404)
            {
                //ERRORE: FA 4 CALLBACK FALSI
                salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # ERRORE IMPORTA FILE 1 (PROBABILE TAVOLO VUOTO) # NOME: " + nome_file + " # ");
                callBack(false);
                console.log("callback falso importazione");
            }
        };

        ClasseXML.xhttp.onerror = function (event) {
            salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # ERRORE IMPORTA FILE 2# NOME: " + nome_file + " # ");
            alert("ATTENZIONE! Sei fuori portata WiFi. Avvicinati all'antenna e riprova (IMPORTAZIONE TAVOLO)");
            event.preventDefault();

            try {
                $(document).off(comanda.eventino, evento_selettore_tavolo);
            } catch (e) {
                console.log(e);
            }
            $(document).on(comanda.eventino, 'div.tavolo.rett,div.tavolo.cerc', evento_selettore_tavolo);
            $('.tasto_bestellung').css('pointer-events', '');
            $('.tasto_bargeld').css('pointer-events', '');
            $('.tasto_quittung').css('pointer-events', '');
            $('.tasto_rechnung').css('pointer-events', '');
            //callBack(false);
            console.error(xhttp.statusText);
        };

        salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # IMPORTA FILE # NOME: " + nome_file + " # ");


        ClasseXML.xhttp.open("GET", nome_file + "?_=" + new Date().getTime(), true);
        ClasseXML.xhttp.send();
    };

    function getPathFromUrl(url) {
        return url.split("?")[0];
    }

    function lista_elementi_importabili(callBack) {

        var OggettoXml = ClasseXML.xhttp.responseXML;

        //Serve a vedere se l'XML è identico al precedente o diverso
        //altrimenti non ha senso rifare l'importazione e perdere tempo per niente
        var hash_nuovo = CryptoJS.MD5(ClasseXML.xhttp.response).toString();
        var nome_file = getPathFromUrl(ClasseXML.xhttp.responseURL);

        var lista_sottotag = OggettoXml.getElementsByTagName(tag_da_importare);
        var elenco_query = new Array();
        var tutte_le_colonne = new Array();

        for (var i = 0; i < lista_sottotag.length; i++) {

            var colonne_separate = '';
            var righe_separate = '';

            if (nome_tabella_db === "comanda") {
                var a = new Array();
                a.id = "";
                a.ora_consegna = "";
                a.tipo_consegna = "";
                a.numeretto_asp = "";
                a.QTA = "";
                a.VAR = "";
                a.QTAP = "";
                a.CAT = "";
                a.PRZ = "";
                a.LIB2 = "";
                a.PRG = "";
                a.LIB5 = "";
                a.NSEGN = "";
                a.AUTORIZ = "";
                a.LIB1 = "";
                a.LIB3 = "";
                a.LIB4 = "";
                a.NCARD5 = "";
                a.nump_xml = "";
                a.jolly = "";
                a.tipo_ricevuta = "";
                a.progressivo_fiscale = "";
                a.ordinamento = "";
                a.ultima_portata = "";
                a.cod_articolo = "";
                a.numero_conto = "";
                a.contiene_variante = "";
                a.dest_stampa = "";
                a.cat_variante = "";
                a.tipo_record = "";
                a.stato_record = "";
                a.planning_colori = "";
                a.data_fisc = "";
                a.ora_fisc = "";
                a.ntav_comanda = "";
                a.ntav_attrib = "";
                a.tab_bis = "";
                a.prog_inser = "";
                a.art_primario = "";
                a.art_variante = "";
                a.tipo_variante = "";
                a.desc_art = "";
                a.quantita = "";
                a.prezzo_un = "";
                a.sconto_perc = "";
                a.sconto_imp = "";
                a.netto = "";
                a.imp_tot_netto = "";
                a.perc_iva = "";
                a.costo = "";
                a.autor_sconto = "";
                a.categoria = "";
                a.gruppo = "";
                a.incassato = "";
                a.residuo = "";
                a.tipo_incasso = "";
                a.contanti = "";
                a.carta_credito = "";
                a.bancomat = "";
                a.assegni = "";
                a.tessera_prepagata = "";
                a.numero_tessera_prepag = "";
                a.stampata_sn = "";
                a.data_stampa = "";
                a.ora_stampa = "";
                a.fiscalizzata_sn = "";
                a.data_ = "";
                a.ora_ = "";
                a.n_fiscale = "";
                a.n_conto_parziale = "";
                a.nodo = "";
                a.portata = "";
                a.ult_portata = "";
                a.centro = "";
                a.terminale = "";
                a.operatore = "";
                a.cod_cliente = "";
                a.rag_soc_cliente = "";
                a.coef_trasf = "";
                a.peso_x_um = "";
                a.peso_ums = "";
                a.peso_tot = "";
                a.qnt_prod = "";
                a.nome_comanda = "";
                a.posizione = "";
                a.fiscale_sospeso = "";
                a.BIS = "";
                a.nome_pc = "";
                a.giorno = "";
                a.cod_promo = "";
                a.qta_fissa = "";
                a.spazio_forno = "";
                a.tipo_impasto = "";
                a.tasto_segue = "";
                a.NCARD2 = "";
                a.NCARD3 = "";
                a.NCARD4 = "";
                a.NEXIT = "";
                a.droppay = "";
                a.dest_stampa_2 = "";
                a.id_pony = "";
                a.prezzo_varianti_aggiuntive = "";
                a.prezzo_varianti_maxi = "";
                a.prezzo_maxi_prima = "";
            } else if (nome_tabella_db === "prodotti" && comanda.numero_licenza_cliente === "0351") {

                var a = new colonna_valore();

                if ($(lista_sottotag[i]).find("CAT").html() !== undefined && $(lista_sottotag[i]).find("CAT").html().substr(0, 1) !== "V" && $(lista_sottotag[i]).find("ORD").html() === "") {
                    continue;
                }

            } else
            {
                var a = new colonna_valore();
            }

            for (var c = 0; c < lista_sottotag[i].children.length; c++) {

                var colonna = '';


                //SOLO SE CE UN VALORE INSERISCE LA COLONNA E LA RIGA NELL'INSERT
                //if (lista_sottotag[i].children[c].innerHTML.addslashes().length > 0)
                //{
                //SOSTITUISCE NOMI COLONNE E VALORI, SE L'ARRAY_CORRISPONDENZE LO SPECIFICA
                if (array_corrispondenze_colonne_valori !== undefined && array_corrispondenze_colonne_valori !== null) {

                    if (array_corrispondenze_colonne_valori[lista_sottotag[i].children[c].nodeName] !== undefined) {


                        if (typeof (array_corrispondenze_colonne_valori[lista_sottotag[i].children[c].nodeName].colonna_db) === "object")
                        {
                            //---
                            array_corrispondenze_colonne_valori[lista_sottotag[i].children[c].nodeName].colonna_db.forEach(function (colonna_foreach) {

                                colonna = colonna_foreach;

                                //SE E' RICHIESTA UNA SOSTITUZIONE NELL'INSERIMENTO NEL DATABASE, IN CASO DI VALORI SPECIFICI
                                if (array_corrispondenze_colonne_valori[lista_sottotag[i].children[c].nodeName].valori !== undefined && array_corrispondenze_colonne_valori[lista_sottotag[i].children[c].nodeName].valori[lista_sottotag[i].children[c].innerHTML] !== undefined)
                                {
                                    if (nome_tabella_db === "comanda") {
                                        a[colonna_foreach] = array_corrispondenze_colonne_valori[lista_sottotag[i].children[c].nodeName].valori[lista_sottotag[i].children[c].innerHTML].addslashes().replace(/'/gi, ' ').replace(/"/gi, ' ');
                                    } else {
                                        a.constructor(colonna_foreach, array_corrispondenze_colonne_valori[lista_sottotag[i].children[c].nodeName].valori[lista_sottotag[i].children[c].innerHTML].addslashes().replace(/'/gi, ' ').replace(/"/gi, ' '));
                                    }
                                } else
                                {

                                    if (nome_tabella_db === "comanda") {
                                        a[colonna_foreach] = lista_sottotag[i].children[c].innerHTML.addslashes().replace(/'/gi, ' ').replace(/"/gi, ' ');
                                    } else {
                                        a.constructor(colonna_foreach, lista_sottotag[i].children[c].innerHTML.addslashes().replace(/'/gi, ' ').replace(/"/gi, ' '));
                                    }
                                }


                                if (bool_crea_nuova_tabella === true && colonna !== undefined && colonna.length > 0) {

                                    //DEVE PRENDERE ENTRAMBE LE COLONNE DEL FOREACH
                                    tutte_le_colonne.pushIfNotExist(colonna, function (e) {
                                        return e === colonna;
                                    });
                                }

                            });
                            //END FOREACH
                        } else if (array_corrispondenze_colonne_valori[lista_sottotag[i].children[c].nodeName].colonna_db !== "DEL") {

                            if (a !== undefined && a.descrizione !== undefined && a.descrizione.indexOf("ADULTI") !== -1) {
                                console.log("STOP");
                            }

                            colonna = array_corrispondenze_colonne_valori[lista_sottotag[i].children[c].nodeName].colonna_db;


                            //SE E' RICHIESTA UNA SOSTITUZIONE NELL'INSERIMENTO NEL DATABASE, IN CASO DI VALORI SPECIFICI
                            if (array_corrispondenze_colonne_valori[lista_sottotag[i].children[c].nodeName].valori !== undefined && array_corrispondenze_colonne_valori[lista_sottotag[i].children[c].nodeName].valori[lista_sottotag[i].children[c].innerHTML] !== undefined)
                            {
                                if (nome_tabella_db === "comanda") {
                                    a[colonna] = array_corrispondenze_colonne_valori[lista_sottotag[i].children[c].nodeName].valori[lista_sottotag[i].children[c].innerHTML].addslashes().replace(/'/gi, ' ').replace(/"/gi, ' ');
                                } else {
                                    a.constructor(colonna, array_corrispondenze_colonne_valori[lista_sottotag[i].children[c].nodeName].valori[lista_sottotag[i].children[c].innerHTML].addslashes().replace(/'/gi, ' ').replace(/"/gi, ' '));
                                }

                            } else
                            {

                                if (nome_tabella_db === "comanda") {
                                    a[colonna] = lista_sottotag[i].children[c].innerHTML.addslashes().replace(/'/gi, ' ').replace(/"/gi, ' ');
                                } else {
                                    a.constructor(colonna, lista_sottotag[i].children[c].innerHTML.addslashes().replace(/'/gi, ' ').replace(/"/gi, ' '));
                                }

                            }


                        }
                    }

                    //---

                    else
                    {

                        colonna = lista_sottotag[i].children[c].nodeName;

                        if (nome_tabella_db === "comanda") {
                            a[colonna] = lista_sottotag[i].children[c].innerHTML.addslashes().replace(/'/gi, ' ').replace(/"/gi, ' ');
                        } else {
                            a.constructor(colonna, lista_sottotag[i].children[c].innerHTML.addslashes().replace(/'/gi, ' ').replace(/"/gi, ' '));
                        }

                    }

                } else
                {
                    colonna = lista_sottotag[i].children[c].nodeName;

                    if (nome_tabella_db === "comanda") {
                        a[colonna] = lista_sottotag[i].children[c].innerHTML.addslashes().replace(/'/gi, ' ').replace(/"/gi, ' ');
                    } else {
                        a.constructor(colonna, lista_sottotag[i].children[c].innerHTML.addslashes().replace(/'/gi, ' ').replace(/"/gi, ' '));
                    }

                }
                //}



                if (bool_elimina_tabella_prima === true) {
                    var query_elimina_tabella_precedente = "DROP TABLE IF EXISTS " + nome_tabella_db + ";";

                }

                if (bool_crea_nuova_tabella === true && colonna !== undefined && colonna.length > 0) {

                    //TORNA TRUE SE IL VALORE E' STATO AGGIUNTO
                    tutte_le_colonne.pushIfNotExist(colonna, function (e) {
                        return e === colonna;
                    });
                }

                //console.log("TUTTE COLONNE", tutte_le_colonne);
            }

            //PER AGGIUNGERE ALCUNI VALORI DEI CAMPI FISSI, SE NECESSARIO
            if (array_campi_aggiuntivi_fissi !== undefined && array_campi_aggiuntivi_fissi !== null) {
                array_campi_aggiuntivi_fissi.forEach(function (oggetto_campo_fisso) {

                    if (nome_tabella_db === "comanda") {
                        a[oggetto_campo_fisso.colonna] = oggetto_campo_fisso.valore;
                    } else {
                        a.constructor(oggetto_campo_fisso.colonna, oggetto_campo_fisso.valore);
                    }
                });
            }

            /*if (bool_colonne_db_minuscole === true)
             {
             colonne_separate = colonne_separate.toLowerCase();
             }
             
             if (bool_record_maiuscoli === true) {
             righe_separate = righe_separate.toUpperCase();
             }*/

            //ALLA FINE INSERISCE LA QUERY NEL VETTORE PER POI ESSERE RIELABORATA DAL SINCROQUERY
            //elenco_query.push("INSERT INTO " + nome_tabella_db + " (" + colonne_separate.slice(0, -1) + ") VALUES ( " + righe_separate.slice(0, -1) + " );");

            elenco_query.push(a);
        }


        //SERVE PER LA CREAZIONE DELLA TABELLA. FACENDO COSI SI RISPARMIA MEMORIA
        if (array_campi_aggiuntivi_fissi !== undefined && array_campi_aggiuntivi_fissi !== null) {

            array_campi_aggiuntivi_fissi.forEach(function (oggetto_campo_fisso) {

                tutte_le_colonne.pushIfNotExist(oggetto_campo_fisso.colonna, function (e) {
                    return e === oggetto_campo_fisso.colonna;
                });

            });
        }

        //-- CONDIZIONI PRINCIPALI DELLA FUNZIONE --//
        if (bool_elimina_tabella_prima === true) {
            comanda.sincro.query(query_elimina_tabella_precedente, function () {
            });

        }

        if (bool_crea_nuova_tabella === true) {

            //console.log("TUTTE LE COLONNE: ", tutte_le_colonne);

            var colonne_tabella = '';

            if (bool_colonne_db_minuscole === true)
            {
                colonne_tabella = tutte_le_colonne.join(",").toLowerCase();
            } else
            {
                colonne_tabella = tutte_le_colonne.join(",");
            }

            var query_nuova_tabella = "CREATE TABLE " + nome_tabella_db + "( " + colonne_tabella + ");";

            comanda.sincro.query(query_nuova_tabella, function () {
            });

        }

        /*if (nome_tabella_db === 'prodotti' || nome_tabella_db === 'categorie') {*/

        //salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # QUERY IMPORTATE # ELENCO: " + elenco_query.join("*") + " # ");

        //IMPORTA TUTTO NELLA TAB


        //elenco_query.forEach(function (query_vera) {

        alasql.tables[nome_tabella_db].data = elenco_query;

        //});

        //PER EVITARE IL BUG DEL NAN NEL PREZZO
        //alasql("UPDATE comanda SET prezzo_un='0,00' WHERE trim(prezzo_un)='';");


        callBack(true);

    }



    //SUDDIVIDE RIGHE E COLONNE DELL' XML PER L'IMPORTAZIONE
    /*function lista_elementi_importabili(callBack) {
     
     var OggettoXml = ClasseXML.xhttp.responseXML;
     
     //Serve a vedere se l'XML è identico al precedente o diverso
     //altrimenti non ha senso rifare l'importazione e perdere tempo per niente
     var hash_nuovo = CryptoJS.MD5(ClasseXML.xhttp.response).toString();
     var nome_file = getPathFromUrl(ClasseXML.xhttp.responseURL);
     
     var lista_sottotag = OggettoXml.getElementsByTagName(tag_da_importare);
     var elenco_query = new Array();
     var tutte_le_colonne = new Array();
     
     for (var i = 0; i < lista_sottotag.length; i++) {
     
     var colonne_separate = '';
     var righe_separate = '';
     
     for (var c = 0; c < lista_sottotag[i].children.length; c++) {
     
     var colonna = '';
     
     
     //SOLO SE CE UN VALORE INSERISCE LA COLONNA E LA RIGA NELL'INSERT
     //if (lista_sottotag[i].children[c].innerHTML.addslashes().length > 0)
     //{
     //SOSTITUISCE NOMI COLONNE E VALORI, SE L'ARRAY_CORRISPONDENZE LO SPECIFICA
     if (array_corrispondenze_colonne_valori !== undefined && array_corrispondenze_colonne_valori !== null) {
     
     if (array_corrispondenze_colonne_valori[lista_sottotag[i].children[c].nodeName] !== undefined) {
     
     
     if (typeof (array_corrispondenze_colonne_valori[lista_sottotag[i].children[c].nodeName].colonna_db) === "object")
     {
     //---
     array_corrispondenze_colonne_valori[lista_sottotag[i].children[c].nodeName].colonna_db.forEach(function (colonna_foreach) {
     
     //SE E' RICHIESTA UNA SOSTITUZIONE NELL'INSERIMENTO NEL DATABASE, IN CASO DI VALORI SPECIFICI
     if (array_corrispondenze_colonne_valori[lista_sottotag[i].children[c].nodeName].valori !== undefined && array_corrispondenze_colonne_valori[lista_sottotag[i].children[c].nodeName].valori[lista_sottotag[i].children[c].innerHTML] !== undefined)
     {
     righe_separate += '"' + array_corrispondenze_colonne_valori[lista_sottotag[i].children[c].nodeName].valori[lista_sottotag[i].children[c].innerHTML].addslashes().replace(/'/gi, ' ') + '",';
     } else
     {
     
     righe_separate += '"' + lista_sottotag[i].children[c].innerHTML.addslashes().replace(/'/gi, ' ') + '",';
     }
     //console.log("COLONNA FOREACH", colonna_foreach);
     colonna = colonna_foreach;
     colonne_separate += colonna_foreach + ",";
     
     if (bool_crea_nuova_tabella === true && colonna !== undefined && colonna.length > 0) {
     
     //DEVE PRENDERE ENTRAMBE LE COLONNE DEL FOREACH
     tutte_le_colonne.pushIfNotExist(colonna, function (e) {
     return e === colonna;
     });
     }
     
     });
     //END FOREACH
     } else if (array_corrispondenze_colonne_valori[lista_sottotag[i].children[c].nodeName].colonna_db !== "DEL") {
     
     //SE E' RICHIESTA UNA SOSTITUZIONE NELL'INSERIMENTO NEL DATABASE, IN CASO DI VALORI SPECIFICI
     if (array_corrispondenze_colonne_valori[lista_sottotag[i].children[c].nodeName].valori !== undefined && array_corrispondenze_colonne_valori[lista_sottotag[i].children[c].nodeName].valori[lista_sottotag[i].children[c].innerHTML] !== undefined)
     {
     righe_separate += '"' + array_corrispondenze_colonne_valori[lista_sottotag[i].children[c].nodeName].valori[lista_sottotag[i].children[c].innerHTML].addslashes().replace(/'/gi, ' ') + '",';
     } else
     {
     righe_separate += '"' + lista_sottotag[i].children[c].innerHTML.addslashes().replace(/'/gi, ' ') + '",';
     }
     colonna = array_corrispondenze_colonne_valori[lista_sottotag[i].children[c].nodeName].colonna_db;
     colonne_separate += colonna + ",";
     
     }
     }
     
     //---
     
     else
     {
     
     colonna = lista_sottotag[i].children[c].nodeName;
     colonne_separate += colonna + ",";
     righe_separate += '"' + lista_sottotag[i].children[c].innerHTML.addslashes().replace(/'/gi, ' ') + '",';
     }
     
     } else
     {
     colonna = lista_sottotag[i].children[c].nodeName;
     
     colonne_separate += colonna + ",";
     righe_separate += '"' + lista_sottotag[i].children[c].innerHTML.addslashes().replace(/'/gi, ' ') + '",';
     }
     //}
     
     
     
     if (bool_elimina_tabella_prima === true) {
     var query_elimina_tabella_precedente = "DROP TABLE IF EXISTS " + nome_tabella_db + ";";
     
     }
     
     if (bool_crea_nuova_tabella === true && colonna !== undefined && colonna.length > 0) {
     
     //TORNA TRUE SE IL VALORE E' STATO AGGIUNTO
     tutte_le_colonne.pushIfNotExist(colonna, function (e) {
     return e === colonna;
     });
     }
     
     //console.log("TUTTE COLONNE", tutte_le_colonne);
     }
     
     //PER AGGIUNGERE ALCUNI VALORI DEI CAMPI FISSI, SE NECESSARIO
     if (array_campi_aggiuntivi_fissi !== undefined && array_campi_aggiuntivi_fissi !== null) {
     array_campi_aggiuntivi_fissi.forEach(function (oggetto_campo_fisso) {
     colonne_separate += oggetto_campo_fisso.colonna + ",";
     righe_separate += '"' + oggetto_campo_fisso.valore + '",';
     });
     }
     
     if (bool_colonne_db_minuscole === true)
     {
     colonne_separate = colonne_separate.toLowerCase();
     }
     
     if (bool_record_maiuscoli === true) {
     righe_separate = righe_separate.toUpperCase();
     }
     
     //ALLA FINE INSERISCE LA QUERY NEL VETTORE PER POI ESSERE RIELABORATA DAL SINCROQUERY
     elenco_query.push("INSERT INTO " + nome_tabella_db + " (" + colonne_separate.slice(0, -1) + ") VALUES ( " + righe_separate.slice(0, -1) + " );");
     }
     
     
     //SERVE PER LA CREAZIONE DELLA TABELLA. FACENDO COSI SI RISPARMIA MEMORIA
     if (array_campi_aggiuntivi_fissi !== undefined && array_campi_aggiuntivi_fissi !== null) {
     
     array_campi_aggiuntivi_fissi.forEach(function (oggetto_campo_fisso) {
     
     tutte_le_colonne.pushIfNotExist(oggetto_campo_fisso.colonna, function (e) {
     return e === oggetto_campo_fisso.colonna;
     });
     
     });
     }
     
     //-- CONDIZIONI PRINCIPALI DELLA FUNZIONE --//
     if (bool_elimina_tabella_prima === true) {
     comanda.sincro.query(query_elimina_tabella_precedente, function () {
     });
     
     }
     
     if (bool_crea_nuova_tabella === true) {
     
     //console.log("TUTTE LE COLONNE: ", tutte_le_colonne);
     
     var colonne_tabella = '';
     
     if (bool_colonne_db_minuscole === true)
     {
     colonne_tabella = tutte_le_colonne.join(",").toLowerCase();
     } else
     {
     colonne_tabella = tutte_le_colonne.join(",");
     }
     
     var query_nuova_tabella = "CREATE TABLE " + nome_tabella_db + "( " + colonne_tabella + ");";
     
     comanda.sincro.query(query_nuova_tabella, function () {
     });
     
     }
     
     
     
     salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # QUERY IMPORTATE # ELENCO: " + elenco_query.join("*") + " # ");
     
     comanda.sincro.db_2.transaction(function (tx, results) {
     
     
     elenco_query.forEach(function (query_vera) {
     
     tx.executeSql(query_vera, [], function (tx, rs) {}, errorHandler2);
     
     function errorHandler2(transaction, error) {
     console.log("ERRORE QUERY INIZIALE", transaction, error, query_vera);
     }
     
     });
     
     //PER EVITARE IL BUG DEL NAN NEL PREZZO
     tx.executeSql("UPDATE comanda SET prezzo_un='0,00' WHERE trim(prezzo_un)='';", [], function (tx, rs) {}, errorHandler2);
     
     function errorHandler2(transaction, error) {
     console.log("ERRORE QUERY INIZIALE", transaction, error, "UPDATE comanda SET prezzo_un='0,00' WHERE trim(prezzo_un)='';");
     }
     
     
     });
     
     
     console.log(elenco_query);
     
     callBack(true);
     
     }*/

    this.elimina_file_xml = function (nome, cancellazione_ampia, callBack) {


        salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # INIZIO ELIMINA FILE # NOME: " + nome + " - CANCELLAZIONE AMPIA:" + cancellazione_ampia + " # ");



        var request = $.ajax({
            timeout: 10000,
            method: "POST",
            url: './classi_php/elimina_file_xml.php',
            data: {directory_dati: comanda.directory_dati, ora_servizio: comanda.ora_servizio, nome: nome, cancellazione_ampia: cancellazione_ampia}
        });

        request.done(function (data) {
            console.log("SALVAFILEXML DONE");
            if (data === "1") {
                salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # FINE ELIMINA FILE # NOME: " + nome + " - CANCELLAZIONE AMPIA:" + cancellazione_ampia + " # ");

                callBack(true);
            } else
            {

                salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # ERRORE ELIMINA FILE 1 # NOME: " + nome + " - CANCELLAZIONE AMPIA:" + cancellazione_ampia + " # " + data);

                alert("ATTENZIONE! ERRORE DI CANCELLAZIONE FILE (1) : " + data);

                try {
                    $(document).off(comanda.eventino, evento_selettore_tavolo);
                } catch (e) {
                    console.log(e);
                }
                $(document).on(comanda.eventino, 'div.tavolo.rett,div.tavolo.cerc', evento_selettore_tavolo);
                $('.tasto_bestellung').css('pointer-events', '');
                $('.tasto_bargeld').css('pointer-events', '');
                $('.tasto_quittung').css('pointer-events', '');
                $('.tasto_rechnung').css('pointer-events', '');

                //NON RESTITUISCE CALLBACK ALTRIMENTI USCIREBBE ANCHE SENZA AVER ELIMINATO IL TAVOLO
                //callBack(false);

            }
        });

        request.fail(function (event) {


            salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # ERRORE ELIMINA FILE 2 # NOME: " + nome + " - CANCELLAZIONE AMPIA:" + cancellazione_ampia + " # ");

            console.log("SALVAFILEXML FAIL");

            alert("ATTENZIONE! Sei fuori portata WiFi. Avvicinati all'antenna e riprova.");
            try {
                $(document).off(comanda.eventino, evento_selettore_tavolo);
            } catch (e) {
                console.log(e);
            }
            $(document).on(comanda.eventino, 'div.tavolo.rett,div.tavolo.cerc', evento_selettore_tavolo);
            $('.tasto_bestellung').css('pointer-events', '');
            $('.tasto_bargeld').css('pointer-events', '');
            $('.tasto_quittung').css('pointer-events', '');
            $('.tasto_rechnung').css('pointer-events', '');
            //event.preventDefault();
            //callBack(false);

        });




    };


    this.elimina_tavolo_occupato = function (nome, cancellazione_ampia, callBack) {


        salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # INIZIO ELIMINA FILE # NOME: " + nome + " - CANCELLAZIONE AMPIA:" + cancellazione_ampia + " # ");

        var counter = 0;

        function ajaxCall() {

            counter++;

            var request = $.ajax({
                timeout: 10000,
                method: "POST",
                url: './classi_php/elimina_file_xml.php',
                data: {directory_dati: comanda.directory_dati, ora_servizio: comanda.ora_servizio, nome: nome, cancellazione_ampia: cancellazione_ampia}
            });

            request.done(function (data) {
                console.log("SALVAFILEXML DONE");
                if (data === "1") {
                    salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # FINE ELIMINA FILE # NOME: " + nome + " - CANCELLAZIONE AMPIA:" + cancellazione_ampia + " # ");

                    callBack(true);
                } else
                {
                    if (counter === 5) {
                        salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # ERRORE ELIMINA FILE 1 # NOME: " + nome + " - CANCELLAZIONE AMPIA:" + cancellazione_ampia + " # " + data);

                        alert("ATTENZIONE! ERRORE DI CANCELLAZIONE FILE (1) : " + data);

                        try {
                            $(document).off(comanda.eventino, evento_selettore_tavolo);
                        } catch (e) {
                            console.log(e);
                        }
                        $(document).on(comanda.eventino, 'div.tavolo.rett,div.tavolo.cerc', evento_selettore_tavolo);
                        $('.tasto_bestellung').css('pointer-events', '');
                        $('.tasto_bargeld').css('pointer-events', '');
                        $('.tasto_quittung').css('pointer-events', '');
                        $('.tasto_rechnung').css('pointer-events', '');

                        //NON RESTITUISCE CALLBACK ALTRIMENTI USCIREBBE ANCHE SENZA AVER ELIMINATO IL TAVOLO
                        //callBack(false);
                    } else {
                        ajaxCall();
                    }
                }
            });

            request.fail(function (event) {

                if (counter === 5) {

                    salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # ERRORE ELIMINA FILE 2 # NOME: " + nome + " - CANCELLAZIONE AMPIA:" + cancellazione_ampia + " # ");

                    console.log("SALVAFILEXML FAIL");

                    alert("ATTENZIONE! Sei fuori portata WiFi. Avvicinati all'antenna e riprova.");
                    try {
                        $(document).off(comanda.eventino, evento_selettore_tavolo);
                    } catch (e) {
                        console.log(e);
                    }
                    $(document).on(comanda.eventino, 'div.tavolo.rett,div.tavolo.cerc', evento_selettore_tavolo);
                    $('.tasto_bestellung').css('pointer-events', '');
                    $('.tasto_bargeld').css('pointer-events', '');
                    $('.tasto_quittung').css('pointer-events', '');
                    $('.tasto_rechnung').css('pointer-events', '');
                    //event.preventDefault();
                    //callBack(false);
                } else
                {
                    ajaxCall();
                }
            });
        }

        ajaxCall();

    };



    this.sistema_planing_ingresso = function (callback) {

        var request = $.ajax({
            timeout: 10000,
            method: "POST",
            url: comanda.url_server + '/classi_php/sistema_planing_ingresso.php',
            data: {directory_dati: comanda.directory_dati, ora_servizio: comanda.ora_servizio, servizio: comanda.folder_number, id_terminale: comanda.terminale}

        });


        request.done(function (data) {

            callback(true);
        });

        request.fail(function (event) {


            alert("ATTENZIONE! Sei fuori portata WiFi. Controlla che la comanda non sia già stata STAMPATA,\nprima di premere di nuovo 'COMANDA'.");
            try {
                $(document).off(comanda.eventino, evento_selettore_tavolo);
            } catch (e) {
                console.log(e);
            }
            $(document).on(comanda.eventino, 'div.tavolo.rett,div.tavolo.cerc', evento_selettore_tavolo);
            $('.tasto_bestellung').css('pointer-events', '');
            $('.tasto_bargeld').css('pointer-events', '');
            $('.tasto_quittung').css('pointer-events', '');
            $('.tasto_rechnung').css('pointer-events', '');

            callback(false);

        });

    };

//CE UNA FUNZIONE UGUALE IN OPERAZIONI XML
    this.rinomina_file_xml = function (nome, struttura, tipo, callBack) {

        salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # INIZIO RINOMINA FILE # NOME: " + nome + " - STRUTTURA: " + struttura + " - TIPO:" + tipo + " # ");


        var request = $.ajax({
            timeout: 10000,
            method: "POST",
            url: comanda.url_server + '/classi_php/rinomina_file_xml.php',
            data: {directory_dati: comanda.directory_dati, ora_servizio: comanda.ora_servizio, nome: nome, struttura: struttura, tipo: tipo, servizio: comanda.folder_number}
        });


        request.done(function (data) {

            salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # FINE RINOMINA FILE # NOME: " + nome + " - STRUTTURA: " + struttura + " - TIPO:" + tipo + " # ");


            callBack(true);

        });

        request.fail(function (event) {

            salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # ERRORE RINOMINA FILE 1 # NOME: " + nome + " - STRUTTURA: " + struttura + " - TIPO:" + tipo + " # ");

            alert("ATTENZIONE! Sei fuori portata WiFi. Controlla che la comanda non sia già stata STAMPATA,\nprima di premere di nuovo 'COMANDA'.");
            try {
                $(document).off(comanda.eventino, evento_selettore_tavolo);
            } catch (e) {
                console.log(e);
            }
            $(document).on(comanda.eventino, 'div.tavolo.rett,div.tavolo.cerc', evento_selettore_tavolo);
            $('.tasto_bestellung').css('pointer-events', '');
            $('.tasto_bargeld').css('pointer-events', '');
            $('.tasto_quittung').css('pointer-events', '');
            $('.tasto_rechnung').css('pointer-events', '');
            callBack(false);

        });

    };


    //CE UNA FUNZIONE UGUALE IN OPERAZIONI XML
    this.salva_file_xml = function (nome, struttura, tipo, callBack) {

        salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # INIZIO SALVA FILE # NOME: " + nome + " - STRUTTURA: " + struttura + " - TIPO:" + tipo + " # ");


        $('.loader').show();

        var totale_scontrino = $(comanda.totale_scontrino).html();
        var md5 = CryptoJS.MD5(struttura).toString();

        var counter = 0;

        ajaxCall();

        function ajaxCall() {

            counter++;


            var request = $.ajax({
                timeout: 10000,
                method: "POST",
                url: comanda.url_server + '/classi_php/salva_file_xml.php',
                data: {directory_dati: comanda.directory_dati, ora_servizio: comanda.ora_servizio, nome: nome, struttura: struttura, tipo: tipo, servizio: comanda.folder_number}
            });


            request.done(function (data) {

                if (data === md5) {
                    comanda.xml.rinomina_file_xml(nome, struttura, tipo, function (res) {

                        salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # FINE SALVA FILE # NOME: " + nome + " - STRUTTURA: " + struttura + " - TIPO:" + tipo + " # ");

                        callBack(true);
                        $('.loader').hide();

                    });

                    //callBack(true);

                } else
                {
                    if (counter === 5) {

                        try {
                            $(document).off(comanda.eventino, evento_selettore_tavolo);
                        } catch (e) {
                            console.log(e);
                        }
                        $(document).on(comanda.eventino, 'div.tavolo.rett,div.tavolo.cerc', evento_selettore_tavolo);
                        $('.tasto_bestellung').css('pointer-events', '');
                        $('.tasto_bargeld').css('pointer-events', '');
                        $('.tasto_quittung').css('pointer-events', '');
                        $('.tasto_rechnung').css('pointer-events', '');
                        $('.loader').hide();
                        salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # ERRORE SALVA FILE 1 # NOME: " + nome + " - STRUTTURA: " + struttura + " - TIPO:" + tipo + " # ");

                        alert("ATTENZIONE! Sei fuori portata WiFi. Avvicinati all'antenna e riprova.");

                    } else
                    {
                        ajaxCall();
                    }
                }
            });

            request.fail(function (event) {


                if (counter === 5) {

                    try {
                        $(document).off(comanda.eventino, evento_selettore_tavolo);
                    } catch (e) {
                        console.log(e);
                    }
                    $(document).on(comanda.eventino, 'div.tavolo.rett,div.tavolo.cerc', evento_selettore_tavolo);
                    $('.tasto_bestellung').css('pointer-events', '');
                    $('.tasto_bargeld').css('pointer-events', '');
                    $('.tasto_quittung').css('pointer-events', '');
                    $('.tasto_rechnung').css('pointer-events', '');
                    $('.loader').hide();

                    salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # ERRORE SALVA FILE 2 # NOME: " + nome + " - STRUTTURA: " + struttura + " - TIPO:" + tipo + " # ");

                    alert("ATTENZIONE! Sei fuori portata WiFi. Avvicinati all'antenna e riprova.");
                } else
                {
                    ajaxCall();
                }
            });
        }
    };
};