/* global comanda */

async function fa_richiesta_codice_lotteria() {

    if (comanda.ultimo_id_cliente !== undefined && !isNaN(comanda.ultimo_id_cliente)) {

        codice_lotteria = alasql("select codice_lotteria from clienti where id=" + comanda.ultimo_id_cliente + ";");

        if (codice_lotteria.length > 0) {
            codice_lotteria = codice_lotteria[0].codice_lotteria;
        }

        if (codice_lotteria.length > 0) {

            await fa_set_codice_lotteria(codice_lotteria);

            return codice_lotteria;
        } else
        {
            return "";
        }

    } else {

        let testo_query = "select codice_lotteria from elenco_codici_lotteria_comanda where progressivo_comanda='" + id_comanda_attuale_alasql() + "' limit 1;";

        let codice_lotteria = alasql(testo_query);

        if (codice_lotteria.length > 0) {
            codice_lotteria = codice_lotteria[0].codice_lotteria;
        }

        if (codice_lotteria.length > 0) {

            return codice_lotteria;

        } else {

            return "";

        }

    }

}

async function fa_set_codice_lotteria(codice_lotteria) {

    if (comanda.ultimo_id_cliente !== undefined && !isNaN(comanda.ultimo_id_cliente)) {

    } else {

        let id_comanda = id_comanda_attuale_alasql();


        if (id_comanda !== false) {

            let testo_query2 = "delete from elenco_codici_lotteria_comanda where progressivo_comanda='" + id_comanda_attuale_alasql() + "';";

            comanda.sock.send({tipo: "aggiornamento_codici_lotteria", operatore: comanda.operatore, query: testo_query2, terminale: comanda.terminale, ip: comanda.ip_address});

            alasql(testo_query2);


            let testo_query = "insert into elenco_codici_lotteria_comanda (progressivo_comanda,codice_lotteria) VALUES ( '" + id_comanda_attuale_alasql() + "','" + codice_lotteria + "');";

            comanda.sock.send({tipo: "aggiornamento_codici_lotteria", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});

            alasql(testo_query);


        }
    }

    return true;
}

function focus_codice_lotteria_cliente() {

    $('[name="codice_lotteria"]:visible').focus();
    
    beep();
    setTimeout(function () {
        beep();
    }, 110);
    setTimeout(function () {
        beep();
    }, 220);

}