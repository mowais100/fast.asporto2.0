if (navigator.getBattery !== undefined) {
    navigator.getBattery().then(function (battery) {
        function updateAllBatteryInfo() {
            updateChargeInfo();
            updateLevelInfo();
            updateChargingInfo();
            updateDischargingInfo();
        }
        updateAllBatteryInfo();

        battery.addEventListener('chargingchange', function () {
            updateChargeInfo();
        });
        function updateChargeInfo() {


            // battery.charging ? $('#stato_batteria').css('background-color','white') :$('#stato_batteria').css('background-color','');


            console.log("Battery charging? "
                    + (battery.charging ? "Yes" : "No"));
        }

        battery.addEventListener('levelchange', function () {
            updateLevelInfo();
        });
        function updateLevelInfo() {

            var percentuale = battery.level * 100;

            if (percentuale >= 80)
            {
                //VERDE
                $('#immagine_batteria').attr('src', './img/batteria/Batteria_Verde60x41.png');
            } else if (percentuale >= 50 && percentuale <= 79) {
                //GIALLO
                $('#immagine_batteria').attr('src', './img/batteria/Batteria_Gialla60x41.png');
            } else if (percentuale >= 21 && percentuale <= 49) {
                //MARRONE
                $('#immagine_batteria').attr('src', './img/batteria/Batteria_Marrone60x41.png');
            } else if (percentuale <= 20) {
                //ROSSO
                $('#immagine_batteria').attr('src', './img/batteria/Batteria_Rossa60x41.png');
            }


            $('#percentuale_batteria').html(parseInt(percentuale) + "%");

            console.log("Battery level: "
                    + battery.level * 100 + "%");
        }

        battery.addEventListener('chargingtimechange', function () {
            updateChargingInfo();
        });

        function updateChargingInfo() {
            //$('#stato_batteria');
            console.log("Battery charging time: "
                    + battery.chargingTime + " seconds");
        }

        battery.addEventListener('dischargingtimechange', function () {
            updateDischargingInfo();
        });
        function updateDischargingInfo() {
            //$('#stato_batteria');
            console.log("Battery discharging time: "
                    + battery.dischargingTime + " seconds");
        }

    });

}