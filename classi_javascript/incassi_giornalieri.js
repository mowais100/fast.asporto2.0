/* global comanda, parseFloat, bootbox */

//Estrae valori univoci da una colonna, in base ad una matrice multidimensionale
function estrazione_univoca_colonna(param_array, nome_colonna) {
    var array_restituito = new Array();

    param_array.forEach(function (element, index) {
        if (array_restituito.indexOf(element[nome_colonna]) === -1) {
            array_restituito.push(element[nome_colonna]);
            //console.log("ARTICOLO GENERICO DEBUG ESTRAZIONE", param_array, array_restituito, element[nome_colonna]);
        }
        //console.log("ARTICOLO GENERICO DEBUG ESTRAZIONE", param_array, array_restituito, "FALSE");
    });

    return array_restituito;
}

//Riordina la matrice multidimensionale in base ai valori di una colonna specifica, in ordine crescente o decrescente
function riordina(param_array, param_indice, tipo_conversione, decrescente, raggruppamento_per_indice) {


    var array_restituito = new Array();
    var result = new Array();

    var param_array2 = new Array();

    var valore1, valore2;

    if (raggruppamento_per_indice !== false && typeof (raggruppamento_per_indice) === 'string') {

        // console.log("array da raggruppare", param_array);

        var result_descrizione = new Array();
        var result = new Array();



        param_array.forEach(function (element) {

            var BreakException = {};

            try {
                var indice = -1;

                result.forEach(function (v, i) {

                    if (v[raggruppamento_per_indice] === element[raggruppamento_per_indice] && v['perc_iva'] === element['perc_iva'] && v['categoria'] === element['categoria']) {
                        indice = i;
                        throw BreakException;
                    }

                });
            } catch (e) {
                if (e !== BreakException)
                    throw e;
            }
            if (element.QTA === "") {
                element.QTA = "0";
            }


            element.totale = parseFloat(((parseFloat(element.prezzo_un)))) /*/ 100 * (100 - element.sconto_perc))*/;

            if (indice === -1) {
                result_descrizione.push(element[raggruppamento_per_indice]);
                element.quantita_tot = parseInt(element.quantita);

                /*element.prezzo = parseFloat(((parseFloat(element.prezzo_un)) * element.quantita) / 100 * (100 - element.sconto_perc));*/

                element.prezzo = parseFloat(element.totale);

                if (raggruppamento_per_indice === 'sconto_perc') {
                    if (element.posizione === 'SCONTO') {
                        element.conti_scontati = 1;
                    } else {
                        element.conti_scontati = 0;
                    }
                    element.descrizione = element.sconto_perc + "%";
                    /*element.sconto_perc_tot = parseFloat(((parseFloat(element.prezzo_un)) * element.quantita) / 100 * element.sconto_perc);*/

                    element.sconto_perc_tot = parseFloat(element.totale);
                }
                result.push(element);
            } else {
                var index = indice;
                result[index].quantita_tot += parseInt(element.quantita);
                /*result[index].prezzo += parseFloat(((parseFloat(element.prezzo_un)) * element.quantita) / 100 * (100 - element.sconto_perc));*/

                result[index].prezzo += parseFloat(element.totale);

                if (raggruppamento_per_indice === 'sconto_perc') {
                    if (element.posizione === 'SCONTO') {
                        result[index].conti_scontati += 1;
                    }
                    /*result[index].sconto_perc_tot += parseFloat(((parseFloat(element.prezzo_un)) * element.quantita) / 100 * element.sconto_perc);*/

                    result[index].sconto_perc_tot += parseFloat(element.totale);
                }
            }
        });

        delete result_descrizione;

        //console.log(result);

        //console.log("RAGGRUPPAMENTO PER INDICE1", param_array);

        param_array2 = result;

    } else {

        param_array2 = param_array.map(function (v) {
            if (v.QTA) {
                v.quantita_tot = v.QTA;
            } else {
                v.quantita_tot = "1";
            }
            v.prezzo = v.prezzo_un;
            return v;
        });


    }




    array_restituito = param_array2.sort(function (a, b) {

        switch (tipo_conversione.toLowerCase()) {
            case "date":
                valore1 = parseInt(a.data_cancellazione.substr(6, 2) + "" + a.data_cancellazione.substr(3, 2) + "" + a.data_cancellazione.substr(0, 2) + "" + a.ora_cancellazione.substr(0, 2) + "" + a.ora_cancellazione.substr(3, 2));
                valore2 = parseInt(b.data_cancellazione.substr(6, 2) + "" + b.data_cancellazione.substr(3, 2) + "" + b.data_cancellazione.substr(0, 2) + "" + b.ora_cancellazione.substr(0, 2) + "" + b.ora_cancellazione.substr(3, 2));
                break;
            case "float":
                valore1 = parseFloat(a[param_indice]);
                valore2 = parseFloat(b[param_indice]);
                break;
            case "int":
                valore1 = parseInt(a[param_indice]);
                valore2 = parseInt(b[param_indice]);
                break;
            case "default":
            default:
                valore1 = a[param_indice];
                valore2 = b[param_indice];
        }

        if (valore1 > valore2) {
            return 1;
        }

        if (valore1 < valore2) {
            return -1;
        }

        return 0;

    });




    if (decrescente === true) {
        array_restituito = array_restituito.reverse();
    }

    return array_restituito;
}




function riordina_impasti(param_array, param_indice, tipo_conversione, decrescente, raggruppamento_per_indice) {


    var array_restituito = new Array();
    var result = new Array();

    var param_array2 = new Array();

    var valore1, valore2;

    if (raggruppamento_per_indice !== false && typeof (raggruppamento_per_indice) === 'string') {

        // console.log("array da raggruppare", param_array);

        var result_descrizione = new Array();
        var result = new Array();



        param_array.forEach(function (element) {

            var BreakException = {};

            try {
                var indice = -1;

                result.forEach(function (v, i) {

                    if (v[raggruppamento_per_indice] === element[raggruppamento_per_indice] && v['perc_iva'] === element['perc_iva'] && v['tipo_impasto'] === element['tipo_impasto']) {
                        indice = i;
                        throw BreakException;
                    }

                });
            } catch (e) {
                if (e !== BreakException)
                    throw e;
            }
            if (element.QTA === "") {
                element.QTA = "0";
            }


            element.totale = parseFloat(((parseFloat(element.prezzo_un)))) /*/ 100 * (100 - element.sconto_perc))*/;

            if (indice === -1) {
                result_descrizione.push(element[raggruppamento_per_indice]);
                element.quantita_tot = parseInt(element.quantita_tot);

                /*element.prezzo = parseFloat(((parseFloat(element.prezzo_un)) * element.quantita) / 100 * (100 - element.sconto_perc));*/

                element.prezzo = parseFloat(element.totale);

                if (raggruppamento_per_indice === 'sconto_perc') {
                    if (element.posizione === 'SCONTO') {
                        element.conti_scontati = 1;
                    } else {
                        element.conti_scontati = 0;
                    }
                    element.descrizione = element.sconto_perc + "%";
                    /*element.sconto_perc_tot = parseFloat(((parseFloat(element.prezzo_un)) * element.quantita) / 100 * element.sconto_perc);*/

                    element.sconto_perc_tot = parseFloat(element.totale);
                }
                result.push(element);
            } else {
                var index = indice;
                result[index].quantita_tot += parseInt(element.quantita_tot);
                /*result[index].prezzo += parseFloat(((parseFloat(element.prezzo_un)) * element.quantita) / 100 * (100 - element.sconto_perc));*/

                result[index].prezzo += parseFloat(element.totale);

                if (raggruppamento_per_indice === 'sconto_perc') {
                    if (element.posizione === 'SCONTO') {
                        result[index].conti_scontati += 1;
                    }
                    /*result[index].sconto_perc_tot += parseFloat(((parseFloat(element.prezzo_un)) * element.quantita) / 100 * element.sconto_perc);*/

                    result[index].sconto_perc_tot += parseFloat(element.totale);
                }
            }
        });

        delete result_descrizione;

        //console.log(result);

        //console.log("RAGGRUPPAMENTO PER INDICE1", param_array);

        param_array2 = result;

    } else {

        param_array2 = param_array.map(function (v) {
            if (v.QTA) {
                v.quantita_tot = v.QTA;
            } else {
                v.quantita_tot = "1";
            }
            v.prezzo = v.prezzo_un;
            return v;
        });


    }




    array_restituito = param_array2.sort(function (a, b) {

        switch (tipo_conversione.toLowerCase()) {
            case "date":
                valore1 = parseInt(a.data_cancellazione.substr(6, 2) + "" + a.data_cancellazione.substr(3, 2) + "" + a.data_cancellazione.substr(0, 2) + "" + a.ora_cancellazione.substr(0, 2) + "" + a.ora_cancellazione.substr(3, 2));
                valore2 = parseInt(b.data_cancellazione.substr(6, 2) + "" + b.data_cancellazione.substr(3, 2) + "" + b.data_cancellazione.substr(0, 2) + "" + b.ora_cancellazione.substr(0, 2) + "" + b.ora_cancellazione.substr(3, 2));
                break;
            case "float":
                valore1 = parseFloat(a[param_indice]);
                valore2 = parseFloat(b[param_indice]);
                break;
            case "int":
                valore1 = parseInt(a[param_indice]);
                valore2 = parseInt(b[param_indice]);
                break;
            case "default":
            default:
                valore1 = a[param_indice];
                valore2 = b[param_indice];
        }

        if (valore1 > valore2) {
            return 1;
        }

        if (valore1 < valore2) {
            return -1;
        }

        return 0;

    });




    if (decrescente === true) {
        array_restituito = array_restituito.reverse();
    }

    return array_restituito;
}

//Somma per indice
//Dato un oggetto con gli stessi nomi di colonna
//Somma tutti i dati numerici all'interno di ogni colonna
//e restituisce un oggetto unito
function somma_array_oggetti(array_oggetti, nomi_colonne_sommabili, indice) {

    try {

        var nuovo_array = new Array();

        array_oggetti.forEach(function (obj) {

            var corrispondenza = false;
            nuovo_array.forEach(function (valore, index) {

                if (typeof valore[indice] === "number") {
                    valore[indice] = valore[indice].toString();
                }

                if (typeof obj[indice] === "number") {
                    obj[indice] = obj[indice].toString();
                }


                if (valore[indice].trim() === obj[indice].trim()) {
                    corrispondenza = index;
                }
            });

            if (corrispondenza === false) {
                nuovo_array.push(obj);
            } else {
                nomi_colonne_sommabili.forEach(function (val, ind) {
                    nuovo_array[corrispondenza][val] = parseFloat(nuovo_array[corrispondenza][val]) + parseFloat(obj[val]);
                });
            }

        });

        return nuovo_array;

    } catch (e) {

        console.log("errore", e);

        return false;

    }



}



//Estrae dei record da una matrice multidimensionale, solo dove 
//una specifica colonna corrisponde ad un valore ben definito
function estrazione_array(param_array, param_indice, param_valore) {

    var array_restituito = new Array();
    var valore1, valore2;



    var i = 0;
    param_array.forEach(function (element, index) {
        if (element[param_indice] === param_valore) {
            array_restituito[i] = element;
            i++;
        }
    });

    return array_restituito;
}

function stampa_totali_giornata(layout, facoltativo_categoria, extra, a_video) {
    /*var array_data_partenza = $('#selettore_data_partenza_grafico:visible').multiDatesPicker('getDates');
     var array_data_arrivo = $('#selettore_data_arrivo_grafico:visible').multiDatesPicker('getDates');*/
    comanda.array_db_centri = new Array();
    $.ajax({
        type: "POST",
        async: true,
        url: comanda.url_server + 'classi_php/trasferimentosqlitemysql_temp.lib.php',
        dataType: "json",
        beforeSend: function () { },
        success: function (rar) {
            if (rar === true) {

                if (comanda.dispositivo === 'back_office') {

                    var array_data_partenza = new Array();
                    var array_data_arrivo = new Array();

                    var partenza = $('#selettore_data_partenza_grafico').val();
                    var arrivo = $('#selettore_data_arrivo_grafico').val();


                    var anno_bo_p = partenza.substr(0, 4);
                    var mese_bo_p = partenza.substr(5, 2);
                    var giorno_bo_p = partenza.substr(8, 2);

                    var anno_bo_a = arrivo.substr(0, 4);
                    var mese_bo_a = arrivo.substr(5, 2);
                    var giorno_bo_a = arrivo.substr(8, 2);

                    array_data_partenza[0] = giorno_bo_p + '/' + mese_bo_p + '/' + anno_bo_p;
                    array_data_arrivo[0] = giorno_bo_a + '/' + mese_bo_a + '/' + anno_bo_a;


                } else if (comanda.dispositivo === 'lang_8') {

                    /*if (array_data_arrivo.length === 0 && array_data_arrivo.length === 0) {*/
                    var array_data_partenza = $('#data_incassi_partenza:visible').multiDatesPicker('getDates');
                    var array_data_arrivo = $('#data_incassi_arrivo:visible').multiDatesPicker('getDates');
                }
                //}

                if (array_data_partenza.length === 0 || array_data_arrivo.length === 0) {

                    var array_data_partenza = new Array();
                    var array_data_arrivo = new Array();

                    var oggi = new Date();
                    var anno = oggi.getFullYear();
                    var mese = addZero((oggi.getMonth() + 1), 2);
                    var giorno = addZero(oggi.getDate(), 2);

                    array_data_partenza[0] = '' + giorno + '/' + mese + '/' + anno;
                    array_data_arrivo[0] = '' + giorno + '/' + mese + '/' + anno;

                }

                var query = "select * from settaggi_profili where id=" + comanda.folder_number + " ;";
                comanda.sincro.query(query, function (settaggi_profili) {
                    settaggi_profili = settaggi_profili[0];
                    /*Tipo:
                     * 
                     * soloincassi 3
                     * 
                     * dettagliovenduti 2
                     * 
                     * misto 1
                     * 
                     */

                    //alert("ATTENZIONE: PRIMA DI STAMPARE I TOTALI, \nASSICURATI CHE OGNI DISPOSITIVO FUNZIONI CORRETTAMENTE \nE CHE NON CI SIANO PROBLEMI DI RETE.");

                    //PARTE CHIUSURA

                    //SETTAGGI BOOL
                    //DISABILITA RAGGRUPPAMENTO PORTATA
                    var set_disabilita_raggruppamento_portata = true;
                    //DISABILITA RAGGRUPPAMENTO DESTINAZIONE
                    var set_disabilita_raggruppamento_destinazione = false;
                    //DISABILITA RAGGRUPPAMENTO CATEGORIA
                    var set_disabilita_raggruppamento_categoria = false;
                    //DICHIARAZIONE VARIABILI
                    //
                    var numero_coperti = 0;
                    //
                    //TOT:0 Totale
                    var totale = 0;
                    //SCONTO ARTICOLO SERVE NEL CICLO
                    var prezzo_articolo = 0;
                    var sconto_articolo = 0;
                    var incasso_singolo = 0;
                    //TOTALE SCONTI:0
                    var totale_sconti = 0;
                    //NON PAGATO
                    var non_pagato = 0;
                    //INCASSO: 0
                    var incasso = 0;
                    //INIZIALIZZAZIONE OGGETTI


                    var totale_incassato = 0;
                    var totale_contanti = 0;
                    var totale_bancomat = 0;
                    var totale_cartecredito = 0;
                    var totale_buonipasto = 0;
                    var totale_buoniacquisto = 0;
                    var totale_non_pagato = 0;
                    var totale_non_riscosso_servizi = 0;
                    var totale_sconto_a_pagare = 0;
                    var totale_arrotondamento = 0;
                    var totale_totale = 0;
                    var totale_buoniacquisto_emessi = 0;

                    var consegna_metro = false;
                    if (comanda.consegna_mezzo_metro.length > 0 || comanda.consegna_metro.length > 0) {
                        consegna_metro = true;
                    }

                    var totale_consegne = new Object();


                    if (consegna_metro === true) {
                        totale_consegne["MAXI"] = { "importo": 0, "qta": 0 };
                        totale_consegne["MEZZO"] = { "importo": 0, "qta": 0 };
                        totale_consegne["METRO"] = { "importo": 0, "qta": 0 };
                        totale_consegne["NORMALI"] = { "importo": 0, "qta": 0 };
                    } else {
                        totale_consegne["PIZZE"] = { "importo": 0, "qta": 0 };
                    }

                    var totale_qta_consegne = 0;

                    //IVA
                    var iva = new Object();
                    //OPERAZIONI_OPERATORE
                    var operazioni_operatore = new Object();
                    //ARTICOLO
                    //var articolo = new Object();
                    var articolo_generico = new Array();
                    var id_consegne = new Array();


                    var per_tavolo_brove = new Object();
                    //STATISTICO
                    var statistico = new Object();
                    //STATISTICO['BAR']
                    statistico['BAR'] = new Object();
                    statistico['BAR'].incasso = 0;
                    //STATISTICO['TAKEAWAY']
                    statistico['TAKEAWAY'] = new Object();
                    statistico['TAKEAWAY'].incasso = 0;
                    //STATISTICO['RISTORAZIONE']
                    statistico['RISTORAZIONE'] = new Object();
                    statistico['RISTORAZIONE'].incasso = 0;

                    var fisco_teste = new Object;

                    if (comanda.lingua_stampa === 'italiano') {
                        fisco_teste['SCONTRINI'] = new Object();
                        fisco_teste['SCONTRINI'].numero = 0;
                        fisco_teste['SCONTRINI'].importo = 0;

                        fisco_teste['FATTURE'] = new Object();

                        fisco_teste['FATTURE'].da_numero = 0;
                        fisco_teste['FATTURE'].a_numero = 0;

                        fisco_teste['FATTURE'].numero = 0;
                        fisco_teste['FATTURE'].importo = 0;
                    } else if (comanda.lingua_stampa === 'deutsch') {

                        fisco_teste['QUITTUNG'] = new Object();
                        fisco_teste['QUITTUNG'].numero = 0;
                        fisco_teste['QUITTUNG'].importo = 0;

                        fisco_teste['RECHNUNG'] = new Object();
                        fisco_teste['RECHNUNG'].numero = 0;
                        fisco_teste['RECHNUNG'].importo = 0;
                    }

                    //CONTEGGIO FATTURE SCONTRINI
                    var fisco = new Object();
                    fisco['FATTURE'] = new Object();
                    fisco['FATTURE'].incasso = 0;
                    fisco['FATTURE'].numero_min = 0;
                    fisco['FATTURE'].numero_max = 0;
                    fisco['SCONTRINI'] = new Object();
                    fisco['SCONTRINI'].incasso = 0;
                    fisco['SCONTRINI'].numero_min = 0;
                    fisco['SCONTRINI'].numero_max = 0;
                    console.log("STAMPA TOTALI QUERY OK");

                    var filtri = '';
                    var filtri_stampa = '';
                    var filtro_data = '';

                    var where_misuratore = "";
                    var where_terminali = "";
                    var where_categorie = "";
                    var where_gruppi = "";
                    var where_giorni = "";
                    if (array_data_partenza.length === 1 && array_data_arrivo.length === 1) {

                        if (comanda.dispositivo === 'back_office') {

                            if ($('[name^=gbo_][type="checkbox"]:checked').length > 0) {

                                var array_giorni = new Array();

                                filtri += "<br/><strong>GIORNI:</strong><br/>";
                                filtri_stampa += "\nGIORNI:\n";

                                $('[name^=gbo_][type="checkbox"]:checked').each(function (a, b) {


                                    array_giorni.push("'" + $(b).val() + "'");
                                    filtri += "- " + $(b).attr('nome_filtro') + "<br/>";
                                    filtri_stampa += "- " + $(b).attr('nome_filtro') + "\n";

                                });

                                var array_giorni_joined = array_giorni.join(',');


                                if (array_giorni_joined.length > 0) {
                                    where_giorni = " AND giorno IN (" + array_giorni_joined + ") ";
                                }
                            }


                            $('#elenco_centri_backoffice [type="checkbox"]:checked').each(function (a, b) {


                                comanda.array_db_centri.push($(b).val());
                                filtri += $(b).val().toLowerCase().capitalize() + ", ";
                                filtri_stampa += $(b).val().toLowerCase().capitalize() + ", ";

                            });

                            if ($('#elenco_terminali_backoffice [type="checkbox"]:checked').length > 0) {

                                var array_terminali = new Array();

                                filtri += "<br/><strong>TERMINALI:</strong><br/>";
                                filtri_stampa += "\nTERMINALI:\n";

                                $('#elenco_terminali_backoffice [type="checkbox"]:checked').each(function (a, b) {


                                    array_terminali.push("'" + $(b).val() + "'");
                                    filtri += "- " + $(b).val() + "<br/>";
                                    filtri_stampa += "- " + $(b).val() + "\n";

                                });

                                var array_terminali_joined = array_terminali.join(',');


                                if (array_terminali_joined.length > 0) {
                                    where_terminali = " AND nome_pc IN (" + array_terminali_joined + ") ";
                                }
                            }

                            if ($('#elenco_categorie_backoffice [type="checkbox"]:checked').length > 0) {
                                var array_categorie = new Array();
                                filtri += "<br/><strong>CATEGORIE:</strong><br/>";
                                filtri_stampa += "\nCATEGORIE:\n";

                                $('#elenco_categorie_backoffice [type="checkbox"]:checked').each(function (a, b) {

                                    array_categorie.push("'" + $(b).val() + "'");
                                    filtri += "- " + $(b).attr('name').substr(4) + "<br/>";
                                    filtri_stampa += "- " + $(b).attr('name').substr(4) + "\n";


                                });

                                var array_categorie_joined = array_categorie.join(',');


                                if (array_categorie_joined.length > 0) {
                                    where_categorie = " AND categoria IN (" + array_categorie_joined + ") ";
                                }
                            }

                            if ($('#elenco_gruppi_backoffice [type="checkbox"]:checked').length > 0) {
                                var array_gruppi = new Array();
                                filtri += "<br/><strong>GRUPPI:</strong><br/>";
                                filtri_stampa += "\nGRUPPI:\n";

                                $('#elenco_gruppi_backoffice [type="checkbox"]:checked').each(function (a, b) {

                                    array_gruppi.push("'" + $(b).val() + "'");
                                    filtri += "- " + $(b).attr('name').substr(4) + "<br/>";
                                    filtri_stampa += "- " + $(b).attr('name').substr(4) + "\n";


                                });

                                var array_gruppi_joined = array_gruppi.join(',');


                                if (array_gruppi_joined.length > 0) {
                                    where_gruppi = " AND gruppo IN (" + array_gruppi_joined + ") ";
                                }
                            }
                        } else if (comanda.dispositivo === 'lang_8') {
                            if (layout === "soloincassi" && extra !== 'giornata' && extra !== 'PUNTOCASSA') {

                                /*EX FUNZIONE PER BROVE MA SBAGLIATA. DEVE ANDARE PER MISURATORE DI RIFERIMENTO*/
                                /*var array_terminali = new Array();
                                 
                                 filtri += "<br/><strong>TERMINALE:</strong><br/>";
                                 filtri_stampa += "TERMINALE:\n";
                                 
                                 array_terminali.push("'" + comanda.nome_pc + "'");
                                 filtri += "- " + comanda.nome_pc + "<br/>";
                                 filtri_stampa += "- " + comanda.nome_pc + "\n";
                                 
                                 var array_terminali_joined = array_terminali.join(',');
                                 
                                 if (array_terminali_joined.length > 0) {
                                 where_terminali = " AND nome_pc IN (" + array_terminali_joined + ") ";
                                 }*/





                                where_misuratore = " AND misuratore_riferimento = '" + numero_layout + "' ";


                            } else if ($('#selezione_pc_incassi [type="checkbox"]:checked').length > 0) {

                                var array_terminali = new Array();

                                filtri += "<br/><strong>TERMINALI:</strong><br/>";
                                filtri_stampa += "TERMINALI:\n";

                                $('#selezione_pc_incassi [type="checkbox"]:checked').each(function (a, b) {


                                    array_terminali.push("'" + $(b).val() + "'");
                                    filtri += "- " + $(b).val() + "<br/>";
                                    filtri_stampa += "- " + $(b).val() + "\n";

                                });

                                var array_terminali_joined = array_terminali.join(',');


                                if (array_terminali_joined.length > 0) {
                                    where_terminali = " AND nome_pc IN (" + array_terminali_joined + ") ";
                                }
                            }
                        }


                        var yY = array_data_partenza[0].substr(6, 4);
                        var yD = array_data_partenza[0].substr(0, 2);
                        var yM = array_data_partenza[0].substr(3, 2);

                        var orario_partenza = $('#selettore_ora_partenza_grafico').val().split(':');

                        if (orario_partenza.length === 2) {
                            var yh = orario_partenza[0]/*"00"*/;
                            var ym = orario_partenza[1]/*"00"*/;
                        } else {
                            var yh = "00";
                            var ym = "00";
                        }

                        var ys = "00";
                        var yms = "000";
                        var data_ieri = yY + '' + yM + '' + yD + '' + yh + '' + ym;


                        var orario_arrivo = $('#selettore_ora_arrivo_grafico').val().split(':');

                        var Y = array_data_arrivo[0].substr(6, 4);
                        var D = array_data_arrivo[0].substr(0, 2);
                        var M = array_data_arrivo[0].substr(3, 2);


                        if (orario_arrivo.length === 2) {
                            var h = orario_arrivo[0]/*"00"*/;
                            var m = orario_arrivo[1]/*"00"*/;

                        } else {
                            var h = "23";
                            var m = "59";

                        }
                        var s = "59";

                        var ms = "000";
                        var data_oggi = Y + '' + M + '' + D + '' + h + '' + m;


                        var check_orario_multigiorno = $('#multigiorno_ora_grafico')[0].checked;

                        /* modificato il 03/07/2020 su segnalazione errori di brovedani. Prima confrontava solo la lunghezza degli array delle date che comunque era sempre 1
                         * in entrambi gli array, perchè anche se non veniva scelto niente venivano valorizzate nel metodo stesso prima di iniziare la statistica
                         */
                        if (array_data_partenza[0] === array_data_arrivo[0]) {
                            if (layout === "soloincassi" && extra !== 'PUNTOCASSA') {

                                var orario_query_com = " (data_servizio='" + yY + "-" + yM + "-" + yD + "') ";
                                var orario_query_tes = " (data_servizio='" + yY + "-" + yM + "-" + yD + "') ";

                            } else if (layout === "soloincassi" && extra === 'PUNTOCASSA') {

                                var orario_query_com = " (data_servizio='" + comanda.data_servizio + "') ";
                                var orario_query_tes = " (data_servizio='" + comanda.data_servizio + "') ";

                            } else if (layout === "soloincassi" && extra === 'giornata') {

                                var orario_query_com = " (data_servizio='" + yY + "-" + yM + "-" + yD + "') ";
                                var orario_query_tes = " (data_servizio='" + yY + "-" + yM + "-" + yD + "') ";

                            } else {

                                if (check_orario_multigiorno === false) {
                                    var orario_query_com = " (data_comanda>='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00') and (data_comanda<='" + Y + "-" + M + "-" + D + "' and ora_comanda <='" + h + ":" + m + ":59') ";
                                    var orario_query_tes = " (data_comanda>='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00') and (data_comanda<='" + Y + "-" + M + "-" + D + "' and ora_comanda <='" + h + ":" + m + ":59') ";
                                } else {
                                    var orario_query_com = " data_comanda>='" + yY + "-" + yM + "-" + yD + "' and data_comanda<='" + Y + "-" + M + "-" + D + "' and ora_comanda >='" + yh + ":" + ym + ":00'  and ora_comanda <='" + h + ":" + m + ":59' ";
                                    var orario_query_tes = " data_comanda>='" + yY + "-" + yM + "-" + yD + "' and data_comanda<='" + Y + "-" + M + "-" + D + "' and ora_comanda >='" + yh + ":" + ym + ":00'  and ora_comanda <='" + h + ":" + m + ":59' ";

                                }
                            }
                        } else {
                            if (layout === "soloincassi" && extra !== 'PUNTOCASSA') {

                                var orario_query_com = " (data_comanda>='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00') and (data_comanda<='" + Y + "-" + M + "-" + D + "' and ora_comanda <='" + h + ":" + m + ":59') ";
                                var orario_query_tes = " (data_comanda>='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00') and (data_comanda<='" + Y + "-" + M + "-" + D + "' and ora_comanda <='" + h + ":" + m + ":59') ";


                            } else if (layout === "soloincassi" && extra === 'PUNTOCASSA') {

                                var orario_query_com = " (data_comanda>='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00') and (data_comanda<='" + Y + "-" + M + "-" + D + "' and ora_comanda <='" + h + ":" + m + ":59') ";
                                var orario_query_tes = " (data_comanda>='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00') and (data_comanda<='" + Y + "-" + M + "-" + D + "' and ora_comanda <='" + h + ":" + m + ":59') ";


                            } else if (layout === "soloincassi" && extra === 'giornata') {

                                var orario_query_com = " (data_comanda>='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00') and (data_comanda<='" + Y + "-" + M + "-" + D + "' and ora_comanda <='" + h + ":" + m + ":59') ";
                                var orario_query_tes = " (data_comanda>='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00') and (data_comanda<='" + Y + "-" + M + "-" + D + "' and ora_comanda <='" + h + ":" + m + ":59') ";


                            } else {

                                if (check_orario_multigiorno === false) {
                                    var orario_query_com = " (data_comanda>='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00') and (data_comanda<='" + Y + "-" + M + "-" + D + "' and ora_comanda <='" + h + ":" + m + ":59') ";
                                    var orario_query_tes = " (data_comanda>='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00') and (data_comanda<='" + Y + "-" + M + "-" + D + "' and ora_comanda <='" + h + ":" + m + ":59') ";
                                } else {
                                    var orario_query_com = " data_comanda>='" + yY + "-" + yM + "-" + yD + "' and data_comanda<='" + Y + "-" + M + "-" + D + "' and ora_comanda >='" + yh + ":" + ym + ":00'  and ora_comanda <='" + h + ":" + m + ":59' ";
                                    var orario_query_tes = " data_comanda>='" + yY + "-" + yM + "-" + yD + "' and data_comanda<='" + Y + "-" + M + "-" + D + "' and ora_comanda >='" + yh + ":" + ym + ":00'  and ora_comanda <='" + h + ":" + m + ":59' ";

                                }
                            }
                        }

                        if (array_data_partenza[0] === array_data_arrivo[0]) {
                            if ($('#selettore_ora_partenza_grafico').val() !== '00:00' || $('#selettore_ora_arrivo_grafico').val() !== '23:59') {
                                var data_report = yD + '/' + yM + '/' + yY + ' - ' + $('#selettore_ora_partenza_grafico').val() + ' - ' + $('#selettore_ora_arrivo_grafico').val();
                            } else {
                                var data_report = yD + '/' + yM + '/' + yY;
                            }
                            filtro_data += data_report + '<br/>';
                        } else {


                            if ($('#selettore_ora_partenza_grafico').val() !== '00:00' || $('#selettore_ora_arrivo_grafico').val() !== '23:59') {
                                var data_report = 'da: ' + yD + '/' + yM + '/' + yY + ' - ' + $('#selettore_ora_partenza_grafico').val() + '\n a: ' + D + '/' + M + '/' + Y + ' - ' + $('#selettore_ora_arrivo_grafico').val();
                                filtro_data += 'da: ' + yD + '/' + yM + '/' + yY + ' alle ' + $('#selettore_ora_partenza_grafico').val() + '<br/>a: ' + D + '/' + M + '/' + Y + ' alle ' + $('#selettore_ora_arrivo_grafico').val() + '<br/>';
                            } else {
                                var data_report = 'da: ' + yD + '/' + yM + '/' + yY + '\n a: ' + D + '/' + M + '/' + Y;
                                filtro_data += 'da: ' + yD + '/' + yM + '/' + yY + '<br/>a: ' + D + '/' + M + '/' + Y + '<br/>';
                            }
                        }
                    } else {

                        var d = new Date();
                        d.setDate(d.getDate() + 1);
                        var Y = d.getFullYear();
                        var M = addZero((d.getMonth() + 1), 2);
                        var D = addZero(d.getDate(), 2);
                        var h = addZero(d.getHours(), 2);
                        var m = addZero(d.getMinutes(), 2);
                        var s = addZero(d.getSeconds(), 2);
                        var ms = addZero(d.getMilliseconds(), 3);
                        var data_oggi = Y + '' + M + '' + D + '' + comanda.ora_servizio + '00';
                        var y = new Date();
                        y.setDate(y.getDate());
                        var yY = y.getFullYear();
                        var yM = addZero((y.getMonth() + 1), 2);
                        var yD = addZero(y.getDate(), 2);
                        var yh = addZero(y.getHours(), 2);
                        var ym = addZero(y.getMinutes(), 2);
                        var ys = addZero(y.getSeconds(), 2);
                        var yms = addZero(y.getMilliseconds(), 3);
                        var data_ieri = yY + '' + yM + '' + yD + '' + comanda.ora_servizio + '00';


                        var check_orario_multigiorno = $('#multigiorno_ora_grafico')[0].checked;

                        if (layout === "soloincassi") {
                            var orario_query_com = " (data_servizio='" + comanda.data_servizio + "') ";
                            var orario_query_tes = " (data_servizio='" + comanda.data_servizio + "') ";
                        } else {

                            if (check_orario_multigiorno === false) {
                                var orario_query_com = " (data_comanda>='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00') and (data_comanda<='" + yY + "-" + yM + "-" + yD + "' and ora_comanda <='" + h + ":" + m + ":59') ";
                                var orario_query_tes = " (data_comanda>='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00') and (data_comanda<='" + yY + "-" + yM + "-" + yD + "' and ora_comanda <='" + h + ":" + m + ":59') ";

                            } else {
                                var orario_query_com = " data_comanda>='" + yY + "-" + yM + "-" + yD + "' and data_comanda<='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00'  and ora_comanda <='" + h + ":" + m + ":59' ";
                                var orario_query_tes = " data_comanda>='" + yY + "-" + yM + "-" + yD + "' and data_comanda<='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00'  and ora_comanda <='" + h + ":" + m + ":59' ";

                            }


                        }
                        var data_report = yD + '/' + yM + '/' + yY;
                        filtri += data_report + '<br/>';
                    }
                    //CICLO DB CENTRALE WHERE STATO RECORD CHIUSO AND POSIZIONE CONTO

                    /*if (layout === "soloincassi") {
                     data_report = comanda.data_servizio;
                     }*/

                    var y = new Date();
                    y.setDate(y.getDate());
                    var yY = y.getFullYear();
                    var yM = addZero((y.getMonth() + 1), 2);
                    var yD = addZero(y.getDate(), 2);
                    var yh = addZero(y.getHours(), 2);
                    var ym = addZero(y.getMinutes(), 2);
                    var ys = addZero(y.getSeconds(), 2);
                    var yms = addZero(y.getMilliseconds(), 3);
                    var data_emissione = yY + '' + yM + '' + yD + '' + yh + '' + ym + '' + ys;



                    var data = yD + '/' + yM + '/' + yY;
                    var ora = yh + ':' + ym + ':' + ys;


                    var data_estrazione_servizio = comanda.data_servizio.substr(0, 4) + "" + comanda.data_servizio.substr(5, 2) + "" + comanda.data_servizio.substr(8, 4);

                    leggi_matricola(function (numero_matricola) {


                        //var query_ultima_chiusura = "select * from chiusure_fiscali where fiscale_sn='n' order by data_emissione desc, progressivo_riga desc limit 1;";
                        //comanda.sincro.query_incassi(query_ultima_chiusura, 1000000, function (result_ultima_chiusura) {


                        //ATTENZIONE: NON PUOI FARE LE DIFFERENZE SE DURANTE IL GIORNO CAMBI I SETTAGGI DELLA STAMPA INCASSI TOTALI
                        // var query_ultima_chiusura = "select sum(importo) as totale_importo,sum(quantita) as totale_quantita, * from chiusure_fiscali where substr(data_emissione,1,8)='" + data_emissione.substr(0, 8) + "' and fiscale_sn='n' and nome_pc='" + comanda.nome_pc + "' group by descrizione,progressivo_riga order by protocollo asc,progressivo_riga asc;";

                        var data_servizio_compatibile = comanda.data_servizio.substr(8, 2) + "/" + comanda.data_servizio.substr(5, 2) + "/" + comanda.data_servizio.substr(0, 4);

                        var query_ultima_chiusura = "select sum(importo) as totale_importo,sum(quantita) as totale_quantita, * from chiusure_fiscali where data_servizio='" + data_servizio_compatibile + "' and fiscale_sn='n' and nome_pc='" + comanda.nome_pc + "' group by descrizione,progressivo_riga order by protocollo asc,progressivo_riga asc;";


                        comanda.sincro.query_centrale(query_ultima_chiusura, function (dati_ultima_chiusura) {


                            var protocollo = '000000000001';
                            var protocollo_se_esistente = '000000000001';

                            var query_ultimo_protocollo = "select protocollo from chiusure_fiscali where data_servizio='" + data_servizio_compatibile + "' and fiscale_sn='n' and nome_pc='" + comanda.nome_pc + "' order by protocollo desc limit 1;";

                            comanda.sincro.query_centrale(query_ultimo_protocollo, function (dati_ultimo_protocollo) {

                                if (dati_ultimo_protocollo[0] !== undefined && dati_ultimo_protocollo[0].protocollo !== undefined) {
                                    protocollo_se_esistente = dati_ultimo_protocollo[0].protocollo;
                                    protocollo = aggZero(parseInt(dati_ultimo_protocollo[0].protocollo) + 1, 12);
                                }

                                var obj_dati_ultima_chiusura = new Object();

                                var ultimo_record_titolo = '';

                                dati_ultima_chiusura.forEach(function (el) {

                                    switch (el.tipo_record) {
                                        case '1':
                                            ultimo_record_titolo = el.descrizione;
                                            obj_dati_ultima_chiusura[el.descrizione] = new Object();

                                            break;
                                        case '2':
                                            ultimo_record_sottotitolo = el.descrizione;
                                            obj_dati_ultima_chiusura[ultimo_record_titolo][el.descrizione] = el;
                                            break;

                                    }

                                });

                                if (dati_ultima_chiusura.length === 0 || extra === 'giornata' || extra === 'PUNTOCASSA') {
                                    obj_dati_ultima_chiusura = false;
                                }


                                //var query_articoli = "select numero, " + comanda.lingua_stampa + " from nomi_portate order by numero asc;";
                                //comanda.sincro.query_incassi(query_articoli, 1000000, function (result_portate) {


                                var query_articoli = "select id, descrizione from categorie order by id asc;";

                                comanda.sincro.query_centrale(query_articoli, function (result_categorie) {



                                    var nomi_categorie = new Object();
                                    for (var a in result_categorie) {
                                        nomi_categorie[result_categorie[a].id] = result_categorie[a].descrizione;
                                    }

                                    var query_articoli = "select id, nome from gruppi_statistici order by id asc;";
                                    comanda.sincro.query_cassa(query_articoli, 1000000, function (result_gruppi) {


                                        var nomi_gruppi = new Object();
                                        for (var a in result_gruppi) {
                                            nomi_gruppi[result_gruppi[a].id] = result_gruppi[a].nome;
                                        }

                                        /* var prezzo_vero = " importo_totale_fiscale as ";
                                         var prezzo_vero_2 = " importo_totale_fiscale ";
                                         
                                         if (comanda.pizzeria_asporto === true) {
                                         prezzo_vero = " (QTA+importo_totale_fiscale) as ";
                                         prezzo_vero_2 = " (QTA+importo_totale_fiscale)  ";
                                         }*/

                                        /* modificato il 26/04/2021 altrimenti non fa i totali sulla stampa del dettaglio venduti. prima c'era sum prima di ogni inizio di parentesi tonda */
                                        /* attenzione. potrebbe comportare problemi su statistiche di grossi dati */
                                        var prezzo_vero = " (importo_totale_fiscale) as ";
                                        var prezzo_vero_2 = " (importo_totale_fiscale) ";

                                        if (comanda.pizzeria_asporto === true) {
                                            prezzo_vero = " (quantita*importo_totale_fiscale) as ";
                                            prezzo_vero_2 = " ((quantita*QTA)+importo_totale_fiscale)  ";
                                        }



                                        var query_articoli = "update comanda set sconto_perc='0.00' where sconto_perc is null;";
                                        comanda.sincro.query_cassa(query_articoli, 1000000, function () {

                                            var query_articoli = "";

                                            if (comanda.licenza_vera === 'BrovedaniVerona') {

                                                query_articoli += "SELECT gruppo," + prezzo_vero_2 + " as valore_sconto, sum(quantita) as quantita_totale, sum(netto) as totale_sconti FROM comanda WHERE  posizione='SCONTO' " + where_misuratore + where_terminali + where_categorie + where_gruppi + where_giorni + " and fiscalizzata_sn='S' and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and tipo_ricevuta!='' and " + orario_query_com + " group by " + prezzo_vero_2 + " UNION ALL ";
                                                query_articoli += "SELECT gruppo," + prezzo_vero_2 + " as valore_sconto, sum(quantita) as quantita_totale, sum(netto) as totale_sconti FROM comanda_temp WHERE  posizione='SCONTO' " + where_misuratore + where_terminali + where_categorie + where_gruppi + where_giorni + " and fiscalizzata_sn='S' and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and tipo_ricevuta!='' and " + orario_query_com + " group by " + prezzo_vero_2 + ";";
                                            } else {

                                                query_articoli += "SELECT gruppo," + prezzo_vero_2 + " as valore_sconto, sum(quantita) as quantita_totale, sum(netto) as totale_sconti FROM comanda WHERE  posizione='SCONTO' " + where_misuratore + where_terminali + where_categorie + where_gruppi + where_giorni + " /*and fiscalizzata_sn='S'*/ and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and tipo_ricevuta!='' and " + orario_query_com + " group by " + prezzo_vero_2 + " UNION ALL ";
                                                query_articoli += "SELECT gruppo," + prezzo_vero_2 + " as valore_sconto, sum(quantita) as quantita_totale, sum(netto) as totale_sconti FROM comanda_temp WHERE  posizione='SCONTO' " + where_misuratore + where_terminali + where_categorie + where_gruppi + where_giorni + " /*and fiscalizzata_sn='S'*/ and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and tipo_ricevuta!='' and " + orario_query_com + " group by " + prezzo_vero_2 + ";";
                                            }


                                            if (comanda.array_db_centri.length > 0) {

                                                var query_articoli = "";
                                                var i = 1;

                                                comanda.array_db_centri.forEach(centro => {

                                                    if (comanda.licenza_vera === 'BrovedaniVerona') {

                                                        query_articoli += "SELECT gruppo," + prezzo_vero_2 + " as valore_sconto, sum(quantita) as quantita_totale, sum(netto) as totale_sconti FROM " + centro + ".comanda WHERE  posizione='SCONTO' " + where_misuratore + where_terminali + where_categorie + where_gruppi + where_giorni + " and fiscalizzata_sn='S' and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and tipo_ricevuta!='' and " + orario_query_com + " group by " + prezzo_vero_2 + " UNION ALL ";
                                                        query_articoli += "SELECT gruppo," + prezzo_vero_2 + " as valore_sconto, sum(quantita) as quantita_totale, sum(netto) as totale_sconti FROM " + centro + ".comanda_temp WHERE  posizione='SCONTO' " + where_misuratore + where_terminali + where_categorie + where_gruppi + where_giorni + " and fiscalizzata_sn='S' and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and tipo_ricevuta!='' and " + orario_query_com + " group by " + prezzo_vero_2 + " order by " + prezzo_vero_2 + " desc ";
                                                    } else {

                                                        query_articoli += "SELECT gruppo," + prezzo_vero_2 + " as valore_sconto, sum(quantita) as quantita_totale, sum(netto) as totale_sconti FROM " + centro + ".comanda WHERE  posizione='SCONTO' " + where_misuratore + where_terminali + where_categorie + where_gruppi + where_giorni + " /*and fiscalizzata_sn='S'*/ and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and tipo_ricevuta!='' and " + orario_query_com + " group by " + prezzo_vero_2 + " UNION ALL ";
                                                        query_articoli += "SELECT gruppo," + prezzo_vero_2 + " as valore_sconto, sum(quantita) as quantita_totale, sum(netto) as totale_sconti FROM " + centro + ".comanda_temp WHERE  posizione='SCONTO' " + where_misuratore + where_terminali + where_categorie + where_gruppi + where_giorni + " /*and fiscalizzata_sn='S'*/ and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and tipo_ricevuta!='' and " + orario_query_com + " group by " + prezzo_vero_2 + " order by " + prezzo_vero_2 + " desc ";
                                                    }

                                                    if (i !== comanda.array_db_centri.length) {
                                                        query_articoli += " UNION ALL ";
                                                    }

                                                    i++;

                                                });
                                            }

                                            comanda.sincro.query_incassi(query_articoli, 1000000, function (result_sconti) {

                                                var query_articoli_zero = "";
                                                if (comanda.escludi_art_zero_statistiche === true) {
                                                    query_articoli_zero = " and " + prezzo_vero_2 + "!=0 ";
                                                }

                                                var ordinamento = "descrizione";
                                                //SE E' NEVODI ORDINA PER COD.ARTICOLO
                                                if (comanda.numero_licenza_cliente === "0568") {
                                                    ordinamento = "cod_articolo";
                                                }

                                                if (ordinamento === "cod_articolo") {
                                                    prodotti = new Object();
                                                    alasql.tables.prodotti.data.forEach(function (prodotto) {
                                                        prodotti[prodotto.id] = new Object();
                                                        prodotti[prodotto.id] = prodotto.descrizione;
                                                    });
                                                }

                                                var query_articoli2 = "";
                                                if (comanda.licenza_vera === 'BrovedaniVerona') {

                                                    query_articoli2 += "select id,QTA,gruppo,fiscalizzata_sn,tipo_ricevuta,n_fiscale,operatore,ntav_comanda,quantita,desc_art,sconto_perc,perc_iva,categoria,dest_stampa,portata," + prezzo_vero + "prezzo_un,tipo_consegna,cod_articolo from comanda where (posizione='CONTO' OR posizione='COPERTO') " + where_misuratore + where_terminali + where_categorie + where_gruppi + where_giorni + " and fiscalizzata_sn='S' and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and tipo_ricevuta!='' and " + orario_query_com + " and portata!='ZZZ'  and cod_promo!=1  " + query_articoli_zero + " UNION ALL ";
                                                    query_articoli2 += "select id,gruppo,QTA,fiscalizzata_sn,tipo_ricevuta,n_fiscale,operatore,ntav_comanda,quantita,desc_art,sconto_perc,perc_iva,categoria,dest_stampa,portata," + prezzo_vero + "prezzo_un,tipo_consegna,cod_articolo from comanda_temp where (posizione='CONTO' OR posizione='COPERTO') " + where_misuratore + where_terminali + where_categorie + where_gruppi + where_giorni + " and fiscalizzata_sn='S' and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and tipo_ricevuta!='' and " + orario_query_com + " and portata!='ZZZ'  and cod_promo!=1  " + query_articoli_zero + ";";
                                                } else {

                                                    query_articoli2 += "select id,gruppo,QTA,fiscalizzata_sn,tipo_ricevuta,n_fiscale,operatore,ntav_comanda,quantita,desc_art,sconto_perc,perc_iva,categoria,dest_stampa,portata," + prezzo_vero + "prezzo_un,tipo_consegna,cod_articolo from comanda where (posizione='CONTO' OR posizione='COPERTO') " + where_misuratore + where_terminali + where_categorie + where_gruppi + where_giorni + " and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and tipo_ricevuta!='' and " + orario_query_com + " and portata!='ZZZ'  and cod_promo!=1  " + query_articoli_zero + " UNION ALL ";
                                                    query_articoli2 += "select id,gruppo,QTA,fiscalizzata_sn,tipo_ricevuta,n_fiscale,operatore,ntav_comanda,quantita,desc_art,sconto_perc,perc_iva,categoria,dest_stampa,portata," + prezzo_vero + "prezzo_un,tipo_consegna,cod_articolo from comanda_temp where (posizione='CONTO' OR posizione='COPERTO') " + where_misuratore + where_terminali + where_categorie + where_gruppi + where_giorni + " and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and tipo_ricevuta!='' and " + orario_query_com + " and portata!='ZZZ'  and cod_promo!=1  " + query_articoli_zero + ";";
                                                }
                                                console.log("STAMPA TOTALI QUERY ARTICOLI", query_articoli2);


                                                if (comanda.array_db_centri.length > 0) {

                                                    var query_articoli2 = "";
                                                    var i = 1;

                                                    comanda.array_db_centri.forEach(centro => {

                                                        if (comanda.licenza_vera === 'BrovedaniVerona') {

                                                            query_articoli2 += "select id,QTA,gruppo,fiscalizzata_sn,tipo_ricevuta,n_fiscale,operatore,ntav_comanda,quantita,desc_art,sconto_perc,perc_iva,categoria,dest_stampa,portata," + prezzo_vero + "prezzo_un,tipo_consegna,cod_articolo from " + centro + ".comanda where (posizione='CONTO' OR posizione='COPERTO') " + where_misuratore + where_terminali + where_categorie + where_gruppi + where_giorni + " and fiscalizzata_sn='S' and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and tipo_ricevuta!='' and " + orario_query_com + " and portata!='ZZZ'  and cod_promo!=1  " + query_articoli_zero + " UNION ALL ";
                                                            query_articoli2 += "select id,gruppo,QTA,fiscalizzata_sn,tipo_ricevuta,n_fiscale,operatore,ntav_comanda,quantita,desc_art,sconto_perc,perc_iva,categoria,dest_stampa,portata," + prezzo_vero + "prezzo_un,tipo_consegna,cod_articolo from " + centro + ".comanda_temp where (posizione='CONTO' OR posizione='COPERTO') " + where_misuratore + where_terminali + where_categorie + where_gruppi + where_giorni + " and fiscalizzata_sn='S' and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and tipo_ricevuta!='' and " + orario_query_com + " and portata!='ZZZ'  and cod_promo!=1  " + query_articoli_zero + " ";
                                                        } else {

                                                            query_articoli2 += "select id,gruppo,QTA,fiscalizzata_sn,tipo_ricevuta,n_fiscale,operatore,ntav_comanda,quantita,desc_art,sconto_perc,perc_iva,categoria,dest_stampa,portata," + prezzo_vero + "prezzo_un,tipo_consegna,cod_articolo from " + centro + ".comanda where (posizione='CONTO' OR posizione='COPERTO') " + where_misuratore + where_terminali + where_categorie + where_gruppi + where_giorni + " and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and tipo_ricevuta!='' and " + orario_query_com + " and portata!='ZZZ'  and cod_promo!=1  " + query_articoli_zero + " UNION ALL ";
                                                            query_articoli2 += "select id,gruppo,QTA,fiscalizzata_sn,tipo_ricevuta,n_fiscale,operatore,ntav_comanda,quantita,desc_art,sconto_perc,perc_iva,categoria,dest_stampa,portata," + prezzo_vero + "prezzo_un,tipo_consegna,cod_articolo from " + centro + ".comanda_temp where (posizione='CONTO' OR posizione='COPERTO') " + where_misuratore + where_terminali + where_categorie + where_gruppi + where_giorni + " and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and tipo_ricevuta!='' and " + orario_query_com + " and portata!='ZZZ'  and cod_promo!=1  " + query_articoli_zero + " ";
                                                        }

                                                        if (i !== comanda.array_db_centri.length) {
                                                            query_articoli2 += " UNION ALL ";
                                                        }

                                                        i++;

                                                    });
                                                }

                                                comanda.sincro.query_incassi(query_articoli2, 1000000, function (result) {



                                                    var query_teste = "select * from record_teste where " + orario_query_tes + " " + where_misuratore + where_terminali + " " + where_giorni + "  UNION ALL ";
                                                    query_teste += "select * from record_teste_temp where " + orario_query_tes + " " + where_misuratore + where_terminali + " " + where_giorni + ";";

                                                    if (comanda.array_db_centri.length > 0) {

                                                        var query_teste = "";
                                                        var i = 1;

                                                        comanda.array_db_centri.forEach(centro => {

                                                            query_teste += "select * from " + centro + ".record_teste where " + orario_query_tes + " " + where_misuratore + where_terminali + " " + where_giorni + "  UNION ALL ";
                                                            query_teste += "select * from " + centro + ".record_teste_temp where " + orario_query_tes + " " + where_misuratore + where_terminali + " " + where_giorni + " ";

                                                            if (i !== comanda.array_db_centri.length) {
                                                                query_teste += " UNION ALL ";
                                                            }

                                                            i++;

                                                        });
                                                    }


                                                    let differenze_totale_incassi = "Differenze teste / venduti<br><br>";
                                                    let differenza_totale_incassi_venduti = 0;

                                                    comanda.sincro.query_incassi(query_teste, 1000000, function (result_teste) {

                                                        for (var key in result_teste) {
                                                            var el = result_teste[key];

                                                            if (isNaN(parseFloat(el.resto))) {
                                                                el.resto = "0";
                                                            }

                                                            if (isNaN(parseFloat(el.contanti))) {
                                                                el.contanti = "0";
                                                            }

                                                            if (isNaN(parseFloat(el.bancomat))) {
                                                                el.bancomat = "0";
                                                            }

                                                            if (isNaN(parseFloat(el.carte_credito))) {
                                                                el.carte_credito = "0";
                                                            }

                                                            if (isNaN(parseFloat(el.carte_credito2))) {
                                                                el.carte_credito2 = "0";
                                                            }

                                                            if (isNaN(parseFloat(el.carte_credito3))) {
                                                                el.carte_credito3 = "0";
                                                            }

                                                            if (isNaN(parseFloat(el.buoni_pasto))) {
                                                                el.buoni_pasto = "0";
                                                            }

                                                            if (isNaN(parseFloat(el.numero_buoni_pasto))) {
                                                                el.numero_buoni_pasto = "0";
                                                            }

                                                            if (isNaN(parseFloat(el.buoni_pasto_2))) {
                                                                el.buoni_pasto_2 = "0";
                                                            }

                                                            if (isNaN(parseFloat(el.numero_buoni_pasto_2))) {
                                                                el.numero_buoni_pasto_2 = "0";
                                                            }

                                                            if (isNaN(parseFloat(el.buoni_pasto_3))) {
                                                                el.buoni_pasto_3 = "0";
                                                            }

                                                            if (isNaN(parseFloat(el.numero_buoni_pasto_3))) {
                                                                el.numero_buoni_pasto_3 = "0";
                                                            }

                                                            if (isNaN(parseFloat(el.buoni_pasto_4))) {
                                                                el.buoni_pasto_4 = "0";
                                                            }

                                                            if (isNaN(parseFloat(el.numero_buoni_pasto_4))) {
                                                                el.numero_buoni_pasto_4 = "0";
                                                            }

                                                            if (isNaN(parseFloat(el.buoni_pasto_5))) {
                                                                el.buoni_pasto_5 = "0";
                                                            }

                                                            if (isNaN(parseFloat(el.numero_buoni_pasto_5))) {
                                                                el.numero_buoni_pasto_5 = "0";
                                                            }

                                                            if (isNaN(parseFloat(el.buoni_acquisto))) {
                                                                el.buoni_acquisto = "0";
                                                            }

                                                            if (isNaN(parseFloat(el.non_pagato))) {
                                                                el.non_pagato = "0";
                                                            }

                                                            if (isNaN(parseFloat(el.buoni_acquisto_emessi))) {
                                                                el.buoni_acquisto_emessi = "0";
                                                            }

                                                            if (per_tavolo_brove[el.tavolo] === undefined) {
                                                                per_tavolo_brove[el.tavolo] = new Object();
                                                                per_tavolo_brove[el.tavolo].numero = 1;
                                                                per_tavolo_brove[el.tavolo].importo = parseFloat(el.contanti) - parseFloat(el.resto) + parseFloat(el.bancomat) + parseFloat(el.carte_credito) + parseFloat(el.carte_credito2) + parseFloat(el.carte_credito3) + (parseFloat(el.buoni_pasto) * parseFloat(el.numero_buoni_pasto)) + (parseFloat(el.buoni_pasto_2) * parseFloat(el.numero_buoni_pasto_2)) + (parseFloat(el.buoni_pasto_3) * parseFloat(el.numero_buoni_pasto_3)) + (parseFloat(el.buoni_pasto_4) * parseFloat(el.numero_buoni_pasto_4)) + (parseFloat(el.buoni_pasto_5) * parseFloat(el.numero_buoni_pasto_5)) + parseFloat(el.buoni_acquisto) + parseFloat(el.non_pagato) + parseFloat(el.non_riscosso_servizi) + parseFloat(el.sconto_a_pagare) - parseFloat(el.arrotondamento);
                                                            } else {
                                                                per_tavolo_brove[el.tavolo].numero++;
                                                                per_tavolo_brove[el.tavolo].importo += parseFloat(el.contanti) - parseFloat(el.resto) + parseFloat(el.bancomat) + parseFloat(el.carte_credito) + parseFloat(el.carte_credito2) + parseFloat(el.carte_credito3) + (parseFloat(el.buoni_pasto) * parseFloat(el.numero_buoni_pasto)) + (parseFloat(el.buoni_pasto_2) * parseFloat(el.numero_buoni_pasto_2)) + (parseFloat(el.buoni_pasto_3) * parseFloat(el.numero_buoni_pasto_3)) + (parseFloat(el.buoni_pasto_4) * parseFloat(el.numero_buoni_pasto_4)) + (parseFloat(el.buoni_pasto_5) * parseFloat(el.numero_buoni_pasto_5)) + parseFloat(el.buoni_acquisto) + parseFloat(el.non_pagato) + parseFloat(el.non_riscosso_servizi) + parseFloat(el.sconto_a_pagare) - parseFloat(el.arrotondamento);
                                                            }

                                                            switch (el.tipologia) {
                                                                case 'SCONTRINO':
                                                                    fisco_teste['SCONTRINI'].numero++;
                                                                    fisco_teste['SCONTRINI'].importo += parseFloat(el.contanti) - parseFloat(el.resto) + parseFloat(el.bancomat) + parseFloat(el.carte_credito) + parseFloat(el.carte_credito2) + parseFloat(el.carte_credito3) + (parseFloat(el.buoni_pasto) * parseFloat(el.numero_buoni_pasto)) + (parseFloat(el.buoni_pasto_2) * parseFloat(el.numero_buoni_pasto_2)) + (parseFloat(el.buoni_pasto_3) * parseFloat(el.numero_buoni_pasto_3)) + (parseFloat(el.buoni_pasto_4) * parseFloat(el.numero_buoni_pasto_4)) + (parseFloat(el.buoni_pasto_5) * parseFloat(el.numero_buoni_pasto_5)) + parseFloat(el.buoni_acquisto) + parseFloat(el.non_pagato) + parseFloat(el.non_riscosso_servizi) + parseFloat(el.sconto_a_pagare) - parseFloat(el.arrotondamento);
                                                                    break;
                                                                case 'FATTURA':
                                                                    fisco_teste['FATTURE'].numero++;
                                                                    fisco_teste['FATTURE'].importo += parseFloat(el.contanti) - parseFloat(el.resto) + parseFloat(el.bancomat) + parseFloat(el.carte_credito) + parseFloat(el.carte_credito2) + parseFloat(el.carte_credito3) + (parseFloat(el.buoni_pasto) * parseFloat(el.numero_buoni_pasto)) + (parseFloat(el.buoni_pasto_2) * parseFloat(el.numero_buoni_pasto_2)) + (parseFloat(el.buoni_pasto_3) * parseFloat(el.numero_buoni_pasto_3)) + (parseFloat(el.buoni_pasto_4) * parseFloat(el.numero_buoni_pasto_4)) + (parseFloat(el.buoni_pasto_5) * parseFloat(el.numero_buoni_pasto_5)) + parseFloat(el.buoni_acquisto) + parseFloat(el.non_pagato) + parseFloat(el.non_riscosso_servizi) + parseFloat(el.sconto_a_pagare) - parseFloat(el.arrotondamento);
                                                                    break;
                                                                case 'QUITTUNG':
                                                                    fisco_teste['QUITTUNG'].numero++;
                                                                    fisco_teste['QUITTUNG'].importo += parseFloat(el.contanti) - parseFloat(el.resto) + parseFloat(el.bancomat) + parseFloat(el.carte_credito) + parseFloat(el.carte_credito2) + parseFloat(el.carte_credito3) + (parseFloat(el.buoni_pasto) * parseFloat(el.numero_buoni_pasto)) + (parseFloat(el.buoni_pasto_2) * parseFloat(el.numero_buoni_pasto_2)) + (parseFloat(el.buoni_pasto_3) * parseFloat(el.numero_buoni_pasto_3)) + (parseFloat(el.buoni_pasto_4) * parseFloat(el.numero_buoni_pasto_4)) + (parseFloat(el.buoni_pasto_5) * parseFloat(el.numero_buoni_pasto_5)) + parseFloat(el.buoni_acquisto) + parseFloat(el.non_pagato) + parseFloat(el.non_riscosso_servizi) + parseFloat(el.sconto_a_pagare) - parseFloat(el.arrotondamento);
                                                                    break;
                                                                case 'RECHNUNG':
                                                                    fisco_teste['RECHNUNG'].numero++;
                                                                    fisco_teste['RECHNUNG'].importo += parseFloat(el.contanti) - parseFloat(el.resto) + parseFloat(el.bancomat) + parseFloat(el.carte_credito) + parseFloat(el.carte_credito2) + parseFloat(el.carte_credito3) + (parseFloat(el.buoni_pasto) * parseFloat(el.numero_buoni_pasto)) + (parseFloat(el.buoni_pasto_2) * parseFloat(el.numero_buoni_pasto_2)) + (parseFloat(el.buoni_pasto_3) * parseFloat(el.numero_buoni_pasto_3)) + (parseFloat(el.buoni_pasto_4) * parseFloat(el.numero_buoni_pasto_4)) + (parseFloat(el.buoni_pasto_5) * parseFloat(el.numero_buoni_pasto_5)) + parseFloat(el.buoni_acquisto) + parseFloat(el.non_pagato) + parseFloat(el.non_riscosso_servizi) + parseFloat(el.sconto_a_pagare) - parseFloat(el.arrotondamento);
                                                                    break;
                                                            }

                                                            totale_contanti += parseFloat(el.contanti) - parseFloat(el.resto);
                                                            totale_bancomat += parseFloat(el.bancomat);
                                                            totale_cartecredito += parseFloat(el.carte_credito);
                                                            totale_cartecredito += parseFloat(el.carte_credito2);
                                                            totale_cartecredito += parseFloat(el.carte_credito3);
                                                            totale_buonipasto += (parseFloat(el.buoni_pasto) * parseFloat(el.numero_buoni_pasto)) + (parseFloat(el.buoni_pasto_2) * parseFloat(el.numero_buoni_pasto_2)) + (parseFloat(el.buoni_pasto_3) * parseFloat(el.numero_buoni_pasto_3)) + (parseFloat(el.buoni_pasto_4) * parseFloat(el.numero_buoni_pasto_4)) + (parseFloat(el.buoni_pasto_5) * parseFloat(el.numero_buoni_pasto_5));
                                                            totale_buoniacquisto += parseFloat(el.buoni_acquisto);
                                                            totale_buoniacquisto_emessi += parseFloat(el.buoni_acquisto_emessi);

                                                            totale_totale += parseFloat(el.contanti) - parseFloat(el.resto) + parseFloat(el.bancomat) + parseFloat(el.carte_credito) + parseFloat(el.carte_credito2) + parseFloat(el.carte_credito3) + (parseFloat(el.buoni_pasto) * parseFloat(el.numero_buoni_pasto)) + (parseFloat(el.buoni_pasto_2) * parseFloat(el.numero_buoni_pasto_2)) + (parseFloat(el.buoni_pasto_3) * parseFloat(el.numero_buoni_pasto_3)) + (parseFloat(el.buoni_pasto_4) * parseFloat(el.numero_buoni_pasto_4)) + (parseFloat(el.buoni_pasto_5) * parseFloat(el.numero_buoni_pasto_5)) + parseFloat(el.buoni_acquisto) + parseFloat(el.non_pagato) + parseFloat(el.non_riscosso_servizi) + parseFloat(el.sconto_a_pagare) - parseFloat(el.arrotondamento);

                                                            totale_incassato += parseFloat(el.contanti) - parseFloat(el.resto) + parseFloat(el.bancomat) + parseFloat(el.carte_credito) + parseFloat(el.carte_credito2) + parseFloat(el.carte_credito3);

                                                            totale_non_pagato += parseFloat(el.non_pagato);

                                                            totale_non_riscosso_servizi += parseFloat(el.non_riscosso_servizi);

                                                            totale_sconto_a_pagare += parseFloat(el.sconto_a_pagare);

                                                            totale_arrotondamento += parseFloat(el.arrotondamento);


                                                            /* PER DEBUG. SEMPRE UTILE */

                                                            try {
                                                                var test = parseFloat(el.contanti) - parseFloat(el.resto) + parseFloat(el.bancomat) + parseFloat(el.carte_credito) + parseFloat(el.carte_credito2) + parseFloat(el.carte_credito3) + (parseFloat(el.buoni_pasto) * parseFloat(el.numero_buoni_pasto)) + (parseFloat(el.buoni_pasto_2) * parseFloat(el.numero_buoni_pasto_2)) + (parseFloat(el.buoni_pasto_3) * parseFloat(el.numero_buoni_pasto_3)) + (parseFloat(el.buoni_pasto_4) * parseFloat(el.numero_buoni_pasto_4)) + (parseFloat(el.buoni_pasto_5) * parseFloat(el.numero_buoni_pasto_5)) + parseFloat(el.buoni_acquisto) + parseFloat(el.non_pagato) + parseFloat(el.non_riscosso_servizi) + parseFloat(el.sconto_a_pagare) - parseFloat(el.arrotondamento);
                                                                if (parseFloat(el.totale).toFixed(2) != test.toFixed(2)) {
                                                                    differenze_totale_incassi += "id: " + el.id + " Incasso: " + parseFloat(el.totale).toFixed(2) + " Totale: " + test.toFixed(2) + "<br>";
                                                                    differenza_totale_incassi_venduti += parseFloat(el.totale) - test;
                                                                }
                                                            } catch (e) {

                                                            }


                                                        }

                                                        differenze_totale_incassi += "Differenza totale: " + differenza_totale_incassi_venduti.toFixed(2);

                                                        bootbox.alert(differenze_totale_incassi);





                                                        console.log("STAMPA TOTALI CALLBACK");
                                                        var i = 0;
                                                        for (var index in result) {

                                                            //INIZIALIZZO L'OGGETTO OGNI VOLTA
                                                            var obj = result[index];
                                                            if (set_disabilita_raggruppamento_portata === true) {
                                                                obj.portata = "default";
                                                            }

                                                            if (set_disabilita_raggruppamento_destinazione === true) {
                                                                obj.dest_stampa = "default";
                                                            }

                                                            if (set_disabilita_raggruppamento_categoria === true) {
                                                                obj.categoria = "default";
                                                            }

                                                            if (obj.prezzo_un === undefined || obj.prezzo_un === null || isNaN(obj.prezzo_un)) {
                                                                obj.prezzo_un = 0;
                                                            }

                                                            if (obj.quantita === undefined || obj.quantita === null || isNaN(obj.quantita)) {
                                                                obj.quantita = 0;
                                                            }


                                                            if (obj.sconto_perc === undefined || obj.sconto_perc === null || isNaN(obj.sconto_perc) || obj.sconto_perc === "") {
                                                                obj.sconto_perc = 0;
                                                            }

                                                            if (obj.QTA === undefined || obj.QTA === null || isNaN(obj.QTA)) {
                                                                obj.QTA = 0;
                                                            }

                                                            if (obj.desc_art === "COPERTI") {
                                                                numero_coperti += parseFloat(obj.quantita);
                                                            }

                                                            if (comanda.pizzeria_asporto === true) {
                                                                if (parseFloat(obj.QTA) > 0) {
                                                                    if (obj.tipo_consegna === "DOMICILIO") {
                                                                        if (id_consegne.find(o => o.id === obj.id) === undefined) {
                                                                            id_consegne.push({ id: obj.id });
                                                                            totale_qta_consegne++;
                                                                        }
                                                                        if (consegna_metro === true) {
                                                                            if (obj.desc_art.indexOf("MAXI") !== -1) {
                                                                                totale_consegne['MAXI'].importo += parseFloat(obj.QTA);
                                                                                totale_consegne['MAXI'].qta++;
                                                                            } else if (obj.desc_art.indexOf("MEZZO METRO") !== -1) {
                                                                                totale_consegne['MEZZO'].importo += parseFloat(obj.QTA);
                                                                                totale_consegne['MEZZO'].qta++;
                                                                            } else if (obj.desc_art.indexOf("UN METRO") !== -1) {
                                                                                totale_consegne['METRO'].importo += parseFloat(obj.QTA);
                                                                                totale_consegne['METRO'].qta++;
                                                                            } else {
                                                                                totale_consegne['NORMALI'].importo += parseFloat(obj.QTA);
                                                                                totale_consegne['NORMALI'].qta++;
                                                                            }
                                                                        } else {
                                                                            totale_consegne['PIZZE'].importo += parseFloat(obj.QTA);
                                                                            totale_consegne['PIZZE'].qta++;
                                                                        }

                                                                    }
                                                                }
                                                            }


                                                            //-------------CONTABILE-----------

                                                            console.log("STAMPA TOTALI IN CICLO", obj);

                                                            var ordinamento = "descrizione";
                                                            //SE E' NEVODI ORDINA PER COD.ARTICOLO
                                                            if (comanda.numero_licenza_cliente === "0568") {
                                                                ordinamento = "cod_articolo";
                                                            }

                                                            if (ordinamento === "cod_articolo" && prodotti[obj.cod_articolo] !== undefined) {
                                                                obj.desc_art = prodotti[obj.cod_articolo];
                                                            }
                                                            //ARTICOLO GENERICO
                                                            articolo_generico[i] = { 'id': obj.id, 'gruppo': obj.gruppo, 'totale_consegna': obj.QTA, 'destinazione': obj.dest_stampa, 'categoria': obj.categoria, 'portata': obj.portata, 'descrizione': obj.desc_art, 'quantita': obj.quantita, 'prezzo_un': obj.prezzo_un, sconto_perc: obj.sconto_perc, 'posizione': obj.posizione, 'cod_articolo': obj.cod_articolo };

                                                            i++;



                                                            //-----DICHIARAZIONE OGGETTI ARTICOLO-----

                                                            //SE DISABILITA RAGGRUPPAMENTO PORTATA
                                                            //PORTATA = DEFAULT
                                                            //SE ARTICOLO[IVA][PORTATA] UNDEFINED
                                                            /*if (articolo[obj.portata] === undefined) {
                                                             articolo[obj.portata] = new Object();
                                                             }
                                                             
                                                             //SE ARTICOLO[IVA][PORTATA][DEST_STAMPA] UNDEFINED
                                                             if (articolo[obj.portata][obj.dest_stampa] === undefined) {
                                                             articolo[obj.portata][obj.dest_stampa] = new Object();
                                                             }
                                                             
                                                             ////DISABILITA RAGGRUPPAMENTO CATEGORIA
                                                             //CATEGORIA = DEFAULT
                                                             //SE ARTICOLO[IVA][PORTATA][DEST_STAMPA][CATEGORIA] UNDEFINED
                                                             if (articolo[obj.portata][obj.dest_stampa][obj.categoria] === undefined) {
                                                             articolo[obj.portata][obj.dest_stampa][obj.categoria] = new Object();
                                                             }
                                                             
                                                             //RAGGRUPPAMENTO SCONTO PERCENTUALE
                                                             if (articolo[obj.portata][obj.dest_stampa][obj.categoria][obj.sconto_perc] === undefined) {
                                                             articolo[obj.portata][obj.dest_stampa][obj.categoria][obj.sconto_perc] = new Object();
                                                             }
                                                             
                                                             //SE ARTICOLO[IVA][PORTATA][DEST_STAMPA][CATEGORIA][DESCRIZIONE] UNDEFINED
                                                             if (articolo[obj.portata][obj.dest_stampa][obj.categoria][obj.sconto_perc][obj.desc_art] === undefined) {
                                                             articolo[obj.portata][obj.dest_stampa][obj.categoria][obj.sconto_perc][obj.desc_art] = new Object();
                                                             
                                                             articolo[obj.portata][obj.dest_stampa][obj.categoria][obj.sconto_perc][obj.desc_art].quantita = obj.quantita;
                                                             articolo[obj.portata][obj.dest_stampa][obj.categoria][obj.sconto_perc][obj.desc_art].prezzo_tot = (obj.prezzo_un * obj.quantita) / 100 * (100 - obj.sconto_perc);
                                                             } else
                                                             {
                                                             articolo[obj.portata][obj.dest_stampa][obj.categoria][obj.sconto_perc][obj.desc_art].quantita += obj.quantita;
                                                             articolo[obj.portata][obj.dest_stampa][obj.categoria][obj.sconto_perc][obj.desc_art].prezzo_tot += (obj.prezzo_un * obj.quantita) / 100 * (100 - obj.sconto_perc);
                                                             
                                                             }
                                                             
                                                             
                                                             //SE ARTICOLO[IVA][PORTATA][DEST_STAMPA][CATEGORIA][DESCRIZIONE][QT] UNDEFINED
                                                             if (articolo[obj.portata][obj.dest_stampa][obj.categoria][obj.sconto_perc][obj.desc_art][obj.quantita] === undefined) {
                                                             articolo[obj.portata][obj.dest_stampa][obj.categoria][obj.sconto_perc][obj.desc_art][obj.quantita] = new Object();
                                                             }
                                                             
                                                             //SE ARTICOLO[IVA][PORTATA][DEST_STAMPA][CATEGORIA][DESCRIZIONE][QT][PREZZO] UNDEFINED
                                                             if (articolo[obj.portata][obj.dest_stampa][obj.categoria][obj.sconto_perc][obj.desc_art][obj.quantita][obj.prezzo_un] === undefined) {
                                                             
                                                             //THIS.CARATTERISTICHE COME DA DB
                                                             articolo[obj.portata][obj.dest_stampa][obj.categoria][obj.sconto_perc][obj.desc_art][obj.quantita][obj.prezzo_un] = obj;
                                                             } else
                                                             {
                                                             articolo[obj.portata][obj.dest_stampa][obj.categoria][obj.sconto_perc][obj.desc_art][obj.quantita][obj.prezzo_un].desc_art += ' / ' + obj.desc_art;
                                                             articolo[obj.portata][obj.dest_stampa][obj.categoria][obj.sconto_perc][obj.desc_art][obj.quantita][obj.prezzo_un].quantita += obj.quantita;
                                                             }
                                                             
                                                             
                                                             */



                                                            //----FINE OGGETTO-----



                                                            //------FINE DICHIARAZIONE OPERAZIONI_OPERATORE----------
                                                            //***

                                                            //TOTALE += QUANTITA X PREZZO_UN
                                                            prezzo_articolo = parseInt(obj.quantita) * parseFloat(obj.prezzo_un);
                                                            totale += prezzo_articolo;
                                                            //TOTALE SCONTI
                                                            sconto_articolo = (parseInt(obj.quantita) * parseFloat(obj.prezzo_un)) / 100 * parseFloat(obj.sconto_perc);
                                                            //IF SCONTO_PERC>0
                                                            if (parseFloat(obj.sconto_perc) > 0) {
                                                                //TOT_SCONTI += TOT/100*SCONTO_PERC
                                                                totale_sconti += sconto_articolo;
                                                            } else {
                                                                totale_sconti += 0;
                                                            }

                                                            //NON PAGATO PER ORA VUOTO (C'E' IL CAMPO INCASSO COMUNQUE)
                                                            non_pagato += 0;
                                                            //INCASSO TOTALE
                                                            //INCASSO : TOTALE-TOT_SCONTI (IN REALTA SAREBBE INCASSATI-SCONTI INCASSATI)
                                                            //temporaneamente disabilitato lo fa alla fine





                                                            incasso_singolo = prezzo_articolo /*- sconto_articolo*/;
                                                            incasso += parseFloat(incasso_singolo);
                                                            console.log("INCASSI - CALCOLO PER ARTICOLO", 'DES', obj.desc_art, 'PRU', obj.prezzo_un, 'QTA', obj.quantita, 'SCP', obj.sconto_perc, 'CP', prezzo_articolo, 'CS', sconto_articolo, 'AQS', incasso_singolo, 'INCT', incasso);
                                                            //-----DICHIARAZIONE OGGETTO OPERAZIONI_OPERATORE (PER I TOTALI SOLAMENTE)-----

                                                            //IF OPERAZIONI_OPERATORE[OPERATORE] UNDEFINED

                                                            //INCASSO PER OPERATORE
                                                            //OPERAZIONI_OPERATORE[OPERATORE].INCASSO+=INCASSO;
                                                            if (operazioni_operatore[obj.operatore] === undefined) {
                                                                operazioni_operatore[obj.operatore] = new Object();
                                                                operazioni_operatore[obj.operatore].incasso = 0;
                                                            }
                                                            //CREA
                                                            operazioni_operatore[obj.operatore].incasso += incasso_singolo;
                                                            operazioni_operatore[obj.operatore].operatore = obj.operatore;



                                                            //SE IVA[IVA] UNDEFINED
                                                            if (iva[obj.tipo_ricevuta] === undefined) {
                                                                iva[obj.tipo_ricevuta] = new Object();
                                                            }

                                                            if (iva[obj.tipo_ricevuta][obj.perc_iva] === undefined) {
                                                                iva[obj.tipo_ricevuta][obj.perc_iva] = new Object();
                                                                //CALCOLO FISCALE
                                                                iva[obj.tipo_ricevuta][obj.perc_iva].percentuale = 0;
                                                                iva[obj.tipo_ricevuta][obj.perc_iva].imposta = 0;
                                                                iva[obj.tipo_ricevuta][obj.perc_iva].netto = 0;
                                                                iva[obj.tipo_ricevuta][obj.perc_iva].imponibile = 0;
                                                            }

                                                            //FISCO
                                                            if (obj.fiscalizzata_sn === 'S') {
                                                                switch (obj.tipo_ricevuta) {

                                                                    case "scontrino":
                                                                    case "scontrino fiscale":
                                                                    case "SCONTRINO":
                                                                        fisco['SCONTRINI'].incasso += incasso_singolo;
                                                                        if (parseInt(obj.n_fiscale) < parseInt(fisco['SCONTRINI'].numero_min) || parseInt(fisco['SCONTRINI'].numero_min) === 0)
                                                                            fisco['SCONTRINI'].numero_min = obj.n_fiscale;
                                                                        if (parseInt(obj.n_fiscale) > parseInt(fisco['SCONTRINI'].numero_max))
                                                                            fisco['SCONTRINI'].numero_max = obj.n_fiscale;
                                                                        break;
                                                                    case "fattura":
                                                                    case "FATTURA":
                                                                        fisco['FATTURE'].incasso += incasso_singolo;
                                                                        if (parseInt(obj.n_fiscale) < parseInt(fisco['FATTURE'].numero_min) || parseInt(fisco['FATTURE'].numero_min) === 0)
                                                                            fisco['FATTURE'].numero_min = obj.n_fiscale;
                                                                        if (parseInt(obj.n_fiscale) > parseInt(fisco['FATTURE'].numero_max))
                                                                            fisco['FATTURE'].numero_max = obj.n_fiscale;
                                                                        break;
                                                                }


                                                                //SOLO I FISCALIZZATI VANNO IN IVA E SERVE A DIFFERENZIARE L'IVA DEI VARI ARTICOLI
                                                                iva[obj.tipo_ricevuta][obj.perc_iva].percentuale = obj.perc_iva;
                                                                //aggiunto aggZero altrimenti se l'iva è a una sola cifra, ad esempio 5, fa / 1.5 invece di /1.05 e sbaglia totalmente tutto
                                                                iva[obj.tipo_ricevuta][obj.perc_iva].imposta += incasso_singolo / parseFloat('1.' + aggZero(obj.perc_iva, 2));
                                                                iva[obj.tipo_ricevuta][obj.perc_iva].netto += incasso_singolo - (incasso_singolo / parseFloat('1.' + aggZero(obj.perc_iva, 2)));
                                                                iva[obj.tipo_ricevuta][obj.perc_iva].imponibile += incasso_singolo;

                                                            }


                                                            console.log("STAMPA TOTALI CALCOLO IVA", incasso_singolo, parseFloat('1.' + obj.perc_iva));
                                                            //STATISTICO
                                                            //SWITCH
                                                            switch (obj.ntav_comanda) {
                                                                // TAVOLO BAR
                                                                case 'BAR':
                                                                case '*BAR*':
                                                                    statistico['BAR'].incasso += incasso_singolo;
                                                                    //STATISTICO['BAR'].INCASSO+=INCASSO
                                                                    break;
                                                                //TAKEAWAY ASPORTO
                                                                case 'ASPORTO':
                                                                case '*ASPORTO*':
                                                                case 'TAKE AWAY':
                                                                case 'TAKEAWAY':
                                                                case '*TAKE AWAY*':
                                                                case '*TAKEAWAY*':
                                                                    statistico['TAKEAWAY'].incasso += incasso_singolo;
                                                                    //STATISTICO['TAKEAWAY'].INCASSO+=INCASSO
                                                                    break;
                                                                //DEFAULT
                                                                default:
                                                                    statistico['RISTORAZIONE'].incasso += incasso_singolo;
                                                                //STATISTICO['RISTORAZIONE'].INCASSO+=INCASSO
                                                            }
                                                        }

                                                        //console.log("ARTICOLO GENERICO", articolo_generico);

                                                        console.log("ARTICOLO GENERICO DOPO SORTING", riordina(articolo_generico, 'prezzo', 'float'));
                                                        console.log("ARTICOLO GENERICO DOPO ESTRAZIONE", estrazione_array(riordina(articolo_generico, 'prezzo', 'float', true), 'destinazione', 'default'));


                                                        //-------------RESOCONTO------------




                                                        var incassi_video = '';
                                                        //DATI DB CHIUSURA
                                                        var array_chiusura = new Array();
                                                        var riga = 0;
                                                        //--------------------------------------------//
                                                        var builder = new epson.ePOSBuilder();

                                                        builder.addTextFont(builder.FONT_A);
                                                        builder.addTextAlign(builder.ALIGN_CENTER);

                                                        if (layout === "soloincassi") {
                                                            builder.addTextSize(2, 2);


                                                            if (layout === "soloincassi" && extra !== 'giornata' && extra !== 'PUNTOCASSA') {
                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);

                                                                incassi_video += '<tr>';
                                                                incassi_video += '<td  style="text-align: center;" colspan="3">';
                                                                builder.addText(comanda.locale + '\n');

                                                                builder.addTextSize(1, 1);
                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                builder.addText('\n');
                                                                builder.addText('\n');
                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);

                                                                builder.addTextSize(2, 2);
                                                                incassi_video += '<strong>' + comanda.locale + '</strong>';
                                                                incassi_video += '</td>';
                                                                incassi_video += '</tr>';
                                                            }


                                                            if (layout === "soloincassi" && extra !== 'giornata' && extra !== 'PUNTOCASSA') {
                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                builder.addText('PARZIALE STAMPA INCASSI\n');

                                                                incassi_video += '<table style="text-align:left;width:100%">';

                                                                incassi_video += '<tr>';
                                                                incassi_video += '<td style="text-align: center;" colspan="3">';
                                                                incassi_video += '<strong>PARZIALE STAMPA INCASSI</strong>';
                                                                incassi_video += '</td>';
                                                                incassi_video += '</tr>';
                                                            } else {
                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                builder.addText('STAMPA INCASSI\n');

                                                                incassi_video += '<table style="text-align:left;width:100%">';

                                                                incassi_video += '<tr>';
                                                                incassi_video += '<td style="text-align: center;" colspan="3">';
                                                                incassi_video += '<strong>STAMPA INCASSI</strong>';
                                                                incassi_video += '</td>';
                                                                incassi_video += '</tr>';
                                                            }
                                                            if (extra === 'PUNTOCASSA') {
                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);

                                                                builder.addText(comanda.nome_pc + '\n');
                                                                incassi_video += '<tr>';
                                                                incassi_video += '<td  colspan="3">';
                                                                incassi_video += comanda.nome_pc + '';
                                                                incassi_video += '</td>';
                                                                incassi_video += '</tr>';
                                                            }
                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                            builder.addTextSize(1, 1);
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td  colspan="3">';

                                                            builder.addText('__________________________________________\n');
                                                            incassi_video += '<hr>';
                                                            //incassi_video += '_________________________________________________________________________________________<br/>';
                                                            builder.addText('__________________________________________\n');
                                                            incassi_video += '<hr>';
                                                            //incassi_video += '_________________________________________________________________________________________';
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';

                                                            builder.addTextSize(1, 1);
                                                        }

                                                        if (settaggi_profili.Field90.length > 0) {
                                                            switch (settaggi_profili.Field91) {
                                                                case "G":
                                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                    break;
                                                                case "N":
                                                                default:
                                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                            }
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td  colspan="3">';
                                                            builder.addText(settaggi_profili.Field90 + '\n');
                                                            incassi_video += settaggi_profili.Field90 + '';
                                                            builder.addText('\n\n');

                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';
                                                        }



                                                        builder.addTextAlign(builder.ALIGN_CENTER);
                                                        builder.addTextStyle(false, false, false, builder.COLOR_1);

                                                        if (settaggi_profili.Field92 === 'true') {
                                                            builder.addTextSize(2, 2);
                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                            //builder.addText('REPORT: ');
                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);


                                                            switch (settaggi_profili.Field104) {
                                                                case "G":
                                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                    break;
                                                                case "N":
                                                                default:
                                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                            }

                                                            if (layout === "soloincassi" && extra !== 'giornata' && extra !== 'PUNTOCASSA') {
                                                                builder.addFeedLine(1);

                                                                builder.addText("SERVIZIO: " + parseInt(protocollo) + '\n');
                                                                incassi_video += '<tr>';
                                                                incassi_video += '<td  colspan="3">';
                                                                incassi_video += "SERVIZIO: " + parseInt(protocollo) + '';
                                                                incassi_video += '</td>';
                                                                incassi_video += '</tr>';


                                                                builder.addTextSize(1, 1);
                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);

                                                                builder.addText('\nDEL ' + data_report + '\nFINO ALLE ' + new Date().format("HH:MM") + '\n');

                                                                //builder.addText('FINO ALLE ' + new Date().format("HH:MM") + '\n');
                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);

                                                                builder.addTextSize(2, 2);
                                                                incassi_video += '<tr>';
                                                                incassi_video += '<td style="text-align: center;" colspan="3">';
                                                                incassi_video += 'Servizio del ' + data_report + '';
                                                                incassi_video += '</td>';
                                                                incassi_video += '</tr>';
                                                                incassi_video += '<tr>';
                                                                incassi_video += '<td style="text-align: center;" colspan="3">';
                                                                incassi_video += 'Fino alle: ' + new Date().format("HH:MM") + '';
                                                                incassi_video += '</td>';
                                                                incassi_video += '</tr>';
                                                            } else {
                                                                builder.addText('\n' + data_report + '\n');
                                                                incassi_video += '<tr>';
                                                                incassi_video += '<td style="text-align: center;" colspan="3">';
                                                                incassi_video += '' + data_report + '';
                                                                incassi_video += '</td>';
                                                                incassi_video += '</tr>';
                                                            }



                                                            builder.addTextAlign(builder.ALIGN_LEFT);

                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);

                                                        }


                                                        if (settaggi_profili.Field93 === 'true') {

                                                            builder.addTextAlign(builder.ALIGN_CENTER);

                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                            builder.addText('CREATO IL: ');
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td  style="text-align: center;" colspan="3">';
                                                            incassi_video += 'CREATO IL: ';

                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);

                                                            switch (settaggi_profili.Field94) {
                                                                case "G":
                                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                    break;
                                                                case "N":
                                                                default:
                                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                            }

                                                            if (comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].intelligent === "SDS") {
                                                                builder.addText('\n' + data + '\n');
                                                            } else {
                                                                builder.addText(data + '\n');
                                                            }
                                                            incassi_video += data + '';

                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';


                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);



                                                        }
                                                        builder.addFeedLine(1);

                                                        builder.addTextSize(2, 2);

                                                        builder.addTextAlign(builder.ALIGN_CENTER);

                                                        if (layout === "soloincassi" && extra !== 'giornata' && extra !== 'PUNTOCASSA') {
                                                            builder.addTextSize(2, 2);

                                                        } else if (layout === "soloincassi") {
                                                            //builder.addText("TOTALE GIORNATA\n");
                                                            //builder.addFeedLine(1);
                                                            builder.addTextSize(2, 2);
                                                        }

                                                        if (!(layout === "soloincassi" && extra !== 'giornata' && extra !== 'PUNTOCASSA')) {
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td  style="text-align: center;" colspan="3">';
                                                            builder.addText(comanda.locale + '\n');
                                                            incassi_video += '<strong>' + comanda.locale + '</strong>';
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';
                                                        }





                                                        //builder.addFeedLine(1);


                                                        if (layout === 'soloincassi' || layout === 'misto') {


                                                            //builder.addTextSize(2, 2);
                                                            //builder.addText('INCASSI GIORNALIERI\n');

                                                            //CORPO DELLO SCONTRINO 
                                                            //builder.addFeedLine(2);
                                                            builder.addTextSize(1, 1);
                                                            builder.addText('__________________________________________\n');
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td  colspan="3">';
                                                            incassi_video += '<hr>';
                                                            //incassi_video += '_________________________________________________________________________________________';
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';
                                                            builder.addFeedLine(1);
                                                            builder.addTextSize(2, 2);
                                                            builder.addText('RIEPILOGO TOTALI\n');
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td style="text-align: center;" colspan="3">';
                                                            incassi_video += '<strong>RIEPILOGO TOTALI</strong>';
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';
                                                            //------------------------------------------------//
                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '1',
                                                                descrizione: 'RIEPILOGO TOTALI',
                                                                quantita: '',
                                                                importo: '',
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });

                                                            //-------------------------------------------------//
                                                            builder.addTextSize(1, 1);

                                                            builder.addFeedLine(1);


                                                            builder.addTextAlign(builder.ALIGN_LEFT);

                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                            builder.addTextAlign(builder.ALIGN_CENTER);
                                                            builder.addText('CORRISPETTIVI RISCOSSI\n');
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td style="text-align: center;" colspan="3">';
                                                            incassi_video += '<strong>CORRISPETTIVI RISCOSSI</strong>';
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';


                                                            //------------------------------------------------//
                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'CORRISPETTIVI RISCOSSI',
                                                                quantita: '',
                                                                importo: '',
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });

                                                            //-------------------------------------------------//
                                                            builder.addTextAlign(builder.ALIGN_LEFT);
                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);


                                                            builder.addText('CONTANTI');
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td>';
                                                            incassi_video += 'CONTANTI';
                                                            incassi_video += '</td>';
                                                            builder.addTextPosition(390);

                                                            var totale_corrispettivi = 0;

                                                            var contanti = parseFloat(totale_contanti).toFixed(2);
                                                            if (obj_dati_ultima_chiusura !== false) {
                                                                contanti = parseFloat(parseFloat(contanti) - parseFloat(obj_dati_ultima_chiusura['RIEPILOGO TOTALI']['CONTANTI'].totale_importo)).toFixed(2);
                                                            }
                                                            totale_corrispettivi += parseFloat(contanti);

                                                            builder.addText('€ ' + contanti + '\n');

                                                            incassi_video += '<td></td>';
                                                            incassi_video += '<td style="text-align:right;">';
                                                            incassi_video += '€ ' + contanti + '';
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';
                                                            //------------------------------------------------//
                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'CONTANTI',
                                                                quantita: '',
                                                                importo: contanti,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });

                                                            //-------------------------------------------------//

                                                            builder.addText('BANCOMAT');
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td>';
                                                            incassi_video += 'BANCOMAT';
                                                            incassi_video += '</td>';
                                                            builder.addTextPosition(390);

                                                            var bancomat = parseFloat(totale_bancomat).toFixed(2);
                                                            if (obj_dati_ultima_chiusura !== false) {
                                                                bancomat = parseFloat(parseFloat(bancomat) - parseFloat(obj_dati_ultima_chiusura['RIEPILOGO TOTALI']['BANCOMAT'].totale_importo)).toFixed(2);
                                                            }
                                                            totale_corrispettivi += parseFloat(bancomat);

                                                            builder.addText('€ ' + bancomat + '\n');
                                                            incassi_video += '<td></td>';
                                                            incassi_video += '<td style="text-align:right;">';
                                                            incassi_video += '€ ' + bancomat + '';
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';

                                                            //------------------------------------------------//
                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'BANCOMAT',
                                                                quantita: '',
                                                                importo: bancomat,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });

                                                            builder.addText('C/C');
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td>';
                                                            incassi_video += 'C/C';
                                                            incassi_video += '</td>';

                                                            builder.addTextPosition(390);

                                                            var cartecredito = parseFloat(totale_cartecredito).toFixed(2);
                                                            if (obj_dati_ultima_chiusura !== false) {
                                                                cartecredito = parseFloat(parseFloat(cartecredito) - parseFloat(obj_dati_ultima_chiusura['RIEPILOGO TOTALI']['C/C'].totale_importo)).toFixed(2);
                                                            }
                                                            totale_corrispettivi += parseFloat(cartecredito);


                                                            builder.addText('€ ' + cartecredito + '\n');
                                                            incassi_video += '<td></td>';
                                                            incassi_video += '<td  style="text-align:right;">';
                                                            incassi_video += '€ ' + cartecredito + '';
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';







                                                            //------------------------------------------------//

                                                            builder.addText('SCONTO A PAGARE\n(INCL. ARROT. DIFETTO)');
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td >';
                                                            incassi_video += 'SCONTO A PAGARE (INCL. ARROT. DIFETTO)';
                                                            incassi_video += '</td>';
                                                            builder.addTextPosition(390);

                                                            var sconto_a_pagare = parseFloat(totale_sconto_a_pagare).toFixed(2);
                                                            if (obj_dati_ultima_chiusura !== false) {
                                                                sconto_a_pagare = parseFloat(parseFloat(sconto_a_pagare) - parseFloat(obj_dati_ultima_chiusura['RIEPILOGO TOTALI']['SCONTO A PAGARE'].totale_importo)).toFixed(2);
                                                            }
                                                            totale_corrispettivi -= parseFloat(sconto_a_pagare);


                                                            builder.addText('€ ' + sconto_a_pagare + '\n');
                                                            incassi_video += '<td></td>';
                                                            incassi_video += '<td  style="text-align:right;">';
                                                            incassi_video += '€ ' + sconto_a_pagare + '';
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';

                                                            //------------------------------------------------//
                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'SCONTO A PAGARE',
                                                                quantita: '',
                                                                importo: sconto_a_pagare,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });
                                                            //------------------------------------------------//

                                                            //------------------------------------------------//

                                                            builder.addText('ARROTONDAMENTO (IN ECCESSO)');
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td >';
                                                            incassi_video += 'ARROTONDAMENTI (IN ECCESSO)';
                                                            incassi_video += '</td>';
                                                            builder.addTextPosition(390);

                                                            var arrotondamento = parseFloat(totale_arrotondamento).toFixed(2);
                                                            if (obj_dati_ultima_chiusura !== false) {
                                                                arrotondamento = parseFloat(parseFloat(arrotondamento) - parseFloat(obj_dati_ultima_chiusura['RIEPILOGO TOTALI']['ARROTONDAMENTO'].totale_importo)).toFixed(2);
                                                            }
                                                            totale_corrispettivi += parseFloat(arrotondamento);

                                                            builder.addText('€ ' + arrotondamento + '\n');
                                                            incassi_video += '<td></td>';
                                                            incassi_video += '<td  style="text-align:right;">';
                                                            incassi_video += '€ ' + arrotondamento + '';
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';

                                                            //------------------------------------------------//
                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'ARROTONDAMENTO',
                                                                quantita: '',
                                                                importo: arrotondamento,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });
                                                            //------------------------------------------------//

                                                            incassi_video += '<tr>';
                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                            builder.addText('TOTALE');
                                                            incassi_video += '<td>';
                                                            incassi_video += '<strong>TOTALE</strong>';
                                                            incassi_video += '</td>';
                                                            builder.addTextPosition(390);
                                                            builder.addText('€ ' + totale_corrispettivi.toFixed(2));
                                                            incassi_video += '<td></td>';
                                                            incassi_video += '<td  style="text-align:right;"> ';
                                                            incassi_video += '<strong>€ ' + totale_corrispettivi.toFixed(2) + '</strong>';
                                                            incassi_video += '</td>';

                                                            incassi_video += '</tr>';
                                                            builder.addFeedLine(1);

                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                            //------------------------------------------------//
                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'C/C',
                                                                quantita: '',
                                                                importo: cartecredito,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });
                                                            //------------------------------------------------//

                                                            builder.addFeedLine(1);

                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                            builder.addTextAlign(builder.ALIGN_CENTER);
                                                            builder.addText('CORRISPETTIVI NON RISCOSSI\n');
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td style="text-align: center;"  colspan="3">';
                                                            incassi_video += '<strong>CORRISPETTIVI NON RISCOSSI</strong>';
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';
                                                            //------------------------------------------------//
                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'CORRISPETTIVI NON RISCOSSI',
                                                                quantita: '',
                                                                importo: '',
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });
                                                            //------------------------------------------------//
                                                            builder.addTextAlign(builder.ALIGN_LEFT);
                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);


                                                            builder.addText('BUONI PASTO');
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td>';
                                                            incassi_video += 'BUONI PASTO';
                                                            incassi_video += '</td>';

                                                            builder.addTextPosition(390);

                                                            var buonipasto = parseFloat(totale_buonipasto).toFixed(2);
                                                            if (obj_dati_ultima_chiusura !== false) {
                                                                buonipasto = parseFloat(parseFloat(buonipasto) - parseFloat(obj_dati_ultima_chiusura['RIEPILOGO TOTALI']['BUONI PASTO'].totale_importo)).toFixed(2);
                                                            }

                                                            builder.addText('€ ' + buonipasto + '\n');
                                                            incassi_video += '<td></td>';
                                                            incassi_video += '<td  style="text-align:right;">';
                                                            incassi_video += '€ ' + buonipasto + '';
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';
                                                            //------------------------------------------------//
                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'BUONI PASTO',
                                                                quantita: '',
                                                                importo: buonipasto,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });


                                                            //------------------------------------------------//


                                                            builder.addText(' -> DI CUI BUONI ACQUISTO\nMULTI-USO EMESSI');
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td>';
                                                            incassi_video += ' -> DI CUI BUONI ACQUISTO MULTI-USO EMESSI';
                                                            incassi_video += '</td>';
                                                            builder.addTextPosition(390);

                                                            var buoniacquisto_emessi = parseFloat(totale_buoniacquisto_emessi).toFixed(2);
                                                            if (obj_dati_ultima_chiusura !== false) {
                                                                buoniacquisto_emessi = parseFloat(parseFloat(buoniacquisto_emessi) - parseFloat(obj_dati_ultima_chiusura['RIEPILOGO TOTALI'][' -> DI CUI BUONI ACQUISTO EMESSI'].totale_importo)).toFixed(2);
                                                            }

                                                            builder.addText('€ ' + buoniacquisto_emessi + '\n');
                                                            incassi_video += '<td></td>';
                                                            incassi_video += '<td  style="text-align:right;">';
                                                            incassi_video += '€ ' + buoniacquisto_emessi + '';
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';
                                                            //------------------------------------------------//

                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: ' -> DI CUI BUONI ACQUISTO EMESSI',
                                                                quantita: '',
                                                                importo: buoniacquisto_emessi,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });
                                                            //------------------------------------------------//


                                                            builder.addText('BUONI ACQUISTO\nMULTI-USO INCASSATI');
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td>';
                                                            incassi_video += 'BUONI ACQUISTO MULTI-USO INCASSATI';
                                                            incassi_video += '</td>';
                                                            builder.addTextPosition(390);

                                                            var buoniacquisto = parseFloat(totale_buoniacquisto).toFixed(2);
                                                            if (obj_dati_ultima_chiusura !== false) {
                                                                buoniacquisto = parseFloat(parseFloat(buoniacquisto) - parseFloat(obj_dati_ultima_chiusura['RIEPILOGO TOTALI']['BUONI ACQUISTO INCASSATI'].totale_importo)).toFixed(2);
                                                            }

                                                            builder.addText('€ ' + buoniacquisto + '\n');
                                                            incassi_video += '<td></td>';
                                                            incassi_video += '<td  style="text-align:right;">';
                                                            incassi_video += '€ ' + buoniacquisto + '';
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';
                                                            //------------------------------------------------//
                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'BUONI ACQUISTO INCASSATI',
                                                                quantita: '',
                                                                importo: buoniacquisto,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });

                                                            //------------------------------------------------//

                                                            builder.addText('NON PAGATI');
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td >';
                                                            incassi_video += 'NON PAGATI';
                                                            incassi_video += '</td>';
                                                            builder.addTextPosition(390);

                                                            var non_pagato = parseFloat(totale_non_pagato).toFixed(2);
                                                            if (obj_dati_ultima_chiusura !== false) {
                                                                non_pagato = parseFloat(parseFloat(non_pagato) - parseFloat(obj_dati_ultima_chiusura['RIEPILOGO TOTALI']['NON PAGATI'].totale_importo)).toFixed(2);
                                                            }

                                                            builder.addText('€ ' + non_pagato + '\n');
                                                            incassi_video += '<td></td>';
                                                            incassi_video += '<td  style="text-align:right;">';
                                                            incassi_video += '€ ' + non_pagato + '';
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';

                                                            //------------------------------------------------//
                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'NON PAGATI',
                                                                quantita: '',
                                                                importo: non_pagato,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });
                                                            //------------------------------------------------//

                                                            //------------------------------------------------//

                                                            builder.addText('NON RISCOSSO SERVIZI');
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td >';
                                                            incassi_video += 'NON RISCOSSO SERVIZI';
                                                            incassi_video += '</td>';
                                                            builder.addTextPosition(390);

                                                            var non_riscosso_servizi = parseFloat(totale_non_riscosso_servizi).toFixed(2);
                                                            if (obj_dati_ultima_chiusura !== false) {
                                                                non_riscosso_servizi = parseFloat(parseFloat(non_riscosso_servizi) - parseFloat(obj_dati_ultima_chiusura['RIEPILOGO TOTALI']['NON RISCOSSO SERVIZI'].totale_importo)).toFixed(2);
                                                            }

                                                            builder.addText('€ ' + non_riscosso_servizi + '\n');
                                                            incassi_video += '<td></td>';
                                                            incassi_video += '<td  style="text-align:right;">';
                                                            incassi_video += '€ ' + non_riscosso_servizi + '';
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';

                                                            //------------------------------------------------//
                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'NON RISCOSSO SERVIZI',
                                                                quantita: '',
                                                                importo: non_riscosso_servizi,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });
                                                            //------------------------------------------------//

                                                            //------------------------------------------------//

                                                            /* builder.addText('SCONTO A PAGARE\n(INCL. ARROT. DIFETTO)');
                                                             incassi_video += '<tr>';
                                                             incassi_video += '<td >';
                                                             incassi_video += 'SCONTO A PAGARE (INCL. ARROT. DIFETTO)';
                                                             incassi_video += '</td>';
                                                             builder.addTextPosition(390);
 
                                                             var sconto_a_pagare = parseFloat(totale_sconto_a_pagare).toFixed(2);
                                                             if (obj_dati_ultima_chiusura !== false) {
                                                                 sconto_a_pagare = parseFloat(parseFloat(sconto_a_pagare) - parseFloat(obj_dati_ultima_chiusura['RIEPILOGO TOTALI']['SCONTO A PAGARE'].totale_importo)).toFixed(2);
                                                             }
 
                                                             builder.addText('€ ' + sconto_a_pagare + '\n');
                                                             incassi_video += '<td></td>';
                                                             incassi_video += '<td  style="text-align:right;">';
                                                             incassi_video += '€ ' + sconto_a_pagare + '';
                                                             incassi_video += '</td>';
                                                             incassi_video += '</tr>';*/

                                                            //------------------------------------------------//
                                                            /*riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'SCONTO A PAGARE',
                                                                quantita: '',
                                                                importo: sconto_a_pagare,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });*/
                                                            //------------------------------------------------//

                                                            //------------------------------------------------//

                                                            /*builder.addText('ARROTONDAMENTO (IN ECCESSO)');
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td >';
                                                            incassi_video += 'ARROTONDAMENTI (IN ECCESSO)';
                                                            incassi_video += '</td>';
                                                            builder.addTextPosition(390);

                                                            var arrotondamento = parseFloat(totale_arrotondamento).toFixed(2);
                                                            if (obj_dati_ultima_chiusura !== false) {
                                                                arrotondamento = parseFloat(parseFloat(arrotondamento) - parseFloat(obj_dati_ultima_chiusura['RIEPILOGO TOTALI']['ARROTONDAMENTO'].totale_importo)).toFixed(2);
                                                            }

                                                            builder.addText('€ ' + arrotondamento + '\n');
                                                            incassi_video += '<td></td>';
                                                            incassi_video += '<td  style="text-align:right;">';
                                                            incassi_video += '€ ' + arrotondamento + '';
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';*/

                                                            //------------------------------------------------//
                                                            /*riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'ARROTONDAMENTO',
                                                                quantita: '',
                                                                importo: arrotondamento,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });*/
                                                            //------------------------------------------------//


                                                            builder.addFeedLine(1);

                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                            builder.addText('TOTALE VENDITE');
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td>';
                                                            incassi_video += '<strong>TOTALE VENDITE</strong>';
                                                            incassi_video += '</td>';
                                                            builder.addTextPosition(390);

                                                            var totale = parseFloat(totale_totale).toFixed(2);
                                                            if (obj_dati_ultima_chiusura !== false) {
                                                                totale = parseFloat(parseFloat(totale) - parseFloat(obj_dati_ultima_chiusura['RIEPILOGO TOTALI']['TOTALE GENERALE'].totale_importo)).toFixed(2);
                                                            }

                                                            builder.addText('€ ' + totale + '\n');
                                                            incassi_video += '<td></td>';
                                                            incassi_video += '<td  style="text-align:right;">';
                                                            incassi_video += '<strong>€ ' + totale + '</strong>';
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';

                                                            builder.addText('TOTALE CORRISPETTIVI RISCOSSI');

                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td>';
                                                            incassi_video += '<strong>TOTALE CORRISPETTIVI RISCOSSI</strong>';
                                                            incassi_video += '</td>';
                                                            builder.addTextPosition(390);
                                                            builder.addText('€ ' + totale_corrispettivi.toFixed(2));
                                                            incassi_video += '<td></td>';
                                                            incassi_video += '<td  style="text-align:right;">';
                                                            incassi_video += '<strong>€ ' + totale_corrispettivi.toFixed(2) + '</strong>';
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';
                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                            //------------------------------------------------//
                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'TOTALE GENERALE',
                                                                quantita: '',
                                                                importo: totale,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });
                                                            //------------------------------------------------//

                                                            builder.addFeedLine(1);

                                                            builder.addText('TOTALE SCONTI (GIA DETRATTI)');
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td>';
                                                            incassi_video += '<strong>TOTALE SCONTI (GIA DETRATTI)</strong>';
                                                            incassi_video += '</td>';

                                                            builder.addTextPosition(390);

                                                            var sconti = parseFloat(totale_sconti).toFixed(2);
                                                            if (obj_dati_ultima_chiusura !== false) {
                                                                sconti = parseFloat(parseFloat(sconti) - parseFloat(obj_dati_ultima_chiusura['RIEPILOGO TOTALI']['TOTALE SCONTI (GIA DETRATTI)'].totale_importo)).toFixed(2);
                                                            }

                                                            builder.addText('€ ' + sconti + '\n');
                                                            incassi_video += '<td></td>';
                                                            incassi_video += '<td  style="text-align:right;"> ';
                                                            incassi_video += '<strong>€ ' + sconti + '</strong>';
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';

                                                            //------------------------------------------------//
                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'TOTALE SCONTI (GIA DETRATTI)',
                                                                quantita: '',
                                                                importo: sconti,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });
                                                            //------------------------------------------------//
                                                            builder.addTextAlign(builder.ALIGN_CENTER);
                                                            builder.addFeedLine(1);



                                                            builder.addText('__________________________________________\n');
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td   colspan="3">';
                                                            incassi_video += '<hr>';
                                                            //incassi_video += '_________________________________________________________________________________________';
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';
                                                            builder.addFeedLine(1);

                                                            //NON PAGATO (disabilitato al momento)
                                                            //INCASSO
                                                            /*builder.addText('INCASSO');
                                                             builder.addTextPosition(390);
                                                             builder.addText('€ ' + parseFloat(totale_incassato).toFixed(2) + '\n\n');
                                                             builder.addTextAlign(builder.ALIGN_CENTER);*/
                                                            //---PARTE DELL'IVA---
                                                            builder.addTextSize(2, 2);


                                                            builder.addText('DATI FISCALI\n\n');
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td style="text-align: center;" colspan="3">';
                                                            incassi_video += '<strong>DATI FISCALI</strong>';
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';

                                                            //------------------------------------------------//
                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '1',
                                                                descrizione: 'DATI FISCALI',
                                                                quantita: '',
                                                                importo: '',
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });
                                                            //------------------------------------------------//
                                                            builder.addTextAlign(builder.ALIGN_LEFT);
                                                            builder.addTextSize(1, 1);

                                                            var totale_ive = 0;

                                                            //------------------------------------------------//

                                                            var imposta = "0";
                                                            var imponibile = "0";
                                                            var netto = "0";

                                                            if (iva["SCONTRINO"] !== undefined) {
                                                                if (iva["SCONTRINO"][ive_misuratore_controller.iva_reparto(1)] !== undefined) {
                                                                    imposta = iva["SCONTRINO"][ive_misuratore_controller.iva_reparto(1)].imposta;
                                                                    if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['DATI FISCALI']['IMPONIBILE (SCONTRINO ' + ive_misuratore_controller.iva_reparto(1) + ')'] !== undefined) {
                                                                        imposta = parseFloat(parseFloat(imposta) - parseFloat(obj_dati_ultima_chiusura['DATI FISCALI']['IMPONIBILE (SCONTRINO ' + ive_misuratore_controller.iva_reparto(1) + ')'].totale_importo)).toFixed(2);
                                                                    }

                                                                    imponibile = iva["SCONTRINO"][ive_misuratore_controller.iva_reparto(1)].imponibile;
                                                                    if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['DATI FISCALI']['TOTALE (SCONTRINO ' + ive_misuratore_controller.iva_reparto(1) + ')'] !== undefined) {
                                                                        imponibile = parseFloat(parseFloat(imponibile) - parseFloat(obj_dati_ultima_chiusura['DATI FISCALI']['TOTALE (SCONTRINO ' + ive_misuratore_controller.iva_reparto(1) + ')'].totale_importo)).toFixed(2);
                                                                    }

                                                                    netto = iva["SCONTRINO"][ive_misuratore_controller.iva_reparto(1)].netto;
                                                                    if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['DATI FISCALI']['IMPOSTA (SCONTRINO ' + ive_misuratore_controller.iva_reparto(1) + ')'] !== undefined) {
                                                                        netto = parseFloat(parseFloat(netto) - parseFloat(obj_dati_ultima_chiusura['DATI FISCALI']['IMPOSTA (SCONTRINO ' + ive_misuratore_controller.iva_reparto(1) + ')'].totale_importo)).toFixed(2);
                                                                    }
                                                                }
                                                            }

                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'IMPONIBILE (SCONTRINO ' + ive_misuratore_controller.iva_reparto(1) + ')',
                                                                quantita: '',
                                                                importo: imposta,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });



                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: '% IVA (SCONTRINO ' + ive_misuratore_controller.iva_reparto(1) + ')',
                                                                quantita: '',
                                                                importo: "10",
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });

                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'IMPOSTA (SCONTRINO ' + ive_misuratore_controller.iva_reparto(1) + ')',
                                                                quantita: '',
                                                                importo: netto,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });

                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'TOTALE (SCONTRINO ' + ive_misuratore_controller.iva_reparto(1) + ')',
                                                                quantita: '',
                                                                importo: imponibile,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });
                                                            //------------------------------------------------//        
                                                            var imposta = "0";
                                                            var imponibile = "0";
                                                            var netto = "0";
                                                            if (iva["SCONTRINO"] !== undefined) {
                                                                if (iva["SCONTRINO"][ive_misuratore_controller.iva_reparto(2)] !== undefined) {
                                                                    imposta = iva["SCONTRINO"][ive_misuratore_controller.iva_reparto(2)].imposta;
                                                                    if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['DATI FISCALI']['IMPONIBILE (SCONTRINO ' + ive_misuratore_controller.iva_reparto(2) + ')'] !== undefined) {
                                                                        imposta = parseFloat(parseFloat(imposta) - parseFloat(obj_dati_ultima_chiusura['DATI FISCALI']['IMPONIBILE (SCONTRINO ' + ive_misuratore_controller.iva_reparto(2) + ')'].totale_importo)).toFixed(2);
                                                                    }

                                                                    imponibile = iva["SCONTRINO"][ive_misuratore_controller.iva_reparto(2)].imponibile;
                                                                    if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['DATI FISCALI']['TOTALE (SCONTRINO ' + ive_misuratore_controller.iva_reparto(2) + ')'] !== undefined) {
                                                                        imponibile = parseFloat(parseFloat(imponibile) - parseFloat(obj_dati_ultima_chiusura['DATI FISCALI']['TOTALE (SCONTRINO ' + ive_misuratore_controller.iva_reparto(2) + ')'].totale_importo)).toFixed(2);
                                                                    }

                                                                    netto = iva["SCONTRINO"][ive_misuratore_controller.iva_reparto(2)].netto;
                                                                    if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['DATI FISCALI']['IMPOSTA (SCONTRINO ' + ive_misuratore_controller.iva_reparto(2) + ')'] !== undefined) {
                                                                        netto = parseFloat(parseFloat(netto) - parseFloat(obj_dati_ultima_chiusura['DATI FISCALI']['IMPOSTA (SCONTRINO ' + ive_misuratore_controller.iva_reparto(2) + ')'].totale_importo)).toFixed(2);
                                                                    }
                                                                }
                                                            }

                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'IMPONIBILE (SCONTRINO ' + ive_misuratore_controller.iva_reparto(2) + ')',
                                                                quantita: '',
                                                                importo: imposta,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });

                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: '% IVA (SCONTRINO ' + ive_misuratore_controller.iva_reparto(2) + ')',
                                                                quantita: '',
                                                                importo: "22",
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });

                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'IMPOSTA (SCONTRINO ' + ive_misuratore_controller.iva_reparto(2) + ')',
                                                                quantita: '',
                                                                importo: netto,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });

                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'TOTALE (SCONTRINO ' + ive_misuratore_controller.iva_reparto(2) + ')',
                                                                quantita: '',
                                                                importo: imponibile,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });
                                                            //------------------------------------------------//
                                                            var imposta = "0";
                                                            var imponibile = "0";
                                                            var netto = "0";
                                                            if (iva["SCONTRINO"] !== undefined) {
                                                                if (iva["SCONTRINO"][ive_misuratore_controller.iva_reparto(3)] !== undefined) {
                                                                    imposta = iva["SCONTRINO"][ive_misuratore_controller.iva_reparto(3)].imposta;
                                                                    if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['DATI FISCALI']['IMPONIBILE (SCONTRINO ' + ive_misuratore_controller.iva_reparto(3) + ')'] !== undefined) {
                                                                        imposta = parseFloat(parseFloat(imposta) - parseFloat(obj_dati_ultima_chiusura['DATI FISCALI']['IMPONIBILE (SCONTRINO ' + ive_misuratore_controller.iva_reparto(3) + ')'].totale_importo)).toFixed(2);
                                                                    }

                                                                    imponibile = iva["SCONTRINO"][ive_misuratore_controller.iva_reparto(3)].imponibile;
                                                                    if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['DATI FISCALI']['TOTALE (SCONTRINO ' + ive_misuratore_controller.iva_reparto(3) + ')'] !== undefined) {
                                                                        imponibile = parseFloat(parseFloat(imponibile) - parseFloat(obj_dati_ultima_chiusura['DATI FISCALI']['TOTALE (SCONTRINO ' + ive_misuratore_controller.iva_reparto(3) + ')'].totale_importo)).toFixed(2);
                                                                    }

                                                                    netto = iva["SCONTRINO"][ive_misuratore_controller.iva_reparto(3)].netto;
                                                                    if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['DATI FISCALI']['IMPOSTA (SCONTRINO ' + ive_misuratore_controller.iva_reparto(3) + ')'] !== undefined) {
                                                                        netto = parseFloat(parseFloat(netto) - parseFloat(obj_dati_ultima_chiusura['DATI FISCALI']['IMPOSTA (SCONTRINO ' + ive_misuratore_controller.iva_reparto(3) + ')'].totale_importo)).toFixed(2);
                                                                    }
                                                                }
                                                            }
                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'IMPONIBILE (SCONTRINO ' + ive_misuratore_controller.iva_reparto(3) + ')',
                                                                quantita: '',
                                                                importo: imposta,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });

                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: '% IVA (SCONTRINO ' + ive_misuratore_controller.iva_reparto(3) + ')',
                                                                quantita: '',
                                                                importo: "4",
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });

                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'IMPOSTA (SCONTRINO ' + ive_misuratore_controller.iva_reparto(3) + ')',
                                                                quantita: '',
                                                                importo: netto,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });

                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'TOTALE (SCONTRINO ' + ive_misuratore_controller.iva_reparto(3) + ')',
                                                                quantita: '',
                                                                importo: imponibile,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });

                                                            //------------------------------------------------//
                                                            //------------------------------------------------//
                                                            var imposta = "0";
                                                            var imponibile = "0";
                                                            var netto = "0";
                                                            if (iva["FATTURA"] !== undefined) {
                                                                if (iva["FATTURA"][ive_misuratore_controller.iva_reparto(1)] !== undefined) {
                                                                    imposta = iva["FATTURA"][ive_misuratore_controller.iva_reparto(1)].imposta;
                                                                    if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['DATI FISCALI']['IMPONIBILE (FATTURA ' + ive_misuratore_controller.iva_reparto(1) + ')'] !== undefined) {
                                                                        imposta = parseFloat(parseFloat(imposta) - parseFloat(obj_dati_ultima_chiusura['DATI FISCALI']['IMPONIBILE (FATTURA ' + ive_misuratore_controller.iva_reparto(1) + ')'].totale_importo)).toFixed(2);
                                                                    }

                                                                    imponibile = iva["FATTURA"][ive_misuratore_controller.iva_reparto(1)].imponibile;
                                                                    if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['DATI FISCALI']['TOTALE (FATTURA ' + ive_misuratore_controller.iva_reparto(1) + ')'] !== undefined) {
                                                                        imponibile = parseFloat(parseFloat(imponibile) - parseFloat(obj_dati_ultima_chiusura['DATI FISCALI']['TOTALE (FATTURA ' + ive_misuratore_controller.iva_reparto(1) + ')'].totale_importo)).toFixed(2);
                                                                    }

                                                                    netto = iva["FATTURA"][ive_misuratore_controller.iva_reparto(1)].netto;
                                                                    if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['DATI FISCALI']['IMPOSTA (FATTURA ' + ive_misuratore_controller.iva_reparto(1) + ')'] !== undefined) {
                                                                        netto = parseFloat(parseFloat(netto) - parseFloat(obj_dati_ultima_chiusura['DATI FISCALI']['IMPOSTA (FATTURA ' + ive_misuratore_controller.iva_reparto(1) + ')'].totale_importo)).toFixed(2);
                                                                    }
                                                                }
                                                            }
                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'IMPONIBILE (FATTURA ' + ive_misuratore_controller.iva_reparto(1) + ')',
                                                                quantita: '',
                                                                importo: imposta,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });

                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: '% IVA (FATTURA ' + ive_misuratore_controller.iva_reparto(1) + ')',
                                                                quantita: '',
                                                                importo: "10",
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });

                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'IMPOSTA (FATTURA ' + ive_misuratore_controller.iva_reparto(1) + ')',
                                                                quantita: '',
                                                                importo: netto,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });

                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'TOTALE (FATTURA ' + ive_misuratore_controller.iva_reparto(1) + ')',
                                                                quantita: '',
                                                                importo: imponibile,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });
                                                            //------------------------------------------------//      
                                                            var imposta = "0";
                                                            var imponibile = "0";
                                                            var netto = "0";
                                                            if (iva["FATTURA"] !== undefined) {
                                                                if (iva["FATTURA"][ive_misuratore_controller.iva_reparto(2)] !== undefined) {
                                                                    imposta = iva["FATTURA"][ive_misuratore_controller.iva_reparto(2)].imposta;
                                                                    if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['DATI FISCALI']['IMPONIBILE (FATTURA ' + ive_misuratore_controller.iva_reparto(2) + ')'] !== undefined) {
                                                                        imposta = parseFloat(parseFloat(imposta) - parseFloat(obj_dati_ultima_chiusura['DATI FISCALI']['IMPONIBILE (FATTURA ' + ive_misuratore_controller.iva_reparto(2) + ')'].totale_importo)).toFixed(2);
                                                                    }

                                                                    imponibile = iva["FATTURA"][ive_misuratore_controller.iva_reparto(2)].imponibile;
                                                                    if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['DATI FISCALI']['TOTALE (FATTURA ' + ive_misuratore_controller.iva_reparto(2) + ')'] !== undefined) {
                                                                        imponibile = parseFloat(parseFloat(imponibile) - parseFloat(obj_dati_ultima_chiusura['DATI FISCALI']['TOTALE (FATTURA ' + ive_misuratore_controller.iva_reparto(2) + ')'].totale_importo)).toFixed(2);
                                                                    }

                                                                    netto = iva["FATTURA"][ive_misuratore_controller.iva_reparto(2)].netto;
                                                                    if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['DATI FISCALI']['IMPOSTA (FATTURA ' + ive_misuratore_controller.iva_reparto(2) + ')'] !== undefined) {
                                                                        netto = parseFloat(parseFloat(netto) - parseFloat(obj_dati_ultima_chiusura['DATI FISCALI']['IMPOSTA (FATTURA ' + ive_misuratore_controller.iva_reparto(2) + ')'].totale_importo)).toFixed(2);
                                                                    }
                                                                }
                                                            }
                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'IMPONIBILE (FATTURA ' + ive_misuratore_controller.iva_reparto(2) + ')',
                                                                quantita: '',
                                                                importo: imposta,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });

                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: '% IVA (FATTURA ' + ive_misuratore_controller.iva_reparto(2) + ')',
                                                                quantita: '',
                                                                importo: "22",
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });

                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'IMPOSTA (FATTURA ' + ive_misuratore_controller.iva_reparto(2) + ')',
                                                                quantita: '',
                                                                importo: netto,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });

                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'TOTALE (FATTURA ' + ive_misuratore_controller.iva_reparto(2) + ')',
                                                                quantita: '',
                                                                importo: imponibile,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });
                                                            //------------------------------------------------//
                                                            if (iva["FATTURA"] !== undefined) {
                                                                if (iva["FATTURA"][ive_misuratore_controller.iva_reparto(3)] !== undefined) {
                                                                    imposta = iva["FATTURA"][ive_misuratore_controller.iva_reparto(3)].imposta;
                                                                    if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['DATI FISCALI']['IMPONIBILE (FATTURA ' + ive_misuratore_controller.iva_reparto(3) + ')'] !== undefined) {
                                                                        imposta = parseFloat(parseFloat(imposta) - parseFloat(obj_dati_ultima_chiusura['DATI FISCALI']['IMPONIBILE (FATTURA ' + ive_misuratore_controller.iva_reparto(3) + ')'].totale_importo)).toFixed(2);
                                                                    }

                                                                    imponibile = iva["FATTURA"][ive_misuratore_controller.iva_reparto(3)].imponibile;
                                                                    if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['DATI FISCALI']['TOTALE (FATTURA ' + ive_misuratore_controller.iva_reparto(3) + ')'] !== undefined) {
                                                                        imponibile = parseFloat(parseFloat(imponibile) - parseFloat(obj_dati_ultima_chiusura['DATI FISCALI']['TOTALE (FATTURA ' + ive_misuratore_controller.iva_reparto(3) + ')'].totale_importo)).toFixed(2);
                                                                    }

                                                                    netto = iva["FATTURA"][ive_misuratore_controller.iva_reparto(3)].netto;
                                                                    if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['DATI FISCALI']['IMPOSTA (FATTURA ' + ive_misuratore_controller.iva_reparto(3) + ')'] !== undefined) {
                                                                        netto = parseFloat(parseFloat(netto) - parseFloat(obj_dati_ultima_chiusura['DATI FISCALI']['IMPOSTA (FATTURA ' + ive_misuratore_controller.iva_reparto(3) + ')'].totale_importo)).toFixed(2);
                                                                    }
                                                                }
                                                            }
                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'IMPONIBILE (FATTURA ' + ive_misuratore_controller.iva_reparto(3) + ')',
                                                                quantita: '',
                                                                importo: imposta,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });

                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: '% IVA (FATTURA ' + ive_misuratore_controller.iva_reparto(3) + ')',
                                                                quantita: '',
                                                                importo: "4",
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });

                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'IMPOSTA (FATTURA ' + ive_misuratore_controller.iva_reparto(3) + ')',
                                                                quantita: '',
                                                                importo: netto,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });

                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'TOTALE (FATTURA ' + ive_misuratore_controller.iva_reparto(3) + ')',
                                                                quantita: '',
                                                                importo: imponibile,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });
                                                            //------------------------------------------------//

                                                            for (var tipo_ricevuta in iva) {

                                                                var nome_ricevuta_video = "";
                                                                if (tipo_ricevuta === "SCONTRINO") {
                                                                    nome_ricevuta_video = "SCONTRINI";
                                                                } else if (tipo_ricevuta === "FATTURA") {
                                                                    nome_ricevuta_video = "FATTURE";
                                                                }

                                                                builder.addText(nome_ricevuta_video + '\n\n');
                                                                incassi_video += '<tr>';
                                                                incassi_video += '<td style="text-align: center;" colspan="3">';
                                                                incassi_video += '<strong>' + nome_ricevuta_video + '</strong>';
                                                                incassi_video += '</td>';
                                                                incassi_video += '</tr>';

                                                                for (var key in iva[tipo_ricevuta]) {

                                                                    builder.addText('IMPONIBILE');
                                                                    incassi_video += '<tr>';
                                                                    incassi_video += '<td >';
                                                                    incassi_video += '<strong>IMPONIBILE</strong>';
                                                                    incassi_video += '</td>';

                                                                    builder.addTextPosition(390);

                                                                    var imposta = parseFloat(iva[tipo_ricevuta][key].imposta).toFixed(2);
                                                                    if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['DATI FISCALI']['IMPONIBILE (' + tipo_ricevuta + ' ' + key + ')'] !== undefined) {
                                                                        imposta = parseFloat(parseFloat(imposta) - parseFloat(obj_dati_ultima_chiusura['DATI FISCALI']['IMPONIBILE (' + tipo_ricevuta + ' ' + key + ')'].totale_importo)).toFixed(2);
                                                                    }

                                                                    builder.addText('€ ' + imposta + '\n');
                                                                    incassi_video += '<td></td>';
                                                                    incassi_video += '<td  style="text-align:right;">';
                                                                    incassi_video += '<strong>€ ' + imposta + '</strong>';
                                                                    incassi_video += '</td>';
                                                                    incassi_video += '</tr>';


                                                                    //%IVA
                                                                    builder.addText('% IVA');
                                                                    incassi_video += '<tr>';
                                                                    incassi_video += '<td>';

                                                                    incassi_video += '% IVA';

                                                                    incassi_video += '</td>';

                                                                    builder.addTextPosition(390);

                                                                    builder.addText('% ' + iva[tipo_ricevuta][key].percentuale + '\n');
                                                                    incassi_video += '<td></td>';
                                                                    incassi_video += '<td  style="text-align:right;">';
                                                                    incassi_video += '% ' + iva[tipo_ricevuta][key].percentuale + '';
                                                                    incassi_video += '</td>';
                                                                    incassi_video += '</tr>';

                                                                    //
                                                                    //IMPOSTA
                                                                    builder.addText('IMPOSTA');
                                                                    incassi_video += '<tr>';
                                                                    incassi_video += '<td>';

                                                                    incassi_video += 'IMPOSTA';
                                                                    incassi_video += '</td>';
                                                                    builder.addTextPosition(390);

                                                                    var netto = parseFloat(iva[tipo_ricevuta][key].netto).toFixed(2);
                                                                    if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['DATI FISCALI']['IMPOSTA (' + tipo_ricevuta + ' ' + key + ')'] !== undefined) {
                                                                        netto = parseFloat(parseFloat(netto) - parseFloat(obj_dati_ultima_chiusura['DATI FISCALI']['IMPOSTA (' + tipo_ricevuta + ' ' + key + ')'].totale_importo)).toFixed(2);
                                                                    }

                                                                    builder.addText('€ ' + netto + '\n');
                                                                    incassi_video += '<td></td>';
                                                                    incassi_video += '<td  style="text-align:right;">';
                                                                    incassi_video += '€ ' + netto + '';
                                                                    incassi_video += '</td>';
                                                                    incassi_video += '</tr>';


                                                                    //IMPONIBILE
                                                                    builder.addText('TOTALE');
                                                                    incassi_video += '<tr>';
                                                                    incassi_video += '<td>';
                                                                    incassi_video += '<strong>TOTALE</strong>';
                                                                    incassi_video += '</td>';
                                                                    builder.addTextPosition(390);

                                                                    var imponibile = parseFloat(iva[tipo_ricevuta][key].imponibile).toFixed(2);
                                                                    if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['DATI FISCALI']['TOTALE (' + tipo_ricevuta + ' ' + key + ')'] !== undefined) {
                                                                        imponibile = parseFloat(parseFloat(imponibile) - parseFloat(obj_dati_ultima_chiusura['DATI FISCALI']['TOTALE (' + tipo_ricevuta + ' ' + key + ')'].totale_importo)).toFixed(2);
                                                                    }

                                                                    builder.addText('€ ' + imponibile + '\n');

                                                                    incassi_video += '<td></td>';
                                                                    incassi_video += '<td  style="text-align:right;"> ';

                                                                    incassi_video += '<strong>€ ' + imponibile + '</strong>';
                                                                    incassi_video += '</td>';
                                                                    incassi_video += '</tr>';

                                                                    incassi_video += '<tr>';
                                                                    incassi_video += '<td>';
                                                                    incassi_video += '&nbsp;';
                                                                    incassi_video += '</td>';
                                                                    incassi_video += '</tr>';




                                                                    builder.addFeedLine(1);




                                                                    totale_ive += parseFloat(iva[tipo_ricevuta][key].imponibile);

                                                                }
                                                            }

                                                            builder.addFeedLine(1);

                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                            builder.addText('NON RISCOSSO SERVIZI');
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td>';
                                                            incassi_video += '<strong>NON RISCOSSO SERVIZI</strong>';
                                                            incassi_video += '</td>';
                                                            builder.addTextPosition(390);



                                                            builder.addText('€ ' + non_riscosso_servizi + '\n');
                                                            incassi_video += '<td></td>';
                                                            incassi_video += '<td  style="text-align:right;">';
                                                            incassi_video += '<strong>€ ' + non_riscosso_servizi + '</strong>';
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';

                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                            builder.addText('TOTALE GENERALE');
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td>';
                                                            incassi_video += '<strong>TOTALE GENERALE</strong>';
                                                            incassi_video += '</td>';
                                                            builder.addTextPosition(390);

                                                            var totale_ive = parseFloat(totale_ive).toFixed(2);
                                                            if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['DATI FISCALI']['TOTALE GENERALE'] !== undefined) {
                                                                totale_ive = parseFloat(parseFloat(totale_ive) - parseFloat(obj_dati_ultima_chiusura['DATI FISCALI']['TOTALE GENERALE'].totale_importo)).toFixed(2);
                                                            }

                                                            builder.addText('€ ' + parseFloat(totale_ive).toFixed(2) + '\n');
                                                            incassi_video += '<td></td>';
                                                            incassi_video += '<td  style="text-align:right;">';
                                                            incassi_video += '<strong>€ ' + parseFloat(totale_ive).toFixed(2) + '</strong>';
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';

                                                            var contenuto_alert = "";

                                                            if ((parseFloat(totale_ive) + parseFloat(non_riscosso_servizi)).toFixed(2) !== totale) {
                                                                contenuto_alert += '<strong>DIFFERENZA RILEVATA</strong><br><br>';


                                                                array_differenze_teste_corpi(orario_query_tes, function (diff) {

                                                                    let differenze_corpi_totali = 0;
                                                                    let differenze_teste_totali = 0;
                                                                    diff.forEach(function (e) {

                                                                        if (e[4] === "C") {
                                                                            differenze_corpi_totali += parseFloat(e[2]) - parseFloat(e[3]);
                                                                        }
                                                                        if (e[4] === "T") {
                                                                            differenze_teste_totali += parseFloat(e[2]) - parseFloat(e[3]);
                                                                        }

                                                                        contenuto_alert += e[4] + " - " + e[0] + " - " + parseFloat(e[2]).toFixed(2) + " - " + parseFloat(e[3]).toFixed(2) + " - " + "<br>";


                                                                    });

                                                                    bootbox.alert(contenuto_alert + "<br><br>Differenze teste: " + differenze_teste_totali.toFixed(2) + "<br><br>Differenze corpi: " + differenze_corpi_totali.toFixed(2));
                                                                });

                                                            }




                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);

                                                            //------------------------------------------------//
                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'TOTALE GENERALE',
                                                                quantita: '',
                                                                importo: totale_ive,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });
                                                            //------------------------------------------------//
                                                            builder.addTextAlign(builder.ALIGN_CENTER);
                                                            builder.addText('__________________________________________\n');
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td  colspan="3">';
                                                            incassi_video += '<hr>';
                                                            //incassi_video += '_________________________________________________________________________________________';
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';
                                                            builder.addFeedLine(1);


                                                            // FINE FISCALE TESTA

                                                            //-----NUMERI FATTURA E SCONTRINO-------

                                                            //QUERY WHERE STATO RECORD CHIUSO E FISCALIZZATA_SN = s AND DATA COME OGGI

                                                            //NEL CASO 1 LA QUERY E' ANCHE SE TIPO_RICEVUTA = fattura CAST AS INT LIMIT 1 ASC + LIMIT 1 DESC

                                                            //NEL CASO 2 LA QUERY E' SE TIPO_RICEVUTA = scontrino CAST AS INT LIMIT 1 ASC + LIMIT 1 DESC

                                                            //----FOR STATISTICO

                                                            //INDEX
                                                            //.INCASSO

                                                            //---STATISTICO---
                                                            builder.addTextSize(2, 2);
                                                            builder.addText('FISCALIZZAZIONE\n\n');
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td colspan="3" style="text-align: center;">';
                                                            incassi_video += '<strong>FISCALIZZAZIONE</strong>';
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';
                                                            //------------------------------------------------//
                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '1',
                                                                descrizione: 'FISCALIZZAZIONE',
                                                                quantita: '',
                                                                importo: '',
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });
                                                            //------------------------------------------------//
                                                            builder.addTextAlign(builder.ALIGN_LEFT);
                                                            builder.addTextSize(1, 1);



                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);

                                                            builder.addText('TIPOLOGIA');
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td>';
                                                            incassi_video += '<strong>TIPOLOGIA</strong>';
                                                            incassi_video += '</td>';

                                                            builder.addTextPosition(310);
                                                            builder.addText("QTA");
                                                            incassi_video += '<td>';
                                                            incassi_video += "<strong>QTA</strong>";
                                                            incassi_video += '</td>';

                                                            builder.addTextPosition(390);
                                                            builder.addText("IMPORTO");
                                                            incassi_video += '<td  style="text-align:right;">';
                                                            incassi_video += "<strong>IMPORTO</strong>";
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';



                                                            builder.addText('\n\n');
                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);



                                                            if (comanda.licenza === 'mattia131') {


                                                            } else {
                                                                //ATTENZIONE: SCONTRINI ARRIVANO SOLO A QUELLI MASSIMI DI 1 CASSA
                                                                //SERVE SOLO PER LE FATTURE SE IL CONTATORE E' UNICO
                                                                for (var key in fisco) {

                                                                    if (key === "FATTURE") {

                                                                        riga++;
                                                                        array_chiusura.push({
                                                                            data_servizio: data_report,
                                                                            data_emissione: data_emissione,
                                                                            lingua_stampa: comanda.lingua_stampa,
                                                                            protocollo: protocollo,
                                                                            progressivo_riga: aggZero(riga, 4),
                                                                            operatore: comanda.operatore,
                                                                            //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                            tipo_record: '2',
                                                                            descrizione: key + "1",
                                                                            quantita: fisco[key].numero_max,
                                                                            importo: "",
                                                                            nome_locale: comanda.locale,
                                                                            fiscale_sn: 'n',
                                                                            nome_pc: comanda.nome_pc
                                                                        });
                                                                        if (obj_dati_ultima_chiusura['FISCALIZZAZIONE'] !== undefined && obj_dati_ultima_chiusura['FISCALIZZAZIONE'][key + "1"] !== undefined) {
                                                                            if (parseInt(fisco[key].numero_min) < parseInt(obj_dati_ultima_chiusura['FISCALIZZAZIONE'][key + "1"].quantita)) {
                                                                                fisco[key].numero_min = (parseInt(obj_dati_ultima_chiusura['FISCALIZZAZIONE'][key + "1"].quantita) + 1).toString();
                                                                            }
                                                                            if (fisco[key].numero_max < fisco[key].numero_min) {
                                                                                fisco[key].numero_min = "0";
                                                                                fisco[key].numero_max = fisco[key].numero_min;
                                                                            }
                                                                        }
                                                                    }

                                                                    builder.addText(key);
                                                                    incassi_video += '<td>';
                                                                    incassi_video += key;
                                                                    if (fisco[key].numero_min === null) {
                                                                        fisco[key].numero_min = 0;
                                                                    }

                                                                    builder.addText(' - da: ' + fisco[key].numero_min + ' a: ' + fisco[key].numero_max);

                                                                    incassi_video += ' - da: ' + fisco[key].numero_min + ' a: ' + fisco[key].numero_max;
                                                                    incassi_video += '</td>';
                                                                    builder.addTextPosition(390);
                                                                    builder.addText('€ ' + parseFloat(fisco[key].incasso).toFixed(2) + '\n');
                                                                    incassi_video += '<td>';
                                                                    incassi_video += '€ ' + parseFloat(fisco[key].incasso).toFixed(2) + '';
                                                                    incassi_video += '</td>';
                                                                    builder.addText('\n\n');
                                                                    incassi_video += '<td>';
                                                                    incassi_video += '</td>';
                                                                }
                                                            }

                                                            incassi_video += '</tr>';

                                                            var totale_fiscalizzazione = 0;

                                                            for (var key in fisco_teste) {

                                                                if (key === "FATTURE") {

                                                                    riga++;
                                                                    array_chiusura.push({
                                                                        data_servizio: data_report,
                                                                        data_emissione: data_emissione,
                                                                        lingua_stampa: comanda.lingua_stampa,
                                                                        protocollo: protocollo,
                                                                        progressivo_riga: aggZero(riga, 4),
                                                                        operatore: comanda.operatore,
                                                                        //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                        tipo_record: '2',
                                                                        descrizione: key + "2",
                                                                        quantita: fisco[key].numero_max,
                                                                        importo: "",
                                                                        nome_locale: comanda.locale,
                                                                        fiscale_sn: 'n',
                                                                        nome_pc: comanda.nome_pc
                                                                    });

                                                                    if (obj_dati_ultima_chiusura['FISCALIZZAZIONE'] !== undefined && obj_dati_ultima_chiusura['FISCALIZZAZIONE'][key + "2"] !== undefined) {
                                                                        if (parseInt(fisco[key].numero_min) < parseInt(obj_dati_ultima_chiusura['FISCALIZZAZIONE'][key + "2"].quantita)) {
                                                                            fisco[key].numero_min = (parseInt(obj_dati_ultima_chiusura['FISCALIZZAZIONE'][key + "2"].quantita) + 1).toString();
                                                                        }

                                                                        if (fisco[key].numero_max < fisco[key].numero_min) {
                                                                            fisco[key].numero_min = "0";
                                                                            fisco[key].numero_max = fisco[key].numero_min;
                                                                        }
                                                                    }
                                                                }

                                                                var el = fisco_teste[key];

                                                                builder.addText(key);
                                                                incassi_video += '<tr>';
                                                                incassi_video += '<td>';
                                                                incassi_video += key;


                                                                if (key === "FATTURE") {
                                                                    builder.addText(' (da: ' + fisco['FATTURE'].numero_min + ' a: ' + fisco['FATTURE'].numero_max + ')');

                                                                    incassi_video += ' (da: ' + fisco['FATTURE'].numero_min + ' a: ' + fisco['FATTURE'].numero_max + ')';
                                                                    incassi_video += '</td>';
                                                                }

                                                                builder.addTextPosition(310);

                                                                var numero = parseInt(el.numero);
                                                                if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['FISCALIZZAZIONE'][key] !== undefined) {
                                                                    numero = parseInt(parseInt(numero) - parseInt(obj_dati_ultima_chiusura['FISCALIZZAZIONE'][key].totale_quantita));
                                                                }
                                                                incassi_video += '<td>';
                                                                builder.addText(numero);
                                                                incassi_video += numero;
                                                                incassi_video += '</td>';

                                                                builder.addTextPosition(390);

                                                                var importo = parseFloat(el.importo).toFixed(2);
                                                                if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['FISCALIZZAZIONE'][key] !== undefined) {
                                                                    importo = parseFloat(parseFloat(importo) - parseFloat(obj_dati_ultima_chiusura['FISCALIZZAZIONE'][key].totale_importo)).toFixed(2);
                                                                }
                                                                builder.addText('€ ' + parseFloat(importo).toFixed(2));
                                                                incassi_video += '<td  style="text-align:right;">';
                                                                incassi_video += '€ ' + parseFloat(importo).toFixed(2);
                                                                incassi_video += '</td>';
                                                                incassi_video += '</tr>';


                                                                //------------------------------------------------//
                                                                riga++;
                                                                array_chiusura.push({
                                                                    data_servizio: data_report,
                                                                    data_emissione: data_emissione,
                                                                    lingua_stampa: comanda.lingua_stampa,
                                                                    protocollo: protocollo,
                                                                    progressivo_riga: aggZero(riga, 4),
                                                                    operatore: comanda.operatore,
                                                                    //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                    tipo_record: '2',
                                                                    descrizione: key,
                                                                    quantita: numero,
                                                                    importo: importo,
                                                                    nome_locale: comanda.locale,
                                                                    fiscale_sn: 'n',
                                                                    nome_pc: comanda.nome_pc
                                                                });
                                                                //------------------------------------------------//
                                                                totale_fiscalizzazione += parseFloat(el.importo);

                                                                builder.addText('\n');

                                                            }
                                                            builder.addFeedLine(1);
                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                            builder.addText('TOTALE FISCALE');
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td>';
                                                            incassi_video += '<strong>TOTALE FISCALE</strong>';
                                                            incassi_video += '</td>';

                                                            incassi_video += '<td>';
                                                            incassi_video += '</td>';

                                                            incassi_video += '<td  style="text-align:right;">';
                                                            builder.addTextPosition(390);

                                                            var fiscalizzazione = parseFloat(totale_fiscalizzazione).toFixed(2);
                                                            if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['FISCALIZZAZIONE']['TOTALE FISCALE'] !== undefined) {
                                                                fiscalizzazione = parseFloat(parseFloat(fiscalizzazione) - parseFloat(obj_dati_ultima_chiusura['FISCALIZZAZIONE']['TOTALE FISCALE'].totale_importo)).toFixed(2);
                                                            }


                                                            builder.addText('€ ' + fiscalizzazione + '\n');
                                                            incassi_video += '<strong>€ ' + fiscalizzazione + '</strong>';
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';
                                                            //------------------------------------------------//


                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '2',
                                                                descrizione: 'TOTALE FISCALE',
                                                                quantita: '',
                                                                importo: fiscalizzazione,
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });
                                                            //------------------------------------------------//
                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);

                                                            builder.addFeedLine(1);
                                                            builder.addTextAlign(builder.ALIGN_CENTER);

                                                            builder.addText('__________________________________________\n');
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td  colspan="3">';
                                                            incassi_video += '<hr>';
                                                            //incassi_video += '_________________________________________________________________________________________<br/>';
                                                            builder.addText('__________________________________________\n');
                                                            incassi_video += '<hr>';
                                                            //incassi_video += '_________________________________________________________________________________________';
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';
                                                            builder.addFeedLine(1);


                                                            builder.addTextAlign(builder.ALIGN_CENTER);



                                                            // FINE FISCALE TESTA

                                                            //-----NUMERI FATTURA E SCONTRINO-------

                                                            //QUERY WHERE STATO RECORD CHIUSO E FISCALIZZATA_SN = s AND DATA COME OGGI

                                                            //NEL CASO 1 LA QUERY E' ANCHE SE TIPO_RICEVUTA = fattura CAST AS INT LIMIT 1 ASC + LIMIT 1 DESC

                                                            //NEL CASO 2 LA QUERY E' SE TIPO_RICEVUTA = scontrino CAST AS INT LIMIT 1 ASC + LIMIT 1 DESC

                                                            //----FOR STATISTICO

                                                            //INDEX
                                                            //.INCASSO

                                                            if (comanda.numero_licenza_cliente === "0561") {

                                                                //---STATISTICO---
                                                                builder.addTextSize(2, 2);
                                                                builder.addText('PER PUNTO CASSA\n\n');
                                                                incassi_video += '<tr>';
                                                                incassi_video += '<td style="text-align: center;" colspan="3">';
                                                                incassi_video += '<strong>PER PUNTO CASSA</strong>';
                                                                incassi_video += '</td>';
                                                                incassi_video += '</tr>';
                                                                //-----------------------------------------------
                                                                -//
                                                                    riga++;
                                                                array_chiusura.push({
                                                                    data_servizio: data_report,
                                                                    data_emissione: data_emissione,
                                                                    lingua_stampa: comanda.lingua_stampa,
                                                                    protocollo: protocollo,
                                                                    progressivo_riga: aggZero(riga, 4),
                                                                    operatore: comanda.operatore,
                                                                    //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                    tipo_record: '1',
                                                                    descrizione: 'PER PUNTO CASSA',
                                                                    quantita: '',
                                                                    importo: '',
                                                                    nome_locale: comanda.locale,
                                                                    fiscale_sn: 'n',
                                                                    nome_pc: comanda.nome_pc
                                                                });
                                                                //------------------------------------------------//
                                                                builder.addTextAlign(builder.ALIGN_LEFT);
                                                                builder.addTextSize(1, 1);
                                                                /*for (var key in fisco) {
                                                                 
                                                                 builder.addText(key);
                                                                 if (fisco[key].numero_min === null)
                                                                 {
                                                                 fisco[key].numero_min = 0;
                                                                 }
                                                                 
                                                                 builder.addText(' - da: ' + fisco[key].numero_min + ' a: ' + fisco[key].numero_max);
                                                                 builder.addTextPosition(390);
                                                                 builder.addText('€ ' + parseFloat(fisco[key].incasso).toFixed(2) + '\n');
                                                                 }*/

                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);

                                                                builder.addText('TIPOLOGIA');
                                                                incassi_video += '<tr>';
                                                                incassi_video += '<td>';
                                                                incassi_video += '<strong>TIPOLOGIA</strong>';
                                                                incassi_video += '</td>';
                                                                builder.addTextPosition(310);

                                                                builder.addText("QTA");
                                                                incassi_video += '<td>';
                                                                incassi_video += "<strong>QTA</strong>";
                                                                incassi_video += '</td>';

                                                                builder.addTextPosition(390);
                                                                builder.addText("IMPORTO");
                                                                incassi_video += '<td  style="text-align:right;">';
                                                                incassi_video += "<strong>IMPORTO</strong>";
                                                                incassi_video += '</td>';
                                                                incassi_video += '</tr>';


                                                                builder.addText('\n\n');
                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);

                                                                var totale_fiscalizzazione = 0;

                                                                for (var key in per_tavolo_brove) {
                                                                    var el = per_tavolo_brove[key];

                                                                    //Temporaneo per Brovedani
                                                                    var tavolo_rinominato = '';
                                                                    switch (key) {
                                                                        case "CONTINUO_1":
                                                                            tavolo_rinominato = "GREEN CORNER";
                                                                            break;
                                                                        case "CONTINUO_2":
                                                                            tavolo_rinominato = "FOCACCERIA";
                                                                            break;
                                                                        case "CONTINUO_3":
                                                                            tavolo_rinominato = "TRAMEZZINI";
                                                                            break;
                                                                    }


                                                                    builder.addText(tavolo_rinominato);
                                                                    incassi_video += '<tr>';
                                                                    incassi_video += '<td>';
                                                                    incassi_video += tavolo_rinominato;
                                                                    incassi_video += '</td>';

                                                                    builder.addTextPosition(310);

                                                                    var numero = parseInt(el.numero);
                                                                    if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['PER PUNTO CASSA'][key] !== undefined) {
                                                                        numero = parseInt(parseInt(numero) - parseInt(obj_dati_ultima_chiusura['PER PUNTO CASSA'][key].totale_quantita));
                                                                    }
                                                                    builder.addText(numero);
                                                                    incassi_video += '<td>';
                                                                    incassi_video += numero;
                                                                    incassi_video += '</td>';

                                                                    builder.addTextPosition(390);

                                                                    var importo = parseFloat(el.importo).toFixed(2);
                                                                    if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['PER PUNTO CASSA'][key] !== undefined) {
                                                                        importo = parseFloat(parseFloat(importo) - parseFloat(obj_dati_ultima_chiusura['PER PUNTO CASSA'][key].totale_importo)).toFixed(2);
                                                                    }
                                                                    builder.addText('€ ' + parseFloat(importo).toFixed(2));
                                                                    incassi_video += '<td  style="text-align:right;">';
                                                                    incassi_video += '€ ' + parseFloat(importo).toFixed(2);
                                                                    incassi_video += '</td>';
                                                                    incassi_video += '</tr>';


                                                                    //------------------------------------------------//
                                                                    riga++;
                                                                    array_chiusura.push({
                                                                        data_servizio: data_report,
                                                                        data_emissione: data_emissione,
                                                                        lingua_stampa: comanda.lingua_stampa,
                                                                        protocollo: protocollo,
                                                                        progressivo_riga: aggZero(riga, 4),
                                                                        operatore: comanda.operatore,
                                                                        //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                        tipo_record: '2',
                                                                        descrizione: key,
                                                                        quantita: numero,
                                                                        importo: importo,
                                                                        nome_locale: comanda.locale,
                                                                        fiscale_sn: 'n',
                                                                        nome_pc: comanda.nome_pc
                                                                    });
                                                                    //------------------------------------------------//
                                                                    totale_fiscalizzazione += parseFloat(el.importo);

                                                                    builder.addText('\n');

                                                                }
                                                                builder.addFeedLine(1);
                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                builder.addText('TOTALE GENERALE');
                                                                incassi_video += '<tr>';
                                                                incassi_video += '<td>';
                                                                incassi_video += '<strong>TOTALE GENERALE</strong>';
                                                                incassi_video += '</td>';
                                                                builder.addTextPosition(390);

                                                                var fiscalizzazione = parseFloat(totale_fiscalizzazione).toFixed(2);
                                                                if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['PER PUNTO CASSA']['TOTALE GENERALE'] !== undefined) {
                                                                    fiscalizzazione = parseFloat(parseFloat(fiscalizzazione) - parseFloat(obj_dati_ultima_chiusura['PER PUNTO CASSA']['TOTALE GENERALE'].totale_importo)).toFixed(2);
                                                                }


                                                                builder.addText('€ ' + fiscalizzazione + '\n');
                                                                incassi_video += '<td>';
                                                                incassi_video += '</td>';
                                                                incassi_video += '<td  style="text-align:right;">';
                                                                incassi_video += '<strong>€ ' + fiscalizzazione + '</strong>';
                                                                incassi_video += '</td>';
                                                                incassi_video += '</tr>';
                                                                //------------------------------------------------//


                                                                riga++;
                                                                array_chiusura.push({
                                                                    data_servizio: data_report,
                                                                    data_emissione: data_emissione,
                                                                    lingua_stampa: comanda.lingua_stampa,
                                                                    protocollo: protocollo,
                                                                    progressivo_riga: aggZero(riga, 4),
                                                                    operatore: comanda.operatore,
                                                                    //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                    tipo_record: '2',
                                                                    descrizione: 'TOTALE GENERALE',
                                                                    quantita: '',
                                                                    importo: fiscalizzazione,
                                                                    nome_locale: comanda.locale,
                                                                    fiscale_sn: 'n',
                                                                    nome_pc: comanda.nome_pc
                                                                });
                                                                //------------------------------------------------//
                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);

                                                                builder.addFeedLine(1);
                                                                builder.addTextAlign(builder.ALIGN_CENTER);

                                                                builder.addText('__________________________________________\n');
                                                                incassi_video += '<tr>';
                                                                incassi_video += '<td  colspan="3"> ';
                                                                incassi_video += '<hr>';
                                                                //incassi_video += '_________________________________________________________________________________________<br/>';
                                                                builder.addText('__________________________________________\n');
                                                                incassi_video += '<hr>';
                                                                //incassi_video += '_________________________________________________________________________________________';
                                                                incassi_video += '</td>';
                                                                incassi_video += '</tr>';
                                                                builder.addFeedLine(1);

                                                                builder.addTextAlign(builder.ALIGN_CENTER);

                                                                builder.addText(data + ' - ' + ora + '\n');
                                                                incassi_video += '<tr>';
                                                                incassi_video += '<td>';
                                                                incassi_video += data + ' - ' + ora + '';
                                                                incassi_video += '</td>';
                                                                incassi_video += '</tr>';

                                                                /*for (var key in statistico) {
                                                                 
                                                                 builder.addText(key);
                                                                 builder.addTextPosition(390);
                                                                 builder.addText('€ ' + parseFloat(statistico[key].incasso).toFixed(2) + '\n');
                                                                 
                                                                 }*/



                                                                //---STATISTICO---

                                                                //----FOR OPERAZIONI_OPERATORE


                                                                incassi_video += '</table>';
                                                            }

                                                        }

                                                        //ARTICOLI TOTALI

                                                        //articolo[obj.portata][obj.dest_stampa][obj.categoria][obj.descrizione][obj.quantita][obj.prezzo_un]



                                                        builder.addTextSize(1, 1);
                                                        builder.addTextStyle(false, false, true, builder.COLOR_1);

                                                        if (!(layout === "soloincassi" && extra !== 'giornata' && extra !== 'PUNTOCASSA')) {
                                                            if (settaggi_profili.Field81.length > 0) {
                                                                switch (settaggi_profili.Field82) {
                                                                    case "G":
                                                                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                        break;
                                                                    case "N":
                                                                    default:
                                                                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                }
                                                                builder.addText(settaggi_profili.Field81 + '\n');
                                                                incassi_video += settaggi_profili.Field81 + '';
                                                                if (settaggi_profili.Field83 === 'true') {
                                                                    builder.addFeedLine(1);
                                                                }
                                                            }
                                                        }


                                                        if (settaggi_profili.Field84.length > 0) {
                                                            switch (settaggi_profili.Field85) {
                                                                case "G":
                                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                    break;
                                                                case "N":
                                                                default:
                                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                            }
                                                            builder.addText(settaggi_profili.Field84 + '\n');
                                                            incassi_video += settaggi_profili.Field84 + '';
                                                            if (settaggi_profili.Field86 === 'true') {
                                                                builder.addFeedLine(1);
                                                            }
                                                        }


                                                        if (settaggi_profili.Field87.length > 0) {
                                                            switch (settaggi_profili.Field88) {
                                                                case "G":
                                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                    break;
                                                                case "N":
                                                                default:
                                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                            }
                                                            builder.addText(settaggi_profili.Field87 + '\n');
                                                            incassi_video += settaggi_profili.Field87 + '';
                                                            if (settaggi_profili.Field89 === 'true') {
                                                                builder.addFeedLine(1);
                                                            }
                                                        }




                                                        builder.addTextStyle(false, false, false, builder.COLOR_1);

                                                        if (settaggi_profili.Field97 === 'true') {

                                                            builder.addTextAlign(builder.ALIGN_CENTER);
                                                            builder.addFeedLine(2);
                                                            builder.addTextSize(2, 2);

                                                            builder.addText('TOTALI PER SERVIZIO\n\n');
                                                            incassi_video += 'TOTALI PER SERVIZIO';
                                                            //------------------------------------------------//
                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '1',
                                                                descrizione: 'TOTALI PER SERVIZIO',
                                                                quantita: '',
                                                                importo: '',
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });
                                                            //------------------------------------------------//
                                                            builder.addTextSize(1, 1);
                                                            builder.addTextAlign(builder.ALIGN_CENTER);

                                                            builder.addText('__________________________________________\n\n');
                                                            incassi_video += '<hr>';
                                                            //incassi_video += '_________________________________________________________________________________________';

                                                            builder.addTextAlign(builder.ALIGN_LEFT);

                                                            var trasformazione_statistico_array = new Array();
                                                            trasformazione_statistico_array[0] = { servizio: 'BAR', incasso: statistico['BAR'].incasso };
                                                            trasformazione_statistico_array[1] = { servizio: 'TAKEAWAY', incasso: statistico['TAKEAWAY'].incasso };
                                                            trasformazione_statistico_array[2] = { servizio: 'RISTORAZIONE', incasso: statistico['RISTORAZIONE'].incasso };


                                                            var statistico_array = riordina(trasformazione_statistico_array, 'incasso', 'float', true, false);
                                                            for (var key in statistico_array) {

                                                                builder.addText(statistico_array[key].servizio);
                                                                incassi_video += statistico_array[key].servizio;
                                                                builder.addTextPosition(390);

                                                                var incasso = parseFloat(statistico_array[key].incasso).toFixed(2);
                                                                if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['TOTALI PER SERVIZIO'][statistico_array[key].servizio] !== undefined) {
                                                                    incasso = parseFloat(parseFloat(incasso) - parseFloat(obj_dati_ultima_chiusura['TOTALI PER SERVIZIO'][statistico_array[key].servizio].totale_importo)).toFixed(2);
                                                                }

                                                                builder.addText('€ ' + incasso + '\n');
                                                                incassi_video += '€ ' + incasso + '';

                                                                //------------------------------------------------//
                                                                riga++;
                                                                array_chiusura.push({
                                                                    data_servizio: data_report,
                                                                    data_emissione: data_emissione,
                                                                    lingua_stampa: comanda.lingua_stampa,
                                                                    protocollo: protocollo,
                                                                    progressivo_riga: aggZero(riga, 4),
                                                                    operatore: comanda.operatore,
                                                                    //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                    tipo_record: '2',
                                                                    descrizione: statistico_array[key].servizio,
                                                                    quantita: '',
                                                                    importo: incasso,
                                                                    nome_locale: comanda.locale,
                                                                    fiscale_sn: 'n',
                                                                    nome_pc: comanda.nome_pc
                                                                });
                                                                //------------------------------------------------//

                                                            }
                                                            builder.addTextAlign(builder.ALIGN_CENTER);


                                                            builder.addText('__________________________________________\n');
                                                            incassi_video += '<hr>';
                                                            //incassi_video += '_________________________________________________________________________________________<br/>';
                                                            builder.addText('__________________________________________\n\n');
                                                            incassi_video += '<hr>';
                                                            //incassi_video += '_________________________________________________________________________________________';
                                                        }


                                                        if (settaggi_profili.Field98 === 'true') {
                                                            console.log("STAMPA TOTALI OPERAZIONI_OPERATORE", operazioni_operatore);
                                                            builder.addTextSize(2, 2);
                                                            builder.addTextAlign(builder.ALIGN_CENTER);
                                                            builder.addFeedLine(1);
                                                            builder.addText('TOTALI PER OPERATORE\n\n');
                                                            incassi_video += 'TOTALI PER OPERATORE';
                                                            //------------------------------------------------//
                                                            riga++;
                                                            array_chiusura.push({
                                                                data_servizio: data_report,
                                                                data_emissione: data_emissione,
                                                                lingua_stampa: comanda.lingua_stampa,
                                                                protocollo: protocollo,
                                                                progressivo_riga: aggZero(riga, 4),
                                                                operatore: comanda.operatore,
                                                                //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                tipo_record: '1',
                                                                descrizione: 'TOTALI PER OPERATORE',
                                                                quantita: '',
                                                                importo: '',
                                                                nome_locale: comanda.locale,
                                                                fiscale_sn: 'n',
                                                                nome_pc: comanda.nome_pc
                                                            });
                                                            //------------------------------------------------//
                                                            builder.addTextSize(1, 1);
                                                            builder.addTextAlign(builder.ALIGN_CENTER);

                                                            builder.addText('__________________________________________\n\n');
                                                            incassi_video += '<hr>';
                                                            //incassi_video += '_________________________________________________________________________________________';
                                                            builder.addTextSize(1, 1);

                                                            builder.addTextAlign(builder.ALIGN_LEFT);

                                                            var trasf_array_operatore = new Array();

                                                            for (var key in operazioni_operatore) {

                                                                trasf_array_operatore.push({ operatore: operazioni_operatore[key].operatore, incasso: operazioni_operatore[key].incasso });

                                                            }

                                                            var statistico_operatore = riordina(trasf_array_operatore, 'incasso', 'float', true, false);

                                                            for (var key in statistico_operatore) {

                                                                builder.addText(statistico_operatore[key].operatore);
                                                                incassi_video += statistico_operatore[key].operatore;
                                                                builder.addTextPosition(390);

                                                                var incasso = parseFloat(statistico_operatore[key].incasso).toFixed(2);
                                                                if (obj_dati_ultima_chiusura !== false && obj_dati_ultima_chiusura['TOTALI PER OPERATORE'][statistico_operatore[key].operatore] !== undefined) {
                                                                    incasso = parseFloat(parseFloat(incasso) - parseFloat(obj_dati_ultima_chiusura['TOTALI PER OPERATORE'][statistico_operatore[key].operatore].totale_importo)).toFixed(2);
                                                                }
                                                                builder.addText('€ ' + incasso + '\n');
                                                                incassi_video += '€ ' + incasso + '';

                                                                //------------------------------------------------//
                                                                riga++;
                                                                array_chiusura.push({
                                                                    data_servizio: data_report,
                                                                    data_emissione: data_emissione,
                                                                    lingua_stampa: comanda.lingua_stampa,
                                                                    protocollo: protocollo,
                                                                    progressivo_riga: aggZero(riga, 4),
                                                                    operatore: comanda.operatore,
                                                                    //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                                    tipo_record: '2',
                                                                    descrizione: statistico_operatore[key].operatore,
                                                                    quantita: '',
                                                                    importo: incasso,
                                                                    nome_locale: comanda.locale,
                                                                    fiscale_sn: 'n',
                                                                    nome_pc: comanda.nome_pc
                                                                });
                                                                //------------------------------------------------//

                                                            }
                                                            builder.addTextAlign(builder.ALIGN_CENTER);

                                                            builder.addText('__________________________________________\n');
                                                            incassi_video += '<hr>';
                                                            //incassi_video += '_________________________________________________________________________________________<br/>';
                                                            builder.addText('__________________________________________\n\n');
                                                            incassi_video += '<hr>';
                                                            //incassi_video += '_________________________________________________________________________________________';
                                                        }


                                                        if (layout === 'dettagliovenduti' || layout === 'misto') {

                                                            builder.addFeedLine(1);
                                                            builder.addTextSize(2, 2);
                                                            builder.addTextAlign(builder.ALIGN_CENTER);
                                                            builder.addText('DETTAGLIO VENDUTI\n');
                                                            incassi_video += 'DETTAGLIO VENDUTI';
                                                            builder.addFeedLine(1);

                                                            builder.addTextSize(1, 1);
                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                            builder.addText('\n' + filtri_stampa + '\n');
                                                            incassi_video += '<tr>';
                                                            incassi_video += '<td style="text-align: center;" colspan="3">';
                                                            incassi_video += '' + filtri_stampa + '';
                                                            incassi_video += '</td>';
                                                            incassi_video += '</tr>';

                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);

                                                            builder.addTextSize(1, 1);



                                                            //TOTALE GENERICO

                                                            var totale_generico = 0;
                                                            var totale_quantita_generica = 0;


                                                            var totale_dest_stampa = 0;

                                                            var articolo_generico_2;
                                                            var decrescente;

                                                            switch (settaggi_profili.Field96) {
                                                                case "C":
                                                                    decrescente = false;
                                                                    break;

                                                                case "D":
                                                                default:
                                                                    decrescente = true;
                                                            }

                                                            //default descrizione
                                                            var ordinamento = "descrizione";

                                                            switch (settaggi_profili.Field95) {

                                                                case "Q":
                                                                    articolo_generico_2 = riordina(articolo_generico, 'quantita_tot', 'int', decrescente, ordinamento);
                                                                    break;

                                                                case "D":
                                                                    articolo_generico_2 = riordina(articolo_generico, 'descrizione', 'default', decrescente, ordinamento);
                                                                    break;

                                                                case "T":
                                                                default:
                                                                    articolo_generico_2 = riordina(articolo_generico, 'prezzo', 'float', decrescente, ordinamento);
                                                            }

                                                            for (var num in articolo_generico_2) {

                                                                var obj_art = articolo_generico_2[num];

                                                                totale_quantita_generica += parseInt(obj_art.quantita_tot);

                                                                totale_generico += parseFloat(obj_art.prezzo);
                                                                totale_dest_stampa += parseFloat(obj_art.prezzo);

                                                            }

                                                            //FINE TOTALE GENERICO

                                                            if (comanda.numero_licenza_cliente !== "0638" && settaggi_profili.Field99 === 'true' && !(facoltativo_categoria !== undefined && typeof (facoltativo_categoria) === 'string')) {

                                                                builder.addText('__________________________________________\n\n');
                                                                incassi_video += '<hr>';
                                                                //incassi_video += '_________________________________________________________________________________________';
                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                builder.addText('TOTALE GENERALE\n\n');
                                                                incassi_video += 'TOTALE GENERALE';
                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                builder.addTextAlign(builder.ALIGN_LEFT);
                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                builder.addTextPosition(0);
                                                                builder.addText('DESCRIZIONE');
                                                                incassi_video += 'DESCRIZIONE';
                                                                builder.addTextPosition(310);
                                                                builder.addText('QTA');
                                                                incassi_video += 'QTA';
                                                                if (false) {
                                                                    builder.addTextPosition(310);
                                                                    builder.addText('UNIT.');
                                                                    incassi_video += 'UNIT.';
                                                                }
                                                                builder.addTextPosition(390);
                                                                builder.addText('TOTALE\n\n');
                                                                incassi_video += 'TOTALE';
                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                var totale_generico = 0;
                                                                var totale_quantita_generica = 0;
                                                                //for (var portata in articolo) {

                                                                //for (var dest_stampa in articolo[portata]) {

                                                                var totale_dest_stampa = 0;
                                                                var builder_destinazione = new epson.ePOSBuilder();


                                                                builder_destinazione.addTextFont(builder.FONT_A);
                                                                builder_destinazione.addTextAlign(builder.ALIGN_CENTER);
                                                                builder_destinazione.addTextSize(2, 2);
                                                                builder_destinazione.addTextStyle(false, false, true, builder.COLOR_1);
                                                                if (set_disabilita_raggruppamento_destinazione !== true) {
                                                                    //builder_destinazione.addText(comanda.nome_stampante[dest_stampa].nome_umano + '\n\n');
                                                                }
                                                                builder_destinazione.addTextStyle(false, false, false, builder.COLOR_1);
                                                                builder_destinazione.addTextSize(1, 1);
                                                                builder_destinazione.addText(data + '\n\n');
                                                                builder_destinazione.addTextAlign(builder.ALIGN_LEFT);
                                                                builder_destinazione.addTextStyle(false, false, true, builder.COLOR_1);
                                                                builder_destinazione.addTextPosition(0);
                                                                builder_destinazione.addText('DESCRIZIONE');

                                                                builder_destinazione.addTextPosition(310);
                                                                builder_destinazione.addText('QTA');

                                                                //Temporaneamente disabilitato per questioni di spazio
                                                                if (false) {
                                                                    builder_destinazione.addTextPosition(310);
                                                                    builder_destinazione.addText('UNIT.');
                                                                }
                                                                builder_destinazione.addTextPosition(390);
                                                                builder_destinazione.addText('TOTALE\n\n');
                                                                builder_destinazione.addTextStyle(false, false, false, builder.COLOR_1);
                                                                //  for (var categoria in articolo[portata][dest_stampa]) {

                                                                //builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                //builder.addText(nomi_categorie[categoria] + '\n\n');
                                                                //builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                //  for (var sconto_perc in articolo[portata][dest_stampa][categoria]) {

                                                                //   for (var descrizione in articolo[portata][dest_stampa][categoria][sconto_perc]) {


                                                                var articolo_generico_2;
                                                                var decrescente;

                                                                switch (settaggi_profili.Field96) {
                                                                    case "C":
                                                                        decrescente = false;
                                                                        break;

                                                                    case "D":
                                                                    default:
                                                                        decrescente = true;
                                                                }

                                                                //default descrizione
                                                                var ordinamento = "descrizione";

                                                                switch (settaggi_profili.Field95) {

                                                                    case "Q":
                                                                        articolo_generico_2 = riordina(articolo_generico, 'quantita_tot', 'int', decrescente, ordinamento);
                                                                        break;

                                                                    case "D":
                                                                        articolo_generico_2 = riordina(articolo_generico, 'descrizione', 'default', decrescente, ordinamento);
                                                                        break;

                                                                    case "T":
                                                                    default:
                                                                        articolo_generico_2 = riordina(articolo_generico, 'prezzo', 'float', decrescente, ordinamento);
                                                                }

                                                                for (var num in articolo_generico_2) {

                                                                    var obj_art = articolo_generico_2[num];
                                                                    //for (var quantita in articolo[portata][dest_stampa][categoria][sconto_perc][descrizione]) {

                                                                    //for (var prezzo_un in articolo[portata][dest_stampa][categoria][sconto_perc][descrizione][quantita]) {

                                                                    //var obj = articolo[portata][dest_stampa][categoria][sconto_perc][descrizione][quantita][prezzo_un];
                                                                    builder.addTextAlign(builder.ALIGN_LEFT);
                                                                    builder.addTextPosition(0);
                                                                    //if (obj.sconto_perc === 0 || obj.sconto_perc === "0.00") {
                                                                    builder.addText(obj_art.descrizione);
                                                                    incassi_video += obj_art.descrizione;
                                                                    //} else
                                                                    //{
                                                                    //    builder.addText(obj.desc_art + ' ' + obj.sconto_perc.substr(0, 2) + ' %');
                                                                    //}
                                                                    builder.addTextPosition(310);
                                                                    builder.addText(obj_art.quantita_tot);
                                                                    incassi_video += obj_art.quantita_tot;
                                                                    totale_quantita_generica += parseInt(obj_art.quantita_tot);
                                                                    //Temporaneamente disabilitato per questioni di spazio
                                                                    if (false) {
                                                                        builder.addTextPosition(310);
                                                                        builder.addText('€ ' + obj.prezzo_un);
                                                                        incassi_video += '€ ' + obj.prezzo_un;
                                                                    }
                                                                    builder.addTextPosition(390);
                                                                    builder.addTextAlign(builder.ALIGN_RIGHT);
                                                                    builder.addText('€ ' + parseFloat(obj_art.prezzo).toFixed(2) + '\n');
                                                                    incassi_video += '€ ' + parseFloat(obj_art.prezzo).toFixed(2) + '';
                                                                    totale_generico += parseFloat(obj_art.prezzo);
                                                                    totale_dest_stampa += parseFloat(obj_art.prezzo);
                                                                    builder_destinazione.addTextAlign(builder.ALIGN_LEFT);
                                                                }
                                                                //
                                                                //}
                                                                //}
                                                                // }
                                                                // }
                                                                // }



                                                                builder_destinazione.addTextStyle(false, false, true, builder.COLOR_1);
                                                                builder_destinazione.addTextAlign(builder.ALIGN_LEFT);
                                                                builder_destinazione.addFeedLine(1);
                                                                builder_destinazione.addText('TOTALE:');
                                                                incassi_video += 'TOTALE:';
                                                                builder_destinazione.addTextPosition(310);
                                                                builder_destinazione.addText(totale_quantita_reparto);
                                                                incassi_video += totale_quantita_reparto;
                                                                builder_destinazione.addTextPosition(390);
                                                                builder_destinazione.addText('€ ' + parseFloat(totale_dest_stampa).toFixed(2) + '\n');
                                                                incassi_video += '€ ' + parseFloat(totale_dest_stampa).toFixed(2) + '';
                                                                builder_destinazione.addTextStyle(false, false, false, builder.COLOR_1);
                                                                //FINE CORPO SCONTRINO TOTALI
                                                                builder_destinazione.addFeedLine(1);
                                                                builder_destinazione.addFeedLine(2);
                                                                builder_destinazione.addCut(builder.CUT_FEED);
                                                                var request_destinazione = builder_destinazione.toString();
                                                                //CONTENUTO CONTO
                                                                //console.log(request);

                                                                //Create a SOAP envelop
                                                                var soap_destinazione = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request_destinazione + '</s:Body></s:Envelope>';
                                                                //Create an XMLHttpRequest object
                                                                var xhr_destinazione = new XMLHttpRequest();
                                                                //Set the end point address
                                                                var url_destinazione = 'http://' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                                                                //Open an XMLHttpRequest object
                                                                if (comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].intelligent === "SDS") {
                                                                    url_destinazione = 'http://' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "") + numero_layout].ip + ':8000/SPOOLER?stampante=' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].devid_nf;
                                                                }

                                                                xhr_destinazione.open('POST', url_destinazione, true);
                                                                //<Header settings>
                                                                xhr_destinazione.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
                                                                xhr_destinazione.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
                                                                xhr_destinazione.setRequestHeader('SOAPAction', '""');
                                                                // Send print document
                                                                //RIABILITARE
                                                                console.log("STAMPA TOTALI INVIATA 597", soap);
                                                                //Temporaneamente disabilitato per non consumare carta
                                                                //xhr_destinazione.send(soap_destinazione);


                                                                //}


                                                                //}

                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                builder.addTextAlign(builder.ALIGN_LEFT);
                                                                builder.addFeedLine(1);
                                                                builder.addText('TOTALE:');
                                                                incassi_video += 'TOTALE:';
                                                                builder.addTextPosition(310);
                                                                builder.addText(totale_quantita_generica);
                                                                incassi_video += totale_quantita_generica;
                                                                builder.addTextPosition(390);
                                                                builder.addText('€ ' + parseFloat(totale_generico).toFixed(2) + '\n\n');
                                                                incassi_video += '€ ' + parseFloat(totale_generico).toFixed(2) + '<br/';
                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                builder.addTextAlign(builder.ALIGN_CENTER);

                                                                builder.addText('__________________________________________\n');
                                                                incassi_video += '<hr>';
                                                                //incassi_video += '_________________________________________________________________________________________<br/>';
                                                                builder.addText('__________________________________________\n\n');
                                                                incassi_video += '<hr>';
                                                                //incassi_video += '_________________________________________________________________________________________';
                                                            }




                                                            if (settaggi_profili.Field101 === 'true') {
                                                                builder.addTextAlign(builder.ALIGN_CENTER);
                                                                builder.addTextSize(2, 2);
                                                                builder.addFeedLine(1);

                                                                builder.addText('ORD. PER DESTINAZIONE\n\n');
                                                                incassi_video += 'ORD. PER DESTINAZIONE';
                                                                builder.addTextSize(1, 1);



                                                                var nome_colonna = 'destinazione';//destinazione o categoria

                                                                var array_codici_univoci = estrazione_univoca_colonna(articolo_generico, nome_colonna);
                                                                var array_percentuali = new Array();
                                                                var contatore_categorie = 0;
                                                                var contatore_stampanti = 1;
                                                                for (var codice_univoco in array_codici_univoci) {

                                                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                                                    builder.addText('__________________________________________\n\n');
                                                                    incassi_video += '<hr>';
                                                                    //                                                incassi_video += '_________________________________________________________________________________________';
                                                                    builder.addTextAlign(builder.ALIGN_LEFT);


                                                                    var totale_quantita_reparto = 0;
                                                                    var totale_dest_stampa = 0;


                                                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);

                                                                    var nome_stampante;
                                                                    if (comanda.nome_stampante[array_codici_univoci[codice_univoco]] !== undefined) {
                                                                        nome_stampante = comanda.nome_stampante[array_codici_univoci[codice_univoco]].nome_umano;
                                                                        builder.addText(nome_stampante + '\n');
                                                                        incassi_video += nome_stampante + '';
                                                                    } else {
                                                                        nome_stampante = 'STAMPANTE CANCELLATA ' + contatore_stampanti;
                                                                        builder.addText(nome_stampante + '\n');
                                                                        incassi_video += nome_stampante + '';
                                                                        contatore_stampanti++;
                                                                    }

                                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);



                                                                    var totale_prezzo_categoria = 0;
                                                                    var totale_quantita_categoria = 0;


                                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);

                                                                    builder.addTextAlign(builder.ALIGN_CENTER);


                                                                    if (contatore_categorie === 0) {
                                                                        builder.addTextAlign(builder.ALIGN_LEFT);

                                                                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                        builder.addTextPosition(0);
                                                                        builder.addText('DESCRIZIONE');
                                                                        incassi_video += 'DESCRIZIONE';
                                                                        builder.addTextPosition(310);
                                                                        builder.addText('QTA');
                                                                        incassi_video += 'QTA';

                                                                        builder.addTextPosition(390);
                                                                        builder.addText('TOTALE\n\n');
                                                                        incassi_video += 'TOTALE';
                                                                        contatore_categorie = 1;
                                                                    }
                                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);


                                                                    //for (var sconto_perc in articolo[portata][dest_stampa][categoria]) {

                                                                    //for (var descrizione in articolo[portata][dest_stampa][categoria][sconto_perc]) {

                                                                    // for (var quantita in articolo[portata][dest_stampa][categoria][sconto_perc][descrizione]) {

                                                                    //   for (var prezzo_un in articolo[portata][dest_stampa][categoria][sconto_perc][descrizione][quantita]) {

                                                                    //var obj = articolo[portata][dest_stampa][categoria][sconto_perc][descrizione][quantita][prezzo_un];

                                                                    builder.addTextAlign(builder.ALIGN_LEFT);

                                                                    builder.addTextPosition(0);
                                                                    var decrescente;

                                                                    switch (settaggi_profili.Field96) {
                                                                        case "C":
                                                                            decrescente = false;
                                                                            break;

                                                                        case "D":
                                                                        default:
                                                                            decrescente = true;
                                                                    }
                                                                    var array_suddiviso;
                                                                    //default descrizione
                                                                    var ordinamento = "descrizione";
                                                                    switch (settaggi_profili.Field95) {

                                                                        case "Q":
                                                                            array_suddiviso = estrazione_array(riordina(articolo_generico, 'quantita_tot', 'int', decrescente, ordinamento), nome_colonna, array_codici_univoci[codice_univoco]);
                                                                            break;

                                                                        case "D":
                                                                            array_suddiviso = estrazione_array(riordina(articolo_generico, 'descrizione', 'default', decrescente, ordinamento), nome_colonna, array_codici_univoci[codice_univoco]);
                                                                            break;

                                                                        case "T":
                                                                        default:
                                                                            array_suddiviso = estrazione_array(riordina(articolo_generico, 'prezzo', 'float', decrescente, ordinamento), nome_colonna, array_codici_univoci[codice_univoco]);
                                                                    }


                                                                    for (num in array_suddiviso) {

                                                                        var obj_art = array_suddiviso[num];

                                                                        //if (obj.sconto_perc === 0 || obj.sconto_perc === "0.00")
                                                                        builder.addText(obj_art.descrizione);
                                                                        incassi_video += obj_art.descrizione;
                                                                        //else
                                                                        //{
                                                                        //    builder.addText(obj.desc_art + ' ' + obj.sconto_perc.substr(0, 2) + ' %');
                                                                        //}

                                                                        builder.addTextPosition(310);
                                                                        builder.addText(obj_art.quantita_tot);
                                                                        incassi_video += obj_art.quantita_tot;
                                                                        totale_prezzo_categoria += parseFloat(obj_art.prezzo);
                                                                        totale_quantita_categoria += parseInt(obj_art.quantita_tot);

                                                                        totale_quantita_reparto += parseInt(obj_art.quantita_tot);
                                                                        //Temporaneamente disabilitato per questioni di spazio
                                                                        if (false) {
                                                                            builder.addTextPosition(310);
                                                                            builder.addText('€ ' + obj.prezzo_un);
                                                                            incassi_video += '€ ' + obj.prezzo_un;
                                                                        }

                                                                        builder.addTextPosition(390);
                                                                        builder.addTextAlign(builder.ALIGN_RIGHT);
                                                                        builder.addText('€ ' + parseFloat(obj_art.prezzo).toFixed(2) + '\n');
                                                                        incassi_video += '€ ' + parseFloat(obj_art.prezzo).toFixed(2) + '';
                                                                        builder.addTextAlign(builder.ALIGN_LEFT);
                                                                        totale_dest_stampa += parseFloat(obj_art.prezzo);
                                                                    }
                                                                    //  }
                                                                    //}
                                                                    //}
                                                                    //}


                                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                    builder.addTextAlign(builder.ALIGN_LEFT);
                                                                    builder.addFeedLine(1);
                                                                    builder.addText('TOTALE REPARTO:');
                                                                    incassi_video += 'TOTALE REPARTO:';
                                                                    builder.addTextPosition(310);
                                                                    builder.addText(totale_quantita_reparto);
                                                                    incassi_video += totale_quantita_reparto;
                                                                    builder.addTextPosition(390);
                                                                    builder.addText('€ ' + parseFloat(totale_dest_stampa).toFixed(2) + '\n');
                                                                    incassi_video += '€ ' + parseFloat(totale_dest_stampa).toFixed(2) + '';
                                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);

                                                                    array_percentuali.push({ nome_categoria: nome_stampante, quantita_totale: totale_quantita_categoria, prezzo_totale: totale_prezzo_categoria, percentuale: (parseFloat(totale_dest_stampa) / parseFloat(totale_generico) * 100).toFixed(2) });


                                                                }


                                                                builder.addTextAlign(builder.ALIGN_CENTER);

                                                                builder.addText('__________________________________________\n');
                                                                incassi_video += '<hr>';
                                                                //incassi_video += '_________________________________________________________________________________________<br/>';
                                                                builder.addText('__________________________________________\n\n');
                                                                incassi_video += '<hr>';
                                                                //incassi_video += '_________________________________________________________________________________________';

                                                                builder.addTextSize(2, 2);
                                                                builder.addTextAlign(builder.ALIGN_CENTER);

                                                            }

                                                            if (settaggi_profili.Field100 === 'true') {

                                                                if (comanda.numero_licenza_cliente !== "0638") {
                                                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                                                    builder.addTextSize(2, 2);
                                                                    builder.addFeedLine(1);
                                                                    builder.addText('ORD. PER CATEGORIA\n\n');
                                                                    builder.addTextSize(1, 1);
                                                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                                                }

                                                                /*CONTINUARE DA QUI*/

                                                                incassi_video += 'ORD. PER CATEGORIA';


                                                                var nome_colonna = 'categoria';//destinazione o categoria

                                                                var array_codici_univoci = estrazione_univoca_colonna(articolo_generico, nome_colonna);
                                                                var array_percentuali = new Array();
                                                                var contatore_categorie = 0;
                                                                var contatore_stampanti = 1;
                                                                for (var codice_univoco in array_codici_univoci) {

                                                                    var totale_quantita_reparto = 0;
                                                                    var totale_dest_stampa = 0;


                                                                    var totale_prezzo_categoria = 0;
                                                                    var totale_quantita_categoria = 0;

                                                                    if (comanda.numero_licenza_cliente !== "0638") {
                                                                        builder.addTextAlign(builder.ALIGN_CENTER);
                                                                        builder.addText('__________________________________________\n\n');
                                                                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                        builder.addTextAlign(builder.ALIGN_CENTER);
                                                                        builder.addText(nomi_categorie[array_codici_univoci[codice_univoco]] + '\n');
                                                                    }

                                                                    incassi_video += '<hr>';
                                                                    console.log("ARTICOLO GENERICO CODICE UNIVOCO", array_codici_univoci[codice_univoco], nomi_categorie[array_codici_univoci[codice_univoco]]);
                                                                    incassi_video += nomi_categorie[array_codici_univoci[codice_univoco]] + '';

                                                                    if (contatore_categorie === 0) {

                                                                        if (comanda.numero_licenza_cliente !== "0638") {
                                                                            builder.addTextAlign(builder.ALIGN_LEFT);
                                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                            builder.addTextPosition(0);
                                                                            builder.addText('DESCRIZIONE');
                                                                            builder.addTextPosition(310);
                                                                            builder.addText('QTA');
                                                                            builder.addTextPosition(390);
                                                                            builder.addText('TOTALE\n\n');
                                                                        }
                                                                        incassi_video += 'DESCRIZIONE';
                                                                        incassi_video += 'QTA';

                                                                        incassi_video += 'TOTALE';
                                                                        contatore_categorie = 1;
                                                                    }

                                                                    if (comanda.numero_licenza_cliente !== "0638") {
                                                                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                        builder.addTextAlign(builder.ALIGN_LEFT);
                                                                        builder.addTextPosition(0);
                                                                    }

                                                                    var decrescente;

                                                                    switch (settaggi_profili.Field96) {
                                                                        case "C":
                                                                            decrescente = false;
                                                                            break;

                                                                        case "D":
                                                                        default:
                                                                            decrescente = true;
                                                                    }
                                                                    var array_suddiviso;

                                                                    //default descrizione
                                                                    var ordinamento = "descrizione";
                                                                    switch (settaggi_profili.Field95) {

                                                                        case "Q":
                                                                            array_suddiviso = estrazione_array(riordina(articolo_generico, 'quantita_tot', 'int', decrescente, ordinamento), nome_colonna, array_codici_univoci[codice_univoco]);
                                                                            break;

                                                                        case "D":
                                                                            array_suddiviso = estrazione_array(riordina(articolo_generico, 'descrizione', 'default', decrescente, ordinamento), nome_colonna, array_codici_univoci[codice_univoco]);
                                                                            break;

                                                                        case "T":
                                                                        default:
                                                                            array_suddiviso = estrazione_array(riordina(articolo_generico, 'prezzo', 'float', decrescente, ordinamento), nome_colonna, array_codici_univoci[codice_univoco]);
                                                                    }


                                                                    for (num in array_suddiviso) {

                                                                        var obj_art = array_suddiviso[num];

                                                                        if (comanda.numero_licenza_cliente !== "0638") {
                                                                            builder.addText(obj_art.descrizione);
                                                                            builder.addTextPosition(310);
                                                                            builder.addText(obj_art.quantita_tot);
                                                                        }

                                                                        incassi_video += obj_art.descrizione;
                                                                        incassi_video += obj_art.quantita_tot;
                                                                        totale_prezzo_categoria += parseFloat(obj_art.prezzo);
                                                                        totale_quantita_categoria += parseInt(obj_art.quantita_tot);
                                                                        totale_quantita_reparto += parseInt(obj_art.quantita_tot);

                                                                        //Temporaneamente disabilitato per questioni di spazio
                                                                        if (false) {
                                                                            if (comanda.numero_licenza_cliente !== "0638") {
                                                                                builder.addTextPosition(310);
                                                                                builder.addText('€ ' + obj.prezzo_un);
                                                                            }
                                                                            incassi_video += '€ ' + obj.prezzo_un;
                                                                        }
                                                                        if (comanda.numero_licenza_cliente !== "0638") {
                                                                            builder.addTextPosition(390);
                                                                            builder.addTextAlign(builder.ALIGN_RIGHT);
                                                                            builder.addText('€ ' + parseFloat(obj_art.prezzo).toFixed(2) + '\n');
                                                                            builder.addTextAlign(builder.ALIGN_LEFT);
                                                                        }
                                                                        incassi_video += '€ ' + parseFloat(obj_art.prezzo).toFixed(2) + '';
                                                                        totale_dest_stampa += parseFloat(obj_art.prezzo);
                                                                    }

                                                                    if (comanda.numero_licenza_cliente !== "0638") {
                                                                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                        builder.addTextAlign(builder.ALIGN_LEFT);
                                                                        builder.addFeedLine(1);
                                                                        builder.addText('TOTALE CATEGORIA:');
                                                                        builder.addTextPosition(310);
                                                                        builder.addText(totale_quantita_categoria);
                                                                        builder.addTextPosition(390);
                                                                        builder.addText('€ ' + parseFloat(totale_prezzo_categoria).toFixed(2) + '\n');
                                                                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                        if (consegna_metro === true) {

                                                                            builder.addTextPosition(0);
                                                                            builder.addText("PIZZE NORMALI");
                                                                            builder.addTextPosition(200);
                                                                            builder.addText(totale_consegne['NORMALI'].qta);
                                                                            builder.addTextPosition(390);
                                                                            builder.addText("€ " + arrotonda_prezzo(totale_consegne['NORMALI'].importo));
                                                                            builder.addText("\n");
        
                                                                            builder.addTextPosition(0);
                                                                            builder.addText("MAXI");
                                                                            builder.addTextPosition(200);
                                                                            builder.addText(totale_consegne['MAXI'].qta);
                                                                            builder.addTextPosition(390);
                                                                            builder.addText("€ " + arrotonda_prezzo(totale_consegne['MAXI'].importo));
                                                                            builder.addText("\n");
        
                                                                            builder.addTextPosition(0);
                                                                            builder.addText("MEZZO METRO");
                                                                            builder.addTextPosition(200);
                                                                            builder.addText(totale_consegne['MEZZO'].qta);
                                                                            builder.addTextPosition(390);
                                                                            builder.addText("€ " + arrotonda_prezzo(totale_consegne['MEZZO'].importo));
                                                                            builder.addText("\n");
        
                                                                            builder.addTextPosition(0);
                                                                            builder.addText("UN METRO");
                                                                            builder.addTextPosition(200);
                                                                            builder.addText(totale_consegne['METRO'].qta);
                                                                            builder.addTextPosition(390);
                                                                            builder.addText("€ " + arrotonda_prezzo(totale_consegne['METRO'].importo));
                                                                            builder.addText("\n");
        
                                                                            builder.addTextPosition(0);
                                                                            builder.addText(totale_qta_consegne + " Consegne");
                                                                            builder.addTextPosition(200);
                                                                            builder.addText(parseInt(totale_consegne['NORMALI'].qta) + parseInt(totale_consegne['MAXI'].qta) + parseInt(totale_consegne['MEZZO'].qta) + parseInt(totale_consegne['METRO'].qta));
                                                                            builder.addTextPosition(390);
                                                                            builder.addText("€ " + arrotonda_prezzo(parseFloat(totale_consegne['NORMALI'].importo) + parseFloat(totale_consegne['MAXI'].importo) + parseFloat(totale_consegne['MEZZO'].importo) + parseFloat(totale_consegne['METRO'].importo)));
                                                                            builder.addText("\n");
                                                                            incassi_video += 'TOTALE';
                                                                            incassi_video += '€ ' + (parseFloat(totale_prezzo_categoria).toFixed(2) + arrotonda_prezzo(totale_consegne['PIZZE'].importo))+ '';
        
                                                                        } else {
        
                                                                            builder.addTextPosition(0);
                                                                            builder.addText(totale_qta_consegne + " Consegne");
                                                                            builder.addTextPosition(310);
                                                                            builder.addText(totale_consegne['PIZZE'].qta);
                                                                            builder.addTextPosition(390);
                                                                            builder.addText("€ " + arrotonda_prezzo(totale_consegne['PIZZE'].importo));
                                                                            builder.addText("\n");
                                                                           
        
                                                                        }
                                                                        
                                                                    }
                                                                    incassi_video += 'TOTALE';
                                                                    let totale_in_tutto = parseFloat(totale_prezzo_categoria).toFixed(2) + arrotonda_prezzo(totale_consegne['PIZZE'].importo)
                                                                    incassi_video += '€ ' + (totale_in_tutto)+ '';
                                                                    incassi_video += 'TOTALE CATEGORIA:';
                                                                    incassi_video += 'totale_quantita_categoria';
                                                                    incassi_video += '€ ' + parseFloat(totale_prezzo_categoria).toFixed(2) + '';
                                                                   
                                                                 
    
    

                                                                    array_percentuali.push({ nome_categoria: nomi_categorie[array_codici_univoci[codice_univoco]], quantita_totale: totale_quantita_categoria, prezzo_totale: totale_prezzo_categoria, percentuale: (parseFloat(totale_prezzo_categoria) / parseFloat(totale_generico) * 100).toFixed(2) });

                                                                }

                                                                if (comanda.numero_licenza_cliente !== "0638") {
                                                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                                                    builder.addText('__________________________________________\n');
                                                                    builder.addText('__________________________________________\n\n');
                                                                    builder.addTextSize(2, 2);
                                                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                                                }

                                                                incassi_video += '€ ' + parseFloat(totale_prezzo_categoria).toFixed(2) + '';
                                                                incassi_video += '<hr>';
                                                                incassi_video += '<hr>';


                                                            }

                                                            if (settaggi_profili.Field412 === 'true') {

                                                                if (comanda.numero_licenza_cliente !== "0638") {
                                                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                                                    builder.addTextSize(2, 2);
                                                                    builder.addFeedLine(1);
                                                                    builder.addText('ORD. PER GRUPPO STATISTICO\n\n');
                                                                    builder.addTextSize(1, 1);
                                                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                                                }
                                                                incassi_video += 'ORD. PER GRUPPO STATISTICO';

                                                                var nome_colonna = 'gruppo';//destinazione o categoria

                                                                var array_codici_univoci = estrazione_univoca_colonna(articolo_generico, nome_colonna);
                                                                var array_percentuali = new Array();
                                                                var contatore_categorie = 0;
                                                                var contatore_stampanti = 1;
                                                                for (var codice_univoco in array_codici_univoci) {

                                                                    var totale_quantita_reparto = 0;
                                                                    var totale_dest_stampa = 0;

                                                                    var totale_prezzo_categoria = 0;
                                                                    var totale_quantita_categoria = 0;

                                                                    if (comanda.numero_licenza_cliente !== "0638") {
                                                                        builder.addTextAlign(builder.ALIGN_CENTER);
                                                                        builder.addText('__________________________________________\n\n');
                                                                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                        builder.addTextAlign(builder.ALIGN_CENTER);
                                                                        builder.addText(nomi_gruppi[array_codici_univoci[codice_univoco]] + '\n');
                                                                    }

                                                                    incassi_video += nomi_gruppi[array_codici_univoci[codice_univoco]] + '';

                                                                    if (contatore_categorie === 0) {

                                                                        if (comanda.numero_licenza_cliente !== "0638") {
                                                                            builder.addTextAlign(builder.ALIGN_LEFT);
                                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                            builder.addTextPosition(0);
                                                                            builder.addText('DESCRIZIONE');
                                                                            builder.addTextPosition(310);
                                                                            builder.addText('QTA');
                                                                            builder.addTextPosition(390);
                                                                            builder.addText('TOTALE\n\n');
                                                                        }

                                                                        incassi_video += 'DESCRIZIONE';
                                                                        incassi_video += 'QTA';
                                                                        incassi_video += 'TOTALE';
                                                                        contatore_categorie = 1;
                                                                    }

                                                                    if (comanda.numero_licenza_cliente !== "0638") {
                                                                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                        builder.addTextAlign(builder.ALIGN_LEFT);
                                                                        builder.addTextPosition(0);
                                                                    }

                                                                    var decrescente;

                                                                    switch (settaggi_profili.Field96) {
                                                                        case "C":
                                                                            decrescente = false;
                                                                            break;

                                                                        case "D":
                                                                        default:
                                                                            decrescente = true;
                                                                    }
                                                                    var array_suddiviso;

                                                                    //default descrizione
                                                                    var ordinamento = "descrizione";
                                                                    switch (settaggi_profili.Field95) {

                                                                        case "Q":
                                                                            array_suddiviso = estrazione_array(riordina(articolo_generico, 'quantita_tot', 'int', decrescente, ordinamento), nome_colonna, array_codici_univoci[codice_univoco]);
                                                                            break;

                                                                        case "D":
                                                                            array_suddiviso = estrazione_array(riordina(articolo_generico, 'descrizione', 'default', decrescente, ordinamento), nome_colonna, array_codici_univoci[codice_univoco]);
                                                                            break;

                                                                        case "T":
                                                                        default:
                                                                            array_suddiviso = estrazione_array(riordina(articolo_generico, 'prezzo', 'float', decrescente, ordinamento), nome_colonna, array_codici_univoci[codice_univoco]);
                                                                    }


                                                                    for (num in array_suddiviso) {

                                                                        var obj_art = array_suddiviso[num];

                                                                        if (comanda.numero_licenza_cliente !== "0638") {
                                                                            builder.addText(obj_art.descrizione);
                                                                            builder.addTextPosition(310);
                                                                            builder.addText(obj_art.quantita_tot);
                                                                        }

                                                                        incassi_video += obj_art.descrizione;
                                                                        incassi_video += obj_art.quantita_tot;
                                                                        totale_prezzo_categoria += parseFloat(obj_art.prezzo);
                                                                        totale_quantita_categoria += parseInt(obj_art.quantita_tot);
                                                                        totale_quantita_reparto += parseInt(obj_art.quantita_tot);


                                                                        //Temporaneamente disabilitato per questioni di spazio
                                                                        if (false) {
                                                                            if (comanda.numero_licenza_cliente !== "0638") {
                                                                                builder.addTextPosition(310);
                                                                                builder.addText('€ ' + obj.prezzo_un);
                                                                            }
                                                                            incassi_video += '€ ' + obj.prezzo_un;
                                                                        }

                                                                        if (comanda.numero_licenza_cliente !== "0638") {
                                                                            builder.addTextPosition(390);
                                                                            builder.addTextAlign(builder.ALIGN_RIGHT);
                                                                            builder.addText('€ ' + parseFloat(obj_art.prezzo).toFixed(2) + '\n');
                                                                            builder.addTextAlign(builder.ALIGN_LEFT);
                                                                        }

                                                                        incassi_video += '€ ' + parseFloat(obj_art.prezzo).toFixed(2) + '';
                                                                        totale_dest_stampa += parseFloat(obj_art.prezzo);
                                                                    }

                                                                    if (comanda.numero_licenza_cliente !== "0638") {
                                                                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                        builder.addTextAlign(builder.ALIGN_LEFT);
                                                                        builder.addFeedLine(1);
                                                                        builder.addText('TOTALE GRUPPO STATISTICO:');
                                                                        builder.addTextPosition(310);
                                                                        builder.addText(totale_quantita_categoria);
                                                                        builder.addTextPosition(390);
                                                                        builder.addText('€ ' + parseFloat(totale_prezzo_categoria).toFixed(2) + '\n');
                                                                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                    }

                                                                    incassi_video += 'TOTALE GRUPPO STATISTICO:';
                                                                    incassi_video += 'totale_quantita_categoria';
                                                                    incassi_video += '€ ' + parseFloat(totale_prezzo_categoria).toFixed(2) + '';

                                                                    array_percentuali.push({ nome_categoria: nomi_gruppi[array_codici_univoci[codice_univoco]], quantita_totale: totale_quantita_categoria, prezzo_totale: totale_prezzo_categoria, percentuale: (parseFloat(totale_prezzo_categoria) / parseFloat(totale_generico) * 100).toFixed(2) });
                                                                }

                                                                if (comanda.numero_licenza_cliente !== "0638") {
                                                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                                                    builder.addText('__________________________________________\n');
                                                                    builder.addText('__________________________________________\n\n');
                                                                    builder.addTextSize(2, 2);
                                                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                                                }
                                                                incassi_video += '<hr>';
                                                                incassi_video += '<hr>';
                                                            }

                                                            if (comanda.pizzeria_asporto === true) {


                                                                builder.addFeedLine(1);
                                                                builder.addTextSize(2, 2);
                                                                builder.addText('CONSEGNA\n\n');
                                                                incassi_video += 'CONSEGNA';
                                                                builder.addTextSize(1, 1);
                                                                builder.addFeedLine(1);

                                                                builder.addTextAlign(builder.ALIGN_LEFT);

                                                                builder.addTextSize(1, 1);

                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                builder.addTextPosition(0);
                                                                builder.addText('QUANTITA');
                                                                incassi_video += 'QUANTITA';
                                                                builder.addTextPosition(200);
                                                                builder.addText('QTA PIZZE');
                                                                incassi_video += 'QTA PIZZE';

                                                                builder.addTextPosition(390);
                                                                builder.addTextAlign(builder.ALIGN_RIGHT);

                                                                builder.addText('TOTALE\n\n');
                                                                incassi_video += 'TOTALE';

                                                                builder.addTextAlign(builder.ALIGN_LEFT);

                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);

                                                                builder.addTextAlign(builder.ALIGN_LEFT);

                                                                if (consegna_metro === true) {

                                                                    builder.addTextPosition(0);
                                                                    builder.addText("PIZZE NORMALI");
                                                                    builder.addTextPosition(200);
                                                                    builder.addText(totale_consegne['NORMALI'].qta);
                                                                    builder.addTextPosition(390);
                                                                    builder.addText("€ " + arrotonda_prezzo(totale_consegne['NORMALI'].importo));
                                                                    builder.addText("\n");

                                                                    builder.addTextPosition(0);
                                                                    builder.addText("MAXI");
                                                                    builder.addTextPosition(200);
                                                                    builder.addText(totale_consegne['MAXI'].qta);
                                                                    builder.addTextPosition(390);
                                                                    builder.addText("€ " + arrotonda_prezzo(totale_consegne['MAXI'].importo));
                                                                    builder.addText("\n");

                                                                    builder.addTextPosition(0);
                                                                    builder.addText("MEZZO METRO");
                                                                    builder.addTextPosition(200);
                                                                    builder.addText(totale_consegne['MEZZO'].qta);
                                                                    builder.addTextPosition(390);
                                                                    builder.addText("€ " + arrotonda_prezzo(totale_consegne['MEZZO'].importo));
                                                                    builder.addText("\n");

                                                                    builder.addTextPosition(0);
                                                                    builder.addText("UN METRO");
                                                                    builder.addTextPosition(200);
                                                                    builder.addText(totale_consegne['METRO'].qta);
                                                                    builder.addTextPosition(390);
                                                                    builder.addText("€ " + arrotonda_prezzo(totale_consegne['METRO'].importo));
                                                                    builder.addText("\n");

                                                                    builder.addTextPosition(0);
                                                                    builder.addText(totale_qta_consegne + " Consegne");
                                                                    builder.addTextPosition(200);
                                                                    builder.addText(parseInt(totale_consegne['NORMALI'].qta) + parseInt(totale_consegne['MAXI'].qta) + parseInt(totale_consegne['MEZZO'].qta) + parseInt(totale_consegne['METRO'].qta));
                                                                    builder.addTextPosition(390);
                                                                    builder.addText("€ " + arrotonda_prezzo(parseFloat(totale_consegne['NORMALI'].importo) + parseFloat(totale_consegne['MAXI'].importo) + parseFloat(totale_consegne['MEZZO'].importo) + parseFloat(totale_consegne['METRO'].importo)));
                                                                    builder.addText("\n");

                                                                } else {

                                                                    builder.addTextPosition(0);
                                                                    builder.addText(totale_qta_consegne + " Consegne");
                                                                    builder.addTextPosition(200);
                                                                    builder.addText(totale_consegne['PIZZE'].qta);
                                                                    builder.addTextPosition(390);
                                                                    builder.addText("€ " + arrotonda_prezzo(totale_consegne['PIZZE'].importo));
                                                                    builder.addText("\n");

                                                                }



                                                                builder.addText('__________________________________________\n');
                                                                incassi_video += '<hr>';
                                                                //incassi_video += '_________________________________________________________________________________________';

                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                builder.addTextAlign(builder.ALIGN_LEFT);


                                                                builder.addTextSize(2, 2);
                                                                builder.addTextAlign(builder.ALIGN_CENTER);
                                                                builder.addFeedLine(1);


                                                            }

                                                            if (settaggi_profili.Field102 === 'true') {

                                                                builder.addFeedLine(1);

                                                                builder.addText('PERCENTUALI\n\n');
                                                                incassi_video += 'PERCENTUALI';
                                                                builder.addFeedLine(1);

                                                                builder.addTextAlign(builder.ALIGN_LEFT);

                                                                builder.addTextSize(1, 1);

                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                builder.addTextPosition(0);
                                                                builder.addText('CATEGORIA');
                                                                incassi_video += 'CATEGORIA';
                                                                builder.addTextPosition(200);
                                                                builder.addText('%');
                                                                incassi_video += '%';

                                                                builder.addTextPosition(310);
                                                                builder.addText('QTA');
                                                                incassi_video += 'QTA';
                                                                //Temporaneamente disabilitato per questioni di spazio
                                                                if (false) {
                                                                    builder.addTextPosition(310);
                                                                    builder.addText('UNIT.');
                                                                    incassi_video += 'UNIT.';
                                                                }
                                                                builder.addTextPosition(390);
                                                                builder.addTextAlign(builder.ALIGN_RIGHT);

                                                                builder.addText('TOTALE\n\n');
                                                                incassi_video += 'TOTALE';

                                                                builder.addTextAlign(builder.ALIGN_LEFT);

                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);

                                                                builder.addTextAlign(builder.ALIGN_CENTER);



                                                                builder.addText('__________________________________________\n');
                                                                incassi_video += '<hr>';
                                                                //incassi_video += '_________________________________________________________________________________________';

                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                builder.addTextAlign(builder.ALIGN_LEFT);

                                                                builder.addText("\nTOTALE GENERALE: \n");
                                                                incassi_video += 'TOTALE GENERALE: ';

                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);

                                                                builder.addTextPosition(200);

                                                                builder.addText('100%');
                                                                incassi_video += '100%';

                                                                builder.addTextPosition(310);

                                                                builder.addText(totale_quantita_generica);
                                                                incassi_video += totale_quantita_generica;

                                                                builder.addTextPosition(390);

                                                                builder.addTextAlign(builder.ALIGN_RIGHT);

                                                                builder.addText('€ ' + parseFloat(totale_generico).toFixed(2) + '\n');
                                                                incassi_video += '€ ' + parseFloat(totale_generico).toFixed(2) + '';

                                                                builder.addTextAlign(builder.ALIGN_LEFT);


                                                                builder.addText('\n');
                                                                incassi_video += '';
                                                                builder.addTextAlign(builder.ALIGN_CENTER);

                                                                builder.addText('__________________________________________\n');
                                                                incassi_video += '<hr>';
                                                                //incassi_video += '_________________________________________________________________________________________';

                                                                riordina(array_percentuali, 'percentuale', 'float', true, false).forEach(function (element, index) {


                                                                    builder.addTextAlign(builder.ALIGN_LEFT);

                                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);

                                                                    //CATEGORIA O STAMPANTE - IN STO CASO STAMPANTE
                                                                    //builder.addText(element.nome_categoria + ': \n');
                                                                    builder.addText(element.nome_categoria + ': \n');
                                                                    incassi_video += element.nome_categoria + ': ';

                                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);

                                                                    builder.addTextPosition(200);

                                                                    builder.addText(element.percentuale + '%');
                                                                    incassi_video += element.percentuale + '%';

                                                                    builder.addTextPosition(310);

                                                                    builder.addText(parseInt(element.quantita_totale));
                                                                    incassi_video += parseInt(element.quantita_totale);

                                                                    builder.addTextPosition(390);
                                                                    builder.addTextAlign(builder.ALIGN_RIGHT);

                                                                    builder.addText('€ ' + parseFloat(element.prezzo_totale).toFixed(2));
                                                                    incassi_video += '€ ' + parseFloat(element.prezzo_totale).toFixed(2);

                                                                    builder.addTextAlign(builder.ALIGN_LEFT);


                                                                    builder.addText('\n');
                                                                    incassi_video += '';

                                                                    builder.addTextAlign(builder.ALIGN_CENTER);

                                                                    builder.addText('__________________________________________\n');
                                                                    incassi_video += '';


                                                                });
                                                                builder.addTextAlign(builder.ALIGN_CENTER);

                                                                builder.addText('__________________________________________\n');
                                                                incassi_video += '<hr>';
                                                                //incassi_video += '_________________________________________________________________________________________';



                                                                builder.addTextSize(2, 2);
                                                                builder.addTextAlign(builder.ALIGN_CENTER);
                                                                builder.addFeedLine(1);
                                                            }

                                                            if (settaggi_profili.Field103 === 'true') {
                                                                builder.addTextAlign(builder.ALIGN_CENTER);
                                                                builder.addTextSize(2, 2);
                                                                builder.addFeedLine(1);
                                                                builder.addText('SCONTI\n\n');
                                                                incassi_video += 'SCONTI';
                                                                builder.addFeedLine(1);

                                                                builder.addTextAlign(builder.ALIGN_LEFT);

                                                                builder.addTextSize(1, 1);

                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                builder.addTextPosition(0);
                                                                builder.addText('SCONTO');
                                                                incassi_video += 'SCONTO';

                                                                builder.addTextPosition(310);
                                                                builder.addText('QTA');
                                                                incassi_video += 'QTA';

                                                                builder.addTextPosition(390);
                                                                builder.addTextAlign(builder.ALIGN_RIGHT);

                                                                builder.addText('TOTALE\n\n');
                                                                incassi_video += 'TOTALE';

                                                                builder.addTextAlign(builder.ALIGN_LEFT);

                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);



                                                                builder.addTextAlign(builder.ALIGN_CENTER);

                                                                builder.addText('__________________________________________\n');
                                                                incassi_video += '<hr>';
                                                                //incassi_video += '_________________________________________________________________________________________';


                                                                var totale_sconti_prezzo = 0;
                                                                var totale_sconti_quantita = 0;

                                                                for (var num in result_sconti) {

                                                                    var element = result_sconti[num];

                                                                    if (element.descrizione !== '0.00%') {

                                                                        builder.addTextStyle(false, false, true, builder.COLOR_1);

                                                                        builder.addTextAlign(builder.ALIGN_LEFT);

                                                                        var valore_sconto = element.valore_sconto.substr(1);

                                                                        builder.addFeedLine(1);

                                                                        if (valore_sconto.slice(-1) === '%') {
                                                                            valore_sconto = parseInt(valore_sconto.slice(0, -1)) + ' %';
                                                                        } else {
                                                                            valore_sconto = '€ ' + valore_sconto;
                                                                        }
                                                                        builder.addText(valore_sconto);
                                                                        incassi_video += valore_sconto;
                                                                        builder.addTextStyle(false, false, false, builder.COLOR_1);


                                                                        builder.addTextPosition(310);

                                                                        builder.addText(parseInt(element.quantita_totale));
                                                                        incassi_video += parseInt(element.quantita_totale);

                                                                        builder.addTextPosition(390);
                                                                        builder.addTextAlign(builder.ALIGN_RIGHT);


                                                                        totale_sconti_quantita += parseInt(element.quantita_totale);
                                                                        totale_sconti_prezzo += parseFloat(element.totale_sconti);

                                                                        console.log("totale sconti", element.totale_sconti, parseFloat(element.totale_sconti).toFixed(2));


                                                                        builder.addText('€ ' + parseFloat(element.totale_sconti).toFixed(2));
                                                                        incassi_video += '€ ' + parseFloat(element.totale_sconti).toFixed(2);


                                                                        builder.addText('\n');
                                                                        incassi_video += '';

                                                                        builder.addTextAlign(builder.ALIGN_CENTER);

                                                                        builder.addText('__________________________________________\n');
                                                                        incassi_video += '<hr>';
                                                                        //incassi_video += '_________________________________________________________________________________________';

                                                                    }
                                                                }




                                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                builder.addTextAlign(builder.ALIGN_LEFT);

                                                                builder.addText("\nTOTALE SCONTI: ");
                                                                incassi_video += 'TOTALE SCONTI: ';


                                                                builder.addTextPosition(310);

                                                                builder.addText(totale_sconti_quantita);
                                                                incassi_video += totale_sconti_quantita;

                                                                builder.addTextPosition(390);

                                                                builder.addTextAlign(builder.ALIGN_RIGHT);

                                                                builder.addText('€ ' + parseFloat(totale_sconti_prezzo).toFixed(2) + '\n');
                                                                incassi_video += '€ ' + parseFloat(totale_sconti_prezzo).toFixed(2) + '';
                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);


                                                                builder.addTextAlign(builder.ALIGN_LEFT);


                                                                builder.addText('\n');
                                                                incassi_video += '';
                                                                builder.addTextAlign(builder.ALIGN_CENTER);

                                                                builder.addText('__________________________________________\n');
                                                                incassi_video += '<hr>';
                                                                //incassi_video += '___________________ ______________________________________________________________________<br/>';
                                                                builder.addText('__________________________________________\n\n');
                                                                incassi_video += '<hr>';
                                                                //incassi_video += '_________________________________________________________________________________________';


                                                            }

                                                            if (comanda.numero_licenza_cliente !== "0638") {
                                                                builder.addText("\nMATRICOLA NUMERO: " + numero_matricola);
                                                            }

                                                        }


                                                        if (comanda.numero_licenza_cliente !== "0638") {
                                                            builder.addFeedLine(1);
                                                            builder.addTextSize(2, 2);
                                                            builder.addTextAlign(builder.ALIGN_CENTER);
                                                        }

                                                        if (obj_dati_ultima_chiusura['COPERTI'] !== undefined && obj_dati_ultima_chiusura['COPERTI']['NUMERO_COPERTI'] !== undefined) {
                                                            numero_coperti -= parseInt(obj_dati_ultima_chiusura['COPERTI']['NUMERO_COPERTI'].totale_quantita);
                                                        }

                                                        if (comanda.numero_licenza_cliente !== "0638") {
                                                            builder.addText('NUMERO COPERTI: ' + numero_coperti);
                                                        }

                                                        incassi_video += '<tr>';
                                                        incassi_video += '<td style="text-align: center;" colspan="3">';
                                                        incassi_video += '<strong>NUMERO COPERTI: ' + numero_coperti + '</strong>';
                                                        incassi_video += '</td>';
                                                        incassi_video += '</tr>';

                                                        riga++;
                                                        array_chiusura.push({
                                                            data_servizio: data_report,
                                                            data_emissione: data_emissione,
                                                            lingua_stampa: comanda.lingua_stampa,
                                                            protocollo: protocollo,
                                                            progressivo_riga: aggZero(riga, 4),
                                                            operatore: comanda.operatore,
                                                            //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                            tipo_record: '1',
                                                            descrizione: 'COPERTI',
                                                            quantita: '',
                                                            importo: '',
                                                            nome_locale: comanda.locale,
                                                            fiscale_sn: 'n',
                                                            nome_pc: comanda.nome_pc
                                                        });
                                                        //------------------------------------------------//
                                                        //------------------------------------------------//
                                                        riga++;
                                                        array_chiusura.push({
                                                            data_servizio: data_report,
                                                            data_emissione: data_emissione,
                                                            lingua_stampa: comanda.lingua_stampa,
                                                            protocollo: protocollo,
                                                            progressivo_riga: aggZero(riga, 4),
                                                            operatore: comanda.operatore,
                                                            //1 TITOLO GRANDE 2 TITOLETTO 3 NORMALE    
                                                            tipo_record: '2',
                                                            descrizione: 'NUMERO_COPERTI',
                                                            quantita: numero_coperti,
                                                            importo: '',
                                                            nome_locale: comanda.locale,
                                                            fiscale_sn: 'n',
                                                            nome_pc: comanda.nome_pc
                                                        });
                                                        //------------------------------------------------//
                                                        builder.addTextAlign(builder.ALIGN_CENTER);
                                                        builder.addFeedLine(1);


                                                        ////CREA TABLE DES QT INCASSOTOT
                                                        //---- FOR ARTICOLO[IVA]
                                                        //FOR [PORTATA]
                                                        //FOR [DEST_STAMPA]
                                                        //FOR [CATEGORIA]
                                                        //FOR [DESCRIZIONE]
                                                        //FOR [PREZZO]


                                                        //--------------------SCONTRINO PER DESTINAZIONE STAMPA-------------------
                                                        ////CREA TABLE DES QT INCASSOTOT
                                                        //---- FOR ARTICOLO[IVA]
                                                        //FOR [PORTATA]
                                                        //
                                                        //FOR [DEST_STAMPA]
                                                        //
                                                        //--IN QUESTO CASO E' LA STESSA ROBA PERO' IN OGNI CICLO DI QUESTI CAMBIA STAMPANTE E SCRIVE LA DESTINAZIONE
                                                        //LA DEST STAMPA E' NELLA VARIABILE comanda.nome_stampante[num_dest].nome
                                                        //
                                                        //FOR [CATEGORIA]
                                                        //FOR [DESCRIZIONE]
                                                        //FOR [PREZZO]



                                                        //FINE CORPO SCONTRINO TOTALI
                                                        builder.addFeedLine(1);
                                                        builder.addFeedLine(2);
                                                        builder.addCut(builder.CUT_FEED);
                                                        var request = builder.toString();
                                                        //CONTENUTO CONTO
                                                        //console.log(request);

                                                        //Create a SOAP envelop
                                                        var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
                                                        //Create an XMLHttpRequest object
                                                        var xhr = new XMLHttpRequest();
                                                        //Set the end point address
                                                        var url = 'http://' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                                                        //Open an XMLHttpRequest object
                                                        if (comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].intelligent === "SDS") {
                                                            url = 'http://' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "") + numero_layout].ip + ':8000/SPOOLER?stampante=' + comanda.nome_stampante[comanda.lang_stampa[60].replace(/\<br\/>/gi, "")].devid_nf;
                                                        }
                                                        //Open an XMLHttpRequest object
                                                        xhr.open('POST', url, true);
                                                        //<Header settings>
                                                        xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
                                                        xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
                                                        xhr.setRequestHeader('SOAPAction', '""');
                                                        // Send print document
                                                        //RIABILITARE

                                                        console.log("STAMPA TOTALI INVIATA 721", soap);

                                                        if (a_video === true) {
                                                            $('#popup_incasso #visu_incassi').html(incassi_video);
                                                            $('#popup_incasso').modal('show');
                                                        } else {
                                                            xhr.send(soap);
                                                            $('#filtri_utilizzati_backoffice').html(filtro_data + '' + filtri);
                                                        }
                                                        //FINE CICLO
                                                        if (layout === "soloincassi" && extra !== 'giornata' && extra !== 'PUNTOCASSA') {
                                                            for (var indice in array_chiusura) {
                                                                var riga = array_chiusura[indice];

                                                                var query_inserimento_chiusura = "insert into chiusure_fiscali (fiscale_sn,data_servizio,data_emissione,lingua_stampa,protocollo,progressivo_riga,operatore,tipo_record,descrizione,quantita,importo,nome_locale,nome_pc)\n\
                                                    values ('" + riga.fiscale_sn + "','" + riga.data_servizio + "','" + riga.data_emissione + "','" + riga.lingua_stampa + "','" + riga.protocollo + "','" + riga.progressivo_riga + "','" + riga.operatore + "','" + riga.tipo_record + "','" + riga.descrizione + "','" + riga.quantita + "','" + riga.importo + "','" + riga.nome_locale + "','" + riga.nome_pc + "');";

                                                                comanda.sock.send({ tipo: "chiusura_non_fiscale", operatore: comanda.operatore, query: query_inserimento_chiusura, terminale: comanda.terminale, ip: comanda.ip_address });

                                                                comanda.sincro.query(query_inserimento_chiusura, function () { });

                                                            }

                                                        }

                                                        comanda.sincro.query_cassa();

                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                                //});
                            });
                            //});
                        });
                    });
                });
            } else {
                bootbox.alert("Errore di Runtime nella sincronia dei dati del giorno.");
            }
        },
        error: function () {
            bootbox.alert("Errore di Ajax nella sincronia dei dati del giorno.");
        }
    });
}
