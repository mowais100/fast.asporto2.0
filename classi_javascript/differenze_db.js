/* global comanda, RAM, Class_Servizio, parseFloat, alasql, sharedPreferences, Ping, stampa_comanda_temporizzata, socket_fast_split */

preload("./img/TEXTURE.png");


var nome_pagina = location.pathname.substring(location.pathname.lastIndexOf("/") + 1);
//PER PC


$(document).on('keyup change', '#popup_mod_art [name="peso"]', function() {

    var peso = $('#popup_mod_art [name="peso"]').val();
    var descrizione = $('#popup_mod_art [name="desc_art"]').val().split(" - ");

    var prezzo_consigliato = parseFloat(peso) * parseFloat(comanda.prezzo_x_um);

    $('#popup_mod_art [name="desc_art"]').val(descrizione[0] + " - " + peso + "" + comanda.peso_ums);
    $('#popup_mod_art [name="prezzo_1"]').val(parseFloat(prezzo_consigliato).toFixed(2));


});


//RICERCA NELLA FATTURA NORMALE
$(document).on('keyup change', '#fattura [name="indirizzo"]', function() {
    var lettere_suggerimento = $('#fattura [name="indirizzo"]').val();

    $('#fattura form select#fattura_elenco_clienti option').not(':selected').remove();

    var opzione = "<option></option>";
    $('#fattura_elenco_clienti').append(opzione);

    comanda.sincro.query('SELECT * FROM clienti WHERE indirizzo LIKE "%' + lettere_suggerimento + '%" ORDER BY ragione_sociale ASC;', function(results) {

        results.forEach(function(row) {
            opzione = "<option value=\"" + row.id + "\">" + row.ragione_sociale + "</option>";
            $('#fattura_elenco_clienti').append(opzione);
        });
    });
});
// per svuotare checkboxes quando tu batti un altra box anche fa ricercare di clienti su base di nome,cognome,partita iva, ragione_sociale(parte del gestione clienti)..
function unckecked_boxes(type)
{ lista_gestione_clienti="";
var lettere_suggerimento = $('#tabella_clienti [name="tasto_recerca_clienti"]').val();
    
    if(type===1)
    {
        $('#tabella_clienti input[name="ricerca_cognome"]').prop("checked", false);
        $('#tabella_clienti input[name="ricerca_ragione_sociale"]').prop("checked", false);
        $('#tabella_clienti input[name="ricerca_partita_iva"]').prop("checked", false);
        comanda.sincro.query('SELECT * FROM clienti WHERE indirizzo LIKE "%' + "" + '%" AND ragione_sociale LIKE "%' + "" + '%"AND cognome LIKE "%' + "" + '%" AND nome LIKE "' + lettere_suggerimento +'%" AND cellulare LIKE "%' + "" + '%" AND codice_fiscale LIKE "%' + "" + '%" AND partita_iva LIKE "%' + "" + '%" AND codice_tessera LIKE "%' + "" + '%"AND numero LIKE "%' + "" + '%" ORDER BY nome ASC,cognome ASC,ragione_sociale ASC;', function(results) {

            results.forEach(function(row) {
                lista_gestione_clienti += '<a style="color:white;" ' + comanda.evento + '="gestisci_cliente(event,\'' + row.id + '\');" class="list-group-item">' + row.ragione_sociale + ' ' + row.nome + ' ' + row.cognome + '</a>';
                $('#lista_gestione_clienti').html(lista_gestione_clienti);
            });
            $('#lista_gestione_clienti').html(lista_gestione_clienti);
        });
    }
    if(type===2)
    {
        $('#tabella_clienti input[name="ricerca_nome"]').prop("checked", false);
        $('#tabella_clienti input[name="ricerca_ragione_sociale"]').prop("checked", false);
        $('#tabella_clienti input[name="ricerca_partita_iva"]').prop("checked", false);
        comanda.sincro.query('SELECT * FROM clienti WHERE indirizzo LIKE "%' + "" + '%" AND ragione_sociale LIKE "%' + "" + '%"AND cognome LIKE "%' + lettere_suggerimento + '%" AND nome LIKE "' + "" +'%" AND cellulare LIKE "%' + "" + '%" AND codice_fiscale LIKE "%' + "" + '%" AND partita_iva LIKE "%' + "" + '%" AND codice_tessera LIKE "%' + "" + '%"AND numero LIKE "%' + "" + '%" ORDER BY nome ASC,cognome ASC,ragione_sociale ASC;', function(results) {

            results.forEach(function(row) {
                lista_gestione_clienti += '<a style="color:white;" ' + comanda.evento + '="gestisci_cliente(event,\'' + row.id + '\');" class="list-group-item">' + row.ragione_sociale + ' ' + row.nome + ' ' + row.cognome + '</a>';
                $('#lista_gestione_clienti').html(lista_gestione_clienti);
            });
            $('#lista_gestione_clienti').html(lista_gestione_clienti);
        });
    }
    if(type===3)
    {
        $('#tabella_clienti input[name="ricerca_cognome"]').prop("checked", false);
        $('#tabella_clienti input[name="ricerca_nome"]').prop("checked", false);
        $('#tabella_clienti input[name="ricerca_partita_iva"]').prop("checked", false);
        comanda.sincro.query('SELECT * FROM clienti WHERE indirizzo LIKE "%' + "" + '%" AND ragione_sociale LIKE "%' + lettere_suggerimento + '%"AND cognome LIKE "%' + "" + '%" AND nome LIKE "' + "" +'%" AND cellulare LIKE "%' + "" + '%" AND codice_fiscale LIKE "%' + "" + '%" AND partita_iva LIKE "%' + "" + '%" AND codice_tessera LIKE "%' + "" + '%"AND numero LIKE "%' + "" + '%" ORDER BY nome ASC,cognome ASC,ragione_sociale ASC;', function(results) {

            results.forEach(function(row) {
                lista_gestione_clienti += '<a style="color:white;" ' + comanda.evento + '="gestisci_cliente(event,\'' + row.id + '\');" class="list-group-item">' + row.ragione_sociale + ' ' + row.nome + ' ' + row.cognome + '</a>';
                $('#lista_gestione_clienti').html(lista_gestione_clienti);
            });
            $('#lista_gestione_clienti').html(lista_gestione_clienti);
        });
        
    }
    if(type===4)
    {
        $('#tabella_clienti input[name="ricerca_cognome"]').prop("checked", false);
        $('#tabella_clienti input[name="ricerca_ragione_sociale"]').prop("checked", false);
        $('#tabella_clienti input[name="ricerca_nome"]').prop("checked", false);
        comanda.sincro.query('SELECT * FROM clienti WHERE indirizzo LIKE "%' + "" + '%" AND ragione_sociale LIKE "%' + "" + '%"AND cognome LIKE "%' + "" + '%" AND nome LIKE "' + "" +'%" AND cellulare LIKE "%' + "" + '%" AND codice_fiscale LIKE "%' + "" + '%" AND partita_iva LIKE "%' + lettere_suggerimento + '%" AND codice_tessera LIKE "%' + "" + '%"AND numero LIKE "%' + "" + '%" ORDER BY nome ASC,cognome ASC,ragione_sociale ASC;', function(results) {

            results.forEach(function(row) {
                lista_gestione_clienti += '<a style="color:white;" ' + comanda.evento + '="gestisci_cliente(event,\'' + row.id + '\');" class="list-group-item">' + row.ragione_sociale + ' ' + row.nome + ' ' + row.cognome + '</a>';
                $('#lista_gestione_clienti').html(lista_gestione_clienti);
            });
            $('#lista_gestione_clienti').html(lista_gestione_clienti);
        });
    }
}
// per ricercare tutti clienti quando il cerca bar e vouta (patre di gestione clienti)
$(document).on('keyup change', '#tabella_clienti [name="tasto_recerca_clienti"]', function() {
    lista_gestione_clienti="";
    
    var lettere_suggerimento = $('#tabella_clienti [name="tasto_recerca_clienti"]').val();
    var ricerca_nome = $('#tabella_clienti input[name="ricerca_nome"]:visible').is(':checked');
    var ricerca_cognome = $('#tabella_clienti input[name="ricerca_cognome"]:visible').is(':checked');
    var ricerca_ragione_sociale = $('#tabella_clienti input[name="ricerca_ragione_sociale"]:visible').is(':checked');
    var ricerca_partita_iva = $('#tabella_clienti input[name="ricerca_partita_iva"]:visible').is(':checked');
    if(ricerca_nome ===true)
    {
        
        comanda.sincro.query('SELECT * FROM clienti WHERE indirizzo LIKE "%' + "" + '%" AND ragione_sociale LIKE "%' + "" + '%"AND cognome LIKE "%' + "" + '%" AND nome LIKE "' + lettere_suggerimento +'%" AND cellulare LIKE "%' + "" + '%" AND codice_fiscale LIKE "%' + "" + '%" AND partita_iva LIKE "%' + "" + '%" AND codice_tessera LIKE "%' + "" + '%"AND numero LIKE "%' + "" + '%" ORDER BY nome ASC,cognome ASC,ragione_sociale ASC;', function(results) {

            results.forEach(function(row) {
                lista_gestione_clienti += '<a style="color:white;" ' + comanda.evento + '="gestisci_cliente(event,\'' + row.id + '\');" class="list-group-item">' + row.ragione_sociale + ' ' + row.nome + ' ' + row.cognome + '</a>';
                $('#lista_gestione_clienti').html(lista_gestione_clienti);
            });
            $('#lista_gestione_clienti').html(lista_gestione_clienti);
        });

    }
    if(ricerca_cognome===true)
    {
        comanda.sincro.query('SELECT * FROM clienti WHERE indirizzo LIKE "%' + "" + '%" AND ragione_sociale LIKE "%' + "" + '%"AND cognome LIKE "%' + lettere_suggerimento + '%" AND nome LIKE "' + "" +'%" AND cellulare LIKE "%' + "" + '%" AND codice_fiscale LIKE "%' + "" + '%" AND partita_iva LIKE "%' + "" + '%" AND codice_tessera LIKE "%' + "" + '%"AND numero LIKE "%' + "" + '%" ORDER BY nome ASC,cognome ASC,ragione_sociale ASC;', function(results) {

            results.forEach(function(row) {
                lista_gestione_clienti += '<a style="color:white;" ' + comanda.evento + '="gestisci_cliente(event,\'' + row.id + '\');" class="list-group-item">' + row.ragione_sociale + ' ' + row.nome + ' ' + row.cognome + '</a>';
                $('#lista_gestione_clienti').html(lista_gestione_clienti);
            });
            $('#lista_gestione_clienti').html(lista_gestione_clienti);
        });
    }
    if(ricerca_partita_iva===true)
    {
        comanda.sincro.query('SELECT * FROM clienti WHERE indirizzo LIKE "%' + "" + '%" AND ragione_sociale LIKE "%' + "" + '%"AND cognome LIKE "%' + "" + '%" AND nome LIKE "' + "" +'%" AND cellulare LIKE "%' + "" + '%" AND codice_fiscale LIKE "%' + "" + '%" AND partita_iva LIKE "%' + lettere_suggerimento + '%" AND codice_tessera LIKE "%' + "" + '%"AND numero LIKE "%' + "" + '%" ORDER BY nome ASC,cognome ASC,ragione_sociale ASC;', function(results) {

            results.forEach(function(row) {
                lista_gestione_clienti += '<a style="color:white;" ' + comanda.evento + '="gestisci_cliente(event,\'' + row.id + '\');" class="list-group-item">' + row.ragione_sociale + ' ' + row.nome + ' ' + row.cognome + '</a>';
                $('#lista_gestione_clienti').html(lista_gestione_clienti);
            });
            $('#lista_gestione_clienti').html(lista_gestione_clienti);
        });
    }
    if(ricerca_ragione_sociale===true)
    {
        comanda.sincro.query('SELECT * FROM clienti WHERE indirizzo LIKE "%' + "" + '%" AND ragione_sociale LIKE "%' + lettere_suggerimento + '%"AND cognome LIKE "%' + "" + '%" AND nome LIKE "' + "" +'%" AND cellulare LIKE "%' + "" + '%" AND codice_fiscale LIKE "%' + "" + '%" AND partita_iva LIKE "%' + "" + '%" AND codice_tessera LIKE "%' + "" + '%"AND numero LIKE "%' + "" + '%" ORDER BY nome ASC,cognome ASC,ragione_sociale ASC;', function(results) {

            results.forEach(function(row) {
                lista_gestione_clienti += '<a style="color:white;" ' + comanda.evento + '="gestisci_cliente(event,\'' + row.id + '\');" class="list-group-item">' + row.ragione_sociale + ' ' + row.nome + ' ' + row.cognome + '</a>';
                $('#lista_gestione_clienti').html(lista_gestione_clienti);
            });
            $('#lista_gestione_clienti').html(lista_gestione_clienti);
        });
    }


   
});

$(document).on('keyup change', '#fattura [name="ragione_sociale"]', function() {
    var lettere_suggerimento = $('#fattura [name="ragione_sociale"]').val();

    $('#fattura form select#fattura_elenco_clienti option').not(':selected').remove();

    var opzione = "<option></option>";
    $('#fattura_elenco_clienti').append(opzione);

    comanda.sincro.query('SELECT * FROM clienti WHERE ragione_sociale LIKE "%' + lettere_suggerimento + '%" ORDER BY ragione_sociale ASC;', function(results) {

        results.forEach(function(row) {
            opzione = "<option value=\"" + row.id + "\">" + row.ragione_sociale + "</option>";
            $('#fattura_elenco_clienti').append(opzione);
        });
    });
});
$(document).on('keyup change', '#fattura [name="cellulare"]', function() {
    var lettere_suggerimento = $('#fattura [name="cellulare"]').val();

    $('#fattura form select#fattura_elenco_clienti option').not(':selected').remove();

    var opzione = "<option></option>";
    $('#fattura_elenco_clienti').append(opzione);

    comanda.sincro.query('SELECT * FROM clienti WHERE cellulare LIKE "' + lettere_suggerimento + '%" ORDER BY ragione_sociale ASC;', function(results) {

        results.forEach(function(row) {
            opzione = "<option value=\"" + row.id + "\">" + row.ragione_sociale + "</option>";
            $('#fattura_elenco_clienti').append(opzione);
        });
    });
});
$(document).on('keyup change', '#fattura [name="codice_fiscale"]', function() {
    var lettere_suggerimento = $('#fattura [name="codice_fiscale"]').val();

    $('#fattura form select#fattura_elenco_clienti option').not(':selected').remove();

    var opzione = "<option></option>";
    $('#fattura_elenco_clienti').append(opzione);

    comanda.sincro.query('SELECT * FROM clienti WHERE codice_fiscale LIKE "' + lettere_suggerimento + '%" ORDER BY ragione_sociale ASC;', function(results) {

        results.forEach(function(row) {
            opzione = "<option value=\"" + row.id + "\">" + row.ragione_sociale + "</option>";
            $('#fattura_elenco_clienti').append(opzione);
        });
    });
});
$(document).on('keyup change', '#fattura [name="partita_iva"]', function() {
    var lettere_suggerimento = $('#fattura [name="partita_iva"]').val();

    $('#fattura form select#fattura_elenco_clienti option').not(':selected').remove();

    var opzione = "<option></option>";

    $('#fattura_elenco_clienti').append(opzione);

    comanda.sincro.query('SELECT * FROM clienti WHERE partita_iva LIKE "' + lettere_suggerimento + '%" ORDER BY ragione_sociale ASC;', function(results) {

        results.forEach(function(row) {

            opzione = "<option value=\"" + row.id + "\">" + row.ragione_sociale + "</option>";
            $('#fattura_elenco_clienti').append(opzione);

        });



    });


});




//RICERCA NEI CLIENTI NON PAGATI
$(document).on('keyup change', '#popup_nonpagato [name="indirizzo"]', function() {
    var lettere_suggerimento = $('#popup_nonpagato [name="indirizzo"]').val();

    $('#popup_nonpagato form select#popup_nonpagato_elenco_clienti option').remove();

    var opzione = "<option></option>";
    $('#popup_nonpagato_elenco_clienti').append(opzione);

    comanda.sincro.query('SELECT * FROM clienti WHERE indirizzo LIKE "%' + lettere_suggerimento + '%" ORDER BY ragione_sociale ASC;', function(results) {

        results.forEach(function(row) {
            opzione = "<option value=\"" + row.id + "\">" + row.ragione_sociale + "</option>";
            $('#popup_nonpagato_elenco_clienti').append(opzione);
        });
    });
});
$(document).on('keyup change', '#popup_nonpagato [name="ragione_sociale"]', function() {
    var lettere_suggerimento = $('#popup_nonpagato [name="ragione_sociale"]').val();

    $('#popup_nonpagato form select#popup_nonpagato_elenco_clienti option').remove();

    var opzione = "<option></option>";
    $('#popup_nonpagato_elenco_clienti').append(opzione);

    comanda.sincro.query('SELECT * FROM clienti WHERE ragione_sociale LIKE "%' + lettere_suggerimento + '%" ORDER BY ragione_sociale ASC;', function(results) {

        results.forEach(function(row) {
            opzione = "<option value=\"" + row.id + "\">" + row.ragione_sociale + "</option>";
            $('#popup_nonpagato_elenco_clienti').append(opzione);
        });
    });
});
$(document).on('keyup change', '#popup_nonpagato [name="cellulare"]', function() {
    var lettere_suggerimento = $('#popup_nonpagato [name="cellulare"]').val();

    $('#popup_nonpagato form select#popup_nonpagato_elenco_clienti option').remove();

    var opzione = "<option></option>";
    $('#popup_nonpagato_elenco_clienti').append(opzione);

    comanda.sincro.query('SELECT * FROM clienti WHERE cellulare LIKE "' + lettere_suggerimento + '%" ORDER BY ragione_sociale ASC;', function(results) {

        results.forEach(function(row) {
            opzione = "<option value=\"" + row.id + "\">" + row.ragione_sociale + "</option>";
            $('#popup_nonpagato_elenco_clienti').append(opzione);
        });
    });
});
$(document).on('keyup change', '#popup_nonpagato [name="codice_fiscale"]', function() {
    var lettere_suggerimento = $('#popup_nonpagato [name="codice_fiscale"]').val();

    $('#popup_nonpagato form select#popup_nonpagato_elenco_clienti option').remove();

    var opzione = "<option></option>";
    $('#popup_nonpagato_elenco_clienti').append(opzione);

    comanda.sincro.query('SELECT * FROM clienti WHERE codice_fiscale LIKE "' + lettere_suggerimento + '%" ORDER BY ragione_sociale ASC;', function(results) {

        results.forEach(function(row) {
            opzione = "<option value=\"" + row.id + "\">" + row.ragione_sociale + "</option>";
            $('#popup_nonpagato_elenco_clienti').append(opzione);
        });
    });
});
$(document).on('keyup change', '#popup_nonpagato [name="partita_iva"]', function() {
    var lettere_suggerimento = $('#popup_nonpagato [name="partita_iva"]').val();

    $('#popup_nonpagato form select#popup_nonpagato_elenco_clienti option').remove();

    var opzione = "<option></option>";
    $('#popup_nonpagato_elenco_clienti').append(opzione);

    comanda.sincro.query('SELECT * FROM clienti WHERE partita_iva LIKE "' + lettere_suggerimento + '%" ORDER BY ragione_sociale ASC;', function(results) {

        results.forEach(function(row) {
            opzione = "<option value=\"" + row.id + "\">" + row.ragione_sociale + "</option>";
            $('#popup_nonpagato_elenco_clienti').append(opzione);
        });
    });
});





//RICERCA NELLA FATTURA DELLA PIZZERIA ASPORTO (PER CAPIRCI)
$(document).on('keyup change', '#popup_scelta_cliente [name="indirizzo"]', function() {

    if ($('#popup_scelta_cliente_elenco_clienti option:selected').val() === undefined) {

        var indirizzo = $('#popup_scelta_cliente [name="indirizzo"]').val();
        var ragione_sociale = $('#popup_scelta_cliente [name="ragione_sociale"]').val().trim();
        var cognome = $('#popup_scelta_cliente [name="cognome"]').val().trim();
        var nome = $('#popup_scelta_cliente [name="nome"]').val().trim();
        var codice_fiscale = $('#popup_scelta_cliente [name="codice_fiscale"]').val();
        var partita_iva = $('#popup_scelta_cliente [name="partita_iva"]').val();
        var cellulare = $('#popup_scelta_cliente [name="cellulare"]').val();
        var codice_tessera = $('#popup_scelta_cliente [name="codice_tessera"]').val();
        var numero_civico = $('#popup_scelta_cliente [name="numero"]').val();

        var opzione = "";

        comanda.sincro.query('SELECT * FROM clienti WHERE indirizzo LIKE "%' + indirizzo + '%" AND ragione_sociale LIKE "' + ragione_sociale + '%" AND cognome LIKE "' + cognome + '%" AND nome LIKE "' + nome + '%" AND cellulare LIKE "%' + cellulare +'%" AND codice_fiscale LIKE "' + codice_fiscale + '%" AND partita_iva LIKE "' + partita_iva + '%" AND codice_tessera LIKE "' + codice_tessera +'%"AND numero LIKE "' + numero_civico + '%"  ORDER BY nome ASC,cognome ASC,ragione_sociale ASC;', function(results) {

            results.forEach(function(row) {

                let style = "";
                let pallina = "";

                if (parseInt(row.id) > 3000000) {
                    //style = " style='background-color:yellow; ' ";
                    pallina = " &bull;";
                }

                opzione += "<option value=\"" + row.id + "\" " + style + ">" + row.nome + " " + row.cognome + pallina;
                if (row.ragione_sociale) {
                    opzione += " - " + row.ragione_sociale;
                }
                opzione += "</option>";
            });
            $('#popup_scelta_cliente_elenco_clienti').html(opzione);
        });
    }
});
$(document).on('keyup change', '#popup_scelta_cliente [name="ragione_sociale"]', function() {
    if ($('#popup_scelta_cliente_elenco_clienti option:selected').val() === undefined) {
        var indirizzo = $('#popup_scelta_cliente [name="indirizzo"]').val();
        var ragione_sociale = $('#popup_scelta_cliente [name="ragione_sociale"]').val().trim();
        var cognome = $('#popup_scelta_cliente [name="cognome"]').val().trim();
        var nome = $('#popup_scelta_cliente [name="nome"]').val().trim();
        var codice_fiscale = $('#popup_scelta_cliente [name="codice_fiscale"]').val();
        var partita_iva = $('#popup_scelta_cliente [name="partita_iva"]').val();
        var codice_tessera = $('#popup_scelta_cliente [name="codice_tessera"]').val();
        var cellulare = $('#popup_scelta_cliente [name="cellulare"]').val();
        var numero_civico = $('#popup_scelta_cliente [name="numero"]').val();

        var opzione = "";

        comanda.sincro.query('SELECT * FROM clienti WHERE indirizzo LIKE "%' + indirizzo + '%" AND ragione_sociale LIKE "' + ragione_sociale + '%" AND cognome LIKE "' + cognome + '%" AND nome LIKE "' + nome +'%" AND cellulare LIKE "%' + cellulare + '%" AND codice_fiscale LIKE "' + codice_fiscale + '%" AND partita_iva LIKE "' + partita_iva + '%" AND codice_tessera LIKE "' + codice_tessera + '%"AND numero LIKE "' + numero_civico + '%" ORDER BY nome ASC,cognome ASC,ragione_sociale ASC;', function (results) {

            results.forEach(function(row) {

                let style = "";
                let pallina = "";

                if (parseInt(row.id) > 3000000) {
                    //style = " style='background-color:yellow; ' ";
                    pallina = " &bull;";
                }

                opzione += "<option value=\"" + row.id + "\" " + style + ">" + row.nome + " " + row.cognome + pallina;
                if (row.ragione_sociale) {
                    opzione += " - " + row.ragione_sociale;
                }
                opzione += "</option>";
            });
            $('#popup_scelta_cliente_elenco_clienti').html(opzione);
        });
    }
});
$(document).on('keyup change', '#popup_scelta_cliente [name="cognome"]', function() {
    if ($('#popup_scelta_cliente_elenco_clienti option:selected').val() === undefined) {
        var indirizzo = $('#popup_scelta_cliente [name="indirizzo"]').val();
        var ragione_sociale = $('#popup_scelta_cliente [name="ragione_sociale"]').val().trim();
        var cognome = $('#popup_scelta_cliente [name="cognome"]').val().trim();
        var nome = $('#popup_scelta_cliente [name="nome"]').val().trim();
        var codice_fiscale = $('#popup_scelta_cliente [name="codice_fiscale"]').val();
        var partita_iva = $('#popup_scelta_cliente [name="partita_iva"]').val();
        var codice_tessera = $('#popup_scelta_cliente [name="codice_tessera"]').val();
        var cellulare = $('#popup_scelta_cliente [name="cellulare"]').val();
        var numero_civico = $('#popup_scelta_cliente [name="numero"]').val();

        var opzione = "";
        comanda.sincro.query('SELECT * FROM clienti WHERE indirizzo LIKE "%' + indirizzo + '%" AND ragione_sociale LIKE "' + ragione_sociale + '%" AND cognome LIKE "' + cognome + '%" AND nome LIKE "' + nome +'%" AND cellulare LIKE "%' + cellulare + '%" AND codice_fiscale LIKE "' + codice_fiscale + '%" AND partita_iva LIKE "' + partita_iva + '%" AND codice_tessera LIKE "' + codice_tessera + '%"AND numero LIKE "' + numero_civico + '%"  ORDER BY nome ASC,cognome ASC,ragione_sociale ASC;', function (results) {

            results.forEach(function(row) {

                let style = "";
                let pallina = "";

                if (parseInt(row.id) > 3000000) {
                    //style = " style='background-color:yellow; ' ";
                    pallina = " &bull;";
                }

                opzione += "<option value=\"" + row.id + "\" " + style + ">" + row.nome + " " + row.cognome + pallina;
                if (row.ragione_sociale) {
                    opzione += " - " + row.ragione_sociale;
                }
                opzione += "</option>";
            });
            $('#popup_scelta_cliente_elenco_clienti').html(opzione);
        });
    }
});
$(document).on('keyup change', '#popup_scelta_cliente [name="nome"]', function() {
    if ($('#popup_scelta_cliente_elenco_clienti option:selected').val() === undefined) {
        var indirizzo = $('#popup_scelta_cliente [name="indirizzo"]').val();
        var ragione_sociale = $('#popup_scelta_cliente [name="ragione_sociale"]').val().trim();
        var cognome = $('#popup_scelta_cliente [name="cognome"]').val().trim();
        var nome = $('#popup_scelta_cliente [name="nome"]').val().trim();
        var codice_fiscale = $('#popup_scelta_cliente [name="codice_fiscale"]').val();
        var partita_iva = $('#popup_scelta_cliente [name="partita_iva"]').val();
        var codice_tessera = $('#popup_scelta_cliente [name="codice_tessera"]').val();
        var cellulare = $('#popup_scelta_cliente [name="cellulare"]').val();
        var numero_civico = $('#popup_scelta_cliente [name="numero"]').val();

        var opzione = "";

        comanda.sincro.query('SELECT * FROM clienti WHERE indirizzo LIKE "%' + indirizzo + '%" AND ragione_sociale LIKE "' + ragione_sociale + '%" AND cognome LIKE "' + cognome + '%" AND nome LIKE "' + nome +'%" AND cellulare LIKE "%' + cellulare + '%" AND codice_fiscale LIKE "' + codice_fiscale + '%" AND partita_iva LIKE "' + partita_iva + '%" AND codice_tessera LIKE "' + codice_tessera + '%"AND numero LIKE "' + numero_civico + '%" ORDER BY nome ASC,cognome ASC,ragione_sociale ASC;', function (results) {

            results.forEach(function(row) {

                let style = "";
                let pallina = "";

                if (parseInt(row.id) > 3000000) {
                    //style = " style='background-color:yellow; ' ";
                    pallina = " &bull;";
                }


                opzione += "<option value=\"" + row.id + "\" " + style + ">" + row.nome + " " + row.cognome + pallina;
                if (row.ragione_sociale) {
                    opzione += " - " + row.ragione_sociale;
                }
                opzione += "</option>";
            });
            $('#popup_scelta_cliente_elenco_clienti').html(opzione);
        });
    }
});
$(document).on('keyup change', '#popup_scelta_cliente [name="cellulare"]', function() {
    if ($('#popup_scelta_cliente_elenco_clienti option:selected').val() === undefined) {
        var indirizzo = $('#popup_scelta_cliente [name="indirizzo"]').val();
        var ragione_sociale = $('#popup_scelta_cliente [name="ragione_sociale"]').val().trim();
        var cognome = $('#popup_scelta_cliente [name="cognome"]').val().trim();
        var nome = $('#popup_scelta_cliente [name="nome"]').val().trim();
        var cellulare = $('#popup_scelta_cliente [name="cellulare"]').val();
        var codice_fiscale = $('#popup_scelta_cliente [name="codice_fiscale"]').val();
        var partita_iva = $('#popup_scelta_cliente [name="partita_iva"]').val();
        var codice_tessera = $('#popup_scelta_cliente [name="codice_tessera"]').val();
        var numero_civico = $('#popup_scelta_cliente [name="numero"]').val();

        var opzione = "";

        comanda.sincro.query('SELECT * FROM clienti WHERE indirizzo LIKE "%' + indirizzo + '%" AND ragione_sociale LIKE "' + ragione_sociale + '%" AND cognome LIKE "' + cognome + '%" AND nome LIKE "' + nome + '%" AND cellulare LIKE "%' + cellulare +'%" AND codice_fiscale LIKE "' + codice_fiscale + '%" AND partita_iva LIKE "' + partita_iva + '%" AND codice_tessera LIKE "' + codice_tessera + '%"AND numero LIKE "' + numero_civico + '%"  ORDER BY nome ASC,cognome ASC,ragione_sociale ASC;', function (results) {

            results.forEach(function(row) {

                let style = "";
                let pallina = "";

                if (parseInt(row.id) > 3000000) {
                    //style = " style='background-color:yellow; ' ";
                    pallina = " &bull;";
                }

                opzione += "<option value=\"" + row.id + "\" " + style + ">" + row.nome + " " + row.cognome + pallina;
                if (row.ragione_sociale) {
                    opzione += " - " + row.ragione_sociale;
                }
                opzione += "</option>";
            });
            $('#popup_scelta_cliente_elenco_clienti').html(opzione);
        });
    }
});
$(document).on('keyup change', '#popup_scelta_cliente [name="codice_fiscale"]', function() {
    if ($('#popup_scelta_cliente_elenco_clienti option:selected').val() === undefined) {
        var indirizzo = $('#popup_scelta_cliente [name="indirizzo"]').val();
        var ragione_sociale = $('#popup_scelta_cliente [name="ragione_sociale"]').val().trim();
        var cognome = $('#popup_scelta_cliente [name="cognome"]').val().trim();
        var nome = $('#popup_scelta_cliente [name="nome"]').val().trim();
        var codice_fiscale = $('#popup_scelta_cliente [name="codice_fiscale"]').val();
        var partita_iva = $('#popup_scelta_cliente [name="partita_iva"]').val();
        var codice_tessera = $('#popup_scelta_cliente [name="codice_tessera"]').val();
        var cellulare = $('#popup_scelta_cliente [name="cellulare"]').val();
        var numero_civico = $('#popup_scelta_cliente [name="numero"]').val();

        var opzione = "";

        comanda.sincro.query('SELECT * FROM clienti WHERE indirizzo LIKE "%' + indirizzo + '%" AND ragione_sociale LIKE "' + ragione_sociale + '%" AND cognome LIKE "' + cognome + '%" AND nome LIKE "' + nome + '%" AND cellulare LIKE "%' + cellulare +'%" AND codice_fiscale LIKE "' + codice_fiscale + '%" AND partita_iva LIKE "' + partita_iva + '%" AND codice_tessera LIKE "' + codice_tessera + '%"AND numero LIKE "' + numero_civico + '%"  ORDER BY nome ASC,cognome ASC,ragione_sociale ASC;', function (results) {

            results.forEach(function(row) {

                let style = "";
                let pallina = "";

                if (parseInt(row.id) > 3000000) {
                    //style = " style='background-color:yellow; ' ";
                    pallina = " &bull;";
                }


                opzione += "<option value=\"" + row.id + "\" " + style + ">" + row.nome + " " + row.cognome + pallina;
                if (row.ragione_sociale) {
                    opzione += " - " + row.ragione_sociale;
                }
                opzione += "</option>";
            });
            $('#popup_scelta_cliente_elenco_clienti').html(opzione);
        });
    }
});
$(document).on('keyup change', '#popup_scelta_cliente [name="partita_iva"]', function() {
    if ($('#popup_scelta_cliente_elenco_clienti option:selected').val() === undefined) {
        var indirizzo = $('#popup_scelta_cliente [name="indirizzo"]').val();
        var ragione_sociale = $('#popup_scelta_cliente [name="ragione_sociale"]').val().trim();
        var cognome = $('#popup_scelta_cliente [name="cognome"]').val().trim();
        var nome = $('#popup_scelta_cliente [name="nome"]').val().trim();
        var codice_fiscale = $('#popup_scelta_cliente [name="codice_fiscale"]').val();
        var partita_iva = $('#popup_scelta_cliente [name="partita_iva"]').val();
        var codice_tessera = $('#popup_scelta_cliente [name="codice_tessera"]').val();
        var cellulare = $('#popup_scelta_cliente [name="cellulare"]').val();
        var numero_civico = $('#popup_scelta_cliente [name="numero"]').val();

        var opzione = "";
        comanda.sincro.query('SELECT * FROM clienti WHERE indirizzo LIKE "%' + indirizzo + '%" AND ragione_sociale LIKE "' + ragione_sociale + '%" AND cognome LIKE "' + cognome + '%" AND nome LIKE "' + nome + '%" AND cellulare LIKE "%' + cellulare +'%" AND codice_fiscale LIKE "' + codice_fiscale + '%" AND partita_iva LIKE "' + partita_iva + '%" AND codice_tessera LIKE "' + codice_tessera + '%"AND numero LIKE "' + numero_civico + '%"  ORDER BY nome ASC,cognome ASC,ragione_sociale ASC;', function (results) {

            results.forEach(function (row) {

                let style = "";
                let pallina = "";

                if (parseInt(row.id) > 3000000) {
                    //style = " style='background-color:yellow; ' ";
                    pallina = " &bull;";
                }

                opzione += "<option value=\"" + row.id + "\" " + style + ">" + row.nome + " " + row.cognome + pallina;
                if (row.ragione_sociale) {
                    opzione += " - " + row.ragione_sociale;
                }
                opzione += "</option>";
            });
            $('#popup_scelta_cliente_elenco_clienti').html(opzione);
        });
    }
});
$(document).on('keyup change', '#popup_scelta_cliente [name="codice_tessera"]', function () {
    if ($('#popup_scelta_cliente_elenco_clienti option:selected').val() === undefined) {
        var indirizzo = $('#popup_scelta_cliente [name="indirizzo"]').val();
        var ragione_sociale = $('#popup_scelta_cliente [name="ragione_sociale"]').val().trim();
        var cognome = $('#popup_scelta_cliente [name="cognome"]').val().trim();
        var nome = $('#popup_scelta_cliente [name="nome"]').val().trim();
        var codice_fiscale = $('#popup_scelta_cliente [name="codice_fiscale"]').val();
        var partita_iva = $('#popup_scelta_cliente [name="partita_iva"]').val();
        var codice_tessera = $('#popup_scelta_cliente [name="codice_tessera"]').val();
        var cellulare = $('#popup_scelta_cliente [name="cellulare"]').val();
        var numero_civico = $('#popup_scelta_cliente [name="numero"]').val();

        var opzione = "";
        comanda.sincro.query('SELECT * FROM clienti WHERE indirizzo LIKE "%' + indirizzo + '%" AND ragione_sociale LIKE "' + ragione_sociale + '%" AND cognome LIKE "' + cognome + '%" AND nome LIKE "' + nome + '%" AND cellulare LIKE "%' + cellulare +'%" AND codice_fiscale LIKE "' + codice_fiscale + '%" AND partita_iva LIKE "' + partita_iva + '%" AND codice_tessera LIKE "' + codice_tessera + '%"AND numero LIKE "' + numero_civico + '%"  ORDER BY nome ASC,cognome ASC,ragione_sociale ASC;', function (results) {

            results.forEach(function(row) {

                let style = "";
                let pallina = "";

                if (parseInt(row.id) > 3000000) {
                    //style = " style='background-color:yellow; ' ";
                    pallina = " &bull;";
                }

                opzione += "<option value=\"" + row.id + "\" " + style + ">" + row.nome + " " + row.cognome + pallina;
                if (row.ragione_sociale) {
                    opzione += " - " + row.ragione_sociale;
                }
                opzione += "</option>";
            });
            $('#popup_scelta_cliente_elenco_clienti').html(opzione);
        });
    }
});
$(document).on('keyup change', '#popup_scelta_cliente [name="numero"]', function () {
    if ($('#popup_scelta_cliente_elenco_clienti option:selected').val() === undefined) {
        var indirizzo = $('#popup_scelta_cliente [name="indirizzo"]').val();
        var ragione_sociale = $('#popup_scelta_cliente [name="ragione_sociale"]').val().trim();
        var cognome = $('#popup_scelta_cliente [name="cognome"]').val().trim();
        var nome = $('#popup_scelta_cliente [name="nome"]').val().trim();
        var codice_fiscale = $('#popup_scelta_cliente [name="codice_fiscale"]').val();
        var partita_iva = $('#popup_scelta_cliente [name="partita_iva"]').val();
        var codice_tessera = $('#popup_scelta_cliente [name="codice_tessera"]').val();
        var cellulare = $('#popup_scelta_cliente [name="cellulare"]').val();
        var numero_civico = $('#popup_scelta_cliente [name="numero"]').val();

        var opzione = "";
        comanda.sincro.query('SELECT * FROM clienti WHERE indirizzo LIKE "%' + indirizzo + '%" AND ragione_sociale LIKE "' + ragione_sociale + '%" AND cognome LIKE "' + cognome + '%" AND nome LIKE "' + nome + '%" AND cellulare LIKE "%' + cellulare +'%" AND codice_fiscale LIKE "' + codice_fiscale + '%" AND partita_iva LIKE "' + partita_iva + '%" AND codice_tessera LIKE "' + codice_tessera + '%" AND numero LIKE "' + numero_civico + '%" ORDER BY nome ASC,cognome ASC,ragione_sociale ASC;', function (results) {

            results.forEach(function(row) {

                let style = "";
                let pallina = "";

                if (parseInt(row.id) > 3000000) {
                    //style = " style='background-color:yellow; ' ";
                    pallina = " &bull;";
                }

                opzione += "<option value=\"" + row.id + "\" " + style + ">" + row.nome + " " + row.cognome + pallina;
                if (row.ragione_sociale) {
                    opzione += " - " + row.ragione_sociale;
                }
                opzione += "</option>";
            });
            $('#popup_scelta_cliente_elenco_clienti').html(opzione);
        });
    }
});
function printer_status() {
    try {
        var xml = '<printerCommand><queryPrinterStatus operator="" statusType="" /></printerCommand>';
        var epos = new epson.fiscalPrint();
        epos.send("http://" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=local_printer&timeout=30000", xml, 30000);
        epos.onreceive = function(result, tag_names_array, add_info, res_add) {
            try {

                /* Firmware della scheda del Misuratore */
                var cpuRel = add_info.cpuRel;

                /* Firmware della Memoria Fiscale (DGFE) */
                var mfRel = add_info.mfRel;

                $("#versione_firmware").val(cpuRel);


            } catch (e1) {}
        };

        var xml2 = '<printerCommand><directIO  command="1138" data="01" /></printerCommand>';
        var epos2 = new epson.fiscalPrint();
        epos2.send("http://" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=local_printer&timeout=30000", xml2, 30000);
        epos2.onreceive = function(result, tag_names_array, add_info, res_add) {
            try {

                let firmwareBuild = add_info.responseData.substr(33, 4);

                $("#revisione_firmware").val(firmwareBuild);


            } catch (e1) {}
        };

    } catch (e) {}
}

function PrimeLettereMaiuscole(str) {
    if (typeof(str) === "string") {
        return str.replace(/\w\S*/g, function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    } else {
        return str;
    }
}

function vedi_categorie_successive() {
    $('#tab_sx').scrollTop($('#tab_sx').scrollTop() + $('#tab_sx a:visible')[0].offsetHeight);
}

function vedi_categorie_precedenti() {
    $('#tab_sx').scrollTop($('#tab_sx').scrollTop() - $('#tab_sx a:visible')[0].offsetHeight);
}


if (nome_pagina.indexOf("LAYOUT_RISTORANTE") !== -1) {

    var touch_cat = new Object();
    $(document).on('touchstart mousedown', '.elenco_categorie_list', function(e) {
        if (e.originalEvent.touches !== undefined) {
            var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];

        } else {
            var touch = e.originalEvent;


        }
        touch_cat.startx = touch.pageX;
        touch_cat.starty = touch.pageY;

    });
    $(document).on('touchend mouseup', '.elenco_categorie_list', function(e) {
        if (e.originalEvent.touches !== undefined) {
            var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];

        } else {
            var touch = e.originalEvent;


        }
        touch_cat.endx = touch.pageX;
        touch_cat.endy = touch.pageY;

        if ((touch_cat.startx - touch_cat.endx > 20) && (touch_cat.starty - touch_cat.endy <= 10)) {
            try {
                touch_cat.endx = undefined;
                touch_cat.endy = undefined;
                $('#tab_sx').scrollTop($('#tab_sx').scrollTop() + $('#tab_sx a:visible')[0].offsetHeight);
            } catch (e) {
                touch_cat.endx = undefined;
                touch_cat.endy = undefined;
                console.log(e);
            }

        } else if ((touch_cat.endx - touch_cat.startx > 20) && (touch_cat.starty - touch_cat.endy <= 10)) {
            try {
                touch_cat.endx = undefined;
                touch_cat.endy = undefined;
                $('#tab_sx').scrollTop($('#tab_sx').scrollTop() - $('#tab_sx a:visible')[0].offsetHeight);
            } catch (e) {
                touch_cat.endx = undefined;
                touch_cat.endy = undefined;
                console.log(e);
            }
        } else {

            touch_cat.endx = undefined;
            touch_cat.endy = undefined;
        }
    });
}

if (nome_pagina.indexOf("tastiera") !== -1 || nome_pagina.indexOf("LAYOUT_RISTORANTE") !== -1) {

    var touch_servito = new Object();
    $(document).on('touchstart', '#conto tr', function(e) {
        var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
        touch_servito.startx = touch.pageX;
        touch_servito.starty = touch.pageY;
        var id_articolo = e.currentTarget.id;
    });
    $(document).on('touchend', '#conto tr', function(e) {
        var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
        var id_articolo = e.currentTarget.id;
        var numero_progressivo = id_articolo.replace('art_', '');
        touch_servito.endx = touch.pageX;
        touch_servito.endy = touch.pageY;
        if ((touch_servito.startx - touch_servito.endx > 150) && (touch_servito.starty - touch_servito.endy <= 10)) {
            try {
                var query = "select nodo from comanda where ntav_comanda='" + comanda.tavolo + "' and prog_inser='" + numero_progressivo + "' and stato_record='ATTIVO' limit 1;";
                comanda.sincro.query(query, function(result) {
                    try {
                        touch_servito.endx = undefined;
                        touch_servito.endy = undefined;
                        var nodo_principale = result[0].nodo.substr(0, 3);
                        comanda.clona_articolo_nodo = nodo_principale;
                        articolo_servito();
                    } catch (e1) {
                        alert(e1);
                    }
                });
            } catch (e) {
                alert(e);
            }

        } else {
            touch_servito.endx = undefined;
            touch_servito.endy = undefined;
        }
    });
} else {
    //PER MOBILE

    var touch_servito = new Object();
    $(document).on('touchstart', '#conto tr', function(e) {
        var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
        touch_servito.startx = touch.pageX;
        touch_servito.starty = touch.pageY;
        var id_articolo = e.currentTarget.id;
    });
    $(document).on('touchend', '#conto tr', function(e) {
        var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
        var id_articolo = e.currentTarget.id;
        var numero_progressivo = id_articolo.replace('art_', '');
        touch_servito.endx = touch.pageX;
        touch_servito.endy = touch.pageY;
        //end-start perchè in questo caso è verso destra
        if ((touch_servito.endx - touch_servito.startx > 100) && (touch_servito.starty - touch_servito.endy <= 10)) {
            try {
                var query = "select nodo from comanda where ntav_comanda='" + comanda.tavolo + "' and prog_inser='" + numero_progressivo + "' and stato_record='ATTIVO' limit 1;";
                comanda.sincro.query(query, function(result) {
                    try {
                        touch_servito.endx = undefined;
                        touch_servito.endy = undefined;
                        var nodo_principale = result[0].nodo.substr(0, 3);
                        comanda.clona_articolo_nodo = nodo_principale;
                        clona_articolo(1);
                    } catch (e1) {
                        alert(e1);
                    }
                });
            } catch (e) {
                alert(e);
            }

        } else if ((touch_servito.startx - touch_servito.endx > 100) && (touch_servito.starty - touch_servito.endy <= 10)) {
            try {
                elimina_articolo(numero_progressivo, true);
            } catch (e) {
                alert(e);
            }
        } else {
            touch_servito.endx = undefined;
            touch_servito.endy = undefined;
        }
    });
    //SU ULTIME BATTITURE
    var touch_servito_ub = new Object();
    $(document).on('touchstart', '.table.ultime_battiture tr', function(e) {
        var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
        touch_servito_ub.startx = touch.pageX;
        touch_servito_ub.starty = touch.pageY;
        var id_articolo = e.currentTarget.id;
    });
    $(document).on('touchend', '.table.ultime_battiture tr', function(e) {
        var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
        var id_articolo = e.currentTarget.id;
        var numero_progressivo = id_articolo.replace('art_', '');
        touch_servito_ub.endx = touch.pageX;
        touch_servito_ub.endy = touch.pageY;
        //end-start perchè in questo caso è verso destra
        if ((touch_servito_ub.endx - touch_servito_ub.startx > 100) && (touch_servito_ub.starty - touch_servito_ub.endy <= 10)) {
            try {
                var query = "select nodo from comanda where ntav_comanda='" + comanda.tavolo + "' and prog_inser='" + numero_progressivo + "' and stato_record='ATTIVO' limit 1;";
                comanda.sincro.query(query, function(result) {
                    try {
                        touch_servito_ub.endx = undefined;
                        touch_servito_ub.endy = undefined;
                        var nodo_principale = result[0].nodo.substr(0, 3);
                        comanda.clona_articolo_nodo = nodo_principale;
                        clona_articolo(1);
                    } catch (e1) {
                        alert(e1);
                    }
                });
            } catch (e) {
                alert(e);
            }

        } else if ((touch_servito_ub.startx - touch_servito_ub.endx > 100) && (touch_servito_ub.starty - touch_servito_ub.endy <= 10)) {
            try {
                elimina_articolo(numero_progressivo, true);
            } catch (e) {
                alert(e);
            }
        } else {
            touch_servito_ub.endx = undefined;
            touch_servito_ub.endy = undefined;
        }
    });
}

function azzera_popup_ristampa_documenti_fiscali() {
    $("[name='ristampa_da_data']").val("");
    $("[name='ristampa_a_data']").val("");
    $("[name='ristampa_da_n_scontrino']").val("");
    $("[name='ristampa_a_n_scontrino']").val("");
    $("[name='ristampa_da_n_fattura']").val("");
    $("[name='ristampa_a_n_fattura']").val("");
    $("[name='ristampa_da_n_chiusura']").val("");
    $("[name='ristampa_a_n_chiusura']").val("");

    $("#riquadro_ristampa_fiscale").html("");
    $("#riquadro_ristampa_fiscale").hide();
    $("#titoli_documenti_selezionati").hide();
    $("#lista_documenti_selezionati").hide();
    $(".ristampa_fisc").hide();
}

function popup_ristampa_documenti_fiscali() {

    azzera_popup_ristampa_documenti_fiscali();

    $('.tasto_ristampa_documenti_fiscali').removeClass('btn-info2');

    //$("#popup_ristampa_documenti_fiscali").modal("show");

    $('#archivio_scontrini').hide();
    $('#tab_ristampa_documenti_fiscali').show();
    $('#tab_ristampa_documenti_fiscali .row').show();
}

function rinomina_file_xml(nome_file, callBack) {



    var request = $.ajax({
        timeout: 10000,
        method: "POST",
        url: comanda.url_server + '/classi_php/rinomina_scontrino_non_fiscale.php',
        data: { nome_file: nome_file }
    });


    request.done(function(data) {

        callBack(true);

    });

    request.fail(function(event) {


        alert("ATTENZIONE! Sei fuori portata WiFi. Controlla che la comanda non sia già stata STAMPATA,\nprima di premere di nuovo 'COMANDA'.");

        callBack(false);

    });

}




function stampa_scontrino_non_intelligent(dati_scontrino, tipo, dati_testa, callBack) {

    var totale_scontrino = $(comanda.totale_scontrino).html();
    var md5 = CryptoJS.MD5(dati_scontrino).toString();

    var counter = 0;
    var nome_file = "scontrino" + new Date().getTime();

    ajaxCall();

    function ajaxCall() {

        counter++;




        var request = $.ajax({
            timeout: 10000,
            method: "POST",
            url: comanda.url_server + '/classi_php/salva_scontrino_non_intellinet.php',
            data: { nome_file: nome_file, struttura: dati_scontrino }
        });


        request.done(function(data) {

            if (data === md5) {
                rinomina_file_xml(nome_file, function(res) {

                    callBack(true);
                    $('.loader').hide();

                });

            } else {
                if (counter === 5) {


                    $('.loader').hide();
                    alert("ATTENZIONE! Sei fuori portata WiFi. Avvicinati all'antenna e riprova.");

                } else {
                    ajaxCall();
                }
            }
        });

        request.fail(function(event) {


            if (counter === 5) {


                $('.loader').hide();
                alert("ATTENZIONE! Sei fuori portata WiFi. Avvicinati all'antenna e riprova.");
            } else {
                ajaxCall();
            }
        });
    }


}

function btn_tavoli(tipo) {



    btn_tavoli: {

        let tavolo_cache = comanda.tavolo;
        if (comanda.tavolo.left(7) === "ASPORTO") {
            tavolo_cache = "ASPORTO";
        }

        var obj = { "intestazione": "", "conto": "" };
        socket_fast_split.send(JSON.stringify(obj));

        var obj = { "totale": "0.00", "residuo": "Totale" };
        socket_fast_split.send(JSON.stringify(obj));


        //NEL DUBBIO MEGLIO LASCIARLO
        //$('#rmenu').hide();
        console.trace();
        var testo_query = "";
        if (tipo !== 'scontrino' && comanda.pizzeria_asporto !== true && $("#totale_scontrino").html() !== undefined && $("#totale_scontrino").html() !== '0.00' && (tavolo_cache === "BANCO" || tavolo_cache === "BAR" || tavolo_cache === "ASPORTO" || tavolo_cache === "TAKE AWAY")) {
            btn_parcheggia(true);
            //SE NON METTO QUESTA QUERY GENERA ERRORE NEL BAR AL SALVATAGGIO
            //testo_query = "select count(0);";
            break btn_tavoli;
        } else if (tavolo_cache !== 0 && tavolo_cache !== '0' && tipo !== "comanda" && tipo !== "scontrino") {



            if (comanda.dispositivo !== "TAVOLO SELEZIONATO") {
                testo_query = "update tavoli set occupato='0' where numero='" + tavolo_cache + "' ; ";
            } else if ((tipo === undefined || tipo === null) && $('#totale_scontrino').html() !== '0.00' && $('#conto tr[id^="art_"]').not('.comanda').not('.non-contare-riga').length >= 1 && $(comanda.conto + ' tr:contains("elaborazione")').length === 0) {
                testo_query = "update tavoli set occupato='0',colore= CASE WHEN colore != '3'  THEN '2' ELSE colore END  where numero='" + tavolo_cache + "' ; ";
                console.log("colore1");
            } else if ((tipo === undefined || tipo === null) && $('#totale_scontrino').html() === '0.00' && $('#conto tr[id^="art_"]').not('.comanda').not('.non-contare-riga').length === 0) {
                console.trace();
                testo_query = "update tavoli set colore= CASE WHEN colore = '2'  THEN 'grey' ELSE colore END ,ora_apertura_tavolo='-',ora_ultima_comanda='-',occupato='0' where numero='" + tavolo_cache + "' ; ";
                console.log("colore2");
            } else if ($('#conto').find('tr[id^="art_"]:not(:contains("%")).nascondibile').length > 0 && $('#conto').find('tr[id^="art_"]:not(:contains("%"))').not('.nascondibile').length === 0) {
                var ora = comanda.funzionidb.data_attuale().substr(9, 8);
                //COLORE 0 1410
                var testo_query = "update tavoli SET occupato='0',ora_apertura_tavolo = ( CASE WHEN ( ora_apertura_tavolo = '-' OR ora_apertura_tavolo = 'null' OR ora_apertura_tavolo = ''  OR ora_apertura_tavolo = NULL ) THEN '" + ora + "' ELSE ora_apertura_tavolo END ), ora_ultima_comanda = (  CASE WHEN ( ora_apertura_tavolo = 'null'  OR ora_apertura_tavolo = '' OR ora_apertura_tavolo = '-'  OR ora_apertura_tavolo = NULL ) THEN ora_ultima_comanda ELSE '" + ora + "' END )  where numero='" + tavolo_cache + "';";
                console.log("colore4");
            } else if (tipo === undefined || tipo === null) {
                testo_query = "update tavoli set occupato='0' where numero='" + tavolo_cache + "' ; ";
                console.log("colore3");
            } else {
                testo_query = "update tavoli set colore= CASE WHEN cast(colore as int) <= 4  THEN '0' ELSE colore END ,occupato='0' where numero='" + tavolo_cache + "' ; ";
                console.log("colore4");
            }

        } else {
            testo_query = "select count(0);";
        }

        //PER RESETTARE IL PREZZO QUANDO ESCE DAL TAVOLO
        $("#totale_scontrino").html('0.00');
        comanda.permesso = 0;
        comanda.sock.send({ tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });

        disegna_ultimo_tavolo_modificato(testo_query);
        //VA QUI ALTRIMENTI SI PERDE LA SINCRONIA CON L'ULTIMO TAVOLO OCCUPATO
        comanda.sincro.query_cassa();

        $("#campo_qta_menu_fisso").val("1");

        comanda.sincro.query(testo_query, function() {
            console.log("OP2");
            //DISABILITATO SENNO' RIPETE LE SINCRONIA LUNGHE

            console.log("btn_tavoli MAPPA TAVOLI");

            $('#qta_menu_fisso').hide();


            $('#tab_sx').scrollTop(0);


            selezione_operatore('MAPPA TAVOLI');
            //25-11-2016 aggiunta
            //PS BLOCCA LA MEMORIZZAZIONE DELLA COMANDA SU BAR
            comanda.tavolo = "0";

            //TEMP (DA ABILITARE E RISTUDIARE)
            //comanda.sincro.sincronizzazione_upload('tavoli');
            comanda.categoria = comanda.categoria_partenza;
            comanda.pagina = "1";
            $('#numero_coperti').val('0');
            if (comanda.mobile === false) {
                $("#tab_sx a").removeClass("cat_accesa");

                try {
                    $("#tab_sx a:nth-child(" + comanda.categoria_partenza + ")").addClass("cat_accesa");
                } catch (e) {

                }

            }


            //TEMP DISABLED
            //elenca_pagine_esistenti();
        });
    }

}

function proporzione_d(valore) {

    //853:100=valore:x

    var x = 100 * valore / 6000;
    return '0';
}


function proporzione(valore) {

    //853:100=valore:x

    //var x = 100 * valore / 2000;
    //x = 0;

    var x = valore / 80;
    return x + 'vh';
}

/*function proporzione_x(valore) {
 
 
 //853:100=valore:x
 
 var x = (100 * valore / 1500) - 11;
 return x;
 }
 
 function proporzione_y(valore) {
 
 //853:100=valore:x
 
 
 var x = (100 * (-valore + 1685.98) / 2100) + 12;
 return x;
 }*/


function proporzione_x_vh(y) {

    var viewport_editor_y = 380;

    var r = (100 * y / viewport_editor_y);

    return r;
}

function proporzione_y_vh(x) {

    var viewport_editor_x = 640;

    var r = (100 * x / viewport_editor_x);


    return r;
}

function proporzione_x(y) {
    var viewport_pc_x = 1908;
    var viewport_pc_y = 937;

    if (comanda.dispositivo === "MAPPA TAVOLI PHONE") {
        var viewport_pda_x = 360;
        var viewport_pda_y = 640;
    } else {
        var viewport_pda_x = 411;
        var viewport_pda_y = 823;
    }

    var altbarramenupc = 80;

    var r = (y * viewport_pda_x / viewport_pc_y) - (altbarramenupc / 2);

    return r;
}

function proporzione_y(x) {

    var viewport_pc_x = 1908;
    var viewport_pc_y = 937;

    if (comanda.dispositivo === "MAPPA TAVOLI PHONE") {
        var viewport_pda_x = 360;
        var viewport_pda_y = 640;
    } else {
        var viewport_pda_x = 411;
        var viewport_pda_y = 823;
    }


    var tasti_cameriere = 63;


    var r = tasti_cameriere + (x * (viewport_pda_y - tasti_cameriere) / viewport_pc_x);
    return r;
}

/*function prop_x_inv(x) {
 
 var viewport = 360;
 
 var r = viewport * x / 360;
 return r;
 }
 
 function prop_y_inv(y) {
 var viewport = 640;
 
 var r = viewport * (y - 200) / 640;
 return r;
 }*/

/**
 * 
 var viewport_pc_x = 1908;
 var viewport_pc_y = 937;
 
 var viewport_pda_x = 360;
 var viewport_pda_y = 640;
 */

function prop_x_inv(x) {
    if (comanda.dispositivo === "MAPPA TAVOLI PHONE") {
        var viewport_pda_x = 360;
        var viewport_pda_y = 640;
    } else {
        var viewport_pda_x = 411;
        var viewport_pda_y = 823;
    }


    var r = viewport_pda_x * x / 360;
    return r;
}

function prop_y_inv(y) {
    if (comanda.dispositivo === "MAPPA TAVOLI PHONE") {
        var viewport_pda_x = 360;
        var viewport_pda_y = 640;
    } else {
        var viewport_pda_x = 411;
        var viewport_pda_y = 823;
    }

    var r = viewport_pda_y * (y - 200) / 640;
    return r;
}

function assegna_stampante_a_tavolo(stampante, tavolo) {

    var numero_stampante = stampante.replace("CONTO", "").replace("KONTO", "").replace("_", "");

    if (numero_stampante.trim() === "") {
        numero_stampante = "1";
    }

    var testo_query = "update tavoli set n_st_conto='" + numero_stampante + "' where numero='" + tavolo + "';";

    comanda.sock.send({ tipo: "aggiornamento_tavoli", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });

    comanda.sincro.query(testo_query, function() {

        disegna_tavoli_salvati();

    });

}

function assegna_stampante_tavoli(nome_stampante) {

    comanda.stampante_da_assegnare = nome_stampante;

    selezione_operatore('MAPPA TAVOLI STAMPANTI');
    comanda.dispositivo = 'MAPPA TAVOLI STAMPANTI';
    disegna_tavoli_salvati();
}

function selettore_assegnazione_stampante() {

    try {
        $('div.tavolo.rett,div.tavolo.cerc').draggable();
        $('div.tavolo.rett,div.tavolo.cerc').resizable();
        $('div.tavolo.rett,div.tavolo.cerc').draggable('destroy');
        $('div.tavolo.rett,div.tavolo.cerc').resizable('destroy');
        $('div.tavolo.rett,div.tavolo.cerc').off(comanda.eventino);
    } catch (e) {
        console.log(e);
    }
    $('div.tavolo.rett,div.tavolo.cerc').on(comanda.eventino, function() {

        var date = new Date();

        var hour = date.getHours();

        if (parseInt(hour) <= 3) {
            date.setDate(date.getDate() - 1);
        }

        var tavolo = $(this).attr('id').substr(4);

        var stampante = comanda.stampante_da_assegnare;

        assegna_stampante_a_tavolo(stampante, tavolo);

    });

}

function disegna_tavoli_salvati(numero) {

    console.log("DISEGNA TAVOLI SALVATI", numero);
    console.trace();
    Class_Servizio.servizio(function(callback_servizio) {
        aggiornamento_tavoli_xml(function() {
            console.log("disegna_tavoli_salvati (btn_tavoli)");
            console.log("disegna_tavoli_salvati (aggiornamento_tavoli_xml)");
            console.log("disegna_tavoli_salvati", "inizio", console.trace());

            var tav_uni = alasql("select fila_tavoli from tavoli_uniti;");
            var lista_tavoli_uniti = new Array();

            tav_uni.forEach(v => {
                v.fila_tavoli.split(";").forEach(v2 => {
                    lista_tavoli_uniti.push(v2);
                });
            });

            var testo_query = "";
            var nome_sala = "nome_sala";
            if ((comanda.compatibile_xml !== true && comanda.mobile === true) || comanda.dispositivo === "MAPPA TAVOLI PHONE") {
                nome_sala = "nome_sala_smartphone";
            }

            if (numero !== undefined) {

                //CICLETTO COSI SONO SICURO
                while ($('#tav_' + numero).length > 0) {
                    console.log("COLORITAVOLI -RIMOZIONE: ", numero);
                    console.log("NUMEROXXX " + numero + " REMOVE");
                    $('#tav_' + numero).remove();
                }

                console.log(numero);


                testo_query = "select * from tavoli where numero='" + numero + "' and abilitato='1' and " + nome_sala + "='" + comanda.nome_sala + "';";
            } else {
                //console.log("DISEGNA_TAVOLI_SALVATI");
                $('.tavolo.rett,.tavolo.cerc').remove();
                testo_query = "select * from tavoli where abilitato='1' and " + nome_sala + "='" + comanda.nome_sala + "';";
            }

            //console.log(testo_query);
            comanda.sincro.query(testo_query, function(result) {

                var i = 0;
                var c = result.length;

                result.forEach(function(obj) {

                    var font_tavolo = "color:white;";
                    if (comanda.mobile === true) {
                        font_tavolo = "color:black;";
                    }

                    var testo_query_tav_collegato = "select * from tavoli where collegamento='" + obj.numero + "' and abilitato='1' and " + nome_sala + "='" + comanda.nome_sala + "';";

                    var r = alasql(testo_query_tav_collegato);

                    if (r.length > 0) {
                        font_tavolo = "color:orange;";
                    } else if (obj.collegamento !== null && obj.collegamento !== "") {
                        font_tavolo = "color:yellow;";
                    } else if (lista_tavoli_uniti.indexOf(obj.numero) !== -1) {
                        font_tavolo = "color:#66FF00;";
                    }


                    //SERVE PER ADATTARE LE PIANTINE SIA AI DISPOSITIVI MOBILE CHE AL PC
                    //OVVIAMENTE VA REVISIONATO UN PO IL DATABASE MANUALMENTE PER ORA
                    //19-01-2017
                    if (comanda.mobile === true) {
                        var x = obj.pos_x_mobile;
                        var y = obj.pos_y_mobile;


                        obj.pos_x = proporzione_x_vh(x) + "vw";

                        obj.pos_y = proporzione_y_vh(y) + "vh";


                        if (!(obj.amm !== undefined && obj.amm !== null && obj.amm !== "")) {
                            obj.amm = "0";
                        }
                        if (!(obj.lmm !== undefined && obj.lmm !== null && obj.lmm !== "")) {
                            obj.lmm = "0";
                        }
                        obj.larghezza = parseFloat(obj.lmm) + "px";
                        obj.altezza = parseFloat(obj.amm) + "px";
                    } else if (comanda.dispositivo === "MAPPA TAVOLI PHONE") {



                        var gap = 200;
                        var x = obj.pos_x_mobile;
                        var y = parseFloat(obj.pos_y_mobile) + gap;

                        if (obj.xmm !== undefined && obj.xmm !== null && obj.xmm !== "") {
                            obj.pos_x = (parseFloat(x) + parseFloat(obj.xmm)) + "px";
                        } else {
                            obj.pos_x = x + "px";
                        }

                        if (obj.ymm !== undefined && obj.ymm !== null && obj.ymm !== "") {
                            obj.pos_y = (parseFloat(y) + parseFloat(obj.ymm)) + "px";
                        } else {
                            obj.pos_y = y + "px";
                        }

                        if (!(obj.amm !== undefined && obj.amm !== null && obj.amm !== "")) {
                            obj.amm = "0";
                        }
                        if (!(obj.lmm !== undefined && obj.lmm !== null && obj.lmm !== "")) {
                            obj.lmm = "0";
                        }
                        obj.larghezza = parseFloat(obj.lmm) + "px";
                        obj.altezza = parseFloat(obj.amm) + "px";
                    } else {
                        obj.pos_x += 'px';
                        obj.pos_y += 'px';

                        obj.larghezza += 'px';
                        obj.altezza += 'px';
                    }


                    if (obj.ora_apertura_tavolo === 'null') {
                        obj.ora_apertura_tavolo = '-';
                    }
                    if (obj.ora_ultima_comanda === 'null') {
                        obj.ora_ultima_comanda = '-';
                    }


                    var colore_tavolo = "";
                    var colore_scritta = "";

                    if (comanda.dispositivo === "MAPPA TAVOLI STAMPANTI") {
                        i++;
                        if (obj.n_st_conto === '1') {
                            colore_tavolo = 'background-color:brown;background-image: linear-gradient(to bottom, #33CCFF,#3366FF);';
                        } else if (obj.n_st_conto === '2') {
                            colore_tavolo = 'background-color:brown;background-image: linear-gradient(to bottom, #CC6699,#CC0099);';
                        } else if (obj.n_st_conto === '3') {
                            colore_tavolo = 'background-color:brown;background-image: linear-gradient(to bottom, #33CC00,#336600);';
                        } else {
                            colore_tavolo = 'background-color:silver;background-image: linear-gradient(to bottom, silver, #797979);';
                        }



                        var id_tavolo = obj.numero;

                        comanda.tipo_mappa = "PC";

                        $('#contenitore_tavoli').prepend('<div style="' + colore_tavolo + 'width:' + obj.larghezza + '; height:' + obj.altezza + ';top:' + obj.pos_y + ';left:' + obj.pos_x + ';" class="' + obj.classe + '" id="tav_' + id_tavolo + '" ><h4></h4><h6><b style="' + font_tavolo + '" class="' + obj.numero + '">' + obj.numero + '</b><br>' + obj.ora_apertura_tavolo + '<br/>' + obj.ora_ultima_comanda + '</h6></div>');


                        if (c === i) {
                            //altrimenti non so perchè ma non lo fa senza timeout
                            //non fa ora a caricare la piantina tipo
                            setTimeout(selettore_assegnazione_stampante, 500);
                        }


                    } else if (obj.numero !== "BANCO" && obj.numero !== "BAR" && obj.numero.left(7) !== "ASPORTO" && obj.numero !== "TAKE AWAY") {
                        i++;
                        if (obj.occupato === '1') {
                            colore_tavolo = 'background-color:brown;background-image: linear-gradient(to bottom, brown,#3c1b1b);';
                        } else if (obj.colore === '3') {
                            colore_tavolo = 'background-color:cyan; background-image: linear-gradient(to bottom, cyan, #3498db);';
                        } else if (obj.colore === '4' || obj.colore === 'azure') {
                            colore_tavolo = 'background-color:#3498db;background-image: linear-gradient(to bottom, #3498db, #2677ac);';
                        } else if (obj.colore === '6' || obj.colore === 'fucsia') {
                            colore_tavolo = 'background-color:FUCHSIA;background-image: linear-gradient(to bottom,FUCHSIA, #b400b4);';
                        } else if (obj.colore === '2') {
                            colore_tavolo = 'background-color:#FFA07A;background-image: linear-gradient(to bottom,#FFA07A, #b06c52);';
                        } else if (obj.colore === 'blue') {
                            colore_tavolo = 'background-color:blue;background-image: linear-gradient(to bottom,blue,#000093);';
                        } else if (obj.colore === 'green') {
                            colore_tavolo = 'background-color:green;background-image: linear-gradient(to bottom, green,#005200);';
                        }
                        //XOR è' un or esclusivo. In pratica è vero se è vera solo una delle due condizioni. se sono vere tutte 2 è falsa. (vedi 'Disgiunzione esclusiva' Wikipedia)
                        else if (obj.ora_apertura_tavolo !== 0 && obj.ora_apertura_tavolo !== null && obj.ora_apertura_tavolo !== 'null' && obj.ora_apertura_tavolo !== '' && obj.ora_apertura_tavolo !== '-') {
                            colore_tavolo = 'background-color:red;background-image: linear-gradient(to bottom,red, #800c00);';
                        } else {
                            colore_tavolo = 'background-color:silver;background-image: linear-gradient(to bottom, silver, #797979);';
                        }


                        var id_tavolo = obj.numero;


                        if (comanda.dispositivo === "MAPPA TAVOLI PHONE") {

                            comanda.tipo_mappa = "MOBILE";

                            var fontmobile = "font-size:2vh;";
                            if (obj.font_mobile !== undefined && obj.font_mobile !== null && obj.font_mobile !== "") {
                                fontmobile = "font-size:" + (parseFloat(obj.font_mobile) / 2).toFixed(2) + "vh;";
                            }
                            $('#contenitore_tavoli').prepend('<div style="' + colore_tavolo + 'padding:5px 10px;width:' + obj.larghezza + '; height:' + obj.altezza + ';top:' + obj.pos_y + ';left:' + obj.pos_x + ';" class="' + obj.classe + '" id="tav_' + id_tavolo + '" ><h4></h4><h6><b style="' + fontmobile + '' + font_tavolo + '" class="' + obj.numero + '">' + obj.numero + '</b></h6></div>');

                        } else if (comanda.mobile === true && obj.disabilita_cellulare !== "true") {

                            comanda.tipo_mappa = "MOBILE";

                            var fontmobile = "";
                            if (obj.font_mobile !== undefined && obj.font_mobile !== null && obj.font_mobile !== "") {
                                fontmobile = "font-size:" + obj.font_mobile + "vh;";
                            }
                            $('#contenitore_tavoli').prepend('<div style="' + colore_tavolo + 'width:' + obj.larghezza + '; height:' + obj.altezza + ';top:' + obj.pos_y + ';left:' + obj.pos_x + ';" class="' + obj.classe + '" id="tav_' + id_tavolo + '" ><h4></h4><h6><b style="' + fontmobile + '' + font_tavolo + '" class="' + obj.numero + '">' + obj.numero + '</b></h6></div>');
                        } else if (comanda.mobile !== true) {
                            comanda.tipo_mappa = "PC";
                            if (obj.numero === "BAR" || obj.numero.left(7) === "ASPORTO") {
                                $('#contenitore_tavoli').prepend('<div style="' + colore_tavolo + 'width:' + obj.larghezza + '; height:' + obj.altezza + ';top:' + obj.pos_y + ';left:' + obj.pos_x + ';" class="' + obj.classe + '" id="tav_' + id_tavolo + '" ><h4></h4><h6><b style="' + font_tavolo + '" class="' + obj.numero + '">' + obj.numero + '</b></h6></div>');
                            } else {
                                $('#contenitore_tavoli').prepend('<div style="' + colore_tavolo + 'width:' + obj.larghezza + '; height:' + obj.altezza + ';top:' + obj.pos_y + ';left:' + obj.pos_x + ';" class="' + obj.classe + '" id="tav_' + id_tavolo + '" ><h4></h4><h6><b style="' + font_tavolo + '" class="' + obj.numero + '">' + obj.numero + '</b><br>' + obj.ora_apertura_tavolo + '<br/>' + obj.ora_ultima_comanda + '</h6></div>');
                            }
                        }


                        if (c === i) {
                            if (comanda.permesso !== 0 && comanda.mobile === false) {

                                make_draggable();
                            } else {
                                selettore_tavolo();
                            }

                            //A QUESTO CONCATENO L'ARRAY ESISTENTE DEI SOCKET CON QUELLA TEMPORANEA E COSI FACENDO LANCIO I SOCKET
                            comanda.socket_ciclizzati = comanda.socket_ciclizzati.concat(comanda.socket_ciclizzati_temp);
                            comanda.socket_ciclizzati_temp = [];
                        }

                    } else {
                        var verifica_record_aperti = "SELECT stato_record FROM comanda WHERE ntav_comanda='" + obj.numero + "' and (stato_record='APERTO' or stato_record='FORNO') LIMIT 1;";
                        comanda.sincro.query(verifica_record_aperti, function(stato_record) {
                            i++;
                            if (stato_record !== undefined && stato_record[0] !== undefined && stato_record[0].stato_record !== undefined) {
                                stato_record = stato_record[0].stato_record;
                            } else {
                                stato_record = 'ATTIVO';
                            }

                            if (obj.occupato === '1') {
                                colore_scritta = 'color:#ffffff;';
                                font_tavolo = 'color:#ffffff;';
                                colore_tavolo = 'background-color:brown;background-image: linear-gradient(to bottom, brown,#3c1b1b);';
                            } else if ((stato_record === 'APERTO' || stato_record === 'FORNO') || (obj.ora_apertura_tavolo !== 0 && obj.ora_apertura_tavolo !== null && obj.ora_apertura_tavolo !== 'null' && obj.ora_apertura_tavolo !== '' && obj.ora_apertura_tavolo !== '-')) {
                                colore_scritta = 'color:red;';
                                font_tavolo = 'color:red;';
                                colore_tavolo = 'background-color:silver;background-image: linear-gradient(to bottom, silver, #797979);';
                            } else {
                                colore_scritta = 'color:#ffffff;';
                                font_tavolo = 'color:#ffffff;';
                                colore_tavolo = 'background-color:silver;background-image: linear-gradient(to bottom, silver, #797979);';
                            }
                            //if (comanda.mobile !== true) {

                            var id_tavolo = obj.numero;

                            if (comanda.dispositivo === "MAPPA TAVOLI PHONE") {

                                comanda.tipo_mappa = "MOBILE";

                                var fontmobile = "font-size:2vh;";
                                if (obj.font_mobile !== undefined && obj.font_mobile !== null && obj.font_mobile !== "") {
                                    fontmobile = "font-size:" + (parseFloat(obj.font_mobile) / 2).toFixed(2) + "vh;";
                                }
                                $('#contenitore_tavoli').prepend('<div style="' + colore_scritta + colore_tavolo + 'width:' + obj.larghezza + '; height:' + obj.altezza + ';top:' + obj.pos_y + ';left:' + obj.pos_x + ';" class="' + obj.classe + '" id="tav_' + id_tavolo + '"><h4></h4><h6 style=' + colore_scritta + '><b style="' + fontmobile + '' + font_tavolo + '" class="' + obj.numero + '">' + obj.numero + '</b><!--<br/>' + obj.ora_apertura_tavolo + '<br/>' + obj.ora_ultima_comanda + '--></h6></div>');

                            } else if ((comanda.mobile === true && obj.disabilita_cellulare !== "true")) {

                                comanda.tipo_mappa = "MOBILE";

                                var fontmobile = "";
                                if (comanda.mobile === true) {

                                    if (obj.font_mobile !== undefined && obj.font_mobile !== null && obj.font_mobile !== "") {
                                        fontmobile = "font-size:" + obj.font_mobile + "vh;";
                                    }
                                }
                                $('#contenitore_tavoli').prepend('<div style="' + colore_scritta + colore_tavolo + 'width:' + obj.larghezza + '; height:' + obj.altezza + ';top:' + obj.pos_y + ';left:' + obj.pos_x + ';" class="' + obj.classe + '" id="tav_' + id_tavolo + '"><h4></h4><h6 style=' + colore_scritta + '><b style="' + fontmobile + '' + font_tavolo + '" class="' + obj.numero + '">' + obj.numero + '</b><!--<br/>' + obj.ora_apertura_tavolo + '<br/>' + obj.ora_ultima_comanda + '--></h6></div>');
                            } else if (comanda.mobile !== true) {
                                comanda.tipo_mappa = "PC";

                                var fontmobile = "";
                                if (comanda.mobile === true) {

                                    if (obj.font_mobile !== undefined && obj.font_mobile !== null && obj.font_mobile !== "") {
                                        fontmobile = "font-size:" + obj.font_mobile + "vh;";
                                    }
                                }

                                if (obj.numero === "BAR" || obj.numero.left(7) === "ASPORTO") {
                                    $('#contenitore_tavoli').prepend('<div style="' + colore_scritta + colore_tavolo + 'width:' + obj.larghezza + '; height:' + obj.altezza + ';top:' + obj.pos_y + ';left:' + obj.pos_x + ';" class="' + obj.classe + '" id="tav_' + id_tavolo + '"><h4></h4><h6 style=' + colore_scritta + '><b style="' + fontmobile + '' + font_tavolo + '" class="' + obj.numero + '">' + obj.numero + '</b></h6></div>');
                                } else {
                                    $('#contenitore_tavoli').prepend('<div style="' + colore_scritta + colore_tavolo + 'width:' + obj.larghezza + '; height:' + obj.altezza + ';top:' + obj.pos_y + ';left:' + obj.pos_x + ';" class="' + obj.classe + '" id="tav_' + id_tavolo + '"><h4></h4><h6 style=' + colore_scritta + '><b style="' + fontmobile + '' + font_tavolo + '" class="' + obj.numero + '">' + obj.numero + '</b><br/>' + obj.ora_apertura_tavolo + '<br/>' + obj.ora_ultima_comanda + '</h6></div>');
                                }
                            }

                            console.log("COLORITAVOLI -DISEGNO TAVOLO: ", obj.numero);
                            if (c === i) {
                                if (comanda.permesso !== 0 && comanda.mobile === false) {

                                    make_draggable();
                                } else {
                                    selettore_tavolo();
                                }

                                //A QUESTO CONCATENO L'ARRAY ESISTENTE DEI SOCKET CON QUELLA TEMPORANEA E COSI FACENDO LANCIO I SOCKET
                                comanda.socket_ciclizzati = comanda.socket_ciclizzati.concat(comanda.socket_ciclizzati_temp);
                                comanda.socket_ciclizzati_temp = [];
                            }
                        });
                    }

                });
            });
        });
    });
}



function scelta_profilo(callback) {

    var r = alasql("SELECT id,nome_profilo FROM settaggi_profili;");

    if (r.length === 0) {

        $('#popup_attesa_socket').modal('hide');
        $('#messaggi_caricamento').hide();
        $('#loading').hide();
        $('#game-over').modal('show');
        bootbox.alert("NESSUN PROFILO DISPONIBILE!");
    } else if (r.length === 1) {
        var nome_profilo = r[0].nome_profilo;
        comanda.folder_number = r[0].id;
        comanda.nome_servizio = nome_profilo;
        comanda.servizio = nome_profilo;
        callback(true);
    } else {
        var out = "";
        r.forEach(function(e) {
            out += "<tr><td style='height: 20vh;vertical-align: inherit;display: table-cell;' nome_servizio= '" + e.nome_profilo + "' id_servizio='" + e.id + "'>" + e.nome_profilo + "</td></tr>";
        });

        $('#popup_scelta_servizio .modal-body .table').html(out);
        $('#popup_scelta_servizio').modal('show');

        $(document).on(comanda.eventino, '#popup_scelta_servizio td', function(event) {
            var nome_profilo = event.target.getAttribute('nome_servizio');

            document.title = "fast.intellinet - " + nome_profilo;

            comanda.folder_number = event.target.getAttribute('id_servizio');
            comanda.nome_servizio = nome_profilo;
            comanda.servizio = nome_profilo;
            $('#popup_scelta_servizio').modal('hide');
            callback(true);
            $(document).off(comanda.eventino, '#popup_scelta_servizio td');
        });
    }

}

function scarica_apk() {
    window.location.href = comanda.url_server + "aggiornamenti/android-debug.apk";

    bootbox.confirm("Il file dell'aggiornamento si sta scaricando.<br><br>Al termine dello scaricamento dovrai Aprirlo ed Installarlo.<br><br>Premendo OK il programma verr&agrave; chiuso e dovrai effettuare l'aggiornamento manualmente.", function(r) {
        if (r === true) {
            chiudi_programma();
        }
    });
}


function selettore_tavolo() {

    console.trace();
    try {
        $('div.tavolo.rett,div.tavolo.cerc').draggable();
        $('div.tavolo.rett,div.tavolo.cerc').resizable();
        $('div.tavolo.rett,div.tavolo.cerc').draggable('destroy');
        $('div.tavolo.rett,div.tavolo.cerc').resizable('destroy');
        $('div.tavolo.rett,div.tavolo.cerc').off(comanda.eventino);
    } catch (e) {
        console.log(e);
    }
    $('div.tavolo.rett,div.tavolo.cerc').on(comanda.eventino, function() {

        var testo_query = "";
        var date = new Date();

        var hour = date.getHours();

        if (parseInt(hour) <= 3) {
            date.setDate(date.getDate() - 1);
        }

        var ora = addZero(date.getHours(), 2) + ':' + addZero(date.getMinutes(), 2);
        var tavolo = $(this).attr('id').substr(4);

        var tavolotxt = $(this).find('h6>b').attr('class');

        if (comanda.mobile !== true) {
            if (tavolo.toUpperCase().indexOf("BAR") !== -1) {
                if (esperimento_output[comanda.cp_bar] !== undefined) {
                    comanda.categoria = comanda.cp_bar;
                    comanda.categoria_predefinita = comanda.cp_bar;
                    comanda.categoria_partenza = comanda.cp_bar;
                    comanda.ultima_categoria_principale = comanda.cp_bar;
                }
            } else if (tavolo.toUpperCase().indexOf("ASPORTO") !== -1) {
                if (esperimento_output[comanda.cp_asporto] !== undefined) {

                    comanda.categoria = comanda.cp_asporto;
                    comanda.categoria_predefinita = comanda.cp_asporto;
                    comanda.categoria_partenza = comanda.cp_asporto;
                    comanda.ultima_categoria_principale = comanda.cp_asporto;
                }
            } else if (tavolo.toUpperCase().indexOf("CONTINUO") !== -1) {
                if (esperimento_output[comanda.cp_continuo] !== undefined) {

                    comanda.categoria = comanda.cp_continuo;
                    comanda.categoria_predefinita = comanda.cp_continuo;
                    comanda.categoria_partenza = comanda.cp_continuo;
                    comanda.ultima_categoria_principale = comanda.cp_continuo;
                }
            } else {
                if (esperimento_output[comanda.cp_tavoli] !== undefined) {

                    comanda.categoria = comanda.cp_tavoli;
                    comanda.categoria_predefinita = comanda.cp_tavoli;
                    comanda.categoria_partenza = comanda.cp_tavoli;
                    comanda.ultima_categoria_principale = comanda.cp_tavoli;
                }
            }

        }

        blocco_concreto(tavolo);

    });

}

function blocco_concreto(tavolo, tavolotxt) {

    comanda.somma_progressivi = 0;

    /* altrimenti rientrando nel tavolo e battendo un articolo me lo mette come variante dell'articolo premuto prima di chiudere il tavolo
     * nel caso l'ultima cosa che ho fatto fosse premere + o -
     * */
    comanda.ultima_opz_variante = "";
    comanda.tavolo_appena_aperto = true;

    var id_tavolo = tavolo;

    if ($('#calendario_prendi_ordine:visible').length !== 1) {
        var bool_calendario = false;
        var testo_query = 'select occupato,collegamento from tavoli where numero="' + id_tavolo + '"; ';
    } else {
        var bool_calendario = true;
        var testo_query = 'select "0" as occupato, "" as collegamento from tavoli limit 1; ';
    }
    comanda.sincro.query(testo_query, function(result) {

        if (result[0].occupato !== '1') {
            if (result[0].collegamento !== undefined && result[0].collegamento !== null && result[0].collegamento !== '') {
                bootbox.alert("Ordinare nel tavolo " + result[0].collegamento);
            } else {
                if (bool_calendario === false) {
                    var testo_query_up = "update tavoli set occupato='1' where numero='" + tavolo + "';";
                } else {
                    var testo_query_up = "update tavoli set occupato='0' where numero='INESISTENTE';";
                }
                console.log("CASSA 440");
                console.trace();
                comanda.sock.send({ tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query_up, terminale: comanda.terminale, ip: comanda.ip_address });
                //A QUESTO CONCATENO L'ARRAY ESISTENTE DEI SOCKET CON QUELLA TEMPORANEA E COSI FACENDO LANCIO I SOCKET
                comanda.socket_ciclizzati = comanda.socket_ciclizzati.concat(comanda.socket_ciclizzati_temp);
                comanda.socket_ciclizzati_temp = [];
                comanda.sincro.query(testo_query_up, function() {

                    comanda.sincro.query_cassa();
                    //TEMP DISABLED
                    //comanda.sincro.sincronizzazione_upload('tavoli');
                });
                comanda.tavolo = tavolo;

                var tid = alasql("select TID from comanda where ntav_comanda='" + id_tavolo + "' and stato_record='ATTIVO';");

                if (tid[0] !== undefined) {
                    comanda.TID = tid[0].TID;
                } else {
                    comanda.TID = "";
                }

                //riempie il conto con il tavolo giusto
                comanda.funzionidb.conto_attivo();
                $('.nome_parcheggio').html(comanda.parcheggio);
                $('.nome_operatore').html("" /*comanda.operatore*/ );

                $('.numero_tavolo').html(comanda.tavolo);

                /*try {
                 if (comanda.pizzeria_asporto !== true && comanda.tavolo === "ASPORTO") {
                 $("#fattura").attr("id", "fattura_buffer");
                 $("#popup_scelta_cliente").attr("id", "fattura");
                 } else {
                 $("#fattura").attr("id", "popup_scelta_cliente");
                 $("#fattura_buffer").attr("id", "fattura");
                 }
                 } catch (e) {
                 
                 }*/

                selezione_operatore("TAVOLO SELEZIONATO");
                if (comanda.mobile === true) {
                    window.scrollTo(0, 0);
                    if (comanda.apri_subito_categoria_predefinita === true) {
                        $('.alfabeto').show();
                        mod_categoria(comanda.categoria_predefinita.toString(), '', '1');
                        $("#tab_sx a", 'body').removeClass("cat_accesa");
                        $('#tab_sx a.cat_' + comanda.categoria_predefinita, 'body').addClass("cat_accesa");
                        $('.alfabeto>.position>div', 'body').removeClass('hover');
                        $('.alfabeto>.position>div:first-child', 'body').addClass('hover');
                        categoria_cliccata();
                    } else {
                        elenco_categorie_main();
                    }
                }

                if (comanda.tavolo === 'BAR') {
                    elenco_comande_aperte();
                } else if (comanda.pizzeria_asporto !== true && comanda.tavolo.left(7) === "ASPORTO") {
                    righe_forno();
                    elenco_comande_aperte();
                }

            }
        } else if (comanda.sblocca_tavolo_cliccato === false) {

            var query_op = "select operatore from comanda where ntav_comanda='" + id_tavolo + "' order by cast(prog_inser as int) desc limit 1;";
            var op = alasql(query_op);
            if (op[0] !== undefined) {
                bootbox.alert(lang[121].replace("...", op[0].operatore));
            } else {
                bootbox.alert("Tavolo in uso da operatore sconosciuto");
            }

        }



    });
}


function nuovo_tavolo() {
    var errori = false;
    var numero = $('#numero_tavolo').val();
    if (numero.length < 1) {
        alert("Devi assegnare obbligatoriamente un numero o un nome al tavolo.");
        errori = true;
    }

    $('div.tavolo.rett,div.tavolo.cerc').each(function() {

        if ($(this).find('h4').html() === numero && errori === false) {
            alert("Errore. Il tavolo con quel numero già esiste");
            errori = true;
        }
    });
    var forma = $('.forma_selezionata').attr('class');
    forma = forma.replace('forma_selezionata', '');
    forma = forma.replace('-min', '');
    ////console.log(forma);
    //forma = forma.slice(0, -4);


    if (forma === undefined) {
        alert("Devi scegliere obbligatoriamente una forma per il tavolo.");
    }

    if (errori === false) {

        var top = ($('#contenitore_tavoli').offset().top) + 20;
        var left = ($('#contenitore_tavoli').offset().left) + 20;

        alasql("delete from tavoli where numero='" + numero + "';");


        var testo_query = "insert into tavoli (nome_sala_smartphone,nome_sala,classe,numero,abilitato,altezza,larghezza,pos_x,pos_y,colore,ora_apertura_tavolo,ora_ultima_comanda,colore) VALUES ('" + comanda.nome_sala + "','" + comanda.nome_sala + "','" + forma + " ui-draggable ui-draggable-handle ui-resizable','" + numero + "','1','103','95','" + top + "','" + left + "','grey','-','-','0');";
        comanda.sock.send({ tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
        alasql(testo_query);
        comanda.sincro.query_cassa();

        ////console.log(top, left);
        $('#contenitore_tavoli').append('<div style="top:' + top + 'px;left:' + left + 'px;" class="' + forma + '" id="tav_' + numero + '" ><h4></h4><h6><b class="' + numero + '">' + numero + '</b><br/>-<br/>-</h6></div>');
        make_draggable();
        //deletor();
        $('#numero_tavolo').val('');
    }

}



function once(fn) {

    if (comanda.cassa_singola === true) {
        fn.apply(null, arguments);
    } else {
        var run = true;

        var timeout = setTimeout(function() {
            if (run) {
                //fn(false);
                run = false;
                alert("ATTENZIONE! RTEngine bloccati a causa di Problemi di Rete. Non puoi continuare a lavorare in queste condizioni.\n\nAssicurati che la rete funzioni bene, e riavvia il programma");
                $('#popup_attesa_socket').modal('hide');
                $('#messaggi_caricamento').hide();
                $('#loading').hide();
                $('#game-over').modal('show');
                window.close();
            }
        }, 20000);

        return function() {
            if (run) {
                fn.apply(null, arguments);
                //resolve(true);
                clearTimeout(timeout);
                run = false;
            }
        };
    }
}

//FILE BROWSER FATTURE
$(document).on(comanda.eventino, '#uploadTrigger', function(e) {
    $("#uploadFile").trigger(comanda.eventino);
});
$(document).on('change', '[name="soldi_ricevuti"]', function(e) {

    console.log("SOLDI RESTO");
    $('[name="soldi_resto"]').val(($('[name="soldi_ricevuti"]').val() - $('[name="importo_da_saldare"]').val()).toFixed(2));
});

function aggiornamento_tavoli_xml(callback) {
    callback(true);
}

$(document).on('change', '#uploadFile', function(e) {
    var file_data = $('#uploadFile').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);
    $.ajax({
        url: comanda.url_server + 'classi_php/upload.php', // point to server-side PHP script
        dataType: 'text', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function(php_script_response) {
            var randomId = new Date().getTime();
            $(".button").css("background-image", "url(./uploads/logo?random=" + randomId + ")");
        }
    });
});

$(document).on(comanda.eventino, '.elenco_prodotti_draggable li', function(e) {

    if (comanda.dispositivo === "LAYOUT TASTI" || comanda.dispositivo === "lang_33") {
        comanda.lastelement = $(e.target);

        var numero_prod = $(comanda.lastelement).closest("li").attr('value');
        $(comanda.lastelement).closest("li").css('background-color', comanda.colore_tasto);
        $(comanda.lastelement).closest("li").css('background-image', '-webkit-linear-gradient(top, ' + comanda.colore_tasto + ', rgba(0, 0, 0, 0.35))');

        esperimento_output[comanda.categoria][comanda.pagina] = $(".pag_" + comanda.pagina + ".cat_" + comanda.categoria)[0].outerHTML;

        if (comanda.colore_tasto !== null && comanda.colore_tasto !== undefined && comanda.colore_tasto !== '' && comanda.colore_tasto !== 'undefined') {
            var testo_query = "UPDATE prodotti SET colore_tasto='" + comanda.colore_tasto + "' WHERE id='" + numero_prod + "';";
        } else {
            var testo_query = "UPDATE prodotti SET colore_tasto=colore_tasto WHERE id='" + numero_prod + "';";
        }
        comanda.sock.send({ tipo: "aggiornamento_prodotti", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });


        if (comanda.query_su_socket === false) {
            console.log("ARRAY_DBCENTRALE_MANCANTI.PUSH", testo_query);
            comanda.array_dbcentrale_mancanti.push(testo_query);
            if (comanda.compatibile_xml !== true) {
                log_query_db_centrale(testo_query);
            }
        }

        comanda.sincro.query(testo_query, function() {

        });

        comanda.sincro.query_cassa();
    }

});

//GESTIONE CACHE
//tolta il 26 agosto perche deprecata
/*window.applicationCache.oncached = function (e) {
 console.log("CACHE SALVATA");
 $('.spia.cache').css('background-color', 'green');
 };
 window.applicationCache.onnoupdate = function (e) {
 console.log("CACHE GIUSTA");
 $('.spia.cache').css('background-color', 'green');
 };
 window.applicationCache.onupdateready = function (e) {
 $('.spia.cache').css('background-color', 'green');
 window.applicationCache.swapCache();
 console.log("CACHE RINNOVATA");
 };
 window.applicationCache.onerror = function (e) {
 console.log("CACHE RINNOVABILE");
 $('.spia.cache').css('background-color', 'red');
 };*/

$(window).on('load',
    function() {
        $('body').addClass('loading');
        //console.log("loading");
    });
$(document).on('keyup', '#form_gestione_categorie [name="descrizione"]', function() {

    console.log("KEYUP1");
    if ($('#form_gestione_categorie [name="descrizione"]').val().substr(0, 2) !== "V." && $('#form_gestione_categorie input').length <= 2) {
        console.log("KEYUP2");
        $('#form_gestione_categorie [name="descrizione"]').val("V.");
    }

});
//PER LA GESTIONE PRODOTTI
$(document).on('change', "#gestione_prodotti_scelta_categoria", function() {

    console.log("GESITONE PRODOTTO", $(this));
    comanda.categoria = $(this).val();
    selezione_operatore('lang_27');
    scegli_default();
});
//PER IL CAMBIO LISTINO
$(document).on('change', "#cambio_listino_scelta_categoria", function() {

    console.log("GESITONE PRODOTTO", $(this));
    comanda.categoria = $(this).val();
    selezione_operatore('CAMBIO LISTINO');
    scegli_default();
});

var oggetto_pony = new Object();
comanda.categorie_con_consegna_abilitata = new Array();

function popup_password_inserimento_settaggi_android() {
    bootbox.prompt({
        size: "medium",
        title: "Inserisci la Password di Amministratore",
        inputType: "password",
        callback: function(result) {

            if (result === "FICHS") {

                popup_inserimento_settaggi_android();

            } else {
                bootbox.alert("Password Sbagliata. Ritenta e sarai pi&ugrave; fortunato!");
            }
        }
    });
}


function popup_inserimento_settaggi_android() {

    var key = "configurazione_base";

    var successCallback = function(preferenze_android) {

        var settings = preferenze_android.split(";");

        if (settings[0] !== undefined) {
            $("#popup_configurazione_base #porta_server_remoto").val(settings[0]);
        }

        if (settings[1] !== undefined) {
            $("#popup_configurazione_base #url_server").val(settings[1]);
        }

        if (settings[2] !== undefined) {
            $("#popup_configurazione_base #ip_server").val(settings[2]);
        }

        if (settings[3] !== undefined) {
            $("#popup_configurazione_base #indirizzo_access_point").val(settings[3]);
        }

        if (settings[4] !== undefined) {
            $("#popup_configurazione_base #terminale").val(settings[4]);
        }

        $("#popup_configurazione_base").modal("show");
    };

    var errorCallback = function(err) {

        $("#popup_configurazione_base").modal("show");

    };

    sharedPreferences.getString(key, successCallback, errorCallback);



}

function salva_configurazione_base() {
    var settaggi = new Array();

    settaggi.push($("#popup_configurazione_base #porta_server_remoto").val());
    settaggi.push($("#popup_configurazione_base #url_server").val());
    settaggi.push($("#popup_configurazione_base #ip_server").val());
    settaggi.push($("#popup_configurazione_base #indirizzo_access_point").val());
    settaggi.push($("#popup_configurazione_base #terminale").val());

    var settaggi_salvabili = settaggi.join(";");

    var key = "configurazione_base";


    var successCallback = function(value) {

    };
    var errorCallback = function(err) {

    };

    sharedPreferences.putString(key, settaggi_salvabili, successCallback, errorCallback);

    $("#popup_configurazione_base").modal("hide");
    location.reload();
}

$(document).ready(
    function() {
        if (comanda.mobile === true && comanda.simulazione_iphone !== true) {
            document.addEventListener("deviceready", inizio_software, false);
        } else {
            inizio_software();
        }
    });

var sharedPreferences = new Object();

sharedPreferences.getString = function(key, cb) {
    cb("PC");
};

function gotFS(fileSystem) {
    fileSystem.root.getFile("logAndroid_" + new Date().format("yyyymmdd"), { create: true, exclusive: false }, gotFileEntry, fail);
}

function gotFileEntry(fileEntry) {
    fileEntry.createWriter(gotFileWriter, fail);
}

let androidFileWriter = null;

function gotFileWriter(writer) {
    androidFileWriter = writer;
}

function fail(error) {
    console.log(error.code);
}

function appendAndroidLog(string) {
    androidFileWriter.seek(androidFileWriter.length);
    androidFileWriter(string);
}

var classe_ping = null;
var dct = null;
async function inizio_software() {

    comanda.k1 = 'bRuD5WYw5wd';

    if (typeof Ping !== "undefined") {

        classe_ping = new Ping();

        pong("127.0.0.1", function(r) {
            console.log("LOCALHOST " + r);
        });

    }

    comanda.k2 = '0rdHR9yLlM6';

    if (window.requestFileSystem !== undefined) {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
    }

    if (window.plugins !== undefined && window.plugins.SharedPreferences !== undefined) {
        sharedPreferences = window.plugins.SharedPreferences.getInstance();
    }

    comanda.k3 = 'wt2vteuiniQ';

    var key = 'configurazione_base';

    comanda.k4 = 'BqE70nAuhU=';

    sharedPreferences.getString(key, function(preferenze_android) {

        if (preferenze_android.trim() === "") {
            popup_inserimento_settaggi_android();
        } else {

            if (preferenze_android !== "PC") {

                var settings = preferenze_android.split(";");
                if (settings[0] !== undefined && settings[0].trim() !== "") {
                    comanda.porta_server_remoto = settings[0];
                } else {
                    popup_inserimento_settaggi_android();
                }

                if (settings[1] !== undefined && settings[0].trim() !== "") {
                    comanda.url_server = settings[1];
                } else {
                    popup_inserimento_settaggi_android();
                }

                if (settings[2] !== undefined && settings[0].trim() !== "") {
                    comanda.ip_server = settings[2];
                } else {
                    popup_inserimento_settaggi_android();
                }

                if (settings[3] !== undefined && settings[0].trim() !== "") {
                    comanda.indirizzo_access_point = settings[3];
                } else {
                    popup_inserimento_settaggi_android();
                }

                if (settings[4] !== undefined && settings[0].trim() !== "") {
                    comanda.terminale = settings[4];
                } else {
                    popup_inserimento_settaggi_android();
                }

            }

            if (comanda.server_replica_abilitato === true) {
                $.ajax({
                    type: "POST",
                    async: false,
                    timeout: 120000,
                    url: 'http://localhost:' + comanda.porta_server_remoto + '/fast.intellinet/classi_php/trasferimentodbCentraleBackOffice.php',
                    data: {
                        ip_server_centrale_remoto: comanda.url_server_replica
                    }
                });
            }

            //comanda.abilita_log_palmare = true;
            //comanda.id_tablet = "PA";


            console.log("AVVIO PROGRAMMA");
            $('body').addClass('loading');
            console.log("AVVIO PROGRAMMA");
            if (comanda.numero_licenza_cliente !== "0629" && comanda.numero_licenza_cliente !== "0656") {
                listen_tastierino_num();
            }
            console.log("AVVIO PROGRAMMA");
            comanda.sincro.sincronizzazione_download(function() {




                let testo_alert = "";

                let c = alasql("select descrizione,perc_iva,perc_iva_takeaway, reparto_servizi, reparto_beni from categorie;");
                c.forEach((v) => {
                    if (v.perc_iva === undefined || v.perc_iva === null || v.perc_iva === "undefined" || v.perc_iva === "null" || v.perc_iva === "undefined" || v.perc_iva.trim() === "") {
                        testo_alert += "La categoria " + v.descrizione + " contiene una percentuale IVA Base non valida<br>";
                    } else if (v.perc_iva_takeaway === undefined || v.perc_iva_takeaway === null || v.perc_iva_takeaway === "undefined" || v.perc_iva_takeaway === "null" || v.perc_iva_takeaway === "undefined" || v.perc_iva_takeaway.trim() === "") {
                        testo_alert += "La categoria " + v.descrizione + " contiene una percentuale IVA Takeaway non valida<br>";
                    } else if (v.reparto_servizi === undefined || v.reparto_servizi === null || v.reparto_servizi === "undefined" || v.reparto_servizi === "null" || v.reparto_servizi === "undefined" || v.reparto_servizi.trim() === "") {
                        testo_alert += "La categoria " + v.descrizione + " contiene il Reparto dei Servizi non valido<br>";
                    } else if (v.reparto_beni === undefined || v.reparto_beni === null || v.reparto_beni === "undefined" || v.reparto_beni === "null" || v.reparto_beni === "undefined" || v.reparto_beni.trim() === "") {
                        testo_alert += "La categoria " + v.descrizione + " contiene il Reparto dei Beni non valido<br>";
                    }
                });

                let d = alasql("select descrizione,perc_iva_base,perc_iva_takeaway, reparto_servizi, reparto_beni from prodotti where categoria!='XXX';");
                d.forEach((v) => {
                    if (v.perc_iva_base === undefined || v.perc_iva_base === null || v.perc_iva_base === "undefined" || v.perc_iva_base === "null" || v.perc_iva_base === "undefined" || v.perc_iva_base.trim() === "") {
                        testo_alert += "Il prodotto " + v.descrizione + " contiene una percentuale IVA Base non valida<br>";
                    } else if (v.perc_iva_takeaway === undefined || v.perc_iva_takeaway === null || v.perc_iva_takeaway === "undefined" || v.perc_iva_takeaway === "null" || v.perc_iva_takeaway === "undefined" || v.perc_iva_takeaway.trim() === "") {
                        testo_alert += "Il prodotto " + v.descrizione + " contiene una percentuale IVA Takeaway non valida<br>";
                    } else if (v.reparto_servizi === undefined || v.reparto_servizi === null || v.reparto_servizi === "undefined" || v.reparto_servizi === "null" || v.reparto_servizi === "undefined" || v.reparto_servizi.trim() === "") {
                        testo_alert += "Il prodotto " + v.descrizione + " contiene il Reparto dei Servizi non valido<br>";
                    } else if (v.reparto_beni === undefined || v.reparto_beni === null || v.reparto_beni === "undefined" || v.reparto_beni === "null" || v.reparto_beni === "undefined" || v.reparto_beni.trim() === "") {
                        testo_alert += "Il prodotto " + v.descrizione + " contiene il Reparto dei Beni non valido<br>";
                    }
                });



                if (comanda.selezione_centri_abilitata !== true) {
                    $('#scelta_centri_backoffice').hide;
                }



                console.log("AVVIO PROGRAMMA");
                //LO SFONDO DEL BODY

                if (comanda.pizzeria_asporto === true) {

                    var query_categorie_con_calcolo_consegna = 'SELECT id FROM categorie WHERE consegna_abilitata="true" ORDER BY id ASC;';


                    comanda.sincro.query(query_categorie_con_calcolo_consegna, function(categorie_con_calcolo_consegna) {


                        categorie_con_calcolo_consegna.forEach(function(categorie_con_calcolo_consegna) {

                            comanda.categorie_con_consegna_abilitata.push(categorie_con_calcolo_consegna.id);

                        });

                    });


                }

                crea_oggetto_pony();
                //scelta_profilo(function (nome_profilo) {

                var query_layout_PC = "select * from settaggi_profili where id=" + comanda.folder_number + " limit 1;";
                comanda.sincro.query(query_layout_PC, function(layout_PC) {

                    if (layout_PC !== undefined && layout_PC[0] !== undefined && layout_PC[0].Field190 !== undefined && layout_PC[0].Field190 !== null && typeof(layout_PC[0].Field190) === 'string') {
                        switch (layout_PC[0].Field190) {
                            case '3':
                                $('body').css('background-color', '#414141');
                                break;
                            case '2':
                                $('body').css('background-color', '#353535');
                                break;
                            default:

                        }
                    }

                    if (comanda.pizzeria_asporto === true) {
                        if (layout_PC[0].Field418 !== "true") {
                            $(".visualizza_totale_visore").remove();
                        }
                    }

                    comanda.nome_sala = layout_PC[0].piantina_partenza;

                    if (layout_PC[0].Field353 !== "true") {
                        //RIPETUTA IN CASSA>ELENCO PRODOTTI>MAPPA TAVOLI
                        $(".apertura_piantina").hide();
                    } else {
                        $(".apertura_piantina").show();
                    }

                    comanda.apertura_piantina = layout_PC[0].Field353;

                    if (layout_PC[0].Field355 !== "true") {
                        $(".apertura_ordinazione").remove();

                    }
                    if (layout_PC[0].Field353 !== "true") {
                        $(".apertura_comanda").remove();
                    }

                    if (layout_PC[0].Field354 !== "true") {
                        $(".apertura_ordine").remove();
                        $(".tasto_reset_ordine").removeClass("col-xs-10");
                        $(".tasto_reset_ordine").addClass("col-xs-12");
                        $(".tasto_reset_ordine").css("padding-right", "");
                    }

                    comanda.abilita_chiusura_ordine = layout_PC[0].Field473;

                    comanda.consumazione_in_pizzeria = layout_PC[0].Field372;

                    comanda.ordine_articoli_video = layout_PC[0].Field395;
                    comanda.tipo_orario_forno = layout_PC[0].Field468;

                    comanda.scritta_consegna_domicilio = layout_PC[0].Field375;
                    comanda.scritta_consegna_ritiro = layout_PC[0].Field376;
                    comanda.scritta_consegna_pizzeria = layout_PC[0].Field377;

                    comanda.apertura_conto = layout_PC[0].Field355;
                    comanda.apertura_scontrino = layout_PC[0].Field356;

                    comanda.numero_tavolo_scontrino = layout_PC[0].Field327;

                    comanda.meno_specificato = layout_PC[0].Field307;

                    comanda.tasto_piu = layout_PC[0].Field297;
                    comanda.tasto_meno = layout_PC[0].Field298;
                    comanda.tasto_specifiche = layout_PC[0].Field299;
                    comanda.tasto_pococotto = layout_PC[0].Field300;
                    comanda.tasto_finecottura = layout_PC[0].Field301;
                    comanda.tasto_gusti_gelato = layout_PC[0].Field302;

                    comanda.tasto_menu_completo = layout_PC[0].Field304;

                    comanda.categoria_gusti = layout_PC[0].Field303;

                    comanda.url_fastorder = layout_PC[0].Field502;

                    if (comanda.tasto_piu === "false") {
                        $('.tasto_piu').remove();
                    }

                    if (comanda.tasto_meno === "false") {
                        $('.tasto_meno').remove();
                    }

                    if (comanda.tasto_specifiche === "false") {
                        $('.tasto_specifiche').remove();
                    }

                    //TEMPORANEAMENTE
                    /*if (comanda.mobile === true) {
                     $('.tasto_specifiche').remove();
                     }*/

                    if (comanda.tasto_pococotto === "false") {
                        if (comanda.mobile === true) {
                            $('.tasto_pococotto').hide();
                            $('.tasto_pococotto').css('visibility', 'hidden');
                        } else {
                            $('.tasto_pococotto').remove();
                        }
                    }

                    if (comanda.tasto_finecottura === "false") {
                        if (comanda.mobile === true) {
                            $('.tasto_finecottura').hide();
                            $('.tasto_finecottura').css('visibility', 'hidden');
                        } else {
                            $('.tasto_finecottura').remove();
                        }
                    }

                    if (comanda.tasto_gusti_gelato === "false") {
                        //non remove altrimenti si sposta la posizione di + e -
                        if (comanda.mobile === true) {
                            $('.tasto_gusti_gelato').hide();
                            $('.tasto_gusti_gelato').css('visibility', 'hidden');
                        } else {
                            $('.tasto_gusti_gelato').remove();
                        }
                    }

                    /* integrazione 07/11/2018 */
                    if (comanda.tasto_pococotto !== "false" || comanda.tasto_finecottura !== "false") {
                        $('.tasto_gusti_gelato').remove();
                    } else if (comanda.tasto_gusti_gelato !== "false") {
                        $('.tasto_pococotto').remove();
                        $('.tasto_finecottura').remove();
                    } else {
                        $('.tasto_gusti_gelato').remove();
                    }

                    if (comanda.tasto_menu_completo === "false") {
                        $('.tasto_menu_completo').remove();
                    }

                    if (layout_PC[0].Field308 === "true") {
                        var styleEl = document.createElement('style'),
                            styleSheet;

                        document.head.appendChild(styleEl);

                        styleSheet = styleEl.sheet;
                        styleSheet.insertRule('.nascondibile:not(.BAR){display:table-row;}', 0);
                    }

                    comanda.scontrino_msg_riga1 = layout_PC[0].Field236;
                    comanda.scontrino_msg_riga1_grassetto = layout_PC[0].Field241;
                    comanda.scontrino_msg_riga1_allineamento = layout_PC[0].Field242;
                    comanda.scontrino_spazio1 = layout_PC[0].Field237;
                    comanda.scontrino_msg_riga2 = layout_PC[0].Field238;
                    comanda.scontrino_msg_riga2_grassetto = layout_PC[0].Field243;
                    comanda.scontrino_msg_riga2_allineamento = layout_PC[0].Field244;
                    comanda.scontrino_spazio2 = layout_PC[0].Field239;
                    comanda.scontrino_msg_riga3 = layout_PC[0].Field240;
                    comanda.scontrino_msg_riga3_grassetto = layout_PC[0].Field245;
                    comanda.scontrino_msg_riga3_allineamento = layout_PC[0].Field246;
                    comanda.scontrino_spazio3 = layout_PC[0].Field247;
                    comanda.scontrino_msg_riga4 = layout_PC[0].Field248;
                    comanda.scontrino_msg_riga4_grassetto = layout_PC[0].Field249;
                    comanda.scontrino_msg_riga4_allineamento = layout_PC[0].Field250;
                    comanda.url_qr_code = layout_PC[0].Field251;

                    comanda.ricevuta_pos_scontrino = layout_PC[0].Field235;
                    comanda.pos_abilitato = layout_PC[0].Field234;

                    comanda.assegno_abilitato = layout_PC[0].Field365;
                    comanda.bonifico_abilitato = layout_PC[0].Field366;
                    comanda.droppay_abilitato = layout_PC[0].Field367;

                    comanda.ip_pos = layout_PC[0].Field231;
                    comanda.porta_pos = layout_PC[0].Field232;
                    comanda.id_term = layout_PC[0].Field233;

                    comanda.inizio_fascia_gialla = "6";
                    comanda.inizio_fascia_rossa = "9";

                    if (!isNaN(parseInt(layout_PC[0].Field184))) {
                        comanda.inizio_fascia_gialla = layout_PC[0].Field184;
                    }
                    if (!isNaN(parseInt(layout_PC[0].Field183))) {
                        comanda.inizio_fascia_rossa = layout_PC[0].Field183;
                    }

                    if (comanda.pizzeria_asporto === true) {
                        comanda.spazio_metro = "4";
                        if (!isNaN(parseInt(layout_PC[0].Field254))) {
                            comanda.spazio_metro = layout_PC[0].Field254;
                        }

                        comanda.spazio_mezzo_metro = "2";
                        if (!isNaN(parseInt(layout_PC[0].Field255))) {
                            comanda.spazio_mezzo_metro = layout_PC[0].Field255;
                        }
                    }

                    comanda.tasto_pasta_kamut = layout_PC[0].Field213;
                    comanda.tasto_pasta_soja = layout_PC[0].Field214;
                    comanda.tasto_pasta_integrale = layout_PC[0].Field215;
                    comanda.tasto_pasta_madre = layout_PC[0].Field216;
                    comanda.tasto_pasta_5_cereali = layout_PC[0].Field217;
                    comanda.tasto_pasta_7_cereali = layout_PC[0].Field437;
                    comanda.tasto_pasta_senza_glutine = layout_PC[0].Field218;
                    comanda.tasto_pasta_farro = layout_PC[0].Field219;
                    comanda.tasto_pasta_carbone = layout_PC[0].Field438;
                    comanda.prezzo_pasta_kamut = layout_PC[0].Field220;
                    comanda.prezzo_pasta_soja = layout_PC[0].Field221;
                    comanda.prezzo_pasta_integrale = layout_PC[0].Field222;
                    comanda.prezzo_pasta_madre = layout_PC[0].Field223;
                    comanda.prezzo_pasta_5_cereali = layout_PC[0].Field224;
                    comanda.prezzo_pasta_7_cereali = layout_PC[0].Field439;
                    comanda.prezzo_pasta_senza_glutine = layout_PC[0].Field225;
                    comanda.prezzo_pasta_farro = layout_PC[0].Field226;
                    comanda.prezzo_pasta_carbone = layout_PC[0].Field440;

                    comanda.tasto_pasta_napoli = layout_PC[0].Field441;
                    comanda.prezzo_pasta_napoli = layout_PC[0].Field442;
                    comanda.tasto_pasta_doppia_pasta = layout_PC[0].Field443;
                    comanda.prezzo_pasta_doppia_pasta = layout_PC[0].Field444;
                    comanda.tasto_pasta_battuta = layout_PC[0].Field445;
                    comanda.prezzo_pasta_battuta = layout_PC[0].Field446;
                    comanda.tasto_pasta_battuta_farcita = layout_PC[0].Field447;
                    comanda.prezzo_pasta_battuta_farcita = layout_PC[0].Field448;
                    comanda.tasto_pasta_enkir = layout_PC[0].Field449;
                    comanda.prezzo_pasta_enkir = layout_PC[0].Field450;
                    comanda.tasto_pasta_canapa = layout_PC[0].Field451;
                    comanda.prezzo_pasta_canapa = layout_PC[0].Field452;
                    comanda.tasto_pasta_grano_arso = layout_PC[0].Field453;
                    comanda.prezzo_pasta_grano_arso = layout_PC[0].Field454;
                    comanda.tasto_pasta_manitoba = layout_PC[0].Field455;
                    comanda.prezzo_pasta_manitoba = layout_PC[0].Field456;
                    comanda.tasto_pasta_patate = layout_PC[0].Field457;
                    comanda.prezzo_pasta_patate = layout_PC[0].Field458;
                    comanda.tasto_pasta_pinsa_romana = layout_PC[0].Field459;
                    comanda.prezzo_pasta_pinsa_romana = layout_PC[0].Field460;
                    comanda.tasto_pasta_fagioli = layout_PC[0].Field461;
                    comanda.prezzo_pasta_fagioli = layout_PC[0].Field462;
                    comanda.tasto_pasta_buratto_tipo2 = layout_PC[0].Field463;
                    comanda.prezzo_pasta_buratto_tipo2 = layout_PC[0].Field464;


                    comanda.check_pasta_personalizzata_1 = layout_PC[0].Field474;
                    comanda.testo_pasta_personalizzata_1 = layout_PC[0].Field475;
                    comanda.prezzo_pasta_personalizzata_1 = layout_PC[0].Field476;
                    comanda.abbreviazione_pasta_personalizzata_1 = layout_PC[0].Field477;

                    comanda.check_pasta_personalizzata_2 = layout_PC[0].Field478;
                    comanda.testo_pasta_personalizzata_2 = layout_PC[0].Field479;
                    comanda.prezzo_pasta_personalizzata_2 = layout_PC[0].Field480;
                    comanda.abbreviazione_pasta_personalizzata_2 = layout_PC[0].Field481;

                    comanda.check_pasta_personalizzata_3 = layout_PC[0].Field482;
                    comanda.testo_pasta_personalizzata_3 = layout_PC[0].Field483;
                    comanda.prezzo_pasta_personalizzata_3 = layout_PC[0].Field484;
                    comanda.abbreviazione_pasta_personalizzata_3 = layout_PC[0].Field485;

                    comanda.check_pasta_personalizzata_4 = layout_PC[0].Field486;
                    comanda.testo_pasta_personalizzata_4 = layout_PC[0].Field487;
                    comanda.prezzo_pasta_personalizzata_4 = layout_PC[0].Field488;
                    comanda.abbreviazione_pasta_personalizzata_4 = layout_PC[0].Field489;

                    comanda.check_pasta_personalizzata_5 = layout_PC[0].Field490;
                    comanda.testo_pasta_personalizzata_5 = layout_PC[0].Field491;
                    comanda.prezzo_pasta_personalizzata_5 = layout_PC[0].Field492;
                    comanda.abbreviazione_pasta_personalizzata_5 = layout_PC[0].Field493;

                    comanda.check_pasta_personalizzata_6 = layout_PC[0].Field494;
                    comanda.testo_pasta_personalizzata_6 = layout_PC[0].Field495;
                    comanda.prezzo_pasta_personalizzata_6 = layout_PC[0].Field496;
                    comanda.abbreviazione_pasta_personalizzata_6 = layout_PC[0].Field497;

                    comanda.calendario_ordinazioni = layout_PC[0].Field227;
                    comanda.categoria_calendario = layout_PC[0].Field228;

                    comanda.iva_esposta = layout_PC[0].Field309;

                    var query_maxi = "select * from prezzi_impasti_maxi where id=" + comanda.folder_number + " limit 1;";
                    comanda.sincro.query(query_maxi, function(maxi) {

                        comanda.prezzo_pasta_kamut_maxi = maxi[0].kumat;
                        comanda.prezzo_pasta_soja_maxi = maxi[0].soja;
                        comanda.prezzo_pasta_integrale_maxi = maxi[0].integrale;
                        comanda.prezzo_pasta_madre_maxi = maxi[0].pasta_madre;
                        comanda.prezzo_pasta_5_cereali_maxi = maxi[0].ch_cerali;
                        comanda.prezzo_pasta_7_cereali_maxi = maxi[0].set_cerali;
                        comanda.prezzo_pasta_senza_glutine_maxi = maxi[0].senza_glutine;
                        comanda.prezzo_pasta_farro_maxi = maxi[0].farro;
                        comanda.prezzo_pasta_carbone_maxi = maxi[0].carbone;
                        comanda.prezzo_pasta_napoli_maxi = maxi[0].napoli;
                        comanda.prezzo_pasta_doppia_pasta_maxi = maxi[0].doppia_pasta;
                        comanda.prezzo_pasta_battuta_maxi = maxi[0].batutta;
                        comanda.prezzo_pasta_battuta_farcita_maxi = maxi[0].batutta_farcita;
                        comanda.prezzo_pasta_enkir_maxi = maxi[0].enkir;
                        comanda.prezzo_pasta_canapa_maxi = maxi[0].canapa;
                        comanda.prezzo_pasta_grano_arso_maxi = maxi[0].grano_arso;
                        comanda.prezzo_pasta_manitoba_maxi = maxi[0].manitoba;
                        comanda.prezzo_pasta_patate_maxi = maxi[0].patate;
                        comanda.prezzo_pasta_pinsa_romana_maxi = maxi[0].pinsa_romana;
                        comanda.prezzo_pasta_fagioli_maxi = maxi[0].fagioli;
                        comanda.prezzo_pasta_buratto_tipo2_maxi = maxi[0].buratto;
                        comanda.prezzo_pasta_personalizzata_1_maxi = maxi[0].testo_pasta_pers_1;
                        comanda.prezzo_pasta_personalizzata_2_maxi = maxi[0].testo_pasta_pers_2;
                        comanda.prezzo_pasta_personalizzata_3_maxi = maxi[0].testo_pasta_pers_3;
                        comanda.prezzo_pasta_personalizzata_4_maxi = maxi[0].testo_pasta_pers_4;
                        comanda.prezzo_pasta_personalizzata_5_maxi = maxi[0].testo_pasta_pers_5;
                        comanda.prezzo_pasta_personalizzata_6_maxi = maxi[0].testo_pasta_pers_6;
                    });

                    if (comanda.iva_esposta !== "true") {
                        $('#campo_iva_conto').hide();
                    }

                    comanda.auto_tavolo_continuo_con_id = false;
                    if (layout_PC[0].Field206 === 'true') {
                        comanda.auto_tavolo_continuo_con_id = true;
                    }

                    if (layout_PC[0].Field209 === 'true') {
                        comanda.tasto_pizza_metro = true;
                        comanda.prezzo_metro = layout_PC[0].Field210;
                        comanda.prezzo_mezzo_metro = layout_PC[0].Field211;
                        comanda.tasto_un_quarto = layout_PC[0].Field212;
                    } else {
                        comanda.tasto_pizza_metro = false;
                    }

                    if (layout_PC[0].Field328 === 'true') {
                        comanda.tasto_pizza_mezzo_metro = true;
                        comanda.prezzo_metro = layout_PC[0].Field210;
                        comanda.prezzo_mezzo_metro = layout_PC[0].Field211;
                        comanda.tasto_un_quarto = layout_PC[0].Field212;
                    } else {
                        comanda.tasto_pizza_mezzo_metro = false;
                    }

                    if (layout_PC[0].Field326 === 'true') {
                        comanda.tasto_pizza_duegusti = true;
                    }

                    /*if (layout_PC[0].Field501 === 'true') {
                        comanda.tasto_schiacciata = true;
                    }*/

                    if (layout_PC[0].Field311 === 'true') {
                        comanda.tasto_pizza_maxi = true;
                        if (!isNaN(parseInt(layout_PC[0].Field312)))
                            comanda.capienza_pizza_maxi = layout_PC[0].Field312;
                    }


                    importa_profilo_aziendale_ram(function() {


                        comanda.sincro.controllo_licenza((data_scadenza) => {

                            if (data_scadenza === "MOBILE") {

                            } else {

                                let query_licenza = "";

                                if (data_scadenza === "") {
                                    query_licenza = "update profilo_aziendale set tentativo_scaricamento_licenza=cast(tentativo_scaricamento_licenza as int)+1;";
                                    alasql(query_licenza);
                                    comanda.sock.send({ tipo: "controllo_licenza", operatore: comanda.operatore, query: query_licenza, terminale: comanda.terminale, ip: comanda.ip_address });
                                    if (comanda.query_su_socket === false) {
                                        comanda.array_dbcentrale_mancanti.push(query_licenza);
                                        if (comanda.compatibile_xml !== true) {
                                            log_query_db_centrale(query_licenza);
                                        }
                                    }
                                    alert("Errore nello scaricamento dei dati della licenza. Contatta l'assistenza al più presto o il software verrà bloccato.");
                                } else {
                                    query_licenza = "update profilo_aziendale set tentativo_scaricamento_licenza=0,data_scadenza_licenza='" + data_scadenza + "';";
                                    alasql(query_licenza);
                                    comanda.sock.send({ tipo: "controllo_licenza", operatore: comanda.operatore, query: query_licenza, terminale: comanda.terminale, ip: comanda.ip_address });
                                    if (comanda.query_su_socket === false) {
                                        comanda.array_dbcentrale_mancanti.push(query_licenza);
                                        if (comanda.compatibile_xml !== true) {
                                            log_query_db_centrale(query_licenza);
                                        }
                                    }
                                }

                                comanda.sincro.query_cassa();

                                let dati_licenza = alasql("select data_scadenza_licenza,tentativo_scaricamento_licenza from profilo_aziendale;");

                                if (dati_licenza.length > 0) {

                                    if (parseInt(dati_licenza[0].tentativo_scaricamento_licenza) > 7) {
                                        alert("Software bloccato per mancanza di aggiornamento dei dati della licenza. Contattare l'assistenza.");
                                        return false;
                                    }

                                    let oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
                                    let firstDate = new Date();
                                    let secondDate = new Date(dati_licenza[0].data_scadenza_licenza);

                                    let diffDays = Math.round((firstDate - secondDate) / oneDay);

                                    console.log("diffDays", diffDays);

                                    if (diffDays > 7) {
                                        alert("Hai superato di una settimana la data di scadenza della licenza, per tanto non puoi più utilizzare il software. Contatta l'assistenza per rinnovarla.");
                                        return false;
                                    } else if (diffDays > 0) {
                                        alert("La tua licenza è già scaduta. Rinnovala entro brevissimo tempo o il software smetterà di funzionare completamente. Contatta l'assistenza per risolvere il problema.");
                                    } else if (diffDays > -7) {
                                        alert("La tua licenza sta per scadere. Rinnovala al più presto o il software smetterà di funzionare. Contatta l'assistenza per risolvere il problema.");
                                    }


                                }

                            }

                            console.log("AVVIO PROGRAMMA");


                            alasql("CREATE TABLE oggetto_conto ( `prezzo_maxi` TEXT NOT NULL DEFAULT '',`tasto_segue` TEXT NOT NULL DEFAULT '',`ord` TEXT NOT NULL DEFAULT '', `cod_promo` TEXT NOT NULL DEFAULT '', `fiscalizzata_sn` TEXT NOT NULL DEFAULT '', `ora_` TEXT NOT NULL DEFAULT '', `rag_soc_cliente` TEXT NOT NULL DEFAULT '', `perc_iva` TEXT NOT NULL DEFAULT '',`reparto` TEXT NOT NULL DEFAULT '', `nome_comanda` TEXT NOT NULL DEFAULT '', `stampata_sn` TEXT NOT NULL DEFAULT '', `numero_conto` TEXT NOT NULL DEFAULT '', `dest_stampa` TEXT NOT NULL DEFAULT '', `portata` TEXT NOT NULL DEFAULT '', `categoria` TEXT NOT NULL DEFAULT '', `prog_inser` TEXT NOT NULL DEFAULT '', `nodo` TEXT NOT NULL DEFAULT '', `desc_art` TEXT NOT NULL DEFAULT '', `prezzo_un` TEXT NOT NULL DEFAULT '', `prezzo_varianti_aggiuntive` TEXT NOT NULL DEFAULT '', `prezzo_varianti_maxi` TEXT NOT NULL DEFAULT '', `prezzo_maxi_prima` TEXT NOT NULL DEFAULT '', `quantita` TEXT NOT NULL DEFAULT '', `contiene_variante` TEXT NOT NULL DEFAULT '', `tipo_impasto` TEXT NOT NULL DEFAULT '', `numero_paganti` TEXT NOT NULL DEFAULT '', `QTA` TEXT NOT NULL DEFAULT '')");
                            alasql("CREATE TABLE oggetto_conto_2 ( `prezzo_maxi` TEXT NOT NULL DEFAULT '',`tasto_segue` TEXT NOT NULL DEFAULT '',`ord` TEXT NOT NULL DEFAULT '', `cod_promo` TEXT NOT NULL DEFAULT '', `fiscalizzata_sn` TEXT NOT NULL DEFAULT '', `ora_` TEXT NOT NULL DEFAULT '', `rag_soc_cliente` TEXT NOT NULL DEFAULT '', `perc_iva` TEXT NOT NULL DEFAULT '',`reparto` TEXT NOT NULL DEFAULT '', `nome_comanda` TEXT NOT NULL DEFAULT '', `stampata_sn` TEXT NOT NULL DEFAULT '', `numero_conto` TEXT NOT NULL DEFAULT '', `dest_stampa` TEXT NOT NULL DEFAULT '', `portata` TEXT NOT NULL DEFAULT '', `categoria` TEXT NOT NULL DEFAULT '', `prog_inser` TEXT NOT NULL DEFAULT '', `nodo` TEXT NOT NULL DEFAULT '', `desc_art` TEXT NOT NULL DEFAULT '', `prezzo_un` TEXT NOT NULL DEFAULT '', `prezzo_varianti_aggiuntive` TEXT NOT NULL DEFAULT '', `prezzo_varianti_maxi` TEXT NOT NULL DEFAULT '', `prezzo_maxi_prima` TEXT NOT NULL DEFAULT '', `quantita` TEXT NOT NULL DEFAULT '', `contiene_variante` TEXT NOT NULL DEFAULT '', `tipo_impasto` TEXT NOT NULL DEFAULT '', `numero_paganti` TEXT NOT NULL DEFAULT '', `QTA` TEXT NOT NULL DEFAULT '')");
                            alasql("CREATE TABLE oggetto_comanda ( `prezzo_maxi` TEXT NOT NULL DEFAULT '',`tasto_segue` TEXT NOT NULL DEFAULT '',`ord` TEXT NOT NULL DEFAULT '', `cod_articolo` TEXT NOT NULL DEFAULT '', `perc_iva` TEXT NOT NULL DEFAULT '', `nome_comanda` TEXT NOT NULL DEFAULT '', `rag_soc_cliente` TEXT NOT NULL DEFAULT '', `perc_iva` TEXT NOT NULL DEFAULT '',`reparto` TEXT NOT NULL DEFAULT '', `stampata_sn` TEXT NOT NULL DEFAULT '', `numero_conto` TEXT NOT NULL DEFAULT '', `dest_stampa` TEXT NOT NULL DEFAULT '', `dest_stampa_2` TEXT NOT NULL DEFAULT '', `portata` TEXT NOT NULL DEFAULT '', `categoria` TEXT NOT NULL DEFAULT '', `prog_inser` TEXT NOT NULL DEFAULT '', `nodo` TEXT NOT NULL DEFAULT '', `desc_art` TEXT NOT NULL DEFAULT '', `prezzo_un` TEXT NOT NULL DEFAULT '', `quantita` TEXT NOT NULL DEFAULT '',  `contiene_variante` TEXT NOT NULL DEFAULT '')");




                            alasql.options.cache = false;

                            var qp = "CREATE TABLE IF NOT EXISTS progressivi_fiscali (ZBERICHT,DATA_ZBERICHT,QUITTUNG,DATA_QUITTUNG,RESCHNUNG,DATA_RESCHNUNG,\n\
                            SCONTRINO,DATA_SCONTRINO,FATTURA,DATA_FATTURA,RICEVUTA,DATA_RICEVUTA,\n\
                            BUONO,DATA_BUONO,ANNO_PROGRESSIVI);";
                            comanda.sincro.query(qp, function() {
                                console.log("AVVIO PROGRAMMA");
                                comanda.sincro.query("CREATE TABLE IF NOT EXISTS spooler_stampa (tentativo, orario, indirizzo, xml , tipo , dati_testa, id_univoco,indirizzo_alternativo) ; ", function() {
                                    //comanda.sincro.query("CREATE TABLE IF NOT EXISTS spooler_stampa_LOG (tentativo, orario, indirizzo, xml , tipo , dati_testa, id_univoco,indirizzo_alternativo) ; ", function () {
                                    comanda.sincro.query("SELECT visualizzazione_coperti,valore_coperto,coperti_obbligatorio,valore_servizio from impostazioni_fiscali  where id=" + comanda.folder_number + " ;", function(risultato) {
                                        console.log("AVVIO PROGRAMMA");
                                        var testo_query = 'SELECT ora_finale_servizio FROM dati_servizio where id="' + comanda.folder_number + '" limit 1;';
                                        comanda.sincro.query(testo_query, function(risultato) {
                                            comanda.ora_servizio = risultato[0].ora_finale_servizio;
                                            var testo_query = 'SELECT * FROM impostazioni_fiscali where id=' + comanda.folder_number + '  limit 1;';
                                            comanda.sincro.query(testo_query, function(risultato) {

                                                var testo_query = 'SELECT * FROM settaggi;';

                                                comanda.tastiera_gigante = false;


                                                comanda.multiprofilo = risultato[0].multiprofilo;
                                                comanda.cerca_prime_lettere = risultato[0].cerca_prime_lettere;
                                                comanda.filtro_ricerca_generico = risultato[0].filtro_ricerca_generico;


                                                comanda.avviso_righe_prezzo_zero = risultato[0].righe_zero;
                                                comanda.grandezza_font_articoli = '3.7';
                                                comanda.grandezza_font_tavoli = '7';
                                                comanda.grandezza_font_categorie = '3.5';

                                                if (comanda.mobile === true) {
                                                    comanda.grandezza_font_articoli = '1';
                                                    comanda.grandezza_font_tavoli = '1';
                                                    comanda.grandezza_font_categorie = '2';
                                                }

                                                if (risultato[0].grandezza_font_articoli !== 0 && risultato[0].grandezza_font_articoli !== null && risultato[0].grandezza_font_articoli !== 'NULL' && risultato[0].grandezza_font_articoli !== '' && risultato[0].grandezza_font_articoli !== '0' && risultato[0].grandezza_font_articoli !== 0) {
                                                    comanda.grandezza_font_articoli = risultato[0].grandezza_font_articoli;
                                                }

                                                if (risultato[0].grandezza_font_categorie !== 0 && risultato[0].grandezza_font_categorie !== null && risultato[0].grandezza_font_categorie !== 'NULL' && risultato[0].grandezza_font_categorie !== '' && risultato[0].grandezza_font_categorie !== '0' && risultato[0].grandezza_font_categorie !== 0) {
                                                    comanda.grandezza_font_categorie = risultato[0].grandezza_font_categorie;
                                                }

                                                if (risultato[0].grandezza_font_tavoli !== 0 && risultato[0].grandezza_font_tavoli !== null && risultato[0].grandezza_font_tavoli !== 'NULL' && risultato[0].grandezza_font_tavoli !== '' && risultato[0].grandezza_font_tavoli !== '0' && risultato[0].grandezza_font_tavoli !== 0) {
                                                    comanda.grandezza_font_tavoli = risultato[0].grandezza_font_tavoli;
                                                }
                                                console.log("AVVIO PROGRAMMA");
                                                comanda.lingua_stampa = risultato[0].lingua_stampa;
                                                comanda.funzionidb.nomi_categorie(function() {

                                                    comanda.font_articoli = "2vh";
                                                    comanda.altezza_articoli = "114px";
                                                    //156 è per la tastiera normale
                                                    //190 per la tastierona
                                                    comanda.larghezza_articoli = "156px";

                                                    if (comanda.mobile !== true) {



                                                        var cp_bar = "";
                                                        var cp_asporto = "";
                                                        var cp_continuo = "";
                                                        var cp_tavoli = "";

                                                        comanda.sincro.query_locale(testo_query, function(risultato_settaggi_locali) {

                                                            risultato_settaggi_locali.forEach(function(o, i) {

                                                                switch (o.nome) {

                                                                    case "cp_bar":
                                                                        cp_bar = o.valore;
                                                                        comanda.cp_bar = cp_bar;
                                                                        break;

                                                                    case "cp_asporto":
                                                                        cp_asporto = o.valore;
                                                                        comanda.cp_asporto = cp_asporto;
                                                                        break;

                                                                    case "cp_continuo":
                                                                        cp_continuo = o.valore;
                                                                        comanda.cp_continuo = cp_continuo;
                                                                        break;

                                                                    case "cp_tavoli":
                                                                        cp_tavoli = o.valore;
                                                                        comanda.cp_tavoli = cp_tavoli;
                                                                        break;

                                                                    case "tastiera_gigante":

                                                                        if (o.valore === "true") {
                                                                            comanda.tastiera_gigante = true;
                                                                            comanda.larghezza_articoli = "190px";
                                                                        }

                                                                        break;

                                                                    case "font_tastierino":

                                                                        if (o.valore !== null && o.valore !== "") {
                                                                            $("#tastiera_ricerca .anteprima_testo,#tastiera_ricerca .anteprima_testo input,#tastiera_lettere .anteprima_testo,#tastiera_lettere .anteprima_testo input,#tastiera_numeri .anteprima_testo,#tastiera_numeri .anteprima_testo input").css("font-size", o.valore + "px");

                                                                            $("#tastiera_ricerca .fila1>div, #tastiera_ricerca .fila2>div, #tastiera_ricerca .fila3>div, #tastiera_ricerca .fila4>div").css("font-size", o.valore + "px");
                                                                            $("#tastiera_lettere .fila1>div, #tastiera_lettere .fila2>div, #tastiera_lettere .fila3>div, #tastiera_lettere .fila4>div").css("font-size", o.valore + "px");
                                                                            $("#tastiera_numeri .fila1>div, #tastiera_numeri .fila2>div, #tastiera_numeri .fila3>div, #tastiera_numeri .fila4>div").css("font-size", o.valore + "px");
                                                                        }

                                                                        break;

                                                                    case "left_tastierino":

                                                                        if (o.valore !== null && o.valore !== "") {
                                                                            comanda.left_tastierino = o.valore;
                                                                            $("#tastiera_numeri").css("left", o.valore + "%");
                                                                        }

                                                                        break;

                                                                    case "left_tastiera":

                                                                        if (o.valore !== null && o.valore !== "") {
                                                                            $("#tastiera_ricerca").css("left", o.valore + "%");
                                                                            $("#tastiera_lettere").css("left", o.valore + "%");
                                                                        }

                                                                        break;

                                                                    case "font_articoli":

                                                                        if (o.valore !== null && o.valore !== "") {
                                                                            //VARIABILE
                                                                            comanda.font_articoli = o.valore;
                                                                        }

                                                                        break;

                                                                    case "altezza_articoli":

                                                                        if (o.valore !== null && o.valore !== "") {
                                                                            //VARIABILE
                                                                            comanda.altezza_articoli = o.valore;
                                                                        }

                                                                        break;

                                                                    case "larghezza_articoli":

                                                                        if (o.valore !== null && o.valore !== "") {
                                                                            //VARIABILE
                                                                            comanda.larghezza_articoli = o.valore;
                                                                        }

                                                                        break;

                                                                    case "zoom_tavoli":

                                                                        if (o.valore !== null && o.valore !== "") {
                                                                            //MODIFICA CSS
                                                                            var sheets = document.styleSheets;

                                                                            sheets[0].insertRule("#mappa_tavoli { zoom: " + o.valore + ";", 0);
                                                                        }

                                                                        break;

                                                                    case "tab_dx":

                                                                        if (o.valore !== null && o.valore !== "") {
                                                                            //MODIFICA CSS
                                                                            var sheets = document.styleSheets;

                                                                            sheets[0].insertRule("#tab_dx { height: " + o.valore + " !important;", 0);
                                                                        }

                                                                        break;

                                                                    case "altezza_pagina_pagamento":

                                                                        if (o.valore !== null && o.valore !== "") {
                                                                            //MODIFICA CSS
                                                                            var sheets = document.styleSheets;

                                                                            sheets[0].insertRule("#altezza_pagina_pagamento { height: " + o.valore + " !important;", 0);
                                                                        }

                                                                        break;

                                                                    case "margin_top_tasti_pagamento":

                                                                        if (o.valore !== null && o.valore !== "") {
                                                                            //MODIFICA CSS
                                                                            var sheets = document.styleSheets;

                                                                            sheets[0].insertRule(".tasti_pagamento { margin-top: " + o.valore + " !important;", 0);
                                                                        }

                                                                        break;

                                                                    case "altezza_calendario":

                                                                        if (o.valore !== null && o.valore !== "") {
                                                                            //VARIABILE
                                                                            $("#altezza_calendario").css("height", o.valore);
                                                                        }

                                                                        break;

                                                                    case "altezza_tasti_inc":

                                                                        if (o.valore !== null && o.valore !== "") {
                                                                            //VARIABILE
                                                                            $("#tasti_fiscali_da_nascondere").css("margin-top", o.valore);
                                                                        }

                                                                        break;

                                                                    case "altezza_barra_scooter":

                                                                        if (o.valore !== null && o.valore !== "") {
                                                                            //VARIABILE
                                                                            $("#altezza_barra_scooter").css("height", o.valore);
                                                                        }

                                                                        break;

                                                                    case "altezza_minima_prodotti":

                                                                        if (o.valore !== null && o.valore !== "") {
                                                                            //VARIABILE
                                                                            $("#prodotti").css("min-height", o.valore);
                                                                        }

                                                                        break;

                                                                }
                                                            });
                                                        });
                                                    }


                                                    if (comanda.mobile !== true && risultato[0].categoria_partenza === 'PREFERITI') {

                                                        var numero_layout = "_1";
                                                        if (leggi_get_url('id') === '2' || leggi_get_url('id') === '3' || leggi_get_url('id') === '4') {
                                                            numero_layout = "_" + leggi_get_url('id');
                                                        }

                                                        var a = '';
                                                        for (var key in comanda.nome_categoria) {

                                                            if (comanda.nome_categoria[key] === 'PREFERITI' + numero_layout) {
                                                                a = key;
                                                            }
                                                        }
                                                        if (a === '') {
                                                            /*bootbox.alert("La categoria dei preferiti per il tuo terminale non esiste. Controlla l'id sull'url.");*/
                                                        } else {

                                                            comanda.categoria_partenza = a;
                                                            //Categoria predefinita quando apro un tavolo, se la variabile sopra è vera (numero categoria)
                                                            comanda.categoria_predefinita = a;
                                                            //Categoria di partenza (Dopo cambierà)
                                                            comanda.categoria = a;
                                                            comanda.ultima_categoria_principale = a;
                                                        }

                                                        //comanda.cp_tavoli = comanda.categoria_partenza;

                                                    } else if (comanda.mobile === true) {
                                                        comanda.categoria_partenza = risultato[0].categoria_partenza_mobile;
                                                        //Categoria predefinita quando apro un tavolo, se la variabile sopra è vera (numero categoria)
                                                        comanda.categoria_predefinita = risultato[0].categoria_partenza_mobile;
                                                        //Categoria di partenza (Dopo cambierà)
                                                        comanda.categoria = risultato[0].categoria_partenza_mobile;
                                                        comanda.ultima_categoria_principale = comanda.categoria_partenza;
                                                        //comanda.cp_tavoli = comanda.categoria_partenza;
                                                    } else {
                                                        comanda.categoria_partenza = risultato[0].categoria_partenza;
                                                        //Categoria predefinita quando apro un tavolo, se la variabile sopra è vera (numero categoria)
                                                        comanda.categoria_predefinita = risultato[0].categoria_partenza;
                                                        //Categoria di partenza (Dopo cambierà)
                                                        comanda.categoria = risultato[0].categoria_partenza;
                                                        comanda.ultima_categoria_principale = comanda.categoria_partenza;
                                                        //comanda.cp_tavoli = comanda.categoria_partenza;
                                                    }

                                                    console.log("AVVIO PROGRAMMA");

                                                    lng(comanda.lingua, function() {
                                                        lngstm(comanda.lingua_stampa, function() {
                                                            console.log("AVVIO PROGRAMMA");
                                                            comanda.funzionidb.elenco_categorie("list", "1", "0", "#tab_sx");
                                                            console.log("AVVIO PROGRAMMA");
                                                            comanda.funzionidb.elenco_categorie("select", "1", "1", ".elenco_categorie_select");
                                                            console.log("AVVIO PROGRAMMA");
                                                            //LISTENER FUNZIONI ALFABETICHE CELLULARI PALMARE
                                                            if (comanda.mobile === true) {
                                                                ordina_alfabeto();
                                                                console.log("AVVIO PROGRAMMA");
                                                            }
                                                            dbgfunzioni();
                                                            console.log("AVVIO PROGRAMMA");
                                                        });




                                                        corrispondenze_stampanti(comanda.lingua_stampa);
                                                        console.log("AVVIO PROGRAMMA");
                                                        corrispondenze_portate(comanda.lingua_stampa);
                                                        console.log("AVVIO PROGRAMMA");


                                                        /* VADO A LEGGERE I REPARTI, IVA, ATECO, ECC.. */



                                                        ATECO_model = new ATECO_model();
                                                        ATECO_view = new ATECO_view();
                                                        ATECO_controller = new ATECO_controller();

                                                        IVA_model = new IVA_model();
                                                        IVA_view = new IVA_view();
                                                        IVA_controller = new IVA_controller();

                                                        REPARTO_model = new REPARTO_model();
                                                        REPARTO_view = new REPARTO_view();
                                                        REPARTO_controller = new REPARTO_controller();

                                                        /* alias */
                                                        ive_misuratore_controller = REPARTO_controller;

                                                        ATECO_controller.aggiorna(function() {
                                                            IVA_controller.aggiorna(function() {
                                                                REPARTO_controller.aggiorna(function() {


                                                                    //DISCONNETTE AD OGNI AVVIO
                                                                    /*if (comanda.pizzeria_asporto !== true) {*/
                                                                    disconnetti();
                                                                    /*}*/

                                                                    if (comanda.pizzeria_asporto === true) {

                                                                        if (comanda.numero_licenza_cliente === "0638") {

                                                                            $('.tasto_in_cottura').css("visibility", "hidden");
                                                                            $('.tasto_poco_cotto').css("visibility", "hidden");
                                                                            $('.tasto_fine_cottura').css("visibility", "hidden");
                                                                        }


                                                                        comanda.clona_diretto = "S";

                                                                        console.log("AVVIO PROGRAMMA");

                                                                        /*var altezza_tasti_incasso = $("#conto_grande .tasti_incasso").height();*/

                                                                        var zoom_body = parseFloat($("body").css("zoom"));

                                                                        $("#tab_dx_grande").css("height", "0");

                                                                        var altezza_body = screen.height;

                                                                        $("#conto_grande").modal("show");

                                                                        var altezza_popup_vuoto = $("#conto_grande .modal-content").height();

                                                                        $("#conto_grande").modal("hide");

                                                                        var altezza_lente_piu = parseInt(((altezza_body / zoom_body) - altezza_popup_vuoto) - 80);

                                                                        $("#tab_dx_grande").css("height", altezza_lente_piu + "px");

                                                                        Class_Servizio.servizio(function(callback_servizio) {

                                                                            dct = new dati_comanda_temporizzata(false, (result, id_comanda) => {

                                                                                funzione_interna_comanda_temporizzata_2(result, id_comanda);

                                                                            }, true);

                                                                            console.log("AVVIO PROGRAMMA");
                                                                            /*setInterval(function () {
                                                                             
                                                                             }, 5000);*/

                                                                            console.log("AVVIO PROGRAMMA");

                                                                            /*
                                                                             comanda.operatore = "Pizzaiolo";
                                                                             comanda.password = "0aa633196ad7095978c9c1a636412486";
                                                                             */

                                                                            /*pagina();*/

                                                                            console.log("AVVIO PROGRAMMA");
                                                                            tabella_sconti_cod_promo_2();

                                                                            $('body').removeClass('loading');



                                                                            if (checkEmptyVariable(testo_alert, "string") === true) {
                                                                                bootbox.alert(testo_alert);
                                                                            }

                                                                            var d = new Date();
                                                                            var Y = d.getFullYear();
                                                                            var M = addZero((d.getMonth() + 1), 2);
                                                                            var D = addZero(d.getDate(), 2);

                                                                            var testo_query = "select * from cambi_listino where giorno='" + D + "' and mese='" + M + "' and anno='" + Y + "';";

                                                                            let pda = alasql(testo_query);

                                                                            //deve updatare quelli con lo stesso id e mettere xxx come categoria, però prima va tenuta in memoria
                                                                            //poi quell'articolo lo copia in un altra riga      
                                                                            //poi lo aggiorna con i dati nuovi che ci sono in cambio_listino
                                                                            //da fare il 29/10/2020

                                                                            pda.forEach(function(v) {

                                                                                let query_select_prodotto = "select * from prodotti where id='" + v.id_prodotto + "' and categoria!='XXX';";

                                                                                let select_prodotto = alasql(query_select_prodotto);

                                                                                if (select_prodotto.length > 0) {

                                                                                    let prodotto_nuovo = new Object();

                                                                                    Object.assign(prodotto_nuovo, select_prodotto[0]);

                                                                                    for (let index in prodotto_nuovo) {

                                                                                        /*let value = prodotto_nuovo.index;*/

                                                                                        if (v[index] !== prodotto_nuovo[index] && v[index] !== null) {
                                                                                            prodotto_nuovo[index] = v[index];
                                                                                        } else if (prodotto_nuovo[index] === null || prodotto_nuovo[index] === "null" || prodotto_nuovo[index] === "undefined" || prodotto_nuovo[index] === "NaN") {
                                                                                            prodotto_nuovo[index] = "";
                                                                                        }

                                                                                    }


                                                                                    let query_update_prodotto = "update prodotti set categoria='XXX',ult_mod='" + new Date().getTime() + "' where id='" + v.id_prodotto + "' and categoria!='XXX';";

                                                                                    comanda.sock.send({ tipo: "update_prodotto", operatore: comanda.operatore, query: query_update_prodotto, terminale: comanda.terminale, ip: comanda.ip_address });

                                                                                    alasql(query_update_prodotto);

                                                                                    let query_insert = "INSERT INTO prodotti (" + Object.keys(prodotto_nuovo).join(",") + ") VALUES (" + Object.values(prodotto_nuovo).map(v => "'" + v + "'").join(",") + ");";

                                                                                    comanda.sock.send({ tipo: "update_prodotto", operatore: comanda.operatore, query: query_insert, terminale: comanda.terminale, ip: comanda.ip_address });

                                                                                    alasql(query_insert);

                                                                                    comanda.sincro.query_cassa();

                                                                                }
                                                                            });

                                                                            console.log("AVVIO PROGRAMMA");
                                                                            //ATTIVO LO SPOOLER DI STAMPA

                                                                            if (layout_PC[0].Field465 === "true") {
                                                                                /*default stampa 15 minuti prima*/
                                                                                let gap_minuti_comanda_temporizzata = 15;
                                                                                let gap_minuti_comanda_temporizzata_domicilio = 10;

                                                                                if (!isNaN(layout_PC[0].Field466)) {
                                                                                    gap_minuti_comanda_temporizzata = parseInt(layout_PC[0].Field466);
                                                                                }
                                                                                if (!isNaN(layout_PC[0].Field467)) {
                                                                                    gap_minuti_comanda_temporizzata_domicilio = parseInt(layout_PC[0].Field467);
                                                                                }
                                                                                secondi_stampa_comanda_temporizzata = 60 * gap_minuti_comanda_temporizzata;
                                                                                secondi_stampa_comanda_temporizzata_domicilio = 60 * gap_minuti_comanda_temporizzata_domicilio;

                                                                                setInterval(stampa_comanda_temporizzata, 30 * 1000);
                                                                            }



                                                                            /*var giro = 1;
                                                                             var ogni_x_giri = 30;*/
                                                                            setInterval(function() {

                                                                                orologio_asporto();

                                                                                start_spooler();

                                                                                //una volta al minuto
                                                                                /*if (giro % ogni_x_giri === 0) {
                                                                                 //nn centra
                                                                                 richiesta_status_terminali();
                                                                                 invia_verifica_tavoli_bloccati();
                                                                                 
                                                                                 }
                                                                                 
                                                                                 giro++;*/

                                                                                //Ogni 30 secondi verifica tramite socket
                                                                                //che i tavoli siano veramente occupati
                                                                                /*if (giro % 10 === 0) {
                                                                                 
                                                                                 invia_verifica_tavoli_bloccati();
                                                                                 
                                                                                 }
                                                                                 
                                                                                 giro++;*/
                                                                            }, 3000);

                                                                            if (comanda.numero_licenza_cliente !== "0629" && comanda.numero_licenza_cliente !== "0656") {
                                                                                listen_tastierino_num();
                                                                            }
                                                                            if (comanda.numero_licenza_cliente === "9000" || comanda.numero_licenza_cliente === "0629" || comanda.numero_licenza_cliente === "0601" || comanda.numero_licenza_cliente === "0666" || comanda.numero_licenza_cliente === "0676" || comanda.numero_licenza_cliente === "0280") {

                                                                                $('.carta_credito_aggiuntiva').remove();
                                                                                $('#tab_dx_grande').css("height", "330px");
                                                                                $("#popup_scelta_cliente .modal-body").css("min-height", "85vh");

                                                                                /*il metodo di pagamento ha l'altezza tasti nella funzione seleziona_metodo_pagamento
                                                                                 * direttamente in JavaScript in cassa.js*/
                                                                            }

                                                                        });
                                                                    } else {

                                                                        comanda.funzionidb.elenco_operatori(function() {



                                                                            controllo_password_db(function(r) {
                                                                                if (r === false) {
                                                                                    testaLogin(function(s) {
                                                                                        if (s === false) {
                                                                                            nascondi_righe();
                                                                                            selezione_operatore();
                                                                                        } else {
                                                                                            pagina();
                                                                                        }
                                                                                    });
                                                                                } else {
                                                                                    pagina();
                                                                                }
                                                                            });
                                                                            if (comanda.lingua_stampa !== 'deutsch') {
                                                                                $('.elimina_tedesco').remove();
                                                                                $('.tasto_comanda').removeClass('gra1');
                                                                                $('.tasto_comanda').addClass('gra3');
                                                                            }

                                                                            tabella_sconti_cod_promo_2();
                                                                            $('body').removeClass('loading');

                                                                            var d = new Date();
                                                                            var Y = d.getFullYear();
                                                                            var M = addZero((d.getMonth() + 1), 2);
                                                                            var D = addZero(d.getDate(), 2);

                                                                            var testo_query = "select * from cambi_listino where giorno='" + D + "' and mese='" + M + "' and anno='" + Y + "';";

                                                                            let pda = alasql(testo_query);

                                                                            //deve updatare quelli con lo stesso id e mettere xxx come categoria, però prima va tenuta in memoria
                                                                            //poi quell'articolo lo copia in un altra riga      
                                                                            //poi lo aggiorna con i dati nuovi che ci sono in cambio_listino
                                                                            //da fare il 29/10/2020

                                                                            pda.forEach(function(v) {

                                                                                let query_select_prodotto = "select * from prodotti where id='" + v.id_prodotto + "' and categoria!='XXX';";

                                                                                let select_prodotto = alasql(query_select_prodotto);

                                                                                if (select_prodotto.length > 0) {

                                                                                    let prodotto_nuovo = new Object();

                                                                                    Object.assign(prodotto_nuovo, select_prodotto[0]);

                                                                                    for (let index in prodotto_nuovo) {

                                                                                        /*let value = prodotto_nuovo.index;*/

                                                                                        if (v[index] !== prodotto_nuovo[index] && v[index] !== null) {
                                                                                            prodotto_nuovo[index] = v[index];
                                                                                        } else if (prodotto_nuovo[index] === null || prodotto_nuovo[index] === "null" || prodotto_nuovo[index] === "undefined" || prodotto_nuovo[index] === "NaN") {
                                                                                            prodotto_nuovo[index] = "";
                                                                                        }

                                                                                    }


                                                                                    let query_update_prodotto = "update prodotti set categoria='XXX',ult_mod='" + new Date().getTime() + "' where id='" + v.id_prodotto + "' and categoria!='XXX';";

                                                                                    comanda.sock.send({ tipo: "update_prodotto", operatore: comanda.operatore, query: query_update_prodotto, terminale: comanda.terminale, ip: comanda.ip_address });

                                                                                    alasql(query_update_prodotto);

                                                                                    let query_insert = "INSERT INTO prodotti (" + Object.keys(prodotto_nuovo).join(",") + ") VALUES (" + Object.values(prodotto_nuovo).map(v => "'" + v + "'").join(",") + ");";

                                                                                    comanda.sock.send({ tipo: "update_prodotto", operatore: comanda.operatore, query: query_insert, terminale: comanda.terminale, ip: comanda.ip_address });

                                                                                    alasql(query_insert);

                                                                                    comanda.sincro.query_cassa();
                                                                                }
                                                                            });

                                                                            if (comanda.mobile !== true) {
                                                                                setInterval(function() {
                                                                                    orologio_asporto();
                                                                                }, 5000);
                                                                                stampa_avvisi_programmati();
                                                                            }
                                                                            //ATTIVO LO SPOOLER DI STAMPA

                                                                            //AI();


                                                                            var tempo_spooler = 1000;
                                                                            var ogni_x_giri = 30;
                                                                            if (comanda.mobile === true) {
                                                                                tempo_spooler = 2000;
                                                                                ogni_x_giri = 15;
                                                                            }
                                                                            /*var giro = 1;*/

                                                                            var giro = 1;
                                                                            setInterval(function() {

                                                                                start_spooler();

                                                                                //una volta al minuto
                                                                                if (giro % ogni_x_giri === 0) {
                                                                                    //nn centra
                                                                                    richiesta_status_terminali();
                                                                                    if (comanda.verifica_periodica_tavoli_abilitata === true) {
                                                                                        invia_verifica_tavoli_bloccati();
                                                                                    }
                                                                                }

                                                                                giro++;

                                                                                //Ogni 30 secondi verifica tramite socket
                                                                                //che i tavoli siano veramente occupati
                                                                                /*if (giro % ogni_x_giri === 0) {
                                                                                 
                                                                                 invia_verifica_tavoli_bloccati();
                                                                                 
                                                                                 }
                                                                                 
                                                                                 giro++;*/
                                                                            }, tempo_spooler);

                                                                            if (comanda.numero_licenza_cliente !== "0629" && comanda.numero_licenza_cliente !== "0656") {
                                                                                listen_tastierino_num();
                                                                            }
                                                                            //FUNZIONE PER ESEGUIRE QUERY CASSA QUANDO L'UTENTE E' IDLE OGNI 10 SECONDI
                                                                            comanda.idleTime = 0;
                                                                            //Increment the idle time counter every 1 second.
                                                                            var idleInterval = setInterval(timerIncrement, 3000);
                                                                        });

                                                                        function timerIncrement() {
                                                                            $('#conteggio_db').val(comanda.array_dbcentrale_mancanti.length + " query in attesa");
                                                                            $('#conteggio_sock').val(comanda.socket_ciclizzati.length + " RTEngine in attesa");
                                                                        }
                                                                        //FINE IDLE
                                                                    }

                                                                    $('#versione_software').html('v: ' + comanda.versione + ' by Checchin Software S.r.l.');

                                                                });
                                                            });
                                                        });
                                                    });

                                                    console.log("AVVIO PROGRAMMA");

                                                    comanda.consegna_a_pizza = risultato[0].consegna_a_pizza;
                                                    comanda.consegna_mezzo_metro = risultato[0].consegna_mezzo_metro;
                                                    comanda.consegna_metro = risultato[0].consegna_metro;
                                                    comanda.consegna_maxi = risultato[0].consegna_maxi;
                                                    comanda.consegna_su_totale = risultato[0].consegna_su_totale;

                                                    comanda.da_fascia_1 = risultato[0].da_fascia_1;
                                                    comanda.a_fascia_1 = risultato[0].a_fascia_1;
                                                    comanda.da_fascia_2 = risultato[0].da_fascia_2;
                                                    comanda.a_fascia_2 = risultato[0].a_fascia_2;

                                                    comanda.una_pizza = risultato[0].una_pizza;
                                                    comanda.piu_di_una_pizza = risultato[0].piu_di_una_pizza

                                                    comanda.fissa_da_fascia_1 = risultato[0].fissa_da_fascia_1;
                                                    comanda.fissa_a_fascia_1 = risultato[0].fissa_a_fascia_1;
                                                    comanda.fissa_da_fascia_2 = risultato[0].fissa_da_fascia_2;
                                                    comanda.fissa_a_fascia_2 = risultato[0].fissa_a_fascia_2;
                                                    comanda.fissa_una_pizza = risultato[0].fissa_una_pizza;
                                                    comanda.fissa_piu_di_una_pizza = risultato[0].fissa_piu_di_una_pizza;


                                                    comanda.touch_articolo_conto = risultato[0].touch_articolo_conto;
                                                    comanda.visualizzazione_coperti = risultato[0].visualizzazione_coperti;
                                                    comanda.valore_coperto = risultato[0].valore_coperto;
                                                    comanda.articolo_a_peso = risultato[0].articolo_a_peso;
                                                    comanda.prezzo_x_um = risultato[0].prezzo_x_um;
                                                    comanda.coperti_obbligatorio = risultato[0].coperti_obbligatorio;

                                                    comanda.orario_preparazione = risultato[0].orario_preparazione;
                                                    if (comanda.orario_preparazione === "0") {
                                                        $(".orario_preparazione").hide();
                                                    }

                                                    comanda.tempo_preparazione_default = risultato[0].tempo_preparazione_default;
                                                    comanda.tempo_preparazione_default_domicilio = risultato[0].tempo_preparazione_default_domicilio;

                                                    comanda.abilita_fattorino1 = risultato[0].abilita_fattorino1;
                                                    if (comanda.abilita_fattorino1 === "0") {
                                                        $("select#tipologia_fattorino option[value=1]").hide();
                                                    }
                                                    comanda.abilita_fattorino2 = risultato[0].abilita_fattorino2;
                                                    if (comanda.abilita_fattorino2 === "0") {
                                                        $("select#tipologia_fattorino option[value=2]").hide();
                                                    }
                                                    comanda.abilita_fattorino3 = risultato[0].abilita_fattorino3;
                                                    if (comanda.abilita_fattorino3 === "0") {
                                                        $("select#tipologia_fattorino option[value=3]").hide();
                                                    }

                                                    if ((comanda.abilita_fattorino1 + comanda.abilita_fattorino2 + comanda.abilita_fattorino3) === "000") {
                                                        $("select#tipologia_fattorino").hide();
                                                    }

                                                    comanda.nome_fattorino1 = risultato[0].nome_fattorino1;
                                                    if (comanda.nome_fattorino1 !== undefined && comanda.nome_fattorino1 !== null && comanda.nome_fattorino1.trim().length > 0) {
                                                        $("select#tipologia_fattorino option[value=1]").text(comanda.nome_fattorino1)
                                                    }
                                                    comanda.nome_fattorino2 = risultato[0].nome_fattorino2;
                                                    if (comanda.nome_fattorino2 !== undefined && comanda.nome_fattorino2 !== null && comanda.nome_fattorino2.trim().length > 0) {
                                                        $("select#tipologia_fattorino option[value=2]").text(comanda.nome_fattorino2)
                                                    }
                                                    comanda.nome_fattorino3 = risultato[0].nome_fattorino3;
                                                    if (comanda.nome_fattorino3 !== undefined && comanda.nome_fattorino3 !== null && comanda.nome_fattorino3.trim().length > 0) {
                                                        $("select#tipologia_fattorino option[value=3]").text(comanda.nome_fattorino3)
                                                    }

                                                    comanda.percentuale_servizio = risultato[0].valore_servizio;

                                                    comanda.percentuale_iva_default = risultato[0].aliquota_default;
                                                    comanda.percentuale_iva_takeaway = risultato[0].aliquota_takeaway;
                                                    comanda.reparto_servizi_default = risultato[0].reparto_servizi_default;
                                                    comanda.reparto_beni_default = risultato[0].reparto_beni_default;

                                                    comanda.reparto_consegna = risultato[0].reparto_consegna;
                                                    comanda.aliquota_consegna = risultato[0].aliquota_consegna;


                                                    if (risultato[0].iva_estera_abilitata === "1") {
                                                        comanda.partita_iva_estera = true;
                                                    }
                                                });

                                            });

                                            console.log("AVVIO PROGRAMMA");
                                        });
                                    });
                                    //});
                                });
                            });
                        });
                    });
                });
                //});

            });
            /* - - - - - - - - - - - - - - - - - - - - - - - - - - - */

            /* - - - - -TEST ACCENSIONE INIZIALE - - - - - - - - - - */

            /*if (comanda.lingua_stampa !== "deutsch" && comanda.mobile !== true&) {
             var test_accensione_iniziale = new epson.fiscalPrint();
             test_accensione_iniziale.send(window.location.protocol + "//" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi", '<printerCommand><queryPrinterStatus operator="1" /></printerCommand>', 15000);
             test_accensione_iniziale.onerror = function (result) {
             if (result.code === "FP_NO_ANSWER" || result.code === "") {
             alert("MISURATORE SPENTO!"); //Misuratore fiscale spento! Si prega di accendere.
             } else if (result.code === "FP_NO_ANSWER_NETWORK")
             {
             alert("MISURATORE SCOLLEGATO!"); //Misuratore fiscale off-line. Controllare il cavo di rete.
             }
             };
             }*/

            console.log("AVVIO PROGRAMMA");
            calcola_totale();
            console.log("AVVIO PROGRAMMA");
            if (comanda.mobile === false) {
                adatta_zoom();
                console.log("AVVIO PROGRAMMA");
                //console.log('zoom adattato');
            }

            var val_iniziale;

        }
    }, function(err) {

        popup_inserimento_settaggi_android();

    });


}


//FUNZIONE RICHIESTA
var ct_numero_click_segnalazione_bug = 0;
var ct_timeout_click_segnalazione_bug;
$(document).on('touchstart', function() {

    if (comanda.dispositivo === "MOVIMENTI" || comanda.dispositivo === "lang_41") {
        if (ct_numero_click_segnalazione_bug < 20) {
            console.log("CSB", ct_numero_click_segnalazione_bug);
            ct_numero_click_segnalazione_bug++;
            //DOPO UN SECONDO IL NUMERO DI CLICK SI RESETTA
            clearTimeout(ct_timeout_click_segnalazione_bug);
            ct_timeout_click_segnalazione_bug = setTimeout(function() {
                ct_numero_click_segnalazione_bug = 0;
            }, 1000);
        } else {
            ct_numero_click_segnalazione_bug = 0;
            comanda.bandierina = "1";
            clearTimeout(ct_timeout_click_segnalazione_bug);
            selezione_operatore('MOVIMENTI');
            comanda.bandierina = "0";
        }
    }
});
String.prototype.taglia_parole = function(n_max_lettere, x_parole) {
    //Taglia le parole in base ad una lunghezza massima, anche se separate da spazi, e le riduce a massimo x_parole

    var parole = this.match(/\w+/gi);
    var parola_finale = "";
    var i = 0;
    parole.forEach(function(obj) {

        if (i === x_parole) {

            return;
        }

        if (obj.length > n_max_lettere) {
            obj = obj.substr(0, n_max_lettere) + ".";
        }

        parola_finale += obj + " ";
        i++;
    });
    return parola_finale;
};
var oggetto_cambio_colore = new Object();
var oggetto_cambio_colore_originale = new Object();
var timeout_cambio_colore = function(id_articolo, cat_id, pag, colore_tasto, timeout, ora, tipo_sconto, valore_sconto) {

    if (oggetto_cambio_colore[ora] === undefined) {
        oggetto_cambio_colore[ora] = new Object();
    }

    if (oggetto_cambio_colore[ora][cat_id] === undefined) {
        oggetto_cambio_colore[ora][cat_id] = new Object();
    }

    if (oggetto_cambio_colore[ora][cat_id][pag] === undefined) {
        oggetto_cambio_colore[ora][cat_id][pag] = new Array();
    }

    oggetto_cambio_colore[ora][cat_id][pag].push({ id_articolo: id_articolo, tipo_sconto: tipo_sconto, valore_sconto: valore_sconto });
};
var intervallo_cambio_colore = setInterval(function() {

    var ora_attuale = new Date().format('HH:MM');
    for (var cat in oggetto_cambio_colore[ora_attuale]) {

        for (var pag in oggetto_cambio_colore[ora_attuale][cat]) {

            var $html = $(esperimento_output[cat][pag]);
            oggetto_cambio_colore[ora_attuale][cat][pag].forEach(function(v) {
                if (comanda.mobile !== true) {
                    $html.find('span#' + v.id_articolo).css('background-image', 'url(\'./img/TEXTURE.png\')').css('background-size', 'contain').css('background-repeat', 'round');
                } else {
                    $html.find('span#' + v.id_articolo).parent("li").css('background-image', 'url(\'./img/TEXTURE.png\')').css('background-size', 'contain').css('background-repeat', 'round');
                    if (v.tipo_sconto === "E") {
                        $html.find('span#' + v.id_articolo + ">.prezzo_smartphone").html(v.valore_sconto);
                    }
                }
            });
            esperimento_output[cat][pag] = $html[0].innerHTML;
            console.log("CAMBIO STILE EFFETTUATO CAMBIO COLORE");
        }

    }

    if (oggetto_cambio_colore[ora_attuale] !== undefined) {
        delete oggetto_cambio_colore[ora_attuale];
        if (comanda.dispositivo === "TAVOLO SELEZIONATO") {
            elenco_prodotti();
        }
    }


}, 8000);
var timeout_colore_originale = function(id_articolo, cat_id, pag, colore_tasto, timeout, ora, tipo_sconto, prezzo_originale) {

    if (oggetto_cambio_colore_originale[ora] === undefined) {
        oggetto_cambio_colore_originale[ora] = new Object();
    }

    if (oggetto_cambio_colore_originale[ora][cat_id] === undefined) {
        oggetto_cambio_colore_originale[ora][cat_id] = new Object();
    }

    if (oggetto_cambio_colore_originale[ora][cat_id][pag] === undefined) {
        oggetto_cambio_colore_originale[ora][cat_id][pag] = new Array();
    }

    oggetto_cambio_colore_originale[ora][cat_id][pag].push({ id_articolo: id_articolo, tipo_sconto: tipo_sconto, prezzo_originale: prezzo_originale });
};
var intervallo_cambio_colore_originale = setInterval(function() {

    var ora_attuale = new Date().format('HH:MM');
    for (var cat in oggetto_cambio_colore_originale[ora_attuale]) {

        for (var pag in oggetto_cambio_colore_originale[ora_attuale][cat]) {

            var $html = $(esperimento_output[cat][pag]);
            oggetto_cambio_colore_originale[ora_attuale][cat][pag].forEach(function(v) {
                if (comanda.mobile !== true) {
                    $html.find('span#' + v.id_articolo).css('background-image', 'initial').css('background-size', 'initial').css('background-repeat', 'initial');
                } else {
                    $html.find('span#' + v.id_articolo).parent("li").css('background-color', 'linen').css('background-image', '-webkit-linear-gradient(top, linen, rgba(0, 0, 0, 0.35))').css('background-size', 'initial').css('background-repeat', 'initial');
                    if (v.tipo_sconto === "E") {
                        $html.find('span#' + v.id_articolo + '>.prezzo_smartphone').html("&euro; " + v.prezzo_originale);
                    }
                }
            });
            esperimento_output[cat][pag] = $html[0].innerHTML;
            console.log("CAMBIO STILE EFFETTUATO CAMBIO COLORE ORIGINALE");
        }

    }

    if (oggetto_cambio_colore_originale[ora_attuale] !== undefined) {
        delete oggetto_cambio_colore_originale[ora_attuale];
        if (comanda.dispositivo === "TAVOLO SELEZIONATO") {
            elenco_prodotti();
        }
    }


}, 9000);


function remove_object_recursive(obj) {

    for (prop in obj) {

        if (typeof obj[prop] === 'string') {

            obj[prop] = "";
            obj[prop] = null;
            delete obj[prop];

        } else if (typeof obj[prop] === 'object') {
            remove_object_recursive(obj[prop]);
        }

    }

}

if (window.location.pathname.replace(/^.*\/([^/]*)/, "$1") === 'LAYOUT_RISTORANTE.html') {

    //FA PARTE DELLA FUNZIONE SOTTO

    var inizio_elenco_prodotti = false;
    var prodotti_fuoriscope = new Array();
    var esperimento_output;
    var conteggio_pagine_output;



    comanda.funzionidb.elenco_prodotti = function(callBack) {

        /* modificato il 6 luglio 2020. aggiungo &&comanda.dispositivo !== "lang_33" altrimenti ogni posizionamento di prodotto svuotava anche le altre categorie */
        if (esperimento_output !== undefined && comanda.dispositivo !== "lang_33") {
            remove_object_recursive(esperimento_output);
        }

        if (comanda.dispositivo === "lang_33") {
            if (esperimento_output !== undefined && esperimento_output[comanda.categoria] !== undefined) {
                /* modificato il 6 luglio 2020. aggiunte le seguenti 2 righe per ottimizzare la memoria */
                esperimento_output[comanda.categoria] = "";
                esperimento_output[comanda.categoria] = null;
                /* fine modifica */
                delete esperimento_output[comanda.categoria];
            } else {
                conteggio_pagine_output = new Object();
                esperimento_output = new Object();
            }
        } else {
            conteggio_pagine_output = new Object();
            esperimento_output = new Object();
        }

        if (inizio_elenco_prodotti === false) {




            inizio_elenco_prodotti = true;
            console.log("INIZIO CALLBACK PRODOTTI");
            //QUERY SETTAGGIO COLORI TABLET
            var query_colori_tablet = "select Field435,Field194,Field205 from settaggi_profili where id=" + comanda.folder_number + " limit 1;";
            comanda.sincro.query(query_colori_tablet, function(colori_tablet) {

                if (colori_tablet[0].Field205 === 'true') {
                    $('#tasto_indietro_menu_jolly').attr(comanda.evento, 'selezione_dispositivo();comanda.dispositivo="COMANDA";');
                    $('#tasto_indietro_menu_jolly').html('MENU');
                }



                layout_tasti_pc();
                //Può essere "true" o "false"
                var colori_tablet_uguali_pc = colori_tablet[0].Field435;
                comanda.abilita_buono_acquisto = colori_tablet[0].Field194;
                //QUERY DI SELEZIONE CATEGORIE


                if (comanda.dispositivo === "lang_33") {
                    try {
                        var query_categorie = "select * from categorie where id='" + comanda.categoria + "' ;";
                    } catch (e) {
                        var query_categorie = "select * from categorie;";
                    }
                } else {
                    var query_categorie = "select * from categorie;";
                }

                var prodotti = new Array();
                comanda.sincro.query(query_categorie, function(categoria) {

                    if (categoria.length > 0) {

                        var count_cat_number = categoria.length;
                        var cat_n = 1;
                        categoria.forEach(function(cat, i1) {

                            //RIEMPIO I DATI DELLE CATEGORIE
                            if (prodotti[cat.id] === undefined) {
                                prodotti[cat.id] = new Object();
                            }

                            var numero_layout = "_1";
                            if (leggi_get_url('id') === '2' || leggi_get_url('id') === '3' || leggi_get_url('id') === '4') {
                                numero_layout = "_" + leggi_get_url('id');
                            }

                            //QUERY DI SELEZIONE PRODOTTI
                            //IN QUESTO CASO LA POSIZIONE HA SOLO UN NUMERO E NON DELLE COORDINATE

                            if (comanda.nome_categoria[cat.id] === 'PREFERITI') {
                                var query_prodotti = 'select id, qta_fissa, ordinamento, cat_varianti,colore_tasto,descrizione,prezzo_1,listino_bar,listino_asporto,listino_continuo, categoria, pagina, posizione' + numero_layout + ' as posizione, perc_iva_base, perc_iva_takeaway  from prodotti WHERE  categoria!="XXX"  and descrizione!=".NUOVO PRODOTTO" and descrizione!="" AND prezzo_1 !="" AND posizione' + numero_layout + '!="999" ORDER BY cast(posizione' + numero_layout + ' as int) ASC;';
                            } else if (comanda.nome_categoria[cat.id] === 'PREFERITI_1') {
                                var query_prodotti = 'select id, qta_fissa, ordinamento, cat_varianti,colore_tasto,descrizione,prezzo_1,listino_bar,listino_asporto,listino_continuo, categoria, pagina, posizione_1 as posizione, perc_iva_base, perc_iva_takeaway  from prodotti WHERE  categoria!="XXX"  and descrizione!=".NUOVO PRODOTTO" and descrizione!="" AND prezzo_1 !="" AND posizione_1!="999" ORDER BY cast(posizione_1 as int) ASC;';
                            } else if (comanda.nome_categoria[cat.id] === 'PREFERITI_2') {
                                var query_prodotti = 'select id, qta_fissa, ordinamento, cat_varianti,colore_tasto,descrizione,prezzo_1,listino_bar,listino_asporto,listino_continuo, categoria, pagina, posizione_2 as posizione, perc_iva_base, perc_iva_takeaway  from prodotti WHERE  categoria!="XXX"  and descrizione!=".NUOVO PRODOTTO" and descrizione!="" AND prezzo_1 !="" AND posizione_2!="999" ORDER BY cast(posizione_2 as int) ASC;';
                            } else if (comanda.nome_categoria[cat.id] === 'PREFERITI_3') {
                                var query_prodotti = 'select id, qta_fissa, ordinamento, cat_varianti,colore_tasto,descrizione,prezzo_1,listino_bar,listino_asporto,listino_continuo, categoria, pagina, posizione_3 as posizione, perc_iva_base, perc_iva_takeaway  from prodotti WHERE  categoria!="XXX" and descrizione!=".NUOVO PRODOTTO"  and descrizione!="" AND prezzo_1 !="" AND posizione_3!="999" ORDER BY cast(posizione_3 as int) ASC;';
                            } else if (comanda.nome_categoria[cat.id] === 'PREFERITI_4') {
                                var query_prodotti = 'select id, qta_fissa, ordinamento, cat_varianti,colore_tasto,descrizione,prezzo_1,listino_bar,listino_asporto,listino_continuo, categoria, pagina, posizione_4 as posizione, perc_iva_base, perc_iva_takeaway  from prodotti WHERE  categoria!="XXX"  and descrizione!=".NUOVO PRODOTTO" and descrizione!="" AND prezzo_1 !="" AND posizione_4!="999" ORDER BY cast(posizione_4 as int) ASC;';
                            } else {
                                //va ordinato se è nella stessa posizione 999 anche per ordine alfabetico quindi descrizione, altrimenti viene una merda
                                var query_prodotti = 'select id, qta_fissa,ordinamento, cat_varianti,colore_tasto,descrizione,prezzo_1,listino_bar,listino_asporto,listino_continuo, categoria, pagina, posizione, perc_iva_base, perc_iva_takeaway  from prodotti WHERE  categoria!="XXX"  and descrizione!=".NUOVO PRODOTTO" and descrizione!="" AND prezzo_1 !="" AND categoria="' + cat.id + '" ORDER BY cast(posizione as int) ASC,descrizione ASC;';
                            }

                            comanda.sincro.query(query_prodotti, function(prodotto) {

                                var minwidth = comanda.larghezza_articoli;
                                var height = comanda.altezza_articoli;
                                var max_prodotti = 56;
                                if (comanda.tastiera_gigante === true) {
                                    max_prodotti = 42;
                                }
                                var fontsize = comanda.font_articoli;

                                var i = 0;
                                var pagina = 1;
                                var ultima_posizione = 1;

                                try {
                                    if (cat.descrizione.substr(0, 2) === "V.") {
                                        ultima_posizione = 2;
                                    }
                                } catch (e) {

                                }
                                //PER OGNI PRODOTTO VADO A CREARE UNA SERIE DI ARRAY CON VARI DATI

                                if (prodotti[cat.id][pagina] === undefined) {
                                    prodotti[cat.id][pagina] = new Object();



                                }

                                prodotto.forEach(function(prod, i2) {


                                    if (prod.posizione !== '999') {
                                        if (prodotti[cat.id][pagina][prod.posizione] === undefined) {
                                            prodotti[cat.id][pagina][prod.posizione] = new Object();
                                        }

                                        prodotti[cat.id][pagina][prod.posizione].id = prod.id;
                                        prodotti[cat.id][pagina][prod.posizione].cat_varianti = prod.cat_varianti;
                                        prodotti[cat.id][pagina][prod.posizione].qta_fissa = prod.qta_fissa;
                                        prodotti[cat.id][pagina][prod.posizione].colore_tasto = prod.colore_tasto;
                                        prodotti[cat.id][pagina][prod.posizione].descrizione = prod.descrizione /*.taglia_parole(10, 5)*/ ;
                                        prodotti[cat.id][pagina][prod.posizione].prezzo_1 = prod.prezzo_1;
                                        if (prod.listino_bar === null || prod.listino_bar === "" || (!isNaN(prod.listino_bar) && parseFloat(prod.listino_bar) === 0)) {
                                            prodotti[cat.id][pagina][prod.posizione].listino_bar = prod.prezzo_1;
                                        } else {
                                            prodotti[cat.id][pagina][prod.posizione].listino_bar = prod.listino_bar;
                                        }

                                        if (prod.listino_asporto === null || prod.listino_asporto === "" || (!isNaN(prod.listino_asporto) && parseFloat(prod.listino_asporto) === 0)) {
                                            prodotti[cat.id][pagina][prod.posizione].listino_asporto = prod.prezzo_1;
                                        } else {
                                            prodotti[cat.id][pagina][prod.posizione].listino_asporto = prod.listino_asporto;
                                        }

                                        if (prod.listino_continuo === null || prod.listino_continuo === "" || (!isNaN(prod.listino_continuo) && parseFloat(prod.listino_continuo) === 0)) {
                                            prodotti[cat.id][pagina][prod.posizione].listino_continuo = prod.prezzo_1;
                                        } else {
                                            prodotti[cat.id][pagina][prod.posizione].listino_continuo = prod.listino_continuo;
                                        }
                                        prodotti[cat.id][pagina][prod.posizione].categoria = prod.categoria;
                                        prodotti[cat.id][pagina][prod.posizione].pagina = prod.pagina;
                                        prodotti[cat.id][pagina][prod.posizione].posizione = prod.posizione;
                                        prodotti[cat.id][pagina][prod.posizione].perc_iva_base = prod.perc_iva_base;
                                        prodotti[cat.id][pagina][prod.posizione].perc_iva_takeaway = prod.perc_iva_takeaway;
                                        ultima_posizione = parseInt(prod.posizione) + 1;
                                    } else if (comanda.dispositivo !== "lang_33") {
                                        if (prodotti[cat.id][pagina][ultima_posizione.toString()] === undefined) {
                                            prodotti[cat.id][pagina][ultima_posizione.toString()] = new Object();
                                        }

                                        prodotti[cat.id][pagina][ultima_posizione.toString()].id = prod.id;
                                        prodotti[cat.id][pagina][ultima_posizione.toString()].qta_fissa = prod.qta_fissa;
                                        prodotti[cat.id][pagina][ultima_posizione.toString()].cat_varianti = prod.cat_varianti;
                                        prodotti[cat.id][pagina][ultima_posizione.toString()].colore_tasto = prod.colore_tasto;
                                        prodotti[cat.id][pagina][ultima_posizione.toString()].descrizione = prod.descrizione /*.taglia_parole(10, 5)*/ ;
                                        prodotti[cat.id][pagina][ultima_posizione.toString()].prezzo_1 = prod.prezzo_1;

                                        if (prod.listino_bar === null || prod.listino_bar === "" || (!isNaN(prod.listino_bar) && parseFloat(prod.listino_bar) === 0)) {
                                            prodotti[cat.id][pagina][ultima_posizione.toString()].listino_bar = prod.prezzo_1;
                                        } else {
                                            prodotti[cat.id][pagina][ultima_posizione.toString()].listino_bar = prod.listino_bar;
                                        }

                                        if (prod.listino_asporto === null || prod.listino_asporto === "" || (!isNaN(prod.listino_asporto) && parseFloat(prod.listino_asporto) === 0)) {
                                            prodotti[cat.id][pagina][ultima_posizione.toString()].listino_asporto = prod.prezzo_1;
                                        } else {
                                            prodotti[cat.id][pagina][ultima_posizione.toString()].listino_asporto = prod.listino_asporto;
                                        }

                                        if (prod.listino_continuo === null || prod.listino_continuo === "" || (!isNaN(prod.listino_continuo) && parseFloat(prod.listino_continuo) === 0)) {
                                            prodotti[cat.id][pagina][ultima_posizione.toString()].listino_continuo = prod.prezzo_1;
                                        } else {
                                            prodotti[cat.id][pagina][ultima_posizione.toString()].listino_continuo = prod.listino_continuo;
                                        }

                                        prodotti[cat.id][pagina][ultima_posizione.toString()].categoria = prod.categoria;
                                        prodotti[cat.id][pagina][ultima_posizione.toString()].pagina = prod.pagina;
                                        prodotti[cat.id][pagina][ultima_posizione.toString()].posizione = prod.posizione;
                                        prodotti[cat.id][pagina][ultima_posizione.toString()].perc_iva_base = prod.perc_iva_base;
                                        prodotti[cat.id][pagina][ultima_posizione.toString()].perc_iva_takeaway = prod.perc_iva_takeaway;
                                        ultima_posizione++;
                                    }

                                    i++;
                                });
                                var i = 1;

                                //PRENDO LE PAGINE NEI PRODOTTI
                                for (var p in prodotti[cat.id]) {


                                    if (esperimento_output[cat.id] === undefined) {
                                        esperimento_output[cat.id] = new Object();
                                    }

                                    if (esperimento_output[cat.id][p] === undefined) {
                                        esperimento_output[cat.id][p] = '';
                                    }


                                    var pag = 1;
                                    for (; pag <= 4; pag++) {
                                        esperimento_output[cat.id][pag] = "";
                                        esperimento_output[cat.id][pag] += "<div class='pag_" + pag + " cat_" + cat.id + "' >";
                                        esperimento_output[cat.id][pag] += "<div class='bs-docs-section btn_COMANDA'>";
                                        esperimento_output[cat.id][pag] += "<div class='bs-glyphicons'>";
                                        esperimento_output[cat.id][pag] += "<ul class='bs-glyphicons-list elenco_prodotti_draggable'>";
                                        //PRENDO I PRODOTTI

                                        if (cat.descrizione.substr(0, 2) === 'V.' && i === 1) {
                                            esperimento_output[cat.id][p] += '<li style="text-shadow: 1px 1px 1px silver;font-size: ' + fontsize + '; min-width: ' + minwidth + ';height: ' + height + ';background-color:lightblue;background-image:-webkit-linear-gradient(top, lightblue,#61aec7);" value="VARIANTELIBERA" class="btn_variante_libera btn_comanda prodotto_esistente"  ' + comanda.evento + '="popup_variante_libera();"><span style="" class="glyphicon-class"><div><strong>+</strong></span></li>';
                                            conteggio_pagine_output[cat.id] = p;
                                            i++;
                                        }

                                        for (; i <= (max_prodotti * pag); i++) {

                                            if (prodotti[cat.id][p][i] !== undefined) {


                                                var articolo = prodotti[cat.id][p][i];
                                                var colore = '';
                                                console.log("COLORE TASTI" + articolo.colore_tasto);
                                                console.log("COLORE TASTI", cat.descrizione.substr(0, 2) === 'V.', comanda.mobile === false && comanda.pizzeria_asporto === false && articolo.colore_tasto !== undefined && articolo.colore_tasto !== null && articolo.colore_tasto.length > 0, comanda.mobile === true && articolo.colore_tasto !== undefined && articolo.colore_tasto !== null && articolo.colore_tasto.length > 0, comanda.pizzeria_asporto === true);
                                                //linen
                                                var colore_tasto = "linen";
                                                if (((colori_tablet_uguali_pc === "true" && comanda.mobile === true) || comanda.mobile === false) && articolo.colore_tasto !== undefined && typeof(articolo.colore_tasto) === 'string' && articolo.colore_tasto.length > 0) {
                                                    colore_tasto = articolo.colore_tasto;
                                                }


                                                //PER IDENTIFICARLO E CAMBIARLO CON UN TIMER ANCHE MENTRE LAVORI
                                                var id_articolo = new Date().getTime();
                                                var stelline = "";
                                                //
                                                //Quando c'e un orario di inizio o fine invece che qta fissa
                                                if (RAM.tabella_sconti_cod_promo_2[articolo.id] !== undefined) {
                                                    stelline = "";
                                                    for (var key in RAM.tabella_sconti_cod_promo_2[articolo.id]) {



                                                        stelline = "";
                                                        var oggetto_scontato = RAM.tabella_sconti_cod_promo_2[articolo.id][key];
                                                        var ora_attuale = new Date();
                                                        if (oggetto_scontato.giorni_settimana === '' || oggetto_scontato.giorni_settimana.indexOf(ora_attuale.getDay().toString()) !== -1) {

                                                            var differenza_inizio = 18400000;
                                                            var differenza_fine = 18400000;

                                                            if (oggetto_scontato.a_ora.length === 5) {
                                                                var ora_fine = new Date();
                                                                var a_ora = oggetto_scontato.a_ora.substr(0, 2);
                                                                ora_fine.setHours(parseInt(a_ora));
                                                                var a_minuti = oggetto_scontato.a_ora.substr(3, 2);
                                                                ora_fine.setMinutes(parseInt(a_minuti));
                                                                ora_fine.setSeconds(00);
                                                                ora_fine.setMilliseconds(000);
                                                                differenza_fine = ora_fine.getTime() - ora_attuale.getTime();
                                                                if (differenza_fine > 0) {
                                                                    var prezzo_originale = "";
                                                                    if (oggetto_scontato.tipo_sconto === "E") {
                                                                        var r = alasql("select prezzo_1 from prodotti where id='" + oggetto_scontato.id_articolo + "' and categoria!='XXX' limit 1;");
                                                                        prezzo_originale = r[0].prezzo_1;
                                                                    }
                                                                    timeout_colore_originale(id_articolo, cat.id, pag, colore_tasto, differenza_fine, oggetto_scontato.a_ora, oggetto_scontato.tipo_sconto, prezzo_originale);
                                                                }
                                                            }

                                                            if (oggetto_scontato.da_ora.length === 5) {
                                                                var ora_inizio = new Date();
                                                                var da_ora = oggetto_scontato.da_ora.substr(0, 2);
                                                                ora_inizio.setHours(parseInt(da_ora));
                                                                var da_minuti = oggetto_scontato.da_ora.substr(3, 2);
                                                                ora_inizio.setMinutes(parseInt(da_minuti));
                                                                ora_inizio.setSeconds(00);
                                                                ora_inizio.setMilliseconds(000);
                                                                differenza_inizio = ora_inizio.getTime() - ora_attuale.getTime();
                                                                if (differenza_inizio > 0) {
                                                                    timeout_cambio_colore(id_articolo, cat.id, pag, colore_tasto, differenza_inizio, oggetto_scontato.da_ora, oggetto_scontato.tipo_sconto, oggetto_scontato.valore_sconto);
                                                                }
                                                            }
                                                            var testo_query = "SELECT Field510 from settaggi_profili";
                                                            var querry_settagio = alasql(testo_query);
                                                            if( querry_settagio[0].Field510!=="1"){
                                                            if ((differenza_inizio < 0 && differenza_fine > 0) || (oggetto_scontato.da_ora.length !== 5 && oggetto_scontato.a_ora.length !== 5)) {
                                                                stelline = "background-image:url(\'./img/TEXTURE.png\');background-size:contain;background-repeat:no-repeat;";
                                                            }}

                                                        }

                                                    }

                                                }



                                                if (cat.descrizione.substr(0, 2) === 'V.') {
                                                    if (colore_tasto === undefined || colore_tasto === null || colore_tasto === 'null' || colore_tasto === '' || colore_tasto === 'linen') {
                                                        colore = ' style="text-shadow: 1px 1px 1px silver;font-size: ' + fontsize + '; min-width: ' + minwidth + ';height: ' + height + ';background-color:lightblue;background-image:-webkit-linear-gradient(top, lightblue,#61aec7);" ';
                                                    } else {
                                                        colore = ' style="text-shadow: 1px 1px 1px silver;font-size:  ' + fontsize + '; min-width: ' + minwidth + ';height: ' + height + ';background-color:' + colore_tasto + ';background-image: -webkit-linear-gradient(top, ' + colore_tasto + ', rgba(0, 0, 0, 0.35));" ';
                                                    }
                                                } else {
                                                    if (comanda.mobile === true) {
                                                        colore = ' style="text-shadow: 1px 1px 1px silver;font-size:  ' + fontsize + '; min-width: ' + minwidth + ';height: ' + height + ';background-color:' + colore_tasto + ';background-image: -webkit-linear-gradient(top, ' + colore_tasto + ', rgba(0, 0, 0, 0.35));' + stelline + '" ';
                                                    } else {
                                                        colore = ' style="text-shadow: 1px 1px 1px silver;font-size:  ' + fontsize + '; min-width: ' + minwidth + ';height: ' + height + ';background-color:' + colore_tasto + ';background-image: -webkit-linear-gradient(top, ' + colore_tasto + ', rgba(0, 0, 0, 0.35));" ';
                                                    }

                                                }

                                                //DIFFERENZA TRA ARTICOLO CON VARIANTE AUTOMATICA E SENZA

                                                console.log("ARTICOLO VARIANTE AUTO", articolo, articolo.cat_varianti !== undefined && articolo.cat_varianti !== "undefined" && articolo.cat_varianti !== "undefined/undefined" && articolo.cat_varianti !== null && articolo.cat_varianti.indexOf("/") !== -1 && articolo.cat_varianti.split('/')[1] !== "ND");
                                                var str_qta_fissa = '';
                                                var str_quantita = "$(\'#quantita_articolo\').val()";
                                                if (articolo.qta_fissa !== null && articolo.qta_fissa !== '') {
                                                    str_quantita = articolo.qta_fissa + "*RAM.quantita_battuta";
                                                    str_qta_fissa = 'N° ' + articolo.qta_fissa + '<br/>';
                                                }


                                                if (articolo.descrizione !== undefined && articolo.descrizione !== null && typeof(articolo.descrizione) === "string") {
                                                    var risultato = new Array();

                                                    var counter = 0;
                                                    var stringa = articolo.descrizione;
                                                    var lettere = 9;
                                                    while (counter < 2 && stringa.length > 0) {
                                                        /*if (counter > 0) {
                                                         builder.addText('\n');
                                                         }*/
                                                        risultato.push(stringa.substring(0, lettere));
                                                        stringa = stringa.substring(lettere);
                                                        counter++;
                                                    }
                                                    /*articolo.descrizione.match(/\w+/gi).forEach(function (v) {
                                                     risultato.push(v.substr(0, 9));
                                                     });*/
                                                    articolo.descrizione = risultato.join("<br>");
                                                }


                                                if (articolo.cat_varianti !== undefined && articolo.cat_varianti !== "undefined" && articolo.cat_varianti !== "undefined/undefined" && articolo.cat_varianti !== null && articolo.cat_varianti.indexOf("/") !== -1 && articolo.cat_varianti.split('/')[1] !== "ND") {
                                                    esperimento_output[cat.id][pag] += '<li ' + colore + ' value="' + articolo.id + '" class="btn_comanda prodotto_esistente"  ' + comanda.evento + '="if (sblocco_tasti === true) {sblocco_tasti = false;comanda.variante_automatica=\'' + articolo.cat_varianti + '\';aggiungi_articolo(\'' + articolo.id + '\',\'' + articolo.descrizione.replace('\'', "\\'") + '\',\'&euro;' + articolo.prezzo_1 + '\',null,' + str_quantita + ',null,\'' + articolo.cat_varianti.match(/\w+/g)[0] + '\',null,null,\'' + articolo.perc_iva_base + '\',\'' + articolo.perc_iva_takeaway + '\');}" >';
                                                    conteggio_pagine_output[cat.id] = pag;
                                                } else {
                                                    //SE LA VARIANTE E' NORMALE
                                                    esperimento_output[cat.id][pag] += '<li  ' + colore + ' value="' + articolo.id + '" class="btn_comanda prodotto_esistente"  ' + comanda.evento + '="if (sblocco_tasti === true) {sblocco_tasti = false;aggiungi_articolo(\'' + articolo.id + '\',\'' + articolo.descrizione.replace('\'', "\\'") + '\',\'&euro;' + articolo.prezzo_1 + '\',null,' + str_quantita + ',null,\'' + articolo.cat_varianti + '\',null,null,\'' + articolo.perc_iva_base + '\',\'' + articolo.perc_iva_takeaway + '\');}">';
                                                    conteggio_pagine_output[cat.id] = pag;
                                                }

                                                if (comanda.mobile !== true) {
                                                    esperimento_output[cat.id][pag] += '<span style="' + stelline + '" id="' + id_articolo + '" class="glyphicon-class">';
                                                } else {
                                                    esperimento_output[cat.id][pag] += '<span id="' + id_articolo + '" class="glyphicon-class">';
                                                }
                                                esperimento_output[cat.id][pag] += '<div>' + str_qta_fissa + '<strong>' + articolo.descrizione + '</strong><br/><span class="prezzo_tavolo">€ ' + articolo.prezzo_1 + '</span><span class="prezzo_bar">€ ' + articolo.listino_bar + '</span><span class="prezzo_asporto">€ ' + articolo.listino_asporto + '</span><span class="prezzo_continuo">€ ' + articolo.listino_continuo + '</span></div></span>';
                                                esperimento_output[cat.id][pag] += '</li>';
                                                console.log("ELENCO PRODOTTI DATI: ", i, pagina);
                                                prodotti_fuoriscope = prodotti;
                                            } else {
                                                esperimento_output[cat.id][pag] += '<li style="font-size: ' + fontsize + '; min-width: ' + minwidth + ';height: ' + height + ';" class="btn_comanda" >';
                                                esperimento_output[cat.id][pag] += '<span class="glyphicon-class"><br/></span>';
                                                esperimento_output[cat.id][pag] += '</li>';
                                            }
                                        }



                                        esperimento_output[cat.id][pag] += "</ul>";
                                        esperimento_output[cat.id][pag] += "</div>";
                                        esperimento_output[cat.id][pag] += "</div>";
                                        esperimento_output[cat.id][pag] += "</div>";
                                    }
                                }

                                console.log(cat_n, count_cat_number);
                                if (cat_n === count_cat_number) {
                                    console.log("OGGETTO_PRODOTTI", prodotti);
                                    inizio_elenco_prodotti = false;
                                    callBack(true);
                                }

                                cat_n++;
                            });
                        });
                    } else {
                        callBack(true);
                    }
                });
            });

        }
    };
} else if (window.location.pathname.replace(/^.*\/([^/]*)/, "$1") === 'LAYOUT_ASPORTO.html') {

    //FA PARTE DELLA FUNZIONE SOTTO
    var inizio_elenco_prodotti = false;
    var prodotti_fuoriscope = new Array();
    var esperimento_output;
    var conteggio_pagine_output;
    comanda.funzionidb.elenco_prodotti = function(callBack) {

        if (comanda.dispositivo === "lang_33") {
            if (esperimento_output !== undefined && esperimento_output[comanda.categoria] !== undefined) {
                delete esperimento_output[comanda.categoria];
            } else {
                conteggio_pagine_output = new Object();
                esperimento_output = new Object();
            }
        } else {
            conteggio_pagine_output = new Object();
            esperimento_output = new Object();
        }

        if (inizio_elenco_prodotti === false) {
            inizio_elenco_prodotti = true;
            console.log("INIZIO CALLBACK PRODOTTI");
            //QUERY SETTAGGIO COLORI TABLET
            var query_colori_tablet = "select Field435,Field194,Field205 from settaggi_profili  where id=" + comanda.folder_number + " limit 1;";
            comanda.sincro.query(query_colori_tablet, function(colori_tablet) {

                if (colori_tablet[0].Field205 === 'true') {
                    $('#tasto_indietro_menu_jolly').attr(comanda.evento, 'selezione_dispositivo();comanda.dispositivo="COMANDA";');
                    $('#tasto_indietro_menu_jolly').html('MENU');
                }

                layout_tasti_pc_pizzeria();
                //Può essere "true" o "false"
                var colori_tablet_uguali_pc = colori_tablet[0].Field435;
                comanda.abilita_buono_acquisto = colori_tablet[0].Field194;
                //QUERY DI SELEZIONE CATEGORIE
                if (comanda.dispositivo === "lang_33") {
                    var query_categorie = "select * from categorie where id='" + comanda.categoria + "' ;";
                } else {
                    var query_categorie = "select * from categorie;";
                }

                var prodotti = new Array();
                comanda.sincro.query(query_categorie, function(categoria) {

                    if (categoria.length > 0) {

                        var count_cat_number = categoria.length;
                        var cat_n = 1;
                        categoria.forEach(function(cat, i1) {

                            //RIEMPIO I DATI DELLE CATEGORIE
                            if (prodotti[cat.id] === undefined) {
                                prodotti[cat.id] = new Object();
                            }

                            var numero_layout = "_1";
                            if (leggi_get_url('id') === '2' || leggi_get_url('id') === '3' || leggi_get_url('id') === '4') {
                                numero_layout = "_" + leggi_get_url('id');
                            }

                            //QUERY DI SELEZIONE PRODOTTI
                            //IN QUESTO CASO LA POSIZIONE HA SOLO UN NUMERO E NON DELLE COORDINATE

                            if (comanda.nome_categoria[cat.id] === 'PREFERITI') {
                                var query_prodotti = 'select id, qta_fissa, ordinamento, cat_varianti,colore_tasto,descrizione,prezzo_1,prezzo_fattorino1_norm,prezzo_fattorino2_norm,prezzo_fattorino3_norm, categoria, pagina, posizione' + numero_layout + ' as posizione, perc_iva_base, perc_iva_takeaway  from prodotti WHERE  categoria!="XXX"  and descrizione!=".NUOVO PRODOTTO" and descrizione!="" AND prezzo_1 !="" AND posizione' + numero_layout + '!="999" ORDER BY cast(posizione' + numero_layout + ' as int) ASC;';
                            } else if (comanda.nome_categoria[cat.id] === 'PREFERITI_1') {
                                var query_prodotti = 'select id, qta_fissa, ordinamento, cat_varianti,colore_tasto,descrizione,prezzo_1,prezzo_fattorino1_norm,prezzo_fattorino2_norm,prezzo_fattorino3_norm, categoria, pagina, posizione_1 as posizione, perc_iva_base, perc_iva_takeaway  from prodotti WHERE  categoria!="XXX" and descrizione!=".NUOVO PRODOTTO"  and descrizione!="" AND prezzo_1 !="" AND posizione_1!="999" ORDER BY cast(posizione_1 as int) ASC;';
                            } else if (comanda.nome_categoria[cat.id] === 'PREFERITI_2') {
                                var query_prodotti = 'select id, qta_fissa, ordinamento, cat_varianti,colore_tasto,descrizione,prezzo_1,prezzo_fattorino1_norm,prezzo_fattorino2_norm,prezzo_fattorino3_norm, categoria, pagina, posizione_2 as posizione, perc_iva_base, perc_iva_takeaway  from prodotti WHERE  categoria!="XXX" and descrizione!=".NUOVO PRODOTTO"  and descrizione!="" AND prezzo_1 !="" AND posizione_2!="999" ORDER BY cast(posizione_2 as int) ASC;';
                            } else if (comanda.nome_categoria[cat.id] === 'PREFERITI_3') {
                                var query_prodotti = 'select id, qta_fissa, ordinamento, cat_varianti,colore_tasto,descrizione,prezzo_1,prezzo_fattorino1_norm,prezzo_fattorino2_norm,prezzo_fattorino3_norm, categoria, pagina, posizione_3 as posizione, perc_iva_base, perc_iva_takeaway  from prodotti WHERE  categoria!="XXX"  and descrizione!=".NUOVO PRODOTTO" and descrizione!="" AND prezzo_1 !="" AND posizione_3!="999" ORDER BY cast(posizione_3 as int) ASC;';
                            } else if (comanda.nome_categoria[cat.id] === 'PREFERITI_4') {
                                var query_prodotti = 'select id, qta_fissa, ordinamento, cat_varianti,colore_tasto,descrizione,prezzo_1,prezzo_fattorino1_norm,prezzo_fattorino2_norm,prezzo_fattorino3_norm, categoria, pagina, posizione_4 as posizione, perc_iva_base, perc_iva_takeaway  from prodotti WHERE  categoria!="XXX" and descrizione!=".NUOVO PRODOTTO"  and descrizione!="" AND prezzo_1 !="" AND posizione_4!="999" ORDER BY cast(posizione_4 as int) ASC;';
                            } else {
                                var query_prodotti = 'select id, qta_fissa,ordinamento, cat_varianti,colore_tasto,descrizione,prezzo_1,prezzo_fattorino1_norm,prezzo_fattorino2_norm,prezzo_fattorino3_norm, categoria, pagina, posizione, perc_iva_base, perc_iva_takeaway  from prodotti WHERE  categoria!="XXX"  and descrizione!=".NUOVO PRODOTTO" and descrizione!="" AND prezzo_1 !="" AND categoria="' + cat.id + '" ORDER BY cast(posizione as int) ASC,descrizione ASC;';
                            }

                            comanda.sincro.query(query_prodotti, function(prodotto) {

                                var minwidth = comanda.larghezza_articoli;
                                var height = comanda.altezza_articoli;

                                var max_prodotti = 42;

                                var fontsize = comanda.font_articoli;

                                var i = 0;
                                var pagina = 1;
                                var max_prodotti = 42;
                                /*if (cat.descrizione.substr(0, 2) === 'V.')
                                 {
                                 max_prodotti = 41;
                                 }*/
                                var ultima_posizione = 1;
                                //PER OGNI PRODOTTO VADO A CREARE UNA SERIE DI ARRAY CON VARI DATI

                                if (prodotti[cat.id][pagina] === undefined) {
                                    prodotti[cat.id][pagina] = new Object();
                                }

                                prodotto.forEach(function(prod, i2) {


                                    if (prod.posizione !== '999') {
                                        if (prodotti[cat.id][pagina][prod.posizione] === undefined) {
                                            prodotti[cat.id][pagina][prod.posizione] = new Object();
                                        }

                                        prodotti[cat.id][pagina][prod.posizione].id = prod.id;
                                        prodotti[cat.id][pagina][prod.posizione].cat_varianti = prod.cat_varianti;
                                        prodotti[cat.id][pagina][prod.posizione].qta_fissa = prod.qta_fissa;
                                        prodotti[cat.id][pagina][prod.posizione].colore_tasto = prod.colore_tasto;
                                        prodotti[cat.id][pagina][prod.posizione].descrizione = prod.descrizione /*.taglia_parole(10, 5)*/ ;
                                        prodotti[cat.id][pagina][prod.posizione].prezzo_1 = prod.prezzo_1;
                                        prodotti[cat.id][pagina][prod.posizione].prezzo_fattorino1_norm = prod.prezzo_fattorino1_norm;
                                        prodotti[cat.id][pagina][prod.posizione].prezzo_fattorino2_norm = prod.prezzo_fattorino2_norm;
                                        prodotti[cat.id][pagina][prod.posizione].prezzo_fattorino3_norm = prod.prezzo_fattorino3_norm;
                                        prodotti[cat.id][pagina][prod.posizione].categoria = prod.categoria;
                                        prodotti[cat.id][pagina][prod.posizione].pagina = prod.pagina;
                                        prodotti[cat.id][pagina][prod.posizione].posizione = prod.posizione;
                                        prodotti[cat.id][pagina][prod.posizione].perc_iva_base = prod.perc_iva_base;
                                        prodotti[cat.id][pagina][prod.posizione].perc_iva_takeaway = prod.perc_iva_takeaway;
                                        ultima_posizione = parseInt(prod.posizione) + 1;
                                    } else /*if (comanda.dispositivo !== "lang_33")*/ {
                                        if (prodotti[cat.id][pagina][ultima_posizione.toString()] === undefined) {
                                            prodotti[cat.id][pagina][ultima_posizione.toString()] = new Object();
                                        }

                                        prodotti[cat.id][pagina][ultima_posizione.toString()].id = prod.id;
                                        prodotti[cat.id][pagina][ultima_posizione.toString()].qta_fissa = prod.qta_fissa;
                                        prodotti[cat.id][pagina][ultima_posizione.toString()].cat_varianti = prod.cat_varianti;
                                        prodotti[cat.id][pagina][ultima_posizione.toString()].colore_tasto = prod.colore_tasto;
                                        prodotti[cat.id][pagina][ultima_posizione.toString()].descrizione = prod.descrizione /*.taglia_parole(10, 5)*/ ;
                                        prodotti[cat.id][pagina][ultima_posizione.toString()].prezzo_1 = prod.prezzo_1;
                                        prodotti[cat.id][pagina][ultima_posizione.toString()].prezzo_fattorino1_norm = prod.prezzo_fattorino1_norm;
                                        prodotti[cat.id][pagina][ultima_posizione.toString()].prezzo_fattorino2_norm = prod.prezzo_fattorino2_norm;
                                        prodotti[cat.id][pagina][ultima_posizione.toString()].prezzo_fattorino3_norm = prod.prezzo_fattorino3_norm;
                                        prodotti[cat.id][pagina][ultima_posizione.toString()].categoria = prod.categoria;
                                        prodotti[cat.id][pagina][ultima_posizione.toString()].pagina = prod.pagina;
                                        prodotti[cat.id][pagina][ultima_posizione.toString()].posizione = prod.posizione;
                                        prodotti[cat.id][pagina][ultima_posizione.toString()].perc_iva_base = prod.perc_iva_base;
                                        prodotti[cat.id][pagina][ultima_posizione.toString()].perc_iva_takeaway = prod.perc_iva_takeaway;
                                        ultima_posizione++;
                                    }

                                    i++;
                                });
                                var i = 1;
                                //PRENDO LE PAGINE NEI PRODOTTI
                                for (var p in prodotti[cat.id]) {


                                    if (esperimento_output[cat.id] === undefined) {
                                        esperimento_output[cat.id] = new Object();
                                    }

                                    if (esperimento_output[cat.id][p] === undefined) {
                                        esperimento_output[cat.id][p] = '';
                                    }


                                    var pag = 1;
                                    for (; pag <= 4; pag++) {
                                        esperimento_output[cat.id][pag] = "";
                                        esperimento_output[cat.id][pag] += "<div class='pag_" + pag + " cat_" + cat.id + "' >";
                                        esperimento_output[cat.id][pag] += "<div class='bs-docs-section btn_COMANDA'>";
                                        esperimento_output[cat.id][pag] += "<div class='bs-glyphicons'>";
                                        esperimento_output[cat.id][pag] += "<ul class='bs-glyphicons-list elenco_prodotti_draggable'>";

                                        /*if (cat.descrizione.substr(0, 2) === 'V.' && i === 1) {
                                         esperimento_output[cat.id][p] += '<li style="text-shadow: 1px 1px 1px silver;font-size: ' + fontsize + '; min-width: ' + minwidth + ';height: ' + height + ';background-color:lightblue;background-image:-webkit-linear-gradient(top, lightblue,#61aec7);" value="VARIANTELIBERA" class="btn_variante_libera btn_comanda prodotto_esistente"  ' + comanda.evento + '="popup_variante_libera();"><span style="" class="glyphicon-class"><div><strong>+</strong></span></li>';
                                         conteggio_pagine_output[cat.id] = p;
                                         i++;
                                         }*/

                                        //PRENDO I PRODOTTI
                                        for (; i <= (max_prodotti * pag); i++) {



                                            if (prodotti[cat.id][p][i] !== undefined) {


                                                var articolo = prodotti[cat.id][p][i];
                                                var colore = '';
                                                console.log("COLORE TASTI" + articolo.colore_tasto);
                                                console.log("COLORE TASTI", cat.descrizione.substr(0, 2) === 'V.', comanda.mobile === false && comanda.pizzeria_asporto === false && articolo.colore_tasto !== undefined && articolo.colore_tasto !== null && articolo.colore_tasto.length > 0, comanda.mobile === true && articolo.colore_tasto !== undefined && articolo.colore_tasto !== null && articolo.colore_tasto.length > 0, comanda.pizzeria_asporto === true);
                                                //linen
                                                var colore_tasto = "linen";
                                                if (((colori_tablet_uguali_pc === "true" && comanda.mobile === true) || comanda.mobile === false) && articolo.colore_tasto !== undefined && typeof(articolo.colore_tasto) === 'string' && articolo.colore_tasto.length > 0) {
                                                    colore_tasto = articolo.colore_tasto;
                                                }


                                                //PER IDENTIFICARLO E CAMBIARLO CON UN TIMER ANCHE MENTRE LAVORI
                                                var id_articolo = new Date().getTime();
                                                var stelline = "";
                                                //
                                                //Quando c'e un orario di inizio o fine invece che qta fissa
                                                if (RAM.tabella_sconti_cod_promo_2[articolo.id] !== undefined) {
                                                    stelline = "";
                                                    for (var key in RAM.tabella_sconti_cod_promo_2[articolo.id]) {



                                                        stelline = "";
                                                        var oggetto_scontato = RAM.tabella_sconti_cod_promo_2[articolo.id][key];
                                                        var ora_attuale = new Date();
                                                        if (oggetto_scontato.giorni_settimana === '' || oggetto_scontato.giorni_settimana.indexOf(ora_attuale.getDay().toString()) !== -1) {
                                                            var differenza_inizio = 18400000;
                                                            var differenza_fine = 18400000;
                                                            if (oggetto_scontato.a_ora.length === 5) {
                                                                var ora_fine = new Date();
                                                                var a_ora = oggetto_scontato.a_ora.substr(0, 2);
                                                                ora_fine.setHours(parseInt(a_ora));
                                                                var a_minuti = oggetto_scontato.a_ora.substr(3, 2);
                                                                ora_fine.setMinutes(parseInt(a_minuti));
                                                                ora_fine.setSeconds(00);
                                                                ora_fine.setMilliseconds(000);
                                                                differenza_fine = ora_fine.getTime() - ora_attuale.getTime();
                                                                if (differenza_fine > 0) {
                                                                    var prezzo_originale = "";
                                                                    if (oggetto_scontato.tipo_sconto === "E") {
                                                                        var r = alasql("select prezzo_1 from prodotti where id='" + oggetto_scontato.id_articolo + "' and categoria!='XXX' limit 1;");
                                                                        prezzo_originale = r[0].prezzo_1;
                                                                    }
                                                                    timeout_colore_originale(id_articolo, cat.id, pag, colore_tasto, differenza_fine, oggetto_scontato.a_ora, oggetto_scontato.tipo_sconto, prezzo_originale);
                                                                }
                                                            }

                                                            if (oggetto_scontato.da_ora.length === 5) {
                                                                var ora_inizio = new Date();
                                                                var da_ora = oggetto_scontato.da_ora.substr(0, 2);
                                                                ora_inizio.setHours(parseInt(da_ora));
                                                                var da_minuti = oggetto_scontato.da_ora.substr(3, 2);
                                                                ora_inizio.setMinutes(parseInt(da_minuti));
                                                                ora_inizio.setSeconds(00);
                                                                ora_inizio.setMilliseconds(000);
                                                                differenza_inizio = ora_inizio.getTime() - ora_attuale.getTime();
                                                                if (differenza_inizio > 0) {
                                                                    timeout_cambio_colore(id_articolo, cat.id, pag, colore_tasto, differenza_inizio, oggetto_scontato.da_ora, oggetto_scontato.tipo_sconto, oggetto_scontato.valore_sconto);
                                                                }
                                                            }
                                                            var testo_query = "SELECT Field510 from settaggi_profili";
                                                            var querry_settagio = alasql(testo_query);
                                                            if( querry_settagio[0].Field510!=="1"){
                                                            if ((differenza_inizio < 0 && differenza_fine > 0) || (oggetto_scontato.da_ora.length !== 5 && oggetto_scontato.a_ora.length !== 5)) {
                                                                stelline = "background-image:url(\'./img/TEXTURE.png\');background-size:contain;background-repeat:no-repeat;";
                                                            }}

                                                        }

                                                    }

                                                }
                                                //


                                                if (cat.descrizione.substr(0, 2) === 'V.') {
                                                    if (colore_tasto === undefined || colore_tasto === null || colore_tasto === 'null' || colore_tasto === '' || colore_tasto === 'linen') {
                                                        colore = ' style="font-size: ' + fontsize + '; min-width: ' + minwidth + ';min-height: ' + height + ';height: ' + height + ';background-color:lightblue;background-image:-webkit-linear-gradient(top, lightblue,#61aec7);" ';
                                                    } else {
                                                        colore = ' style="font-size: ' + fontsize + '; min-width: ' + minwidth + ';min-height: ' + height + ';height: ' + height + ';background-color:' + colore_tasto + ';background-image: -webkit-linear-gradient(top, ' + colore_tasto + ', rgba(0, 0, 0, 0.35));" ';
                                                    }
                                                } else {

                                                    if (comanda.mobile === true) {
                                                        colore = ' style="font-size: ' + fontsize + '; min-width: ' + minwidth + ';min-height: ' + height + ';height: ' + height + ';background-color:' + colore_tasto + ';background-image: -webkit-linear-gradient(top, ' + colore_tasto + ', rgba(0, 0, 0, 0.35));' + stelline + '" ';
                                                    } else {
                                                        colore = ' style="font-size: ' + fontsize + '; min-width: ' + minwidth + ';min-height: ' + height + ';height: ' + height + ';background-color:' + colore_tasto + ';background-image: -webkit-linear-gradient(top, ' + colore_tasto + ', rgba(0, 0, 0, 0.35));" ';

                                                    }

                                                }

                                                //DIFFERENZA TRA ARTICOLO CON VARIANTE AUTOMATICA E SENZA

                                                console.log("ARTICOLO VARIANTE AUTO", articolo, articolo.cat_varianti !== undefined && articolo.cat_varianti !== "undefined" && articolo.cat_varianti !== "undefined/undefined" && articolo.cat_varianti !== null && articolo.cat_varianti.indexOf("/") !== -1 && articolo.cat_varianti.split('/')[1] !== "ND");
                                                var str_qta_fissa = '';
                                                var str_quantita = "$(\'#quantita_articolo\').val()";
                                                if (articolo.qta_fissa !== null && articolo.qta_fissa !== '') {
                                                    str_quantita = articolo.qta_fissa + "*RAM.quantita_battuta";
                                                    str_qta_fissa = 'N° ' + articolo.qta_fissa + '<br/>';
                                                }


                                                if (articolo.descrizione !== undefined && articolo.descrizione !== null && typeof(articolo.descrizione) === "string") {
                                                    var risultato = new Array();

                                                    var counter = 0;
                                                    var stringa = articolo.descrizione;
                                                    var lettere = 9;
                                                    while (counter < 2 && stringa.length > 0) {
                                                        /*if (counter > 0) {
                                                         builder.addText('\n');
                                                         }*/
                                                        risultato.push(stringa.substring(0, lettere));
                                                        stringa = stringa.substring(lettere);
                                                        counter++;
                                                    }

                                                    /*articolo.descrizione.match(/\w+/gi).forEach(function (v) {
                                                     risultato.push(v.substr(0, 9));
                                                     });*/
                                                    articolo.descrizione = risultato.join("<br>");
                                                }


                                                if (articolo.cat_varianti !== undefined && articolo.cat_varianti !== "undefined" && articolo.cat_varianti !== "undefined/undefined" && articolo.cat_varianti !== null && articolo.cat_varianti.indexOf("/") !== -1 && articolo.cat_varianti.split('/')[1] !== "ND") {
                                                    esperimento_output[cat.id][pag] += '<li ' + colore + ' value="' + articolo.id + '" class="btn_comanda prodotto_esistente"  ' + comanda.evento + '="if (sblocco_tasti === true) {sblocco_tasti = false;comanda.variante_automatica=\'' + articolo.cat_varianti + '\';aggiungi_articolo(\'' + articolo.id + '\',\'' + articolo.descrizione.replace('\'', "\\'") + '\',\'&euro;' + articolo.prezzo_1 + '\',null,' + str_quantita + ',null,\'' + articolo.cat_varianti.match(/\w+/g)[0] + '\',null,null,\'' + articolo.perc_iva_base + '\',\'' + articolo.perc_iva_takeaway + '\');}">';
                                                    conteggio_pagine_output[cat.id] = pag;
                                                } else {
                                                    //SE LA VARIANTE E' NORMALE
                                                    esperimento_output[cat.id][pag] += '<li  ' + colore + ' value="' + articolo.id + '" class="btn_comanda prodotto_esistente"  ' + comanda.evento + '="if (sblocco_tasti === true) {sblocco_tasti = false;aggiungi_articolo(\'' + articolo.id + '\',\'' + articolo.descrizione.replace('\'', "\\'") + '\',\'&euro;' + articolo.prezzo_1 + '\',null,' + str_quantita + ',null,\'' + articolo.cat_varianti + '\',null,null,\'' + articolo.perc_iva_base + '\',\'' + articolo.perc_iva_takeaway + '\');}">';
                                                    conteggio_pagine_output[cat.id] = pag;
                                                }

                                                if (comanda.mobile !== true) {
                                                    esperimento_output[cat.id][pag] += '<span style="' + stelline + '" id="' + id_articolo + '" class="glyphicon-class">';
                                                } else {
                                                    esperimento_output[cat.id][pag] += '<span id="' + id_articolo + '" class="glyphicon-class">';
                                                }


                                                esperimento_output[cat.id][pag] += '<div>' + str_qta_fissa + '<strong>' + articolo.descrizione + '</strong><br/><span class="prezzo_normale">€ ' + articolo.prezzo_1 + '</span><span class="prezzo_fattorino1">€ ' + articolo.prezzo_fattorino1_norm + '</span><span class="prezzo_fattorino2">€ ' + articolo.prezzo_fattorino2_norm + '</span><span class="prezzo_fattorino3">€ ' + articolo.prezzo_fattorino3_norm + '</span></div></span>';

                                                esperimento_output[cat.id][pag] += '</li>';
                                                console.log("ELENCO PRODOTTI DATI: ", i, pagina);
                                                prodotti_fuoriscope = prodotti;
                                            } else {
                                                esperimento_output[cat.id][pag] += '<li style="font-size: ' + fontsize + '; min-width: ' + minwidth + ';min-height: ' + height + ';height: ' + height + ';" class="btn_comanda" >';
                                                esperimento_output[cat.id][pag] += '<span class="glyphicon-class"><br/></span>';
                                                esperimento_output[cat.id][pag] += '</li>';
                                            }
                                        }



                                        esperimento_output[cat.id][pag] += "</ul>";
                                        esperimento_output[cat.id][pag] += "</div>";
                                        esperimento_output[cat.id][pag] += "</div>";
                                        esperimento_output[cat.id][pag] += "</div>";
                                    }
                                }

                                console.log(cat_n, count_cat_number);
                                if (cat_n === count_cat_number) {
                                    console.log("OGGETTO_PRODOTTI", prodotti);
                                    inizio_elenco_prodotti = false;
                                    callBack(true);
                                }

                                cat_n++;
                            });
                        });
                    } else {
                        callBack(true);
                    }
                });
            });
        }
    };
} else {
    //FA PARTE DELLA FUNZIONE SOTTO
    var esperimento_output;
    var conteggio_pagine_output;
    var inizio_elenco_prodotti = false;
    var prodotti_fuoriscope = new Array();
    comanda.funzionidb.elenco_prodotti = function(callBack) {

        if (inizio_elenco_prodotti === false) {
            inizio_elenco_prodotti = true;
            console.log("INIZIO CALLBACK PRODOTTI");
            conteggio_pagine_output = new Object();
            esperimento_output = new Object();
            //QUERY SETTAGGIO COLORI TABLET
            var query_colori_tablet = "select Field435 from settaggi_profili where id=" + comanda.folder_number + " limit 1;";
            comanda.sincro.query(query_colori_tablet, function(colori_tablet) {

                //Può essere "true" o "false"
                var colori_tablet_uguali_pc = colori_tablet[0].Field435;
                //QUERY DI SELEZIONE CATEGORIE

                var query_categorie = "select * from categorie;";
                var prodotti = new Array();
                //VARIABILE DI OUTPUT



                comanda.sincro.query(query_categorie, function(categoria) {


                    if (categoria.length > 0) {
                        var count_cat_number = categoria.length;
                        var cat_n = 1;
                        categoria.forEach(function(cat, i1) {

                            //RIEMPIO I DATI DELLE CATEGORIE
                            if (prodotti[cat.id] === undefined) {
                                prodotti[cat.id] = new Object();
                            }

                            var settaggi_profili = alasql("SELECT Field191 FROM settaggi_profili WHERE id=" + comanda.folder_number + " limit 1;");
                            var Field191 = settaggi_profili[0].Field191;

                            //QUERY DI SELEZIONE PRODOTTI
                            //IN QUESTO CASO LA POSIZIONE HA SOLO UN NUMERO E NON DELLE COORDINATE
                            var query_prodotti = '';
                            if (comanda.mobile !== true) {

                                query_prodotti = 'select id, ordinamento, cat_varianti,colore_tasto,descrizione,prezzo_1, categoria, pagina, posizione, perc_iva_base, perc_iva_takeaway  from prodotti WHERE  categoria!="XXX" and descrizione!=".NUOVO PRODOTTO"  and descrizione!="" AND prezzo_1 !="" AND categoria="' + cat.id + '"  ORDER BY cast(posizione as int) ASC, descrizione ASC;';
                            } else {
                                query_prodotti = 'select id, ordinamento, cat_varianti,colore_tasto,descrizione,prezzo_1, categoria, pagina, posizione, perc_iva_base, perc_iva_takeaway  from prodotti WHERE  categoria!="XXX"  and descrizione!=".NUOVO PRODOTTO" and descrizione!="" AND prezzo_1 !="" AND categoria="' + cat.id + '" ORDER BY cast(ordinamento as int) ASC, descrizione ASC;';

                            }

                            console.log("PROD QUERY", query_prodotti);
                            //FUNZIONE DELLA QUERY
                            comanda.sincro.query(query_prodotti, function(prodotto) {
                                var i = 0;
                                var pagina = 0;
                                //PER OGNI PRODOTTO VADO A CREARE UNA SERIE DI ARRAY CON VARI DATI
                                prodotto.forEach(function(prod, i2) {

                                    //CALCOLA LA PAGINA IN BASE AL NUMERO DI PRODOTTI
                                    //Al 50esimo articolo cambia pagina e non 49esimo!

                                    //ATTENZIONE A QUESTA CONDIZIONE PUO' ESSERE PERICOLOSA!
                                    var max_prodotti = 0;
                                    if (cat.descrizione.substr(0, 2) === 'V.') {

                                        max_prodotti = 48;
                                    } else {

                                        max_prodotti = 49;
                                    }


                                    if (i % max_prodotti === 0 && comanda.mobile !== true) {
                                        pagina++;
                                    } else if (comanda.mobile === true) {
                                        pagina = 1;
                                    }

                                    if (prodotti[cat.id][pagina] === undefined) {
                                        prodotti[cat.id][pagina] = new Object();
                                    }

                                    if (prodotti[cat.id][pagina][i] === undefined) {
                                        prodotti[cat.id][pagina][i] = new Object();
                                    }

                                    //DATI PRODOTTO
                                    prodotti[cat.id][pagina][i].id = prod.id;
                                    prodotti[cat.id][pagina][i].cat_varianti = prod.cat_varianti;
                                    prodotti[cat.id][pagina][i].colore_tasto = prod.colore_tasto;
                                    prodotti[cat.id][pagina][i].descrizione = prod.descrizione /*.taglia_parole(10, 5)*/ ;
                                    prodotti[cat.id][pagina][i].prezzo_1 = parseFloat(prod.prezzo_1).toFixed(2);
                                    prodotti[cat.id][pagina][i].categoria = prod.categoria;
                                    prodotti[cat.id][pagina][i].pagina = prod.pagina;
                                    prodotti[cat.id][pagina][i].posizione = prod.posizione;
                                    prodotti[cat.id][pagina][i].perc_iva_base = prod.perc_iva_base;
                                    prodotti[cat.id][pagina][i].perc_iva_takeaway = prod.perc_iva_takeaway;
                                    i++;
                                });
                                //PRENDO LE PAGINE NEI PRODOTTI
                                for (var p in prodotti[cat.id]) {
                                    if (esperimento_output[cat.id] === undefined) {
                                        esperimento_output[cat.id] = new Object();
                                    }

                                    if (esperimento_output[cat.id][p] === undefined) {
                                        esperimento_output[cat.id][p] = '';
                                    }

                                    esperimento_output[cat.id][p] += "<div class='pag_" + p + " cat_" + cat.id + "' >";
                                    esperimento_output[cat.id][p] += "<div class='bs-docs-section btn_COMANDA'>";
                                    esperimento_output[cat.id][p] += "<div class='bs-glyphicons'>";
                                    esperimento_output[cat.id][p] += "<ul class='bs-glyphicons-list elenco_prodotti_draggable'>";
                                    var i = 0;
                                    if (cat.descrizione.substr(0, 2) === 'V.' && i === 0) {
                                        i++;
                                        if (comanda.mobile === true) {
                                            esperimento_output[cat.id][p] += '<li style="background-color: lightblue;background-image: -webkit-linear-gradient(top, lightblue, rgb(97, 174, 199));font-weight: bold;text-align: left;padding-left: 18vh;" value="VARIANTELIBERA" class="btn_variante_libera btn_comanda prodotto_esistente"  ' + comanda.evento + '="popup_variante_libera();">+</li>';
                                            conteggio_pagine_output[cat.id] = p;
                                        } else {
                                            esperimento_output[cat.id][p] += '<li style="background-color:lightblue;background-image:-webkit-linear-gradient(top, lightblue,#61aec7); color:aliceblue; font-weight: bold; font-size:3em;" value="VARIANTELIBERA" class="btn_variante_libera btn_comanda prodotto_esistente"  ' + comanda.evento + '="popup_variante_libera();">+</li>';
                                            conteggio_pagine_output[cat.id] = p;
                                        }
                                    } else if (comanda.mobile === true && Field191 === "true") {
                                        esperimento_output[cat.id][p] += '<li style="background-color:rgb(255, 245, 238);background-image: -webkit-linear-gradient(top, rgb(255, 245, 238), rgba(0, 0, 0, 0.35)); font-size: 2.5vh;" class="btn_variante_libera btn_comanda prodotto_esistente"  ' + comanda.evento + '="popup_prodotto_libero();"><span class="glyphicon-class"><strong>+</strong></span></li>';
                                        conteggio_pagine_output[cat.id] = p;
                                    }

                                    //PRENDO I PRODOTTI
                                    for (var art in prodotti[cat.id][p]) {

                                        var articolo = prodotti[cat.id][p][art];
                                        var colore = '';
                                        console.log("COLORE TASTI" + articolo.colore_tasto);
                                        console.log("COLORE TASTI", cat.descrizione.substr(0, 2) === 'V.', comanda.mobile === false && articolo.colore_tasto !== undefined && articolo.colore_tasto !== null && articolo.colore_tasto.length > 0, comanda.mobile === true && articolo.colore_tasto !== undefined && articolo.colore_tasto !== null && articolo.colore_tasto.length > 0, comanda.pizzeria_asporto === true);
                                        //linen
                                        var colore_tasto = "linen";
                                        if (((colori_tablet_uguali_pc === "true" && comanda.mobile === true) || comanda.mobile === false) && articolo.colore_tasto !== undefined && typeof(articolo.colore_tasto) === 'string' && articolo.colore_tasto.length > 0) {
                                            colore_tasto = articolo.colore_tasto;
                                        }

                                        var id_articolo = new Date().getTime();
                                        var stelline = "";
                                        //
                                        //Quando c'e un orario di inizio o fine invece che qta fissa
                                        if (RAM.tabella_sconti_cod_promo_2[articolo.id] !== undefined) {
                                            stelline = "";
                                            for (var key in RAM.tabella_sconti_cod_promo_2[articolo.id]) {

                                                stelline = "";
                                                var oggetto_scontato = RAM.tabella_sconti_cod_promo_2[articolo.id][key];
                                                var ora_attuale = new Date();
                                                if (oggetto_scontato.giorni_settimana === '' || oggetto_scontato.giorni_settimana.indexOf(ora_attuale.getDay().toString()) !== -1) {


                                                    var differenza_inizio = 18400000;
                                                    var differenza_fine = 18400000;
                                                    if (oggetto_scontato.a_ora.length === 5) {
                                                        var ora_fine = new Date();
                                                        var a_ora = oggetto_scontato.a_ora.substr(0, 2);
                                                        ora_fine.setHours(parseInt(a_ora));
                                                        var a_minuti = oggetto_scontato.a_ora.substr(3, 2);
                                                        ora_fine.setMinutes(parseInt(a_minuti));
                                                        ora_fine.setSeconds(00);
                                                        ora_fine.setMilliseconds(000);
                                                        differenza_fine = ora_fine.getTime() - ora_attuale.getTime();
                                                        if (differenza_fine > 0) {
                                                            var prezzo_originale = "";
                                                            if (oggetto_scontato.tipo_sconto === "E") {
                                                                var r = alasql("select prezzo_1 from prodotti where id='" + oggetto_scontato.id_articolo + "' and categoria!='XXX' limit 1;");
                                                                prezzo_originale = r[0].prezzo_1;
                                                            }
                                                            timeout_colore_originale(id_articolo, cat.id, p, colore_tasto, differenza_fine, oggetto_scontato.a_ora, oggetto_scontato.tipo_sconto, prezzo_originale);
                                                        }
                                                    }

                                                    if (oggetto_scontato.da_ora.length === 5) {
                                                        var ora_inizio = new Date();
                                                        var da_ora = oggetto_scontato.da_ora.substr(0, 2);
                                                        ora_inizio.setHours(parseInt(da_ora));
                                                        var da_minuti = oggetto_scontato.da_ora.substr(3, 2);
                                                        ora_inizio.setMinutes(parseInt(da_minuti));
                                                        ora_inizio.setSeconds(00);
                                                        ora_inizio.setMilliseconds(000);
                                                        differenza_inizio = ora_inizio.getTime() - ora_attuale.getTime();
                                                        if (differenza_inizio > 0) {
                                                            timeout_cambio_colore(id_articolo, cat.id, p, colore_tasto, differenza_inizio, oggetto_scontato.da_ora, oggetto_scontato.tipo_sconto, oggetto_scontato.valore_sconto);
                                                        }
                                                    }
                                                    var testo_query = "SELECT Field510 from settaggi_profili";
                                                     var querry_settagio = alasql(testo_query);
                                                     if( querry_settagio[0].Field510!=="1"){

                                                    if ((differenza_inizio < 0 && differenza_fine > 0) || (oggetto_scontato.da_ora.length !== 5 && oggetto_scontato.a_ora.length !== 5 && oggetto_scontato.qta_soglia.length > 0)) {
                                                        stelline = "background-image:url(\'./img/TEXTURE.png\');background-size:contain;background-repeat:no-repeat;";
                                                    }}

                                                }

                                            }

                                        }

                                        if (cat.descrizione.substr(0, 2) === 'V.') {
                                            if (colore_tasto === undefined || colore_tasto === null || colore_tasto === 'null' || colore_tasto === '' || colore_tasto === 'linen') {
                                                colore = ' style="background-color:lightblue;background-image:-webkit-linear-gradient(top, lightblue,#61aec7);" ';
                                            } else {
                                                colore = ' style=" background-color:' + colore_tasto + ';background-image: -webkit-linear-gradient(top, ' + colore_tasto + ', rgba(0, 0, 0, 0.35));" ';
                                            }
                                        } else {
                                            if (comanda.mobile === true) {
                                                colore = ' style="background-color:' + colore_tasto + ';background-image: -webkit-linear-gradient(top, ' + colore_tasto + ', rgba(0, 0, 0, 0.35));' + stelline + '" ';
                                            } else {
                                                colore = ' style="background-color:' + colore_tasto + ';background-image: -webkit-linear-gradient(top, ' + colore_tasto + ', rgba(0, 0, 0, 0.35));" ';
                                            }
                                        }


                                        //DIFFERENZA TRA ARTICOLO CON VARIANTE AUTOMATICA E SENZA

                                        console.log("ARTICOLO VARIANTE AUTO", articolo, articolo.cat_varianti !== undefined && articolo.cat_varianti !== "undefined" && articolo.cat_varianti !== "undefined/undefined" && articolo.cat_varianti !== null && articolo.cat_varianti.indexOf("/") !== -1 && articolo.cat_varianti.split('/')[1] !== "ND");
                                        if (articolo.cat_varianti !== undefined && articolo.cat_varianti !== "undefined" && articolo.cat_varianti !== "undefined/undefined" && articolo.cat_varianti !== null && articolo.cat_varianti.indexOf("/") !== -1 && articolo.cat_varianti.split('/')[1] !== "ND") {
                                            esperimento_output[cat.id][p] += '<li ' + colore + ' value="' + articolo.id + '" class="btn_comanda prodotto_esistente"  ' + comanda.evento + '="if (sblocco_tasti === true) {sblocco_tasti = false;comanda.variante_automatica=\'' + articolo.cat_varianti + '\';aggiungi_articolo(\'' + articolo.id + '\',\'' + articolo.descrizione.replace('\'', "\\'") + '\',\'&euro;' + articolo.prezzo_1 + '\',null,$(\'#quantita_articolo\').val(),null,\'' + articolo.cat_varianti.match(/\w+/g)[0] + '\',null,null,\'' + articolo.perc_iva_base + '\',\'' + articolo.perc_iva_takeaway + '\');}">';
                                            conteggio_pagine_output[cat.id] = p;
                                        } else {
                                            //SE LA VARIANTE E' NORMALE
                                            esperimento_output[cat.id][p] += '<li ' + colore + ' value="' + articolo.id + '" class="btn_comanda prodotto_esistente"  ' + comanda.evento + '="if (sblocco_tasti === true) {sblocco_tasti = false;aggiungi_articolo(\'' + articolo.id + '\',\'' + articolo.descrizione.replace('\'', "\\'") + '\',\'&euro;' + articolo.prezzo_1 + '\',null,$(\'#quantita_articolo\').val(),null,\'' + articolo.cat_varianti + '\',null,null,\'' + articolo.perc_iva_base + '\',\'' + articolo.perc_iva_takeaway + '\');}">';
                                            conteggio_pagine_output[cat.id] = p;
                                        }

                                        if (articolo.descrizione !== undefined && articolo.descrizione !== null && typeof(articolo.descrizione) === "string") {
                                            var risultato = new Array();

                                            var counter = 0;
                                            var stringa = articolo.descrizione;
                                            var lettere = 9;
                                            while (counter < 2 && stringa.length > 0) {
                                                /*if (counter > 0) {
                                                 builder.addText('\n');
                                                 }*/
                                                risultato.push(stringa.substring(0, lettere));
                                                stringa = stringa.substring(lettere);
                                                counter++;
                                            }

                                            /*articolo.descrizione.match(/\w+/gi).forEach(function (v) {
                                             risultato.push(v.substr(0, 9));
                                             });*/
                                            articolo.descrizione = risultato.join("<br>");
                                        }

                                        if (comanda.mobile !== true) {
                                            esperimento_output[cat.id][p] += '<span style="' + stelline + '" id="' + id_articolo + '" class="glyphicon-class">';
                                        } else {
                                            esperimento_output[cat.id][p] += '<span id="' + id_articolo + '" class="glyphicon-class">';
                                        }

                                        if (comanda.mobile === true && stelline !== "" && oggetto_scontato.tipo_sconto === "E") {
                                            esperimento_output[cat.id][p] += '<strong>' + articolo.descrizione + '</strong><br/><span class="prezzo_smartphone">€ ' + oggetto_scontato.valore_sconto + '</span></span>';
                                        } else {
                                            esperimento_output[cat.id][p] += '<strong>' + articolo.descrizione + '</strong><br/><span class="prezzo_smartphone">€ ' + articolo.prezzo_1 + '</span></span>';
                                        }

                                        esperimento_output[cat.id][p] += '</li>';
                                        i++;
                                        console.log("ELENCO PRODOTTI DATI: ", i, pagina);
                                    }

                                    prodotti_fuoriscope = prodotti;
                                    //Serve a riempire le restanti caselle della griglia
                                    if (comanda.mobile !== true) {

                                        var numero_prodotti = 49;
                                        for (; i % numero_prodotti !== 0;) {
                                            esperimento_output[cat.id][p] += '<li class="btn_comanda" >';
                                            esperimento_output[cat.id][p] += '<span class="glyphicon-class"><br/></span>';
                                            esperimento_output[cat.id][p] += '</li>';
                                            i++;
                                            console.log("ELENCO PRODOTTI DATI: ", i, pagina);
                                        }
                                    }



                                    esperimento_output[cat.id][p] += "</ul>";
                                    esperimento_output[cat.id][p] += "</div>";
                                    esperimento_output[cat.id][p] += "</div>";
                                    esperimento_output[cat.id][p] += "</div>";
                                    console.log("ELENCO PRODOTTI DATI " + p + " - NUMERO ARTICOLI: ", prodotti[cat.id][p]);
                                }

                                console.log(cat_n, count_cat_number);
                                if (cat_n === count_cat_number) {
                                    console.log("OGGETTO_PRODOTTI", prodotti);
                                    inizio_elenco_prodotti = false;
                                    callBack(true);
                                }

                                cat_n++;
                            });
                        });
                    } else {
                        callBack(true);
                    }
                });
            });
        }
    };
}


async function stampa_comanda(saltafaseparcheggio, disabilita_avviso, cambio_orario_consegna, torna_a_tavolo, solo_riepiloghi, bool_ristampa, solo_servito, numero_progressivo) {


    if (comanda.pizzeria_asporto !== true && comanda.tavolo.left(7) === "ASPORTO") {
        stampa(["comanda"]);
    } else {

        if (comanda.query_su_socket === true && comanda.stampa_ordini_qui !== true) {

            //invia_socket
            comanda.sock.send({ tipo: "stampa_comanda", operatore: comanda.operatore, query: comanda.tavolo, terminale: comanda.terminale, ip: comanda.ip_address });

            btn_tavoli();

        } else {


            var numero_tavolo = comanda.tavolo;

            if (comanda.mobile === true && numero_tavolo === "BAR") {
                indietro_categoria();
            }

            var data = comanda.funzionidb.data_attuale().substr(0, 8);
            var ora = comanda.funzionidb.data_attuale().substr(9, 8);

            var descrizione_coperti, quantita_coperti;

            $(comanda.intestazione_conto).find('tr:contains("COPERTI"),tr:contains("GEDECKE")').each(function() {
                descrizione_coperti = $(this).find('td:nth-child(2)').html().replace(/&nbsp;/g, "");
                quantita_coperti = $(this).find('td:nth-child(1)').html();
            });

            if (quantita_coperti === undefined || quantita_coperti === null || quantita_coperti === 'undefined') {
                quantita_coperti = 0;
            }



            var promessa = new Promise(function(resolve, reject) {

                console.log("PROMISE: INIZIO");

                var query = "select * from comanda where ntav_comanda='" + numero_tavolo + "' and stato_record='ATTIVO' and desc_art!='EXTRA' and desc_art!='=EXTRA' order by posizione asc,nodo asc;";

                comanda.sincro.query(query, function(calcolo_varianti) {

                    console.log("PROMISE: QUERY CALCOLO VARIANTE");

                    var buffer_nodo_princ = new Object();

                    var passa_funzione = true;
                    var codici_promo_1_presenti = false;


                    calcolo_varianti.forEach(function(e) {
                        if (e.cod_promo === "1") {
                            codici_promo_1_presenti = true;
                            buffer_nodo_princ[e.nodo] = new Object();
                            buffer_nodo_princ[e.nodo].totale_chiamante = parseFloat(e.prezzo_un) * parseInt(e.quantita);
                            buffer_nodo_princ[e.nodo].totale_varianti = 0;
                        } else if (e.cod_promo === "V_1" && e.nodo.length > 3 && buffer_nodo_princ[e.nodo.substr(0, 3)] !== undefined) {
                            buffer_nodo_princ[e.nodo.substr(0, 3)].totale_varianti += parseFloat(e.prezzo_un) * parseInt(e.quantita);
                        } else if (codici_promo_1_presenti === true && e.posizione === "SCONTO" && comanda.sconto_su_promo === "0") {
                            passa_funzione = "ERR_SCONTO_PROMO";
                        }

                    });


                    for (var nodo in buffer_nodo_princ) {
                        if (parseFloat(buffer_nodo_princ[nodo].totale_chiamante).toFixed(2) !== parseFloat(buffer_nodo_princ[nodo].totale_varianti).toFixed(2)) {
                            passa_funzione = "ERR_TOTALI";
                        }
                    }


                    if (passa_funzione === true) {

                        console.log("PROMISE: PASSA FUNZIONE TRUE");

                        if (numero_tavolo === "BAR" || numero_tavolo.indexOf("CONTINUO") !== -1) {



                            if (comanda.stampa_comanda === false) {
                                var testo_query = "update comanda set stampata_sn='S',data_stampa='" + data + "',ora_stampa='" + ora + "',fiscalizzata_sn='C' where stato_record='ATTIVO' and ntav_comanda='" + numero_tavolo + "'";
                            } else {
                                var testo_query = "update comanda set fiscalizzata_sn='C' where stato_record='ATTIVO' and ntav_comanda='" + numero_tavolo + "'";
                            }
                            comanda.sock.send({ tipo: "comanda_stampata", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                            comanda.sincro.query(testo_query, function() {

                                console.log("PROMISE: QUERY COMANDA STAMPATA");

                                //salva_comanda(comanda.parcheggio, true);
                                if (comanda.array_dbcentrale_mancanti.length > 0) {
                                    comanda.sincro.query_cassa();
                                }

                                console.log("PROMISE: QUERY COMANDA STAMPATA RESOLVE TRUE");
                                resolve(true);
                            });
                        } else if (comanda.licenza_vera === 'BrovedaniVerona' && comanda.avviso_righe_prezzo_zero === '1' && $(comanda.conto).not(comanda.intestazione_conto).find('tr[id^="art_"]:contains("€"):contains("€0.00")').not('.non-contare-riga').length > 0) {

                            bootbox.confirm("Attenzione: ci sono righe a prezzo zero. Vuoi continuare a stampare lo scontrino?", function(a) {

                                if (a === true) {
                                    console.log("PROMISE: AVVISO BROVEDANI RESOLVE TRUE");
                                    resolve(true);
                                } else {
                                    console.log("PROMISE: AVVISO BROVEDANI RESOLVE FALSE");
                                    resolve(false);
                                }

                            });

                        } else {
                            console.log("PROMISE: NO AVVISO BROVEDANI RESOLVE TRUE");
                            resolve(true);
                        }

                    } else if (passa_funzione === "ERR_TOTALI") {
                        bootbox.alert("Il totale delle componenti non corrisponde al valore della promozione." +
                            "<br><br>Totale dovuto: &euro; " + parseFloat(buffer_nodo_princ[nodo].totale_chiamante).toFixed(2) +
                            "<br>Totale inserito: &euro; " + parseFloat(buffer_nodo_princ[nodo].totale_varianti).toFixed(2));
                    } else if (passa_funzione === "ERR_SCONTO_PROMO") {
                        bootbox.alert("Non si possono effettuare sconti su una promozione.");
                    } else {
                        console.log("PROMISE: PASSA FUNZIONE FALSE " + passa_funzione);
                    }


                });

                console.log("PROMISE: ISTRUZIONE FINE QUERY IN PROMISE");

            });

            promessa.then(
                // Scrivi un log con un messaggio e un valore
                function(val) {

                    console.log("PROMISE RESOLVED: " + val);

                    var nome_cliente = "";

                    if (val === true) {

                        test_copertura(function(stato_copertura) {

                            var query = "select * from settaggi_profili where id=" + comanda.folder_number + ";";
                            comanda.sincro.query(query, function(settaggi_profili) {
                                settaggi_profili = settaggi_profili[0];

                                if (stato_copertura === true) {

                                    if (solo_riepiloghi !== true && saltafaseparcheggio !== true && (numero_tavolo.indexOf("CONTINUO") !== -1 || numero_tavolo === "BANCO" || numero_tavolo === "BAR" || numero_tavolo.left(7) === "ASPORTO" || numero_tavolo === "TAKE AWAY")) {
                                        comanda.stampato = false;
                                        btn_parcheggia();
                                    } else {
                                        comanda.stampato = true;
                                        console.log("stampa_comanda asporto", comanda.stampato, saltafaseparcheggio, numero_tavolo, comanda.parcheggio);
                                        nome_cliente = comanda.parcheggio;
                                        if (bool_ristampa !== true && $("#conto tr").not('.comanda').not('.non-contare-riga').length === 0) {

                                            if (disabilita_avviso !== true && numero_tavolo !== 'BAR' && numero_tavolo.indexOf('CONTINUO') === -1) {
                                                if (solo_servito !== true && $("#conto tr").not('.comanda').not('.non-contare-riga').length === 0 && $("#conto tr.comanda").not('.non-contare-riga').length > 0) {



                                                    bootbox.confirm({

                                                        message: "Non hai nuovi articoli da lanciare in comanda. Vuoi ristampare la comanda vecchia?",
                                                        buttons: {
                                                            cancel: {
                                                                label: '<i class="fa fa-times"></i> No'
                                                            },
                                                            confirm: {
                                                                label: '<i class="fa fa-check"></i> Si'
                                                            }
                                                        },
                                                        callback: function(risp) {
                                                            if (risp === true) {
                                                                stampa_comanda(undefined, undefined, undefined, undefined, undefined, true);
                                                            } else {
                                                                btn_tavoli();
                                                            }
                                                        }
                                                    });


                                                } else {
                                                    btn_tavoli();
                                                }
                                            } else if (solo_riepiloghi === true && numero_tavolo.indexOf("CONTINUO") !== -1) {
                                                comanda.funzionidb.conto_attivo(function() {
                                                    //seleziona_metodo_pagamento('SCONTRINO FISCALE');
                                                    stampa_scontrino('SCONTRINO FISCALE', undefined, undefined);
                                                });
                                            }

                                        } else {
                                            var testo_query = "select id,ricetta from prodotti;";
                                            console.log("STAMPA COMANDA", testo_query);

                                            comanda.sincro.query(testo_query, function(result_prodotti) {

                                                var corrispondenza_id_prodotti = new Object();

                                                result_prodotti.forEach(function(element) {

                                                    corrispondenza_id_prodotti[element.id] = element.ricetta;

                                                });





                                                var query_base = "SELECT contiene_variante,cod_promo,cod_articolo,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,dest_stampa_2,portata,categoria,prog_inser,nodo,desc_art,tasto_segue,prezzo_un,quantita FROM comanda WHERE ntav_comanda='" + numero_tavolo + "' and posizione='CONTO' and stato_record='ATTIVO'";

                                                if (solo_riepiloghi !== true) {
                                                    if (bool_ristampa === true) {

                                                    } else {
                                                        query_base += " and (stampata_sn is null or stampata_sn='null' or stampata_sn='N' or stampata_sn='') ";
                                                    }
                                                } else {
                                                    query_base += " and (stampata_sn is null or stampata_sn='null' or stampata_sn='N' or stampata_sn='') and riepilogo='S' ";
                                                }






                                                var prom = new Promise(function(resolve, reject) {



                                                    if (settaggi_profili.Field9 === 'true') {
                                                        dati_comanda(true, function(res) {
                                                            resolve(res);
                                                        });
                                                    } else {

                                                        var r = alasql(query_base);


                                                        var obj_risultati_group = new Object();
                                                        var array_risultati_preorder = new Array();

                                                        r.forEach(function(attivo) {

                                                            var ord = "";

                                                            if (attivo.contiene_variante === '1') {

                                                                ord = attivo.desc_art.substr(0, 3) + 'M' + attivo.nodo.substr(0, 3);
                                                                attivo.ord = ord;
                                                                array_risultati_preorder.push(attivo);
                                                            } else if (attivo.nodo.length === 7) {

                                                                var g = alasql("select substr(desc_art,1,3) as desc_art from comanda where nodo='" + attivo.nodo.substr(0, 3) + "' and stato_record='ATTIVO'  and ntav_comanda='" + numero_tavolo + "' limit 1;");
                                                                ord = g[0].desc_art.substr(0, 3) + 'M' + attivo.nodo;
                                                                attivo.ord = ord;
                                                                array_risultati_preorder.push(attivo);
                                                            } else if (attivo.contiene_variante !== '1' && attivo.nodo.length === 3) {
                                                                ord = attivo.desc_art.substr(0, 3) + 'M' + attivo.nodo.substr(0, 3);

                                                                if (obj_risultati_group[attivo.tasto_segue] === undefined) {
                                                                    obj_risultati_group[attivo.tasto_segue] = new Object();
                                                                }

                                                                if (obj_risultati_group[attivo.tasto_segue][attivo.desc_art] === undefined) {
                                                                    obj_risultati_group[attivo.tasto_segue][attivo.desc_art] = new Object();
                                                                }

                                                                if (obj_risultati_group[attivo.tasto_segue][attivo.desc_art][attivo.nome_comanda] === undefined) {
                                                                    attivo.quantita = parseInt(attivo.quantita);
                                                                    obj_risultati_group[attivo.tasto_segue][attivo.desc_art][attivo.nome_comanda] = attivo;
                                                                } else {
                                                                    obj_risultati_group[attivo.tasto_segue][attivo.desc_art][attivo.nome_comanda].quantita += parseInt(attivo.quantita);
                                                                }

                                                                /*if (obj_risultati_group[attivo.nome_comanda] === undefined) {
                                                                 obj_risultati_group[attivo.nome_comanda] = new Object();
                                                                 }
                                                                 if (obj_risultati_group[attivo.nome_comanda][ord] === undefined) {
                                                                 attivo.ord = ord;
                                                                 attivo.quantita = parseInt(attivo.quantita);
                                                                 obj_risultati_group[attivo.nome_comanda][ord] = attivo;
                                                                 } else {
                                                                 obj_risultati_group[attivo.nome_comanda][ord].quantita += parseInt(attivo.quantita);
                                                                 }*/
                                                            }



                                                        });

                                                        for (var tasto_segue in obj_risultati_group) {
                                                            for (var desc_art in obj_risultati_group[tasto_segue]) {
                                                                for (var nome_comanda in obj_risultati_group[tasto_segue][desc_art]) {
                                                                    array_risultati_preorder.push(obj_risultati_group[tasto_segue][desc_art][nome_comanda]);
                                                                }
                                                            }
                                                        }


                                                        /*for (var nome_comanda in obj_risultati_group) {
                                                         for (var ord in obj_risultati_group[nome_comanda])
                                                         array_risultati_preorder.push(obj_risultati_group[nome_comanda][ord]);
                                                         }*/

                                                        var res = alasql("select * FROM ? ORDER BY nome_comanda DESC, ord ASC;", [array_risultati_preorder]);

                                                        console.log(array_risultati_preorder);

                                                        resolve(res);

                                                    }
                                                });
                                                var array_comanda = new Array();
                                                var array_dest_stampa = new Array();
                                                var stampaa_dest = new Array();
                                                var array_comandaa = new Array();
                                                var i = 0;

                                                prom.then(function(result) {



                                                    if (result.length > 0) {
                                                        fa_richiesta_progressivo(function(n_prog) {

                                                            console.log("RISULTATI COMANDA", result);
                                                            var stampato = false;
                                                            try {
                                                                var progressivo = 0;
                                                                result.forEach(function(obj) {

                                                                    try {
                                                                        if (obj['cod_promo'] === "V_1") {
                                                                            var descrizione = obj['desc_art'].replace(/&#37;/gi, "%").replace(/&#8364;/gi, "�").replace(/=/gi, "...");
                                                                        } else {
                                                                            var descrizione = obj['desc_art'].replace(/&#37;/gi, "%").replace(/&#8364;/gi, "�");
                                                                        }
                                                                        var prezzo = obj['prezzo_un'];
                                                                        var nodo = obj['nodo'];
                                                                        var quantita = obj['quantita'];
                                                                        var prog_inser = obj['prog_inser'];

                                                                        var cod_articolo = obj['cod_articolo'];
                                                                        if (cod_articolo !== undefined && cod_articolo !== 'null' && cod_articolo.length > 0) {
                                                                            console.log("ricette", corrispondenza_id_prodotti, cod_articolo);
                                                                            var ricetta = corrispondenza_id_prodotti[cod_articolo];
                                                                        }

                                                                        var dest_stampa = obj['dest_stampa'];
                                                                        var portata = obj['portata'];

                                                                        var codice_ordinamento = portata;
                                                                        if (codice_ordinamento_stampa(portata) !== "") {
                                                                            codice_ordinamento = codice_ordinamento_stampa(portata);
                                                                        }

                                                                        var tasto_segue = obj['tasto_segue'];
                                                                        var categoria = obj['categoria'];

                                                                        if (dest_stampa === "T") {
                                                                            //CICLO STAMPANTI
                                                                            comanda.nome_stampante.forEach(function(value, index) {

                                                                                if (value.fiscale === "n") {

                                                                                    if (array_comanda[index] === undefined) {
                                                                                        array_comanda[index] = new Object();
                                                                                        array_comandaa[index] = new Object();
                                                                                    }

                                                                                    if (array_dest_stampa[index] === undefined) {
                                                                                        array_dest_stampa[index] = new Object();
                                                                                        array_dest_stampa[index].val = comanda.nome_stampante[index].nome;
                                                                                        array_dest_stampa[index].dimensione = comanda.nome_stampante[index].dimensione;
                                                                                        array_dest_stampa[index].ip = comanda.nome_stampante[index].ip;
                                                                                        array_dest_stampa[index].ip_alternativo = comanda.nome_stampante[index].ip_alternativo;
                                                                                        array_dest_stampa[index].intelligent = comanda.nome_stampante[index].intelligent;
                                                                                        array_dest_stampa[index].fiscale = comanda.nome_stampante[index].fiscale;
                                                                                        array_dest_stampa[index].nome_umano = comanda.nome_stampante[index].nome_umano;
                                                                                        array_dest_stampa[index].devid_nf = comanda.nome_stampante[index].devid_nf;
                                                                                        array_dest_stampa[index].devicetype = comanda.nome_stampante[index].devicetype;
                                                                                        array_dest_stampa[index].devicemodel = comanda.nome_stampante[index].devicemodel;
                                                                                    }

                                                                                    if (array_comanda[index][tasto_segue] === undefined) {
                                                                                        array_comanda[index][tasto_segue] = new Object();
                                                                                    }

                                                                                    if (array_comanda[index][tasto_segue][portata] === undefined) {
                                                                                        array_comanda[index][tasto_segue][portata] = new Object();
                                                                                        array_comanda[index][tasto_segue][portata].val = comanda.nome_portata[portata];
                                                                                    }


                                                                                    if (array_comanda[index][tasto_segue][portata][(progressivo + 1)] === undefined) {
                                                                                        array_comanda[index][tasto_segue][portata][(progressivo + 1)] = [quantita, descrizione, prezzo, ricetta];

                                                                                    }



                                                                                    //_____________________________________
                                                                                    if (array_comandaa[index] === undefined) {
                                                                                        array_comandaa[index] = new Object();
                                                                                    }

                                                                                    if (array_comandaa[index][portata] === undefined) {
                                                                                        array_comandaa[index][portata] = new Object();
                                                                                        array_comandaa[index][portata].val = comanda.nome_portata[portata];
                                                                                    }


                                                                                    if (array_comandaa[index][portata][(progressivo + 1)] === undefined) {
                                                                                        array_comandaa[index][portata][(progressivo + 1)] = [quantita, descrizione, prezzo, ricetta];

                                                                                    }
                                                                                    progressivo++;

                                                                                }
                                                                            });


                                                                        } else {
                                                                            if (array_comanda[dest_stampa] === undefined) {
                                                                                array_comanda[dest_stampa] = new Object();
                                                                                array_comandaa[dest_stampa] = new Object();
                                                                            }

                                                                            if (array_dest_stampa[dest_stampa] === undefined) {
                                                                                array_dest_stampa[dest_stampa] = new Object();
                                                                                array_dest_stampa[dest_stampa].val = comanda.nome_stampante[dest_stampa].nome;
                                                                                array_dest_stampa[dest_stampa].dimensione = comanda.nome_stampante[dest_stampa].dimensione;
                                                                                array_dest_stampa[dest_stampa].ip = comanda.nome_stampante[dest_stampa].ip;
                                                                                array_dest_stampa[dest_stampa].ip_alternativo = comanda.nome_stampante[dest_stampa].ip_alternativo;
                                                                                array_dest_stampa[dest_stampa].intelligent = comanda.nome_stampante[dest_stampa].intelligent;
                                                                                array_dest_stampa[dest_stampa].fiscale = comanda.nome_stampante[dest_stampa].fiscale;
                                                                                array_dest_stampa[dest_stampa].nome_umano = comanda.nome_stampante[dest_stampa].nome_umano;
                                                                                array_dest_stampa[dest_stampa].devid_nf = comanda.nome_stampante[dest_stampa].devid_nf;
                                                                                array_dest_stampa[dest_stampa].devicetype = comanda.nome_stampante[dest_stampa].devicetype;
                                                                                array_dest_stampa[dest_stampa].devicemodel = comanda.nome_stampante[dest_stampa].devicemodel;
                                                                            }

                                                                            if (array_comanda[dest_stampa][tasto_segue] === undefined) {
                                                                                array_comanda[dest_stampa][tasto_segue] = new Object();
                                                                            }

                                                                            /*if (array_comanda[dest_stampa][tasto_segue][portata] === undefined) {
                                                                                array_comanda[dest_stampa][tasto_segue][portata] = new Object();
                                                                                array_comanda[dest_stampa][tasto_segue][portata].val = comanda.nome_portata[portata];
                                                                            }


                                                                            if (array_comanda[dest_stampa][tasto_segue][portata][(progressivo + 1)] === undefined) {
                                                                                array_comanda[dest_stampa][tasto_segue][portata][(progressivo + 1)] = [quantita, descrizione, prezzo, ricetta];
                                                                            }*/


                                                                            if (array_comanda[dest_stampa][tasto_segue][codice_ordinamento] === undefined) {
                                                                                array_comanda[dest_stampa][tasto_segue][codice_ordinamento] = new Object();
                                                                                array_comanda[dest_stampa][tasto_segue][codice_ordinamento].val = comanda.nome_portata[obj['portata']];
                                                                                array_comanda[dest_stampa][tasto_segue][codice_ordinamento].portata = portata;
                                                                            }

                                                                            if (array_comanda[dest_stampa][tasto_segue][codice_ordinamento][(progressivo + 1)] === undefined) {
                                                                                array_comanda[dest_stampa][tasto_segue][codice_ordinamento][(progressivo + 1)] = [quantita, descrizione, prezzo, ricetta, categoria];
                                                                            }

                                                                            //______________________________________________________


                                                                            if (array_comandaa[dest_stampa] === undefined) {
                                                                                array_comandaa[dest_stampa] = new Object();

                                                                            }



                                                                            if (array_comandaa[dest_stampa][codice_ordinamento] === undefined) {
                                                                                array_comandaa[dest_stampa][codice_ordinamento] = new Object();
                                                                                array_comandaa[dest_stampa][codice_ordinamento].val = comanda.nome_portata[obj['portata']];
                                                                                array_comandaa[dest_stampa][codice_ordinamento].portata = portata;
                                                                            }

                                                                            if (array_comandaa[dest_stampa][codice_ordinamento][(progressivo + 1)] === undefined) {
                                                                                array_comandaa[dest_stampa][codice_ordinamento][(progressivo + 1)] = [quantita, descrizione, prezzo, ricetta, categoria, "N", cod_articolo, prog_inser];
                                                                            }

                                                                            progressivo++;


                                                                        }


                                                                        //DESTINAZIONE STAMPA 2

                                                                        if (obj['cod_promo'] === "V_1") {
                                                                            var descrizione = obj['desc_art'].replace(/&#37;/gi, "%").replace(/&#8364;/gi, "�").replace(/=/gi, "...");
                                                                        } else {
                                                                            var descrizione = obj['desc_art'].replace(/&#37;/gi, "%").replace(/&#8364;/gi, "�");
                                                                        }
                                                                        var prezzo = obj['prezzo_un'];
                                                                        var nodo = obj['nodo'];
                                                                        var quantita = obj['quantita'];

                                                                        var cod_articolo = obj['cod_articolo'];
                                                                        if (cod_articolo !== undefined && cod_articolo !== 'null' && cod_articolo.length > 0) {
                                                                            console.log("ricette", corrispondenza_id_prodotti, cod_articolo);
                                                                            var ricetta = corrispondenza_id_prodotti[cod_articolo];
                                                                        }

                                                                        var dest_stampa = obj['dest_stampa_2'];
                                                                        var stampaa = obj['dest_stampa'];
                                                                        var portata = obj['portata'];

                                                                        var codice_ordinamento = portata;
                                                                        if (codice_ordinamento_stampa(portata) !== "") {
                                                                            codice_ordinamento = codice_ordinamento_stampa(portata);
                                                                        }


                                                                        var tasto_segue = obj['tasto_segue'];
                                                                        var categoria = obj['categoria'];
                                                                        stampaa_dest[i] = { 'dest_stampa': stampaa };
                                                                        i++;

                                                                        //Aggiunto !==undefined 25/09/2019
                                                                        if (dest_stampa !== undefined && dest_stampa !== null && dest_stampa !== "" && dest_stampa !== "undefined" && dest_stampa !== "null" && dest_stampa !== obj['dest_stampa'] && obj['dest_stampa'] !== "T") {


                                                                            if (array_comanda[dest_stampa] === undefined) {
                                                                                array_comanda[dest_stampa] = new Object();
                                                                                array_comandaa[dest_stampa] = new Object();
                                                                            }

                                                                            if (array_dest_stampa[dest_stampa] === undefined) {
                                                                                array_dest_stampa[dest_stampa] = new Object();
                                                                                array_dest_stampa[dest_stampa].val = comanda.nome_stampante[dest_stampa].nome;
                                                                                array_dest_stampa[dest_stampa].dimensione = comanda.nome_stampante[dest_stampa].dimensione;
                                                                                array_dest_stampa[dest_stampa].ip = comanda.nome_stampante[dest_stampa].ip;
                                                                                array_dest_stampa[dest_stampa].ip_alternativo = comanda.nome_stampante[dest_stampa].ip_alternativo;
                                                                                array_dest_stampa[dest_stampa].intelligent = comanda.nome_stampante[dest_stampa].intelligent;
                                                                                array_dest_stampa[dest_stampa].fiscale = comanda.nome_stampante[dest_stampa].fiscale;
                                                                                array_dest_stampa[dest_stampa].nome_umano = comanda.nome_stampante[dest_stampa].nome_umano;
                                                                                array_dest_stampa[dest_stampa].devid_nf = comanda.nome_stampante[dest_stampa].devid_nf;
                                                                                array_dest_stampa[dest_stampa].devicetype = comanda.nome_stampante[dest_stampa].devicetype;
                                                                                array_dest_stampa[dest_stampa].devicemodel = comanda.nome_stampante[dest_stampa].devicemodel;
                                                                            }

                                                                            if (array_comanda[dest_stampa][tasto_segue] === undefined) {
                                                                                array_comanda[dest_stampa][tasto_segue] = new Object();
                                                                            }




                                                                            if (array_comanda[dest_stampa][tasto_segue][codice_ordinamento] === undefined) {
                                                                                array_comanda[dest_stampa][tasto_segue][codice_ordinamento] = new Object();
                                                                                array_comanda[dest_stampa][tasto_segue][codice_ordinamento].val = comanda.nome_portata[obj['portata']];
                                                                                array_comanda[dest_stampa][tasto_segue][codice_ordinamento].portata = portata;
                                                                            }

                                                                            if (array_comanda[dest_stampa][tasto_segue][codice_ordinamento][(progressivo + 1)] === undefined) {
                                                                                array_comanda[dest_stampa][tasto_segue][codice_ordinamento][(progressivo + 1)] = [quantita, descrizione, prezzo, ricetta, categoria];
                                                                            }



                                                                            //__________________________________

                                                                            if (array_comandaa[dest_stampa][tasto_segue] === undefined) {
                                                                                array_comandaa[dest_stampa][tasto_segue] = new Object();
                                                                            }




                                                                            if (array_comandaa[dest_stampa][tasto_segue][codice_ordinamento] === undefined) {
                                                                                array_comandaa[dest_stampa][tasto_segue][codice_ordinamento] = new Object();
                                                                                array_comandaa[dest_stampa][tasto_segue][codice_ordinamento].val = comanda.nome_portata[obj['portata']];
                                                                                array_comandaa[dest_stampa][tasto_segue][codice_ordinamento].portata = portata;
                                                                            }

                                                                            if (array_comandaa[dest_stampa][tasto_segue][codice_ordinamento][(progressivo + 1)] === undefined) {
                                                                                array_comandaa[dest_stampa][codice_ordinamento][(progressivo + 1)] = [quantita, descrizione, prezzo, ricetta, categoria, "D", cod_articolo, prog_inser];
                                                                            }

                                                                            progressivo++;

                                                                        }
                                                                    } catch (e) {

                                                                        throw descrizione;


                                                                    }
                                                                });
                                                                //---COMANDA INTELLIGENT---//

                                                            } catch (e) {
                                                                stampato = true;

                                                                if (solo_servito === true) {
                                                                    stampa_comanda_fase_2(array_comanda, array_comandaa, array_dest_stampa, settaggi_profili, n_prog, nome_cliente);

                                                                } else {
                                                                    bootbox.confirm("L'articolo ''" + e + "'' ha un problema con la destinazione di stampa.<br>Ti consigliamo di cambiare la destinazione prima di stampare la comanda, altrimenti l'articolo non verr&agrave; stampato.<br><br>Vuoi stampare la comanda ugualmente?", function(r) {
                                                                        if (r === false) {
                                                                            stampato = true;
                                                                        } else {

                                                                            stampato = true;

                                                                            stampa_comanda_fase_2(array_comanda, array_comandaa, array_dest_stampa, settaggi_profili, n_prog, nome_cliente);
                                                                        }
                                                                    });
                                                                }
                                                            } finally {
                                                                for (var num in stampaa_dest) {
                                                                    obj_art = stampaa_dest[0];
                                                                }

                                                                if (stampato === false) {
                                                                    stampato = true;













                                                                    stampa_comanda_fase_2(array_comanda, array_comandaa, array_dest_stampa, settaggi_profili, n_prog, nome_cliente);
                                                                }

                                                            }
                                                        });
                                                    } //FINE CONDIZIONE DI STAMPA COMANDA
                                                    else if (solo_riepiloghi === true && numero_tavolo.indexOf("CONTINUO") !== -1) {
                                                        comanda.funzionidb.conto_attivo(function() {
                                                            //seleziona_metodo_pagamento('SCONTRINO FISCALE');
                                                            stampa_scontrino('SCONTRINO FISCALE', undefined, undefined);
                                                        });
                                                    }

                                                });
                                            });
                                        }

                                        //});
                                    }
                                }
                            });
                        });
                    }
                });


            function stampa_comanda_fase_2(array_comanda, array_comandaa, array_dest_stampa, settaggi_profili, n_prog, nome_cliente) {
                let oggetto_concomitanze = null;
                let testo_concomitanze = '';
                let concomitanze = '';




                for (var dest_stampa in array_comanda) {
                    oggetto_concomitanze = funzione_concomitanze(array_comandaa, dest_stampa, array_dest_stampa);

                    concomitanze = oggetto_concomitanze['concomitanze'];
                    testo_concomitanze = oggetto_concomitanze['testo_concomitanze'];

                    switch (array_dest_stampa[dest_stampa].intelligent) {


                        case "N":
                        case "n":
                            console.log("STAMPA COMANDA NORMALE", array_dest_stampa[dest_stampa].ip);
                            //---COMANDA NON INTELLIGENT---//

                            var corpo;
                            corpo = '<epos-print>\n\
     <text  dw="1" dh="1" description="' + comanda.lang_stampa[11] + '" />\n\
     <feed  type="1" />\n\
     <feed  type="1" />';
                            corpo += '<text  dw="1" dh="1" description="' + comanda.lang_stampa[68].toUpperCase() + ': ' + numero_tavolo + '" />\n\
     <feed  type="1" />\n\
     <feed  type="1" />';
                            corpo += '<text  dw="0" dh="0" description="--------------------------------------" />\n\
     <feed  type="1" />\n\
     <feed  type="1" />';
                            corpo += '<text  dw="0" dh="0" description="' + comanda.lang_stampa[109] + ': ' + comanda.operatore + '" />\n\
     <feed  type="1" />\n\
     <feed  type="1" />';
                            corpo += '<text  dw="0" dh="0" description="' + comanda.lang_stampa[110] + ': ' + comanda.funzionidb.data_attuale() + '" />\n\
     <feed  type="1" />\n\
     <feed  type="1" />';
                            corpo += '<text  dw="0" dh="0" description="' + comanda.lang_stampa[111] + ': [ ' + array_dest_stampa[dest_stampa].nome_umano + ' ]" />\n\
     <feed  type="1" />\n\
     <feed  type="1" />';
                            corpo += '<text  dw="0" dh="0" description="--------------------------------------" />\n\
     <feed  type="1" />\n\
     <feed  type="1" />';


                            corpo += '<text  dw="0" dh="0" description="- - - ' + concomitanze + ' - - -" />\n\
     <feed  type="1" />\n\
     <feed  type="1" />';


                            for (var dest_stampa_concomitanza in array_comanda) {

                                corpo += '<text  dw="0" dh="0" description="-------------CONCOMITANZE-------------" />\n';
                                corpo += '<text  dw="0" dh="0" description="' + comanda.lang_stampa[111] + ': [ ' + array_dest_stampa[dest_stampa_concomitanza].nome_umano + ' ]" />\n';
                                if (dest_stampa_concomitanza !== dest_stampa) {

                                    corpo += '<text  dw="0" dh="0" description="- - - ' + array_comanda[dest_stampa][portata].val + ' - - -" />\n\
     <feed  type="1" />\n\
     <feed  type="1" />';
                                    for (var node_concomitanza in array_comanda[dest_stampa_concomitanza][portata]) {

                                        var nodo = array_comanda[dest_stampa_concomitanza][portata][node_concomitanza];
                                        if (nodo !== undefined && nodo[1] !== undefined && nodo[1].length > 1) {

                                            var articolo = filtra_accenti(nodo[1]);
                                            var quantita = nodo[0];
                                            if (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(" && articolo[0] !== "=" && articolo[0] !== "." && articolo[0] !== "*") {
                                                totale_quantita = parseInt(quantita) + totale_quantita;
                                                corpo += '  <text  dw="0" dh="0" description="' + quantita + ' ' + articolo + '" />\n\
     <feed  type="1" />\n\
     <feed  type="1" />';
                                            } else {

                                                corpo += '  <text  dw="0" dh="0" description="' + articolo + '" />\n\
     <feed  type="1" />\n\
     <feed  type="1" />';
                                            }
                                        }
                                    }
                                }


                            }

                            corpo += '  <text  dw="0" dh="0" description="' + totale_quantita + '" />\n\
     <feed  type="1" />\n\
     <feed  type="1" />';
                            corpo += '  <feed  type="1" />\n\
     <feed  type="1" />\n\
     <feed  type="1" />\n\
     <feed  type="1" />\n\
     <feed  type="1" />\n\
     <cut  type="feed" />\n\
    <pulse type="drawer" />\n\
     </epos-print>';
                            //CAMBIANDO QUESTO PARAMETRO CAMBIA L'IP DI STAMPA (quindi si potrebbe prendere da comanda.nome_stampante[num].ip
                            //SI POTREBBE TESTARE LA DIMENSIONE CON comanda.nome_stampante[num].dimensione
                            //E SE E' O NO FISCALE CON IL PARAMETRO comanda.nome_stampante[num].fiscale
                            //test.send(window.location.protocol + "//" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=" + deviceid, corpo, 10000);

                            corpo = '<?xml version="1.0" encoding="utf-8"?>\n' +
                                '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">\n' +
                                '<s:Body>\n' + corpo +
                                '</s:Body>\n' +
                                '</s:Envelope>\n';
                            console.log("CORPO", corpo);

                            aggiungi_spool_stampa(window.location.protocol + "//" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=" + array_dest_stampa[dest_stampa].devid_nf + "&timeout=30000", corpo, array_dest_stampa[dest_stampa].nome_umano);
                            //---FINE COMANDA NON INTELLIGENT---//

                            break;
                        case "S":
                        case "s":
                        case "SDS":
                            if (array_dest_stampa[dest_stampa].fiscale.toUpperCase() === 'N') {

                                var concomitanze_presenti = false;
                                console.log("STAMPA COMANDA INTELLIGENT", array_dest_stampa[dest_stampa].ip);
                                var com = '';
                                var data = comanda.funzionidb.data_attuale();
                                var builder = new epson.ePOSBuilder();
                                var messaggio_finale = new epson.ePOSBuilder();

                                var parola_conto = comanda.lang_stampa[11];
                                builder.addTextFont(builder.FONT_A);
                                builder.addTextAlign(builder.ALIGN_CENTER);

                                if (settaggi_profili.Field34.trim().length > 0) {
                                    switch (settaggi_profili.Field35) {
                                        case "G":
                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                            break;
                                        case "N":
                                        default:
                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                    }
                                    builder.addText(settaggi_profili.Field34 + '\n');
                                    if (settaggi_profili.Field36 === 'true') {
                                        builder.addFeedLine(1);
                                    }
                                }


                                if (settaggi_profili.Field37.trim().length > 0) {
                                    switch (settaggi_profili.Field38) {
                                        case "G":
                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                            break;
                                        case "N":
                                        default:
                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                    }
                                    builder.addText(settaggi_profili.Field37 + '\n');
                                    if (settaggi_profili.Field39 === 'true') {
                                        builder.addFeedLine(1);
                                    }
                                }


                                if (settaggi_profili.Field40.trim().length > 0) {
                                    switch (settaggi_profili.Field41) {
                                        case "G":
                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                            break;
                                        case "N":
                                        default:
                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                    }
                                    builder.addText(settaggi_profili.Field40 + '\n');
                                    if (settaggi_profili.Field42 === 'true') {
                                        builder.addFeedLine(1);
                                    }
                                }


                                if (settaggi_profili.Field43.trim().length > 0) {
                                    switch (settaggi_profili.Field44) {
                                        case "G":
                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                            break;
                                        case "N":
                                        default:
                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                    }
                                    builder.addText(settaggi_profili.Field43 + '\n');
                                }

                                builder.addText('\n\n');


                                builder.addTextSize(3, 3);

                                if (numero_tavolo.left(8) !== "CONTINUO" && numero_tavolo !== "BANCO" && numero_tavolo !== "BAR" && numero_tavolo.left(7) !== "ASPORTO" && numero_tavolo !== "TAKE AWAY") {


                                    if (settaggi_profili.Field3 === 'true') {
                                        builder.addText(parola_conto + ' - ');
                                    }

                                    if (solo_riepiloghi === true && numero_tavolo.indexOf("CONTINUO") !== -1) {
                                        builder.addText('COMANDA ' + n_prog + '\n');
                                    } else {

                                        builder.addText('Tavolo: ' + numero_tavolo + '\n');
                                    }


                                    builder.addTextFont(builder.FONT_A);
                                    builder.addFeedLine(1);
                                    if (settaggi_profili.Field4 === 'true') {
                                        var scritta_ristampa = "";
                                        if (bool_ristampa === true) {
                                            scritta_ristampa = 'RISTAMPA ';
                                        }
                                        builder.addText(scritta_ristampa + array_dest_stampa[dest_stampa].nome_umano + '\n');
                                        builder.addFeedLine(1);
                                    }
                                } else if (numero_tavolo === "BANCO" || numero_tavolo.left(7) === "ASPORTO" || numero_tavolo === "TAKE AWAY") {
                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                    //var numeretto = $('#popup_scelta_cliente input[name="numeretto"]').val();
                                    //builder.addText('' + numeretto + '\n');

                                    builder.addText('ASPORTO\n');

                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                    builder.addFeedLine(1);
                                } else if (numero_tavolo === "BAR" || numero_tavolo.left(8) === "CONTINUO") {
                                    if (settaggi_profili.Field3 === 'true') {
                                        builder.addText(parola_conto + ' - ');
                                    }

                                    builder.addText(comanda.parcheggio + '\n');
                                    builder.addTextFont(builder.FONT_A);
                                    builder.addFeedLine(1);
                                    if (settaggi_profili.Field4 === 'true') {
                                        var scritta_ristampa = "";
                                        if (bool_ristampa === true) {
                                            scritta_ristampa = 'RISTAMPA ';
                                        }
                                        builder.addText(scritta_ristampa + array_dest_stampa[dest_stampa].nome_umano + '\n');
                                        builder.addFeedLine(1);
                                    }
                                }

                                builder.addTextAlign(builder.ALIGN_LEFT);
                                if (numero_tavolo !== "BANCO" && numero_tavolo.left(7) !== "ASPORTO" && numero_tavolo !== "TAKE AWAY") {
                                    //QUI PRIMA C'ERA IL TAVOLO
                                } else {
                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                    builder.addText(nome_cliente + '\n');
                                    builder.addTextAlign(builder.ALIGN_LEFT);
                                }

                                if (numero_tavolo === "BANCO" || numero_tavolo.left(7) === "ASPORTO" || numero_tavolo === "TAKE AWAY") {


                                    var s = alasql("select  sum(cast(prog_inser as int)) as p from comanda where stato_record='ATTIVO' and ntav_comanda='" + numero_tavolo + "';");



                                    if (cambio_orario_consegna === true) {

                                        comanda.sincro.query_cassa();

                                        builder.addFeedLine(1);
                                        builder.addTextAlign(builder.ALIGN_CENTER);
                                        builder.addText('CAMBIO ORA CONSEGNA!\n');
                                        builder.addTextAlign(builder.ALIGN_LEFT);
                                    }

                                    /* lasciare != */
                                    if (comanda.somma_progressivi != 0 && s[0].p != comanda.somma_progressivi) {
                                        builder.addFeedLine(1);
                                        builder.addTextAlign(builder.ALIGN_CENTER);
                                        builder.addText('CAMBIO ORDINAZIONE!\n');
                                        builder.addTextAlign(builder.ALIGN_LEFT);
                                    }

                                    if ($('#parcheggia_comanda input[name="ora_consegna"]').val().length > 0) {
                                        builder.addFeedLine(1);

                                        builder.addTextAlign(builder.ALIGN_CENTER);
                                        builder.addTextSize(2, 2);
                                        builder.addText('CONSEGNA ' + ' ' + aggZero($('#parcheggia_comanda input[name="ora_consegna"]').val(), 2) + ' : ' + aggZero($('#parcheggia_comanda input[name="minuti_consegna"]').val(), 2) + '\n');
                                        builder.addTextSize(1, 1);
                                        builder.addFeedLine(1);
                                        builder.addTextAlign(builder.ALIGN_LEFT);
                                    }
                                }


                                builder.addTextSize(1, 1);
                                builder.addFeedLine(1);
                                builder.addTextStyle(false, false, true, builder.COLOR_1);


                                if (settaggi_profili.Field5 === 'true') {
                                    switch (settaggi_profili.Field30) {
                                        case "G":
                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                            break;
                                        case "N":
                                        default:
                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                    }
                                    builder.addText(comanda.lang_stampa[109].toLowerCase().capitalize() + ': ' + comanda.operatore + '\n');
                                }

                                if (settaggi_profili.Field6 === 'true' || settaggi_profili.Field7 === 'true') {

                                    if (settaggi_profili.Field6 === 'true') {
                                        switch (settaggi_profili.Field31) {
                                            case "G":
                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                break;
                                            case "N":
                                            default:
                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                        }
                                        builder.addText(data.substr(0, 9).replace(/-/gi, '/') + '   ');
                                    }

                                    if (settaggi_profili.Field7 === 'true') {
                                        switch (settaggi_profili.Field32) {
                                            case "G":
                                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                break;
                                            case "N":
                                            default:
                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                        }
                                        builder.addText(data.substr(9, 5));
                                    }

                                    builder.addText('\n');
                                }

                                //COPERTI
                                if (settaggi_profili.Field24 === 'true' && numero_tavolo !== 'BAR') {

                                    builder.addText('\n');

                                    switch (settaggi_profili.Field33) {
                                        case "G":
                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                            break;
                                        case "N":
                                        default:
                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                    }


                                    builder.addText(comanda.lang_stampa[15].toLowerCase().capitalize() + ': ' + quantita_coperti + '\n');

                                }

                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                builder.addTextSize(1, 1);

                                var totale_quantita = 0;
                                var totale_prezzo = 0.00;
                                var giro_tasto_segue = 0;

                                var ordered = {};
                                Object.keys(array_comanda[dest_stampa]).sort().forEach(function(key) {
                                    ordered[key] = array_comanda[dest_stampa][key];
                                });

                                array_comanda[dest_stampa] = Object.keys(array_comanda[dest_stampa]).sort().reduce(
                                    (obj, key) => {
                                        obj[key] = array_comanda[dest_stampa][key];
                                        return obj;
                                    }, {}
                                );


                                for (var tasto_segue in ordered) {

                                    if (giro_tasto_segue > 0) {
                                        builder.addTextSize(2, 2);
                                        builder.addTextAlign(builder.ALIGN_CENTER);
                                        builder.addText("\nSEGUE\n");
                                        builder.addTextSize(1, 1);
                                    }
                                    giro_tasto_segue++;


                                    for (let codice_ordinamento of Object.keys(array_comanda[dest_stampa][tasto_segue]).sort()) {

                                        if (array_comanda[dest_stampa][tasto_segue][codice_ordinamento].val !== undefined) {

                                            /*for (var portata in Object.keys(array_comanda[dest_stampa][tasto_segue]).sort()) {*/

                                            /*var indice_portata = Object.keys(array_comanda[dest_stampa][tasto_segue]).sort()[portata];*/
                                            var nome_portata = array_comanda[dest_stampa][tasto_segue][codice_ordinamento].val;


                                            if (nome_portata !== undefined) {

                                                builder.addFeedLine(1);
                                                builder.addTextAlign(builder.ALIGN_CENTER);

                                                if (codice_ordinamento === "ZZZ") {
                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                    builder.addText(trattini("MESSAGGI"));
                                                } else if (settaggi_profili.Field8 === 'true') {
                                                    if (settaggi_profili.Field25 === 'G') {
                                                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                    }
                                                    builder.addText(trattini(nome_portata));
                                                } else {

                                                    builder.addText("-----------------------------------------\n");

                                                }
                                                builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                builder.addTextAlign(builder.ALIGN_LEFT);

                                                var indice = 0;
                                                for (var node in array_comanda[dest_stampa][tasto_segue][codice_ordinamento]) {

                                                    var nodo = array_comanda[dest_stampa][tasto_segue][codice_ordinamento][node];


                                                    console.log("nodo comanda", nodo);

                                                    if (nodo !== undefined && nodo[1] !== undefined && nodo[1].length > 1) {

                                                        var articolo = filtra_accenti(nodo[1]);
                                                        var quantita = nodo[0];
                                                        var prezzo = nodo[2];

                                                        if (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(" && articolo[0] !== "=" && articolo[0] !== "." && articolo[0] !== "*" && articolo[0] !== "x") {

                                                            switch (settaggi_profili.Field28) {
                                                                case "D":
                                                                    builder.addTextSize(2, 2);
                                                                    articolo = articolo.slice(0, 20);
                                                                    break;
                                                                case "G":
                                                                    builder.addTextSize(2, 2);
                                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                    articolo = articolo.slice(0, 40);
                                                                    break;
                                                                case "N":
                                                                default:
                                                                    builder.addTextSize(2, 2);
                                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                    articolo = articolo.slice(0, 40);
                                                            }
                                                            /*if (indice > 0) {
                                                             builder.addText('\n');
                                                             }*/
                                                            if (codice_ordinamento === "ZZZ") {
                                                                builder.addTextAlign(builder.ALIGN_CENTER);
                                                                builder.addText(articolo);
                                                                builder.addTextAlign(builder.ALIGN_LEFT);
                                                            } else {

                                                                builder.addText(quantita);
                                                                builder.addTextPosition(50);
                                                                totale_quantita += parseInt(quantita);
                                                                builder.addTextPosition(50);

                                                                builder.addText(articolo);

                                                                if (settaggi_profili.Field10 === 'true') {
                                                                    builder.addTextPosition(420);
                                                                    builder.addText('€ ' + prezzo);
                                                                }
                                                            }

                                                            builder.addText('\n');

                                                            builder.addTextSize(2, 2);


                                                        } else {

                                                            switch (settaggi_profili.Field29) {
                                                                case "D":
                                                                    builder.addTextSize(2, 2);
                                                                    break;
                                                                case "G":
                                                                    builder.addTextSize(2, 2);
                                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                    break;
                                                                case "N":
                                                                default:
                                                                    builder.addTextSize(2, 2);
                                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                            }




                                                            var articolo_a_capo = "";

                                                            var articolo_originale = articolo;

                                                            articolo_a_capo = articolo.replace(/\+/gi, "\n\t  +").replace(/\-/gi, "\n\t  -").replace(/\(/gi, "\n\t  (").replace(/\=/gi, "\n\t  =").replace(/\*/gi, "\n\t  *").replace(/\x/gi, "\n\t  x").replace(/\.\.\./gi, "\n\t  ...").replace("\n\t  ", "");

                                                            articolo_a_capo.split(/\\n\\t/gi).forEach(function(articolo) {

                                                                if (settaggi_profili.Field310 === 'true' && articolo[0] === "-") {
                                                                    builder.addTextStyle(true, false, false, builder.COLOR_1);
                                                                }

                                                                if (settaggi_profili.Field410 === 'true' && articolo[0] === "+") {
                                                                    builder.addTextStyle(true, false, false, builder.COLOR_1);
                                                                }

                                                                /*poco*/
                                                                if (settaggi_profili.Field422 === 'true' && articolo.indexOf("*POCO") !== -1) {
                                                                    builder.addTextStyle(true, false, false, builder.COLOR_1);
                                                                }

                                                                /*fine cottura*/
                                                                if (settaggi_profili.Field423 === 'true' && articolo.indexOf("xFINE COT.") !== -1) {
                                                                    builder.addTextStyle(true, false, false, builder.COLOR_1);
                                                                }

                                                                if (settaggi_profili.Field9 === 'true') {

                                                                    articolo = '\t' + quantita + ' ' + articolo_a_capo;

                                                                } else {

                                                                    articolo = '\t' + articolo_originale;
                                                                }

                                                                builder.addTextPosition(50);


                                                                builder.addText(articolo.slice(0, 40));

                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);

                                                                if (settaggi_profili.Field10 === 'true') {
                                                                    builder.addTextPosition(420);
                                                                    builder.addText('€ ' + prezzo);
                                                                }


                                                                builder.addText('\n');


                                                                builder.addTextSize(1, 1);
                                                                builder.addTextStyle(false, false, false, builder.COLOR_1);

                                                            });

                                                            /*var articolo_a_capo="";
                                                             var articolo_pulito="";
                                                             var lunghezza_articolo=0;
                                                             
                                                             if (settaggi_profili.Field9 === 'true') {
                                                             
                                                             articolo_pulito=articolo.replace(/\\t/gi,"").replace(/\\n/gi,"");
                                                             lunghezza_articolo=articolo_pulito.length;
                                                             
                                                             for(var i=17;i<lunghezza_articolo;i+=18){
                                                             
                                                             if(articolo_pulito[i]!==undefined){
                                                             articolo_pulito=articolo_pulito.insert(i,"\n\t ");
                                                             i+=5;
                                                             }
                                                             
                                                             }
                                                             
                                                             articolo = '\t' + quantita + ' ' + articolo_pulito;
                                                             } else {
                                                             
                                                             articolo_pulito=articolo.replace(/\\t/gi,"").replace(/\\n/gi,"");
                                                             lunghezza_articolo=articolo_pulito.length;
                                                             
                                                             for(var i=17;i<lunghezza_articolo;i+=18){
                                                             
                                                             if(articolo_pulito[i]!==undefined){
                                                             articolo_pulito=articolo_pulito.insert(i,"\n\t ");
                                                             i+=5;
                                                             }
                                                             }
                                                             
                                                             articolo = '\t' + articolo_pulito;
                                                             }*/






                                                        }

                                                        totale_prezzo += prezzo * quantita;



                                                        if (nodo[3] !== undefined && settaggi_profili.Field11 === 'true' && (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(" && articolo[0] !== "=" && articolo[0] !== "." && articolo[0] !== "*")) {

                                                            var ricetta = filtra_accenti(nodo[3]);
                                                            if (ricetta.length > 1) {
                                                                builder.addTextPosition(50);
                                                                builder.addTextSize(1, 1);
                                                                builder.addText('(' + ricetta + ') \n');
                                                            }

                                                        }
                                                    }
                                                    indice++;
                                                }




                                                builder.addTextAlign(builder.ALIGN_CENTER);
                                                if (settaggi_profili.Field22 === 'true') {

                                                    for (var dest_stampa_concomitanza in array_comanda) {


                                                        if (comanda.numero_licenza_cliente === "0664" || (parseInt(codice_ordinamento) >= 0 && parseInt(codice_ordinamento) <= 9)) {

                                                            if (array_dest_stampa[dest_stampa_concomitanza].nome_umano !== undefined && dest_stampa_concomitanza === dest_stampa && array_comanda[dest_stampa_concomitanza][tasto_segue][codice_ordinamento] !== undefined && array_comanda[dest_stampa_concomitanza][tasto_segue][codice_ordinamento] !== null && typeof(array_comanda[dest_stampa_concomitanza][tasto_segue][codice_ordinamento]) === "object" && Object.keys(array_comanda[dest_stampa_concomitanza][tasto_segue][codice_ordinamento]).length > 0) {

                                                                concomitanze_presenti = true;
                                                                concomitanze.addTextAlign(builder.ALIGN_CENTER);
                                                                concomitanze.addTextStyle(false, false, true, builder.COLOR_1);
                                                                concomitanze.addText('\n' + array_dest_stampa[dest_stampa_concomitanza].nome_umano + ' - ' + array_comanda[dest_stampa][tasto_segue][codice_ordinamento].val + '\n');
                                                                concomitanze.addTextStyle(false, false, false, builder.COLOR_1);
                                                                concomitanze.addTextAlign(builder.ALIGN_LEFT);

                                                                for (var node_concomitanza in array_comanda[dest_stampa_concomitanza][tasto_segue][codice_ordinamento]) {

                                                                    var nodo = array_comanda[dest_stampa_concomitanza][tasto_segue][codice_ordinamento][node_concomitanza];
                                                                    if (nodo !== undefined && nodo[1] !== undefined && nodo[1].length > 1) {

                                                                        var articolo = filtra_accenti(nodo[1]);
                                                                        if (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(" && articolo[0] !== "=" && articolo[0] !== "." && articolo[0] !== "*") {
                                                                            concomitanze.addText(quantita);
                                                                            concomitanze.addTextPosition(50);
                                                                        } else {
                                                                            articolo = ' \t' + articolo;
                                                                        }

                                                                        concomitanze.addTextPosition(50);
                                                                        concomitanze.addText(articolo.slice(0, 40) + '\n');
                                                                    }
                                                                }

                                                                concomitanze.addTextAlign(builder.ALIGN_LEFT);
                                                            }
                                                        }
                                                    }
                                                }
                                                builder.addTextSize(1, 1);
                                                builder.addText("--------------------------------------\n");
                                                builder.addTextAlign(builder.ALIGN_LEFT);



                                            }

                                        }

                                    } //FINE CICLO PORTATA
                                } //FINE CICLO TASTO SEGUE

                                //SETTAGGI DEL MESSAGGIO FINALE

                                if (settaggi_profili.Field14 === 'G')
                                    messaggio_finale.addTextStyle(false, false, true, builder.COLOR_1);
                                else if (settaggi_profili.Field14 === 'N')
                                    messaggio_finale.addTextStyle(false, false, false, builder.COLOR_1);

                                if (settaggi_profili.Field15 === 'x--')
                                    messaggio_finale.addTextAlign(builder.ALIGN_LEFT);
                                else if (settaggi_profili.Field15 === '-x-')
                                    messaggio_finale.addTextAlign(builder.ALIGN_CENTER);
                                else if (settaggi_profili.Field15 === '--x')
                                    messaggio_finale.addTextAlign(builder.ALIGN_RIGHT);

                                if (settaggi_profili.Field13.trim().length > 0)
                                    messaggio_finale.addText(settaggi_profili.Field13 + '\n');

                                if (settaggi_profili.Field26 === 'true')
                                    messaggio_finale.addFeedLine(1);




                                if (settaggi_profili.Field17 === 'G')
                                    messaggio_finale.addTextStyle(false, false, true, builder.COLOR_1);
                                else if (settaggi_profili.Field17 === 'N')
                                    messaggio_finale.addTextStyle(false, false, false, builder.COLOR_1);

                                if (settaggi_profili.Field18 === 'x--')
                                    messaggio_finale.addTextAlign(builder.ALIGN_LEFT);
                                else if (settaggi_profili.Field18 === '-x-')
                                    messaggio_finale.addTextAlign(builder.ALIGN_CENTER);
                                else if (settaggi_profili.Field18 === '--x')
                                    messaggio_finale.addTextAlign(builder.ALIGN_RIGHT);

                                if (settaggi_profili.Field16.trim().length > 0)
                                    messaggio_finale.addText(settaggi_profili.Field16 + '\n');

                                if (settaggi_profili.Field27 === 'true')
                                    messaggio_finale.addFeedLine(1);



                                if (settaggi_profili.Field20 === 'G')
                                    messaggio_finale.addTextStyle(false, false, true, builder.COLOR_1);
                                else if (settaggi_profili.Field20 === 'N')
                                    messaggio_finale.addTextStyle(false, false, false, builder.COLOR_1);

                                if (settaggi_profili.Field21 === 'x--')
                                    messaggio_finale.addTextAlign(builder.ALIGN_LEFT);
                                else if (settaggi_profili.Field21 === '-x-')
                                    messaggio_finale.addTextAlign(builder.ALIGN_CENTER);
                                else if (settaggi_profili.Field21 === '--x')
                                    messaggio_finale.addTextAlign(builder.ALIGN_RIGHT);

                                if (settaggi_profili.Field19.trim().length > 0)
                                    messaggio_finale.addText(settaggi_profili.Field19 + '\n');

                                //FINE SETTAGGI MESSAGGIO FINALE

                                builder.addTextStyle(false, false, true, builder.COLOR_1);
                                builder.addFeedLine(1);

                                if (settaggi_profili.Field12 === 'true') {
                                    builder.addText(totale_quantita);
                                    builder.addTextPosition(50);
                                    builder.addText('prodotti' + '               '); //quantita
                                }

                                if (settaggi_profili.Field10 === 'true') {
                                    builder.addText("Totale: ");
                                    builder.addTextPosition(415);
                                    builder.addText('€ ' + parseFloat(totale_prezzo).toFixed(2)); //quantita
                                }

                                builder.addText('\n\n');



                                //METTERE SOTTO LE CONCOMITANZE E CON SETTAGGIO

                                if (settaggi_profili.Field427 === 'true' && (numero_tavolo !== "BANCO" && numero_tavolo !== "BAR")) {
                                    messaggio_finale.addTextStyle(false, false, false, builder.COLOR_1);
                                    messaggio_finale.addFeedLine(1);
                                    messaggio_finale.addTextSize(2, 2);
                                    messaggio_finale.addTextAlign(builder.ALIGN_CENTER);
                                    if (comanda.tavolo.indexOf("CONTINUO") !== -1 && numero_progressivo !== undefined) {
                                        messaggio_finale.addText('n° ' + numero_progressivo + '\n');
                                    } else {
                                        messaggio_finale.addText('n° ' + numero_tavolo + '\n');
                                    }
                                    messaggio_finale.addTextFont(builder.FONT_A);

                                }

                                //AGGIUNTO IL 19-01-2017 PER EVITARE DI MOSTRARE LA SCRITTA CONCOMITANZE SE NON CE NE SONO
                                if (settaggi_profili.Field22 === 'true' && concomitanze_presenti === true) {

                                    console.log("OGGETTO CONCOMITANZE", array_comanda);

                                    if (Object.keys(array_comanda).length > 0) {

                                        builder.addTextSize(1, 1);
                                        builder.addTextStyle(false, false, true, builder.COLOR_1);

                                        builder.addTextAlign(builder.ALIGN_CENTER);

                                        builder.addText("-----------------------------------------\n");


                                        builder.addTextAlign(builder.ALIGN_LEFT);
                                    }
                                }


                                builder.addTextAlign(builder.ALIGN_LEFT);

                                //19-01-2017 CAMBIATO OR CON AND, PERCHE' TUTTE DUE LE CONDIZIONI DEVONO ESSERE VERE
                                //SE E' VERA SOLO UNA DELLE DUE NON VUOL DIRE NIENTE
                                if (concomitanze_presenti === true && settaggi_profili.Field22 === 'true') {

                                }

                                var messaggio_finale = '<feed line="1"/>' + messaggio_finale.toString().substr(72).slice(0, -13);

                                var request = builder.toString().slice(0, -13) + testo_concomitanze + messaggio_finale + '<feed line="2"/><cut type="feed"/><pulse type="drawer" /></epos-print>';
                                console.log("REQUEST", request);
                                var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
                                //CAMBIANDO QUESTO PARAMETRO CAMBIA L'IP DI STAMPA (quindi si potrebbe prendere da comanda.nome_stampante[num].ip
                                //SI POTREBBE TESTARE LA DIMENSIONE CON comanda.nome_stampante[num].dimensione
                                //E SE E' O NO FISCALE CON IL PARAMETRO comanda.nome_stampante[num].fiscale

                                var url = "";
                                var url_alternativo = "";
                                if (array_dest_stampa[dest_stampa].intelligent === "SDS") {
                                    url = 'http://' + array_dest_stampa[dest_stampa].ip + ':8000/SPOOLER?stampante=' + array_dest_stampa[dest_stampa].devid_nf;
                                } else {
                                    url = 'http://' + array_dest_stampa[dest_stampa].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                                    if (array_dest_stampa[dest_stampa].ip_alternativo !== undefined && array_dest_stampa[dest_stampa].ip_alternativo !== null && array_dest_stampa[dest_stampa].ip_alternativo !== "") {
                                        url_alternativo = 'http://' + array_dest_stampa[dest_stampa].ip_alternativo + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                                    }
                                }

                                if (solo_servito === true) {} else {
                                    if (settaggi_profili.Field106 === 'true') {
                                        preconto(function(cb) {

                                            if (cb[3] === 'true') {
                                                aggiungi_spool_stampa(cb[0], cb[1], cb[2]);
                                            }
                                            aggiungi_spool_stampa(cb[0], cb[1], cb[2]);

                                            if (settaggi_profili.Field23 === 'true') {
                                                var url2 = 'http://' + comanda.intelligent_non_fiscale + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                                                //LO STAMPA NELLA STAMPANTE PRINCIPALE
                                                aggiungi_spool_stampa(url2, soap, array_dest_stampa[dest_stampa].nome_umano);
                                            }

                                            aggiungi_spool_stampa(url, soap, array_dest_stampa[dest_stampa].nome_umano);

                                        });
                                    } else {
                                        if (settaggi_profili.Field23 === 'true') {
                                            aggiungi_spool_stampa(url, soap, array_dest_stampa[dest_stampa].nome_umano, undefined, undefined, url_alternativo);
                                        }


                                        aggiungi_spool_stampa(url, soap, array_dest_stampa[dest_stampa].nome_umano, undefined, undefined, url_alternativo);
                                    }


                                    //document.getElementById("conto").hide;
                                    // document.getElementById("conto").style.visibility = "none";
                                }


                            } else if (array_dest_stampa[dest_stampa].fiscale.toUpperCase() === 'S') {

                                var corpo = '';
                                var data = comanda.funzionidb.data_attuale();
                                corpo += '<printerNonFiscal>\n\
                                    <beginNonFiscal  operator="1" />';
                                corpo += '<printNormal  operator="1" font="1" data="---' + comanda.lang_stampa[68].toUpperCase() + ' N. ' + numero_tavolo + '---" />';
                                corpo += '<printNormal  operator="1" font="1" data="---' + array_dest_stampa[dest_stampa].nome_umano + '---" />';
                                corpo += '<printNormal  operator="1" font="1" data=" " />';
                                corpo += '<printNormal  operator="1" font="1" data="---' + data + '---" />';
                                corpo += '<printNormal  operator="1" font="1" data=" " />';
                                corpo += '<printNormal  operator="1" font="1" data="---' + comanda.operatore + '---" />';
                                corpo += '<printNormal  operator="1" font="1" data=" " />';
                                var totale_quantita = 0;
                                for (var portata in array_comanda[dest_stampa]) {
                                    if (array_comanda[dest_stampa][portata].val !== undefined) {

                                        corpo += '<printNormal  operator="1" font="1" data="---' + array_comanda[dest_stampa][portata].val + '---" />';
                                        corpo += '<printNormal  operator="1" font="1" data=" " />';
                                        for (var node in array_comanda[dest_stampa][portata]) {

                                            var nodo = array_comanda[dest_stampa][portata][node];
                                            if (nodo !== undefined && nodo[1] !== undefined && nodo[1].length > 1) {

                                                var articolo = filtra_accenti(nodo[1]);
                                                var quantita = nodo[0];
                                                if (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(" && articolo[0] !== "=" && articolo[0] !== "." && articolo[0] !== "*") {
                                                    corpo += '<printNormal  operator="1" font="1" data="' + quantita + ' ' + articolo + '" />';
                                                    totale_quantita += parseInt(quantita);
                                                } else {
                                                    corpo += '<printNormal  operator="1" font="1" data="' + articolo + '" />';
                                                }
                                            }
                                        }
                                    }
                                }

                                corpo += '<printNormal  operator="1" font="1" data="' + totale_quantita + '" />';
                                concomitanze += '<printNormal  operator="1" font="1" data="\n" />';
                                concomitanze += '<printNormal  operator="1" font="1" data="\nCONCOMITANZE:\n" />';
                                for (var dest_stampa_concomitanza in array_comanda) {
                                    if (array_dest_stampa[dest_stampa_concomitanza].nome_umano !== undefined && dest_stampa_concomitanza !== dest_stampa && array_comanda[dest_stampa_concomitanza][portata] !== undefined && array_comanda[dest_stampa_concomitanza][portata] !== null && typeof(array_comanda[dest_stampa_concomitanza][portata]) === "object" && Object.keys(array_comanda[dest_stampa_concomitanza][portata]).length > 0) {
                                        concomitanze_presenti = true;
                                        console.log("CONCOMITANZE: " + dest_stampa + " P: " + portata, array_comanda[dest_stampa_concomitanza][portata], Object.keys(array_comanda[dest_stampa_concomitanza][portata]).length);
                                        concomitanze += '<printNormal  operator="1" font="1" data="\n' + array_dest_stampa[dest_stampa_concomitanza].nome_umano + ' - ' + array_comanda[dest_stampa][portata].val + '\n" />';
                                        for (var node_concomitanza in array_comanda[dest_stampa_concomitanza][portata]) {

                                            var nodo = array_comanda[dest_stampa_concomitanza][portata][node_concomitanza];
                                            if (nodo !== undefined && nodo[1] !== undefined && nodo[1].length > 1) {

                                                var articolo = filtra_accenti(nodo[1]);
                                                var quantita = nodo[0];
                                                if (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(" && articolo[0] !== "=" && articolo[0] !== "." && articolo[0] !== "*") {
                                                    concomitanze += '<printNormal  operator="1" font="1" data="' + quantita + ' ' + articolo + '" />';
                                                } else {

                                                    concomitanze += '<printNormal  operator="1" font="1" data="' + articolo + '" />';
                                                }

                                            }
                                        }
                                    }
                                }

                                var fine = '<endNonFiscal  operator="1" />\n\
     </printerNonFiscal>';
                                var url = 'http://' + array_dest_stampa[dest_stampa].ip + '/cgi-bin/fpmate.cgi?devid=local_printer&timeout=30000';
                                var soap = '';
                                if (concomitanze_presenti === true) {
                                    soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + corpo + concomitanze + fine + '</s:Body></s:Envelope>';
                                } else {
                                    soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + corpo + fine + '</s:Body></s:Envelope>';
                                }

                                aggiungi_spool_stampa(url, soap, array_dest_stampa[dest_stampa].nome_umano);
                                break;
                            }

                    }
                }
            }
        }


    }
}






//FUNZIONI PRECONTO


function a_capi(stringa, numero_caratteri) {

    var riga = "";

    var numero_caratteri_parziali = numero_caratteri - 24;

    while (stringa.length > numero_caratteri) {
        riga += stringa.substr(0, numero_caratteri) + "\n";
        stringa = stringa.substr(numero_caratteri);
    }


    while (stringa.length > numero_caratteri_parziali) {
        if (stringa.length < numero_caratteri) {
            riga += aggSpaziDopo(stringa, numero_caratteri) + "\n";

            stringa = "";

            riga += aggSpaziDopo("", numero_caratteri_parziali);
            break;
        } else {
            riga += stringa.substr(0, numero_caratteri_parziali) + "\n";
            stringa = stringa.substr(numero_caratteri_parziali);
        }
    }

    if (stringa.length > 0) {
        riga += aggSpaziDopo(stringa, numero_caratteri_parziali);
    }


    return riga;
}

function riga_conto_totale(prezzo_totale) {


    var spazi_prezzo_totale = 8;



    var prezzo_totale_formattato = aggSpazi(prezzo_totale, spazi_prezzo_totale);


    var riga = prezzo_totale_formattato;


    return riga;

}

function riga_conto_formattata(prodotto, quantita, prezzo_unitario, prezzo_totale, numero_caratteri) {


    var spazi_quantita_con_x = 4;
    var spazi_prezzo_unitario = 9;
    var spazi_prezzo_totale = 8;


    var prodotto_formattato = a_capi(prodotto, numero_caratteri);


    var prezzo_quantita_formattato = aggSpazi(quantita + " x", spazi_quantita_con_x);
    if (quantita.trim() === "") {
        prezzo_quantita_formattato = aggSpazi("", spazi_quantita_con_x);
    }

    var prezzo_unitario_formattato = aggSpazi(" " + prezzo_unitario, spazi_prezzo_unitario) + " ";
    var prezzo_totale_formattato = "€" + aggSpazi(prezzo_totale, spazi_prezzo_totale);
    if (prezzo_totale === "") {
        prezzo_totale_formattato = " " + aggSpazi("", spazi_prezzo_totale);;
    }


    var riga = prodotto_formattato + prezzo_quantita_formattato + prezzo_unitario_formattato + prezzo_totale_formattato + "\n";


    return riga;

}

function aggSpazi(x, n) {
    while (x.toString().length < n) {
        x = " " + x;
    }
    return x;
}

function aggSpaziDopo(x, n) {
    while (x.toString().length < n) {
        x = x + " ";
    }
    return x;
}

function visualizza_totale_visore_sep_1() {

    var totale_visore = totale_scontrino_separato_1.innerHTML.concat();

    visore(["TOTALE", "€ " + totale_visore], "CENTRO");

    var testo_query = "select Field415,Field416,Field419 from settaggi_profili where id=" + comanda.folder_number + " ";
    var s = alasql(testo_query);
    var settaggi_profilo = s[0];

    if (settaggi_profilo.Field415 !== null && settaggi_profilo.Field415 !== "") {
        setTimeout(function() {
            visore([settaggi_profilo.Field415, settaggi_profilo.Field416], "CENTRO");
        }, parseInt(settaggi_profilo.Field419) * 1000);
    }

    return true;
}

function visualizza_totale_visore_sep_2() {

    var totale_visore = totale_scontrino_separato_2.innerHTML.concat();

    visore(["TOTALE", "€ " + totale_visore], "CENTRO");

    var testo_query = "select Field415,Field416,Field419 from settaggi_profili where id=" + comanda.folder_number + " ";
    var s = alasql(testo_query);
    var settaggi_profilo = s[0];

    if (settaggi_profilo.Field415 !== null && settaggi_profilo.Field415 !== "") {
        setTimeout(function() {
            visore([settaggi_profilo.Field415, settaggi_profilo.Field416], "CENTRO");
        }, parseInt(settaggi_profilo.Field419) * 1000);
    }

    return true;
}

function visualizza_totale_visore() {

    var totale_visore = totale_scontrino.innerHTML.concat();

    visore(["TOTALE", "€ " + totale_visore], "CENTRO");

    var testo_query = "select Field415,Field416,Field419 from settaggi_profili where id=" + comanda.folder_number + " ";
    var s = alasql(testo_query);
    var settaggi_profilo = s[0];

    if (settaggi_profilo.Field415 !== null && settaggi_profilo.Field415 !== "") {
        setTimeout(function() {
            visore([settaggi_profilo.Field415, settaggi_profilo.Field416], "CENTRO");
        }, parseInt(settaggi_profilo.Field419) * 1000);
    }

    return true;
}

function visore(array_righe_testo, allineamento) {

    function allineamento_interno(testo, allineamento) {

        if (allineamento === "CENTRO") {

            var numero_spazi_iniziali = Math.floor((20 - testo.length) / 2);

            var numero_spazi_finali = 20 - (numero_spazi_iniziali + testo.length);

            var testo_finale = "";

            for (var a = 0; a < numero_spazi_iniziali; a++) {

                testo_finale += " ";

            }

            testo_finale += testo;

            for (var a = 0; a < numero_spazi_finali; a++) {

                testo_finale += " ";

            }

        } else {
            var numero_spazi_iniziali = 0;

            var numero_spazi_finali = 20 - (numero_spazi_iniziali + testo.length);

            var testo_finale = "";

            for (var a = 0; a < numero_spazi_iniziali; a++) {

                testo_finale += " ";

            }

            testo_finale += testo;

            for (var a = 0; a < numero_spazi_finali; a++) {

                testo_finale += " ";

            }


        }

        return testo_finale;
    }


    var testo_finale = "";

    array_righe_testo.forEach(function(v) {

        testo_finale += allineamento_interno(v, allineamento);


    });

    if (testo_finale.length === 20) {
        testo_finale += "                    ";
    }



    var xml = '<printerCommand><directIO  command="1062" data="010' + testo_finale + '00" /></printerCommand>';
    var epos = new epson.fiscalPrint();
    epos.send("http://" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=local_printer&timeout=30000", xml, 30000);

    return true;

}