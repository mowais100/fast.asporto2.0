﻿function leggi_get_url(name, url) {
    if (!url)
        url = location.href;
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(url);
    return results === null ? null : results[1];
}

function getIPs(callback) {
    window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection; //compatibility for firefox and chrome
    var pc = new RTCPeerConnection({iceServers: []}), noop = function () {};
    pc.createDataChannel(""); //create a bogus data channel
    pc.createOffer(pc.setLocalDescription.bind(pc), noop); // create offer and set local description
    pc.onicecandidate = function (ice) {  //listen for candidate events
        if (!ice || !ice.candidate || !ice.candidate.candidate)
            return;
        var myIP = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/.exec(ice.candidate.candidate)[1];
        console.log('my IP: ', myIP);
        callback(myIP);
        pc.onicecandidate = noop;
    };
}

var socket_fast_split;
function Comanda() {
    
    this.url_fastorder="";
    
    this.ultima_comanda_forno_aperta_stampata=false;
    this.ultimo_id="";

    this.stampa_ordini_qui = true;
    
    this.controlla_giacenza = false;
    this.selezione_tavoli_piantina = true;
    this.consegna_fattorino_extra = "0.00";
    this.tipologia_fattorino = "0";
    this.verifica_tavoli_periodica_abilitata = false;
    this.uuid_sdcard = "";
    this.simulazione_iphone = true;
    this.db_server_replica = "";
    this.url_server_mysql = "";
    this.secondi_attesa_replica_mysql = "";
    this.array_db_centri = new Array();
    this.selezione_centri_abilitata = true;

    this.AndroidPrinter = false;

    /* LISTENERS TOTALI IN ASCOLTO SU DOCUMENT
     * var c=Object.values(jQuery._data( document, "events" )).map(v=>v.length).reduce(function(a,v){return a+=v;});*/

    this.settaggi_TSE = true;

    this.file_settaggi = "SETTAGGI";
    if (this.settaggi_TSE === true) {
        this.file_settaggi = "SETTAGGITSE";
    }

    this.set_tid = false;


    this.somma_progressivi = 0;

    this.partenza_tasto = "";
    this.provenienza_conto = false;
    this.stampante_da_assegnare = "";
    this.viewport = aggZero(visualViewport.width, 4) + "-" + aggZero(visualViewport.height, 4);
    this.tipo_mappa = "PC";
    this.variante_automatica = false;
    this.numero_pos_utilizzato = "POS 1";
    this.importo_acconto_fattura = 0;
    this.sezionale = "0009";
    this.cassa_singola = true;
    //Se quella sopra è true, quella sotto dev'essere per forza false
    this.query_su_socket = false;
    this.fattura_elettronica = true;
    this.contiene_variante = false;
    this.numero_licenza_cliente = "9000";
    this.visualizza_tutti_venduti = true;
    this.conto_in_lingua = false;
    this.escludi_art_zero_statistiche = false;
    this.ricerca_alfabetica = "N";
    this.partita_iva_estera = false;
    this.tasto_pizza_maxi = false;
    this.capienza_pizza_maxi = "2";
    this.cancellazione_articolo = 'S';
    this.ultimo_numero_intercettato = "";
    this.evento = "onclick";
    this.eventino = "click";
    this.filtro_ricerca_generico = "S";
    //AGGIUNTO IL 26/01/2018
    this.id = leggi_get_url('id');
    if (this.id === null || this.id === '') {
        //alert("ID TERMINALE VUOTO");

        this.id = 'MB';
    }

    //ATTENZIONE - INTELLINET A MANO
    this.multiquantita = "N";
    this.socket_fastSplit = true;
    if (this.socket_fastSplit === true) {
        socket_fast_split = new WebSocket("ws://localhost:7777");
    } else
    {
        socket_fast_split = new Object();
        socket_fast_split.send = function () {};
    }

    //Serve ad indicare se c'� pi� di un misuratore fiscale
    //con la stessa numerazione delle fatture
    this.progressivo_unico_fatture = false;
    //FACOLTATIVO: SOLO PER CHI HA IL SERVER IN REPLICA TIPO BROVEDANI
    this.server_replica_abilitato = false;
    this.url_server_replica = 'http://192.168.0.73/fast.intellinet/';
    //Abilitare log palmare
    this.abilita_log_palmare = false;
    this.tasto_comanda_konto = "N";
    this.filtro_lettera_bloccato = "N";
    this.ultima_portata_abilitata = "S";
    this.varianti_riunite = "N";
    this.tasto_sconto = "N";
    this.blocco_tavolo_cameriere = "N";
    this.tasto_pococotto = "N";
    this.tasto_finecottura = "N";
    this.tasto_rechnung = "N";
    this.tasto_quittung = "N";
    this.tasto_konto = "N";
    this.tasto_bargeld = "N";
    this.tasto_bestellung = "S";
    this.tasto_fastconto = "N";
    var d = new Date();
    var n = d.getDay();
    var giorno_settimanale = '';
    switch (n) {
        case 0:
            giorno_settimanale = 7;
            break;
        case 1:
            giorno_settimanale = 1;
            break;
        case 2:
            giorno_settimanale = 2;
            break;
        case 3:
            giorno_settimanale = 3;
            break;
        case 4:
            giorno_settimanale = 4;
            break;
        case 5:
            giorno_settimanale = 5;
            break;
        case 6:
            giorno_settimanale = 6;
            break;
    }


    this.giorno = giorno_settimanale;
    this.versione = '16/12/2021 10:00';
    this.ip_fisso_terminale = 'X';
    this.licenza = "mattia131";
    this.licenza_vera = "Imperial";
    this.esecuzione_comanda_immediata = false;
    this.loading_tab_comanda = false;
    this.socket_sospesi = new Array();
    this.socket_sospesi_tavoli = new Array();
    //Errore quando si spengono tutti i socket
    this.rt_engine_error = false;
    //TEST PER IL NUOVO SPOOLER
    this.id_stampa = new Array();
    this.terminale = leggi_get_url('id');
    //ELIMINATO 02/11/2016
    //if (this.ip_fisso_terminale === undefined || this.ip_fisso_terminale.length === 0 || this.ip_fisso_terminale === '' || this.ip_fisso_terminale === '0' || this.ip_fisso_terminale === 0)
    //{
    //    getIPs(function (d) {
    //
    //        comanda.ip_address = d;
    //        comanda.terminale = leggi_get_url('id') + '-' + d;
    //
    //    });
    //}

    //CAMBIARE DOPO

    //ANDROID CANTIERE 8080
    //WINDOWS DEFAULT 80
    this.porta_server_remoto = '80';
    this.url_server = 'http://192.168.0.73/fast.intellinet/';
    this.ip_server = '192.168.0.73';
    this.indirizzo_access_point = 'http://192.168.0.73';
    //QUESTA SERVE COME STAMPANTE NON FISCALE PRINCIPALE. 
    //SE DEVE STAMPARE INCASSI O RIEPILOGHI IN BAR STAMPA SEMPRE QUI
    this.intelligent_non_fiscale = "192.168.0.52";
    this.ricerca_numerica_abilitata = false;
    this.split_abilitato = true;
    //LASCIARE DI DEFAULT
    this.tipo_ricerca_articolo = undefined;
    this.numero_conto_split = 0;
    this.colore_tasti_mobile = true;
    this.tipo_consegna = "NIENTE";
    this.prezzo_tasti_sotto = '0.00';
    this.specifica_tasti_sotto = '';
    //PREZZO NON MODIFICABILE IN MODIFICA ARTICOLO
    this.prezzo_non_modificabile = false;
    this.socket_ciclizzati_temp = new Array();
    this.socket_ciclizzati = new Array();
    this.partenza_automatica_socket_dispositivo = true;
    this.tentativo_fallito = new Array();
    this.server_cloud = false;
    this.ora_ultimo_spooler = Math.floor(Date.now() / 1000);
    this.disable_logs = false;
    this.percentuale_iva_default = 22;
    this.bandierina = "0";
    //DISATTIVA CONSOLE.LOG E CONSOLE.TRACE
    if (this.disable_logs === true) {
        console.log = function () {
        };
        console.trace = function () {
        };
    }

    //Qui si trovano tutti i settaggi locali

    //QUESTI PARAMETRI SONO SOLO PER KONTO QUITTUNG RESHNUNG SCONTRINO FATTURA
    //PER LE COMANDE VEDERE NOMI_STAMPANTI NEL DATABASE

    //INDIRIZZO IP DEL MISURATORE FISCALE INTELLINET
    this.indirizzo_cassa = "192.168.1.61";
    //INDIRIZZO IP DELLA STAMPANTE NON FISCALE INTELLINET
    //this.intelligent_non_fiscale = "192.168.0.60";    

    //this.intelligent_non_fiscale = "192.168.0.140";

    //INDIRIZZO IP DELLA STAMPANTE NON FISCALE NON INTELLINET
    //this.indirizzo_non_fiscale = "192.168.0.51"; //STAMPANTE COMANDA N.F.


    //NON SERVE SCEGLIERE SE IL QUITTUNG E' O NO INTELLINET PERCHE' LO E' SEMPRE

    //STAMPA COMANDA INTELLINET O NO? (S/N)
    //SE STAMPO IN TEDESCO AD ESEMPIO PER ORA E' SI    

    //this.opz_comanda_intellinet = "N";

    //PREFISSO PER LA STAMPA FISSA
    //this.carattere_stampa_piccola = String.fromCharCode(18);//Ascii 27+71+1

    //SE LA STAMPANTE PER SCONTRINI E' PICCOLA SI SETTA SU S
    //E I CARATTERI DI QUITTUNG, RESHNUNG E CONTO VENGONO RIDOTTI DI 10

    this.nsegn = 0;
    this.sincro_query_cassa_attivo = false;
    this.stampante_piccola = "N";
    this.stampante_piccola_conto = "N";
    //VAR DI SISTEMA - NON TOCCARE
    this.stampato = true;
    //FINE DEI COMANDI PER LE STAMPE ESCLUSE LE COMANDE


    //DISPOSITIVO INIZIALE DI DEFAULT
    this.dispositivo = "COMANDA";
    /*
     * LINGUE DISPONIBILI:
     * italiano
     * english
     * deutsch
     */

    //Lingua del display locale
    this.lingua = "italiano";
    //Lingua di stampa predefinita (in caso di errori NON TOCCARE)
    this.lingua_stampa = "italiano";
    //Operatore predefinito
    this.operatore = 0;
    //Password predefinita
    this.password = 0;
    //Quanto apro il tavolo apre subito la categoria predefinita (FACOLTATIVO VALE SOLO SU PALMARE)
    this.apri_subito_categoria_predefinita = true;
    //Pagina degli articoli predefinita (numero pagina)
    this.pagina = 1;
    //Tavolo predefinito (numero tavolo)
    this.tavolo = 0;
    //Debug abilitato
    this.debug = 0;
    //Variabili del conto separato o no (NE MANCANO ALCUNE FISCALI)
    //this.intestazioni_conto=false;
    this.conto = '#conto';
    this.intestazione_conto = '#intestazioni_conto';
    this.totale_scontrino = '#totale_scontrino';
    //La finestra dei coperti sopra i totali è necessaria?
    this.coperti_necessari = true;
    //Funzione per reimpostare le variabili di default dopo il conto separato
    this.reimposta_variabili_conto_separato = function () {
        this.conto = '#conto';
        this.intestazione_conto = '#intestazioni_conto';
        this.totale_scontrino = '#totale_scontrino';
    };
    //Settaggio per scegliere se tornare a bar o a tavoli
    this.torna_bar = true;
    //Settaggi di sistema di default (non toccare se non sei un programmatore esperto)

    this.spooler_aperto = false;
    this.spooler_risposte_catturate = 0;
    this.syncerror = false;
    this.compatibile_xml = false;
    this.parcheggio_torna_tavoli === false;
    this.tasto_indietro_parcheggio = false;
    this.popup_modifica_articolo = false;
    this.loading = true;
    this.popup_modifica_articolo = false;
    this.join = false;
    this.ajax_attivo = 0;
    this.parcheggio = 0;
    this.ora_consegna = '';
    this.minuti_consegna = '';
    this.ultima_cat_variante = 0; //V_Bibite
    this.ultima_opz_variante = ''; // + o -
    //this.ultima_categoria_principale = 1;
    this.sblocca_tavolo_cliccato = false;
    this.permesso = 1;
    this.drag = false;
    //Metodi di sistema (non toccare se non sei un programmatore esperto)
    this.sincro = new Sincro();
    this.funzionidb = new FunzioniDB();
    this.sock = new Funzioni_Socket();
    //Memorizzazione delle sincronie mancanti
    //Array di oggetti
    //L'array è numerata e contiene per ogni numero l'oggetto sock
    if (this.array_dbcentrale_mancanti === undefined) {
        this.array_dbcentrale_mancanti = new Array();
    }

    //Questa è solo per i tavoli. Serve ad inviare un tavolo quando è stato completato nell'array vero
    if (this.array_dbcentrale_mancanti_temp === undefined) {
        this.array_dbcentrale_mancanti_temp = new Array();
    }

    //Funzione Ajax di base (non toccare se non sei un programmatore esperto)
    this.costruttore_ajax = function (async) {

        //la sincronia predefinita è SINCRONA
        if (async === undefined)
        {
            async = false;
        }

        ////console.log(async);

        $.ajax({
            type: "POST",
            async: async,
            timeout: 10000,
            url: comanda.url_server + 'classi_php/cassa_ajax_' + comanda.lingua_stampa + '.lib.php',
            data: {
                action: "costruttore_ajax",
                //passo dati_costruttore come array
                dati_costruttore: {
                    nome_dispositivo: comanda.dispositivo,
                    nome_operatore: comanda.operatore,
                    password_md5: comanda.password,
                    id_categoria: comanda.categoria,
                    numero_pagina: comanda.pagina,
                    numero_tavolo: comanda.tavolo,
                    nome_parcheggio: comanda.parcheggio
                }
            }
        });
    };
}




//OGGETTO PER CREARE LA COMANDA CON TUTTE LE SUE PROPRIETA'
var comanda = new Comanda();

