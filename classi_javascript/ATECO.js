/****/

/* global epson, comanda, bootbox */



function ATECO_model() {

    let misuratore_presente = false;

    /* Metodo Salva */

    this.salva_dato_singolo = async function (p, i) {

        return new Promise(function (resolve, reject) {


            var xml = '<printerCommand><directIO  command="4037" data="' + aggZero(i, 2) + '' + aggZero(p[i], 6) + '01000000" /></printerCommand>';
            var epos = new epson.fiscalPrint();
            epos.send("http://" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=local_printer&timeout=10000", xml, 10000);
            epos.onreceive = function (result, tag_names_array, add_info, res_add) {
                resolve(true);
            };
            epos.onerror = function () {
                if (alert_aperto_misuratore === false) {
                    alert_aperto_misuratore = true;
                    bootbox.alert("Problema nella lettura dei reparti dal misuratore RT.<br><br>NON LAVORARE SE DEVI STAMPARE SCONTRINI O FATTURE!<br><br>RIAVVIA IL MISURATORE, ATTENDI UN MINUTO, E POI RIAVVIA IL SOFTWARE.<br><br>Non preoccuparti, sono cose di routine.");
                    resolve(false);
                } else {
                    resolve(false);
                }
            };


        });
    };

    this.salva_in_misuratore = async function (p) {

        if (comanda.lingua_stampa === "italiano") {

            let data_length = p.length;

            for (let i = 1; i < data_length; i++) {
                await this.salva_dato_singolo(p, i);
            }

        }
    };

    /* Costruttore classe Model */

    var self = this;

    this.listaATECO = new listaATECO();

    function listaATECO() {

        if (comanda.lingua_stampa === "italiano") {
            this.ATECO_1 = "";
            this.ATECO_2 = "";
            this.ATECO_3 = "";



        } else if (comanda.lingua_stampa === "deutsch") {
            this.ATECO_1 = "";
            this.ATECO_2 = "";
            this.ATECO_3 = "";



        }

    }


    function oggettoRispostaMisuratore(datoGrezzo) {

        this.rispostaGrezza = datoGrezzo.responseData;

        this.numero = this.rispostaGrezza.substr(0, 2);

        //attenzione che sia giusto
        this.ateco = this.rispostaGrezza.substr(2, 6);

    }


    this.init = async function () {

        if (comanda.lingua_stampa === "italiano") {

            if (comanda.compatibile_xml !== true) {

                let r = alasql("select * from settaggi_profili");

                if (comanda.pizzeria_asporto === true) {

                    if (r[0].Field317 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field318 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field319 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field321 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field421 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field364 === "true") {
                        misuratore_presente = true;
                    }

                    if (r[0].Field503 === "true") {
                        misuratore_presente = true;
                    }

                    if (r[0].Field504 === "true") {
                        misuratore_presente = true;
                    }

                    if (r[0].Field200 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field322 === "true") {
                        misuratore_presente = true;
                    }

                } else {

                    if (comanda.mobile === true) {

                        if (r[0].Field430 === "true") {
                            misuratore_presente = true;
                        }
                        if (r[0].Field431 === "true") {
                            misuratore_presente = true;
                        }
                        if (r[0].Field432 === "true") {
                            misuratore_presente = true;
                        }
                        if (r[0].Field433 === "true") {
                            misuratore_presente = true;
                        }

                    }

                    if (r[0].Field197 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field198 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field199 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field200 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field329 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field201 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field202 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field364 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field365 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field366 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field367 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field207 === "true") {
                        misuratore_presente = true;
                    }



                }
            }

            for (let i = 1; i <= 3; i++) {
                await this.richiedi_dato(i);
            }

        }
    };


    this.richiedi_dato = async function (i) {



        return new Promise(function (resolve, reject) {

            if (misuratore_presente === true) {

                var xml = '<printerCommand><directIO  command="4237" data="' + aggZero(i, 2) + '" /></printerCommand>';
                var epos = new epson.fiscalPrint();
                epos.send("http://" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=local_printer&timeout=10000", xml, 10000);
                epos.onreceive = function (result, tag_names_array, add_info, res_add) {
                    var ogg_risp = new oggettoRispostaMisuratore(add_info);
                    self.listaATECO["ATECO_" + i] = ogg_risp.ateco;
                    resolve(true);
                };

                epos.onerror = function () {

                    if (alert_aperto_misuratore === false) {
                        alert_aperto_misuratore = true;
                        bootbox.alert("Problema nella lettura dei reparti dal misuratore RT.<br><br>NON LAVORARE SE DEVI STAMPARE SCONTRINI O FATTURE!<br><br>RIAVVIA IL MISURATORE, ATTENDI UN MINUTO, E POI RIAVVIA IL SOFTWARE.<br><br>Non preoccuparti, sono cose di routine.");
                        resolve(false);
                    } else {
                        resolve(false);
                    }

                };

            } else {

                resolve(false);


            }

        });



    };

    //this.init();



}
;



function ATECO_view() {

    for (let i = 1; i <= 3; i++) {
        this["ATECO_" + i] = document.getElementById("ATECO_" + i);
        this["REPARTO_ATECO_" + i] = document.getElementById("REPARTO_ATECO_" + i);
    }

}



function ATECO_controller() {


    this.init = function () {

        for (let i = 1; i <= 3; i++) {
            ATECO_view["ATECO_" + i].value = ATECO_model.listaATECO["ATECO_" + i];

            /*tabelle accessorie*/
            ATECO_view["REPARTO_ATECO_" + i].innerHTML = ATECO_model.listaATECO["ATECO_" + i];
        }

    };

    this.salva = async function () {

        /* p significa parametro */

        let p = new Array();

        for (let i = 1; i <= 3; i++) {
            p[i] = ATECO_view["ATECO_" + i].value;
        }

        await ATECO_model.salva_in_misuratore(p);

        ATECO_controller.aggiorna();
    };

    this.aggiorna = async function (callback) {
        await ATECO_model.init();
        ATECO_controller.init();
        if (typeof callback === "function") {
            callback(true);
        }
    };

    this.indice = function (numero) {

        /*return ATECO_model.listaATECO["ATECO_" + numero];*/

    };

}

/*Dichiaro le classi statiche*/
/* le variabili vengono inizializzate quando vedo gli ip delle stampanti su multilanguage.js */

var ATECO_model;
var ATECO_view;
var ATECO_controller;