/* global comanda, bootbox, data_inizio_servizio */

// MANCANO COPERTI
function esporta_xml_tavolo(tipo, cB, incasso_definito) {
    var annulla_comanda = true;
    var annulla_comanda2 = false;
    var bis_presente = false;
    var variabile_tavolo = comanda.tavolo;
    var numero_coperti = $("#numero_coperti").val();


    oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)] = new Object();
    if (comanda.incasso_auto === 'S')
    {
        incasso_auto = "S";
    } else if (comanda.incasso_auto === 'N')
    {
        incasso_auto = "N";
    }

    var BIS = '';
    if (comanda.split_abilitato === true) {
        if (lettera_conto_split().length >= 1)
        {
            BIS = "-" + lettera_conto_split();
        }
    }

    console.log("esporta_xml_tavolo", "elimina file");
    //ASINCRONA RISPETTO AL SALVATAGGIO XML (SI POSSONO FARE ANCHE ASSIEME)
    //});

    //ASINCRONA RISPETTO ALLO SVUOTAMENTO TAVOLO OCCUPATO (SI POSSONO FARE ANCHE ASSIEME)
    var totale = 0;
    var variabile = '';
    var testo_query = "select NCARD3,AGG_GIAC,carta_credito,ora_,operatore,BIS,QTAP,LIB3,LIB2,dest_stampa_2,NSEGN,LIB1,LIB4,NCARD5,sconto_perc,perc_iva,ordinamento,ultima_portata,peso_ums,cod_articolo,dest_stampa,contiene_variante,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita,stampata_sn,tipo_ricevuta  from comanda where desc_art NOT LIKE 'RECORD TESTA%' and  contiene_variante='1' and ntav_comanda='" + variabile_tavolo + "' and posizione='CONTO' and stato_record='ATTIVO' \n\
                        union all select NCARD3,AGG_GIAC,carta_credito,ora_,operatore,BIS,QTAP,LIB3,LIB2,dest_stampa_2,NSEGN,LIB1,LIB4,NCARD5,sconto_perc,perc_iva,ordinamento,ultima_portata,peso_ums,cod_articolo,dest_stampa,contiene_variante,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita,stampata_sn,tipo_ricevuta from comanda where  desc_art NOT LIKE 'RECORD TESTA%' and (contiene_variante !='1' or contiene_variante is null) and length(nodo)=3 and ntav_comanda='" + variabile_tavolo + "' and posizione='CONTO' and stato_record='ATTIVO'  \n\
                        union all select NCARD3,AGG_GIAC,carta_credito,ora_,operatore,BIS,QTAP,LIB3,LIB2,dest_stampa_2,NSEGN,LIB1,LIB4,NCARD5,sconto_perc,perc_iva,ordinamento,ultima_portata,peso_ums,cod_articolo,dest_stampa,contiene_variante,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita,stampata_sn,tipo_ricevuta from comanda where  desc_art NOT LIKE 'RECORD TESTA%' and length(nodo)=7 and ntav_comanda='" + variabile_tavolo + "'  and posizione='CONTO' and stato_record='ATTIVO';";
    console.log("esporta_xml_tavolo", testo_query);
    let result = alasql(testo_query);

    result = alasql("select * from ? order by dest_stampa,portata,nodo,prog_inser", [result]);

    if (result.length === 0)
    {
        //ELIMINA IL  TAVOLO E PLANING SE NON CI SONO PIU ARTICOLI
        if (typeof (cB) === "function") {
            console.log(" esporta_xml_tavolo tavolo inesistente");
            var nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3);
            comanda.xml.elimina_file_xml("_" + comanda.folder_number + "/" + nome_file, true, function () {

                var nome_file = "/TAVOLO" + aggZero(variabile_tavolo, 3) + ".XML";
                comanda.xml.elimina_file_xml("_" + comanda.folder_number + "/" + nome_file, false, function () {
                    cB(true);
                });
            });
        }
        //ELIMINATO 24 OTTOBRE 2016
        //btn_tavoli("CONTO");
        return false;
    }

    console.log("esporta_xml_tavolo", result);
    //INTESTAZIONE
    var output = '<?xml version="1.0" standalone="yes"?>\r\n\
                    <dataroot xmlns="http://www.w3.org/2001/TAVOLI.xsd">\r\n';
    /*output += ' <TAVOLI>\r\n\
     <TC>X</TC>\r\n\
     <DATA>01/01/2005</DATA>\r\n\
     <TAVOLO>X</TAVOLO>\r\n\
     <BIS />\r\n\
     <PRG />\r\n\
     <ART />\r\n\
     <ART2 />\r\n\
     <VAR />\r\n\
     <DES />\r\n\
     <PRZ />\r\n\
     <COSTO />\r\n\
     <QTA />\r\n\
     <SN>N</SN>\r\n\
     <CAT />\r\n\
     <TOTALE />\r\n\
     <SCONTO />\r\n\
     <AUTORIZ />\r\n\
     <INC />\r\n\
     <CARTACRED />\r\n\
     <BANCOMAT />\r\n\
     <ASSEGNI />\r\n\
     <NCARD1 />\r\n\
     <NCARD2 />\r\n\
     <NCARD3 />\r\n\
     <NCARD4 />\r\n\
     <NCARD5 />\r\n\
     <NSEGN />\r\n\
     <NEXIT />\r\n\
     <TS />\r\n\
     <NOME />\r\n\
     <ORA />\r\n\
     <LIB1 />\r\n\
     <LIB2 />\r\n\
     <LIB3 />\r\n\
     <LIB4 />\r\n\
     <CLI />\r\n\
     <QTAP />\r\n\
     <NODO />\r\n\
     <PORTATA />\r\n\
     <NUMP />\r\n\
     <TAVOLOTXT />\r\n\
     <ULTPORT />\r\n\
     <LIB5 />\r\n\
     <AGG_GIAC />\r\n\
     </TAVOLI>\r\n';*/
    var i = 0;
    var cod_articolo_princ;
    var ts_princ;
    var prog_principale;
    var descrizione_articolo;
    var ordinamento = '';

    var testa_totale = new Object();
    var qta_princ = 1;

    result.forEach(function (obj) {
        if (obj.prezzo_un.trim() === "") {
            obj.prezzo_un = "0,00";
        }
        console.log("OGGETTO TAVOLO", obj);
        //SEGNALIBRO

        totale += parseFloat(obj.prezzo_un.replace(',', '.')) * parseInt(obj.quantita);
        //PRIMA ERA +1 MA NON SE NE CAPISCE IL MOTIVO
        obj.prog_inser = parseInt(obj.prog_inser);
        obj.dest_stampa !== undefined ? ts_princ = obj.dest_stampa : ts_princ = '1';
        obj.peso_ums !== undefined ? obj.peso_ums : obj.peso_ums = '';
        obj.ordinamento !== undefined ? ordinamento = obj.ordinamento : ordinamento = '01';
        prog_principale = aggZero(obj.prog_inser, 3);
        descrizione_articolo = obj.desc_art;
        console.log(obj.dest_stampa);
        console.log(obj.dest_stampa !== undefined);
        console.log(ts_princ);
        var contiene_variante = '';
        obj.contiene_variante === '1' ? contiene_variante = 'SI' : contiene_variante = '';
//---


        if (obj.desc_art[0] === "+" || obj.desc_art[0] === "-" || obj.desc_art[0] === "x" || obj.desc_art[0] === "*" || obj.desc_art[0] === "=")
        {
            prog_principale = aggZero(prog_principale.substring(0, 3), 3) + ' ' + aggZero(obj.prog_inser, 3) + ' ';
            descrizione_articolo = obj.desc_art.substring(1);
            variabile = obj.desc_art[0];
        } else if (obj.desc_art[0] === "(") {

            prog_principale = aggZero(prog_principale.substring(0, 3), 3) + ' ' + aggZero(obj.prog_inser, 3) + ' ';
            variabile = "*";
        } else
        {
            cod_articolo_princ = obj.cod_articolo;
            variabile = "";
        }

        console.log("PRIMA LETTERA ARTICOLO", obj.desc_art[0], variabile);
        if (obj.stampata_sn === null)
        {
            obj.stampata_sn = '';
        }

        var d = new Date();
        var Y = d.getFullYear().toString();
        var D = addZero(d.getDate(), 2);
        var M = addZero((d.getMonth() + 1), 2);
        var h = addZero(d.getHours(), 2);
        var m = addZero(d.getMinutes(), 2);
        var s = addZero(d.getSeconds(), 2);
        var ms = addZero(d.getMilliseconds(), 3);
        var data = D + '/' + M + '/' + Y;
        var ora = h + ':' + m + ':' + s;
        var stampata_sn;
        if (obj.stampata_sn !== undefined && obj.stampata_sn !== null)
        {
            stampata_sn = obj.stampata_sn.toUpperCase();
        } else
        {
            stampata_sn = "N";
        }

        var tipo_ricevuta = '';
        if (obj.tipo_ricevuta === 'SERVITO')
        {
            tipo_ricevuta = 'SERVITO';
        }

        if (tipo_ricevuta === 'SERVITO' || stampata_sn === 'N' || descrizione_articolo === "MEMO INC,SCONTO,DRINK,LISTINO,COPERTI" || descrizione_articolo.indexOf(" hg.") !== -1)
        {
            annulla_comanda = false;
            var ora = h + ':' + m + ':' + s;
        } else
        {
            var ora = obj.ora_;
        }

        var operatore = obj.operatore;
        if (operatore.length === 0) {
            operatore = comanda.operatore;
        }

        //ELIMINATO 24 OTTOBRE 2016
        //CAPIRE IL MOTIVO PER CUI C'ERA
        /*if (tipo !== "SENZA_AGGIUNTE") {
         stampata_sn = "S";
         }*/

        if (testa_totale[obj.BIS] === undefined) {
            testa_totale[obj.BIS] = 0;
        }
        testa_totale[obj.BIS] += parseFloat(conv_prz_fc_ibr(obj.prezzo_un)) * parseInt(obj.quantita);


        var ncard3 = '';
        if (obj.NCARD3) {
            ncard3 = obj.NCARD3;
        }

        let tab_iva = alasql("select aliquota from tabella_iva where lingua='" + comanda.lingua_stampa + "';");
        let percentuali_iva = tab_iva.map(d => d.aliquota);

        if (percentuali_iva.indexOf(obj.perc_iva.trim()) === -1) {
            /*bootbox.alert("Attenzione: l'IVA dell'articolo " + descrizione_articolo + " non &egrave; presente nella tabella IVA. Modificalo e ri-lancia l'operazione");
             annulla_comanda2 = true;
             return false;*/

            obj.perc_iva = comanda.percentuale_iva_default;
        } else if (isNaN(parseFloat(conv_prz_fc_ibr(obj.prezzo_un)).toFixed(2))) {
            bootbox.alert("Attenzione: il prezzo dell'articolo " + descrizione_articolo + " non &egrave; valido. Modificalo e ri-lancia l'operazione");
            annulla_comanda2 = true;
            return false;
        } else if (isNaN(parseInt(obj.quantita)) || parseInt(obj.quantita) < 1) {
            bootbox.alert("Attenzione: la quantit&agrave; dell'articolo " + descrizione_articolo + " non &egrave; valida. Modificala e ri-lancia l'operazione");
            annulla_comanda2 = true;
            return false;
        }

        console.log("ESPORTAZIONE XML", obj);
        //RIGHE CORPO
        output += '<TAVOLI>\r\n';
        output += '<TC>C</TC>\r\n';
        output += '<DATA>' + data_inizio_servizio + '</DATA>\r\n';
        output += '<TAVOLO>' + aggZero(variabile_tavolo, 3) + '</TAVOLO>\r\n';
        output += '<BIS>' + obj.BIS + '</BIS>\r\n';
        output += '<PRG>' + aggZero(obj.prog_inser, 3) + '</PRG>\r\n';
        output += '<ART>' + cod_articolo_princ + '</ART>\r\n';
        output += '<ART2>' + obj.cod_articolo + '</ART2>\r\n';
        output += '<VAR>' + variabile + '</VAR>\r\n';
        output += '<DES>' + descrizione_articolo + '</DES>\r\n';
        output += '<PRZ>' + obj.prezzo_un.replace('.', ',') + '</PRZ>\r\n';
        output += '<COSTO>0</COSTO>\r\n';
        output += '<QTA>' + obj.quantita + '</QTA>\r\n';
        //Dato che esporta solo se uno stampa scontrino o comanda ÃƒÆ’Ã‚Â¨ sempre S
        output += '<SN>' + stampata_sn + '</SN>\r\n';
        output += '<CAT>' + obj.categoria + '</CAT>\r\n';
        output += '<TOTALE />\r\n';
        output += '<SCONTO />\r\n';
        output += '<AUTORIZ>' + obj.sconto_perc + '</AUTORIZ>\r\n';
        output += '<INC>0</INC>\r\n';
        output += '<CARTACRED>' + obj.carta_credito + '</CARTACRED>\r\n';
        output += '<BANCOMAT/>\r\n';
        output += '<ASSEGNI>' + obj.perc_iva + '</ASSEGNI>\r\n';
        output += '<NCARD1>' + contiene_variante + '</NCARD1>\r\n';
        output += '<NCARD2/>\r\n';
        output += '<NCARD3>' + ncard3 + '</NCARD3>\r\n';
        output += '<NCARD4/>\r\n';
        output += '<NCARD5>' + obj.NCARD5 + '</NCARD5>\r\n'; //ORDINAMENTO
        output += '<NSEGN>' + obj.NSEGN + '</NSEGN>\r\n';
        output += '<NEXIT/>\r\n';
        output += '<TS>' + ts_princ + '</TS>\r\n';
        output += '<NOME>' + operatore + '</NOME>\r\n';
        output += '<ORA>' + ora + '</ORA>\r\n';
        output += '<LIB1>' + obj.LIB1 + '</LIB1>\r\n';
        output += '<LIB2>' + obj.LIB2 + '</LIB2>\r\n';
        output += '<LIB3>' + obj.LIB3 + '</LIB3>\r\n';
        output += '<LIB4>' + obj.LIB4 + '</LIB4>\r\n';
        output += '<LIB5>' + obj.dest_stampa_2 + '</LIB5>\r\n';
        output += '<CLI/>\r\n';
        output += '<QTAP>' + obj.QTAP + '</QTAP>\r\n';
        output += '<NODO>' + obj.nodo + '</NODO>\r\n';
        output += '<PORTATA>' + obj.portata + '</PORTATA>\r\n';
        output += '<NUMP>0</NUMP>\r\n';
        output += '<TAVOLOTXT>' + comanda.tavolotxt + '</TAVOLOTXT>\r\n';
        output += '<ULTPORT>' + obj.ultima_portata + '</ULTPORT>\r\n';
        output += '<AGG_GIAC>' + obj.AGG_GIAC + '</AGG_GIAC>\r\n';
        output += '</TAVOLI>\r\n';
        i++;
    });

    if (annulla_comanda2 === true) {
        return false;
    }

    let colore_azzurro = false;

    comanda.sincro.query('select BIS from comanda where stato_record="ATTIVO" and posizione="CONTO" and ntav_comanda="' + variabile_tavolo + '" group by id,BIS order by prog_inser asc;', function (b1) {


        b1.forEach(function (e) {
            var bi2 = alasql('select desc_art as desc_sconto,prezzo_un as sconto from comanda where stato_record="ATTIVO" and ntav_comanda="' + variabile_tavolo + '" and BIS="' + e.BIS + '" and posizione="SCONTO" limit 1;');
            var bi3 = alasql('select nump_xml as numero_coperti from comanda  where stato_record="ATTIVO" and ntav_comanda="' + variabile_tavolo + '" and BIS="' + e.BIS + '" order by nump_xml desc limit 1;');
            var bi4 = alasql('select desc_art,prezzo_un as resto,dest_stampa as storicizzazione,perc_iva,prog_inser,VAR,QTAP,categoria,sc,LIB2,portata,quantita,nodo,stampata_sn,sconto_perc,sconto_imp,incassato,costo,operatore,contiene_variante as NCARD1,NCARD2,NCARD3,NCARD4,NCARD5,NSEGN,NEXIT,carta_credito,bancomat from comanda  where ntav_comanda="' + variabile_tavolo + '"  and  desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" and BIS="' + e.BIS + '" limit 1;');
            
            var b2 = bi2[0];
            var b3 = bi3[0];
            var b4 = bi4[0];

            var rec = new Object();
            // SEZIONE BIS
            rec.BIS = e.BIS;
            if (rec.BIS !== "") {
                bis_presente = true;
            }

            // SEZIONE SCONTISTICA
            rec.desc_sconto = "";
            rec.sconto = "";
            if (b2 !== undefined) {
                rec.desc_sconto = b2.desc_sconto;
                rec.sconto = b2.sconto;
            }
            // SEZIONE COPERTI
            var numero_coperti = $("#numero_coperti").val();

            rec.numero_coperti = "";

            if (b3 !== undefined) {
                rec.numero_coperti = b3.numero_coperti;

            }
            console.log("BIS COPERTI 0", "LCS", lettera_conto_split(), "BIS", rec.BIS, "ncop", numero_coperti, "ncop2", rec.numero_coperti, "B3", b3);

            // SEZIONE PAGAMENTI
            rec.desc_art = "";
            rec.perc_iva = "";
            rec.prog_inser = "";
            rec.VAR = "";
            rec.QTAP = "";
            rec.categoria = "";
            rec.LIB2 = "";
            rec.portata = "";
            rec.quantita = "";
            rec.nodo = "";
            rec.stampata_sn = "";
            rec.sconto_perc = "";
            rec.sconto_imp = "";

            rec.incassato = "";
            rec.costo = "";
            rec.operatore = "";
            rec.NCARD1 = "";
            rec.NCARD2 = "";
            rec.NCARD3 = "";
            rec.NCARD4 = "";
            rec.NCARD5 = "";
            rec.NSEGN = "";
            rec.NEXIT = "";
            rec.carta_credito = "";
            rec.bancomat = "";
            rec.storicizzazione = "";
            rec.resto = "";
            if (b4 !== undefined) {
                rec.desc_art = b4.desc_art;
                rec.perc_iva = b4.perc_iva;
                rec.prog_inser = b4.prog_inser;
                rec.VAR = b4.VAR;
                rec.QTAP = b4.QTAP;
                rec.categoria = b4.categoria;
                rec.LIB2 = b4.LIB2;
                rec.portata = b4.portata;
                rec.quantita = b4.quantita;
                rec.nodo = b4.nodo;
                rec.stampata_sn = b4.stampata_sn;
                rec.sconto_perc = b4.sconto_perc;
                rec.sconto_imp = b4.sconto_imp;

                rec.incassato = b4.incassato;
                rec.costo = b4.costo;
                rec.operatore = b4.operatore;
                rec.NCARD1 = b4.NCARD1;
                rec.NCARD2 = b4.NCARD2;
                rec.NCARD3 = b4.NCARD3;
                rec.NCARD4 = b4.NCARD4;
                rec.NCARD5 = b4.NCARD5;
                rec.NSEGN = b4.NSEGN;
                rec.NEXIT = b4.NEXIT;
                rec.carta_credito = b4.carta_credito;
                rec.bancomat = b4.bancomat;
                rec.storicizzazione = b4.storicizzazione;
                rec.resto = b4.resto;
            }


            var d = new Date();
            var Y = d.getFullYear().toString();
            var D = addZero(d.getDate(), 2);
            var M = addZero((d.getMonth() + 1), 2);
            var h = addZero(d.getHours(), 2);
            var m = addZero(d.getMinutes(), 2);
            var s = addZero(d.getSeconds(), 2);
            var ms = addZero(d.getMilliseconds(), 3);
            var data = D + '/' + M + '/' + Y;
            var ora = h + ':' + m + ':' + s;
            //METODO 16-06-2016



            var incassato = 0;
            var autoriz = 0;
            var sconto = '0.00';



            if (rec !== undefined && typeof rec.sconto === 'string' && rec !== "") {
                if (rec.sconto !== undefined && rec.sconto !== "" && rec.sconto.substr(-1) === "%") {

                    //Bisogna fare il calcolo
                    sconto = testa_totale[rec.BIS] / 100 * rec.sconto.substr(1).slice(0, -1);

                    sconto = sconto.toString();
                    console.log("CALCOLO CONTO SCONTO", sconto);
                } else if (rec.sconto !== undefined && rec.sconto !== "") {
                    sconto = rec.sconto.substr(1);
                }
                autoriz = rec.sconto_perc;
            }

            //PER LA SAGRA NON SERVE PERCHE' INCASSA IN AUTOMATICO
            //E POI SE SI RIPRENDE UNA COMANDA DOPO AVER LASCIATO LA L'INCASSO FA CASINO
            //DA VERIFICARE


            if (incasso_auto === 'N')
            {
                if ((rec.incassato !== undefined && rec.incassato !== null && rec.incassato !== 'undefined' && !isNaN(parseFloat(rec.incassato))))
                {
                    incassato = rec.incassato;
                } else
                {
                    incassato = '0,00';
                }
            } else if (incasso_auto === 'S')
            {
                incassato = testa_totale[rec.BIS] - parseFloat(sconto);
                incassato = incassato.toString();
                console.log("CALCOLO CONTO INCASSO", incassato);
            }

            //Nel fare parseFloat bisogna convertire la , in ., altrimenti restituisce il numero senza decimali, se era un incasso non automatico
            incassato = parseFloat(conv_prz_fc_ibr(incassato)).toFixed(2).replace('.', ',');

            if (incasso_definito !== undefined) {
                incassato = incasso_definito;
            }

            if (rec.BIS === lettera_conto_split()) {

                rec.incassato = incassato;


                if (numero_coperti !== undefined && !isNaN(numero_coperti) && numero_coperti !== "")
                {
                    testa_totale[rec.BIS] += parseInt(numero_coperti) * parseFloat(comanda.valore_coperto);


                    if (parseInt(numero_coperti) - comanda.ultimi_coperti_importati !== 0) {
                        annulla_comanda = false;
                    }
                }
            } else
            {
                numero_coperti = rec.numero_coperti;

            }

            sconto = parseFloat(conv_prz_fc_ibr(sconto)).toFixed(2).replace('.', ',');

            var prg;
            if (rec.prog_inser) {
                prg = aggZero(rec.prog_inser, 3);
            } else if (result[result.length - 1] !== undefined)
            {
                //Aggiunge 1 all'ultimo progressivo del corpo
                prg = aggZero(parseInt(result[result.length - 1].prog_inser) + 1, 3);
            } else {
                //Se non c'ÃƒÆ’Ã‚Â¨ niente in testa o nel corpo (ECCEZIONE)
                prg = 1;
            }


            if (autoriz.toString() != "NaN") {
                autoriz = "";
            }

            var DES = "RECORD TESTA";
            if (b4 && b4.desc_art) {
                DES = b4.desc_art;
            } else if (comanda.set_tid === true) {
                DES += " TID=" + comanda.TID;

            }

            if (rec.BIS === "" && parseFloat(testa_totale[rec.BIS]).toFixed(2).replace(".", ",") === rec.incassato && rec.incassato !== "0,00")
            {
                colore_azzurro = true;
            }

            let totale_testa = "0,00";
            if (!isNaN(parseFloat(testa_totale[rec.BIS]))) {
                totale_testa = parseFloat(testa_totale[rec.BIS]).toFixed(2).replace(".", ",");
            } else {
                bootbox.alert("Attenzione: il totale della testa ha un valore errato.");
                annulla_comanda2 = true;
            }

            let totale_incassato = "";
            if (!isNaN(parseFloat(rec.incassato)) || rec.incassato.trim() === "") {
                totale_incassato = rec.incassato;
            } else {
                bootbox.alert("Attenzione: l incasso ha un valore errato.");
                annulla_comanda2 = true;
            }

            let totale_carta_credito = "";
            if (!isNaN(parseFloat(rec.carta_credito)) || rec.carta_credito.trim() === "") {
                totale_carta_credito = rec.carta_credito;
            } else {
                bootbox.alert("Attenzione: il valore della carta di credito ha un valore errato.");
                annulla_comanda2 = true;
            }

            let totale_bancomat = "";
            if (!isNaN(parseFloat(rec.bancomat)) || rec.bancomat.trim() === "") {
                totale_bancomat = rec.bancomat;
            } else {
                bootbox.alert("Attenzione: il  7un valore errato.");
                annulla_comanda2 = true;
            }

            function readTextFile(File){
                var rawFile = new XMLHttpRequest();
                rawFile.open("GET",File,false);
                rawFile.onreadystatechange = function (){
                    if(rawFile.readyState === 4){
                        if(rawFile.status === 200 || rawFile.status == 0){
                            var allText = rawFile.responseText;
                            alert(allText);
                        }
                    }
                }
                rawFile.send(null);
            }
            var today = new Date(); 
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();
            today = yyyy +'-'+ mm +'-'+ dd;
           
            let xmlContent = '';

            var request = new XMLHttpRequest();
            request.open("GET", "C:/TAVOLI/DATI/" +  today +"_3"+ "/TAVOLO" + comanda.tavolo +".XML", false);
            xml.setRequestHeader("Content-Type", "text/xml");
            request.send();
            var xml = request.responseXML;
            var users = xml.getElementsByTagName("TAVOLI");
            for(var i = 0; i < users.length; i++) {
                var user = users[i];
                var names = user.getElementsByTagName("CLI");
                for(var j = 0; j < names.length; j++) {
                    alert(names[j].childNodes[0].nodeValue);
                }
            }





           
            var xml = new XMLHttpRequest();
            xml.open('GET',"C:/TAVOLI/DATI/" +  today +"_3"+ "/TAVOLO" + comanda.tavolo +".XML",false);
           
            xml.send();
            var xmldata=xml.responseXML;
            document.write(xmldata);
            fetch("C:/TAVOLI/DATI/" +  today + "/TAVOLO" + comanda.tavolo +".XML").then((response)=> {
                response.text().then((xml)=>{
                    xmlContent = xml;
                    let parser = new DOMParser();
                    let xmlDOM = parser.parseFromString(xmlContent, 'application/xml');
                    let tavoli = xmlDOM.querySelectorAll('TAVOLI');
                    tavoli.forEach(bookXmlNode => {
                    });
                });
            });
            var xmlDoc = readTextFile("C:/TAVOLI/DATI/" +  today + "/TAVOLO" + comanda.tavolo +".XML" );
            parser = new DOMParser();
            xmlDoc = parser.parseFromString(text, "text/xml");
            var id = console.log(xmlDoc.getElementByTagName("CLI").at(-1).childNodes[0].nodeValue);

            output += '<TAVOLI>\r\n';
            output += '<TC>T</TC>\r\n';
            output += '<DATA>' + data_inizio_servizio + '</DATA>\r\n';
            output += '<TAVOLO>' + aggZero(variabile_tavolo, 3) + '</TAVOLO>\r\n';
            output += '<BIS>' + rec.BIS + '</BIS>\r\n';
            output += '<PRG>' + prg + '</PRG>\r\n';
            output += '<ART />\r\n';
            output += '<ART2>' + ora + '</ART2>\r\n';
            output += '<DES>' + DES + '</DES>\r\n';
            output += '<PRZ>' + rec.resto + '</PRZ>\r\n';
            output += '<COSTO>' + rec.costo + '</COSTO>\r\n';
            output += '<QTA>' + rec.quantita + '</QTA>\r\n';
            output += '<SN>N</SN>\r\n';
            output += '<CAT>' + rec.categoria + '</CAT>\r\n';
            output += '<TOTALE>' + totale_testa + '</TOTALE>\r\n';
            output += '<SCONTO>' + sconto + '</SCONTO>\r\n';
            output += '<AUTORIZ>' + autoriz + '</AUTORIZ>\r\n';
            output += '<INC>' + totale_incassato + '</INC>\r\n';
            output += '<CARTACRED>' + totale_carta_credito + '</CARTACRED>\r\n';
            output += '<BANCOMAT>' + totale_bancomat + '</BANCOMAT>\r\n';
            output += '<ASSEGNI>' + rec.perc_iva + '</ASSEGNI>\r\n';
            output += '<NCARD1>' + rec.NCARD1 + '</NCARD1>\r\n';
            output += '<NCARD2>' + rec.NCARD2 + '</NCARD2>\r\n';
            output += '<NCARD3>' + rec.NCARD3 + '</NCARD3>\r\n';
            output += '<NCARD4>' + rec.NCARD4 + '</NCARD4>\r\n';
            output += '<NCARD5>' + rec.NCARD5 + '</NCARD5>\r\n';
            output += '<NSEGN>' + rec.NSEGN + '</NSEGN>\r\n';
            output += '<NEXIT>' + rec.NEXIT + '</NEXIT>\r\n';
            output += '<TS>' + rec.storicizzazione + '</TS>\r\n';
            output += '<NOME>' + comanda.operatore + '</NOME>\r\n';
            output += '<ORA>' + ora + '</ORA>\r\n';
            output += '<LIB1>1</LIB1>\r\n';
            output += '<LIB2>' + rec.LIB2 + '</LIB2>\r\n';
            output += '<LIB3>0</LIB3>\r\n';
            output += '<LIB4>0</LIB4>\r\n';
            output += '<LIB5/>\r\n';
            output += '<CLI/>'+id+'<CLI/>\r\n';
            output += '<QTAP>' + rec.QTAP + '</QTAP>\r\n';
            output += '<NODO>' + rec.nodo + '</NODO>\r\n';
            output += '<PORTATA>' + rec.portata + '</PORTATA>\r\n';
            output += '<NUMP>' + numero_coperti + '</NUMP>\r\n';
            output += '<TAVOLOTXT>' + comanda.tavolotxt + '</TAVOLOTXT>\r\n';
            output += '<ULTPORT>' + rec.ultima_portata + '</ULTPORT>\r\n';
            output += '<AGG_GIAC>N</AGG_GIAC>\r\n';
            output += '<VAR>' + rec.VAR + '</VAR>\r\n';
            output += '</TAVOLI>\r\n';
        });

        if (annulla_comanda2 === true) {
            return false;
        }

        output += '</dataroot>';
        output = output.replace(/>null</gi, '><');
        output = output.replace(/>undefined</gi, '><');
        var nome_file = "/TAVOLO" + aggZero(variabile_tavolo, 3) + ".XM_";
        console.log("OUTPUT XML TAVOLO" + comanda.tavolotxt, output);
        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].nome_file_sds = "_" + comanda.folder_number + "/" + nome_file;
        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].contenuto_file_sds = output;
        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].tipo_file_sds = "ROOT";
        comanda.xml.salva_file_xml("_" + comanda.folder_number + "/" + nome_file, output, "ROOT", function () {

            var numeretto = '';
            totale = parseFloat($(comanda.totale_scontrino).html());
            console.log("TOTALE", totale);

            if (comanda.discoteca === true) {
                if ((totale >= 0 || isNaN(totale)) && totale <= comanda.SCAGL1)
                {
                    numeretto = '.1';
                }

                if (totale > comanda.SCAGL1 && totale <= comanda.SCAGL2)
                {
                    numeretto = '.2';
                }

                if (totale > comanda.SCAGL2) {
                    numeretto = '.3';
                }
            } else
            {
                numeretto = '.1';
            }

            if (tipo === "conto" || tipo === "CONTO") {
                numeretto = '.7';
            }

            if (incasso_definito !== undefined && testa_totale[""] === incasso_definito) {
                numeretto = '.4';
            }

            if (colore_azzurro === true) {
                numeretto = '.4';
            }

            if (annulla_comanda === false || numeretto === ".4") {

                if (bis_presente === true) {
                    numeretto = ".8";
                }

                var nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + numeretto;
                oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].nome_file_planing = "_" + comanda.folder_number + "/" + nome_file;
                oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].contenuto_file_planing = "";
                oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].tipo_file_planing = "PLANING";
                comanda.xml.salva_file_xml("_" + comanda.folder_number + "/" + nome_file, "", "PLANING", function () {

                    console.log("esporta_xml_tavolo 1");
                    if (typeof (cB) === "function") {
                        console.log("esporta_xml_tavolo 2");
                        cB(true);
                    }

                });
            } else
            {
                if (typeof (cB) === "function") {
                    console.log("esporta_xml_tavolo 2");
                    cB(true);
                }
            }

        });
    });

}

function stampa_comanda(tipo) {


    var avviso_coperti_differenziati = true;
    var supero_10 = false;
    var bis_presente = false;
    $('#conto tr:not(.non-contare-riga) td:first-child').each(function (e, a) {
        if (a.innerText > 10) {
            supero_10 = true;
        }
    });
    $('.tasto_bestellung_konto').css('pointer-events', 'none');
    $('.tasto_bestellung').css('pointer-events', 'none');
    $('.tasto_konto').css('pointer-events', 'none');

    $('.tasto_quittung').css('pointer-events', 'none');
    $('.tasto_rechnung').css('pointer-events', 'none');

    $('.tasto_bargeld').css('pointer-events', 'none');


    var promessa_confirm = new Promise(function (resolve, reject) {
        if (supero_10 === true && comanda.licenza_vera === "MaximilianAlPonte") {
            bootbox.confirm("I 10 articoli sono stati superati. Vuoi procedere lo stesso?", function (risposta_confirm) {
                if (risposta_confirm === true) {
                    if (comanda.coperti_obbligatorio === '1' && ($("#numero_coperti").val() === '' || $("#numero_coperti").val() === '0'))
                    {
                        bootbox.confirm("Mancano i coperti. Vuoi procedere lo stesso?", function (risposta_confirm) {
                            if (risposta_confirm === true) {
                                resolve(true);
                            } else
                            {
                                resolve(false);
                                $('.tasto_bestellung_konto').css('pointer-events', '');
                                $('.tasto_bestellung').css('pointer-events', '');
                                $('.tasto_konto').css('pointer-events', '');

                                $('.tasto_quittung').css('pointer-events', '');
                                $('.tasto_rechnung').css('pointer-events', '');

                                $('.tasto_bargeld').css('pointer-events', '');
                            }
                        });
                    } else
                    {
                        resolve(true);
                    }
                } else
                {
                    resolve(false);
                    $('.tasto_bestellung_konto').css('pointer-events', '');
                    $('.tasto_bestellung').css('pointer-events', '');
                    $('.tasto_konto').css('pointer-events', '');

                    $('.tasto_quittung').css('pointer-events', '');
                    $('.tasto_rechnung').css('pointer-events', '');

                    $('.tasto_bargeld').css('pointer-events', '');
                }
            });
        } else if (comanda.avviso_coperti_comanda === '1' && comanda.coperti_obbligatorio === '1' && ($("#numero_coperti").val() === '' || $("#numero_coperti").val() === '0'))
        {
            bootbox.confirm("Mancano i coperti. Vuoi procedere lo stesso?", function (risposta_confirm) {
                if (risposta_confirm === true) {
                    resolve(true);
                } else
                {
                    resolve(false);
                    $('.tasto_bestellung_konto').css('pointer-events', '');
                    $('.tasto_bestellung').css('pointer-events', '');
                    $('.tasto_konto').css('pointer-events', '');

                    $('.tasto_quittung').css('pointer-events', '');
                    $('.tasto_rechnung').css('pointer-events', '');

                    $('.tasto_bargeld').css('pointer-events', '');
                }
            });
        } else
        {
            resolve(true);
        }

    });

    promessa_confirm.then(function (risposta_confirm) {
        if (risposta_confirm === true) {

            var variabile_tavolo = comanda.tavolo;

            salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + aggZero(new Date().getMilliseconds(), 3) + " # INIZIO STAMPA COMANDA # TAVOLO: " + variabile_tavolo + " # ");



            oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)] = new Object();

            var annulla_comanda = true;
            var annulla_comanda2 = false;


            $('#body_replacement').css('pointer-events', 'none');
            $('#body_replacement').css('opacity', '0.6');

            //KS0 GERMANIA
            //C ITALIA
            //S CONTO CHE CHIUDE E STORICIZZA AUTOMATICO

            //VALORI DEFAULT
            var storicizzazione = 'N';
            var settaggio_comandapiuconto = 'N';
            var incasso_auto = 'N';
            var BIS = '';

            if (comanda.split_abilitato === true) {
                if (lettera_conto_split().length >= 1)
                {
                    BIS = "-" + lettera_conto_split();
                }
            }

            if (comanda.storicizzazione_auto === 'S')
            {
                storicizzazione = "CS";
            } else if (comanda.storicizzazione_auto === 'N')
            {
                if (comanda.comanda_conto === 'S')
                {
                    //ELIMINATO 26 OTTOBRE 2016
                    //PRIMA ERA "C" MA FACEVA 2 CONTI
                    storicizzazione = " ";
                } else
                {
                    storicizzazione = " ";
                }
            }

            if (comanda.comanda_conto === 'S')
            {
                settaggio_comandapiuconto = "S";
                var lib5 = 'S';
            } else if (comanda.comanda_conto === 'N')
            {
                settaggio_comandapiuconto = " ";
                var lib5 = ' ';
            }

            if (comanda.incasso_auto === 'S')
            {
                incasso_auto = "S";
            } else if (comanda.incasso_auto === 'N')
            {
                incasso_auto = "N";
            }

            var ultima_portata_non_stampata = "N";

            var d = new Date();
            var Y = d.getFullYear().toString();
            var D = addZero(d.getDate(), 2);
            var M = addZero((d.getMonth() + 1), 2);
            var h = addZero(d.getHours(), 2);
            var m = addZero(d.getMinutes(), 2);
            var s = addZero(d.getSeconds(), 2);
            var ms = addZero(d.getMilliseconds(), 3);
            var data = D + '/' + M + '/' + Y;
            var ora = h + ':' + m + ':' + s;
            var variabile = '';
            var specifica_comanda = "";

            /*if (comanda.storicizzazione_auto === 'N') {
             specifica_comanda = "and (stampata_sn is null OR stampata_sn='N' OR stampata_sn='n' OR stampata_sn='') ";
             }*/

            specifica_comanda = '';

            var testo_query = " select NCARD3,AGG_GIAC,LIB4,LIB2,dest_stampa_2,carta_credito,ora_,operatore,sconto_perc,perc_iva,BIS,NSEGN,QTAP,ordinamento,ultima_portata,peso_ums,cod_articolo,dest_stampa,contiene_variante,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita,stampata_sn,tipo_ricevuta from comanda where desc_art NOT LIKE 'RECORD TESTA%' and  contiene_variante='1' and ntav_comanda='" + variabile_tavolo + "' and posizione='CONTO' and stato_record='ATTIVO' " + specifica_comanda + " \n\
                            union all\n\
                                select NCARD3,AGG_GIAC,LIB4,LIB2,dest_stampa_2,carta_credito,ora_,operatore,sconto_perc,perc_iva,BIS,NSEGN,QTAP,ordinamento,ultima_portata,peso_ums,cod_articolo,dest_stampa,contiene_variante,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita,stampata_sn,tipo_ricevuta from comanda where desc_art NOT LIKE 'RECORD TESTA%' and  (contiene_variante !='1' or contiene_variante is null) and length(nodo)=3 and ntav_comanda='" + variabile_tavolo + "' and posizione='CONTO' and stato_record='ATTIVO'  " + specifica_comanda + " \n\
                            union all\n\
                                select NCARD3,AGG_GIAC,LIB4,LIB2,dest_stampa_2,carta_credito,ora_,operatore,sconto_perc,perc_iva,BIS,NSEGN,QTAP,ordinamento,ultima_portata,peso_ums,cod_articolo,dest_stampa,contiene_variante,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita,stampata_sn,tipo_ricevuta from comanda where desc_art NOT LIKE 'RECORD TESTA%' and  length(nodo)=7 and ntav_comanda='" + variabile_tavolo + "'  and posizione='CONTO' and stato_record='ATTIVO' " + specifica_comanda + " ;";



            comanda.sincro.query(testo_query, function (result1) {

                var result = alasql("select * from ? order by dest_stampa,portata,nodo,prog_inser", [result1]);

                console.log("QUERY COMANDA RESULT", result);

                if (result.length === 0)
                {
                    if (typeof (cB) === "function") {
                        console.log("esporta_xml_tavolo tavolo inesistente");
                        cB(true);

                    }
                    console.log("QUERY COMANDA BUTTON TAVOLI");
                    //esporta_xml_tavolo("SENZA_AGGIUNTE", function () {

                    if (variabile_tavolo !== undefined && typeof (variabile_tavolo) === "string") {
                        comanda.xml.elimina_tavolo_occupato("_" + comanda.folder_number + "/COND/Tav" + variabile_tavolo.replace(/^0+/, '') + ".CO1", false, function () {
                            btn_tavoli("CONTO");
                        });
                    }
                    //});
                    return false;
                }

                //console.log("result", result);

                //INTESTAZIONE
                var output = '<?xml version="1.0" standalone="yes"?>\r\n\
                        <dataroot xmlns="http://www.w3.org/2001/TAVOLI.xsd">\r\n';
                var i = 0;
                var cod_articolo_princ;
                var ts_princ;
                var prog_principale;
                var descrizione_articolo;
                var ordinamento = '';
                var testa_totale = new Object();
                var qta_princ = 1;


                result.forEach(function (obj) {
                    if (obj.prezzo_un.trim() === "") {
                        obj.prezzo_un = "0,00";
                    }
                    ////console.log(obj);

                    //PRIMA ERA +1 MA NON SE NE CAPISCE IL MOTIVO
                    obj.prog_inser = parseInt(obj.prog_inser);

                    if (obj.BIS === BIS) {
                        obj.dest_stampa !== undefined ? ts_princ = obj.dest_stampa : ts_princ = '1';
                    } else
                    {
                        ts_princ = obj.dest_stampa;
                    }

                    obj.peso_ums !== undefined ? obj.peso_ums : obj.peso_ums = '';
                    obj.ordinamento !== undefined ? ordinamento = obj.ordinamento : ordinamento = '01';
                    prog_principale = aggZero(obj.prog_inser, 3);
                    descrizione_articolo = obj.desc_art;
                    console.log(obj.dest_stampa);
                    console.log(obj.dest_stampa !== undefined);
                    console.log(ts_princ);
                    var contiene_variante = '';
                    obj.contiene_variante === '1' ? contiene_variante = 'SI' : contiene_variante = '';
//---
                    if (obj.desc_art[0] === "+" || obj.desc_art[0] === "-" || obj.desc_art[0] === "x" || obj.desc_art[0] === "*" || obj.desc_art[0] === "=")
                    {
                        prog_principale = aggZero(prog_principale.substring(0, 3), 3) + ' ' + aggZero(obj.prog_inser, 3) + ' ';
                        descrizione_articolo = obj.desc_art.substring(1);
                        variabile = obj.desc_art[0];
                    } else if (obj.desc_art[0] === "(") {
                        prog_principale = aggZero(prog_principale.substring(0, 3), 3) + ' ' + aggZero(obj.prog_inser, 3) + ' ';
                        variabile = "*";
                    } else
                    {
                        cod_articolo_princ = obj.cod_articolo;
                        variabile = '';
                    }

                    var stampata_sn = 'N';
                    if (obj.stampata_sn === 'S')
                    {
                        stampata_sn = 'S';
                    }

                    var tipo_ricevuta = '';
                    if (obj.tipo_ricevuta === 'SERVITO')
                    {
                        tipo_ricevuta = 'SERVITO';
                    }

                    var qtap = '0';
                    if (!isNaN(obj.QTAP) && obj.QTAP > 0) {
                        qtap = obj.QTAP;
                    }

                    var nsegn = '0';
                    if (!isNaN(obj.NSEGN) && obj.NSEGN > 0) {
                        nsegn = obj.NSEGN;
                    }

                    var ncard3 = '';
                    if (obj.NCARD3) {
                        ncard3 = obj.NCARD3;
                    }

                    if (tipo_ricevuta === 'SERVITO' || stampata_sn === 'N' || descrizione_articolo === "MEMO INC,SCONTO,DRINK,LISTINO,COPERTI" || descrizione_articolo.indexOf(" hg.") !== -1)
                    {
                        annulla_comanda = false;

                        var ora = h + ':' + m + ':' + s;

                        if (obj.ultima_portata === "S") {
                            ultima_portata_non_stampata = "S";
                        }
                    } else
                    {
                        var ora = obj.ora_;
                    }

                    var operatore = obj.operatore;
                    if (operatore.length === 0) {
                        operatore = comanda.operatore;
                    }

                    if (testa_totale[obj.BIS] === undefined) {
                        testa_totale[obj.BIS] = 0;
                    }
                    testa_totale[obj.BIS] += parseFloat(conv_prz_fc_ibr(obj.prezzo_un)) * parseInt(obj.quantita);


                    if (comanda.conteggio_coperti_differenziati === true) {
                        if (comanda.conteggio_coperti_differenziati_COD1 === obj.cod_articolo) {
                            avviso_coperti_differenziati = false;
                        }
                        if (comanda.conteggio_coperti_differenziati_COD2 === obj.cod_articolo) {
                            avviso_coperti_differenziati = false;
                        }
                        if (comanda.conteggio_coperti_differenziati_COD3 === obj.cod_articolo) {
                            avviso_coperti_differenziati = false;
                        }
                    }

                    var ncard3 = '';
                    if (obj.NCARD3) {
                        ncard3 = obj.NCARD3;
                    }

                    let tab_iva = alasql("select aliquota from tabella_iva where lingua='" + comanda.lingua_stampa + "';");
                    let percentuali_iva = tab_iva.map(d => d.aliquota);

                    if (percentuali_iva.indexOf(obj.perc_iva.trim()) === -1) {
                        /*bootbox.alert("Attenzione: l'IVA dell'articolo " + descrizione_articolo + " non &egrave; presente nella tabella IVA. Modificalo e ri-lancia l'operazione");
                         annulla_comanda2 = true;
                         return false;*/

                        obj.perc_iva = comanda.percentuale_iva_default;
                    } else if (isNaN(parseFloat(conv_prz_fc_ibr(obj.prezzo_un)).toFixed(2))) {
                        bootbox.alert("Attenzione: il prezzo dell'articolo " + descrizione_articolo + " non &egrave; valido. Modificalo e ri-lancia l'operazione");
                        annulla_comanda2 = true;
                        return false;
                    } else if (isNaN(parseInt(obj.quantita)) || parseInt(obj.quantita) < 1) {
                        bootbox.alert("Attenzione: la quantit&agrave; dell'articolo " + descrizione_articolo + " non &egrave; valida. Modificala e ri-lancia l'operazione");
                        annulla_comanda2 = true;
                        return false;
                    }

                    output += '<TAVOLI>\r\n';
                    output += '<TC>C</TC>\r\n';
                    output += '<DATA>' + data_inizio_servizio + '</DATA>\r\n';
                    output += '<TAVOLO>' + aggZero(variabile_tavolo, 3) + '</TAVOLO>\r\n';
                    output += '<BIS>' + obj.BIS + '</BIS>\r\n';
                    output += '<PRG>' + aggZero(obj.prog_inser, 3) + '</PRG>\r\n';
                    output += '<ART>' + cod_articolo_princ + '</ART>\r\n';
                    output += '<ART2>' + obj.cod_articolo + '</ART2>\r\n';
                    output += '<VAR>' + variabile + '</VAR>\r\n';
                    output += '<DES>' + descrizione_articolo + '</DES>\r\n';
                    output += '<PRZ>' + parseFloat(conv_prz_fc_ibr(obj.prezzo_un)).toFixed(2).replace('.', ',') + '</PRZ>\r\n';
                    output += '<COSTO>0</COSTO>\r\n';
                    output += '<QTA>' + obj.quantita + '</QTA>\r\n';
                    output += '<SN>' + stampata_sn + '</SN>\r\n';
                    output += '<CAT>' + obj.categoria + '</CAT>\r\n';
                    output += '<TOTALE />\r\n';
                    output += '<SCONTO />\r\n';
                    output += '<AUTORIZ>' + obj.sconto_perc + '</AUTORIZ>\r\n';
                    output += '<INC>0</INC>\r\n';
                    output += '<CARTACRED>' + obj.carta_credito + '</CARTACRED>\r\n';
                    output += '<BANCOMAT/>\r\n';
                    output += '<ASSEGNI>' + obj.perc_iva + '</ASSEGNI>\r\n';
                    output += '<NCARD1>' + contiene_variante + '</NCARD1>\r\n';
                    output += '<NCARD2/>\r\n';
                    output += '<NCARD3>' + ncard3 + '</NCARD3>\r\n';
                    output += '<NCARD4/>\r\n';
                    output += '<NCARD5>' + ordinamento + '</NCARD5>\r\n'; //ORDINAMENTO
                    output += '<NSEGN>' + nsegn + '</NSEGN>\r\n';
                    output += '<NEXIT/>\r\n';
                    output += '<TS>' + ts_princ + '</TS>\r\n'; //' + ts_princ + '
                    output += '<NOME>' + operatore + '</NOME>\r\n';
                    output += '<ORA>' + ora + '</ORA>\r\n';
                    output += '<LIB1>1</LIB1>\r\n';
                    output += '<LIB2>' + obj.LIB2 + '</LIB2>\r\n';
                    output += '<LIB3/>\r\n';
                    output += '<LIB4>' + obj.LIB4 + '</LIB4>\r\n';
                    output += '<LIB5>' + obj.dest_stampa_2 + '</LIB5>\r\n';
                    output += '<CLI/>\r\n';
                    output += '<QTAP>' + qtap + '</QTAP>\r\n';
                    output += '<NODO>' + obj.nodo + '</NODO>\r\n';
                    output += '<PORTATA>' + obj.portata + '</PORTATA>\r\n';
                    output += '<NUMP>0</NUMP>\r\n';
                    output += '<TAVOLOTXT>' + comanda.tavolotxt + '</TAVOLOTXT>\r\n';
                    output += '<ULTPORT>' + obj.ultima_portata + '</ULTPORT>\r\n';
                    output += '<AGG_GIAC>' + obj.AGG_GIAC + '</AGG_GIAC>\r\n';
                    output += '</TAVOLI>\r\n';
                    i++;
                });

                if (annulla_comanda2 === true) {
                    $('.tasto_bestellung').css('pointer-events', '');
                    $('.tasto_konto').css('pointer-events', '');

                    $('.tasto_quittung').css('pointer-events', '');
                    $('.tasto_rechnung').css('pointer-events', '');

                    $('.tasto_bargeld').css('pointer-events', '');
                    return false;
                } else if (comanda.conteggio_coperti_differenziati === true && avviso_coperti_differenziati === true) {
                    bootbox.confirm("Mancano i coperti manuali!!!<br>Vuoi continuare lo stesso?", function (conf_cop) {
                        if (conf_cop === true) {
                            f2();
                        } else {
                            $('.tasto_bestellung').css('pointer-events', '');
                            $('.tasto_konto').css('pointer-events', '');

                            $('.tasto_quittung').css('pointer-events', '');
                            $('.tasto_rechnung').css('pointer-events', '');

                            $('.tasto_bargeld').css('pointer-events', '');
                        }
                    });
                } else {
                    f2();
                }

                function f2() {

                    comanda.sincro.query('select BIS from comanda where stato_record="ATTIVO" and posizione="CONTO" and ntav_comanda="' + variabile_tavolo + '" group by id,BIS order by prog_inser asc;', function (b1) {


                        b1.forEach(function (e) {
                            var bi2 = alasql('select desc_art as desc_sconto,prezzo_un as sconto from comanda where stato_record="ATTIVO" and ntav_comanda="' + variabile_tavolo + '" and BIS="' + e.BIS + '" and posizione="SCONTO" limit 1;');
                            var bi3 = alasql('select nump_xml as numero_coperti from comanda  where stato_record="ATTIVO" and ntav_comanda="' + variabile_tavolo + '" and BIS="' + e.BIS + '" order by nump_xml desc limit 1;');
                            var bi4 = alasql('select desc_art,LIB2,dest_stampa as storicizzazione,prezzo_un,incassato,costo,operatore,contiene_variante as NCARD1,NCARD2,NCARD3,NCARD4,NCARD5,NSEGN,NEXIT,carta_credito,bancomat from comanda  where ntav_comanda="' + variabile_tavolo + '"  and  desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" and BIS="' + e.BIS + '" limit 1;');
                            var b2 = bi2[0];
                            var b3 = bi3[0];
                            var b4 = bi4[0];

                            var rec = new Object();
                            // SEZIONE BIS
                            rec.BIS = e.BIS;
                            if (rec.BIS !== "") {
                                bis_presente = true;
                            }
                            // SEZIONE SCONTISTICA
                            rec.desc_sconto = "";
                            rec.sconto = "";
                            if (b2 !== undefined) {
                                rec.desc_sconto = b2.desc_sconto;
                                rec.sconto = b2.sconto;
                            }
                            // SEZIONE COPERTI
                            var numero_coperti = $("#numero_coperti").val();
                            rec.numero_coperti = "";

                            if (b3 !== undefined) {
                                rec.numero_coperti = b3.numero_coperti;

                            }
                            console.log("BIS COPERTI 0", "LCS", lettera_conto_split(), "BIS", rec.BIS, "ncop", numero_coperti, "ncop2", rec.numero_coperti, "B3", b3);

                            // SEZIONE PAGAMENTI
                            rec.incassato = "";
                            rec.costo = "";
                            rec.operatore = "";
                            rec.NCARD1 = "";
                            rec.NCARD2 = "";
                            rec.NCARD3 = "";
                            rec.NCARD4 = "";
                            rec.NCARD5 = "";
                            rec.NSEGN = "";
                            rec.NEXIT = "";
                            rec.LIB2 = "";
                            rec.carta_credito = "";
                            rec.bancomat = "";
                            rec.storicizzazione = "";
                            rec.desc_art = "";
                            if (b4 !== undefined) {
                                rec.LIB2 = b4.LIB2;
                                rec.incassato = b4.incassato;
                                rec.costo = b4.costo;
                                rec.operatore = b4.operatore;
                                rec.NCARD1 = b4.NCARD1;
                                rec.NCARD2 = b4.NCARD2;
                                rec.NCARD3 = b4.NCARD3;
                                rec.NCARD4 = b4.NCARD4;
                                rec.NCARD5 = b4.NCARD5;
                                rec.NSEGN = b4.NSEGN;
                                rec.NEXIT = b4.NEXIT;
                                rec.carta_credito = b4.carta_credito;
                                rec.bancomat = b4.bancomat;
                                rec.storicizzazione = b4.storicizzazione;
                                rec.desc_art = b4.desc_art;
                            }




                            var incassato = 0;



                            var autoriz = 0;


                            var sconto = '0.00';



                            if (rec !== undefined && typeof rec.sconto === 'string' && rec !== "") {
                                if (rec.sconto !== undefined && rec.sconto !== "" && rec.sconto.substr(-1) === "%") {

                                    //Bisogna fare il calcolo
                                    sconto = testa_totale[rec.BIS] / 100 * rec.sconto.substr(1).slice(0, -1);


                                    sconto = sconto.toString();
                                    console.log("CALCOLO CONTO SCONTO", sconto);
                                } else if (rec.sconto !== undefined && rec.sconto !== "") {
                                    sconto = rec.sconto.substr(1);

                                }
                                autoriz = rec.desc_sconto;


                            }

                            //PER LA SAGRA NON SERVE PERCHE' INCASSA IN AUTOMATICO
                            //E POI SE SI RIPRENDE UNA COMANDA DOPO AVER LASCIATO LA L'INCASSO FA CASINO
                            //DA VERIFICARE




                            if (incasso_auto === 'N')
                            {
                                if ((rec.incassato !== undefined && rec !== null && rec.incassato !== 'undefined' && !isNaN(parseFloat(rec.incassato))))
                                {
                                    incassato = rec.incassato;
                                } else
                                {
                                    incassato = '0,00';
                                }
                            } else if (incasso_auto === 'S')
                            {

                                incassato = testa_totale[rec.BIS] - parseFloat(sconto);
                                incassato = incassato.toString();

                                console.log("CALCOLO CONTO INCASSO", incassato);

                            }

                            //Nel fare parseFloat bisogna convertire la , in ., altrimenti restituisce il numero senza decimali, se era un incasso non automatico
                            incassato = parseFloat(conv_prz_fc_ibr(incassato)).toFixed(2).replace('.', ',');
                            sconto = parseFloat(conv_prz_fc_ibr(sconto)).toFixed(2).replace('.', ',');


                            if (rec.BIS === lettera_conto_split()) {

                                rec.incassato = incassato;



                                rec.storicizzazione = storicizzazione;

                                if (numero_coperti !== undefined && !isNaN(numero_coperti) && numero_coperti !== "")
                                {
                                    testa_totale[rec.BIS] += parseInt(numero_coperti) * parseFloat(comanda.valore_coperto);


                                    if (parseInt(numero_coperti) - comanda.ultimi_coperti_importati !== 0) {
                                        annulla_comanda = false;
                                    }
                                }
                            } else
                            {
                                numero_coperti = rec.numero_coperti;
                            }


                            testa_totale[rec.BIS] = parseFloat(testa_totale[rec.BIS]).toFixed(2).replace(".", ",");

                            if (autoriz.toString() != "NaN") {
                                autoriz = "";
                            }

                            var DES = "RECORD TESTA";
                            if (b4 && b4.desc_art) {
                                DES = b4.desc_art;
                            } else if (comanda.set_tid === true) {
                                DES += " TID=" + comanda.TID;

                            }

                            let totale_testa = "0,00";
                            if (!isNaN(parseFloat(testa_totale[rec.BIS]))) {
                                totale_testa = testa_totale[rec.BIS];
                            } else {
                                bootbox.alert("Attenzione: il totale della testa ha un valore errato.");
                                annulla_comanda2 = true;
                            }

                            let totale_incassato = "";
                            if (!isNaN(parseFloat(rec.incassato)) || rec.incassato.trim() === "") {
                                totale_incassato = rec.incassato;
                            } else {
                                bootbox.alert("Attenzione: l incasso ha un valore errato.");
                                annulla_comanda2 = true;
                            }

                            let totale_carta_credito = "";
                            if (!isNaN(parseFloat(rec.carta_credito)) || rec.carta_credito.trim() === "") {
                                totale_carta_credito = rec.carta_credito;
                            } else {
                                bootbox.alert("Attenzione: il valore della carta di credito ha un valore errato.");
                                annulla_comanda2 = true;
                            }

                            let totale_bancomat = "";
                            if (!isNaN(parseFloat(rec.bancomat)) || rec.bancomat.trim() === "") {
                                totale_bancomat = rec.bancomat;
                            } else {
                                bootbox.alert("Attenzione: il  bancomat ha un valore errato.");
                                annulla_comanda2 = true;
                            }

                            output += '<TAVOLI>\r\n\
                <TC>T</TC>\r\n\
                <DATA>' + data_inizio_servizio + '</DATA>\r\n\
                <TAVOLO>' + aggZero(variabile_tavolo, 3) + '</TAVOLO>\r\n\
                <BIS>' + rec.BIS + '</BIS>\r\n\
                <PRG>000</PRG>\r\n\
                <ART>01</ART>\r\n\
                <ART2/>\r\n\
                <VAR/>\r\n\
                <DES>' + DES + '</DES>\r\n\
                <PRZ/>\r\n\
                <COSTO>' + rec.costo + '</COSTO>\r\n\
                <QTA/>\r\n\
                <SN>N</SN>\r\n\
                <CAT/>\r\n\
                <TOTALE>' + totale_testa + '</TOTALE>\r\n\
                <SCONTO>' + sconto + '</SCONTO>\r\n\
                <AUTORIZ>' + autoriz + '</AUTORIZ>\r\n\
                <INC>' + totale_incassato + '</INC>\r\n\
                <CARTACRED>' + totale_carta_credito + '</CARTACRED>\r\n\
                <BANCOMAT>' + totale_bancomat + '</BANCOMAT>\r\n\
                <ASSEGNI>0,00</ASSEGNI>\r\n\
                <NCARD1>' + rec.NCARD1 + '</NCARD1>\r\n\
                <NCARD2>' + rec.NCARD2 + '</NCARD2>\r\n\
                <NCARD3>' + rec.NCARD3 + '</NCARD3>\r\n\
                <NCARD4>' + rec.NCARD4 + '</NCARD4>\r\n\
                <NCARD5>' + rec.NCARD5 + '</NCARD5>\r\n\
                <NSEGN>' + rec.NSEGN + '</NSEGN>\r\n\
                <NEXIT>' + rec.NEXIT + '</NEXIT>\r\n\
                <TS>' + rec.storicizzazione + '</TS>\r\n\
                <NOME>' + comanda.operatore + '</NOME>\r\n\
                <ORA>' + ora + '</ORA>\r\n\
                <LIB1>1</LIB1>\r\n\
                <LIB2>' + rec.LIB2 + '</LIB2>\r\n\
                <LIB3/>\r\n\
                <LIB4/>\r\n\
                <LIB5>' + lib5 + '</LIB5>\r\n\
                <CLI/>\r\n\
                <QTAP>0</QTAP>\r\n\
                <NODO>' + settaggio_comandapiuconto + '</NODO>\r\n\
                <PORTATA/>\r\n\
                <NUMP>' + numero_coperti + '</NUMP>\r\n\
                <TAVOLOTXT>' + comanda.tavolotxt + '</TAVOLOTXT>\r\n\
                <ULTPORT>N</ULTPORT>\r\n\
                <AGG_GIAC>N</AGG_GIAC>\r\n\
                </TAVOLI>\r\n';
                            //LIB5 S=CONTO STAMPATO

                        });
                        if (annulla_comanda2 === true) {
                            return false;
                        }
                        output += '</dataroot>';
                        output = output.replace(/>null</gi, '><');
                        output = output.replace(/>undefined</gi, '><');

                        console.log("OUTPUT XML CONTO" + comanda.tavolotxt, output);


                        var d = new Date();
                        var Y = d.getFullYear();
                        var M = addZero(d.getDate(), 2);
                        var D = addZero((d.getMonth() + 1), 2);
                        var h = addZero(d.getHours(), 2);
                        var m = addZero(d.getMinutes(), 2);
                        var s = addZero(d.getSeconds(), 2);
                        var ms = addZero(d.getMilliseconds(), 3);
                        var data_tablet = h + '' + m + '' + s;

                        var id_tablet = "29";
                        var id_temp = leggi_get_url('id');

                        if (id_temp !== undefined && id_temp !== null && id_temp !== 'null' && id_temp.length === 2) {
                            id_tablet = id_temp;
                        }

                        var prom_storicizzazione = new Promise(function (resolve, reject) {

                            if (annulla_comanda === false) {

                                var nome_file = "/SDS/PDA" + id_tablet + "_" + data_tablet + BIS + ".XM_";

                                oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].nome_file_sds = "_" + comanda.folder_number + "/" + nome_file;
                                oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].contenuto_file_sds = output;
                                oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].tipo_file_sds = "SDS";

                                comanda.xml.salva_file_xml("_" + comanda.folder_number + "/" + nome_file, output, "SDS", function () {

                                    if (comanda.storicizzazione_auto === 'N')
                                    {
                                        if (comanda.comanda_conto === 'N') {
                                            nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + '.1';

                                            if (comanda.ultima_portata_abilitata === "S" && ultima_portata_non_stampata === 'S') {
                                                nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + '.6';
                                            } else {

                                                if (comanda.discoteca === true) {
                                                    if ((testa_totale[""] > 0 || isNaN(testa_totale[""])) && testa_totale[""] <= comanda.SCAGL1)
                                                    {
                                                        nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + '.1';
                                                    }

                                                    if (testa_totale[""] > comanda.SCAGL1 && testa_totale[""] <= comanda.SCAGL2)
                                                    {
                                                        nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + '.2';
                                                    }

                                                    if (testa_totale[""] > comanda.SCAGL2) {
                                                        nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + '.3';
                                                    }
                                                } else
                                                {
                                                    nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + '.1';
                                                }


                                            }
                                        } else if (comanda.comanda_conto === 'S')
                                        {
                                            if (comanda.licenza_vera === "DeFiorenze") {
                                                nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + '.1';
                                            } else {
                                                if (comanda.incasso_auto === "S" || comanda.incasso_auto === "s") {
                                                    nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + '.4';
                                                } else
                                                {
                                                    nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + '.7';
                                                }
                                            }
                                        }

                                        if (bis_presente === true) {
                                            nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + ".8";
                                        }

                                        console.log("esporta_xml_tavolo", nome_file);

                                        //esporta_xml_tavolo(tipo, function () {

                                        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].nome_file_planing = "_" + comanda.folder_number + "/" + nome_file;
                                        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].contenuto_file_planing = "";
                                        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].tipo_file_planing = "PLANING";

                                        comanda.xml.salva_file_xml("_" + comanda.folder_number + "/" + nome_file, "", "PLANING", function () {
                                            resolve(true);
                                        });
                                        //});
                                    } else if (comanda.storicizzazione_auto === 'S')
                                    {
                                        //esporta_xml_tavolo(tipo, function () {
                                        resolve(true);
                                        //});
                                    }
                                });
                            } else
                            {
                                //IN CASO NON CI SIANO NUOVI DATI IN COMANDA
                                if (variabile_tavolo !== undefined && typeof (variabile_tavolo) === "string") {
                                    comanda.xml.elimina_tavolo_occupato("_" + comanda.folder_number + "/COND/Tav" + variabile_tavolo.replace(/^0+/, '') + ".CO1", false, function () {
                                        resolve(true);
                                    });
                                }
                            }
                        });

                        //Promessa per risolvere il bug del btn tavoli se non la usassi
                        //Serve ad esportare il file del SDS dopo l'esportazione sicura del tavolo
                        prom_storicizzazione.then(function () {

                            var data_intera = comanda.funzionidb.data_attuale();
                            var data = data_intera.substring(0, 10);
                            var ora = data_intera.substring(11, 16);
                            var testo_query = "update comanda set stampata_sn='S',data_stampa='" + data + "',ora_stampa='" + ora + "',fiscalizzata_sn='N' where stato_record='ATTIVO' and ntav_comanda='" + variabile_tavolo + "'";
                            ////console.log(testo_query);
                            comanda.sock.send({tipo: "comanda_stampata", operatore: comanda.operatore, query: testo_query});
                            comanda.sincro.query(testo_query, function () {

                                //---Una volta esportato per certo il file, torna ai tavoli o al parcheggio(impossibile nel caso di Padova)---
                                if (comanda.tavolotxt !== "BANCO" && comanda.tavolotxt !== "BAR" && comanda.tavolotxt.left(7) !== "ASPORTO" && comanda.tavolotxt !== "TAKEAWAY") {

                                    btn_tavoli("CONTO");
                                    $('.tasto_bestellung').css('pointer-events', '');
                                    $('.tasto_konto').css('pointer-events', '');

                                    $('.tasto_quittung').css('pointer-events', '');
                                    $('.tasto_rechnung').css('pointer-events', '');

                                    $('.tasto_bargeld').css('pointer-events', '');

                                } else
                                {
                                    btn_parcheggia();

                                }

                            });
                            var date = new Date();
                            var ora = addZero(date.getHours(), 2) + ':' + addZero(date.getMinutes(), 2);
                            var testo_query = "update tavoli SET colore='4',occupato='0',ora_apertura_tavolo = ( CASE WHEN ( ora_apertura_tavolo = '-' OR ora_apertura_tavolo = 'null' OR ora_apertura_tavolo = ''  OR ora_apertura_tavolo = NULL ) THEN '" + ora + "' ELSE ora_apertura_tavolo END ), ora_ultima_comanda = (  CASE WHEN ( ora_apertura_tavolo = 'null'  OR ora_apertura_tavolo = '' OR ora_apertura_tavolo = '-'  OR ora_apertura_tavolo = NULL ) THEN ora_ultima_comanda ELSE '" + ora + "' END )  where numero = '" + variabile_tavolo + "';";
                            comanda.sock.send({tipo: "comanda_stampata", operatore: comanda.operatore, query: testo_query});
                            comanda.sincro.query(testo_query, function () {
                                disegna_ultimo_tavolo_modificato(testo_query);

                            });
                        });
                    });
                }
            });
        }
    });
    //}).catch(() => console.log("REJECTED"));
}

function stampa_konto(tipo) {

    var avviso_coperti_differenziati = true;
    var annulla_comanda = false;
    var annulla_comanda2 = false;


    var promessa_confirm = new Promise(function (resolve, reject) {

        if (comanda.coperti_obbligatorio === '1' && ($("#numero_coperti").val() === '' || $("#numero_coperti").val() === '0'))
        {
            bootbox.confirm("Mancano i coperti. Vuoi procedere lo stesso?", function (risposta_confirm) {
                if (risposta_confirm === true) {
                    resolve(true);
                } else
                {
                    resolve(false);
                    $('.tasto_bestellung_konto').css('pointer-events', '');
                    $('.tasto_bestellung').css('pointer-events', '');
                    $('.tasto_konto').css('pointer-events', '');

                    $('.tasto_quittung').css('pointer-events', '');
                    $('.tasto_rechnung').css('pointer-events', '');

                    $('.tasto_bargeld').css('pointer-events', '');
                }
            });
        } else
        {
            resolve(true);
        }

    });

    promessa_confirm.then(function (risposta_confirm) {
        if (risposta_confirm === true) {

            var righe_prezzo_zero = new Promise(function (resolve, reject) {
                if (comanda.avviso_righe_prezzo_zero === '1' && ($(comanda.conto).not(comanda.intestazione_conto).find('tr[id^="art_"]:contains("€"):contains("€0.00")').not('.non-contare-riga').length > 0 || $(comanda.conto).not(comanda.intestazione_conto).find('tr[id^="art_"]:contains("€"):contains("€0,00")').not('.non-contare-riga').length > 0)) {
                    bootbox.confirm("Attenzione: ci sono righe a prezzo zero. Vuoi continuare a stampare lo scontrino?", function (a) {

                        if (a === true) {
                            resolve(true);
                        } else
                        {
                            reject();
                        }

                    });
                } else
                {
                    resolve(true);
                }
            });



            righe_prezzo_zero.then(function () {
                $('.tasto_bestellung').css('pointer-events', 'none');
                $('.tasto_konto').css('pointer-events', 'none');

                $('.tasto_quittung').css('pointer-events', 'none');
                $('.tasto_rechnung').css('pointer-events', 'none');

                $('.tasto_bargeld').css('pointer-events', 'none');

                var variabile_tavolo = comanda.tavolo;

                salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + aggZero(new Date().getMilliseconds(), 3) + " # INIZIO STAMPA COMANDA # TAVOLO: " + variabile_tavolo + " # ");

                var numero_coperti = $("#numero_coperti").val();

                oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)] = new Object();


                //KS0 GERMANIA
                //C ITALIA
                //S CONTO CHE CHIUDE E STORICIZZA AUTOMATICO

                //---LEGGO I SETTAGGI---

                //VALORI DEFAULT
                var storicizzazione = 'N';
                var settaggio_comandapiuconto = 'N';
                var incasso_auto = 'N';
                var bis_presente = false;



                if (comanda.storicizzazione_auto === 'S')
                {
                    storicizzazione = "CS";
                } else if (comanda.storicizzazione_auto === 'N')
                {
                    storicizzazione = "C";
                }

                if (comanda.comanda_conto === 'S')
                {
                    settaggio_comandapiuconto = "S";
                } else if (comanda.comanda_conto === 'N')
                {
                    settaggio_comandapiuconto = " ";
                }

                if (comanda.incasso_auto === 'S')
                {
                    incasso_auto = "S";
                } else if (comanda.incasso_auto === 'N')
                {
                    incasso_auto = "N";
                }

                //---Creo la data di adesso dall'anno ai minuti---
                var d = new Date();
                var Y = d.getFullYear().toString();
                var D = addZero(d.getDate(), 2);
                var M = addZero((d.getMonth() + 1), 2);
                var h = addZero(d.getHours(), 2);
                var m = addZero(d.getMinutes(), 2);
                var s = addZero(d.getSeconds(), 2);
                var ms = addZero(d.getMilliseconds(), 3);
                var data = D + '/' + M + '/' + Y;
                var ora = h + ':' + m + ':' + s;

                var BIS = '';

                if (comanda.split_abilitato === true) {
                    if (lettera_conto_split().length >= 1)
                    {
                        BIS = "-" + lettera_conto_split();
                    }
                }

                //---Query per prelevare il conto dal db locale---
                var variabile = '';
                var testo_query = " select NCARD3,AGG_GIAC,LIB4,LIB2,BIS,dest_stampa_2,carta_credito,ora_,operatore,BIS,sconto_perc,perc_iva,ordinamento,ultima_portata,peso_ums,cod_articolo,dest_stampa,contiene_variante,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita,stampata_sn from comanda where desc_art NOT LIKE 'RECORD TESTA%' and  contiene_variante='1' and ntav_comanda='" + variabile_tavolo + "' and posizione='CONTO' and stato_record='ATTIVO'  \n\
                            union all\n\
                            select NCARD3,AGG_GIAC,LIB4,LIB2,BIS,dest_stampa_2,carta_credito,ora_,operatore,BIS,sconto_perc,perc_iva,ordinamento,ultima_portata,peso_ums,cod_articolo,dest_stampa,contiene_variante,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita,stampata_sn from comanda where desc_art NOT LIKE 'RECORD TESTA%' and  (contiene_variante !='1' or contiene_variante is null) and length(nodo)=3 and ntav_comanda='" + variabile_tavolo + "' and posizione='CONTO' and stato_record='ATTIVO'   \n\
                            union all\n\
                            select NCARD3,AGG_GIAC,LIB4,LIB2,BIS,dest_stampa_2,carta_credito,ora_,operatore,BIS,sconto_perc,perc_iva,ordinamento,ultima_portata,peso_ums,cod_articolo,dest_stampa,contiene_variante,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita,stampata_sn from comanda where desc_art NOT LIKE 'RECORD TESTA%' and  length(nodo)=7 and ntav_comanda='" + variabile_tavolo + "'  and posizione='CONTO' and stato_record='ATTIVO';";

                //console.log(testo_query);
                comanda.sincro.query(testo_query, function (result) {
                    //---Risultato della query del conto---
                    result = alasql("select * from ? order by dest_stampa,portata,nodo,prog_inser", [result]);


                    //---Se non c'ÃƒÆ’Ã‚Â¨ niente dentro---
                    if (result.length === 0)
                    {
                        //---Se c'ÃƒÆ’Ã‚Â¨ il CallBack lo Esco---
                        if (typeof (cB) === "function") {
                            console.log(" esporta_xml_tavolo tavolo inesistente");
                            cB(true);

                        }

                        //Con il nuovo settaggio non va piÃƒÆ’Ã‚Â¹ esportato l'xml tavolo
                        //esporta_xml_tavolo("SENZA_AGGIUNTE", function () {
                        btn_tavoli("CONTO");
                        //});
                        //});

                        //---Fermo la funzione tornando un valore falso---
                        return false;
                    }

                    //console.log("result", result);

                    //INTESTAZIONE
                    var output = '<?xml version="1.0" standalone="yes"?>\r\n\
                        <dataroot xmlns="http://www.w3.org/2001/TAVOLI.xsd">\r\n';



                    //---Intestazione XML---
                    var i = 0;
                    var cod_articolo_princ;
                    var ts_princ;
                    var prog_principale;
                    var descrizione_articolo;
                    var ordinamento = '';
                    var testa_totale = new Object();
                    var qta_princ = 1;

                    //------
                    result.forEach(function (obj) {
                        if (obj.prezzo_un.trim() === "") {
                            obj.prezzo_un = "0,00";
                        }
                        ////console.log(obj);

                        //---Imposto le variabili da inserire nell'xml del SDS---
                        //PRIMA ERA +1 MA NON SE NE CAPISCE IL MOTIVO
                        obj.prog_inser = parseInt(obj.prog_inser);
                        obj.dest_stampa !== undefined ? ts_princ = obj.dest_stampa : ts_princ = '1';
                        obj.peso_ums !== undefined ? obj.peso_ums : obj.peso_ums = '';
                        obj.ordinamento !== undefined ? ordinamento = obj.ordinamento : ordinamento = '01';
                        prog_principale = aggZero(obj.prog_inser, 3);
                        descrizione_articolo = obj.desc_art;
                        console.log(obj.dest_stampa);
                        console.log(obj.dest_stampa !== undefined);
                        console.log(ts_princ);
                        var contiene_variante = '';
                        obj.contiene_variante === '1' ? contiene_variante = 'SI' : contiene_variante = '';

                        //---Imposto le variabili in caso di VARIANTI---
                        if (obj.desc_art[0] === "+" || obj.desc_art[0] === "-" || obj.desc_art[0] === "x" || obj.desc_art[0] === "*" || obj.desc_art[0] === "=")
                        {
                            prog_principale = aggZero(prog_principale.substring(0, 3), 3) + ' ' + aggZero(obj.prog_inser, 3) + ' ';
                            descrizione_articolo = obj.desc_art.substring(1);
                            variabile = obj.desc_art[0];
                        } else if (obj.desc_art[0] === "(") {
                            prog_principale = aggZero(prog_principale.substring(0, 3), 3) + ' ' + aggZero(obj.prog_inser, 3) + ' ';
                            variabile = "*";
                        } else
                        {
                            cod_articolo_princ = obj.cod_articolo;
                            variabile = '';
                        }

                        //---Imposto il valore di default di STAMPATA_SN---
                        var stampata_sn = 'N';
                        if (obj.stampata_sn === 'S')
                        {


                            stampata_sn = 'S';
                            var ora = obj.ora_;
                        } else
                        {

                            //CAMBIATO IL 31 AGOSTO 2020 
                            //PER RISOLVERE IL PROBLEMA DELLA COMANDA + CONTO
                            //SOLUZIONE INDICATA DA ANDREA: SOSTITUIRE GLI SN A N CON S

                            stampata_sn = 'S';

                            var ora = h + ':' + m + ':' + s;
                        }


                        //---Imposto il valore di default per la storicizzazione automatica---
                        //e per la comanda
                        var ts = 'C';

                        if (obj.BIS === BIS) {
                            if (comanda.comanda_conto === 'S')
                            {
                                ts = ts_princ;
                            } else if (comanda.comanda_conto === 'N')
                            {
                                //28 AGOSTO 2020
                                ts = ts_princ;/*"";*/
                            }
                        } else
                        {
                            ts = obj.dest_stampa;
                        }

                        var operatore = obj.operatore;
                        if (operatore.length === 0) {
                            operatore = comanda.operatore;
                        }

                        if (testa_totale[obj.BIS] === undefined) {
                            testa_totale[obj.BIS] = 0;
                        }
                        testa_totale[obj.BIS] += parseFloat(conv_prz_fc_ibr(obj.prezzo_un)) * parseInt(obj.quantita);

                        if (comanda.conteggio_coperti_differenziati === true) {
                            if (comanda.conteggio_coperti_differenziati_COD1 === obj.cod_articolo) {
                                avviso_coperti_differenziati = false;
                            }
                            if (comanda.conteggio_coperti_differenziati_COD2 === obj.cod_articolo) {
                                avviso_coperti_differenziati = false;
                            }
                            if (comanda.conteggio_coperti_differenziati_COD3 === obj.cod_articolo) {
                                avviso_coperti_differenziati = false;
                            }
                        }

                        var ncard3 = '';
                        if (obj.NCARD3) {
                            ncard3 = obj.NCARD3;
                        }



                        let tab_iva = alasql("select aliquota from tabella_iva where lingua='" + comanda.lingua_stampa + "';");
                        let percentuali_iva = tab_iva.map(d => d.aliquota);

                        if (percentuali_iva.indexOf(obj.perc_iva.trim()) === -1) {
                            /*bootbox.alert("Attenzione: l'IVA dell'articolo " + descrizione_articolo + " non &egrave; presente nella tabella IVA. Modificalo e ri-lancia l'operazione");
                             annulla_comanda2 = true;
                             return false;*/

                            obj.perc_iva = comanda.percentuale_iva_default;
                        } else if (isNaN(parseFloat(conv_prz_fc_ibr(obj.prezzo_un)).toFixed(2))) {
                            bootbox.alert("Attenzione: il prezzo dell'articolo " + descrizione_articolo + " non &egrave; valido. Modificalo e ri-lancia l'operazione");
                            annulla_comanda = true;
                            return false;
                        } else if (isNaN(parseInt(obj.quantita)) || parseInt(obj.quantita) < 1) {
                            bootbox.alert("Attenzione: la quantit&agrave; dell'articolo " + descrizione_articolo + " non &egrave; valida. Modificala e ri-lancia l'operazione");
                            annulla_comanda = true;
                            return false;
                        }


                        //---XML SERVER DI STAMPA---
                        output += '<TAVOLI>\r\n';
                        output += '<TC>C</TC>\r\n';
                        output += '<DATA>' + data_inizio_servizio + '</DATA>\r\n';
                        output += '<TAVOLO>' + aggZero(variabile_tavolo, 3) + '</TAVOLO>\r\n';
                        output += '<BIS>' + obj.BIS + '</BIS>\r\n';
                        output += '<PRG>' + aggZero(obj.prog_inser, 3) + '</PRG>\r\n';
                        output += '<ART>' + cod_articolo_princ + '</ART>\r\n';
                        output += '<ART2>' + obj.cod_articolo + '</ART2>\r\n';
                        output += '<VAR>' + variabile + '</VAR>\r\n';
                        output += '<DES>' + descrizione_articolo + '</DES>\r\n';
                        output += '<PRZ>' + parseFloat(conv_prz_fc_ibr(obj.prezzo_un)).toFixed(2).replace('.', ',') + '</PRZ>\r\n';
                        output += '<COSTO>0</COSTO>\r\n';
                        output += '<QTA>' + obj.quantita + '</QTA>\r\n';
                        output += '<SN>' + stampata_sn + '</SN>\r\n';
                        output += '<CAT>' + obj.categoria + '</CAT>\r\n';
                        output += '<TOTALE />\r\n';
                        output += '<SCONTO />\r\n';
                        output += '<AUTORIZ>' + obj.sconto_perc + '</AUTORIZ>\r\n';
                        output += '<INC>0</INC>\r\n';
                        output += '<CARTACRED>' + obj.carta_credito + '</CARTACRED>\r\n';
                        output += '<BANCOMAT/>\r\n';
                        output += '<ASSEGNI>' + obj.perc_iva + '</ASSEGNI>\r\n';
                        output += '<NCARD1>' + contiene_variante + '</NCARD1>\r\n';
                        output += '<NCARD2/>\r\n';
                        output += '<NCARD3>' + ncard3 + '</NCARD3>\r\n';
                        output += '<NCARD4/>\r\n';
                        output += '<NCARD5>' + ordinamento + '</NCARD5>\r\n'; //ORDINAMENTO
                        output += '<NSEGN>0</NSEGN>\r\n';
                        output += '<NEXIT/>\r\n';
                        output += '<TS>' + ts + '</TS>\r\n'; //' + ts_princ + '
                        output += '<NOME>' + operatore + '</NOME>\r\n';
                        output += '<ORA>' + ora + '</ORA>\r\n';
                        output += '<LIB1>1</LIB1>\r\n';
                        output += '<LIB2>' + obj.LIB2 + '</LIB2>\r\n';
                        output += '<LIB3/>\r\n';
                        output += '<LIB4>' + obj.LIB4 + '</LIB4>\r\n';
                        output += '<LIB5>' + obj.dest_stampa_2 + '</LIB5>\r\n';
                        output += '<CLI/>\r\n';
                        output += '<QTAP>0</QTAP>\r\n';
                        output += '<NODO>' + obj.nodo + '</NODO>\r\n';
                        output += '<PORTATA>' + obj.portata + '</PORTATA>\r\n';
                        output += '<NUMP>0</NUMP>\r\n';
                        output += '<TAVOLOTXT>' + comanda.tavolotxt + '</TAVOLOTXT>\r\n';
                        output += '<ULTPORT>' + obj.ultima_portata + '</ULTPORT>\r\n';
                        output += '<AGG_GIAC>' + obj.AGG_GIAC + '</AGG_GIAC>\r\n';
                        output += '</TAVOLI>\r\n';
                        i++;
                    });
                    //---Fine delle righe corpo---

                    if (annulla_comanda === true) {
                        $('.tasto_bestellung').css('pointer-events', '');
                        $('.tasto_konto').css('pointer-events', '');

                        $('.tasto_quittung').css('pointer-events', '');
                        $('.tasto_rechnung').css('pointer-events', '');

                        $('.tasto_bargeld').css('pointer-events', '');
                        return false;
                    } else if (comanda.conteggio_coperti_differenziati === true && avviso_coperti_differenziati === true) {
                        bootbox.confirm("Mancano i coperti manuali!!!<br>Vuoi continuare lo stesso?", function (conf_cop) {
                            if (conf_cop === true) {
                                f2();
                            } else {
                                $('.tasto_bestellung').css('pointer-events', '');
                                $('.tasto_konto').css('pointer-events', '');

                                $('.tasto_quittung').css('pointer-events', '');
                                $('.tasto_rechnung').css('pointer-events', '');

                                $('.tasto_bargeld').css('pointer-events', '');
                            }
                        });
                    } else {
                        f2();
                    }

                    function f2() {
                        comanda.sincro.query('select BIS from comanda where stato_record="ATTIVO" and posizione="CONTO" and ntav_comanda="' + variabile_tavolo + '" group by id,BIS order by prog_inser asc;', function (b1) {


                            b1.forEach(function (e) {
                                var bi2 = alasql('select desc_art as desc_sconto,prezzo_un as sconto from comanda where stato_record="ATTIVO" and ntav_comanda="' + variabile_tavolo + '" and BIS="' + e.BIS + '" and posizione="SCONTO" limit 1;');
                                var bi3 = alasql('select nump_xml as numero_coperti from comanda  where stato_record="ATTIVO" and ntav_comanda="' + variabile_tavolo + '" and BIS="' + e.BIS + '" order by nump_xml desc limit 1;');
                                var bi4 = alasql('select desc_art,LIB2,dest_stampa as storicizzazione,prezzo_un,incassato,costo,operatore,contiene_variante as NCARD1,NCARD2,NCARD3,NCARD4,NCARD5,NSEGN,NEXIT,carta_credito,bancomat from comanda  where ntav_comanda="' + variabile_tavolo + '"  and  desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" and BIS="' + e.BIS + '" limit 1;');

                                var b2 = bi2[0];
                                var b3 = bi3[0];
                                var b4 = bi4[0];

                                var rec = new Object();
                                // SEZIONE BIS
                                rec.BIS = e.BIS;
                                if (rec.BIS !== "") {
                                    bis_presente = true;
                                }
                                // SEZIONE SCONTISTICA
                                rec.desc_sconto = "";
                                rec.sconto = "";
                                if (b2 !== undefined) {
                                    rec.desc_sconto = b2.desc_sconto;
                                    rec.sconto = b2.sconto;
                                }
                                // SEZIONE COPERTI
                                var numero_coperti = $("#numero_coperti").val();
                                rec.numero_coperti = "";

                                if (b3 !== undefined) {
                                    rec.numero_coperti = b3.numero_coperti;

                                }
                                console.log("BIS COPERTI 0", "LCS", lettera_conto_split(), "BIS", rec.BIS, "ncop", numero_coperti, "ncop2", rec.numero_coperti, "B3", b3);
                                // SEZIONE PAGAMENTI
                                rec.incassato = "";
                                rec.costo = "";
                                rec.operatore = "";
                                rec.NCARD1 = "";
                                rec.NCARD2 = "";
                                rec.NCARD3 = "";
                                rec.NCARD4 = "";
                                rec.NCARD5 = "";
                                rec.NSEGN = "";
                                rec.NEXIT = "";
                                rec.carta_credito = "";
                                rec.bancomat = "";
                                rec.storicizzazione = "";
                                rec.LIB2 = "";
                                if (b4 !== undefined) {
                                    rec.LIB2 = b4.LIB2;
                                    rec.incassato = b4.incassato;
                                    rec.costo = b4.costo;
                                    rec.operatore = b4.operatore;
                                    rec.NCARD1 = b4.NCARD1;
                                    rec.NCARD2 = b4.NCARD2;
                                    rec.NCARD3 = b4.NCARD3;
                                    rec.NCARD4 = b4.NCARD4;
                                    rec.NCARD5 = b4.NCARD5;
                                    rec.NSEGN = b4.NSEGN;
                                    rec.NEXIT = b4.NEXIT;
                                    rec.carta_credito = b4.carta_credito;
                                    rec.bancomat = b4.bancomat;
                                    rec.storicizzazione = b4.storicizzazione;
                                }


                                //---Variabili testa---
                                var incassato = 0;
                                var autoriz = 0;


                                var sconto = '0.00';



                                if (rec !== undefined && typeof rec.sconto === 'string' && rec !== "") {
                                    if (rec.sconto !== undefined && rec.sconto !== "" && rec.sconto.substr(-1) === "%") {

                                        //Bisogna fare il calcolo
                                        sconto = parseFloat(testa_totale[rec.BIS]) / 100 * rec.sconto.substr(1).slice(0, -1);


                                        sconto = sconto.toString();
                                        console.log("CALCOLO CONTO SCONTO", sconto);
                                    } else if (rec.sconto !== undefined && rec.sconto !== "") {

                                        sconto = rec.sconto.substr(1);
                                    }
                                    autoriz = rec.desc_sconto;

                                }

                                //PER LA SAGRA NON SERVE PERCHE' INCASSA IN AUTOMATICO
                                //E POI SE SI RIPRENDE UNA COMANDA DOPO AVER LASCIATO LA L'INCASSO FA CASINO
                                //DA VERIFICARE

                                if (incasso_auto === 'N')
                                {
                                    if ((rec.incassato !== undefined && rec.incassato !== null && rec.incassato !== 'undefined' && !isNaN(parseFloat(rec.incassato))))
                                    {
                                        incassato = rec.incassato;
                                    } else
                                    {
                                        incassato = '0,00';
                                    }
                                } else if (incasso_auto === 'S')
                                {

                                    incassato = testa_totale[rec.BIS] - parseFloat(sconto);
                                    incassato = incassato.toString();

                                    console.log("CALCOLO CONTO INCASSO", incassato);

                                }


                                //Nel fare parseFloat bisogna convertire la , in ., altrimenti restituisce il numero senza decimali, se era un incasso non automatico
                                incassato = parseFloat(conv_prz_fc_ibr(incassato)).toFixed(2).replace('.', ',');
                                sconto = parseFloat(conv_prz_fc_ibr(sconto)).toFixed(2).replace('.', ',');




                                if (rec.BIS === lettera_conto_split()) {



                                    rec.incassato = incassato;

                                    rec.storicizzazione = storicizzazione;

                                    if (numero_coperti !== undefined && !isNaN(numero_coperti) && numero_coperti !== "")
                                    {
                                        testa_totale[rec.BIS] += parseInt(numero_coperti) * parseFloat(comanda.valore_coperto);


                                        if (parseInt(numero_coperti) - comanda.ultimi_coperti_importati !== 0) {
                                            annulla_comanda = false;
                                        }
                                    }
                                } else
                                {
                                    numero_coperti = rec.numero_coperti;
                                }

                                if (autoriz.toString() != "NaN") {
                                    autoriz = "";
                                }

                                var DES = "RECORD TESTA";
                                if (b4 && b4.desc_art) {
                                    DES = b4.desc_art;
                                } else if (comanda.set_tid === true) {
                                    DES += " TID=" + comanda.TID;

                                }

                                let totale_testa = "0,00";
                                if (!isNaN(parseFloat(testa_totale[rec.BIS]))) {
                                    totale_testa = parseFloat(testa_totale[rec.BIS]).toFixed(2).replace(".", ",");
                                } else {
                                    bootbox.alert("Attenzione: il totale della testa ha un valore errato.");
                                    annulla_comanda2 = true;
                                }

                                let totale_incassato = "";
                                if (!isNaN(parseFloat(rec.incassato)) || rec.incassato.trim() === "") {
                                    totale_incassato = rec.incassato;
                                } else {
                                    bootbox.alert("Attenzione: l incasso ha un valore errato.");
                                    annulla_comanda2 = true;
                                }

                                let totale_carta_credito = "";
                                if (!isNaN(parseFloat(rec.carta_credito)) || rec.carta_credito.trim() === "") {
                                    totale_carta_credito = rec.carta_credito;
                                } else {
                                    bootbox.alert("Attenzione: il valore della carta di credito ha un valore errato.");
                                    annulla_comanda2 = true;
                                }

                                let totale_bancomat = "";
                                if (!isNaN(parseFloat(rec.bancomat)) || rec.bancomat.trim() === "") {
                                    totale_bancomat = rec.bancomat;
                                } else {
                                    bootbox.alert("Attenzione: il  bancomat ha un valore errato.");
                                    annulla_comanda2 = true;
                                }

                                //---XML TESTA---
                                output += '<TAVOLI>\r\n\
                <TC>T</TC>\r\n\
                <DATA>' + data_inizio_servizio + '</DATA>\r\n\
                <TAVOLO>' + aggZero(variabile_tavolo, 3) + '</TAVOLO>\r\n\
                <BIS>' + rec.BIS + '</BIS>\r\n\
                <PRG>000</PRG>\r\n\
                <ART>01</ART>\r\n\
                <ART2/>\r\n\
                <VAR/>\r\n\
                <DES>' + DES + '</DES>\r\n\
                <PRZ/>\r\n\
                <COSTO>0</COSTO>\r\n\
                <QTA/>\r\n\
                <SN>N</SN>\r\n\
                <CAT/>\r\n\
                <TOTALE>' + totale_testa + '</TOTALE>\r\n\
                <SCONTO>' + sconto + '</SCONTO>\r\n\
                <AUTORIZ>' + autoriz + '</AUTORIZ>\r\n\
                <INC>' + totale_incassato + '</INC>\r\n\
                <CARTACRED>' + totale_carta_credito + '</CARTACRED>\r\n\
                <BANCOMAT>' + totale_bancomat + '</BANCOMAT>\r\n\
                <ASSEGNI>0,00</ASSEGNI>\r\n\
                <NCARD1>' + rec.NCARD1 + '</NCARD1>\r\n\
                <NCARD2>' + rec.NCARD2 + '</NCARD2>\r\n\
                <NCARD3>' + rec.NCARD3 + '</NCARD3>\r\n\
                <NCARD4>' + rec.NCARD4 + '</NCARD4>\r\n\
                <NCARD5>' + rec.NCARD5 + '</NCARD5>\r\n\
                <NSEGN>' + rec.NSEGN + '</NSEGN>\r\n\
                <NEXIT>' + rec.NEXIT + '</NEXIT>\r\n\
                <TS>' + rec.storicizzazione + '</TS>\r\n\
                <NOME>' + comanda.operatore + '</NOME>\r\n\
                <ORA>' + ora + '</ORA>\r\n\
                <LIB1>1</LIB1>\r\n\
                <LIB2>' + rec.LIB2 + '</LIB2>\r\n\
                <LIB3/>\r\n\
                <LIB4/>\r\n\
                <LIB5>S</LIB5>\r\n\
                <CLI/>\r\n\
                <QTAP>0</QTAP>\r\n\
                <NODO>' + settaggio_comandapiuconto + '</NODO>\r\n\
                <PORTATA/>\r\n\
                <NUMP>' + numero_coperti + '</NUMP>\r\n\
                <TAVOLOTXT>' + comanda.tavolotxt + '</TAVOLOTXT>\r\n\
                <ULTPORT>N</ULTPORT>\r\n\
                <AGG_GIAC>N</AGG_GIAC>\r\n\
                </TAVOLI>\r\n';


                            });
                            if (annulla_comanda2 === true) {
                                return false;
                            }
                            output += '</dataroot>';

                            //---Toglie i null e undefined dall'output---
                            output = output.replace(/>null</gi, '><');
                            output = output.replace(/>undefined</gi, '><');

                            console.log("OUTPUT XML CONTO" + comanda.tavolotxt, output);

                            //---Crea una data---
                            var d = new Date();
                            var Y = d.getFullYear();
                            var M = addZero(d.getDate(), 2);
                            var D = addZero((d.getMonth() + 1), 2);
                            var h = addZero(d.getHours(), 2);
                            var m = addZero(d.getMinutes(), 2);
                            var s = addZero(d.getSeconds(), 2);
                            var ms = addZero(d.getMilliseconds(), 3);
                            var data_tablet = h + '' + m + '' + s;

                            //---Legge l'id del tablet dall'url---
                            var id_tablet = "29";
                            var id_temp = leggi_get_url('id');

                            if (id_temp !== undefined && id_temp !== null && id_temp !== 'null' && id_temp.length === 2) {
                                id_tablet = id_temp;
                            }

                            //---Imposta il nome file del SDS---
                            var nome_file = "/SDS/PDA" + id_tablet + "_" + data_tablet + BIS + ".XM_";

                            //---Promise, che in caso di storicizzazione NON automatica, esporta il planing del tavolo---
                            //FinchÃƒÆ’Ã‚Â¨ il tavolo non viene esportato e riletto con l'hash di origine, la funzione resta bloccata
                            var prom_storicizzazione = new Promise(function (resolve, reject) {
                                if (comanda.storicizzazione_auto === 'N')
                                {
                                    var nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + '.7';

                                    console.log("esporta_xml_tavolo", nome_file);

                                    if (bis_presente === true) {
                                        nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + '.8';
                                    }

                                    //esporta_xml_tavolo(tipo, function () {

                                    oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].nome_file_planing = "_" + comanda.folder_number + "/" + nome_file;
                                    oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].contenuto_file_planing = "";
                                    oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].tipo_file_planing = "PLANING";

                                    comanda.xml.salva_file_xml("_" + comanda.folder_number + "/" + nome_file, "", "PLANING", function () {
                                        resolve(true);
                                    });
                                    //});
                                } else if (comanda.storicizzazione_auto === 'S')
                                {
                                    //esporta_xml_tavolo(tipo, function () {
                                    resolve(true);
                                    //    });
                                }
                            });

                            //Promessa per risolvere il bug del btn tavoli se non la usassi
                            //Serve ad esportare il file del SDS dopo l'esportazione sicura del tavolo
                            prom_storicizzazione.then(function () {

                                oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].nome_file_sds = "_" + comanda.folder_number + "/" + nome_file;
                                oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].contenuto_file_sds = output;
                                oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].tipo_file_sds = "SDS";

                                comanda.xml.salva_file_xml("_" + comanda.folder_number + "/" + nome_file, output, "SDS", function () {

                                    var data_intera = comanda.funzionidb.data_attuale();
                                    var data = data_intera.substring(0, 10);
                                    var ora = data_intera.substring(11, 16);
                                    var testo_query = "update comanda set stampata_sn='S',data_stampa='" + data + "',ora_stampa='" + ora + "',fiscalizzata_sn='N' where stato_record='ATTIVO' and ntav_comanda='" + variabile_tavolo + "'";
                                    ////console.log(testo_query);
                                    comanda.sock.send({tipo: "comanda_stampata", operatore: comanda.operatore, query: testo_query});
                                    comanda.sincro.query(testo_query, function () {

                                        //---Una volta esportato per certo il file, torna ai tavoli o al parcheggio(impossibile nel caso di Padova)---
                                        if (comanda.tavolotxt !== "BANCO" && comanda.tavolotxt !== "BAR" && comanda.tavolotxt.left(7) !== "ASPORTO" && comanda.tavolotxt !== "TAKEAWAY") {

                                            btn_tavoli("CONTO");
                                            $('.tasto_bestellung').css('pointer-events', '');
                                            $('.tasto_konto').css('pointer-events', '');

                                            $('.tasto_quittung').css('pointer-events', '');
                                            $('.tasto_rechnung').css('pointer-events', '');

                                            $('.tasto_bargeld').css('pointer-events', '');


                                        } else
                                        {
                                            btn_parcheggia();
                                        }

                                    });
                                    var date = new Date();
                                    var ora = addZero(date.getHours(), 2) + ':' + addZero(date.getMinutes(), 2);
                                    var testo_query = "update tavoli SET colore='4',occupato='0',ora_apertura_tavolo = ( CASE WHEN ( ora_apertura_tavolo = '-' OR ora_apertura_tavolo = 'null' OR ora_apertura_tavolo = ''  OR ora_apertura_tavolo = NULL ) THEN '" + ora + "' ELSE ora_apertura_tavolo END ), ora_ultima_comanda = (  CASE WHEN ( ora_apertura_tavolo = 'null'  OR ora_apertura_tavolo = '' OR ora_apertura_tavolo = '-'  OR ora_apertura_tavolo = NULL ) THEN ora_ultima_comanda ELSE '" + ora + "' END )  where numero = '" + variabile_tavolo + "';";
                                    comanda.sock.send({tipo: "comanda_stampata", operatore: comanda.operatore, query: testo_query});
                                    comanda.sincro.query(testo_query, function () {
                                        disegna_ultimo_tavolo_modificato(testo_query);


                                    });
                                });

                            });
                        });
                    }
                });
            });
        }
    });
}


function stampa_comanda_piu_conto(tipo) {
    var avviso_coperti_differenziati = true;
    var supero_10 = false;
    var bis_presente = false;
    $('#conto tr:not(.non-contare-riga) td:first-child').each(function (e, a) {
        if (a.innerText > 10) {
            supero_10 = true;
        }
    });
    $('.tasto_bestellung_konto').css('pointer-events', 'none');
    $('.tasto_bestellung').css('pointer-events', 'none');
    $('.tasto_konto').css('pointer-events', 'none');

    $('.tasto_quittung').css('pointer-events', 'none');
    $('.tasto_rechnung').css('pointer-events', 'none');

    $('.tasto_bargeld').css('pointer-events', 'none');


    var promessa_confirm = new Promise(function (resolve, reject) {
        if (supero_10 === true && comanda.licenza_vera === "MaximilianAlPonte") {
            bootbox.confirm("I 10 articoli sono stati superati. Vuoi procedere lo stesso?", function (risposta_confirm) {
                if (risposta_confirm === true) {
                    if (comanda.coperti_obbligatorio === '1' && ($("#numero_coperti").val() === '' || $("#numero_coperti").val() === '0'))
                    {
                        bootbox.confirm("Mancano i coperti. Vuoi procedere lo stesso?", function (risposta_confirm) {
                            if (risposta_confirm === true) {
                                resolve(true);
                            } else
                            {
                                resolve(false);
                                $('.tasto_bestellung_konto').css('pointer-events', '');
                                $('.tasto_bestellung').css('pointer-events', '');
                                $('.tasto_konto').css('pointer-events', '');

                                $('.tasto_quittung').css('pointer-events', '');
                                $('.tasto_rechnung').css('pointer-events', '');

                                $('.tasto_bargeld').css('pointer-events', '');
                            }
                        });
                    } else
                    {
                        resolve(true);
                    }
                } else
                {
                    resolve(false);
                    $('.tasto_bestellung_konto').css('pointer-events', '');
                    $('.tasto_bestellung').css('pointer-events', '');
                    $('.tasto_konto').css('pointer-events', '');

                    $('.tasto_quittung').css('pointer-events', '');
                    $('.tasto_rechnung').css('pointer-events', '');

                    $('.tasto_bargeld').css('pointer-events', '');
                }
            });
        } else if (comanda.coperti_obbligatorio === '1' && ($("#numero_coperti").val() === '' || $("#numero_coperti").val() === '0'))
        {
            bootbox.confirm("Mancano i coperti. Vuoi procedere lo stesso?", function (risposta_confirm) {
                if (risposta_confirm === true) {
                    resolve(true);
                } else
                {
                    resolve(false);
                    $('.tasto_bestellung_konto').css('pointer-events', '');
                    $('.tasto_bestellung').css('pointer-events', '');
                    $('.tasto_konto').css('pointer-events', '');

                    $('.tasto_quittung').css('pointer-events', '');
                    $('.tasto_rechnung').css('pointer-events', '');

                    $('.tasto_bargeld').css('pointer-events', '');
                }
            });
        } else
        {
            resolve(true);
        }

    });

    promessa_confirm.then(function (risposta_confirm) {
        if (risposta_confirm === true) {

            var variabile_tavolo = comanda.tavolo;
            var numero_coperti = $("#numero_coperti").val();

            oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)] = new Object();

            var annulla_comanda = true;
            var annulla_comanda2 = false;


            $('#body_replacement').css('pointer-events', 'none');
            $('#body_replacement').css('opacity', '0.6');

            //KS0 GERMANIA
            //C ITALIA
            //S CONTO CHE CHIUDE E STORICIZZA AUTOMATICO

            //VALORI DEFAULT
            var storicizzazione = 'N';
            var settaggio_comandapiuconto = 'S';
            var incasso_auto = 'N';
            var BIS = '';

            if (comanda.split_abilitato === true) {
                if (lettera_conto_split().length >= 1)
                {
                    BIS = "-" + lettera_conto_split();
                }
            }

            if (comanda.storicizzazione_auto === 'S')
            {
                storicizzazione = "CS";
            } else if (comanda.storicizzazione_auto === 'N')
            {

                storicizzazione = " ";

            }


            var lib5 = 'S';

            if (comanda.incasso_auto === 'S')
            {
                incasso_auto = "S";
            } else if (comanda.incasso_auto === 'N')
            {
                incasso_auto = "N";
            }

            var ultima_portata_non_stampata = "N";

            var d = new Date();
            var Y = d.getFullYear().toString();
            var D = addZero(d.getDate(), 2);
            var M = addZero((d.getMonth() + 1), 2);
            var h = addZero(d.getHours(), 2);
            var m = addZero(d.getMinutes(), 2);
            var s = addZero(d.getSeconds(), 2);
            var ms = addZero(d.getMilliseconds(), 3);
            var data = D + '/' + M + '/' + Y;
            var ora = h + ':' + m + ':' + s;
            var variabile = '';
            var specifica_comanda = "";

            /*if (comanda.storicizzazione_auto === 'N') {
             specifica_comanda = "and (stampata_sn is null OR stampata_sn='N' OR stampata_sn='n' OR stampata_sn='') ";
             }*/

            specifica_comanda = '';

            var testo_query = " select NCARD3,AGG_GIAC,LIB4,LIB2,dest_stampa_2,carta_credito,ora_,operatore,sconto_perc,perc_iva,BIS,NSEGN,QTAP,ordinamento,ultima_portata,peso_ums,cod_articolo,dest_stampa,contiene_variante,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita,stampata_sn from comanda where  desc_art NOT LIKE 'RECORD TESTA%' and contiene_variante='1' and ntav_comanda='" + variabile_tavolo + "' and posizione='CONTO' and stato_record='ATTIVO' " + specifica_comanda + " \n\
                            union all\n\
                                select NCARD3,AGG_GIAC,LIB4,LIB2,dest_stampa_2,carta_credito,ora_,operatore,sconto_perc,perc_iva,BIS,NSEGN,QTAP,ordinamento,ultima_portata,peso_ums,cod_articolo,dest_stampa,contiene_variante,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita,stampata_sn from comanda where  desc_art NOT LIKE 'RECORD TESTA%' and (contiene_variante !='1' or contiene_variante is null) and length(nodo)=3 and ntav_comanda='" + variabile_tavolo + "' and posizione='CONTO' and stato_record='ATTIVO'  " + specifica_comanda + " \n\
                            union all\n\
                                select NCARD3,AGG_GIAC,LIB4,LIB2,dest_stampa_2,carta_credito,ora_,operatore,sconto_perc,perc_iva,BIS,NSEGN,QTAP,ordinamento,ultima_portata,peso_ums,cod_articolo,dest_stampa,contiene_variante,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita,stampata_sn from comanda where  desc_art NOT LIKE 'RECORD TESTA%' and length(nodo)=7 and ntav_comanda='" + variabile_tavolo + "'  and posizione='CONTO' and stato_record='ATTIVO' " + specifica_comanda + ";";

            console.log("QUERY COMANDA", testo_query);
            comanda.sincro.query(testo_query, function (result) {

                result = alasql("select * from ? order by dest_stampa,portata,nodo,prog_inser", [result]);

                console.log("QUERY COMANDA RESULT", result);

                if (result.length === 0)
                {
                    if (typeof (cB) === "function") {
                        console.log("esporta_xml_tavolo tavolo inesistente");
                        cB(true);

                    }
                    console.log("QUERY COMANDA BUTTON TAVOLI");
                    //esporta_xml_tavolo("SENZA_AGGIUNTE", function () {

                    if (variabile_tavolo !== undefined && typeof (variabile_tavolo) === "string") {
                        comanda.xml.elimina_tavolo_occupato("_" + comanda.folder_number + "/COND/Tav" + variabile_tavolo.replace(/^0+/, '') + ".CO1", false, function () {
                            btn_tavoli("CONTO");
                        });
                    }
                    //});
                    return false;
                }

                //console.log("result", result);

                //INTESTAZIONE
                var output = '<?xml version="1.0" standalone="yes"?>\r\n\
                        <dataroot xmlns="http://www.w3.org/2001/TAVOLI.xsd">\r\n';
                var i = 0;
                var cod_articolo_princ;
                var ts_princ;
                var prog_principale;
                var descrizione_articolo;
                var ordinamento = '';
                var testa_totale = new Object();
                var qta_princ = 1;

                result.forEach(function (obj) {
                    if (obj.prezzo_un.trim() === "") {
                        obj.prezzo_un = "0,00";
                    }
                    ////console.log(obj);

                    //PRIMA ERA +1 MA NON SE NE CAPISCE IL MOTIVO
                    obj.prog_inser = parseInt(obj.prog_inser);

                    if (obj.BIS === BIS) {
                        obj.dest_stampa !== undefined ? ts_princ = obj.dest_stampa : ts_princ = '1';
                    } else
                    {
                        ts_princ = obj.dest_stampa;
                    }

                    obj.peso_ums !== undefined ? obj.peso_ums : obj.peso_ums = '';
                    obj.ordinamento !== undefined ? ordinamento = obj.ordinamento : ordinamento = '01';
                    prog_principale = aggZero(obj.prog_inser, 3);
                    descrizione_articolo = obj.desc_art;
                    console.log(obj.dest_stampa);
                    console.log(obj.dest_stampa !== undefined);
                    console.log(ts_princ);
                    var contiene_variante = '';
                    obj.contiene_variante === '1' ? contiene_variante = 'SI' : contiene_variante = '';
//---
                    if (obj.desc_art[0] === "+" || obj.desc_art[0] === "-" || obj.desc_art[0] === "x" || obj.desc_art[0] === "*" || obj.desc_art[0] === "=")
                    {
                        prog_principale = aggZero(prog_principale.substring(0, 3), 3) + ' ' + aggZero(obj.prog_inser, 3) + ' ';
                        descrizione_articolo = obj.desc_art.substring(1);
                        variabile = obj.desc_art[0];
                    } else if (obj.desc_art[0] === "(") {
                        prog_principale = aggZero(prog_principale.substring(0, 3), 3) + ' ' + aggZero(obj.prog_inser, 3) + ' ';
                        variabile = "*";
                    } else
                    {
                        cod_articolo_princ = obj.cod_articolo;
                        variabile = '';
                    }

                    var stampata_sn = 'N';
                    if (obj.stampata_sn === 'S')
                    {
                        stampata_sn = 'S';
                    }

                    var qtap = '0';
                    if (!isNaN(obj.QTAP) && obj.QTAP > 0) {
                        qtap = obj.QTAP;
                    }

                    var nsegn = '0';
                    if (!isNaN(obj.NSEGN) && obj.NSEGN > 0) {
                        nsegn = obj.NSEGN;
                    }

                    if (stampata_sn === 'N' || descrizione_articolo === "MEMO INC,SCONTO,DRINK,LISTINO,COPERTI" || descrizione_articolo.indexOf(" hg.") !== -1)
                    {
                        annulla_comanda = false;

                        var ora = h + ':' + m + ':' + s;

                        if (obj.ultima_portata === "S") {
                            ultima_portata_non_stampata = "S";
                        }
                    } else
                    {
                        var ora = obj.ora_;
                    }

                    var operatore = obj.operatore;
                    if (operatore.length === 0) {
                        operatore = comanda.operatore;
                    }

                    if (testa_totale[obj.BIS] === undefined) {
                        testa_totale[obj.BIS] = 0;
                    }
                    testa_totale[obj.BIS] += parseFloat(conv_prz_fc_ibr(obj.prezzo_un)) * parseInt(obj.quantita);

                    if (comanda.conteggio_coperti_differenziati === true) {
                        if (comanda.conteggio_coperti_differenziati_COD1 === obj.cod_articolo) {
                            avviso_coperti_differenziati = false;
                        }
                        if (comanda.conteggio_coperti_differenziati_COD2 === obj.cod_articolo) {
                            avviso_coperti_differenziati = false;
                        }
                        if (comanda.conteggio_coperti_differenziati_COD3 === obj.cod_articolo) {
                            avviso_coperti_differenziati = false;
                        }
                    }

                    var ncard3 = '';
                    if (obj.NCARD3) {
                        ncard3 = obj.NCARD3;
                    }

                    let tab_iva = alasql("select aliquota from tabella_iva where lingua='" + comanda.lingua_stampa + "';");
                    let percentuali_iva = tab_iva.map(d => d.aliquota);

                    if (percentuali_iva.indexOf(obj.perc_iva.trim()) === -1) {
                        /*bootbox.alert("Attenzione: l'IVA dell'articolo " + descrizione_articolo + " non &egrave; presente nella tabella IVA. Modificalo e ri-lancia l'operazione");
                         annulla_comanda2 = true;
                         return false;*/

                        obj.perc_iva = comanda.percentuale_iva_default;
                    } else if (isNaN(parseFloat(conv_prz_fc_ibr(obj.prezzo_un)).toFixed(2))) {
                        bootbox.alert("Attenzione: il prezzo dell'articolo " + descrizione_articolo + " non &egrave; valido. Modificalo e ri-lancia l'operazione");
                        annulla_comanda2 = true;
                        return false;
                    } else if (isNaN(parseInt(obj.quantita)) || parseInt(obj.quantita) < 1) {
                        bootbox.alert("Attenzione: la quantit&agrave; dell'articolo " + descrizione_articolo + " non &egrave; valida. Modificala e ri-lancia l'operazione");
                        annulla_comanda2 = true;
                        return false;
                    }

                    output += '<TAVOLI>\r\n';
                    output += '<TC>C</TC>\r\n';
                    output += '<DATA>' + data_inizio_servizio + '</DATA>\r\n';
                    output += '<TAVOLO>' + aggZero(variabile_tavolo, 3) + '</TAVOLO>\r\n';
                    output += '<BIS>' + obj.BIS + '</BIS>\r\n';
                    output += '<PRG>' + aggZero(obj.prog_inser, 3) + '</PRG>\r\n';
                    output += '<ART>' + cod_articolo_princ + '</ART>\r\n';
                    output += '<ART2>' + obj.cod_articolo + '</ART2>\r\n';
                    output += '<VAR>' + variabile + '</VAR>\r\n';
                    output += '<DES>' + descrizione_articolo + '</DES>\r\n';
                    output += '<PRZ>' + parseFloat(conv_prz_fc_ibr(obj.prezzo_un)).toFixed(2).replace('.', ',') + '</PRZ>\r\n';
                    output += '<COSTO>0</COSTO>\r\n';
                    output += '<QTA>' + obj.quantita + '</QTA>\r\n';
                    output += '<SN>' + stampata_sn + '</SN>\r\n';
                    output += '<CAT>' + obj.categoria + '</CAT>\r\n';
                    output += '<TOTALE />\r\n';
                    output += '<SCONTO />\r\n';
                    output += '<AUTORIZ>' + obj.sconto_perc + '</AUTORIZ>\r\n';
                    output += '<INC>0</INC>\r\n';
                    output += '<CARTACRED>' + obj.carta_credito + '</CARTACRED>\r\n';
                    output += '<BANCOMAT/>\r\n';
                    output += '<ASSEGNI>' + obj.perc_iva + '</ASSEGNI>\r\n';
                    output += '<NCARD1>' + contiene_variante + '</NCARD1>\r\n';
                    output += '<NCARD2/>\r\n';
                    output += '<NCARD3>' + ncard3 + '</NCARD3>\r\n';
                    output += '<NCARD4/>\r\n';
                    output += '<NCARD5>' + ordinamento + '</NCARD5>\r\n'; //ORDINAMENTO
                    output += '<NSEGN>' + nsegn + '</NSEGN>\r\n';
                    output += '<NEXIT/>\r\n';
                    output += '<TS>' + ts_princ + '</TS>\r\n'; //' + ts_princ + '
                    output += '<NOME>' + operatore + '</NOME>\r\n';
                    output += '<ORA>' + ora + '</ORA>\r\n';
                    output += '<LIB1>1</LIB1>\r\n';
                    output += '<LIB2>' + obj.LIB2 + '</LIB2>\r\n';
                    output += '<LIB3/>\r\n';
                    output += '<LIB4>' + obj.LIB4 + '</LIB4>\r\n';
                    output += '<LIB5>' + obj.dest_stampa_2 + '</LIB5>\r\n';
                    output += '<CLI/>\r\n';
                    output += '<QTAP>' + qtap + '</QTAP>\r\n';
                    output += '<NODO>' + obj.nodo + '</NODO>\r\n';
                    output += '<PORTATA>' + obj.portata + '</PORTATA>\r\n';
                    output += '<NUMP>0</NUMP>\r\n';
                    output += '<TAVOLOTXT>' + comanda.tavolotxt + '</TAVOLOTXT>\r\n';
                    output += '<ULTPORT>' + obj.ultima_portata + '</ULTPORT>\r\n';
                    output += '<AGG_GIAC>' + obj.AGG_GIAC + '</AGG_GIAC>\r\n';
                    output += '</TAVOLI>\r\n';
                    i++;
                });

                if (annulla_comanda2 === true) {
                    $('.tasto_bestellung').css('pointer-events', '');
                    $('.tasto_konto').css('pointer-events', '');

                    $('.tasto_quittung').css('pointer-events', '');
                    $('.tasto_rechnung').css('pointer-events', '');

                    $('.tasto_bargeld').css('pointer-events', '');
                    return false;
                } else if (comanda.conteggio_coperti_differenziati === true && avviso_coperti_differenziati === true) {
                    bootbox.confirm("Mancano i coperti manuali!!!<br>Vuoi continuare lo stesso?", function (conf_cop) {
                        if (conf_cop === true) {
                            f2();
                        } else {
                            $('.tasto_bestellung').css('pointer-events', '');
                            $('.tasto_konto').css('pointer-events', '');

                            $('.tasto_quittung').css('pointer-events', '');
                            $('.tasto_rechnung').css('pointer-events', '');

                            $('.tasto_bargeld').css('pointer-events', '');
                        }
                    });
                } else {
                    f2();
                }

                function f2() {

                    comanda.sincro.query('select BIS from comanda where stato_record="ATTIVO" and posizione="CONTO" and ntav_comanda="' + variabile_tavolo + '" group by id,BIS order by prog_inser asc;', function (b1) {


                        b1.forEach(function (e) {
                            var bi2 = alasql('select desc_art as desc_sconto,prezzo_un as sconto from comanda where stato_record="ATTIVO" and ntav_comanda="' + variabile_tavolo + '" and BIS="' + e.BIS + '" and posizione="SCONTO" limit 1;');
                            var bi3 = alasql('select nump_xml as numero_coperti from comanda  where stato_record="ATTIVO" and ntav_comanda="' + variabile_tavolo + '" and BIS="' + e.BIS + '" order by nump_xml desc limit 1;');
                            var bi4 = alasql('select desc_art,LIB2,dest_stampa as storicizzazione,prezzo_un,incassato,costo,operatore,contiene_variante as NCARD1,NCARD2,NCARD3,NCARD4,NCARD5,NSEGN,NEXIT,carta_credito,bancomat from comanda  where ntav_comanda="' + variabile_tavolo + '"  and  desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" and BIS="' + e.BIS + '" limit 1;');
                            var b2 = bi2[0];
                            var b3 = bi3[0];
                            var b4 = bi4[0];

                            var rec = new Object();
                            // SEZIONE BIS
                            rec.BIS = e.BIS;
                            if (rec.BIS !== "") {
                                bis_presente = true;
                            }
                            // SEZIONE SCONTISTICA
                            rec.desc_sconto = "";
                            rec.sconto = "";
                            if (b2 !== undefined) {
                                rec.desc_sconto = b2.desc_sconto;
                                rec.sconto = b2.sconto;
                            }
                            // SEZIONE COPERTI
                            var numero_coperti = $("#numero_coperti").val();
                            rec.numero_coperti = "";

                            if (b3 !== undefined) {
                                rec.numero_coperti = b3.numero_coperti;

                            }
                            console.log("BIS COPERTI 0", "LCS", lettera_conto_split(), "BIS", rec.BIS, "ncop", numero_coperti, "ncop2", rec.numero_coperti, "B3", b3);

                            // SEZIONE PAGAMENTI
                            rec.desc_art = "";
                            rec.incassato = "";
                            rec.costo = "";
                            rec.operatore = "";
                            rec.NCARD1 = "";
                            rec.NCARD2 = "";
                            rec.NCARD3 = "";
                            rec.NCARD4 = "";
                            rec.NCARD5 = "";
                            rec.NSEGN = "";
                            rec.NEXIT = "";
                            rec.carta_credito = "";
                            rec.bancomat = "";
                            rec.storicizzazione = "";
                            rec.LIB2 = "";
                            if (b4 !== undefined) {
                                rec.desc_art = b4.desc_art;
                                rec.incassato = b4.incassato;
                                rec.costo = b4.costo;
                                rec.operatore = b4.operatore;
                                rec.NCARD1 = b4.NCARD1;
                                rec.NCARD2 = b4.NCARD2;
                                rec.NCARD3 = b4.NCARD3;
                                rec.NCARD4 = b4.NCARD4;
                                rec.NCARD5 = b4.NCARD5;
                                rec.NSEGN = b4.NSEGN;
                                rec.NEXIT = b4.NEXIT;
                                rec.carta_credito = b4.carta_credito;
                                rec.bancomat = b4.bancomat;
                                rec.storicizzazione = b4.storicizzazione;
                                rec.LIB2 = b4.LIB2;
                            }

                            var incassato = 0;



                            var autoriz = 0;


                            var sconto = '0.00';

                            if (rec !== undefined && typeof rec.sconto === 'string' && rec !== "") {
                                if (rec.sconto !== undefined && rec.sconto !== "" && rec.sconto.substr(-1) === "%") {

                                    //Bisogna fare il calcolo
                                    sconto = testa_totale[rec.BIS] / 100 * rec.sconto.substr(1).slice(0, -1);

                                    sconto = sconto.toString();
                                    console.log("CALCOLO CONTO SCONTO", sconto);
                                } else if (rec.sconto !== undefined && rec.sconto !== "") {
                                    sconto = rec.sconto.substr(1);
                                }
                                autoriz = rec.desc_sconto;


                            }

                            //PER LA SAGRA NON SERVE PERCHE' INCASSA IN AUTOMATICO
                            //E POI SE SI RIPRENDE UNA COMANDA DOPO AVER LASCIATO LA L'INCASSO FA CASINO
                            //DA VERIFICARE




                            if (incasso_auto === 'N')
                            {
                                if ((rec.incassato !== undefined && rec !== null && rec.incassato !== 'undefined' && !isNaN(parseFloat(rec.incassato))))
                                {
                                    incassato = rec.incassato;
                                } else
                                {
                                    incassato = '0,00';
                                }
                            } else if (incasso_auto === 'S')
                            {

                                incassato = testa_totale[rec.BIS] - parseFloat(sconto);
                                incassato = incassato.toString();

                                console.log("CALCOLO CONTO INCASSO", incassato);

                            }



                            //Nel fare parseFloat bisogna convertire la , in ., altrimenti restituisce il numero senza decimali, se era un incasso non automatico
                            incassato = parseFloat(conv_prz_fc_ibr(incassato)).toFixed(2).replace('.', ',');
                            sconto = parseFloat(conv_prz_fc_ibr(sconto)).toFixed(2).replace('.', ',');


                            if (rec.BIS === lettera_conto_split()) {

                                rec.incassato = incassato;



                                rec.storicizzazione = storicizzazione;

                                if (numero_coperti !== undefined && !isNaN(numero_coperti) && numero_coperti !== "")
                                {
                                    testa_totale[rec.BIS] += parseInt(numero_coperti) * parseFloat(comanda.valore_coperto);


                                    if (parseInt(numero_coperti) - comanda.ultimi_coperti_importati !== 0) {
                                        annulla_comanda = false;
                                    }
                                }
                            } else
                            {
                                numero_coperti = rec.numero_coperti;
                            }

                            if (autoriz.toString() != "NaN") {
                                autoriz = "";
                            }

                            var DES = "RECORD TESTA";
                            if (b4 && b4.desc_art) {
                                DES = b4.desc_art;
                            } else if (comanda.set_tid === true) {
                                DES += " TID=" + comanda.TID;

                            }

                            let totale_testa = "0,00";
                            if (!isNaN(parseFloat(testa_totale[rec.BIS]))) {
                                totale_testa = parseFloat(testa_totale[rec.BIS]).toFixed(2).replace(".", ",");
                            } else {
                                bootbox.alert("Attenzione: il totale della testa ha un valore errato.");
                                annulla_comanda2 = true;
                            }

                            let totale_incassato = "";
                            if (!isNaN(parseFloat(rec.incassato)) || rec.incassato.trim() === "") {
                                totale_incassato = rec.incassato;
                            } else {
                                bootbox.alert("Attenzione: l incasso ha un valore errato.");
                                annulla_comanda2 = true;
                            }

                            let totale_carta_credito = "";
                            if (!isNaN(parseFloat(rec.carta_credito)) || rec.carta_credito.trim() === "") {
                                totale_carta_credito = rec.carta_credito;
                            } else {
                                bootbox.alert("Attenzione: il valore della carta di credito ha un valore errato.");
                                annulla_comanda2 = true;
                            }

                            let totale_bancomat = "";
                            if (!isNaN(parseFloat(rec.bancomat)) || rec.bancomat.trim() === "") {
                                totale_bancomat = rec.bancomat;
                            } else {
                                bootbox.alert("Attenzione: il  bancomat ha un valore errato.");
                                annulla_comanda2 = true;
                            }

                            output += '<TAVOLI>\r\n\
                <TC>T</TC>\r\n\
                <DATA>' + data_inizio_servizio + '</DATA>\r\n\
                <TAVOLO>' + aggZero(variabile_tavolo, 3) + '</TAVOLO>\r\n\
                <BIS>' + rec.BIS + '</BIS>\r\n\
                <PRG>000</PRG>\r\n\
                <ART>01</ART>\r\n\
                <ART2/>\r\n\
                <VAR/>\r\n\
                <DES>' + DES + '</DES>\r\n\
                <PRZ/>\r\n\
                <COSTO>0</COSTO>\r\n\
                <QTA/>\r\n\
                <SN>N</SN>\r\n\
                <CAT/>\r\n\
                <TOTALE>' + totale_testa + '</TOTALE>\r\n\
                <SCONTO>' + sconto + '</SCONTO>\r\n\
                <AUTORIZ>' + autoriz + '</AUTORIZ>\r\n\
                <INC>' + totale_incassato + '</INC>\r\n\
                <CARTACRED>' + totale_carta_credito + '</CARTACRED>\r\n\
                <BANCOMAT>' + totale_bancomat + '</BANCOMAT>\r\n\
                <ASSEGNI>0,00</ASSEGNI>\r\n\
                <NCARD1>' + rec.NCARD1 + '</NCARD1>\r\n\
                <NCARD2>' + rec.NCARD2 + '</NCARD2>\r\n\
                <NCARD3>' + rec.NCARD3 + '</NCARD3>\r\n\
                <NCARD4>' + rec.NCARD4 + '</NCARD4>\r\n\
                <NCARD5>' + rec.NCARD5 + '</NCARD5>\r\n\
                <NSEGN>' + rec.NSEGN + '</NSEGN>\r\n\
                <NEXIT>' + rec.NEXIT + '</NEXIT>\r\n\
                <TS>' + rec.storicizzazione + '</TS>\r\n\
                <NOME>' + comanda.operatore + '</NOME>\r\n\
                <ORA>' + ora + '</ORA>\r\n\
                <LIB1>1</LIB1>\r\n\
                <LIB2>' + rec.LIB2 + '</LIB2>\r\n\
                <LIB3/>\r\n\
                <LIB4/>\r\n\
                <LIB5>' + lib5 + '</LIB5>\r\n\
                <CLI/>\r\n\
                <QTAP>0</QTAP>\r\n\
                <NODO>' + settaggio_comandapiuconto + '</NODO>\r\n\
                <PORTATA/>\r\n\
                <NUMP>' + numero_coperti + '</NUMP>\r\n\
                <TAVOLOTXT>' + comanda.tavolotxt + '</TAVOLOTXT>\r\n\
                <ULTPORT>N</ULTPORT>\r\n\
                <AGG_GIAC>N</AGG_GIAC>\r\n\
                </TAVOLI>\r\n';
                            //LIB5 S=CONTO STAMPATO

                        });
                        if (annulla_comanda2 === true) {
                            return false;
                        }
                        output += '</dataroot>';
                        output = output.replace(/>null</gi, '><');
                        output = output.replace(/>undefined</gi, '><');

                        console.log("OUTPUT XML CONTO" + comanda.tavolotxt, output);


                        var d = new Date();
                        var Y = d.getFullYear();
                        var M = addZero(d.getDate(), 2);
                        var D = addZero((d.getMonth() + 1), 2);
                        var h = addZero(d.getHours(), 2);
                        var m = addZero(d.getMinutes(), 2);
                        var s = addZero(d.getSeconds(), 2);
                        var ms = addZero(d.getMilliseconds(), 3);
                        var data_tablet = h + '' + m + '' + s;

                        var id_tablet = "29";
                        var id_temp = leggi_get_url('id');

                        if (id_temp !== undefined && id_temp !== null && id_temp !== 'null' && id_temp.length === 2) {
                            id_tablet = id_temp;
                        }

                        var prom_storicizzazione = new Promise(function (resolve, reject) {

                            if (annulla_comanda === false) {

                                var nome_file = "/SDS/PDA" + id_tablet + "_" + data_tablet + BIS + ".XM_";

                                oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].nome_file_sds = "_" + comanda.folder_number + "/" + nome_file;
                                oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].contenuto_file_sds = output;
                                oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].tipo_file_sds = "SDS";

                                comanda.xml.salva_file_xml("_" + comanda.folder_number + "/" + nome_file, output, "SDS", function () {

                                    if (comanda.storicizzazione_auto === 'N')
                                    {

                                        if (comanda.licenza_vera === "DeFiorenze") {
                                            nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + '.1';
                                        } else {
                                            if (comanda.incasso_auto === "S" || comanda.incasso_auto === "s") {
                                                nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + '.4';
                                            } else
                                            {
                                                nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + '.7';
                                            }
                                        }

                                        console.log("esporta_xml_tavolo", nome_file);

                                        //esporta_xml_tavolo(tipo, function () {

                                        if (bis_presente === true) {
                                            nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + '.8';
                                        }
                                        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].nome_file_planing = "_" + comanda.folder_number + "/" + nome_file;
                                        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].contenuto_file_planing = "";
                                        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].tipo_file_planing = "PLANING";

                                        comanda.xml.salva_file_xml("_" + comanda.folder_number + "/" + nome_file, "", "PLANING", function () {
                                            resolve(true);
                                        });
                                        //});
                                    } else if (comanda.storicizzazione_auto === 'S')
                                    {
                                        //esporta_xml_tavolo(tipo, function () {
                                        resolve(true);
                                        //});
                                    }
                                });
                            } else
                            {
                                //IN CASO NON CI SIANO NUOVI DATI IN COMANDA
                                if (variabile_tavolo !== undefined && typeof (variabile_tavolo) === "string") {
                                    comanda.xml.elimina_tavolo_occupato("_" + comanda.folder_number + "/COND/Tav" + variabile_tavolo.replace(/^0+/, '') + ".CO1", false, function () {
                                        resolve(true);
                                    });
                                }
                            }
                        });

                        //Promessa per risolvere il bug del btn tavoli se non la usassi
                        //Serve ad esportare il file del SDS dopo l'esportazione sicura del tavolo
                        prom_storicizzazione.then(function () {

                            var data_intera = comanda.funzionidb.data_attuale();
                            var data = data_intera.substring(0, 10);
                            var ora = data_intera.substring(11, 16);
                            var testo_query = "update comanda set stampata_sn='S',data_stampa='" + data + "',ora_stampa='" + ora + "',fiscalizzata_sn='N' where stato_record='ATTIVO' and ntav_comanda='" + variabile_tavolo + "'";
                            ////console.log(testo_query);
                            comanda.sock.send({tipo: "comanda_stampata", operatore: comanda.operatore, query: testo_query});
                            comanda.sincro.query(testo_query, function () {

                                //---Una volta esportato per certo il file, torna ai tavoli o al parcheggio(impossibile nel caso di Padova)---
                                if (comanda.tavolotxt !== "BANCO" && comanda.tavolotxt !== "BAR" && comanda.tavolotxt.left(7) !== "ASPORTO" && comanda.tavolotxt !== "TAKEAWAY") {

                                    btn_tavoli("CONTO");
                                    $('.tasto_bestellung').css('pointer-events', '');
                                    $('.tasto_konto').css('pointer-events', '');

                                    $('.tasto_quittung').css('pointer-events', '');
                                    $('.tasto_rechnung').css('pointer-events', '');

                                    $('.tasto_bargeld').css('pointer-events', '');

                                } else
                                {
                                    btn_parcheggia();

                                }

                            });
                            var date = new Date();
                            var ora = addZero(date.getHours(), 2) + ':' + addZero(date.getMinutes(), 2);
                            var testo_query = "update tavoli SET colore='4',occupato='0',ora_apertura_tavolo = ( CASE WHEN ( ora_apertura_tavolo = '-' OR ora_apertura_tavolo = 'null' OR ora_apertura_tavolo = ''  OR ora_apertura_tavolo = NULL ) THEN '" + ora + "' ELSE ora_apertura_tavolo END ), ora_ultima_comanda = (  CASE WHEN ( ora_apertura_tavolo = 'null'  OR ora_apertura_tavolo = '' OR ora_apertura_tavolo = '-'  OR ora_apertura_tavolo = NULL ) THEN ora_ultima_comanda ELSE '" + ora + "' END )  where numero = '" + variabile_tavolo + "';";
                            comanda.sock.send({tipo: "comanda_stampata", operatore: comanda.operatore, query: testo_query});
                            comanda.sincro.query(testo_query, function () {
                                disegna_ultimo_tavolo_modificato(testo_query);

                            });
                        });
                    });
                }
            });
        }
    });
}

let xClick = 0;
function x() {
    if ($('#tab_dx').hasClass('tasti_incasso_visibili')) {
        xClick++;
        if (xClick === 8) {
            seleziona_metodo_pagamento('X');
        }
    }
}

$(document).on(comanda.eventino, function (event) {

    if ($(event.target).hasClass("totale_scontrino") !== true) {

        xClick = 0;

    }
});


function stampa_scontrino(tipo, non_serve_su_ibrido, pagamento_con_carta) {

    var bis_presente = false;
    var annulla_comanda2 = false;

    var procedi = function () {

        $('.tasto_bestellung').css('pointer-events', 'none');
        $('.tasto_konto').css('pointer-events', 'none');

        $('.tasto_quittung').css('pointer-events', 'none');
        $('.tasto_rechnung').css('pointer-events', 'none');

        $('.tasto_bargeld').css('pointer-events', 'none');

        if (comanda.conto === '#prodotti_conto_separato_grande' || comanda.conto === '#prodotti_conto_separato_2_grande') {
            $('.loader2').show();
            $('#conto_separato_grande').css('pointer-events', 'none');

            setTimeout(function () {
                $('#conto_separato_grande').css('pointer-events', '');
                $('.loader2').hide();
            }, 4500);
        }



        var variabile_tavolo = comanda.tavolo;
        var BIS = '';

        if (comanda.split_abilitato === true) {
            if (lettera_conto_split().length >= 1)
            {
                BIS = "-" + lettera_conto_split();
            }
        }
        var numero_coperti = $("#numero_coperti").val();

        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)] = new Object();




        //KS0 GERMANIA
        //C ITALIA
        //S CONTO CHE CHIUDE E STORICIZZA AUTOMATICO

        //---LEGGO I SETTAGGI---

        var testo_query = "select NSEGN from comanda where ntav_comanda = '" + variabile_tavolo + "' and posizione = 'CONTO' and stato_record = 'ATTIVO' ORDER BY cast(NSEGN as int) DESC LIMIT 1;";
        var rnsegn = alasql(testo_query);


        var nsegn_conto_2 = "1";


        if (rnsegn !== undefined && rnsegn[0] !== undefined && rnsegn[0].NSEGN !== null && !isNaN(rnsegn[0].NSEGN))
        {
            if (rnsegn[0].NSEGN === "") {
                rnsegn[0].NSEGN = "0";
            }
            nsegn_conto_2 = (parseInt(rnsegn[0].NSEGN) + 1).toString();
        }




        //---Creo la data di adesso dall'anno ai minuti---
        var d = new Date();
        var Y = d.getFullYear().toString();
        var D = addZero(d.getDate(), 2);
        var M = addZero((d.getMonth() + 1), 2);
        var h = addZero(d.getHours(), 2);
        var m = addZero(d.getMinutes(), 2);
        var s = addZero(d.getSeconds(), 2);
        var ms = addZero(d.getMilliseconds(), 3);
        var data = D + '/' + M + '/' + Y;
        var ora = h + ':' + m + ':' + s;

        //---Query per prelevare il conto dal db locale---
        var variabile = '';
        var testo_query = " select  NCARD3,AGG_GIAC,LIB4,LIB2,BIS,dest_stampa_2,ora_,operatore,sconto_perc,perc_iva,NSEGN,QTAP,numero_conto,ordinamento,ultima_portata,peso_ums,cod_articolo,dest_stampa,contiene_variante,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita,stampata_sn from comanda where desc_art NOT LIKE 'RECORD TESTA%' and  contiene_variante='1' and ntav_comanda='" + variabile_tavolo + "' and posizione='CONTO' and stato_record='ATTIVO'  \n\
                            union all\n\
                                select  NCARD3,AGG_GIAC,LIB4,LIB2,BIS,dest_stampa_2,ora_,operatore,sconto_perc,perc_iva,NSEGN,QTAP,numero_conto,ordinamento,ultima_portata,peso_ums,cod_articolo,dest_stampa,contiene_variante,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita,stampata_sn from comanda where desc_art NOT LIKE 'RECORD TESTA%' and  (contiene_variante !='1' or contiene_variante is null) and length(nodo)=3 and ntav_comanda='" + variabile_tavolo + "' and posizione='CONTO' and stato_record='ATTIVO'  \n\
                            union all\n\
                                select  NCARD3,AGG_GIAC,LIB4,LIB2,BIS,dest_stampa_2,ora_,operatore,sconto_perc,perc_iva,NSEGN,QTAP,numero_conto,ordinamento,ultima_portata,peso_ums,cod_articolo,dest_stampa,contiene_variante,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita,stampata_sn from comanda where desc_art NOT LIKE 'RECORD TESTA%' and  length(nodo)=7 and ntav_comanda='" + variabile_tavolo + "'  and posizione='CONTO' and stato_record='ATTIVO';";


        //console.log(testo_query);
        var result = alasql(testo_query);
        //---Risultato della query del conto---

        result = alasql("select * from ? order by dest_stampa,portata,nodo,prog_inser", [result]);


        //---Se non c'ÃƒÆ’Ã‚Â¨ niente dentro---
        if (result.length === 0)
        {
            //---Se c'ÃƒÆ’Ã‚Â¨ il CallBack lo Esco---
            if (typeof (cB) === "function") {
                console.log(" esporta_xml_tavolo tavolo inesistente");
                cB(true);

            }

            //Con il nuovo settaggio non va piÃƒÆ’Ã‚Â¹ esportato l'xml tavolo
            //esporta_xml_tavolo("SENZA_AGGIUNTE", function () {
            if (variabile_tavolo !== undefined && typeof (variabile_tavolo) === "string") {
                comanda.xml.elimina_tavolo_occupato("_" + comanda.folder_number + "/COND/Tav" + variabile_tavolo.replace(/^0+/, '') + ".CO1", false, function () {
                    btn_tavoli("CONTO");
                });
            }
//});
            //});

            //---Fermo la funzione tornando un valore falso---
            return false;
        }



        //console.log("result", result);



        //INTESTAZIONE
        var output = '<?xml version="1.0" standalone="yes"?>\r\n\
                        <dataroot xmlns="http://www.w3.org/2001/TAVOLI.xsd">\r\n';

        //VALORI DEFAULT
        var incasso_annullato = false;
        var storicizzazione = 'N';
        var spref = '';
        var nsegn = '';
        var nsegn_storicizzazione = '';
        var qtap = '';

        if (comanda.conto === '#prodotti_conto_separato_2_grande') {
            nsegn_storicizzazione = nsegn_conto_2;

            var prezzo_1 = $('#totale_scontrino_separato_1').html();
            if (parseFloat(prezzo_1) === 0) {
                spref = 'S';
            }

        } else if (comanda.conto === '#prodotti_conto_separato_grande') {
            nsegn_storicizzazione = nsegn_conto_2;

            var prezzo_2 = $('#totale_scontrino_separato_2').html();
            if (parseFloat(prezzo_2) === 0) {
                spref = 'S';
            }
        } else if (nsegn_conto_2 !== '1' && !isNaN(nsegn_conto_2))
        {
            nsegn_storicizzazione = nsegn_conto_2;
            spref = 'S';
        } else
        {
            nsegn_storicizzazione = '';
            spref = 'S';
        }



        if (tipo === 'FATTURA')
        {
            storicizzazione = "R" + spref + nsegn_storicizzazione;
        } else if (tipo === 'SCONTRINO FISCALE')
        {
            storicizzazione = "Q" + spref + nsegn_storicizzazione;
        } else if (tipo === 'X') {
            storicizzazione = "#";
        }


        if (nsegn_storicizzazione === "NaN") {
            nsegn_storicizzazione = "";
        }



        //---Intestazione XML---
        var i = 0;
        var cod_articolo_princ;
        var ts_princ;
        var prog_principale;
        var descrizione_articolo;
        var ordinamento = '';
        var incasso_parziale = 0;
        var testa_totale = new Object();
        var qta_princ = 1;
        var importo_gutshein_corpo = "";

        //------
        result.forEach(function (obj) {

            if (obj.prezzo_un.trim() === "") {
                obj.prezzo_un = "0,00";
            }

            nsegn = obj.NSEGN;

            if (obj.QTAP === '') {
                obj.QTAP = '0';
            }

            qtap = obj.QTAP;

            if (comanda.conto === '#prodotti_conto_separato_2_grande') {


                if (obj.numero_conto === '2' && obj.QTAP === '0') {
                    qtap = obj.quantita;
                    nsegn = nsegn_conto_2;
                    importo_gutshein_corpo = $('#gutshein__importo_gutshein').val().replace('.', ',');
                    incasso_parziale += parseFloat(obj.prezzo_un.replace(',', '.'));
                }

            } else if (comanda.conto === '#prodotti_conto_separato_grande') {



                if ((obj.numero_conto === '' || obj.numero_conto === '1') && obj.QTAP === '0') {
                    qtap = obj.quantita;
                    nsegn = nsegn_conto_2;
                    importo_gutshein_corpo = $('#gutshein__importo_gutshein').val().replace('.', ',');
                    incasso_parziale += parseFloat(obj.prezzo_un.replace(',', '.'));

                }
            } else if (nsegn_conto_2 !== '1' && obj.QTAP === '0')
            {


                nsegn = nsegn_conto_2;
                importo_gutshein_corpo = $('#gutshein__importo_gutshein').val().replace('.', ',');
                qtap = obj.quantita;
                incasso_parziale += parseFloat(obj.prezzo_un.replace(',', '.'));
            }

            if ($(comanda.conto + " tr").length === 0) {
                incasso_annullato = true;

            }


            //---Imposto le variabili da inserire nell'xml del SDS---
            //PRIMA ERA +1 MA NON SE NE CAPISCE IL MOTIVO
            obj.prog_inser = parseInt(obj.prog_inser);
            obj.dest_stampa !== undefined ? ts_princ = obj.dest_stampa : ts_princ = '1';
            obj.peso_ums !== undefined ? obj.peso_ums : obj.peso_ums = '';
            obj.ordinamento !== undefined ? ordinamento = obj.ordinamento : ordinamento = '01';
            prog_principale = aggZero(obj.prog_inser, 3);
            descrizione_articolo = obj.desc_art;
            console.log(obj.dest_stampa);
            console.log(obj.dest_stampa !== undefined);
            console.log(ts_princ);
            var contiene_variante = '';
            obj.contiene_variante === '1' ? contiene_variante = 'SI' : contiene_variante = '';

            //---Imposto le variabili in caso di VARIANTI---
            if (obj.desc_art[0] === "+" || obj.desc_art[0] === "-" || obj.desc_art[0] === "x" || obj.desc_art[0] === "*" || obj.desc_art[0] === "=")
            {
                prog_principale = aggZero(prog_principale.substring(0, 3), 3) + ' ' + aggZero(obj.prog_inser, 3) + ' ';
                descrizione_articolo = obj.desc_art.substring(1);
                variabile = obj.desc_art[0];
            } else if (obj.desc_art[0] === "(") {
                prog_principale = aggZero(prog_principale.substring(0, 3), 3) + ' ' + aggZero(obj.prog_inser, 3) + ' ';
                variabile = "*";
            } else
            {
                cod_articolo_princ = obj.cod_articolo;
                variabile = '';
            }

            //---Imposto il valore di default di STAMPATA_SN---
            var stampata_sn = 'N';
            //Il TakeAway puÃ² stampare sempre
            if (obj.stampata_sn === 'S' || comanda.tavolotxt === '*TAKEAWAY*')
            {
                stampata_sn = 'S';
            }


            //---Imposto il valore di default per la storicizzazione automatica---
            //e per la comanda
            var ts = '';
            ts = ts_princ;

            if (comanda.gutshein === true) {
                stampata_sn = "S";
            } else if (stampata_sn === 'N')
            {
                incasso_annullato = true;
                var ora = h + ':' + m + ':' + s;
            } else
            {
                var ora = obj.ora_;
            }

            var operatore = obj.operatore;
            if (operatore.length === 0) {
                operatore = comanda.operatore;
            }

            if (testa_totale[obj.BIS] === undefined) {
                testa_totale[obj.BIS] = 0;
            }
            testa_totale[obj.BIS] += parseFloat(conv_prz_fc_ibr(obj.prezzo_un)) * parseInt(obj.quantita);

            var iva = "0";
            if (obj.perc_iva.trim().length > 0) {
                iva = obj.perc_iva;
            }

            var ncard3 = '';

            /* se il conto corrisponde al conto che sto storicizzando (puo anche essere vuoto che significa storicizza tutto) */
            if (nsegn === nsegn_storicizzazione) {
                if (pagamento_con_carta === true) {
                    /*carta credito*/
                    ncard3 = '3';
                } else {
                    /*contanti*/
                    ncard3 = '1';
                }
            } else {
                if (obj.NCARD3) {
                    ncard3 = obj.NCARD3;
                } /*non possono esserci eccezioni*/
            }

            let tab_iva = alasql("select aliquota from tabella_iva where lingua='" + comanda.lingua_stampa + "';");
            let percentuali_iva = tab_iva.map(d => d.aliquota);

            if (percentuali_iva.indexOf(obj.perc_iva.trim()) === -1) {
                /*bootbox.alert("Attenzione: l'IVA dell'articolo " + descrizione_articolo + " non &egrave; presente nella tabella IVA. Modificalo e ri-lancia l'operazione");
                 annulla_comanda2 = true;
                 return false;*/

                obj.perc_iva = comanda.percentuale_iva_default;
            } else if (isNaN(parseFloat(conv_prz_fc_ibr(obj.prezzo_un)).toFixed(2))) {
                bootbox.alert("Attenzione: il prezzo dell'articolo " + descrizione_articolo + " non &egrave; valido. Modificalo e ri-lancia l'operazione");
                annulla_comanda2 = true;
                return false;
            } else if (isNaN(parseInt(obj.quantita)) || parseInt(obj.quantita) < 1) {
                bootbox.alert("Attenzione: la quantit&agrave; dell'articolo " + descrizione_articolo + " non &egrave; valida. Modificala e ri-lancia l'operazione");
                annulla_comanda2 = true;
                return false;
            }

            //---XML SERVER DI STAMPA---
            output += '<TAVOLI>\r\n';
            output += '<TC>C</TC>\r\n';
            output += '<DATA>' + data_inizio_servizio + '</DATA>\r\n';
            output += '<TAVOLO>' + aggZero(variabile_tavolo, 3) + '</TAVOLO>\r\n';
            output += '<BIS>' + obj.BIS + '</BIS>\r\n';
            output += '<PRG>' + aggZero(obj.prog_inser, 3) + '</PRG>\r\n';
            output += '<ART>' + cod_articolo_princ + '</ART>\r\n';
            output += '<ART2>' + obj.cod_articolo + '</ART2>\r\n';
            output += '<VAR>' + variabile + '</VAR>\r\n';
            output += '<DES>' + descrizione_articolo + '</DES>\r\n';
            output += '<PRZ>' + parseFloat(conv_prz_fc_ibr(obj.prezzo_un)).toFixed(2).replace('.', ',') + '</PRZ>\r\n';
            output += '<COSTO>0</COSTO>\r\n';
            output += '<QTA>' + obj.quantita + '</QTA>\r\n';
            output += '<SN>' + stampata_sn + '</SN>\r\n';
            output += '<CAT>' + obj.categoria + '</CAT>\r\n';
            output += '<TOTALE />\r\n';
            output += '<SCONTO />\r\n';
            output += '<AUTORIZ>' + obj.sconto_perc + '</AUTORIZ>\r\n';
            output += '<INC>0</INC>\r\n';
            output += '<CARTACRED>' + importo_gutshein_corpo + '</CARTACRED>\r\n';
            output += '<BANCOMAT/>\r\n';
            output += '<ASSEGNI>' + iva + '</ASSEGNI>\r\n';
            output += '<NCARD1>' + contiene_variante + '</NCARD1>\r\n';
            output += '<NCARD2/>\r\n';
            output += '<NCARD3>' + ncard3 + '</NCARD3>\r\n';
            output += '<NCARD4/>\r\n';
            output += '<NCARD5>' + ordinamento + '</NCARD5>\r\n'; //ORDINAMENTO
            output += '<NSEGN>' + nsegn + '</NSEGN>\r\n';
            output += '<NEXIT/>\r\n';
            output += '<TS>' + ts + '</TS>\r\n'; //' + ts_princ + '
            output += '<NOME>' + operatore + '</NOME>\r\n';
            output += '<ORA>' + ora + '</ORA>\r\n';
            output += '<LIB1>1</LIB1>\r\n';
            output += '<LIB2>' + obj.LIB2 + '</LIB2>\r\n';
            output += '<LIB3/>\r\n';
            output += '<LIB4>' + obj.LIB4 + '</LIB4>\r\n';
            output += '<LIB5>' + obj.dest_stampa_2 + '</LIB5>\r\n';
            output += '<CLI/>\r\n';
            output += '<QTAP>' + qtap + '</QTAP>\r\n';
            output += '<NODO>' + obj.nodo + '</NODO>\r\n';
            output += '<PORTATA>' + obj.portata + '</PORTATA>\r\n';
            output += '<NUMP>0</NUMP>\r\n';
            output += '<TAVOLOTXT>' + comanda.tavolotxt + '</TAVOLOTXT>\r\n';
            output += '<ULTPORT>' + obj.ultima_portata + '</ULTPORT>\r\n';
            output += '<AGG_GIAC>' + obj.AGG_GIAC + '</AGG_GIAC>\r\n';
            output += '</TAVOLI>\r\n';
            i++;

        });

        if (annulla_comanda2 === true) {
            return false;
        }
        //---Fine delle righe corpo---

        //var testo_query = 'select (select prezzo_un from comanda where stato_record="ATTIVO" and ntav_comanda="' + variabile_tavolo + '" and posizione="SCONTO") as sconto,(select desc_art from comanda where stato_record="ATTIVO" and ntav_comanda="' + variabile_tavolo + '" and posizione="SCONTO") as desc_sconto,(select incassato from comanda where  desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as incassato,(select costo from comanda where  desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as costo,operatore,(select contiene_variante from comanda where  desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as NCARD1,(select NCARD2 from comanda where  desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as NCARD2,(select NCARD3 from comanda where  desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as NCARD3,(select NCARD4 from comanda where  desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as NCARD4,(select NCARD5 from comanda where  desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as NCARD5,(select NSEGN  from comanda where  desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as NSEGN,(select NEXIT from comanda where  desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as NEXIT,(select carta_credito from comanda where  desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as carta_credito,(select bancomat from comanda where  desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" ) as bancomat, (select nump_xml from comanda where stato_record="ATTIVO" and ntav_comanda="' + variabile_tavolo + '" order by nump_xml desc limit 1) as numero_coperti \n\ from comanda where stato_record="ATTIVO" and (posizione="CONTO" or posizione="COPERTO") and ntav_comanda="' + variabile_tavolo + '" group by id order by prog_inser asc;';
        //Query testa


        console.log("QUERY SCONTO INCASSATO", testo_query);

        comanda.sincro.query('select BIS from comanda where stato_record="ATTIVO" and posizione="CONTO" and ntav_comanda="' + variabile_tavolo + '" group by id,BIS order by prog_inser asc;', function (b1) {


            b1.forEach(function (e) {
                var bi2 = alasql('select desc_art as desc_sconto,prezzo_un as sconto from comanda where stato_record="ATTIVO" and ntav_comanda="' + variabile_tavolo + '" and BIS="' + e.BIS + '" and posizione="SCONTO" limit 1;');
                var bi3 = alasql('select nump_xml as numero_coperti from comanda  where stato_record="ATTIVO" and ntav_comanda="' + variabile_tavolo + '" and BIS="' + e.BIS + '" order by nump_xml desc limit 1;');
                var bi4 = alasql('select desc_art,LIB2,dest_stampa as storicizzazione,prezzo_un,incassato,costo,operatore,contiene_variante as NCARD1,NCARD2,NCARD3,NCARD4,NCARD5,NSEGN,NEXIT,carta_credito,bancomat from comanda  where ntav_comanda="' + variabile_tavolo + '"  and  desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" and BIS="' + e.BIS + '" limit 1;');
                var b2 = bi2[0];
                var b3 = bi3[0];
                var b4 = bi4[0];

                var rec = new Object();
                // SEZIONE BIS
                rec.BIS = e.BIS;
                if (rec.BIS !== "") {
                    bis_presente = true;
                }
                // SEZIONE SCONTISTICA
                rec.desc_sconto = "";
                rec.sconto = "";
                if (b2 !== undefined) {
                    rec.desc_sconto = b2.desc_sconto;
                    rec.sconto = b2.sconto;
                }
                // SEZIONE COPERTI
                var numero_coperti = $("#numero_coperti").val();
                rec.numero_coperti = "";

                if (b3 !== undefined) {
                    rec.numero_coperti = b3.numero_coperti;

                }
                console.log("BIS COPERTI 0", "LCS", lettera_conto_split(), "BIS", rec.BIS, "ncop", numero_coperti, "ncop2", rec.numero_coperti, "B3", b3);

                // SEZIONE PAGAMENTI
                rec.desc_art = "";
                rec.incassato = "";
                rec.costo = "";
                rec.operatore = "";
                rec.NCARD1 = "";
                rec.NCARD2 = "";
                rec.NCARD3 = "";
                rec.NCARD4 = "";
                rec.NCARD5 = "";
                rec.NSEGN = "";
                rec.NEXIT = "";
                rec.carta_credito = "";
                rec.bancomat = "";
                rec.storicizzazione = "";
                rec.LIB2 = "";
                if (b4 !== undefined) {
                    rec.desc_art = b4.desc_art;
                    rec.incassato = b4.incassato;
                    rec.costo = b4.costo;
                    rec.operatore = b4.operatore;
                    rec.NCARD1 = b4.NCARD1;
                    rec.NCARD2 = b4.NCARD2;
                    rec.NCARD3 = b4.NCARD3;
                    rec.NCARD4 = b4.NCARD4;
                    rec.NCARD5 = b4.NCARD5;
                    rec.NSEGN = b4.NSEGN;
                    rec.NEXIT = b4.NEXIT;
                    rec.carta_credito = b4.carta_credito;
                    rec.bancomat = b4.bancomat;
                    rec.storicizzazione = b4.storicizzazione;
                    rec.LIB2 = b4.LIB2;
                }

                //---Variabili testa---
                var incassato = 0;
                var costo = 0;
                var costo_parziale = 0;
                var autoriz = 0;
                var carte_parziale = 0;
                var carte = 0;


                var sconto = '0.00';

                if (rec !== undefined && typeof rec.sconto === 'string' && rec !== "") {
                    if (rec.sconto !== undefined && rec.sconto !== "" && rec.sconto.substr(-1) === "%") {

                        //Bisogna fare il calcolo
                        sconto = testa_totale[rec.BIS] / 100 * rec.sconto.substr(1).slice(0, -1);

                        sconto = sconto.toString();
                        console.log("CALCOLO CONTO SCONTO", sconto);
                    } else if (rec.sconto !== undefined && rec.sconto !== "") {
                        sconto = rec.sconto.substr(1);
                    }
                    autoriz = rec.desc_sconto;


                }
                //PER LA SAGRA NON SERVE PERCHE' INCASSA IN AUTOMATICO
                //E POI SE SI RIPRENDE UNA COMANDA DOPO AVER LASCIATO LA L'INCASSO FA CASINO
                //DA VERIFICARE



                incassato = testa_totale[rec.BIS] - parseFloat(sconto);
                incassato = incassato.toString();



                if (rec.incassato === null) {
                    rec.incassato = '0,00';
                }

                if (rec.costo === null) {
                    rec.costo = '0,00';
                }

                if (rec.carta_credito === null) {
                    rec.carta_credito = '0,00';
                }

                var incassato_testa_temp = parseFloat(conv_prz_fc_ibr(rec.incassato).replace(',', '.'));

                var condizione_succ = false;

                if ($('#metodo_pagamento_gutshein').hasClass('in') === true) {
                    costo_parziale = parseFloat($('#gutshein__importo_gutshein:visible').val().replace(',', '.'));
                    incasso_parziale = $('#gutshein_importo_contanti:visible').val().replace(',', '.');
                    $('#metodo_pagamento_gutshein').modal('hide');
                    condizione_succ = true;
                }

                if (pagamento_con_carta === true) {
                    carte_parziale = incasso_parziale;
                    incasso_parziale = 0;
                }



                if (spref !== 'S' || condizione_succ === true) {
                    incassato = (parseFloat(incasso_parziale) + incassato_testa_temp).toString();
                } else if (spref === "S") {
                    //vuol dire che è conto separato

                    if (pagamento_con_carta === true) {
                        //incassato è il totale del conto intero

                        carte_parziale = (parseFloat(incassato) - parseFloat(conv_prz_fc_ibr(rec.carta_credito)) - parseFloat(costo_parziale) - parseFloat(conv_prz_fc_ibr(rec.costo)) - parseFloat(incasso_parziale) - parseFloat(conv_prz_fc_ibr(rec.incassato))).toFixed(2);
                        //l'incassato è il totale
                        //il costo parziale è per togliere l'importo di questo conto separato del gutshein
                        //gutshein che erano già nei conti precedenti
                        //incasso parziale dovrebbe essere il totale articoli di quel conto separato, però nel caso di guthein se paghi con carta è 0, sennò è l'importo dei gutshein
                        //incassato dei conti precedenti


                    }

                    incassato = (parseFloat(incassato) - parseFloat(costo_parziale) - parseFloat(conv_prz_fc_ibr(rec.costo)) - parseFloat(carte_parziale) - parseFloat(conv_prz_fc_ibr(rec.carta_credito))).toFixed(2);



                }

                if (typeof carte_parziale === "string") {
                    carte_parziale = parseFloat(carte_parziale);
                }

                if (typeof costo_parziale === "string") {
                    costo_parziale = parseFloat(costo_parziale);
                }

                //Deve stare fuori perche anche se incassassi senza Gutshein potrei averne dalla volta precedente
                carte = parseFloat((carte_parziale + parseFloat(conv_prz_fc_ibr(rec.carta_credito))).toString()).toFixed(2).replace('.', ',');
                costo = parseFloat((costo_parziale + parseFloat(conv_prz_fc_ibr(rec.costo))).toString()).toFixed(2).replace('.', ',');
                incassato = parseFloat(conv_prz_fc_ibr(incassato)).toFixed(2).replace('.', ',');
                sconto = parseFloat(conv_prz_fc_ibr(sconto)).toFixed(2).replace('.', ',');



                if (rec.BIS === lettera_conto_split()) {

                    rec.incassato = incassato;



                    rec.storicizzazione = storicizzazione;

                    if (numero_coperti !== undefined && !isNaN(numero_coperti) && numero_coperti !== "")
                    {
                        testa_totale[rec.BIS] += parseInt(numero_coperti) * parseFloat(comanda.valore_coperto);


                        if (parseInt(numero_coperti) - comanda.ultimi_coperti_importati !== 0) {
                            annulla_comanda = false;
                        }
                    }
                } else
                {
                    numero_coperti = rec.numero_coperti;
                    costo = rec.costo;
                }




                testa_totale[rec.BIS] = parseFloat(testa_totale[rec.BIS]).toFixed(2).replace(".", ",");



                /*if (pagamento_con_carta === true) {
                 rec.carta_credito = rec.incassato;
                 rec.incassato = "0,00";
                 }*/
                if (autoriz.toString() != "NaN") {
                    autoriz = "";
                }

                if (comanda.gutshein === true) {
                    autoriz = "RICARICA";
                    storicizzazione = "QS";
                }

                var DES = "RECORD TESTA";
                if (b4 && b4.desc_art) {
                    DES = b4.desc_art;
                } else if (comanda.set_tid === true) {
                    DES += " TID=" + comanda.TID;

                }

                let totale_testa = "0,00";
                if (!isNaN(parseFloat(testa_totale[rec.BIS]))) {
                    totale_testa = testa_totale[rec.BIS];
                } else {
                    bootbox.alert("Attenzione: il totale della testa ha un valore errato.");
                    annulla_comanda2 = true;
                }

                let totale_incassato = "";
                if (!isNaN(parseFloat(rec.incassato)) || rec.incassato.trim() === "") {
                    totale_incassato = rec.incassato;
                } else {
                    bootbox.alert("Attenzione: l incasso ha un valore errato.");
                    annulla_comanda2 = true;
                }

                let totale_carta_credito = "";
                if (!isNaN(parseFloat(carte)) || carte.trim() === "") {
                    totale_carta_credito = carte;
                } else {
                    bootbox.alert("Attenzione: il valore della carta di credito ha un valore errato.");
                    annulla_comanda2 = true;
                }

                let totale_bancomat = "";
                if (!isNaN(parseFloat(rec.bancomat)) || rec.bancomat.trim() === "") {
                    totale_bancomat = rec.bancomat;
                } else {
                    bootbox.alert("Attenzione: il  bancomat ha un valore errato.");
                    annulla_comanda2 = true;
                }

                //---XML TESTA---
                output += '<TAVOLI>\r\n\
                <TC>T</TC>\r\n\
                <DATA>' + data_inizio_servizio + '</DATA>\r\n\
                <TAVOLO>' + aggZero(variabile_tavolo, 3) + '</TAVOLO>\r\n\
                <BIS>' + rec.BIS + '</BIS>\r\n\
                <PRG>000</PRG>\r\n\
                <ART>01</ART>\r\n\
                <ART2/>\r\n\
                <VAR/>\r\n\
                <DES>' + DES + '</DES>\r\n\
                <PRZ/>\r\n\
                <COSTO>' + costo + '</COSTO>\r\n\
                <QTA/>\r\n\
                <SN>N</SN>\r\n\
                <CAT/>\r\n\
                <TOTALE>' + totale_testa + '</TOTALE>\r\n\
                <SCONTO>' + sconto + '</SCONTO>\r\n\
                <AUTORIZ>' + autoriz + '</AUTORIZ>\r\n\
                <INC>' + totale_incassato + '</INC>\r\n\
                <CARTACRED>' + totale_carta_credito + '</CARTACRED>\r\n\
                <BANCOMAT>' + totale_bancomat + '</BANCOMAT>\r\n\
                <ASSEGNI>0,00</ASSEGNI>\r\n\
                <NCARD1>' + rec.NCARD1 + '</NCARD1>\r\n\
                <NCARD2>' + rec.NCARD2 + '</NCARD2>\r\n\
                <NCARD3>' + rec.NCARD3 + '</NCARD3>\r\n\
                <NCARD4>' + rec.NCARD4 + '</NCARD4>\r\n\
                <NCARD5>' + rec.NCARD5 + '</NCARD5>\r\n\
                <NSEGN>' + rec.NSEGN + '</NSEGN>\r\n\
                <NEXIT>' + rec.NEXIT + '</NEXIT>\r\n\
                <TS>' + rec.storicizzazione + '</TS>\r\n\
                <NOME>' + comanda.operatore + '</NOME>\r\n\
                <ORA>' + ora + '</ORA>\r\n\
                <LIB1>1</LIB1>\r\n\
                <LIB2>' + rec.LIB2 + '</LIB2>\r\n\
                <LIB3/>\r\n\
                <LIB4/>\r\n\
                <LIB5>S</LIB5>\r\n\
                <CLI/>\r\n\
                <QTAP>0</QTAP>\r\n\
                <NODO></NODO>\r\n\
                <PORTATA/>\r\n\
                <NUMP>' + numero_coperti + '</NUMP>\r\n\
                <TAVOLOTXT>' + comanda.tavolotxt + '</TAVOLOTXT>\r\n\
                <ULTPORT>N</ULTPORT>\r\n\
                <AGG_GIAC>N</AGG_GIAC>\r\n\
                </TAVOLI>\r\n';

            });
            if (annulla_comanda2 === true) {
                return false;
            }
            output += '</dataroot>';

            //---Toglie i null e undefined dall'output---
            output = output.replace(/>null</gi, '><');
            output = output.replace(/>undefined</gi, '><');

            console.log("OUTPUT XML CONTO" + comanda.tavolotxt, output);

            //---Crea una data---
            var d = new Date();
            var Y = d.getFullYear();
            var M = addZero(d.getDate(), 2);
            var D = addZero((d.getMonth() + 1), 2);
            var h = addZero(d.getHours(), 2);
            var m = addZero(d.getMinutes(), 2);
            var s = addZero(d.getSeconds(), 2);
            var ms = addZero(d.getMilliseconds(), 3);
            var data_tablet = h + '' + m + '' + s;

            //---Legge l'id del tablet dall'url---
            var id_tablet = "29";
            var id_temp = leggi_get_url('id');

            if (id_temp !== undefined && id_temp !== null && id_temp !== 'null' && id_temp.length === 2) {
                id_tablet = id_temp;
            }

            //---Imposta il nome file del SDS---
            var nome_file = "/SDS/PDA" + id_tablet + "_" + data_tablet + BIS + ".XM_";

            //---Promise, che in caso di storicizzazione NON automatica, esporta il planing del tavolo---
            //FinchÃƒÆ’Ã‚Â¨ il tavolo non viene esportato e riletto con l'hash di origine, la funzione resta bloccata
            //In entrambi i casi comunque esporta l'xml del tavolo (vedere salva_file)
            var prom_storicizzazione = new Promise(function (resolve, reject) {
                //Il takeaway puÃ² stampare anche senza aver fatto la comanda
                if (incasso_annullato === true && comanda.tavolotxt !== '*TAKEAWAY*') {
                    alert("PRIMA SI LANCIA TUTTO IN COMANDA E POI SI INCASSA.");

                    $('.tasto_bestellung').css('pointer-events', '');
                    $('.tasto_konto').css('pointer-events', '');

                    $('.tasto_quittung').css('pointer-events', '');
                    $('.tasto_rechnung').css('pointer-events', '');

                    $('.tasto_bargeld').css('pointer-events', '');
                } else {
                    //SEMPRE
                    resolve(true);
                }

            });

            //Promessa per risolvere il bug del btn tavoli se non la usassi
            //Serve ad esportare il file del SDS dopo l'esportazione sicura del tavolo
            prom_storicizzazione.then(function () {

                oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].nome_file_sds = "_" + comanda.folder_number + "/" + nome_file;
                oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].contenuto_file_sds = output;
                oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].tipo_file_sds = "SDS";
                comanda.xml.salva_file_xml("_" + comanda.folder_number + "/" + nome_file, output, "SDS", function () {

                    if (bis_presente === true) {
                        nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + ".8";

                        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].nome_file_planing = "_" + comanda.folder_number + "/" + nome_file;
                        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].contenuto_file_planing = "";
                        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].tipo_file_planing = "PLANING";
                        comanda.xml.salva_file_xml("_" + comanda.folder_number + "/" + nome_file, "", "PLANING", function () {
                            $('#conto_separato_grande').modal('hide');
                            $('.tasto_bestellung').css('pointer-events', '');
                            $('.tasto_konto').css('pointer-events', '');

                            $('.tasto_quittung').css('pointer-events', '');
                            $('.tasto_rechnung').css('pointer-events', '');

                            $('.tasto_bargeld').css('pointer-events', '');
                            btn_tavoli('CONTO');
                        });

                    } else if (spref === '') {
                        if (comanda.discoteca === true) {
                            var nome_file = "";
                            if ((testa_totale[""] > 0 || isNaN(testa_totale[""])) && testa_totale[""] <= comanda.SCAGL1)
                            {
                                nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + '.1';
                            }

                            if (testa_totale[""] > comanda.SCAGL1 && testa_totale[""] <= comanda.SCAGL2)
                            {
                                nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + '.2';
                            }

                            if (testa_totale[""] > comanda.SCAGL2) {
                                nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + '.3';
                            }
                        } else
                        {
                            nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + '.1';
                        }



                        console.log("esporta_xml_tavolo", nome_file);

                        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].nome_file_planing = "_" + comanda.folder_number + "/" + nome_file;
                        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].contenuto_file_planing = "";
                        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].tipo_file_planing = "PLANING";
                        comanda.xml.salva_file_xml("_" + comanda.folder_number + "/" + nome_file, "", "PLANING", function () {
                            /*setTimeout(function () {
                             lettura_tavolo_xml(variabile_tavolo, function () {
                             comanda.funzionidb.conto_attivo(function () {
                             $('.tasto_bestellung').css('pointer-events', '');
                             $('.tasto_konto').css('pointer-events', '');
                             
                             $('.tasto_quittung').css('pointer-events', '');
                             $('.tasto_rechnung').css('pointer-events', '');
                             
                             $('.tasto_bargeld').css('pointer-events', '');
                             });
                             });
                             }, 2000);*/
                            comanda.reimposta_variabili_conto_separato();
                            comanda.popup_modifica_articolo = false;
                            $('.tasto_bestellung').css('pointer-events', '');
                            $('.tasto_konto').css('pointer-events', '');

                            $('.tasto_quittung').css('pointer-events', '');
                            $('.tasto_rechnung').css('pointer-events', '');

                            $('.tasto_bargeld').css('pointer-events', '');
                            $("#conto_separato_grande").modal('hide');
                            btn_tavoli('CONTO');
                        });
                    } else
                    {
                        nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3);
                        //NEL QUITTUNG E RECHNUNG VA ELIMINATO IL COLORE A MANO
                        comanda.xml.elimina_file_xml("_" + comanda.folder_number + "/" + nome_file, true, function () {
                            $('#conto_separato_grande').modal('hide');
                            $('.tasto_bestellung').css('pointer-events', '');
                            $('.tasto_konto').css('pointer-events', '');

                            $('.tasto_quittung').css('pointer-events', '');
                            $('.tasto_rechnung').css('pointer-events', '');

                            $('.tasto_bargeld').css('pointer-events', '');

                            if (comanda.tavolotxt !== "*TAKEAWAY*") {
                                btn_tavoli('CONTO');
                            } else {
                                lettura_tavolo_xml(comanda.tavolo, function () {}, true);
                            }
                        });

                    }
                });
            });
        });


    };

    if (tipo === "SCONTRINO FISCALE" && comanda.avviso_quittung === "S") {

        bootbox.confirm("Sei sicuro di voler incassare con QUITTUNG ?", function (risposta) {
            if (risposta === true) {
                procedi();
            }
        });
    } else if (tipo === "FATTURA" && comanda.avviso_rechnung === "S") {

        bootbox.confirm("Sei sicuro di voler incassare con RECHNUNG ?", function (risposta) {
            if (risposta === true) {
                procedi();
            }
        });
    } else
    {
        procedi();
    }

}

function storicizza_xml(numero_conto) {
    var bis_presente = false;
    var annulla_comanda2 = false;
    var procedi = function () {
        $('.tasto_bestellung').css('pointer-events', 'none');
        $('.tasto_konto').css('pointer-events', 'none');

        $('.tasto_quittung').css('pointer-events', 'none');
        $('.tasto_rechnung').css('pointer-events', 'none');

        $('.tasto_bargeld').css('pointer-events', 'none');

        if (comanda.conto === '#prodotti_conto_separato_grande' || comanda.conto === '#prodotti_conto_separato_2_grande') {
            $('.loader2').show();
            $('#conto_separato_grande').css('pointer-events', 'none');

            setTimeout(function () {
                $('#conto_separato_grande').css('pointer-events', '');
                $('.loader2').hide();
            }, 4500);
        }

        var variabile_tavolo = comanda.tavolo;
        var BIS = '';

        if (comanda.split_abilitato === true) {
            if (lettera_conto_split().length >= 1)
            {
                BIS = "-" + lettera_conto_split();
            }
        }
        var numero_coperti = $("#numero_coperti").val();

        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)] = new Object();


        var incasso_annullato = false;



        var testo_query = "select NSEGN from comanda where ntav_comanda = '" + variabile_tavolo + "' and posizione = 'CONTO' and stato_record = 'ATTIVO' ORDER BY cast(NSEGN as int) DESC LIMIT 1;";
        var rnsegn = alasql(testo_query);


        var nsegn_conto_2 = "1";

        if (rnsegn !== undefined && rnsegn[0] !== undefined && rnsegn[0].NSEGN !== null && !isNaN(rnsegn[0].NSEGN))
        {
            if (rnsegn[0].NSEGN === "") {
                rnsegn[0].NSEGN = "0";
            }
            nsegn_conto_2 = (parseInt(rnsegn[0].NSEGN) + 1).toString();
        }


        //---Creo la data di adesso dall'anno ai minuti---
        var d = new Date();
        var Y = d.getFullYear().toString();
        var D = addZero(d.getDate(), 2);
        var M = addZero((d.getMonth() + 1), 2);
        var h = addZero(d.getHours(), 2);
        var m = addZero(d.getMinutes(), 2);
        var s = addZero(d.getSeconds(), 2);
        var ms = addZero(d.getMilliseconds(), 3);
        var data = D + '/' + M + '/' + Y;
        var ora = h + ':' + m + ':' + s;

        //---Query per prelevare il conto dal db locale---
        var variabile = '';
        var testo_query = " select NCARD3,AGG_GIAC,LIB4,LIB2,BIS,dest_stampa_2,ora_,operatore,sconto_perc,perc_iva,NSEGN,QTAP,numero_conto,ordinamento,ultima_portata,peso_ums,cod_articolo,dest_stampa,contiene_variante,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita,stampata_sn from comanda where desc_art NOT LIKE 'RECORD TESTA%' and  contiene_variante='1' and ntav_comanda='" + variabile_tavolo + "' and posizione='CONTO' and stato_record='ATTIVO'  \n\
                            union all\n\
                                select NCARD3,AGG_GIAC,LIB4,LIB2,BIS,dest_stampa_2,ora_,operatore,sconto_perc,perc_iva,NSEGN,QTAP,numero_conto,ordinamento,ultima_portata,peso_ums,cod_articolo,dest_stampa,contiene_variante,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita,stampata_sn from comanda where  desc_art NOT LIKE 'RECORD TESTA%' and (contiene_variante !='1' or contiene_variante is null) and length(nodo)=3 and ntav_comanda='" + variabile_tavolo + "' and posizione='CONTO' and stato_record='ATTIVO'  \n\
                            union all\n\
                                select NCARD3,AGG_GIAC,LIB4,LIB2,BIS,dest_stampa_2,ora_,operatore,sconto_perc,perc_iva,NSEGN,QTAP,numero_conto,ordinamento,ultima_portata,peso_ums,cod_articolo,dest_stampa,contiene_variante,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita,stampata_sn from comanda where desc_art NOT LIKE 'RECORD TESTA%' and  length(nodo)=7 and ntav_comanda='" + variabile_tavolo + "'  and posizione='CONTO' and stato_record='ATTIVO';";

        //console.log(testo_query);
        var result = alasql(testo_query);
        //---Risultato della query del conto---
        result = alasql("select * from ? order by dest_stampa,portata,nodo,prog_inser", [result]);


        //---Se non c'ÃƒÆ’Ã‚Â¨ niente dentro---
        if (result.length === 0)
        {
            //---Se c'ÃƒÆ’Ã‚Â¨ il CallBack lo Esco---
            if (typeof (cB) === "function") {
                console.log(" esporta_xml_tavolo tavolo inesistente");

                cB(true);

            }

            //Con il nuovo settaggio non va piÃƒÆ’Ã‚Â¹ esportato l'xml tavolo
            //esporta_xml_tavolo("SENZA_AGGIUNTE", function () {
            if (variabile_tavolo !== undefined && typeof (variabile_tavolo) === "string") {
                comanda.xml.elimina_tavolo_occupato("_" + comanda.folder_number + "/COND/Tav" + variabile_tavolo.replace(/^0+/, '') + ".CO1", false, function () {

                    btn_tavoli("CONTO");
                });
            }
//});
            //});

            //---Fermo la funzione tornando un valore falso---
            return false;
        }



        //console.log("result", result);

        //INTESTAZIONE
        var output = '<?xml version="1.0" standalone="yes"?>\r\n\
                        <dataroot xmlns="http://www.w3.org/2001/TAVOLI.xsd">\r\n';

        //VALORI DEFAULT
        var storicizzazione = 'N';
        var spref = '';
        var nsegn = '';
        var nsegn_storicizzazione = '';
        var qtap = '';


        if (comanda.conto === '#prodotti_conto_separato_2_grande') {
            nsegn_storicizzazione = nsegn_conto_2;

            var prezzo_1 = $('#totale_scontrino_separato_1').html();
            if (parseFloat(prezzo_1) === 0) {
                spref = 'S';
            }

        } else if (comanda.conto === '#prodotti_conto_separato_grande') {
            nsegn_storicizzazione = nsegn_conto_2;

            var prezzo_2 = $('#totale_scontrino_separato_2').html();
            if (parseFloat(prezzo_2) === 0) {
                spref = 'S';
            }
        } else if (nsegn_conto_2 !== '1' && !isNaN(nsegn_conto_2))
        {
            nsegn_storicizzazione = nsegn_conto_2;
            spref = 'S';
        } else
        {
            nsegn_storicizzazione = '';
            spref = 'S';
        }

        if ($(comanda.conto + " tr").length === 0 && comanda.tavolotxt !== "VERBRAUCHT") {
            incasso_annullato = true;

        }


        storicizzazione = spref;

        if (comanda.SETB36 === 'S' && comanda.PRF2 === 'SI') {
            storicizzazione = '#2';
        }


        //---Intestazione XML---
        var i = 0;
        var cod_articolo_princ;
        var ts_princ;
        var prog_principale;
        var descrizione_articolo;
        var ordinamento = '';
        var incasso_parziale = 0;
        var importo_gutshein_corpo = "";
        var testa_totale = new Object();
        var qta_princ = 1;


        //------
        result.forEach(function (obj) {

            if (obj.prezzo_un.trim() === "") {
                obj.prezzo_un = "0,00";
            }

            //PRIMA IPOTESI
            //Sei su #prodotti_conto_separato_2_grande
            //Quelli con numero conto 2 progrediscono di progressivo, cosÃƒÆ’Ã‚Â¬ come il numero di storicizzazione
            //La S c'ÃƒÆ’Ã‚Â¨ solo se il prezzo dell'altro conto non supera lo 0

            //SECONDA IPOTESI
            //Sei su #prodotti_conto_separato_grande
            //Quelli con numero conto '' o 1 progrediscono di progressivo, cosÃƒÆ’Ã‚Â¬ come il numero di storicizzazione
            //La S c'ÃƒÆ’Ã‚Â¨ solo se il prezzo dell'altro conto non supera lo 0

            //TERZA IPOTESI (DEFAULT)
            //Sei sullo scontrino normale
            //Il numero del conto non esiste perchÃƒÆ’Ã‚Â¨ sei sullo scontrino normale quindi solo qs
            //nsegn ÃƒÆ’Ã‚Â¨ sempre ''
            //La S c'ÃƒÆ’Ã‚Â¨ sempre


            nsegn = obj.NSEGN;

            if (obj.QTAP === '') {
                obj.QTAP = '0';
            }

            qtap = obj.QTAP;

            if (comanda.conto === '#prodotti_conto_separato_2_grande') {

                if (obj.numero_conto === '2' && obj.QTAP === '0') {
                    qtap = obj.quantita;
                    nsegn = nsegn_conto_2;
                    importo_gutshein_corpo = $('#gutshein__importo_gutshein').val().replace('.', ',');

                    incasso_parziale += parseFloat(obj.prezzo_un.replace(',', '.'));

                }

            } else if (comanda.conto === '#prodotti_conto_separato_grande') {

                if ((obj.numero_conto === '' || obj.numero_conto === '1') && obj.QTAP === '0') {
                    qtap = obj.quantita;
                    nsegn = nsegn_conto_2;
                    importo_gutshein_corpo = $('#gutshein__importo_gutshein').val().replace('.', ',');

                    incasso_parziale += parseFloat(obj.prezzo_un.replace(',', '.'));


                }
            } else if (nsegn_conto_2 !== '1' && obj.QTAP === '0')
            {
                nsegn = nsegn_conto_2;
                importo_gutshein_corpo = $('#gutshein__importo_gutshein').val().replace('.', ',');

                qtap = obj.quantita;
                incasso_parziale += parseFloat(obj.prezzo_un.replace(',', '.'));

            }


            //---Imposto le variabili da inserire nell'xml del SDS---
            //PRIMA ERA +1 MA NON SE NE CAPISCE IL MOTIVO
            obj.prog_inser = parseInt(obj.prog_inser);
            obj.dest_stampa !== undefined ? ts_princ = obj.dest_stampa : ts_princ = '1';
            obj.peso_ums !== undefined ? obj.peso_ums : obj.peso_ums = '';
            obj.ordinamento !== undefined ? ordinamento = obj.ordinamento : ordinamento = '01';
            prog_principale = aggZero(obj.prog_inser, 3);
            descrizione_articolo = obj.desc_art;
            console.log(obj.dest_stampa);
            console.log(obj.dest_stampa !== undefined);
            console.log(ts_princ);
            var contiene_variante = '';
            obj.contiene_variante === '1' ? contiene_variante = 'SI' : contiene_variante = '';

            //---Imposto le variabili in caso di VARIANTI---
            if (obj.desc_art[0] === "+" || obj.desc_art[0] === "-" || obj.desc_art[0] === "x" || obj.desc_art[0] === "*" || obj.desc_art[0] === "=")
            {
                prog_principale = aggZero(prog_principale.substring(0, 3), 3) + ' ' + aggZero(obj.prog_inser, 3) + ' ';
                descrizione_articolo = obj.desc_art.substring(1);
                variabile = obj.desc_art[0];
            } else if (obj.desc_art[0] === "(") {
                prog_principale = aggZero(prog_principale.substring(0, 3), 3) + ' ' + aggZero(obj.prog_inser, 3) + ' ';
                variabile = "*";
            } else
            {
                cod_articolo_princ = obj.cod_articolo;
                variabile = '';
            }

            //---Imposto il valore di default di STAMPATA_SN---
            var stampata_sn = 'N';
            if (obj.stampata_sn === 'S' || comanda.tavolotxt === '*TAKEAWAY*')
            {

                stampata_sn = 'S';
                var ora = h + ':' + m + ':' + s;
            } else {
                var ora = h + ':' + m + ':' + s;
            }


            //---Imposto il valore di default per la storicizzazione automatica---
            //e per la comanda
            var ts = '';
            ts = ts_princ;

            if (stampata_sn === 'N' && comanda.tavolotxt !== "VERBRAUCHT")
            {
                incasso_annullato = true;

            }

            var operatore = obj.operatore;
            if (operatore.length === 0) {
                operatore = comanda.operatore;
            }

            if (testa_totale[obj.BIS] === undefined) {
                testa_totale[obj.BIS] = 0;
            }
            testa_totale[obj.BIS] += parseFloat(conv_prz_fc_ibr(obj.prezzo_un)) * parseInt(obj.quantita);

            var ncard3 = '';
            if (obj.NCARD3) {
                ncard3 = obj.NCARD3;
            }

            let tab_iva = alasql("select aliquota from tabella_iva where lingua='" + comanda.lingua_stampa + "';");
            let percentuali_iva = tab_iva.map(d => d.aliquota);

            if (percentuali_iva.indexOf(obj.perc_iva.trim()) === -1) {
                /*bootbox.alert("Attenzione: l'IVA dell'articolo " + descrizione_articolo + " non &egrave; presente nella tabella IVA. Modificalo e ri-lancia l'operazione");
                 annulla_comanda2 = true;
                 return false;*/

                obj.perc_iva = comanda.percentuale_iva_default;
            } else if (isNaN(parseFloat(conv_prz_fc_ibr(obj.prezzo_un)).toFixed(2))) {
                bootbox.alert("Attenzione: il prezzo dell'articolo " + descrizione_articolo + " non &egrave; valido. Modificalo e ri-lancia l'operazione");
                annulla_comanda2 = true;
                return false;
            } else if (isNaN(parseInt(obj.quantita)) || parseInt(obj.quantita) < 1) {
                bootbox.alert("Attenzione: la quantit&agrave; dell'articolo " + descrizione_articolo + " non &egrave; valida. Modificala e ri-lancia l'operazione");
                annulla_comanda2 = true;
                return false;
            }

            //---XML SERVER DI STAMPA---
            output += '<TAVOLI>\r\n';
            output += '<TC>C</TC>\r\n';
            output += '<DATA>' + data_inizio_servizio + '</DATA>\r\n';
            output += '<TAVOLO>' + aggZero(variabile_tavolo, 3) + '</TAVOLO>\r\n';
            output += '<BIS>' + obj.BIS + '</BIS>\r\n';
            output += '<PRG>' + aggZero(obj.prog_inser, 3) + '</PRG>\r\n';
            output += '<ART>' + cod_articolo_princ + '</ART>\r\n';
            output += '<ART2>' + obj.cod_articolo + '</ART2>\r\n';
            output += '<VAR>' + variabile + '</VAR>\r\n';
            output += '<DES>' + descrizione_articolo + '</DES>\r\n';
            output += '<PRZ>' + parseFloat(conv_prz_fc_ibr(obj.prezzo_un)).toFixed(2).replace('.', ',') + '</PRZ>\r\n';
            output += '<COSTO>0</COSTO>\r\n';
            output += '<QTA>' + obj.quantita + '</QTA>\r\n';
            output += '<SN>' + stampata_sn + '</SN>\r\n';
            output += '<CAT>' + obj.categoria + '</CAT>\r\n';
            output += '<TOTALE />\r\n';
            output += '<SCONTO />\r\n';
            output += '<AUTORIZ>' + obj.sconto_perc + '</AUTORIZ>\r\n';
            output += '<INC>0</INC>\r\n';
            output += '<CARTACRED>' + importo_gutshein_corpo + '</CARTACRED>\r\n';
            output += '<BANCOMAT/>\r\n';
            output += '<ASSEGNI>' + obj.perc_iva + '</ASSEGNI>\r\n';
            output += '<NCARD1>' + contiene_variante + '</NCARD1>\r\n';
            output += '<NCARD2/>\r\n';
            output += '<NCARD3>' + ncard3 + '</NCARD3>\r\n';
            output += '<NCARD4/>\r\n';
            output += '<NCARD5>' + ordinamento + '</NCARD5>\r\n'; //ORDINAMENTO
            output += '<NSEGN>' + nsegn + '</NSEGN>\r\n';
            output += '<NEXIT/>\r\n';
            output += '<TS></TS>\r\n'; //' + ts_princ + '
            output += '<NOME>' + operatore + '</NOME>\r\n';
            output += '<ORA>' + ora + '</ORA>\r\n';
            output += '<LIB1>1</LIB1>\r\n';
            output += '<LIB2>' + obj.LIB2 + '</LIB2>\r\n';
            output += '<LIB3/>\r\n';
            output += '<LIB4>' + obj.LIB4 + '</LIB4>\r\n';
            output += '<LIB5>' + obj.dest_stampa_2 + '</LIB5>\r\n';
            output += '<CLI/>\r\n';
            output += '<QTAP>' + qtap + '</QTAP>\r\n';
            output += '<NODO>' + obj.nodo + '</NODO>\r\n';
            output += '<PORTATA>' + obj.portata + '</PORTATA>\r\n';
            output += '<NUMP>0</NUMP>\r\n';
            output += '<TAVOLOTXT>' + comanda.tavolotxt + '</TAVOLOTXT>\r\n';
            output += '<ULTPORT>' + obj.ultima_portata + '</ULTPORT>\r\n';
            output += '<AGG_GIAC>' + obj.AGG_GIAC + '</AGG_GIAC>\r\n';
            output += '</TAVOLI>\r\n';
            i++;
        });

        if (annulla_comanda2 === true) {
            return false;
        }
        //---Fine delle righe corpo---

        var b1 = alasql('select BIS from comanda where stato_record="ATTIVO" and posizione="CONTO" and ntav_comanda="' + variabile_tavolo + '" group by id,BIS order by prog_inser asc;');


        b1.forEach(function (e) {
            var bi2 = alasql('select desc_art as desc_sconto,prezzo_un as sconto from comanda where stato_record="ATTIVO" and ntav_comanda="' + variabile_tavolo + '" and BIS="' + e.BIS + '" and posizione="SCONTO" limit 1;');
            var bi3 = alasql('select nump_xml as numero_coperti from comanda  where stato_record="ATTIVO" and ntav_comanda="' + variabile_tavolo + '" and BIS="' + e.BIS + '" order by nump_xml desc limit 1;');
            var bi4 = alasql('select desc_art,LIB2,dest_stampa as storicizzazione,prezzo_un,incassato,costo,operatore,contiene_variante as NCARD1,NCARD2,NCARD3,NCARD4,NCARD5,NSEGN,NEXIT,carta_credito,bancomat from comanda  where ntav_comanda="' + variabile_tavolo + '"  and  desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" and BIS="' + e.BIS + '" limit 1;');
            var b2 = bi2[0];
            var b3 = bi3[0];
            var b4 = bi4[0];

            var rec = new Object();
            // SEZIONE BIS
            rec.BIS = e.BIS;
            if (rec.BIS !== "") {
                bis_presente = true;
            }
            // SEZIONE SCONTISTICA
            rec.desc_sconto = "";
            rec.sconto = "";
            if (b2 !== undefined) {
                rec.desc_sconto = b2.desc_sconto;
                rec.sconto = b2.sconto;
            }
            // SEZIONE COPERTI
            var numero_coperti = $("#numero_coperti").val();
            rec.numero_coperti = "";

            if (b3 !== undefined) {
                rec.numero_coperti = b3.numero_coperti;

            }
            console.log("BIS COPERTI 0", "LCS", lettera_conto_split(), "BIS", rec.BIS, "ncop", numero_coperti, "ncop2", rec.numero_coperti, "B3", b3);

            // SEZIONE PAGAMENTI
            rec.desc_art = "";
            rec.storicizzazione = "";
            rec.incassato = "";
            rec.costo = "";
            rec.operatore = "";
            rec.NCARD1 = "";
            rec.NCARD2 = "";
            rec.NCARD3 = "";
            rec.NCARD4 = "";
            rec.NCARD5 = "";
            rec.NSEGN = "";
            rec.NEXIT = "";
            rec.carta_credito = "";
            rec.bancomat = "";
            rec.LIB2 = "";
            if (b4 !== undefined) {
                rec.desc_art = b4.desc_art;
                rec.incassato = b4.incassato;
                rec.costo = b4.costo;
                rec.operatore = b4.operatore;
                rec.NCARD1 = b4.NCARD1;
                rec.NCARD2 = b4.NCARD2;
                rec.NCARD3 = b4.NCARD3;
                rec.NCARD4 = b4.NCARD4;
                rec.NCARD5 = b4.NCARD5;
                rec.NSEGN = b4.NSEGN;
                rec.NEXIT = b4.NEXIT;
                rec.carta_credito = b4.carta_credito;
                rec.bancomat = b4.bancomat;
                rec.storicizzazione = b4.storicizzazione;
                rec.LIB2 = b4.LIB2;
            }

            //---Variabili testa---
            var incassato = 0;
            var costo = 0;
            var costo_parziale = 0;
            var autoriz = 0;


            var sconto = '0.00';


            if (rec !== undefined && typeof rec.sconto === 'string' && rec !== "") {
                if (rec.sconto !== undefined && rec.sconto !== "" && rec.sconto.substr(-1) === "%") {

                    //Bisogna fare il calcolo
                    sconto = testa_totale[rec.BIS] / 100 * rec.sconto.substr(1).slice(0, -1);

                    sconto = sconto.toString();
                    console.log("CALCOLO CONTO SCONTO", sconto);
                } else if (rec.sconto !== undefined && rec.sconto !== "") {
                    sconto = rec.sconto.substr(1);
                }
                autoriz = rec.desc_sconto;


            }

            //PER LA SAGRA NON SERVE PERCHE' INCASSA IN AUTOMATICO
            //E POI SE SI RIPRENDE UNA COMANDA DOPO AVER LASCIATO LA L'INCASSO FA CASINO
            //DA VERIFICARE



            incassato = testa_totale[rec.BIS] - parseFloat(sconto);
            incassato = incassato.toString();



            if (rec.incassato === null) {
                rec.incassato = '0,00';
            }

            if (rec.costo === null) {
                rec.costo = '0,00';
            }

            var condizione_succ = false;
            if ($('#metodo_pagamento_gutshein').hasClass('in') === true) {
                costo_parziale = parseFloat($('#gutshein__importo_gutshein:visible').val().replace(',', '.'));
                incasso_parziale = $('#gutshein_importo_contanti:visible').val().replace(',', '.');
                $('#metodo_pagamento_gutshein').modal('hide');
                condizione_succ = true;
            }



            if (spref !== 'S' || condizione_succ === true) {
                incassato = (parseFloat(incasso_parziale) + parseFloat(conv_prz_fc_ibr(rec.incassato))).toString();
            } else if (spref === "S") {
                incassato = (parseFloat(incassato) - parseFloat(costo_parziale) - parseFloat(conv_prz_fc_ibr(rec.costo)) - parseFloat(carte_parziale) - parseFloat(conv_prz_fc_ibr(rec.carta_credito))).toFixed(2);
            }
            //Deve stare fuori perche anche se incassassi senza Gutshein potrei averne dalla volta precedente
            costo = parseFloat((costo_parziale + parseFloat(conv_prz_fc_ibr(rec.costo))).toString()).toFixed(2).replace('.', ',');
            incassato = parseFloat(conv_prz_fc_ibr(incassato)).toFixed(2).replace('.', ',');
            sconto = parseFloat(conv_prz_fc_ibr(sconto)).toFixed(2).replace('.', ',');




            if (comanda.tavolotxt === 'VERBRAUCHT') {
                autoriz = "100% VERBRAUCHT";
                sconto = testa_totale[rec.BIS];
            }


            if (rec.BIS === lettera_conto_split()) {

                rec.incassato = incassato;



                rec.storicizzazione = storicizzazione;

                if (numero_coperti !== undefined && !isNaN(numero_coperti) && numero_coperti !== "")
                {
                    testa_totale[rec.BIS] += parseInt(numero_coperti) * parseFloat(comanda.valore_coperto);


                    if (parseInt(numero_coperti) - comanda.ultimi_coperti_importati !== 0) {
                        annulla_comanda = false;
                    }
                }
            } else
            {
                numero_coperti = rec.numero_coperti;
                costo = rec.costo;
            }

            testa_totale[rec.BIS] = parseFloat(testa_totale[rec.BIS]).toFixed(2).replace(".", ",");

            var DES = "RECORD TESTA";
            if (b4 && b4.desc_art) {
                DES = b4.desc_art;
            } else if (comanda.set_tid === true) {
                DES += " TID=" + comanda.TID;

            }

            let totale_testa = "0,00";
            if (!isNaN(parseFloat(testa_totale[rec.BIS]))) {
                totale_testa = testa_totale[rec.BIS];
            } else {
                bootbox.alert("Attenzione: il totale della testa ha un valore errato.");
                annulla_comanda2 = true;
            }

            let totale_incassato = "";
            if (!isNaN(parseFloat(rec.incassato)) || rec.incassato.trim() === "") {
                totale_incassato = rec.incassato;
            } else {
                bootbox.alert("Attenzione: l incasso ha un valore errato.");
                annulla_comanda2 = true;
            }

            let totale_carta_credito = "";
            if (!isNaN(parseFloat(rec.carta_credito)) || rec.carta_credito.trim() === "") {
                totale_carta_credito = rec.carta_credito;
            } else {
                bootbox.alert("Attenzione: il valore della carta di credito ha un valore errato.");
                annulla_comanda2 = true;
            }

            let totale_bancomat = "";
            if (!isNaN(parseFloat(rec.bancomat)) || rec.bancomat.trim() === "") {
                totale_bancomat = rec.bancomat;
            } else {
                bootbox.alert("Attenzione: il  bancomat ha un valore errato.");
                annulla_comanda2 = true;
            }

            //---XML TESTA---
            output += '<TAVOLI>\r\n\
                <TC>T</TC>\r\n\
                <DATA>' + data_inizio_servizio + '</DATA>\r\n\
                <TAVOLO>' + aggZero(variabile_tavolo, 3) + '</TAVOLO>\r\n\
                <BIS>' + rec.BIS + '</BIS>\r\n\
                <PRG>000</PRG>\r\n\
                <ART>01</ART>\r\n\
                <ART2/>\r\n\
                <VAR/>\r\n\
                <DES>' + DES + '</DES>\r\n\
                <PRZ/>\r\n\
                <COSTO>' + costo + '</COSTO>\r\n\
                <QTA/>\r\n\
                <SN>N</SN>\r\n\
                <CAT/>\r\n\
               <TOTALE>' + totale_testa + '</TOTALE>\r\n\
                <SCONTO>' + sconto + '</SCONTO>\r\n\
                <AUTORIZ>' + parseFloat(autoriz).toFixed(2).replace('.', ',') + '</AUTORIZ>\r\n\
                <INC>' + totale_incassato + '</INC>\r\n\
                <CARTACRED>' + totale_carta_credito + '</CARTACRED>\r\n\
                <BANCOMAT>' + totale_bancomat + '</BANCOMAT>\r\n\
                <ASSEGNI>0,00</ASSEGNI>\r\n\
                <NCARD1>' + rec.NCARD1 + '</NCARD1>\r\n\
                <NCARD2>' + rec.NCARD2 + '</NCARD2>\r\n\
                <NCARD3>' + rec.NCARD3 + '</NCARD3>\r\n\
                <NCARD4>' + rec.NCARD4 + '</NCARD4>\r\n\
                <NCARD5>' + rec.NCARD5 + '</NCARD5>\r\n\
                <NSEGN>' + rec.NSEGN + '</NSEGN>\r\n\
                <NEXIT>' + rec.NEXIT + '</NEXIT>\r\n\
                <TS>' + rec.storicizzazione + '</TS>\r\n\
                <NOME>' + comanda.operatore + '</NOME>\r\n\
                <ORA>' + ora + '</ORA>\r\n\
                <LIB1>1</LIB1>\r\n\
                <LIB2>' + rec.LIB2 + '</LIB2>\r\n\
                <LIB3/>\r\n\
                <LIB4/>\r\n\
                <LIB5>S</LIB5>\r\n\
                <CLI/>\r\n\
                <QTAP>0</QTAP>\r\n\
                <NODO></NODO>\r\n\
                <PORTATA/>\r\n\
                <NUMP>' + numero_coperti + '</NUMP>\r\n\
                <TAVOLOTXT>' + comanda.tavolotxt + '</TAVOLOTXT>\r\n\
                <ULTPORT>N</ULTPORT>\r\n\
                <AGG_GIAC>N</AGG_GIAC>\r\n\
                </TAVOLI>\r\n';

        });

        if (annulla_comanda2 === true) {
            return false;
        }

        output += '</dataroot>';


        //---Toglie i null e undefined dall'output---
        output = output.replace(/>null</gi, '><');
        output = output.replace(/>undefined</gi, '><');

        console.log("OUTPUT XML CONTO" + comanda.tavolotxt, output);

        //---Crea una data---
        var d = new Date();
        var Y = d.getFullYear();
        var M = addZero(d.getDate(), 2);
        var D = addZero((d.getMonth() + 1), 2);
        var h = addZero(d.getHours(), 2);
        var m = addZero(d.getMinutes(), 2);
        var s = addZero(d.getSeconds(), 2);
        var ms = addZero(d.getMilliseconds(), 3);
        var data_tablet = h + '' + m + '' + s;

        //---Legge l'id del tablet dall'url---
        var id_tablet = "29";
        var id_temp = leggi_get_url('id');

        if (id_temp !== undefined && id_temp !== null && id_temp !== 'null' && id_temp.length === 2) {
            id_tablet = id_temp;
        }

        //---Imposta il nome file del SDS---
        var nome_file = "/SDS/PDA" + id_tablet + "_" + data_tablet + BIS + ".XM_";

        //---Promise, che in caso di storicizzazione NON automatica, esporta il planing del tavolo---
        //FinchÃƒÆ’Ã‚Â¨ il tavolo non viene esportato e riletto con l'hash di origine, la funzione resta bloccata
        //In entrambi i casi comunque esporta l'xml del tavolo (vedere salva_file)
        var prom_storicizzazione = new Promise(function (resolve, reject) {
            if (incasso_annullato === true && comanda.tavolotxt !== '*TAKEAWAY*') {
                alert("PRIMA SI LANCIA TUTTO IN COMANDA E POI SI INCASSA.");
                $('.tasto_bestellung').css('pointer-events', '');
                $('.tasto_konto').css('pointer-events', '');

                $('.tasto_quittung').css('pointer-events', '');
                $('.tasto_rechnung').css('pointer-events', '');

                $('.tasto_bargeld').css('pointer-events', '');
            } else {
                //SEMPRE
                resolve(true);
            }
        });

        //Promessa per risolvere il bug del btn tavoli se non la usassi
        //Serve ad esportare il file del SDS dopo l'esportazione sicura del tavolo
        prom_storicizzazione.then(function () {

            oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].nome_file_sds = "_" + comanda.folder_number + "/" + nome_file;
            oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].contenuto_file_sds = output;
            oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].tipo_file_sds = "SDS";
            comanda.xml.salva_file_xml("_" + comanda.folder_number + "/" + nome_file, output, "SDS", function () {


                if (numero_conto === 2) {
                    var prezzo_1 = $('#totale_scontrino_separato_1').html();
                    if (parseFloat(prezzo_1) > 0) {
                        var nome_file = "";

                        if (comanda.discoteca === true) {
                            if ((testa_totale[""] > 0 || isNaN(testa_totale[""])) && testa_totale[""] <= comanda.SCAGL1)
                            {
                                nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + '.1';
                            }

                            if (testa_totale[""] > comanda.SCAGL1 && testa_totale[""] <= comanda.SCAGL2)
                            {
                                nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + '.2';
                            }

                            if (testa_totale[""] > comanda.SCAGL2) {
                                nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + '.3';
                            }
                        } else
                        {
                            nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + '.1';
                        }

                        if (bis_presente === true) {
                            nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + ".8";
                        }

                        console.log("esporta_xml_tavolo", nome_file);

                        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].nome_file_planing = "_" + comanda.folder_number + "/" + nome_file;
                        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].contenuto_file_planing = "";
                        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].tipo_file_planing = "PLANING";
                        comanda.xml.salva_file_xml("_" + comanda.folder_number + "/" + nome_file, "", "PLANING", function () {
                            /*setTimeout(function () {
                             lettura_tavolo_xml(variabile_tavolo, function () {
                             comanda.funzionidb.conto_attivo(function () {
                             $('.tasto_bestellung').css('pointer-events', '');
                             $('.tasto_konto').css('pointer-events', '');
                             
                             $('.tasto_quittung').css('pointer-events', '');
                             $('.tasto_rechnung').css('pointer-events', '');
                             
                             $('.tasto_bargeld').css('pointer-events', '');
                             });
                             });
                             }, 2000);*/
                            comanda.reimposta_variabili_conto_separato();
                            comanda.popup_modifica_articolo = false;
                            $('.tasto_bestellung').css('pointer-events', '');
                            $('.tasto_konto').css('pointer-events', '');

                            $('.tasto_quittung').css('pointer-events', '');
                            $('.tasto_rechnung').css('pointer-events', '');

                            $('.tasto_bargeld').css('pointer-events', '');
                            
                            $("#conto_separato_grande").modal('hide');
                            btn_tavoli('CONTO');
                        });
                    } else
                    {
                        nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3);
                        //NEL QUITTUNG E RECHNUNG VA ELIMINATO IL COLORE A MANO
                        comanda.xml.elimina_file_xml("_" + comanda.folder_number + "/" + nome_file, true, function () {
                            $('#conto_separato_grande').modal('hide');
                            $('.tasto_bestellung').css('pointer-events', '');
                            $('.tasto_konto').css('pointer-events', '');

                            $('.tasto_quittung').css('pointer-events', '');
                            $('.tasto_rechnung').css('pointer-events', '');

                            $('.tasto_bargeld').css('pointer-events', '');
                            btn_tavoli('CONTO');
                        });
                    }
                } else if (numero_conto === 1) {
                    var prezzo_2 = $('#totale_scontrino_separato_2').html();
                    if (parseFloat(prezzo_2) > 0) {
                        var nome_file = "";

                        if (comanda.discoteca === true) {
                            if ((testa_totale[""] > 0 || isNaN(testa_totale[""])) && testa_totale[""] <= comanda.SCAGL1)
                            {
                                nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + '.1';
                            }

                            if (testa_totale[""] > comanda.SCAGL1 && testa_totale[""] <= comanda.SCAGL2)
                            {
                                nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + '.2';
                            }

                            if (testa_totale[""] > comanda.SCAGL2) {
                                nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + '.3';
                            }
                        } else
                        {
                            nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + '.1';
                        }

                        if (bis_presente === true) {
                            nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + ".8";
                        }

                        console.log("esporta_xml_tavolo", nome_file);
                        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].nome_file_planing = "_" + comanda.folder_number + "/" + nome_file;
                        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].contenuto_file_planing = "";
                        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].tipo_file_planing = "PLANING";
                        comanda.xml.salva_file_xml("_" + comanda.folder_number + "/" + nome_file, "", "PLANING", function () {
                            /*setTimeout(function () {
                             lettura_tavolo_xml(variabile_tavolo, function () {
                             comanda.funzionidb.conto_attivo(function () {
                             $('.tasto_bestellung').css('pointer-events', '');
                             $('.tasto_konto').css('pointer-events', '');
                             
                             $('.tasto_quittung').css('pointer-events', '');
                             $('.tasto_rechnung').css('pointer-events', '');
                             
                             $('.tasto_bargeld').css('pointer-events', '');
                             });
                             });
                             }, 2000);*/
                            comanda.reimposta_variabili_conto_separato();
                            comanda.popup_modifica_articolo = false;
                            $('.tasto_bestellung').css('pointer-events', '');
                            $('.tasto_konto').css('pointer-events', '');

                            $('.tasto_quittung').css('pointer-events', '');
                            $('.tasto_rechnung').css('pointer-events', '');

                            $('.tasto_bargeld').css('pointer-events', '');
                            
                            $("#conto_separato_grande").modal('hide');
                            btn_tavoli('CONTO');
                        });
                    } else
                    {
                        nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3);
                        //NEL QUITTUNG E RECHNUNG VA ELIMINATO IL COLORE A MANO
                        comanda.xml.elimina_file_xml("_" + comanda.folder_number + "/" + nome_file, true, function () {
                            $('#conto_separato_grande').modal('hide');
                            $('.tasto_bestellung').css('pointer-events', '');
                            $('.tasto_konto').css('pointer-events', '');

                            $('.tasto_quittung').css('pointer-events', '');
                            $('.tasto_rechnung').css('pointer-events', '');

                            $('.tasto_bargeld').css('pointer-events', '');
                            btn_tavoli('CONTO');
                        });
                    }
                } else {
                    nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3);
                    //NEL QUITTUNG E RECHNUNG VA ELIMINATO IL COLORE A MANO
                    comanda.xml.elimina_file_xml("_" + comanda.folder_number + "/" + nome_file, true, function () {
                        $('#conto_separato_grande').modal('hide');
                        $('.tasto_bestellung').css('pointer-events', '');
                        $('.tasto_konto').css('pointer-events', '');

                        $('.tasto_quittung').css('pointer-events', '');
                        $('.tasto_rechnung').css('pointer-events', '');

                        $('.tasto_bargeld').css('pointer-events', '');

                        if (comanda.tavolotxt !== "*TAKEAWAY*") {
                            btn_tavoli('CONTO');
                        } else {
                            lettura_tavolo_xml(comanda.tavolo, function () {}, true);
                        }

                    });
                }



            });
        });

    };

    if (comanda.avviso_bargeld === "S") {
        bootbox.confirm("Sei sicuro di voler incassare con Bargeld?", function (risposta) {
            if (risposta === true) {
                procedi();
            }
        });
    } else {
        procedi();
    }


}


function storicizza() {

    var bis_presente = false;
    var annulla_comanda2 = false;

    var procedi = function () {
        $('.tasto_bestellung').css('pointer-events', 'none');
        $('.tasto_konto').css('pointer-events', 'none');

        $('.tasto_quittung').css('pointer-events', 'none');
        $('.tasto_rechnung').css('pointer-events', 'none');

        $('.tasto_bargeld').css('pointer-events', 'none');

        if (comanda.conto === '#prodotti_conto_separato_grande' || comanda.conto === '#prodotti_conto_separato_2_grande') {
            $('.loader2').show();
            $('#conto_separato_grande').css('pointer-events', 'none');

            setTimeout(function () {
                $('#conto_separato_grande').css('pointer-events', '');
                $('.loader2').hide();
            }, 4500);
        }



        var variabile_tavolo = comanda.tavolo;
        var testa_totale = new Object();
        var BIS = '';



        if (comanda.split_abilitato === true) {

            //0351 è brovedani
            if (comanda.numero_licenza_cliente === "0351") {
                //VEDO TUTTI I CORPI CON TAVOLO OCCUPATO
                var tavoli_occupati = alasql("select BIS,desc_art from comanda WHERE ntav_comanda='" + variabile_tavolo + "' and  desc_art = 'TAVOLO OCCUPATO' and stato_record='ATTIVO'");

                tavoli_occupati.forEach(v => {

//VEDO SE QUELLE TESTE HANNO ANCHE ALTRI ARTICOLI ATTIVI ALL'INTERNO
                    var articoli_aggiuntivi = alasql("select BIS,desc_art from comanda WHERE ntav_comanda='" + variabile_tavolo + "' and desc_art NOT LIKE 'RECORD TESTA%' and desc_art!='TAVOLO OCCUPATO' and  BIS = '" + v.BIS + "' and stato_record='ATTIVO'");

                    if (articoli_aggiuntivi.length === 0) {
                        //SE NON CI SONO ALTRI ARTICOLI ATTIVI OLTRE QUELLO METTO LA TESTA COME STORICIZZATA
                        //alasql("update comanda set dest_stampa = 'S' WHERE ntav_comanda='" + variabile_tavolo + "' and desc_art LIKE 'RECORD TESTA%' and  BIS = '_164807' and stato_record='ATTIVO'");

                        alasql("delete from comanda WHERE ntav_comanda='" + variabile_tavolo + "'  and  BIS = '" + v.BIS + "' and stato_record='ATTIVO'");

                    }

                });
            }

            if (lettera_conto_split().length >= 1)
            {
                BIS = "-" + lettera_conto_split();
            }
        }
        var numero_coperti = $("#numero_coperti").val();

        oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)] = new Object();


        //VALORI DEFAULT
        var storicizzazione = 'S';
        if (comanda.SETB36 === 'S' && comanda.PRF2 === 'SI') {
            storicizzazione = '#2';
        }


        var incasso_annullato = false;
        var d = new Date();
        var Y = d.getFullYear().toString();
        var D = addZero(d.getDate(), 2);
        var M = addZero((d.getMonth() + 1), 2);
        var h = addZero(d.getHours(), 2);
        var m = addZero(d.getMinutes(), 2);
        var s = addZero(d.getSeconds(), 2);
        var ms = addZero(d.getMilliseconds(), 3);
        var data = D + '/' + M + '/' + Y;
        var ora = h + ':' + m + ':' + s;
        var variabile = '';
        var testo_query = " select NCARD3,AGG_GIAC,LIB4,LIB2,BIS,dest_stampa_2,carta_credito,ora_,operatore,sconto_perc,perc_iva,ordinamento,ultima_portata,peso_ums,cod_articolo,dest_stampa,contiene_variante,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita,stampata_sn from comanda where desc_art NOT LIKE 'RECORD TESTA%' and  contiene_variante='1' and ntav_comanda='" + variabile_tavolo + "' and posizione='CONTO' and stato_record='ATTIVO'  \n\
                            union all\n\
                            select NCARD3,AGG_GIAC,LIB4,LIB2,BIS,dest_stampa_2,carta_credito,ora_,operatore,sconto_perc,perc_iva,ordinamento,ultima_portata,peso_ums,cod_articolo,dest_stampa,contiene_variante,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita,stampata_sn from comanda where desc_art NOT LIKE 'RECORD TESTA%' and  (contiene_variante !='1' or contiene_variante is null) and length(nodo)=3 and ntav_comanda='" + variabile_tavolo + "' and posizione='CONTO' and stato_record='ATTIVO' \n\
                            union all\n\
                            select NCARD3,AGG_GIAC,LIB4,LIB2,BIS,dest_stampa_2,carta_credito,ora_,operatore,sconto_perc,perc_iva,ordinamento,ultima_portata,peso_ums,cod_articolo,dest_stampa,contiene_variante,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita,stampata_sn from comanda where  desc_art NOT LIKE 'RECORD TESTA%' and length(nodo)=7 and ntav_comanda='" + variabile_tavolo + "'  and posizione='CONTO' and stato_record='ATTIVO';";

        //console.log(testo_query);
        var result = alasql(testo_query);

        result = alasql("select * from ? order by dest_stampa,portata,nodo,prog_inser", [result]);

        if (result.length === 0)
        {
            if (typeof (cB) === "function") {
                console.log(" esporta_xml_tavolo tavolo inesistente");
                cB(true);

            }
            btn_tavoli("CONTO");
            return false;
        }

        //console.log("result", result);

        //INTESTAZIONE
        var output = '<?xml version="1.0" standalone="yes"?>\r\n\
                        <dataroot xmlns="http://www.w3.org/2001/TAVOLI.xsd">\r\n';
        var i = 0;
        var cod_articolo_princ;
        var ts_princ;
        var prog_principale;
        var descrizione_articolo;
        var ordinamento = '';

        var qta_princ = 1;

        result.forEach(function (obj) {
            ////console.log(obj);
            if (obj.prezzo_un.trim() === "") {
                obj.prezzo_un = "0,00";
            }

            //PRIMA ERA +1 MA NON SE NE CAPISCE IL MOTIVO
            obj.prog_inser = parseInt(obj.prog_inser);
            obj.dest_stampa !== undefined ? ts_princ = obj.dest_stampa : ts_princ = '1';
            obj.peso_ums !== undefined ? obj.peso_ums : obj.peso_ums = '';
            obj.ordinamento !== undefined ? ordinamento = obj.ordinamento : ordinamento = '01';
            prog_principale = aggZero(obj.prog_inser, 3);
            descrizione_articolo = obj.desc_art;
            console.log(obj.dest_stampa);
            console.log(obj.dest_stampa !== undefined);
            console.log(ts_princ);
            var contiene_variante = '';
            obj.contiene_variante === '1' ? contiene_variante = 'SI' : contiene_variante = '';
//---
            if (obj.desc_art[0] === "+" || obj.desc_art[0] === "-" || obj.desc_art[0] === "x" || obj.desc_art[0] === "*" || obj.desc_art[0] === "=")
            {
                prog_principale = aggZero(prog_principale.substring(0, 3), 3) + ' ' + aggZero(obj.prog_inser, 3) + ' ';
                descrizione_articolo = obj.desc_art.substring(1);
                variabile = obj.desc_art[0];
            } else if (obj.desc_art[0] === "(") {
                prog_principale = aggZero(prog_principale.substring(0, 3), 3) + ' ' + aggZero(obj.prog_inser, 3) + ' ';
                variabile = "*";
            } else
            {
                cod_articolo_princ = obj.cod_articolo;
                variabile = '';
            }

            var stampata_sn = 'N';
            if (obj.stampata_sn === 'S' || comanda.tavolotxt === '*TAKEAWAY*')
            {
                stampata_sn = 'S';
            }

            if (stampata_sn === 'N')
            {
                incasso_annullato = true;
                var ora = h + ':' + m + ':' + s;
            } else
            {
                var ora = obj.ora_;
            }

            var operatore = obj.operatore;
            if (operatore.length === 0) {
                operatore = comanda.operatore;
            }

            if (testa_totale[obj.BIS] === undefined) {
                testa_totale[obj.BIS] = 0;
            }
            testa_totale[obj.BIS] += parseFloat(conv_prz_fc_ibr(obj.prezzo_un)) * parseInt(obj.quantita);

            var ncard3 = '';
            if (obj.NCARD3) {
                ncard3 = obj.NCARD3;
            }

            let tab_iva = alasql("select aliquota from tabella_iva where lingua='" + comanda.lingua_stampa + "';");
            let percentuali_iva = tab_iva.map(d => d.aliquota);

            if (percentuali_iva.indexOf(obj.perc_iva.trim()) === -1) {
                /*bootbox.alert("Attenzione: l'IVA dell'articolo " + descrizione_articolo + " non &egrave; presente nella tabella IVA. Modificalo e ri-lancia l'operazione");
                 annulla_comanda2 = true;
                 return false;*/

                obj.perc_iva = comanda.percentuale_iva_default;
            } else if (isNaN(parseFloat(conv_prz_fc_ibr(obj.prezzo_un)).toFixed(2))) {
                bootbox.alert("Attenzione: il prezzo dell'articolo " + descrizione_articolo + " non &egrave; valido. Modificalo e ri-lancia l'operazione");
                annulla_comanda2 = true;
                return false;
            } else if (isNaN(parseInt(obj.quantita)) || parseInt(obj.quantita) < 1) {
                bootbox.alert("Attenzione: la quantit&agrave; dell'articolo " + descrizione_articolo + " non &egrave; valida. Modificala e ri-lancia l'operazione");
                annulla_comanda2 = true;
                return false;
            }

            output += '<TAVOLI>\r\n';
            output += '<TC>C</TC>\r\n';
            output += '<DATA>' + data_inizio_servizio + '</DATA>\r\n';
            output += '<TAVOLO>' + aggZero(variabile_tavolo, 3) + '</TAVOLO>\r\n';
            output += '<BIS>' + obj.BIS + '</BIS>\r\n';
            output += '<PRG>' + aggZero(obj.prog_inser, 3) + '</PRG>\r\n';
            output += '<ART>' + cod_articolo_princ + '</ART>\r\n';
            output += '<ART2>' + obj.cod_articolo + '</ART2>\r\n';
            output += '<VAR>' + variabile + '</VAR>\r\n';
            output += '<DES>' + descrizione_articolo + '</DES>\r\n';
            output += '<PRZ>' + parseFloat(conv_prz_fc_ibr(obj.prezzo_un)).toFixed(2).replace('.', ',') + '</PRZ>\r\n';
            output += '<COSTO>0</COSTO>\r\n';
            output += '<QTA>' + obj.quantita + '</QTA>\r\n';
            output += '<SN>' + stampata_sn + '</SN>\r\n';
            output += '<CAT>' + obj.categoria + '</CAT>\r\n';
            output += '<TOTALE />\r\n';
            output += '<SCONTO />\r\n';
            output += '<AUTORIZ>' + obj.sconto_perc + '</AUTORIZ>\r\n';
            output += '<INC>0</INC>\r\n';
            output += '<CARTACRED>' + obj.carta_credito + '</CARTACRED>\r\n';
            output += '<BANCOMAT/>\r\n';
            output += '<ASSEGNI>' + obj.perc_iva + '</ASSEGNI>\r\n';
            output += '<NCARD1>' + contiene_variante + '</NCARD1>\r\n';
            output += '<NCARD2/>\r\n';
            output += '<NCARD3>' + ncard3 + '</NCARD3>\r\n';
            output += '<NCARD4/>\r\n';
            output += '<NCARD5>' + ordinamento + '</NCARD5>\r\n'; //ORDINAMENTO
            output += '<NSEGN>0</NSEGN>\r\n';
            output += '<NEXIT/>\r\n';
            output += '<TS>' + ts_princ + '</TS>\r\n'; //' + ts_princ + '
            output += '<NOME>' + operatore + '</NOME>\r\n';
            output += '<ORA>' + ora + '</ORA>\r\n';
            output += '<LIB1>1</LIB1>\r\n';
            output += '<LIB2>' + obj.LIB2 + '</LIB2>\r\n';
            output += '<LIB3/>\r\n';
            output += '<LIB4>' + obj.LIB4 + '</LIB4>\r\n';
            output += '<LIB5>' + obj.dest_stampa_2 + '</LIB5>\r\n';
            output += '<CLI/>\r\n';
            output += '<QTAP>0</QTAP>\r\n';
            output += '<NODO>' + obj.nodo + '</NODO>\r\n';
            output += '<PORTATA>' + obj.portata + '</PORTATA>\r\n';
            output += '<NUMP>0</NUMP>\r\n';
            output += '<TAVOLOTXT>' + comanda.tavolotxt + '</TAVOLOTXT>\r\n';
            output += '<ULTPORT>' + obj.ultima_portata + '</ULTPORT>\r\n';
            output += '<AGG_GIAC>' + obj.AGG_GIAC + '</AGG_GIAC>\r\n';
            output += '</TAVOLI>\r\n';
            i++;
        });

        if (annulla_comanda2 === true) {
            return false;
        }

        var b1 = alasql('select BIS from comanda where stato_record="ATTIVO" and posizione="CONTO" and ntav_comanda="' + variabile_tavolo + '" group by id,BIS order by prog_inser asc;');


        b1.forEach(function (e) {
            var bi2 = alasql('select desc_art as desc_sconto,prezzo_un as sconto from comanda where stato_record="ATTIVO" and ntav_comanda="' + variabile_tavolo + '" and BIS="' + e.BIS + '" and posizione="SCONTO" limit 1;');
            var bi3 = alasql('select nump_xml as numero_coperti from comanda  where stato_record="ATTIVO" and ntav_comanda="' + variabile_tavolo + '" and BIS="' + e.BIS + '" order by nump_xml desc limit 1;');
            var bi4 = alasql('select desc_art,LIB2,dest_stampa as storicizzazione,prezzo_un,incassato,costo,operatore,contiene_variante as NCARD1,NCARD2,NCARD3,NCARD4,NCARD5,NSEGN,NEXIT,carta_credito,bancomat from comanda  where ntav_comanda="' + variabile_tavolo + '"  and  desc_art LIKE "RECORD TESTA%" and stato_record="ATTIVO" and BIS="' + e.BIS + '" limit 1;');
            var b2 = bi2[0];
            var b3 = bi3[0];
            var b4 = bi4[0];

            var rec = new Object();
            // SEZIONE BIS
            rec.BIS = e.BIS;
            if (rec.BIS !== "") {
                bis_presente = true;
            }
            // SEZIONE SCONTISTICA
            rec.desc_sconto = "";
            rec.sconto = "";
            if (b2 !== undefined) {
                rec.desc_sconto = b2.desc_sconto;
                rec.sconto = b2.sconto;
            }
            // SEZIONE COPERTI
            var numero_coperti = $("#numero_coperti").val();
            rec.numero_coperti = "";

            if (b3 !== undefined) {
                rec.numero_coperti = b3.numero_coperti;

            }
            console.log("BIS COPERTI 0", "LCS", lettera_conto_split(), "BIS", rec.BIS, "ncop", numero_coperti, "ncop2", rec.numero_coperti, "B3", b3);

            // SEZIONE PAGAMENTI
            rec.incassato = "";
            rec.costo = "";
            rec.operatore = "";
            rec.NCARD1 = "";
            rec.NCARD2 = "";
            rec.NCARD3 = "";
            rec.NCARD4 = "";
            rec.NCARD5 = "";
            rec.NSEGN = "";
            rec.NEXIT = "";
            rec.carta_credito = "";
            rec.bancomat = "";
            rec.storicizzazione = "";
            rec.LIB2 = "";
            rec.desc_art = "";
            if (b4 !== undefined) {
                rec.desc_art = b4.desc_art;
                rec.incassato = b4.incassato;
                rec.costo = b4.costo;
                rec.operatore = b4.operatore;
                rec.NCARD1 = b4.NCARD1;
                rec.NCARD2 = b4.NCARD2;
                rec.NCARD3 = b4.NCARD3;
                rec.NCARD4 = b4.NCARD4;
                rec.NCARD5 = b4.NCARD5;
                rec.NSEGN = b4.NSEGN;
                rec.NEXIT = b4.NEXIT;
                rec.carta_credito = b4.carta_credito;
                rec.bancomat = b4.bancomat;
                rec.storicizzazione = b4.storicizzazione;
                rec.LIB2 = b4.LIB2;
            }

            var incassato = 0;




            var sconto = '0.00';



            if (rec !== undefined && typeof rec.sconto === 'string' && rec !== "") {
                if (rec.sconto !== undefined && rec.sconto !== "" && rec.sconto.substr(-1) === "%") {

                    //Bisogna fare il calcolo
                    sconto = testa_totale[rec.BIS] / 100 * rec.sconto.substr(1).slice(0, -1);

                    sconto = sconto.toString();
                    console.log("CALCOLO CONTO SCONTO", sconto);
                } else if (rec.sconto !== undefined && rec.sconto !== "") {
                    sconto = rec.sconto.substr(1);
                }
                autoriz = rec.desc_sconto;


            }

            //PER LA SAGRA NON SERVE PERCHE' INCASSA IN AUTOMATICO
            //E POI SE SI RIPRENDE UNA COMANDA DOPO AVER LASCIATO LA L'INCASSO FA CASINO
            //DA VERIFICARE



            incassato = testa_totale[rec.BIS] - parseFloat(sconto);
            incassato = incassato.toString();

            //Nel fare parseFloat bisogna convertire la , in ., altrimenti restituisce il numero senza decimali, se era un incasso non automatico
            incassato = parseFloat(conv_prz_fc_ibr(incassato)).toFixed(2).replace('.', ',');



            incassato = (parseFloat(conv_prz_fc_ibr(incassato)) - parseFloat(conv_prz_fc_ibr(rec.costo)) - parseFloat(conv_prz_fc_ibr(rec.carta_credito))).toFixed(2);

            incassato = parseFloat(conv_prz_fc_ibr(incassato)).toFixed(2).replace('.', ',');

            sconto = parseFloat(conv_prz_fc_ibr(sconto)).toFixed(2).replace('.', ',');

            if (rec.BIS === lettera_conto_split()) {

                rec.incassato = incassato.replace(".", ",");


                rec.storicizzazione = storicizzazione;

                if (numero_coperti !== undefined && !isNaN(numero_coperti) && numero_coperti !== "")
                {
                    testa_totale[rec.BIS] += parseInt(numero_coperti) * parseFloat(comanda.valore_coperto);


                    if (parseInt(numero_coperti) - comanda.ultimi_coperti_importati !== 0) {
                        annulla_comanda = false;
                    }
                }
            } else
            {
                numero_coperti = rec.numero_coperti;
            }

            testa_totale[rec.BIS] = parseFloat(testa_totale[rec.BIS]).toFixed(2).replace(".", ",");

            var DES = "RECORD TESTA";
            if (b4 && b4.desc_art) {
                DES = b4.desc_art;
            } else if (comanda.set_tid === true) {
                DES += " TID=" + comanda.TID;

            }

            let totale_testa = "0,00";
            if (!isNaN(parseFloat(testa_totale[rec.BIS]))) {
                totale_testa = testa_totale[rec.BIS];
            }

            let totale_incassato = "";
            if (!isNaN(parseFloat(rec.incassato)) || rec.incassato.trim() === "") {
                totale_incassato = rec.incassato;
            }

            let totale_carta_credito = "";
            if (!isNaN(parseFloat(rec.carta_credito)) || rec.carta_credito.trim() === "") {
                totale_carta_credito = rec.carta_credito;
            }

            let totale_bancomat = "";
            if (!isNaN(parseFloat(rec.bancomat)) || rec.bancomat.trim() === "") {
                totale_bancomat = rec.bancomat;
            }

            output += '<TAVOLI>\r\n\
                <TC>T</TC>\r\n\
                <DATA>' + data_inizio_servizio + '</DATA>\r\n\
                <TAVOLO>' + aggZero(variabile_tavolo, 3) + '</TAVOLO>\r\n\
                <BIS>' + rec.BIS + '</BIS>\r\n\
                <PRG>000</PRG>\r\n\
                <ART>01</ART>\r\n\
                <ART2/>\r\n\
                <VAR/>\r\n\
                <DES>' + DES + '</DES>\r\n\
                <PRZ/>\r\n\
                <COSTO>' + rec.costo + '</COSTO>\r\n\
                <QTA/>\r\n\
                <SN>N</SN>\r\n\
                <CAT/>\r\n\
                <TOTALE>' + totale_testa + '</TOTALE>\r\n\
                <SCONTO>' + sconto + '</SCONTO>\r\n\
                <AUTORIZ/>\r\n\
                <INC>' + totale_incassato + '</INC>\r\n\
                <CARTACRED>' + totale_carta_credito + '</CARTACRED>\r\n\
                <BANCOMAT>' + totale_bancomat + '</BANCOMAT>\r\n\
                <ASSEGNI>0,00</ASSEGNI>\r\n\
                <NCARD1>' + rec.NCARD1 + '</NCARD1>\r\n\
                <NCARD2>' + rec.NCARD2 + '</NCARD2>\r\n\
                <NCARD3>' + rec.NCARD3 + '</NCARD3>\r\n\
                <NCARD4>' + rec.NCARD4 + '</NCARD4>\r\n\
                <NCARD5>' + rec.NCARD5 + '</NCARD5>\r\n\
                <NSEGN>' + rec.NSEGN + '</NSEGN>\r\n\
                <NEXIT>' + rec.NEXIT + '</NEXIT>\r\n\
                <TS>' + rec.storicizzazione + '</TS>\r\n\
                <NOME>' + comanda.operatore + '</NOME>\r\n\
                <ORA>' + ora + '</ORA>\r\n\
                <LIB1>1</LIB1>\r\n\
                <LIB2>' + rec.LIB2 + '</LIB2>\r\n\
                <LIB3/>\r\n\
                <LIB4/>\r\n\
                <LIB5>S</LIB5>\r\n\
                <CLI/>\r\n\
                <QTAP>0</QTAP>\r\n\
                <NODO> </NODO>\r\n\
                <PORTATA/>\r\n\
                <NUMP>' + numero_coperti + '</NUMP>\r\n\
                <TAVOLOTXT>' + comanda.tavolotxt + '</TAVOLOTXT>\r\n\
                <ULTPORT>N</ULTPORT>\r\n\
                <AGG_GIAC>N</AGG_GIAC>\r\n\
                </TAVOLI>\r\n';


        });
        if (annulla_comanda2 === true) {
            return false;
        }
        output += '</dataroot>';
        output = output.replace(/>null</gi, '><');
        output = output.replace(/>undefined</gi, '><');

        console.log("OUTPUT XML CONTO" + comanda.tavolotxt, output);


        var d = new Date();
        var Y = d.getFullYear();
        var M = addZero(d.getDate(), 2);
        var D = addZero((d.getMonth() + 1), 2);
        var h = addZero(d.getHours(), 2);
        var m = addZero(d.getMinutes(), 2);
        var s = addZero(d.getSeconds(), 2);
        var ms = addZero(d.getMilliseconds(), 3);
        var data_tablet = h + '' + m + '' + s;

        var id_tablet = "29";
        var id_temp = leggi_get_url('id');

        if (id_temp !== undefined && id_temp !== null && id_temp !== 'null' && id_temp.length === 2) {
            id_tablet = id_temp;
        }


//---Imposta il nome file del SDS---
        var nome_file = "/SDS/PDA" + id_tablet + "_" + data_tablet + BIS + ".XM_";

        //---Promise, che in caso di storicizzazione NON automatica, esporta il planing del tavolo---
        //FinchÃƒÆ’Ã‚Â¨ il tavolo non viene esportato e riletto con l'hash di origine, la funzione resta bloccata
        //In entrambi i casi comunque esporta l'xml del tavolo (vedere salva_file)
        var prom_storicizzazione = new Promise(function (resolve, reject) {
            if (incasso_annullato === true && comanda.tavolotxt !== '*TAKEAWAY*') {
                alert("PRIMA SI LANCIA TUTTO IN COMANDA E POI SI INCASSA.");
                $('.tasto_bestellung').css('pointer-events', '');
                $('.tasto_konto').css('pointer-events', '');

                $('.tasto_quittung').css('pointer-events', '');
                $('.tasto_rechnung').css('pointer-events', '');

                $('.tasto_bargeld').css('pointer-events', '');
            } else {
                //SEMPRE
                resolve(true);
            }

        });

        //Promessa per risolvere il bug del btn tavoli se non la usassi
        //Serve ad esportare il file del SDS dopo l'esportazione sicura del tavolo
        prom_storicizzazione.then(function () {

            oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].nome_file_sds = "_" + comanda.folder_number + "/" + nome_file;
            oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].contenuto_file_sds = output;
            oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].tipo_file_sds = "SDS";
            comanda.xml.salva_file_xml("_" + comanda.folder_number + "/" + nome_file, output, "SDS", function () {

                if (bis_presente === true && b1.length > 1) {

                    nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3) + ".8";


                    oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].nome_file_planing = "_" + comanda.folder_number + "/" + nome_file;
                    oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].contenuto_file_planing = "";
                    oggetto_tavoli_presalvataggio[aggZero(variabile_tavolo, 3)].tipo_file_planing = "PLANING";
                    comanda.xml.salva_file_xml("_" + comanda.folder_number + "/" + nome_file, "", "PLANING", function () {
                        var data_intera = comanda.funzionidb.data_attuale();
                        var data = data_intera.substring(0, 10);
                        var ora = data_intera.substring(11, 16);
                        var testo_query = "update comanda set stampata_sn='S',data_stampa='" + data + "',ora_stampa='" + ora + "',fiscalizzata_sn='N' where stato_record='ATTIVO' and ntav_comanda='" + variabile_tavolo + "'";
                        comanda.sock.send({tipo: "comanda_stampata", operatore: comanda.operatore, query: testo_query});
                        comanda.sincro.query(testo_query, function () {

                            if (comanda.tavolotxt !== "BANCO" && comanda.tavolotxt !== "BAR" && comanda.tavolotxt.left(7) !== "ASPORTO" && comanda.tavolotxt !== "TAKEAWAY") {
                                $('#conto_separato_grande').modal('hide');
                                $('.tasto_bestellung').css('pointer-events', '');
                                $('.tasto_konto').css('pointer-events', '');

                                $('.tasto_quittung').css('pointer-events', '');
                                $('.tasto_rechnung').css('pointer-events', '');

                                $('.tasto_bargeld').css('pointer-events', '');
                                btn_tavoli('CONTO');

                            } else
                            {
                                btn_parcheggia();
                            }

                        });
                    });
                } else {
                    nome_file = "/PLANING/P" + aggZero(variabile_tavolo, 3);
                    //NEL QUITTUNG E RECHNUNG VA ELIMINATO IL COLORE A MANO
                    comanda.xml.elimina_file_xml("_" + comanda.folder_number + "/" + nome_file, true, function () {
                        var data_intera = comanda.funzionidb.data_attuale();
                        var data = data_intera.substring(0, 10);
                        var ora = data_intera.substring(11, 16);
                        var testo_query = "update comanda set stampata_sn='S',data_stampa='" + data + "',ora_stampa='" + ora + "',fiscalizzata_sn='N' where stato_record='ATTIVO' and ntav_comanda='" + variabile_tavolo + "'";
                        comanda.sock.send({tipo: "comanda_stampata", operatore: comanda.operatore, query: testo_query});
                        comanda.sincro.query(testo_query, function () {

                            if (comanda.tavolotxt !== "BANCO" && comanda.tavolotxt !== "BAR" && comanda.tavolotxt.left(7) !== "ASPORTO" && comanda.tavolotxt !== "TAKEAWAY") {
                                $('#conto_separato_grande').modal('hide');
                                $('.tasto_bestellung').css('pointer-events', '');
                                $('.tasto_konto').css('pointer-events', '');

                                $('.tasto_quittung').css('pointer-events', '');
                                $('.tasto_rechnung').css('pointer-events', '');

                                $('.tasto_bargeld').css('pointer-events', '');
                                btn_tavoli('CONTO');

                            } else
                            {
                                btn_parcheggia();
                            }

                        });


                    });

                }
            });
        });




    };

    if ($('#metodo_pagamento_gutshein').hasClass('in')) {
        switch (comanda.conto) {
            case "#prodotti_conto_separato_grande":
                storicizza_xml(1);

                break;
            case "#prodotti_conto_separato_2_grande":
                storicizza_xml(2);

                break;
            default:
                if (comanda.avviso_bargeld === "S") {
                    bootbox.confirm("Sei sicuro di voler incassare con Bargeld?", function (risposta) {
                        if (risposta === true) {
                            procedi();
                        }
                    });
                } else {
                    procedi();
                }
        }
    } else if (comanda.avviso_bargeld === "S") {
        bootbox.confirm("Sei sicuro di voler incassare con Bargeld?", function (risposta) {
            if (risposta === true) {
                procedi();
            }
        });
    } else {
        procedi();
    }
}


function layout_tasti_tablet_ibrido(parametro) {

    let togli_tasto_bargeld = false;

    let query_testa_gutshein = "select * from comanda where desc_art LIKE 'RECORD_TESTA%';";
    let rtg = alasql(query_testa_gutshein);


    if (rtg.length > 0) {

        if (rtg[0].incassato === "0,00" && (rtg[0].costo === "0,00" || rtg[0].costo === ""))
        {

        } else {
            if (comanda.SETB36 === 'S' && comanda.PRF2 === 'SI') {
                togli_tasto_bargeld = true;
            }
        }
    }

    if (comanda.SETB36 === 'S' && comanda.PRF2 === 'SI') {
        $('.tasto_bargeld').hide();
    }

    var array_tasti = new Array();
    var array_tasti_incasso = new Array();
    var classi_tasti = new Array();
    var numero_tasti = 9;
    //<a '+comanda.evento+'="popup_scelta_sconto()" class="col-xs-6 btn btn-info" style="line-height: 90px;font-size:2em;text-transform: uppercase;">lang_58</a>

    if (comanda.tasto_carte_rechnung === 'N') {
        numero_tasti--;
    } else {
        array_tasti.push('tasto_carte_rechnung');
        classi_tasti.push('tasto_carte_rechnung');
    }

    if (comanda.tasto_incasso === 'N') {
        numero_tasti--;
    } else {
        array_tasti.push('incasso_manuale');
        classi_tasti.push('tasto_incasso_manuale');
    }

    if (comanda.tasto_sconto === 'N') {
        numero_tasti--;
    } else {
        array_tasti.push('sconto');
        classi_tasti.push('tasto_sconto');
    }

    if (comanda.tasto_rechnung === 'N') {
        numero_tasti--;
    } else {
        array_tasti.push('fattura');
        array_tasti_incasso.push('fattura');
        classi_tasti.push('tasto_rechnung');
    }

    if (comanda.tasto_quittung === "N") {
        numero_tasti--;
    } else {
        array_tasti.push('scontrino');
        array_tasti_incasso.push('scontrino');
        classi_tasti.push('tasto_quittung');
    }

    /* Anche Italiani tipo 131 */
    if (comanda.tasto_bargeld === "N")
    {
        numero_tasti--;
    } else
    {
        if (togli_tasto_bargeld === true) {
            numero_tasti--;

        } else {
            array_tasti.push('incasso');
            //array_tasti_incasso.push('incasso');
            classi_tasti.push('tasto_bargeld');


        }
    }


    if (comanda.tasto_konto === "N") {
        numero_tasti--;
    } else
    {
        array_tasti.push('conto');
        classi_tasti.push('tasto_konto');
    }

    if (comanda.tasto_comanda_konto === "N") {
        numero_tasti--;
    } else
    {
        array_tasti.push('comanda_conto');
        classi_tasti.push('tasto_comanda_konto');
    }

    if (comanda.tasto_comanda_konto_su_comanda === "N") {
        $(".tasto_comanda_conto").hide();
        $(".tasto_bestellung").removeClass("col-xs-6");
        $(".tasto_bestellung").addClass("col-xs-12");
    } else
    {
        $(".tasto_bestellung").removeClass("col-xs-12");
        $(".tasto_bestellung").addClass("col-xs-6");
        $(".tasto_comanda_conto").show();
    }

    if (comanda.tasto_gutshein === "N") {
        numero_tasti--;
    } else
    {
        array_tasti.push('gutshein');
        classi_tasti.push('tasto_gutshein');
    }







    var conta_tasti = numero_tasti;
    var filadue = Math.floor(parseInt(conta_tasti / 2));
    var filauno = conta_tasti % 2;
    var conta_tasti_2 = array_tasti_incasso.length;
    var filadue_2 = Math.floor(parseInt(conta_tasti_2 / 2));
    var filauno_2 = conta_tasti_2 % 2;
    console.log("LAYOUT TASTI: File da due: " + filadue + " - File da uno: " + filauno);
    var codice_tasti = '';
    var codice_tasti_2 = '';
    var fontsize = "36px";
    var lineheight = "10.55vh";
    if (comanda.iphone === true) {
        fontsize = "2.5vh";
        lineheight = "6vh";
    }

    for (var i = 0; i < (filadue * 2); )
    {

        codice_tasti += '<a ' + comanda.evento + '="' + testo_colore(array_tasti[i])[2] + '" class="col-xs-6 btn ' + testo_colore(array_tasti[i])[0] + ' ' + classi_tasti[i] + '" style="line-height: ' + lineheight + ';font-size:' + fontsize + ';text-transform: uppercase;">' + testo_colore(array_tasti[i])[1] + '</a><a ' + comanda.evento + '="' + testo_colore(array_tasti[i + 1])[2] + '" class="col-xs-6 btn ' + testo_colore(array_tasti[i + 1])[0] + ' ' + classi_tasti[i + 1] + '" style="line-height: ' + lineheight + ';font-size:' + fontsize + ';text-transform: uppercase;">' + testo_colore(array_tasti[i + 1])[1] + '</a>';
        codice_tasti += '<div class="col-md-12 col-xs-12" style="height:0.47vh;"></div>';
        i = i + 2;
    }

    if (filauno > 0) {
        codice_tasti += '<a ' + comanda.evento + '="' + testo_colore(array_tasti[i])[2] + '" class="col-xs-12 btn ' + testo_colore(array_tasti[i])[0] + ' ' + classi_tasti[i] + '" style="line-height: ' + lineheight + ';font-size:' + fontsize + ';text-transform: uppercase;">' + testo_colore(array_tasti[i])[1] + '</a>';
        codice_tasti += '<div class="col-md-12 col-xs-12" style="height:0.47vh;"></div>';
    }

    for (var i = 0; i < (filadue_2 * 2); )
    {

        codice_tasti_2 += '<a ' + comanda.evento + '="if(parseFloat($(\'#gutshein_resto\').html())>0){bootbox.alert(\'Importo inserito troppo alto rispetto al totale da incassare.\');}else{' + testo_colore(array_tasti_incasso[i])[2] + '}" class="col-xs-6 btn ' + testo_colore(array_tasti_incasso[i])[0] + ' ' + classi_tasti[i] + '" style="line-height: ' + lineheight + ';font-size:' + fontsize + ';text-transform: uppercase;">' + testo_colore(array_tasti_incasso[i])[1] + '</a><a ' + comanda.evento + '="if(parseFloat($(\'#gutshein_resto\').html())>0){bootbox.alert(\'Importo inserito troppo alto rispetto al totale da incassare.\');}else{' + testo_colore(array_tasti_incasso[i + 1])[2] + '}" class="col-xs-6 btn ' + testo_colore(array_tasti_incasso[i + 1])[0] + ' ' + classi_tasti[i + 1] + '" style="line-height: ' + lineheight + ';font-size:' + fontsize + ';text-transform: uppercase;">' + testo_colore(array_tasti_incasso[i + 1])[1] + '</a>';
        codice_tasti_2 += '<div class="col-md-12 col-xs-12" style="height:0.47vh;"></div>';
        i = i + 2;
    }

    if (filauno_2 > 0) {
        codice_tasti_2 += '<a ' + comanda.evento + '="if(parseFloat($(\'#gutshein_resto\').html())>0){bootbox.alert(\'Importo inserito troppo alto rispetto al totale da incassare.\');}else{' + testo_colore(array_tasti_incasso[i])[2] + '}" class="col-xs-12 btn ' + testo_colore(array_tasti_incasso[i])[0] + ' ' + classi_tasti[i] + '" style="line-height: ' + lineheight + ';font-size:' + fontsize + ';text-transform: uppercase;">' + testo_colore(array_tasti_incasso[i])[1] + '</a>';
        codice_tasti_2 += '<div class="col-md-12 col-xs-12" style="height:0.47vh;"></div>';
    }


    $('.tasti_incasso_ibrido').html(codice_tasti_2);
    if (parametro !== true) {
        $('#tasti_incasso_nascosti_palmare').html(codice_tasti);
        var file_totali = filadue + filauno;
        if (comanda.iphone === true) {
            if (file_totali === 4) {
                $('#tab_dx').css('height', '23.4vh');
            } else if (file_totali === 3) {
                $('#tab_dx').css('height', '32vh');
            } else if (file_totali === 2) {
                $('#tab_dx').css('height', '42.2vh');
            } else if (file_totali === 1) {
                $('#tab_dx').css('height', '54.6vh');
            } else if (file_totali === 0) {
                $('#tab_dx').css('height', '63.8vh');
            }
        } else if (comanda.iOS === true) {
            if (file_totali === 4) {
                $('#tab_dx').css('height', '34.6vh');
            } else if (file_totali === 3) {
                $('#tab_dx').css('height', '43.45vh');
            } else if (file_totali === 2) {
                $('#tab_dx').css('height', '50vh');
            } else if (file_totali === 1) {
                $('#tab_dx').css('height', '56vh');
            } else if (file_totali === 0) {
                $('#tab_dx').css('height', '62vh');
            }
        } else {
            if (file_totali === 4) {
                $('#tab_dx').css('height', '69.17vh');
            } else if (file_totali === 3) {
                $('#tab_dx').css('height', '86.89vh');
            } else if (file_totali === 2) {
                $('#tab_dx').css('height', '100vh');
            } else if (file_totali === 1) {
                $('#tab_dx').css('height', '112vh');
            } else if (file_totali === 0) {
                $('#tab_dx').css('height', '124vh');
            }
        }
    }

    console.log("LAYOUT TASTI: " + codice_tasti);
    function testo_colore(nome_elemento)
    {
        var colore;
        var testo;
        var funzione;
        switch (nome_elemento) {
            case "tasto_carte_rechnung":
                colore = 'btn-success';
                if (comanda.lingua_stampa === 'deutsch') {
                    testo = 'KARTE';
                } else
                {
                    testo = 'NON ITALIANO';
                }
                funzione = 'intesta_fattura(true);';
                break;
            case "incasso_manuale":
                colore = 'btn-warning';
                if (comanda.lingua_stampa === 'deutsch') {
                    testo = 'Incasso';
                } else
                {
                    testo = 'Incasso';
                }
                funzione = 'incasso_manuale_xml();';
                break;
            case "sconto":
                colore = 'btn-warning';
                if (comanda.lingua_stampa === 'deutsch') {
                    testo = 'Rabatt';
                } else
                {
                    testo = 'Sconto';
                }
                funzione = 'popup_scelta_sconto();';
                break;
            case "conto":
                colore = 'btn-info';
                if (comanda.lingua_stampa === 'deutsch') {
                    testo = 'Konto';
                } else
                {
                    testo = 'Conto';
                }
                funzione = 'preconto();';
                break;
            case "comanda_conto":
                colore = 'btn-warning';
                if (comanda.lingua_stampa === 'deutsch') {
                    testo = 'Bestellung + Konto';
                } else
                {
                    testo = 'Comanda + Conto';
                }
                funzione = 'stampa_comanda_piu_conto();';
                break;
            case "comanda_conto_su_comanda":
                colore = 'btn-warning';
                if (comanda.lingua_stampa === 'deutsch') {
                    testo = 'Bestellung + Konto';
                } else
                {
                    testo = 'Comanda + Conto';
                }
                funzione = 'stampa_comanda_piu_conto();';
                break;
            case "scontrino":
                colore = 'btn-success';
                if (comanda.lingua_stampa === 'deutsch') {
                    testo = 'Quittung';
                } else
                {
                    testo = 'Scontrino';
                }
                funzione = 'seleziona_metodo_pagamento(\'SCONTRINO FISCALE\');';
                break;
            case "fattura":
                colore = 'btn-success';
                if (comanda.lingua_stampa === 'deutsch') {
                    testo = 'Rechnung';
                } else
                {
                    testo = 'Fattura';
                }
                funzione = 'intesta_fattura();';
                break;
            case "incasso":
                colore = 'btn-success';
                if (comanda.lingua_stampa === 'deutsch') {
                    testo = 'Bargeld';
                } else
                {
                    testo = 'Storicizza';
                }
                funzione = 'storicizza();';
                break;
            case "gutshein":
                colore = 'btn-success';
                testo = 'Gutschein';
                funzione = 'gutshein_xml();';
                break;


        }

        return [colore, testo, funzione];
    }

}