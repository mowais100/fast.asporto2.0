function invia_verifica_tavoli_bloccati() {
    //CON TIMEOUT DI RISPOSTA
    comanda.sock.send({tipo: "richiesta_status_tavoli", operatore: comanda.operatore, query: "", terminale: comanda.terminale, ip: comanda.ip_address});
    timeout_tavoli_bloccati();
}

function conferma_propri_tavoli_bloccati() {
    if (comanda.dispositivo === "TAVOLO SELEZIONATO")
    {
        comanda.sock.send({tipo: "risposta_status_tavoli", operatore: comanda.operatore, query: comanda.tavolo, terminale: comanda.terminale, ip: comanda.ip_address});
    }
}

var array_tavoli_occupati_davvero=new Array();

function timeout_tavoli_bloccati(){
    array_tavoli_occupati_davvero=new Array();
    
    setTimeout(function(){
        sblocca_tavoli_non_occupati(array_tavoli_occupati_davvero);
    },5000);
    //5 secondi di timeout per i socket    
}

function aggiungi_tavolo_bloccato_davvero(msg){
    array_tavoli_occupati_davvero.push(msg.query);
}

function sblocca_tavoli_non_occupati(array_tavoli_occupati_davvero){
    
    //Prima li sblocca tutti per un millesimo di secondo
    alasql("update tavoli set occupato='0' where numero!='"+comanda.tavolo+"';");
    
    //Poi rioccupa i bloccati davvero
    array_tavoli_occupati_davvero.forEach(function(n){
        alasql("update tavoli set occupato='1' where numero='"+n+"';");
    });
    
 
    //Poi li disegna sulla piantina
    disegna_tavoli_salvati();
    
    //Poi svuota l'array degli occupati
    array_tavoli_occupati_davvero=new Array();
}