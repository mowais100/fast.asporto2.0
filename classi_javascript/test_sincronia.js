function richiesta_status_terminali() {
    comanda.sock.send({tipo: "richiesta_status_terminali", operatore: comanda.operatore, query: "", terminale: comanda.terminale, ip: comanda.ip_address});
    timeout_ip_accesi();
}

function risposta_status_terminali() {
    comanda.sock.send({tipo: "risposta_status_terminali", operatore: comanda.operatore, query: comanda.ip_address, terminale: comanda.terminale, ip: comanda.ip_address});
}

var array_ip_terminali_accesi = new Array();

function timeout_ip_accesi() {
    array_ip_terminali_accesi = new Array();

    setTimeout(function () {
        compila_tabella_ip_accesi(array_ip_terminali_accesi);
    }, 5000);
    //5 secondi di timeout per i socket    
}

function aggiungi_terminale_acceso_davvero(msg) {
    array_ip_terminali_accesi.push(msg.query);
}

function compila_tabella_ip_accesi(array_ip_terminali_accesi) {

    //Poi rioccupa i bloccati davvero
    array_ip_terminali_accesi.forEach(function (n) {
        console.log("IP ACCESO",n);
        //alasql("update tavoli set occupato='1' where numero='" + n + "';");
    });

    //Poi svuota l'array degli occupati
    array_ip_terminali_accesi = new Array();
}