/* global comanda */

let id_comanda = id_comanda_attuale_alasql();
if (id_comanda !== false) {

    let query_select = "select * from comanda where length(nodo)=3 and stato_record='ATTIVO' and id='" + id_comanda + "';";
    let result = alasql(query_select);
    result.forEach((v) => {

        let query_select_prodotti = "select * from prodotti where id='" + v.cod_articolo + "' and categoria!='XXX' limit 1;";
        let result_prodotti = alasql(query_select_prodotti);
        let reparto = "";
        let perc_iva = "";
        if (result_prodotti.length > 0) {

            result_prodotti.forEach((p) => {


                /*CONDIZIONI
                 * 
                 * tavolo asporto (che può essere anche in pizzeria
                 * 
                 *      non xml
                 * 
                 *      tipo consegna pizzeria in pizzeria da asporto
                 *      
                 *      comanda.tavolo === 'TAKE AWAY' || comanda.tavolo === '*TAKEAWAY*' || comanda.tavolotxt === '*TAKEAWAY*'
                 *      
                 *      altri casi
                 *      
                 * */

                if (comanda.compatibile_xml !== true) {

                    if (comanda.tipo_consegna === "PIZZERIA") {

                        if (p.perc_iva_base !== undefined && p.perc_iva_base !== null && p.perc_iva_base !== '')
                        {
                            perc_iva = p.perc_iva_base;
                            if (comanda.compatibile_xml !== true) {
                                reparto = p.reparto_servizi;
                            }
                        } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                            perc_iva = comanda.perc_iva_default;
                            if (comanda.compatibile_xml !== true) {
                                reparto = p.reparto_servizi;
                            }
                        } else {
                            perc_iva = "10";
                            if (comanda.compatibile_xml !== true) {
                                reparto = p.reparto_servizi;
                            }
                        }

                    } else if (comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === 'TAKE AWAY' || comanda.tavolo === '*TAKEAWAY*' || comanda.tavolotxt === '*TAKEAWAY*') {

                        if (p.perc_iva_takeaway !== undefined && p.perc_iva_takeaway !== null && !isNaN(p.perc_iva_takeaway) && p.perc_iva_takeaway !== '')
                        {
                            perc_iva = p.perc_iva_takeaway;
                            if (comanda.compatibile_xml !== true) {
                                reparto = p.reparto_beni;
                            }
                        } else if (comanda.percentuale_iva_takeaway !== undefined && comanda.percentuale_iva_takeaway !== null && !isNaN(comanda.percentuale_iva_takeaway) && comanda.percentuale_iva_takeaway !== '') {
                            perc_iva = comanda.percentuale_iva_takeaway;
                            if (comanda.compatibile_xml !== true) {
                                reparto = p.reparto_beni;
                            }
                        } else if (p.perc_iva_base !== undefined && p.perc_iva_base !== null && p.perc_iva_base !== '')
                        {
                            perc_iva = p.perc_iva_base;
                            if (comanda.compatibile_xml !== true) {
                                reparto = p.reparto_servizi;
                            }
                        } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                            perc_iva = comanda.perc_iva_default;
                            if (comanda.compatibile_xml !== true) {
                                reparto = p.reparto_servizi;
                            }
                        } else {
                            perc_iva = "22";
                            if (comanda.compatibile_xml !== true) {
                                reparto = p.reparto_beni;
                            }
                        }

                    } else {

                        if (p.perc_iva_base !== undefined && p.perc_iva_base !== null && p.perc_iva_base !== '')
                        {
                            perc_iva = p.perc_iva_base;
                            if (comanda.compatibile_xml !== true) {
                                reparto = p.reparto_servizi;
                            }
                        } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                            perc_iva = comanda.perc_iva_default;
                            if (comanda.compatibile_xml !== true) {
                                reparto = p.reparto_servizi;
                            }
                        } else {
                            perc_iva = "10";
                            if (comanda.compatibile_xml !== true) {
                                reparto = p.reparto_servizi;
                            }
                        }

                    }

                } else {

                    if (p.perc_iva_base !== undefined && p.perc_iva_base !== null && p.perc_iva_base !== '')
                    {
                        perc_iva = p.perc_iva_base;
                        if (comanda.compatibile_xml !== true) {
                            reparto = p.reparto_servizi;
                        }
                    } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
                        perc_iva = comanda.perc_iva_default;
                        if (comanda.compatibile_xml !== true) {
                            reparto = p.reparto_servizi;
                        }
                    } else {
                        perc_iva = "10";
                        if (comanda.compatibile_xml !== true) {
                            reparto = p.reparto_servizi;
                        }
                    }

                }

                let query_update = "update comanda set reparto='" + reparto + "',perc_iva='" + perc_iva + "' where nodo LIKE '" + v.nodo + "%' and stato_record='ATTIVO' and id='" + id_comanda + "';";
                alasql(query_update);
                comanda.sock.send({tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: query_update, terminale: comanda.terminale, ip: comanda.ip_address});
                comanda.funzionidb.conto_attivo();
            });
            return true;
        } else {
            return true;
            /* lasci le ive come sono se non hai corrispondenze */
        }
    });
}

/********* ESPERIMENTO VELOCITA *********/

if (operazione !== undefined && operazione !== null && (operazione === '+' || operazione === '-' || operazione === '*' || operazione === '=' || prodotto.descrizione[0] === '('))
{
    //se è variante prende IVA articolo padre a prescindere da tutto quanto
    perc_iva = cb2.perc_iva;
    reparto = cb2.reparto;
} else if (comanda.lingua_stampa === 'deutsch')
{
    if (comanda.tavolo === 'TAKE AWAY' || comanda.tavolo === '*TAKEAWAY*' || comanda.tavolotxt === '*TAKEAWAY*')
    {
        if (perc_iva_takeaway !== undefined && perc_iva_takeaway !== null && perc_iva_takeaway !== '')
        {
            perc_iva = perc_iva_takeaway;
        } else
        {
            perc_iva = '7';
        }
    } else
    {
        if (perc_iva_base !== undefined && perc_iva_base !== null && perc_iva_base !== '')
        {
            perc_iva = perc_iva_base;
        } else
        {
            perc_iva = '19';
        }
    }
} else
{


    if (comanda.tipo_consegna === "PIZZERIA") {

        if (perc_iva_base !== undefined && perc_iva_base !== null && perc_iva_base !== '')
        {
            perc_iva = perc_iva_base;
            if (comanda.compatibile_xml !== true) {
                reparto = prodotto.reparto_servizi;
            }
        } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
            perc_iva = comanda.perc_iva_default;
            if (comanda.compatibile_xml !== true) {
                reparto = comanda.reparto_servizi_default;
            }
        } else {
            perc_iva = "10";
            if (comanda.compatibile_xml !== true) {
                reparto = comanda.reparto_servizi_default;
            }
        }

    } else if (comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === 'TAKE AWAY' || comanda.tavolo === '*TAKEAWAY*' || comanda.tavolotxt === '*TAKEAWAY*') {

        if (perc_iva_takeaway !== undefined && perc_iva_takeaway !== null && !isNaN(perc_iva_takeaway) && perc_iva_takeaway !== '')
        {
            perc_iva = perc_iva_takeaway;
            if (comanda.compatibile_xml !== true) {
                reparto = prodotto.reparto_beni;
            }
        } else if (comanda.percentuale_iva_takeaway !== undefined && comanda.percentuale_iva_takeaway !== null && !isNaN(comanda.percentuale_iva_takeaway) && comanda.percentuale_iva_takeaway !== '') {
            perc_iva = comanda.percentuale_iva_takeaway;
            if (comanda.compatibile_xml !== true) {
                reparto = comanda.reparto_beni_default;
            }
        } else if (perc_iva_base !== undefined && perc_iva_base !== null && perc_iva_base !== '')
        {
            perc_iva = p.perc_iva_base;
            if (comanda.compatibile_xml !== true) {
                reparto = prodotto.reparto_servizi;
            }
        } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
            perc_iva = comanda.perc_iva_default;
            if (comanda.compatibile_xml !== true) {
                reparto = comanda.reparto_servizi_default;
            }
        } else {
            perc_iva = "22";
            if (comanda.compatibile_xml !== true) {
                reparto = comanda.reparto_beni_default;
            }
        }

    } else {

        if (perc_iva_base !== undefined && perc_iva_base !== null && perc_iva_base !== '')
        {
            perc_iva = perc_iva_base;
            if (comanda.compatibile_xml !== true) {
                reparto = prodotto.reparto_servizi;
            }
        } else if (comanda.percentuale_iva_default !== undefined && comanda.percentuale_iva_default !== null && !isNaN(comanda.percentuale_iva_default) && comanda.percentuale_iva_default !== '') {
            perc_iva = comanda.perc_iva_default;
            if (comanda.compatibile_xml !== true) {
                reparto = comanda.reparto_servizi_default;
            }
        } else {
            perc_iva = "10";
            if (comanda.compatibile_xml !== true) {
                reparto = comanda.reparto_servizi_default;
            }
        }

    }

} 