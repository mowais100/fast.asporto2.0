/* global comanda */

function unisci_pagamento_tavolo() {
    var ora_apertura_tavolo = comanda.funzionidb.data_attuale().substr(9, 8);
    var ora_ultima_comanda = comanda.funzionidb.data_attuale().substr(9, 8);
    var da = $('#popup_unisci_pagamento_tavoli--input_text--unisci_pagamento_tavolo_da').val();
    var a = $('#popup_unisci_pagamento_tavoli--input_text--unisci_pagamento_tavolo_a').val();
    if (da === undefined || a === undefined || da === '' || a === '') {
        alert("Inserire tavolo di partenza e tavolo di arrivo, per unirli.");
    } else
    {
        var query_verifica = "SELECT * FROM tavoli WHERE numero='" + a + "';";
        comanda.sincro.query(query_verifica, function (risultato_verifica) {

            if (risultato_verifica.length === 0) {
                alert("Il tavolo " + a + " non esiste.");
            } else
            {
                comanda.tavolo = "0";
                var tavolo_mittente = da;
                var tavolo_destinazione = a;
                var data = comanda.funzionidb.data_attuale();
                var builder = new epson.ePOSBuilder();

                builder.addTextFont(builder.FONT_A);
                builder.addTextAlign(builder.ALIGN_CENTER);
                builder.addTextSize(1, 1);
                builder.addText("-----------------------------------------\n");
                builder.addTextSize(2, 2);
                builder.addText('UNIONE PAGAMENTO TAVOLI\n');
                builder.addTextSize(1, 1);
                builder.addText("-----------------------------------------\n");
                builder.addTextSize(2, 2);
                builder.addFeedLine(1);
                builder.addText('TAVOLO: ' + da + '\nCON TAVOLO: ' + a + '\n');
                builder.addTextSize(1, 1);
                builder.addFeedLine(1);
                builder.addText("-----------------------------------------\n");
                builder.addTextSize(1, 1);
                builder.addText('OPERATORE: ' + comanda.operatore + '\n');
                builder.addText(data.replace(/-/gi, '/') + '\n');
                builder.addFeedLine(1);
                builder.addCut(builder.CUT_FEED);
                var request = builder.toString();
                var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
                //CONTENUTO CONTO
                //console.log(request);



                /*var lista_ip_gia_usati = new Array();
                 for (var i in comanda.nome_stampante) {
                 
                 if (comanda.nome_stampante[i].nome_umano !== undefined && comanda.nome_stampante[i].intelligent === 's' && comanda.nome_stampante[i].fiscale === 'n' && comanda.nome_stampante[i].nome_umano !== 'NESSUNA' && lista_ip_gia_usati.indexOf(comanda.nome_stampante[i].ip) === -1) {
                 lista_ip_gia_usati.push(comanda.nome_stampante[i].ip);
                 //Create a SOAP envelop
                 var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
                 //Create an XMLHttpRequest object
                 var xhr = new XMLHttpRequest();
                 //Set the end point address
                 
                 var url = 'http://' + comanda.nome_stampante[i].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                 //Open an XMLHttpRequest object
                 xhr.open('POST', url, true);
                 //<Header settings>
                 xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
                 xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
                 xhr.setRequestHeader('SOAPAction', '""');
                 // Send print document
                 xhr.send(soap);
                 }
                 }*/


                var query_stampa_sn = "SELECT Field391,Field399 FROM settaggi_profili where id=" + comanda.folder_number + " limit 1;";
                var r = alasql(query_stampa_sn);
                var lista_ip_gia_usati = new Array();

                if (r[0] !== undefined && r[0].Field399 !== "" && r[0].Field399 !== "TUTTE" && lista_ip_gia_usati.indexOf(comanda.nome_stampante[r[0].Field399].ip) === -1) {

                    var url = 'http://' + comanda.nome_stampante[r[0].Field399].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';


                    if (r[0] !== undefined && r[0].Field391 === "true") {
                        //Create an XMLHttpRequest object
                        var xhr = new XMLHttpRequest();
                        //Open an XMLHttpRequest object
                        xhr.open('POST', url, true);
                        //<Header settings>
                        xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
                        xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
                        xhr.setRequestHeader('SOAPAction', '""');
                        // Send print document
                        lista_ip_gia_usati.push(comanda.nome_stampante[r[0].Field399].ip);
                        xhr.send(soap);
                    }
                } else if (r[0] !== undefined && r[0].Field399 !== "" && r[0].Field399 === "TUTTE") {

                    comanda.nome_stampante.forEach(function (e, i) {

                        if (lista_ip_gia_usati.indexOf(comanda.nome_stampante[i].ip) === -1) {
                            var url = 'http://' + comanda.nome_stampante[i].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                            if (r[0] !== undefined && r[0].Field391 === "true") {
                                //Create an XMLHttpRequest object
                                var xhr = new XMLHttpRequest();
                                //Open an XMLHttpRequest object
                                xhr.open('POST', url, true);
                                //<Header settings>
                                xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
                                xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
                                xhr.setRequestHeader('SOAPAction', '""');
                                // Send print document
                                lista_ip_gia_usati.push(comanda.nome_stampante[i].ip);
                                xhr.send(soap);
                            }
                        }
                    });
                }

                //-------------------------------------

                var paga_lui = "SELECT fila_tavoli FROM tavoli_uniti;";
                var r = alasql(paga_lui);


                var fila_tavoli_presente = false;
                var tavolo_mittente_presente = false;
                var tavolo_destinazione_presente = false;

                r.some(
                        v => {
                            console.log(v.fila_tavoli);

                            var b = v.fila_tavoli.split(";").some(
                                    v1 => {

                                        return v1 === tavolo_mittente || v1 === tavolo_destinazione;


                                    });

                            if (b === true) {
                                fila_tavoli_presente = v.fila_tavoli;

                                tavolo_mittente_presente = fila_tavoli_presente.split(";").some(v2 => {
                                    return v2 === tavolo_mittente;
                                });

                                tavolo_destinazione_presente = fila_tavoli_presente.split(";").some(v2 => {
                                    return v2 === tavolo_destinazione;
                                });

                                return v.fila_tavoli;
                            }

                        });

                var fila_tavoli_nuova = false;

                if (fila_tavoli_presente !== false) {
                    /*
                     * 
                     * v è l indice del mysql
                     
                     la modifica di v è il dato con cui sostituire l'indice
                     
                     **/


                    if (tavolo_mittente_presente === true && tavolo_destinazione_presente === true) {
                        //NIENTE
                    } else if (tavolo_mittente_presente === true) {
                        var a1 = fila_tavoli_presente.split(";");
                        var a2 = a1.push(tavolo_destinazione);
                        var fila_tavoli_nuova = a1.join(";");
                    } else if (tavolo_destinazione_presente === true) {
                        var a1 = fila_tavoli_presente.split(";");
                        var a2 = a1.push(tavolo_mittente);
                        var fila_tavoli_nuova = a1.join(";");
                    }

                    if (fila_tavoli_nuova !== false) {
                        var query_modifica = "UPDATE tavoli_uniti SET fila_tavoli='" + fila_tavoli_nuova + "' WHERE fila_tavoli = '" + fila_tavoli_presente + "'";
                        comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: query_modifica, terminale: comanda.terminale, ip: comanda.ip_address});
                        alasql(query_modifica);
                    }
                } else {

                    fila_tavoli_nuova = new Array();

                    fila_tavoli_nuova.push(tavolo_mittente);
                    fila_tavoli_nuova.push(tavolo_destinazione);

                    var query_inserimento = "INSERT INTO tavoli_uniti (fila_tavoli) VALUES ('" + fila_tavoli_nuova.join(";") + "');";
                    comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: query_inserimento, terminale: comanda.terminale, ip: comanda.ip_address});
                    alasql(query_inserimento);
                }
//-------------------------------------





                disegna_tavoli_salvati();
                comanda.funzionidb.conto_attivo();
                $('#popup_sposta_articolo--input_text--numero_tavolo_a').val('');
                $('#popup_sposta_articolo').modal('hide');
                //ORIGINALI
                $('#popup_sposta_tavoli').modal('hide');
                $('#popup_sposta_tavoli input').val('');
                btn_tavoli();





            }
        });
    }
}


function collega_tavoli() {
    var ora_apertura_tavolo = comanda.funzionidb.data_attuale().substr(9, 8);
    var ora_ultima_comanda = comanda.funzionidb.data_attuale().substr(9, 8);
    var da = $('#popup_collega_tavoli--input_text--collega_tavolo_da').val();
    var a = $('#popup_collega_tavoli--input_text--collega_tavolo_a').val();
    if (da === undefined || a === undefined || da === '' || a === '') {
        alert("Inserire tavolo di partenza e tavolo di arrivo, per unirli.");
    } else
    {
        var query_verifica = "SELECT * FROM tavoli WHERE numero='" + a + "';";
        comanda.sincro.query(query_verifica, function (risultato_verifica) {

            if (risultato_verifica.length === 0) {
                alert("Il tavolo " + a + " non esiste.");
            } else
            {
                comanda.tavolo = "0";
                var tavolo_mittente = da;
                var tavolo_destinazione = a;
                var data = comanda.funzionidb.data_attuale();




                var builder = new epson.ePOSBuilder();
                builder.addTextFont(builder.FONT_A);
                builder.addTextAlign(builder.ALIGN_CENTER);
                builder.addTextSize(1, 1);
                builder.addText("-----------------------------------------\n");
                builder.addTextSize(2, 2);
                builder.addText('COLLEGAMENTO TAVOLI\n');
                builder.addTextSize(1, 1);
                builder.addText("-----------------------------------------\n");
                builder.addTextSize(2, 2);
                builder.addFeedLine(1);
                builder.addText('TAVOLO: ' + da + '\nCON TAVOLO: ' + a + '\n');
                builder.addTextSize(1, 1);
                builder.addFeedLine(1);
                builder.addText("-----------------------------------------\n");
                builder.addTextSize(1, 1);
                builder.addText('OPERATORE: ' + comanda.operatore + '\n');
                builder.addText(data.replace(/-/gi, '/') + '\n');
                builder.addFeedLine(1);
                builder.addCut(builder.CUT_FEED);
                var request = builder.toString();
                var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
                //CONTENUTO CONTO
                //console.log(request);



                /*var lista_ip_gia_usati = new Array();
                 for (var i in comanda.nome_stampante) {
                 
                 if (comanda.nome_stampante[i].nome_umano !== undefined && comanda.nome_stampante[i].intelligent === 's' && comanda.nome_stampante[i].fiscale === 'n' && comanda.nome_stampante[i].nome_umano !== 'NESSUNA' && lista_ip_gia_usati.indexOf(comanda.nome_stampante[i].ip) === -1) {
                 lista_ip_gia_usati.push(comanda.nome_stampante[i].ip);
                 //Create a SOAP envelop
                 var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
                 //Create an XMLHttpRequest object
                 var xhr = new XMLHttpRequest();
                 //Set the end point address
                 
                 var url = 'http://' + comanda.nome_stampante[i].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                 //Open an XMLHttpRequest object
                 xhr.open('POST', url, true);
                 //<Header settings>
                 xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
                 xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
                 xhr.setRequestHeader('SOAPAction', '""');
                 // Send print document
                 xhr.send(soap);
                 }
                 }*/


                var query_stampa_sn = "SELECT Field391,Field399 FROM settaggi_profili where id=" + comanda.folder_number + " limit 1;";
                var r = alasql(query_stampa_sn);
                var lista_ip_gia_usati = new Array();

                if (r[0] !== undefined && r[0].Field399 !== "" && r[0].Field399 !== "TUTTE" && lista_ip_gia_usati.indexOf(comanda.nome_stampante[r[0].Field399].ip) === -1) {

                    var url = 'http://' + comanda.nome_stampante[r[0].Field399].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';


                    if (r[0] !== undefined && r[0].Field391 === "true") {
                        //Create an XMLHttpRequest object
                        var xhr = new XMLHttpRequest();
                        //Open an XMLHttpRequest object
                        xhr.open('POST', url, true);
                        //<Header settings>
                        xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
                        xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
                        xhr.setRequestHeader('SOAPAction', '""');
                        // Send print document
                        lista_ip_gia_usati.push(comanda.nome_stampante[r[0].Field399].ip);
                        xhr.send(soap);
                    }
                } else if (r[0] !== undefined && r[0].Field399 !== "" && r[0].Field399 === "TUTTE") {

                    comanda.nome_stampante.forEach(function (e, i) {

                        if (lista_ip_gia_usati.indexOf(comanda.nome_stampante[i].ip) === -1) {
                            var url = 'http://' + comanda.nome_stampante[i].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                            if (r[0] !== undefined && r[0].Field391 === "true") {
                                //Create an XMLHttpRequest object
                                var xhr = new XMLHttpRequest();
                                //Open an XMLHttpRequest object
                                xhr.open('POST', url, true);
                                //<Header settings>
                                xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
                                xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
                                xhr.setRequestHeader('SOAPAction', '""');
                                // Send print document
                                lista_ip_gia_usati.push(comanda.nome_stampante[i].ip);
                                xhr.send(soap);
                            }
                        }
                    });
                }

                var collegamento = tavolo_destinazione;

                if (risultato_verifica[0].collegamento !== undefined && risultato_verifica[0].collegamento !== null && risultato_verifica[0].collegamento !== "") {
                    collegamento = risultato_verifica[0].collegamento;
                }

                var ultimo_nodo_utile = '000';
                var nodo_finale = '000';
                var nodo_principale = '000';
                var nodo_variante = '000';
                var progressivo = '0';
                var ultimo_nodo_destinatario = "SELECT id,substr(nodo,1,3) as nodo, prog_inser FROM comanda WHERE stato_record='ATTIVO' and ntav_comanda='" + collegamento + "' order by nodo DESC; ";
                comanda.sincro.query(ultimo_nodo_destinatario, function (result_dest) {
                    if (result_dest[0] !== undefined && result_dest[0].nodo !== undefined) {
                        nodo_principale = result_dest[0].nodo;
                    }
                    if (result_dest[0] !== undefined && result_dest[0].prog_inser !== undefined) {
                        progressivo = result_dest[0].prog_inser;
                    }

                    var ultimo_nodo_mittente = "SELECT * FROM comanda WHERE ntav_comanda='" + tavolo_mittente + "' and stato_record='ATTIVO' ORDER BY nodo ASC;";
                    //console.log(ultimo_nodo_mittente);
                    comanda.sincro.query(ultimo_nodo_mittente, function (result_mitt) {
                        result_mitt.forEach(function (obj) {

                            if (obj.numero_paganti === "null") {
                                obj.numero_paganti = "";
                            }

                            progressivo = (parseInt(progressivo) + 1).toString();
                            if (obj.desc_art[0] !== "+" && obj.desc_art[0] !== "-" && obj.desc_art[0] !== "(" && obj.desc_art[0] !== "*" && obj.desc_art[0] !== "x" && obj.desc_art[0] !== "=") {
                                console.log("originario", nodo_principale);
                                nodo_finale = parseInt(nodo_principale) + 1;
                                nodo_principale = nodo_finale;
                                console.log("piu uno", nodo_finale);
                                nodo_finale = aggZero(nodo_finale, 3);
                                console.log("ascii", nodo_finale);
                                ultimo_nodo_utile = nodo_finale;
                            } else
                            {

                                nodo_finale = aggZero(progressivo, 3);
                                nodo_finale = ultimo_nodo_utile + ' ' + aggZero(progressivo, 3);
                            }



                            var spostamento = "INSERT INTO comanda \n\
                                        VALUES \n\
                                        ('','" + obj.id + "','','','','','','','','','','','','','','','','','','','','','" + obj.progressivo_fiscale + "','" + obj.ordinamento + "','" + obj.ultima_portata + "','" + obj.cod_articolo + "','" + obj.numero_conto + "','" + obj.contiene_variante + "','" + obj.dest_stampa + "','" + obj.cat_variante + "','" + obj.tipo_record + "',\n\
                                        '" + obj.stato_record + "','" + obj.planning_colori + "','" + obj.data_fisc + "','" + obj.ora_fisc + "','" + collegamento + "',\n\
                                        '" + obj.ntav_attrib + "','" + obj.tab_bis + "','" + progressivo + "','" + obj.art_primario + "','" + obj.art_variante + "','" + obj.tipo_variante + "','" + obj.desc_art + "','" + obj.quantita + "','" + obj.prezzo_un + "','" + obj.sconto_perc + "','" + obj.sconto_imp + "',\n\
                                        '" + obj.netto + "','" + obj.imp_tot_netto + "','" + obj.perc_iva + "','" + obj.costo + "','" + obj.autor_sconto + "','" + obj.categoria + "','" + obj.gruppo + "','" + obj.incassato + "','" + obj.residuo + "','" + obj.tipo_incasso + "','" + obj.contanti + "','" + obj.carta_credito + "','" + obj.bancomat + "',\n\
                                        '" + obj.assegni + "','" + obj.tessera_prepagata + "','" + obj.numero_tessera_prepag + "','" + obj.stampata_sn + "','" + obj.data_stampa + "','" + obj.ora_stampa + "','" + obj.fiscalizzata_sn + "','" + obj.data_ + "','" + obj.ora_ + "','" + obj.n_fiscale + "','" + obj.n_conto_parziale + "',\n\
                                        '" + nodo_finale + "','" + obj.portata + "','" + obj.ult_portata + "','" + obj.centro + "','" + obj.terminale + "','" + obj.operatore + "','" + obj.cod_cliente + "','" + obj.rag_soc_cliente + "','" + obj.coef_trasf + "','" + obj.peso_x_um + "','" + obj.peso_ums + "','" + obj.peso_tot + "','" + obj.qnt_prod + "',\n\
                                        '" + obj.nome_comanda + "','" + obj.posizione + "','" + obj.fiscale_sospeso + "','" + obj.BIS + "','" + comanda.nome_pc + "','" + obj.giorno + "','" + obj.cod_promo + "','" + obj.qta_fissa + "','" + obj.spazio_forno + "','" + obj.tipo_impasto + "','" + obj.tasto_segue + "','" + obj.NCARD2 + "','" + obj.NCARD3 + "',\n\
                                        '" + obj.NCARD4 + "','" + obj.NEXIT + "','" + obj.droppay + "','" + obj.dest_stampa_2 + "','" + obj.id_pony + "','" + obj.prezzo_varianti_aggiuntive + "','" + obj.prezzo_varianti_maxi + "','" + obj.prezzo_maxi_prima + "','" + obj.prezzo_vero + "','" + obj.riepilogo + "','" + obj.data_servizio + "','" + obj.data_comanda + "','" + obj.ora_comanda + "','" + obj.numero_paganti + "','" + obj.numero_servizio + "',\n\
                                        '" + obj.imponibile_rep_1 + "','" + obj.imponibile_rep_2 + "','" + obj.imponibile_rep_3 + "','" + obj.imponibile_rep_4 + "','" + obj.imponibile_rep_5 + "','" + obj.imposta_rep_1 + "','" + obj.imposta_rep_2 + "','" + obj.imposta_rep_3 + "','" + obj.imposta_rep_4 + "','" + obj.imposta_rep_5 + "','" + obj.totale_scontato_ivato + "','" + obj.prezzo_maxi + "','" + obj.misuratore_riferimento + "','" + obj.operatore_cancellazione + "','" + obj.TID + "','" + obj.tipologia_fattorino + "','" + obj.AGG_GIAC + "','" + obj.orario_preparazione + "','" + obj.reparto + "','" + obj.importo_totale_fiscale + "','" + obj.prog_gg_uni + "');";
                            console.log(spostamento);
                            comanda.sock.send({tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: spostamento, terminale: comanda.terminale, ip: comanda.ip_address});
                            comanda.sincro.query(spostamento, function () {
                            });
                        });
                        /* var update_tavolo = "UPDATE tavoli SET colore='0',ora_apertura_tavolo='" + ora_apertura_tavolo + "',ora_ultima_comanda='" + ora_ultima_comanda + "' WHERE numero='" + collegamento + "';";
                         comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: update_tavolo, terminale: comanda.terminale, ip: comanda.ip_address});
                         comanda.sincro.query(update_tavolo, function () {
                         
                         disegna_ultimo_tavolo_modificato(update_tavolo);*/




                        var update_tavolo = "UPDATE tavoli SET collegamento='" + collegamento + "',colore='0',ora_apertura_tavolo='-',ora_ultima_comanda='-' WHERE numero='" + tavolo_mittente + "';";
                        comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: update_tavolo, terminale: comanda.terminale, ip: comanda.ip_address});
                        comanda.sincro.query(update_tavolo, function () {
                            //disegna_ultimo_tavolo_modificato(update_tavolo);
                            var eliminazione_vecchio = "DELETE FROM comanda WHERE ntav_comanda='" + tavolo_mittente + "' and stato_record='ATTIVO';";
                            comanda.sock.send({tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: eliminazione_vecchio, terminale: comanda.terminale, ip: comanda.ip_address});
                            comanda.sincro.query(eliminazione_vecchio, function () {

                                //disegna_ultimo_tavolo_modificato(update_tavolo);

                                disegna_tavoli_salvati();

                                comanda.funzionidb.conto_attivo();
                                $('#popup_sposta_articolo--input_text--numero_tavolo_a').val('');
                                $('#popup_sposta_articolo').modal('hide');
                                //ORIGINALI
                                $('#popup_sposta_tavoli').modal('hide');
                                $('#popup_sposta_tavoli input').val('');
                                btn_tavoli();
                            });
                        });
                        /* });*/
                    });
                });
            }
        });
    }
}

function unisci_parcheggio(id, nome_comanda) {

    var da = $('.popup_unisci_parcheggio--input_text--numero_tavolo:visible').val();
    var id_comanda = id;
    var nome_comanda = nome_comanda;
    var ora_apertura_tavolo = comanda.funzionidb.data_attuale().substr(9, 8);
    var ora_ultima_comanda = comanda.funzionidb.data_attuale().substr(9, 8);
    if (da === undefined || da === '') {
        alert("Inserire tavolo di partenza e tavolo di arrivo, per unirli.");
    } else
    {
        var data = comanda.funzionidb.data_attuale();
        var builder = new epson.ePOSBuilder();
        builder.addTextFont(builder.FONT_A);
        builder.addTextAlign(builder.ALIGN_CENTER);
        builder.addTextSize(1, 1);
        builder.addText("-----------------------------------------\n");
        builder.addTextSize(2, 2);
        builder.addText('UNIONE A PARCHEGGIO\n');
        builder.addTextSize(1, 1);
        builder.addText("-----------------------------------------\n");
        builder.addTextSize(2, 2);
        builder.addFeedLine(1);
        if (comanda.tavolo !== "BANCO" && comanda.tavolo !== "BAR" && comanda.tavolo.left(7) !== "ASPORTO" && comanda.tavolo !== "TAKE AWAY") {
            builder.addText('DA TAVOLO: ' + da + '\nA: ' + nome_comanda + '\n');
        } else
        {
            builder.addText('DA: ' + comanda.parcheggio + '\nA: ' + nome_comanda + '\n');
        }

        builder.addTextSize(1, 1);
        builder.addFeedLine(1);
        builder.addText("-----------------------------------------\n");
        builder.addTextSize(1, 1);
        builder.addText(comanda.lang[109] + ': ' + comanda.operatore + '\n');
        builder.addText(data.replace(/-/gi, '/') + '\n');
        builder.addFeedLine(1);
        builder.addCut(builder.CUT_FEED);
        var request = builder.toString();
        //CONTENUTO CONTO
        //console.log(request);

        var url = "";

        //Create a SOAP envelop
        var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
        var query_stampa_sn = "SELECT Field390,Field398 FROM settaggi_profili where id=" + comanda.folder_number + " limit 1;";
        var r = alasql(query_stampa_sn);

        var lista_ip_gia_usati = new Array();


        if (r[0] !== undefined && r[0].Field398 !== "" && r[0].Field398 !== "TUTTE" && lista_ip_gia_usati.indexOf(comanda.nome_stampante[r[0].Field398].ip) === -1) {

            url = 'http://' + comanda.nome_stampante[r[0].Field398].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';


            if (r[0] !== undefined && r[0].Field390 === "true") {
                //Create an XMLHttpRequest object
                var xhr = new XMLHttpRequest();
                //Open an XMLHttpRequest object
                xhr.open('POST', url, true);
                //<Header settings>
                xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
                xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
                xhr.setRequestHeader('SOAPAction', '""');
                // Send print document
                lista_ip_gia_usati.push(comanda.nome_stampante[r[0].Field398].ip);
                xhr.send(soap);
            }
        } else if (r[0] !== undefined && r[0].Field398 !== "" && r[0].Field398 === "TUTTE") {

            comanda.nome_stampante.forEach(function (e, i) {
                if (lista_ip_gia_usati.indexOf(comanda.nome_stampante[i].ip) === -1) {
                    url = 'http://' + comanda.nome_stampante[i].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';

                    if (r[0] !== undefined && r[0].Field390 === "true") {
                        //Create an XMLHttpRequest object
                        var xhr = new XMLHttpRequest();
                        //Open an XMLHttpRequest object
                        xhr.open('POST', url, true);
                        //<Header settings>
                        xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
                        xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
                        xhr.setRequestHeader('SOAPAction', '""');
                        // Send print document
                        lista_ip_gia_usati.push(comanda.nome_stampante[i].ip);
                        xhr.send(soap);
                    }
                }
            });
        }
        var tavolo_mittente = da;
        var tavolo_destinazione = a;
        var ultimo_nodo_utile = '000';
        var nodo_finale = '000';
        var nodo_principale = '000';
        var nodo_variante = '000';
        var progressivo = '0';
        var a = 'BAR';
        var ultimo_nodo_destinatario = "SELECT id,ntav_comanda,substr(nodo,1,3) as nodo, prog_inser FROM comanda WHERE nome_comanda='" + nome_comanda + "' order by nodo DESC; ";
        //console.log(ultimo_nodo_destinatario);
        comanda.sincro.query(ultimo_nodo_destinatario, function (result_dest) {
            if (result_dest[0] !== undefined && result_dest[0].nodo !== undefined) {
                nodo_principale = result_dest[0].nodo;
            }
            if (result_dest[0] !== undefined && result_dest[0].prog_inser !== undefined) {
                progressivo = result_dest[0].prog_inser;
            }

            if (result_dest[0] !== undefined && result_dest[0].ntav_comanda !== undefined) {
                a = result_dest[0].ntav_comanda;
            }

            var ultimo_nodo_mittente = "SELECT * FROM comanda WHERE ntav_comanda='" + tavolo_mittente + "' and stato_record='ATTIVO' ORDER BY nodo ASC;";
            //console.log(ultimo_nodo_mittente);
            comanda.sincro.query(ultimo_nodo_mittente, function (result_mitt) {
                result_mitt.forEach(function (obj) {



                    progressivo = (parseInt(progressivo) + 1).toString();
                    if (obj.desc_art[0] !== "+" && obj.desc_art[0] !== "-" && obj.desc_art[0] !== "(" && obj.desc_art[0] !== "*" && obj.desc_art[0] !== "x" && obj.desc_art[0] !== "=") {
                        console.log("originario", nodo_principale);
                        nodo_finale = parseInt(nodo_principale) + 1;
                        nodo_principale = nodo_finale;
                        console.log("piu uno", nodo_finale);
                        nodo_finale = aggZero(nodo_finale, 3);
                        console.log("ascii", nodo_finale);
                        ultimo_nodo_utile = nodo_finale;
                    } else
                    {

                        nodo_finale = aggZero(progressivo, 3);
                        nodo_finale = ultimo_nodo_utile + ' ' + aggZero(progressivo, 3);
                    }

                    var spostamento = "INSERT INTO comanda\n\
                                        VALUES \n\
                                        ('','" + id_comanda + "','','','','','','','','','','','','','','','','','','','','','" + obj.progressivo_fiscale + "','" + obj.ordinamento + "','" + obj.ultima_portata + "','" + obj.cod_articolo + "','" + obj.numero_conto + "','" + obj.contiene_variante + "','" + obj.dest_stampa + "','" + obj.cat_variante + "','" + obj.tipo_record + "',\n\
                                        'APERTO','" + obj.planning_colori + "','" + obj.data_fisc + "','" + obj.ora_fisc + "','" + a + "',\n\
                                        '" + obj.ntav_attrib + "','" + obj.tab_bis + "','" + progressivo + "','" + obj.art_primario + "','" + obj.art_variante + "','" + obj.tipo_variante + "','" + obj.desc_art + "','" + obj.quantita + "','" + obj.prezzo_un + "','" + obj.sconto_perc + "','" + obj.sconto_imp + "',\n\
                                        '" + obj.netto + "','" + obj.imp_tot_netto + "','" + obj.perc_iva + "','" + obj.costo + "','" + obj.autor_sconto + "','" + obj.categoria + "','" + obj.gruppo + "','" + obj.incassato + "','" + obj.residuo + "','" + obj.tipo_incasso + "','" + obj.contanti + "','" + obj.carta_credito + "','" + obj.bancomat + "',\n\
                                        '" + obj.assegni + "','" + obj.tessera_prepagata + "','" + obj.numero_tessera_prepag + "','" + obj.stampata_sn + "','" + obj.data_stampa + "','" + obj.ora_stampa + "','" + obj.fiscalizzata_sn + "','" + obj.data_ + "','" + obj.ora_ + "','" + obj.n_fiscale + "','" + obj.n_conto_parziale + "',\n\
                                        '" + nodo_finale + "','" + obj.portata + "','" + obj.ult_portata + "','" + obj.centro + "','" + obj.terminale + "','" + obj.operatore + "','" + obj.cod_cliente + "','" + obj.rag_soc_cliente + "','" + obj.coef_trasf + "','" + obj.peso_x_um + "','" + obj.peso_ums + "','" + obj.peso_tot + "','" + obj.qnt_prod + "','" + nome_comanda + "',\n\
                                        '" + obj.posizione + "','" + obj.fiscale_sospeso + "','" + obj.BIS + "','" + comanda.nome_pc + "','" + obj.giorno + "','" + obj.cod_promo + "','" + obj.qta_fissa + "','" + obj.spazio_forno + "','" + obj.tipo_impasto + "','" + obj.tasto_segue + "','" + obj.NCARD2 + "','" + obj.NCARD3 + "','" + obj.NCARD4 + "','" + obj.NEXIT + "','" + obj.droppay + "',\n\
                                        '" + obj.dest_stampa_2 + "','" + obj.id_pony + "','" + obj.prezzo_varianti_aggiuntive + "','" + obj.prezzo_varianti_maxi + "','" + obj.prezzo_maxi_prima + "','" + obj.prezzo_vero + "','" + obj.riepilogo + "','" + obj.data_servizio + "','" + obj.data_comanda + "','" + obj.ora_comanda + "','" + obj.numero_paganti + "','" + obj.numero_servizio + "',\n\
                                        '" + obj.imponibile_rep_1 + "','" + obj.imponibile_rep_2 + "','" + obj.imponibile_rep_3 + "','" + obj.imponibile_rep_4 + "','" + obj.imponibile_rep_5 + "','" + obj.imposta_rep_1 + "','" + obj.imposta_rep_2 + "','" + obj.imposta_rep_3 + "','" + obj.imposta_rep_4 + "','" + obj.imposta_rep_5 + "','" + obj.totale_scontato_ivato + "','" + obj.prezzo_maxi + "','" + obj.misuratore_riferimento + "','" + obj.operatore_cancellazione + "','" + obj.TID + "','" + obj.tipologia_fattorino + "','" + obj.AGG_GIAC + "','" + obj.orario_preparazione + "','" + obj.reparto + "','" + obj.importo_totale_fiscale + "','" + obj.prog_gg_uni + "');";
                    console.log(spostamento);
                    comanda.sock.send({tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: spostamento, terminale: comanda.terminale, ip: comanda.ip_address});
                    comanda.sincro.query(spostamento, function () {
                    });
                });
                var update_tavolo = "UPDATE tavoli SET colore='0',ora_apertura_tavolo='" + ora_apertura_tavolo + "',ora_ultima_comanda='" + ora_ultima_comanda + "' WHERE numero='" + tavolo_destinazione + "';";
                comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: update_tavolo, terminale: comanda.terminale, ip: comanda.ip_address});
                comanda.sincro.query(update_tavolo, function () {

                    update_tavolo = "UPDATE tavoli SET colore='0',ora_apertura_tavolo='-',ora_ultima_comanda='-' WHERE numero='" + tavolo_mittente + "';";
                    comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: update_tavolo, terminale: comanda.terminale, ip: comanda.ip_address});
                    comanda.sincro.query(update_tavolo, function () {

                        var eliminazione_vecchio = "DELETE FROM comanda WHERE ntav_comanda='" + tavolo_mittente + "' and stato_record='ATTIVO';";
                        comanda.sock.send({tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: eliminazione_vecchio, terminale: comanda.terminale, ip: comanda.ip_address});
                        comanda.sincro.query(eliminazione_vecchio, function () {

                            disegna_ultimo_tavolo_modificato(update_tavolo);
                            comanda.funzionidb.conto_attivo();
                            $('#popup_sposta_articolo--input_text--numero_tavolo_a').val('');
                            $('#popup_sposta_articolo').modal('hide');
                            $('#popup_unisci_parcheggio').modal('hide');
                            //ORIGINALI

                            //SE E' IL BAR
                            $('#popup_sposta_parcheggio').modal('hide');
                            $('#popup_sposta_parcheggio input').val('');
                            $('#popup_sposta_tavoli').modal('hide');
                            $('#popup_sposta_tavoli input').val('');
                            if (comanda.tavolo !== "BANCO" && comanda.tavolo !== "BAR" && comanda.tavolo.left(7) !== "ASPORTO" && comanda.tavolo !== "TAKE AWAY") {
                                btn_tavoli();
                                comanda.tavolo = "0";
                            } else
                            {
                                if (comanda.tavolo === 'BAR')
                                {
                                    btn_tavoli();
                                }
                                comanda.parcheggio = '';

                                $('.nome_parcheggio').html(comanda.parcheggio);
                            }

                        });
                    });
                });
            });
        });
    }
}

function unisci_parcheggio_BAR(id, nome_comanda) {
    var da = comanda.parcheggio;
    var id_comanda = id;
    var nome_comanda = nome_comanda;
    var ora_apertura_tavolo = comanda.funzionidb.data_attuale().substr(9, 8);
    var ora_ultima_comanda = comanda.funzionidb.data_attuale().substr(9, 8);
    var data = comanda.funzionidb.data_attuale();
    var builder = new epson.ePOSBuilder();
    builder.addTextFont(builder.FONT_A);
    builder.addTextAlign(builder.ALIGN_CENTER);
    builder.addFeedLine(2);
    builder.addTextSize(2, 2);
    builder.addText('UNIONE PARCHEGGI\n');
    builder.addFeedLine(1);
    builder.addText(data + '\n');
    builder.addFeedLine(1);
    builder.addText('DA PARK.' + da + ' A PARK.' + nome_comanda + '\n');
    builder.addFeedLine(1);
    builder.addFeedLine(2);
    builder.addCut(builder.CUT_FEED);
    var request = builder.toString();
    //CONTENUTO CONTO
    //console.log(request);

    var url = "";

    //Create a SOAP envelop
    var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
    var query_stampa_sn = "SELECT Field390,Field398 FROM settaggi_profili where id=" + comanda.folder_number + " limit 1;";
    var r = alasql(query_stampa_sn);
    var lista_ip_gia_usati = new Array();

    if (r[0] !== undefined && r[0].Field398 !== "" && r[0].Field398 !== "TUTTE" && lista_ip_gia_usati.indexOf(comanda.nome_stampante[r[0].Field398].ip) === -1) {

        url = 'http://' + comanda.nome_stampante[r[0].Field398].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';


        if (r[0] !== undefined && r[0].Field390 === "true") {
            //Create an XMLHttpRequest object
            var xhr = new XMLHttpRequest();
            //Open an XMLHttpRequest object
            xhr.open('POST', url, true);
            //<Header settings>
            xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
            xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
            xhr.setRequestHeader('SOAPAction', '""');
            // Send print document
            lista_ip_gia_usati.push(comanda.nome_stampante[r[0].Field398].ip);
            xhr.send(soap);
        }
    } else if (r[0] !== undefined && r[0].Field398 !== "" && r[0].Field398 === "TUTTE") {

        comanda.nome_stampante.forEach(function (e, i) {

            if (lista_ip_gia_usati.indexOf(comanda.nome_stampante[i].ip) === -1) {
                url = 'http://' + comanda.nome_stampante[i].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                if (r[0] !== undefined && r[0].Field390 === "true") {
                    //Create an XMLHttpRequest object
                    var xhr = new XMLHttpRequest();
                    //Open an XMLHttpRequest object
                    xhr.open('POST', url, true);
                    //<Header settings>
                    xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
                    xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
                    xhr.setRequestHeader('SOAPAction', '""');
                    // Send print document
                    lista_ip_gia_usati.push(comanda.nome_stampante[i].ip);
                    xhr.send(soap);
                }
            }
        });
    }
    comanda.tavolo = "0";
    var tavolo_mittente = da;
    var tavolo_destinazione = a;
    var ultimo_nodo_utile = '000';
    var nodo_finale = '000';
    var nodo_principale = '000';
    var nodo_variante = '000';
    var progressivo = '0';
    var a = 'BAR';
    var ultimo_nodo_destinatario = "SELECT id,ntav_comanda,substr(nodo,1,3) as nodo, prog_inser FROM comanda WHERE nome_comanda='" + nome_comanda + "' order by nodo DESC; ";
    //console.log(ultimo_nodo_destinatario);
    comanda.sincro.query(ultimo_nodo_destinatario, function (result_dest) {
        if (result_dest[0] !== undefined && result_dest[0].nodo !== undefined) {
            nodo_principale = result_dest[0].nodo;
        }
        if (result_dest[0] !== undefined && result_dest[0].prog_inser !== undefined) {
            progressivo = result_dest[0].prog_inser;
        }

        if (result_dest[0] !== undefined && result_dest[0].ntav_comanda !== undefined) {
            a = result_dest[0].ntav_comanda;
        }

        var ultimo_nodo_mittente = "SELECT * FROM comanda WHERE nome_comanda='" + tavolo_mittente + "' and stato_record='ATTIVO' ORDER BY nodo ASC;";
        //console.log(ultimo_nodo_mittente);
        comanda.sincro.query(ultimo_nodo_mittente, function (result_mitt) {
            result_mitt.forEach(function (obj) {

                if (obj.numero_paganti === "null") {
                    obj.numero_paganti = "";
                }

                progressivo = (parseInt(progressivo) + 1).toString();
                if (obj.desc_art[0] !== "+" && obj.desc_art[0] !== "-" && obj.desc_art[0] !== "(" && obj.desc_art[0] !== "*" && obj.desc_art[0] !== "x" && obj.desc_art[0] !== "=") {
                    console.log("originario", nodo_principale);
                    nodo_finale = parseInt(nodo_principale) + 1;
                    nodo_principale = nodo_finale;
                    console.log("piu uno", nodo_finale);
                    nodo_finale = aggZero(nodo_finale, 3);
                    console.log("ascii", nodo_finale);
                    ultimo_nodo_utile = nodo_finale;
                } else
                {

                    nodo_finale = aggZero(progressivo, 3);
                    nodo_finale = ultimo_nodo_utile + ' ' + aggZero(progressivo, 3);
                }

                var spostamento = "INSERT INTO comanda\n\
                                        VALUES \n\
                                        ('','" + id_comanda + "','','','','','','','','','','','','','','','','','','','','','" + obj.progressivo_fiscale + "','" + obj.ordinamento + "','" + obj.ultima_portata + "','" + obj.cod_articolo + "','" + obj.numero_conto + "','" + obj.contiene_variante + "','" + obj.dest_stampa + "','" + obj.cat_variante + "','" + obj.tipo_record + "',\n\
                                        'APERTO','" + obj.planning_colori + "','" + obj.data_fisc + "','" + obj.ora_fisc + "','" + a + "',\n\
                                        '" + obj.ntav_attrib + "','" + obj.tab_bis + "','" + progressivo + "','" + obj.art_primario + "','" + obj.art_variante + "','" + obj.tipo_variante + "','" + obj.desc_art + "','" + obj.quantita + "','" + obj.prezzo_un + "','" + obj.sconto_perc + "','" + obj.sconto_imp + "',\n\
                                        '" + obj.netto + "','" + obj.imp_tot_netto + "','" + obj.perc_iva + "','" + obj.costo + "','" + obj.autor_sconto + "','" + obj.categoria + "','" + obj.gruppo + "','" + obj.incassato + "','" + obj.residuo + "','" + obj.tipo_incasso + "','" + obj.contanti + "','" + obj.carta_credito + "','" + obj.bancomat + "',\n\
                                        '" + obj.assegni + "','" + obj.tessera_prepagata + "','" + obj.numero_tessera_prepag + "','" + obj.stampata_sn + "','" + obj.data_stampa + "','" + obj.ora_stampa + "','" + obj.fiscalizzata_sn + "','" + obj.data_ + "','" + obj.ora_ + "','" + obj.n_fiscale + "','" + obj.n_conto_parziale + "',\n\
                                        '" + nodo_finale + "','" + obj.portata + "','" + obj.ult_portata + "','" + obj.centro + "','" + obj.terminale + "','" + obj.operatore + "','" + obj.cod_cliente + "','" + obj.rag_soc_cliente + "','" + obj.coef_trasf + "','" + obj.peso_x_um + "','" + obj.peso_ums + "','" + obj.peso_tot + "','" + obj.qnt_prod + "','" + nome_comanda + "',\n\
                                        '" + obj.posizione + "','" + obj.fiscale_sospeso + "','" + obj.BIS + "','" + comanda.nome_pc + "','" + obj.giorno + "','" + obj.cod_promo + "','" + obj.qta_fissa + "','" + obj.spazio_forno + "','" + obj.tipo_impasto + "','" + obj.tasto_segue + "','" + obj.NCARD2 + "','" + obj.NCARD3 + "','" + obj.NCARD4 + "','" + obj.NEXIT + "','" + obj.droppay + "',\n\
                                        '" + obj.dest_stampa_2 + "','" + obj.id_pony + "','" + obj.prezzo_varianti_aggiuntive + "','" + obj.prezzo_varianti_maxi + "','" + obj.prezzo_maxi_prima + "','" + obj.prezzo_vero + "','" + obj.riepilogo + "','" + obj.data_servizio + "','" + obj.data_comanda + "','" + obj.ora_comanda + "','" + obj.numero_paganti + "','" + obj.numero_servizio + "',\n\
                                        '" + obj.imponibile_rep_1 + "','" + obj.imponibile_rep_2 + "','" + obj.imponibile_rep_3 + "','" + obj.imponibile_rep_4 + "','" + obj.imponibile_rep_5 + "','" + obj.imposta_rep_1 + "','" + obj.imposta_rep_2 + "','" + obj.imposta_rep_3 + "','" + obj.imposta_rep_4 + "','" + obj.imposta_rep_5 + "','" + obj.totale_scontato_ivato + "','" + obj.prezzo_maxi + "','" + obj.misuratore_riferimento + "','" + obj.operatore_cancellazione + "','" + obj.TID + "','" + obj.tipologia_fattorino + "','" + obj.AGG_GIAC + "','" + obj.orario_preparazione + "','" + obj.reparto + "','" + obj.importo_totale_fiscale + "','" + obj.prog_gg_uni + "');";
                console.log(spostamento);
                comanda.sock.send({tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: spostamento, terminale: comanda.terminale, ip: comanda.ip_address});
                comanda.sincro.query(spostamento, function () {
                });
            });
            var update_tavolo = "UPDATE tavoli SET colore='0',ora_apertura_tavolo='" + ora_apertura_tavolo + "',ora_ultima_comanda='" + ora_ultima_comanda + "' WHERE numero='" + a + "';";
            comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: update_tavolo, terminale: comanda.terminale, ip: comanda.ip_address});
            comanda.sincro.query(update_tavolo, function () {

                update_tavolo = "UPDATE tavoli SET colore='0',ora_apertura_tavolo='-',ora_ultima_comanda='-' WHERE numero='" + a + "';";
                comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: update_tavolo, terminale: comanda.terminale, ip: comanda.ip_address});
                comanda.sincro.query(update_tavolo, function () {

                    var eliminazione_vecchio = "DELETE FROM comanda WHERE nome_comanda='" + tavolo_mittente + "' and stato_record='ATTIVO';";
                    comanda.sock.send({tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: eliminazione_vecchio, terminale: comanda.terminale, ip: comanda.ip_address});
                    comanda.sincro.query(eliminazione_vecchio, function () {

                        disegna_ultimo_tavolo_modificato(update_tavolo);
                        comanda.funzionidb.conto_attivo();
                        $('#popup_sposta_articolo--input_text--numero_tavolo_a').val('');
                        $('#popup_sposta_articolo').modal('hide');
                        $('#popup_unisci_parcheggio').modal('hide');
                        //ORIGINALI
                        $('#popup_sposta_tavoli').modal('hide');
                        $('#popup_sposta_tavoli input').val('');
                        btn_tavoli();
                    });
                });
            });
        });
    });
}

function unisci_tavoli() {
    var ora_apertura_tavolo = comanda.funzionidb.data_attuale().substr(9, 8);
    var ora_ultima_comanda = comanda.funzionidb.data_attuale().substr(9, 8);
    var da = $('#popup_unisci_tavoli--input_text--unisci_tavolo_da').val();
    var a = $('#popup_unisci_tavoli--input_text--unisci_tavolo_a').val();
    if (da === undefined || a === undefined || da === '' || a === '') {
        alert("Inserire tavolo di partenza e tavolo di arrivo, per unirli.");
    } else
    {
        var query_verifica = "SELECT * FROM tavoli WHERE numero='" + a + "';";
        comanda.sincro.query(query_verifica, function (risultato_verifica) {

            if (risultato_verifica.length === 0) {
                alert("Il tavolo " + a + " non esiste.");
            } else
            {
                comanda.tavolo = "0";
                var tavolo_mittente = da;
                var tavolo_destinazione = a;
                var data = comanda.funzionidb.data_attuale();




                var builder = new epson.ePOSBuilder();
                builder.addTextFont(builder.FONT_A);
                builder.addTextAlign(builder.ALIGN_CENTER);
                builder.addTextSize(1, 1);
                builder.addText("-----------------------------------------\n");
                builder.addTextSize(2, 2);
                builder.addText('UNIONE TAVOLI\n');
                builder.addTextSize(1, 1);
                builder.addText("-----------------------------------------\n");
                builder.addTextSize(2, 2);
                builder.addFeedLine(1);
                builder.addText('TAVOLO: ' + da + '\nCON TAVOLO: ' + a + '\n');
                builder.addTextSize(1, 1);
                builder.addFeedLine(1);
                builder.addText("-----------------------------------------\n");
                builder.addTextSize(1, 1);
                builder.addText('OPERATORE: ' + comanda.operatore + '\n');
                builder.addText(data.replace(/-/gi, '/') + '\n');
                builder.addFeedLine(1);
                builder.addCut(builder.CUT_FEED);
                var request = builder.toString();
                var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
                //CONTENUTO CONTO
                //console.log(request);



                /*var lista_ip_gia_usati = new Array();
                 for (var i in comanda.nome_stampante) {
                 
                 if (comanda.nome_stampante[i].nome_umano !== undefined && comanda.nome_stampante[i].intelligent === 's' && comanda.nome_stampante[i].fiscale === 'n' && comanda.nome_stampante[i].nome_umano !== 'NESSUNA' && lista_ip_gia_usati.indexOf(comanda.nome_stampante[i].ip) === -1) {
                 lista_ip_gia_usati.push(comanda.nome_stampante[i].ip);
                 //Create a SOAP envelop
                 var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
                 //Create an XMLHttpRequest object
                 var xhr = new XMLHttpRequest();
                 //Set the end point address
                 
                 var url = 'http://' + comanda.nome_stampante[i].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                 //Open an XMLHttpRequest object
                 xhr.open('POST', url, true);
                 //<Header settings>
                 xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
                 xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
                 xhr.setRequestHeader('SOAPAction', '""');
                 // Send print document
                 xhr.send(soap);
                 }
                 }*/


                var query_stampa_sn = "SELECT Field391,Field399 FROM settaggi_profili where id=" + comanda.folder_number + " limit 1;";
                var r = alasql(query_stampa_sn);
                var lista_ip_gia_usati = new Array();

                if (r[0] !== undefined && r[0].Field399 !== "" && r[0].Field399 !== "TUTTE" && lista_ip_gia_usati.indexOf(comanda.nome_stampante[r[0].Field399].ip) === -1) {

                    var url = 'http://' + comanda.nome_stampante[r[0].Field399].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';


                    if (r[0] !== undefined && r[0].Field391 === "true") {
                        //Create an XMLHttpRequest object
                        var xhr = new XMLHttpRequest();
                        //Open an XMLHttpRequest object
                        xhr.open('POST', url, true);
                        //<Header settings>
                        xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
                        xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
                        xhr.setRequestHeader('SOAPAction', '""');
                        // Send print document
                        lista_ip_gia_usati.push(comanda.nome_stampante[r[0].Field399].ip);
                        xhr.send(soap);
                    }
                } else if (r[0] !== undefined && r[0].Field399 !== "" && r[0].Field399 === "TUTTE") {

                    comanda.nome_stampante.forEach(function (e, i) {

                        if (lista_ip_gia_usati.indexOf(comanda.nome_stampante[i].ip) === -1) {
                            var url = 'http://' + comanda.nome_stampante[i].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                            if (r[0] !== undefined && r[0].Field391 === "true") {
                                //Create an XMLHttpRequest object
                                var xhr = new XMLHttpRequest();
                                //Open an XMLHttpRequest object
                                xhr.open('POST', url, true);
                                //<Header settings>
                                xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
                                xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
                                xhr.setRequestHeader('SOAPAction', '""');
                                // Send print document
                                lista_ip_gia_usati.push(comanda.nome_stampante[i].ip);
                                xhr.send(soap);
                            }
                        }
                    });
                }

                var ultimo_nodo_utile = '000';
                var nodo_finale = '000';
                var nodo_principale = '000';
                var nodo_variante = '000';
                var progressivo = '0';
                
                var ultimo_nodo_destinatario = "SELECT id,substr(nodo,1,3) as nodo, prog_inser FROM comanda WHERE stato_record='ATTIVO' and ntav_comanda='" + tavolo_destinazione + "' order by nodo DESC; ";
                comanda.sincro.query(ultimo_nodo_destinatario, function (result_dest) {
                    if (result_dest[0] !== undefined && result_dest[0].nodo !== undefined) {
                        nodo_principale = result_dest[0].nodo;
                    }
                    if (result_dest[0] !== undefined && result_dest[0].prog_inser !== undefined) {
                        progressivo = result_dest[0].prog_inser;
                    }

                    var ultimo_nodo_mittente = "SELECT * FROM comanda WHERE ntav_comanda='" + tavolo_mittente + "' and stato_record='ATTIVO' ORDER BY nodo ASC;";
                    //console.log(ultimo_nodo_mittente);
                    comanda.sincro.query(ultimo_nodo_mittente, function (result_mitt) {
                        result_mitt.forEach(function (obj) {

                            if (obj.numero_paganti === "null") {
                                obj.numero_paganti = "";
                            }

                            progressivo = (parseInt(progressivo) + 1).toString();
                            if (obj.desc_art[0] !== "+" && obj.desc_art[0] !== "-" && obj.desc_art[0] !== "(" && obj.desc_art[0] !== "*" && obj.desc_art[0] !== "x" && obj.desc_art[0] !== "=") {
                                console.log("originario", nodo_principale);
                                nodo_finale = parseInt(nodo_principale) + 1;
                                nodo_principale = nodo_finale;
                                console.log("piu uno", nodo_finale);
                                nodo_finale = aggZero(nodo_finale, 3);
                                console.log("ascii", nodo_finale);
                                ultimo_nodo_utile = nodo_finale;
                            } else
                            {

                                nodo_finale = aggZero(progressivo, 3);
                                nodo_finale = ultimo_nodo_utile + ' ' + aggZero(progressivo, 3);
                            }

                            var spostamento = "INSERT INTO comanda \n\
                                        VALUES \n\
                                        ('','" + obj.id + "','','','','','','','','','','','','','','','','','','','','','" + obj.progressivo_fiscale + "','" + obj.ordinamento + "','" + obj.ultima_portata + "','" + obj.cod_articolo + "','" + obj.numero_conto + "','" + obj.contiene_variante + "','" + obj.dest_stampa + "','" + obj.cat_variante + "','" + obj.tipo_record + "',\n\
                                        '" + obj.stato_record + "','" + obj.planning_colori + "','" + obj.data_fisc + "','" + obj.ora_fisc + "','" + tavolo_destinazione + "',\n\
                                        '" + obj.ntav_attrib + "','" + obj.tab_bis + "','" + progressivo + "','" + obj.art_primario + "','" + obj.art_variante + "','" + obj.tipo_variante + "','" + obj.desc_art + "','" + obj.quantita + "','" + obj.prezzo_un + "','" + obj.sconto_perc + "','" + obj.sconto_imp + "',\n\
                                        '" + obj.netto + "','" + obj.imp_tot_netto + "','" + obj.perc_iva + "','" + obj.costo + "','" + obj.autor_sconto + "','" + obj.categoria + "','" + obj.gruppo + "','" + obj.incassato + "','" + obj.residuo + "','" + obj.tipo_incasso + "','" + obj.contanti + "','" + obj.carta_credito + "','" + obj.bancomat + "',\n\
                                        '" + obj.assegni + "','" + obj.tessera_prepagata + "','" + obj.numero_tessera_prepag + "','" + obj.stampata_sn + "','" + obj.data_stampa + "','" + obj.ora_stampa + "','" + obj.fiscalizzata_sn + "','" + obj.data_ + "','" + obj.ora_ + "','" + obj.n_fiscale + "','" + obj.n_conto_parziale + "',\n\
                                        '" + nodo_finale + "','" + obj.portata + "','" + obj.ult_portata + "','" + obj.centro + "','" + obj.terminale + "','" + obj.operatore + "','" + obj.cod_cliente + "','" + obj.rag_soc_cliente + "','" + obj.coef_trasf + "','" + obj.peso_x_um + "','" + obj.peso_ums + "','" + obj.peso_tot + "','" + obj.qnt_prod + "',\n\
                                        '" + obj.nome_comanda + "','" + obj.posizione + "','" + obj.fiscale_sospeso + "','" + obj.BIS + "','" + comanda.nome_pc + "','" + obj.giorno + "','" + obj.cod_promo + "','" + obj.qta_fissa + "','" + obj.spazio_forno + "','" + obj.tipo_impasto + "','" + obj.tasto_segue + "','" + obj.NCARD2 + "','" + obj.NCARD3 + "',\n\
                                        '" + obj.NCARD4 + "','" + obj.NEXIT + "','" + obj.droppay + "','" + obj.dest_stampa_2 + "','" + obj.id_pony + "','" + obj.prezzo_varianti_aggiuntive + "','" + obj.prezzo_varianti_maxi + "','" + obj.prezzo_maxi_prima + "','" + obj.prezzo_vero + "','" + obj.riepilogo + "','" + obj.data_servizio + "','" + obj.data_comanda + "','" + obj.ora_comanda + "','" + obj.numero_paganti + "','" + obj.numero_servizio + "',\n\
                                        '" + obj.imponibile_rep_1 + "','" + obj.imponibile_rep_2 + "','" + obj.imponibile_rep_3 + "','" + obj.imponibile_rep_4 + "','" + obj.imponibile_rep_5 + "','" + obj.imposta_rep_1 + "','" + obj.imposta_rep_2 + "','" + obj.imposta_rep_3 + "','" + obj.imposta_rep_4 + "','" + obj.imposta_rep_5 + "','" + obj.totale_scontato_ivato + "','" + obj.prezzo_maxi + "','" + obj.misuratore_riferimento + "','" + obj.operatore_cancellazione + "','" + obj.TID + "','" + obj.tipologia_fattorino + "','" + obj.AGG_GIAC + "','" + obj.orario_preparazione + "','" + obj.reparto + "','" + obj.importo_totale_fiscale + "','" + obj.prog_gg_uni + "');";
                            
                            console.log(spostamento);
                            comanda.sock.send({tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: spostamento, terminale: comanda.terminale, ip: comanda.ip_address});
                            comanda.sincro.query(spostamento, function () {
                            });
                        });
                        
                        var update_tavolo = "UPDATE tavoli SET colore='0',ora_apertura_tavolo='" + ora_apertura_tavolo + "',ora_ultima_comanda='" + ora_ultima_comanda + "' WHERE numero='" + tavolo_destinazione + "';";
                        comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: update_tavolo, terminale: comanda.terminale, ip: comanda.ip_address});
                        comanda.sincro.query(update_tavolo, function () {
                            disegna_ultimo_tavolo_modificato(update_tavolo);
                            update_tavolo = "UPDATE tavoli SET colore='0',ora_apertura_tavolo='-',ora_ultima_comanda='-' WHERE numero='" + tavolo_mittente + "';";
                            comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: update_tavolo, terminale: comanda.terminale, ip: comanda.ip_address});
                            comanda.sincro.query(update_tavolo, function () {
                                disegna_ultimo_tavolo_modificato(update_tavolo);
                                var eliminazione_vecchio = "DELETE FROM comanda WHERE ntav_comanda='" + tavolo_mittente + "' and stato_record='ATTIVO';";
                                comanda.sock.send({tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: eliminazione_vecchio, terminale: comanda.terminale, ip: comanda.ip_address});
                                comanda.sincro.query(eliminazione_vecchio, function () {


                                    var numero = tavolo_mittente;
                                    var paga_lui = "SELECT fila_tavoli FROM tavoli_uniti;";
                                    var r = alasql(paga_lui);


                                    var fila_tavoli_presente = new Array();


                                    r.some(
                                            v => {
                                                console.log(v.fila_tavoli);

                                                var b = v.fila_tavoli.split(";").some(
                                                        v1 => {

                                                            return v1 === numero;


                                                        });

                                                if (b === true) {
                                                    fila_tavoli_presente = v.fila_tavoli.split(";");
                                                    return v.fila_tavoli;
                                                }

                                            });

                                    var tp = fila_tavoli_presente;
                                    if (fila_tavoli_presente.length !== 0)
                                    {

                                        var tp_ind = tp.indexOf(numero);
                                        tp.splice(tp_ind, 1);
                                    }

                                    numero = tavolo_destinazione;

                                    r.some(
                                            v => {
                                                console.log(v.fila_tavoli);

                                                var b = v.fila_tavoli.split(";").some(
                                                        v1 => {

                                                            return v1 === numero;


                                                        });

                                                if (b !== true) {
                                                    tp.push(numero);
                                                    return v.fila_tavoli;
                                                }

                                            });


                                    if (r[0] !== undefined) {
                                        var testo_query = "UPDATE tavoli_uniti SET fila_tavoli='" + tp.join(";") + "' WHERE fila_tavoli='" + r[0].fila_tavoli + "'; ";
                                        comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                                        alasql(testo_query);
                                    }

                                    testo_query = "DELETE FROM tavoli_uniti WHERE fila_tavoli=''; ";
                                    comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                                    alasql(testo_query);


                                    disegna_tavoli_salvati();





                                    comanda.funzionidb.conto_attivo();
                                    $('#popup_sposta_articolo--input_text--numero_tavolo_a').val('');
                                    $('#popup_sposta_articolo').modal('hide');
                                    //ORIGINALI
                                    $('#popup_sposta_tavoli').modal('hide');
                                    $('#popup_sposta_tavoli input').val('');
                                    btn_tavoli();
                                });
                            });
                        });
                    });
                });
            }
        });
    }
}


var cancella_tavolo = function () {

    var numero = $('#popup_cancella_tavolo--btn--numero_tavolo').val();
    //var r = confirm("Sei sicuro di voler cancellare il tavolo " + numero + "?"); //Sei sicuro di voler cancellare il conto?
    //if (r === true) {

    if (comanda.compatibile_xml === true) {

        var nome_file = "/TAVOLO" + aggZero(numero, 3) + ".XML";
        comanda.xml.elimina_file_xml("_" + comanda.folder_number + nome_file, false, function () {
            comanda.xml.elimina_tavolo_occupato("_" + comanda.folder_number + "/COND/Tav" + numero + ".CO1", false, function () {
                var nome_file = "/PLANING/P" + aggZero(numero, 3);
                comanda.xml.elimina_file_xml("_" + comanda.folder_number + nome_file, true, function () {
                    //disegna_tavoli_salvati();
                });
            });
        });
    }

    var data = comanda.funzionidb.data_attuale().substr(0, 8);
    var ora = comanda.funzionidb.data_attuale().substr(9, 8).replace(/-/gi, '/');

    $('tr[id^="art_"]').remove();
    var testo_query = "UPDATE comanda SET operatore_cancellazione='" + comanda.operatore + "',stato_record='TAVOLO CANCELLATO " + data + " " + ora + "' WHERE (stato_record='ATTIVO' OR stato_record='APERTO') AND ntav_comanda='" + numero + "' AND (stampata_sn is null OR stampata_sn = 'null' OR stampata_sn = 'N');";
    console.log(testo_query);
    comanda.sock.send({tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
    comanda.sincro.query(testo_query, function () {

        testo_query = "UPDATE comanda SET operatore_cancellazione='" + comanda.operatore + "',stato_record='TAVOLO CANCELLATO " + data + " " + ora + "' WHERE (stato_record='ATTIVO' OR stato_record='APERTO') AND ntav_comanda='" + numero + "' AND stampata_sn = 'S';";
        console.log(testo_query);
        comanda.sock.send({tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
        comanda.sincro.query(testo_query, function () {

            testo_query = "UPDATE tavoli SET colore = '0',ora_apertura_tavolo='-',ora_ultima_comanda='-' WHERE numero='" + numero + "'; ";
            comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
            comanda.sincro.query(testo_query, function () {


                var paga_lui = "SELECT fila_tavoli FROM tavoli_uniti;";
                var r = alasql(paga_lui);


                var fila_tavoli_presente = new Array();


                r.some(
                        v => {
                            console.log(v.fila_tavoli);

                            var b = v.fila_tavoli.split(";").some(
                                    v1 => {

                                        return v1 === numero;


                                    });

                            if (b === true) {
                                fila_tavoli_presente = v.fila_tavoli.split(";");
                                return v.fila_tavoli;
                            }

                        });

                var tp = fila_tavoli_presente;
                if (fila_tavoli_presente.length !== 0)
                {

                    var tp_ind = tp.indexOf(numero);
                    tp.splice(tp_ind, 1);

                    if (tp.length === 1) {
                        tp = new Array();
                    }

                }

                if (r[0] !== undefined) {
                    var testo_query = "UPDATE tavoli_uniti SET fila_tavoli='" + tp.join(";") + "' WHERE fila_tavoli='" + r[0].fila_tavoli + "'; ";
                    comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                    alasql(testo_query);
                }

                testo_query = "DELETE FROM tavoli_uniti WHERE fila_tavoli=''; ";
                comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                alasql(testo_query);


                disegna_tavoli_salvati();

                $('#popup_sposta_tavoli').modal('hide');
                $('#popup_sposta_tavoli input').val('');
            });
        });
    });
    //}
};


var scollega_tavolo = function () {

    var numero = $('#popup_scollega_tavolo--btn--numero_tavolo').val();

    var testo_query = "UPDATE tavoli SET collegamento='',colore = '0',ora_apertura_tavolo='-',ora_ultima_comanda='-' WHERE numero='" + numero + "'; ";
    comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
    comanda.sincro.query(testo_query, function () {

        disegna_tavoli_salvati();
        $('#popup_sposta_tavoli').modal('hide');
        $('#popup_sposta_tavoli input').val('');

    });

};

var annulla_unione_pagamento = function (n) {

    var numero = $('#popup_annulla_unione_pagamento--btn--numero_tavolo').val();
    if (n !== undefined) {
        numero = n;
    }

    var paga_lui = "SELECT fila_tavoli FROM tavoli_uniti;";
    var r = alasql(paga_lui);


    var fila_tavoli_presente = false;


    r.some(
            v => {
                console.log(v.fila_tavoli);

                var b = v.fila_tavoli.split(";").some(
                        v1 => {

                            return v1 === numero;


                        });

                if (b === true) {
                    fila_tavoli_presente = v.fila_tavoli;
                    return v.fila_tavoli;
                }

            });

    if (fila_tavoli_presente !== false)
    {
        var tp = fila_tavoli_presente.split(";");
        var tp_ind = tp.indexOf(numero);
        tp.splice(tp_ind, 1);


        var testo_query = "UPDATE tavoli_uniti SET fila_tavoli='" + tp.join(";") + "' WHERE fila_tavoli='" + fila_tavoli_presente + "'; ";
        comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
        comanda.sincro.query(testo_query, function () {

            testo_query = "DELETE FROM tavoli_uniti WHERE fila_tavoli NOT LIKE '%;%'; ";
            comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
            comanda.sincro.query(testo_query, function () {


                disegna_tavoli_salvati();


            });
        });
    }

    $('#popup_sposta_tavoli').modal('hide');
    $('#popup_sposta_tavoli input').val('');

};


function sposta_tavolo() {

    var da = $('#popup_sposta_tavoli--input_text--sposta_tavolo_da').val();
    var a = $('#popup_sposta_tavoli--input_text--sposta_tavolo_a').val();
    if (comanda.compatibile_xml === true)
    {
        scambia_xml_tavolo(da, a, comanda.operatore, function () {
            setTimeout(function () {
                comanda.tavolo = "0";
                $('#popup_sposta_tavoli').modal('hide');
                $('#popup_sposta_tavoli input').val('');
                disegna_tavoli_salvati();
            }, 1500);
        });
    } else
    {
        var testo_query = '';
        if (da === undefined || a === undefined || da === '' || a === '') {
            alert("Devi inserire entrambi i numeri dei tavoli, se li vuoi spostare.");
        } else
        {
            var query_verifica = "SELECT * FROM tavoli WHERE numero='" + a + "';";
            comanda.sincro.query(query_verifica, function (risultato_verifica) {

                if (risultato_verifica.length === 0) {
                    alert("Il tavolo " + a + " non esiste.");
                } else
                {



                    var data = comanda.funzionidb.data_attuale();
                    var builder = new epson.ePOSBuilder();
                    builder.addTextFont(builder.FONT_A);
                    builder.addTextAlign(builder.ALIGN_CENTER);
                    builder.addTextSize(1, 1);
                    builder.addText("-----------------------------------------\n");
                    builder.addTextSize(2, 2);
                    builder.addText('SCAMBIO TAVOLO\n');
                    builder.addTextSize(1, 1);
                    builder.addText("-----------------------------------------\n");
                    builder.addFeedLine(1);
                    builder.addTextSize(2, 2);
                    builder.addText('TAVOLO: ' + da + '\nCON TAVOLO: ' + a + '\n');
                    builder.addFeedLine(1);
                    builder.addTextSize(1, 1);
                    builder.addText("-----------------------------------------\n");
                    builder.addText(comanda.lang[109] + ': ' + comanda.operatore + '\n');
                    builder.addText(data.replace(/-/gi, '/') + '\n');
                    builder.addFeedLine(1);
                    builder.addCut(builder.CUT_FEED);
                    var request = builder.toString();
                    var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
                    //SOSTITUITO 04/11/2016

                    var query_stampa_sn = "SELECT Field390,Field398 FROM settaggi_profili where id=" + comanda.folder_number + " limit 1;";
                    var r = alasql(query_stampa_sn);
                    var lista_ip_gia_usati = new Array();

                    if (r[0] !== undefined && r[0].Field398 !== "" && r[0].Field398 !== "TUTTE" && lista_ip_gia_usati.indexOf(comanda.nome_stampante[r[0].Field398].ip) === -1) {

                        var url = 'http://' + comanda.nome_stampante[r[0].Field398].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';


                        if (r[0] !== undefined && r[0].Field390 === "true") {
                            //Create an XMLHttpRequest object
                            var xhr = new XMLHttpRequest();
                            //Open an XMLHttpRequest object
                            xhr.open('POST', url, true);
                            //<Header settings>
                            xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
                            xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
                            xhr.setRequestHeader('SOAPAction', '""');
                            // Send print document
                            lista_ip_gia_usati.push(comanda.nome_stampante[r[0].Field398].ip);
                            xhr.send(soap);
                        }
                    } else if (r[0] !== undefined && r[0].Field398 !== "" && r[0].Field398 === "TUTTE") {

                        comanda.nome_stampante.forEach(function (e, i) {

                            if (lista_ip_gia_usati.indexOf(comanda.nome_stampante[i].ip) === -1) {
                                var url = 'http://' + comanda.nome_stampante[i].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                                if (r[0] !== undefined && r[0].Field390 === "true") {
                                    //Create an XMLHttpRequest object
                                    var xhr = new XMLHttpRequest();
                                    //Open an XMLHttpRequest object
                                    xhr.open('POST', url, true);
                                    //<Header settings>
                                    xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
                                    xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
                                    xhr.setRequestHeader('SOAPAction', '""');
                                    // Send print document
                                    lista_ip_gia_usati.push(comanda.nome_stampante[i].ip);
                                    xhr.send(soap);
                                }
                            }
                        });
                    }


                    /* var query_stampa_sn = "SELECT Field390 FROM settaggi_profili where id=" + comanda.folder_number + " limit 1;";
                     var r = alasql(query_stampa_sn);
                     
                     if (r[0] !== undefined && r[0].Field390 === "true") {
                     
                     var lista_ip_gia_usati = new Array();
                     for (var i in comanda.nome_stampante) {
                     
                     if (comanda.nome_stampante[i].nome_umano !== undefined && comanda.nome_stampante[i].intelligent === 's' && comanda.nome_stampante[i].fiscale === 'n' && comanda.nome_stampante[i].nome_umano !== 'NESSUNA' && lista_ip_gia_usati.indexOf(comanda.nome_stampante[i].ip) === -1) {
                     lista_ip_gia_usati.push(comanda.nome_stampante[i].ip);
                     //Create a SOAP envelop
                     var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
                     //Create an XMLHttpRequest object
                     var xhr = new XMLHttpRequest();
                     //Set the end point address
                     
                     var url = 'http://' + comanda.nome_stampante[i].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                     //Open an XMLHttpRequest object
                     xhr.open('POST', url, true);
                     //<Header settings>
                     xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
                     xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
                     xhr.setRequestHeader('SOAPAction', '""');
                     // Send print document
                     xhr.send(soap);
                     }
                     }
                     }*/
                    comanda.tavolo = "0";
                    testo_query = "UPDATE comanda SET ntav_comanda = 'temp_" + da + "'  WHERE stato_record='ATTIVO' and ntav_comanda='" + da + "';";
                    comanda.sock.send({tipo: "aggiornamento_tavoli", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                    comanda.sincro.query(testo_query, function () {

                        testo_query = "UPDATE comanda SET ntav_comanda = '" + da + "'  WHERE stato_record='ATTIVO' and ntav_comanda='" + a + "';";
                        comanda.sock.send({tipo: "aggiornamento_tavoli", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                        comanda.sincro.query(testo_query, function () {

                            testo_query = "UPDATE comanda SET ntav_comanda = '" + a + "'  WHERE stato_record='ATTIVO' and ntav_comanda='temp_" + da + "';";
                            comanda.sock.send({tipo: "aggiornamento_tavoli", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                            comanda.sincro.query(testo_query, function () {


                                testo_query = "SELECT colore,ora_apertura_tavolo,ora_ultima_comanda FROM tavoli WHERE numero='" + a + "';";
                                comanda.sincro.query(testo_query, function (tavolo_a) {

                                    testo_query = "SELECT colore,ora_apertura_tavolo,ora_ultima_comanda FROM tavoli WHERE numero='" + da + "';";
                                    comanda.sincro.query(testo_query, function (tavolo_da) {

                                        testo_query = "UPDATE tavoli SET colore='" + tavolo_da[0].colore + "', occupato='0',ora_apertura_tavolo='" + tavolo_da[0].ora_apertura_tavolo + "',ora_ultima_comanda='" + tavolo_da[0].ora_ultima_comanda + "' WHERE numero='" + a + "' ;";
                                        comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                                        comanda.sincro.query(testo_query, function () {
                                            disegna_ultimo_tavolo_modificato(testo_query);
                                            testo_query = "UPDATE tavoli SET colore='" + tavolo_a[0].colore + "', occupato='0',ora_apertura_tavolo='" + tavolo_a[0].ora_apertura_tavolo + "',ora_ultima_comanda='" + tavolo_a[0].ora_ultima_comanda + "' WHERE numero='" + da + "' ;";
                                            comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                                            comanda.sincro.query(testo_query, function () {

                                                var paga_lui = "SELECT fila_tavoli FROM tavoli_uniti;";
                                                var r = alasql(paga_lui);

                                                if (r[0] !== undefined) {
                                                    var paga_lui = r[0].fila_tavoli.split(";");

                                                    if (paga_lui.indexOf(da) !== -1 && paga_lui.indexOf(a) !== -1) {

                                                    } else if (paga_lui.indexOf(da) !== -1) {
                                                        paga_lui.splice(paga_lui.indexOf(da), 1);
                                                        paga_lui.push(a);
                                                    } else if (paga_lui.indexOf(a) !== -1) {
                                                        paga_lui.splice(paga_lui.indexOf(a), 1);
                                                        paga_lui.push(da);
                                                    }

                                                    if (r[0] !== undefined) {
                                                        var testo_query = "UPDATE tavoli_uniti SET fila_tavoli='" + paga_lui.join(";") + "' WHERE fila_tavoli='" + r[0].fila_tavoli + "'; ";
                                                        comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                                                        alasql(testo_query);
                                                    }

                                                    testo_query = "DELETE FROM tavoli_uniti WHERE fila_tavoli=''; ";
                                                    comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                                                    alasql(testo_query);
                                                }

                                                disegna_tavoli_salvati();

                                                $('#popup_sposta_tavoli').modal('hide');
                                                $('#popup_sposta_tavoli input').val('');
                                                btn_tavoli();
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                }
            });
        }
    }
}