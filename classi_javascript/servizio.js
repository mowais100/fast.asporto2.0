/*se nel campo data_servizio c'è una data inferiore a quella di oggi, sempre nel formato id
 e sono passate le 5 di mattina*/



//STATIC
function Class_Servizio() {
}

Class_Servizio.data_id_comanda = function () {

    var d = new Date();
    var Y = d.getFullYear();
    var M = addZero((d.getMonth() + 1), 2);
    var D = addZero(d.getDate(), 2);
    var h = addZero(d.getHours(), 2);
    var m = addZero(d.getMinutes(), 2);
    var s = addZero(d.getSeconds(), 2);
    var ms = addZero(d.getMilliseconds(), 3);

    var data = Y + '' + M + '' + D;

    return data;

};

Class_Servizio.data_servizio = function () {

    var d = new Date();
    var Y = d.getFullYear();
    var M = addZero((d.getMonth() + 1), 2);
    var D = addZero(d.getDate(), 2);
    var h = addZero(d.getHours(), 2);
    var m = addZero(d.getMinutes(), 2);
    var s = addZero(d.getSeconds(), 2);
    var ms = addZero(d.getMilliseconds(), 3);

    comanda.data_servizio = Y + '-' + M + '-' + D;


};



Class_Servizio.servizio = function (callback) {

    var d = new Date();
    var Y = d.getFullYear();
    var M = addZero((d.getMonth() + 1), 2);
    var D = addZero(d.getDate(), 2);



    var testo_query = "select * from dati_servizio where id='" + comanda.folder_number + "';";

    comanda.sincro.query(testo_query, function (dato_servizio) {

        comanda.ora_servizio = dato_servizio[0].ora_finale_servizio;

        var d = new Date();

        console.log("Servizio select 1");

        console.log("SERVIZIO", parseInt(Class_Servizio.data_id_comanda()), ">", parseInt(dato_servizio[0].data), "&&", parseInt(d.getHours()), ">=", parseInt(dato_servizio[0].ora_finale_servizio));

        console.log(parseInt(Class_Servizio.data_id_comanda()) > parseInt(dato_servizio[0].data) && parseInt(d.getHours()) >= parseInt(dato_servizio[0].ora_finale_servizio));

        if (parseInt(Class_Servizio.data_id_comanda()) > parseInt(dato_servizio[0].data) && parseInt(d.getHours()) >= parseInt(dato_servizio[0].ora_finale_servizio)) {

            Class_Servizio.aggiorna_servizio("Servizio aggiornato", function () {
                callback("Servizio aggiornato");
            });

        } else

        {

            var formattedDate = dato_servizio[0].data.slice(0, 4) + "-" + dato_servizio[0].data.slice(4, 6) + "-" + dato_servizio[0].data.slice(6, 8);
            comanda.data_servizio = formattedDate;
            callback("Servizio mantenuto");

        }

    });



};

Class_Servizio.aggiorna_servizio = function (condizione_precedente, callback) {

    if (condizione_precedente === "Servizio aggiornato") {
        var testo_query = "update tavoli set occupato='0',colore='grey',ora_apertura_tavolo='-',ora_ultima_comanda='-',collegamento='';";

        comanda.sock.send({tipo: "reset_tavoli_vecchi", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});

        if (comanda.query_su_socket === false) {
console.log("ARRAY_DBCENTRALE_MANCANTI.PUSH",testo_query);
            comanda.array_dbcentrale_mancanti.push(testo_query);
            if (comanda.compatibile_xml !== true) {
            log_query_db_centrale(testo_query);}
        }



        comanda.sincro.query(testo_query, function () {

            var testo_query = "DELETE FROM tavoli_uniti;";

            comanda.sock.send({tipo: "reset_tavoli_vecchi_uniti", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});

            if (comanda.query_su_socket === false) {
console.log("ARRAY_DBCENTRALE_MANCANTI.PUSH",testo_query);
                comanda.array_dbcentrale_mancanti.push(testo_query);
                if (comanda.compatibile_xml !== true) {
                log_query_db_centrale(testo_query);}
            }



            comanda.sincro.query(testo_query, function () {

                console.log("Servizio tavoli 1");

                testo_query = "update comanda set stato_record='CANCELLATO DA SERVIZIO' where (stato_record='ATTIVO' OR stato_record='APERTO') and substr(nome_comanda,1,2)!='C_' and cast(substr(id,5,8) as int)<" + parseInt(Class_Servizio.data_id_comanda()) + ";";

                comanda.sock.send({tipo: "cancellazione_comande_vecchie", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});

                //14-12-2016 DEVE FARLO ANCHE SU DB CENTRALE ALTRIMENTI QUANDO RIAVVIO IL PROGRAMMA MI RITROVO IN MERDA
                //PS ERA STATO TOLTO PER QUALCHE MOTIVO
                if (comanda.query_su_socket === false) {
console.log("ARRAY_DBCENTRALE_MANCANTI.PUSH",testo_query);
                    comanda.array_dbcentrale_mancanti.push(testo_query);
                    if (comanda.compatibile_xml !== true) {
                    log_query_db_centrale(testo_query);}
                }

                comanda.sincro.query(testo_query, function () {

                    console.log("Servizio comanda 1");

                    testo_query = "update dati_servizio set data='" + Class_Servizio.data_id_comanda() + "' where id='" + comanda.folder_number + "';";
                    Class_Servizio.data_servizio();

                    comanda.sock.send({tipo: "aggiornamento_data_servizio", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});

                    if (comanda.query_su_socket === false) {
console.log("ARRAY_DBCENTRALE_MANCANTI.PUSH",testo_query);
                        comanda.array_dbcentrale_mancanti.push(testo_query);
                        if (comanda.compatibile_xml !== true) {
                        log_query_db_centrale(testo_query);}
                    }

                    comanda.sincro.query(testo_query, function () {

                        console.log("Servizio dati 1");

                        //Progressivo parte da 1 ogni giorno
                        testo_query = "update progressivo_asporto set numero='1';";

                        comanda.sock.send({tipo: "aggiornamento_progressivo_asporto", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});

                        comanda.sincro.query(testo_query, function () {

                            testo_query = "update progressivo_asporto_domicilio set numero='1';";

                            comanda.sock.send({tipo: "aggiornamento_progressivo_asporto_domicilio", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});

                            comanda.sincro.query(testo_query, function () {


                                callback("Servizio aggiornato");
                            });
                        });

                    });

                });
            });
        });
    } else
    {
        callback("Servizio mantenuto");
    }
};