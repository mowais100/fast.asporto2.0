var access_token = "";
var token_type = "";
var auth_id = "";
var merchant_custom_id = "";
var merchant_custom_label = "";
var intervallo_polling_droppay;
var importo_pagamento = 0;
function eventuale_pagamento_droppay(testa_droppay, callback) {
    if (parseFloat(testa_droppay) > 0) {

        if (comanda.mobile !== true) {
            $("#pagamento_droppay .modal-body .scritte").html("IN ATTESA DI PAGAMENTO...");
            $("#pagamento_droppay .modal-body #cornice_qr_mobile").hide();
            $("#pagamento_droppay").modal("show");
        } else
        {
            $("#pagamento_droppay .modal-body  .scritte").html("IN ATTESA DEL CODICE QR...");
            $("#pagamento_droppay .modal-body #cornice_qr_mobile").hide();
            $("#pagamento_droppay").modal("show");
        }

        importo_pagamento = testa_droppay;
        test_autorizzazione_droppay_auth(callback);

    } else
    {
        callback(true);
    }
}

function  test_autorizzazione_droppay_auth(callback) {

    try {
        clearInterval(intervallo_polling_droppay);
    } catch (e)
    {
    }

    var data = JSON.stringify({
        "grant_type": "authorization_code",
        "client_id": "7bda978fd1124a5ba49416991abc162a",
        "client_secret": "fc71a0885822481a988529c565243cb3",
        "code": "e51ac7e761ef452b84fa6a87aeff570a",
        "scope": "app"
    });
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "https://api.drop-pay.io/oa2/v1/ac/token");
    xhr.setRequestHeader("content-type", "application/json");
    xhr.setRequestHeader("cache-control", "no-cache");
    xhr.send(data);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
            if (xhr.status === 200) {

                var obj_access_token = JSON.parse(xhr.responseText);
                access_token = obj_access_token.access_token;
                token_type = obj_access_token.token_type;
                test_autorizzazione_droppay_step2(callback);
            } else if (xhr.status === 400) {
                console.log('There was an error 400');
            } else {
                console.log('something else other than 200 was returned');
            }
        }
    };
}

var qrcode;

function test_autorizzazione_droppay_step2(callback) {
    var data = JSON.stringify({
        "description": "Conto",
        "charge_amount": importo_pagamento,
        "pos_id": "POSO09MKELJP9"

    });
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "https://api.drop-pay.io/shop/v1/pos/authorization");
    xhr.setRequestHeader("content-type", "application/json");
    xhr.setRequestHeader("cache-control", "no-cache");
    xhr.setRequestHeader("authorization", "Bearer" + " " + access_token);
    xhr.send(data);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
            if (xhr.status === 200) {
                var risposta = JSON.parse(xhr.responseText);

                if (comanda.mobile !== true) {
                    socket_fast_split.send(JSON.stringify({stato_qr: true, tipo_qr: "droppay", codice_qr: risposta.sharing}));
                } else
                {
                    if (qrcode === undefined) {
                        qrcode = new QRCode(document.getElementById("qrcode_mobile"), {
                            text: risposta.sharing,
                            width: 200,
                            height: 200
                        });
                    } else {
                        qrcode.clear();
                        qrcode.makeCode(risposta.sharing);
                    }

                    $("#pagamento_droppay .modal-body  .scritte").html("");
                    $("#pagamento_droppay .modal-body #cornice_qr_mobile").show();
                    $("#pagamento_droppay").modal("show");
                }

                auth_id = risposta.id;
                if (risposta.merchant_custom_label)
                    merchant_custom_label = risposta.merchant_custom_label;
                if (risposta.merchant_custom_id)
                    merchant_custom_id = risposta.merchant_custom_id;
                var i = 0;
                //DOPO UN MINUTO I TENTATIVI DI POLLING SI FERMANO
                //E VA RICHIESTA UNA NUOVA AUTORIZZAZIONE
                intervallo_polling_droppay = setInterval(function () {
                    if (i < 30) {
                        test_autorizzazione_droppay_polling(callback);
                    } else
                    {
                        clearInterval(intervallo_polling_droppay);
                    }
                    i++;
                }, 2000);
            } else if (xhr.status === 400) {
                console.log('There was an error 400');
            } else {
                console.log('something else other than 200 was returned');
            }
        }
    };
}

function test_autorizzazione_droppay_polling(callback) {

    var xhr = new XMLHttpRequest();
    xhr.open("GET", "https://api.drop-pay.io/shop/v1/pos/authorization/" + auth_id);
    xhr.setRequestHeader("content-type", "application/json");
    xhr.setRequestHeader("cache-control", "no-cache");
    xhr.setRequestHeader("authorization", "Bearer" + " " + access_token);
    xhr.send();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
            if (xhr.status === 200) {
                var risposta = JSON.parse(xhr.responseText);
                if (risposta.pay_token) {
                    test_autorizzazione_droppay_addebito(risposta, callback);
                }

            } else if (xhr.status === 400) {
                console.log('There was an error 400');
            } else {
                console.log('something else other than 200 was returned');
            }
        }
    };
}

function test_autorizzazione_droppay_addebito(risposta, callback) {
    var data = JSON.stringify({
        "pay_token_val": risposta.pay_token.val,
        "authorization_id": auth_id,
        "description": "Conto",
        "amount": importo_pagamento/*,
         "merchant_custom_id": merchant_custom_id,
         "merchant_custom_label": merchant_custom_label*/
    });
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "https://api.drop-pay.io/shop/v1/pos/charge");
    xhr.setRequestHeader("content-type", "application/json");
    xhr.setRequestHeader("cache-control", "no-cache");
    xhr.setRequestHeader("authorization", "Bearer" + " " + access_token);
    xhr.send(data);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4

            clearInterval(intervallo_polling_droppay);


            if (xhr.status === 200) {
                socket_fast_split.send(JSON.stringify({stato_qr: false, tipo_qr: "droppay"}));

                $("#pagamento_droppay .modal-body  .scritte").html("PAGAMENTO AVVENUTO...");
                $("#pagamento_droppay .modal-body #cornice_qr_mobile").hide();
                $("#pagamento_droppay").modal("show");

                if (typeof (callback) === "function") {
                    callback(true);
                }
            } else if (xhr.status === 400) {

                $("#pagamento_droppay .modal-body .scritte").html("ERRORE 400...");
                $("#pagamento_droppay .modal-body #cornice_qr_mobile").hide();

                $("#pagamento_droppay").modal("show");

                socket_fast_split.send(JSON.stringify({stato_qr: false, tipo_qr: "droppay"}));
            } else {

                $("#pagamento_droppay .modal-body .scritte").html("ERRORE GENERICO...");
                $("#pagamento_droppay .modal-body #cornice_qr_mobile").hide();
                $("#pagamento_droppay").modal("show");

                socket_fast_split.send(JSON.stringify({stato_qr: false, tipo_qr: "droppay"}));
            }
        }
    };
}