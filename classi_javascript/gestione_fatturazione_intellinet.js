/* global comanda, bootbox */

function salva_cliente_tavolo() {

    let cliente_selezionato = $("#fattura_elenco_clienti").val();

    let id = false;

    if (!cliente_selezionato) {

        cliente_selezionato = "";

    }

    let ragione_sociale = $('#fattura [name="ragione_sociale"]').val().toUpperCase();
    let indirizzo = $('#fattura [name="indirizzo"]').val().toUpperCase();
    let numero = $('#fattura [name="numero"]').val().toUpperCase();
    let cap = $('#fattura [name="cap"]').val();
    let citta = $('#fattura [name="comune"]').val();
    let provincia = $('#fattura [name="provincia"]').val().toUpperCase();
    let paese = $('#fattura [name="paese"]').val();
    let cellulare = $('#fattura [name="cellulare"]').val();
    let telefono = $('#fattura [name="telefono"]').val();
    let fax = $('#fattura [name="fax"]').val();
    let email = $('#fattura [name="email"]').val();
    let codice_fiscale = $('#fattura [name="codice_fiscale"]').val().toUpperCase();
    let partita_iva = $('#fattura [name="partita_iva"]').val();
    let partita_iva_estera = $('#fattura [name="partita_iva_estera"]').val();
    let formato_trasmissione = $('#fattura [name="formato_trasmissione"]').prop("checked");
    let split_payment = $('#fattura [name="split_payment"]').prop("checked");
    let codice_destinatario = $('#fattura [name="codice_destinatario"]').val().toUpperCase();
    let regime_fiscale = $('#fattura [name="regime_fiscale"]').val();
    let pec = $('#fattura [name="pec"]').val();
    let comune = $('#fattura [name="comune"]').val().toUpperCase();
    let nazione = $('#fattura [name="nazione"] option:selected').val();
    let azienda_privato = $('[name="azienda_privato"]:checked').attr("id");



    if (cliente_selezionato !== "") {

        id = parseInt(cliente_selezionato);

        /* update con id conosciuto*/

        let query_update = "update clienti set \n\
                                        tipo_cliente='" + azienda_privato + "',\n\
                                        ragione_sociale='" + ragione_sociale + "',\n\
                                        indirizzo='" + indirizzo + "',\n\
                                        numero='" + numero + "',\n\
                                        cap='" + cap + "',\n\
                                        comune='" + citta + "',\n\
                                        provincia='" + provincia + "',\n\
                                        cellulare='" + cellulare + "',\n\
                                        telefono='" + telefono + "',\n\
                                        email='" + email + "',\n\
                                        codice_fiscale='" + codice_fiscale + "',\n\
                                        partita_iva='" + partita_iva + "',\n\
                                        partita_iva_estera='" + partita_iva_estera + "',\n\
                                        split_payment = '" + split_payment + "',\n\
                                        formato_trasmissione='" + formato_trasmissione + "',\n\
                                        codice_destinatario='" + codice_destinatario + "',\n\
                                        regime_fiscale='" + regime_fiscale + "',\n\
                                        pec='" + pec + "',\n\
                                        comune='" + comune + "',\n\
                                        nazione='" + nazione + "' \n\
                                        where id='" + id + "';";

        comanda.sock.send({tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: query_update, terminale: comanda.terminale, ip: comanda.ip_address});

        alasql(query_update);


    } else if(ragione_sociale)

    {


        /* select ultimo id */

        let testo_query1 = "SELECT id FROM clienti WHERE id<3000000 ORDER BY cast(id as int) DESC LIMIT 1;";

        let result = alasql(testo_query1);

        if (result[0] && result[0].id)
        {

            id = parseInt(result[0].id) + 1;

        }

        /* insert con id definito */

        let testo_query2 = "INSERT INTO clienti (tipo_cliente,split_payment,formato_trasmissione,codice_destinatario,regime_fiscale,pec,comune,id_nazione,nazione,id,ragione_sociale,indirizzo,numero,cap,comune,provincia,paese,cellulare,telefono,fax,email,codice_fiscale,partita_iva,iva_non_pagati,partita_iva_estera) VALUES ('" + azienda_privato + "','" + split_payment + "','" + formato_trasmissione + "','" + codice_destinatario + "','" + regime_fiscale + "','" + pec + "','" + comune + "','" + nazione + "','" + nazione + "'," + id + ",'" + ragione_sociale.toUpperCase() + "','" + indirizzo + "','" + numero + "','" + cap + "','" + citta + "','" + provincia + "','" + paese + "','" + cellulare + "','" + telefono + "','" + fax + "','" + email + "','" + codice_fiscale + "','" + partita_iva + "','10','" + partita_iva_estera + "')";

        comanda.sock.send({tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: testo_query2, terminale: comanda.terminale, ip: comanda.ip_address});

        alasql(testo_query2);

    }

    if(ragione_sociale){
    /* update tavolo con id */

    let update_tavolo = "UPDATE tavoli SET id_cliente_salvato='" + id + "' WHERE numero='" + comanda.tavolo + "';";

    comanda.sock.send({tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: update_tavolo, terminale: comanda.terminale, ip: comanda.ip_address});

    alasql(update_tavolo);

    console.log(cliente_selezionato);

   

    $("#tasti_fiscali_da_nascondere .tasto_incasso_fattura").html("FATTURA<br>" + ragione_sociale.substr(0, 8));

    $("#tasti_fiscali_da_nascondere .tasto_incasso_fattura").css("line-height", "5.275vh");

    }
    
     $("#fattura").modal("hide");
    
        return true;

}

function riprendi_cliente_tavolo() {

    /* select from tavolo where cliente */

    let select_tavolo = "SELECT id_cliente_salvato FROM tavoli WHERE numero='" + comanda.tavolo + "' LIMIT 1;";

    let result = alasql(select_tavolo);

    if (result[0] && result[0].id_cliente_salvato) {

        return result[0].id_cliente_salvato;

    } else {

        return false;

    }

}

function reset_fattura() {

    let update_tavolo = "UPDATE tavoli SET id_cliente_salvato='' WHERE numero='" + comanda.tavolo + "';";

    comanda.sock.send({tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: update_tavolo, terminale: comanda.terminale, ip: comanda.ip_address});

    alasql(update_tavolo);

    intesta_fattura();

    $("#tasti_fiscali_da_nascondere .tasto_incasso_fattura").html("FATTURA");

    $("#tasti_fiscali_da_nascondere .tasto_incasso_fattura").css("line-height", "10.55vh");

    return true;

}