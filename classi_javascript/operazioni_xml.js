/* global comanda */

function nome_servizio(callback) {

    var xhttp = new XMLHttpRequest();

    xhttp.timeout = 5000;

    xhttp.onreadystatechange = function () {
        if (xhttp.readyState === 4 && xhttp.status === 200) {

            var lines = xhttp.responseText;

            var lineArr = lines.split('\n')[5];

            comanda.servizio = lineArr;

            if (typeof (callback) === "function") {
                callback(true);
                console.log("callback vero importazione");
            }

        } else if (xhttp.readyState === 4 && xhttp.status === 404)
        {

            if (typeof (callback) === "function") {
                callback(false);
            }
            console.log("callback falso importazione");
        }
    };

    xhttp.onerror = function (event) {

        alert("ATTENZIONE! Sei fuori portata WiFi. Avvicinati all'antenna e riprova.");

        try {
            $(document).off(comanda.eventino, evento_selettore_tavolo);
        } catch (e) {
            console.log(e);
        }
        $(document).on(comanda.eventino, 'div.tavolo.rett,div.tavolo.cerc', evento_selettore_tavolo);
        $('.tasto_bestellung').css('pointer-events', '');
        $('.tasto_bargeld').css('pointer-events', '');
        $('.tasto_quittung').css('pointer-events', '');
        $('.tasto_rechnung').css('pointer-events', '');

        event.preventDefault();
    };

    xhttp.open("GET", "/" + comanda.directory_maga + "/ConfigWebComanda.TXT" + "?_=" + new Date().getTime(), true);
    xhttp.send();
}

function leggi_nomi_tavoli(cb) {

    var xhttp = new XMLHttpRequest();

    xhttp.timeout = 5000;

    xhttp.onreadystatechange = function () {
        if (xhttp.readyState === 4 && xhttp.status === 200) {

            var temp1 = $(xhttp.responseXML);

            //PARENT VUOL DIRE I SETTAGGI CON LA SOTTOCATEGORIA SERA

            var a = new Date();

            if (a.getHours() < parseInt(comanda.ora_servizio)) {
                a.setDate(a.getDate() - 1);
            }
            var lista_nodi = $(temp1).find("NOMI>REC:containsExact('" + a.format("dd/mm/yyyy") + comanda.folder_number + "')").parent();

            //console.log("LISTA NODI",lista_nodi);
            comanda.nomi_tavoli = new Array();

            lista_nodi.each(function (i, nodo) {

                var rec = $(nodo).find("REC").text();
                var tav = $(nodo).find("TAV").text();
                var nome = $(nodo).find("NOME").text().toUpperCase();
                var rif = $(nodo).find("RIF").text();
                var codcli = $(nodo).find("CODCLI").text();
                var categoria = $(nodo).find("CATEGORIA").text();

                if (nome.length > 3) {
                    comanda.nomi_tavoli.push({rec: rec, tav: tav, nome: nome, rif: rif, codcli: codcli, categoria: categoria});
                }

            });

            if (typeof (cb) === "function") {
                cb(true);
            }

        } else if (xhttp.readyState === 4 && xhttp.status === 404)
        {
            $('body').removeClass('loading');
            $('body').addClass('game-over');
            bootbox.alert(lang[251].replace("...", comanda.servizio));


        }
    };

    xhttp.onerror = function (event) {
        $('body').removeClass('loading');
        $('body').addClass('game-over');
        bootbox.alert(lang[251].replace("...", comanda.servizio));


    };

    var a = new Date();

    if (a.getHours() < parseInt(comanda.ora_servizio)) {
        a.setDate(a.getDate() - 1);
    }

    var cartella = a.format("yyyy-mm-dd") + "_" + comanda.folder_number;

    xhttp.open("GET", "/" + comanda.directory_dati + "/" + cartella + "/NOMI.XML" + "?_=" + new Date().getTime(), true);
    xhttp.send();
}

var ultimo_tavolo_colorato = "";

function colora_tavolo_ingresso(stringa_id_tavolo, tavolotxt) {

    ultimo_tavolo_colorato = aggZero(stringa_id_tavolo, 3);

    var query_sala = "select nome_sala from tavoli where numero='" + parseInt(stringa_id_tavolo) + "' and estensione='" + comanda.piantina + "';";
    comanda.sincro.query(query_sala, function (r) {

        var nome_file = "/PLANING/P" + aggZero(stringa_id_tavolo, 3) + '.I' + comanda.terminale;

        comanda.xml.salva_file_xml("_" + comanda.folder_number + "/" + nome_file, "", "PLANING", function () {

            comanda.nome_sala = r[0].nome_sala;

            disegna_tavoli_salvati();


            var tipologia_locale = "_ristorante";
            if (comanda.discoteca === true)
            {
                tipologia_locale = "_disco";
            }
            $(".nome_sala" + tipologia_locale).css('background-color', '');
            $(".nome_sala" + tipologia_locale).css('background-image', '');

            $(".nome_sala" + tipologia_locale + ":contains(" + comanda.nome_sala + ")").css('background-color', 'yellow');
            $(".nome_sala" + tipologia_locale + ":contains(" + comanda.nome_sala + ")").css('background-image', '-webkit-linear-gradient(top,yellow,yellow)');

        });
    });
}

/*function invia_messaggio_cliente_seduto(tavolo, tavolotxt) {
 
 var nome_file = "/PLANING/P" + aggZero(tavolo, 3) + '.I';
 
 comanda.xml.salva_file_xml("_" + comanda.folder_number + "/" + nome_file, "", "PLANING", function () {
 
 
 var stampante = "1";
 if (comanda.stampante_fastingresso && comanda.stampante_fastingresso !== "") {
 stampante = comanda.stampante_fastingresso;
 }
 
 var d = new Date();
 
 var h = addZero(d.getHours(), 2);
 var m = addZero(d.getMinutes(), 2);
 var s = addZero(d.getSeconds(), 2);
 
 var data = h + '' + m + '' + s;
 
 tavolo = aggZero(tavolo, 3);
 
 var d = new Date();
 var Y = d.getFullYear().toString();
 var D = addZero(d.getDate(), 2);
 var M = addZero((d.getMonth() + 1), 2);
 var h = addZero(d.getHours(), 2);
 var m = addZero(d.getMinutes(), 2);
 var s = addZero(d.getSeconds(), 2);
 var ms = addZero(d.getMilliseconds(), 3);
 var data = D + '/' + M + '/' + Y;
 var data2 = h + '' + m + '' + s;
 var ora = h + ':' + m + ':' + s;
 
 var id_tablet = "29";
 var id_temp = leggi_get_url('id');
 
 if (id_temp !== undefined && id_temp !== null && id_temp !== 'null' && id_temp.length === 2) {
 id_tablet = id_temp;
 }
 
 var nome_file = "PDA" + id_tablet + "_" + data2 + ".XM_";
 
 var pagina = '<?xml version="1.0" standalone="yes"?>\n\
 <dataroot xmlns="http://www.w3.org/2001/TAVOLI.xsd">\n\
 <TAVOLI>\n\
 <TC>C</TC>\n\
 <DATA>' + data + '</DATA>\n\
 <TAVOLO>' + tavolo + '</TAVOLO>\n\
 <BIS></BIS>\n\
 <PRG>001</PRG>\n\
 <ART>999 001</ART>\n\
 <ART2>999 001</ART2>\n\
 <VAR></VAR>\n\
 <DES>IL CLIENTE E\' AL TAVOLO</DES>\n\
 <PRZ>0,00</PRZ>\n\
 <COSTO>0</COSTO>\n\
 <QTA>1</QTA>\n\
 <SN>N</SN>\n\
 <CAT>ZZZ</CAT>\n\
 <TOTALE />\n\
 <SCONTO />\n\
 <AUTORIZ></AUTORIZ>\n\
 <INC>0</INC>\n\
 <CARTACRED/>\n\
 <BANCOMAT/>\n\
 <ASSEGNI />\n\
 <NCARD1 />\n\
 <NCARD2/>\n\
 <NCARD3/>\n\
 <NCARD4/>\n\
 <NCARD5 />\n\
 <NSEGN />\n\
 <NEXIT/>\n\
 <TS>' + stampante + '</TS>\n\
 <NOME>' + comanda.operatore + '</NOME>\n\
 <ORA>' + ora + '</ORA>\n\
 <LIB1>1</LIB1>\n\
 <LIB2/>\n\
 <LIB3/>\n\
 <LIB4>0</LIB4>\n\
 <LIB5/>\n\
 <CLI/>\n\
 <QTAP>0</QTAP>\n\
 <NODO>001</NODO>\n\
 <PORTATA>N</PORTATA>\n\
 <NUMP>0</NUMP>\n\
 <TAVOLOTXT>' + tavolotxt + '</TAVOLOTXT>\n\
 <ULTPORT></ULTPORT>\n\
 <AGG_GIAC>N</AGG_GIAC>\n\
 </TAVOLI>\n\
 <TAVOLI>\n\
 <TC>T</TC>\n\
 <DATA>' + data + '</DATA>\n\
 <TAVOLO>' + tavolo + '</TAVOLO>\n\
 <BIS />\n\
 <PRG>000</PRG>\n\
 <ART>01</ART>\n\
 <ART2/>\n\
 <VAR/>\n\
 <DES>RECORD TESTA</DES>\n\
 <PRZ/>\n\
 <COSTO>0</COSTO>\n\
 <QTA/>\n\
 <SN>N</SN>\n\
 <CAT/>\n\
 <TOTALE>0,00</TOTALE>\n\
 <SCONTO>0,00</SCONTO>\n\
 <AUTORIZ>0</AUTORIZ>\n\
 <INC>0,00</INC>\n\
 <CARTACRED></CARTACRED>\n\
 <BANCOMAT></BANCOMAT>\n\
 <ASSEGNI>0,00</ASSEGNI>\n\
 <NCARD1></NCARD1>\n\
 <NCARD2></NCARD2>\n\
 <NCARD3></NCARD3>\n\
 <NCARD4></NCARD4>\n\
 <NCARD5></NCARD5>\n\
 <NSEGN></NSEGN>\n\
 <NEXIT></NEXIT>\n\
 <TS> </TS>\n\
 <NOME>' + comanda.operatore + '</NOME>\n\
 <ORA>' + ora + '</ORA>\n\
 <LIB1>1</LIB1>\n\
 <LIB2/>\n\
 <LIB3/>\n\
 <LIB4/>\n\
 <LIB5> </LIB5>\n\
 <CLI/>\n\
 <QTAP>0</QTAP>\n\
 <NODO> </NODO>\n\
 <PORTATA/>\n\
 <NUMP></NUMP>\n\
 <TAVOLOTXT>' + tavolotxt + '</TAVOLOTXT>\n\
 <ULTPORT>N</ULTPORT>\n\
 <AGG_GIAC>N</AGG_GIAC>\n\
 </TAVOLI>\n\
 </dataroot>';
 
 
 comanda.xml.salva_file_xml("_" + comanda.folder_number + "/SDS/" + nome_file, pagina, "SDS", function () {
 
 vedi_clienti_seduti = false;
 
 elenco_nomi_prenotati();
 
 
 if (typeof (cB) === "function") {
 cB(true);
 }
 
 });
 });
 }*/

function apri_tavolo_prenotato(stringa_id_tavolo, tavolotxt) {

    if (comanda.ingresso === true) {

        colora_tavolo_ingresso(stringa_id_tavolo, tavolotxt);

    } else
    {

        evento_selettore_tavolo(undefined, stringa_id_tavolo, tavolotxt);


    }
    $("#popup_nomi_tavoli").modal("hide");
}

function recupera_tavolo_prenotato(stringa_id_tavolo, tavolotxt) {

    if (comanda.ingresso === true) {

        var nome_file = "/PLANING/P" + aggZero(stringa_id_tavolo, 3);

        comanda.xml.elimina_file_xml("_" + comanda.folder_number + "/" + nome_file, true, function () {

            elenco_nomi_prenotati();

        });

    }
}

function compare1(a, b) {
    if (a.tav < b.tav)
        return -1;
    if (a.tav > b.tav)
        return 1;
    return 0;
}

function compare3(a, b) {
    if (aggZero(a.tavolotxt, 3) < aggZero(b.tavolotxt, 3))
        return -1;
    if (aggZero(a.tavolotxt, 3) > aggZero(b.tavolotxt, 3))
        return 1;
    return 0;
}

function compare2(a, b) {
    if (a.nome < b.nome)
        return -1;
    if (a.nome > b.nome)
        return 1;
    return 0;
}

function elenco_tavoli_prenotati() {

    var sql = "select numero,tavolotxt,colore from tavoli where estensione='" + comanda.piantina + "';";

    comanda.sincro.query(sql, function (db) {

        var nomi_tavolitxt = new Array();


        comanda.nomi_tavoli.sort(compare1).forEach(function (obj) {
            try {
                var BreakException = {};
                var tavolotxt = "";
                db.forEach(function (tavdb) {

                    if (aggZero(obj.tav, 3) === aggZero(tavdb.numero, 3)) {
                        tavolotxt = tavdb.tavolotxt;

                        nomi_tavolitxt.push({categoria: obj.categoria, codcli: obj.codcli, nome: obj.nome, rec: obj.rec, rif: obj.rif, tav: obj.tav, tavolotxt: tavolotxt});

                        throw BreakException;
                    }
                });

            } catch (e) {
                if (e !== BreakException)
                    throw e;
            }
        });


        var output = "<table class='table table-responsive table-hover  table-striped'>";

        nomi_tavolitxt.sort(compare3).forEach(function (obj) {

            var BreakException = {};
            var tavolotxt = "";
            try {
                db.forEach(function (tavdb) {

                    if (aggZero(obj.tav, 3) === aggZero(tavdb.numero, 3)) {
                        tavolotxt = tavdb.tavolotxt;
                        throw BreakException;
                    }
                });

            } catch (e) {
                if (e !== BreakException)
                    throw e;
            }

            output += "<tr " + comanda.evento + "=\"comanda.tavolotxt='" + tavolotxt + "';apri_tavolo_prenotato('" + aggZero(obj.tav, 3) + "','" + tavolotxt + "');\">";
            output += "<td>";
            output += "Tav: " + tavolotxt;
            output += "</td>";
            output += "<td>";
            output += obj.nome;
            output += "</td>";
            output += "<td>";
            output += "";
            output += "</td>";
            output += "</tr>";
        });
        output += "</table>";

        $("#popup_nomi_tavoli .modal-body").html(output);
        $("#popup_nomi_tavoli").modal("show");
    });
}



var vedi_clienti_seduti = false;

function toggle_clienti_seduti() {
    if (vedi_clienti_seduti === true) {
        vedi_clienti_seduti = false;
    } else
    {
        vedi_clienti_seduti = true;
    }
}

function elenco_nomi_prenotati() {


    comanda.xml.sistema_planing_ingresso(function () {

        disegna_tavoli_salvati(undefined, function () {

            var sql = "select numero,tavolotxt,colore from tavoli  where estensione='" + comanda.piantina + "';";

            comanda.sincro.query(sql, function (db) {

                var output = "<table class='table table-responsive table-hover  table-striped'>";

                comanda.nomi_tavoli.sort(compare2).forEach(function (obj) {

                    var BreakException = {};
                    var tavolotxt = "";
                    var colore = "";
                    try {
                        db.forEach(function (tavdb) {

                            if (aggZero(obj.tav, 3) === aggZero(tavdb.numero, 3) && ((vedi_clienti_seduti === false && tavdb.colore !== "0") || (vedi_clienti_seduti === true) || (comanda.ingresso !== true))) {
                                tavolotxt = tavdb.tavolotxt;
                                if (tavdb.colore === "0") {
                                    colore = "style='background-color:green;'";
                                }
                                throw BreakException;
                            }
                        });

                    } catch (e) {
                        if (e !== BreakException)
                            throw e;
                    }

                    if (tavolotxt !== "") {

                        output += "<tr " + colore + " >";
                        output += "<td " + comanda.evento + "=\"comanda.tavolotxt='" + tavolotxt + "';apri_tavolo_prenotato('" + aggZero(obj.tav, 3) + "','" + tavolotxt + "');\">";
                        output += obj.nome;
                        output += "</td>";
                        output += "<td " + comanda.evento + "=\"comanda.tavolotxt='" + tavolotxt + "';apri_tavolo_prenotato('" + aggZero(obj.tav, 3) + "','" + tavolotxt + "');\">";
                        output += "Tav: " + tavolotxt;
                        output += "</td>";
                        if (colore === "style='background-color:green;'") {
                            output += "<td style='color:white;' " + comanda.evento + "=\"comanda.tavolotxt='" + tavolotxt + "';recupera_tavolo_prenotato('" + aggZero(obj.tav, 3) + "','" + tavolotxt + "');\">";
                            output += "&uarr;";
                            output += "</td>";
                        } else
                        {
                            output += "<td " + comanda.evento + "=\"comanda.tavolotxt='" + tavolotxt + "';apri_tavolo_prenotato('" + aggZero(obj.tav, 3) + "','" + tavolotxt + "');\">";
                            output += "";
                            output += "</td>";
                        }
                        output += "</tr>";
                    }

                });
                output += "</table>";

                $("#popup_nomi_tavoli .modal-body").html(output);
                $("#popup_nomi_tavoli").modal("show");
            });
        });
    });
}





function leggi_utenti(callback) {

    var temp1;

    var xhttp = new XMLHttpRequest();

    xhttp.timeout = 5000;

    xhttp.onerror = function (event) {

        alert("ATTENZIONE! Sei fuori portata WiFi. Avvicinati all'antenna e riprova.");

        try {
            $(document).off(comanda.eventino, evento_selettore_tavolo);
        } catch (e) {
            console.log(e);
        }
        $(document).on(comanda.eventino, 'div.tavolo.rett,div.tavolo.cerc', evento_selettore_tavolo);
        $('.tasto_bestellung').css('pointer-events', '');
        $('.tasto_bargeld').css('pointer-events', '');
        $('.tasto_quittung').css('pointer-events', '');
        $('.tasto_rechnung').css('pointer-events', '');

        event.preventDefault();
    };

    xhttp.onreadystatechange = function () {
        if (xhttp.readyState === 4 && xhttp.status === 200) {

            temp1 = $(xhttp.responseXML);

            var testo_query = "delete from operatori;";
            comanda.sincro.query(testo_query, function () {

                $(temp1).find("UTENTI").each(function (i, e) {

                    if (comanda.ingresso === true) {
                        if ($(e).find("COD").html()[0] === "I") {
                            testo_query = "INSERT INTO operatori (id,nome,password,permesso) VALUES ('" + $(e).find("COD").html() + "','" + $(e).find("NOME").html() + "','" + CryptoJS.MD5($(e).find("PSW").html()).toString() + "','" + $(e).find("LIVELLO").html() + "');";
                            comanda.sincro.query(testo_query, function () {

                            });
                        }
                    } else {
                        if ($(e).find("COD").html()[0] !== "I") {
                            testo_query = "INSERT INTO operatori (id,nome,password,permesso) VALUES ('" + $(e).find("COD").html() + "','" + $(e).find("NOME").html() + "','" + CryptoJS.MD5($(e).find("PSW").html()).toString() + "','" + $(e).find("LIVELLO").html() + "');";
                            comanda.sincro.query(testo_query, function () {

                            });
                        }
                    }
                });
            });
            if (typeof (callback) === "function") {
                callback(true);
            }

        } else if (xhttp.readyState === 4 && xhttp.status === 404)
        {

            if (typeof (callback) === "function") {
                callback(false);
            }
            console.log("callback falso importazione");
        }
    };

    xhttp.open("GET", "/" + comanda.directory_maga + "/UTENTI.XML" + "?_=" + new Date().getTime(), true);
    xhttp.send();

}


function seleziona_tastierino_abilitato(callback) {

    if (comanda.ricerca_numerica_abilitata === true)
    {
        if (comanda.tipo_ricerca_articolo === undefined) {

            var a = '';

            a += "<tr><td " + comanda.evento + "=\"comanda.tipo_ricerca_articolo='NUMERICA'; $('#popup_scelta_ricerca').modal('hide');\">RICERCA NUMERICA</td></tr>";
            a += "<tr><td>&nbsp;</td></tr>";
            a += "<tr><td " + comanda.evento + "=\"comanda.tipo_ricerca_articolo='NORMALE'; $('#popup_scelta_ricerca').modal('hide');\">RICERCA NORMALE</td></tr>";

            $('#popup_scelta_ricerca .modal-body .table').html(a);
            $('#popup_scelta_ricerca').modal('show');


            var interval = setInterval(function () {
                if (comanda.tipo_ricerca_articolo !== undefined)
                {
                    if (typeof (callback) === "function") {
                        callback(true);
                        clearInterval(interval);
                    }
                }

            }, 100);
        } else {
            callback(true);
        }
    } else
    {
        callback(true);
    }
}

function seleziona_servizio(callback) {

    var temp1;
    var a = '';

    var height = 20;
    if (comanda.fastconto === true) {
        height = 10;
    }

    var font = 5;
    if (comanda.fastconto === true) {
        font = 3;
    }

    var xhttp = new XMLHttpRequest();

    xhttp.timeout = 5000;

    xhttp.onerror = function (event) {

        alert("ATTENZIONE! Sei fuori portata WiFi. Avvicinati all'antenna e riprova.");

        try {
            $(document).off(comanda.eventino, evento_selettore_tavolo);
        } catch (e) {
            console.log(e);
        }
        $(document).on(comanda.eventino, 'div.tavolo.rett,div.tavolo.cerc', evento_selettore_tavolo);
        $('.tasto_bestellung').css('pointer-events', '');
        $('.tasto_bargeld').css('pointer-events', '');
        $('.tasto_quittung').css('pointer-events', '');
        $('.tasto_rechnung').css('pointer-events', '');

        event.preventDefault();
    };

    xhttp.onreadystatechange = function () {
        if (xhttp.readyState === 4 && xhttp.status === 200) {

            temp1 = $(xhttp.responseXML);

            $(temp1).find("SETTAGGI").each(function (i, e) {

                if (comanda.creatore_piantine === true) {
                    comanda.servizio = $(e).find("REC").html();
                    comanda.folder_number = $(e).find("SERVIZIO").html();
                    comanda.nome_servizio = $(e).find("REC").html();
                } else if ($(e).find("LIST").html() !== undefined && $(e).find("REC").html().indexOf('ZSERVICE') === -1 && $(e).find("REC").html().indexOf('_') === -1 && $(e).find("REC").html().trim().length > 0) {

                    a += "<tr><td style='font-size:" + font + "vh;height: " + height + "vh;vertical-align: inherit;display: table-cell;' " + comanda.evento + "=\"comanda.servizio='" + $(e).find("REC").html() + "'; comanda.folder_number='" + $(e).find("SERVIZIO").html() + "'; comanda.nome_servizio='" + $(e).find("REC").html() + "'; $('#popup_scelta_servizio').modal('hide');\">" + $(e).find("REC").html() + "</td></tr>";
                }

            });

            /* a += "<tr><td style='font-size:"+font+"vh;height: "+height+"vh;vertical-align: inherit;display: table-cell;' "+comanda.evento+"=\"javascript:top.frames.location.reload();\">Aggiorna Software</td></tr>"; */



            $('#popup_scelta_servizio .modal-body .table').html(a);
            $('#popup_scelta_servizio').modal('show');


            var interval = setInterval(function () {
                if (comanda.folder_number !== undefined && comanda.nome_servizio !== undefined)
                {
                    if (typeof (callback) === "function") {
                        callback(true);
                        clearInterval(interval);
                    }
                }


            }, 100);
        }
    };

    xhttp.open("GET", "/MAGA/" + comanda.file_settaggi + ".XML" + "?_=" + new Date().getTime(), true);
    xhttp.send();

}


function leggi_coperti(callback) {

    var xhttp = new XMLHttpRequest();

    xhttp.timeout = 5000;

    xhttp.onreadystatechange = function () {
        if (xhttp.readyState === 4 && xhttp.status === 200) {

            var temp1 = $(xhttp.responseXML);

            //PARENT VUOL DIRE I SETTAGGI CON LA SOTTOCATEGORIA SERA
            var a = $(temp1).find("SETTAGGI>REC:containsExact('" + comanda.nome_servizio + "')").parent();

            console.log("leggi_coperti", temp1);

            if (a.find("LIB1").text().length > 0) {

                var testo_query = "UPDATE impostazioni_fiscali SET valore_coperto='" + a.find("LIB1").text().replace(",", ".") + "'  where id=" + comanda.folder_number + " ;";
                comanda.valore_coperto = a.find("LIB1").text().replace(",", ".");

                console.log("leggi_settaggi", testo_query);
                comanda.sincro.query(testo_query, function () {

                    if (typeof (callback) === "function") {

                        console.log("IMPOSTAZIONI FISCALI", "CALLBACK");
                        callback(true);
                        console.log("callback vero importazione");
                    }

                });

            } else if (typeof (callback) === "function") {
                var testo_query = "UPDATE impostazioni_fiscali SET valore_coperto='0'  where id=" + comanda.folder_number + " ;";
                comanda.valore_coperto = "0";

                console.log("leggi_settaggi", testo_query);
                comanda.sincro.query(testo_query, function () {

                    if (typeof (callback) === "function") {

                        console.log("IMPOSTAZIONI FISCALI", "CALLBACK");
                        callback(true);
                        console.log("callback vero importazione");
                    }

                });

            }

            comanda.conteggio_coperti_differenziati = false;

            comanda.conteggio_coperti_differenziati_COD1 = "";
            comanda.conteggio_coperti_differenziati_COD2 = "";
            comanda.conteggio_coperti_differenziati_COD3 = "";

            var b = $(temp1).find("SETTAGGI>REC:containsExact('" + comanda.nome_servizio + "+1')").parent();

            if (b.find("SET74").text().length > 0) {
                comanda.conteggio_coperti_differenziati = true;
                comanda.conteggio_coperti_differenziati_COD1 = b.find("SET74").text();
            }
            if (b.find("SET75").text().length > 0) {
                comanda.conteggio_coperti_differenziati = true;
                comanda.conteggio_coperti_differenziati_COD2 = b.find("SET75").text();
            }
            if (b.find("SET76").text().length > 0) {
                comanda.conteggio_coperti_differenziati = true;
                comanda.conteggio_coperti_differenziati_COD3 = b.find("SET76").text();
            }

        } else if (xhttp.readyState === 4 && xhttp.status === 404)
        {

            if (typeof (callback) === "function") {
                callback(false);
            }
            console.log("callback falso importazione");
        }
    };

    xhttp.onerror = function (event) {

        alert("ATTENZIONE! Sei fuori portata WiFi. Avvicinati all'antenna e riprova.");

        try {
            $(document).off(comanda.eventino, evento_selettore_tavolo);
        } catch (e) {
            console.log(e);
        }
        $(document).on(comanda.eventino, 'div.tavolo.rett,div.tavolo.cerc', evento_selettore_tavolo);
        $('.tasto_bestellung').css('pointer-events', '');
        $('.tasto_bargeld').css('pointer-events', '');
        $('.tasto_quittung').css('pointer-events', '');
        $('.tasto_rechnung').css('pointer-events', '');

        event.preventDefault();
    };

    xhttp.open("GET", "/MAGA/" + comanda.file_settaggi + ".XML" + "?_=" + new Date().getTime(), true);
    xhttp.send();
}

function leggi_nome_menu(callback) {

    var xhttp = new XMLHttpRequest();

    xhttp.timeout = 5000;

    xhttp.onreadystatechange = function () {
        if (xhttp.readyState === 4 && xhttp.status === 200) {

            var temp1 = $(xhttp.responseXML);

            //PARENT VUOL DIRE I SETTAGGI CON LA SOTTOCATEGORIA SERA
            var a = $(temp1).find("SETTAGGI>REC:containsExact('" + comanda.nome_servizio + "')").parent();

            console.log("leggi_nome_menu", temp1);

            if (a.find("SALA").text().length > 0) {

                var testo_query = "UPDATE settaggi_ibrido SET menu='" + a.find("SALA").text().replace('.XML', '') + "' WHERE id='" + comanda.nome_servizio + "';";

                console.log("leggi_settaggi", testo_query);
                comanda.sincro.query(testo_query, function () {

                    if (typeof (callback) === "function") {

                        console.log("IMPOSTAZIONI FISCALI", "CALLBACK");
                        callback(true);
                        console.log("callback vero importazione");
                    }

                });

            } else if (typeof (callback) === "function") {
                callback(true);
                console.log("callback vero importazione");

            }

        } else if (xhttp.readyState === 4 && xhttp.status === 404)
        {

            if (typeof (callback) === "function") {
                callback(false);
            }
            console.log("callback falso importazione");
        }
    };

    xhttp.onerror = function (event) {

        alert("ATTENZIONE! Sei fuori portata WiFi. Avvicinati all'antenna e riprova.");

        try {
            $(document).off(comanda.eventino, evento_selettore_tavolo);
        } catch (e) {
            console.log(e);
        }
        $(document).on(comanda.eventino, 'div.tavolo.rett,div.tavolo.cerc', evento_selettore_tavolo);
        $('.tasto_bestellung').css('pointer-events', '');
        $('.tasto_bargeld').css('pointer-events', '');
        $('.tasto_quittung').css('pointer-events', '');
        $('.tasto_rechnung').css('pointer-events', '');

        event.preventDefault();
    };

    xhttp.open("GET", "/MAGA/" + comanda.file_settaggi + ".XML" + "?_=" + new Date().getTime(), true);
    xhttp.send();
}

function leggi_cartelle_maga_dati(callback) {

    var xhttp = new XMLHttpRequest();

    xhttp.timeout = 5000;

    xhttp.onreadystatechange = function () {
        if (xhttp.readyState === 4 && xhttp.status === 200) {

            var temp1 = $(xhttp.responseXML);

            //PARENT VUOL DIRE I SETTAGGI CON LA SOTTOCATEGORIA SERA
            var a = $(temp1).find("SETTAGGI>REC:containsExact('" + comanda.nome_servizio + "')").parent();


            comanda.directory_dati = "DATI";
            if (a.find("SETB15").text().trim().length > 0) {
                comanda.directory_dati = a.find("SETB15").text();
            }

            comanda.directory_maga = "MAGA";
            if (a.find("SETB16").text().trim().length > 0) {
                comanda.directory_maga = a.find("SETB16").text();
            }

            callback(true);
        } else if (xhttp.readyState === 4 && xhttp.status === 404)
        {

            if (typeof (callback) === "function") {
                callback(false);
            }
        }
    };

    xhttp.onerror = function (event) {

        alert("ATTENZIONE! Sei fuori portata WiFi. Avvicinati all'antenna e riprova.");

        try {
            $(document).off(comanda.eventino, evento_selettore_tavolo);
        } catch (e) {
            console.log(e);
        }
        $(document).on(comanda.eventino, 'div.tavolo.rett,div.tavolo.cerc', evento_selettore_tavolo);
        $('.tasto_bestellung').css('pointer-events', '');
        $('.tasto_bargeld').css('pointer-events', '');
        $('.tasto_quittung').css('pointer-events', '');
        $('.tasto_rechnung').css('pointer-events', '');

        event.preventDefault();
        //callback(false);
    };

    xhttp.open("GET", "/MAGA/" + comanda.file_settaggi + ".XML" + "?_=" + new Date().getTime(), true);
    xhttp.send();
}


function leggi_settaggi(nome_file_settaggi_esclusa_estensione, tab_importazione, profilo, campi_xml, campi_tab_corr, tentativi, callback) {

    var xhttp = new XMLHttpRequest();

    xhttp.timeout = 5000;

    xhttp.onreadystatechange = function () {
        if (xhttp.readyState === 4 && xhttp.status === 200) {

            var temp1 = $(xhttp.responseXML);

            var b = $(temp1).find("SETTAGGI>REC:containsExact('" + comanda.nome_servizio + "+1')").parent();

            comanda.SETB36 = "";
            if (b.find("SETB36").text().length > 0) {
                comanda.SETB36 = b.find("SETB36").text();
            }

            comanda.SETB38 = "";
            if (b.find("SETB38").text().length > 0) {
                comanda.SETB38 = b.find("SETB38").text();
            }

            comanda.SETB39 = "";
            if (b.find("SETB39").text().length > 0) {
                comanda.SETB39 = b.find("SETB39").text();
            }

            //PARENT VUOL DIRE I SETTAGGI CON LA SOTTOCATEGORIA SERA
            var a = $(temp1).find("SETTAGGI>REC:containsExact('" + comanda.nome_servizio + "')").parent();

            /* <SET88>CASSA01\SQLEXPRESS</SET88>
             <SET89>fastcomanda</SET89>
             <SET90>FCcsl</SET90>*/

            comanda.PRF2 = "";
            if (a.find("PRF2").text().trim().length > 0) {
                comanda.PRF2 = a.find("PRF2").text();
            }
            
            /*comanda.tasto_bargeld = 'N';
            if(comanda.SETB36==='S'&&comanda.PRF2==='S'){
                comanda.tasto_bargeld = 'S';
            }*/

            comanda.SET88 = "";
            if (a.find("SET88").text().trim().length > 0) {
                comanda.SET88 = a.find("SET88").text();
            }

            comanda.SET89 = "";
            if (a.find("SET89").text().trim().length > 0) {
                comanda.SET89 = a.find("SET89").text();
            }

            comanda.SET90 = "";
            if (a.find("SET90").text().trim().length > 0) {
                comanda.SET90 = a.find("SET90").text();
            }

            comanda.piantina = "";
            if (a.find("PIANTINA").text().trim().length > 0) {
                comanda.piantina = a.find("PIANTINA").text();
            }

            var IP5 = a.find("IP5").text();
            if (IP5.trim().length > 0) {
                comanda.categoria_gusti = IP5;
            } else
            {
                comanda.categoria_gusti = 0;
            }

            var DISCORISTO = a.find("DR").text();
            if (DISCORISTO === "D") {
                comanda.discoteca = true;
            } else
            {
                comanda.discoteca = false;
            }

            var data_servizio_xml = a.find("DATAS").text();

            var data_giorno_dopo = new Date(data_servizio_xml.substr(6, 4) + '/' + data_servizio_xml.substr(3, 2) + '/' + data_servizio_xml.substr(0, 2));
            data_giorno_dopo.setDate(data_giorno_dopo.getDate() + 1);

            data_inizio_servizio = a.find("DATAS").text();
            data_inizio_servizio_giorno_dopo = data_giorno_dopo.format('dd/mm/yyyy');


            var SCAGL1 = a.find("SCAGL1").text();
            if (SCAGL1 === "") {
                SCAGL1 = "260";
            }
            var SCAGL2 = a.find("SCAGL2").text();
            if (SCAGL2 === "") {
                SCAGL2 = "390";
            }
            var SCAGL3 = a.find("SCAGL3").text();
            if (SCAGL3 === "") {
                SCAGL3 = "0";
            }

            var VARMENO = a.find("VARMENO").text();
            if (VARMENO === "") {
                VARMENO = "N";
            }

            var VAR_PICCOLA = a.find("VAR_PICCOLA").text();
            comanda.ARRAY_VAR_PICCOLA = VAR_PICCOLA.match(/(\w+\s\w+)/gi);


            var SET16 = a.find("SET16").text().trim();


            var SETB35 = a.find("SETB35").text();
            if (SETB35 === "") {
                SETB35 = "1";
            }

            var ora_servizio = "5";

            if (a.find("SETB94").text().trim().length > 0 && !isNaN(a.find("SETB94").text().trim())) {
                ora_servizio = a.find("SETB94").text().trim();
            }

            comanda.ora_servizio = ora_servizio;


            comanda.SCAGL1 = parseFloat(SCAGL1.replace(",", "."));
            comanda.SCAGL2 = parseFloat(SCAGL2.replace(",", "."));
            comanda.SCAGL3 = parseFloat(SCAGL3.replace(",", "."));
            comanda.VARMENO = VARMENO;
            comanda.SET16 = SET16;
            comanda.SETB35 = SETB35;
            comanda.VAR_PICCOLA = VAR_PICCOLA;

            var DCQT = a.find("DCQT").text();
            if (DCQT === "") {
                if (comanda.lingua_stampa === "italiano") {
                    DCQT = "10";
                } else if (comanda.lingua_stampa === "deutsch") {
                    DCQT = "16";
                }
            }
            comanda.percentuale_iva_default = DCQT;

            var testo_query = "DROP TABLE IF EXISTS " + tab_importazione + ";";
            comanda.sincro.query(testo_query, function () {


                testo_query = "CREATE TABLE IF NOT EXISTS " + tab_importazione + " (id,numero,italiano,english,deutsch,ip,dimensione,nome,intelligent,fiscale,devid_nf,devicetype,devicemodel,ip_alternativo,matricola); ";
                console.log("leggi_settaggi", testo_query);

                comanda.sincro.query(testo_query, function () {
                    for (var i = 1; i <= tentativi; i++)
                    {
                        var id = i;


                        var ns = "NS" + i;


                        var dest = "DEST" + i;
                        var ncop = "NCOP" + i;
                        var buz = "BUZ" + i;

                        if (i >= 9) {
                            ns = "NSN" + i;
                            dest = "DESTN" + i;
                            ncop = "NCOPN" + i;
                            buz = "BUZN" + i;
                        }




                        if (a.find(ns).text().length > 0) {




                            testo_query = "INSERT INTO " + tab_importazione + " (" + campi_tab_corr + ") VALUES ('" + id + "','" + id + "','" + a.find(dest).text().replace(/\'/, '') + "','" + a.find(ns).text().replace(/\'/, '') + "');";

                            console.log("leggi_settaggi", testo_query);
                            comanda.sincro.query(testo_query, function () {
                                if (i === tentativi) {
                                    if (typeof (callback) === "function") {
                                        callback(true);
                                        console.log("callback vero importazione");
                                    }
                                }
                            });


                        } else if (i === tentativi) {
                            if (typeof (callback) === "function") {
                                callback(true);
                                console.log("callback vero importazione");
                            }
                        }

                    }
                });
            });
        } else if (xhttp.readyState === 4 && xhttp.status === 404)
        {

            if (typeof (callback) === "function") {
                callback(false);
            }
            console.log("callback falso importazione");
        }
    };

    xhttp.onerror = function (event) {

        alert("ATTENZIONE! Sei fuori portata WiFi. Avvicinati all'antenna e riprova.");

        try {
            $(document).off(comanda.eventino, evento_selettore_tavolo);
        } catch (e) {
            console.log(e);
        }
        $(document).on(comanda.eventino, 'div.tavolo.rett,div.tavolo.cerc', evento_selettore_tavolo);
        $('.tasto_bestellung').css('pointer-events', '');
        $('.tasto_bargeld').css('pointer-events', '');
        $('.tasto_quittung').css('pointer-events', '');
        $('.tasto_rechnung').css('pointer-events', '');

        event.preventDefault();
        //callback(false);
    };

    xhttp.open("GET", "/MAGA/" + nome_file_settaggi_esclusa_estensione + ".XML" + "?_=" + new Date().getTime(), true);
    xhttp.send();
}

var Xml = function () {
//colonne separate da virgola, collezione di elementi HTML, nome tabella, callback
    var _xml_to_db_ = function (colonna, dataroot, tab, callBack) {

        var sync = new Sincro();

        if (tab === "comanda") {
            var id_com_context = id_comanda(false);
        }

        if (tab !== "comanda") {
            var testo_query = "DROP TABLE IF EXISTS " + tab + ";";
        } else
        {
            var testo_query = "select count(0)";
        }

        sync.query(testo_query, function () {

            var length = dataroot.length;


            if (tab === "comanda") {
                //FACENDO LUNGHEZZA NODO -1 DOVREBBE EVITARE DI PRENDERE IL RECORD TESTA CHE E' L'ULTIMO
                //length = length - 1;
            }

            console.log("LUNGHEZZA ELEMENTI");

            for (var a = 0; a < length; a++) {
                console.log("CONTATORE A", a);

                var lunghezza_nodo = dataroot[a].children.length;
                ////console.log("colonna", colonna);


                var ar_colonne = new Array();
                var ar_righe = new Array();

                function addslashes(str) {

                    return (str + '')
                            .replace(/[\\"']/g, '\\$&')
                            .replace(/\u0000/g, '\\0');
                }

                var colonne = new Array();


                for (var b = 0; b < lunghezza_nodo; b++)
                {
                    ////console.log(a, b, dataroot[a].children[b].innerHTML);

                    //document.write(a, "----", dataroot[a].children[b].nodeName, "---", dataroot[a].children[b].innerHTML, "<br/>");
                    ////console.log(b);


                    if (colonna === 0)
                    {
                        ////console.log(typeof (dataroot[a].children[b].nodeName));
                        ////console.log(dataroot[a].children[b].nodeName);

                        ar_colonne.push(dataroot[a].children[b].nodeName);

                    } else
                    {


                        if (tab === "prodotti" && dataroot[a].children[b].nodeName.substring(0, 3) === "LIB") {
                            //NIENTE                
                        } else {
                            ar_colonne.push(colonna[dataroot[a].children[b].nodeName]);

                            if (dataroot[a].children[b].nodeName === "NCARD1" && dataroot[a].children[b].innerHTML === "SI") { //IN QUESTO CASO SI TRATTA DI SN DELLA COMANDA
                                ar_righe.push("\"1\"");
                            } else if (dataroot[a].children[b].nodeName === "DES" && tab === "comanda") {
                                ar_righe.push("\"" + dataroot[a].children[7].innerHTML + dataroot[a].children[b].innerHTML + "\"");
                            } else if (dataroot[a].children[b].nodeName === "SN") { //IN QUESTO CASO SI TRATTA DI SN DELLA COMANDA
                                ar_righe.push("\"" + dataroot[a].children[b].innerHTML.toLowerCase() + "\"");
                            } else if (dataroot[a].children[b].nodeName === "PRZ" && dataroot[a].children[b].innerHTML.length === 0) {
                                ar_righe.push("\"\"");
                            } else if (dataroot[a].children[b].nodeName === "PRZ" && dataroot[a].children[b].innerHTML.length > 0) {
                                ar_righe.push("\"" + dataroot[a].children[b].innerHTML + "\"");
                            } else if (dataroot[a].children[b].nodeName !== "PRZ")
                            {
                                ar_righe.push("\"" + dataroot[a].children[b].innerHTML + "\"");
                            }
                        }
                    }
                }

                //SERVE PER AGGIUNGERE DATI ALLA TABELLA CHE NON CI SONO NELL'XML
                //ESEMPIO: SE NELL XML NON CE LO STATO RECORD, PER METTERLO SEMPRE ATTIVO SI AGGIUNGE QUI SOTTO
                //VANNO SEMPRE URILIZZARE LE VIRGOLETTE CON LO SLASH PER METTERLO COME STRINGA, ALTRIMENTI ACCETTA SOLO VARIABILI
                if (tab === "comanda") {


                    ar_colonne.push("id");
                    ar_righe.push("\"" + id_com_context + "\"");

                    ar_colonne.push("tipo_record");
                    ar_righe.push("\"CORPO\"");

                    ar_colonne.push("posizione");
                    ar_righe.push("\"CONTO\"");

                    ar_colonne.push("stato_record");
                    ar_righe.push("\"ATTIVO\"");


                }

                if (tab === "prodotti") {

                    ar_colonne.push("id");
                    ar_righe.push(a);
                    ar_colonne.push("pagina");
                    ar_righe.push("1");
                    ar_colonne.push("colore_tasto");
                    ar_righe.push("\"silver\"");
                }

                if (tab === "categorie") {

                    ar_colonne.push("cat_var");
                    ar_righe.push("\"null\"");

                }



                colonne = ar_colonne.join();
                //console.log(colonne);

                var righe = ar_righe.join();

                var insert = function (a, length, tab, colonne, righe) {


                    testo_query = "INSERT INTO " + tab + " (" + colonne + ") VALUES (" + righe + ");";


                    console.log("CONTATORE A", testo_query);

                    sync.query(testo_query, function () {

                        //console.log(a, length);
                        if (a === (length - 1)) {
                            console.log("CALLBACK TAVOLI ESEGUITO");
                            callBack(true);
                        }

                    });
                };

                if (a === 0) {

                    var colonne_iniziali = '';

                    if (typeof (colonna) === "object") {
                        var i = 0;
                        //console.log(colonna);
                        for (var key in colonna) {

                            var value = colonna[key];

                            if (value !== undefined) {
                                if (i === 0) {
                                    colonne_iniziali += value + ' TEXT ';
                                } else {
                                    colonne_iniziali += ',' + value + ' TEXT ';
                                }
                            }

                            ////console.log(colonna[key]);
                            i++;

                        }
                    }


                    var testo_query = "CREATE TABLE '" + tab + "' (" + colonne_iniziali + ");";
                    //console.log(testo_query);

                    sync.query(testo_query, function () {
                        insert(a, length, tab, colonne, righe);
                    });
                } else
                {
                    insert(a, length, tab, colonne, righe);
                }
            }
            ;
        });
    };


    this.elimina_file_xml = function (nome, cancellazione_ampia, callBack) {


        var request = $.ajax({
            timeout: 10000,
            method: "POST",
            url: './classi_php/elimina_file_xml.php',
            data: {directory_dati: comanda.directory_dati, ora_servizio: comanda.ora_servizio, nome: nome, cancellazione_ampia: cancellazione_ampia}
        });

        request.done(function (data) {
            console.log("SALVAFILEXML DONE");
            if (data === "1") {
                callBack(true);
            } else
            {
                alert("ATTENZIONE! Sei fuori portata WiFi. Avvicinati all'antenna e riprova.");

                try {
                    $(document).off(comanda.eventino, evento_selettore_tavolo);
                } catch (e) {
                    console.log(e);
                }
                $(document).on(comanda.eventino, 'div.tavolo.rett,div.tavolo.cerc', evento_selettore_tavolo);
                $('.tasto_bestellung').css('pointer-events', '');
                $('.tasto_bargeld').css('pointer-events', '');
                $('.tasto_quittung').css('pointer-events', '');
                $('.tasto_rechnung').css('pointer-events', '');

                callBack(false);
            }
        });

        request.fail(function (event) {
            console.log("SALVAFILEXML FAIL");

            alert("ATTENZIONE! Sei fuori portata WiFi. Avvicinati all'antenna e riprova.");

            try {
                $(document).off(comanda.eventino, evento_selettore_tavolo);
            } catch (e) {
                console.log(e);
            }
            $(document).on(comanda.eventino, 'div.tavolo.rett,div.tavolo.cerc', evento_selettore_tavolo);
            $('.tasto_bestellung').css('pointer-events', '');
            $('.tasto_bargeld').css('pointer-events', '');
            $('.tasto_quittung').css('pointer-events', '');
            $('.tasto_rechnung').css('pointer-events', '');

            //event.preventDefault();
            callBack(false);
        });
    };


    this.salva_file_xml = function (nome, struttura, tipo, callBack) {



        var totale_scontrino = $(comanda.totale_scontrino).html();

        var request = $.ajax({
            timeout: 10000,
            method: "POST",
            url: './classi_php/salva_file_xml.php',
            data: {directory_dati: comanda.directory_dati, ora_servizio: comanda.ora_servizio, nome: nome, struttura: struttura, tipo: tipo}
        });

        request.done(function (data) {

            console.log("SALVAFILEXML DONE", struttura, CryptoJS.MD5(struttura).toString(), data);

            if (data === CryptoJS.MD5(struttura).toString()) {
                callBack(true);
            } else
            {
                alert("ATTENZIONE! Il tavolo non Ã¨ stato storicizzato correttamente per motivi di rete.\nSEGNATI QUESTO ERRORE: 1 e il prezzo: " + totale_scontrino);
                callBack(false);
            }
        });

        request.fail(function (event) {
            console.log("SALVAFILEXML FAIL");

            alert("ATTENZIONE! Il tavolo non Ã¨ stato storicizzato correttamente per motivi di rete.\nSEGNATI QUESTO ERRORE: 2 e il prezzo: " + totale_scontrino);

            //event.preventDefault();
            callBack(false);
        });





    };
    //colonne personalizzate, altrimenti mettere 0
    this.importa_xml = function (colonne, file, element, table, folder, callBack) {

        var xhttp = new XMLHttpRequest();

        xhttp.timeout = 5000;

        xhttp.onreadystatechange = function () {
            if (xhttp.readyState === 4 && xhttp.status === 200) {
                var xmlDoc = xhttp.responseXML;
                var dataroot = xmlDoc.querySelectorAll(element);
                ////console.log("colonne", colonne);
                _xml_to_db_(colonne, dataroot, table, function () {
                    console.log("CALLBACK TAVOLI ESEGUITO");
                    callBack(true);
                });
            } else if (xhttp.status === 404)
            {
                //SE NON TROVA IL FILE IL CALLBACK E'FALSO MA COMUNQUE C'E'
                callBack(false);
            }
        };
        //FOLDER E' LA CARTELLA DA BARRA A BARRA '/sdsda/dasdsaD/sada/'

        xhttp.onerror = function (event) {

            alert("ATTENZIONE! Sei fuori portata WiFi. Avvicinati all'antenna e riprova.");

            try {
                $(document).off(comanda.eventino, evento_selettore_tavolo);
            } catch (e) {
                console.log(e);
            }
            $(document).on(comanda.eventino, 'div.tavolo.rett,div.tavolo.cerc', evento_selettore_tavolo);
            $('.tasto_bestellung').css('pointer-events', '');
            $('.tasto_bargeld').css('pointer-events', '');
            $('.tasto_quittung').css('pointer-events', '');
            $('.tasto_rechnung').css('pointer-events', '');

            event.preventDefault();
            //callBack(false);
        };

        xhttp.open("GET", "../.." + folder + file + ((/\?/).test("../.." + folder + file) ? "&" : "?") + (new Date()).getTime(), true);

        xhttp.send();
    };
}
;