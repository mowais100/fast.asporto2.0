/* global dati_testa, parseFloat, comanda, epson, bootbox */

var fattura_elettronica = new Object();
var index_recmessage = 1;

async function documento_commerciale_saldo() {

    let zrep = $("#dc_ndc1").val();
    let          numero_fattura_rif_saldo = $("#dc_ndc2").val();
    let     gg = $("#dc_ddc1").val();
    let    mm = $("#dc_ddc2").val();
    let    aa = $("#dc_ddc3").val();

    let  anno_fattura_rif_saldo = aa + "" + mm + "" + gg;

    comanda.tipo_fattura = "SCONTRINO_SALDO";

    var query_fatture_saldo = "select * from record_teste where misuratore_riferimento='" + numero_layout + "' and progressivo_comanda LIKE '%_" + anno_fattura_rif_saldo + "%' and zrep='" + zrep + "' and numero_fiscale='" + parseInt(numero_fattura_rif_saldo) + "' and tipologia='SCONTRINO' LIMIT 1;";

    comanda.sincro.query_movimenti(query_fatture_saldo, 10000, function (scontrino_acconto) {

        if (scontrino_acconto !== undefined && scontrino_acconto !== null && scontrino_acconto.length > 0) {

            if (scontrino_acconto !== undefined && scontrino_acconto !== null && scontrino_acconto.length > 0) {

                var query_fatture_saldo = "select * from comanda where id='" + scontrino_acconto[0].progressivo_comanda + "' and desc_art LIKE '%ACCONTO%' LIMIT 1;";

                comanda.sincro.query_movimenti(query_fatture_saldo, 10000, function (riga_acconto) {

                    if (riga_acconto !== undefined && riga_acconto !== null && riga_acconto.length > 0) {
                        comanda.importo_acconto_fattura = riga_acconto[0].prezzo_un;
                        comanda.reparto_acconto_fattura = riga_acconto[0].reparto;
                    } else {
                        comanda.importo_acconto_fattura = 0;
                        comanda.reparto_acconto_fattura = "";
                        bootbox.alert("Righe di acconto mancanti nello scontrino indicato. Controlla il numero e l'anno di emissione.");
                        return false;
                    }

                    seleziona_metodo_pagamento('SCONTRINO FISCALE');
                });
            } else {
                bootbox.alert("Scontrino di riferimento non esistente. Controlla il numero e l'anno di emissione.");
                return false;
            }
        }
    });
}

async function annullo_veloce_scontrino(zzzz, nnnn, yyyy, mm, dd) {

    bootbox.alert("Annullamento Scontrino Inviato. Attendere la stampa di conferma.");

    var xml = '<printerFiscalReceipt><printRecMessage  operator="1" message="ANNULLAMENTO N.' + zzzz + '-' + nnnn + ' del ' + dd + '-' + mm + '-' + yyyy + '" messageType="4" /><printRecTotal  operator="1" /></printerFiscalReceipt>';

    var epos = new epson.fiscalPrint();
    epos.send("http://" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=local_printer&timeout=30000", xml, 30000);
    epos.onreceive = function (result, tag_names_array, add_info, res_add) {


    };
}

async function ristampa_veloce_scontrino(zzzz, nnnn, yyyy, mm, dd) {

    bootbox.alert("Ristampa Scontrino Inviata. Attendere la stampa effettiva.");
    

    var xml = '<printerCommand><printContentByNumbers  operator="1" dataType="1" day="' + dd + '" month="' + mm + '" year="' + yyyy.substr(2) + '" fromNumber="' + nnnn + '" toNumber="' + nnnn + '" /></printerCommand>';

    var epos = new epson.fiscalPrint();
    epos.send("http://" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=local_printer&timeout=30000", xml, 30000);
    epos.onreceive = function (result, tag_names_array, add_info, res_add) {


    };
}

async function ristampa_veloce_fattura(zzzz, nnnn, yyyy, mm, dd) {

    bootbox.alert("Ristampa Fattura Inviata. Attendere la stampa effettiva.");

    var xml = '<printerCommand><printContentByNumbers  operator="1" dataType="2" day="' + parseInt(dd) + '" month="' + parseInt(mm) + '" year="' + yyyy.substr(2) + '" fromNumber="' + parseInt(nnnn) + '" toNumber="' + parseInt(nnnn) + '" /></printerCommand>';

    var epos = new epson.fiscalPrint();
    epos.send("http://" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=local_printer&timeout=30000", xml, 30000);
    epos.onreceive = function (result, tag_names_array, add_info, res_add) {


    };
}

function apri_popup_annullo_reso() {
    $("#annullo_reso").modal("show");
}

async function verifica_esistenza_scontrino_riferimento(zzzz, nnnn, dd, mm, yyyy) {
    return new Promise(resolve => {
        var tipo_documento = "2";
        var matricola_fiscale = comanda.matricola_misuratore;
        var dataggmmaaaa = dd + "" + mm + "" + yyyy;
        var numero_documento = nnnn;
        var numero_zreport = zzzz;
        //SOLO RESI
        var valore_reparto_centesimi = "000000000";
        var reparto = "00";
        var query_misuratore = tipo_documento + "" + matricola_fiscale + "" + dataggmmaaaa + "" + numero_documento + "" + numero_zreport + "" + valore_reparto_centesimi + "" + reparto;
        var xml = '<printerCommand><directIO  command="9205" data="' + query_misuratore + '" /></printerCommand>';
        var epos = new epson.fiscalPrint();
        epos.send("http://" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=local_printer&timeout=10000", xml, 10000);
        epos.onreceive = function (result, tag_names_array, add_info) {

            try {
                var risposta_tecnica = add_info.responseData[0];
                var risposta = "";

                switch (risposta_tecnica) {
                    case "0":
                        risposta = true;
                        break;
                    case "1":
                        risposta = "Documento non trovato (di altro RT, MPD / DGFE precedente ecc.)";
                        break;
                    case "2":
                        risposta = "Documento non trovato (ma in range MPD / DGFE, data futura ecc.)";
                        break;
                    case "3":
                        risposta = "Documento gi&agrave; reso";
                        break;
                    case "4":
                        risposta = "Documento gi&agrave; annullato";
                        break;
                    case "5":
                        risposta = "Documento è del tipo 'di reso'";
                        break;
                    case "6":
                        risposta = "Documento è del tipo 'di annullo'";
                        break;
                }

                resolve(risposta);
            } catch (e) {
                bootbox.alert("PRINTER_ERROR. Controllare il formato della data e numeri fiscali.");
            }

        };
        epos.onerror = function (err) {

            /* FUNZIONE NON DISPONIBILE */
            resolve("Errore sconosciuto nella ricerca dei riferimenti fiscali.");

        };
    });
}


async function stampa_ndc(tipo, dati_testa_principali, operazione, matricola) {
    var dati_reparto = new Object();
    var errori = new Array();
    var nome_operazione;
    var annullamento = false;
    if (operazione === 3) {
        nome_operazione = "PRATICA DI RESO ";
    } else {
        //DATI RT

        var zzzz = $("#dc_ndc1").val();
        var nnnn = $("#dc_ndc2").val();
        var dd = $("#dc_ddc1").val();
        var mm = $("#dc_ddc2").val();
        var yyyy = $("#dc_ddc3").val();

        if (zzzz.length === 4) {
            if (!isNaN(zzzz)) {

            } else
            {
                errori.push(1);
            }
        } else {
            errori.push(2);
        }

        if (nnnn.length === 4) {
            if (!isNaN(nnnn)) {

            } else {
                errori.push(3);
            }
        } else {
            errori.push(4);
        }

        if (dd.length === 1 || dd.length === 2) {
            if (!isNaN(dd)) {

            } else {
                errori.push(5);
            }
        } else {
            errori.push(6);
        }

        if (mm.length === 1 || mm.length === 2) {
            if (!isNaN(mm)) {

            } else {
                errori.push(7);
            }
        } else {
            errori.push(8);
        }

        if (yyyy.length === 4) {
            if (!isNaN(yyyy)) {

            } else {
                errori.push(9);
            }
        } else
        {
            errori.push(10);
        }


        if (operazione === 2) {
            nome_operazione = "RESO MERCE N." + zzzz + "-" + nnnn + " del " + dd + "-" + mm + "-" + yyyy + "";
        } else if (operazione === 1) {
            if (matricola !== undefined && matricola.length === 11) {
                nome_operazione = "VOID " + zzzz + " " + nnnn + " " + dd + "" + mm + "" + yyyy + " " + matricola;
            } else {
                nome_operazione = "ANNULLAMENTO N." + zzzz + "-" + nnnn + " del " + dd + "-" + mm + "-" + yyyy + "";
            }
            annullamento = true;
        }
    }


    if (errori.length === 0) {

        if (matricola !== undefined && matricola.length === 11) {
            var esistenza_scontrino = true;
        } else {
            var esistenza_scontrino = await verifica_esistenza_scontrino_riferimento(zzzz, nnnn, dd, mm, yyyy);
        }
        /* --- */
        if (esistenza_scontrino === true) {

            if (comanda.numero_licenza_cliente === "0586") {
                storicizza();
                return true;
            }

            var testa = new Array();
            var corpo = new Array();
            var totali = new Array();
            var pagamento = new Array();
            var o = new Totali();
            var p = new Pagamento();
            salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # STAMPA SCONTRINO # # ");
            index_recmessage = 1;
            $('#tasti_fiscali_da_nascondere', 'body #creazione_comanda').css('pointer-events', 'none');
            $('#tasti_fiscali_da_nascondere', 'body #creazione_comanda').css('opacity', '0.5');
            $('#prodotti', 'body #creazione_comanda').css('opacity', '0.5');
            $('#prodotti', 'body #creazione_comanda').css('pointer-events', 'none');
            test_copertura(function (stato_copertura) {
                var query = "select * from settaggi_profili  where id=" + comanda.folder_number + " ;";
                comanda.sincro.query(query, function (settaggi_profili) {
                    settaggi_profili = settaggi_profili[0];
                    if (stato_copertura === true) {

                        /*globale*/
                        /*var dati_testa = new Object();*/
                        var canvas = document.getElementById("canvas_omaggio");
                        var context = canvas.getContext("2d");
                        var img = document.getElementById("img_omaggio");
                        context.drawImage(img, 0, 0, 429, 856);
                        if (($('#numero_coperti_effettivi').html() <= '0' || $('#numero_coperti_effettivi').html() === undefined) && comanda.coperti_obbligatorio === "1")
                        {
                            var conferma = confirm(comanda.lang[80]); //Attenzione: il numero di coperti selezionato è pari a ZERO. Vuoi procedere ugualmente?
                        } else
                        {
                            var conferma = true;
                        }

                        if (conferma === true) {

                            var form = $('#fattura form');
                            var dati_scontrino = '';
                            //INIZIALIZZAZIONE VAR
                            var stampa = "";
                            var totale = 0.0;
                            if ($(comanda.conto + ' tr[id^="art_"]').length > 0 || annullamento === true) {

                                //PARTE ITALIANA
                                /*var dati_testa = new Object();*/
                                if (comanda.licenza_vera !== 'BrovedaniVerona') {
                                    if (comanda.avviso_righe_prezzo_zero === '1') {
                                        if ($(comanda.conto).not(comanda.intestazione_conto).find('tr[id^="art_"]:contains("€"):contains("€0.00")').not('.non-contare-riga').length > 0)
                                        {
                                            bootbox.confirm("Ci sono righe a prezzo zero. Vuoi continuare a stampare lo scontrino?", function (a) {

                                                if (a !== true) {
                                                    return false;
                                                }

                                            });
                                        }
                                    }
                                }
                                dati_testa.resto = '0.00';
                                dati_testa.totale_buoni_acquisto = 0;
                                dati_testa.totale_buoni_acquisto_emessi = 0;
                                dati_testa.totale_buoni_pasto = 0;
                                dati_testa.numero_buoni = 0;
                                dati_testa.nome_buono = '';
                                dati_testa.buoni_pasto = '0';
                                dati_testa.numero_buoni_pasto = '0';
                                dati_testa.nome_buono_2 = '';
                                dati_testa.buoni_pasto_2 = '0';
                                dati_testa.numero_buoni_pasto_2 = '0';
                                dati_testa.nome_buono_3 = '';
                                dati_testa.buoni_pasto_3 = '0';
                                dati_testa.numero_buoni_pasto_3 = '0';
                                dati_testa.nome_buono_4 = '';
                                dati_testa.buoni_pasto_4 = '0';
                                dati_testa.numero_buoni_pasto_4 = '0';
                                dati_testa.nome_buono_5 = '';
                                dati_testa.buoni_pasto_5 = '0';
                                dati_testa.numero_buoni_pasto_5 = '0';
                                var i = 1;
                                if (buoni_memorizzati.length >= 1) {
                                    buoni_memorizzati.forEach(function (val, index) {
                                        switch (i) {
                                            case 1:
                                                dati_testa.nome_buono = val.id_circuito;
                                                dati_testa.buoni_pasto = val.importo_taglio;
                                                dati_testa.numero_buoni_pasto = val.quantita;
                                                //SOLO PER USO INTERNO ALLO SCONTRINO
                                                dati_testa.totale_buoni_pasto += parseFloat(val.importo_taglio) * parseInt(val.quantita);
                                                dati_testa.numero_buoni += parseInt(val.quantita);
                                                break;
                                            case 2:
                                                dati_testa.nome_buono_2 = val.id_circuito;
                                                dati_testa.buoni_pasto_2 = val.importo_taglio;
                                                dati_testa.numero_buoni_pasto_2 = val.quantita;
                                                //SOLO PER USO INTERNO ALLO SCONTRINO
                                                dati_testa.totale_buoni_pasto += parseFloat(val.importo_taglio) * parseInt(val.quantita);
                                                dati_testa.numero_buoni += parseInt(val.quantita);
                                                break;
                                            case 3:
                                                dati_testa.nome_buono_3 = val.id_circuito;
                                                dati_testa.buoni_pasto_3 = val.importo_taglio;
                                                dati_testa.numero_buoni_pasto_3 = val.quantita;
                                                //SOLO PER USO INTERNO ALLO SCONTRINO
                                                dati_testa.totale_buoni_pasto += parseFloat(val.importo_taglio) * parseInt(val.quantita);
                                                dati_testa.numero_buoni += parseInt(val.quantita);
                                                break;
                                            case 4:
                                                dati_testa.nome_buono_4 = val.id_circuito;
                                                dati_testa.buoni_pasto_4 = val.importo_taglio;
                                                dati_testa.numero_buoni_pasto_4 = val.quantita;
                                                //SOLO PER USO INTERNO ALLO SCONTRINO
                                                dati_testa.totale_buoni_pasto += parseFloat(val.importo_taglio) * parseInt(val.quantita);
                                                dati_testa.numero_buoni += parseInt(val.quantita);
                                                break;
                                            case 5:
                                                dati_testa.nome_buono_5 = val.id_circuito;
                                                dati_testa.buoni_pasto_5 = val.importo_taglio;
                                                dati_testa.numero_buoni_pasto_5 = val.quantita;
                                                //SOLO PER USO INTERNO ALLO SCONTRINO
                                                dati_testa.totale_buoni_pasto += parseFloat(val.importo_taglio) * parseInt(val.quantita);
                                                dati_testa.numero_buoni += parseInt(val.quantita);
                                                break;
                                        }
                                        i++;
                                    });
                                }

                                var totale_conto = parseFloat($(comanda.totale_scontrino).html());
                                dati_testa.buoniacquisto = 0;
                                var buoni_spesa = $('#scelta_buoni [name="scelta_buoni__buono_spesa"]').val();
                                if (parseFloat(buoni_spesa) > 0) {
                                    dati_testa.buoniacquisto = parseFloat(buoni_spesa);
                                }

                                var restante = totale_conto - dati_testa.totale_buoni_pasto - dati_testa.buoniacquisto;
                                //Per l'iva, ma da sistemare'
                                //La soluzione pi� pratica � inserire l'iva in una class per riga per ogni prodotto
                                //Di default IVA 10 dato che si tratta di ristorazione
                                var department = '1';
                                var p1 = new Promise(function (resolve, reject) {

                                    resolve("0");
                                });
                                p1.then(function (numero_fattura) {

                                    var numero_scontrino = "ND";
                                    var promessa = new Promise(function (resolve, reject) {
                                        if (restante < 0 && tipo === "SCONTRINO FISCALE") {
                                            resolve(true);
                                        } else if (tipo === "SCONTRINO FISCALE") {
                                            resolve(false);
                                        } else
                                        {
                                            resolve(false);
                                        }
                                    });
                                    promessa.then(function (extra_emesso) {


                                        //INIZIO
                                        var totale = 0;
                                        dati_scontrino += '<printerFiscalReceipt>';
                                        dati_scontrino += '<printRecMessage  ' + comanda.istr_cass["operator"] + '="1" message="' + nome_operazione + '" messageType="4" />';
                                        if (comanda.cassa_intellinet === "N") {
                                            dati_scontrino += '<Printer Num="1" />';
                                        }



                                        //COPERTI
                                        $(comanda.intestazione_conto).find('tr:contains("COPERTI"),tr:contains("GEDECKE")').each(function () {
                                            var descrizione = $(this).find('td:nth-child(2)').html().replace(/&nbsp;/g, "").replace(/&nbsp;/g, "");
                                            var quantita = $(this).find('td:nth-child(1)').html();
                                            var prezzo = parseFloat($(this).find('td:nth-child(4)').html().replace(',', '.').replace('-', '').replace('€', ''));
                                            department = $(this)[0].className.match(/rep_\w+/gi)[0].substr(4);
                                            totale += prezzo * parseFloat(quantita);
                                            var prezzo_print = prezzo.toFixed(2);
                                            if (comanda.cassa_intellinet === "N") {
                                                prezzo_print = prezzo_print.replace('.', ',');
                                            }
                                            dati_scontrino += '<printRecRefund  ' + comanda.istr_cass["operator"] + '="1" ' + comanda.istr_cass["description"] + '="' + descrizione + '" ' + comanda.istr_cass["quantity"] + '="' + quantita + '" ' + comanda.istr_cass["unitPrice"] + '="' + prezzo_print + '" ' + comanda.istr_cass["department"] + '="' + department + '" ' + comanda.istr_cass["justification"] + '="1" />';

                                            var codice_reparto = parseInt(department);
                                            var iva_reparto = parseInt(REPARTO_controller.iva_reparto(parseInt(REPARTO_controller.indice(parseInt(department)))));

                                            if (dati_reparto[codice_reparto] === undefined) {
                                                dati_reparto[codice_reparto] = new Object();
                                                dati_reparto[codice_reparto].percentuale_iva = iva_reparto;
                                                dati_reparto[codice_reparto].codice_reparto = parseInt(department);
                                                dati_reparto[codice_reparto].totale_per_reparto = 0;
                                                dati_reparto[codice_reparto].imponibile = 0;
                                                dati_reparto[codice_reparto].imposta = 0;
                                                dati_reparto[codice_reparto].totale_per_reparto = 0;
                                                dati_reparto[codice_reparto].percentuale_iva = parseInt(REPARTO_controller.iva_reparto(parseInt(REPARTO_controller.indice(parseInt(department)))));
                                                dati_reparto[codice_reparto].quoziente_scorporo = iva_reparto / 100 + 1;
                                            }

                                            dati_reparto[codice_reparto].imponibile += (100 * (parseFloat(prezzo) * parseInt(quantita))) / (100 + iva_reparto);
                                            dati_reparto[codice_reparto].imposta += (parseFloat(prezzo) * parseInt(quantita)) - ((100 * (parseFloat(prezzo) * parseInt(quantita))) / (100 + iva_reparto));
                                            dati_reparto[codice_reparto].totale_per_reparto += parseFloat(prezzo) * parseInt(quantita);
                                        });
                                        //ARTICOLI ESCLUSI QUELLI A PREZZO ZERO

                                        var totale_articoli = 0.00;
                                        $(comanda.conto).not(comanda.intestazione_conto).find('tr[id^="art_"]:contains("€")').not('.non-contare-riga').each(function () {
                                            var descrizione = $(this).find('td:nth-child(2)').html().replace(/&nbsp;/g, "");
                                            var quantita = $(this).find('td:nth-child(1)').html();
                                            var prezzo = parseFloat($(this).find('td:nth-child(4)').html().replace('€', '').replace(',', '.'));
                                            department = $(this)[0].className.match(/rep_\w+/gi)[0].substr(4);
                                            if (descrizione.indexOf('SERVIZIO') !== -1) {
                                                var form = "categoria=" + comanda.ultima_categoria_principale + "&descrizione=" + descrizione + "&prezzo_1=" + prezzo + "&iva=" + comanda.percentuale_iva_default + "&dest_stampa=0&portata=N";
                                                aggiungi_articolo(null, descrizione, '&euro;' + prezzo, form, '1', undefined, undefined, undefined, undefined, comanda.percentuale_iva_default, comanda.percentuale_iva_takeaway, true);
                                            }

                                            console.log(descrizione, quantita, prezzo);
                                            if ($('.tasto_menu_del_giorno')[0] === undefined || $('.tasto_menu_del_giorno:checked')[0] === undefined) {

                                                if (descrizione[0] === "+" || descrizione[0] === "-" || descrizione[0] === "=" || descrizione[0] === "(" || descrizione[0] === "*" || descrizione[0] === "x")
                                                {
                                                    descrizione = '  ' + descrizione;
                                                }

                                                if (prezzo === 0 && $(this).hasClass('cod_promo_V_1') === true) {
                                                    var prezzo_print = prezzo.toFixed(2);
                                                    if (comanda.cassa_intellinet === "N") {
                                                        prezzo_print = prezzo_print.replace('.', ',');
                                                    }

                                                    dati_scontrino += '<printRecRefund  ' + comanda.istr_cass["operator"] + '="1" ' + comanda.istr_cass["description"] + '="' + filtra_accenti("..." + descrizione.trim().substr(1)) + '" ' + comanda.istr_cass["quantity"] + '="' + quantita + '" ' + comanda.istr_cass["unitPrice"] + '="' + prezzo_print + '" ' + comanda.istr_cass["department"] + '="' + department + '" ' + comanda.istr_cass["justification"] + '="1" />';
                                                } else if (prezzo > 0) {

                                                    var codice_reparto = parseInt(department);
                                                    var iva_reparto = parseInt(REPARTO_controller.iva_reparto(parseInt(REPARTO_controller.indice(parseInt(department)))));

                                                    if (dati_reparto[codice_reparto] === undefined) {
                                                        dati_reparto[codice_reparto] = new Object();
                                                        dati_reparto[codice_reparto].percentuale_iva = iva_reparto;
                                                        dati_reparto[codice_reparto].codice_reparto = parseInt(department);
                                                        dati_reparto[codice_reparto].totale_per_reparto = 0;
                                                        dati_reparto[codice_reparto].imponibile = 0;
                                                        dati_reparto[codice_reparto].imposta = 0;
                                                        dati_reparto[codice_reparto].totale_per_reparto = 0;
                                                        dati_reparto[codice_reparto].percentuale_iva = parseInt(REPARTO_controller.iva_reparto(parseInt(REPARTO_controller.indice(parseInt(department)))));
                                                        dati_reparto[codice_reparto].quoziente_scorporo = iva_reparto / 100 + 1;
                                                    }

                                                    dati_reparto[codice_reparto].imponibile += (100 * (parseFloat(prezzo) * parseInt(quantita))) / (100 + iva_reparto);
                                                    dati_reparto[codice_reparto].imposta += (parseFloat(prezzo) * parseInt(quantita)) - ((100 * (parseFloat(prezzo) * parseInt(quantita))) / (100 + iva_reparto));
                                                    dati_reparto[codice_reparto].totale_per_reparto += parseFloat(prezzo) * parseInt(quantita);

                                                    var prezzo_print = prezzo.toFixed(2);
                                                    if (comanda.cassa_intellinet === "N") {
                                                        prezzo_print = prezzo_print.replace('.', ',');
                                                    }
                                                    dati_scontrino += '<printRecRefund  ' + comanda.istr_cass["operator"] + '="1" ' + comanda.istr_cass["description"] + '="' + filtra_accenti(descrizione) + '" ' + comanda.istr_cass["quantity"] + '="' + quantita + '" ' + comanda.istr_cass["unitPrice"] + '="' + prezzo_print + '" ' + comanda.istr_cass["department"] + '="' + department + '" ' + comanda.istr_cass["justification"] + '="1" />';
                                                    //CALCOLO TOT
                                                    totale += prezzo * parseFloat(quantita);


                                                } else if (prezzo < 0) {

                                                    var codice_reparto = parseInt(department);
                                                    var iva_reparto = parseInt(REPARTO_controller.iva_reparto(parseInt(REPARTO_controller.indice(parseInt(department)))));

                                                    if (dati_reparto[codice_reparto] === undefined) {
                                                        dati_reparto[codice_reparto] = new Object();
                                                        dati_reparto[codice_reparto].percentuale_iva = iva_reparto;
                                                        dati_reparto[codice_reparto].codice_reparto = parseInt(department);
                                                        dati_reparto[codice_reparto].totale_per_reparto = 0;
                                                        dati_reparto[codice_reparto].imponibile = 0;
                                                        dati_reparto[codice_reparto].imposta = 0;
                                                        dati_reparto[codice_reparto].totale_per_reparto = 0;
                                                        dati_reparto[codice_reparto].percentuale_iva = parseInt(REPARTO_controller.iva_reparto(parseInt(REPARTO_controller.indice(parseInt(department)))));
                                                        dati_reparto[codice_reparto].quoziente_scorporo = iva_reparto / 100 + 1;
                                                    }

                                                    dati_reparto[codice_reparto].imponibile += (100 * (parseFloat(prezzo) * parseInt(quantita))) / (100 + iva_reparto);
                                                    dati_reparto[codice_reparto].imposta += (parseFloat(prezzo) * parseInt(quantita)) - ((100 * (parseFloat(prezzo) * parseInt(quantita))) / (100 + iva_reparto));
                                                    dati_reparto[codice_reparto].totale_per_reparto += parseFloat(prezzo) * parseInt(quantita);

                                                    totale += prezzo * parseFloat(quantita);
                                                    prezzo = parseFloat(prezzo.toString().replace('-', '')) * parseInt(quantita);
                                                    var prezzo_print = prezzo.toFixed(2);
                                                    if (comanda.cassa_intellinet === "N") {
                                                        prezzo_print = prezzo_print.replace('.', ',');
                                                    }
                                                    //dati_scontrino += '<printRecItemAdjustment ' + comanda.istr_cass["operator"] + '="1" ' + comanda.istr_cass["adjustmentType"] + '="3" ' + comanda.istr_cass["description"] + '="' + descrizione + '" amount="' + prezzo_print + '" ' + comanda.istr_cass["department"] + '="' + department + '" ' + comanda.istr_cass["justification"] + '="1" />';
                                                }
                                            } else {
                                                if (prezzo > 0) {

                                                    var codice_reparto = parseInt(department);
                                                    var iva_reparto = parseInt(REPARTO_controller.iva_reparto(parseInt(REPARTO_controller.indice(parseInt(department)))));

                                                    if (dati_reparto[codice_reparto] === undefined) {
                                                        dati_reparto[codice_reparto] = new Object();
                                                        dati_reparto[codice_reparto].percentuale_iva = iva_reparto;
                                                        dati_reparto[codice_reparto].codice_reparto = parseInt(department);
                                                        dati_reparto[codice_reparto].totale_per_reparto = 0;
                                                        dati_reparto[codice_reparto].imponibile = 0;
                                                        dati_reparto[codice_reparto].imposta = 0;
                                                        dati_reparto[codice_reparto].totale_per_reparto = 0;
                                                        dati_reparto[codice_reparto].percentuale_iva = parseInt(REPARTO_controller.iva_reparto(parseInt(REPARTO_controller.indice(parseInt(department)))));
                                                        dati_reparto[codice_reparto].quoziente_scorporo = iva_reparto / 100 + 1;
                                                    }

                                                    dati_reparto[codice_reparto].imponibile += (100 * (parseFloat(prezzo) * parseInt(quantita))) / (100 + iva_reparto);
                                                    dati_reparto[codice_reparto].imposta += (parseFloat(prezzo) * parseInt(quantita)) - ((100 * (parseFloat(prezzo) * parseInt(quantita))) / (100 + iva_reparto));
                                                    dati_reparto[codice_reparto].totale_per_reparto += parseFloat(prezzo) * parseInt(quantita);

                                                    totale_articoli += prezzo * parseFloat(quantita);
                                                } else if (prezzo < 0) {
                                                    var codice_reparto = parseInt(department);
                                                    var iva_reparto = parseInt(REPARTO_controller.iva_reparto(parseInt(REPARTO_controller.indice(parseInt(department)))));

                                                    if (dati_reparto[codice_reparto] === undefined) {
                                                        dati_reparto[codice_reparto] = new Object();
                                                        dati_reparto[codice_reparto].percentuale_iva = iva_reparto;
                                                        dati_reparto[codice_reparto].codice_reparto = parseInt(department);
                                                        dati_reparto[codice_reparto].totale_per_reparto = 0;
                                                        dati_reparto[codice_reparto].imponibile = 0;
                                                        dati_reparto[codice_reparto].imposta = 0;
                                                        dati_reparto[codice_reparto].totale_per_reparto = 0;
                                                        dati_reparto[codice_reparto].percentuale_iva = parseInt(REPARTO_controller.iva_reparto(parseInt(REPARTO_controller.indice(parseInt(department)))));
                                                        dati_reparto[codice_reparto].quoziente_scorporo = iva_reparto / 100 + 1;
                                                    }

                                                    dati_reparto[codice_reparto].imponibile += (100 * (parseFloat(prezzo) * parseInt(quantita))) / (100 + iva_reparto);
                                                    dati_reparto[codice_reparto].imposta += (parseFloat(prezzo) * parseInt(quantita)) - ((100 * (parseFloat(prezzo) * parseInt(quantita))) / (100 + iva_reparto));
                                                    dati_reparto[codice_reparto].totale_per_reparto += parseFloat(prezzo) * parseInt(quantita);

                                                    totale_articoli += prezzo * parseFloat(quantita);
                                                }
                                            }
                                        });
                                        if ($('.tasto_menu_del_giorno')[0] !== undefined && $('.tasto_menu_del_giorno:checked')[0] !== undefined) {



                                            var qta_menu_fisso = "1";
                                            if (!isNaN(parseInt($("#campo_qta_menu_fisso").val()))) {
                                                qta_menu_fisso = $("#campo_qta_menu_fisso").val();
                                            }


                                            var prezzo_print = (totale_articoli / qta_menu_fisso).toFixed(2);
                                            if (comanda.cassa_intellinet === "N") {
                                                prezzo_print = prezzo_print.replace('.', ',');
                                            }


                                            totale += totale_articoli;
                                            dati_scontrino += '<printRecRefund  ' + comanda.istr_cass["operator"] + '="1" ' + comanda.istr_cass["description"] + '="MENU COMPLETO" ' + comanda.istr_cass["quantity"] + '="' + qta_menu_fisso + '" ' + comanda.istr_cass["unitPrice"] + '="' + prezzo_print + '" ' + comanda.istr_cass["department"] + '="1" ' + comanda.istr_cass["justification"] + '="1" />';
                                            $('.tasto_menu_del_giorno').prop('checked', false);

                                            var prezzo = prezzo_print;
                                            var quantita = qta_menu_fisso;

                                            var codice_reparto = parseInt(department);
                                            var iva_reparto = parseInt(REPARTO_controller.iva_reparto(parseInt(REPARTO_controller.indice(parseInt(department)))));

                                            if (dati_reparto[codice_reparto] === undefined) {
                                                dati_reparto[codice_reparto] = new Object();
                                                dati_reparto[codice_reparto].percentuale_iva = iva_reparto;
                                                dati_reparto[codice_reparto].codice_reparto = parseInt(department);
                                                dati_reparto[codice_reparto].totale_per_reparto = 0;
                                                dati_reparto[codice_reparto].imponibile = 0;
                                                dati_reparto[codice_reparto].imposta = 0;
                                                dati_reparto[codice_reparto].totale_per_reparto = 0;
                                                dati_reparto[codice_reparto].percentuale_iva = parseInt(REPARTO_controller.iva_reparto(parseInt(REPARTO_controller.indice(parseInt(department)))));
                                                dati_reparto[codice_reparto].quoziente_scorporo = iva_reparto / 100 + 1;
                                            }

                                            dati_reparto[codice_reparto].imponibile += (100 * (parseFloat(prezzo) * parseInt(quantita))) / (100 + iva_reparto);
                                            dati_reparto[codice_reparto].imposta += (parseFloat(prezzo) * parseInt(quantita)) - ((100 * (parseFloat(prezzo) * parseInt(quantita))) / (100 + iva_reparto));
                                            dati_reparto[codice_reparto].totale_per_reparto += parseFloat(prezzo) * parseInt(quantita);

                                        }

                                        //SCONTI FISSI
                                        $(comanda.intestazione_conto + " tr").not(':contains("%")').not(':contains(' + comanda.lang_stampa[15] + ')').each(function () {

                                            //var descrizione = $(this).find('td:nth-child(2)').html().replace(/&nbsp;/g, "");

                                            var quantita = $(this).find('td:nth-child(1)').html();
                                            var prezzo = parseFloat($(this).find('td:nth-child(4)').html().replace(',', '.').replace('-', ''));
                                            department = $(this)[0].className.match(/rep_\w+/gi)[0].substr(4);
                                            totale += (prezzo * parseFloat(quantita)) * -1;
                                            dati_scontrino += '<printRecSubtotal  ' + comanda.istr_cass["operator"] + '="1" ' + comanda.istr_cass["option"] + '="0" />';
                                            var prezzo_print = prezzo.toFixed(2);
                                            if (comanda.cassa_intellinet === "N") {
                                                prezzo_print = prezzo_print.replace('.', ',');
                                            }
                                            var descrizione = "SCONTO " + prezzo_print + " EURO";
                                            //dati_scontrino += '<printRecItemAdjustment  ' + comanda.istr_cass["operator"] + '="1"  ' + comanda.istr_cass["adjustmentType"] + '="3" ' + comanda.istr_cass["description"] + '="' + descrizione + '" amount="' + prezzo_print + '" ' + comanda.istr_cass["department"] + '="' + department + '" ' + comanda.istr_cass["justification"] + '="1" />';
                                        });
                                        //SCONTI PERCENTUALI
                                        var percentuale_sconto = 0;
                                        var testo = '';
                                        //PARTE DEGLI SCONTI PERCENTUALI
                                        $(comanda.intestazione_conto).find('tr:contains("%")').each(function () {
                                            var prezzo = parseFloat($(this).find('td:nth-child(4)').html().slice(0, -1));
                                            department = $(this)[0].className.match(/rep_\w+/gi)[0].substr(4);
                                            //testo = $(this).find('td:nth-child(2)').html();

                                            //CALCOLO TOT
                                            percentuale_sconto += prezzo;
                                            testo = "SCONTO " + percentuale_sconto.toString().substr(1) + "%";
                                        });
                                        var differenza = parseFloat((totale / 100 * percentuale_sconto) * -1).toFixed(2);
                                        if (differenza > 0)
                                        {
                                            dati_scontrino += '<printRecSubtotal  ' + comanda.istr_cass["operator"] + '="1" ' + comanda.istr_cass["option"] + '="0" />';
                                            var prezzo_print = parseFloat(differenza).toFixed(2);
                                            if (comanda.cassa_intellinet === "N") {
                                                prezzo_print = prezzo_print.replace('.', ',');
                                            }
                                            //dati_scontrino += '<printRecItemAdjustment ' + comanda.istr_cass["operator"] + '="1" ' + comanda.istr_cass["adjustmentType"] + '="3" ' + comanda.istr_cass["description"] + '="' + testo + '" amount="' + prezzo_print + '" ' + comanda.istr_cass["department"] + '="' + department + '" ' + comanda.istr_cass["justification"] + '="1" />';
                                        }



                                        totale -= differenza;
                                        totale = parseFloat(totale).toFixed(2);
                                        if (extra_emesso === true) {
                                            dati_scontrino += '<printRecSubtotal  ' + comanda.istr_cass["operator"] + '="1" ' + comanda.istr_cass["option"] + '="0" />';
                                            var prezzo_print = parseFloat(restante * -1).toFixed(2);
                                            if (comanda.cassa_intellinet === "N") {
                                                prezzo_print = prezzo_print.replace('.', ',');
                                            }
                                            dati_scontrino += '<printRecRefund  ' + comanda.istr_cass["operator"] + '="1" ' + comanda.istr_cass["description"] + '="EXTRA" ' + comanda.istr_cass["quantity"] + '="1" ' + comanda.istr_cass["unitPrice"] + '="' + prezzo_print + '" ' + comanda.istr_cass["department"] + '="' + department + '" ' + comanda.istr_cass["justification"] + '="1" />';
                                        }


                                        var pagamento_ricevuto = $('[name="soldi_ricevuti"]').val();
                                        if (pagamento_ricevuto === undefined || pagamento_ricevuto === "") {
                                            pagamento_ricevuto = totale;
                                        } else
                                        {
                                            $('#calcola_resto [name=\'importo_da_saldare\']').val(totale);
                                            dati_testa.resto = ($('#calcola_resto [name=\'soldi_ricevuti\']').val() - $('#calcola_resto [name=\'importo_da_saldare\']').val()).toFixed(2);
                                        }


                                        dati_testa.contanti = '0.00';
                                        dati_testa.pos = '0.00';
                                        dati_testa.droppay = '0.00';
                                        dati_testa.bancomat = '0.00';
                                        dati_testa.carte_credito = '0.00';
                                        dati_testa.locale = comanda.locale;
                                        //APERTURA AUTOMATICA CASSETTO


                                        tipo = "SCONTRINO";
                                        dati_testa.tipo = tipo;
                                        dati_testa.totale = parseFloat($(comanda.totale_scontrino).html()).toFixed(2);
                                        dati_testa.conto = comanda.conto;
                                        dati_testa.tavolo = comanda.tavolo;
                                        dati_testa.operatore = comanda.operatore;
                                        if (dati_testa_principali !== undefined && typeof dati_testa_principali === "object")
                                        {
                                            dati_testa.id_cliente = dati_testa_principali.id_cliente;
                                        }

                                        $('[name="soldi_ricevuti"]').val('');
                                        console.log("DATI SCONTRINO", dati_scontrino, totale);
                                        //if (parseFloat(totale) > 0) {
                                        assegna_progressivo_conto_incassato(function () {

                                            console.log(comanda.nome_stampante);
                                            console.log(tipo);
                                            console.log(comanda.nome_stampante[tipo]);
                                            console.log(comanda.nome_stampante[tipo].nome);
                                            var sconto_perc = 0;
                                            var sconto_imp = 0;
                                            var tipo_sconto = 'N';
                                            //SCONTI PERCENTUALI
                                            $(comanda.intestazione_conto + ' tr:contains(%)').not(':contains(' + comanda.lang[15] + ')').each(function (index, element) {
                                                sconto_perc += parseFloat($(element).find('td:nth-child(4)').html().slice(1, -1).replace(',', '.'));
                                                console.log("CALCOLO SCONTISTICA TOTALI", $(element).find('td:nth-child(4)').html().slice(1, -1), typeof ($(element).find('td:nth-child(4)').html().slice(1, -1)));
                                                tipo_sconto = 'P';
                                            });
                                            //SCONTI IMPONIBILI
                                            $(comanda.intestazione_conto + ' tr').not(':contains(%)').not(':contains(' + comanda.lang[15] + ')').each(function (index, element) {
                                                sconto_imp += parseFloat($(element).find('td:nth-child(4)').html().substr(1).replace(',', '.'));
                                                console.log("CALCOLO SCONTISTICA TOTALI", $(element).find('td:nth-child(4)').html(), typeof ($(element).find('td:nth-child(4)').html()));
                                                tipo_sconto = 'F';
                                            });
                                            //CALCOLO PERCENTUALE IN BASE ALLO SCONTO FISSO
                                            var netto_dopo_sconto = parseFloat($('#totale_scontrino').html().replace(',', '.')).toFixed(2);
                                            var totale = parseFloat(netto_dopo_sconto) + sconto_imp;
                                            //Lo sommo alla percentuale che c'era già quindi 0
                                            sconto_perc += 100 / (totale / sconto_imp);
                                            if (tipo_sconto === 'P' && isNaN(sconto_perc) === true) {
                                                sconto_perc = 100;
                                            } else if (tipo_sconto === 'N') {
                                                sconto_perc = 0;
                                            }



                                            console.log("CALCOLO SCONTISTICA TOTALI", totale, sconto_imp, sconto_perc, 100 / (totale / sconto_imp));
                                            var sconto_netto = netto_dopo_sconto / 100 * sconto_perc;
                                            if (sconto_imp > 0)
                                            {
                                                sconto_netto = sconto_imp;
                                            }

                                            dati_testa.sconto_netto = parseFloat(sconto_netto).toFixed(2);
                                            dati_testa.sconto_perc = parseFloat(sconto_perc).toFixed(2);
                                            dati_testa.sconto_imp = tipo_sconto;
                                            dati_testa.tavolotxt = comanda.tavolo;
                                            id_comanda_attuale(function (data) {
                                                if (data !== false) {
                                                    dati_testa.id_comanda = data;
                                                    dati_testa.posizione_conto = comanda.conto;
                                                    var data = comanda.funzionidb.data_attuale().substr(0, 8);
                                                    var ora = comanda.funzionidb.data_attuale().substr(9, 8);
                                                    eventuale_pagamento_droppay(dati_testa.droppay, function () {
                                                        //INGENICO POS
                                                        ing.abilita_stampa_ECR(dati_testa.pos, function (dati_pos) {


                                                            dati_scontrino += '<printRecTotal  ' + comanda.istr_cass["operator"] + '="1" />';
                                                            dati_scontrino += '<' + comanda.istr_cass["endFiscalReceipt"] + ' ' + comanda.istr_cass["operator"] + '="1" />';
                                                            dati_scontrino += '</printerFiscalReceipt>';
                                                            dati_scontrino = dati_scontrino.replace(/&/gi, '&amp;');
                                                            if (comanda.cassa_intellinet === "N") {
                                                                dati_scontrino = '<?xml version="1.0" encoding="utf-16"?>\n' + dati_scontrino;
                                                            } else {
                                                                dati_scontrino = '<?xml version="1.0" encoding="utf-8"?>\n' +
                                                                        '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">\n' +
                                                                        '<s:Body>\n' + dati_scontrino +
                                                                        '</s:Body>\n' +
                                                                        '</s:Envelope>\n';
                                                            }


                                                            $('span.nome_parcheggio').html('0');
                                                            comanda.popup_modifica_articolo = false;
                                                            $('#conto_grande').modal('hide');
                                                            socket_fast_split.send(JSON.stringify({scontrino: dati_testa.totale}));
                                                            if (dati_testa.tipo === "FATTURA") {

                                                                var p = new Pagamento();
                                                                p.C241 = "TP02"; /* Condizioni Pagamento */
                                                                p.C2422 = "MP01"; /* Modalità Pagamento */
                                                                p.C2426 = parseFloat(dati_testa.totale).toFixed(2); /* Importo Pagamento */
                                                                pagamento.push(p);
                                                                var cliente = alasql("select * from clienti where id=" + dati_testa.id_cliente + " limit 1;");
                                                                var t = new Testa();
                                                                /* Intermediario Trasmissione Fattura */
                                                                t.C1111; /* Id Paese + */
                                                                t.C1112; /* Id Codice + */
                                                                t.C112 = " "; /* Progressivo Invio + */

                                                                /* FPA12 per pubblica amministrazione */
                                                                if (cliente[0].formato_trasmissione === "PA") {
                                                                    t.C113 = "FPA12";
                                                                } else
                                                                {
                                                                    t.C113 = "FPR12"; /* Formato Trasmissione + */
                                                                }
                                                                /* ANAGRAFICA DA COMPILARE */
                                                                if (cliente[0].codice_destinatario === "") {
                                                                    t.C114 = "0000000"; /* Codice Destinatario + */
                                                                } else {
                                                                    t.C114 = cliente[0].codice_destinatario; /* Codice Destinatario + */
                                                                }
                                                                t.C116 = cliente[0].pec; /* PEC Destinatario + */

                                                                /* Ristoratore */
                                                                t.C12111 = comanda.profilo_aziendale.id_nazione; /* Id Paese + */

                                                                if (comanda.profilo_aziendale.partita_iva.length === 11) {
                                                                    t.C12112 = comanda.profilo_aziendale.partita_iva; /* Id Codice + */
                                                                } else
                                                                {
                                                                    t.C12112 = comanda.profilo_aziendale.codice_fiscale;
                                                                }
                                                                t.C12131 = comanda.profilo_aziendale.ragione_sociale; /* Denominazione + */
                                                                t.C1218; /* Regime Fiscale + */
                                                                t.C1221 = comanda.profilo_aziendale.indirizzo + ", " + comanda.profilo_aziendale.numero; /* Indirizzo + */
                                                                t.C1223 = comanda.profilo_aziendale.cap; /* CAP + */
                                                                t.C1224 = comanda.profilo_aziendale.comune; /* Comune + */
                                                                t.C1225 = comanda.profilo_aziendale.provincia; /* Provincia + */
                                                                t.C1226 = comanda.profilo_aziendale.nazione; /* Nazione + */

                                                                /* Consumatore Finale (cliente del cliente) */

                                                                t.C14111 = cliente[0].id_nazione;
                                                                if (cliente[0].partita_iva.length === 11) {
                                                                    t.C14112 = cliente[0].partita_iva; /* Id Codice + */
                                                                } else
                                                                {
                                                                    t.C14112 = cliente[0].codice_fiscale;
                                                                }
                                                                t.C14131 = cliente[0].ragione_sociale; /* Denominazione + */
                                                                t.C1421 = cliente[0].indirizzo + ", " + cliente[0].numero; /* Indirizzo + */
                                                                t.C1423 = cliente[0].cap; /* CAP + */
                                                                t.C1424 = cliente[0].comune; /* Comune + */
                                                                t.C1425 = cliente[0].provincia; /* Provincia + */
                                                                t.C1426 = cliente[0].nazione; /* Nazione + */

                                                                /* Per fatture nelle fatture (non utilizzato nella ristorazione) */
                                                                t.C15111 = cliente[0].id_nazione; /* Id Paese */
                                                                t.C15112; /* Id Codice */
                                                                t.C15131; /* Denominazione */

                                                                /* Dati fiscali */
                                                                t.C2111 = "TD01"; /* Tipo Documento + */
                                                                t.C2112 = "EUR"; /* Divisa + */
                                                                var a = new Date();
                                                                t.C2113 = a.format("yyyy-mm-dd"); /* Data + */

                                                                /* NUMERO FISCALE */
                                                                t.C2114 = ""; /* Numero + */

                                                                /* Per ritenute */
                                                                t.C21151; /* Tipo Ritenuta */
                                                                t.C21152; /* Importo Ritenuta */
                                                                t.C21153; /* Aliquota Ritenuta */
                                                                t.C21154; /* Causale Pagamento */

                                                                /* SC SE CE SCONTO */

                                                                t.C21181 = ""; /* Tipo */
                                                                if (sconto_netto > 0) {
                                                                    t.C21181 = "SC";
                                                                }
                                                                t.C21182; /* Percentuale */

                                                                /* IMPORTO SCONTO SE CE */
                                                                t.C21183 = ""; /* Importo */
                                                                t.C21182 = ""; /* Ripartizione Sconto */
                                                                if (sconto_netto > 0) {

                                                                    corpo.forEach(function (v) {
                                                                        //v.C2219 =(parseFloat(v.C2219) - (parseFloat(v.C2219) / 100 * parseFloat(dati_testa.sconto_perc))).toFixed(2);
                                                                        v.C22111 = (parseFloat(v.C22111) - (parseFloat(v.C22111) / 100 * parseFloat(dati_testa.sconto_perc))).toFixed(2);
                                                                    });

                                                                    for (let codice_reparto = 1; codice_reparto <= 5; codice_reparto++) {
                                                                        if (parseFloat(dati_reparto[codice_reparto].imponibile) > 0) {
                                                                            dati_reparto[codice_reparto].imponibile = (parseFloat(dati_reparto[codice_reparto].imponibile) - (parseFloat(dati_reparto[codice_reparto].imponibile) / 100 * parseFloat(dati_testa.sconto_perc))).toFixed(2);
                                                                        }
                                                                    }

                                                                    totali = new Array();
                                                                    /* Tabella Totali OK */
                                                                    for (let codice_reparto = 1; codice_reparto <= 5; codice_reparto++) {
                                                                        if (dati_reparto[codice_reparto] !== undefined && dati_reparto[codice_reparto].imponibile !== undefined && dati_reparto[codice_reparto].imponibile > 0) {
                                                                            var o = new Totali();
                                                                            o.C2221 = "10"; /* Aliquota IVA + */
                                                                            o.C2222; /* Natura */
                                                                            o.C2225 = parseFloat(dati_reparto[codice_reparto].imponibile).toFixed(2); /* Imponibile Importo + */
                                                                            o.C2226 = (parseFloat(dati_reparto[codice_reparto].imponibile) / 100 * 10).toFixed(2); /* Imposta + */
                                                                            o.C2227 = "I"; /* Esigibilità IVA */
                                                                            totali.push(o);
                                                                        }
                                                                    }

                                                                    t.C21182 = dati_testa.sconto_perc;
                                                                    t.C21183 = ((100 * parseFloat(dati_testa.totale) / (100 - parseFloat(dati_testa.sconto_perc))) - parseFloat(dati_testa.totale)).toFixed(2);
                                                                }

                                                                t.C2119 = dati_testa.totale; /* Importo Totale Documento + */
                                                                t.C21111 = "VENDITA FISSA"; /* Causale */
                                                                t.SPED; /*  */
                                                                t.DATAS; /*  */
                                                                t.INDIRIZZO; /*  */

                                                                testa.push(t);
                                                                //invia_fattura_elettronica(testa, corpo, totali, pagamento);

                                                                fattura_elettronica["testa"] = testa;
                                                                fattura_elettronica["corpo"] = corpo;
                                                                fattura_elettronica["totali"] = totali;
                                                                fattura_elettronica["pagamento"] = pagamento;
                                                            }

                                                            aggiungi_spool_stampa("http://" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=local_printer&timeout=30000", dati_scontrino, tipo, "stampa_fiscale", dati_testa);
                                                            $("#annullo_reso input").val("");
                                                            $("#annullo_reso").modal("hide");
                                                            $("#campo_qta_menu_fisso").val("1");
                                                            //NON E' VALIDO PER LA VERSIONE MOBILE
                                                            if (nome_pagina.indexOf("LAYOUT_RISTORANTE") !== -1) {
                                                                mod_categoria(comanda.categoria_predefinita);
                                                                mod_pagina(1);
                                                            }

                                                            //SPOSTATO IN SPOOLER
                                                            //Resetto la situazione dei buoni pasto
                                                            if ($('.scelta_buoni__tipo_pagamento_restante')[0] !== undefined) {
                                                                $('.scelta_buoni__tipo_pagamento_restante')[0].selectedIndex = 0;
                                                            }
                                                            $('.scelta_buoni__restante').html(0);
                                                            $('[name="scelta_buoni__quantita_buoni"]').val(1);
                                                            $('.scelta_buoni__ammontare_buono').html(0);
                                                            $('#scelta_buoni [name="scelta_buoni__buono_spesa"]').val(0);
                                                            if ($('.scelta_buoni__tipo_pagamento_restante')[0] !== undefined) {
                                                                $('.scelta_buoni__tipo_pagamento_restante')[0].selectedIndex = 0;
                                                            }
                                                            buoni_memorizzati = [];
                                                            $('#calcola_resto').modal('hide');
                                                            //Fine del reset

                                                            console.log("DATI SCONTRINO", dati_scontrino);
                                                            if (comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === 'TAKEAWAY' || comanda.tavolo === 'TAKE AWAY' || comanda.tavolo === 'BAR') {
                                                                indietro_categoria();
                                                            }

                                                            comanda.sincro.query_cassa();
                                                        });
                                                    });
                                                } else if (annullamento === true) {

                                                    dati_scontrino += '<printRecTotal  ' + comanda.istr_cass["operator"] + '="1" />';
                                                    dati_scontrino += '<' + comanda.istr_cass["endFiscalReceipt"] + ' ' + comanda.istr_cass["operator"] + '="1" />';
                                                    dati_scontrino += '</printerFiscalReceipt>';
                                                    dati_scontrino = dati_scontrino.replace(/&/gi, '&amp;');
                                                    if (comanda.cassa_intellinet === "N") {
                                                        dati_scontrino = '<?xml version="1.0" encoding="utf-16"?>\n' + dati_scontrino;
                                                    } else {
                                                        dati_scontrino = '<?xml version="1.0" encoding="utf-8"?>\n' +
                                                                '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">\n' +
                                                                '<s:Body>\n' + dati_scontrino +
                                                                '</s:Body>\n' +
                                                                '</s:Envelope>\n';
                                                    }
                                                    $("#annullo_reso input").val("");
                                                    $("#annullo_reso").modal("hide");
                                                    aggiungi_spool_stampa("http://" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=local_printer&timeout=30000", dati_scontrino, tipo, "stampa_fiscale", dati_testa);
                                                }
                                            });
                                            if (comanda.compatibile_xml === true) {

                                                comanda.xml.elimina_file_xml("/TAVOLO" + aggZero(comanda.tavolo, 3) + ".XML", false, function () {
                                                    comanda.xml.elimina_file_xml("/PLANING/P" + aggZero(comanda.tavolo, 3), true, function () {
                                                        comanda.xml.elimina_file_xml("_" + comanda.folder_number + "/COND/Tav" + comanda.tavolo + ".CO1", false, function () {
                                                        });
                                                    });
                                                });
                                            }


                                        });
                                        $('#numero_coperti').val('0');
                                        if (comanda.conto !== '#prodotti_conto_separato_2_grande' && comanda.conto !== '#prodotti_conto_separato_grande')
                                        {
                                            console.log("COMANDA CONTO", comanda.conto);
                                            comanda.parcheggio = '';
                                            reset_progressivo_asporto();
                                        }


                                    });
                                });
                            } else
                            {
                                /* sblocca schermata se batti scontrino vuoto */
                                $('#tasti_fiscali_da_nascondere', 'body #creazione_comanda').css('pointer-events', '');
                                $('#tasti_fiscali_da_nascondere', 'body #creazione_comanda').css('opacity', '');
                                $('#prodotti', 'body #creazione_comanda').css('opacity', '');
                                $('#prodotti', 'body #creazione_comanda').css('pointer-events', '');
                            }
                        }

                    }
                });
            });
        } else {


            bootbox.prompt(esistenza_scontrino + "<br><br>E' possibile uscire dall'operazione col tasto Cancel, oppure entrare nella comanda, battere gli articoli da annullare, ri-cliccare il tasto Fiscali, re-inserire i dati dello scontrino da annullare e rifare l'annullo inserendo la matricola del misuratore di riferimento (diversa da quella del misuratore presente nella postazione) nel campo sottostante.", function (matricola) {
                if (matricola === null) {
                    bootbox.hideAll();
                } else if (esistenza_scontrino.indexOf("Documento non trovato") !== -1 && matricola.length === 11) {
                    //RICORSIONE
                    stampa_ndc(tipo, dati_testa_principali, operazione, matricola);
                } else if (esistenza_scontrino.indexOf("Documento non trovato") !== -1 && matricola.length !== 11)
                {
                    bootbox.alert("Lunghezza matricola non valida.");
                }

            });
        }
    } else {
        bootbox.alert("ATTENZIONE: Controllare il numero del documento commerciale, che &egrave; composto da due serie di 4 cifre (esempio: 0000-0000).<br>Controllare inoltre la corrispondenza della data.");
    }
}