/* global comanda */

fa_richiesta_progressivo = function (callback) {

    if (typeof (comanda.tavolo) === "string" && comanda.tavolo.indexOf("CONTINUO") !== -1) {

        var testo_query = "select numero from progressivo_asporto;";

        comanda.sincro.query(testo_query, function (n) {

            //MEGLIO UN PROGRESSIVO IN PIU' CHE UN PROGRESSIVO DOPPIO

            var testo_query = "update progressivo_asporto set numero=cast(numero as int)+1;";

            comanda.sock.send({tipo: "aggiornamento_progressivo_asporto", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});

            comanda.sincro.query(testo_query, function () {

                callback(n[0].numero);

            });

        });
    } else
    {
        callback('ERR_PROGR');
    }
};

function fa_richiesta_progressivo_domicilio(id_comanda_definito) {

    if (id_comanda_definito === undefined) {
        id_comanda_definito = id_comanda_attuale_alasql();
    }

    if (typeof (comanda.tavolo) === "string" && comanda.tavolo.indexOf("CONTINUO") !== -1) {

        var testo_query = "select numero from progressivo_asporto_domicilio;";

        let n = alasql(testo_query);

        //MEGLIO UN PROGRESSIVO IN PIU' CHE UN PROGRESSIVO DOPPIO

        var testo_query = "update progressivo_asporto_domicilio set numero=cast(numero as int)+1;";

        comanda.sock.send({tipo: "aggiornamento_progressivo_asporto_domicilio", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});

        alasql(testo_query);

        return n[0].numero;


    } else
    {
        callback('ERR_PROGR');
    }
}
