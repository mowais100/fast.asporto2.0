/* global comanda, parseFloat, RAM, bootbox, oggetto_pony, epson */

$(document).on('keypress', function (event) {
    if (comanda.dispositivo === 'lang_41') {
        //Se premi altro non va
        //Se sbagli devi rientrare
        //Se l'hai già stampata e vuoi ristampare devi uscire e rientrare
    }
});

//CallerIdFinished

var socket_CallerId = new WebSocket("ws://localhost:7778");

socket_CallerId.onmessage = function (event) {
    console.log("messaggio ricevuto");


    popup_callerid(event.data);

    //socket_CallerId.close();
};

socket_CallerId.onopen = function () {
    console.log("connessione effettuata");
    //socket_CallerId.send("hello!");
};

socket_CallerId.onclose = function () {
    console.log("connessione chiusa");
};

socket_CallerId.onerror = function () {
    console.log("errore nella connessione");
};




function popup_callerid(numero_telefonico) {
    $("#numero_callerid,#cognome_callerid,#nome_callerid,#via_callerid,#civico_callerid,#cap_callerid,#citta_callerid").html("");

    $("#numero_callerid").html(numero_telefonico);

    var query = "SELECT * FROM clienti WHERE telefono LIKE '%" + numero_telefonico + "%' OR telefono_3 LIKE '%" + numero_telefonico + "%' OR telefono_4 LIKE '%" + numero_telefonico + "%' OR telefono_5 LIKE '%" + numero_telefonico + "%' OR telefono_6 LIKE '%" + numero_telefonico + "%' OR cellulare LIKE '%" + numero_telefonico + "%' LIMIT 1";

    comanda.sincro.query(query, function (result) {

        if (result.length > 0) {

            $("#cognome_callerid").html(result[0].cognome);
            $("#nome_callerid").html(result[0].nome);
            $("#via_callerid").html(result[0].indirizzo);
            $("#civico_callerid").html(result[0].numero);
            $("#cap_callerid").html(result[0].cap);
            $("#citta_callerid").html(result[0].comune);

            $("#accetta_cliente").attr("onclick", "accetta_cliente('ID','" + result[0].id + "');");

        } else {

            $("#nome_callerid").html("CLIENTE");

            $("#cognome_callerid").html("SCONOSCIUTO");

            $("#accetta_cliente").attr("onclick", "accetta_cliente('TEL','" + numero_telefonico + "');");

        }

    });

    $("#popup_callerid").modal("show");
}

function accetta_cliente(campo_id, campo_numerico) {

    if (campo_id === "ID") {

        $("#popup_callerid").modal("hide");

        $("#popup_scelta_cliente_elenco_clienti option").prop('selected', false);

        intesta_cliente();

        setTimeout(function () {
            $("#popup_scelta_cliente_elenco_clienti option[value='" + campo_numerico + "']").prop('selected', true).trigger('change');
        }, 200);

    } else if (campo_id === "TEL") {

        $("#popup_callerid").modal("hide");

        intesta_cliente();

        comanda.ultimo_numero_intercettato = campo_numerico;

        //setTimeout(function () {
        //$("#popup_scelta_cliente input[name='cellulare']").val(campo_numerico);
        //$("#popup_scelta_cliente input[name='telefono']").val(campo_numerico);
        //}, 200);
    } else {
        bootbox.alert("Errore nella funzione Accetta Cliente");
    }

}

function associa_ultimo_numero() {

    if ($("#popup_scelta_cliente input[name='cellulare']").val().trim().length === 0) {
        $("#popup_scelta_cliente input[name='cellulare']").val(comanda.ultimo_numero_intercettato);
    } else if ($("#popup_scelta_cliente input[name='telefono']").val().trim().length === 0) {
        $("#popup_scelta_cliente input[name='telefono']").val(comanda.ultimo_numero_intercettato);
    } else if ($("#popup_scelta_cliente input[name='telefono_3']").val().trim().length === 0) {
        $("#popup_scelta_cliente input[name='telefono_3']").val(comanda.ultimo_numero_intercettato);
    } else if ($("#popup_scelta_cliente input[name='telefono_4']").val().trim().length === 0) {
        $("#popup_scelta_cliente input[name='telefono_4']").val(comanda.ultimo_numero_intercettato);
    } else if ($("#popup_scelta_cliente input[name='telefono_5']").val().trim().length === 0) {
        $("#popup_scelta_cliente input[name='telefono_5']").val(comanda.ultimo_numero_intercettato);
    } else if ($("#popup_scelta_cliente input[name='telefono_6']").val().trim().length === 0) {
        $("#popup_scelta_cliente input[name='telefono_6']").val(comanda.ultimo_numero_intercettato);
    } else {
        bootbox.alert("I campo del telefono sono tutti occupati. Cancellarne uno e ri-associare il numero.");
    }


}

function assegna_impasto(nome_impasto) {
    
    
    $('#popup_mod_art').modal('hide');


    var query_verifica = "select desc_art,prezzo_un,tipo_impasto from comanda where stato_record='ATTIVO' and ntav_comanda='" + comanda.tavolo + "' and prog_inser='" + comanda.ultimo_progressivo_articolo + "';";

    comanda.sincro.query(query_verifica, function (v) {

        if (v !== undefined && v[0] !== undefined) {



            var descrizione = v[0].desc_art;
            
            if(v[0].desc_art.indexOf("MAXI") !== -1)
            {// funziona per calcolare prezzo per tipo impasti se la pizza e maxi se no mette prezzo normale.
                assegna_impasto_maxi(nome_impasto);
            }
            else{

            var prezzo = parseFloat(v[0].prezzo_un).toFixed(2);

            if (v[0].tipo_impasto === nome_impasto) {




                switch (nome_impasto) {
                    case "K":
                        descrizione = v[0].desc_art.slice(0, -5);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_kamut);
                        break;

                    case "I":
                        descrizione = v[0].desc_art.slice(0, -10);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_integrale);
                        break;

                    case "F":
                        descrizione = v[0].desc_art.slice(0, -6);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_farro);
                        break;

                    case "PM":
                        descrizione = v[0].desc_art.slice(0, -12);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_madre);
                        break;

                    case "5":
                        descrizione = v[0].desc_art.slice(0, -10);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_5_cereali);
                        break;

                    case "7":
                        descrizione = v[0].desc_art.slice(0, -10);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_7_cereali);
                        break;

                    case "S":
                        descrizione = v[0].desc_art.slice(0, -5);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_soja);
                        break;

                    case "G":
                        descrizione = v[0].desc_art.slice(0, -14);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_senza_glutine);
                        break;

                    case "C":
                        descrizione = v[0].desc_art.slice(0, -8);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_carbone);
                        break;

                    case "N":
                        descrizione = v[0].desc_art.slice(0, -7);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_napoli);
                        break;

                    case "DP":
                        descrizione = v[0].desc_art.slice(0, -13);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_doppia_pasta);
                        break;

                    case "B":
                        descrizione = v[0].desc_art.slice(0, -8);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_battuta);
                        break;

                    case "BF":
                        descrizione = v[0].desc_art.slice(0, -16);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_battuta_farcita);
                        break;

                    case "E":
                        descrizione = v[0].desc_art.slice(0, -6);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_enkir);
                        break;

                    case "CP":
                        descrizione = v[0].desc_art.slice(0, -7);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_canapa);
                        break;

                    case "GA":
                        descrizione = v[0].desc_art.slice(0, -11);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_grano_arso);
                        break;

                    case "M":
                        descrizione = v[0].desc_art.slice(0, -9);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_manitoba);
                        break;

                    case "P":
                        descrizione = v[0].desc_art.slice(0, -7);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_patate);
                        break;

                    case "PR":
                        descrizione = v[0].desc_art.slice(0, -13);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_pinsa_romana);
                        break;

                    case "FG":
                        descrizione = v[0].desc_art.slice(0, -8);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_fagioli);
                        break;

                    case "BU":
                        descrizione = v[0].desc_art.slice(0, -8);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_buratto_tipo2);
                        break;

                    case "P1":
                        descrizione = v[0].desc_art.slice(0, -1 * (comanda.testo_pasta_personalizzata_1.length + 1));
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_personalizzata_1);
                        break;

                    case "P2":
                        descrizione = v[0].desc_art.slice(0, -1 * (comanda.testo_pasta_personalizzata_2.length + 1));
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_personalizzata_2);
                        break;

                    case "P3":
                        descrizione = v[0].desc_art.slice(0, -1 * (comanda.testo_pasta_personalizzata_3.length + 1));
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_personalizzata_3);
                        break;

                    case "P4":
                        descrizione = v[0].desc_art.slice(0, -1 * (comanda.testo_pasta_personalizzata_4.length + 1));
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_personalizzata_4);
                        break;

                    case "P5":
                        descrizione = v[0].desc_art.slice(0, -1 * (comanda.testo_pasta_personalizzata_5.length + 1));
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_personalizzata_5);
                        break;

                    case "P6":
                        descrizione = v[0].desc_art.slice(0, -1 * (comanda.testo_pasta_personalizzata_6.length + 1));
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_personalizzata_6);
                        break;

                    /*<button onclick="assegna_impasto('N');" type="button" class="pasta_napoli btn btn-default" style="width: 80px;background-image:-webkit-linear-gradient(top, rgb(128, 128, 128),rgb(70, 69, 69));border:1px solid black;"><img src="./img/paste/Napoli.png" style="height:50px;"></button>
                     <button onclick="assegna_impasto('DP');" type="button" class="pasta_doppia_pasta btn btn-default" style="width: 80px;background-image:-webkit-linear-gradient(top, rgb(128, 128, 128),rgb(70, 69, 69));border:1px solid black;"><img src="./img/paste/DoppiaPasta.png" style="height:50px;"></button>
                     <button onclick="assegna_impasto('B');" type="button" class="pasta_battuta btn btn-default" style="width: 80px;background-image:-webkit-linear-gradient(top, rgb(128, 128, 128),rgb(70, 69, 69));border:1px solid black;"><img src="./img/paste/Battuta.png" style="height:50px;"></button>
                     <button onclick="assegna_impasto('BF');" type="button" class="pasta_battuta_farcita btn btn-default" style="width: 80px;background-image:-webkit-linear-gradient(top, rgb(128, 128, 128),rgb(70, 69, 69));border:1px solid black;"><img src="./img/paste/BattutaFarcita.png" style="height:50px;"></button>
                     <button onclick="assegna_impasto('E');" type="button" class="pasta_enkir btn btn-default" style="width: 80px;background-image:-webkit-linear-gradient(top, rgb(128, 128, 128),rgb(70, 69, 69));border:1px solid black;"><img src="./img/paste/Enkir.png" style="height:50px;"></button>
                     <button onclick="assegna_impasto('CP');" type="button" class="pasta_canapa btn btn-default" style="width: 80px;background-image:-webkit-linear-gradient(top, rgb(128, 128, 128),rgb(70, 69, 69));border:1px solid black;"><img src="./img/paste/Canapa.png" style="height:50px;"></button>
                     <button onclick="assegna_impasto('GA');" type="button" class="pasta_grano_arso btn btn-default" style="width: 80px;background-image:-webkit-linear-gradient(top, rgb(128, 128, 128),rgb(70, 69, 69));border:1px solid black;"><img src="./img/paste/GranoArso.png" style="height:50px;"></button>
                     <button onclick="assegna_impasto('M');" type="button" class="pasta_manitoba btn btn-default" style="width: 80px;background-image:-webkit-linear-gradient(top, rgb(128, 128, 128),rgb(70, 69, 69));border:1px solid black;"><img src="./img/paste/Manitoba.png" style="height:50px;"></button>
                     <button onclick="assegna_impasto('P');" type="button" class="pasta_patate btn btn-default" style="width: 80px;background-image:-webkit-linear-gradient(top, rgb(128, 128, 128),rgb(70, 69, 69));border:1px solid black;"><img src="./img/paste/Patate.png" style="height:50px;"></button>
                     <button onclick="assegna_impasto('PR');" type="button" class="pasta_pinsa_romana btn btn-default" style="width: 80px;background-image:-webkit-linear-gradient(top, rgb(128, 128, 128),rgb(70, 69, 69));border:1px solid black;"><img src="./img/paste/PinsaRomana.png" style="height:50px;"></button>
                     <button onclick="assegna_impasto('FG');" type="button" class="pasta_fagioli btn btn-default" style="width: 80px;background-image:-webkit-linear-gradient(top, rgb(128, 128, 128),rgb(70, 69, 69));border:1px solid black;"><img src="./img/paste/Fagioli.png" style="height:50px;"></button>
                     <button onclick="assegna_impasto('BU');" type="button" class="pasta_buratto_tipo2 btn btn-default" style="width: 80px;background-image:-webkit-linear-gradient(top, rgb(128, 128, 128),rgb(70, 69, 69));border:1px solid black;"><img src="./img/paste/BurattoTipo2.png" style="height:50px;"></button>
                     */
                }

                comanda.stampata_n = true;

                var query_update = "update comanda set desc_art='" + descrizione + "',prezzo_un='" + parseFloat(prezzo).toFixed(2) + "',tipo_impasto='' where stato_record='ATTIVO' and ntav_comanda='" + comanda.tavolo + "' and prog_inser='" + comanda.ultimo_progressivo_articolo + "';";
            } else {
                switch (nome_impasto) {
                    case "K":
                        descrizione = v[0].desc_art + " KAMUT";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_kamut);
                        break;

                    case "I":
                        descrizione = v[0].desc_art + " INTEGRALE";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_integrale);
                        break;

                    case "F":
                        descrizione = v[0].desc_art + " FARRO";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_farro);
                        break;

                    case "PM":
                        descrizione = v[0].desc_art + " PASTA MADRE";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_madre);
                        break;

                    case "5":
                        descrizione = v[0].desc_art + " 5 CEREALI";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_5_cereali);
                        break;

                    case "7":
                        descrizione = v[0].desc_art + " 7 CEREALI";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_7_cereali);
                        break;

                    case "S":
                        descrizione = v[0].desc_art + " SOJA";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_soja);
                        break;

                    case "G":
                        descrizione = v[0].desc_art + " SENZA GLUTINE";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_senza_glutine);
                        break;

                    case "C":
                        descrizione = v[0].desc_art + " CARBONE";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_carbone);
                        break;



                    case "N":
                        descrizione = v[0].desc_art + " NAPOLI";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_napoli);
                        break;

                    case "DP":
                        descrizione = v[0].desc_art + " DOPPIA PASTA";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_doppia_pasta);
                        break;

                    case "B":
                        descrizione = v[0].desc_art + " BATTUTA";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_battuta);
                        break;

                    case "BF":
                        descrizione = v[0].desc_art + " BATTUTA FARCITA";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_battuta_farcita);
                        break;

                    case "E":
                        descrizione = v[0].desc_art + " ENKIR";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_enkir);
                        break;

                    case "CP":
                        descrizione = v[0].desc_art + " CANAPA";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_canapa);
                        break;

                    case "GA":
                        descrizione = v[0].desc_art + " GRANO ARSO";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_grano_arso);
                        break;

                    case "M":
                        descrizione = v[0].desc_art + " MANITOBA";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_manitoba);
                        break;

                    case "P":
                        descrizione = v[0].desc_art + " PATATE";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_patate);
                        break;

                    case "PR":
                        descrizione = v[0].desc_art + " PINSA ROMANA";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_pinsa_romana);
                        break;

                    case "FG":
                        descrizione = v[0].desc_art + " FAGIOLI";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_fagioli);
                        break;

                    case "BU":
                        descrizione = v[0].desc_art + " BURATTO";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_buratto_tipo2);
                        break;

                    case "P1":
                        descrizione = v[0].desc_art + " " + comanda.testo_pasta_personalizzata_1;
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_personalizzata_1);
                        break;

                    case "P2":
                        descrizione = v[0].desc_art + " " + comanda.testo_pasta_personalizzata_2;
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_personalizzata_2);
                        break;

                    case "P3":
                        descrizione = v[0].desc_art + " " + comanda.testo_pasta_personalizzata_3;
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_personalizzata_3);
                        break;

                    case "P4":
                        descrizione = v[0].desc_art + " " + comanda.testo_pasta_personalizzata_4;
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_personalizzata_4);
                        break;

                    case "P5":
                        descrizione = v[0].desc_art + " " + comanda.testo_pasta_personalizzata_5;
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_personalizzata_5);
                        break;

                    case "P6":
                        descrizione = v[0].desc_art + " " + comanda.testo_pasta_personalizzata_6;
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_personalizzata_6);
                        break;

                }

                comanda.stampata_n = true;
                var query_update = "update comanda set desc_art='" + descrizione + "',prezzo_un='" + parseFloat(prezzo).toFixed(2) + "',tipo_impasto='" + nome_impasto + "' where stato_record='ATTIVO' and ntav_comanda='" + comanda.tavolo + "' and prog_inser='" + comanda.ultimo_progressivo_articolo + "';";
            }

            comanda.sock.send({ tipo: "aggiornamento_tavoli", operatore: comanda.operatore, query: query_update, terminale: comanda.terminale, ip: comanda.ip_address });

            comanda.sincro.query(query_update, function () {

                comanda.sincro.query_cassa();

                comanda.funzionidb.conto_attivo(function () {
                    $('.table.ultime_battiture tr').css('background-color', '');
                    $('.table.ultime_battiture tr#art_' + comanda.ultimo_progressivo_articolo).css('background-color', '#3498db');
                }, false);

                

            });
        }
        }

    });
    
}


// funziona per calcolare prezzo per tipo impasti se la pizza e maxi se no mette prezzo normale.
function assegna_impasto_maxi(nome_impasto) {

    $('#popup_mod_art').modal('hide');


    var query_verifica = "select desc_art,prezzo_un,tipo_impasto from comanda where stato_record='ATTIVO' and ntav_comanda='" + comanda.tavolo + "' and prog_inser='" + comanda.ultimo_progressivo_articolo + "';";

    comanda.sincro.query(query_verifica, function (v) {

        if (v !== undefined && v[0] !== undefined) {



            var descrizione = v[0].desc_art;

            var prezzo = parseFloat(v[0].prezzo_un).toFixed(2);

            if (v[0].tipo_impasto === nome_impasto) {




                switch (nome_impasto) {
                    case "K":
                        descrizione = v[0].desc_art.slice(0, -5);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_kamut_maxi);
                        break;

                    case "I":
                        descrizione = v[0].desc_art.slice(0, -10);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_integrale_maxi);
                        break;

                    case "F":
                        descrizione = v[0].desc_art.slice(0, -6);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_farro_maxi);
                        break;

                    case "PM":
                        descrizione = v[0].desc_art.slice(0, -12);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_madre_maxi);
                        break;

                    case "5":
                        descrizione = v[0].desc_art.slice(0, -10);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_5_cereali_maxi);
                        break;

                    case "7":
                        descrizione = v[0].desc_art.slice(0, -10);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_7_cereali_maxi);
                        break;

                    case "S":
                        descrizione = v[0].desc_art.slice(0, -5);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_soja_maxi);
                        break;

                    case "G":
                        descrizione = v[0].desc_art.slice(0, -14);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_senza_glutine_maxi);
                        break;

                    case "C":
                        descrizione = v[0].desc_art.slice(0, -8);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_carbone_maxi);
                        break;

                    case "N":
                        descrizione = v[0].desc_art.slice(0, -7);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_napoli_maxi);
                        break;

                    case "DP":
                        descrizione = v[0].desc_art.slice(0, -13);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_doppia_pasta_maxi);
                        break;

                    case "B":
                        descrizione = v[0].desc_art.slice(0, -8);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_battuta_maxi);
                        break;

                    case "BF":
                        descrizione = v[0].desc_art.slice(0, -16);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_battuta_farcita_maxi);
                        break;

                    case "E":
                        descrizione = v[0].desc_art.slice(0, -6);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_enkir_maxi);
                        break;

                    case "CP":
                        descrizione = v[0].desc_art.slice(0, -7);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_canapa_maxi);
                        break;

                    case "GA":
                        descrizione = v[0].desc_art.slice(0, -11);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_grano_arso_maxi);
                        break;

                    case "M":
                        descrizione = v[0].desc_art.slice(0, -9);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_manitoba_maxi);
                        break;

                    case "P":
                        descrizione = v[0].desc_art.slice(0, -7);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_patate_maxi);
                        break;

                    case "PR":
                        descrizione = v[0].desc_art.slice(0, -13);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_pinsa_romana_maxi);
                        break;

                    case "FG":
                        descrizione = v[0].desc_art.slice(0, -8);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_fagioli_maxi);
                        break;

                    case "BU":
                        descrizione = v[0].desc_art.slice(0, -8);
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_buratto_tipo2_maxi);
                        break;

                    case "P1":
                        descrizione = v[0].desc_art.slice(0, -1 * (comanda.testo_pasta_personalizzata_1.length + 1));
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_personalizzata_1_maxi);
                        break;

                    case "P2":
                        descrizione = v[0].desc_art.slice(0, -1 * (comanda.testo_pasta_personalizzata_2.length + 1));
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_personalizzata_2_maxi);
                        break;

                    case "P3":
                        descrizione = v[0].desc_art.slice(0, -1 * (comanda.testo_pasta_personalizzata_3.length + 1));
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_personalizzata_3_maxi);
                        break;

                    case "P4":
                        descrizione = v[0].desc_art.slice(0, -1 * (comanda.testo_pasta_personalizzata_4.length + 1));
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_personalizzata_4_maxi);
                        break;

                    case "P5":
                        descrizione = v[0].desc_art.slice(0, -1 * (comanda.testo_pasta_personalizzata_5.length + 1));
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_personalizzata_5_maxi);
                        break;

                    case "P6":
                        descrizione = v[0].desc_art.slice(0, -1 * (comanda.testo_pasta_personalizzata_6.length + 1));
                        prezzo = parseFloat(prezzo) - parseFloat(comanda.prezzo_pasta_personalizzata_6_maxi);
                        break;

                    /*<button onclick="assegna_impasto('N');" type="button" class="pasta_napoli btn btn-default" style="width: 80px;background-image:-webkit-linear-gradient(top, rgb(128, 128, 128),rgb(70, 69, 69));border:1px solid black;"><img src="./img/paste/Napoli.png" style="height:50px;"></button>
                     <button onclick="assegna_impasto('DP');" type="button" class="pasta_doppia_pasta btn btn-default" style="width: 80px;background-image:-webkit-linear-gradient(top, rgb(128, 128, 128),rgb(70, 69, 69));border:1px solid black;"><img src="./img/paste/DoppiaPasta.png" style="height:50px;"></button>
                     <button onclick="assegna_impasto('B');" type="button" class="pasta_battuta btn btn-default" style="width: 80px;background-image:-webkit-linear-gradient(top, rgb(128, 128, 128),rgb(70, 69, 69));border:1px solid black;"><img src="./img/paste/Battuta.png" style="height:50px;"></button>
                     <button onclick="assegna_impasto('BF');" type="button" class="pasta_battuta_farcita btn btn-default" style="width: 80px;background-image:-webkit-linear-gradient(top, rgb(128, 128, 128),rgb(70, 69, 69));border:1px solid black;"><img src="./img/paste/BattutaFarcita.png" style="height:50px;"></button>
                     <button onclick="assegna_impasto('E');" type="button" class="pasta_enkir btn btn-default" style="width: 80px;background-image:-webkit-linear-gradient(top, rgb(128, 128, 128),rgb(70, 69, 69));border:1px solid black;"><img src="./img/paste/Enkir.png" style="height:50px;"></button>
                     <button onclick="assegna_impasto('CP');" type="button" class="pasta_canapa btn btn-default" style="width: 80px;background-image:-webkit-linear-gradient(top, rgb(128, 128, 128),rgb(70, 69, 69));border:1px solid black;"><img src="./img/paste/Canapa.png" style="height:50px;"></button>
                     <button onclick="assegna_impasto('GA');" type="button" class="pasta_grano_arso btn btn-default" style="width: 80px;background-image:-webkit-linear-gradient(top, rgb(128, 128, 128),rgb(70, 69, 69));border:1px solid black;"><img src="./img/paste/GranoArso.png" style="height:50px;"></button>
                     <button onclick="assegna_impasto('M');" type="button" class="pasta_manitoba btn btn-default" style="width: 80px;background-image:-webkit-linear-gradient(top, rgb(128, 128, 128),rgb(70, 69, 69));border:1px solid black;"><img src="./img/paste/Manitoba.png" style="height:50px;"></button>
                     <button onclick="assegna_impasto('P');" type="button" class="pasta_patate btn btn-default" style="width: 80px;background-image:-webkit-linear-gradient(top, rgb(128, 128, 128),rgb(70, 69, 69));border:1px solid black;"><img src="./img/paste/Patate.png" style="height:50px;"></button>
                     <button onclick="assegna_impasto('PR');" type="button" class="pasta_pinsa_romana btn btn-default" style="width: 80px;background-image:-webkit-linear-gradient(top, rgb(128, 128, 128),rgb(70, 69, 69));border:1px solid black;"><img src="./img/paste/PinsaRomana.png" style="height:50px;"></button>
                     <button onclick="assegna_impasto('FG');" type="button" class="pasta_fagioli btn btn-default" style="width: 80px;background-image:-webkit-linear-gradient(top, rgb(128, 128, 128),rgb(70, 69, 69));border:1px solid black;"><img src="./img/paste/Fagioli.png" style="height:50px;"></button>
                     <button onclick="assegna_impasto('BU');" type="button" class="pasta_buratto_tipo2 btn btn-default" style="width: 80px;background-image:-webkit-linear-gradient(top, rgb(128, 128, 128),rgb(70, 69, 69));border:1px solid black;"><img src="./img/paste/BurattoTipo2.png" style="height:50px;"></button>
                     */
                }

                comanda.stampata_n = true;

                var query_update = "update comanda set desc_art='" + descrizione + "',prezzo_un='" + parseFloat(prezzo).toFixed(2) + "',tipo_impasto='' where stato_record='ATTIVO' and ntav_comanda='" + comanda.tavolo + "' and prog_inser='" + comanda.ultimo_progressivo_articolo + "';";
            } else {
                switch (nome_impasto) {
                    case "K":
                        descrizione = v[0].desc_art + " KAMUT";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_kamut_maxi);
                        break;

                    case "I":
                        descrizione = v[0].desc_art + " INTEGRALE";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_integrale_maxi);
                        break;

                    case "F":
                        descrizione = v[0].desc_art + " FARRO";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_farro_maxi);
                        break;

                    case "PM":
                        descrizione = v[0].desc_art + " PASTA MADRE";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_madre_maxi);
                        break;

                    case "5":
                        descrizione = v[0].desc_art + " 5 CEREALI";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_5_cereali_maxi);
                        break;

                    case "7":
                        descrizione = v[0].desc_art + " 7 CEREALI";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_7_cereali_maxi);
                        break;

                    case "S":
                        descrizione = v[0].desc_art + " SOJA";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_soja_maxi);
                        break;

                    case "G":
                        descrizione = v[0].desc_art + " SENZA GLUTINE";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_senza_glutine_maxi);
                        break;

                    case "C":
                        descrizione = v[0].desc_art + " CARBONE";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_carbone_maxi);
                        break;



                    case "N":
                        descrizione = v[0].desc_art + " NAPOLI";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_napoli_maxi);
                        break;

                    case "DP":
                        descrizione = v[0].desc_art + " DOPPIA PASTA";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_doppia_pasta_maxi);
                        break;

                    case "B":
                        descrizione = v[0].desc_art + " BATTUTA";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_battuta_maxi);
                        break;

                    case "BF":
                        descrizione = v[0].desc_art + " BATTUTA FARCITA";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_battuta_farcita_maxi);
                        break;

                    case "E":
                        descrizione = v[0].desc_art + " ENKIR";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_enkir_maxi);
                        break;

                    case "CP":
                        descrizione = v[0].desc_art + " CANAPA";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_canapa_maxi);
                        break;

                    case "GA":
                        descrizione = v[0].desc_art + " GRANO ARSO";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_grano_arso_maxi);
                        break;

                    case "M":
                        descrizione = v[0].desc_art + " MANITOBA";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_manitoba_maxi);
                        break;

                    case "P":
                        descrizione = v[0].desc_art + " PATATE";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_patate_maxi);
                        break;

                    case "PR":
                        descrizione = v[0].desc_art + " PINSA ROMANA";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_pinsa_romana_maxi);
                        break;

                    case "FG":
                        descrizione = v[0].desc_art + " FAGIOLI";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_fagioli_maxi);
                        break;

                    case "BU":
                        descrizione = v[0].desc_art + " BURATTO";
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_buratto_tipo2_maxi);
                        break;

                    case "P1":
                        descrizione = v[0].desc_art + " " + comanda.testo_pasta_personalizzata_1;
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_personalizzata_1_maxi);
                        break;

                    case "P2":
                        descrizione = v[0].desc_art + " " + comanda.testo_pasta_personalizzata_2;
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_personalizzata_2_maxi);
                        break;

                    case "P3":
                        descrizione = v[0].desc_art + " " + comanda.testo_pasta_personalizzata_3;
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_personalizzata_3_maxi);
                        break;

                    case "P4":
                        descrizione = v[0].desc_art + " " + comanda.testo_pasta_personalizzata_4;
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_personalizzata_4_maxi);
                        break;

                    case "P5":
                        descrizione = v[0].desc_art + " " + comanda.testo_pasta_personalizzata_5;
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_personalizzata_5_maxi);
                        break;

                    case "P6":
                        descrizione = v[0].desc_art + " " + comanda.testo_pasta_personalizzata_6;
                        prezzo = parseFloat(prezzo) + parseFloat(comanda.prezzo_pasta_personalizzata_6_maxi);
                        break;

                }

                comanda.stampata_n = true;
                var query_update = "update comanda set desc_art='" + descrizione + "',prezzo_un='" + parseFloat(prezzo).toFixed(2) + "',tipo_impasto='" + nome_impasto + "' where stato_record='ATTIVO' and ntav_comanda='" + comanda.tavolo + "' and prog_inser='" + comanda.ultimo_progressivo_articolo + "';";
            }

            comanda.sock.send({ tipo: "aggiornamento_tavoli", operatore: comanda.operatore, query: query_update, terminale: comanda.terminale, ip: comanda.ip_address });

            comanda.sincro.query(query_update, function () {

                comanda.sincro.query_cassa();

                comanda.funzionidb.conto_attivo(function () {
                    $('.table.ultime_battiture tr').css('background-color', '');
                    $('.table.ultime_battiture tr#art_' + comanda.ultimo_progressivo_articolo).css('background-color', '#3498db');
                }, false);

                /* il 29 marzo 2021 il cambia colore è stato portato a false, altrimenti perde il focus quando assegni l'impasto */

            });

        }

    });

}








function archivio_pizze_consegnate() {
    $('#archivio_scontrini .tab_1 tbody tr').css('background-image', 'initial');
    $('#creazione_comanda').hide();
    $("#div_modifica_pagamento_scontrino").html("");

    //var query_archivio = "select id,id_pony,ora_consegna,tipo_consegna,nome_comanda,rag_soc_cliente,stato_record,sum(spazio_forno) as qt_pizze,sum(prezzo_un) as prezzo_tot from comanda where id LIKE '%" + new Date().format('yyyymmdd') + "%' and stato_record NOT LIKE 'CANCELLATO%' and fiscalizzata_sn='S' group by id order by ora_consegna desc";
    var nome_prezzo = "prezzo_un";
    if (comanda.pizzeria_asporto === true) {
        nome_prezzo = "prezzo_vero";
    }

    var query_archivio = "select QTA,portata,tipo_ricevuta,progressivo_fiscale,id,id_pony,ora_consegna,tipo_consegna,consegna_gap,nome_comanda,rag_soc_cliente,stato_record,spazio_forno as qt_pizze," + nome_prezzo + " * quantita as prezzo_tot from comanda where data_servizio = '" + comanda.data_servizio + "' and stato_record NOT LIKE 'CANCELLATO%' and fiscalizzata_sn='S' order by ora_consegna desc;";

    var r = alasql(query_archivio);

    var g = new Object();

    r.forEach(function (e) {

        if (isNaN(parseFloat(e.QTA))) {
            e.QTA = 0;
        }

        if (g[e.id + e.progressivo_fiscale] === undefined) {

            g[e.id + e.progressivo_fiscale] = new Object();
            if (e.portata.indexOf("P") === 0) {
                e.qt_pizze = parseInt(e.qt_pizze);
            } else {
                e.qt_pizze = 0;
            }
            e.prezzo_tot = parseFloat(e.prezzo_tot);
            if (e.tipo_consegna === "DOMICILIO") {
                e.prezzo_tot += parseFloat(e.QTA);
            }
            e.totale_consegne = parseFloat(e.QTA);
            g[e.id + e.progressivo_fiscale] = e;
        } else {
            if (e.portata.indexOf("P") === 0) {
                g[e.id + e.progressivo_fiscale].qt_pizze += parseInt(e.qt_pizze);
            }

            g[e.id + e.progressivo_fiscale].prezzo_tot += parseFloat(e.prezzo_tot);
            if (g[e.id + e.progressivo_fiscale].tipo_consegna === "DOMICILIO") {
                g[e.id + e.progressivo_fiscale].prezzo_tot += parseFloat(e.QTA);
            }
            g[e.id + e.progressivo_fiscale].totale_consegne += parseFloat(e.QTA);
        }

    }
    );

    var a = new Array();

    for (var id in g) {
        a.push(g[id]);
    }


    var righe_html = "";

    var risultato = alasql("select * from ? order by ora_consegna desc;", [a]);

    risultato.forEach(function (set) {

        var nome_cliente = "";
        var cellulare = "";
        var comune = "";

        if (set.nome_comanda === "") {
            nome_cliente = set.rag_soc_cliente + " ";
        } else {
            nome_cliente = set.nome_comanda.toUpperCase() + " ";
        }



        var nome_pony = "";
        if (oggetto_pony[set.id_pony] !== undefined) {
            nome_pony = oggetto_pony[set.id_pony];
        }

        let tipo_consegna = set.tipo_consegna;
        if (set.tipo_consegna === "DOMICILIO") {

            if (checkEmptyVariable(set.rag_soc_cliente) === true) {

                let cliente = alasql("select * from clienti where id=" + set.rag_soc_cliente);

                if (checkEmptyVariable(cliente) === true) {

                    if (checkEmptyVariable(cliente[0]) === true) {

                        if (checkEmptyVariable(cliente[0].indirizzo) === true) {
                            tipo_consegna = cliente[0].indirizzo;
                        }

                        if (checkEmptyVariable(cliente[0].numero) === true) {
                            tipo_consegna += ", " + cliente[0].numero;
                        }

                        if (checkEmptyVariable(cliente[0].comune) === true) {
                            comune += cliente[0].comune;
                        }



                        if (checkEmptyVariable(cliente[0].cellulare) === true) {
                            cellulare += cliente[0].cellulare;
                        } else if (checkEmptyVariable(cliente[0].telefono) === true) {
                            nome_cliente += cliente[0].telefono;
                        } else if (checkEmptyVariable(cliente[0].telefono_3) === true) {
                            nome_cliente += cliente[0].telefono_3;
                        } else if (checkEmptyVariable(cliente[0].telefono_4) === true) {
                            nome_cliente += cliente[0].telefono_4;
                        } else if (checkEmptyVariable(cliente[0].telefono_5) === true) {
                            nome_cliente += cliente[0].telefono_5;
                        } else if (checkEmptyVariable(cliente[0].telefono_6) === true) {
                            nome_cliente += cliente[0].telefono_6;
                        }


                    }
                }
            }



        }
        var query_gap_consegna = "select time_gap_consegna from impostazioni_fiscali where id=" + comanda.folder_number + ";"
        var result = alasql(query_gap_consegna);
        if (result[0].time_gap_consegna == "1" && set.consegna_gap != "undefined" && set.consegna_gap != "" && set.ora_consegna != "") {
            var ora_consegna = set.ora_consegna;
            var minuti_somma = ora_consegna.substring(ora_consegna.indexOf(':') + 1)
            var int_miunti = parseInt(minuti_somma);
            var int_gap_miunti = parseInt(set.consegna_gap);;
            var min_somma = int_miunti + int_gap_miunti;
            var ora_gap = ora_consegna.substring(0, ora_consegna.indexOf(':'))
            var hours = Math.floor(min_somma / 60);
            var minutes = min_somma % 60;
            var ora = parseInt(ora_gap);

            if (minutes < 10) {
                if (ora < 10) {
                    var oraa = ora + hours
                    if (oraa === 10) {
                        var totale_ora = oraa + ":" + minutes;

                    }
                    else {
                        var totale_ora = "0" + oraa + ":" + minutes;
                    }
                }
                else {
                    var totale_ora = ora + hours + ":" + "0" + minutes;

                }

            }
            else {
                if (ora < 10) {
                    var oraa = ora + hours
                    if (oraa === 10) {
                        var totale_ora = oraa + ":" + minutes;

                    }
                    else {
                        var totale_ora = "0" + oraa + ":" + minutes;
                    }
                }
                else {

                    var totale_ora = ora + hours + ":" + minutes;
                }
            }
            if (totale_ora === "NaN:NaN") {
                totale_ora = ""
            }
            if (set.rag_soc_cliente === "") {
                set.rag_soc_cliente = "0";
                let cliente = alasql("select * from clienti where id=" + set.rag_soc_cliente);

                righe_html += "<tr style='background-color:#3d3d3d;' " + comanda.evento + "='visualizza_scontrino_archiviato(\"" + set.id + "\",\"" + set.progressivo_fiscale + "\",\"" + set.tipo_ricevuta + "\");'><td>" + set.ora_consegna + "<br>" + totale_ora +  "</td><td>" + nome_cliente + "<br>" + cellulare + "</td><td>" + set.qt_pizze + "</td><td>" + tipo_consegna + "<br>" + comune + "</td><td>" + nome_pony + "</td><td class='text-right'>&euro; " + parseFloat(set.prezzo_tot).toFixed(2) + "</td></tr>";




                righe_html += "<tr style='height:5px;'></tr>";

            }
            else {
                let cliente = alasql("select * from clienti where id=" + set.rag_soc_cliente);

                righe_html += "<tr style='background-color:#3d3d3d;' " + comanda.evento + "='visualizza_scontrino_archiviato(\"" + set.id + "\",\"" + set.progressivo_fiscale + "\",\"" + set.tipo_ricevuta + "\");'><td>" + set.ora_consegna + "<br>" + totale_ora +  "</td><td>" + nome_cliente + "<br>" + cellulare + "</td><td>" + set.qt_pizze + "</td><td>" + tipo_consegna + "<br>" + comune + "</td><td>" + nome_pony + "</td><td class='text-right'>&euro; " + parseFloat(set.prezzo_tot).toFixed(2) + "</td></tr>";




                righe_html += "<tr style='height:5px;'></tr>";

            }
        }
        else{
            if (set.rag_soc_cliente === "") {
                set.rag_soc_cliente = "0";
                let cliente = alasql("select * from clienti where id=" + set.rag_soc_cliente);
    
                righe_html += "<tr style='background-color:#3d3d3d;' " + comanda.evento + "='visualizza_scontrino_archiviato(\"" + set.id + "\",\"" + set.progressivo_fiscale + "\",\"" + set.tipo_ricevuta + "\");'><td>" + set.ora_consegna + "</td><td>" + nome_cliente + "<br>" + cellulare + "</td><td>" + set.qt_pizze + "</td><td>" + tipo_consegna + "<br>" + comune + "</td><td>" + nome_pony + "</td><td class='text-right'>&euro; " + parseFloat(set.prezzo_tot).toFixed(2) + "</td></tr>";
    
    
    
    
                righe_html += "<tr style='height:5px;'></tr>";
    
            }
            else {
                let cliente = alasql("select * from clienti where id=" + set.rag_soc_cliente);
    
                righe_html += "<tr style='background-color:#3d3d3d;' " + comanda.evento + "='visualizza_scontrino_archiviato(\"" + set.id + "\",\"" + set.progressivo_fiscale + "\",\"" + set.tipo_ricevuta + "\");'><td>" + set.ora_consegna + "</td><td>" + nome_cliente + "<br>" + cellulare + "</td><td>" + set.qt_pizze + "</td><td>" + tipo_consegna + "<br>" + comune + "</td><td>" + nome_pony + "</td><td class='text-right'>&euro; " + parseFloat(set.prezzo_tot).toFixed(2) + "</td></tr>";
    
    
    
    
                righe_html += "<tr style='height:5px;'></tr>";
    
            }
        }

     


    });

    $("#archivio_scontrini .tab_1  tbody").html(righe_html);



    $("#archivio_scontrini .tab_2").hide();
    $('#archivio_scontrini .row').show();
    $('#archivio_scontrini').show();

}

function conferma_metodo_pagamento(bypass, gia_infornato) {

    var testa = new Object();
    testa.totale = parseFloat($(comanda.totale_scontrino).html()).toFixed(2);
    testa.contanti = 0;
    testa.pos = 0;
    testa.droppay = 0;
    testa.bancomat = 0;
    testa.cartadicredito = 0;
    testa.cartadicredito2 = 0;
    testa.cartadicredito3 = 0;
    testa.buoniacquisto = 0;
    testa.buonipasto = 0;
    testa.nonpagato = 0;
    testa.non_riscosso_servizi = 0;
    testa.sconto_a_pagare = 0;
    if (comanda.compatibile_xml === true || bypass === true) {
        testa.contanti = testa.totale;
        /*stampa_scontrino_asporto(comanda.tipologia_ricevuta, testa);*/
        stampa_scontrino(comanda.tipologia_ricevuta, testa);

        setTimeout(function () {
            if (gia_infornato !== true) {
                inforna(true);
            }
            if ($('.scelta_buoni__tipo_pagamento_restante')[0] !== undefined) {
                $('.scelta_buoni__tipo_pagamento_restante')[0].selectedIndex = 0;
            }
            $('.scelta_buoni__restante').html(0);
            $('[name="scelta_buoni__quantita_buoni"]').val(1);
            $('.scelta_buoni__ammontare_buono').html(0);
            $('#scelta_buoni [name="scelta_buoni__buono_spesa"]').val(0);
            if ($('.scelta_buoni__tipo_pagamento_restante')[0] !== undefined) {
                $('.scelta_buoni__tipo_pagamento_restante')[0].selectedIndex = 0;
            }
            buoni_memorizzati = [];
        }, 2000);
    } else if ($('#popup_metodo_pagamento_da_pagare:visible').html() !== '0.00' && $('#popup_metodo_pagamento_da_pagare:visible').html() !== null && $('#popup_metodo_pagamento_da_pagare:visible').html() !== undefined) {
        bootbox.confirm("Mancano ancora da pagare " + " € " + $('#popup_metodo_pagamento_da_pagare').html() + ".", function (risp) {
            if (risp === true) {
                $("#popup_metodo_pagamento_importo_sconto_a_pagare").val($('#popup_metodo_pagamento_da_pagare').html());

                $("#popup_metodo_pagamento_importo_sconto_a_pagare").trigger("change");

                //quasi ricorsivo
                conferma_metodo_pagamento();
            }
        });
    } else {

        if ($('#scelta_buoni:visible').length === 1) {

            var buoni_spesa = $('#scelta_buoni [name="scelta_buoni__buono_spesa"]').val();
            if (parseFloat(buoni_spesa) > 0) {
                testa.buoniacquisto = parseFloat(buoni_spesa);
            }

            testa.totale_buoni_pasto = 0;
            testa.numero_buoni = 0;
            testa.nome_buono = '';
            testa.buoni_pasto = '0';
            testa.numero_buoni_pasto = '0';
            testa.nome_buono_2 = '';
            testa.buoni_pasto_2 = '0';
            testa.numero_buoni_pasto_2 = '0';
            testa.nome_buono_3 = '';
            testa.buoni_pasto_3 = '0';
            testa.numero_buoni_pasto_3 = '0';
            testa.nome_buono_4 = '';
            testa.buoni_pasto_4 = '0';
            testa.numero_buoni_pasto_4 = '0';
            testa.nome_buono_5 = '';
            testa.buoni_pasto_5 = '0';
            testa.numero_buoni_pasto_5 = '0';
            var i = 1;
            if (buoni_memorizzati.length >= 1) {
                buoni_memorizzati.forEach(function (val, index) {
                    switch (i) {
                        case 1:

                            testa.nome_buono = val.id_circuito;
                            testa.buoni_pasto = val.importo_taglio;
                            testa.numero_buoni_pasto = val.quantita;
                            //SOLO PER USO INTERNO ALLO SCONTRINO
                            testa.totale_buoni_pasto += parseFloat(val.importo_taglio) * parseInt(val.quantita);
                            testa.numero_buoni += parseInt(val.quantita);
                            break;
                        case 2:
                            testa.nome_buono_2 = val.id_circuito;
                            testa.buoni_pasto_2 = val.importo_taglio;
                            testa.numero_buoni_pasto_2 = val.quantita;
                            //SOLO PER USO INTERNO ALLO SCONTRINO
                            testa.totale_buoni_pasto += parseFloat(val.importo_taglio) * parseInt(val.quantita);
                            testa.numero_buoni += parseInt(val.quantita);
                            break;
                        case 3:
                            testa.nome_buono_3 = val.id_circuito;
                            testa.buoni_pasto_3 = val.importo_taglio;
                            testa.numero_buoni_pasto_3 = val.quantita;
                            //SOLO PER USO INTERNO ALLO SCONTRINO
                            testa.totale_buoni_pasto += parseFloat(val.importo_taglio) * parseInt(val.quantita);
                            testa.numero_buoni += parseInt(val.quantita);
                            break;
                        case 4:
                            testa.nome_buono_4 = val.id_circuito;
                            testa.buoni_pasto_4 = val.importo_taglio;
                            testa.numero_buoni_pasto_4 = val.quantita;
                            //SOLO PER USO INTERNO ALLO SCONTRINO
                            testa.totale_buoni_pasto += parseFloat(val.importo_taglio) * parseInt(val.quantita);
                            testa.numero_buoni += parseInt(val.quantita);
                            break;
                        case 5:
                            testa.nome_buono_5 = val.id_circuito;
                            testa.buoni_pasto_5 = val.importo_taglio;
                            testa.numero_buoni_pasto_5 = val.quantita;
                            //SOLO PER USO INTERNO ALLO SCONTRINO
                            testa.totale_buoni_pasto += parseFloat(val.importo_taglio) * parseInt(val.quantita);
                            testa.numero_buoni += parseInt(val.quantita);
                            break;
                    }
                    i++;
                });
            }

            var totale_conto = parseFloat($(comanda.totale_scontrino).html());
            var restante = parseFloat(totale_conto - testa.totale_buoni_pasto - testa.buoniacquisto).toFixed(2);
            if (restante > 0) {

                var tipo_pagamento__restante = $('.scelta_buoni__tipo_pagamento_restante option:selected').val();
                if (tipo_pagamento__restante !== null) {

                    switch (tipo_pagamento__restante) {
                        case "CONTANTI":
                            var resto = $('#popup_metodo_pagamento_resto').html();
                            if (resto === undefined) {
                                resto = "0";
                            }
                            testa.contanti = restante - parseFloat(resto);
                            break;
                        case "POS":
                            testa.pos = restante;
                            break;
                        case "DROPPAY":
                            testa.droppay = restante;
                            break;
                        case "BANCOMAT":
                            testa.bancomat = restante;
                            break;
                        case "CARTA DI CREDITO":
                            testa.cartadicredito = restante;
                            break;
                        case "CARTA DI CREDITO 2":
                            testa.cartadicredito2 = restante;
                            break;
                        case "CARTA DI CREDITO 3":
                            testa.cartadicredito3 = restante;
                            break;

                    }

                } else {
                    alert("Devi scegliere un metodo di pagamento per l'importo restante.");
                    return false;
                }
            }



            console.log("DATI TESTA TEMPORANEI", testa);
            $('#scelta_metodo_pagamento').modal('hide');
            $('#scelta_buoni').modal('hide');
            /*stampa_scontrino_asporto(comanda.tipologia_ricevuta, testa);*/
            stampa_scontrino(comanda.tipologia_ricevuta, testa);

            setTimeout(function () {
                if (gia_infornato !== true) {
                    inforna(true);
                }
                if ($('.scelta_buoni__tipo_pagamento_restante')[0] !== undefined) {
                    $('.scelta_buoni__tipo_pagamento_restante')[0].selectedIndex = 0;
                }
                $('.scelta_buoni__restante').html(0);
                $('[name="scelta_buoni__quantita_buoni"]').val(1);
                $('.scelta_buoni__ammontare_buono').html(0);
                $('#scelta_buoni [name="scelta_buoni__buono_spesa"]').val(0);
                if ($('.scelta_buoni__tipo_pagamento_restante')[0] !== undefined) {
                    $('.scelta_buoni__tipo_pagamento_restante')[0].selectedIndex = 0;
                }
                buoni_memorizzati = [];
            }, 2000);
        } else if ($('#popup_nonpagato:visible').length === 1) {

            comanda.tipologia_ricevuta = "SCONTRINO FISCALE";

            var ragione_sociale = $('#popup_nonpagato [name="ragione_sociale"]').val();
            var indirizzo = $('#popup_nonpagato [name="indirizzo"]').val();
            var numero = $('#popup_nonpagato [name="numero"]').val();
            var cap = $('#popup_nonpagato [name="cap"]').val();
            var citta = $('#popup_nonpagato [name="citta"]').val();
            var comune = $('#popup_nonpagato [name="comune"]').val();
            var provincia = $('#popup_nonpagato [name="provincia"]').val();
            var paese = $('#popup_nonpagato [name="paese"]').val();
            var cellulare = $('#popup_nonpagato [name="cellulare"]').val();
            var telefono = $('#popup_nonpagato [name="telefono"]').val();
            var fax = $('#popup_nonpagato [name="fax"]').val();
            var email = $('#popup_nonpagato [name="email"]').val();
            var codice_fiscale = $('#popup_nonpagato [name="codice_fiscale"]').val();
            var partita_iva = $('#popup_nonpagato [name="partita_iva"]').val();
            var partita_iva_estera = $('#popup_nonpagato [name="partita_iva_estera"]').val();
            //-----

            var tendina = $('#popup_nonpagato_elenco_clienti option:selected').val();


            if ((tendina === undefined || tendina.length < 1) && ragione_sociale.length > 2 && ((comanda.partita_iva_estera === true && partita_iva_estera.length > 0) || (partita_iva.length === 11 || partita_iva.length === 16))) {
                var testo_query = "SELECT id FROM clienti WHERE id<3000000 ORDER BY cast(id as int) DESC LIMIT 1";
                comanda.sincro.query(testo_query, function (result) {
                    var id = 0;
                    if (result[0] !== undefined) {
                        id = parseInt(result[0].id) + 1;
                    }
                    testo_query = "INSERT INTO clienti (id,ragione_sociale,indirizzo,numero,cap,citta,comune,provincia,paese,cellulare,telefono,fax,email,codice_fiscale,partita_iva,iva_non_pagati,partita_iva_estera) VALUES (" + id + ",'" + ragione_sociale + "','" + indirizzo + "','" + numero + "','" + cap + "','" + comune + "','" + comune + "','" + provincia + "','" + paese + "','" + cellulare + "','" + telefono + "','" + fax + "','" + email + "','" + codice_fiscale + "','" + partita_iva + "','NO','" + partita_iva_estera + "')";
                    comanda.sock.send({ tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                    comanda.sincro.query(testo_query, function () {
                        testa.id_cliente = id;
                        testa.nonpagato = $(comanda.totale_scontrino).html();
                        console.log("DATI TESTA TEMPORANEI", testa);
                        $('#scelta_metodo_pagamento').modal('hide');

                        /*stampa_scontrino_asporto(comanda.tipologia_ricevuta, testa);*/
                        stampa_scontrino(comanda.tipologia_ricevuta, testa);

                        setTimeout(function () {
                            if (gia_infornato !== true) {
                                inforna(true);
                            }
                            if ($('.scelta_buoni__tipo_pagamento_restante')[0] !== undefined) {
                                $('.scelta_buoni__tipo_pagamento_restante')[0].selectedIndex = 0;
                            }
                            $('.scelta_buoni__restante').html(0);
                            $('[name="scelta_buoni__quantita_buoni"]').val(1);
                            $('.scelta_buoni__ammontare_buono').html(0);
                            $('#scelta_buoni [name="scelta_buoni__buono_spesa"]').val(0);
                            if ($('.scelta_buoni__tipo_pagamento_restante')[0] !== undefined) {
                                $('.scelta_buoni__tipo_pagamento_restante')[0].selectedIndex = 0;
                            }
                            buoni_memorizzati = [];
                        }, 2000);
                        $('#popup_nonpagato').modal('hide');
                    });
                });
            } else if ((tendina === undefined || tendina.length < 1) && ragione_sociale.length < 2 && ((comanda.partita_iva_estera === true && partita_iva_estera.length > 0) || (partita_iva.length === 11 || partita_iva.length === 16))) {
                var testo_query = "SELECT id FROM clienti WHERE id<3000000 ORDER BY cast(id as int) DESC LIMIT 1";
                comanda.sincro.query(testo_query, function (result) {
                    var id = 0;
                    if (result[0] !== undefined) {
                        id = parseInt(result[0].id) + 1;
                    }
                    testo_query = "INSERT INTO clienti (id,ragione_sociale,indirizzo,numero,cap,citta,comune,provincia,paese,cellulare,telefono,fax,email,codice_fiscale,partita_iva,iva_non_pagati,partita_iva_estera) VALUES (" + id + ",'" + partita_iva + "','" + indirizzo + "','" + numero + "','" + cap + "','" + comune + "','" + comune + "','" + provincia + "','" + paese + "','" + cellulare + "','" + telefono + "','" + fax + "','" + email + "','" + codice_fiscale + "','" + partita_iva + "','NO','" + partita_iva_estera + "')";
                    comanda.sock.send({ tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                    comanda.sincro.query(testo_query, function () {
                        testa.id_cliente = id;
                        testa.nonpagato = $(comanda.totale_scontrino).html();
                        console.log("DATI TESTA TEMPORANEI", testa);
                        $('#scelta_metodo_pagamento').modal('hide');
                        /*stampa_scontrino_asporto(comanda.tipologia_ricevuta, testa);*/
                        stampa_scontrino(comanda.tipologia_ricevuta, testa);

                        setTimeout(function () {
                            if (gia_infornato !== true) {
                                inforna(true);
                            }
                            if ($('.scelta_buoni__tipo_pagamento_restante')[0] !== undefined) {
                                $('.scelta_buoni__tipo_pagamento_restante')[0].selectedIndex = 0;
                            }
                            $('.scelta_buoni__restante').html(0);
                            $('[name="scelta_buoni__quantita_buoni"]').val(1);
                            $('.scelta_buoni__ammontare_buono').html(0);
                            $('#scelta_buoni [name="scelta_buoni__buono_spesa"]').val(0);
                            if ($('.scelta_buoni__tipo_pagamento_restante')[0] !== undefined) {
                                $('.scelta_buoni__tipo_pagamento_restante')[0].selectedIndex = 0;
                            }
                            buoni_memorizzati = [];
                        }, 2000);
                        $('#popup_nonpagato').modal('hide');
                    });
                });
            } else if (tendina !== "" && ragione_sociale.length > 2 && ((comanda.partita_iva_estera === true && partita_iva_estera.length > 0) || (partita_iva.length === 11 || partita_iva.length === 16))) {
                testa.id_cliente = tendina;
                testa.nonpagato = $(comanda.totale_scontrino).html();
                console.log("DATI TESTA TEMPORANEI", testa);
                $('#scelta_metodo_pagamento').modal('hide');

                /*stampa_scontrino_asporto(comanda.tipologia_ricevuta, testa);*/
                stampa_scontrino(comanda.tipologia_ricevuta, testa);

                setTimeout(function () {
                    if (gia_infornato !== true) {
                        inforna(true);
                    }
                    if ($('.scelta_buoni__tipo_pagamento_restante')[0] !== undefined) {
                        $('.scelta_buoni__tipo_pagamento_restante')[0].selectedIndex = 0;
                    }
                    $('.scelta_buoni__restante').html(0);
                    $('[name="scelta_buoni__quantita_buoni"]').val(1);
                    $('.scelta_buoni__ammontare_buono').html(0);
                    $('#scelta_buoni [name="scelta_buoni__buono_spesa"]').val(0);
                    if ($('.scelta_buoni__tipo_pagamento_restante')[0] !== undefined) {
                        $('.scelta_buoni__tipo_pagamento_restante')[0].selectedIndex = 0;
                    }
                    buoni_memorizzati = [];
                }, 2000);
                $('#popup_nonpagato').modal('hide');
            } else {
                alert('DEVI INSERIRE I DATI DEL CLIENTE.');
            }



        } else if ($('#scelta_metodo_pagamento:visible').length === 1) {

            var premessa = new Promise(function (resolve, reject) {

                if (comanda.tipologia_ricevuta === "FATTURA") {

                    var ragione_sociale = $('#fattura [name="ragione_sociale"]').val();
                    var indirizzo = $('#fattura [name="indirizzo"]').val();
                    var numero = $('#fattura [name="numero"]').val();
                    var cap = $('#fattura [name="cap"]').val();
                    var citta = $('#fattura [name="citta"]').val();
                    var provincia = $('#fattura [name="provincia"]').val();
                    var paese = $('#fattura [name="paese"]').val();
                    var cellulare = $('#fattura [name="cellulare"]').val();
                    var telefono = $('#fattura [name="telefono"]').val();
                    var fax = $('#fattura [name="fax"]').val();
                    var email = $('#fattura [name="email"]').val();
                    var codice_fiscale = $('#fattura [name="codice_fiscale"]').val();
                    var partita_iva = $('#fattura [name="partita_iva"]').val();
                    var formato_trasmissione = $('#fattura [name="formato_trasmissione"]').prop("checked");
                    var tipo_cliente = $('[name="azienda_privato"]:checked').attr("id");
                    var codice_destinatario = $('#fattura [name="codice_destinatario"]').val();
                    /* non serve metterlo obbligatoriamente. Se non c'è diventa privato con 7 zeri*/
                    var regime_fiscale = $('#fattura [name="regime_fiscale"]').val();
                    var comune = $('#fattura [name="comune"]').val();
                    var nazione = $('v [name="nazione"]').val();
                    var partita_iva_estera = $('#fattura [name="partita_iva_estera"]').val();
                    var pec = $('#fattura [name="pec"]').val();
                    //-----

                    var tendina = $('#fattura_elenco_clienti option:selected').val();
                    if ((tendina === undefined || tendina.length < 1) && ragione_sociale.length > 2 && ((comanda.partita_iva_estera === true && partita_iva_estera.length > 0) || (partita_iva.length === 11 || partita_iva.length === 16))) {

                        var testo_query = "SELECT id FROM clienti WHERE id<3000000 ORDER BY cast(id as int) DESC LIMIT 1";
                        comanda.sincro.query(testo_query, function (result) {
                            var id = 0;
                            if (result[0] !== undefined) {
                                id = parseInt(result[0].id) + 1;
                            }
                            testo_query = "INSERT INTO clienti (tipo_cliente,partita_iva_estera,formato_trasmissione,codice_destinatario,regime_fiscale,pec,comune,id_nazione,nazione,id,ragione_sociale,indirizzo,numero,cap,citta,provincia,paese,cellulare,telefono,fax,email,codice_fiscale,partita_iva,iva_non_pagati,partita_iva_estera) VALUES ('" + tipo_cliente + "','" + partita_iva_estera + "','" + formato_trasmissione + "','" + codice_destinatario + "','" + regime_fiscale + "','" + pec + "','" + comune + "','" + nazione + "','" + nazione + "'," + id + ",'" + ragione_sociale.toUpperCase() + "','" + indirizzo + "','" + numero + "','" + cap + "','" + comune + "','" + provincia + "','" + paese + "','" + cellulare + "','" + telefono + "','" + fax + "','" + email + "','" + codice_fiscale + "','" + partita_iva + "','NO','" + partita_iva_estera + "')";
                            comanda.sock.send({ tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                            comanda.sincro.query(testo_query, function () {
                                resolve(id);
                            });
                        });
                    } else if ((tendina === undefined || tendina.length < 1) && ragione_sociale.length < 2 && ((comanda.partita_iva_estera === true && partita_iva_estera.length > 0) || (partita_iva.length === 11 || partita_iva.length === 16))) {


                        var testo_query = "SELECT * FROM clienti WHERE partita_iva='" + partita_iva + "' ORDER BY ragione_sociale DESC LIMIT 1;";
                        comanda.sincro.query(testo_query, function (result_clienti_corrispondenti) {

                            if (result_clienti_corrispondenti[0].id) {

                                resolve(result_clienti_corrispondenti[0].id);
                            } else {

                                testo_query = "SELECT id FROM clienti WHERE id<3000000 ORDER BY cast(id as int) DESC LIMIT 1;";
                                comanda.sincro.query(testo_query, function (result) {
                                    var id = 0;
                                    if (result[0] !== undefined) {
                                        id = parseInt(result[0].id) + 1;
                                    }
                                    testo_query = "INSERT INTO clienti (tipo_cliente,partita_iva_estera,formato_trasmissione,codice_destinatario,regime_fiscale,pec,comune,id,ragione_sociale,indirizzo,numero,cap,citta,provincia,paese,cellulare,telefono,fax,email,codice_fiscale,partita_iva,iva_non_pagati,partita_iva_estera) VALUES ('" + tipo_cliente + "','" + partita_iva_estera + "','" + formato_trasmissione + "','" + codice_destinatario + "','" + regime_fiscale + "','" + pec + "','" + comune + "','" + nazione + "','" + nazione + "'," + id + ",'" + partita_iva + "','" + indirizzo + "','" + numero + "','" + cap + "','" + comune + "','" + provincia + "','" + paese + "','" + cellulare + "','" + telefono + "','" + fax + "','" + email + "','" + codice_fiscale + "','" + partita_iva + "','NO','" + partita_iva_estera + "')";
                                    comanda.sock.send({ tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                                    comanda.sincro.query(testo_query, function () {
                                        resolve(id);
                                    });
                                });
                            }
                        });
                    } else if (tendina !== "" && ragione_sociale.length > 2 && ((comanda.partita_iva_estera === true && partita_iva_estera.length > 0) || (partita_iva.length === 11 || partita_iva.length === 16))) {
                        resolve(tendina);
                    } else {
                        resolve('N');
                    }

                } else if (comanda.tipologia_ricevuta.indexOf("SCONTRINO") !== -1) {
                    resolve('N');
                }

            });
            premessa.then(function (id_cliente) {

                testa.id_cliente = id_cliente;
                var totale_progressivo = 0;
                var contanti = $('#popup_metodo_pagamento_importo_contanti').val();
                if (parseFloat(contanti) > 0 && totale_progressivo < testa.totale) {
                    var resto = $('#popup_metodo_pagamento_resto').html();
                    testa.contanti = parseFloat(contanti) - parseFloat(resto);
                    totale_progressivo += parseFloat(contanti);
                } else if (parseFloat(contanti) > 0) {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai già raggiunto o superato il totale.");
                    return false;
                }

                var pos = $('#popup_metodo_pagamento_importo_pos').val();
                if (parseFloat(pos) > 0 && totale_progressivo < testa.totale) {
                    testa.pos = pos;
                    totale_progressivo += parseFloat(pos);
                } else if (parseFloat(pos) > 0) {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai già raggiunto o superato il totale.");
                    return false;
                }

                var droppay = $('#popup_metodo_pagamento_importo_droppay').val();
                if (parseFloat(droppay) > 0 && totale_progressivo < testa.totale) {
                    testa.droppay = droppay;
                    totale_progressivo += parseFloat(droppay);
                } else if (parseFloat(droppay) > 0) {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai già raggiunto o superato il totale.");
                    return false;
                }

                var bancomat = $('#popup_metodo_pagamento_importo_bancomat').val();
                if (parseFloat(bancomat) > 0 && totale_progressivo < testa.totale) {
                    testa.bancomat = bancomat;
                    totale_progressivo += parseFloat(bancomat);
                } else if (parseFloat(bancomat) > 0) {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai già raggiunto o superato il totale.");
                    return false;
                }

                var cartadicredito = $('#popup_metodo_pagamento_importo_cc').val();
                if (parseFloat(cartadicredito) > 0 && totale_progressivo < testa.totale) {
                    testa.cartadicredito = cartadicredito;
                    totale_progressivo += parseFloat(cartadicredito);
                } else if (parseFloat(cartadicredito) > 0) {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai già raggiunto o superato il totale.");
                    return false;
                }

                var cartadicredito2 = $('#popup_metodo_pagamento_importo_cc2').val();
                if (parseFloat(cartadicredito2) > 0 && totale_progressivo < testa.totale) {
                    testa.cartadicredito2 = cartadicredito2;
                    totale_progressivo += parseFloat(cartadicredito2);
                } else if (parseFloat(cartadicredito2) > 0) {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai giÃ Â  raggiunto o superato il totale.");
                    return false;
                }

                var cartadicredito3 = $('#popup_metodo_pagamento_importo_cc3').val();
                if (parseFloat(cartadicredito3) > 0 && totale_progressivo < testa.totale) {
                    testa.cartadicredito3 = cartadicredito3;
                    totale_progressivo += parseFloat(cartadicredito3);
                } else if (parseFloat(cartadicredito3) > 0) {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai giÃ Â  raggiunto o superato il totale.");
                    return false;
                }

                var monouso = $('#popup_metodo_pagamento_importo_monouso').val();
                if (parseFloat(monouso) > 0 && totale_progressivo < testa.totale) {
                    testa.monouso = monouso;
                    totale_progressivo += parseFloat(monouso);
                } else if (parseFloat(monouso) > 0) {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai giÃ Â  raggiunto o superato il totale.");
                    return false;
                }


                var buoni_acquisto = $('#popup_metodo_pagamento_importo_buoni_acquisto').val();
                if (parseFloat(buoni_acquisto) > 0 && totale_progressivo < testa.totale) {
                    testa.buoniacquisto = buoni_acquisto;
                    totale_progressivo += parseFloat(buoni_acquisto);
                } else if (parseFloat(buoni_acquisto) > 0) {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai già raggiunto o superato il totale.");
                    return false;
                }

                var non_riscosso_servizi = $('#popup_metodo_pagamento_importo_non_riscosso_servizi').val();
                if (parseFloat(non_riscosso_servizi) > 0 && totale_progressivo < testa.totale) {
                    testa.non_riscosso_servizi = non_riscosso_servizi;
                    totale_progressivo += parseFloat(non_riscosso_servizi);
                } else if (parseFloat(non_riscosso_servizi) > 0) {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai già raggiunto o superato il totale.");
                    return false;
                }

                var sconto_a_pagare = $('#popup_metodo_pagamento_importo_sconto_a_pagare').val();
                if (parseFloat(sconto_a_pagare) > 0 && totale_progressivo < testa.totale) {
                    testa.sconto_a_pagare = sconto_a_pagare;
                    totale_progressivo += parseFloat(sconto_a_pagare);
                } else if (parseFloat(sconto_a_pagare) > 0) {
                    alert("Non puoi aggiungere ulteriori metodi di pagamento, se hai già raggiunto o superato il totale.");
                    return false;
                }



                console.log("DATI TESTA TEMPORANEI", testa);
                $('#scelta_metodo_pagamento').modal('hide');

                /*stampa_scontrino_asporto(comanda.tipologia_ricevuta, testa);*/
                stampa_scontrino(comanda.tipologia_ricevuta, testa);

                setTimeout(function () {

                    if (gia_infornato !== true) {
                        /* aggiunto true, true il 17/11/2020 per evitare che chieda l intestazione del cliente in caso di scontrino diretto*/
                        inforna(true, true);
                    }
                    if ($('.scelta_buoni__tipo_pagamento_restante')[0] !== undefined) {
                        $('.scelta_buoni__tipo_pagamento_restante')[0].selectedIndex = 0;
                    }
                    $('.scelta_buoni__restante').html(0);
                    $('[name="scelta_buoni__quantita_buoni"]').val(1);
                    $('.scelta_buoni__ammontare_buono').html(0);
                    $('#scelta_buoni [name="scelta_buoni__buono_spesa"]').val(0);
                    if ($('.scelta_buoni__tipo_pagamento_restante')[0] !== undefined) {
                        $('.scelta_buoni__tipo_pagamento_restante')[0].selectedIndex = 0;
                    }
                    buoni_memorizzati = [];
                }, 2000);
            });
        }
    }
}

function dati_comanda(varianti_unite, CallBack) {

    if (varianti_unite === true) {
        var testo_query = "select cod_articolo,desc_art,quantita,prezzo_un,nodo,prog_inser,contiene_variante,categoria,dest_stampa,portata,(select desc_art from comanda as c2 where stato_record='ATTIVO' and c2.nodo=substr(c1.nodo,1,3)) as articolo_main from comanda as c1 where stato_record='ATTIVO' order by articolo_main,nodo asc;";

        comanda.sincro.query(testo_query, function (risultato) {

            var conto = new Array();
            var buffer_varianti = new Array();
            risultato.forEach(function (obj) {


                var posizione = -1; //INDEX OF FOUND ELEMENT IN CONTO


                //Solo per gli articoli principali
                if (obj.desc_art[0] !== '+' && obj.desc_art[0] !== '-' && obj.desc_art[0] !== '(' && obj.desc_art[0] !== '*' && obj.desc_art[0] !== '=') {

                    //Iteratore per vedere se ci sono articoli uguali
                    conto.some(function (iterator, index, _ary) {

                        console.log("CONFRONTO ARTICOLI UGUALI", iterator.desc_art, obj.desc_art);
                        if (iterator.desc_art === obj.desc_art) {
                            posizione = index;
                            return true;
                        }

                    });
                    console.log("POSIZIONE TROVATA: ", posizione);
                    //FINE ITERATORE

                    console.log(posizione);
                    if (posizione !== -1) {
                        conto[posizione].quantita = (parseInt(conto[posizione].quantita) + parseInt(obj.quantita)).toString();
                    } else {
                        conto.push(obj);
                    }
                }
                //Solo per le varianti
                else {


                    //Se la variante appartiene allo stesso articolo di quella di prima la unisce nella stessa riga
                    //NB Quella di prima però dev'essere una variante
                    if (conto[conto.length - 1] !== undefined && conto[conto.length - 1].nodo !== undefined && conto[conto.length - 1].nodo.length > 3 && obj.nodo.substr(0, 3) === conto[conto.length - 1].nodo.substr(0, 3)) {
                        conto[conto.length - 1].desc_art += ' ' + obj.desc_art;
                        conto[conto.length - 1].prezzo_un = parseFloat(conto[conto.length - 1].prezzo_un) + parseFloat(obj.prezzo_un);
                    } else {
                        conto.push(obj);
                    }
                }

            });
            var conto_varianti_perfezionate = new Array();
            conto.forEach(function (obj) {

                console.log(obj);
                var posizione = -1; //INDEX OF FOUND ELEMENT IN CONTO

                //Solo per gli articoli principali
                if (obj.desc_art[0] !== '+' && obj.desc_art[0] !== '-' && obj.desc_art[0] !== '(' && obj.desc_art[0] !== '*' && obj.desc_art[0] !== '=') {

                    //Iteratore per vedere se ci sono articoli uguali
                    conto_varianti_perfezionate.some(function (iterator, index, _ary) {

                        console.log(iterator.desc_art, obj.desc_art);
                        if (iterator.desc_art === obj.desc_art) {

                            posizione = index;
                            return true;
                        }

                    });
                    console.log("POSIZIONE TROVATA: ", posizione);
                    //FINE ITERATORE

                    console.log(posizione);
                    if (posizione !== -1) {
                        conto_varianti_perfezionate[posizione].quantita = (parseInt(conto_varianti_perfezionate[posizione].quantita) + parseInt(obj.quantita)).toString();
                    } else {
                        conto_varianti_perfezionate.push(obj);
                    }
                } else {
                    //Se la variante ha lo stesso nome dell' articolo precedente aggiunge solo la quantita

                    var quantita_aumentata = false;

                    conto_varianti_perfezionate.forEach(function (a, b) {

                        if (a.desc_art === obj.desc_art) {
                            conto_varianti_perfezionate[b].quantita = (parseInt(conto_varianti_perfezionate[b].quantita) + parseInt(obj.quantita)).toString();

                            quantita_aumentata = true;
                        }

                    });

                    if (quantita_aumentata === false) {
                        conto_varianti_perfezionate.push(obj);
                    }
                }

            });
            console.log(conto_varianti_perfezionate);
            if (typeof CallBack === "function") {
                CallBack(conto_varianti_perfezionate);
            }

        });
    } else {
        //24/05/2018
        //MODIFICATO 'M' ||substr(nodo,1,3)  in 'M' || nodo  //4 RIGHE - ANCHE SUL SECONDO CONTO - ESPERIMENTO
        //MODIFICATO PER VEDERE LE VARIANTI IN ORDINE DI BATTITURA E PERFORMANCE
        //WHERE substr(desc_art,1,1)

        var testo_query = "select  substr(desc_art,1,3)||'M'|| substr(nodo,1,3) as ord,cod_articolo,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita from comanda as c1 where contiene_variante='1' and ntav_comanda='" + comanda.tavolo + "'  and posizione='CONTO' and stato_record='ATTIVO' \n\
                           union all\n\
                           select  substr(desc_art,1,3)||'M'||substr(nodo,1,3) as ord,cod_articolo,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,sum(quantita) as quantita from comanda as c1 where (contiene_variante !='1' or contiene_variante is null or contiene_variante='null') and length(nodo)=3 and ntav_comanda='" + comanda.tavolo + "' and posizione='CONTO' and stato_record='ATTIVO' group by desc_art,nome_comanda\n\
                           union all\n\
                           select  (select substr(desc_art,1,3) from comanda where nodo=substr(c1.nodo,1,3) and stato_record='ATTIVO'  and ntav_comanda='" + comanda.tavolo + "')||'M' ||nodo as ord,cod_articolo,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita from comanda as c1 where length(nodo)=7 and substr(desc_art,1,1)='-' and ntav_comanda='" + comanda.tavolo + "'   and posizione='CONTO' and stato_record='ATTIVO'\n\
                           union all\n\
                           select (select substr(desc_art,1,3) from comanda where nodo=substr(c1.nodo,1,3) and stato_record='ATTIVO'  and ntav_comanda='" + comanda.tavolo + "')||'M'||nodo as ord,cod_articolo,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita from comanda as c1 where length(nodo)=7 and substr(desc_art,1,1)!='-' and ntav_comanda='" + comanda.tavolo + "'  and posizione='CONTO' and stato_record='ATTIVO'\n\
                           order by nome_comanda DESC, ord ASC;";
        comanda.sincro.query(testo_query, function (res) {

            //QUESTA E' LA COMANDA DIVISA PER NODI
            var comanda_riunita_diviggiano = new Object();

            res.forEach(function (el, key) {
                if (comanda_riunita_diviggiano[el.ord] === undefined) {
                    comanda_riunita_diviggiano[el.ord] = new Array();
                }

                comanda_riunita_diviggiano[el.ord].push(el);
            });

            //QUESTA E' LA COMANDA RIUNITA IN UN OGGETTO
            var comanda_finale = new Object();

            for (var ord in comanda_riunita_diviggiano) {

                //Slice serve a copiare l'array senza puntarci dentro
                var array_vecchio = JSON.parse(JSON.stringify(comanda_riunita_diviggiano[ord]));

                array_vecchio.forEach(function (el, key, arr) {
                    arr[key].ord = "";
                    arr[key].prog_inser = "";
                    arr[key].nodo = "";
                    arr[key].quantita = "";
                });


                var bool = false;
                for (var ord2 in comanda_finale) {

                    //Slice serve a copiare l'array senza puntarci dentro
                    var array_comanda = JSON.parse(JSON.stringify(comanda_finale[ord2]));

                    array_comanda.forEach(function (el, key, arr) {
                        arr[key].ord = "";
                        arr[key].prog_inser = "";
                        arr[key].nodo = "";
                        arr[key].quantita = "";
                    });

                    //Bisogna confrontare l'array diviggiano con tutti i comanda

                    //Se in comanda non ce n'è uno con lunghezza uguale e stessi elementi lo si aggiunge a comanda stesso


                    //condizione 1 : controllo che la lunghezza dell'array sia la stessa in entrambi gli oggetti
                    if (array_vecchio.length === array_comanda.length) {

                        if (JSON.stringify(array_vecchio.sort(mySorter)) == JSON.stringify(array_comanda.sort(mySorter))) {

                            comanda_finale[ord2][0].quantita = parseInt(comanda_finale[ord2][0].quantita) + 1;

                            bool = true;

                        }

                    }

                }

                if (bool !== true) {

                    comanda_finale[ord] = comanda_riunita_diviggiano[ord];

                }

            }

            //QUESTO E' L'OGGETTO FINALE TRASFORMATO IN ARRAY
            var oggetto_finale_comanda = new Array();
            for (var key in comanda_finale) {
                comanda_finale[key].forEach(function (el, key) {
                    oggetto_finale_comanda.push(el);
                });
            }


            console.log("COMANDA RIUNITA DIVIGGIANO", res, comanda_riunita_diviggiano, comanda_finale, oggetto_finale_comanda);

            if (typeof CallBack === "function") {
                CallBack(oggetto_finale_comanda);
            }
        });
    }
}



function mappa_consegne() {
    $("#popup_google_maps_mappa").modal('show');
    initMap();
}


function assegna_pony(id_comanda) {

    var query_selezione_pony = 'SELECT * FROM pony_express ORDER BY nome ASC;';

    comanda.sincro.query(query_selezione_pony, function (array_risultato) {

        var contenuto_dinamico_popup_pony = '';

        array_risultato.forEach(function (singolo_risultato) {

            contenuto_dinamico_popup_pony += '<div class="col-xs-4" style="height:50px;"><a onclick="segna_pony(\'' + singolo_risultato.id + '\',\'' + id_comanda + '\')">' + singolo_risultato.nome + '</a></div>';

        });


        var contenuto_fisso_popup_pony = '<div class="modal-body" style="overflow:auto;min-height: 14vh;max-height: 54vh;">\n\
                                ' + contenuto_dinamico_popup_pony + '\n\
                                </div>\n\
                                <div class="modal-footer">\n\
                                    <div style="margin:0;" class = "col-xs-4 btn btn-danger" data-dismiss="modal">CHIUDI</div>\n\
                            </div>';

        $("#contenuto_popup_pony").html(contenuto_fisso_popup_pony);

        $("#popup_assegna_pony").modal('show');

    });

}