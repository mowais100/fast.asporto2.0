$(document).on("change", "[name='nazione']", function () {
    selezione_campi_fattura();
});

$(document).on("click", "#fattura [name='formato_trasmissione'],#fattura  [name='azienda_privato']", function () {
    selezione_campi_fattura();
});

function selezione_campi_fattura(non_rivalorizzare_campi = false) {
    var nazione = $('#fattura [name="nazione"] option:selected').val();
    var azienda_privato = $('[name="azienda_privato"]:checked').attr("id");
    var formato_trasmissione = $('#fattura [name="formato_trasmissione"]').prop("checked");


    var ragione_sociale = $('#fattura [name="ragione_sociale"]');
    var indirizzo = $('#fattura [name="indirizzo"]');
    var numero = $('#fattura [name="numero"]');
    var cap = $('#fattura [name="cap"]');
    var citta = $('#fattura [name="citta"]');
    var comune = $('#fattura [name="comune"]');
    var provincia = $('#fattura [name="provincia"]');
    var codice_fiscale = $('#fattura [name="codice_fiscale"]');
    var partita_iva = $('#fattura [name="partita_iva"]');
    var partita_iva_estera = $('#fattura [name="partita_iva_estera"]');
    var pec = $('#fattura [name="pec"]');
    var codice_destinatario = $('#fattura [name="codice_destinatario"]');


    var cls_fat_provincia = $('#fattura .cls_fat_provincia');
    var cls_fat_codice_fiscale = $('#fattura .cls_fat_codice_fiscale');
    var cls_fat_partita_iva = $('#fattura .cls_fat_partita_iva');
    var cls_fat_partita_iva_estera = $('#fattura .cls_fat_partita_iva_estera');
    var cls_fat_pec = $('#fattura .cls_fat_pec');
    var cls_fat_codice_destinatario = $('#fattura .cls_fat_codice_destinatario');
    var cls_formato_trasmissione = $('#fattura .cls_formato_trasmissione');

    if (azienda_privato === "ap_privato") {

        $("#label_ragione_sociale").html("Nome e Cognome");

        if (nazione === "IT") {


            cap.attr("maxlength", "5");

            if (non_rivalorizzare_campi !== true) {
                codice_destinatario.val('');
                provincia.val('');

            }

            partita_iva_estera.val('');

            cls_fat_partita_iva_estera.hide();

            cls_formato_trasmissione.hide();
            cls_fat_provincia.show();
            cls_fat_partita_iva.hide();
            cls_fat_codice_fiscale.show();
            cls_fat_pec.hide();
            cls_fat_codice_destinatario.hide();

        } else if (
                nazione === "AT" || nazione === "BE" || nazione === "BG" || nazione === "CY" || nazione === "HR" ||
                nazione === "DK" || nazione === "EE" || nazione === "FI" || nazione === "FR" || nazione === "DE" ||
                nazione === "GR" || nazione === "IE" || nazione === "LV" || nazione === "LT" || nazione === "LU" ||
                nazione === "MT" || nazione === "NL" || nazione === "PL" || nazione === "PT" || nazione === "UK" ||
                nazione === "CZ" || nazione === "RO" || nazione === "SK" || nazione === "SI" || nazione === "ES" ||
                nazione === "SE" || nazione === "HU") {

            cap.attr("maxlength", "100");

            $('#fattura [name="formato_trasmissione"]').prop("checked", false);

            codice_destinatario.val('XXXXXXX');
            if (non_rivalorizzare_campi !== true) {
                provincia.val('DA');
            }
            codice_fiscale.val('');
            partita_iva.val('');

            cls_formato_trasmissione.hide();
            cls_fat_codice_fiscale.hide();
            cls_fat_partita_iva.hide();
            cls_fat_pec.hide();
            cls_fat_provincia.hide();
            cls_fat_codice_destinatario.hide();

            cls_fat_partita_iva_estera.hide();
        } else {
            cap.attr("maxlength", "100");

            $('#fattura [name="formato_trasmissione"]').prop("checked", false);

            if (non_rivalorizzare_campi !== true) {
                provincia.val('DA');
            }
            codice_fiscale.val('');
            partita_iva.val('');

            codice_destinatario.val('XXXXXXX');

            cls_formato_trasmissione.hide();
            cls_fat_provincia.hide();
            cls_fat_partita_iva.hide();
            cls_fat_codice_fiscale.hide();
            cls_fat_pec.hide();
            cls_fat_codice_destinatario.hide();

            cls_fat_partita_iva_estera.hide();
        }
    } else {

        $("#label_ragione_sociale").html("Ragione Sociale");

        if (nazione === "IT") {


            cap.attr("maxlength", "5");

            if (non_rivalorizzare_campi !== true) {
                codice_destinatario.val('');
                provincia.val('');
            }

            partita_iva_estera.val('');

            cls_fat_partita_iva_estera.hide();

            cls_formato_trasmissione.show();
            cls_fat_provincia.show();
            cls_fat_partita_iva.show();
            cls_fat_codice_fiscale.show();
            cls_fat_pec.show();
            cls_fat_codice_destinatario.show();

        } else if (
                nazione === "AT" || nazione === "BE" || nazione === "BG" || nazione === "CY" || nazione === "HR" ||
                nazione === "DK" || nazione === "EE" || nazione === "FI" || nazione === "FR" || nazione === "DE" ||
                nazione === "GR" || nazione === "IE" || nazione === "LV" || nazione === "LT" || nazione === "LU" ||
                nazione === "MT" || nazione === "NL" || nazione === "PL" || nazione === "PT" || nazione === "UK" ||
                nazione === "CZ" || nazione === "RO" || nazione === "SK" || nazione === "SI" || nazione === "ES" ||
                nazione === "SE" || nazione === "HU") {

            cap.attr("maxlength", "100");

            $('#fattura [name="formato_trasmissione"]').prop("checked", false);

            codice_destinatario.val('XXXXXXX');

            if (non_rivalorizzare_campi !== true) {
                provincia.val('DA');
            }
            codice_fiscale.val('');
            partita_iva.val('');

            cls_formato_trasmissione.hide();
            cls_fat_codice_fiscale.hide();
            cls_fat_partita_iva.hide();
            cls_fat_pec.hide();
            cls_fat_provincia.hide();
            cls_fat_codice_destinatario.hide();

            cls_fat_partita_iva_estera.show();
        } else {
            cap.attr("maxlength", "100");

            $('#fattura [name="formato_trasmissione"]').prop("checked", false);

            if (non_rivalorizzare_campi !== true) {
                provincia.val('DA');
            }
            codice_fiscale.val('');
            partita_iva.val('');

            codice_destinatario.val('XXXXXXX');

            cls_formato_trasmissione.hide();
            cls_fat_provincia.hide();
            cls_fat_partita_iva.hide();
            cls_fat_codice_fiscale.hide();
            cls_fat_pec.hide();
            cls_fat_codice_destinatario.hide();

            cls_fat_partita_iva_estera.show();
        }
}


}

function selezione_campi_gestione_cliente(non_rivalorizzare_campi = false) {
    var nazione = $('#form_gestione_clienti [name="nazione"] option:selected').val();
    var azienda_privato = $('#form_gestione_clienti [name="azienda_privato"]:checked').attr("id");
    if (azienda_privato === "gest_cli_ap_privato") {
        azienda_privato = "ap_privato";
    } else {
        azienda_privato = "ap_azienda";
    }
    var formato_trasmissione = $('#form_gestione_clienti [name="formato_trasmissione"]').prop("checked");


    var ragione_sociale = $('#form_gestione_clienti [name="ragione_sociale"]');
    var indirizzo = $('#form_gestione_clienti [name="indirizzo"]');
    var numero = $('#form_gestione_clienti [name="numero"]');
    var cap = $('#form_gestione_clienti [name="cap"]');
    var citta = $('v [name="citta"]');
    var comune = $('#form_gestione_clienti [name="comune"]');
    var provincia = $('#form_gestione_clienti [name="provincia"]');
    var codice_fiscale = $('#form_gestione_clienti [name="codice_fiscale"]');
    var partita_iva = $('#form_gestione_clienti [name="partita_iva"]');
    var partita_iva_estera = $('#form_gestione_clienti [name="partita_iva_estera"]');
    var pec = $('#form_gestione_clienti [name="pec"]');
    var codice_destinatario = $('#form_gestione_clienti [name="codice_destinatario"]');


    var cls_fat_provincia = $('#form_gestione_clienti .cls_fat_provincia');
    var cls_fat_codice_fiscale = $('#form_gestione_clienti .cls_fat_codice_fiscale');
    var cls_fat_partita_iva = $('#form_gestione_clienti .cls_fat_partita_iva');
    var cls_fat_partita_iva_estera = $('#form_gestione_clienti .cls_fat_partita_iva_estera');
    var cls_fat_pec = $('#form_gestione_clienti .cls_fat_pec');
    var cls_fat_codice_destinatario = $('#form_gestione_clienti .cls_fat_codice_destinatario');
    var cls_formato_trasmissione = $('#form_gestione_clienti .cls_formato_trasmissione');

    if (azienda_privato === "ap_privato") {

        $("#label_ragione_sociale_gestione_clienti").html("Nome e Cognome");

        if (nazione === "IT") {


            cap.attr("maxlength", "5");

            if (non_rivalorizzare_campi !== true) {
                codice_destinatario.val('');
                provincia.val('');

            }

            partita_iva_estera.val('');

            cls_fat_partita_iva_estera.hide();

            cls_formato_trasmissione.hide();
            cls_fat_provincia.show();
            cls_fat_partita_iva.hide();
            cls_fat_codice_fiscale.show();
            cls_fat_pec.hide();
            cls_fat_codice_destinatario.hide();

        } else if (
                nazione === "AT" || nazione === "BE" || nazione === "BG" || nazione === "CY" || nazione === "HR" ||
                nazione === "DK" || nazione === "EE" || nazione === "FI" || nazione === "FR" || nazione === "DE" ||
                nazione === "GR" || nazione === "IE" || nazione === "LV" || nazione === "LT" || nazione === "LU" ||
                nazione === "MT" || nazione === "NL" || nazione === "PL" || nazione === "PT" || nazione === "UK" ||
                nazione === "CZ" || nazione === "RO" || nazione === "SK" || nazione === "SI" || nazione === "ES" ||
                nazione === "SE" || nazione === "HU") {

            cap.attr("maxlength", "100");

            $('#form_gestione_clienti [name="formato_trasmissione"]').prop("checked", false);

            codice_destinatario.val('XXXXXXX');
            if (non_rivalorizzare_campi !== true) {
                provincia.val('DA');
            }
            codice_fiscale.val('');
            partita_iva.val('');

            cls_formato_trasmissione.hide();
            cls_fat_codice_fiscale.hide();
            cls_fat_partita_iva.hide();
            cls_fat_pec.hide();
            cls_fat_provincia.hide();
            cls_fat_codice_destinatario.hide();

            cls_fat_partita_iva_estera.hide();
        } else {
            cap.attr("maxlength", "100");

            $('#form_gestione_clienti [name="formato_trasmissione"]').prop("checked", false);

            if (non_rivalorizzare_campi !== true) {
                provincia.val('DA');
            }
            codice_fiscale.val('');
            partita_iva.val('');

            codice_destinatario.val('XXXXXXX');

            cls_formato_trasmissione.hide();
            cls_fat_provincia.hide();
            cls_fat_partita_iva.hide();
            cls_fat_codice_fiscale.hide();
            cls_fat_pec.hide();
            cls_fat_codice_destinatario.hide();

            cls_fat_partita_iva_estera.hide();
        }
    } else {

        $("#label_ragione_sociale_gestione_clienti").html("Ragione Sociale");

        if (nazione === "IT") {


            cap.attr("maxlength", "5");

            if (non_rivalorizzare_campi !== true) {
                codice_destinatario.val('');
                provincia.val('');
            }

            partita_iva_estera.val('');

            cls_fat_partita_iva_estera.hide();

            cls_formato_trasmissione.show();
            cls_fat_provincia.show();
            cls_fat_partita_iva.show();
            cls_fat_codice_fiscale.show();
            cls_fat_pec.show();
            cls_fat_codice_destinatario.show();

        } else if (
                nazione === "AT" || nazione === "BE" || nazione === "BG" || nazione === "CY" || nazione === "HR" ||
                nazione === "DK" || nazione === "EE" || nazione === "FI" || nazione === "FR" || nazione === "DE" ||
                nazione === "GR" || nazione === "IE" || nazione === "LV" || nazione === "LT" || nazione === "LU" ||
                nazione === "MT" || nazione === "NL" || nazione === "PL" || nazione === "PT" || nazione === "UK" ||
                nazione === "CZ" || nazione === "RO" || nazione === "SK" || nazione === "SI" || nazione === "ES" ||
                nazione === "SE" || nazione === "HU") {

            cap.attr("maxlength", "100");

            $('#form_gestione_clienti [name="formato_trasmissione"]').prop("checked", false);

            codice_destinatario.val('XXXXXXX');

            if (non_rivalorizzare_campi !== true) {
                provincia.val('DA');
            }
            codice_fiscale.val('');
            partita_iva.val('');

            cls_formato_trasmissione.hide();
            cls_fat_codice_fiscale.hide();
            cls_fat_partita_iva.hide();
            cls_fat_pec.hide();
            cls_fat_provincia.hide();
            cls_fat_codice_destinatario.hide();

            cls_fat_partita_iva_estera.show();
        } else {
            cap.attr("maxlength", "100");

            $('#form_gestione_clienti [name="formato_trasmissione"]').prop("checked", false);

            if (non_rivalorizzare_campi !== true) {
                provincia.val('DA');
            }
            codice_fiscale.val('');
            partita_iva.val('');

            codice_destinatario.val('XXXXXXX');

            cls_formato_trasmissione.hide();
            cls_fat_provincia.hide();
            cls_fat_partita_iva.hide();
            cls_fat_codice_fiscale.hide();
            cls_fat_pec.hide();
            cls_fat_codice_destinatario.hide();

            cls_fat_partita_iva_estera.show();
        }
}


}

function selezione_campi_nonpagato(non_rivalorizzare_campi = false) {
    var nazione = "IT";
    var azienda_privato = "ap_azienda";
    var formato_trasmissione = $('#popup_nonpagato [name="formato_trasmissione"]').prop("checked");


    var ragione_sociale = $('#popup_nonpagato [name="ragione_sociale"]');
    var indirizzo = $('#popup_nonpagato [name="indirizzo"]');
    var numero = $('#popup_nonpagato [name="numero"]');
    var cap = $('#popup_nonpagato [name="cap"]');
    var citta = $('#popup_nonpagato [name="citta"]');
    var comune = $('#popup_nonpagato [name="comune"]');
    var provincia = $('#popup_nonpagato [name="provincia"]');
    var codice_fiscale = $('#popup_nonpagato [name="codice_fiscale"]');
    var partita_iva = $('#popup_nonpagato [name="partita_iva"]');
    var partita_iva_estera = $('#popup_nonpagato [name="partita_iva_estera"]');
    var pec = $('#popup_nonpagato [name="pec"]');
    var codice_destinatario = $('#popup_nonpagato [name="codice_destinatario"]');


    var cls_fat_provincia = $('#popup_nonpagato .cls_fat_provincia');
    var cls_fat_codice_fiscale = $('#popup_nonpagato .cls_fat_codice_fiscale');
    var cls_fat_partita_iva = $('#popup_nonpagato .cls_fat_partita_iva');
    var cls_fat_partita_iva_estera = $('#popup_nonpagato .cls_fat_partita_iva_estera');
    var cls_fat_pec = $('#popup_nonpagato .cls_fat_pec');
    var cls_fat_codice_destinatario = $('#popup_nonpagato .cls_fat_codice_destinatario');
    var cls_formato_trasmissione = $('#popup_nonpagato .cls_formato_trasmissione');

    if (azienda_privato === "ap_privato") {

        $("#label_ragione_sociale_nonpagato").html("Nome e Cognome");

        if (nazione === "IT") {


            cap.attr("maxlength", "5");

            if (non_rivalorizzare_campi !== true) {
                codice_destinatario.val('');
                provincia.val('');

            }

            partita_iva_estera.val('');

            cls_fat_partita_iva_estera.hide();

            cls_formato_trasmissione.hide();
            cls_fat_provincia.show();
            cls_fat_partita_iva.hide();
            cls_fat_codice_fiscale.show();
            cls_fat_pec.hide();
            cls_fat_codice_destinatario.hide();

        } else if (
                nazione === "AT" || nazione === "BE" || nazione === "BG" || nazione === "CY" || nazione === "HR" ||
                nazione === "DK" || nazione === "EE" || nazione === "FI" || nazione === "FR" || nazione === "DE" ||
                nazione === "GR" || nazione === "IE" || nazione === "LV" || nazione === "LT" || nazione === "LU" ||
                nazione === "MT" || nazione === "NL" || nazione === "PL" || nazione === "PT" || nazione === "UK" ||
                nazione === "CZ" || nazione === "RO" || nazione === "SK" || nazione === "SI" || nazione === "ES" ||
                nazione === "SE" || nazione === "HU") {

            cap.attr("maxlength", "100");

            $('#popup_nonpagato [name="formato_trasmissione"]').prop("checked", false);

            codice_destinatario.val('XXXXXXX');
            if (non_rivalorizzare_campi !== true) {
                provincia.val('DA');
            }
            codice_fiscale.val('');
            partita_iva.val('');

            cls_formato_trasmissione.hide();
            cls_fat_codice_fiscale.hide();
            cls_fat_partita_iva.hide();
            cls_fat_pec.hide();
            cls_fat_provincia.hide();
            cls_fat_codice_destinatario.hide();

            cls_fat_partita_iva_estera.hide();
        } else {
            cap.attr("maxlength", "100");

            $('#popup_nonpagato [name="formato_trasmissione"]').prop("checked", false);

            if (non_rivalorizzare_campi !== true) {
                provincia.val('DA');
            }
            codice_fiscale.val('');
            partita_iva.val('');

            codice_destinatario.val('XXXXXXX');

            cls_formato_trasmissione.hide();
            cls_fat_provincia.hide();
            cls_fat_partita_iva.hide();
            cls_fat_codice_fiscale.hide();
            cls_fat_pec.hide();
            cls_fat_codice_destinatario.hide();

            cls_fat_partita_iva_estera.hide();
        }
    } else {

        $("#label_ragione_sociale_nonpagato").html("Ragione Sociale");

        if (nazione === "IT") {


            cap.attr("maxlength", "5");

            if (non_rivalorizzare_campi !== true) {
                codice_destinatario.val('');
                provincia.val('');
            }

            partita_iva_estera.val('');

            cls_fat_partita_iva_estera.hide();

            cls_formato_trasmissione.show();
            cls_fat_provincia.show();
            cls_fat_partita_iva.show();
            cls_fat_codice_fiscale.show();
            cls_fat_pec.show();
            cls_fat_codice_destinatario.show();

        } else if (
                nazione === "AT" || nazione === "BE" || nazione === "BG" || nazione === "CY" || nazione === "HR" ||
                nazione === "DK" || nazione === "EE" || nazione === "FI" || nazione === "FR" || nazione === "DE" ||
                nazione === "GR" || nazione === "IE" || nazione === "LV" || nazione === "LT" || nazione === "LU" ||
                nazione === "MT" || nazione === "NL" || nazione === "PL" || nazione === "PT" || nazione === "UK" ||
                nazione === "CZ" || nazione === "RO" || nazione === "SK" || nazione === "SI" || nazione === "ES" ||
                nazione === "SE" || nazione === "HU") {

            cap.attr("maxlength", "100");

            $('#popup_nonpagato [name="formato_trasmissione"]').prop("checked", false);

            codice_destinatario.val('XXXXXXX');

            if (non_rivalorizzare_campi !== true) {
                provincia.val('DA');
            }
            codice_fiscale.val('');
            partita_iva.val('');

            cls_formato_trasmissione.hide();
            cls_fat_codice_fiscale.hide();
            cls_fat_partita_iva.hide();
            cls_fat_pec.hide();
            cls_fat_provincia.hide();
            cls_fat_codice_destinatario.hide();

            cls_fat_partita_iva_estera.show();
        } else {
            cap.attr("maxlength", "100");

            $('#popup_nonpagato [name="formato_trasmissione"]').prop("checked", false);

            if (non_rivalorizzare_campi !== true) {
                provincia.val('DA');
            }
            codice_fiscale.val('');
            partita_iva.val('');

            codice_destinatario.val('XXXXXXX');

            cls_formato_trasmissione.hide();
            cls_fat_provincia.hide();
            cls_fat_partita_iva.hide();
            cls_fat_codice_fiscale.hide();
            cls_fat_pec.hide();
            cls_fat_codice_destinatario.hide();

            cls_fat_partita_iva_estera.show();
        }
}


}

async function controlla_esistenza_piva_fattura()
{
    return new Promise(resolve => {

        /* DATI NUOVI */
        var ragione_sociale = $('#fattura [name="ragione_sociale"]').val().toUpperCase();
        var indirizzo = $('#fattura [name="indirizzo"]').val().toUpperCase();
        var numero = $('#fattura [name="numero"]').val().toUpperCase();
        var cap = $('#fattura [name="cap"]').val();
        var citta = $('#fattura [name="citta"]').val();
        var provincia = $('#fattura [name="provincia"]').val().toUpperCase();
        var paese = $('#fattura [name="paese"]').val();
        var cellulare = $('#fattura [name="cellulare"]').val();
        var telefono = $('#fattura [name="telefono"]').val();
        var fax = $('#fattura [name="fax"]').val();
        var email = $('#fattura [name="email"]').val();
        var codice_fiscale = $('#fattura [name="codice_fiscale"]').val().toUpperCase();
        var partita_iva = $('#fattura [name="partita_iva"]').val();
        var partita_iva_estera = $('#fattura [name="partita_iva_estera"]').val();
        var formato_trasmissione = $('#fattura [name="formato_trasmissione"]').prop("checked");
        var split_payment = $('#fattura [name="split_payment"]').prop("checked");
        var codice_destinatario = $('#fattura [name="codice_destinatario"]').val().toUpperCase();
        var regime_fiscale = $('#fattura [name="regime_fiscale"]').val();
        var pec = $('#fattura [name="pec"]').val();
        var comune = $('#fattura [name="comune"]').val().toUpperCase();
        var nazione = $('#fattura [name="nazione"] option:selected').val();
        var azienda_privato = $('[name="azienda_privato"]:checked').attr("id");

        /*DATI VECCHI*/
        var testo_query = "select * from clienti where partita_iva='" + partita_iva + "' limit 1;";
        var old = alasql(testo_query);

        if (old[0] !== undefined) {

            for (var b in old[0]) {
                if (old[0][b] === null) {
                    old[0][b] = "";
                }
            }

            var dati_identici = true;

            /* CONTROLLI NON STRETTI PER EVITARE INCOMPATIBILITA TRA TIPI DI VARIABILE TIPO false e "false" */
            if (old[0].ragione_sociale.trim().toUpperCase() != ragione_sociale.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].indirizzo.trim().toUpperCase() != indirizzo.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].numero.trim().toUpperCase() != numero.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].cap.trim().toUpperCase() != cap.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].citta !== undefined && citta !== undefined && old[0].citta.trim().toUpperCase() != citta.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].provincia.trim().toUpperCase() != provincia.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].cellulare.trim().toUpperCase() != cellulare.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].telefono.trim().toUpperCase() != telefono.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].email.trim().toUpperCase() != email.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].codice_fiscale.trim().toUpperCase() != codice_fiscale.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].partita_iva.trim().toUpperCase() != partita_iva.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].partita_iva_estera.trim().toUpperCase() != partita_iva_estera.trim().toUpperCase()) {
                dati_identici = false;
            }

            if (old[0].formato_trasmissione === "") {
                old[0].formato_trasmissione = "false";
            }

            if (old[0].formato_trasmissione != formato_trasmissione.toString()) {
                dati_identici = false;
            }

            if (old[0].codice_destinatario.trim().toUpperCase() != codice_destinatario.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].regime_fiscale.trim().toUpperCase() != regime_fiscale.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].pec.trim().toUpperCase() != pec.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].comune.trim().toUpperCase() != comune.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].nazione.trim().toUpperCase() != nazione.trim().toUpperCase()) {
                dati_identici = false;
            }

            if (dati_identici === false) {

                bootbox.confirm("Vuoi sovrascrivere i dati precedenti di " + ragione_sociale.trim().toUpperCase() + "?", function (risposta) {

                    if (risposta === true) {

                        var query_update = "update clienti set \n\
                                        tipo_cliente='" + azienda_privato + "',\n\
                                        ragione_sociale='" + ragione_sociale + "',\n\
                                        indirizzo='" + indirizzo + "',\n\
                                        numero='" + numero + "',\n\
                                        cap='" + cap + "',\n\
                                        citta='" + citta + "',\n\
                                        provincia='" + provincia + "',\n\
                                        cellulare='" + cellulare + "',\n\
                                        telefono='" + telefono + "',\n\
                                        email='" + email + "',\n\
                                        codice_fiscale='" + codice_fiscale + "',\n\
                                        partita_iva='" + partita_iva + "',\n\
                                        partita_iva_estera='" + partita_iva_estera + "',\n\
                                        split_payment = '" + split_payment + "',\n\
                                        formato_trasmissione='" + formato_trasmissione + "',\n\
                                        codice_destinatario='" + codice_destinatario + "',\n\
                                        regime_fiscale='" + regime_fiscale + "',\n\
                                        pec='" + pec + "',\n\
                                        comune='" + comune + "',\n\
                                        nazione='" + nazione + "' \n\
                                        where partita_iva='" + partita_iva + "';";
                        comanda.sock.send({tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: query_update, terminale: comanda.terminale, ip: comanda.ip_address});
                        alasql(query_update);

                        $("#fattura_elenco_clienti option[value='" + old[0].id + "']").prop('selected', true);
                        $("#fattura_elenco_clienti").trigger("change");

                        resolve(true);

                    } else
                    {

                        $("#fattura_elenco_clienti option[value='" + old[0].id + "']").prop('selected', true);
                        $("#fattura_elenco_clienti").trigger("change");

                        resolve(false);

                    }

                });

            } else
            {



                $("#fattura_elenco_clienti option[value='" + old[0].id + "']").prop('selected', true);
                $("#fattura_elenco_clienti").trigger("change");

                resolve(true);

            }




        } else
        {
            resolve(true);
        }
    });
}

async function controlla_esistenza_cliente_fattura()
{
    return new Promise(resolve => {

        /* DATI NUOVI */
        var ragione_sociale = $('#fattura [name="ragione_sociale"]').val().toUpperCase();
        var indirizzo = $('#fattura [name="indirizzo"]').val().toUpperCase();
        var numero = $('#fattura [name="numero"]').val().toUpperCase();
        var cap = $('#fattura [name="cap"]').val();
        var citta = $('#fattura [name="citta"]').val();
        var provincia = $('#fattura [name="provincia"]').val().toUpperCase();
        var paese = $('#fattura [name="paese"]').val();
        var cellulare = $('#fattura [name="cellulare"]').val();
        var telefono = $('#fattura [name="telefono"]').val();
        var fax = $('#fattura [name="fax"]').val();
        var email = $('#fattura [name="email"]').val();
        var codice_fiscale = $('#fattura [name="codice_fiscale"]').val().toUpperCase();
        var partita_iva = $('#fattura [name="partita_iva"]').val();
        var partita_iva_estera = $('#fattura [name="partita_iva_estera"]').val();
        var formato_trasmissione = $('#fattura [name="formato_trasmissione"]').prop("checked");
        var split_payment = $('#fattura [name="split_payment"]').prop("checked");
        var codice_destinatario = $('#fattura [name="codice_destinatario"]').val().toUpperCase();
        var regime_fiscale = $('#fattura [name="regime_fiscale"]').val();
        var pec = $('#fattura [name="pec"]').val();
        var comune = $('#fattura [name="comune"]').val().toUpperCase();
        var nazione = $('#fattura [name="nazione"] option:selected').val();
        var azienda_privato = $('[name="azienda_privato"]:checked').attr("id");
        var cliente_selezionato = $("#fattura_elenco_clienti option:selected").val();

        /*DATI VECCHI*/
        var testo_query = "";
        var indice = "";
        var valore = "";

        if (cliente_selezionato !== undefined && cliente_selezionato.length > 0) {
            testo_query = "SELECT * FROM clienti WHERE id=" + parseInt(cliente_selezionato) + " LIMIT 1;";
            indice = "id";
            valore = parseInt(cliente_selezionato);
        } else if (partita_iva.trim().length > 0) {
            testo_query = "SELECT * FROM clienti WHERE partita_iva='" + partita_iva + "' LIMIT 1;";
            indice = "partita_iva";
            valore = "'" + partita_iva + "'";
        } else if (partita_iva_estera.trim().length > 0) {
            testo_query = "SELECT * FROM clienti WHERE partita_iva_estera='" + partita_iva_estera + "' LIMIT 1;";
            indice = "partita_iva_estera";
            valore = "'" + partita_iva_estera + "'";
        } else if (codice_fiscale.trim().length > 0) {
            testo_query = "SELECT * FROM clienti WHERE codice_fiscale='" + codice_fiscale + "' LIMIT 1;";
            indice = "codice_fiscale";
            valore = "'" + codice_fiscale + "'";
        } else {
            testo_query = "SELECT * FROM clienti WHERE  ragione_sociale='" + ragione_sociale + "' LIMIT 1;";
            indice = "ragione_sociale";
            valore = "'" + ragione_sociale + "'";
        }

        var old = alasql(testo_query);

        if (old[0] !== undefined) {

            for (var b in old[0]) {
                if (old[0][b] === null) {
                    old[0][b] = "";
                }
            }

            var dati_identici = true;

            /* CONTROLLI NON STRETTI PER EVITARE INCOMPATIBILITA TRA TIPI DI VARIABILE TIPO false e "false" */
            if (old[0].ragione_sociale.trim().toUpperCase() != ragione_sociale.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].indirizzo.trim().toUpperCase() != indirizzo.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].numero.trim().toUpperCase() != numero.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].cap.trim().toUpperCase() != cap.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].citta !== undefined && citta !== undefined && old[0].citta.trim().toUpperCase() != citta.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].provincia.trim().toUpperCase() != provincia.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].cellulare.trim().toUpperCase() != cellulare.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].telefono.trim().toUpperCase() != telefono.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].email.trim().toUpperCase() != email.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].codice_fiscale.trim().toUpperCase() != codice_fiscale.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].partita_iva.trim().toUpperCase() != partita_iva.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].partita_iva_estera.trim().toUpperCase() != partita_iva_estera.trim().toUpperCase()) {
                dati_identici = false;
            }
            
            //Non fare equazione identica, perchè uno è booleano e uno è stringa
            if(split_payment.toString()!==old[0].split_payment){
                dati_identici = false;
            }

            if (old[0].formato_trasmissione === "") {
                old[0].formato_trasmissione = "false";
            }

            if (old[0].formato_trasmissione != formato_trasmissione.toString()) {
                dati_identici = false;
            }

            if (old[0].codice_destinatario.trim().toUpperCase() != codice_destinatario.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].regime_fiscale.trim().toUpperCase() != regime_fiscale.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].pec.trim().toUpperCase() != pec.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].comune.trim().toUpperCase() != comune.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].nazione.trim().toUpperCase() != nazione.trim().toUpperCase()) {
                dati_identici = false;
            }

            if (dati_identici === false) {

                bootbox.confirm("Vuoi sovrascrivere i dati precedenti di " + ragione_sociale.trim().toUpperCase() + "?", function (risposta) {

                    if (risposta === true) {


                        var query_update = "update clienti set \n\
                                        tipo_cliente='" + azienda_privato + "',\n\
                                        ragione_sociale='" + ragione_sociale + "',\n\
                                        indirizzo='" + indirizzo + "',\n\
                                        numero='" + numero + "',\n\
                                        cap='" + cap + "',\n\
                                        citta='" + citta + "',\n\
                                        provincia='" + provincia + "',\n\
                                        cellulare='" + cellulare + "',\n\
                                        telefono='" + telefono + "',\n\
                                        email='" + email + "',\n\
                                        codice_fiscale='" + codice_fiscale + "',\n\
                                        partita_iva='" + partita_iva + "',\n\
                                        partita_iva_estera='" + partita_iva_estera + "',\n\
                                        split_payment = '" + split_payment + "',\n\
                                        formato_trasmissione='" + formato_trasmissione + "',\n\
                                        codice_destinatario='" + codice_destinatario + "',\n\
                                        regime_fiscale='" + regime_fiscale + "',\n\
                                        pec='" + pec + "',\n\
                                        comune='" + comune + "',\n\
                                        nazione='" + nazione + "' \n\
                                        where " + indice + "=" + valore + ";";
                        comanda.sock.send({tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: query_update, terminale: comanda.terminale, ip: comanda.ip_address});
                        alasql(query_update);

                        $("#fattura_elenco_clienti option[value='" + old[0].id + "']").prop('selected', true);
                        $("#fattura_elenco_clienti").trigger("change");

                        resolve(true);

                    } else
                    {

                        $("#fattura_elenco_clienti option[value='" + old[0].id + "']").prop('selected', true);
                        $("#fattura_elenco_clienti").trigger("change");

                        resolve(false);

                    }

                });

            } else
            {



                $("#fattura_elenco_clienti option[value='" + old[0].id + "']").prop('selected', true);
                $("#fattura_elenco_clienti").trigger("change");

                resolve(true);

            }




        } else
        {
            resolve(true);
        }
    });
}


async function controlla_esistenza_piva_nonpagato()
{
    return new Promise(resolve => {
        /* DATI NUOVI */
        var ragione_sociale = $('#popup_nonpagato [name="ragione_sociale"]').val();
        var indirizzo = $('#popup_nonpagato [name="indirizzo"]').val();
        var numero = $('#popup_nonpagato [name="numero"]').val();
        var cap = $('#popup_nonpagato [name="cap"]').val();
        var citta = $('#popup_nonpagato [name="citta"]').val();
        var provincia = $('#popup_nonpagato [name="provincia"]').val();
        var paese = $('#popup_nonpagato [name="paese"]').val();
        var cellulare = $('#popup_nonpagato [name="cellulare"]').val();
        var telefono = $('#popup_nonpagato [name="telefono"]').val();
        var fax = $('#popup_nonpagato [name="fax"]').val();
        var email = $('#popup_nonpagato [name="email"]').val();
        var codice_fiscale = $('#popup_nonpagato [name="codice_fiscale"]').val();
        var partita_iva = $('#popup_nonpagato [name="partita_iva"]').val();
        var partita_iva_estera = $('#popup_nonpagato [name="partita_iva_estera"]').val();
        var formato_trasmissione = $('#popup_nonpagato [name="formato_trasmissione"]').prop("checked");
        var split_payment = $('#popup_nonpagato [name="split_payment"]').prop("checked");
        var codice_destinatario = $('#popup_nonpagato [name="codice_destinatario"]').val();
        var regime_fiscale = $('#popup_nonpagato [name="regime_fiscale"]').val();
        var pec = $('#popup_nonpagato [name="pec"]').val();
        var comune = $('#popup_nonpagato [name="comune"]').val();
        var nazione = $('#fattura [name="nazione"] option:selected').val();
        var azienda_privato = $('[name="azienda_privato"]:checked').attr("id");

        /*DATI VECCHI*/
        var testo_query = "select * from clienti where partita_iva='" + partita_iva + "' limit 1;";
        var old = alasql(testo_query);

        if (old[0] !== undefined) {

            var dati_identici = true;

            /* CONTROLLI NON STRETTI PER EVITARE INCOMPATIBILITA TRA TIPI DI VARIABILE TIPO false e "false" */
            if (old[0].ragione_sociale.trim().toUpperCase() != ragione_sociale.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].indirizzo.trim().toUpperCase() != indirizzo.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].numero.trim().toUpperCase() != numero.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].cap.trim().toUpperCase() != cap.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].comune.trim().toUpperCase() != comune.trim().toUpperCase()) {
                dati_identici = false;
            }
            /*if (old[0].citta.trim().toUpperCase() != citta.trim().toUpperCase()) {
             dati_identici = false;
             }*/
            if (old[0].provincia.trim().toUpperCase() != provincia.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].cellulare.trim().toUpperCase() != cellulare.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].telefono.trim().toUpperCase() != telefono.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].email.trim().toUpperCase() != email.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].codice_fiscale.trim().toUpperCase() != codice_fiscale.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].partita_iva.trim().toUpperCase() != partita_iva.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].partita_iva_estera.trim().toUpperCase() != partita_iva_estera.trim().toUpperCase()) {
                dati_identici = false;
            }

            if (old[0].formato_trasmissione === "") {
                old[0].formato_trasmissione = "false";
            }
            if (old[0].formato_trasmissione != formato_trasmissione.toString()) {
                dati_identici = false;
            }
            if (old[0].codice_destinatario.trim().toUpperCase() != codice_destinatario.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].regime_fiscale.trim().toUpperCase() != regime_fiscale.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].pec.trim().toUpperCase() != pec.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].comune.trim().toUpperCase() != comune.trim().toUpperCase()) {
                dati_identici = false;
            }
            if (old[0].nazione.trim().toUpperCase() != nazione.trim().toUpperCase()) {
                dati_identici = false;
            }

            if (dati_identici === false) {

                bootbox.confirm("Vuoi sovrascrivere i dati precedenti di " + ragione_sociale.trim().toUpperCase() + "?", function (risposta) {

                    if (risposta === true) {

                        /*
                         *  var ragione_sociale = $('#fattura [name="ragione_sociale"]').val();
                         var indirizzo = $('#fattura [name="indirizzo"]').val();
                         var numero = $('#fattura [name="numero"]').val();
                         var cap = $('#fattura [name="cap"]').val();
                         var citta = $('#fattura [name="citta"]').val();
                         var provincia = $('#fattura [name="provincia"]').val();
                         var cellulare = $('#fattura [name="cellulare"]').val();
                         var telefono = $('#fattura [name="telefono"]').val();
                         var email = $('#fattura [name="email"]').val();
                         var codice_fiscale = $('#fattura [name="codice_fiscale"]').val();
                         var partita_iva = $('#fattura [name="partita_iva"]').val();
                         var partita_iva_estera = $('#fattura [name="partita_iva_estera"]').val();
                         var formato_trasmissione = $('#fattura [name="formato_trasmissione"]').prop("checked");
                         var codice_destinatario = $('#fattura [name="codice_destinatario"]').val();
                         var regime_fiscale = $('#fattura [name="regime_fiscale"]').val();
                         var pec = $('#fattura [name="pec"]').val();
                         var comune = $('#fattura [name="comune"]').val();
                         var nazione = $('#fattura [name="nazione"] option:selected').val();
                         */

                        var query_update = "update clienti set \n\
                                        tipo_cliente='" + azienda_privato + "',\n\
                                        ragione_sociale='" + ragione_sociale + "',\n\
                                        indirizzo='" + indirizzo + "',\n\
                                        numero='" + numero + "',\n\
                                        cap='" + cap + "',\n\
                                        /*citta='" + citta + "',*/\n\
                                        comune='" + comune + "',\n\
                                        provincia='" + provincia + "',\n\
                                        cellulare='" + cellulare + "',\n\
                                        telefono='" + telefono + "',\n\
                                        email='" + email + "',\n\
                                        codice_fiscale='" + codice_fiscale + "',\n\
                                        partita_iva='" + partita_iva + "',\n\
                                        partita_iva_estera='" + partita_iva_estera + "',\n\
                                        split_payment='" + split_payment + "',\n\
                                        formato_trasmissione='" + formato_trasmissione + "',\n\
                                        codice_destinatario='" + codice_destinatario + "',\n\
                                        regime_fiscale='" + regime_fiscale + "',\n\
                                        pec='" + pec + "',\n\
                                        comune='" + comune + "',\n\
                                        nazione='" + nazione + "' \n\
                                        where partita_iva='" + partita_iva + "';";
                        comanda.sock.send({tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: query_update, terminale: comanda.terminale, ip: comanda.ip_address});
                        alasql(query_update);
                        $("#popup_nonpagato_elenco_clienti option[value='" + old[0].id + "']").prop('selected', true);
                        $("#popup_nonpagato_elenco_clienti").trigger("change");
                        resolve(true);
                    } else
                    {
                        $("#popup_nonpagato_elenco_clienti option[value='" + old[0].id + "']").prop('selected', true);
                        $("#popup_nonpagato_elenco_clienti").trigger("change");
                        resolve(false);
                    }

                });

            } else
            {
                $("#popup_nonpagato_elenco_clienti option[value='" + old[0].id + "']").prop('selected', true);
                $("#popup_nonpagato_elenco_clienti").trigger("change");
                resolve(true);
            }




        } else
        {
            resolve(true);
        }
    });
}