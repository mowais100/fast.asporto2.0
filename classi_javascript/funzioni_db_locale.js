/* global Promise, comanda, testo_query, posizione_attuale, ive_misuratore_controller, parseFloat, REPARTO_controller */

var FunzioniDB = function() {

    //-----INIZIO FUNZIONI STATICHE-----//

    this.richiesta_progressivo = function(richiesta, callBack) {
        $.ajax({
            type: "POST",
            async: true,
            url: comanda.url_server + 'librerie_progressivi/lpnf.php',
            timeout: 5000,
            data: {
                richiesta: richiesta
            },
            beforeSend: function() {
                //CREA LA TABELLA DEL PROGRESSIVO FISCALE SE NON ESISTE

                /*
                 *
                 ,QUITTUNG:49,DATA_QUITTUNG:05 Jan 2016 11:01:33,RECHNUNG:3,DATA_RESHNUNG:0,
                 SCONTRINO:0,DATA_SCONTRINO:0,FATTURA:0,DATA_FATTURA:0,RICEVUTA:0,DATA_RICEVUTA:0,
                 BUONO:0,DATA_BUONO:0,ANNO_PROGRESSIVI:2016,                 *
                 */

            },
            success: function(data_centrale) {

                //PRENDE IL NUMERO DAL DB CENTRALE (GIA FATTO PRIMA)

                //CONFRONTA CON IL LOCALE
                var qp = "SELECT " + richiesta + " FROM progressivi_fiscali WHERE " + richiesta + "!='undefined' AND " + richiesta + "!='' AND " + richiesta + "!='NaN' ORDER BY CAST(" + richiesta + " AS INT) DESC LIMIT 1;";
                comanda.sincro.query(qp, function(data_locale) {

                    var numero_richiesta = 0;
                    data_locale.length === 0 ? numero_richiesta = 0 : numero_richiesta = data_locale[0][richiesta];
                    data_locale = parseInt(numero_richiesta) + 1;
                    console.log("CENTRALE", data_centrale, "LOCALE", data_locale);
                    //SE IL PROGRESSIVO CENTRALE E' MAGGIORE DEL LOCALE AGGIORNA IL DB LOCALE E USA IL PROG CENTRALE
                    if (data_locale <= data_centrale) {

                        //AGGIUNGE IL N AL DB LOCALE
                        var qp = "INSERT INTO progressivi_fiscali (" + richiesta + ") VALUES ('" + data_centrale + "');";
                        console.log("PROGRESSIVO 1 " + qp);
                        comanda.sock.send({ tipo: "progressivo_fiscale", operatore: comanda.operatore, query: qp, terminale: comanda.terminale, ip: comanda.ip_address });
                        comanda.sincro.query(qp, function() {

                            //CALLBACK
                            callBack(data_centrale);
                        });
                    }
                    //SE IL PROGRESSIVO CENTRALE E' MINORE DEL LOCALE AGGIORNA IL CENTRALE E USA IL LOCALE IN CB
                    else if (data_locale > data_centrale) {

                        $.post(comanda.url_server + 'librerie_progressivi/lpnf_allineamento.php', { richiesta: richiesta, numero_corretto: data_locale });
                        var qp = "INSERT INTO progressivi_fiscali (" + richiesta + ") VALUES ('" + data_locale + "');";
                        console.log("PROGRESSIVO 2 " + qp);
                        comanda.sock.send({ tipo: "progressivo_fiscale", operatore: comanda.operatore, query: qp, terminale: comanda.terminale, ip: comanda.ip_address });
                        callBack(data_locale);
                    }

                });
            },
            error: function() {

                //RICHIEDE IL NUMERO AL DB LOCALE
                var qp = "SELECT " + richiesta + " FROM progressivi_fiscali WHERE " + richiesta + "!='undefined' AND " + richiesta + "!='NaN' ORDER BY CAST(" + richiesta + " AS INT) DESC LIMIT 1;";
                comanda.sincro.query(qp, function(data) {
                    data = parseInt(data[0][richiesta]) + 1;
                    var conferma = confirm("ATTENZIONE: Sei sicuro di voler stampare il " + richiesta + " numero " + data + "? \n\nCLICCANDO OK, PUOI CONTINUARE A FARE QUITTUNG E RESHNUNG SOLO DA QUESTO TABLET.\nQUANDO LA CASSA SARA' RIPARATA, FAI L'ULTIMO QUITTUNG E RESHNUNG DA QUI, OPPURE CONTATTACI.\n\n\nSTAI LAVORANDO SENZA LA STAMPANTE CHE FA DA SERVER.\n\n\DEVI RIPRISTINARE SUBITO LA RETE, ALTRIMENTI POTRESTI AVERE ERRORI.!");
                    if (conferma === true) {

                        //AGGIUNGE IL PROGRESSIVO AL DB LOCALE
                        qp = "INSERT INTO progressivi_fiscali (" + richiesta + ") VALUES ('" + data + "');";
                        console.log("PROGRESSIVO 3 " + qp);
                        comanda.sock.send({ tipo: "progressivo_fiscale", operatore: comanda.operatore, query: qp, terminale: comanda.terminale, ip: comanda.ip_address });
                        comanda.sincro.query(qp, function() {
                            //INVIA SOCKET

                            //CALLBACK
                            callBack(data);
                        });
                    }
                });
            }
        });
    };

    this.richiesta_progressivo_ita = function(richiesta, callBack) {
        $.ajax({
            type: "POST",
            async: true,
            url: comanda.url_server + 'librerie_progressivi_ita/lpnf.php',
            timeout: 5000,
            data: {
                richiesta: richiesta
            },
            beforeSend: function() {
                //CREA LA TABELLA DEL PROGRESSIVO FISCALE SE NON ESISTE

                /*
                 *
                 ,QUITTUNG:49,DATA_QUITTUNG:05 Jan 2016 11:01:33,RECHNUNG:3,DATA_RESHNUNG:0,
                 SCONTRINO:0,DATA_SCONTRINO:0,FATTURA:0,DATA_FATTURA:0,RICEVUTA:0,DATA_RICEVUTA:0,
                 BUONO:0,DATA_BUONO:0,ANNO_PROGRESSIVI:2016,                 *
                 */

            },
            success: function(data_centrale) {

                //PRENDE IL NUMERO DAL DB CENTRALE (GIA FATTO PRIMA)

                //CONFRONTA CON IL LOCALE
                var qp = "SELECT " + richiesta + " FROM progressivi_fiscali WHERE " + richiesta + "!='undefined' AND " + richiesta + "!='NaN' ORDER BY CAST(" + richiesta + " AS INT) DESC LIMIT 1;";
                comanda.sincro.query(qp, function(data_locale) {

                    var numero_richiesta = 0;
                    data_locale.length === 0 ? numero_richiesta = 0 : numero_richiesta = data_locale[0][richiesta];
                    data_locale = parseInt(numero_richiesta) + 1;
                    console.log("CENTRALE", data_centrale, "LOCALE", data_locale);
                    //SE IL PROGRESSIVO CENTRALE E' MAGGIORE DEL LOCALE AGGIORNA IL DB LOCALE E USA IL PROG CENTRALE
                    if (data_locale <= data_centrale) {

                        //AGGIUNGE IL NÂ° AL DB LOCALE
                        var qp = "INSERT INTO progressivi_fiscali (" + richiesta + ") VALUES ('" + data_centrale + "');";
                        console.log("PROGRESSIVO 1 " + qp);
                        comanda.sock.send({ tipo: "progressivo_fiscale", operatore: comanda.operatore, query: qp, terminale: comanda.terminale, ip: comanda.ip_address });
                        comanda.sincro.query(qp, function() {

                            //CALLBACK
                            callBack(data_centrale);
                        });
                    }
                    //SE IL PROGRESSIVO CENTRALE E' MINORE DEL LOCALE AGGIORNA IL CENTRALE E USA IL LOCALE IN CB
                    else if (data_locale > data_centrale) {

                        $.post(comanda.url_server + 'librerie_progressivi/lpnf_allineamento.php', { richiesta: richiesta, numero_corretto: data_locale });
                        var qp = "INSERT INTO progressivi_fiscali (" + richiesta + ") VALUES ('" + data_locale + "');";
                        console.log("PROGRESSIVO 2 " + qp);
                        comanda.sock.send({ tipo: "progressivo_fiscale", operatore: comanda.operatore, query: qp, terminale: comanda.terminale, ip: comanda.ip_address });
                        callBack(data_locale);
                    } else {
                        bootbox.alert("Errore nel database o nel file dei progressivi fiscali.");
                    }

                });
            },
            error: function() {

                //RICHIEDE IL NUMERO AL DB LOCALE
                var qp = "SELECT " + richiesta + " FROM progressivi_fiscali WHERE " + richiesta + "!='undefined' AND " + richiesta + "!='NaN' ORDER BY CAST(" + richiesta + " AS INT) DESC LIMIT 1;";
                comanda.sincro.query(qp, function(data) {
                    data = parseInt(data[0][richiesta]) + 1;
                    var conferma = confirm("ATTENZIONE: Sei sicuro di voler stampare il " + richiesta + " numero " + data + "? \n\nCLICCANDO OK, PUOI CONTINUARE A FARE QUITTUNG E RESHNUNG SOLO DA QUESTO TABLET.\nQUANDO LA CASSA SARA' RIPARATA, FAI L'ULTIMO QUITTUNG E RESHNUNG DA QUI, OPPURE CONTATTACI.\n\n\nSTAI LAVORANDO SENZA LA STAMPANTE CHE FA DA SERVER.\n\n\DEVI RIPRISTINARE SUBITO LA RETE, ALTRIMENTI POTRESTI AVERE ERRORI.!");
                    if (conferma === true) {

                        //AGGIUNGE IL PROGRESSIVO AL DB LOCALE
                        qp = "INSERT INTO progressivi_fiscali (" + richiesta + ") VALUES ('" + data + "');";
                        console.log("PROGRESSIVO 3 " + qp);
                        comanda.sock.send({ tipo: "progressivo_fiscale", operatore: comanda.operatore, query: qp, terminale: comanda.terminale, ip: comanda.ip_address });
                        comanda.sincro.query(qp, function() {
                            //INVIA SOCKET

                            //CALLBACK
                            callBack(data);
                        });
                    }
                });
            }
        });
    };

    this.modifica_progressivo_centrale = function(data_locale) {

    };

    this.valida_licenza = function(callBack) {

        var cb;
        callBack(cb);
        return true;
    };

    this.elenco_operatori = function(callBack) {


        var testo_query = 'SELECT nome FROM operatori';
        comanda.sincro.query(testo_query, function(cb) {

            var output = '<div class = "col-md-12">';

            cb.forEach(function(row) {
                output += "<div class=\"tastone_iniziale\"><p>" + row.nome.toUpperCase() + "</p></div>";
            });



            if (comanda.compatibile_xml !== true && comanda.mobile === true) {
                if (comanda.lingua_stampa === "italiano") {
                    output += "<div class=\"tastone_iniziale\"><p " + comanda.evento + "=\"chiusura_fiscale(true);\">CHIUSURA FISCALE</p></div>";
                } else {
                    output += "<div class=\"tastone_iniziale\"><p " + comanda.evento + "=\"z_bericht();\">Z-BERICHT</p></div>";
                }
            }

            if (comanda.compatibile_xml === true && comanda.ingresso !== true) {
                output += "<div class=\"tastone_iniziale\"><p  " + comanda.evento + "=\"nascondi_righe();comanda.dispositivo='SETTAGGI';elenco_prodotti();\">SETTAGGI<br/>IBRIDO</p></div>";
            }

            if (comanda.compatibile_xml !== true) {
                output += "<div class=\"tastone_iniziale\"><p " + comanda.evento + "=\"chiudi_programma();\">CHIUDI PROGRAMMA</p></div>";
                if (comanda.mobile === true) {
                    output += "<div class=\"tastone_iniziale\"><p " + comanda.evento + "=\"scarica_apk();\">AGGIORNA<br>SOFTWARE</p></div>";
                }
            }



            output += '</div>';

            $('#selezione_operatore > div').html(output);

            callBack(true);
        });
    };

    var fine_conto_attivo = function(callBACCO, salta_colore) {


        $('#intestazioni_conto_separato_grande').show();
        //console.log(indice, array_numero_righe, indice === array_numero_righe);
        $('#intestazioni_conto_separato_2_grande').show();
        //console.log("TOTALE REINSERITO");

        calcola_totale();

        // if (comanda.mobile === true || comanda.pizzeria_asporto === true) {

        if (comanda.fastorder !== true) {

            ultime_battiture(salta_colore);
        }


        if (comanda.mobile === true) {
            if ($('#prodotti_conto_separato_grande tr').length === 0) {
                $('.totale_conto_separato_grande_barra').hide();
            }
            if ($('#prodotti_conto_separato_2_grande tr').length === 0) {
                $('.totale_conto_separato_2_grande_barra').hide();
            }
            if ($('#conto tr').length === 0) {
                $('#tasti_incasso_nascosti_palmare').hide();
                $('#tab_dx').removeClass('tasti_incasso_visibili');
                $('#tab_dx').css('height', '124vh');
            }

        }

        //Sblocca i tasti di incasso e conto
        $('.tasto_konto').css('pointer-events', '');
        $('.tasto_quittung').css('pointer-events', '');
        $('.tasto_rechnung').css('pointer-events', '');
        $('.tasto_bargeld').css('pointer-events', '');

        sblocco_tasti = true;

        if (typeof(callBACCO) === "function") {
            console.log("CALLBACK CONTO ATTIVO");
            callBACCO(true);
        }

    };

    var progressivo_ultimo_conto = 0;

    this.conto_attivo = function(callBACCO, salta_colore) {

        comanda.consegna_presente = false;

        comanda.quantita_pizze_totali = 0;

        comanda.totale_consegna = "0.00";

        comanda.stampata_s = false;

        $('.split_successivo').attr('disabled', false);
        comanda.disabilita_tasto_bis = false;

        progressivo_ultimo_conto++;
        var progressivo_ultimo_conto_interno = progressivo_ultimo_conto;

        var inizio_conto = new Date().getTime();
        console.log("INIZIO CONTO", inizio_conto);

        //CANCELLA RIGHE DAL CONTO ATTIVO
        //CREATE TABLE COMANDA
        //$('.ultime_battiture tr,#intestazioni_conto_separato_grande tr,#intestazioni_conto_separato_2_grande tr,#conto tr,#intestazioni_conto tr,.tab_conto_grande tr,#intestazioni_conto_grande tr,.tab_conto_separato_grande tr,.tab_conto_separato_2_grande tr').remove();

        var ultima_due_gusti = false,
            ultima_maxi = false,
            numero_nodo_variante_piu = 0,
            consegna_presente = false,
            calcolo_consegna_una_pizza = 0,
            calcolo_consegna_piu_di_una_pizza = 0,
            calcolo_consegna_su_totale = 0,
            calcolo_consegna_mezzo_metro = 0,
            calcolo_consegna_un_metro = 0,
            calcolo_consegna_maxi = 0,
            calcolo_consegna_a_pizza = 0,
            riga_calcolo_consegna = '',
            ultime_battiture = '',
            intestazioni_conto_separato_grande = '',
            intestazioni_conto_separato_2_grande = '',
            intestazioni_conto_separato_2_grande_split = '',
            conto = '',
            intestazioni_conto = '',
            tab_conto_grande = '',
            intestazioni_conto_grande = '',
            intestazioni_conto_split = '',
            tab_conto_separato_grande = '',
            tab_conto_separato_2_grande = '',
            tab_conto_separato_2_grande_split = '';

        /*comanda.consegna_a_pizza = risultato[0].consegna_a_pizza;
         comanda.consegna_mezzo_metro = risultato[0].consegna_mezzo_metro;
         comanda.consegna_metro = risultato[0].consegna_metro;
         comanda.consegna_su_totale = risultato[0].consegna_su_totale;*/

        if (comanda.pizzeria_asporto === true && (comanda.una_pizza.length > 0 || comanda.piu_di_una_pizza.length > 0 || comanda.consegna_a_pizza.length > 0 || comanda.consegna_mezzo_metro.length > 0 || comanda.consegna_metro.length > 0 || comanda.consegna_maxi.length > 0 || comanda.consegna_su_totale.length > 0)) {
            comanda.consegna_presente = true;
        }


        //Crea tabella comanda

        if (comanda.tavolo !== 0) {







            //LA QUERY DEV'ESSERE MODIFICATA IN MODO DI RAGGRUPPARE I null, 'null', 1 in uno e 2 in un altro
            //var query_numero_conto = "select distinct numero_conto FROM comanda WHERE stato_record='ATTIVO' AND ntav_comanda='" + comanda.tavolo + "' ORDER BY numero_conto ASC;";

            comanda.totale_conto = new Object();

            //BLOCCO
            var p2 = new Promise(function(resolve, reject) {

                var esclusione_fiscale_sospeso = '';
                //if (typeof (comanda.tavolo) === 'string' && (comanda.tavolo === "BAR" || comanda.tavolo.indexOf('CONTINUO') !== -1)) {
                esclusione_fiscale_sospeso = ' and fiscale_sospeso!="STAMPAFISCALESOSPESO" ';
                //}

                var groupby = "group by cod_promo,fiscalizzata_sn,portata,tipo_impasto,desc_art,prog_inser,nome_comanda,prezzo_un";
                if (comanda.tasto_contosep === "S" || comanda.multiquantita === "N") {
                    groupby = "group by cod_promo,fiscalizzata_sn,portata,tipo_impasto,desc_art,nome_comanda,prezzo_un";
                }
                //24/05/2018
                //MODIFICATO 'M' ||substr(nodo,1,3)  in 'M' || nodo  //4 RIGHE - ANCHE SUL SECONDO CONTO - ESPERIMENTO
                //MODIFICATO PER VEDERE LE VARIANTI IN ORDINE DI BATTITURA E PERFORMANCE
                //WHERE substr(desc_art,1,1)

                var testo_query = "";


                testo_query = " select * from (\n\
                                        select  substr(desc_art,1,3)||'M'||substr(nodo,1,3) as ord,cod_promo,fiscalizzata_sn,ora_,rag_soc_cliente,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,prezzo_varianti_aggiuntive,prezzo_varianti_maxi,prezzo_maxi_prima,quantita from comanda as c1 where desc_art NOT LIKE 'RECORD TESTA%' and  BIS='" + lettera_conto_split() + "' and contiene_variante='1' and ntav_comanda='" + comanda.tavolo + "'  and posizione='CONTO'   and (trim(QTAP)='0' or trim(QTAP) is null or trim(QTAP)='') and stato_record='ATTIVO' and   (numero_conto='' OR numero_conto='undefined' OR numero_conto is null OR numero_conto='null' OR numero_conto='1' OR numero_conto=1)  " + esclusione_fiscale_sospeso + " \n\
                                        union all\n\
                                        select  substr(desc_art,1,3)||'M'||substr(nodo,1,3) as ord,cod_promo,fiscalizzata_sn,ora_,rag_soc_cliente,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,prezzo_varianti_aggiuntive,prezzo_varianti_maxi,prezzo_maxi_prima,sum(quantita) as quantita from comanda as c1 where desc_art NOT LIKE 'RECORD TESTA%' and  BIS='" + lettera_conto_split() + "' and  (contiene_variante !='1' or contiene_variante='null' or contiene_variante is null) and length(nodo)=3 and ntav_comanda='" + comanda.tavolo + "' and posizione='CONTO'  and (trim(QTAP)='0' or trim(QTAP) is null or trim(QTAP)='')  and stato_record='ATTIVO'  and  (numero_conto='' OR numero_conto='undefined' OR numero_conto is null OR numero_conto='null' OR numero_conto='1' OR numero_conto=1)  " + esclusione_fiscale_sospeso + "   " + groupby + "\n\
                                        union all		\n\
                                        select  (select substr(desc_art,1,3) from comanda where desc_art NOT LIKE 'RECORD TESTA%' and  BIS='" + lettera_conto_split() + "' and  nodo=substr(c1.nodo,1,3)  and (trim(QTAP)='0' or trim(QTAP) is null or trim(QTAP)='') and stato_record='ATTIVO'  and ntav_comanda='" + comanda.tavolo + "')||'M'||nodo as ord,cod_promo,fiscalizzata_sn,ora_,rag_soc_cliente,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,prezzo_varianti_aggiuntive,prezzo_varianti_maxi,prezzo_maxi_prima,quantita from comanda as c1 where desc_art NOT LIKE 'RECORD TESTA%' and  BIS='" + lettera_conto_split() + "' and  length(nodo)=7 and substr(desc_art,1,1)='-' and ntav_comanda='" + comanda.tavolo + "' and posizione='CONTO'  and (trim(QTAP)='0' or trim(QTAP) is null or trim(QTAP)='')  and stato_record='ATTIVO'  and  (numero_conto='' OR numero_conto='undefined' OR numero_conto is null OR numero_conto='null' OR numero_conto='1' OR numero_conto=1)  " + esclusione_fiscale_sospeso + " \n\
                                        union all\n\
                                        select (select substr(desc_art,1,3) from comanda where desc_art NOT LIKE 'RECORD TESTA%' and  BIS='" + lettera_conto_split() + "' and  nodo=substr(c1.nodo,1,3)  and (trim(QTAP)='0' or trim(QTAP) is null or trim(QTAP)='') and stato_record='ATTIVO'  and ntav_comanda='" + comanda.tavolo + "')||'M'||nodo as ord,cod_promo,fiscalizzata_sn,ora_,rag_soc_cliente,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,prezzo_varianti_aggiuntive,prezzo_varianti_maxi,prezzo_maxi_prima,quantita from comanda as c1 where desc_art NOT LIKE 'RECORD TESTA%' and  BIS='" + lettera_conto_split() + "' and  length(nodo)=7 and substr(desc_art,1,1)!='-' and ntav_comanda='" + comanda.tavolo + "' and posizione='CONTO'   and (trim(QTAP)='0' or trim(QTAP) is null or trim(QTAP)='') and stato_record='ATTIVO'  and  (numero_conto='' OR numero_conto='undefined' OR numero_conto is null OR numero_conto='null' OR numero_conto='1' OR numero_conto=1)  " + esclusione_fiscale_sospeso + "  \n\
                                        order by nome_comanda DESC, ord ASC);";


                console.log("QUERY CONTO ATTIVO", testo_query);



                comanda.sincro.query(testo_query, function(d1) {


                    var elemento_comanda = new Object();
                    //SERVE A DARE UN ORDINE MIGLIORE ALLE VARIANTI
                    //E' NECESSARISSIMO
                    var ord = 0;
                    d1.forEach(function(attivo) {

                        attivo.desc_art = attivo.desc_art.substr(0, 21);

                        console.log("prodotto", attivo);
                        if (comanda.totale_conto[comanda.conto] === undefined) {
                            comanda.totale_conto[comanda.conto] = new Object();
                        }

                        if (comanda.totale_conto[comanda.conto][attivo.perc_iva] === undefined) {
                            comanda.totale_conto[comanda.conto][attivo.perc_iva] = new Object();
                            comanda.totale_conto[comanda.conto][attivo.perc_iva].iva = 0;
                            comanda.totale_conto[comanda.conto][attivo.perc_iva].netto = 0;
                            comanda.totale_conto[comanda.conto][attivo.perc_iva].mwst = 0;
                            comanda.totale_conto[comanda.conto][attivo.perc_iva].gesamt = 0;
                        }

                        comanda.totale_conto[comanda.conto][attivo.perc_iva].iva = parseInt(attivo.perc_iva);
                        comanda.totale_conto[comanda.conto][attivo.perc_iva].netto += parseFloat(((attivo.prezzo_un / 100 * (100 - parseInt(attivo.perc_iva)))) * parseInt(attivo.quantita));
                        comanda.totale_conto[comanda.conto][attivo.perc_iva].gesamt += parseFloat((attivo.prezzo_un * attivo.quantita));
                        comanda.totale_conto[comanda.conto][attivo.perc_iva].mwst = parseFloat((comanda.totale_conto[comanda.conto][attivo.perc_iva].gesamt / 100 * attivo.perc_iva));
                        console.log(attivo);

                        if (elemento_comanda[attivo.portata] === undefined) {
                            elemento_comanda[attivo.portata] = new Object();
                        }



                        //ORDINANDO PER ORD RESTANO IN ORDINE ALFABETICO GLI ARTICOLI PER OGNI PORTATA
                        if (elemento_comanda[attivo.portata][attivo.ord + "_" + ord] === undefined) {
                            //Diventa come (che equivale a dire uguale) attivo
                            elemento_comanda[attivo.portata][attivo.ord + "_" + ord] = new Object();
                            elemento_comanda[attivo.portata][attivo.ord + "_" + ord] = attivo;
                        }
                        ord++;
                    });
                    console.log("ELEMENTO COMANDA", elemento_comanda);
                    for (var portata in elemento_comanda) {
                        console.log("conto_attivo - analisi - portata", portata);
                        //CONDIZIONI PER MOSTRARE GLI ARTICOLI, VARIANTI, STAMPATI, SERVITI, ECCETERA...

                        if (comanda.nome_portata[portata] === undefined) {
                            comanda.nome_portata[portata] = "";
                        }

                        conto += '<tr class="comanda non-contare-riga portata text-center" style="border:1px solid black;text-shadow: -1px 0 black, 0 1px black, 2px 0 black, 0 -1px black;cursor:pointer;background-color:grey;font-weight:bold;" ' + comanda.evento + '="$(\'.nascondibile.' + comanda.nome_portata[portata] + '\').toggle();"><td colspan="5">' + comanda.nome_portata[portata] + '</td></tr>';

                        if (comanda.mobile !== true) {
                            tab_conto_grande += '<tr class="comanda non-contare-riga portata text-center" style="border:1px solid black;text-shadow: -1px 0 black, 0 1px black, 2px 0 black, 0 -1px black;cursor:pointer;background-color:grey;font-weight:bold;" ' + comanda.evento + '="$(\'.nascondibile.' + comanda.nome_portata[portata] + '\').toggle();"><td colspan="6">' + comanda.nome_portata[portata] + '</td></tr>';
                        }

                        for (var indice_articolo in elemento_comanda[portata]) {



                            //ATTIVO RIPRENDE I SUOI ATTRIBUTI
                            var attivo = elemento_comanda[portata][indice_articolo];

                            //IVA DI DEFAULT 10 REPARTO 2
                            //10% ï¿½ il reparto 2 (FPWizard)
                            var reparto = "rep_1";
                            var iva_esposta = ive_misuratore_controller.iva_reparto(1);

                            if (typeof(attivo.perc_iva) === "string") {
                                switch (attivo.perc_iva.trim()) {
                                    case ive_misuratore_controller.iva_reparto(2):
                                        //22% ï¿½ il reparto 1 (FPWizard)
                                        reparto = "rep_2";
                                        break;
                                    case ive_misuratore_controller.iva_reparto(3):
                                        //4% ï¿½ il reparto 3 (FPWizard)
                                        reparto = "rep_3";
                                        break;
                                    default:
                                }

                                iva_esposta = attivo.perc_iva;
                                if (comanda.iva_esposta !== 'true') {
                                    iva_esposta = "";
                                }
                            }

                            var fiscalizzata_sn = '';
                            if (attivo.fiscalizzata_sn !== undefined && attivo.fiscalizzata_sn !== null && attivo.fiscalizzata_sn === 'S') {
                                fiscalizzata_sn = 'fiscalizzata';
                            }

                            var cod_promo = '';
                            if (attivo.cod_promo !== undefined && attivo.cod_promo !== null && attivo.cod_promo !== '') {
                                cod_promo = 'cod_promo_' + attivo.cod_promo;
                            }

                            var prezzo_un = attivo.prezzo_un;
                            if (cod_promo === 'cod_promo_V_1') {
                                prezzo_un = '0.00';
                            }


                            if (comanda.consegna_presente === true) {
                                if (comanda.categorie_con_consegna_abilitata.indexOf(attivo.categoria) !== -1 && attivo.desc_art[0] !== "+" && attivo.desc_art[0] !== "-" && attivo.desc_art[0] !== "(" && attivo.desc_art[0] !== "*" && attivo.desc_art[0] !== "x" && attivo.desc_art[0] !== "=") {
                                    if (comanda.consegna_su_totale.length > 0) {
                                        calcolo_consegna_su_totale += parseFloat(prezzo_un) * parseInt(attivo.quantita);
                                        comanda.quantita_pizze_totali++;
                                    }

                                    if (attivo.desc_art.indexOf("MEZZO METRO") !== -1) {
                                        calcolo_consegna_mezzo_metro += parseInt(attivo.quantita);
                                    } else if (attivo.desc_art.indexOf("UN METRO") !== -1) {
                                        calcolo_consegna_un_metro += parseInt(attivo.quantita);
                                    } else if (attivo.desc_art.indexOf("MAXI") !== -1) {
                                        calcolo_consegna_maxi += parseInt(attivo.quantita);
                                    } else if (comanda.consegna_a_pizza.length > 0 && attivo.desc_art.indexOf("+") === -1 && attivo.desc_art.indexOf("-") === -1 && attivo.desc_art.indexOf("=") === -1) {
                                        calcolo_consegna_a_pizza += parseInt(attivo.quantita);
                                    }

                                    if ((comanda.una_pizza.length > 0 || comanda.piu_di_una_pizza.length > 0) && attivo.desc_art.indexOf("+") === -1 && attivo.desc_art.indexOf("-") === -1 && attivo.desc_art.indexOf("=") === -1) {
                                        calcolo_consegna_una_pizza += parseInt(attivo.quantita);
                                        calcolo_consegna_piu_di_una_pizza += parseInt(attivo.quantita);
                                    }
                                }
                            }

                            console.log("conto_attivo - analisi - art", attivo.desc_art, attivo.quantita);
                            console.log("funzionidb conto_attivo", attivo);

                            if (attivo.stampata_sn === "S") {
                                comanda.stampata_s = true;
                            } else {
                                comanda.disabilita_tasto_bis = true;
                            }
                            if (comanda.pizzeria_asporto !== true && attivo.stampata_sn === "S") {



                                //E' UNA REGOLA CHE NON VALE PER MATTIA131
                                if (comanda.mobile === true && comanda.licenza !== "mattia131") {

                                    if (attivo.desc_art[0] === "+" || attivo.desc_art[0] === "-" || attivo.desc_art[0] === "(" || attivo.desc_art[0] === "*" || attivo.desc_art[0] === "x" || attivo.desc_art[0] === "=") {

                                        conto += '<tr style="height:0;" class="comanda non-contare-riga"></tr>';
                                        ultime_battiture += '<tr style="height:0;" class="comanda non-contare-riga"></tr>';
                                        tab_conto_grande += '<tr style="height:0;" class="comanda non-contare-riga"></tr>';




                                        tab_conto_separato_grande += '<tr style="height:0;" class="comanda non-contare-riga"></tr>';
                                        tab_conto_separato_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda ' + comanda.tavolo + ' ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td style="color:transparent;" ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td><td></td></tr>';



                                        conto += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda nascondibile ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td    ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="color:transparent;background-color:black;" id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro;' + prezzo_un + '</td><td></td></tr>';
                                        tab_conto_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda nascondibile ' + comanda.tavolo + ' ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td   ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="color:transparent;background-color:black;" id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro; ' + prezzo_un + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro; ' + parseFloat(parseFloat(prezzo_un) * parseInt(attivo.quantita)).toFixed(2) + '</td><td></td></tr>';

                                    } else {
                                        conto += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda nascondibile ' + comanda.tavolo + ' ' + comanda.nome_portata[portata] + '" ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '" ><td>' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td>&euro;' + prezzo_un + '</td><td></td></tr>';
                                        tab_conto_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda nascondibile ' + comanda.tavolo + ' ' + comanda.nome_portata[portata] + '"  ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '"><td>' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td>&euro; ' + prezzo_un + '</td><td>&euro; ' + parseFloat(parseFloat(prezzo_un) * parseInt(attivo.quantita)).toFixed(2) + '</td><td></td></tr>';


                                        tab_conto_separato_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '"  style="background-color:black;" id="art_' + attivo.prog_inser + '" ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')"><td>' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td>&euro;' + prezzo_un + '</td><td></td></tr>';

                                    }
                                } else {
                                    if (comanda.cancellazione_articolo === 'S' || (comanda.tavolo === "BANCO" || comanda.tavolo === "BAR" || comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === "TAKE AWAY")) {
                                        var x = 'X';
                                        var eliminart = ' ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\',false,true);" ';
                                    } else {
                                        var x = '';
                                        var eliminart = '';
                                    }

                                    //PER IL PC CHE HA GIA' STAMPATO LA COMANDA

                                    console.log("CA ARTICOLO PRINC", attivo.desc_art, attivo.desc_art[0]);
                                    //SE L'ARTICOLO E' UNA VARIANTE
                                    if (attivo.desc_art[0] === "+" || attivo.desc_art[0] === "-" || attivo.desc_art[0] === "(" || attivo.desc_art[0] === "*" || attivo.desc_art[0] === "x" || attivo.desc_art[0] === "=") {

                                        conto += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                        ultime_battiture += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                        tab_conto_grande += '<tr style="height:0;" class="non-contare-riga"></tr>';


                                        tab_conto_separato_grande += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                        tab_conto_separato_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td style="color:transparent;" ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td><td ' + eliminart + ' style="color:red;cursor:pointer;">' + x + '</td></tr>';

                                        console.log("CA ARTICOLO VARIANTE", attivo.desc_art[0]);

                                        conto += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda nascondibile ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="color:transparent;background-color:black;" id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro;' + prezzo_un + '</td><td ' + eliminart + ' style="color:red;cursor:pointer;">' + x + '</td></tr>';
                                        tab_conto_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda nascondibile ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="color:transparent;background-color:black;" id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro; ' + prezzo_un + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro;' + parseFloat(parseFloat(prezzo_un) * parseInt(attivo.quantita)).toFixed(2) + '</td><td ' + eliminart + ' style="color:red;cursor:pointer;">' + x + '</td></tr>';

                                    }
                                    //ALTRIMENTI LI STAMPA IN MODO NORMALE
                                    else {
                                        console.log("CA ARTICOLO NON VARIANTE", attivo.desc_art);

                                        conto += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda nascondibile ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td style="background-color:black;" ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')"  id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro;' + prezzo_un + '</td><td ' + eliminart + ' style="color:red;background-color:black;cursor:pointer;">' + x + '</td></tr>';
                                        tab_conto_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda nascondibile ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td  ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro; ' + prezzo_un + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro; ' + parseFloat(parseFloat(prezzo_un) * parseInt(attivo.quantita)).toFixed(2) + '</td><td ' + eliminart + ' style="color:red;cursor:pointer;">' + x + '</td></tr>';



                                        tab_conto_separato_grande += '<tr style="background-color:black;" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '"><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td><td ' + eliminart + ' style="color:red;cursor:pointer;">' + x + '</td></tr>';

                                    }
                                }

                            }

                            //ALTRIMENTI
                            else {


                                console.log("CA ARTICOLO PRINC", attivo.desc_art, attivo.desc_art[0]);
                                //SE L'ARTICOLO E' UNA VARIANTE
                                if (attivo.desc_art[0] === "+" || attivo.desc_art[0] === "-" || attivo.desc_art[0] === "(" || attivo.desc_art[0] === "*" || attivo.desc_art[0] === "x" || attivo.desc_art[0] === "=") {

                                    if (attivo.desc_art[0] === "+") {
                                        numero_nodo_variante_piu++;
                                    }





                                    if (ultima_maxi === true && attivo.desc_art[0] !== "-") {

                                        if (numero_nodo_variante_piu > 1 && attivo.prezzo_varianti_maxi !== undefined && attivo.prezzo_varianti_maxi !== null && attivo.prezzo_varianti_maxi !== "undefined" && attivo.prezzo_varianti_maxi !== "null" && attivo.prezzo_varianti_maxi !== "" && !isNaN(parseFloat(attivo.prezzo_varianti_maxi))) {
                                            prezzo_un = attivo.prezzo_varianti_maxi;
                                        } else if (attivo.prezzo_maxi_prima !== undefined && attivo.prezzo_maxi_prima !== null && attivo.prezzo_maxi_prima !== "undefined" && attivo.prezzo_maxi_prima !== "null" && attivo.prezzo_maxi_prima !== "" && !isNaN(parseFloat(attivo.prezzo_maxi_prima))) {
                                            prezzo_un = attivo.prezzo_maxi_prima;

                                        }
                                    } else {
                                        if (numero_nodo_variante_piu > 1 && attivo.prezzo_varianti_aggiuntive !== undefined && attivo.prezzo_varianti_aggiuntive !== null && attivo.prezzo_varianti_aggiuntive !== "undefined" && attivo.prezzo_varianti_aggiuntive !== "null" && attivo.prezzo_varianti_aggiuntive !== "" && !isNaN(parseFloat(attivo.prezzo_varianti_aggiuntive))) {
                                            prezzo_un = attivo.prezzo_varianti_aggiuntive;
                                        }
                                    }

                                    if (ultima_due_gusti === true && attivo.desc_art[0] === "=") {
                                        prezzo_un = "0.00";
                                    }



                                    conto += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                    ultime_battiture += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                    tab_conto_grande += '<tr style="height:0;" class="non-contare-riga"></tr>';


                                    tab_conto_separato_grande += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                    tab_conto_separato_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td style="color:transparent;" ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="color:red;cursor:pointer;">X</td></tr>';

                                    console.log("CA ARTICOLO VARIANTE", attivo.desc_art[0]);

                                    conto += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  style="color:transparent;" ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="color:red;cursor:pointer;">X</td></tr>';
                                    ultime_battiture += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  style="color:transparent;" ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="color:red;cursor:pointer;">X</td></tr>';
                                    tab_conto_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  style="color:transparent;" ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro; ' + prezzo_un + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro; ' + parseFloat(parseFloat(prezzo_un) * parseInt(attivo.quantita)).toFixed(2) + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="color:red;cursor:pointer;">X</td></tr>';

                                    //MODIFICA 07/06/2018
                                    /*if (comanda.compatibile_xml !== true) {
                                     //BASSA PRIORITA PER NON INTACCARE PRESTAZIONI
                                     var query_prezzo_vero = "update comanda set prezzo_vero='" + prezzo_un + "' where  desc_art NOT LIKE '= 1/2 %' AND stato_record='ATTIVO' AND prog_inser='" + attivo.prog_inser + "';";
                                     
                                     comanda.sock.send({tipo: "set_prezzo_vero", operatore: comanda.operatore, query: query_prezzo_vero, terminale: comanda.terminale, ip: comanda.ip_address});
                                     
                                     comanda.sincro.query(query_prezzo_vero, function () {});
                                     }*/

                                }
                                //ALTRIMENTI LI STAMPA IN MODO NORMALE
                                else {

                                    if (attivo.desc_art.indexOf("MAXI") !== -1) {
                                        ultima_maxi = true;

                                    } else {
                                        ultima_maxi = false;
                                    }

                                    if (attivo.desc_art.indexOf("DUE GUSTI") !== -1) {
                                        ultima_due_gusti = true;
                                        var nodo_art_princ = attivo.nodo.substr(0, 3);
                                        var prezzo_due_gusti = "0.00";
                                        var contatore_duegusti = 0;
                                        for (var articolo in elemento_comanda[portata]) {
                                            var art = elemento_comanda[portata][articolo];
                                            var nodo_var = art.nodo.substr(0, 3);
                                            if (nodo_art_princ === nodo_var && art.desc_art[0] === "=") {

                                                contatore_duegusti++;

                                                if (parseFloat(art.prezzo_un) > parseFloat(prezzo_due_gusti)) {
                                                    prezzo_due_gusti = art.prezzo_un;
                                                }
                                            }
                                        }
                                        prezzo_un = prezzo_due_gusti;

                                        var prezzo_diviso = arrotonda_prezzo(parseFloat(prezzo_due_gusti) / parseInt(contatore_duegusti));

                                        //MODIFICA 07/06/2018
                                        //IN QUESTO CASO SUDDIVIDE L'INCASSO PER OGNI GUSTO IN BASE AL NODO
                                        //SOLO NEL NODO PIU LUNGO DI TRE RISPETTO A NODO_ART_PRINC
                                        //CIOE' AI GUSTI
                                        //IL CONTATORE DICE QUANTI GUSTI SONO
                                        /*if (comanda.compatibile_xml !== true) {
                                         //BASSA PRIORITA PER NON INTACCARE PRESTAZIONI
                                         var query_prezzo_vero = "update comanda set prezzo_vero='" + prezzo_diviso + "' where stato_record='ATTIVO' AND nodo LIKE '" + nodo_art_princ + " %' AND  nodo!='" + nodo_art_princ + "' and desc_art LIKE '= 1/2 %';";
                                         
                                         comanda.sock.send({tipo: "set_prezzo_vero", operatore: comanda.operatore, query: query_prezzo_vero, terminale: comanda.terminale, ip: comanda.ip_address});
                                         
                                         comanda.sincro.query(query_prezzo_vero, function () {});
                                         
                                         }*/

                                    } else {
                                        ultima_due_gusti = false;

                                        //MODIFICA 07/06/2018
                                        /*if (comanda.compatibile_xml !== true) {
                                         //BASSA PRIORITA PER NON INTACCARE PRESTAZIONI
                                         var query_prezzo_vero = "update comanda set prezzo_vero='" + prezzo_un + "' where desc_art NOT LIKE '= 1/2 %' AND stato_record='ATTIVO' AND prog_inser='" + attivo.prog_inser + "';";
                                         
                                         comanda.sock.send({tipo: "set_prezzo_vero", operatore: comanda.operatore, query: query_prezzo_vero, terminale: comanda.terminale, ip: comanda.ip_address});
                                         
                                         comanda.sincro.query(query_prezzo_vero, function () {});
                                         }*/

                                    }

                                    numero_nodo_variante_piu = 0;

                                    console.log("CA ARTICOLO NON VARIANTE", attivo.desc_art);
                                    conto += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="color:red;cursor:pointer;">X</td></tr>';
                                    ultime_battiture += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="color:red;cursor:pointer;">X</td></tr>';
                                    tab_conto_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro; ' + prezzo_un + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro; ' + parseFloat(parseFloat(prezzo_un) * parseInt(attivo.quantita)).toFixed(2) + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="color:red;cursor:pointer;">X</td></tr>';
                                    tab_conto_separato_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="color:red;cursor:pointer;">X</td></tr>';



                                }



                            }
                            //FINE CONDIZIONI
                        }
                        //FINE CICLO FOR IN 1 --ARTICOLI
                    }
                    //FINE CICLO FOR IN 2

                    console.log("conto_attivo", d1);
                    if (d1 !== undefined && d1[0] !== undefined && d1[0].rag_soc_cliente !== undefined && d1[0].rag_soc_cliente !== null && d1[0].rag_soc_cliente.length > 0) {
                        comanda.ultimo_id_cliente = d1[0].rag_soc_cliente;
                        comanda.blocca_progressivo = true;

                        var testo_query = "select ora_consegna,tipo_consegna,jolly from comanda where desc_art NOT LIKE 'RECORD TESTA%' and  BIS='" + lettera_conto_split() + "' and  id='" + comanda.ultimo_id_asporto + "' order by ora_consegna desc, tipo_consegna desc, jolly desc limit 1;";
                        comanda.sincro.query(testo_query, function(dati_cliente) {

                            if (dati_cliente !== undefined && dati_cliente[0] !== undefined && dati_cliente[0].ora_consegna !== undefined && dati_cliente[0].tipo_consegna !== undefined) {

                                var ora = dati_cliente[0].ora_consegna;
                                $('[name="ora_consegna"]').val(ora);
                                var tipo_consegna = dati_cliente[0].tipo_consegna;
                                if (tipo_consegna === undefined) {
                                    //NIENTE
                                } else if (tipo_consegna === 'RITIRO') {
                                    comanda.tipo_consegna = "RITIRO";
                                    $('#tipo_consegna_ritiro').attr('src', './img/CasaVerde100x100.png');
                                } else if (tipo_consegna === 'DOMICILIO') {
                                    comanda.tipo_consegna = "DOMICILIO";
                                    $('#tipo_consegna_domicilio').attr('src', './img/ScooterVerde100x100.png');
                                }
                            }

                            if (dati_cliente !== undefined && dati_cliente[0] !== undefined && dati_cliente[0].jolly !== undefined && dati_cliente[0].jolly === "SUBITO") {
                                comanda.esecuzione_comanda_immediata = true;
                                var data_js = new Date();
                                var ora_adesso = addZero(data_js.getHours(), 2) + ':' + addZero(data_js.getMinutes(), 2);
                                $('#popup_scelta_cliente input[name="ora_consegna"]:visible').val(ora_adesso);
                                comanda.tipo_consegna = 'RITIRO';
                                $('#tipo_consegna_ritiro').attr('src', './img/CasaVerde100x100.png');
                                $('#tipo_consegna_domicilio').attr('src', './img/ScooterBianco100x100.png');
                                $('#switch_esecuzione_comanda_immediata').css('background-color', 'grey');
                            } else {
                                comanda.esecuzione_comanda_immediata = false;
                                $('#switch_esecuzione_comanda_immediata').css('background-color', '');
                            }
                            //FACENDO COSI DOVREI RISOLVERE UN PROBLEMA DELLA PIZZERIA ASPORTO
                            //CIOE' CHE RIAPRENDO LE RIGHE FORNO, QUANDO FACCIO CONTO ATTIVO
                            //NON RIPRENDE I DATI DEL CLIENTE
                            resolve(true);
                        });
                    } else {
                        resolve(true);
                    }

                });
            });
            //FINE BLOCCO


            //BLOCCO
            var p3 = new Promise(function(resolve, reject) {
                var esclusione_fiscale_sospeso = '';
                //if (typeof (comanda.tavolo) === 'string' && (comanda.tavolo === "BAR" || comanda.tavolo.indexOf('CONTINUO') !== -1)) {
                esclusione_fiscale_sospeso = ' and fiscale_sospeso!="STAMPAFISCALESOSPESO" ';
                //}

                testo_query = "SELECT perc_iva,numero_conto,nodo,prog_inser,sum(quantita) as quantita,desc_art,prezzo_un FROM comanda WHERE desc_art NOT LIKE 'RECORD TESTA%' and  BIS='" + lettera_conto_split() + "' and   (numero_conto='' OR numero_conto='undefined' OR numero_conto is null OR numero_conto='null' OR numero_conto='1' OR numero_conto=1)  AND stato_record='ATTIVO' AND posizione='SCONTO' AND ntav_comanda='" + comanda.tavolo + "' " + esclusione_fiscale_sospeso + " and  BIS='" + lettera_conto_split() + "' GROUP BY numero_conto,prezzo_un ORDER BY nodo ASC;";


                comanda.sincro.query(testo_query, function(d2) {

                    d2.forEach(function(attivo) {
                        //IVA DI DEFAULT 10 REPARTO 2
                        //10% ï¿½ il reparto 2 (FPWizard)
                        var reparto = "rep_1";
                        var iva_esposta = ive_misuratore_controller.iva_reparto(1);

                        if (typeof(attivo.perc_iva) === "string") {
                            switch (attivo.perc_iva.trim()) {
                                case ive_misuratore_controller.iva_reparto(2):
                                    //22% ï¿½ il reparto 1 (FPWizard)
                                    reparto = "rep_2";
                                    break;
                                case ive_misuratore_controller.iva_reparto(3):
                                    //4% ï¿½ il reparto 3 (FPWizard)
                                    reparto = "rep_3";
                                    break;
                                default:
                            }
                            iva_esposta = attivo.perc_iva;
                            if (comanda.iva_esposta !== 'true') {
                                iva_esposta = "";
                            }
                        }

                        var settaggio_sconto_non_eliminabile = '<td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="width:10%;color:red;cursor:pointer;">X</td>';
                        if (comanda.compatibile_xml === true && comanda.sconto_non_eliminabile === "S") {
                            settaggio_sconto_non_eliminabile = '<td style="width:10%;color:red;cursor:pointer;"></td>';
                        }

                        intestazioni_conto = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td>' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td>' + attivo.prezzo_un + '</td>' + settaggio_sconto_non_eliminabile + '</tr>' + intestazioni_conto;
                        intestazioni_conto_grande = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td style="width:10%;">' + attivo.quantita + '</td><td style="width:55%;">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td  style="width:25%;">' + attivo.prezzo_un + '</td><td  style="width:25%;">' + parseFloat(parseFloat(attivo.prezzo_un) * parseInt(attivo.quantita)).toFixed(2) + '</td>' + settaggio_sconto_non_eliminabile + '</tr>' + intestazioni_conto_grande;
                        intestazioni_conto_split = '<tr  style="font-weight:bold;"   class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td>' + attivo.quantita + '</td><td>SCONTO</td><td>' + iva_esposta + '</td><td>' + attivo.prezzo_un + '</td><td></td><td></td></tr>' + intestazioni_conto_split;


                        intestazioni_conto_separato_grande = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:10%;"  >' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:55%;" >' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:25%;" >' + attivo.prezzo_un + '</td>' + settaggio_sconto_non_eliminabile + '</tr>' + intestazioni_conto_separato_grande;

                    });

                    resolve(true);

                });
            });
            //FINE BLOCCO


            //BLOCCO
            var p4 = new Promise(function(resolve, reject) {
                var testo_query = "SELECT perc_iva,numero_conto,nodo,prog_inser,sum(quantita) as quantita,desc_art,prezzo_un FROM comanda WHERE desc_art NOT LIKE 'RECORD TESTA%' and  BIS='" + lettera_conto_split() + "' and   (numero_conto='' OR numero_conto='undefined' OR numero_conto is null OR numero_conto='null' OR numero_conto='1' OR numero_conto=1)  AND stato_record='ATTIVO' AND posizione='COPERTO' AND ntav_comanda='" + comanda.tavolo + "' and  BIS='" + lettera_conto_split() + "' GROUP BY numero_conto ORDER BY nodo ASC;";
                console.log(testo_query);
                comanda.sincro.query(testo_query, function(d3) {
                    d3.forEach(function(attivo) {

                        //IVA DI DEFAULT 10 REPARTO 2
                        //10% ï¿½ il reparto 2 (FPWizard)
                        var reparto = "rep_1";
                        var iva_esposta = ive_misuratore_controller.iva_reparto(1);

                        if (typeof(attivo.perc_iva) === "string") {
                            switch (attivo.perc_iva.trim()) {
                                case ive_misuratore_controller.iva_reparto(2):
                                    //22% ï¿½ il reparto 1 (FPWizard)
                                    reparto = "rep_2";
                                    break;
                                case ive_misuratore_controller.iva_reparto(3):
                                    //4% ï¿½ il reparto 3 (FPWizard)
                                    reparto = "rep_3";
                                    break;
                                default:
                            }
                            iva_esposta = attivo.perc_iva;
                            if (comanda.iva_esposta !== 'true') {
                                iva_esposta = "";
                            }
                        }


                        intestazioni_conto = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td id="numero_coperti_effettivi">' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td>' + attivo.prezzo_un + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="color:red;cursor:pointer;">X</td></tr>' + intestazioni_conto;
                        intestazioni_conto_grande = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td  style="width:10%;" id="numero_coperti_effettivi">' + attivo.quantita + '</td><td  style="width:55%;">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td  style="width:25%;">' + attivo.prezzo_un + '</td><td  style="width:25%;">' + parseFloat(parseFloat(attivo.prezzo_un) * parseInt(attivo.quantita)).toFixed(2) + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="width:10%;color:red;cursor:pointer;">X</td></tr>' + intestazioni_conto_grande;
                        intestazioni_conto_split = '<tr  style="font-weight:bold;"  class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td id="numero_coperti_effettivi">' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td>' + attivo.prezzo_un + '</td><td></td><td></td></tr>' + intestazioni_conto_split;


                        //CONTO SEPARATO
                        intestazioni_conto_separato_grande = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:10%;" id="numero_coperti_effettivi" >' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:55%;" >' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:25%;" >' + attivo.prezzo_un + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="width:10%;color:red;cursor:pointer;">X</td></tr>' + intestazioni_conto_separato_grande;

                    });

                    resolve(true);
                });
            });
            //FINE CONTO 1








            Promise.all([p2, p3, p4]).then(function() {



                if (progressivo_ultimo_conto_interno === progressivo_ultimo_conto) {



                    var totale_calcolo_consegna = 0;

                    comanda.consegna_totale = 0;
                    comanda.consegna_unitario_mezzo_metro = 0;
                    comanda.consegna_unitario_metro = 0;
                    comanda.consegna_unitario_a_pizza = 0;
                    comanda.consegna_una_pizza = 0;
                    comanda.consegna_piu_di_una_pizza = 0;
                    comanda.consegna_unitario_maxi = 0;

                    if (comanda.disabilita_tasto_bis === true) {
                        $('.split_successivo').attr('disabled', true);
                    }


                    if (comanda.consegna_presente === true) {
                        if (calcolo_consegna_su_totale > 0 && comanda.consegna_su_totale.length > 0) {
                            if (comanda.consegna_su_totale.indexOf("%") !== -1) {
                                var add = parseFloat(calcolo_consegna_su_totale) / 100 * parseFloat(comanda.consegna_su_totale.replace("%", ""))
                                totale_calcolo_consegna += add;
                                comanda.consegna_totale += add;
                            } else {
                                var add = parseFloat(comanda.consegna_su_totale);
                                totale_calcolo_consegna += add;
                                comanda.consegna_totale += add;
                            }
                        }
                        if (calcolo_consegna_maxi > 0 && comanda.consegna_maxi.length > 0) {
                            var add = parseInt(calcolo_consegna_maxi) * parseFloat(comanda.consegna_maxi);
                            totale_calcolo_consegna += add;
                            comanda.consegna_unitario_maxi = comanda.consegna_maxi;
                        }
                        if (calcolo_consegna_mezzo_metro > 0 && comanda.consegna_mezzo_metro.length > 0) {
                            var add = parseInt(calcolo_consegna_mezzo_metro) * parseFloat(comanda.consegna_mezzo_metro);
                            totale_calcolo_consegna += add;
                            comanda.consegna_unitario_mezzo_metro = comanda.consegna_mezzo_metro;
                        }
                        if (calcolo_consegna_un_metro > 0 && comanda.consegna_metro.length > 0) {
                            var add = parseInt(calcolo_consegna_un_metro) * parseFloat(comanda.consegna_metro);
                            totale_calcolo_consegna += add;
                            comanda.consegna_unitario_metro = comanda.consegna_metro;
                        }
                        if (calcolo_consegna_a_pizza > 0 && comanda.consegna_a_pizza.length > 0) {
                            var add = parseInt(calcolo_consegna_a_pizza) * parseFloat(comanda.consegna_a_pizza);
                            totale_calcolo_consegna += add;
                            comanda.consegna_unitario_a_pizza = comanda.consegna_a_pizza;
                        }

                        if (calcolo_consegna_una_pizza === 1 && comanda.una_pizza.length > 0) {
                            var add = parseInt(calcolo_consegna_una_pizza) * parseFloat(comanda.una_pizza);
                            totale_calcolo_consegna += add;
                            comanda.consegna_una_pizza = comanda.una_pizza;
                        } else if (calcolo_consegna_piu_di_una_pizza > 1 && comanda.piu_di_una_pizza.length > 0) {
                            var add = parseInt(calcolo_consegna_piu_di_una_pizza) * parseFloat(comanda.piu_di_una_pizza);
                            totale_calcolo_consegna += add;
                            comanda.consegna_piu_di_una_pizza = comanda.piu_di_una_pizza;
                        }

                        totale_calcolo_consegna = arrotonda_prezzo(parseFloat(totale_calcolo_consegna));

                        riga_calcolo_consegna = "";
                        if (comanda.tipo_consegna === "DOMICILIO" && parseFloat(totale_calcolo_consegna) > 0) {

                            var reparto = "rep_1";
                            var iva_esposta = "";

                            try {
                                iva_esposta = ive_misuratore_controller.iva_reparto(1);
                                if (typeof(comanda.percentuale_iva_default) === "string") {
                                    switch (comanda.percentuale_iva_default) {
                                        case ive_misuratore_controller.iva_reparto(2):
                                            reparto = "rep_2";
                                            break;
                                        case ive_misuratore_controller.iva_reparto(3):
                                            reparto = "rep_3";
                                            break;
                                        default:
                                    }

                                    iva_esposta = comanda.percentuale_iva_default;
                                    if (comanda.iva_esposta !== 'true') {
                                        iva_esposta = "";
                                    }
                                }
                            } catch (e) {}

                            comanda.totale_consegna = totale_calcolo_consegna;
                            riga_calcolo_consegna = "<tr  class='" + reparto + "' id='art_999'><td style='color:transparent;'>1</td><td>CONSEGNA</td><td></td><td>&euro;" + totale_calcolo_consegna + "</td><td>€ " + totale_calcolo_consegna + "</td><td></td></tr>";
                        }
                    }

                    $('.ultime_battiture').html(ultime_battiture);
                    $('#intestazioni_conto_separato_grande').html(intestazioni_conto_separato_grande);
                    $('#intestazioni_conto_separato_2_grande').html(intestazioni_conto_separato_2_grande);
                    $('#conto').html(conto + riga_calcolo_consegna);
                    $('#campo_ordinazione .anteprima').html(conto);

                    console.log("CAMPO_ORDINAZIONI CONTO_1", conto);

                    console.log("FINE CONTO", inizio_conto, new Date().getTime(), conto);



                    $('#intestazioni_conto').html(intestazioni_conto);



                    $('.tab_conto_grande').html(tab_conto_grande + riga_calcolo_consegna);
                    $('#intestazioni_conto_grande').html(intestazioni_conto_grande);



                    socket_fast_split.send(JSON.stringify({ intestazione: intestazioni_conto_split, conto: tab_conto_grande }));
                    //$(popup_fast_split.document.getElementById("fS_prodotti_conto_grande")).html(tab_conto_grande);
                    //$(popup_fast_split.document.getElementById("fS_intestazioni_conto_grande")).html(intestazioni_conto_grande);

                    $('.tab_conto_separato_grande').html(tab_conto_separato_grande);
                    $('.tab_conto_separato_2_grande').html(tab_conto_separato_2_grande);

                    comanda.sincro.query_cassa();

                    fine_conto_attivo(callBACCO, salta_colore);

                }

            });








        }


    };

    this.nomi_categorie = function(cb) {

        var testo_query = 'SELECT * FROM categorie;';

        comanda.nome_categoria = new Object();
        comanda.caratteristiche_categoria = new Object();

        comanda.sincro.query(testo_query, function(categorie) {
            comanda.nome_categoria['ND'] = "NESSUNA";

            categorie.forEach(function(obj) {

                comanda.caratteristiche_categoria[obj.id] = new Object();


                /*cat_var,ordinamento as ordinamento_prod,dest_stampa,dest_stampa_2,portata as portata_cat*/
                comanda.caratteristiche_categoria[obj.id].cat_var = obj.cat_var;
                comanda.caratteristiche_categoria[obj.id].ordinamento_prod = obj.ordinamento;
                comanda.caratteristiche_categoria[obj.id].dest_stampa = obj.dest_stampa;
                comanda.caratteristiche_categoria[obj.id].dest_stampa_2 = obj.dest_stampa_2;
                comanda.caratteristiche_categoria[obj.id].portata_cat = obj.portata;

                comanda.nome_categoria[obj.id] = obj.descrizione;

            });

            cb(true);
        });
    };

    /* Void */
    this.elenco_categorie = function(tipo, modalita, varianti, css_patch) {

        comanda.funzionidb.nomi_categorie(function() {});

        elenco_categorie_aperte();

        var numero_layout = "_1";
        if (leggi_get_url('id') === '2' || leggi_get_url('id') === '3' || leggi_get_url('id') === '4') {
            numero_layout = "_" + leggi_get_url('id');
        }

        var testo_query = '';
        //I tipi possono essere: lista | select
        //$mod Ã¨ per cambiare nell'oggetto la categoria. Ã¨ tipo booleano

        var esclusione_visualizzazione = '';
        if (comanda.mobile === true) {
            esclusione_visualizzazione = '((categorie.escl_pda!="S" OR categorie.escl_pda is null) and (categorie.disabilita_pda!="S" OR categorie.disabilita_pda is null)) and ';
        } else {
            esclusione_visualizzazione = '(categorie.disabilita_server!="S" OR categorie.disabilita_server is null) and ';
        }

        var alias = "";
        if (comanda.fastconto !== true) {
            alias = "categorie.alias,categorie.visu_tavoli,categorie.visu_bar,categorie.visu_asporto,categorie.visu_continuo,";
        }
        if (varianti === "1") {
            testo_query = 'SELECT * FROM categorie WHERE descrizione LIKE "V.%" and descrizione NOT LIKE "GLOBALE%" UNION SELECT * FROM categorie WHERE ' + esclusione_visualizzazione + '  descrizione NOT LIKE "V.%" and descrizione NOT LIKE "GLOBALE%" ;';
        } else {
            if (tipo === "list") {
                if (comanda.pizzeria_asporto === true) {
                    if (RAM.tipo_categorie_layout_tasti === 'Varianti') {
                        testo_query = 'select distinct categorie.id,categorie.descrizione,' + alias + 'categorie.ordinamento from categorie,prodotti WHERE ' + esclusione_visualizzazione + ' categorie.descrizione LIKE "V.%" and categorie.descrizione NOT LIKE "GLOBALE%"  ORDER BY categorie.ordinamento ASC;';
                    } else {
                        testo_query = 'select distinct categorie.id,categorie.descrizione,' + alias + 'categorie.ordinamento from categorie,prodotti WHERE ' + esclusione_visualizzazione + ' categorie.abilita_asporto="true" and categorie.descrizione NOT LIKE "V.%" and categorie.descrizione NOT LIKE "GLOBALE%"  ORDER BY categorie.ordinamento ASC;';
                    }
                } else if (comanda.compatibile_xml !== true) {
                    if (comanda.mobile === true) {
                        testo_query = 'select distinct categorie.id,categorie.descrizione,' + alias + 'categorie.ordinamento from categorie,prodotti WHERE ' + esclusione_visualizzazione + ' categorie.descrizione NOT LIKE "V.%" and categorie.descrizione NOT LIKE "GLOBALE%" and categorie.descrizione NOT LIKE "PREFERITI_%" and categorie.disabilita_visualizzazione!="true"  ORDER BY categorie.ordinamento ASC;';
                    } else {
                        if (RAM.tipo_categorie_layout_tasti === 'Varianti') {
                            testo_query = 'select distinct categorie.id,categorie.descrizione,' + alias + 'categorie.ordinamento from categorie,prodotti WHERE ' + esclusione_visualizzazione + ' categorie.descrizione LIKE "V.%" and  categorie.descrizione NOT LIKE "GLOBALE%" and categorie.disabilita_visualizzazione!="true"  ORDER BY categorie.ordinamento ASC;';
                        } else {
                            testo_query = 'select distinct categorie.id,categorie.descrizione,' + alias + 'categorie.ordinamento from categorie,prodotti WHERE ' + esclusione_visualizzazione + ' categorie.descrizione NOT LIKE "V.%" and  categorie.descrizione NOT LIKE "GLOBALE%" and categorie.disabilita_visualizzazione!="true"  ORDER BY categorie.ordinamento ASC;';
                        }

                    }

                } else {
                    testo_query = 'select distinct categorie.id,categorie.descrizione,categorie.ordinamento from categorie,prodotti WHERE ' + esclusione_visualizzazione + ' categorie.descrizione NOT LIKE "V.%" and categorie.descrizione NOT LIKE "GLOBALE%"  ORDER BY categorie.ordinamento ASC;';
                }
            } else {
                testo_query = 'SELECT * FROM categorie WHERE ' + esclusione_visualizzazione + ' descrizione NOT LIKE "V.%"  and descrizione NOT LIKE "GLOBALE%"  ORDER BY ordinamento ASC;';
            }
        }


        console.log("QUERY CATEGORIE", testo_query);
        var data = [];
        var prom = new Promise(function(resolve, reject) {

            comanda.sincro.query(testo_query, function(categoria) {


                //DARE LA CONDIZIONE CHE ESISTA IL PRODOTTO
                resolve(categoria);
            });
        });
        data.push(prom);
        Promise.all(data).then(function(data) {
            var i = 0;

            var output = '';

            switch (tipo) {
                case "list":
                    if (comanda.mobile === true) {
                        output += "<div class=\"list-group\" style=\"font-size:" + comanda.grandezza_font_categorie + "vh;font-weight:bold;\">";
                    } else {
                        output += "<div class=\"list-group\" style=\"font-size:2.50vh;font-weight:bold;\">";
                    }

                    data[0].forEach(function(obj) {



                        if (comanda.compatibile_xml === true && i === 0) {
                            comanda.categoria_partenza = obj.id;
                            comanda.categoria_predefinita = obj.id;
                            comanda.ultima_categoria_principale = obj.id;
                            comanda.categoria = obj.id;
                        }
                        i++;

                        ////console.log(obj);
                        var mobile;
                        if (comanda.mobile === true && comanda.palmare !== true) {
                            //SE IL TABLET E' ASUS TESTARE
                            output += "<a " + comanda.evento + "=\"mod_categoria('" + obj.id + "','','1');categoria_cliccata();\" class=\"cat_" + obj.id + " list-group-item\">" + obj.descrizione + "</a>";
                        } else {

                            var classe_preferiti = "";
                            if (obj.visu_tavoli === "true") {
                                classe_preferiti += " visu_tavoli ";
                            }
                            if (obj.visu_bar === "true") {
                                classe_preferiti += " visu_bar ";
                            }
                            if (obj.visu_asporto === "true") {
                                classe_preferiti += " visu_asporto ";
                            }
                            if (obj.visu_continuo === "true") {
                                classe_preferiti += " visu_continuo ";
                            }

                            if (obj.descrizione === 'PREFERITI' || obj.descrizione === 'PREFERITI' + numero_layout) {
                                output += "<a style='color:goldenrod;' " + comanda.evento + "=\"mod_categoria('" + obj.id + "','','1');categoria_cliccata();\" class=\"cat_" + obj.id + " preferiti " + classe_preferiti + " list-group-item\">" + obj.alias + "</a>";
                            } else if (obj.descrizione.indexOf('PREFERITI') !== -1) {
                                output += "<a style='color:yellow;' " + comanda.evento + "=\"mod_categoria('" + obj.id + "','','1');categoria_cliccata();\" class=\"cat_" + obj.id + "  preferiti " + classe_preferiti + " list-group-item\">" + obj.alias.replace('_', ' ') + "</a>";
                            } else {
                                output += "<a " + comanda.evento + "=\"mod_categoria('" + obj.id + "','','1');categoria_cliccata();\" class=\"cat_" + obj.id + " list-group-item\">" + obj.descrizione + "</a>";
                            }
                        }

                    });

                    if (comanda.tasto_pizza_maxi === true) {
                        output += "<a style=\"float:right;margin-right: 15px;\" " + comanda.evento + "=\"";

                        output += 'pizza_maxi();';

                        output += "\" class=\"pizza_maxi cat_999 btn btn-warning\">&nbsp;MAXI&nbsp;&nbsp;</a>";
                    }

                    if (comanda.tasto_pizza_metro === true) {

                        output += "<a style=\"float:right;margin-right: 15px;\" " + comanda.evento + "=\"categoria_cliccata();";

                        output += 'pizza_al_metro();';

                        output += "\" class=\"pizza_un_metro cat_999 btn btn-warning\">&nbsp;1 M&nbsp;&nbsp;</a>";
                    }
                    if (comanda.tasto_pizza_mezzo_metro === true) {

                        output += "<a style=\"float:right;\" " + comanda.evento + "=\"categoria_cliccata();";

                        output += 'pizza_al_metro(\'MEZZO\');';

                        output += "\" class=\"pizza_mezzo_metro cat_999 btn btn-warning\">1/2 M</a>";

                    }

                    if (comanda.tasto_pizza_duegusti === true) {
                        output += "<a style=\"float:right;margin-right: 15px;\" " + comanda.evento + "=\"";

                        output += 'pizza_duegusti();';

                        output += "\" class=\"pizza_due_gusti cat_999 btn btn-warning\">&nbsp;DUE GUSTI&nbsp;&nbsp;</a>";
                    }

                    /*if (comanda.tasto_schiacciata === true) {
                        output += "<a style=\"float:right;margin-right: 15px;\" " + comanda.evento + "=\"";

                        output += 'schiacciata();';

                        output += "\" class=\"tasto_schiacciata cat_999 btn btn-warning\">&nbsp;SCHIACCIATA&nbsp;&nbsp;</a>";
                    }*/

                    output += "</div>";



                    break;
                case "select":
                    output += "<select class='select_elenco_categorie form-control' ";
                    if (modalita === "1") {
                        output += "onchange=\"javascript:mod_categoria($(this).find('option:selected').val());\"";
                    }
                    output += " >";


                    data[0].forEach(function(obj) {


                        /* Eliminato perchè su XML non ci sono le tabelle gestionali */
                        /* Poi diceva a Tiziana che non trova la categoria predefinita (Andrea sa)*/

                        /*if (comanda.compatibile_xml === true && i === 0) {
                            comanda.categoria_partenza = obj.id;
                            comanda.categoria_predefinita = obj.id;
                            comanda.ultima_categoria_principale = obj.id;
                            comanda.categoria = obj.id;
                        }*/

                        i++;

                        output += "<option value=\"" + obj.id + "\">(" + obj.id + ") " + obj.descrizione + "</option>";
                    });
                    output += "</select>";
                    break;
                default:
                    break;
            }

            // alla fine scrive solamente nell'html il codice senza callback
            $(css_patch).html(output);
            //aggiunge un listener per tenere evidenziato il tasto della categoria sui pc

            try {
                $(document).off(comanda.eventino, evento_listener_categorie);
            } catch (e) {
                console.log(e);
            }

            $(document).on(comanda.eventino, "#tab_sx a:not(.pizza_maxi)", evento_listener_categorie);
        });
    };


    function elenco_categorie_aperte() {
        comanda.categorie_varianti_aperte = new Array();
        var query = "select * from categorie where var_aperte='S';";

        comanda.sincro.query(query, function(i) {

            i.forEach(function(obj) {
                if (obj.var_aperte === 'S') {
                    comanda.categorie_varianti_aperte.push(obj.id);
                }
            });
        });
    }



    var evento_listener_categorie = function(event) {

        //AGGIUNTA RIGA SUCCESSIVA IL 18/02/2019 ALTRIMENTI SI SPEGNE IL FOCUS SULLE CATEGORIE
        if (comanda.pizzeria_asporto === true) {
            if ($(this).hasClass("cat_accesa")) {
                if ($(this).hasClass("pizza_mezzo_metro") || $(this).hasClass("pizza_un_metro")) {
                    //Aggiunto .not('.list-group-item') il 25/07/2019 per accendere la categoria selezionata
                    $("#tab_sx a").not(".list-group-item").removeClass("cat_accesa");
                } else {
                    $("#tab_sx a").not('.list-group-item,.pizza_un_metro,.pizza_mezzo_metro,.pizza_maxi').removeClass("cat_accesa");
                    //Aggiunto il 25/07/2019 per accendere la categoria selezionata
                    $(event.target).addClass("cat_accesa");
                }
            } else {
                if ($(this).hasClass("pizza_mezzo_metro") || $(this).hasClass("pizza_un_metro")) {
                    $("#tab_sx a").not(".list-group-item").removeClass("cat_accesa");
                } else {
                    $("#tab_sx a").not('.list-group-item,.pizza_un_metro,.pizza_mezzo_metro,.pizza_maxi').removeClass("cat_accesa");
                }
                $(event.target).addClass("cat_accesa");
            }

            //AGGIUNTA 15/07/2019 per non far spegnere il focus sulle maxi e due gusti se cambio categoria
            if (pizza_due_gusti === true) {
                $(".pizza_due_gusti").addClass("cat_accesa");
            } else {
                $(".pizza_due_gusti").removeClass("cat_accesa");
            }

            /*if (boolean_schiacciata === true) {
                $(".tasto_schiacciata").addClass("cat_accesa");
            } else {
                $(".tasto_schiacciata").removeClass("cat_accesa");
            }*/

            if (bool_pizza_maxi === true) {
                $(".pizza_maxi").addClass("cat_accesa");
            } else {
                $(".pizza_maxi").removeClass("cat_accesa");
            }
        }
    };

    this.pagine = function(callBack) {
        var cb;
        callBack(cb);
        return true;
    };
    this.aggiungi_articolo = function(id, form, quantita, pos, operazione, progressivo, perc_iva_base, perc_iva_takeaway, prechiuso, cb) {

        console.log("aggiungi_articolo **", id, form, quantita, pos, operazione, progressivo, perc_iva_base, perc_iva_takeaway, prechiuso, cb);
        var testo_query;
        var prodotto;
        var id_prodotto = id;
        //-------------------------------------------------------------------------------------------------
        //QUI INIZIA IL PROGRAMMA IN REALTA', MA IL SECONDO BLOCCO E' PRIMA ALTRIMENTI LA FUNZIONE SAREBBE UNDEFINED

        //se Ã¨ un articolo con id
        if (id && form === null) {
            testo_query = 'SELECT categorie.cat_var,prodotti.gruppo,prodotti.automodifica_prezzo,prodotti.spazio_forno,prodotti.cat_varianti,prodotti.id,prodotti.categoria,prodotti.prezzo_fattorino1_norm,prodotti.prezzo_fattorino2_norm,prodotti.prezzo_fattorino3_norm,prodotti.prezzo_1,prodotti.prezzo_2,prodotti.prezzo_3,prodotti.prezzo_4,prodotti.prezzo_varianti_aggiuntive,prodotti.listino_bar,prodotti.listino_asporto,prodotti.listino_continuo,prodotti.prezzo_2,prodotti.cod_articolo,prodotti.descrizione,\n\
                             prodotti.prezzo_varianti_maxi,prodotti.prezzo_maxi,prodotti.prezzo_maxi_prima,prodotti.prezzo_varianti_maxi,categorie.ordinamento as ordinamento_prod,prodotti.ordinamento as ordinamento_cat,prodotti.peso_ums,\n\
				categorie.dest_stampa,categorie.dest_stampa_2,prodotti.dest_st_1,prodotti.dest_st_2,prodotti.cod_promo,categorie.portata as portata_cat,prodotti.portata as portata_prod,prodotti.ultima_portata \n\
					FROM prodotti,categorie WHERE prodotti.categoria!="XXX" and prodotti.descrizione!=".NUOVO PRODOTTO" and prodotti.id="' + id + '" and categorie.id=prodotti.categoria;';
            console.log("QUERY_DESTINAZIONE_STAMPA", testo_query);
            comanda.sincro.query(testo_query, function(cb1) {
                //Memorizzo in prodotto le informazioni del prodotto da DataBase
                prodotto = cb1[0];
                console.log("funzioni db aggiungi_articolo", prodotto);
                secondo_blocco(id, id_prodotto, progressivo, prodotto, quantita, pos, operazione, perc_iva_base, perc_iva_takeaway, prechiuso, cb);
                console.log("passaggio1 riga>");
            });
        } else if (form && id === null) {
            var arr = {};
            parse_str(form.replace(/%/, '%25'), arr);
            //Memorizzo in prodotto le informazioni del prodotto da form
            prodotto = arr;
            console.log("funzioni db aggiungi_articolo", prodotto);
            secondo_blocco(id, id_prodotto, progressivo, prodotto, quantita, pos, operazione, perc_iva_base, perc_iva_takeaway, prechiuso, cb);
        }
    };



    this.data_attuale = function(full_year) {

        var d = new Date();
        if (full_year !== true)
            var Y = d.getFullYear().toString().substr(2, 2);
        else
            var Y = d.getFullYear().toString();
        var D = addZero(d.getDate(), 2);
        var M = addZero((d.getMonth() + 1), 2);
        var h = addZero(d.getHours(), 2);
        var m = addZero(d.getMinutes(), 2);
        var s = addZero(d.getSeconds(), 2);
        var ms = addZero(d.getMilliseconds(), 3);
        var output = D + '-' + M + '-' + Y + ' ' + h + ':' + m;
        return output;
    };


    this.data_cartella_xml = function() {

        var d = new Date();

        var ora = d.getHours();

        if (ora < comanda.ora_servizio) {
            d.setDate(d.getDate() - 1);
        }

        var Y = d.getFullYear().toString();
        var M = addZero((d.getMonth() + 1), 2);
        var D = addZero(d.getDate(), 2);


        var output = Y + '-' + M + '-' + D;
        return output;

    };
    this.aggiungi_categoria = function(callBack) {

        var cb;
        callBack(cb);
        return true;
    };
    this.aggiungi_cliente = function(callBack) {

        var cb;
        callBack(cb);
        return true;
    };
    this.aggiungi_prodotto = function(callBack) {

        var cb;
        callBack(cb);
        return true;
    };
    this.aggiungi_sconto = function(callBack) {

        var cb;
        callBack(cb);
        return true;
    };

    this.cancella_conto_intero = function() {

        bootbox.confirm(comanda.lang[117], function(r) {

            if (r === true) {

                comanda.consegna_fattorino_extra = "0.00";

                let c = alasql("select desc_art,stato_record from comanda where stampata_sn='S' and stato_record='ATTIVO'  and fiscalizzata_sn!='S' limit 1;");

                /*if (c.length > 0) {

                    var data = comanda.funzionidb.data_attuale();
                    var builder = new epson.ePOSBuilder();
                    builder.addTextFont(builder.FONT_A);
                    builder.addTextAlign(builder.ALIGN_CENTER);
                    builder.addTextSize(2, 2);
                    //OVVERO SE L'ARTICOLO E' UNA VARIANTE
                    builder.addFeedLine(1);
                    builder.addFeedLine(1);
                    builder.addText(comanda.parcheggio + '\n');
                    builder.addFeedLine(1);
                    builder.addText('CONSEGNA: ' + comanda.ora_consegna + ':' + comanda.minuti_consegna + '\n');
                    builder.addFeedLine(1);
                    builder.addTextSize(1, 1);
                    builder.addText("-----------------------------------------\n");
                    builder.addTextSize(2, 2);
                    builder.addTextStyle(true, false, true, builder.COLOR_1);
                    builder.addText('ORDINE CANCELLATO\n');
                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                    builder.addTextSize(1, 1);
                    builder.addText("-----------------------------------------\n");
                    builder.addTextSize(2, 2);
                    builder.addFeedLine(1);
                    builder.addText(comanda.tipo_consegna + '\n');

                    if (comanda.numero_licenza_cliente === "0629") {
                        builder.addFeedLine(1);
                        let testo_query = "select * from elenco_progressivi_asporto_domicilio where progressivo_comanda='" + id_comanda_attuale_alasql() + "'  and fiscalizzata_sn!='S' limit 1;";

                        let progressivo_attuale = alasql(testo_query);

                        if (progressivo_attuale.length > 0) {

                            builder.addText("Num. Consegna: " + progressivo_attuale[0].numero + '\n');
                        }
                    }

                    builder.addFeedLine(1);

                    builder.addTextSize(1, 1);
                    builder.addTextAlign(builder.ALIGN_LEFT);
                    builder.addText('Cancellato alle ' + data.replace(/-/gi, '/').substr(9) + '\n');
                    builder.addFeedLine(1);
                    builder.addFeedLine(1);
                    builder.addCut(builder.CUT_FEED);
                    var request = builder.toString();
                    //CONTENUTO CONTO
                    //console.log(request);

                    //Create a SOAP envelop
                    var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
                    //Create an XMLHttpRequest object
                    var xhr = new XMLHttpRequest();
                    //Set the end point address

                    let d = alasql("select distinct dest_stampa from comanda where id='" + id_comanda_attuale_alasql() + "'  and fiscalizzata_sn!='S';");
                    let url = "";

                    d.forEach(v => {
                        url = 'http://' + comanda.nome_stampante[v.dest_stampa].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';

                        if (comanda.nome_stampante[v.dest_stampa].intelligent === "SDS") {
                            url = 'http://' + comanda.nome_stampante[v.dest_stampa].ip + ':8000/SPOOLER?stampante=' + comanda.nome_stampante[v.dest_stampa].devid_nf;
                        }

                        //Open an XMLHttpRequest object
                        xhr.open('POST', url, true);
                        //<Header settings>
                        xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
                        xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
                        xhr.setRequestHeader('SOAPAction', '""');
                        // Send print document
                        xhr.send(soap);
                    });




                }*/



                //VECCHIA QUERY 02/05/2017
                //var testo_query = "DELETE FROM comanda WHERE stato_record='ATTIVO' AND ntav_comanda='" + comanda.tavolo + "' AND (stampata_sn ='' OR stampata_sn='null' OR stampata_sn='n') ;";

                var data = comanda.funzionidb.data_attuale().substr(0, 8);
                var ora = comanda.funzionidb.data_attuale().substr(9, 8).replace(/-/gi, '/');





                var testo_query = "update comanda set operatore_cancellazione='" + comanda.operatore + "',stato_record='CANCELLATO DURANTE CREAZIONE COMANDA " + data + " " + ora + "' WHERE stato_record='ATTIVO' AND ntav_comanda='" + comanda.tavolo + "' AND (stampata_sn ='' OR stampata_sn='null' OR stampata_sn='N') and fiscalizzata_sn!='S';";

                if (comanda.tavolo === "BANCO" || comanda.tavolo === "BAR" || comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === "TAKE AWAY") {

                    /* tolto if perchè non vanno cancellati i fiscalizzati in nessun caso, e non solo se è pizzeria da asporto */
                    /*if (comanda.pizzeria_asporto === true) {*/
                    testo_query = "UPDATE comanda SET operatore_cancellazione='" + comanda.operatore + "',stato_record='CANCELLATO CONTO INTERO " + data + " " + ora + "' WHERE stato_record='ATTIVO' AND ntav_comanda='" + comanda.tavolo + "' and fiscalizzata_sn!='S';";
                    /*} else
                     {
                     testo_query = "UPDATE comanda SET operatore_cancellazione='" + comanda.operatore + "',stato_record='CANCELLATO CONTO INTERO " + data + " " + ora + "' WHERE stato_record='ATTIVO' AND ntav_comanda='" + comanda.tavolo + "';";
                     }*/
                }

                /* tolta il 14/12/2020 perche rum e cioccolato non lo abbiamo piu quindi farebbe solo casino */
                /*if (comanda.licenza_vera === "RUMECIOCCOLATO" && (comanda.tavolo === "BAR" || (typeof (comanda.tavolo) === "string" && comanda.tavolo.indexOf("CONTINUO") !== -1))) {
                 testo_query = "delete from comanda WHERE stato_record='ATTIVO' AND ntav_comanda='" + comanda.tavolo + "' and fiscalizzata_sn!='S';";
                 }*/

                var settaggi_profili = alasql("select Field386 from settaggi_profili limit 1;");
                settaggi_profili = settaggi_profili[0];

                if (settaggi_profili.Field386 === "true") {
                    testo_query = "update comanda set operatore_cancellazione='" + comanda.operatore + "',stato_record='CANCELLATO CONTO INTERO - GUARDARE STAMPATA_SN. " + data + " " + ora + "' WHERE stato_record='ATTIVO' AND ntav_comanda='" + comanda.tavolo + "' and fiscalizzata_sn!='S';";
                }






                console.log(testo_query);
                comanda.sock.send({ tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });


                salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + aggZero(new Date().getMilliseconds(), 3) + " # CANCELLA CONTO INTERO # QUERY: " + testo_query.replace(/[\n\r]+/g, '') + " # ");


                alasql(testo_query);


                /* tolta il 14/12/2020 perche rum e cioccolato non lo abbiamo piu quindi farebbe solo casino */
                /*if (comanda.licenza_vera === "RUMECIOCCOLATO" && (comanda.tavolo === "BAR" || (typeof (comanda.tavolo) === "string" && comanda.tavolo.indexOf("CONTINUO") !== -1))) {
                 testo_query = "update tavoli SET colore='grey' WHERE numero='" + comanda.tavolo + "';";
                 
                 comanda.sock.send({tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
                 
                 comanda.sincro.query(testo_query, function () {});
                 }*/


                //CHIUSURA
                //D22072016

                let conta_articoli_rimanenti = alasql("select desc_art FROM comanda WHERE stato_record='ATTIVO' AND ntav_comanda='" + comanda.tavolo + "';");

                /* aggiunto if il 14/12/2020 perchè sennò toglieva il parcheggio anche se restano articoli nell ordine*/
                if (conta_articoli_rimanenti.length === 0) {
                    comanda.parcheggio = '';
                    reset_progressivo_asporto();
                    $('span.nome_parcheggio').html(comanda.parcheggio);


                    //Va meglio col timeout soprattutto in fastorder
                    //setTimeout(function () {


                    $('#numero_coperti').val('0');

                    if (comanda.mobile !== true) {
                        mod_categoria(comanda.ultima_categoria_principale);
                    } else {
                        comanda.categoria = comanda.ultima_categoria_principale;
                        indietro_categoria();
                        elenco_prodotti();
                    }
                }

                if (comanda.pizzeria_asporto === true) {
                    righe_forno();
                }

                comanda.funzionidb.conto_attivo();


                comanda.sincro.query_cassa();
                //},500);


            }
        });
    };
    this.chiudi_scontrino_tedesco = function(numero_progressivo_scontrino, tipo_ricevuta, dati_testa) {
        var data = comanda.funzionidb.data_attuale().substr(0, 8);
        var ora = comanda.funzionidb.data_attuale().substr(9, 8);
        var data_fiscale = comanda.funzionidb.data_attuale();
        var testo_query = '';
        var sconto_netto = dati_testa.sconto_netto;
        var sconto_perc = dati_testa.sconto_perc;
        var tipo_sconto = dati_testa.sconto_imp;
        var tavolo_testa = dati_testa.tavolo;



        salva_record_testa(dati_testa, function() {
            //CHIUDE DEI DB DIVERSI A SECONDA DEL TIPO DI CONTO
            switch (comanda.conto) {

                case "#prodotti_conto_separato_2_grande":
                    testo_query = "UPDATE comanda SET netto='" + sconto_netto + "',sconto_perc='" + sconto_perc + "',sconto_imp='" + tipo_sconto + "',fiscalizzata_sn='N',tipo_ricevuta='" + tipo_ricevuta + "',/*data_stampa='" + data + "',*/data_fisc='" + data + "',/*ora_stampa='" + ora + "',*/ora_fisc='" + ora + "',progressivo_fiscale='" + numero_progressivo_scontrino + "',stato_record='CHIUSO' WHERE progressivo_fiscale='' and numero_conto='2' and stato_record='ATTIVO' and ntav_comanda='" + tavolo_testa + "';";
                    break;
                case "#prodotti_conto_separato_grande":
                    testo_query = "UPDATE comanda SET netto='" + sconto_netto + "',sconto_perc='" + sconto_perc + "',sconto_imp='" + tipo_sconto + "',fiscalizzata_sn='N',tipo_ricevuta='" + tipo_ricevuta + "',/*data_stampa='" + data + "',*/data_fisc='" + data + "',/*ora_stampa='" + ora + "',*/ora_fisc='" + ora + "',progressivo_fiscale='" + numero_progressivo_scontrino + "',stato_record='CHIUSO' WHERE progressivo_fiscale='' and (numero_conto='1' or numero_conto is null or numero_conto='null') and stato_record='ATTIVO' and ntav_comanda='" + tavolo_testa + "';";
                    break;
                case "#conto":
                    testo_query = "UPDATE comanda SET netto='" + sconto_netto + "',sconto_perc='" + sconto_perc + "',sconto_imp='" + tipo_sconto + "',fiscalizzata_sn='N',tipo_ricevuta='" + tipo_ricevuta + "',/*data_stampa='" + data + "',*/data_fisc='" + data + "',/*ora_stampa='" + ora + "',*/ora_fisc='" + ora + "',progressivo_fiscale='" + numero_progressivo_scontrino + "',stato_record='CHIUSO' WHERE progressivo_fiscale='' and stato_record='ATTIVO' and ntav_comanda='" + tavolo_testa + "';";
                    break;
            }

            //console.log(testo_query);

            comanda.sock.send({ tipo: "chiudi_scontrino", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
            comanda.sincro.query(testo_query, function() {

                //CHIUSURA
                comanda.sincro.query_cassa();
                if (tavolo_testa !== "BANCO" && tavolo_testa !== "BAR" && tavolo_testa.left(7) !== "ASPORTO" && tavolo_testa !== "TAKE AWAY") {

                    //console.log("577");

                    //console.log($('#prodotti_conto_separato_2_grande tr,#prodotti_conto_separato_grande tr').length);

                    if ($('#prodotti_conto_separato_grande tr').length === 0 || $('#prodotti_conto_separato_2_grande tr').length === 0) {
                        comanda.reimposta_variabili_conto_separato();
                        $('#conto_separato_grande').modal('hide');
                        //console.log("583");

                        var testo_query = "update tavoli set colore='0',occupato='0',ora_apertura_tavolo='-',ora_ultima_comanda='-' where numero='" + tavolo_testa + "' ";
                        comanda.sock.send({ tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });

                        comanda.sincro.query(testo_query, function() {
                            //TOLTO 1
                            disegna_ultimo_tavolo_modificato(testo_query);
                            console.log("TAVOLO TEDESCO GIUSTO");
                            btn_tavoli('scontrino');

                        });


                    } else
                    //SE L'INCASSO NON E L'ULTIMO FA IL TAVOLO BLU
                    {

                        var testo_query = "update tavoli set colore='3' where numero='" + tavolo_testa + "' ";
                        comanda.sock.send({ tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                        comanda.sincro.query(testo_query, function() {
                            disegna_ultimo_tavolo_modificato(testo_query);
                            console.log("INCASSO TEDESCO GIUSTO");
                        });
                    }
                } else if ($('#prodotti_conto_separato_grande tr').length === 0 || $('#prodotti_conto_separato_2_grande tr').length === 0) {
                    comanda.parcheggio = '';
                    reset_progressivo_asporto();
                    $('.nome_parcheggio').html(comanda.parcheggio);
                    if (comanda.torna_bar === true) {
                        //SE SONO IN BAR O SIMILI
                        $('#conto_separato_grande').modal('hide');
                    } else {
                        $('#conto_separato_grande').modal('hide');
                        //TOLTO 1
                        //btn_tavoli('scontrino');
                    }
                }
                comanda.funzionidb.conto_attivo();
            });
        });
    };
    this.chiudi_scontrino = function(tipo, dati_testa, risposta_xml) {

        console.log("CHIUDI SCONTRINO 1", dati_testa.id_comanda, dati_testa);
        var testo_query = '';
        var xml;
        dati_testa = JSON.parse(dati_testa);
        var id_comanda = dati_testa.id_comanda;
        console.log("DATI TESTA CHIUSURA SCONTRINO", dati_testa);
        var sconto_perc = dati_testa.sconto_perc;
        var tipo_sconto = dati_testa.sconto_imp;
        var sconto_netto = dati_testa.sconto_netto;
        var posizione_conto = dati_testa.posizione_conto;
        console.log("GESTIONE SCONTRINO", tipo, posizione_conto);
        var prezzo_vero = "prezzo_un";

        if (comanda.pizzeria_asporto === true) {
            prezzo_vero = "prezzo_vero";
        }


        if (comanda.pizzeria_asporto !== true && dati_testa.tavolo.left(7) === "ASPORTO" && (tipo === "stampante fiscale" || tipo === "scontrino" || tipo === "scontrino fiscale")) {
            var epos2 = new epson.fiscalPrint();
            xml = '<printerCommand><directIO  command="1078" data="0130011" /></printerCommand>';
            epos2.send("http://" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=local_printer&timeout=30000", xml, 30000);
            epos2.onreceive = function(result, tag_names_array, add_info, res_add) {

            }

            var tipo = dati_testa.tipo;
            console.log("CHIUDI SCONTRINO 2");
            xml = '<printerCommand><directIO  command="1070" data="01" /></printerCommand>';
            var epos = new epson.fiscalPrint();
            epos.send("http://" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=local_printer&timeout=30000", xml, 30000);
            epos.onreceive = function(result, tag_names_array, add_info, res_add) {
                console.log("CHIUDI SCONTRINO 3");

                var numero_chiusura = "";

                try {
                    numero_chiusura = aggZero(risposta_xml.getElementsByTagName("zRepNumber")[0].innerHTML, 4);
                } catch (e) {

                }

                var numero_scontrino = (parseInt(add_info.responseData.substr(2, 4)) - 1);
                //console.log("Numero scontrino: " + numero_scontrino);

                var fiscalizzata_sn = "N";
                if (result.success === true && result.status === 2) {
                    fiscalizzata_sn = "S";
                }

                var data = comanda.funzionidb.data_attuale().substr(0, 8);
                var ora = comanda.funzionidb.data_attuale().substr(9, 8);
                var data_fiscale = comanda.funzionidb.data_attuale();
                dati_testa.zrep = numero_chiusura;
                dati_testa.numero_scontrino = numero_scontrino;
                console.log("CALL SALVA DATI TESTA", dati_testa);
                salva_record_testa(dati_testa, function() {


                    salva_consegna_scontrino(id_comanda);


                    switch (posizione_conto) {


                        case "#prodotti_conto_separato_2_grande":
                            testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='SCONTRINO',tipo_ricevuta='" + tipo.toUpperCase() + "', /*data_stampa='" + data + "',*/data_fisc='" + data + "',/*ora_stampa='" + ora + "',*/ora_fisc='" + ora + "',progressivo_fiscale='" + numero_scontrino + "',n_fiscale='" + numero_scontrino + "',fiscalizzata_sn='S' WHERE progressivo_fiscale='' and  numero_conto='2' and id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO' and ntav_comanda='" + dati_testa.tavolo + "' and desc_art!='EXTRA';";
                            break;
                        case "#prodotti_conto_separato_grande":
                            testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='SCONTRINO',tipo_ricevuta='" + tipo.toUpperCase() + "',/*data_stampa='" + data + "',*/data_fisc='" + data + "',/*ora_stampa='" + ora + "',*/ora_fisc='" + ora + "',progressivo_fiscale='" + numero_scontrino + "',n_fiscale='" + numero_scontrino + "',fiscalizzata_sn='S' WHERE progressivo_fiscale='' and (numero_conto='1' or numero_conto is null or numero_conto='null')  and id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO' and ntav_comanda='" + dati_testa.tavolo + "' and desc_art!='EXTRA';";
                            break;
                            //#conto
                        default:
                            testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='SCONTRINO',/*data_stampa='" + data + "',*/data_fisc='" + data + "',/*ora_stampa='" + ora + "',*/ora_fisc='" + ora + "',progressivo_fiscale='" + numero_scontrino + "',n_fiscale='" + numero_scontrino + "',fiscalizzata_sn='S' WHERE progressivo_fiscale='' and id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO' and ntav_comanda='" + dati_testa.tavolo + "' and desc_art!='EXTRA';";
                            break;
                    }


                    console.log("GESTIONE SCONTRINO STAMPA CHIUSA", posizione_conto, testo_query);
                    //testo_query = "UPDATE comanda SET  data_stampa='" + data + "',data_fisc='" + data + "',ora_stampa='" + ora + "',ora_fisc='" + ora + "',fiscalizzata_sn='" + fiscalizzata_sn + "',n_fiscale='" + numero_scontrino + "',stato_record='CHIUSO' WHERE stato_record='ATTIVO' and ntav_comanda='" + comanda.tavolo + "';";
                    comanda.sock.send({ tipo: "chiudi_scontrino", operatore: dati_testa.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                    comanda.sincro.query(testo_query, function() {

                        testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='SCONTRINO',tipo_ricevuta='" + tipo.toUpperCase() + "',/*data_stampa='" + data + "',*/data_fisc='" + data + "',/*ora_stampa='" + ora + "',*/ora_fisc='" + ora + "',progressivo_fiscale='" + numero_scontrino + "',stato_record='CHIUSO',n_fiscale='" + numero_scontrino + "',fiscalizzata_sn='S' WHERE id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO' and ntav_comanda='" + dati_testa.tavolo + "' and  desc_art='EXTRA';";
                        comanda.sock.send({ tipo: "chiudi_scontrino", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                        comanda.sincro.query(testo_query, function() {

                            testo_query = "UPDATE comanda SET totale_scontato_ivato =ROUND(((" + prezzo_vero + "*quantita)-((" + prezzo_vero + "*quantita)*sconto_perc/100)),2)  where (sconto_perc!='' and sconto_perc!='0' and sconto_perc!='0.00');";
                            comanda.sock.send({ tipo: "chiudi_scontrino", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                            comanda.sincro.query(testo_query, function() {

                                testo_query = "UPDATE comanda SET totale_scontato_ivato =ROUND(((" + prezzo_vero + "*quantita)),2)  where (sconto_perc='' or sconto_perc='0' or sconto_perc='0.00');";
                                comanda.sock.send({ tipo: "chiudi_scontrino", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                                comanda.sincro.query(testo_query, function() {



                                    let iva_rep = new Object();

                                    for (let department = 1; department <= 5; department++) {


                                        iva_rep[department] = parseInt(REPARTO_controller.iva_reparto(parseInt(REPARTO_controller.indice(parseInt(department)))));

                                        if (isNaN(iva_rep[department])) {
                                            iva_rep[department] = 0;
                                        }

                                        testo_query = "UPDATE comanda SET imponibile_rep_" + department + "=ROUND((totale_scontato_ivato/" + (iva_rep[department] / 100 + 1) + "),8)  where reparto='" + department + "'";
                                        comanda.sock.send({ tipo: "chiudi_scontrino", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                                        alasql(testo_query);

                                        testo_query = "UPDATE comanda SET imposta_rep_" + department + "=ROUND((totale_scontato_ivato-imponibile_rep_" + department + "),8) where reparto='" + department + "';";
                                        comanda.sock.send({ tipo: "chiudi_scontrino", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                                        alasql(testo_query);

                                        testo_query = "UPDATE comanda SET importo_totale_fiscale=(imponibile_rep_1+imponibile_rep_2+imponibile_rep_3+imponibile_rep_4+imponibile_rep_5+imposta_rep_1+imposta_rep_2+imposta_rep_3+imposta_rep_4+imposta_rep_5);";
                                        comanda.sock.send({ tipo: "chiudi_scontrino", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                                        alasql(testo_query);

                                    }


                                    righe_forno();


                                    console.log("CHIUDI SCONTRINO 4");
                                    comanda.sincro.query_cassa();
                                    if (dati_testa.conto === '#prodotti_conto_separato_2_grande' || dati_testa.conto === '#prodotti_conto_separato_grande' || dati_testa.tavolo === "BANCO" || dati_testa.tavolo === "BAR" || dati_testa.tavolo.left(7) === "ASPORTO" || dati_testa.tavolo === "TAKE AWAY") {

                                        //LO TOLGO SENNO LO RIFA QUANDO HA STAMPATO E NON VOGLIO
                                        //TOLTO
                                        //btn_tavoli('scontrino');

                                        comanda.funzionidb.conto_attivo();
                                        setTimeout(function() {
                                            if ($('#conto tr').length === 0) {
                                                comanda.parcheggio = '';
                                                reset_progressivo_asporto();
                                                $('.nome_parcheggio').html(comanda.parcheggio);
                                            }
                                        }, 100);
                                    }


                                });
                            });
                        });
                    });
                });
            };
        } else if (comanda.cassa_intellinet === "N" && (tipo === "stampante fiscale" || tipo === "scontrino" || tipo === "SCONTRINO" || tipo === "scontrino fiscale")) {

            console.log("CHIUDI SCONTRINO 3 NON INTELLIGENT");

            var numero_scontrino = "N.D.";

            var data = comanda.funzionidb.data_attuale().substr(0, 8);
            var ora = comanda.funzionidb.data_attuale().substr(9, 8);

            dati_testa.zrep = "N.D.";
            dati_testa.numero_scontrino = "N.D.";

            console.log("CALL SALVA DATI TESTA", dati_testa);
            salva_record_testa(dati_testa, function() {


                salva_consegna_scontrino(id_comanda);

                if (comanda.pizzeria_asporto !== true) {
                    //CHIUDE DEI DB DIVERSI A SECONDA DEL TIPO DI CONTO
                    switch (posizione_conto) {

                        case "#prodotti_conto_separato_2_grande":
                            testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='SCONTRINO',netto='" + sconto_netto + "',sconto_perc='" + sconto_perc + "',sconto_imp='" + tipo_sconto + "',tipo_ricevuta='" + tipo.toUpperCase() + "',/*data_stampa='" + data + "',*/data_fisc='" + data + "',/*ora_stampa='" + ora + "',*/ora_fisc='" + ora + "',progressivo_fiscale='" + numero_scontrino + "',stato_record='CHIUSO',n_fiscale='" + numero_scontrino + "',fiscalizzata_sn='S' WHERE progressivo_fiscale='' and numero_conto='2' and id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO' and ntav_comanda='" + dati_testa.tavolo + "' and desc_art!='EXTRA';";
                            break;
                        case "#prodotti_conto_separato_grande":
                            testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='SCONTRINO',netto='" + sconto_netto + "',sconto_perc='" + sconto_perc + "',sconto_imp='" + tipo_sconto + "',tipo_ricevuta='" + tipo.toUpperCase() + "',/*data_stampa='" + data + "',*/data_fisc='" + data + "',/*ora_stampa='" + ora + "',*/ora_fisc='" + ora + "',progressivo_fiscale='" + numero_scontrino + "',stato_record='CHIUSO',n_fiscale='" + numero_scontrino + "',fiscalizzata_sn='S' WHERE progressivo_fiscale='' and (numero_conto='1' or numero_conto is null or numero_conto='null')  and id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO' and ntav_comanda='" + dati_testa.tavolo + "' and desc_art!='EXTRA';";
                            break;
                            //#conto
                        default:
                            testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='SCONTRINO',netto='" + sconto_netto + "',sconto_perc='" + sconto_perc + "',sconto_imp='" + tipo_sconto + "',tipo_ricevuta='" + tipo.toUpperCase() + "',/*data_stampa='" + data + "',*/data_fisc='" + data + "',/*ora_stampa='" + ora + "',*/ora_fisc='" + ora + "',progressivo_fiscale='" + numero_scontrino + "',stato_record='CHIUSO',n_fiscale='" + numero_scontrino + "',fiscalizzata_sn='S' WHERE progressivo_fiscale='' and id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO' and ntav_comanda='" + dati_testa.tavolo + "' and desc_art!='EXTRA';";
                            break;
                    }
                } else {
                    switch (posizione_conto) {

                        case "#prodotti_conto_separato_2_grande":
                            testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='SCONTRINO',netto='" + sconto_netto + "',sconto_perc='" + sconto_perc + "',sconto_imp='" + tipo_sconto + "',tipo_ricevuta='" + tipo.toUpperCase() + "',/*data_stampa='" + data + "',*/data_fisc='" + data + "',/*ora_stampa='" + ora + "',*/ora_fisc='" + ora + "',progressivo_fiscale='" + numero_scontrino + "',n_fiscale='" + numero_scontrino + "',fiscalizzata_sn='S' WHERE progressivo_fiscale='' and numero_conto='2' and id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO' and ntav_comanda='" + dati_testa.tavolo + "' and desc_art!='EXTRA';";
                            break;
                        case "#prodotti_conto_separato_grande":
                            testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='SCONTRINO',netto='" + sconto_netto + "',sconto_perc='" + sconto_perc + "',sconto_imp='" + tipo_sconto + "',tipo_ricevuta='" + tipo.toUpperCase() + "',/*data_stampa='" + data + "',*/data_fisc='" + data + "',/*ora_stampa='" + ora + "',*/ora_fisc='" + ora + "',progressivo_fiscale='" + numero_scontrino + "',n_fiscale='" + numero_scontrino + "',fiscalizzata_sn='S' WHERE progressivo_fiscale='' and (numero_conto='1' or numero_conto is null or numero_conto='null')  and id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO' and ntav_comanda='" + dati_testa.tavolo + "' and desc_art!='EXTRA';";
                            break;
                            //#conto
                        default:
                            testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='SCONTRINO',netto='" + sconto_netto + "',sconto_perc='" + sconto_perc + "',sconto_imp='" + tipo_sconto + "',tipo_ricevuta='" + tipo.toUpperCase() + "',/*data_stampa='" + data + "',*/data_fisc='" + data + "',/*ora_stampa='" + ora + "',*/ora_fisc='" + ora + "',progressivo_fiscale='" + numero_scontrino + "',n_fiscale='" + numero_scontrino + "',fiscalizzata_sn='S' WHERE progressivo_fiscale='' and id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO' and ntav_comanda='" + dati_testa.tavolo + "' and desc_art!='EXTRA';";
                            break;
                    }
                }
                /* } else
                 {
                     switch (posizione_conto) {
 
                         case "#prodotti_conto_separato_2_grande":
                             testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='SCONTRINO',tipo_ricevuta='" + tipo.toUpperCase() + "', data_fisc='" + data + "',ora_fisc='" + ora + "',progressivo_fiscale='" + numero_scontrino + "',n_fiscale='" + numero_scontrino + "',fiscalizzata_sn='S' WHERE numero_conto='2' and id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO' and ntav_comanda='" + dati_testa.tavolo + "' and desc_art!='EXTRA';";
                             break;
                         case "#prodotti_conto_separato_grande":
                             testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='SCONTRINO',tipo_ricevuta='" + tipo.toUpperCase() + "',data_fisc='" + data + "',ora_fisc='" + ora + "',progressivo_fiscale='" + numero_scontrino + "',n_fiscale='" + numero_scontrino + "',fiscalizzata_sn='S' WHERE (numero_conto='1' or numero_conto is null or numero_conto='null')  and id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO' and ntav_comanda='" + dati_testa.tavolo + "' and desc_art!='EXTRA';";
                             break;
                             //#conto
                         default:
                             testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='SCONTRINO',data_fisc='" + data + "',ora_fisc='" + ora + "',progressivo_fiscale='" + numero_scontrino + "',n_fiscale='" + numero_scontrino + "',fiscalizzata_sn='S' WHERE id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO' and ntav_comanda='" + dati_testa.tavolo + "' and desc_art!='EXTRA';";
                             break;
                     }
                 }*/

                console.log("GESTIONE SCONTRINO STAMPA CHIUSA", posizione_conto, testo_query);

                comanda.sock.send({ tipo: "chiudi_scontrino", operatore: dati_testa.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                comanda.sincro.query(testo_query, function() {

                    /*var totale_scontato_ivato = "";
                     if (sconto_perc !== '' && sconto_perc !== '0' && sconto_perc !== '0.00') {
                     totale_scontato_ivato = ((prezzo_un * quantita) * sconto_perc / 100);
                     } else {
                     totale_scontato_ivato = ((prezzo_un * quantita));
                     }
                     
                     var imponibile_rep_1="0";
                     var imponibile_rep_2="0";
                     var imponibile_rep_3="0";
                     var imponibile_rep_4="0";
                     var imponibile_rep_5="0";
                     
                     var imposta_rep_1="0";
                     var imposta_rep_2="0";
                     var imposta_rep_3="0";
                     var imposta_rep_4="0";
                     var imposta_rep_5="0";
                     
                     imponibile_rep_1 = (totale_scontato_ivato / parseInt(ive_misuratore_controller.iva_reparto(1))/100+1);
                     imposta_rep_1 = (totale_scontato_ivato - imponibile_rep_1);*/

                    testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='SCONTRINO',tipo_ricevuta='" + tipo.toUpperCase() + "',/*data_stampa='" + data + "',*/data_fisc='" + data + "',/*ora_stampa='" + ora + "',*/ora_fisc='" + ora + "',progressivo_fiscale='" + numero_scontrino + "',stato_record='CHIUSO',n_fiscale='" + numero_scontrino + "',fiscalizzata_sn='S' WHERE id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO' and ntav_comanda='" + dati_testa.tavolo + "' and  desc_art='EXTRA';";
                    comanda.sock.send({ tipo: "chiudi_scontrino", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                    comanda.sincro.query(testo_query, function() {

                        testo_query = "UPDATE comanda SET totale_scontato_ivato =ROUND(((" + prezzo_vero + "*quantita)-((prezzo_un*quantita)*sconto_perc/100)),2)  where (sconto_perc!='' and sconto_perc!='0' and sconto_perc!='0.00');";
                        comanda.sock.send({ tipo: "chiudi_scontrino", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                        comanda.sincro.query(testo_query, function() {

                            testo_query = "UPDATE comanda SET totale_scontato_ivato =ROUND(((" + prezzo_vero + "*quantita)),2)  where (sconto_perc='' or sconto_perc='0' or sconto_perc='0.00');";
                            comanda.sock.send({ tipo: "chiudi_scontrino", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                            comanda.sincro.query(testo_query, function() {

                                let iva_rep = new Object();

                                for (let department = 1; department <= 5; department++) {


                                    iva_rep[department] = parseInt(REPARTO_controller.iva_reparto(parseInt(REPARTO_controller.indice(parseInt(department)))));

                                    if (isNaN(iva_rep[department])) {
                                        iva_rep[department] = 0;
                                    }

                                    testo_query = "UPDATE comanda SET imponibile_rep_" + department + "=ROUND((totale_scontato_ivato/" + (iva_rep[department] / 100 + 1) + "),8)  where reparto='" + department + "'";
                                    comanda.sock.send({ tipo: "chiudi_scontrino", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                                    alasql(testo_query);

                                    testo_query = "UPDATE comanda SET imposta_rep_" + department + "=ROUND((totale_scontato_ivato-imponibile_rep_" + department + "),8) where reparto='" + department + "';";
                                    comanda.sock.send({ tipo: "chiudi_scontrino", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                                    alasql(testo_query);

                                    testo_query = "UPDATE comanda SET importo_totale_fiscale=(imponibile_rep_1+imponibile_rep_2+imponibile_rep_3+imponibile_rep_4+imponibile_rep_5+imposta_rep_1+imposta_rep_2+imposta_rep_3+imposta_rep_4+imposta_rep_5);";
                                    comanda.sock.send({ tipo: "chiudi_scontrino", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                                    alasql(testo_query);

                                }


                                if (typeof(comanda.tavolo) === 'string' && comanda.tavolo.indexOf("CONTINUO") !== -1) {
                                    comanda.funzionidb.conto_attivo();
                                }

                                if (comanda.pizzeria_asporto === true) {
                                    righe_forno();
                                }

                                console.log("CHIUDI SCONTRINO 4");
                                comanda.sincro.query_cassa();
                                if (dati_testa.conto === '#prodotti_conto_separato_2_grande' || dati_testa.conto === '#prodotti_conto_separato_grande' || dati_testa.tavolo === "BANCO" || dati_testa.tavolo === "BAR" || dati_testa.tavolo.left(7) === "ASPORTO" || dati_testa.tavolo === "TAKE AWAY") {

                                    comanda.funzionidb.conto_attivo();
                                    setTimeout(function() {
                                        if ($('#conto tr').length === 0) {
                                            comanda.parcheggio = '';
                                            reset_progressivo_asporto();
                                            $('.nome_parcheggio').html(comanda.parcheggio);
                                        }
                                    }, 100);
                                }

                            });
                        });
                    });
                });
            });
        } else
        if (tipo === "stampante fiscale" || tipo === "scontrino" || tipo === "scontrino fiscale") {

            var epos2 = new epson.fiscalPrint();
            xml = '<printerCommand><directIO  command="1078" data="0130011" /></printerCommand>';
            epos2.send("http://" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=local_printer&timeout=30000", xml, 30000);
            epos2.onreceive = function(result, tag_names_array, add_info, res_add) {};

            var tipo = dati_testa.tipo;
            console.log("CHIUDI SCONTRINO 2");
            xml = '<printerCommand><directIO  command="1070" data="01" /></printerCommand>';
            var epos = new epson.fiscalPrint();
            epos.send("http://" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=local_printer&timeout=30000", xml, 30000);
            epos.onreceive = function(result, tag_names_array, add_info, res_add) {
                console.log("CHIUDI SCONTRINO 3");

                var numero_chiusura = "";

                try {
                    numero_chiusura = aggZero(risposta_xml.getElementsByTagName("zRepNumber")[0].innerHTML, 4);
                } catch (e) {

                }

                var numero_scontrino = (parseInt(add_info.responseData.substr(2, 4)) - 1);
                //console.log("Numero scontrino: " + numero_scontrino);

                var fiscalizzata_sn = "N";
                if (result.success === true && result.status === 2) {
                    fiscalizzata_sn = "S";
                }

                var data = comanda.funzionidb.data_attuale().substr(0, 8);
                var ora = comanda.funzionidb.data_attuale().substr(9, 8);
                var data_fiscale = comanda.funzionidb.data_attuale();
                dati_testa.zrep = numero_chiusura;
                dati_testa.numero_scontrino = numero_scontrino;
                console.log("CALL SALVA DATI TESTA", dati_testa);
                salva_record_testa(dati_testa, function() {


                    salva_consegna_scontrino(id_comanda);


                    if (comanda.pizzeria_asporto !== true) {
                        //CHIUDE DEI DB DIVERSI A SECONDA DEL TIPO DI CONTO
                        switch (posizione_conto) {

                            case "#prodotti_conto_separato_2_grande":
                                testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='SCONTRINO',netto='" + sconto_netto + "',sconto_perc='" + sconto_perc + "',sconto_imp='" + tipo_sconto + "',tipo_ricevuta='" + tipo.toUpperCase() + "',/*data_stampa='" + data + "',*/data_fisc='" + data + "',/*ora_stampa='" + ora + "',*/ora_fisc='" + ora + "',progressivo_fiscale='" + numero_scontrino + "',stato_record='CHIUSO',n_fiscale='" + numero_scontrino + "',fiscalizzata_sn='S' WHERE progressivo_fiscale='' and numero_conto='2' and id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO' and ntav_comanda='" + dati_testa.tavolo + "' and desc_art!='EXTRA';";
                                break;
                            case "#prodotti_conto_separato_grande":
                                testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='SCONTRINO',netto='" + sconto_netto + "',sconto_perc='" + sconto_perc + "',sconto_imp='" + tipo_sconto + "',tipo_ricevuta='" + tipo.toUpperCase() + "',/*data_stampa='" + data + "',*/data_fisc='" + data + "',/*ora_stampa='" + ora + "',*/ora_fisc='" + ora + "',progressivo_fiscale='" + numero_scontrino + "',stato_record='CHIUSO',n_fiscale='" + numero_scontrino + "',fiscalizzata_sn='S' WHERE progressivo_fiscale='' and (numero_conto='1' or numero_conto is null or numero_conto='null')  and id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO' and ntav_comanda='" + dati_testa.tavolo + "' and desc_art!='EXTRA';";
                                break;
                                //#conto
                            default:
                                testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='SCONTRINO',netto='" + sconto_netto + "',sconto_perc='" + sconto_perc + "',sconto_imp='" + tipo_sconto + "',tipo_ricevuta='" + tipo.toUpperCase() + "',/*data_stampa='" + data + "',*/data_fisc='" + data + "',/*ora_stampa='" + ora + "',*/ora_fisc='" + ora + "',progressivo_fiscale='" + numero_scontrino + "',stato_record='CHIUSO',n_fiscale='" + numero_scontrino + "',fiscalizzata_sn='S' WHERE progressivo_fiscale='' and id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO' and ntav_comanda='" + dati_testa.tavolo + "' and desc_art!='EXTRA';";
                                break;
                        }
                    } else {
                        switch (posizione_conto) {


                            case "#prodotti_conto_separato_2_grande":
                                testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='SCONTRINO',netto='" + sconto_netto + "',sconto_perc='" + sconto_perc + "',sconto_imp='" + tipo_sconto + "',tipo_ricevuta='" + tipo.toUpperCase() + "',/*data_stampa='" + data + "',*/data_fisc='" + data + "',/*ora_stampa='" + ora + "',*/ora_fisc='" + ora + "',progressivo_fiscale='" + numero_scontrino + "',n_fiscale='" + numero_scontrino + "',fiscalizzata_sn='S' WHERE progressivo_fiscale='' and numero_conto='2' and id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO' and ntav_comanda='" + dati_testa.tavolo + "' and desc_art!='EXTRA';";
                                break;
                            case "#prodotti_conto_separato_grande":
                                testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='SCONTRINO',netto='" + sconto_netto + "',sconto_perc='" + sconto_perc + "',sconto_imp='" + tipo_sconto + "',tipo_ricevuta='" + tipo.toUpperCase() + "',/*data_stampa='" + data + "',*/data_fisc='" + data + "',/*ora_stampa='" + ora + "',*/ora_fisc='" + ora + "',progressivo_fiscale='" + numero_scontrino + "',n_fiscale='" + numero_scontrino + "',fiscalizzata_sn='S' WHERE progressivo_fiscale='' and (numero_conto='1' or numero_conto is null or numero_conto='null')  and id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO' and ntav_comanda='" + dati_testa.tavolo + "' and desc_art!='EXTRA';";
                                break;
                                //#conto
                            default:
                                testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='SCONTRINO',netto='" + sconto_netto + "',sconto_perc='" + sconto_perc + "',sconto_imp='" + tipo_sconto + "',tipo_ricevuta='" + tipo.toUpperCase() + "',/*data_stampa='" + data + "',*/data_fisc='" + data + "',/*ora_stampa='" + ora + "',*/ora_fisc='" + ora + "',progressivo_fiscale='" + numero_scontrino + "',n_fiscale='" + numero_scontrino + "',fiscalizzata_sn='S' WHERE progressivo_fiscale='' and id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO' and ntav_comanda='" + dati_testa.tavolo + "' and desc_art!='EXTRA';";
                               
                                break;
                        }
                    }

                    console.log("GESTIONE SCONTRINO STAMPA CHIUSA", posizione_conto, testo_query);
                    //testo_query = "UPDATE comanda SET  data_stampa='" + data + "',data_fisc='" + data + "',ora_stampa='" + ora + "',ora_fisc='" + ora + "',fiscalizzata_sn='" + fiscalizzata_sn + "',n_fiscale='" + numero_scontrino + "',stato_record='CHIUSO' WHERE stato_record='ATTIVO' and ntav_comanda='" + comanda.tavolo + "';";
                    comanda.sock.send({ tipo: "chiudi_scontrino", operatore: dati_testa.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                    comanda.sincro.query(testo_query, function() {

                        testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='SCONTRINO',tipo_ricevuta='" + tipo.toUpperCase() + "',/*data_stampa='" + data + "',*/data_fisc='" + data + "',/*ora_stampa='" + ora + "',*/ora_fisc='" + ora + "',progressivo_fiscale='" + numero_scontrino + "',stato_record='CHIUSO',n_fiscale='" + numero_scontrino + "',fiscalizzata_sn='S' WHERE id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO' and ntav_comanda='" + dati_testa.tavolo + "' and  desc_art='EXTRA';";
                        comanda.sock.send({ tipo: "chiudi_scontrino", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                        comanda.sincro.query(testo_query, function() {

                            testo_query = "UPDATE comanda SET totale_scontato_ivato =ROUND(((" + prezzo_vero + "*quantita)-((prezzo_un*quantita)*sconto_perc/100)),2)  where (sconto_perc!='' and sconto_perc!='0' and sconto_perc!='0.00');";
                            comanda.sock.send({ tipo: "chiudi_scontrino", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                            comanda.sincro.query(testo_query, function() {

                                testo_query = "UPDATE comanda SET totale_scontato_ivato =ROUND(((" + prezzo_vero + "*quantita)),2)  where (sconto_perc='' or sconto_perc='0' or sconto_perc='0.00');";
                                comanda.sock.send({ tipo: "chiudi_scontrino", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                                comanda.sincro.query(testo_query, function() {

                                    let iva_rep = new Object();

                                    let totale_documento = dati_testa.totale;
                                    let non_riscosso_servizi = dati_testa.non_riscosso_servizi;

                                    let dati_comanda = alasql("select * from comanda where id='" + id_comanda + "' and tipo_ricevuta='SCONTRINO' and (stato_record='CHIUSO' or stato_record='FORNO'or stato_record='ATTIVO');");

                                    dati_comanda.forEach((corpo) => {

                                        let where_progressivo_interno = corpo.prog_inser;

                                        //reparto
                                        let department = corpo.reparto;

                                        if (department !== undefined && department !== null && department !== "undefined" && department !== "") {

                                            //percentuale iva del reparto
                                            iva_rep[department] = parseInt(REPARTO_controller.iva_reparto(parseInt(REPARTO_controller.indice(parseInt(department)))));

                                            if (isNaN(iva_rep[department])) {
                                                iva_rep[department] = 0;
                                            }



                                            // caso di acconto su servizio
                                            //calcolo delle percentuali di incidenza
                                            if (non_riscosso_servizi > 0) {

                                                /*
                                                 perc = 100 / totale_scontrino * 100;
                                                 ivato_non_risc = perc * non_riscosso / 100; //residuo lordo
                                                 totale_riscosso = totale_scontato_ivato - ivato_non_risc;
                                                 imponibile e imposta servizi
                                                 
                                                 e imponibile non riscosso servizi (l imposta non ce su non riscosso)*/

                                                /*corpo.totale_scontato_ivato;
                                                 department;
                                                 iva_rep[department];
                                                 totale_documento;
                                                 non_riscosso_servizi;*/

                                                let percentuale_incidenza = corpo.totale_scontato_ivato / totale_documento * 100;
                                                let totale_ivato_non_riscosso_servizi = percentuale_incidenza * non_riscosso_servizi / 100;
                                                let totale_ivato_riscosso = corpo.totale_scontato_ivato - totale_ivato_non_riscosso_servizi;

                                                let imponibile_riscosso = totale_ivato_riscosso / (iva_rep[department] / 100 + 1);
                                                let imposta_riscosso = totale_ivato_riscosso - imponibile_riscosso;
                                                let imponibile_non_riscosso_servizi = totale_ivato_non_riscosso_servizi / (iva_rep[department] / 100 + 1);

                                                let imponibile_reparto = imponibile_riscosso;
                                                let imposta_reparto = imposta_riscosso;
                                                let residuo = imponibile_non_riscosso_servizi;
                                                //let totale_reparto = totale_ivato_riscosso;



                                                testo_query = "UPDATE comanda SET imponibile_rep_" + department + "='" + imponibile_reparto + "', imposta_rep_" + department + "='" + imposta_reparto + "', residuo='" + residuo + "' where id='" + id_comanda + "' and prog_inser='" + where_progressivo_interno + "' and tipo_ricevuta='SCONTRINO' and (stato_record='CHIUSO' or stato_record='FORNO'or stato_record='ATTIVO');";
                                                comanda.sock.send({ tipo: "chiudi_scontrino", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                                                alasql(testo_query);

                                            } else {

                                                testo_query = "UPDATE comanda SET imponibile_rep_" + department + "=ROUND((totale_scontato_ivato / " + (iva_rep[department] / 100 + 1) + "), 8) where id='" + id_comanda + "' and prog_inser='" + where_progressivo_interno + "' and tipo_ricevuta='SCONTRINO' and (stato_record='CHIUSO' or stato_record='FORNO'or stato_record='ATTIVO')";
                                                comanda.sock.send({ tipo: "chiudi_scontrino", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                                                alasql(testo_query);


                                                testo_query = "UPDATE comanda SET imposta_rep_" + department + "=ROUND((totale_scontato_ivato-imponibile_rep_" + department + "),8) where id='" + id_comanda + "' and prog_inser='" + where_progressivo_interno + "' and tipo_ricevuta='SCONTRINO' and (stato_record='CHIUSO' or stato_record='FORNO'or stato_record='ATTIVO')";
                                                comanda.sock.send({ tipo: "chiudi_scontrino", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                                                alasql(testo_query);

                                            }

                                            testo_query = "UPDATE comanda SET importo_totale_fiscale=(imponibile_rep_1+imponibile_rep_2+imponibile_rep_3+imponibile_rep_4+imponibile_rep_5+imposta_rep_1+imposta_rep_2+imposta_rep_3+imposta_rep_4+imposta_rep_5);";
                                            comanda.sock.send({ tipo: "chiudi_scontrino", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                                            alasql(testo_query);
                                            if(corpo.rag_soc_cliente ===undefined || corpo.rag_soc_cliente ==="")
                                            {
                                                var numeretto = "";
                                                testo_query = "UPDATE comanda SET stato_record='CHIUSO',ora_consegna='" + ora + "'  ,rag_soc_cliente='0',nome_comanda='SCONTRINO DIRETTO' ,tipologia_fattorino='RITIRO',jolly='SUBITO' ,numeretto_asp='" + numeretto + "' where id='" + id_comanda + "';";
                                            comanda.sock.send({ tipo: "chiudi_scontrino", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                                            alasql(testo_query);
                                            $('#popup_scelta_cliente').modal('hide');

                                            }


                                        }

                                    });


                                    if (typeof(comanda.tavolo) === 'string' && comanda.tavolo.indexOf("CONTINUO") !== -1) {
                                        comanda.funzionidb.conto_attivo();
                                    }

                                    if (comanda.pizzeria_asporto === true) {
                                        righe_forno();
                                    }

                                    console.log("CHIUDI SCONTRINO 4");
                                    comanda.sincro.query_cassa();
                                    if (dati_testa.conto === '#prodotti_conto_separato_2_grande' || dati_testa.conto === '#prodotti_conto_separato_grande' || dati_testa.tavolo === "BANCO" || dati_testa.tavolo === "BAR" || dati_testa.tavolo.left(7) === "ASPORTO" || dati_testa.tavolo === "TAKE AWAY") {

                                        //LO TOLGO SENNO LO RIFA QUANDO HA STAMPATO E NON VOGLIO
                                        //TOLTO
                                        //btn_tavoli('scontrino');

                                        comanda.funzionidb.conto_attivo();
                                        setTimeout(function() {
                                            if ($('#conto tr').length === 0) {
                                                comanda.parcheggio = '';
                                                reset_progressivo_asporto();
                                                $('.nome_parcheggio').html(comanda.parcheggio);
                                            }
                                        }, 100);
                                    }

                                });
                            });
                        });
                    });
                });
            };
        } else if (tipo.toLowerCase() === "fattura") {

            var tipo = dati_testa.tipo;
            xml = '<printerCommand><directIO  command="4225" data="" /></printerCommand>';
            var epos = new epson.fiscalPrint();
            epos.send("http://" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=local_printer&timeout=30000", xml, 30000);
            epos.onreceive = function(result, tag_names_array, add_info, res_add) {

                var numero_chiusura = "";

                try {
                    numero_chiusura = aggZero(risposta_xml.getElementsByTagName("zRepNumber")[0].innerHTML, 4);
                } catch (e) {

                }

                var numero_fattura = (parseInt(add_info.responseData.substr(0, 4)) - 1);

                fattura_elettronica["testa"][0].C2114 = numero_fattura;

                invia_fattura_elettronica(fattura_elettronica["testa"], fattura_elettronica["corpo"], fattura_elettronica["totali"], fattura_elettronica["pagamento"]);

                ////console.log(result);

                var fiscalizzata_sn = "N";
                if (result.success === true && result.status === 2) {
                    fiscalizzata_sn = "S";
                }

                var data_fiscale = comanda.funzionidb.data_attuale().substr(0, 8);
                var ora_fiscale = comanda.funzionidb.data_attuale().substr(9, 8);
                dati_testa.zrep = numero_chiusura;
                dati_testa.numero_scontrino = numero_fattura;
                salva_record_testa(dati_testa, function() {

                    salva_consegna_scontrino(id_comanda);

                    if (comanda.pizzeria_asporto !== true) {
                        switch (posizione_conto) {

                            case "#prodotti_conto_separato_2_grande":
                                testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='FATTURA',netto='" + sconto_netto + "',sconto_perc='" + sconto_perc + "',sconto_imp='" + tipo_sconto + "',tipo_ricevuta='" + tipo.toUpperCase() + "',data_fisc='" + data_fiscale + "',/*data_stampa='" + data_fiscale + "',ora_stampa='" + ora_fiscale + "',*/ora_fisc='" + ora_fiscale + "',fiscalizzata_sn='" + fiscalizzata_sn + "',progressivo_fiscale='" + numero_fattura + "',stato_record='CHIUSO',n_fiscale='" + numero_fattura + "' WHERE progressivo_fiscale='' and numero_conto='2'  and id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO'  and ntav_comanda='" + dati_testa.tavolo + "' and desc_art!='EXTRA';";
                                break;
                            case "#prodotti_conto_separato_grande":
                                testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='FATTURA',netto='" + sconto_netto + "',sconto_perc='" + sconto_perc + "',sconto_imp='" + tipo_sconto + "',tipo_ricevuta='" + tipo.toUpperCase() + "',data_fisc='" + data_fiscale + "',/*data_stampa='" + data_fiscale + "',ora_stampa='" + ora_fiscale + "',*/ora_fisc='" + ora_fiscale + "',fiscalizzata_sn='" + fiscalizzata_sn + "',progressivo_fiscale='" + numero_fattura + "',stato_record='CHIUSO',n_fiscale='" + numero_fattura + "'  WHERE progressivo_fiscale='' and (numero_conto='1' or numero_conto is null or numero_conto='null') and id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO'  and ntav_comanda='" + dati_testa.tavolo + "' and desc_art!='EXTRA';";
                                break;
                                //#conto
                            default:
                                testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='FATTURA',netto='" + sconto_netto + "',sconto_perc='" + sconto_perc + "',sconto_imp='" + tipo_sconto + "',tipo_ricevuta='" + tipo.toUpperCase() + "',data_fisc='" + data_fiscale + "',/*data_stampa='" + data_fiscale + "',ora_stampa='" + ora_fiscale + "',*/ora_fisc='" + ora_fiscale + "',fiscalizzata_sn='" + fiscalizzata_sn + "',progressivo_fiscale='" + numero_fattura + "',stato_record='CHIUSO',n_fiscale='" + numero_fattura + "' WHERE progressivo_fiscale='' and id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO' and ntav_comanda='" + dati_testa.tavolo + "' and desc_art!='EXTRA';";
                                break;
                        }
                    } else {
                        switch (posizione_conto) {

                            case "#prodotti_conto_separato_2_grande":
                                testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='FATTURA',netto='" + sconto_netto + "',sconto_perc='" + sconto_perc + "',sconto_imp='" + tipo_sconto + "',tipo_ricevuta='" + tipo.toUpperCase() + "',data_fisc='" + data_fiscale + "',/*data_stampa='" + data_fiscale + "',ora_stampa='" + ora_fiscale + "',*/ora_fisc='" + ora_fiscale + "',fiscalizzata_sn='" + fiscalizzata_sn + "',progressivo_fiscale='" + numero_fattura + "',n_fiscale='" + numero_fattura + "' WHERE progressivo_fiscale='' and numero_conto='2'  and id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO'  and ntav_comanda='" + dati_testa.tavolo + "' and desc_art!='EXTRA';";
                                break;
                            case "#prodotti_conto_separato_grande":
                                testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='FATTURA',netto='" + sconto_netto + "',sconto_perc='" + sconto_perc + "',sconto_imp='" + tipo_sconto + "',tipo_ricevuta='" + tipo.toUpperCase() + "',data_fisc='" + data_fiscale + "',/*data_stampa='" + data_fiscale + "',ora_stampa='" + ora_fiscale + "',*/ora_fisc='" + ora_fiscale + "',fiscalizzata_sn='" + fiscalizzata_sn + "',progressivo_fiscale='" + numero_fattura + "',n_fiscale='" + numero_fattura + "'  WHERE progressivo_fiscale='' and (numero_conto='1' or numero_conto is null or numero_conto='null') and id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO'  and ntav_comanda='" + dati_testa.tavolo + "' and desc_art!='EXTRA';";
                                break;
                                //#conto
                            default:
                                testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='FATTURA',netto='" + sconto_netto + "',sconto_perc='" + sconto_perc + "',sconto_imp='" + tipo_sconto + "',tipo_ricevuta='" + tipo.toUpperCase() + "',data_fisc='" + data_fiscale + "',/*data_stampa='" + data_fiscale + "',ora_stampa='" + ora_fiscale + "',*/ora_fisc='" + ora_fiscale + "',fiscalizzata_sn='" + fiscalizzata_sn + "',progressivo_fiscale='" + numero_fattura + "',n_fiscale='" + numero_fattura + "' WHERE progressivo_fiscale='' and  id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO' and ntav_comanda='" + dati_testa.tavolo + "' and desc_art!='EXTRA';";
                                break;
                        }
                    }
                    /* } else {
                             switch (posizione_conto) {
     
                                 case "#prodotti_conto_separato_2_grande":
                                     testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='FATTURA',tipo_ricevuta='" + tipo.toUpperCase() + "',data_fisc='" + data_fiscale + "',/*data_stampa='" + data_fiscale + "',ora_stampa='" + ora_fiscale + "',ora_fisc='" + ora_fiscale + "',fiscalizzata_sn='" + fiscalizzata_sn + "',progressivo_fiscale='" + numero_fattura + "',n_fiscale='" + numero_fattura + "' WHERE numero_conto='2'  and id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO'  and ntav_comanda='" + dati_testa.tavolo + "' and desc_art!='EXTRA';";
                                     break;
                                 case "#prodotti_conto_separato_grande":
                                     testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='FATTURA',tipo_ricevuta='" + tipo.toUpperCase() + "',data_fisc='" + data_fiscale + "',/*data_stampa='" + data_fiscale + "',ora_stampa='" + ora_fiscale + "',ora_fisc='" + ora_fiscale + "',fiscalizzata_sn='" + fiscalizzata_sn + "',progressivo_fiscale='" + numero_fattura + "',n_fiscale='" + numero_fattura + "'  WHERE (numero_conto='1' or numero_conto is null or numero_conto='null') and id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO'  and ntav_comanda='" + dati_testa.tavolo + "' and desc_art!='EXTRA';";
                                     break;
                                     //#conto
                                 default:
                                     testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='FATTURA',tipo_ricevuta='" + tipo.toUpperCase() + "',data_fisc='" + data_fiscale + "',/*data_stampa='" + data_fiscale + "',ora_stampa='" + ora_fiscale + "',ora_fisc='" + ora_fiscale + "',fiscalizzata_sn='" + fiscalizzata_sn + "',progressivo_fiscale='" + numero_fattura + "',n_fiscale='" + numero_fattura + "' WHERE id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO' and ntav_comanda='" + dati_testa.tavolo + "' and desc_art!='EXTRA';";
                                     break;
                             }
                         }*/
                    console.log("GESTIONE SCONTRINO STAMPA CHIUSA", posizione_conto, testo_query);
                    comanda.sock.send({ tipo: "chiudi_scontrino", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                    comanda.sincro.query(testo_query, function() {

                        testo_query = "UPDATE comanda SET fiscale_sospeso='',tipo_ricevuta='FATTURA',tipo_ricevuta='" + tipo.toUpperCase() + "',data_fisc='" + data_fiscale + "',/*data_stampa='" + data_fiscale + "',ora_stampa='" + ora_fiscale + "',*/ora_fisc='" + ora_fiscale + "',fiscalizzata_sn='" + fiscalizzata_sn + "',progressivo_fiscale='" + numero_fattura + "',stato_record='CHIUSO',n_fiscale='" + numero_fattura + "' WHERE progressivo_fiscale='' and id='" + id_comanda + "' and fiscale_sospeso='STAMPAFISCALESOSPESO' and ntav_comanda='" + dati_testa.tavolo + "' and desc_art='EXTRA';";
                        comanda.sock.send({ tipo: "chiudi_scontrino", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                        comanda.sincro.query(testo_query, function() {

                            testo_query = "UPDATE comanda SET totale_scontato_ivato =ROUND(((" + prezzo_vero + "*quantita)-((prezzo_un*quantita)*sconto_perc/100)),2)  where (sconto_perc!='' and sconto_perc!='0' and sconto_perc!='0.00');";
                            comanda.sock.send({ tipo: "chiudi_scontrino", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                            comanda.sincro.query(testo_query, function() {

                                testo_query = "UPDATE comanda SET totale_scontato_ivato =ROUND(((" + prezzo_vero + "*quantita)),2)  where (sconto_perc='' or sconto_perc='0' or sconto_perc='0.00');";
                                comanda.sock.send({ tipo: "chiudi_scontrino", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                                comanda.sincro.query(testo_query, function() {

                                    let iva_rep = new Object();

                                    for (let department = 1; department <= 5; department++) {


                                        iva_rep[department] = parseInt(REPARTO_controller.iva_reparto(parseInt(REPARTO_controller.indice(parseInt(department)))));

                                        if (isNaN(iva_rep[department])) {
                                            iva_rep[department] = 0;
                                        }

                                        testo_query = "UPDATE comanda SET imponibile_rep_" + department + "=ROUND((totale_scontato_ivato/" + (iva_rep[department] / 100 + 1) + "),8)  where reparto='" + department + "'";
                                        comanda.sock.send({ tipo: "chiudi_scontrino", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                                        alasql(testo_query);

                                        testo_query = "UPDATE comanda SET imposta_rep_" + department + "=ROUND((totale_scontato_ivato-imponibile_rep_" + department + "),8) where reparto='" + department + "';";
                                        comanda.sock.send({ tipo: "chiudi_scontrino", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                                        alasql(testo_query);

                                        testo_query = "UPDATE comanda SET importo_totale_fiscale=(imponibile_rep_1+imponibile_rep_2+imponibile_rep_3+imponibile_rep_4+imponibile_rep_5+imposta_rep_1+imposta_rep_2+imposta_rep_3+imposta_rep_4+imposta_rep_5);";
                                        comanda.sock.send({ tipo: "chiudi_scontrino", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                                        alasql(testo_query);

                                    }

                                    if (comanda.tavolo !== 0 && comanda.tavolo.indexOf("CONTINUO") !== -1) {
                                        comanda.funzionidb.conto_attivo();
                                    }

                                    if (comanda.pizzeria_asporto === true) {
                                        righe_forno();
                                    }

                                    comanda.sincro.query_cassa();
                                    if (comanda.conto === '#prodotti_conto_separato_2_grande' || comanda.conto === '#prodotti_conto_separato_grande' || comanda.tavolo === "BANCO" || comanda.tavolo === "BAR" || comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === "TAKE AWAY") {
                                        //TOLTO1
                                        //btn_tavoli('fattura');
                                        comanda.funzionidb.conto_attivo();
                                        setTimeout(function() {
                                            if ($('#conto tr').length === 0) {
                                                comanda.parcheggio = '';
                                                reset_progressivo_asporto();
                                                $('.nome_parcheggio').html(comanda.parcheggio);
                                            }
                                        }, 100);
                                    }

                                });
                            });
                        });
                    });
                });
            };
        }
    };

    function salva_consegna_scontrino(id_comanda) {

        $('#tipo_consegna_modal').html("");
        $('#tipo_consegna_modal_tipo').html("" + "");
        $('#tipo_consegna_modal_gap_scritta').html("" + "");
        let queryVerificaCostiConsegnaPrecedenti = "select consegna_costo from consegna_tempo where id='" + id_comanda + "' limit 1;";

        let consegnePrecedenti = alasql(queryVerificaCostiConsegnaPrecedenti);

        let totale_consegna = 0;

        let queryRicalcoloConsegnaAttuale = "select sum(cast(QTA as float)) as totale_consegne from comanda where id='" + id_comanda + "' and QTA!=''";
        let ricalcoloConsegnaAttualealasql = alasql(queryRicalcoloConsegnaAttuale)[0].totale_consegne;

        if (consegnePrecedenti.length > 0) {

            let update_query_tipo_consegna = 'update consegna_tempo SET consegna_costo= "' + ricalcoloConsegnaAttualealasql.toFixed(2) + '"   where id="' + id_comanda + '"';
            comanda.sock.send({ tipo: "aggiornamento", operatore: comanda.operatore, query: update_query_tipo_consegna, terminale: comanda.terminale, ip: comanda.ip_address });
            alasql(update_query_tipo_consegna);

        } else {

            let update_query_tipo_consegna = 'insert into consegna_tempo (id,consegna_costo) VALUES ("' + id_comanda + '","' + ricalcoloConsegnaAttualealasql.toFixed(2) + '");';
            comanda.sock.send({ tipo: "aggiornamento", operatore: comanda.operatore, query: update_query_tipo_consegna, terminale: comanda.terminale, ip: comanda.ip_address });
            alasql(update_query_tipo_consegna);

        }

    }



    this.dati_categoria = function(callBack) {

        var cb;
        callBack(cb);
        return true;
    };
    this.dati_operatore = function(callBack) {

        var cb;
        callBack(cb);
        return true;
    };
    this.elenco_comande_aperte = function(tasto) {
        var testo_query = "SELECT * FROM comanda WHERE ntav_comanda='" + comanda.tavolo + "' and (stato_record='APERTO' OR stato_record='ATTIVO') ORDER BY nome_comanda ASC;";



        var data = [];
        var prom = new Promise(function(resolve, reject) {

            comanda.sincro.query(testo_query, function(array) {

                var grouped = new Object();
                var array_2 = new Array();

                array.forEach(function(e) {
                    if (grouped[e.id] === undefined) {
                        grouped[e.id] = e;
                        array_2.push(e);
                    }
                });

                //DARE LA CONDIZIONE CHE ESISTA IL PRODOTTO
                resolve(array_2);
            });
        });
        data.push(prom);
        Promise.all(data).then(function(data) {

            var output = '';
            var nome_comanda = '';
            ////console.log(data[0]);

            console.log("DATA ELENCO COMANDE APERTE", data);
            data[0].forEach(function(ar) {
                if (ar.nome_comanda !== undefined) {
                    var ora = ar.id.substr(12, 2);
                    var minuti = ar.id.substr(14, 2);
                    var data = ora + ':' + minuti;
                    nome_comanda = ar.nome_comanda;
                    if (comanda.tavolo === 'BAR') {
                        output += '<a style="cursor:pointer;" ' + comanda.evento + '="riapri_comanda(\'' + ar.id + '\',\'' + nome_comanda + '\');" class="list-group-item">' + nome_comanda + ' - ' + data + '</a>';
                    } else {
                        output += '<a style="cursor:pointer;" ' + comanda.evento + '="riapri_comanda(\'' + ar.id + '\',\'' + nome_comanda + '\');" class="list-group-item">' + nome_comanda + ' - ' + data + '<span style="float:right">' + ar.ora_ + '</span></a>';
                    }
                } else {
                    var ora = ar.id.substr(12, 2);
                    var minuti = ar.id.substr(14, 2);
                    var data = ora + ':' + minuti;
                    nome_comanda = ar.id;
                    if (comanda.tavolo === 'BAR') {
                        output += '<a style="cursor:pointer;" ' + comanda.evento + '="riapri_comanda(\'' + ar.id + '\',\'' + nome_comanda + '\');" class="list-group-item">' + nome_comanda + ' - ' + data + '</a>';
                    } else {
                        output += '<a style="cursor:pointer;" ' + comanda.evento + '="riapri_comanda(\'' + ar.id + '\',\'' + nome_comanda + '\');" class="list-group-item">' + nome_comanda + ' - ' + data + '<span style="float:right">' + ar.ora_ + '</span></a>';
                    }
                }



            });

            $('#riprendi_comanda > div > div > div.modal-body > div').html(output);

            //Si apre il popup solo se ci sono comande aperte
            if ($('#riprendi_comanda > div > div > div.modal-body > div').html().length > 0) {
                $('#riprendi_comanda').modal('show');
            } else if (tasto === "LEGGI") {
                bootbox.alert("NON HAI COMANDE PARCHEGGIATE.");
            }

        });
    };
    this.elenco_comande_aperte_unisci_parcheggio = function() {
        $('#popup_unisci_parcheggio').modal('show');
        var testo_query = "SELECT * FROM comanda WHERE stato_record='APERTO' and ntav_comanda='BAR' ORDER BY nome_comanda ASC;";

        var data = [];
        var prom = new Promise(function(resolve, reject) {

            comanda.sincro.query(testo_query, function(array) {


                var grouped = new Object();
                var array_2 = new Array();

                array.forEach(function(e) {
                    if (grouped[e.id] === undefined) {
                        grouped[e.id] = e;
                        array_2.push(e);
                    }
                });

                //DARE LA CONDIZIONE CHE ESISTA IL PRODOTTO
                resolve(array_2);

            });
        });
        data.push(prom);
        Promise.all(data).then(function(data) {

            var output = '';
            var nome_comanda = '';
            ////console.log(data[0]);
            output += '<button class="btn btn-info" ' + comanda.evento + '="popup_spostamento_tavolo_parcheggio()">' + comanda.lang_stampa[142].toUpperCase() + '</button><br/><br/>';
            data[0].forEach(function(ar) {
                if (ar.nome_comanda !== undefined) {
                    var ora = ar.id.substr(12, 2);
                    var minuti = ar.id.substr(14, 2);
                    var data = ora + ':' + minuti;
                    nome_comanda = ar.nome_comanda;
                    output += '<a style="cursor:pointer;" ' + comanda.evento + '="unisci_parcheggio(\'' + ar.id + '\',\'' + nome_comanda + '\');" class="list-group-item">' + nome_comanda + ' - ' + data + '</a>';

                } else if (ar.id !== undefined) {
                    var ora = ar.id.substr(12, 2);
                    var minuti = ar.id.substr(14, 2);
                    var data = ora + ':' + minuti;
                    nome_comanda = ar.id;
                    output += '<a style="cursor:pointer;" ' + comanda.evento + '="unisci_parcheggio(\'' + ar.id + '\',\'' + nome_comanda + '\');" class="list-group-item">' + nome_comanda + ' - ' + data + '</a>';

                }

            });
            $('#popup_unisci_parcheggio > div > div > div.modal-body > div').html(output);
        });
    };
    this.elenco_comande_aperte_unisci_parcheggio_BAR = function() {
        $('#popup_unisci_parcheggio').modal('show');
        var testo_query = "SELECT * FROM comanda WHERE stato_record='APERTO' and ntav_comanda='BAR' ORDER BY nome_comanda ASC;";
        //group by id

        var data = [];
        var prom = new Promise(function(resolve, reject) {

            comanda.sincro.query(testo_query, function(array) {


                var grouped = new Object();
                var array_2 = new Array();

                array.forEach(function(e) {
                    if (grouped[e.id] === undefined) {
                        grouped[e.id] = e;
                        array_2.push(e);
                    }
                });

                //DARE LA CONDIZIONE CHE ESISTA IL PRODOTTO
                resolve(array_2);

            });
        });
        data.push(prom);
        Promise.all(data).then(function(data) {

            var output = '';
            var nome_comanda = '';
            ////console.log(data[0]);

            output += '<button class="btn btn-info" ' + comanda.evento + '="popup_spostamento_articolo_parcheggio()">' + comanda.lang_stampa[142].toUpperCase() + '</button><br/><br/>';
            data[0].forEach(function(ar) {
                if (ar.nome_comanda !== undefined) {
                    nome_comanda = ar.nome_comanda;
                    output += '<a style="cursor:pointer;" ' + comanda.evento + '="sposta_articolo_BAR(\'' + ar.id + '\',\'' + nome_comanda + '\');" class="list-group-item">' + nome_comanda + ' - Banco</a>';

                } else {
                    nome_comanda = ar.id;
                    output += '<a style="cursor:pointer;" ' + comanda.evento + '="sposta_articolo_BAR(\'' + ar.id + '\',\'' + nome_comanda + '\');" class="list-group-item">' + nome_comanda + ' - Banco</a>';

                }

            });
            $('#popup_unisci_parcheggio > div > div > div.modal-body > div').html(output);
        });
    };


    this.elenco_comande_aperte_unisci_parcheggio_TAVOLO = function() {
        $('#popup_unisci_parcheggio').modal('show');
        var testo_query = "SELECT * FROM comanda WHERE stato_record='APERTO' and ntav_comanda='BAR' ORDER BY nome_comanda ASC;";

        var data = [];
        var prom = new Promise(function(resolve, reject) {

            comanda.sincro.query(testo_query, function(array) {


                var grouped = new Object();
                var array_2 = new Array();

                array.forEach(function(e) {
                    if (grouped[e.id] === undefined) {
                        grouped[e.id] = e;
                        array_2.push(e);
                    }
                });

                //DARE LA CONDIZIONE CHE ESISTA IL PRODOTTO
                resolve(array_2);

            });
        });
        data.push(prom);
        Promise.all(data).then(function(data) {

            var output = '';
            var nome_comanda = '';
            ////console.log(data[0]);

            output += '<button class="btn btn-info" ' + comanda.evento + '="popup_spostamento_articolo_parcheggio()">' + comanda.lang_stampa[142].toUpperCase() + '</button><br/><br/>';
            data[0].forEach(function(ar) {
                if (ar.nome_comanda !== undefined) {
                    nome_comanda = ar.nome_comanda;
                    output += '<a style="cursor:pointer;" ' + comanda.evento + '="sposta_articolo_BAR(\'' + ar.id + '\',\'' + nome_comanda + '\');" class="list-group-item">' + nome_comanda + ' - Banco</a>';

                } else {
                    nome_comanda = ar.id;
                    output += '<a style="cursor:pointer;" ' + comanda.evento + '="sposta_articolo_BAR(\'' + ar.id + '\',\'' + nome_comanda + '\');" class="list-group-item">' + nome_comanda + ' - Banco</a>';

                }

            });
            $('#popup_unisci_parcheggio > div > div > div.modal-body > div').html(output);
        });
    };

    this.elenco_movimenti = function(callBack) {

        var cb;
        callBack(cb);
        return true;
    };
    this.elimina_articolo = function(id) {
        var frase = comanda.lang[118].replace('*', "'" + $('.tab_conto tr#art_' + id + '>td:nth-child(2)').html() + "'");
        var r = confirm(frase); //Sei sicuro di voler cancellare l'articolo?
        if (r === true) {

            var articolo = $('.tab_conto tr#art_' + id + ' td:nth-child(2)').html();
            if (articolo[0] === "+" || articolo[0] === "-" || articolo[0] === "(" || articolo[0] === "*" || articolo[0] === "x" || articolo[0] === "=") {
                var row = $('.tab_conto tr#art_' + id).closest('tr');
                row.prev().remove();
            }

            $('.tab_conto tr#art_' + id).remove();
            var testo_query = "DELETE FROM comanda WHERE  ntav_comanda='" + comanda.tavolo + "' and stato_record='ATTIVO' AND prog_inser='" + id + "' AND operatore='" + comanda.operatore + "';";
            //console.log(testo_query);
            comanda.sock.send({ tipo: "elimina_articolo", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
            comanda.sincro.query(testo_query, function() {
                calcola_totale();
            });
        }
    };
    this.elimina_da_tabella = function(callBack) {

        var cb;
        callBack(cb);
        return true;
    };
    this.modifica_articolo = function(callBack) {

        var cb;
        callBack(cb);
        return true;
    };
    this.preleva_scontrino = function(callBack) {

        var cb;
        callBack(cb);
        return true;
    };
    this.riapri_comanda = function(id, nome_parcheggio) {

        var testo_query = "UPDATE comanda SET stato_record='APERTO' WHERE stato_record='ATTIVO' and ntav_comanda='" + comanda.tavolo + "';";
        comanda.sock.send({ tipo: "aggiorna_tavolo", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
        comanda.sincro.query(testo_query, function() {

            testo_query = "UPDATE comanda SET stato_record='ATTIVO' WHERE stato_record='APERTO' and id='" + id + "' and ntav_comanda='" + comanda.tavolo + "';";
            comanda.sock.send({ tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
            comanda.sincro.query(testo_query, function() {
                comanda.funzionidb.conto_attivo();
            });
        });
    };
    this.riempi_form_fattura = function(callBack) {

        var cb;
        callBack(cb);
        return true;
    };
    this.salva_articolo = function(form, callBack) {

        var arr = {};
        parse_str(form.replace(/%/, '%25'), arr);
        //console.log(arr);

        var testo_query = "UPDATE prodotti SET posizione=null, pagina=null WHERE categoria!='XXX' and categoria='" + comanda.categoria + "' AND pagina='" + comanda.pagina + "' AND posizione='" + arr.posizione + "';";
        comanda.sock.send({ tipo: "parcheggio", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
        comanda.sincro.query(testo_query, function() {
            //console.log("query1");

            testo_query = "UPDATE prodotti SET posizione='" + arr.posizione + "',pagina='" + comanda.pagina + "',colore_tasto='" + $('[name="colore_tasto"]').val() + "' WHERE categoria!='XXX' and  id='" + arr.id_articolo + "';";
            comanda.sock.send({ tipo: "parcheggio", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
            comanda.sincro.query(testo_query, function() {
                //console.log("query2");
                comanda.funzionidb.elenco_prodotti(function(output) {
                    //$('.elenco_prodotti').html(output);
                    //console.log("nuovi prodotti creati");
                    callBack(true);
                });
            });
        });
        return true;
    };

    this.salva_comanda = function(nome_comanda, eventuale_tavolo) {

        var id_comanda_attuale_caso_asporto_su_risto = id_comanda_attuale_alasql();
        //MODIFICA CATEGORIA E LA CLICCA
        mod_categoria(comanda.categoria_predefinita, '', '1');
        $("#tab_sx a").removeClass("cat_accesa");
        $('#tab_sx a.cat_' + comanda.categoria_predefinita).addClass("cat_accesa");
        //---//

        nome_comanda = nome_comanda.toUpperCase();


        var testo_query = "SELECT nome_comanda FROM comanda WHERE stato_record='APERTO' AND nome_comanda='" + nome_comanda + "';";
        comanda.sincro.query(testo_query, function(result) {

            console.log("SELECT COMANDA", result.length);
            if ($('#nome_nuovo_parcheggio').val().trim() === nome_comanda && result.length > 0) {
                alert("Esiste gia\' un parcheggio con questo nome.");
            } else {
                $('#nome_nuovo_parcheggio').val("");
                $('#prodotti', 'body #creazione_comanda').css('opacity', '0.5');
                $('#prodotti', 'body #creazione_comanda').css('pointer-events', 'none');
                $('#tasti_fiscali_da_nascondere', 'body #creazione_comanda').css('pointer-events', 'none');
                $('#tasti_fiscali_da_nascondere', 'body #creazione_comanda').css('opacity', '0.5');



                $('#parcheggia_comanda').modal('hide');

                if (eventuale_tavolo.left(7) === "ASPORTO" || eventuale_tavolo === "TAKEAWAY" || eventuale_tavolo === "TAKE AWAY") {

                    var ora_consegna = $('#parcheggia_comanda [name="ora_consegna"]').val();
                    var minuti_consegna = $('#parcheggia_comanda [name="minuti_consegna"]').val();

                    if (!isNaN(ora_consegna) && ora_consegna.length > 0) {

                        if (minuti_consegna === "") {
                            minuti_consegna = "00";
                        }
                        //SE L'ORA DI CONSEGNA E' STATA MODIFICATA STAMPA UNA COMANDA SPECIALE
                        if ((comanda.parcheggio !== 0 && comanda.parcheggio !== '') && (comanda.ora_consegna !== ora_consegna || comanda.minuti_consegna !== minuti_consegna)) {
                            stampa_comanda(true, true, true);
                        } else if (comanda.stampato === false) {
                            comanda.parcheggio = nome_comanda.toUpperCase();
                            stampa_comanda(true);
                        }

                        ora_consegna = aggZero(ora_consegna, 2) + ':' + aggZero(minuti_consegna, 2);

                        $('#popup_scelta_cliente input[name="ora_consegna"]').val(ora_consegna);
                    } else {
                        comanda.parcheggio = nome_comanda.toUpperCase();
                        /*modificato 11/09/2019*/

                        var set = alasql("select Field420  from settaggi_profili where id=" + comanda.folder_number + " ;");

                        if (comanda.provenienza_conto === true) {
                            if (set[0].Field420 === "true") {
                                stampa_comanda(true);
                            } else {
                                //Stampa solo conto quindi non fa un cazzo
                            }
                        } else {
                            stampa_comanda(true);
                        }


                        /*if (comanda.conto_in_attesa !== true && set[0].Field420 === "true") {
                         stampa_comanda(true);
                         }*/
                    }

                    //aggiunto il 19-12-2016
                } else if (eventuale_tavolo.indexOf("CONTINUO") !== -1 || eventuale_tavolo === "BAR") {
                    comanda.stampato = false;
                    comanda.parcheggio = nome_comanda.toUpperCase();
                    stampa_comanda(true, nome_comanda);
                }

                /*if (comanda.stampato === false) {
                 
                 comanda.parcheggio = nome_comanda.toUpperCase();
                 stampa_comanda(true);
                 }*/



                if (comanda.conto_in_attesa === true) {
                    comanda.conto_in_attesa = false;
                    comanda.parcheggio = nome_comanda.toUpperCase();
                    preconto();
                }

                setTimeout(function() {


                    $('#conto > tr').remove();
                    $('.ultime_battiture tr').remove();
                    $('#numero_coperti').val('0');
                    $('#intestazioni_conto').html('');
                    comanda.parcheggio = '';
                    reset_progressivo_asporto();
                    comanda.ora_consegna = '';
                    comanda.minuti_consegna = '';
                    $('.nome_parcheggio').html(comanda.parcheggio);
                    calcola_totale();
                    if (comanda.tasto_indietro_parcheggio === true && comanda.parcheggio_torna_tavoli === true) {
                        btn_tavoli();
                    }

                    console.log("COMANDA2");
                    if (eventuale_tavolo.indexOf("CONTINUO") === -1 && eventuale_tavolo.left(7) !== "ASPORTO" && eventuale_tavolo !== "TAKEAWAY" && eventuale_tavolo !== "TAKE AWAY") {
                        testo_query = "UPDATE comanda SET  stampata_sn='S',stato_record='APERTO', nome_comanda='" + nome_comanda + "' WHERE stato_record='ATTIVO' and ntav_comanda='" + eventuale_tavolo + "';";
                    } else {
                        if (comanda.pizzeria_asporto !== true && eventuale_tavolo.left(7) === "ASPORTO") {
                            testo_query = "UPDATE comanda SET stato_record='FORNO', nome_comanda='" + nome_comanda + "',ora_consegna='" + ora_consegna + "',ora_='" + ora_consegna + "' WHERE id='" + id_comanda_attuale_caso_asporto_su_risto + "' and ntav_comanda='" + eventuale_tavolo + "';";
                        } else
                        if (eventuale_tavolo.indexOf("CONTINUO") === -1) {
                            testo_query = "UPDATE comanda SET stato_record='APERTO', nome_comanda='" + nome_comanda + "',ora_='" + ora_consegna + "' WHERE stato_record='ATTIVO' and ntav_comanda='" + eventuale_tavolo + "';";
                        } else {
                            testo_query = "UPDATE comanda SET stato_record='APERTO', nome_comanda='" + nome_comanda + "' WHERE stato_record='ATTIVO' and ntav_comanda='" + eventuale_tavolo + "';";
                        }
                    }

                    comanda.sock.send({ tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });

                    comanda.sincro.query(testo_query, function() {

                        $('#prodotti', 'body #creazione_comanda').css('opacity', '');
                        $('#prodotti', 'body #creazione_comanda').css('pointer-events', '');
                        $('#tasti_fiscali_da_nascondere', 'body #creazione_comanda').css('pointer-events', '');
                        $('#tasti_fiscali_da_nascondere', 'body #creazione_comanda').css('opacity', '');


                        if (socket_fast_split !== undefined && socket_fast_split.readyState !== undefined && socket_fast_split.readyState === 1) {

                            socket_fast_split.send(JSON.stringify({ parcheggio: $(comanda.totale_scontrino).html() }));
                        }

                        if (comanda.licenza === 'mattia131' && typeof(comanda.tavolo) === "string" && comanda.tavolo !== "BAR" && comanda.tavolo.indexOf("CONTINUO") === -1 && comanda.mobile !== true) {

                            var testo_query = "update tavoli set occupato='0' where numero='" + eventuale_tavolo + "' ; ";

                            comanda.sincro.query(testo_query, function() {

                                //SE FACCIO SELEZIONE OPERATORE NON MI COLORA IL TAVOLO ALLA PRIMA VOLTA
                                btn_tavoli();
                                //selezione_operatore('MAPPA TAVOLI');
                            });

                        }

                    });
                }, 1500);
            }
        });
    };
    this.salva_mod_tab = function(callBack) {

        var cb;
        callBack(cb);
        return true;
    };
    this.select_elenco_clienti = function(callBack) {

        var cb;
        callBack(cb);
        return true;
    };
    this.tabella_categorie = function(callBack) {

        var cb;
        callBack(cb);
        return true;
    };
    this.tabella_clienti = function(callBack) {

        var cb;
        callBack(cb);
        return true;
    };
    this.tabella_impostazioni_fiscali = function(callBack) {

        var cb;
        callBack(cb);
        return true;
    };
    this.tabella_prodotti = function(callBack) {

        var cb;
        callBack(cb);
        return true;
    };
    this.tabella_sconti = function(callBack) {

        var cb;
        callBack(cb);
        return true;
    };
    this.tabella_operatori = function(callBack) {

        var cb;
        callBack(cb);
        return true;
    };
    //-----FINE FUNZIONI DINAMICHE-----//
};

function parse_str(str, array) {
    //       discuss at: http://phpjs.org/functions/parse_str/
    //      original by: Cagri Ekin
    //      improved by: Michael White (http://getsprink.com)
    //      improved by: Jack
    //      improved by: Brett Zamir (http://brett-zamir.me)
    //      bugfixed by: Onno Marsman
    //      bugfixed by: Brett Zamir (http://brett-zamir.me)
    //      bugfixed by: stag019
    //      bugfixed by: Brett Zamir (http://brett-zamir.me)
    //      bugfixed by: MIO_KODUKI (http://mio-koduki.blogspot.com/)
    // reimplemented by: stag019
    //         input by: Dreamer
    //         input by: Zaide (http://zaidesthings.com/)
    //         input by: David Pesta (http://davidpesta.com/)
    //         input by: jeicquest
    //             note: When no argument is specified, will put variables in global scope.
    //             note: When a particular argument has been passed, and the returned value is different parse_str of PHP. For example, a=b=c&d====c
    //             test: skip
    //        example 1: var arr = {};
    //        example 1: parse_str('first=foo&second=bar', arr);
    //        example 1: $result = arr
    //        returns 1: { first: 'foo', second: 'bar' }
    //        example 2: var arr = {};
    //        example 2: parse_str('str_a=Jack+and+Jill+didn%27t+see+the+well.', arr);
    //        example 2: $result = arr
    //        returns 2: { str_a: "Jack and Jill didn't see the well." }
    //        example 3: var abc = {3:'a'};
    //        example 3: parse_str('abc[a][b]["c"]=def&abc[q]=t+5');
    //        returns 3: {"3":"a","a":{"b":{"c":"def"}},"q":"t 5"}

    var strArr = String(str)
        .replace(/^&/, '')
        .replace(/&$/, '')
        .split('&'),
        sal = strArr.length,
        i, j, ct, p, lastObj, obj, lastIter, undef, chr, tmp, key, value, postLeftBracketPos, keys, keysLen,
        fixStr = function(str) {
            //MODIFICATO 02/11/2016
            //In console va (15-11-2018)
            return decodeURIComponent(str.replace(/\+/g, '%20+'));
        };
    if (!array) {
        array = this.window;
    }

    for (i = 0; i < sal; i++) {
        tmp = strArr[i].split('=');
        key = fixStr(tmp[0]);
        value = (tmp.length < 2) ? '' : fixStr(tmp[1]);
        while (key.charAt(0) === ' ') {
            key = key.slice(1);
        }
        if (key.indexOf('\x00') > -1) {
            key = key.slice(0, key.indexOf('\x00'));
        }
        if (key && key.charAt(0) !== '[') {
            keys = [];
            postLeftBracketPos = 0;
            for (j = 0; j < key.length; j++) {
                if (key.charAt(j) === '[' && !postLeftBracketPos) {
                    postLeftBracketPos = j + 1;
                } else if (key.charAt(j) === ']') {
                    if (postLeftBracketPos) {
                        if (!keys.length) {
                            keys.push(key.slice(0, postLeftBracketPos - 1));
                        }
                        keys.push(key.substr(postLeftBracketPos, j - postLeftBracketPos));
                        postLeftBracketPos = 0;
                        if (key.charAt(j + 1) !== '[') {
                            break;
                        }
                    }
                }
            }
            if (!keys.length) {
                keys = [key];
            }
            for (j = 0; j < keys[0].length; j++) {
                chr = keys[0].charAt(j);
                if (chr === ' ' || chr === '.' || chr === '[') {
                    keys[0] = keys[0].substr(0, j) + '_' + keys[0].substr(j + 1);
                }
                if (chr === '[') {
                    break;
                }
            }

            obj = array;
            for (j = 0, keysLen = keys.length; j < keysLen; j++) {
                key = keys[j].replace(/^['"]/, '')
                    .replace(/['"]$/, '');
                lastIter = j !== keys.length - 1;
                lastObj = obj;
                if ((key !== '' && key !== ' ') || j === 0) {
                    if (obj[key] === undef) {
                        obj[key] = {};
                    }
                    obj = obj[key];
                } else { // To insert new dimension
                    ct = -1;
                    for (p in obj) {
                        if (obj.hasOwnProperty(p)) {
                            if (+p > ct && p.match(/^\d+$/g)) {
                                ct = +p;
                            }
                        }
                    }
                    key = ct + 1;
                }
            }
            lastObj[key] = value;
        }
    }
}


function aggZero(x, n) {
    while (x.toString().length < n) {
        x = "0" + x;
    }
    return x;
}

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};