/* global riordina, parseFloat, comanda */


function statistiche_articolo() {
    comanda.array_db_centri = new Array();

    let dati_centri_statistica = alasql("select * from centri_statistica");



    let url_server = comanda.url_server + 'classi_php/trasferimentosqlitemysql_temp.lib.php';
    if (comanda.url_server_mysql !== undefined && comanda.url_server_mysql !== "") {
        url_server = comanda.url_server_mysql + 'classi_php/trasferimentosqlitemysql_temp.lib.php';
    }
    $.ajax({
        type: "POST",
        async: true,
        data: {url_db_replica: JSON.stringify(dati_centri_statistica.map(v => {
                return {url_db_sqlite: v.url_db_sqlite, nome_db: v.nome_db};
            })), secondi_attesa_replica_mysql: comanda.secondi_attesa_replica_mysql,
            download_local_db: download_local_db},
        url: url_server,
        dataType: "json",
        beforeSend: function () {
            $('.loader2').show();
        },
        success: function (rar) {
            if (rar === true) {

                download_local_db = false;


                $('.loader2').show();

                var prezzo_vero = " prezzo_un as ";
                var prezzo_vero_2 = " prezzo_un ";
                if (comanda.pizzeria_asporto === true) {
                    prezzo_vero = " prezzo_vero as ";
                    prezzo_vero_2 = " prezzo_vero  ";
                }

                var array_data_partenza = $('#stat_art_selettore_data_partenza_grafico').val();
                var array_data_arrivo = $('#stat_art_selettore_data_arrivo_grafico').val();


                var layout = "dettagliovenduti";
                var dati_categorie = new Array();
                var labels_categorie = new Array();
                var backgroundColor_categorie = new Array();

                var query = "select tipo,percentuale from tabella_sconti_buoni where percentuale!='';";
                comanda.sincro.query(query, function (tsb) {

                    var tabella_sconti_buoni = new Array();
                    tsb.forEach(function (e) {
                        if (e.tipo === "SCONTO") {
                            tabella_sconti_buoni.push(e.percentuale + " %");
                        } else if (e.tipo === "BUONO") {
                            tabella_sconti_buoni.push("€ " + e.percentuale);
                        }
                    });


                    var query = "select * from settaggi_profili where id=" + comanda.folder_number + ";";
                    comanda.sincro.query(query, function (settaggi_profili) {
                        settaggi_profili = settaggi_profili[0];


                        /*Tipo:
                         *
                         * soloincassi 3
                         *
                         * dettagliovenduti 2
                         *
                         * misto 1
                         *
                         */

//alert("ATTENZIONE: PRIMA DI STAMPARE I TOTALI, \nASSICURATI CHE OGNI DISPOSITIVO FUNZIONI CORRETTAMENTE \nE CHE NON CI SIANO PROBLEMI DI RETE.");

//PARTE CHIUSURA

//SETTAGGI BOOL
//DISABILITA RAGGRUPPAMENTO PORTATA
                        var set_disabilita_raggruppamento_portata = true;
                        //DISABILITA RAGGRUPPAMENTO DESTINAZIONE
                        var set_disabilita_raggruppamento_destinazione = false;
//DISABILITA RAGGRUPPAMENTO CATEGORIA
                        var set_disabilita_raggruppamento_categoria = false;
//DICHIARAZIONE VARIABILI
//TOT:0 Totale
                        var totale = 0;
//SCONTO ARTICOLO SERVE NEL CICLO
                        var prezzo_articolo = 0;
                        var sconto_articolo = 0;
                        var incasso_singolo = 0;
//TOTALE SCONTI:0
                        var totale_sconti = 0;
//NON PAGATO
                        var non_pagato = 0;
//INCASSO: 0
                        var incasso = 0;
//INIZIALIZZAZIONE OGGETTI





                        var consegna_metro = false;
                        if (comanda.consegna_mezzo_metro.length > 0 || comanda.consegna_metro.length > 0) {
                            consegna_metro = true;
                        }


                        var qta_coperti = 0;
                        var totale_coperti = 0.00;


                        var totale_consegne = new Object();

                        var totale_consegne_extra = new Object();


                        if (consegna_metro === true) {
                            totale_consegne["MAXI"] = {"importo": 0, "qta": 0};
                            totale_consegne["MEZZO"] = {"importo": 0, "qta": 0};
                            totale_consegne["METRO"] = {"importo": 0, "qta": 0};
                            totale_consegne["NORMALI"] = {"importo": 0, "qta": 0};
                        } else {
                            totale_consegne["PIZZE"] = {"importo": 0, "qta": 0};

                        }

                        var totale_qta_consegne = 0;
                        var totale_qta_consegne_extra = new Object();


                        var totale_qta_ritiri = 0;
                        var totale_qta_mangiarequi = 0;

                        var totale_qta_ritiri = 0;
                        var totale_qta_mangiarequi = 0;
//IVA
                        var iva = new Object();
//OPERAZIONI_OPERATORE
                        var operazioni_operatore = new Object();
//ARTICOLO
                        //var articolo = new Object();
                        var articolo_generico = new Array();
                        var id_consegne = new Array();
                        var id_consegne_ritiri = new Array();
                        var id_consegne_mangiarequi = new Array();

                        //STATISTICHE PER GIORNO
                        var statistiche_giorno = new Object();
                        var giorni_estratti = new Array();

//STATISTICO
                        var statistico = new Object();
//STATISTICO['BAR']
                        statistico['BAR'] = new Object();
                        statistico['BAR'].incasso = 0;
//STATISTICO['TAKEAWAY']
                        statistico['TAKEAWAY'] = new Object();
                        statistico['TAKEAWAY'].incasso = 0;
//STATISTICO['RISTORAZIONE']
                        statistico['RISTORAZIONE'] = new Object();
                        statistico['RISTORAZIONE'].incasso = 0;
                        var fisco_teste = new Object;

                        fisco_teste['SCONTRINI'] = new Object();
                        fisco_teste['SCONTRINI'].numero = 0;
                        fisco_teste['SCONTRINI'].importo = 0;
                        fisco_teste['FATTURE'] = new Object();
                        fisco_teste['FATTURE'].numero = 0;
                        fisco_teste['FATTURE'].importo = 0;


                        fisco_teste['QUITTUNG'] = new Object();
                        fisco_teste['QUITTUNG'].numero = 0;
                        fisco_teste['QUITTUNG'].importo = 0;
                        fisco_teste['RECHNUNG'] = new Object();
                        fisco_teste['RECHNUNG'].numero = 0;
                        fisco_teste['RECHNUNG'].importo = 0;


                        //CONTEGGIO FATTURE SCONTRINI
                        var fisco = new Object();
                        fisco['FATTURE'] = new Object();
                        fisco['FATTURE'].incasso = 0;
                        fisco['FATTURE'].numero_min = null;
                        fisco['FATTURE'].numero_max = 0;
                        fisco['SCONTRINI'] = new Object();
                        fisco['SCONTRINI'].incasso = 0;
                        fisco['SCONTRINI'].numero_min = null;
                        fisco['SCONTRINI'].numero_max = 0;
                        console.log("STAMPA TOTALI QUERY OK");
                        var filtri = '';
                        var filtri_stampa = '';
                        var filtro_data = '';
                        var where_terminali = "";
                        var where_categorie = "";
                        var where_gruppi = "";
                        var where_giorni = "";
                        if (array_data_partenza.length > 1 && array_data_arrivo.length > 1) {

                            if ($('[name^=gbo_][type="checkbox"]:checked').length > 0) {

                                var array_giorni = new Array();

                                filtri += "<strong>GIORNI:</strong> ";
                                filtri_stampa += "<br/><strong>GIORNI:</strong> ";

                                $('[name^=gbo_][type="checkbox"]:checked').each(function (a, b) {


                                    array_giorni.push("'" + $(b).val() + "'");
                                    filtri += $(b).attr('nome_filtro').toLowerCase().capitalize() + ", ";
                                    filtri_stampa += $(b).attr('nome_filtro').toLowerCase().capitalize() + ", ";

                                });
                                filtri = filtri.slice(0, -2);
                                filtri_stampa = filtri_stampa.slice(0, -2);

                                var array_giorni_joined = array_giorni.join(',');


                                if (array_giorni_joined.length > 0) {
                                    where_giorni = " AND giorno IN (" + array_giorni_joined + ") ";
                                }
                            }

                            $('#stat_art_elenco_centri_backoffice [type="checkbox"]:checked').each(function (a, b) {


                                comanda.array_db_centri.push($(b).val());
                                filtri += $(b).val().toLowerCase().capitalize() + ", ";
                                filtri_stampa += $(b).val().toLowerCase().capitalize() + ", ";

                            });

                            if ($('#stat_art_elenco_terminali_backoffice [type="checkbox"]:checked').length > 0) {

                                var array_terminali = new Array();

                                filtri += "<br/><strong>TERMINALI:</strong> ";
                                filtri_stampa += "<br/><strong>TERMINALI:</strong> ";

                                $('#stat_art_elenco_terminali_backoffice [type="checkbox"]:checked').each(function (a, b) {


                                    array_terminali.push("'" + $(b).val() + "'");
                                    filtri += $(b).val().toLowerCase().capitalize() + ", ";
                                    filtri_stampa += $(b).val().toLowerCase().capitalize() + ", ";

                                });

                                filtri = filtri.slice(0, -2);
                                filtri_stampa = filtri_stampa.slice(0, -2);

                                var array_terminali_joined = array_terminali.join(',');


                                if (array_terminali_joined.length > 0) {
                                    where_terminali = " AND nome_pc IN (" + array_terminali_joined + ") ";
                                }
                            }

                            if ($('#stat_art_elenco_categorie_backoffice [type="checkbox"]:checked').length > 0) {
                                var array_categorie = new Array();
                                filtri += "<br/><strong>CATEGORIE:</strong> ";
                                filtri_stampa += "\n<strong>CATEGORIE:</strong> ";

                                $('#stat_art_elenco_categorie_backoffice [type="checkbox"]:checked').each(function (a, b) {

                                    array_categorie.push("'" + $(b).val() + "'");
                                    filtri += $(b).attr('name').substr(4).toLowerCase().capitalize() + ", ";
                                    filtri_stampa += $(b).attr('name').substr(4).toLowerCase().capitalize() + ", ";


                                });

                                filtri = filtri.slice(0, -2);
                                filtri_stampa = filtri_stampa.slice(0, -2);

                                var array_categorie_joined = array_categorie.join(',');


                                if (array_categorie_joined.length > 0) {
                                    where_categorie = " AND categoria IN (" + array_categorie_joined + ") ";
                                }
                            }

                            if ($('#stat_art_elenco_gruppi_backoffice [type="checkbox"]:checked').length > 0) {
                                var array_gruppi = new Array();
                                filtri += "<br/><strong>GRUPPI:</strong><br/>";
                                filtri_stampa += "\nGRUPPI:\n";

                                $('#stat_art_elenco_gruppi_backoffice [type="checkbox"]:checked').each(function (a, b) {

                                    array_gruppi.push("'" + $(b).val() + "'");
                                    filtri += "- " + $(b).attr('name').substr(4) + "<br/>";
                                    filtri_stampa += "- " + $(b).attr('name').substr(4) + "\n";


                                });

                                var array_gruppi_joined = array_gruppi.join(',');


                                if (array_gruppi_joined.length > 0) {
                                    where_gruppi = " AND gruppo IN (" + array_gruppi_joined + ") ";
                                }
                            }

                            var orario_partenza = $('#stat_art_selettore_ora_partenza_grafico').val().split(':');

                            var yY = array_data_partenza.substr(0, 4);
                            var yD = array_data_partenza.substr(8, 2);
                            var yM = array_data_partenza.substr(5, 2);
                            var yh = orario_partenza[0]/*"00"*/;
                            var ym = orario_partenza[1]/*"00"*/;
                            var ys = "00";
                            var yms = "000";
                            var data_ieri = yY + '' + yM + '' + yD + '' + yh + '' + ym /*+ comanda.ora_servizio*/;


                            var orario_arrivo = $('#stat_art_selettore_ora_arrivo_grafico').val().split(':');

                            var Y = array_data_arrivo.substr(0, 4);
                            var D = array_data_arrivo.substr(8, 2);
                            var M = array_data_arrivo.substr(5, 2);
                            var h = orario_arrivo[0]/*"00"*/;
                            var m = orario_arrivo[1]/*"00"*/;
                            var s = "00";
                            var ms = "000";
                            var data_oggi = Y + '' + M + '' + D + '' + h + '' + m /*+ comanda.ora_servizio*/;

                            var check_orario_multigiorno = $('#stat_art_multigiorno_ora_grafico')[0].checked;

                            if (check_orario_multigiorno === false) {
                                var orario_query = " (data_comanda>='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00') and (data_comanda<='" + Y + "-" + M + "-" + D + "' and ora_comanda <='" + h + ":" + m + ":59') ";
                                var orario_query_tes = " (data_comanda>='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00') and (data_comanda<='" + Y + "-" + M + "-" + D + "' and ora_comanda <='" + h + ":" + m + ":59') ";
                            } else {
                                var orario_query = " data_comanda>='" + yY + "-" + yM + "-" + yD + "' and data_comanda<='" + Y + "-" + M + "-" + D + "' and ora_comanda >='" + yh + ":" + ym + ":00'  and ora_comanda <='" + h + ":" + m + ":59' ";
                                var orario_query_tes = " data_comanda>='" + yY + "-" + yM + "-" + yD + "' and data_comanda<='" + Y + "-" + M + "-" + D + "' and ora_comanda >='" + yh + ":" + ym + ":00'  and ora_comanda <='" + h + ":" + m + ":59' ";

                            }

                            if (array_data_partenza === array_data_arrivo) {
                                if ($('#stat_art_selettore_ora_partenza_grafico').val() !== '00:00' || $('#stat_art_selettore_ora_arrivo_grafico').val() !== '23:59') {
                                    var data_report = yD + '/' + yM + '/' + yY + ' dalle ' + $('#stat_art_selettore_ora_partenza_grafico').val() + ' alle ' + $('#stat_art_selettore_ora_arrivo_grafico').val();
                                } else {
                                    var data_report = yD + '/' + yM + '/' + yY;
                                }
                                filtro_data += data_report + '<br/>';
                            } else {


                                if ($('#stat_art_selettore_ora_partenza_grafico').val() !== '00:00' || $('#stat_art_selettore_ora_arrivo_grafico').val() !== '23:59') {
                                    var data_report = 'da: ' + yD + '/' + yM + '/' + yY + ' alle ' + $('#stat_art_selettore_ora_partenza_grafico').val() + '\na: ' + D + '/' + M + '/' + Y + ' alle ' + $('#stat_art_selettore_ora_arrivo_grafico').val();
                                    filtro_data += 'da: ' + yD + '/' + yM + '/' + yY + ' alle ' + $('#stat_art_selettore_ora_partenza_grafico').val() + ' a: ' + D + '/' + M + '/' + Y + ' alle ' + $('#stat_art_selettore_ora_arrivo_grafico').val() + '<br/>';
                                } else {
                                    var data_report = 'da: ' + yD + '/' + yM + '/' + yY + '\na: ' + D + '/' + M + '/' + Y;
                                    filtro_data += 'da: ' + yD + '/' + yM + '/' + yY + ' a: ' + D + '/' + M + '/' + Y + '<br/>';
                                }
                            }
                        } else {

                            var d = new Date();
                            d.setDate(d.getDate() + 1);
                            var Y = d.getFullYear();
                            var M = addZero((d.getMonth() + 1), 2);
                            var D = addZero(d.getDate(), 2);
                            var h = addZero(d.getHours(), 2);
                            var m = addZero(d.getMinutes(), 2);
                            var s = addZero(d.getSeconds(), 2);
                            var ms = addZero(d.getMilliseconds(), 3);
                            var data_oggi = Y + '' + M + '' + D + '' + comanda.ora_servizio + '00';
                            var y = new Date();
                            y.setDate(y.getDate());
                            var yY = y.getFullYear();
                            var yM = addZero((y.getMonth() + 1), 2);
                            var yD = addZero(y.getDate(), 2);
                            var yh = addZero(y.getHours(), 2);
                            var ym = addZero(y.getMinutes(), 2);
                            var ys = addZero(y.getSeconds(), 2);
                            var yms = addZero(y.getMilliseconds(), 3);
                            var data_ieri = yY + '' + yM + '' + yD + '' + comanda.ora_servizio + '00';

                            var check_orario_multigiorno = $('#stat_art_multigiorno_ora_grafico')[0].checked;

                            if (check_orario_multigiorno === false) {
                                var orario_query = " (data_comanda>='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00') and (data_comanda<='" + yY + "-" + yM + "-" + yD + "' and ora_comanda <='" + h + ":" + m + ":59') ";
                                var orario_query_tes = " (data_comanda>='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00') and (data_comanda<='" + yY + "-" + yM + "-" + yD + "' and ora_comanda <='" + h + ":" + m + ":59') ";

                            } else {
                                var orario_query = " data_comanda>='" + yY + "-" + yM + "-" + yD + "' and data_comanda<='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00'  and ora_comanda <='" + h + ":" + m + ":59' ";
                                var orario_query_tes = " data_comanda>='" + yY + "-" + yM + "-" + yD + "' and data_comanda<='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00'  and ora_comanda <='" + h + ":" + m + ":59' ";

                            }

                            var data_report = yD + '/' + yM + '/' + yY;
                            filtro_data += data_report + '<br/>';
                        }
//CICLO DB CENTRALE WHERE STATO RECORD CHIUSO AND POSIZIONE CONTO

                        var tempo_iniziale = new Date().getTime();

                        var orario_multigiorno = $('#stat_art_multigiorno_ora_grafico')[0].checked;

                        /* String */
                        var condizione_fiscalizzata_sn = " and fiscalizzata_sn='NONVALIDA' "

                        var password = CryptoJS.MD5($('#stat_art_campo_password').val()).toString();
                        var pass_statistiche_teste = CryptoJS.MD5("FI" + comanda.numero_licenza_cliente).toString();

                        var pass_statistiche_comande = CryptoJS.MD5("FI" + comanda.numero_licenza_cliente + "TOTALE").toString();
                        if (comanda.numero_licenza_cliente === "0595") {
                            pass_statistiche_comande = CryptoJS.MD5("FI" + comanda.numero_licenza_cliente + "IMPERIAL").toString();
                        }

                        if (password === pass_statistiche_teste) {
                            condizione_fiscalizzata_sn = " and fiscalizzata_sn='S' ";
                        } else if (comanda.visualizza_tutti_venduti === true && password === pass_statistiche_comande)
                        {
                            condizione_fiscalizzata_sn = " and (fiscalizzata_sn='S' OR tipo_ricevuta='conto' OR tipo_ricevuta='STORICIZZAZIONE DI PROVA') ";
                        } else {
                            $('.loader2').hide();
                            bootbox.alert("Inserire una password valida!");
                            return false;
                        }


                        var query_articoli = "select numero, " + comanda.lingua_stampa + " from nomi_portate order by numero asc;";
                        comanda.sincro.query_cassa(query_articoli, 1000000, function (result_portate) {

                            var query_articoli = "select id, descrizione from categorie order by id asc;";
                            comanda.sincro.query_cassa(query_articoli, 1000000, function (result_categorie) {

                                //Nomi categorie da id
                                var nomi_categorie = new Object();
                                for (var a in result_categorie) {
                                    nomi_categorie[result_categorie[a].id] = result_categorie[a].descrizione;
                                }

                                var query_articoli = "select id, nome from gruppi_statistici order by id asc;";
                                comanda.sincro.query_cassa(query_articoli, 1000000, function (result_gruppi) {


                                    var nomi_gruppi = new Object();
                                    for (var a in result_gruppi) {
                                        nomi_gruppi[result_gruppi[a].id] = result_gruppi[a].nome;
                                    }

                                    var query_articoli = "update comanda set sconto_perc='0.00' where sconto_perc is null;";
                                    comanda.sincro.query_cassa(query_articoli, 1000000, function () {

                                        var query_articoli2 = "create TEMPORARY TABLE test SELECT  '' as centro,'' as indice,id,gruppo,giorno,sconto_imp," + prezzo_vero_2 + " as valore_sconto , sum(quantita) as quantita_totale, sum(netto) as totale_sconti FROM back_office WHERE  desc_art NOT LIKE '%COD:%' and  posizione='SCONTO' " + where_terminali + where_categorie + where_gruppi + where_giorni + " " + condizione_fiscalizzata_sn + " and " + orario_query + " group by " + prezzo_vero_2 + " ";
                                        query_articoli2 += " UNION ALL                     SELECT  '' as centro,'' as indice,id,gruppo,giorno,sconto_imp," + prezzo_vero_2 + " as valore_sconto , sum(quantita) as quantita_totale, sum(netto) as totale_sconti FROM comanda_temp WHERE  desc_art NOT LIKE '%COD:%' and  posizione='SCONTO' " + where_terminali + where_categorie + where_gruppi + where_giorni + " " + condizione_fiscalizzata_sn + " and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and tipo_ricevuta!='' and " + orario_query + " group by " + prezzo_vero_2 + " ;";

                                        if (comanda.array_db_centri.length > 0) {

                                            var query_articoli2 = "create TEMPORARY TABLE test ";
                                            var i = 1;

                                            comanda.array_db_centri.forEach(centro => {
                                                query_articoli2 += "SELECT '" + centro + "' as centro,'' as indice,id,gruppo,giorno,sconto_imp," + prezzo_vero_2 + " as valore_sconto , sum(quantita) as quantita_totale, sum(netto) as totale_sconti FROM " + centro + ".back_office WHERE  desc_art NOT LIKE '%COD:%' and  posizione='SCONTO' " + where_terminali + where_categorie + where_gruppi + where_giorni + " " + condizione_fiscalizzata_sn + " and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and tipo_ricevuta!='' and " + orario_query + " group by " + prezzo_vero_2 + " UNION ALL ";
                                                query_articoli2 += " UNION ALL ";
                                                query_articoli2 += "SELECT '" + centro + "' as centro,'' as indice,id,gruppo,giorno,sconto_imp," + prezzo_vero_2 + " as valore_sconto, sum(quantita) as quantita_totale, sum(netto) as totale_sconti FROM " + centro + ".comanda_temp WHERE  desc_art NOT LIKE '%COD:%' and  posizione='SCONTO' " + where_terminali + where_categorie + where_gruppi + where_giorni + " " + condizione_fiscalizzata_sn + " and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and tipo_ricevuta!='' and " + orario_query + " group by " + prezzo_vero_2 + " ";

                                                if (i !== comanda.array_db_centri.length) {
                                                    query_articoli2 += " UNION ALL ";
                                                }

                                                i++;

                                            });
                                        }



                                        //DA ORDINARE MANUALMENTE
                                        //CAMBIARE WHERE GIORNI E WHERE TERMINALI

                                        //FARE PROMISE 
                                        var p1 = new Promise(function (resolve, reject) {
                                            //19/10/2020
                                            comanda.sincro.query_statistiche(JSON.stringify([query_articoli2]), 1000000, function (result_sconti) {

                                                var rs = new Array();

                                                result_sconti.forEach(function (e) {
                                                    if (e.sconto_imp === "P") {
                                                        e.valore_sconto = e.valore_sconto + " %";
                                                    }
                                                    rs.push(e);
                                                });

                                                resolve(rs);
                                            });
                                        });


                                        var giorno_incasso_gb = "";
                                        if (array_giorni_joined !== undefined) {
                                            giorno_incasso_gb = "and (giorno_incasso IN (" + array_giorni_joined + ") or giorno_emissione IN(" + array_giorni_joined + "))";
                                        }

                                        var terminali_gb = "";
                                        if (array_terminali_joined !== undefined) {
                                            terminali_gb = "and (nome_pc_incasso IN (" + array_terminali_joined + ") or nome_pc_emissione IN (" + array_terminali_joined + "))";
                                        }
                                        var query_articoli = "SELECT * FROM gestione_buoni_sconto WHERE  ((data_emissione_ascii>='" + yY + "" + yM + "" + yD + "" + yh + "" + ym + "" + ys + "' and data_emissione<'" + Y + "" + M + "" + D + "" + h + "" + m + "" + s + "') or (data_utilizzo_ascii>='" + yY + "" + yM + "" + yD + "" + yh + "" + ym + "" + ys + "' and data_utilizzo_ascii<'" + Y + "" + M + "" + D + "" + h + "" + m + "" + s + "')) " + giorno_incasso_gb + " " + terminali_gb + " order by percentuale_buono asc;";


                                        var p2 = new Promise(function (resolve, reject) {
                                            comanda.sincro.query_centrale(query_articoli, function (result_buoni_sconto) {
                                                resolve(result_buoni_sconto);
                                            });

                                        });


                                        var query_articoli_zero = "";
                                        if (comanda.escludi_art_zero_statistiche === true) {
                                            query_articoli_zero = " and " + prezzo_vero_2 + "!=0 ";
                                        }

                                        var ordinamento = "descrizione";
                                        //SE E' NEVODI ORDINA PER COD.ARTICOLO
                                        if (comanda.numero_licenza_cliente === "0568") {
                                            ordinamento = "cod_articolo";
                                        }

                                        if (ordinamento === "cod_articolo") {
                                            let prodotti = new Object();
                                            alasql.tables.prodotti.data.forEach(function (prodotto) {
                                                prodotti[prodotto.id] = new Object();
                                                prodotti[prodotto.id] = prodotto.descrizione;
                                            });
                                        }


                                        let query_articoli33 = "create TEMPORARY TABLE test2 select '' as centro,'' as indice,data_comanda,id,tipologia_fattorino,gruppo,giorno,fiscalizzata_sn,tipo_ricevuta,n_fiscale,operatore,ntav_comanda,quantita,desc_art,sconto_perc,perc_iva,categoria,dest_stampa,portata," + prezzo_vero_2 + " as prezzo_un,QTA,tipo_consegna,cod_articolo,totale,'' as totale_costo,'' as totale_ricavo  from back_office  where  (posizione='CONTO' OR posizione='COPERTO') " + where_terminali + where_categorie + where_gruppi + where_giorni + " " + condizione_fiscalizzata_sn + " and " + orario_query + " and portata!='ZZZ' and cod_promo!=1 " + query_articoli_zero + " group by id asc,desc_art asc,prezzo_un asc,sconto_perc asc ";
                                        query_articoli33 += " UNION ALL                     select  '' as centro,'' as indice,data_comanda,id,tipologia_fattorino,gruppo,giorno,fiscalizzata_sn,tipo_ricevuta,n_fiscale,operatore,ntav_comanda,quantita,desc_art,sconto_perc,perc_iva,categoria,dest_stampa,portata," + prezzo_vero_2 + " as prezzo_un,QTA,tipo_consegna,cod_articolo,(prezzo_un*sum(quantita))/100*(100-sconto_perc)  as totale,'' as totale_costo,'' as totale_ricavo from comanda_temp where (posizione='CONTO' OR posizione='COPERTO') " + where_terminali + where_categorie + where_gruppi + where_giorni + " " + condizione_fiscalizzata_sn + " and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and tipo_ricevuta!='' and " + orario_query + " and portata!='ZZZ'  and cod_promo!=1 " + query_articoli_zero + " group by id asc,desc_art asc,prezzo_un asc,sconto_perc asc;";


                                        if (comanda.array_db_centri.length > 0) {
                                            query_articoli33 = "create TEMPORARY TABLE test2 ";
                                            var i = 1;
                                            comanda.array_db_centri.forEach(centro => {
                                                query_articoli33 += "select '" + centro + "' as centro,'' as indice,data_comanda,id,tipologia_fattorino,gruppo,giorno,fiscalizzata_sn,tipo_ricevuta,n_fiscale,operatore,ntav_comanda,quantita,desc_art,sconto_perc,perc_iva,categoria,dest_stampa,portata," + prezzo_vero_2 + " as prezzo_un,QTA,tipo_consegna,cod_articolo,totale,'' as totale_costo,'' as totale_ricavo from " + centro + ".back_office      where (posizione='CONTO' OR posizione='COPERTO') " + where_terminali + where_categorie + where_gruppi + where_giorni + " " + condizione_fiscalizzata_sn + "  and " + orario_query + " and portata!='ZZZ'  and cod_promo!=1 " + query_articoli_zero + " group by id asc,desc_art asc,prezzo_un asc,sconto_perc asc ";
                                                query_articoli33 += " UNION ALL ";
                                                query_articoli33 += "select '" + centro + "' as centro,'' as indice,data_comanda,id,tipologia_fattorino,gruppo,giorno,fiscalizzata_sn,tipo_ricevuta,n_fiscale,operatore,ntav_comanda,quantita,desc_art,sconto_perc,perc_iva,categoria,dest_stampa,portata," + prezzo_vero_2 + " as prezzo_un,QTA,tipo_consegna,cod_articolo,(prezzo_un*sum(quantita))/100*(100-sconto_perc) as totale,'' as totale_costo,'' as totale_ricavo from " + centro + ".comanda_temp where (posizione='CONTO' OR posizione='COPERTO') " + where_terminali + where_categorie + where_gruppi + where_giorni + " " + condizione_fiscalizzata_sn + " and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and tipo_ricevuta!='' and " + orario_query + " and portata!='ZZZ'  and cod_promo!=1 " + query_articoli_zero + " group by id asc,desc_art asc,prezzo_un asc,sconto_perc asc ";

                                                if (i !== comanda.array_db_centri.length) {
                                                    query_articoli33 += " UNION ALL ";
                                                }

                                                i++;

                                            });
                                        }
                                        query_articoli33 += ";";


                                        //QUI METTO GLI INDICI

                                        let periodo_confronto = $("#stat_art_periodo_confronto").val();

                                        let query_periodo_confronto = "";
                                        let indice = "";
                                        let tab = new Object();

                                        switch (periodo_confronto) {
                                            case "1":
                                                query_periodo_confronto = "giorno as indice,";
                                                indice = "giorno";
                                                break;
                                            case "2":
                                                query_periodo_confronto = "substr(data_comanda,6,2) as indice,";
                                                indice = "substr(data_comanda,6,2)";
                                                break;
                                            case "3":
                                                query_periodo_confronto = "substr(data_comanda,1,4) as indice,";
                                                indice = "substr(data_comanda,1,4)";
                                                break;
                                        }

                                        let query_articoli3 = "create TEMPORARY TABLE test select centro," + query_periodo_confronto + "id,tipologia_fattorino,gruppo,giorno,fiscalizzata_sn,tipo_ricevuta,n_fiscale,operatore,ntav_comanda,sum(quantita) as quantita,desc_art,sconto_perc,perc_iva,categoria,dest_stampa,portata, prezzo_un,QTA,tipo_consegna,cod_articolo,sum(totale) as totale,'' as costo,'' as totale_costo,'' as totale_ricavo  from test2 group by " + indice + " asc, desc_art asc,centro asc;";


                                        var p3 = new Promise(function (resolve, reject) {

                                            comanda.sincro.query_statistiche(JSON.stringify(["TRUNCATE test;", "TRUNCATE test2;", query_articoli33, query_articoli3]), 1000000, function (result) {

                                                resolve(result);

                                            });
                                        });

                                        //AGGIUNGERE TUTTE LE PROMISE
                                        p3.then(function (result) {








                                            result.forEach((v, i) => {


                                                let indice = "";
                                                switch (periodo_confronto) {
                                                    case "1":


                                                        switch (v.indice) {
                                                            case "1":
                                                                indice = "Lunedi";
                                                                break;
                                                            case "2":
                                                                indice = "Martedi";
                                                                break;
                                                            case "3":
                                                                indice = "Mercoledi";
                                                                break;
                                                            case "4":
                                                                indice = "Giovedi";
                                                                break;
                                                            case "5":
                                                                indice = "Venerdi";
                                                                break;
                                                            case "6":
                                                                indice = "Sabato";
                                                                break;
                                                            case "7":
                                                                indice = "Domenica";
                                                                break;
                                                        }

                                                        if (tab[v.desc_art + " - " + v.centro] === undefined) {
                                                            tab[v.desc_art + " - " + v.centro] = new Object();
                                                            tab[v.desc_art + " - " + v.centro]["Lunedi"] = new Array();
                                                            tab[v.desc_art + " - " + v.centro]["Martedi"] = new Array();
                                                            tab[v.desc_art + " - " + v.centro]["Mercoledi"] = new Array();
                                                            tab[v.desc_art + " - " + v.centro]["Giovedi"] = new Array();
                                                            tab[v.desc_art + " - " + v.centro]["Venerdi"] = new Array();
                                                            tab[v.desc_art + " - " + v.centro]["Sabato"] = new Array();
                                                            tab[v.desc_art + " - " + v.centro]["Domenica"] = new Array();



                                                            tab[v.desc_art + " - " + v.centro].prezzo_totale = 0.00;
                                                            tab[v.desc_art + " - " + v.centro].quantita_totale = 0;
                                                        }
                                                        break;
                                                    case "2":

                                                        switch (v.indice) {
                                                            case "01":
                                                                indice = "Gennaio";
                                                                break;
                                                            case "02":
                                                                indice = "Febbraio";
                                                                break;
                                                            case "03":
                                                                indice = "Marzo";
                                                                break;
                                                            case "04":
                                                                indice = "Aprile";
                                                                break;
                                                            case "05":
                                                                indice = "Maggio";
                                                                break;
                                                            case "06":
                                                                indice = "Giugno";
                                                                break;
                                                            case "07":
                                                                indice = "Luglio";
                                                                break;
                                                            case "08":
                                                                indice = "Agosto";
                                                                break;
                                                            case "09":
                                                                indice = "Settembre";
                                                                break;
                                                            case "10":
                                                                indice = "Ottobre";
                                                                break;
                                                            case "11":
                                                                indice = "Novembre";
                                                                break;
                                                            case "12":
                                                                indice = "Dicembre";
                                                                break;
                                                        }

                                                        if (tab[v.desc_art + " - " + v.centro] === undefined) {
                                                            tab[v.desc_art + " - " + v.centro] = new Object();
                                                            tab[v.desc_art + " - " + v.centro]["Gennaio"] = new Array();
                                                            tab[v.desc_art + " - " + v.centro]["Febbraio"] = new Array();
                                                            tab[v.desc_art + " - " + v.centro]["Marzo"] = new Array();
                                                            tab[v.desc_art + " - " + v.centro]["Aprile"] = new Array();
                                                            tab[v.desc_art + " - " + v.centro]["Maggio"] = new Array();
                                                            tab[v.desc_art + " - " + v.centro]["Giugno"] = new Array();
                                                            tab[v.desc_art + " - " + v.centro]["Luglio"] = new Array();
                                                            tab[v.desc_art + " - " + v.centro]["Agosto"] = new Array();
                                                            tab[v.desc_art + " - " + v.centro]["Settembre"] = new Array();
                                                            tab[v.desc_art + " - " + v.centro]["Ottobre"] = new Array();
                                                            tab[v.desc_art + " - " + v.centro]["Novembre"] = new Array();
                                                            tab[v.desc_art + " - " + v.centro]["Dicembre"] = new Array();
                                                            tab[v.desc_art + " - " + v.centro].prezzo_totale = 0.00;
                                                            tab[v.desc_art + " - " + v.centro].quantita_totale = 0;
                                                        }


                                                        break;

                                                    case "3":
                                                        indice = v.indice;

                                                        if (tab[v.desc_art + " - " + v.centro] === undefined) {
                                                            tab[v.desc_art + " - " + v.centro] = new Object();
                                                        }

                                                        if (tab[v.desc_art + " - " + v.centro][indice] === undefined) {
                                                            tab[v.desc_art + " - " + v.centro][indice] = new Array();
                                                            tab[v.desc_art + " - " + v.centro].prezzo_totale = 0.00;
                                                            tab[v.desc_art + " - " + v.centro].quantita_totale = 0;
                                                        }
                                                        break;
                                                }

                                                tab[v.desc_art + " - " + v.centro][indice].quantita = v.quantita;
                                                tab[v.desc_art + " - " + v.centro][indice].totale = v.totale;

                                                tab[v.desc_art + " - " + v.centro].prezzo_totale += v.totale;
                                                tab[v.desc_art + " - " + v.centro].quantita_totale += v.quantita;

                                            });

                                            //console.log(tab);

                                            let tabella_finale = "<table class='table table-striped table-hover table-responsive'>";

                                            tabella_finale += "<tr>";
                                            tabella_finale += "<th></th>";
                                            for (let articolo in tab) {
                                                for (let indice in tab[articolo]) {
                                                    if (Array.isArray(tab[articolo][indice])) {
                                                        tabella_finale += "<th>" + indice + "</th><th></th>";
                                                    }
                                                }
                                                break;
                                            }

                                            tabella_finale += "</tr>";

                                            tabella_finale += "<tr>";
                                            tabella_finale += "<th>Descrizione</th>";
                                            for (let articolo in tab) {
                                                for (let indice in tab[articolo]) {
                                                    if (Array.isArray(tab[articolo][indice])) {
                                                        tabella_finale += "<th>Qt</th><th>Tot</th>";
                                                    }
                                                }
                                                break;
                                            }
                                            tabella_finale += "<th>Qt</th>";
                                            tabella_finale += "<th>Tot</th>";
                                            tabella_finale += "</tr>";


                                            for (let articolo in tab) {

                                                tabella_finale += "<tr>";
                                                console.log("nome articolo", articolo);

                                                tabella_finale += "<td>" + articolo + "</td>";


                                                console.log(tab[articolo]);
                                                //INDICE. ES. GIORNO
                                                for (let indice in tab[articolo]) {
                                                    if (Array.isArray(tab[articolo][indice])) {


                                                        //console.log("nome indice", indice);
                                                        if (tab[articolo][indice].quantita !== undefined && tab[articolo][indice].totale !== undefined) {
                                                            tabella_finale += "<td>" + tab[articolo][indice].quantita + "</td>";
                                                            tabella_finale += "<td>" + tab[articolo][indice].totale.toFixed(2) + "</td>";
                                                        } else {
                                                            tabella_finale += "<td></td>";
                                                            tabella_finale += "<td></td>";

                                                            /*tabella_finale += "<td>0";
                                                             tabella_finale += " - 0.00</td>";*/
                                                        }

                                                    }
                                                }


                                                console.log("quantita totale articolo", tab[articolo].quantita_totale);
                                                tabella_finale += "<td>" + tab[articolo].quantita_totale + "</td>";

                                                console.log("prezzo totale articolo", tab[articolo].prezzo_totale);
                                                tabella_finale += "<td>" + tab[articolo].prezzo_totale.toFixed(2) + "</td>";


                                                tabella_finale += "</tr>";


                                            }

                                            tabella_finale += "</table>";

                                            $('#stat_art_tabella_venduti_giorno').html(tabella_finale);
                                            $('.loader2').hide();

                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            } else
            {
                bootbox.alert("Errore di Runtime nella sincronia dei dati del giorno.");
            }
        },
        error: function () {
            bootbox.alert("Errore di Ajax nella sincronia dei dati del giorno.");
        }
    });


}


function statistiche_margini_guadagno() {
    comanda.array_db_centri = new Array();

    let dati_centri_statistica = alasql("select * from centri_statistica");



    let url_server = comanda.url_server + 'classi_php/trasferimentosqlitemysql_temp.lib.php';
    if (comanda.url_server_mysql !== undefined && comanda.url_server_mysql !== "") {
        url_server = comanda.url_server_mysql + 'classi_php/trasferimentosqlitemysql_temp.lib.php';
    }
    $.ajax({
        type: "POST",
        async: true,
        data: {url_db_replica: JSON.stringify(dati_centri_statistica.map(v => {
                return {url_db_sqlite: v.url_db_sqlite, nome_db: v.nome_db};
            })), secondi_attesa_replica_mysql: comanda.secondi_attesa_replica_mysql,
            download_local_db: download_local_db},
        url: url_server,
        dataType: "json",
        beforeSend: function () {
            $('.loader2').show();
        },
        success: function (rar) {
            if (rar === true) {

                download_local_db = false;


                $('.loader2').show();

                var prezzo_vero = " prezzo_un as ";
                var prezzo_vero_2 = " prezzo_un ";
                if (comanda.pizzeria_asporto === true) {
                    prezzo_vero = " prezzo_vero as ";
                    prezzo_vero_2 = " prezzo_vero  ";
                }

                var array_data_partenza = $('#stat_art_selettore_data_partenza_grafico').val();
                var array_data_arrivo = $('#stat_art_selettore_data_arrivo_grafico').val();


                var layout = "dettagliovenduti";
                var dati_categorie = new Array();
                var labels_categorie = new Array();
                var backgroundColor_categorie = new Array();

                var query = "select tipo,percentuale from tabella_sconti_buoni where percentuale!='';";
                comanda.sincro.query(query, function (tsb) {

                    var tabella_sconti_buoni = new Array();
                    tsb.forEach(function (e) {
                        if (e.tipo === "SCONTO") {
                            tabella_sconti_buoni.push(e.percentuale + " %");
                        } else if (e.tipo === "BUONO") {
                            tabella_sconti_buoni.push("€ " + e.percentuale);
                        }
                    });


                    var query = "select * from settaggi_profili where id=" + comanda.folder_number + ";";
                    comanda.sincro.query(query, function (settaggi_profili) {
                        settaggi_profili = settaggi_profili[0];


                        /*Tipo:
                         *
                         * soloincassi 3
                         *
                         * dettagliovenduti 2
                         *
                         * misto 1
                         *
                         */

//alert("ATTENZIONE: PRIMA DI STAMPARE I TOTALI, \nASSICURATI CHE OGNI DISPOSITIVO FUNZIONI CORRETTAMENTE \nE CHE NON CI SIANO PROBLEMI DI RETE.");

//PARTE CHIUSURA

//SETTAGGI BOOL
//DISABILITA RAGGRUPPAMENTO PORTATA
                        var set_disabilita_raggruppamento_portata = true;
                        //DISABILITA RAGGRUPPAMENTO DESTINAZIONE
                        var set_disabilita_raggruppamento_destinazione = false;
//DISABILITA RAGGRUPPAMENTO CATEGORIA
                        var set_disabilita_raggruppamento_categoria = false;
//DICHIARAZIONE VARIABILI
//TOT:0 Totale
                        var totale = 0;
//SCONTO ARTICOLO SERVE NEL CICLO
                        var prezzo_articolo = 0;
                        var sconto_articolo = 0;
                        var incasso_singolo = 0;
//TOTALE SCONTI:0
                        var totale_sconti = 0;
//NON PAGATO
                        var non_pagato = 0;
//INCASSO: 0
                        var incasso = 0;
//INIZIALIZZAZIONE OGGETTI





                        var consegna_metro = false;
                        if (comanda.consegna_mezzo_metro.length > 0 || comanda.consegna_metro.length > 0) {
                            consegna_metro = true;
                        }


                        var qta_coperti = 0;
                        var totale_coperti = 0.00;


                        var totale_consegne = new Object();

                        var totale_consegne_extra = new Object();


                        if (consegna_metro === true) {
                            totale_consegne["MAXI"] = {"importo": 0, "qta": 0};
                            totale_consegne["MEZZO"] = {"importo": 0, "qta": 0};
                            totale_consegne["METRO"] = {"importo": 0, "qta": 0};
                            totale_consegne["NORMALI"] = {"importo": 0, "qta": 0};
                        } else {
                            totale_consegne["PIZZE"] = {"importo": 0, "qta": 0};

                        }

                        var totale_qta_consegne = 0;
                        var totale_qta_consegne_extra = new Object();


                        var totale_qta_ritiri = 0;
                        var totale_qta_mangiarequi = 0;

                        var totale_qta_ritiri = 0;
                        var totale_qta_mangiarequi = 0;
//IVA
                        var iva = new Object();
//OPERAZIONI_OPERATORE
                        var operazioni_operatore = new Object();
//ARTICOLO
                        //var articolo = new Object();
                        var articolo_generico = new Array();
                        var id_consegne = new Array();
                        var id_consegne_ritiri = new Array();
                        var id_consegne_mangiarequi = new Array();

                        //STATISTICHE PER GIORNO
                        var statistiche_giorno = new Object();
                        var giorni_estratti = new Array();

//STATISTICO
                        var statistico = new Object();
//STATISTICO['BAR']
                        statistico['BAR'] = new Object();
                        statistico['BAR'].incasso = 0;
//STATISTICO['TAKEAWAY']
                        statistico['TAKEAWAY'] = new Object();
                        statistico['TAKEAWAY'].incasso = 0;
//STATISTICO['RISTORAZIONE']
                        statistico['RISTORAZIONE'] = new Object();
                        statistico['RISTORAZIONE'].incasso = 0;
                        var fisco_teste = new Object;

                        fisco_teste['SCONTRINI'] = new Object();
                        fisco_teste['SCONTRINI'].numero = 0;
                        fisco_teste['SCONTRINI'].importo = 0;
                        fisco_teste['FATTURE'] = new Object();
                        fisco_teste['FATTURE'].numero = 0;
                        fisco_teste['FATTURE'].importo = 0;


                        fisco_teste['QUITTUNG'] = new Object();
                        fisco_teste['QUITTUNG'].numero = 0;
                        fisco_teste['QUITTUNG'].importo = 0;
                        fisco_teste['RECHNUNG'] = new Object();
                        fisco_teste['RECHNUNG'].numero = 0;
                        fisco_teste['RECHNUNG'].importo = 0;


                        //CONTEGGIO FATTURE SCONTRINI
                        var fisco = new Object();
                        fisco['FATTURE'] = new Object();
                        fisco['FATTURE'].incasso = 0;
                        fisco['FATTURE'].numero_min = null;
                        fisco['FATTURE'].numero_max = 0;
                        fisco['SCONTRINI'] = new Object();
                        fisco['SCONTRINI'].incasso = 0;
                        fisco['SCONTRINI'].numero_min = null;
                        fisco['SCONTRINI'].numero_max = 0;
                        console.log("STAMPA TOTALI QUERY OK");
                        var filtri = '';
                        var filtri_stampa = '';
                        var filtro_data = '';
                        var where_terminali = "";
                        var where_categorie = "";
                        var where_gruppi = "";
                        var where_giorni = "";
                        if (array_data_partenza.length > 1 && array_data_arrivo.length > 1) {

                            if ($('[name^=gbo_][type="checkbox"]:checked').length > 0) {

                                var array_giorni = new Array();

                                filtri += "<strong>GIORNI:</strong> ";
                                filtri_stampa += "<br/><strong>GIORNI:</strong> ";

                                $('[name^=gbo_][type="checkbox"]:checked').each(function (a, b) {


                                    array_giorni.push("'" + $(b).val() + "'");
                                    filtri += $(b).attr('nome_filtro').toLowerCase().capitalize() + ", ";
                                    filtri_stampa += $(b).attr('nome_filtro').toLowerCase().capitalize() + ", ";

                                });
                                filtri = filtri.slice(0, -2);
                                filtri_stampa = filtri_stampa.slice(0, -2);

                                var array_giorni_joined = array_giorni.join(',');


                                if (array_giorni_joined.length > 0) {
                                    where_giorni = " AND giorno IN (" + array_giorni_joined + ") ";
                                }
                            }

                            $('#stat_art_elenco_centri_backoffice [type="checkbox"]:checked').each(function (a, b) {


                                comanda.array_db_centri.push($(b).val());
                                filtri += $(b).val().toLowerCase().capitalize() + ", ";
                                filtri_stampa += $(b).val().toLowerCase().capitalize() + ", ";

                            });

                            if ($('#stat_art_elenco_terminali_backoffice [type="checkbox"]:checked').length > 0) {

                                var array_terminali = new Array();

                                filtri += "<br/><strong>TERMINALI:</strong> ";
                                filtri_stampa += "<br/><strong>TERMINALI:</strong> ";

                                $('#stat_art_elenco_terminali_backoffice [type="checkbox"]:checked').each(function (a, b) {


                                    array_terminali.push("'" + $(b).val() + "'");
                                    filtri += $(b).val().toLowerCase().capitalize() + ", ";
                                    filtri_stampa += $(b).val().toLowerCase().capitalize() + ", ";

                                });

                                filtri = filtri.slice(0, -2);
                                filtri_stampa = filtri_stampa.slice(0, -2);

                                var array_terminali_joined = array_terminali.join(',');


                                if (array_terminali_joined.length > 0) {
                                    where_terminali = " AND nome_pc IN (" + array_terminali_joined + ") ";
                                }
                            }

                            if ($('#stat_art_elenco_categorie_backoffice [type="checkbox"]:checked').length > 0) {
                                var array_categorie = new Array();
                                filtri += "<br/><strong>CATEGORIE:</strong> ";
                                filtri_stampa += "\n<strong>CATEGORIE:</strong> ";

                                $('#stat_art_elenco_categorie_backoffice [type="checkbox"]:checked').each(function (a, b) {

                                    array_categorie.push("'" + $(b).val() + "'");
                                    filtri += $(b).attr('name').substr(4).toLowerCase().capitalize() + ", ";
                                    filtri_stampa += $(b).attr('name').substr(4).toLowerCase().capitalize() + ", ";


                                });

                                filtri = filtri.slice(0, -2);
                                filtri_stampa = filtri_stampa.slice(0, -2);

                                var array_categorie_joined = array_categorie.join(',');


                                if (array_categorie_joined.length > 0) {
                                    where_categorie = " AND categoria IN (" + array_categorie_joined + ") ";
                                }
                            }

                            if ($('#stat_art_elenco_gruppi_backoffice [type="checkbox"]:checked').length > 0) {
                                var array_gruppi = new Array();
                                filtri += "<br/><strong>GRUPPI:</strong><br/>";
                                filtri_stampa += "\nGRUPPI:\n";

                                $('#stat_art_elenco_gruppi_backoffice [type="checkbox"]:checked').each(function (a, b) {

                                    array_gruppi.push("'" + $(b).val() + "'");
                                    filtri += "- " + $(b).attr('name').substr(4) + "<br/>";
                                    filtri_stampa += "- " + $(b).attr('name').substr(4) + "\n";


                                });

                                var array_gruppi_joined = array_gruppi.join(',');


                                if (array_gruppi_joined.length > 0) {
                                    where_gruppi = " AND gruppo IN (" + array_gruppi_joined + ") ";
                                }
                            }

                            var orario_partenza = $('#stat_art_selettore_ora_partenza_grafico').val().split(':');

                            var yY = array_data_partenza.substr(0, 4);
                            var yD = array_data_partenza.substr(8, 2);
                            var yM = array_data_partenza.substr(5, 2);
                            var yh = orario_partenza[0]/*"00"*/;
                            var ym = orario_partenza[1]/*"00"*/;
                            var ys = "00";
                            var yms = "000";
                            var data_ieri = yY + '' + yM + '' + yD + '' + yh + '' + ym /*+ comanda.ora_servizio*/;


                            var orario_arrivo = $('#stat_art_selettore_ora_arrivo_grafico').val().split(':');

                            var Y = array_data_arrivo.substr(0, 4);
                            var D = array_data_arrivo.substr(8, 2);
                            var M = array_data_arrivo.substr(5, 2);
                            var h = orario_arrivo[0]/*"00"*/;
                            var m = orario_arrivo[1]/*"00"*/;
                            var s = "00";
                            var ms = "000";
                            var data_oggi = Y + '' + M + '' + D + '' + h + '' + m /*+ comanda.ora_servizio*/;

                            var check_orario_multigiorno = $('#stat_art_multigiorno_ora_grafico')[0].checked;

                            if (check_orario_multigiorno === false) {
                                var orario_query = " (data_comanda>='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00') and (data_comanda<='" + Y + "-" + M + "-" + D + "' and ora_comanda <='" + h + ":" + m + ":59') ";
                                var orario_query_tes = " (data_comanda>='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00') and (data_comanda<='" + Y + "-" + M + "-" + D + "' and ora_comanda <='" + h + ":" + m + ":59') ";
                            } else {
                                var orario_query = " data_comanda>='" + yY + "-" + yM + "-" + yD + "' and data_comanda<='" + Y + "-" + M + "-" + D + "' and ora_comanda >='" + yh + ":" + ym + ":00'  and ora_comanda <='" + h + ":" + m + ":59' ";
                                var orario_query_tes = " data_comanda>='" + yY + "-" + yM + "-" + yD + "' and data_comanda<='" + Y + "-" + M + "-" + D + "' and ora_comanda >='" + yh + ":" + ym + ":00'  and ora_comanda <='" + h + ":" + m + ":59' ";

                            }

                            if (array_data_partenza === array_data_arrivo) {
                                if ($('#stat_art_selettore_ora_partenza_grafico').val() !== '00:00' || $('#stat_art_selettore_ora_arrivo_grafico').val() !== '23:59') {
                                    var data_report = yD + '/' + yM + '/' + yY + ' dalle ' + $('#stat_art_selettore_ora_partenza_grafico').val() + ' alle ' + $('#stat_art_selettore_ora_arrivo_grafico').val();
                                } else {
                                    var data_report = yD + '/' + yM + '/' + yY;
                                }
                                filtro_data += data_report + '<br/>';
                            } else {


                                if ($('#stat_art_selettore_ora_partenza_grafico').val() !== '00:00' || $('#stat_art_selettore_ora_arrivo_grafico').val() !== '23:59') {
                                    var data_report = 'da: ' + yD + '/' + yM + '/' + yY + ' alle ' + $('#stat_art_selettore_ora_partenza_grafico').val() + '\na: ' + D + '/' + M + '/' + Y + ' alle ' + $('#stat_art_selettore_ora_arrivo_grafico').val();
                                    filtro_data += 'da: ' + yD + '/' + yM + '/' + yY + ' alle ' + $('#stat_art_selettore_ora_partenza_grafico').val() + ' a: ' + D + '/' + M + '/' + Y + ' alle ' + $('#stat_art_selettore_ora_arrivo_grafico').val() + '<br/>';
                                } else {
                                    var data_report = 'da: ' + yD + '/' + yM + '/' + yY + '\na: ' + D + '/' + M + '/' + Y;
                                    filtro_data += 'da: ' + yD + '/' + yM + '/' + yY + ' a: ' + D + '/' + M + '/' + Y + '<br/>';
                                }
                            }
                        } else {

                            var d = new Date();
                            d.setDate(d.getDate() + 1);
                            var Y = d.getFullYear();
                            var M = addZero((d.getMonth() + 1), 2);
                            var D = addZero(d.getDate(), 2);
                            var h = addZero(d.getHours(), 2);
                            var m = addZero(d.getMinutes(), 2);
                            var s = addZero(d.getSeconds(), 2);
                            var ms = addZero(d.getMilliseconds(), 3);
                            var data_oggi = Y + '' + M + '' + D + '' + comanda.ora_servizio + '00';
                            var y = new Date();
                            y.setDate(y.getDate());
                            var yY = y.getFullYear();
                            var yM = addZero((y.getMonth() + 1), 2);
                            var yD = addZero(y.getDate(), 2);
                            var yh = addZero(y.getHours(), 2);
                            var ym = addZero(y.getMinutes(), 2);
                            var ys = addZero(y.getSeconds(), 2);
                            var yms = addZero(y.getMilliseconds(), 3);
                            var data_ieri = yY + '' + yM + '' + yD + '' + comanda.ora_servizio + '00';

                            var check_orario_multigiorno = $('#stat_art_multigiorno_ora_grafico')[0].checked;

                            if (check_orario_multigiorno === false) {
                                var orario_query = " (data_comanda>='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00') and (data_comanda<='" + yY + "-" + yM + "-" + yD + "' and ora_comanda <='" + h + ":" + m + ":59') ";
                                var orario_query_tes = " (data_comanda>='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00') and (data_comanda<='" + yY + "-" + yM + "-" + yD + "' and ora_comanda <='" + h + ":" + m + ":59') ";

                            } else {
                                var orario_query = " data_comanda>='" + yY + "-" + yM + "-" + yD + "' and data_comanda<='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00'  and ora_comanda <='" + h + ":" + m + ":59' ";
                                var orario_query_tes = " data_comanda>='" + yY + "-" + yM + "-" + yD + "' and data_comanda<='" + yY + "-" + yM + "-" + yD + "' and ora_comanda >='" + yh + ":" + ym + ":00'  and ora_comanda <='" + h + ":" + m + ":59' ";

                            }

                            var data_report = yD + '/' + yM + '/' + yY;
                            filtro_data += data_report + '<br/>';
                        }
//CICLO DB CENTRALE WHERE STATO RECORD CHIUSO AND POSIZIONE CONTO

                        var tempo_iniziale = new Date().getTime();

                        var orario_multigiorno = $('#stat_art_multigiorno_ora_grafico')[0].checked;

                        /* String */
                        var condizione_fiscalizzata_sn = " and fiscalizzata_sn='NONVALIDA' "

                        var password = CryptoJS.MD5($('#stat_art_campo_password').val()).toString();
                        var pass_statistiche_teste = CryptoJS.MD5("FI" + comanda.numero_licenza_cliente).toString();

                        var pass_statistiche_comande = CryptoJS.MD5("FI" + comanda.numero_licenza_cliente + "TOTALE").toString();
                        if (comanda.numero_licenza_cliente === "0595") {
                            pass_statistiche_comande = CryptoJS.MD5("FI" + comanda.numero_licenza_cliente + "IMPERIAL").toString();
                        }

                        if (password === pass_statistiche_teste) {
                            condizione_fiscalizzata_sn = " and fiscalizzata_sn='S' ";
                        } else if (comanda.visualizza_tutti_venduti === true && password === pass_statistiche_comande)
                        {
                            condizione_fiscalizzata_sn = " and (fiscalizzata_sn='S' OR tipo_ricevuta='conto' OR tipo_ricevuta='STORICIZZAZIONE DI PROVA') ";
                        } else {
                            $('.loader2').hide();
                            bootbox.alert("Inserire una password valida!");
                            return false;
                        }


                        var query_articoli = "select numero, " + comanda.lingua_stampa + " from nomi_portate order by numero asc;";
                        comanda.sincro.query_cassa(query_articoli, 1000000, function (result_portate) {

                            var query_articoli = "select id, descrizione from categorie order by id asc;";
                            comanda.sincro.query_cassa(query_articoli, 1000000, function (result_categorie) {

                                //Nomi categorie da id
                                var nomi_categorie = new Object();
                                for (var a in result_categorie) {
                                    nomi_categorie[result_categorie[a].id] = result_categorie[a].descrizione;
                                }

                                var query_articoli = "select id, nome from gruppi_statistici order by id asc;";
                                comanda.sincro.query_cassa(query_articoli, 1000000, function (result_gruppi) {


                                    var nomi_gruppi = new Object();
                                    for (var a in result_gruppi) {
                                        nomi_gruppi[result_gruppi[a].id] = result_gruppi[a].nome;
                                    }

                                    var query_articoli = "update comanda set sconto_perc='0.00' where sconto_perc is null;";
                                    comanda.sincro.query_cassa(query_articoli, 1000000, function () {

                                        var query_articoli2 = "create TEMPORARY TABLE test SELECT  '' as centro,'' as indice,costo,id,gruppo,giorno,sconto_imp," + prezzo_vero_2 + " as valore_sconto, sum(quantita) as quantita_totale, sum(netto) as totale_sconti FROM back_office WHERE  desc_art NOT LIKE '%COD:%' and  posizione='SCONTO' " + where_terminali + where_categorie + where_gruppi + where_giorni + " " + condizione_fiscalizzata_sn + " and " + orario_query + " group by " + prezzo_vero_2 + " ";
                                        query_articoli2 += " UNION ALL                     SELECT  '' as centro,'' as indice,costo,id,gruppo,giorno,sconto_imp," + prezzo_vero_2 + " as valore_sconto, sum(quantita) as quantita_totale, sum(netto) as totale_sconti FROM comanda_temp WHERE  desc_art NOT LIKE '%COD:%' and  posizione='SCONTO' " + where_terminali + where_categorie + where_gruppi + where_giorni + " " + condizione_fiscalizzata_sn + " and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and tipo_ricevuta!='' and " + orario_query + " group by " + prezzo_vero_2 + " ;";

                                        if (comanda.array_db_centri.length > 0) {

                                            var query_articoli2 = "create TEMPORARY TABLE test ";
                                            var i = 1;

                                            comanda.array_db_centri.forEach(centro => {
                                                query_articoli2 += "SELECT '" + centro + "' as centro,'' as indice,costo,id,gruppo,giorno,sconto_imp," + prezzo_vero_2 + " as valore_sconto, sum(quantita) as quantita_totale, sum(netto) as totale_sconti FROM " + centro + ".back_office WHERE  desc_art NOT LIKE '%COD:%' and  posizione='SCONTO' " + where_terminali + where_categorie + where_gruppi + where_giorni + " " + condizione_fiscalizzata_sn + " and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and tipo_ricevuta!='' and " + orario_query + " group by " + prezzo_vero_2 + " UNION ALL ";
                                                query_articoli2 += " UNION ALL ";
                                                query_articoli2 += "SELECT '" + centro + "' as centro,'' as indice,costo,id,gruppo,giorno,sconto_imp," + prezzo_vero_2 + " as valore_sconto, sum(quantita) as quantita_totale, sum(netto) as totale_sconti FROM " + centro + ".comanda_temp WHERE  desc_art NOT LIKE '%COD:%' and  posizione='SCONTO' " + where_terminali + where_categorie + where_gruppi + where_giorni + " " + condizione_fiscalizzata_sn + " and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and tipo_ricevuta!='' and " + orario_query + " group by " + prezzo_vero_2 + " ";

                                                if (i !== comanda.array_db_centri.length) {
                                                    query_articoli2 += " UNION ALL ";
                                                }

                                                i++;

                                            });
                                        }



                                        //DA ORDINARE MANUALMENTE
                                        //CAMBIARE WHERE GIORNI E WHERE TERMINALI

                                        //FARE PROMISE 
                                        var p1 = new Promise(function (resolve, reject) {
                                            //19/10/2020
                                            comanda.sincro.query_statistiche(JSON.stringify([query_articoli2]), 1000000, function (result_sconti) {

                                                var rs = new Array();

                                                result_sconti.forEach(function (e) {
                                                    if (e.sconto_imp === "P") {
                                                        e.valore_sconto = e.valore_sconto + " %";
                                                    }
                                                    rs.push(e);
                                                });

                                                resolve(rs);
                                            },"SCONTI");
                                        });


                                        var giorno_incasso_gb = "";
                                        if (array_giorni_joined !== undefined) {
                                            giorno_incasso_gb = "and (giorno_incasso IN (" + array_giorni_joined + ") or giorno_emissione IN(" + array_giorni_joined + "))";
                                        }

                                        var terminali_gb = "";
                                        if (array_terminali_joined !== undefined) {
                                            terminali_gb = "and (nome_pc_incasso IN (" + array_terminali_joined + ") or nome_pc_emissione IN (" + array_terminali_joined + "))";
                                        }
                                        var query_articoli = "SELECT * FROM gestione_buoni_sconto WHERE  ((data_emissione_ascii>='" + yY + "" + yM + "" + yD + "" + yh + "" + ym + "" + ys + "' and data_emissione<'" + Y + "" + M + "" + D + "" + h + "" + m + "" + s + "') or (data_utilizzo_ascii>='" + yY + "" + yM + "" + yD + "" + yh + "" + ym + "" + ys + "' and data_utilizzo_ascii<'" + Y + "" + M + "" + D + "" + h + "" + m + "" + s + "')) " + giorno_incasso_gb + " " + terminali_gb + " order by percentuale_buono asc;";


                                        var p2 = new Promise(function (resolve, reject) {
                                            comanda.sincro.query_centrale(query_articoli, function (result_buoni_sconto) {
                                                resolve(result_buoni_sconto);
                                            });

                                        });


                                        var query_articoli_zero = "";
                                        if (comanda.escludi_art_zero_statistiche === true) {
                                            query_articoli_zero = " and " + prezzo_vero_2 + "!=0 ";
                                        }

                                        var ordinamento = "descrizione";
                                        //SE E' NEVODI ORDINA PER COD.ARTICOLO
                                        if (comanda.numero_licenza_cliente === "0568") {
                                            ordinamento = "cod_articolo";
                                        }

                                        if (ordinamento === "cod_articolo") {
                                            prodotti = new Object();
                                            alasql.tables.prodotti.data.forEach(function (prodotto) {
                                                prodotti[prodotto.id] = new Object();
                                                prodotti[prodotto.id] = prodotto.descrizione;
                                            });
                                        }


                                        let query_articoli33 = "create TEMPORARY TABLE test2 select '' as centro,'' as indice,costo,data_comanda,id,tipologia_fattorino,gruppo,giorno,fiscalizzata_sn,tipo_ricevuta,n_fiscale,operatore,ntav_comanda,quantita,desc_art,sconto_perc,perc_iva,categoria,dest_stampa,portata," + prezzo_vero_2 + " as prezzo_un,QTA,tipo_consegna,cod_articolo,totale,totale_costo,totale_ricavo  from back_office  where  (posizione='CONTO' OR posizione='COPERTO') " + where_terminali + where_categorie + where_gruppi + where_giorni + " " + condizione_fiscalizzata_sn + " and " + orario_query + " and portata!='ZZZ' and cod_promo!=1 " + query_articoli_zero + " group by id asc,desc_art asc,prezzo_un asc,sconto_perc asc ";
                                        query_articoli33 += " UNION ALL                     select  '' as centro,'' as indice,costo,data_comanda,id,tipologia_fattorino,gruppo,giorno,fiscalizzata_sn,tipo_ricevuta,n_fiscale,operatore,ntav_comanda,quantita,desc_art,sconto_perc,perc_iva,categoria,dest_stampa,portata," + prezzo_vero_2 + " as prezzo_un,QTA,tipo_consegna,cod_articolo,(prezzo_un*sum(quantita))/100*(100-sconto_perc)  as totale,costo*sum(quantita) as totale_costo,((prezzo_un*sum(quantita))/100*(100-sconto_perc))-(costo*sum(quantita))  as totale_ricavo from comanda_temp where (posizione='CONTO' OR posizione='COPERTO') " + where_terminali + where_categorie + where_gruppi + where_giorni + " " + condizione_fiscalizzata_sn + " and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and tipo_ricevuta!='' and " + orario_query + " and portata!='ZZZ'  and cod_promo!=1 " + query_articoli_zero + " group by id asc,desc_art asc,prezzo_un asc,sconto_perc asc;";


                                        if (comanda.array_db_centri.length > 0) {
                                            query_articoli33 = "create TEMPORARY TABLE test2 ";
                                            var i = 1;
                                            comanda.array_db_centri.forEach(centro => {
                                                query_articoli33 += "select '" + centro + "' as centro,'' as indice,costo,data_comanda,id,tipologia_fattorino,gruppo,giorno,fiscalizzata_sn,tipo_ricevuta,n_fiscale,operatore,ntav_comanda,quantita,desc_art,sconto_perc,perc_iva,categoria,dest_stampa,portata," + prezzo_vero_2 + " as prezzo_un,QTA,tipo_consegna,cod_articolo,totale,totale_costo,totale_ricavo from " + centro + ".back_office      where (posizione='CONTO' OR posizione='COPERTO') " + where_terminali + where_categorie + where_gruppi + where_giorni + " " + condizione_fiscalizzata_sn + "  and " + orario_query + " and portata!='ZZZ'  and cod_promo!=1 " + query_articoli_zero + " group by id asc,desc_art asc,prezzo_un asc,sconto_perc asc ";
                                                query_articoli33 += " UNION ALL ";
                                                query_articoli33 += "select '" + centro + "' as centro,'' as indice,costo,data_comanda,id,tipologia_fattorino,gruppo,giorno,fiscalizzata_sn,tipo_ricevuta,n_fiscale,operatore,ntav_comanda,quantita,desc_art,sconto_perc,perc_iva,categoria,dest_stampa,portata," + prezzo_vero_2 + " as prezzo_un,QTA,tipo_consegna,cod_articolo,(prezzo_un*sum(quantita))/100*(100-sconto_perc) as totale,costo*sum(quantita)) as totale_costo,((prezzo_un*sum(quantita))/100*(100-sconto_perc))-(costo*sum(quantita))  as totale_ricavo from " + centro + ".comanda_temp where (posizione='CONTO' OR posizione='COPERTO') " + where_terminali + where_categorie + where_gruppi + where_giorni + " " + condizione_fiscalizzata_sn + " and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and tipo_ricevuta!='' and " + orario_query + " and portata!='ZZZ'  and cod_promo!=1 " + query_articoli_zero + " group by id asc,desc_art asc,prezzo_un asc,sconto_perc asc ";

                                                if (i !== comanda.array_db_centri.length) {
                                                    query_articoli33 += " UNION ALL ";
                                                }

                                                i++;

                                            });
                                        }
                                        query_articoli33 += ";";


                                        //QUI METTO GLI INDICI

                                        let periodo_confronto = $("#stat_art_periodo_confronto").val();

                                        let query_periodo_confronto = "";
                                        let indice = "";
                                        let tab = new Object();

                                        switch (periodo_confronto) {
                                            case "1":
                                                query_periodo_confronto = "giorno as indice,";
                                                indice = "giorno";
                                                break;
                                            case "2":
                                                query_periodo_confronto = "substr(data_comanda,6,2) as indice,";
                                                indice = "substr(data_comanda,6,2)";
                                                break;
                                            case "3":
                                                query_periodo_confronto = "substr(data_comanda,1,4) as indice,";
                                                indice = "substr(data_comanda,1,4)";
                                                break;
                                        }

                                        let query_articoli3 = "create TEMPORARY TABLE test select centro," + query_periodo_confronto + "id,tipologia_fattorino,gruppo,giorno,fiscalizzata_sn,tipo_ricevuta,n_fiscale,operatore,ntav_comanda,sum(quantita) as quantita,desc_art,sconto_perc,perc_iva,categoria,dest_stampa,portata, prezzo_un,QTA,tipo_consegna,cod_articolo,sum(totale) as totale,costo,sum(totale_costo) as totale_costo,sum(totale_ricavo) as totale_ricavo  from test2 group by " + indice + " asc, desc_art asc,centro asc;";


                                        var p3 = new Promise(function (resolve, reject) {

                                            comanda.sincro.query_statistiche(JSON.stringify(["TRUNCATE test;", "TRUNCATE test2;", query_articoli33, query_articoli3]), 1000000, function (result) {

                                                resolve(result);

                                            });
                                        });

                                        //AGGIUNGERE TUTTE LE PROMISE
                                        p3.then(function (result) {

                                            result.forEach((v, i) => {

                                                if (tab[v.desc_art + " - " + v.centro] === undefined) {
                                                    tab[v.desc_art + " - " + v.centro] = new Object();

                                                    tab[v.desc_art + " - " + v.centro].prezzo_totale = 0.00;
                                                    tab[v.desc_art + " - " + v.centro].quantita_totale = 0;
                                                    tab[v.desc_art + " - " + v.centro].costo = 0;
                                                    tab[v.desc_art + " - " + v.centro].totale_costo = 0;
                                                    tab[v.desc_art + " - " + v.centro].totale_ricavo = 0;
                                                }

                                                tab[v.desc_art + " - " + v.centro].prezzo_totale += v.totale;
                                                tab[v.desc_art + " - " + v.centro].quantita_totale += v.quantita;
                                                tab[v.desc_art + " - " + v.centro].costo = parseFloat(v.costo);
                                                tab[v.desc_art + " - " + v.centro].totale_costo += v.totale_costo;
                                                tab[v.desc_art + " - " + v.centro].totale_ricavo += v.totale_ricavo;
                                                /*if(v.perc_ricavo===null){
                                                    v.perc_ricavo=0;
                                                }
                                                tab[v.desc_art + " - " + v.centro].percentuale_ricavo = v.perc_ricavo;*/

                                            });

                                            //console.log(tab);

                                            let tabella_finale = "<table class='table table-striped table-hover table-responsive'>";



                                            tabella_finale += "<tr>";
                                            tabella_finale += "<th>Descrizione</th>";
                                            tabella_finale += "<th>T.Qta</th>";
                                            tabella_finale += "<th>Costo</th>";
                                            tabella_finale += "<th>T.Costo</th>";
                                            tabella_finale += "<th>T.Valore</th>";
                                            tabella_finale += "<th>T.Ricavo</th>";
                                            tabella_finale += "<th>Perc.Ricavo</th>";
                                            tabella_finale += "</tr>";


                                            for (let articolo in tab) {
                                                
                                                let percentuale_ricavo=((tab[articolo].prezzo_totale-tab[articolo].totale_costo)/tab[articolo].prezzo_totale)*100;
                                                if(isNaN(percentuale_ricavo)){
                                                    percentuale_ricavo=0;
                                                }

                                                tabella_finale += "<tr>";
                                                tabella_finale += "<td>" + articolo + "</td>";
                                                tabella_finale += "<td>" + tab[articolo].quantita_totale + "</td>";
                                                tabella_finale += "<td>" + tab[articolo].costo.toFixed(2) + "</td>";
                                                tabella_finale += "<td>" + tab[articolo].totale_costo.toFixed(2) + "</td>";
                                                tabella_finale += "<td>" + tab[articolo].prezzo_totale.toFixed(2) + "</td>";
                                                tabella_finale += "<td>" + tab[articolo].totale_ricavo.toFixed(2) + "</td>";
                                                tabella_finale += "<td>" +  percentuale_ricavo.toFixed(2) + "</td>";
                                                tabella_finale += "</tr>";


                                            }

                                            tabella_finale += "</table>";

                                            $('#stat_art_tabella_venduti_giorno').html(tabella_finale);
                                            $('.loader2').hide();

                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            } else
            {
                bootbox.alert("Errore di Runtime nella sincronia dei dati del giorno.");
            }
        },
        error: function () {
            bootbox.alert("Errore di Ajax nella sincronia dei dati del giorno.");
        }
    });


}

function estrai_statistiche_articolo() {
    let tipo_tabella = $("#tipologia_tabella_statistiche_articolo").val();

    switch (tipo_tabella)
    {
        case "2":
            statistiche_margini_guadagno();
            break;

        case "1":
        default:
            statistiche_articolo();

    }
}

function stampa_statistiche_A4_statistiche_articolo() {

    let layout_stampa = "";

    let anno_p = $("#stat_art_selettore_data_partenza_grafico").val().substr(0, 4);
    let mese_p = $("#stat_art_selettore_data_partenza_grafico").val().substr(5, 2);
    let giorno_p = $("#stat_art_selettore_data_partenza_grafico").val().substr(8);

    let anno_a = $("#stat_art_selettore_data_arrivo_grafico").val().substr(0, 4);
    let mese_a = $("#stat_art_selettore_data_arrivo_grafico").val().substr(5, 2);
    let giorno_a = $("#stat_art_selettore_data_arrivo_grafico").val().substr(8);

    layout_stampa += "<h2>Periodo: da " + giorno_p + "/" + mese_p + "/" + anno_p + " a " + giorno_a + "/" + mese_a + "/" + anno_a + "</h2>";

    if ($("#stat_art_multigiorno_ora_grafico").prop("checked") === true) {
        layout_stampa += "<h2>Limite orario giornaliero attivo</h2>";
    } else {
        layout_stampa += "<h2>Limite orario giornaliero NON attivo</h2>";
    }

    let periodo = "";
    switch ($("#stat_art_periodo_confronto").val()) {
        case "1":
            periodo = "Confronto per Giorno";
            break;
        case "2":
            periodo = "Confronto per Mese";
            break;
        case "3":
            periodo = "Confronto per Anno";
            break;
    }
    layout_stampa += "<h2>" + periodo + "</h2>";

    let gg = new Array();

    if ($("input[name='gbo_1']:visible").prop("checked") === true) {
        gg.push("Lunedi");
    }
    if ($("input[name='gbo_2']:visible").prop("checked") === true) {
        gg.push("Martedi");
    }
    if ($("input[name='gbo_3']:visible").prop("checked") === true) {
        gg.push("Mercoledi");
    }
    if ($("input[name='gbo_4']:visible").prop("checked") === true) {
        gg.push("Giovedi");
    }
    if ($("input[name='gbo_5']:visible").prop("checked") === true) {
        gg.push("Venerdi");
    }
    if ($("input[name='gbo_6']:visible").prop("checked") === true) {
        gg.push("Sabato");
    }
    if ($("input[name='gbo_7']:visible").prop("checked") === true) {
        gg.push("Domenica");
    }

    if (gg.length > 0) {
        layout_stampa += "<h3>Giorni visualizzati: " + gg.join(', ') + "</h3>";
    }

    if ($("#stat_art_elenco_centri_backoffice input[type='checkbox']:checked").length > 1) {
        layout_stampa += "<h3>Visualizzazione Multi-Centro</h3>";
    }

    layout_stampa += $('#stat_art_tabella_venduti_giorno').html();

    $('.print').html(layout_stampa);

    $('.print table td').css("border", "1px solid black");
    $('.print table td').css("padding", "1px");

    $('.print').css("font-size", "10px");

    $('.screen').hide();
    $('.print').show();

    window.print();

    $('.screen').show();
    $('.print').hide();

}

function statistiche_articolo_csv() {

    let tabella = '';


    $('#stat_art_tabella_venduti_giorno table tr').each((i1, r) => {

        $(r).find('td,th').each((i2, c) => {
            let valore = $(c).html().replace(/[\+\-\=\*\(]/g, '').trim();

            tabella += valore + ';';
        });

        tabella += '\t\t\t\n';

    });



    console.log(tabella);

    let nome_file = new Date().format("ddmmyyyyHHMMss") + ".csv";

    $.post("./classi_php/salva_file_generico.php", {nome: nome_file, contenuto: tabella}, function (r) {

        bootbox.alert("File salvato in /file_salvati/" + nome_file);

    });


}