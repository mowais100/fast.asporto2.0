/* global comanda */

var righe_forno_in_corso = false;

function righe_forno(bool_provenienza_socket) {

    if (true) {



        var d = new Date();
        var Y = d.getFullYear();
        var M = addZero((d.getMonth() + 1), 2);
        var D = addZero(d.getDate(), 2);
        var h = addZero(d.getHours(), 2);
        var m = addZero(d.getMinutes(), 2);
        var s = addZero(d.getSeconds(), 2);
        var ms = addZero(d.getMilliseconds(), 3);
        var data_oggi = Y + '' + M + '' + D;
        var query_tot_pizze_da_fare = "select tipo_impasto,sum(quantita) as quantita from comanda where substr(comanda.id,5,8)='" + data_oggi + "' and portata='P' and (substr(desc_art,1,1)!='+' and substr(desc_art,1,1)!='-' and substr(desc_art,1,1)!='=' and substr(desc_art,1,1)!='*' and substr(desc_art,1,1)!='x' and substr(desc_art,1,1)!='(') and (stato_record='FORNO' or stato_record='ATTIVO') and tipo_consegna!='NIENTE' and tipo_consegna!='' group by comanda.portata,comanda.tipo_impasto;";
        comanda.sincro.query_sync(query_tot_pizze_da_fare, function (risultato) {

            var quantita_pizze_per_impasto = new Object();
            var quantita_pizze_per_impasto_normali = 0;

            risultato.forEach(function (set) {
                if (set.tipo_impasto === '') {

                    quantita_pizze_per_impasto_normali += parseFloat(set.quantita);
                } else
                {
                    if (quantita_pizze_per_impasto[set.tipo_impasto] === undefined) {
                        quantita_pizze_per_impasto[set.tipo_impasto] = 0;
                    }
                    quantita_pizze_per_impasto[set.tipo_impasto] += parseFloat(set.quantita);
                }
            });

            $('#tot_pizze_da_fare').html('0');

            var testo_forno = "N: " + quantita_pizze_per_impasto_normali;

            var acapo = 2;
            for (var index in quantita_pizze_per_impasto) {
                var nome_impasto = "";
                switch (index) {
                    case "5":
                        nome_impasto = "5";
                        break;
                    case "F":
                        nome_impasto = "F";
                        break;
                    case "G":
                        nome_impasto = "G";
                        break;
                    case "I":
                        nome_impasto = "I";
                        break;
                    case "K":
                        nome_impasto = "K";
                        break;
                    case "M":
                        nome_impasto = "P";
                        break;
                    case "S":
                        nome_impasto = "S";
                        break;
                }

                testo_forno += "&nbsp;&nbsp;&nbsp;" + nome_impasto + ": " + quantita_pizze_per_impasto[index];


                acapo++;
            }

            $('#tot_pizze_da_fare').html(testo_forno);

        });
        var query_forno = " select comanda.id,comanda.desc_art,comanda.portata,clienti.citta,clienti.indirizzo,clienti.numero,comanda.tipo_consegna,comanda.ora_consegna,comanda.nome_comanda,sum(comanda.quantita) as quantita from comanda,clienti where substr(comanda.id,5,8)='" + data_oggi + "' and clienti.id=comanda.rag_soc_cliente and (stato_record='FORNO' or stato_record='ATTIVO') and comanda.tipo_consegna!='NIENTE' and comanda.tipo_consegna!='' group by comanda.id,comanda.nome_comanda,comanda.ora_consegna\n\
                                        union all\n\
                                        select id,comanda.desc_art,comanda.portata,'','','',tipo_consegna,ora_consegna,rag_soc_cliente,sum(quantita) as quantita from comanda where substr(comanda.id,5,8)='" + data_oggi + "' and SUBSTR(rag_soc_cliente,1,1)='P' and (stato_record='FORNO' or stato_record='ATTIVO') and comanda.tipo_consegna!='NIENTE' and comanda.tipo_consegna!='' group by comanda.id,comanda.nome_comanda,comanda.ora_consegna order by comanda.ora_consegna asc;";
        console.log("QUERY RIGHE FORNO", query_forno);
        comanda.sincro.query_sync(query_forno, function (risultato) {

            $(".forno_pag table tbody tr").remove();
            $(".forno_popup table tbody tr").remove();
            var ultima_ora_consegna = '';
            var righe_forno_html = '';
            risultato.forEach(function (obj) {
                var pizze = 0;
                var spazio_forno = 0;
                var altri_articoli = 0;
                var query_conti = "select desc_art,portata,quantita,spazio_forno from comanda where id='" + obj['comanda.id'] + "' and (stato_record='FORNO' or stato_record='ATTIVO') and tipo_consegna!='NIENTE' and tipo_consegna!='';";
                comanda.sincro.query_sync(query_conti, function (conti) {

                    var query_tipo_ricevuta = 'select tipo_ricevuta,stampata_sn from comanda where id="' + obj['comanda.id'] + '" and (stato_record="FORNO" or stato_record="ATTIVO") and tipo_consegna!="NIENTE" and tipo_consegna!="" order by tipo_ricevuta asc,stampata_sn asc limit 1;';
                    var query_raggruppamento_quantita_orario = "select sum(spazio_forno) as quantita from comanda where ora_consegna='" + obj['comanda.ora_consegna'] + "' and substr(comanda.id,5,8)='" + data_oggi + "' and portata='P' and (substr(desc_art,1,1)!='+' and substr(desc_art,1,1)!='-' and substr(desc_art,1,1)!='=' and substr(desc_art,1,1)!='*' and substr(desc_art,1,1)!='x' and substr(desc_art,1,1)!='(') and (stato_record='FORNO' or stato_record='ATTIVO') and tipo_consegna!='NIENTE' and tipo_consegna!='' group by comanda.ora_consegna order by comanda.ora_consegna asc;";
                    comanda.sincro.query_sync(query_raggruppamento_quantita_orario, function (risultato_raggruppamento_quantita_orario) {
                        comanda.sincro.query_sync(query_tipo_ricevuta, function (risultato_tipo_ricevuta) {

                            conti.forEach(function (calcoli) {

                                console.log("CALCOLI", calcoli);
                                if (calcoli.desc_art.substr(0, 1) === '+' || calcoli.desc_art.substr(0, 1) === '-' || calcoli.desc_art.substr(0, 1) === '*' || calcoli.desc_art.substr(0, 1) === '=' || calcoli.desc_art.substr(0, 1) === 'x' || calcoli.desc_art.substr(0, 1) === '(')
                                {
                                    //UN CAZZO
                                } else if (calcoli.portata !== 'P')
                                {
                                    altri_articoli += parseInt(calcoli.quantita);
                                } else
                                {
                                    pizze += parseInt(calcoli.quantita);
                                    if (isNaN(parseInt(calcoli.spazio_forno))) {
                                        spazio_forno++;
                                    } else {
                                        spazio_forno += parseInt(calcoli.spazio_forno);
                                    }
                                }
                            });

                            var img_consegna;
                            if (obj['comanda.tipo_consegna'] === "DOMICILIO")
                            {
                                img_consegna = "./img/SCOOTER50x50.png";
                            } else if (obj['comanda.tipo_consegna'] === "RITIRO")
                            {
                                img_consegna = "./img/CasaBianca45su51.png";
                            }



                            console.log("ORA CONSEGNA 1", obj['comanda.ora_consegna']);
                            if (obj['comanda.ora_consegna'] !== ultima_ora_consegna) {
                                righe_forno_html += "<tr style='height:1mm;'></tr>";
                                righe_forno_html += "<tr style='height:1mm;'></tr>";
                            }


                            var tipo_ricevuta = '';

                            if (risultato_tipo_ricevuta[0].stampata_sn === "S") {
                                tipo_ricevuta += '<span style="font-size:14px;"><strong>C</strong></span>';
                            }


                            switch (risultato_tipo_ricevuta[0].tipo_ricevuta)
                            {
                                case "conto":
                                case "CONTO":
                                    tipo_ricevuta += "<img style='width: auto;height: 50%;' src='./img/BLU_51x51.png' />";
                                    break;
                                case "scontrino":
                                case "fattura":
                                case "SCONTRINO":
                                case "FATTURA":
                                    tipo_ricevuta += "<img style='width: auto;height: 50%;' src='./img/ROSSO_51x51.png' />";
                                    break;
                            }

                            var qnt_ragg_ora = 0;
                            if (risultato_raggruppamento_quantita_orario[0] !== undefined && risultato_raggruppamento_quantita_orario[0].quantita !== undefined && !isNaN(risultato_raggruppamento_quantita_orario[0].quantita))
                            {
                                qnt_ragg_ora = risultato_raggruppamento_quantita_orario[0].quantita;
                            }

                            var colore_riga = '';
                            switch (true) {

                                case (qnt_ragg_ora >= 1 && qnt_ragg_ora <= 5):
                                    colore_riga = "#008966";
                                    break;
                                case (qnt_ragg_ora >= 6 && qnt_ragg_ora <= 8):
                                    colore_riga = "#c87f0a";
                                    break;
                                case (qnt_ragg_ora >= 9):
                                    colore_riga = "#d62c1a";
                                    break;
                                default:
                                    colore_riga = "initial";
                            }

                            ultima_ora_consegna = obj['comanda.ora_consegna'];
                            console.log("ORA CONSEGNA 2", ultima_ora_consegna);

                            righe_forno_html += "<tr  style='background-color:" + colore_riga + "' class='" + obj['comanda.id'] + "'><td class='text-center'>" + obj['comanda.ora_consegna'] + "</td><td class='text-center'>" + pizze + " (+" + altri_articoli + ")</td><td class='text-center'>" + spazio_forno + "</td><td class='nome_comanda text-center'>" + obj['comanda.nome_comanda'].toUpperCase() + "</td><td class='text-center' style='padding: 0;'><img src='" + img_consegna + "' /></td ><td class='tipo_ricevuta text-center'>" + tipo_ricevuta + "</td><td class='escludi-listener'><input class='form-control' type='checkbox' style='margin:0;padding:0;' " + comanda.evento + "='cancella_riga_forno(\"" + obj['comanda.id'] + "\")' /></td></tr>";
                            $(".forno_pag table tbody").append("<tr  style='background-color:" + colore_riga + "' class='" + obj['comanda.id'] + "'><td class='text-center'>" + obj['comanda.ora_consegna'] + "</td><td class='text-center'>" + pizze + " (+" + altri_articoli + ")</td><td class='text-center'>" + spazio_forno + "</td><td class='nome_comanda text-center'>" + obj['comanda.nome_comanda'].toUpperCase() + "</td><td class='text-center' style='padding: 0;'><img src='" + img_consegna + "' /></td ><td class='tipo_ricevuta text-center'>" + tipo_ricevuta + "</td><td class='escludi-listener'><input class='form-control' type='checkbox' style='margin:0;padding:0;' " + comanda.evento + "='cancella_riga_forno(\"" + obj['comanda.id'] + "\")' /></td></tr>");
                            if (obj['comanda.tipo_consegna'] !== "RITIRO" && obj['clienti.citta'] !== undefined && obj['clienti.citta'] !== null && obj['clienti.citta'].length > 0)
                            {
                                righe_forno_html += "<tr style='background-color:" + colore_riga + "' class='" + obj['comanda.id'] + "'><td class='text-center'>" + obj['comanda.ora_consegna'] + "</td><td class='text-center'>" + pizze + " (+" + altri_articoli + ")</td><td class='text-center'>" + spazio_forno + "</td><td class='text-center' style='padding: 0;'><img src='" + img_consegna + "' /></td><td class='text-center'>" + obj['clienti.indirizzo'] + " ," + obj['clienti.numero'] + " - " + obj['clienti.citta'] + "</td><td class='nome_comanda text-center'>" + obj['comanda.nome_comanda'].toUpperCase() + "</td><td class='tipo_ricevuta text-center'>" + tipo_ricevuta + "</td><td  class='escludi-listener'><input class='form-control' type='checkbox' style='margin:0;padding:0;' " + comanda.evento + "='cancella_riga_forno(\"" + obj['comanda.id'] + "\")' /></td></tr>";
                                $(".forno_popup table tbody").append("<tr style='background-color:" + colore_riga + "' class='" + obj['comanda.id'] + "'><td class='text-center'>" + obj['comanda.ora_consegna'] + "</td><td class='text-center'>" + pizze + " (+" + altri_articoli + ")</td><td class='text-center'>" + spazio_forno + "</td><td class='text-center' style='padding: 0;'><img src='" + img_consegna + "' /></td><td class='text-center'>" + obj['clienti.indirizzo'] + " ," + obj['clienti.numero'] + " - " + obj['clienti.citta'] + "</td><td class='nome_comanda text-center'>" + obj['comanda.nome_comanda'].toUpperCase() + "</td><td class='tipo_ricevuta text-center'>" + tipo_ricevuta + "</td><td  class='escludi-listener'></td></tr>");
                            } else
                            {
                                righe_forno_html += "<tr style='background-color:" + colore_riga + "' class='" + obj['comanda.id'] + "'><td class='text-center'>" + obj['comanda.ora_consegna'] + "</td><td class='text-center'>" + pizze + " (+" + altri_articoli + ")</td><td class='text-center'>" + spazio_forno + "</td><td class='text-center' style='padding: 0;'><img src='" + img_consegna + "' /></td><td class='text-center'>RITIRO</td><td class='nome_comanda text-center'>" + obj['comanda.nome_comanda'].toUpperCase() + "</td><td class='tipo_ricevuta text-center'>" + tipo_ricevuta + "</td><td class='escludi-listener' ><input class='form-control' type='checkbox' style='margin:0;padding:0;' " + comanda.evento + "='cancella_riga_forno(\"" + obj['comanda.id'] + "\"></td></tr>";
                                $(".forno_popup table tbody").append("<tr style='background-color:" + colore_riga + "' class='" + obj['comanda.id'] + "'><td class='text-center'>" + obj['comanda.ora_consegna'] + "</td><td class='text-center'>" + pizze + " (+" + altri_articoli + ")</td><td class='text-center'>" + spazio_forno + "</td><td class='text-center' style='padding: 0;'><img src='" + img_consegna + "' /></td><td class='text-center'>RITIRO</td><td class='nome_comanda text-center'>" + obj['comanda.nome_comanda'].toUpperCase() + "</td><td class='tipo_ricevuta text-center'>" + tipo_ricevuta + "</td><td class='escludi-listener' ></td></tr>");
                            }


                        });
                    });
                });
            });

            //RIGHE CONSEGNA
            $(".consegna table tbody tr").remove();
            var ultima_ora_consegna = '';
            risultato.forEach(function (obj) {

                var pizze = 0;
                var altri_articoli = 0;
                var query_conti = "select desc_art,portata,quantita from comanda where id='" + obj['comanda.id'] + "' and (stato_record='FORNO' or stato_record='ATTIVO');";
                comanda.sincro.query_sync(query_conti, function (conti) {


                    conti.forEach(function (calcoli) {

                        console.log("CALCOLI", calcoli);
                        if (calcoli.desc_art.substr(0, 1) === '+' || calcoli.desc_art.substr(0, 1) === '-' || calcoli.desc_art.substr(0, 1) === '*' || calcoli.desc_art.substr(0, 1) === '=' || calcoli.desc_art.substr(0, 1) === 'x' || calcoli.desc_art.substr(0, 1) === '(')
                        {
                            //UN CAZZO
                        } else if (calcoli.portata !== 'P')
                        {
                            altri_articoli += parseInt(calcoli.quantita);
                        } else
                        {
                            pizze += parseInt(calcoli.quantita);
                        }
                    });

                    if (obj['comanda.tipo_consegna'] === "DOMICILIO") {

                        if (obj['comanda.ora_consegna'] !== ultima_ora_consegna) {
                            $(".consegna table tbody").append("<tr style='height:1mm;'></tr>");
                        }

                        ultima_ora_consegna = obj['comanda.ora_consegna'];

                        $(".consegna table tbody").append("<tr class='" + obj['comanda.id'] + "'><td class='text-center'><a " + comanda.evento + "='stampa_qr_cliente(\"" + obj['comanda.ora_consegna'] + "\");'>" + obj['comanda.ora_consegna'] + "</a></td><td class='text-center'>" + pizze + " (+" + altri_articoli + ")</td><td class='nome_comanda text-center'>" + obj['comanda.nome_comanda'].toUpperCase() + "</td><td class='escludi-listener text-center' >" + obj['clienti.indirizzo'] + ", " + obj['clienti.numero'] + "<br/>" + obj['clienti.citta'] + "</td><td class='escludi-listener'><input class='form-control' type='checkbox' style='margin:0;padding:0;' " + comanda.evento + "='cancella_riga_forno(\"" + obj['comanda.id'] + "\")' /></td></tr>");
                    }


                });
            });
        });

        if (bool_provenienza_socket !== true) {
                comanda.sock.send({tipo: "aggiornamento_righe_forno", operatore: comanda.operatore, query: "select null", terminale: comanda.terminale, ip: comanda.ip_address});
        }

    }
}