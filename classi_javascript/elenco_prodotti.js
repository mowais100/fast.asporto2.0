
//Dichiaro la funzione che è all'interno della classe funzionidb
this.elenco_prodotti =
            //il callBack serve per restituire una funzione alla fine dell'esecuzione del programma
            //praticamente per la sincronizzazione
            function (callBack) {
                //comanda.mobile è una proprietà booleana che identifica se il dispositivo è mobile o no
                //gliela passo io nel file comanda_tel
                if (comanda.mobile === false)
                {
                    //testo_query sono le query del database locale
                    //questa query seleziona tutti i prodotti praticamente che non hanno i campi richiesti vuoti
                    var testo_query = 'SELECT cat_varianti,colore_tasto,id,descrizione,prezzo_1, categoria, pagina, posizione FROM prodotti WHERE descrizione!="" AND prezzo_1 !="" AND posizione!="" AND pagina!="" AND categoria!="" ORDER BY posizione ASC,categoria ASC,pagina ASC ;';
                }
                else
                {
                    //questa query fa la stessa cosa ma in ordine alfabetico
                    //ma non funziona per il motivo che capirai di seguito
                    var testo_query = 'SELECT cat_varianti,colore_tasto,id,descrizione,prezzo_1, categoria, pagina, posizione FROM prodotti WHERE descrizione!="" AND prezzo_1 !="" AND posizione!="" AND pagina!="" AND categoria!="" ORDER BY descrizione ASC;';
                }

                //array di dati da inserire successivamente e dinamicamente
                var data = [];
                
                //una Promise rappresenta un'operazione che non è ancora completata, ma lo sarà in futuro.
                //serve per facilitare la sincronizzazione una volta completata, anche se non sarebbe indispensabile
                //perchè rende la funziona asincrona sincrona praticamente
                
                //Vedi https://developer.mozilla.org/it/docs/Web/JavaScript/Reference/Global_Objects/Promise
                
                var prom = new Promise(function (resolve, reject) {

                    //qui viene eseguita la query di testo_query
                    //function è il callback - ovvero la funzione restituita - dal metodo query
                    comanda.sincro.query(testo_query, function (prodotto)
                    {
                        //la promessa tramite il metodo resolve, diventa un array di prodotti
                        //ogni prodotto in questo caso è un oggetto
                        resolve({prodotto: prodotto});
                    });
                });
                //l'array di prodotti diventa data
                data.push(prom);
                

                //una volta completata di tutte le variabili la promessa data
                //al callback esegue la funzione interna
                Promise.all(data).then(function (data) {

                    //array prodotti NON prende il valore del primo prodotto promesso come si potrebbe pensare
                    //in quanto l'array 0 rappresenta TUTTI i prodotti, anche se non sarebbe molto corretto
                    //dal punto di vista della programmazione, ma siccome era così per comodità ho tenuto com'era
                    
                    var array_prodotti = data[0].prodotto;
                    
                    //inizializzo un nuovo array di prodotti
                    var prodotti = new Array();
                    
                    //per ogni prodotto
                    //lo vede come un oggetto ar
                    array_prodotti.forEach(function (ar) {

                        //divide la posizione, ovvero riga e colonna, che è separata dal trattino
                        var split = ar['posizione'].split('-');
                        var riga = split[0];
                        var colonna = split[1];
                        
                        //inizializza l'array prodotti
                        if (prodotti[ar.categoria] === undefined)
                        {
                            prodotti[ar.categoria] = new Array();
                        }

                        if (prodotti[ar.categoria][ar.pagina] === undefined)
                        {
                            prodotti[ar.categoria][ar.pagina] = new Array();
                        }

                        if (prodotti[ar.categoria][ar.pagina][riga] === undefined)
                        {
                            prodotti[ar.categoria][ar.pagina][riga] = new Array();
                        }

                        //questa è la struttura finale dell'array prodotti
                        //praticamente è indicizzato per categoria, pagina, riga e colonna
                        //e questo serve per non fare fatica nell'ordinamento
                        //dell'elenco prodotti per i computer
                        if (prodotti[ar.categoria][ar.pagina][riga][colonna] === undefined)
                        {
                            prodotti[ar.categoria][ar.pagina][riga][colonna] = new Object();
                            prodotti[ar.categoria][ar.pagina][riga][colonna] = ar;
                        }
                    });
                    
                    //inizializzo la variabile che conterrà l'output finale
                    var output = '';
                    
                    //per ogni categoria, che chiameremo a
                    for (var a in prodotti) {
                        
                        //per ogni pagina, che chiameremo b
                        for (var b in prodotti[a]) {

                            //creo la pagina con la sua classe giusta
                            //elenco prodotti alla fine si muoverà nelle classi, senza ricreare i prodotti ogni volta
                            
                            output += "<div class='pag_" + b + " cat_" + a + "' style='display:none;'>";
                            output += '<div class="bs-docs-section btn_COMANDA">\n\
                                    <div class="bs-glyphicons">\n\
                                        <ul class="bs-glyphicons-list">';

                            //se è un dispositivo mobile
                            if (comanda.mobile === true) {
                                //al massimo arriverà a 4 righe
                                var c_until = 4;
                            }
                            else
                            {
                                //altrimenti a 6 righe
                                var c_until = 6;
                            }
                            
                            //per riga
                            for (var c = 1; c <= c_until; c++) {
                                
                                //per colonna
                                for (var d = 1; d <= 8; d++) {

                                    //se tutto l'array è definito bene
                                    if (prodotti[a][b] !== undefined &&
                                            prodotti[a][b][c] !== undefined && prodotti[a][b][c][d] !== undefined)
                                    {
                                        //inizializzo una nuova var prodotto
                                        var prodotto = prodotti[a][b][c][d];
                                        
                                        //a seconda della funzione che mi serve cioè comanda o layout tasti
                                        switch (comanda.dispositivo) {
                                            case 'COMANDA':
                                            case "lang_11":
                                                //se è un mobile fa i tasti solamente grigi
                                                if (comanda.mobile === false) {
                                                    output += '<li class="btn_comanda" style="background-color:' + prodotto.colore_tasto + ';" onClick="aggiungi_articolo(\'' + prodotto.id + '\',\'' + prodotto.descrizione.replace(/'/g, "\\'") + '\',\'' + prodotto.prezzo_1 + '&euro;\',null,$(\'#quantita_articolo\').val(),null,\'' + prodotto.cat_varianti + '\');"> \n\
                                                                <span class="glyphicon-class">' + prodotto.descrizione + '<br/>€ ' + prodotto.prezzo_1 + '</span> \n\
                                                                 </li>';
                                                }
                                                else
                                                {
                                                    //sennò colorati
                                                    output += '<li class="btn_comanda" onClick="aggiungi_articolo(\'' + prodotto.id + '\',\'' + prodotto.descrizione.replace(/'/g, "\\'") + '\',\'' + prodotto.prezzo_1 + '&euro;\',null,$(\'#quantita_articolo\').val(),null,\'' + prodotto.cat_varianti + '\');"> \n\
                                                                <span class="glyphicon-class">' + prodotto.descrizione + '<br/>€ ' + prodotto.prezzo_1 + '</span> \n\
                                                                 </li>';
                                                }

                                                break;
                                                
                                            case 'LAYOUT TASTI':
                                            case "lang_33":

                                                //se è un layout tasti permette di impostare il layout
                                                output += '<li ';
                                                if (prodotto.colore_tasto !== undefined) {
                                                    output += 'style="background-color:' + prodotto.colore_tasto + '"';
                                                }
                                                output += ' onClick="javascript:modifica_articolo(\'' + comanda.categoria + '\',\'' + prodotto.posizione + '\',\'' + prodotto.colore_tasto + '\');"> \n\
                                                        <span  class="glyphicon-class">' + prodotto.descrizione + '</span> \n\
                                                            </li>';
                                                break;
                                            default:
                                                break;
                                        }

                                    }
                                    else
                                    {
                                        //altrimenti disegna solo tasti vuoti
                                        output += '<li style="display:table;"> \n\
                                     <span class="glyphicon" aria-hidden="true"></span> \n\
                                     <span class="glyphicon-class"></span> \n\
                                     </li>';
                                    }
                                }
                            }
                            //chiude l'elenco prodotti
                            output += '</ul>\n\
                                </div>\n\
                                    </div>\n\
                                        </div>';
                        }
                    }
                    //ritorna come callback l'output
                    callBack(output);
                });
            };
