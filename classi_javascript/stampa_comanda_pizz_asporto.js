/* global totale_scontrino */



$(document).on('change', 'select#tipologia_fattorino', function () {
    scelta_listino_fattorino($("select#tipologia_fattorino").children(":selected").val());
});

function scelta_listino_fattorino(numero) {
    comanda.tipologia_fattorino = numero;
    elenco_prodotti();
}

function eventuale_pagamento_droppay(variabile_vuota, callBack) {
    callBack(true);
}


function aggiungi_dispositivo(devid_nf, devicetype, devicemodel, deviceip) {
    var aggiunta_dispositivo = '<fpMateConfiguration><addDevice  deviceId = "' + devid_nf + '" deviceIp = "' + deviceip + '" deviceType = "' + devicetype + '" deviceModel = "' + devicemodel + '" deviceRetry = "10" /></fpMateConfiguration>';
    var test = new epson.fiscalPrint();
    test.send("http://" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi", aggiunta_dispositivo, 10000);
}






function stampa_comanda(saltafaseparcheggio) {

    if ($('.nome_parcheggio').html().length > 0) {
        if ($('#popup_scelta_cliente input[name="ora_consegna"]').val().length > 0) {
            if (comanda.tipo_consegna.length > 0 && comanda.tipo_consegna !== 'NIENTE') {

                test_copertura(function () {

                    var query = "select * from settaggi_profili where id=" + comanda.folder_number + " ;";
                    comanda.sincro.query(query, function (settaggi_profili) {
                        settaggi_profili = settaggi_profili[0];
                        if (comanda.pizzeria_asporto !== true && saltafaseparcheggio !== true && (comanda.tavolo === "BANCO" || comanda.tavolo === "BAR" || comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === "TAKEAWAY" || comanda.tavolo === "TAKE AWAY")) {
                            comanda.stampato = false;
                            btn_parcheggia();
                        } else {
                            comanda.stampato = true;
                            console.log("stampa_comanda asporto", comanda.stampato, saltafaseparcheggio, comanda.tavolo, comanda.parcheggio)
                            var nome_cliente = comanda.parcheggio;
                            var testo_query = "select  substr(desc_art,1,3)||'M'|| substr(nodo,1,3) as ord,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita from comanda as c1 where contiene_variante='1' and ntav_comanda='" + comanda.tavolo + "'  and posizione='CONTO'   and stato_record='ATTIVO' \n\
                           union all\n\
                           select  substr(desc_art,1,3)||'M'||substr(nodo,1,3) as ord,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,sum(quantita) as quantita from comanda as c1 where (contiene_variante !='1' or contiene_variante is null or contiene_variante='null') and length(nodo)=3 and ntav_comanda='" + comanda.tavolo + "' and posizione='CONTO'   and stato_record='ATTIVO' group by desc_art,contiene_variante,nome_comanda\n\
                           union all\n\
                           select  (select substr(desc_art,1,3) from comanda where nodo=substr(c1.nodo,1,3) and stato_record='ATTIVO'  and ntav_comanda='" + comanda.tavolo + "')||'M' ||substr(nodo,1,3) as ord,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita from comanda as c1 where length(nodo)=7 and substr(desc_art,1,1)='-' and ntav_comanda='" + comanda.tavolo + "'   and posizione='CONTO'  and stato_record='ATTIVO'\n\
                           union all\n\
                           select (select substr(desc_art,1,3) from comanda where nodo=substr(c1.nodo,1,3) and stato_record='ATTIVO'  and ntav_comanda='" + comanda.tavolo + "')||'M'||substr(nodo,1,3) as ord,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita from comanda as c1 where length(nodo)=7 and substr(desc_art,1,1)!='-' and ntav_comanda='" + comanda.tavolo + "'  and posizione='CONTO'   and stato_record='ATTIVO'\n\
                           order by ord ASC;";
                            console.log("STAMPA COMANDA", testo_query);
                            comanda.sincro.query(testo_query, function (result) {

                                console.log("RISULTATI COMANDA", result);
                                var array_comanda = new Array();
                                comanda.sincro.query(testo_query, function (result) {

                                    var progressivo = 0;
                                    result.forEach(function (obj) {

                                        var descrizione = obj['desc_art'];
                                        var prezzo = obj['prezzo_un'];
                                        var nodo = obj['nodo'];
                                        var quantita = obj['quantita'];
                                        var dest_stampa = obj['dest_stampa'];
                                        var portata = obj['portata'];
                                        var categoria = obj['categoria'];
                                        if (array_comanda[dest_stampa] === undefined) {

                                            array_comanda[dest_stampa] = new Object();
                                            array_comanda[dest_stampa].val = comanda.nome_stampante[dest_stampa].nome;
                                            array_comanda[dest_stampa].dimensione = comanda.nome_stampante[dest_stampa].dimensione;
                                            array_comanda[dest_stampa].ip = comanda.nome_stampante[dest_stampa].ip;
                                            array_comanda[dest_stampa].intelligent = comanda.nome_stampante[dest_stampa].intelligent;
                                            array_comanda[dest_stampa].fiscale = comanda.nome_stampante[dest_stampa].fiscale;
                                            array_comanda[dest_stampa].devid_nf = comanda.nome_stampante[dest_stampa].devid_nf;
                                            array_comanda[dest_stampa].devicetype = comanda.nome_stampante[dest_stampa].devicetype;
                                            array_comanda[dest_stampa].devicemodel = comanda.nome_stampante[dest_stampa].devicemodel;
                                        }

                                        if (array_comanda[dest_stampa][portata] === undefined) {
                                            array_comanda[dest_stampa][portata] = new Object();
                                            array_comanda[dest_stampa][portata].val = comanda.nome_portata[portata];
                                        }

                                        if (array_comanda[dest_stampa][portata][(progressivo + 1)] === undefined) {
                                            array_comanda[dest_stampa][portata][(progressivo + 1)] = [quantita, descrizione, prezzo];
                                        }

                                        progressivo++;
                                    });
                                    //---COMANDA INTELLIGENT---//

                                    for (var dest_stampa in array_comanda) {

                                        console.log("NOME STAMPANTI", comanda.nome_stampante[dest_stampa]);
                                        switch (comanda.nome_stampante[dest_stampa].intelligent) {
                                            case "n":
                                                console.log("STAMPA COMANDA NORMALE", array_comanda[dest_stampa].ip);
                                                //---COMANDA NON INTELLIGENT---//

                                                var corpo;
                                                corpo = '<epos-print>\n\
                    <text  dw="1" dh="1" description="' + comanda.lang_stampa[11] + '" />\n\
                    <feed  type="1" />\n\
                    <feed  type="1" />';
                                                corpo += '<text  dw="1" dh="1" description="' + comanda.lang_stampa[68].toUpperCase() + ': ' + comanda.tavolo + '" />\n\
                    <feed  type="1" />\n\
                    <feed  type="1" />';
                                                corpo += '<text  dw="0" dh="0" description="--------------------------------------" />\n\
                    <feed  type="1" />\n\
                    <feed  type="1" />';
                                                corpo += '<text  dw="0" dh="0" description="' + comanda.lang_stampa[109] + ': ' + comanda.operatore + '" />\n\
                    <feed  type="1" />\n\
                    <feed  type="1" />';
                                                corpo += '<text  dw="0" dh="0" description="' + comanda.lang_stampa[110] + ': ' + comanda.funzionidb.data_attuale() + '" />\n\
                    <feed  type="1" />\n\
                    <feed  type="1" />';
                                                corpo += '<text  dw="0" dh="0" description="' + comanda.lang_stampa[111] + ': [ ' + array_comanda[dest_stampa].val + ' ]" />\n\
                    <feed  type="1" />\n\
                    <feed  type="1" />';
                                                corpo += '<text  dw="0" dh="0" description="--------------------------------------" />\n\
                    <feed  type="1" />\n\
                    <feed  type="1" />';
                                                var totale_quantita = 0;
                                                for (var portata in array_comanda[dest_stampa]) {
                                                    if (array_comanda[dest_stampa][portata].val !== undefined) {

                                                        corpo += '<text  dw="0" dh="0" description="- - - ' + array_comanda[dest_stampa][portata].val + ' - - -" />\n\
                                            <feed  type="1" />\n\
                                                <feed  type="1" />';
                                                        for (var node in array_comanda[dest_stampa][portata]) {

                                                            var nodo = array_comanda[dest_stampa][portata][node];
                                                            if (nodo !== undefined && nodo[1] !== undefined && nodo[1].length > 1) {

                                                                var articolo = filtra_accenti(nodo[1]);
                                                                var quantita = nodo[0];
                                                                if (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(") {
                                                                    totale_quantita = parseInt(quantita) + totale_quantita;
                                                                    corpo += '  <text  dw="0" dh="0" description="' + quantita + ' ' + articolo + '" />\n\
                                <feed  type="1" />\n\
                                <feed  type="1" />';
                                                                } else {

                                                                    corpo += '  <text  dw="0" dh="0" description="' + articolo + '" />\n\
                                <feed  type="1" />\n\
                                <feed  type="1" />';
                                                                }
                                                            }
                                                        }
                                                    }

                                                    for (var dest_stampa_concomitanza in array_comanda) {

                                                        corpo += '<text  dw="0" dh="0" description="-------------CONCOMITANZE-------------" />\n';
                                                        corpo += '<text  dw="0" dh="0" description="' + comanda.lang_stampa[111] + ': [ ' + array_comanda[dest_stampa_concomitanza].val + ' ]" />\n';
                                                        if (dest_stampa_concomitanza !== dest_stampa) {

                                                            corpo += '<text  dw="0" dh="0" description="- - - ' + array_comanda[dest_stampa][portata].val + ' - - -" />\n\
                                                    <feed  type="1" />\n\
                                                    <feed  type="1" />';
                                                            for (var node_concomitanza in array_comanda[dest_stampa_concomitanza][portata]) {

                                                                var nodo = array_comanda[dest_stampa_concomitanza][portata][node_concomitanza];
                                                                if (nodo !== undefined && nodo[1] !== undefined && nodo[1].length > 1) {

                                                                    var articolo = filtra_accenti(nodo[1]);
                                                                    var quantita = nodo[0];
                                                                    if (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(") {
                                                                        totale_quantita = parseInt(quantita) + totale_quantita;
                                                                        corpo += '  <text  dw="0" dh="0" description="' + quantita + ' ' + articolo + '" />\n\
                                                                <feed  type="1" />\n\
                                                                <feed  type="1" />';
                                                                    } else {

                                                                        corpo += '  <text  dw="0" dh="0" description="' + articolo + '" />\n\
                                                                <feed  type="1" />\n\
                                                                <feed  type="1" />';
                                                                    }
                                                                }
                                                            }
                                                        }

                                                    }
                                                }

                                                corpo += '  <text  dw="0" dh="0" description="' + totale_quantita + '" />\n\
                                <feed  type="1" />\n\
                                <feed  type="1" />';
                                                corpo += '  <feed  type="1" />\n\
                                <feed  type="1" />\n\
                                    <feed  type="1" />\n\
                                        <feed  type="1" />\n\
                                            <feed  type="1" />\n\
                                                <cut  type="feed" />\n\
                                                    </epos-print>';
                                                //CAMBIANDO QUESTO PARAMETRO CAMBIA L'IP DI STAMPA (quindi si potrebbe prendere da comanda.nome_stampante[num].ip
                                                //SI POTREBBE TESTARE LA DIMENSIONE CON comanda.nome_stampante[num].dimensione
                                                //E SE E' O NO FISCALE CON IL PARAMETRO comanda.nome_stampante[num].fiscale

                                                corpo = '<?xml version="1.0" encoding="utf-8"?>\n' +
                                                    '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">\n' +
                                                    '<s:Body>\n' + corpo +
                                                    '</s:Body>\n' +
                                                    '</s:Envelope>\n';
                                                console.log("CORPO", corpo);
                                                aggiungi_spool_stampa("http://" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=" + array_comanda[dest_stampa].devid_nf + "&timeout=10000", corpo, comanda.nome_stampante[dest_stampa].nome_umano);
                                                //---FINE COMANDA NON INTELLIGENT---//

                                                break;
                                            case "s":
                                            case "SDS":
                                                var numeretto = $('#popup_scelta_cliente input[name="numeretto"]').val();
                                                if (comanda.nome_stampante[dest_stampa].fiscale === 'n') {

                                                    var concomitanze_presenti = false;
                                                    console.log("STAMPA COMANDA INTELLIGENT", array_comanda[dest_stampa].ip);
                                                    var com = '';
                                                    var data = comanda.funzionidb.data_attuale();
                                                    var builder = new epson.ePOSBuilder();
                                                    var concomitanze = new epson.ePOSBuilder();
                                                    var parola_conto = comanda.lang_stampa[11];
                                                    builder.addTextFont(builder.FONT_A);
                                                    builder.addTextAlign(builder.ALIGN_CENTER);
                                                    builder.addTextSize(2, 2);
                                                    if (comanda.tavolo !== "BANCO" && comanda.tavolo !== "BAR" && comanda.tavolo.left(7) !== "ASPORTO" && comanda.tavolo !== "TAKE AWAY") {
                                                        builder.addText(parola_conto + '\n');
                                                        builder.addTextFont(builder.FONT_A);
                                                        builder.addFeedLine(1);
                                                        builder.addText('' + array_comanda[dest_stampa].val + '\n');
                                                    } else {

                                                        builder.addFeedLine(2);
                                                    }

                                                    builder.addFeedLine(1);
                                                    builder.addTextAlign(builder.ALIGN_LEFT);
                                                    if (comanda.tavolo !== "BANCO" && comanda.tavolo !== "BAR" && comanda.tavolo.left(7) !== "ASPORTO" && comanda.tavolo !== "TAKE AWAY") {
                                                        builder.addText(comanda.lang_stampa[68].toLowerCase().capitalize() + ' N°' + comanda.tavolo + '\n');
                                                    } else if (nome_cliente !== 0) {
                                                        builder.addTextAlign(builder.ALIGN_CENTER);
                                                        builder.addText(nome_cliente + ' (' + numeretto + ')\n');
                                                        builder.addTextAlign(builder.ALIGN_LEFT);
                                                    } else {
                                                        builder.addTextAlign(builder.ALIGN_CENTER);
                                                        builder.addText('P: ' + numeretto + '\n');
                                                        builder.addTextAlign(builder.ALIGN_LEFT);
                                                    }


                                                    builder.addTextSize(1, 1);
                                                    builder.addFeedLine(1);
                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                    if (comanda.tavolo !== "BANCO" && comanda.tavolo !== "BAR" && comanda.tavolo.left(7) !== "ASPORTO" && comanda.tavolo !== "TAKE AWAY") {
                                                        builder.addText(comanda.lang_stampa[109].toLowerCase().capitalize() + ': ' + comanda.operatore + '\n');
                                                        builder.addText(data + '\n');
                                                    } else {
                                                        builder.addTextAlign(builder.ALIGN_CENTER);
                                                        builder.addTextSize(2, 2);
                                                        if (comanda.esecuzione_comanda_immediata === true) {
                                                            builder.addText('FARE SUBITO !\n');
                                                        } else {
                                                            var query_gap_consegna = "select time_gap_consegna from impostazioni_fiscali where id=" + comanda.folder_number + ";"
                                                            var result = alasql(query_gap_consegna);
                                                            if (result[0].time_gap_consegna == "1")
                                                            {
                                                                var ora_consegna=$('#popup_scelta_cliente input[name="ora_consegna"]').val()
                                                                var minuti_somma = ora_consegna.substring(ora_consegna.indexOf(':') +1)
                                                                var int_miunti = parseInt(minuti_somma);
                                                                var int_gap_miunti = parseInt(comanda.consegna_gap);;
                                                                var min_somma = int_miunti + int_gap_miunti;
                                                                var ora_gap =ora_consegna.substring(0,ora_consegna.indexOf(':'))
                                                                var hours = Math.floor(min_somma  / 60);          
                                                                var minutes = min_somma  % 60;
                                                                var ora =parseInt(ora_gap);
                                                                
                                                                if(minutes<10)
                                                                {var totale_ora = ora + hours + ":" +"0"+minutes; 
                                                                }
                                                                else{
                                                                    var totale_ora = ora + hours + ":" +minutes; 
                                                                }
                                                                
                                                                comanda.ora_consegna_gap = totale_ora;
                                                                builder.addText('CONSEGNA ' + ' ' + $('#popup_scelta_cliente input[name="ora_consegna"]').val() + '\n');
                                                                builder.addText('CONSEGNA GAP' + ' ' +totale_ora  + '\n');

                                                            }
                                                            else{
                                                                builder.addText('CONSEGNA ' + ' ' + $('#popup_scelta_cliente input[name="ora_consegna"]').val() + '\n');
                                                            }
                                                                
                                                        }

                                                        builder.addTextSize(1, 1);
                                                        builder.addFeedLine(1);
                                                        builder.addTextAlign(builder.ALIGN_LEFT);
                                                    }

                                                    builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                    builder.addTextSize(1, 1);
                                                    var totale_quantita = 0;
                                                    for (var portata in array_comanda[dest_stampa]) {
                                                        if (array_comanda[dest_stampa][portata].val !== undefined) {

                                                            builder.addFeedLine(1);
                                                            builder.addTextAlign(builder.ALIGN_CENTER);
                                                            builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                            builder.addTextSize(1, 1);
                                                            builder.addText('-------------- ' + array_comanda[dest_stampa][portata].val + ' -------------- \n');
                                                            builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                            builder.addTextAlign(builder.ALIGN_LEFT);
                                                            builder.addTextSize(2, 2);
                                                            for (var node in array_comanda[dest_stampa][portata]) {

                                                                var nodo = array_comanda[dest_stampa][portata][node];
                                                                if (nodo !== undefined && nodo[1] !== undefined && nodo[1].length > 1) {

                                                                    var articolo = filtra_accenti(nodo[1]);
                                                                    var quantita = nodo[0];
                                                                    if (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(" && articolo[0] !== "*" && articolo[0] !== "x") {

                                                                        builder.addTextSize(2, 2);
                                                                        builder.addText(quantita);
                                                                        builder.addTextPosition(50);
                                                                        totale_quantita += parseInt(quantita);
                                                                        builder.addTextPosition(50);
                                                                        builder.addText(articolo.slice(0, 40) + '\n');
                                                                        builder.addTextSize(1, 1);
                                                                    } else {
                                                                        builder.addTextSize(2, 2);
                                                                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                                        if (articolo[0] === "*" || articolo[0] === "x") {


                                                                            articolo = articolo.substr(1);
                                                                        }

                                                                        articolo = '\t' + articolo;
                                                                        builder.addTextPosition(50);
                                                                        builder.addText(articolo.slice(0, 40) + '\n');
                                                                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                                        builder.addTextSize(1, 1);
                                                                    }


                                                                }
                                                            }
                                                            builder.addTextAlign(builder.ALIGN_CENTER);
                                                            builder.addTextSize(1, 1);
                                                            builder.addText("-----------------------------------------\n");
                                                            builder.addTextAlign(builder.ALIGN_LEFT);
                                                        }
                                                    }
                                                    builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                    builder.addTextSize(1, 1);
                                                    if (comanda.tavolo === "BANCO" || comanda.tavolo == "BAR" || comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === "TAKE AWAY") {

                                                        builder.addTextStyle(false, false, false, builder.COLOR_1);
                                                        builder.addText('\n\nData stampa: ' + data.substr(0, 8));
                                                        builder.addText('\nOra stampa: ' + data.slice(-5));
                                                        builder.addTextSize(2, 2);
                                                        if (nome_cliente.length === 0) {
                                                            builder.addText('\n' + numeretto + '\n');
                                                        }

                                                        builder.addFeedLine(2);
                                                        builder.addTextAlign(builder.ALIGN_CENTER);
                                                        builder.addText(comanda.tipo_consegna);
                                                        builder.addTextSize(1, 1);
                                                        builder.addTextStyle(false, false, true, builder.COLOR_1);
                                                    }
                                                    builder.addTextAlign(builder.ALIGN_LEFT);
                                                    var testo_concomitanze = '';
                                                    if (concomitanze_presenti === true) {
                                                        testo_concomitanze = '<feed line="2"/><text align="center">_______________________________ \n&#10;</text><text align="center">CONCOMITANZE&#10;</text>' + concomitanze.toString().substr(72).slice(0, -13);
                                                    }
                                                    var request = builder.toString().slice(0, -13) + testo_concomitanze + '<feed line="3"/><cut type="feed"/></epos-print>';
                                                    console.log("REQUEST", request);
                                                    var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + request + '</s:Body></s:Envelope>';
                                                    //CAMBIANDO QUESTO PARAMETRO CAMBIA L'IP DI STAMPA (quindi si potrebbe prendere da comanda.nome_stampante[num].ip
                                                    //SI POTREBBE TESTARE LA DIMENSIONE CON comanda.nome_stampante[num].dimensione
                                                    //E SE E' O NO FISCALE CON IL PARAMETRO comanda.nome_stampante[num].fiscale

                                                    var url = 'http://' + comanda.nome_stampante[dest_stampa].ip + '/cgi-bin/epos/service.cgi?devid=local_printer&timeout=30000';
                                                    aggiungi_spool_stampa(url, soap, comanda.nome_stampante[dest_stampa].nome_umano);
                                                } else if (comanda.nome_stampante[dest_stampa].fiscale === 's') {

                                                    var corpo = '';
                                                    var data = comanda.funzionidb.data_attuale();
                                                    corpo += '<printerNonFiscal>\n\
                                            <beginNonFiscal  operator="1" />';
                                                    corpo += '<printNormal  operator="1" font="1" data="---' + comanda.lang_stampa[68].toUpperCase() + ' N. ' + comanda.tavolo + '---" />';
                                                    corpo += '<printNormal  operator="1" font="1" data="---' + array_comanda[dest_stampa].val + '---" />';
                                                    corpo += '<printNormal  operator="1" font="1" data=" " />';
                                                    corpo += '<printNormal  operator="1" font="1" data="---' + data + '---" />';
                                                    corpo += '<printNormal  operator="1" font="1" data=" " />';
                                                    corpo += '<printNormal  operator="1" font="1" data="---' + comanda.operatore + '---" />';
                                                    corpo += '<printNormal  operator="1" font="1" data=" " />';
                                                    var totale_quantita = 0;
                                                    for (var portata in array_comanda[dest_stampa]) {
                                                        if (array_comanda[dest_stampa][portata].val !== undefined) {

                                                            corpo += '<printNormal  operator="1" font="1" data="---' + array_comanda[dest_stampa][portata].val + '---" />';
                                                            corpo += '<printNormal  operator="1" font="1" data=" " />';
                                                            for (var node in array_comanda[dest_stampa][portata]) {

                                                                var nodo = array_comanda[dest_stampa][portata][node];
                                                                if (nodo !== undefined && nodo[1] !== undefined && nodo[1].length > 1) {

                                                                    var articolo = filtra_accenti(nodo[1]);
                                                                    var quantita = nodo[0];
                                                                    if (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(") {
                                                                        corpo += '<printNormal  operator="1" font="1" data="' + quantita + ' ' + articolo + '" />';
                                                                        totale_quantita += parseInt(quantita);
                                                                    } else {
                                                                        corpo += '<printNormal  operator="1" font="1" data="' + articolo + '" />';
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }

                                                    corpo += '<printNormal  operator="1" font="1" data="' + totale_quantita + '" />';
                                                    concomitanze += '<printNormal  operator="1" font="1" data="\n" />';
                                                    concomitanze += '<printNormal  operator="1" font="1" data="\nCONCOMITANZE:\n" />';
                                                    for (var dest_stampa_concomitanza in array_comanda) {
                                                        if (array_comanda[dest_stampa_concomitanza].val !== undefined && dest_stampa_concomitanza !== dest_stampa && array_comanda[dest_stampa_concomitanza][portata] !== undefined && array_comanda[dest_stampa_concomitanza][portata] !== null && typeof (array_comanda[dest_stampa_concomitanza][portata]) === "object" && Object.keys(array_comanda[dest_stampa_concomitanza][portata]).length > 0) {
                                                            concomitanze_presenti = true;
                                                            console.log("CONCOMITANZE: " + dest_stampa + " P: " + portata, array_comanda[dest_stampa_concomitanza][portata], Object.keys(array_comanda[dest_stampa_concomitanza][portata]).length);
                                                            concomitanze += '<printNormal  operator="1" font="1" data="\n' + array_comanda[dest_stampa_concomitanza].val + ' - ' + array_comanda[dest_stampa][portata].val + '\n" />';
                                                            for (var node_concomitanza in array_comanda[dest_stampa_concomitanza][portata]) {

                                                                var nodo = array_comanda[dest_stampa_concomitanza][portata][node_concomitanza];
                                                                if (nodo !== undefined && nodo[1] !== undefined && nodo[1].length > 1) {

                                                                    var articolo = filtra_accenti(nodo[1]);
                                                                    var quantita = nodo[0];
                                                                    if (articolo[0] !== "+" && articolo[0] !== "-" && articolo[0] !== "(") {
                                                                        concomitanze += '<printNormal  operator="1" font="1" data="' + quantita + ' ' + articolo + '" />';
                                                                    } else {
                                                                        concomitanze += '<printNormal  operator="1" font="1" data="' + articolo + '" />';
                                                                    }

                                                                }
                                                            }
                                                        }
                                                    }

                                                    var fine = '<endNonFiscal  operator="1" />\n\
                                            </printerNonFiscal>';
                                                    var url = 'http://' + comanda.nome_stampante[dest_stampa].ip + '/cgi-bin/fpmate.cgi?devid=local_printer&timeout=10000';
                                                    var soap = '';
                                                    if (concomitanze_presenti === true) {
                                                        soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + corpo + concomitanze + fine + '</s:Body></s:Envelope>';
                                                    } else {
                                                        soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">' + '<s:Body>' + corpo + fine + '</s:Body></s:Envelope>';
                                                    }

                                                    aggiungi_spool_stampa(url, soap, comanda.nome_stampante[dest_stampa].nome_umano);
                                                    break;
                                                }




                                        }
                                    }

                                    //---FINE COMANDA INTELLIGENT---//

                                    var data = comanda.funzionidb.data_attuale().substr(0, 8);
                                    var ora = comanda.funzionidb.data_attuale().substr(9, 8);
                                    var testo_query = "update comanda set stampata_sn='S',data_stampa='" + data + "',ora_stampa='" + ora + "' where comanda.stato_record='ATTIVO' and comanda.ntav_comanda='" + comanda.tavolo + "'";
                                    comanda.sock.send({ tipo: "comanda_stampata", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address });
                                    comanda.sincro.query(testo_query, function () {

                                    });
                                });
                            });
                        }
                    });
                });
            } else {
                intesta_cliente();
            }
        } else {
            intesta_cliente();
        }
    } else {
        intesta_cliente();
    }
}

/* Definizione Struttura Oggetti */
function Testa() {

    /*this.IDTESTA;   */
    this.C1111; /* Id Paese + */
    this.C1112; /* Id Codice + */
    this.C112; /* Progressivo Invio + */
    this.C113; /* Formato Trasmissione + */
    this.C114; /* Codice Destinatario + */
    this.C116; /* PEC Destinatario + */
    this.C12111; /* Id Paese + */
    this.C12112; /* Id Codice + */
    this.C12131; /* Denominazione + */
    this.C1218; /* Regime Fiscale + */
    this.C1221; /* Indirizzo + */
    this.C1223; /* CAP + */
    this.C1224; /* Comune + */
    this.C1225; /* Provincia + */
    this.C1226; /* Nazione + */
    this.C14111; /* Id Paese + */
    this.C14112; /* Id Codice + */
    this.C14131; /* Denominazione + */
    this.C1421; /* Indirizzo + */
    this.C1423; /* CAP + */
    this.C1424; /* Comune + */
    this.C1425; /* Provincia + */
    this.C1426; /* Nazione + */
    this.C15111; /* Id Paese */
    this.C15112; /* Id Codice */
    this.C15131; /* Denominazione */
    this.C2111; /* Tipo Documento + */
    this.C2112; /* Divisa + */
    this.C2113; /* Data + */
    this.C2114; /* Numero + */
    this.C21151; /* Tipo Ritenuta */
    this.C21152; /* Importo Ritenuta */
    this.C21153; /* Aliquota Ritenuta */
    this.C21154; /* Causale Pagamento */
    this.C21181; /* Tipo */
    this.C21182; /* Percentuale */
    this.C21183; /* Importo */
    this.C2119; /* Importo Totale Documento + */
    this.C21111; /* Causale */
    this.SPED; /*  */
    this.DATAS; /*  */
    this.INDIRIZZO; /*  */

}

function Corpo() {

    /*this.IDTESTA;  Id Testa */
    this.C2211; /* Numero Linea + */
    this.C2212; /* Tipo Cessione Prestazione */
    this.C22131; /* Codice Tipo + */
    this.C22132; /* Codice Valore + */
    this.C2214; /* Descrizione + */
    this.C2215; /* Quantità + */
    this.C2216; /* Unità Misura + */
    this.C2219; /* Prezzo Unitario + */
    this.C221101; /* Tipo (Sconto-Magg-iorazione) */
    this.C221103; /* Importo Sconto */
    this.C22111; /* Prezzo Totale + */
    this.C22112; /* Aliquota IVA + */
}

function Totali() {

    /*this.IDTESTA;  Id Testa */
    this.C2221; /* Aliquota IVA + */
    this.C2222; /* Natura */
    this.C2225; /* Imponibile Importo + */
    this.C2226; /* Imposta + */
    this.C2227; /* Esigibilità IVA */

}

function Pagamento() {

    /*this.IDTESTA;  Id Testa */
    this.C241; /* Condizioni Pagamento */
    this.C2422; /* Modalità Pagamento */
    this.C2426; /* Importo Pagamento */

}



var fattura_elettronica = new Object();




