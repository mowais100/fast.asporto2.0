/* global ive_misuratore_controller, comanda, REPARTO_controller, parseFloat */

var progressivo_ultimo_conto = 0;
comanda.funzionidb.conto_attivo = function (callBACCO, salta_colore) {

    var reparto = "rep_";
    var iva_esposta = "";

    $("#numero_paganti").html("");
    //log_velocita(false, "inizio conto attivo");

    comanda.consegna_presente = false;
    comanda.quantita_pizze_totali = 0;
    comanda.totale_consegna = "0.00";
    comanda.fissa_totale_consegna = "0.00";
    comanda.stampata_s = false;
    $('.tasto_numero_tavolo,.split_successivo').css('pointer-events', '');

    comanda.disabilita_tasto_bis = false;
    comanda.disabilita_tasto_tavolo = false;

    progressivo_ultimo_conto++;
    var progressivo_ultimo_conto_interno = progressivo_ultimo_conto;
    var inizio_conto = new Date().getTime();
    var numero_conti_attivi = 1;
    console.log("INIZIO CONTO", inizio_conto);
    //CANCELLA RIGHE DAL CONTO ATTIVO
    //CREATE TABLE COMANDA
    //$('.ultime_battiture tr,#intestazioni_conto_separato_grande tr,#intestazioni_conto_separato_2_grande tr,#conto tr,#intestazioni_conto tr,.tab_conto_grande tr,#intestazioni_conto_grande tr,.tab_conto_separato_grande tr,.tab_conto_separato_2_grande tr').remove();

    var /*ultima_schiacciata = false, */ultima_due_gusti = false, ultima_maxi = false, numero_nodo_variante_piu = 0, consegna_presente = false, fissa_calcolo_consegna_una_pizza = 0, fissa_calcolo_consegna_piu_di_una_pizza = 0, calcolo_consegna_una_pizza = 0, calcolo_consegna_piu_di_una_pizza = 0, calcolo_consegna_su_totale = 0, calcolo_consegna_mezzo_metro = 0, calcolo_consegna_un_metro = 0, calcolo_consegna_maxi = 0, calcolo_consegna_a_pizza = 0, riga_calcolo_consegna = '', ultime_battiture = '', intestazioni_conto_separato_grande = '', intestazioni_conto_separato_2_grande = '', intestazioni_conto_separato_2_grande_split = '', conto = '', intestazioni_conto = '', tab_conto_grande = '', intestazioni_conto_grande = '', intestazioni_conto_split = '', tab_conto_separato_grande = '', tab_conto_separato_2_grande = '', tab_conto_separato_2_grande_split = '';


    if (comanda.pizzeria_asporto === true && (parseInt(comanda.consegna_fattorino_extra) > 0 && comanda.fissa_una_pizza.length > 0 || comanda.fissa_piu_di_una_pizza.length > 0 || comanda.una_pizza.length > 0 || comanda.piu_di_una_pizza.length > 0 || comanda.consegna_a_pizza.length > 0 || comanda.consegna_mezzo_metro.length > 0 || comanda.consegna_metro.length > 0 || comanda.consegna_maxi.length > 0 || comanda.consegna_su_totale.length > 0)) {
        comanda.consegna_presente = true;
    }


    //Crea tabella comanda

    if (comanda.tavolo !== 0) {

        /* PARTE NUOVA */

        comanda.totale_conto = new Object();
        //BLOCCO
        var p2 = new Promise(function (resolve, reject) {

            var esclusione_fiscale_sospeso = '';
            //if (typeof (comanda.tavolo) === 'string' && (comanda.tavolo === "BAR" || comanda.tavolo.indexOf('CONTINUO') !== -1)) {
            esclusione_fiscale_sospeso = ' and fiscale_sospeso!="STAMPAFISCALESOSPESO" ';
            //}



            var groupby = "group by cod_promo,fiscalizzata_sn,portata,tipo_impasto,desc_art,prog_inser,nome_comanda,prezzo_un";
            if (comanda.tasto_contosep === "S" || comanda.multiquantita === "N") {
                groupby = "group by cod_promo,fiscalizzata_sn,portata,tipo_impasto,desc_art,nome_comanda,prezzo_un";
            }

            var testo_query = "select QTA,numero_paganti,tasto_segue,BIS,cod_promo,fiscalizzata_sn,ora_,rag_soc_cliente,perc_iva,reparto,nome_comanda,stampata_sn,numero_conto,dest_stampa,portata,categoria,prog_inser,nodo,desc_art,prezzo_maxi,prezzo_un,prezzo_varianti_aggiuntive,prezzo_varianti_maxi,prezzo_maxi_prima,quantita,contiene_variante,tipo_impasto from comanda where desc_art NOT LIKE 'RECORD TESTA%' and  BIS='" + lettera_conto_split() + "' and ntav_comanda='" + comanda.tavolo + "' and posizione='CONTO'   and (trim(QTAP)='0' or trim(QTAP) is null or trim(QTAP)='') and stato_record='ATTIVO'    " + esclusione_fiscale_sospeso + ";";
            var d1 = alasql(testo_query);

            //log_velocita(false, "conto attivo 1 query");

            alasql("DELETE FROM oggetto_conto");
            alasql("DELETE FROM oggetto_conto_2");
            d1.forEach(function (attivo, i) {

                var ord = "";

                if (comanda.pizzeria_asporto !== true && !(comanda.pizzeria_asporto !== true && comanda.tavolo.left(7) === "ASPORTO")) {
                    if (attivo.stampata_sn === 'S') {
                        ord = "A";
                    } else {
                        ord = "B";
                    }
                }

                if (comanda.ordine_articoli_video === "B") {
                    if (attivo.contiene_variante === '1') {
                        ord += 'M' + attivo.nodo.substr(0, 3);
                    } else if (attivo.nodo.length === 7) {
                        var query_interna = "select substr(desc_art,1,3) as desc_art_princ from comanda where desc_art NOT LIKE 'RECORD TESTA%' and  BIS='" + lettera_conto_split() + "' and  nodo='" + attivo.nodo.substr(0, 3) + "'  and (trim(QTAP)='0' or trim(QTAP) is null or trim(QTAP)='') and stato_record='ATTIVO'  and ntav_comanda='" + comanda.tavolo + "' limit 1;";
                        var rqi = alasql(query_interna);

                        //log_velocita(false, "conto attivo 2 query");
                        if (attivo.desc_art.substr(0, 1) === '-') {
                            //SBAGLIATO (RIVEDERE QUERY C1)
                            ord += 'M' + attivo.nodo;
                        } else if (attivo.desc_art.substr(0, 1) !== '-') {
                            ord += 'M' + attivo.nodo;
                        }
                    } else if (attivo.contiene_variante !== '1') {
                        ord += 'M' + attivo.nodo.substr(0, 3);
                    }

                    if (attivo.numero_conto === "2") {
                        numero_conti_attivi = 2;
                        alasql.tables.oggetto_conto_2.data.push(new riga_conto_ricostruito(attivo.prezzo_maxi, attivo.tasto_segue, ord, attivo.cod_promo, attivo.fiscalizzata_sn, attivo.ora_, attivo.rag_soc_cliente, attivo.perc_iva, attivo.reparto, attivo.nome_comanda, attivo.stampata_sn, attivo.numero_conto, attivo.dest_stampa, attivo.portata, attivo.categoria, attivo.prog_inser, attivo.nodo, attivo.desc_art, attivo.prezzo_un, attivo.prezzo_varianti_aggiuntive, attivo.prezzo_varianti_maxi, attivo.prezzo_maxi_prima, attivo.quantita, attivo.contiene_variante, attivo.tipo_impasto, attivo.numero_paganti, attivo.BIS, attivo.QTA));
                    } else {
                        alasql.tables.oggetto_conto.data.push(new riga_conto_ricostruito(attivo.prezzo_maxi, attivo.tasto_segue, ord, attivo.cod_promo, attivo.fiscalizzata_sn, attivo.ora_, attivo.rag_soc_cliente, attivo.perc_iva, attivo.reparto, attivo.nome_comanda, attivo.stampata_sn, attivo.numero_conto, attivo.dest_stampa, attivo.portata, attivo.categoria, attivo.prog_inser, attivo.nodo, attivo.desc_art, attivo.prezzo_un, attivo.prezzo_varianti_aggiuntive, attivo.prezzo_varianti_maxi, attivo.prezzo_maxi_prima, attivo.quantita, attivo.contiene_variante, attivo.tipo_impasto, attivo.numero_paganti, attivo.BIS, attivo.QTA));
                    }
                }
                /*ORIGINALE*/
                else {
                    if (attivo.contiene_variante === '1') {
                        ord += attivo.desc_art.substr(0, 3) + 'M' + attivo.nodo.substr(0, 3);
                    } else if (attivo.nodo.length === 7) {
                        var query_interna = "select substr(desc_art,1,3) as desc_art_princ from comanda where desc_art NOT LIKE 'RECORD TESTA%'  and  BIS='" + lettera_conto_split() + "' and  nodo='" + attivo.nodo.substr(0, 3) + "'  and (trim(QTAP)='0' or trim(QTAP) is null or trim(QTAP)='') and stato_record='ATTIVO'  and ntav_comanda='" + comanda.tavolo + "' limit 1;";
                        var rqi = alasql(query_interna);

                        //log_velocita(false, "conto attivo 2 query");

                        if (attivo.desc_art.substr(0, 1) === '-') {
                            //SBAGLIATO (RIVEDERE QUERY C1)
                            ord += rqi[0].desc_art_princ + 'M' + attivo.nodo;
                        } else if (attivo.desc_art.substr(0, 1) !== '-') {
                            ord += rqi[0].desc_art_princ + 'M' + attivo.nodo;
                        }
                    } else if (attivo.contiene_variante !== '1') {
                        ord += attivo.desc_art.substr(0, 3) + 'M' + attivo.nodo.substr(0, 3);
                    }

                    if (attivo.numero_conto === "2") {
                        numero_conti_attivi = 2;
                        alasql.tables.oggetto_conto_2.data.push(new riga_conto_ricostruito(attivo.prezzo_maxi, attivo.tasto_segue, ord, attivo.cod_promo, attivo.fiscalizzata_sn, attivo.ora_, attivo.rag_soc_cliente, attivo.perc_iva, attivo.reparto, attivo.nome_comanda, attivo.stampata_sn, attivo.numero_conto, attivo.dest_stampa, attivo.portata, attivo.categoria, attivo.prog_inser, attivo.nodo, attivo.desc_art, attivo.prezzo_un, attivo.prezzo_varianti_aggiuntive, attivo.prezzo_varianti_maxi, attivo.prezzo_maxi_prima, attivo.quantita, attivo.contiene_variante, attivo.tipo_impasto, attivo.numero_paganti, attivo.BIS, attivo.QTA));
                    } else {
                        alasql.tables.oggetto_conto.data.push(new riga_conto_ricostruito(attivo.prezzo_maxi, attivo.tasto_segue, ord, attivo.cod_promo, attivo.fiscalizzata_sn, attivo.ora_, attivo.rag_soc_cliente, attivo.perc_iva, attivo.reparto, attivo.nome_comanda, attivo.stampata_sn, attivo.numero_conto, attivo.dest_stampa, attivo.portata, attivo.categoria, attivo.prog_inser, attivo.nodo, attivo.desc_art, attivo.prezzo_un, attivo.prezzo_varianti_aggiuntive, attivo.prezzo_varianti_maxi, attivo.prezzo_maxi_prima, attivo.quantita, attivo.contiene_variante, attivo.tipo_impasto, attivo.numero_paganti, attivo.BIS, attivo.QTA));
                    }
                }
            });


            var groupby = 1;
            if (comanda.tasto_contosep === "S" || comanda.multiquantita === "N") {
                groupby = 2;
            }


            var raggruppamento = new Object();
            alasql("select * from oggetto_conto where contiene_variante!='1' and length(nodo)=3 order by tasto_segue ASC;").forEach(function (e) {

                //log_velocita(false, "conto attivo 3 query");

                if (e.stampata_sn !== "S") {
                    e.stampata_sn = "N";
                }

                if (groupby === 1) {

                    if (raggruppamento[e.tasto_segue] === undefined) {
                        raggruppamento[e.tasto_segue] = new Object();
                    }

                    if (raggruppamento[e.tasto_segue][e.cod_promo] === undefined) {
                        raggruppamento[e.tasto_segue][e.cod_promo] = new Object();
                    }

                    if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn] === undefined) {
                        raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn] = new Object();
                    }

                    if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn] === undefined) {
                        raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn] = new Object();
                    }

                    if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata] === undefined) {
                        raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata] = new Object();
                    }

                    if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto] === undefined) {
                        raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto] = new Object();
                    }

                    if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art] === undefined) {
                        raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art] = new Object();
                    }

                    if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.prog_inser] === undefined) {
                        raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.prog_inser] = new Object();
                    }

                    if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.prog_inser][e.nome_comanda] === undefined) {
                        raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.prog_inser][e.nome_comanda] = new Object();
                    }


                    if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.prog_inser][e.nome_comanda][e.prezzo_un] === undefined) {
                        e.quantita = parseInt(e.quantita);
                        raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.prog_inser][e.nome_comanda][e.prezzo_un] = e;
                    } else {
                        raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.prog_inser][e.nome_comanda][e.prezzo_un].quantita += parseInt(e.quantita);
                    }


                } else if (groupby === 2) {
                    if (raggruppamento[e.tasto_segue] === undefined) {
                        raggruppamento[e.tasto_segue] = new Object();
                    }

                    if (raggruppamento[e.tasto_segue][e.cod_promo] === undefined) {
                        raggruppamento[e.tasto_segue][e.cod_promo] = new Object();
                    }

                    if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn] === undefined) {
                        raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn] = new Object();
                    }

                    if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn] === undefined) {
                        raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn] = new Object();
                    }

                    if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata] === undefined) {
                        raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata] = new Object();
                    }

                    if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto] === undefined) {
                        raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto] = new Object();
                    }

                    if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art] === undefined) {
                        raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art] = new Object();
                    }



                    if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.nome_comanda] === undefined) {
                        raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.nome_comanda] = new Object();
                    }


                    if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.nome_comanda][e.prezzo_un] === undefined) {
                        e.quantita = parseInt(e.quantita);
                        raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.nome_comanda][e.prezzo_un] = e;
                    } else {
                        raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.nome_comanda][e.prezzo_un].quantita += parseInt(e.quantita);
                        raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.nome_comanda][e.prezzo_un].prog_inser = e.prog_inser;
                    }
                }
            });

            alasql("delete from oggetto_conto where contiene_variante!='1' and length(nodo)=3;");

            if (groupby === 1) {
                for (var tasto_segue in raggruppamento) {
                    for (var cod_promo in raggruppamento[tasto_segue]) {
                        for (var fiscalizzata_sn in raggruppamento[tasto_segue][cod_promo]) {
                            for (var stampata_sn in raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn]) {
                                for (var portata in raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn][stampata_sn]) {
                                    for (var tipo_impasto in raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn][stampata_sn][portata]) {
                                        for (var desc_art in raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto]) {
                                            for (var prog_inser in raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto][desc_art]) {
                                                for (var nome_comanda in raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto][desc_art][prog_inser]) {
                                                    for (var prezzo_un in raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto][desc_art][prog_inser][nome_comanda]) {
                                                        alasql.tables.oggetto_conto.data.push(raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto][desc_art][prog_inser][nome_comanda][prezzo_un]);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else if (groupby === 2) {
                for (var tasto_segue in raggruppamento) {
                    for (var cod_promo in raggruppamento[tasto_segue]) {
                        for (var fiscalizzata_sn in raggruppamento[tasto_segue][cod_promo]) {
                            for (var stampata_sn in raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn]) {
                                for (var portata in raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn][stampata_sn]) {
                                    for (var tipo_impasto in raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn][stampata_sn][portata]) {
                                        for (var desc_art in raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto]) {
                                            for (var nome_comanda in raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto][desc_art]) {
                                                for (var prezzo_un in raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto][desc_art][nome_comanda]) {
                                                    alasql.tables.oggetto_conto.data.push(raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto][desc_art][nome_comanda][prezzo_un]);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            raggruppamento = {};
            raggruppamento = null;

            if (numero_conti_attivi === 2) {
                var raggruppamento = new Object();
                alasql("select * from oggetto_conto_2 where contiene_variante!='1' and length(nodo)=3  order by tasto_segue ASC;").forEach(function (e) {

                    //log_velocita(false, "conto attivo 4 query");

                    if (e.stampata_sn !== "S") {
                        e.stampata_sn = "N";
                    }
                    if (groupby === 1) {

                        if (raggruppamento[e.tasto_segue] === undefined) {
                            raggruppamento[e.tasto_segue] = new Object();
                        }

                        if (raggruppamento[e.tasto_segue][e.cod_promo] === undefined) {
                            raggruppamento[e.tasto_segue][e.cod_promo] = new Object();
                        }

                        if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn] === undefined) {
                            raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn] = new Object();
                        }

                        if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn] === undefined) {
                            raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn] = new Object();
                        }


                        if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata] === undefined) {
                            raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata] = new Object();
                        }

                        if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto] === undefined) {
                            raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto] = new Object();
                        }

                        if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art] === undefined) {
                            raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art] = new Object();
                        }

                        if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.prog_inser] === undefined) {
                            raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.prog_inser] = new Object();
                        }

                        if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.prog_inser][e.nome_comanda] === undefined) {
                            raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.prog_inser][e.nome_comanda] = new Object();
                        }


                        if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.prog_inser][e.nome_comanda][e.prezzo_un] === undefined) {
                            e.quantita = parseInt(e.quantita);
                            raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.prog_inser][e.nome_comanda][e.prezzo_un] = e;
                        } else {
                            raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.prog_inser][e.nome_comanda][e.prezzo_un].quantita += parseInt(e.quantita);
                        }


                    } else if (groupby === 2) {

                        if (raggruppamento[e.tasto_segue] === undefined) {
                            raggruppamento[e.tasto_segue] = new Object();
                        }

                        if (raggruppamento[e.tasto_segue][e.cod_promo] === undefined) {
                            raggruppamento[e.tasto_segue][e.cod_promo] = new Object();
                        }

                        if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn] === undefined) {
                            raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn] = new Object();
                        }

                        if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn] === undefined) {
                            raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn] = new Object();
                        }

                        if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata] === undefined) {
                            raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata] = new Object();
                        }

                        if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto] === undefined) {
                            raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto] = new Object();
                        }

                        if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art] === undefined) {
                            raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art] = new Object();
                        }



                        if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.nome_comanda] === undefined) {
                            raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.nome_comanda] = new Object();
                        }


                        if (raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.nome_comanda][e.prezzo_un] === undefined) {
                            e.quantita = parseInt(e.quantita);
                            raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.nome_comanda][e.prezzo_un] = e;
                        } else {
                            raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.nome_comanda][e.prezzo_un].quantita += parseInt(e.quantita);
                            raggruppamento[e.tasto_segue][e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.nome_comanda][e.prezzo_un].prog_inser = e.prog_inser;
                        }
                    }
                });


                alasql("delete from oggetto_conto_2 where contiene_variante!='1' and length(nodo)=3;")

                if (groupby === 1) {
                    for (var tasto_segue in raggruppamento) {
                        for (var cod_promo in raggruppamento[tasto_segue]) {
                            for (var fiscalizzata_sn in raggruppamento[tasto_segue][cod_promo]) {
                                for (var stampata_sn in raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn]) {
                                    for (var portata in raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn][stampata_sn]) {
                                        for (var tipo_impasto in raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn][stampata_sn][portata]) {
                                            for (var desc_art in raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto]) {
                                                for (var prog_inser in raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto][desc_art]) {
                                                    for (var nome_comanda in raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto][desc_art][prog_inser]) {
                                                        for (var prezzo_un in raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto][desc_art][prog_inser][nome_comanda]) {
                                                            alasql.tables.oggetto_conto_2.data.push(raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto][desc_art][prog_inser][nome_comanda][prezzo_un]);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else if (groupby === 2) {
                    for (var tasto_segue in raggruppamento) {
                        for (var cod_promo in raggruppamento[tasto_segue]) {
                            for (var fiscalizzata_sn in raggruppamento[tasto_segue][cod_promo]) {
                                for (var stampata_sn in raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn]) {
                                    for (var portata in raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn][stampata_sn]) {
                                        for (var tipo_impasto in raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn][stampata_sn][portata]) {
                                            for (var desc_art in raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto]) {
                                                for (var nome_comanda in raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto][desc_art]) {
                                                    for (var prezzo_un in raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto][desc_art][nome_comanda]) {
                                                        alasql.tables.oggetto_conto_2.data.push(raggruppamento[tasto_segue][cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto][desc_art][nome_comanda][prezzo_un]);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                raggruppamento = {};
                raggruppamento = null;
                //delete raggruppamento;
            }

            var d1 = alasql("select * from oggetto_conto order by nome_comanda DESC,tasto_segue ASC ,portata ASC,ord ASC;");

            //log_velocita(false, "conto attivo 5 query");

            alasql.tables.oggetto_conto.data = d1;

            var elemento_comanda = new Object();
            //SERVE A DARE UN ORDINE MIGLIORE ALLE VARIANTI
            //E' NECESSARISSIMO
            var ord = 0;
            d1.forEach(function (attivo, i) {

                if (ord === 0) {
                    $("#numero_paganti").html(attivo.numero_paganti);
                }



                /*FINE PARTE NUOVA*/

                /*RAGGRUPPAMENTO VECCHIO PER PORTATA*/


                attivo.descrizione_vera = attivo.desc_art;
                //05/05/2019
                //attivo.desc_art = attivo.desc_art.substr(0, 21);

                console.log("prodotto", attivo);
                if (comanda.totale_conto[comanda.conto] === undefined) {
                    comanda.totale_conto[comanda.conto] = new Object();
                }

                if (comanda.totale_conto[comanda.conto][attivo.perc_iva] === undefined) {
                    comanda.totale_conto[comanda.conto][attivo.perc_iva] = new Object();
                    comanda.totale_conto[comanda.conto][attivo.perc_iva].iva = 0;
                    comanda.totale_conto[comanda.conto][attivo.perc_iva].netto = 0;
                    comanda.totale_conto[comanda.conto][attivo.perc_iva].mwst = 0;
                    comanda.totale_conto[comanda.conto][attivo.perc_iva].gesamt = 0;
                }

                comanda.totale_conto[comanda.conto][attivo.perc_iva].iva = parseInt(attivo.perc_iva);
                comanda.totale_conto[comanda.conto][attivo.perc_iva].netto += parseFloat(((attivo.prezzo_un / 100 * (100 - parseInt(attivo.perc_iva)))) * parseInt(attivo.quantita));
                comanda.totale_conto[comanda.conto][attivo.perc_iva].gesamt += parseFloat((attivo.prezzo_un * attivo.quantita));
                comanda.totale_conto[comanda.conto][attivo.perc_iva].mwst = parseFloat((comanda.totale_conto[comanda.conto][attivo.perc_iva].gesamt / 100 * attivo.perc_iva));
                console.log(attivo);

                if (elemento_comanda[attivo.tasto_segue] === undefined) {
                    elemento_comanda[attivo.tasto_segue] = new Object();
                }

                if (elemento_comanda[attivo.tasto_segue][attivo.portata] === undefined) {
                    elemento_comanda[attivo.tasto_segue][attivo.portata] = new Object();
                }



                //ORDINANDO PER ORD RESTANO IN ORDINE ALFABETICO GLI ARTICOLI PER OGNI PORTATA
                if (elemento_comanda[attivo.tasto_segue][attivo.portata][attivo.ord + "_" + ord] === undefined) {
                    //Diventa come (che equivale a dire uguale) attivo
                    elemento_comanda[attivo.tasto_segue][attivo.portata][attivo.ord + "_" + ord] = new Object();
                    elemento_comanda[attivo.tasto_segue][attivo.portata][attivo.ord + "_" + ord] = attivo;
                }
                ord++;

            });


            var conta_segue = 0;
            for (var tasto_segue in elemento_comanda) {
                if (conta_segue > 0) {
                    conto += '<tr class="non-contare-riga text-center" style="text-shadow: -1px 0 black, 0 1px black, 2px 0 black, 0 -1px black;cursor:pointer;font-weight:bold;"><td colspan="5">Segue</td></tr>';
                }
                conta_segue++;

                for (var portata in elemento_comanda[tasto_segue]) {
                    console.log("conto_attivo - analisi - portata", portata);
                    //CONDIZIONI PER MOSTRARE GLI ARTICOLI, VARIANTI, STAMPATI, SERVITI, ECCETERA...

                    if (comanda.nome_portata[portata] === undefined) {
                        comanda.nome_portata[portata] = "";
                    }

                    /**/
                    var array1 = Object.values(elemento_comanda[tasto_segue][portata]);
                    var initialValue = 0;
                    var sommaArticoliPortata = array1.reduce(function (accumulator, currentValue) {
                        if (currentValue.nodo.length > 3) {
                            return accumulator + 0;
                        } else {
                            return accumulator + parseInt(currentValue.quantita);
                        }
                    }, initialValue);


                    /**/

                    conto += '<tr class="comanda non-contare-riga portata text-center" style="border:1px solid black;text-shadow: -1px 0 black, 0 1px black, 2px 0 black, 0 -1px black;cursor:pointer;background-color:grey;font-weight:bold;" ' + comanda.evento + '="$(\'.nascondibile.' + comanda.nome_portata[portata] + '\').toggle();"><td colspan="5">' + comanda.nome_portata[portata] + ' (' + sommaArticoliPortata + ')</td></tr>';
                    if (comanda.mobile !== true) {
                        tab_conto_grande += '<tr class="comanda non-contare-riga portata text-center" style="border:1px solid black;text-shadow: -1px 0 black, 0 1px black, 2px 0 black, 0 -1px black;cursor:pointer;background-color:grey;font-weight:bold;" ' + comanda.evento + '="$(\'.nascondibile.' + comanda.nome_portata[portata] + '\').toggle();"><td colspan="6">' + comanda.nome_portata[portata] + ' (' + sommaArticoliPortata + ')</td></tr>';
                    }

                    for (var indice_articolo in elemento_comanda[tasto_segue][portata]) {



                        //ATTIVO RIPRENDE I SUOI ATTRIBUTI
                        var attivo = elemento_comanda[tasto_segue][portata][indice_articolo];

                        if (comanda.compatibile_xml === true) {
                            iva_esposta = attivo.perc_iva;
                        } else {
                            iva_esposta = ive_misuratore_controller.iva_reparto(parseInt(REPARTO_controller.indice(attivo.reparto.trim())));

                            reparto = "rep_" + parseInt(attivo.reparto.trim());
                        }

                        if (comanda.iva_esposta !== 'true') {
                            iva_esposta = "";
                        }



                        var fiscalizzata_sn = '';
                        if (attivo.fiscalizzata_sn !== undefined && attivo.fiscalizzata_sn !== null && attivo.fiscalizzata_sn === 'S') {
                            fiscalizzata_sn = 'fiscalizzata';
                        }

                        var cod_promo = '';
                        if (attivo.cod_promo !== undefined && attivo.cod_promo !== null && attivo.cod_promo !== 'undefined' && attivo.cod_promo !== '') {
                            cod_promo = 'cod_promo_' + attivo.cod_promo;
                        }

                        var prezzo_un = attivo.prezzo_un;
                        if (cod_promo === 'cod_promo_V_1') {
                            prezzo_un = '0.00';
                        }


                        if (comanda.consegna_presente === true) {
                            if (parseInt(comanda.consegna_fattorino_extra) > 0) {

                                calcolo_consegna_su_totale += parseFloat(prezzo_un) * parseInt(attivo.quantita);
                                comanda.quantita_pizze_totali += parseInt(attivo.quantita);

                            } else
                                if (comanda.categorie_con_consegna_abilitata.indexOf(attivo.categoria) !== -1 && attivo.desc_art[0] !== "+" && attivo.desc_art[0] !== "-" && attivo.desc_art[0] !== "(" && attivo.desc_art[0] !== "*" && attivo.desc_art[0] !== "x" && attivo.desc_art[0] !== "=" && attivo.QTA != "00000") {
                                    if (comanda.consegna_su_totale.length > 0) {
                                        calcolo_consegna_su_totale += parseFloat(prezzo_un) * parseInt(attivo.quantita);
                                        comanda.quantita_pizze_totali += parseInt(attivo.quantita);
                                    }

                                    if (attivo.QTA === "00000") {
                                        /* ARTICOLO OMAGGIO */
                                    } else if (attivo.desc_art.indexOf("MEZZO METRO") !== -1) {
                                        calcolo_consegna_mezzo_metro += parseInt(attivo.quantita);
                                    } else if (attivo.desc_art.indexOf("UN METRO") !== -1) {
                                        calcolo_consegna_un_metro += parseInt(attivo.quantita);
                                    } else if (attivo.desc_art.indexOf("MAXI") !== -1) {
                                        calcolo_consegna_maxi += parseInt(attivo.quantita);
                                        //} else if (comanda.consegna_a_pizza.length > 0 && attivo.desc_art.indexOf("+") === -1 && attivo.desc_art.indexOf("-") === -1 && attivo.desc_art.indexOf("=") === -1)
                                    } else if (comanda.consegna_a_pizza.length > 0 && attivo.desc_art.indexOf("+") !== 0 && attivo.desc_art.indexOf("-") !== 0 && attivo.desc_art.indexOf("=") !== 0) {
                                        calcolo_consegna_a_pizza += parseInt(attivo.quantita);
                                    }


                                    /* MODIFICA FASCE CONSEGNA E PIZZA MISTE */
                                    if ((comanda.una_pizza.length > 0 || comanda.piu_di_una_pizza.length > 0) && attivo.desc_art.indexOf("+") !== 0 && attivo.desc_art.indexOf("-") !== 0 && attivo.desc_art.indexOf("=") !== 0) {
                                        calcolo_consegna_una_pizza += parseInt(attivo.quantita);
                                        calcolo_consegna_piu_di_una_pizza += parseInt(attivo.quantita);

                                    }

                                    if ((comanda.fissa_una_pizza.length > 0 || comanda.fissa_piu_di_una_pizza.length > 0) && attivo.desc_art.indexOf("+") !== 0 && attivo.desc_art.indexOf("-") !== 0 && attivo.desc_art.indexOf("=") !== 0) {
                                        fissa_calcolo_consegna_una_pizza += parseInt(attivo.quantita);
                                        fissa_calcolo_consegna_piu_di_una_pizza += parseInt(attivo.quantita);
                                        comanda.quantita_pizze_totali += parseInt(attivo.quantita);
                                    }
                                }
                        }

                        console.log("conto_attivo - analisi - art", attivo.desc_art, attivo.quantita);
                        console.log("funzionidb conto_attivo", attivo);
                        if (attivo.stampata_sn === "S") {
                            comanda.stampata_s = true;
                        } else {
                            comanda.disabilita_tasto_bis = true;
                            if (attivo.BIS !== lettera_conto_split()) {
                                comanda.disabilita_tasto_tavolo = true;
                            }
                        }
                        if ((comanda.pizzeria_asporto !== true && (attivo.stampata_sn === "S")) && !(comanda.pizzeria_asporto !== true && comanda.tavolo.left(7) === "ASPORTO")) {



                            //E' UNA REGOLA CHE NON VALE PER MATTIA131
                            if (comanda.mobile === true && comanda.licenza !== "mattia131") {

                                if (attivo.desc_art[0] === "+" || attivo.desc_art[0] === "-" || attivo.desc_art[0] === "(" || attivo.desc_art[0] === "*" || attivo.desc_art[0] === "x" || attivo.desc_art[0] === "=") {

                                    conto += '<tr style="height:0;" class="comanda non-contare-riga"></tr>';
                                    ultime_battiture += '<tr style="height:0;" class="comanda non-contare-riga"></tr>';
                                    tab_conto_grande += '<tr style="height:0;" class="comanda non-contare-riga"></tr>';
                                    tab_conto_separato_grande += '<tr style="height:0;" class="comanda non-contare-riga"></tr>';
                                    tab_conto_separato_grande += '<tr categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda ' + comanda.tavolo + ' ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td style="color:transparent;" ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td><td></td></tr>';
                                    conto += '<tr portata="' + attivo.portata + '" categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda nascondibile ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td    ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="color:transparent;background-color:black;" id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="max-width:50vw;overflow:hidden;background-color:black;" id="art_' + attivo.prog_inser + '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro;' + prezzo_un + '</td><td></td></tr>';
                                    tab_conto_grande += '<tr categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda nascondibile ' + comanda.tavolo + ' ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td   ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="color:transparent;background-color:black;" id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro; ' + prezzo_un + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro; ' + parseFloat(parseFloat(prezzo_un.replace(",", ".")) * parseInt(attivo.quantita)).toFixed(2) + '</td><td></td></tr>';
                                } else {
                                    conto += '<tr  portata="' + attivo.portata + '" categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda nascondibile ' + comanda.tavolo + ' ' + comanda.nome_portata[portata] + '" ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '" ><td>' + attivo.quantita + '</td><td style="max-width:50vw;overflow:hidden;">' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td>&euro;' + prezzo_un + '</td><td></td></tr>';
                                    tab_conto_grande += '<tr categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda nascondibile ' + comanda.tavolo + ' ' + comanda.nome_portata[portata] + '"  ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '"><td>' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td>&euro; ' + prezzo_un + '</td><td>&euro; ' + parseFloat(parseFloat(prezzo_un.replace(",", ".")) * parseInt(attivo.quantita)).toFixed(2) + '</td><td></td></tr>';
                                    tab_conto_separato_grande += '<tr categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '"  style="background-color:black;" id="art_' + attivo.prog_inser + '" ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')"><td>' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td>&euro;' + prezzo_un + '</td><td></td></tr>';
                                }
                            } else {
                                if (comanda.cancellazione_articolo === 'S' || (comanda.tavolo === "BANCO" || comanda.tavolo === "BAR" || comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === "TAKE AWAY")) {
                                    var x = 'X';
                                    var eliminart = ' ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\',false,true);" ';
                                } else {
                                    var x = '';
                                    var eliminart = '';
                                }

                                //PER IL PC CHE HA GIA' STAMPATO LA COMANDA

                                console.log("CA ARTICOLO PRINC", attivo.desc_art, attivo.desc_art[0]);
                                //SE L'ARTICOLO E' UNA VARIANTE
                                if (attivo.desc_art[0] === "+" || attivo.desc_art[0] === "-" || attivo.desc_art[0] === "(" || attivo.desc_art[0] === "*" || attivo.desc_art[0] === "x" || attivo.desc_art[0] === "=") {

                                    conto += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                    ultime_battiture += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                    tab_conto_grande += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                    tab_conto_separato_grande += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                    tab_conto_separato_grande += '<tr  categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td style="color:transparent;" ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td><td ' + eliminart + ' style="background-color:black;color:red;cursor:pointer;">' + x + '</td></tr>';
                                    console.log("CA ARTICOLO VARIANTE", attivo.desc_art[0]);
                                    conto += '<tr portata="' + attivo.portata + '" categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda nascondibile ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="color:transparent;background-color:black;" id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="max-width:50vw;overflow:hidden;background-color:black;" id="art_' + attivo.prog_inser + '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro;' + prezzo_un + '</td><td ' + eliminart + ' style="background-color:black;color:red;cursor:pointer;">' + x + '</td></tr>';
                                    tab_conto_grande += '<tr categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda nascondibile ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="color:transparent;background-color:black;" id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro; ' + prezzo_un + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro;' + parseFloat(parseFloat(prezzo_un.replace(",", ".")) * parseInt(attivo.quantita)).toFixed(2) + '</td><td ' + eliminart + ' style="background-color:black;color:red;cursor:pointer;">' + x + '</td></tr>';
                                }
                                //ALTRIMENTI LI STAMPA IN MODO NORMALE
                                else {
                                    console.log("CA ARTICOLO NON VARIANTE", attivo.desc_art);
                                    conto += '<tr portata="' + attivo.portata + '" categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda nascondibile ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td style="background-color:black;" ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')"  id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;max-width:50vw;overflow:hidden;" id="art_' + attivo.prog_inser + '">' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro;' + prezzo_un + '</td><td ' + eliminart + ' style="color:red;background-color:black;cursor:pointer;">' + x + '</td></tr>';
                                    tab_conto_grande += '<tr categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda nascondibile ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td  ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro; ' + prezzo_un + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro; ' + parseFloat(parseFloat(prezzo_un.replace(",", ".")) * parseInt(attivo.quantita)).toFixed(2) + '</td><td ' + eliminart + ' style="background-color:black;color:red;cursor:pointer;">' + x + '</td></tr>';
                                    tab_conto_separato_grande += '<tr categoria="' + attivo.categoria + '" style="background-color:black;" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '"><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td><td ' + eliminart + ' style="background-color:black;color:red;cursor:pointer;">' + x + '</td></tr>';
                                }
                            }

                        }

                        //ALTRIMENTI
                        else {


                            console.log("CA ARTICOLO PRINC", attivo.desc_art, attivo.desc_art[0]);
                            //SE L'ARTICOLO E' UNA VARIANTE
                            if (attivo.desc_art[0] === "+" || attivo.desc_art[0] === "-" || attivo.desc_art[0] === "(" || attivo.desc_art[0] === "*" || attivo.desc_art[0] === "x" || attivo.desc_art[0] === "=") {

                                if (attivo.desc_art[0] === "+") {
                                    numero_nodo_variante_piu++;
                                }




                                if (attivo.desc_art[0] !== "-") {
                                    if (ultima_maxi === true) {

                                        if (numero_nodo_variante_piu > 1 && attivo.prezzo_varianti_maxi !== undefined && attivo.prezzo_varianti_maxi !== null && attivo.prezzo_varianti_maxi !== "undefined" && attivo.prezzo_varianti_maxi !== "null" && attivo.prezzo_varianti_maxi !== "" && !isNaN(parseFloat(attivo.prezzo_varianti_maxi))) {
                                            prezzo_un = attivo.prezzo_varianti_maxi;
                                        } else if (attivo.prezzo_maxi_prima !== undefined && attivo.prezzo_maxi_prima !== null && attivo.prezzo_maxi_prima !== "undefined" && attivo.prezzo_maxi_prima !== "null" && attivo.prezzo_maxi_prima !== "" && !isNaN(parseFloat(attivo.prezzo_maxi_prima))) {
                                            prezzo_un = attivo.prezzo_maxi_prima;
                                        }
                                    } else {
                                        if (numero_nodo_variante_piu > 1 && attivo.prezzo_varianti_aggiuntive !== undefined && attivo.prezzo_varianti_aggiuntive !== null && attivo.prezzo_varianti_aggiuntive !== "undefined" && attivo.prezzo_varianti_aggiuntive !== "null" && attivo.prezzo_varianti_aggiuntive !== "" && !isNaN(parseFloat(attivo.prezzo_varianti_aggiuntive))) {
                                            prezzo_un = attivo.prezzo_varianti_aggiuntive;
                                        }
                                    }
                                }

                                if (ultima_due_gusti === true && attivo.desc_art[0] === "=") {
                                    prezzo_un = "0.00";
                                }
                                /*if (ultima_schiacciata === true && attivo.desc_art[0] === "=") {
                                    prezzo_un = "0.00";
                                }*/



                                conto += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                ultime_battiture += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                tab_conto_grande += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                tab_conto_separato_grande += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                tab_conto_separato_grande += '<tr categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td style="color:transparent;" ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="color:red;cursor:pointer;">X</td></tr>';
                                console.log("CA ARTICOLO VARIANTE", attivo.desc_art[0]);
                                conto += '<tr portata="' + attivo.portata + '" categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  style="color:transparent;" ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td style="max-width:50vw;overflow:hidden;" ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="color:red;cursor:pointer;">X</td></tr>';
                                ultime_battiture += '<tr categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  style="color:transparent;" ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="color:red;cursor:pointer;">X</td></tr>';
                                tab_conto_grande += '<tr categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  style="color:transparent;" ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro; ' + prezzo_un + '</td><td ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro; ' + parseFloat(parseFloat(prezzo_un.replace(",", ".")) * parseInt(attivo.quantita)).toFixed(2) + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="color:red;cursor:pointer;">X</td></tr>';
                                //MODIFICA 07/06/2018
                                if (comanda.pizzeria_asporto === true) {

                                    var where = "";
                                    if (attivo.desc_art[0] === "+" || attivo.desc_art[0] === "-" || attivo.desc_art[0] === "*" || attivo.desc_art[0] === "x" || attivo.desc_art[0] === "(" || attivo.desc_art[0] === "=") {
                                        where += " AND prog_inser='" + attivo.prog_inser + "' ";
                                    } else {
                                        where += " AND desc_art='" + attivo.descrizione_vera + "' and prezzo_un='" + attivo.prezzo_un + "' ";
                                    }

                                    where += " AND ntav_comanda = '" + comanda.tavolo + "' ";
                                    //BASSA PRIORITA PER NON INTACCARE PRESTAZIONI
                                    var query_prezzo_vero = "update comanda set prezzo_vero='" + prezzo_un + "' where  desc_art NOT LIKE '= 1/2 %' AND stato_record='ATTIVO' " + where + ";";

                                    comanda.sock.send({ tipo: "set_prezzo_vero", operatore: comanda.operatore, query: query_prezzo_vero, terminale: comanda.terminale, ip: comanda.ip_address });

                                    comanda.sincro.query(query_prezzo_vero, function () { });
                                }

                            }
                            //ALTRIMENTI LI STAMPA IN MODO NORMALE
                            else {

                                if (attivo.desc_art.indexOf("MAXI") !== -1) {
                                    ultima_maxi = true;
                                } else {
                                    ultima_maxi = false;
                                }

                                /*if (attivo.desc_art.indexOf("SCHIACCIATA") !== -1) {
                                    ultima_due_gusti = true;
                                    var nodo_art_princ = attivo.nodo.substr(0, 3);
                                    var prezzo_schiacciata = "0.00";
                                    var contatore_schiacciata = 0;
                                    for (var articolo in elemento_comanda[tasto_segue][portata]) {
                                        var art = elemento_comanda[tasto_segue][portata][articolo];
                                        var nodo_var = art.nodo.substr(0, 3);
                                        if (nodo_art_princ === nodo_var && art.desc_art[0] === "=") {

                                            contatore_schiacciata++;
                                            var prz = art.prezzo_un;
                                            if (attivo.desc_art.indexOf("MAXI") !== -1) {
                                                prz = art.prezzo_maxi;
                                            }

                                            if (parseFloat(prz) > parseFloat(prezzo_schiacciata)) {
                                                prezzo_schiacciata = prz;
                                            }
                                        }
                                    }
                                    prezzo_un = prezzo_schiacciata;
                                   
                                    //MODIFICA 07/06/2018
                                    //IN QUESTO CASO SUDDIVIDE L'INCASSO PER OGNI GUSTO IN BASE AL NODO
                                    //SOLO NEL NODO PIU LUNGO DI TRE RISPETTO A NODO_ART_PRINC
                                    //CIOE' AI GUSTI
                                    //IL CONTATORE DICE QUANTI GUSTI SONO
                                    if (comanda.pizzeria_asporto === true) {
                                        var where = "";
                                        if (attivo.desc_art[0] === "+" || attivo.desc_art[0] === "-" || attivo.desc_art[0] === "*" || attivo.desc_art[0] === "x" || attivo.desc_art[0] === "(" || attivo.desc_art[0] === "=") {
                                            where += " AND prog_inser='" + attivo.prog_inser + "' ";
                                        } else if (attivo.desc_art.indexOf("SCHIACCIATA") !== -1) {
                                            where += " AND prog_inser='" + attivo.prog_inser + "' ";
                                        } else {
                                            where += " AND desc_art='" + attivo.descrizione_vera + "' and prezzo_un='" + attivo.prezzo_un + "' ";
                                        }

                                        where += " AND ntav_comanda = '" + comanda.tavolo + "' ";
                                        //BASSA PRIORITA PER NON INTACCARE PRESTAZIONI

                                        if (attivo.prezzo_un === "") {
                                            attivo.prezzo_un = "0";
                                        }
                                        prezzo_un = parseFloat(prezzo_un).toFixed(2);

                                        var query_prezzo_vero = "update comanda set prezzo_vero='" + prezzo_un + "' where  desc_art NOT LIKE '= 1/2 %' AND stato_record='ATTIVO' " + where + ";";

                                        comanda.sock.send({ tipo: "set_prezzo_vero", operatore: comanda.operatore, query: query_prezzo_vero, terminale: comanda.terminale, ip: comanda.ip_address });

                                        comanda.sincro.query(query_prezzo_vero, function () { });
                                    }

                                } else */
                                if (attivo.desc_art.indexOf("DUE GUSTI") !== -1) {
                                    ultima_due_gusti = true;
                                    var nodo_art_princ = attivo.nodo.substr(0, 3);
                                    var prezzo_due_gusti = attivo.prezzo_un;
                                    var contatore_duegusti = 0;
                                    for (var articolo in elemento_comanda[tasto_segue][portata]) {
                                        var art = elemento_comanda[tasto_segue][portata][articolo];
                                        var nodo_var = art.nodo.substr(0, 3);
                                        if (nodo_art_princ === nodo_var && art.desc_art[0] === "=") {

                                            contatore_duegusti++;
                                            // per calcolare prezzo di due gusti e aggiunte attivo.prezzo_un e prezzo di due gusti art.prezzo_un e prezzo di aggiunte
                                            var prz = parseFloat(art.prezzo_un)+parseFloat(attivo.prezzo_un);
                                            if (attivo.desc_art.indexOf("MAXI") !== -1) {
                                                  // per calcolare prezzo di due gusti e aggiunte attivo.prezzo_un e prezzo di due gusti art.prezzo_un e prezzo di aggiunte se la pizza e maxi
                                                prz = parseFloat(art.prezzo_maxi)+parseFloat( attivo.prezzo_un);
                                            }

                                            if (parseFloat(prz) > parseFloat(prezzo_due_gusti)) {
                                                prezzo_due_gusti = prz;
                                            }
                                        }
                                    }
                                    prezzo_un = prezzo_due_gusti;
                                    /*var prezzo_diviso = arrotonda_prezzo(parseFloat(prezzo_due_gusti) / parseInt(contatore_duegusti));*/
                                    //MODIFICA 07/06/2018
                                    //IN QUESTO CASO SUDDIVIDE L'INCASSO PER OGNI GUSTO IN BASE AL NODO
                                    //SOLO NEL NODO PIU LUNGO DI TRE RISPETTO A NODO_ART_PRINC
                                    //CIOE' AI GUSTI
                                    //IL CONTATORE DICE QUANTI GUSTI SONO
                                    if (comanda.pizzeria_asporto === true) {
                                        var where = "";
                                        if (attivo.desc_art[0] === "+" || attivo.desc_art[0] === "-" || attivo.desc_art[0] === "*" || attivo.desc_art[0] === "x" || attivo.desc_art[0] === "(" || attivo.desc_art[0] === "=") {
                                            where += " AND prog_inser='" + attivo.prog_inser + "' ";
                                        } /*else if (attivo.desc_art.indexOf("SCHIACCIATA") !== -1) {
                                            where += " AND prog_inser='" + attivo.prog_inser + "' ";
                                        }*/ else if (attivo.desc_art.indexOf("DUE GUSTI") !== -1) {
                                            where += " AND prog_inser='" + attivo.prog_inser + "' ";
                                        } else {
                                            where += " AND desc_art='" + attivo.descrizione_vera + "' and prezzo_un='" + attivo.prezzo_un + "' ";
                                        }
                                        //BASSA PRIORITA PER NON INTACCARE PRESTAZIONI

                                        if (attivo.prezzo_un === "") {
                                            attivo.prezzo_un = "0";
                                        }
                                        prezzo_un = parseFloat(prezzo_un).toFixed(2);

                                        var query_prezzo_vero = "update comanda set prezzo_vero='" + prezzo_un + "' where  desc_art NOT LIKE '= 1/2 %' AND stato_record='ATTIVO' " + where + ";";

                                        comanda.sock.send({ tipo: "set_prezzo_vero", operatore: comanda.operatore, query: query_prezzo_vero, terminale: comanda.terminale, ip: comanda.ip_address });

                                        comanda.sincro.query(query_prezzo_vero, function () { });
                                    }

                                } else {
                                    ultima_due_gusti = false;
                                    //MODIFICA 07/06/2018
                                    if (comanda.pizzeria_asporto === true) {
                                        var where = "";
                                        if (attivo.desc_art[0] === "+" || attivo.desc_art[0] === "-" || attivo.desc_art[0] === "*" || attivo.desc_art[0] === "x" || attivo.desc_art[0] === "(" || attivo.desc_art[0] === "=") {
                                            where += " AND prog_inser='" + attivo.prog_inser + "' ";
                                        } else {
                                            where += " AND desc_art='" + attivo.descrizione_vera + "' and prezzo_un='" + attivo.prezzo_un + "' ";
                                        }

                                        where += " AND ntav_comanda = '" + comanda.tavolo + "' ";
                                        //BASSA PRIORITA PER NON INTACCARE PRESTAZIONI
                                        var query_prezzo_vero = "update comanda set prezzo_vero='" + prezzo_un + "' where  desc_art NOT LIKE '= 1/2 %' AND stato_record='ATTIVO' " + where + ";";

                                        comanda.sock.send({ tipo: "set_prezzo_vero", operatore: comanda.operatore, query: query_prezzo_vero, terminale: comanda.terminale, ip: comanda.ip_address });

                                        comanda.sincro.query(query_prezzo_vero, function () { });

                                    }

                                }

                                numero_nodo_variante_piu = 0;
                                console.log("CA ARTICOLO NON VARIANTE", attivo.desc_art);
                                conto += '<tr portata="' + attivo.portata + '" categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td style="max-width:50vw;overflow:hidden;" ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="color:red;cursor:pointer;">X</td></tr>';
                                ultime_battiture += '<tr categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td style="max-width:50vw;overflow:hidden;" ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="color:red;cursor:pointer;">X</td></tr>';
                                tab_conto_grande += '<tr  categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro; ' + prezzo_un + '</td><td ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro; ' + parseFloat(parseFloat(prezzo_un.replace(",", ".")) * parseInt(attivo.quantita)).toFixed(2) + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="color:red;cursor:pointer;">X</td></tr>';
                                tab_conto_separato_grande += '<tr categoria="' + attivo.categoria + '"  class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="color:red;cursor:pointer;">X</td></tr>';
                            }



                        }
                        //FINE CONDIZIONI
                    }
                    //FINE CICLO FOR IN 1 --ARTICOLI
                }
            }
            //FINE CICLO FOR IN 2


            if (numero_conti_attivi === 2) {
                var d1 = alasql("select * from oggetto_conto_2 order by nome_comanda DESC, tasto_segue ASC,portata ASC, ord ASC;");

                //log_velocita(false, "conto attivo 6 query");
                alasql.tables.oggetto_conto_2.data = d1;
                //console.log(d1);

                var elemento_comanda = new Object();
                //SERVE A DARE UN ORDINE MIGLIORE ALLE VARIANTI
                //E' NECESSARISSIMO
                var ord = 0;
                d1.forEach(function (attivo, i) {

                    /*FINE PARTE NUOVA*/

                    /*RAGGRUPPAMENTO VECCHIO PER PORTATA*/


                    attivo.descrizione_vera = attivo.desc_art;

                    //05/05/2019
                    //attivo.desc_art = attivo.desc_art.substr(0, 21);
                    console.log("prodotto", attivo);
                    if (comanda.totale_conto[comanda.conto] === undefined) {
                        comanda.totale_conto[comanda.conto] = new Object();
                    }

                    if (comanda.totale_conto[comanda.conto][attivo.perc_iva] === undefined) {
                        comanda.totale_conto[comanda.conto][attivo.perc_iva] = new Object();
                        comanda.totale_conto[comanda.conto][attivo.perc_iva].iva = 0;
                        comanda.totale_conto[comanda.conto][attivo.perc_iva].netto = 0;
                        comanda.totale_conto[comanda.conto][attivo.perc_iva].mwst = 0;
                        comanda.totale_conto[comanda.conto][attivo.perc_iva].gesamt = 0;
                    }

                    comanda.totale_conto[comanda.conto][attivo.perc_iva].iva = parseInt(attivo.perc_iva);
                    comanda.totale_conto[comanda.conto][attivo.perc_iva].netto += parseFloat(((attivo.prezzo_un / 100 * (100 - parseInt(attivo.perc_iva)))) * parseInt(attivo.quantita));
                    comanda.totale_conto[comanda.conto][attivo.perc_iva].gesamt += parseFloat((attivo.prezzo_un * attivo.quantita));
                    comanda.totale_conto[comanda.conto][attivo.perc_iva].mwst = parseFloat((comanda.totale_conto[comanda.conto][attivo.perc_iva].gesamt / 100 * attivo.perc_iva));
                    console.log(attivo);
                    if (elemento_comanda[attivo.tasto_segue] === undefined) {
                        elemento_comanda[attivo.tasto_segue] = new Object();
                    }

                    if (elemento_comanda[attivo.tasto_segue][attivo.portata] === undefined) {
                        elemento_comanda[attivo.tasto_segue][attivo.portata] = new Object();
                    }



                    //ORDINANDO PER ORD RESTANO IN ORDINE ALFABETICO GLI ARTICOLI PER OGNI PORTATA
                    if (elemento_comanda[attivo.tasto_segue][attivo.portata][attivo.ord + "_" + ord] === undefined) {
                        //Diventa come (che equivale a dire uguale) attivo
                        elemento_comanda[attivo.tasto_segue][attivo.portata][attivo.ord + "_" + ord] = new Object();
                        elemento_comanda[attivo.tasto_segue][attivo.portata][attivo.ord + "_" + ord] = attivo;
                    }
                    ord++;

                });

                conto += '<tr class="non-contare-riga" id="righe_conto_2"><td colspan="5"><center><strong> - - - CONTO 2 - - - </strong></center></td></tr>';
                ultime_battiture += '<tr class="non-contare-riga" id="righe_conto_2"><td colspan="5"><center><strong> - - - CONTO 2 - - - </strong></center></td></tr>';
                tab_conto_grande += '<tr class="non-contare-riga"><td colspan="6"><center><strong> - - - CONTO 2 - - - </strong></center></td></tr>';

                var conta_segue = 0;
                for (var tasto_segue in elemento_comanda) {
                    if (conta_segue > 0) {
                        conto += '<tr class="non-contare-riga text-center" style="text-shadow: -1px 0 black, 0 1px black, 2px 0 black, 0 -1px black;cursor:pointer;font-weight:bold;"><td colspan="5">Segue</td></tr>';
                    }
                    conta_segue++;
                    for (var portata in elemento_comanda[tasto_segue]) {
                        console.log("conto_attivo - analisi - portata", portata);
                        //CONDIZIONI PER MOSTRARE GLI ARTICOLI, VARIANTI, STAMPATI, SERVITI, ECCETERA...

                        if (comanda.nome_portata[portata] === undefined) {
                            comanda.nome_portata[portata] = "";
                        }

                        var array1 = Object.values(elemento_comanda[tasto_segue][portata]);
                        var initialValue = 0;
                        var sommaArticoliPortata = array1.reduce(function (accumulator, currentValue) {
                            if (currentValue.nodo.length > 3) {
                                return accumulator + 0;
                            } else {
                                return accumulator + parseInt(currentValue.quantita);
                            }
                        }, initialValue);

                        conto += '<tr class="comanda non-contare-riga portata text-center" style="border:1px solid black;text-shadow: -1px 0 black, 0 1px black, 2px 0 black, 0 -1px black;cursor:pointer;background-color:grey;font-weight:bold;" ' + comanda.evento + '="$(\'.nascondibile.' + comanda.nome_portata[portata] + '\').toggle();"><td colspan="5">' + comanda.nome_portata[portata] + ' (' + sommaArticoliPortata + ')</td></tr>';
                        if (comanda.mobile !== true) {
                            tab_conto_grande += '<tr class="comanda non-contare-riga portata text-center" style="border:1px solid black;text-shadow: -1px 0 black, 0 1px black, 2px 0 black, 0 -1px black;cursor:pointer;background-color:grey;font-weight:bold;" ' + comanda.evento + '="$(\'.nascondibile.' + comanda.nome_portata[portata] + '\').toggle();"><td colspan="6">' + comanda.nome_portata[portata] + ' (' + sommaArticoliPortata + ')</td></tr>';
                        }

                        for (var indice_articolo in elemento_comanda[tasto_segue][portata]) {



                            //ATTIVO RIPRENDE I SUOI ATTRIBUTI
                            var attivo = elemento_comanda[tasto_segue][portata][indice_articolo];

                            if (comanda.compatibile_xml === true) {
                                iva_esposta = attivo.perc_iva;
                            } else {
                                iva_esposta = ive_misuratore_controller.iva_reparto(parseInt(REPARTO_controller.indice(attivo.reparto.trim())));

                                reparto = "rep_" + parseInt(attivo.reparto.trim());
                            }
                            if (comanda.iva_esposta !== 'true') {
                                iva_esposta = "";
                            }

                            var fiscalizzata_sn = '';
                            if (attivo.fiscalizzata_sn !== undefined && attivo.fiscalizzata_sn !== null && attivo.fiscalizzata_sn === 'S') {
                                fiscalizzata_sn = 'fiscalizzata';
                            }

                            var cod_promo = '';
                            if (attivo.cod_promo !== undefined && attivo.cod_promo !== null && attivo.cod_promo !== '') {
                                cod_promo = 'cod_promo_' + attivo.cod_promo;
                            }

                            var prezzo_un = attivo.prezzo_un;
                            if (cod_promo === 'cod_promo_V_1') {
                                prezzo_un = '0.00';
                            }


                            if (comanda.consegna_presente === true) {
                                if (parseInt(comanda.consegna_fattorino_extra) > 0) {
                                    calcolo_consegna_su_totale += parseFloat(prezzo_un) * parseInt(attivo.quantita);
                                    comanda.quantita_pizze_totali += parseInt(attivo.quantita);
                                } else
                                    if (comanda.categorie_con_consegna_abilitata.indexOf(attivo.categoria) !== -1 && attivo.desc_art[0] !== "+" && attivo.desc_art[0] !== "-" && attivo.desc_art[0] !== "(" && attivo.desc_art[0] !== "*" && attivo.desc_art[0] !== "x" && attivo.desc_art[0] !== "=" && attivo.QTA != "00000") {
                                        if (comanda.consegna_su_totale.length > 0) {
                                            calcolo_consegna_su_totale += parseFloat(prezzo_un) * parseInt(attivo.quantita);
                                            comanda.quantita_pizze_totali += parseInt(attivo.quantita);
                                        }

                                        if (attivo.QTA === "00000") {
                                            /* ARTICOLO OMAGGIO */
                                        } else if (attivo.desc_art.indexOf("MEZZO METRO") !== -1) {
                                            calcolo_consegna_mezzo_metro += parseInt(attivo.quantita);
                                        } else if (attivo.desc_art.indexOf("UN METRO") !== -1) {
                                            calcolo_consegna_un_metro += parseInt(attivo.quantita);
                                        } else if (attivo.desc_art.indexOf("MAXI") !== -1) {
                                            calcolo_consegna_maxi += parseInt(attivo.quantita);
                                        } else if (comanda.consegna_a_pizza.length > 0 && attivo.desc_art.indexOf("+") !== 0 && attivo.desc_art.indexOf("-") !== 0 && attivo.desc_art.indexOf("=") !== 0) {
                                            calcolo_consegna_a_pizza += parseInt(attivo.quantita);
                                        }

                                        /* MODIFICA CONSEGNE MISTE */
                                        if ((comanda.una_pizza.length > 0 || comanda.piu_di_una_pizza.length > 0) && attivo.desc_art.indexOf("+") !== 0 && attivo.desc_art.indexOf("-") !== 0 && attivo.desc_art.indexOf("=") !== 0) {
                                            calcolo_consegna_una_pizza += parseInt(attivo.quantita);
                                            calcolo_consegna_piu_di_una_pizza += parseInt(attivo.quantita);
                                        }
                                        if ((comanda.fissa_una_pizza.length > 0 || comanda.fissa_piu_di_una_pizza.length > 0) && attivo.desc_art.indexOf("+") !== 0 && attivo.desc_art.indexOf("-") !== 0 && attivo.desc_art.indexOf("=") !== 0) {
                                            fissa_calcolo_consegna_una_pizza += parseInt(attivo.quantita);
                                            fissa_calcolo_consegna_piu_di_una_pizza += parseInt(attivo.quantita);
                                            comanda.quantita_pizze_totali += parseInt(attivo.quantita);
                                        }
                                    }
                            }

                            console.log("conto_attivo - analisi - art", attivo.desc_art, attivo.quantita);
                            console.log("funzionidb conto_attivo", attivo);
                            if (attivo.stampata_sn === "S") {
                                comanda.stampata_s = true;
                            } else {
                                comanda.disabilita_tasto_bis = true;
                                if (attivo.BIS !== lettera_conto_split()) {
                                    comanda.disabilita_tasto_tavolo = true;
                                }
                            }
                            if ((comanda.pizzeria_asporto !== true && (attivo.stampata_sn === "S")) && !(comanda.pizzeria_asporto !== true && comanda.tavolo.left(7) === "ASPORTO")) {



                                //E' UNA REGOLA CHE NON VALE PER MATTIA131
                                if (comanda.mobile === true && comanda.licenza !== "mattia131") {

                                    if (attivo.desc_art[0] === "+" || attivo.desc_art[0] === "-" || attivo.desc_art[0] === "(" || attivo.desc_art[0] === "*" || attivo.desc_art[0] === "x" || attivo.desc_art[0] === "=") {

                                        conto += '<tr style="height:0;" class="comanda non-contare-riga"></tr>';
                                        ultime_battiture += '<tr style="height:0;" class="comanda non-contare-riga"></tr>';
                                        tab_conto_grande += '<tr style="height:0;" class="comanda non-contare-riga"></tr>';
                                        tab_conto_separato_2_grande += '<tr style="height:0;" class="comanda non-contare-riga"></tr>';
                                        tab_conto_separato_grande += '<tr categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda ' + comanda.tavolo + ' ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td style="color:transparent;" ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td><td></td></tr>';
                                        conto += '<tr portata="' + attivo.portata + '" categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda nascondibile ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td    ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="color:transparent;background-color:black;" id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;max-width:50vw;overflow:hidden;" id="art_' + attivo.prog_inser + '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro;' + prezzo_un + '</td><td></td></tr>';
                                        tab_conto_grande += '<tr categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda nascondibile ' + comanda.tavolo + ' ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td   ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="color:transparent;background-color:black;" id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro; ' + prezzo_un + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro; ' + parseFloat(parseFloat(prezzo_un.replace(",", ".")) * parseInt(attivo.quantita)).toFixed(2) + '</td><td></td></tr>';
                                    } else {
                                        conto += '<tr portata="' + attivo.portata + '" categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda nascondibile ' + comanda.tavolo + ' ' + comanda.nome_portata[portata] + '" ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '" ><td>' + attivo.quantita + '</td><td style="max-width:50vw;overflow:hidden;">' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td>&euro;' + prezzo_un + '</td><td></td></tr>';
                                        tab_conto_grande += '<tr categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda nascondibile ' + comanda.tavolo + ' ' + comanda.nome_portata[portata] + '"  ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '"><td>' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td>&euro; ' + prezzo_un + '</td><td>&euro; ' + parseFloat(parseFloat(prezzo_un.replace(",", ".")) * parseInt(attivo.quantita)).toFixed(2) + '</td><td></td></tr>';
                                        tab_conto_separato_2_grande += '<tr categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '"  style="background-color:black;" id="art_' + attivo.prog_inser + '" ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')"><td>' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td>&euro;' + prezzo_un + '</td><td></td></tr>';
                                    }
                                } else {
                                    if (comanda.cancellazione_articolo === 'S' || (comanda.tavolo === "BANCO" || comanda.tavolo === "BAR" || comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === "TAKE AWAY")) {
                                        var x = 'X';
                                        var eliminart = ' ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\',false,true);" ';
                                    } else {
                                        var x = '';
                                        var eliminart = '';
                                    }

                                    //PER IL PC CHE HA GIA' STAMPATO LA COMANDA

                                    console.log("CA ARTICOLO PRINC", attivo.desc_art, attivo.desc_art[0]);
                                    //SE L'ARTICOLO E' UNA VARIANTE
                                    if (attivo.desc_art[0] === "+" || attivo.desc_art[0] === "-" || attivo.desc_art[0] === "(" || attivo.desc_art[0] === "*" || attivo.desc_art[0] === "x" || attivo.desc_art[0] === "=") {

                                        conto += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                        ultime_battiture += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                        tab_conto_grande += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                        tab_conto_separato_2_grande += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                        tab_conto_separato_2_grande += '<tr categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td style="color:transparent;" ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td><td ' + eliminart + ' style="background-color:black;color:red;cursor:pointer;">' + x + '</td></tr>';
                                        console.log("CA ARTICOLO VARIANTE", attivo.desc_art[0]);
                                        conto += '<tr portata="' + attivo.portata + '" categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda nascondibile ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="color:transparent;background-color:black;" id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;max-width:50vw;overflow:hidden;" id="art_' + attivo.prog_inser + '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro;' + prezzo_un + '</td><td ' + eliminart + ' style="background-color:black;color:red;cursor:pointer;">' + x + '</td></tr>';
                                        tab_conto_grande += '<tr categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda nascondibile ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="color:transparent;background-color:black;" id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro; ' + prezzo_un + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro;' + parseFloat(parseFloat(prezzo_un.replace(",", ".")) * parseInt(attivo.quantita)).toFixed(2) + '</td><td ' + eliminart + ' style="background-color:black;color:red;cursor:pointer;">' + x + '</td></tr>';
                                    }
                                    //ALTRIMENTI LI STAMPA IN MODO NORMALE
                                    else {
                                        console.log("CA ARTICOLO NON VARIANTE", attivo.desc_art);
                                        conto += '<tr portata="' + attivo.portata + '" categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda nascondibile ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td style="background-color:black;" ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')"  id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;max-width:50vw;overflow:hidden;" id="art_' + attivo.prog_inser + '">' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro;' + prezzo_un + '</td><td ' + eliminart + ' style="color:red;background-color:black;cursor:pointer;">' + x + '</td></tr>';
                                        tab_conto_grande += '<tr categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda nascondibile ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td  ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro; ' + prezzo_un + '</td><td ' + comanda.evento + '="popup_sposta_articolo(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro; ' + parseFloat(parseFloat(prezzo_un.replace(",", ".")) * parseInt(attivo.quantita)).toFixed(2) + '</td><td ' + eliminart + ' style="background-color:black;color:red;cursor:pointer;">' + x + '</td></tr>';
                                        tab_conto_separato_2_grande += '<tr categoria="' + attivo.categoria + '" style="background-color:black;" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '"><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td><td ' + eliminart + ' style="background-color:black;color:red;cursor:pointer;">' + x + '</td></tr>';
                                    }
                                }

                            }

                            //ALTRIMENTI
                            else {


                                console.log("CA ARTICOLO PRINC", attivo.desc_art, attivo.desc_art[0]);
                                //SE L'ARTICOLO E' UNA VARIANTE
                                if (attivo.desc_art[0] === "+" || attivo.desc_art[0] === "-" || attivo.desc_art[0] === "(" || attivo.desc_art[0] === "*" || attivo.desc_art[0] === "x" || attivo.desc_art[0] === "=") {

                                    if (attivo.desc_art[0] === "+") {
                                        numero_nodo_variante_piu++;
                                    }




                                    if (attivo.desc_art[0] !== "-") {
                                        if (ultima_maxi === true) {

                                            if (numero_nodo_variante_piu > 1 && attivo.prezzo_varianti_maxi !== undefined && attivo.prezzo_varianti_maxi !== null && attivo.prezzo_varianti_maxi !== "undefined" && attivo.prezzo_varianti_maxi !== "null" && attivo.prezzo_varianti_maxi !== "" && !isNaN(parseFloat(attivo.prezzo_varianti_maxi))) {
                                                prezzo_un = attivo.prezzo_varianti_maxi;
                                            } else if (attivo.prezzo_maxi_prima !== undefined && attivo.prezzo_maxi_prima !== null && attivo.prezzo_maxi_prima !== "undefined" && attivo.prezzo_maxi_prima !== "null" && attivo.prezzo_maxi_prima !== "" && !isNaN(parseFloat(attivo.prezzo_maxi_prima))) {
                                                prezzo_un = attivo.prezzo_maxi_prima;
                                            }
                                        } else {
                                            if (numero_nodo_variante_piu > 1 && attivo.prezzo_varianti_aggiuntive !== undefined && attivo.prezzo_varianti_aggiuntive !== null && attivo.prezzo_varianti_aggiuntive !== "undefined" && attivo.prezzo_varianti_aggiuntive !== "null" && attivo.prezzo_varianti_aggiuntive !== "" && !isNaN(parseFloat(attivo.prezzo_varianti_aggiuntive))) {
                                                prezzo_un = attivo.prezzo_varianti_aggiuntive;
                                            }
                                        }
                                    }

                                    if (ultima_due_gusti === true && attivo.desc_art[0] === "=") {
                                        prezzo_un = "0.00";
                                    }
                                    /*if (ultima_schiacciata === true && attivo.desc_art[0] === "=") {
                                        prezzo_un = "0.00";
                                    }*/


                                    conto += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                    ultime_battiture += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                    tab_conto_grande += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                    tab_conto_separato_2_grande += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                    tab_conto_separato_2_grande += '<tr categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td style="color:transparent;" ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td  ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="color:red;cursor:pointer;">X</td></tr>';
                                    console.log("CA ARTICOLO VARIANTE", attivo.desc_art[0]);
                                    conto += '<tr portata="' + attivo.portata + '" categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  style="color:transparent;" ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td style="max-width:50vw;overflow:hidden;" ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="color:red;cursor:pointer;">X</td></tr>';
                                    ultime_battiture += '<tr categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  style="color:transparent;" ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td style="max-width:50vw;overflow:hidden;" ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="color:red;cursor:pointer;">X</td></tr>';
                                    tab_conto_grande += '<tr categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  style="color:transparent;" ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro; ' + prezzo_un + '</td><td ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro; ' + parseFloat(parseFloat(prezzo_un.replace(",", ".")) * parseInt(attivo.quantita)).toFixed(2) + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="color:red;cursor:pointer;">X</td></tr>';
                                    //MODIFICA 07/06/2018
                                    if (comanda.pizzeria_asporto === true) {
                                        var where = "";
                                        if (attivo.desc_art[0] === "+" || attivo.desc_art[0] === "-" || attivo.desc_art[0] === "*" || attivo.desc_art[0] === "x" || attivo.desc_art[0] === "(" || attivo.desc_art[0] === "=") {
                                            where += " AND prog_inser='" + attivo.prog_inser + "' ";
                                        } else {
                                            where += " AND desc_art='" + attivo.descrizione_vera + "' and prezzo_un='" + attivo.prezzo_un + "' ";
                                        }

                                        where += " AND ntav_comanda = '" + comanda.tavolo + "' ";
                                        //BASSA PRIORITA PER NON INTACCARE PRESTAZIONI
                                        var query_prezzo_vero = "update comanda set prezzo_vero='" + prezzo_un + "' where  desc_art NOT LIKE '= 1/2 %' AND stato_record='ATTIVO' " + where + ";";

                                        comanda.sock.send({ tipo: "set_prezzo_vero", operatore: comanda.operatore, query: query_prezzo_vero, terminale: comanda.terminale, ip: comanda.ip_address });

                                        comanda.sincro.query(query_prezzo_vero, function () { });
                                    }

                                }
                                //ALTRIMENTI LI STAMPA IN MODO NORMALE
                                else {

                                    if (attivo.desc_art.indexOf("MAXI") !== -1) {
                                        ultima_maxi = true;
                                    } else {
                                        ultima_maxi = false;
                                    }

                                    /*if (attivo.desc_art.indexOf("SCHIACCIATA") !== -1) {
                                        ultima_schiacciata = true;
                                        var nodo_art_princ = attivo.nodo.substr(0, 3);
                                        var prezzo_schiacciata = "0.00";
                                        var contatore_schiacciata = 0;
                                        for (var articolo in elemento_comanda[tasto_segue][portata]) {
                                            var art = elemento_comanda[tasto_segue][portata][articolo];
                                            var nodo_var = art.nodo.substr(0, 3);
                                            if (nodo_art_princ === nodo_var && art.desc_art[0] === "=") {

                                                contatore_schiacciata++;

                                                var prz = art.prezzo_un;
                                                if (attivo.desc_art.indexOf("MAXI") !== -1) {
                                                    prz = art.prezzo_maxi;
                                                }

                                                if (parseFloat(prz) > parseFloat(prezzo_schiacciata)) {
                                                    prezzo_schiacciata = prz;
                                                }
                                            }
                                        }
                                        prezzo_un = prezzo_schiacciata;
                                        
                                        //MODIFICA 07/06/2018
                                        //IN QUESTO CASO SUDDIVIDE L'INCASSO PER OGNI GUSTO IN BASE AL NODO
                                        //SOLO NEL NODO PIU LUNGO DI TRE RISPETTO A NODO_ART_PRINC
                                        //CIOE' AI GUSTI
                                        //IL CONTATORE DICE QUANTI GUSTI SONO
                                        if (comanda.pizzeria_asporto === true) {
                                            var where = "";
                                            if (attivo.desc_art[0] === "+" || attivo.desc_art[0] === "-" || attivo.desc_art[0] === "*" || attivo.desc_art[0] === "x" || attivo.desc_art[0] === "(" || attivo.desc_art[0] === "=") {
                                                where += " AND prog_inser='" + attivo.prog_inser + "' ";
                                            } else if (attivo.desc_art.indexOf("SCHIACCIATA") !== -1) {
                                                where += " AND prog_inser='" + attivo.prog_inser + "' ";
                                            } else
                                            {
                                                where += " AND desc_art='" + attivo.descrizione_vera + "' AND prezzo_un='" + attivo.prezzo_un + "' ";
                                            }
                                          where += " AND ntav_comanda = '" + comanda.tavolo + "' ";
                                            //BASSA PRIORITA PER NON INTACCARE PRESTAZIONI

                                            if (attivo.prezzo_un === "") {
                                                attivo.prezzo_un = "0";
                                            }

                                            prezzo_un = parseFloat(prezzo_un).toFixed(2);

                                            var query_prezzo_vero = "update comanda set prezzo_vero='" + prezzo_un + "' where  desc_art NOT LIKE '= 1/2 %' AND stato_record='ATTIVO' " + where + ";";


                                            comanda.sock.send({tipo: "set_prezzo_vero", operatore: comanda.operatore, query: query_prezzo_vero, terminale: comanda.terminale, ip: comanda.ip_address});

                                            comanda.sincro.query(query_prezzo_vero, function () {});

                                        }

                                    } else */if (attivo.desc_art.indexOf("DUE GUSTI") !== -1) {
                                        ultima_due_gusti = true;
                                        var nodo_art_princ = attivo.nodo.substr(0, 3);
                                        var prezzo_due_gusti = "0.00";
                                        var contatore_duegusti = 0;
                                        for (var articolo in elemento_comanda[tasto_segue][portata]) {
                                            var art = elemento_comanda[tasto_segue][portata][articolo];
                                            var nodo_var = art.nodo.substr(0, 3);
                                            if (nodo_art_princ === nodo_var && art.desc_art[0] === "=") {

                                                contatore_duegusti++;

                                                var prz = art.prezzo_un;
                                                if (attivo.desc_art.indexOf("MAXI") !== -1) {
                                                    prz = art.prezzo_maxi;
                                                }

                                                if (parseFloat(prz) > parseFloat(prezzo_due_gusti)) {
                                                    prezzo_due_gusti = prz;
                                                }
                                            }
                                        }
                                        prezzo_un = prezzo_due_gusti;
                                        /*var prezzo_diviso = arrotonda_prezzo(parseFloat(prezzo_due_gusti) / parseInt(contatore_duegusti));*/
                                        //MODIFICA 07/06/2018
                                        //IN QUESTO CASO SUDDIVIDE L'INCASSO PER OGNI GUSTO IN BASE AL NODO
                                        //SOLO NEL NODO PIU LUNGO DI TRE RISPETTO A NODO_ART_PRINC
                                        //CIOE' AI GUSTI
                                        //IL CONTATORE DICE QUANTI GUSTI SONO
                                        if (comanda.pizzeria_asporto === true) {
                                            var where = "";
                                            if (attivo.desc_art[0] === "+" || attivo.desc_art[0] === "-" || attivo.desc_art[0] === "*" || attivo.desc_art[0] === "x" || attivo.desc_art[0] === "(" || attivo.desc_art[0] === "=") {
                                                where += " AND prog_inser='" + attivo.prog_inser + "' ";
                                            } /*else if (attivo.desc_art.indexOf("SCHIACCIATA") !== -1) {
                                                where += " AND prog_inser='" + attivo.prog_inser + "' ";
                                            }*/ else if (attivo.desc_art.indexOf("DUE GUSTI") !== -1) {
                                                where += " AND prog_inser='" + attivo.prog_inser + "' ";
                                            } else {
                                                where += " AND desc_art='" + attivo.descrizione_vera + "' AND prezzo_un='" + attivo.prezzo_un + "' ";
                                            }

                                            where += " AND ntav_comanda = '" + comanda.tavolo + "' ";
                                            //BASSA PRIORITA PER NON INTACCARE PRESTAZIONI

                                            if (attivo.prezzo_un === "") {
                                                attivo.prezzo_un = "0";
                                            }
                                            prezzo_un = parseFloat(prezzo_un).toFixed(2);

                                            var query_prezzo_vero = "update comanda set prezzo_vero='" + prezzo_un + "' where  desc_art NOT LIKE '= 1/2 %' AND stato_record='ATTIVO' " + where + ";";


                                            comanda.sock.send({ tipo: "set_prezzo_vero", operatore: comanda.operatore, query: query_prezzo_vero, terminale: comanda.terminale, ip: comanda.ip_address });

                                            comanda.sincro.query(query_prezzo_vero, function () { });

                                        }

                                    } else {
                                        ultima_due_gusti = false;
                                        //MODIFICA 07/06/2018
                                        if (comanda.pizzeria_asporto === true) {
                                            var where = "";
                                            if (attivo.desc_art[0] === "+" || attivo.desc_art[0] === "-" || attivo.desc_art[0] === "*" || attivo.desc_art[0] === "x" || attivo.desc_art[0] === "(" || attivo.desc_art[0] === "=") {
                                                where += " AND prog_inser='" + attivo.prog_inser + "' ";
                                            } else {
                                                where += " AND desc_art='" + attivo.descrizione_vera + "' and prezzo_un='" + attivo.prezzo_un + "' ";
                                            }

                                            where += " AND ntav_comanda = '" + comanda.tavolo + "' ";

                                            //BASSA PRIORITA PER NON INTACCARE PRESTAZIONI
                                            var query_prezzo_vero = "update comanda set prezzo_vero='" + prezzo_un + "' where  desc_art NOT LIKE '= 1/2 %' AND stato_record='ATTIVO' " + where + ";";

                                            comanda.sock.send({ tipo: "set_prezzo_vero", operatore: comanda.operatore, query: query_prezzo_vero, terminale: comanda.terminale, ip: comanda.ip_address });

                                            comanda.sincro.query(query_prezzo_vero, function () { });
                                        }

                                    }

                                    numero_nodo_variante_piu = 0;
                                    console.log("CA ARTICOLO NON VARIANTE", attivo.desc_art);
                                    conto += '<tr portata="' + attivo.portata + '" categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td style="max-width:50vw;overflow:hidden;" ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="color:red;cursor:pointer;">X</td></tr>';
                                    ultime_battiture += '<tr categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td style="max-width:50vw;overflow:hidden;" ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="color:red;cursor:pointer;">X</td></tr>';
                                    tab_conto_grande += '<tr categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro; ' + prezzo_un + '</td><td ' + comanda.evento + '="popup_mod_art(event,\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro; ' + parseFloat(parseFloat(prezzo_un.replace(",", ".")) * parseInt(attivo.quantita)).toFixed(2) + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="color:red;cursor:pointer;">X</td></tr>';
                                    tab_conto_separato_2_grande += '<tr categoria="' + attivo.categoria + '" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="color:red;cursor:pointer;">X</td></tr>';
                                }



                            }
                            //FINE CONDIZIONI
                        }
                        //FINE CICLO FOR IN 1 --ARTICOLI
                    }
                }
            }

            console.log("conto_attivo", d1);
            if (d1 !== undefined && d1[0] !== undefined && d1[0].rag_soc_cliente !== undefined && d1[0].rag_soc_cliente !== null && d1[0].rag_soc_cliente.length > 0) {
                comanda.ultimo_id_cliente = d1[0].rag_soc_cliente;
                comanda.blocca_progressivo = true;
                /* 14-10-2019 */
                /*var testo_query = "select ora_consegna,tipo_consegna,jolly from comanda where desc_art NOT LIKE 'RECORD TESTA%' and  BIS='" + lettera_conto_split() + "' and  id='" + comanda.ultimo_id_asporto + "' order by ora_consegna desc, tipo_consegna desc, jolly desc limit 1;";
                 comanda.sincro.query(testo_query, function () {
                 resolve(true);
                 });*/
                resolve(true);
            } else {
                resolve(true);
            }

        });

        var p3 = new Promise(function (resolve, reject) {
            var esclusione_fiscale_sospeso = ' and fiscale_sospeso!="STAMPAFISCALESOSPESO" ';

            var rd2 = alasql("SELECT perc_iva,reparto,numero_conto,nodo,prog_inser,quantita,desc_art,prezzo_un FROM comanda WHERE desc_art NOT LIKE 'RECORD TESTA%' and  BIS='" + lettera_conto_split() + "'   AND stato_record='ATTIVO' AND posizione='SCONTO' AND ntav_comanda='" + comanda.tavolo + "' " + esclusione_fiscale_sospeso + " ORDER BY nodo ASC;");
            //GROUP BY numero_conto,prezzo_un ;

            if (rd2.length > 0) {

                var raggruppamento = new Object();

                rd2.forEach(function (e) {
                    if (raggruppamento[e.numero_conto] === undefined) {
                        raggruppamento[e.numero_conto] = new Object();
                    }
                    if (raggruppamento[e.numero_conto][e.prezzo_un] === undefined) {
                        e.quantita = parseInt(e.quantita);
                        raggruppamento[e.numero_conto][e.prezzo_un] = e;
                    } else {
                        raggruppamento[e.numero_conto][e.prezzo_un].quantita += parseInt(e.quantita);
                    }
                });

                var sconto_conto_uno = new Array();
                var sconto_conto_due = new Array();

                for (var numero_conto in raggruppamento) {
                    for (var prezzo_un in raggruppamento[numero_conto]) {
                        if (numero_conti_attivi === 2 && raggruppamento[numero_conto][prezzo_un].prezzo_un.indexOf("%") !== -1) {
                            sconto_conto_uno.push(raggruppamento[numero_conto][prezzo_un]);
                            sconto_conto_due.push(raggruppamento[numero_conto][prezzo_un]);
                        } else {
                            if (numero_conto === '2') {
                                sconto_conto_due.push(raggruppamento[numero_conto][prezzo_un]);
                            } else {
                                sconto_conto_uno.push(raggruppamento[numero_conto][prezzo_un]);
                            }
                        }
                    }
                }

                raggruppamento = {};
                raggruppamento = null;

                sconto_conto_uno.forEach(function (attivo) {




                    if (comanda.compatibile_xml === true) {
                        iva_esposta = attivo.perc_iva;
                    } else {
                        iva_esposta = ive_misuratore_controller.iva_reparto(parseInt(REPARTO_controller.indice(attivo.reparto.trim())));

                        reparto = "rep_" + parseInt(attivo.reparto.trim());
                    }
                    if (comanda.iva_esposta !== 'true') {
                        iva_esposta = "";
                    }

                    if (isNaN(parseInt(attivo.reparto.trim()))) {
                        reparto = "rep_11";
                        iva_esposta = "";
                    }

                    var settaggio_sconto_non_eliminabile = '<td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="width:10%;color:red;cursor:pointer;">X</td>';
                    if (comanda.compatibile_xml === true && comanda.sconto_non_eliminabile === "S") {
                        settaggio_sconto_non_eliminabile = '<td style="width:10%;color:red;cursor:pointer;"></td>';
                    }

                    intestazioni_conto = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td>' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td>' + attivo.prezzo_un + '</td>' + settaggio_sconto_non_eliminabile + '</tr>' + intestazioni_conto;
                    let prezzo = '';
                    if (attivo.prezzo_un.indexOf("%") !== -1) {
                        intestazioni_conto_grande = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td style="width:10%;">' + attivo.quantita + '</td><td style="width:55%;">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td  style="width:25%;">' + attivo.prezzo_un + '</td><td  style="width:25%;">' + '</td>' + settaggio_sconto_non_eliminabile + '</tr>' + intestazioni_conto_grande;
                    } else {
                        intestazioni_conto_grande = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td style="width:10%;">' + attivo.quantita + '</td><td style="width:55%;">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td  style="width:25%;">' + '€ ' + attivo.prezzo_un + '</td><td  style="width:25%;">' + '</td>' + settaggio_sconto_non_eliminabile + '</tr>' + intestazioni_conto_grande;
                    }



                    intestazioni_conto_split = '<tr  style="font-weight:bold;"   class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td>' + attivo.quantita + '</td><td>SCONTO</td><td>' + iva_esposta + '</td><td>' + attivo.prezzo_un + '</td><td></td><td></td></tr>' + intestazioni_conto_split;


                    intestazioni_conto_separato_grande = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="width:10%;"  >' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="width:55%;" >' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="width:25%;" >' + attivo.prezzo_un + '</td>' + settaggio_sconto_non_eliminabile + '</tr>' + intestazioni_conto_separato_grande;

                });


                sconto_conto_due.forEach(function (attivo) {


                    if (comanda.compatibile_xml === true) {
                        iva_esposta = attivo.perc_iva;
                    } else {
                        iva_esposta = ive_misuratore_controller.iva_reparto(parseInt(REPARTO_controller.indice(attivo.reparto.trim())));

                        reparto = "rep_" + parseInt(attivo.reparto.trim());
                    }

                    if (comanda.iva_esposta !== 'true') {
                        iva_esposta = "";
                    }

                    var settaggio_sconto_non_eliminabile = '<td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="width:10%;color:red;cursor:pointer;">X</td>';
                    if (comanda.compatibile_xml === true && comanda.sconto_non_eliminabile === "S") {
                        settaggio_sconto_non_eliminabile = '<td style="width:10%;color:red;cursor:pointer;"></td>';
                    }

                    if (isNaN(parseInt(attivo.reparto.trim()))) {
                        reparto = "rep_11";
                        iva_esposta = "";
                    }


                    intestazioni_conto = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td>' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td>' + attivo.prezzo_un + '</td>' + settaggio_sconto_non_eliminabile + '</tr>' + intestazioni_conto;
                    intestazioni_conto_grande = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td style="width:10%;">' + attivo.quantita + '</td><td style="width:55%;">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td  style="width:25%;">' + attivo.prezzo_un + '</td><td  style="width:25%;">' + parseFloat(parseFloat(attivo.prezzo_un) * parseInt(attivo.quantita)).toFixed(2) + '</td>' + settaggio_sconto_non_eliminabile + '</tr>' + intestazioni_conto_grande;
                    intestazioni_conto_split = '<tr  style="font-weight:bold;"   class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td>' + attivo.quantita + '</td><td>SCONTO</td><td>' + iva_esposta + '</td><td>' + attivo.prezzo_un + '</td><td></td><td></td></tr>' + intestazioni_conto_split;


                    intestazioni_conto_separato_2_grande = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="width:10%;"  >' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="width:55%;" >' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="width:25%;" >' + attivo.prezzo_un + '</td>' + settaggio_sconto_non_eliminabile + '</tr>' + intestazioni_conto_separato_2_grande;

                });

                resolve(true);

            } else {

                resolve(true);
            }
        });
        var p4 = new Promise(function (resolve, reject) {
            var dr3 = alasql("SELECT perc_iva,reparto,numero_conto,nodo,prog_inser,quantita,desc_art,prezzo_un FROM comanda WHERE desc_art NOT LIKE 'RECORD TESTA%' and  BIS='" + lettera_conto_split() + "' and   stato_record='ATTIVO' AND posizione='COPERTO' AND ntav_comanda='" + comanda.tavolo + "' ORDER BY nodo ASC;");
            //GROUP BY numero_conto ;

            if (dr3.length > 0) {

                var raggruppamento = new Object();

                dr3.forEach(function (e) {
                    if (raggruppamento[e.numero_conto] === undefined) {
                        //raggruppamento[e.numero_conto] = new Object();
                        e.quantita = parseInt(e.quantita);
                        raggruppamento[e.numero_conto] = e;
                    } else {
                        raggruppamento[e.numero_conto].quantita += parseInt(e.quantita);
                    }
                });

                var coperti_conto_uno = new Array();
                var coperti_conto_due = new Array();

                for (var numero_conto in raggruppamento) {
                    if (numero_conto === '2') {
                        coperti_conto_due.push(raggruppamento[numero_conto]);
                    } else {
                        coperti_conto_uno.push(raggruppamento[numero_conto]);
                    }
                }

                raggruppamento = {};
                raggruppamento = null;

                coperti_conto_uno.forEach(function (attivo) {



                    if (comanda.compatibile_xml === true) {
                        iva_esposta = attivo.perc_iva;
                    } else {
                        iva_esposta = ive_misuratore_controller.iva_reparto(parseInt(REPARTO_controller.indice(attivo.reparto.trim())));

                        reparto = "rep_" + parseInt(attivo.reparto.trim());
                    }

                    if (comanda.iva_esposta !== 'true') {
                        iva_esposta = "";
                    }

                    if (isNaN(parseInt(attivo.reparto.trim()))) {
                        reparto = "rep_11";
                        iva_esposta = "";
                    }

                    intestazioni_conto = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td id="numero_coperti_effettivi">' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td>' + attivo.prezzo_un + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="color:red;cursor:pointer;">X</td></tr>' + intestazioni_conto;
                    intestazioni_conto_grande = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td  style="width:10%;" id="numero_coperti_effettivi">' + attivo.quantita + '</td><td  style="width:55%;">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td  style="width:25%;">' + attivo.prezzo_un + '</td><td  style="width:25%;">' + parseFloat(parseFloat(attivo.prezzo_un) * parseInt(attivo.quantita)).toFixed(2) + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="width:10%;color:red;cursor:pointer;">X</td></tr>' + intestazioni_conto_grande;
                    intestazioni_conto_split = '<tr  style="font-weight:bold;"  class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td id="numero_coperti_effettivi">' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td>' + attivo.prezzo_un + '</td><td></td><td></td></tr>' + intestazioni_conto_split;


                    //CONTO SEPARATO
                    intestazioni_conto_separato_grande = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="width:10%;" id="numero_coperti_effettivi" >' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="width:55%;" >' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="width:25%;" >' + attivo.prezzo_un + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="width:10%;color:red;cursor:pointer;">X</td></tr>' + intestazioni_conto_separato_grande;

                });


                coperti_conto_due.forEach(function (attivo) {


                    if (comanda.compatibile_xml === true) {
                        iva_esposta = attivo.perc_iva;
                    } else {
                        iva_esposta = ive_misuratore_controller.iva_reparto(parseInt(REPARTO_controller.indice(attivo.reparto.trim())));

                        reparto = "rep_" + parseInt(attivo.reparto.trim());
                    }

                    if (comanda.iva_esposta !== 'true') {
                        iva_esposta = "";
                    }

                    if (isNaN(parseInt(attivo.reparto.trim()))) {
                        reparto = "rep_11";
                        iva_esposta = "";
                    }

                    intestazioni_conto = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td id="numero_coperti_effettivi">' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td>' + attivo.prezzo_un + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="color:red;cursor:pointer;">X</td></tr>' + intestazioni_conto;
                    intestazioni_conto_grande = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td  style="width:10%;" id="numero_coperti_effettivi">' + attivo.quantita + '</td><td  style="width:55%;">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td  style="width:25%;">' + attivo.prezzo_un + '</td><td  style="width:25%;">' + parseFloat(parseFloat(attivo.prezzo_un) * parseInt(attivo.quantita)).toFixed(2) + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="width:10%;color:red;cursor:pointer;">X</td></tr>' + intestazioni_conto_grande;
                    intestazioni_conto_split = '<tr  style="font-weight:bold;"  class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td id="numero_coperti_effettivi">' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td>' + attivo.prezzo_un + '</td><td></td><td></td></tr>' + intestazioni_conto_split;


                    //CONTO SEPARATO
                    intestazioni_conto_separato_2_grande = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="width:10%;" id="numero_coperti_effettivi" >' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="width:55%;" >' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(event,\'' + attivo.nodo.substr(0, 3) + '\')" style="width:25%;" >' + attivo.prezzo_un + '</td><td ' + comanda.evento + '="elimina_articolo(\'' + attivo.prog_inser + '\');" style="width:10%;color:red;cursor:pointer;">X</td></tr>' + intestazioni_conto_separato_2_grande;

                });

                resolve(true);
            } else {
                resolve(true);
            }


        });
        Promise.all([p2, p3, p4]).then(function () {


            //log_velocita(false, "conto attivo promise ultima");


            if (progressivo_ultimo_conto_interno === progressivo_ultimo_conto) {

                if (comanda.split_abilitato === true) {
                    if (comanda.disabilita_tasto_bis === true) {
                        $('.split_successivo').css('pointer-events', 'none');
                        if (comanda.disabilita_tasto_tavolo === true) {
                            $('.tasto_numero_tavolo').css('pointer-events', 'none');
                        }
                    }
                }



                var totale_calcolo_consegna = 0;
                var fissa_totale_calcolo_consegna = 0;
                comanda.consegna_totale = 0;
                comanda.consegna_unitario_mezzo_metro = 0;
                comanda.consegna_unitario_metro = 0;
                comanda.consegna_unitario_a_pizza = 0;
                comanda.consegna_una_pizza = 0;
                comanda.consegna_piu_di_una_pizza = 0;
                comanda.fissa_consegna_una_pizza = 0;
                comanda.fissa_consegna_piu_di_una_pizza = 0;
                comanda.consegna_unitario_maxi = 0;

                if (comanda.consegna_presente === true) {

                    if (comanda.compatibile_xml !== true) {
                        if (comanda.pizzeria_asporto === true || (comanda.tavolo === 'TAKE AWAY' || comanda.tavolo.left(7) === "ASPORTO" || comanda.tavolo === '*TAKEAWAY*' || comanda.tavolotxt === '*TAKEAWAY*')) {
                            iva_esposta = ive_misuratore_controller.iva_reparto(parseInt(REPARTO_controller.indice(parseInt(comanda.reparto_consegna))));
                            reparto = "rep_" + comanda.reparto_consegna;
                        } else {
                            iva_esposta = ive_misuratore_controller.iva_reparto(parseInt(REPARTO_controller.indice(parseInt(comanda.reparto_consegna))));
                            reparto = "rep_" + comanda.reparto_consegna;
                        }
                    }

                    if (comanda.iva_esposta !== 'true') {
                        iva_esposta = "";
                    }




                    /* Aggiunto controllo domicilio l'11 dicembre 2020 per problema prezzo consegne sbagliato sui ritiri */


                    let differenza_consegna = 0;
                    if (comanda.tipo_consegna === "DOMICILIO") {

                        if (parseInt(comanda.consegna_fattorino_extra) > 0) {

                            var add = parseFloat(comanda.consegna_fattorino_extra).toFixed(2);
                            totale_calcolo_consegna += add;
                            comanda.consegna_totale += add;


                            totale_calcolo_consegna = parseFloat(totale_calcolo_consegna).toFixed(2);
                            fissa_totale_calcolo_consegna = parseFloat(fissa_totale_calcolo_consegna).toFixed(2);

                            riga_calcolo_consegna = "";
                            if (/*comanda.tipo_consegna === "DOMICILIO" && */parseFloat(totale_calcolo_consegna) > 0) {
                                comanda.totale_consegna = totale_calcolo_consegna;

                                let consegna_video = calcolo_differenza_consegna(comanda.totale_consegna);

                                if (consegna_video[0].consegna_gia_pagata !== "0.00") {



                                }

                                if (consegna_video[0].consegna_da_pagare !== "0.00") {
                                    riga_calcolo_consegna += "<tr  class='" + reparto + "' id='art_999'><td style='color:transparent;'>1</td><td>CONSEGNA</td><td>" + iva_esposta + "</td><td>&euro;" + consegna_video[0].consegna_da_pagare + "</td><td>&euro; " + consegna_video[0].consegna_da_pagare + "</td><td></td></tr>";
                                    if (consegna_video[0].consegna_gia_pagata !== "0.00") {


                                        riga_calcolo_consegna += "<tr  class='fiscalizzata " + reparto + "' id='art_998'><td style='color:transparent;'>1</td><td>CONSEGNA PRECEDENTE</td><td>" + iva_esposta + "</td><td>&euro;" + consegna_video[0].consegna_gia_pagata + "</td><td>&euro; " + consegna_video[0].consegna_gia_pagata + "</td><td></td></tr>";
                                    }

                                }
                                else {
                                    riga_calcolo_consegna += "<tr  class='fiscalizzata " + reparto + "' id='art_998'><td style='color:transparent;'>1</td><td>CONSEGNA</td><td>" + iva_esposta + "</td><td>&euro;" + consegna_video[0].consegna_gia_pagata + "</td><td>&euro; " + consegna_video[0].consegna_gia_pagata + "</td><td></td></tr>";
                                }

                            } else if (/*comanda.tipo_consegna === "DOMICILIO" && */parseFloat(fissa_totale_calcolo_consegna) > 0) {
                                comanda.fissa_totale_consegna = fissa_totale_calcolo_consegna;
                                comanda.consegna_totale = fissa_totale_calcolo_consegna;

                                let consegna_video = calcolo_differenza_consegna( comanda.consegna_totale);

                                if (consegna_video[0].consegna_gia_pagata !== "0.00") {



                                }


                                if (consegna_video[0].consegna_da_pagare !== "0.00") {
                                    riga_calcolo_consegna += "<tr  class='" + reparto + "' id='art_999'><td style='color:transparent;'>1</td><td>CONSEGNA</td><td>" + iva_esposta + "</td><td>&euro;" + consegna_video[0].consegna_da_pagare + "</td><td>&euro; " + consegna_video[0].consegna_da_pagare + "</td><td></td></tr>";
                                    if (consegna_video[0].consegna_gia_pagata !== "0.00") {


                                        riga_calcolo_consegna += "<tr  class='fiscalizzata " + reparto + "' id='art_998'><td style='color:transparent;'>1</td><td>CONSEGNA PRECEDENTE</td><td>" + iva_esposta + "</td><td>&euro;" + consegna_video[0].consegna_gia_pagata + "</td><td>&euro; " + consegna_video[0].consegna_gia_pagata + "</td><td></td></tr>";
                                    }

                                }
                                else {
                                    riga_calcolo_consegna += "<tr  class='fiscalizzata " + reparto + "' id='art_998'><td style='color:transparent;'>1</td><td>CONSEGNA</td><td>" + iva_esposta + "</td><td>&euro;" + consegna_video[0].consegna_gia_pagata + "</td><td>&euro; " + consegna_video[0].consegna_gia_pagata + "</td><td></td></tr>";
                                }
                            }

                        } else {
                            if (calcolo_consegna_su_totale > 0 && comanda.consegna_su_totale.length > 0) {
                                if (comanda.consegna_su_totale.indexOf("%") !== -1) {
                                    var add = parseFloat(calcolo_consegna_su_totale) / 100 * parseFloat(comanda.consegna_su_totale.replace("%", ""))
                                    totale_calcolo_consegna += add;
                                    comanda.consegna_totale += add;
                                } else {
                                    var add = parseFloat(comanda.consegna_su_totale);
                                    totale_calcolo_consegna += add;
                                    comanda.consegna_totale += add;
                                }
                            }
                            if (calcolo_consegna_maxi > 0 && comanda.consegna_maxi.length > 0) {
                                var add = parseInt(calcolo_consegna_maxi) * parseFloat(comanda.consegna_maxi);
                                totale_calcolo_consegna += add;
                                comanda.consegna_unitario_maxi = comanda.consegna_maxi;
                            }
                            if (calcolo_consegna_mezzo_metro > 0 && comanda.consegna_mezzo_metro.length > 0) {
                                var add = parseInt(calcolo_consegna_mezzo_metro) * parseFloat(comanda.consegna_mezzo_metro);
                                totale_calcolo_consegna += add;
                                comanda.consegna_unitario_mezzo_metro = comanda.consegna_mezzo_metro;
                            }
                            if (calcolo_consegna_un_metro > 0 && comanda.consegna_metro.length > 0) {
                                var add = parseInt(calcolo_consegna_un_metro) * parseFloat(comanda.consegna_metro);
                                totale_calcolo_consegna += add;
                                comanda.consegna_unitario_metro = comanda.consegna_metro;
                            }
                            if (calcolo_consegna_a_pizza > 0 && comanda.consegna_a_pizza.length > 0) {
                                var add = parseInt(calcolo_consegna_a_pizza) * parseFloat(comanda.consegna_a_pizza);
                                totale_calcolo_consegna += add;
                                comanda.consegna_unitario_a_pizza = comanda.consegna_a_pizza;
                            }

                            /*MODIFICA CONSEGNE MISTE*/


                            if (calcolo_consegna_una_pizza >= parseInt(comanda.da_fascia_1) && calcolo_consegna_una_pizza <= parseInt(comanda.a_fascia_1) && comanda.una_pizza.length > 0) {
                                 var add = parseInt(calcolo_consegna_una_pizza) * parseFloat(comanda.una_pizza);
                                 totale_calcolo_consegna += add;
                                 comanda.consegna_una_pizza = comanda.una_pizza;
                             }
                             if (calcolo_consegna_piu_di_una_pizza >= parseInt(comanda.da_fascia_2) && calcolo_consegna_piu_di_una_pizza <= parseInt(comanda.a_fascia_2) && comanda.piu_di_una_pizza.length > 0) {
                                 var add = parseInt(calcolo_consegna_piu_di_una_pizza) * parseFloat(comanda.piu_di_una_pizza);
                                 totale_calcolo_consegna += add;
                                 comanda.consegna_piu_di_una_pizza = comanda.piu_di_una_pizza;
                             }

                            /*var add = 0;

                            for (let i = 1; i <= calcolo_consegna_piu_di_una_pizza; i++) {

                                if (i >= parseInt(comanda.da_fascia_1) && i <= parseInt(comanda.a_fascia_1) && comanda.una_pizza.length > 0) {

                                    add += parseFloat(comanda.una_pizza);
                                    totale_calcolo_consegna = add;
                                    comanda.consegna_piu_di_una_pizza = comanda.una_pizza;

                                } else if (i >= parseInt(comanda.da_fascia_2) && i <= parseInt(comanda.a_fascia_2) && comanda.piu_di_una_pizza.length > 0) {

                                    add += parseFloat(comanda.piu_di_una_pizza);
                                    totale_calcolo_consegna = add;
                                    comanda.consegna_piu_di_una_pizza = comanda.piu_di_una_pizza;
                                }

                            }*/













                            if (fissa_calcolo_consegna_una_pizza >= parseInt(comanda.fissa_da_fascia_1) && fissa_calcolo_consegna_una_pizza <= parseInt(comanda.fissa_a_fascia_1) && comanda.fissa_una_pizza.length > 0) {
                                var add = parseFloat(comanda.fissa_una_pizza);
                                fissa_totale_calcolo_consegna = add;
                                comanda.fissa_consegna_una_pizza = comanda.una_pizza;
                            }
                            if (fissa_calcolo_consegna_piu_di_una_pizza >= parseInt(comanda.fissa_da_fascia_2) && fissa_calcolo_consegna_piu_di_una_pizza <= parseInt(comanda.fissa_a_fascia_2) && comanda.fissa_piu_di_una_pizza.length > 0) {
                                var add = parseFloat(comanda.fissa_piu_di_una_pizza);
                                fissa_totale_calcolo_consegna = add;
                                comanda.fissa_consegna_piu_di_una_pizza = comanda.fissa_piu_di_una_pizza;
                            }

                            totale_calcolo_consegna = parseFloat(totale_calcolo_consegna);
                            fissa_totale_calcolo_consegna = parseFloat(fissa_totale_calcolo_consegna);

                            riga_calcolo_consegna = "";
                            if (/*comanda.tipo_consegna === "DOMICILIO" &&*/ parseFloat(totale_calcolo_consegna) > 0) {
                                comanda.totale_consegna = (parseFloat(totale_calcolo_consegna)+ parseFloat(fissa_totale_calcolo_consegna)).toFixed(2);

                                let consegna_video = calcolo_differenza_consegna(comanda.totale_consegna);


                                if (consegna_video[0].consegna_gia_pagata !== "0.00") {



                                }


                                if (consegna_video[0].consegna_da_pagare !== "0.00") {
                                    riga_calcolo_consegna += "<tr  class='" + reparto + "' id='art_999'><td style='color:transparent;'>1</td><td>CONSEGNA</td><td>" + iva_esposta + "</td><td>&euro;" + consegna_video[0].consegna_da_pagare + "</td><td>&euro; " + consegna_video[0].consegna_da_pagare + "</td><td></td></tr>";
                                    if (consegna_video[0].consegna_gia_pagata !== "0.00") {


                                        riga_calcolo_consegna += "<tr  class='fiscalizzata " + reparto + "' id='art_998'><td style='color:transparent;'>1</td><td>CONSEGNA PRECEDENTE</td><td>" + iva_esposta + "</td><td>&euro;" + consegna_video[0].consegna_gia_pagata + "</td><td>&euro; " + consegna_video[0].consegna_gia_pagata + "</td><td></td></tr>";
                                    }

                                }
                                else {
                                    riga_calcolo_consegna += "<tr  class='fiscalizzata " + reparto + "' id='art_998'><td style='color:transparent;'>1</td><td>CONSEGNA</td><td>" + iva_esposta + "</td><td>&euro;" + consegna_video[0].consegna_gia_pagata + "</td><td>&euro; " + consegna_video[0].consegna_gia_pagata + "</td><td></td></tr>";
                                }

                            } else if (/*comanda.tipo_consegna === "DOMICILIO" &&*/ parseFloat(fissa_totale_calcolo_consegna) > 0) {



                                comanda.fissa_totale_consegna = fissa_totale_calcolo_consegna;
                                comanda.consegna_totale = fissa_totale_calcolo_consegna;

                                let consegna_video = calcolo_differenza_consegna(comanda.consegna_totale);




                                if (consegna_video[0].consegna_da_pagare !== "0.00") {
                                    riga_calcolo_consegna += "<tr  class='" + reparto + "' id='art_999'><td style='color:transparent;'>1</td><td>CONSEGNA</td><td>" + iva_esposta + "</td><td>&euro;" + consegna_video[0].consegna_da_pagare + "</td><td>&euro; " + consegna_video[0].consegna_da_pagare + "</td><td></td></tr>";
                                    if (consegna_video[0].consegna_gia_pagata !== "0.00") {


                                        riga_calcolo_consegna += "<tr  class='fiscalizzata " + reparto + "' id='art_998'><td style='color:transparent;'>1</td><td>CONSEGNA PRECEDENTE</td><td>" + iva_esposta + "</td><td>&euro;" + consegna_video[0].consegna_gia_pagata + "</td><td>&euro; " + consegna_video[0].consegna_gia_pagata + "</td><td></td></tr>";
                                    }

                                }
                                else {
                                    riga_calcolo_consegna += "<tr  class='fiscalizzata " + reparto + "' id='art_998'><td style='color:transparent;'>1</td><td>CONSEGNA</td><td>" + iva_esposta + "</td><td>&euro;" + consegna_video[0].consegna_gia_pagata + "</td><td>&euro; " + consegna_video[0].consegna_gia_pagata + "</td><td></td></tr>";
                                }
                            }
                        }
                    }
                }

                $('.ultime_battiture').html(ultime_battiture);
                $('#conto').html(conto + riga_calcolo_consegna);
                $('#intestazioni_conto').html(intestazioni_conto);
                $('#campo_ordinazione .anteprima').html(conto);


                /*$('.tab_conto_grande').html(tab_conto_grande + riga_calcolo_consegna);
                 $('#intestazioni_conto_grande').html(intestazioni_conto_grande);*/



                visualizza_conto_grande(tab_conto_grande + riga_calcolo_consegna, intestazioni_conto_grande);


                /*$('#intestazioni_conto_separato_grande').html(intestazioni_conto_separato_grande);
                 $('.tab_conto_separato_grande').html(tab_conto_separato_grande);*/

                visualizza_conto_separato(intestazioni_conto_separato_grande, tab_conto_separato_grande);


                /*$('#intestazioni_conto_separato_2_grande').html(intestazioni_conto_separato_2_grande);
                 $('.tab_conto_separato_2_grande').html(tab_conto_separato_2_grande);*/


                visualizza_conto_separato_2(intestazioni_conto_separato_2_grande, tab_conto_separato_2_grande);


                socket_fast_split.send(JSON.stringify({ intestazione: intestazioni_conto_split, conto: tab_conto_grande }));

                //$(popup_fast_split.document.getElementById("fS_prodotti_conto_grande")).html(tab_conto_grande);
                //$(popup_fast_split.document.getElementById("fS_intestazioni_conto_grande")).html(intestazioni_conto_grande);



                if (numero_conti_attivi === 2) {
                    socket_fast_split.send(JSON.stringify({ intestazione: intestazioni_conto_separato_2_grande_split, conto: tab_conto_separato_2_grande_split }));
                }

                comanda.sincro.query_cassa();

                //log_velocita(false, "secondo pre-fine conto attivo");
                fine_conto_attivo(callBACCO, salta_colore);
            }

        });
    }
};

function calcolo_differenza_consegna_bak() {
    if (comanda.tipo_consegna === "DOMICILIO") {

        let id_comanda = id_comanda_attuale_alasql();
        let ora_consegna = $('#popup_scelta_cliente input[name="ora_consegna"]').val();

        let differenza = 0.00;


        var query_tipo_consegna_controllo = 'select * from consegna_tempo where id="' + id_comanda + '"AND consegna_ora="' + ora_consegna + '"';
        var testing = alasql(query_tipo_consegna_controllo);
        if (testing.length > 0) {
            var comanda_tipo_ricevuta = 'select * from comanda where id="' + id_comanda + '"';
            var testing2 = alasql(comanda_tipo_ricevuta);
            if (testing2[0].tipo_ricevuta === "SCONTRINO") {

                var query_tipo_consegna_ora_prendlo = 'select * from consegna_tempo where id="' + id_comanda + '"';
                var testing4 = alasql(query_tipo_consegna_ora_prendlo);
                if (parseFloat(comanda.totale_consegna) - parseFloat(testing4[0].consegna_costo) === 0) {
                    differenza = parseFloat(comanda.totale_consegna) - parseFloat(testing4[0].consegna_costo)
                    return [{ "consegna_gia_pagata": parseFloat(testing4[0].consegna_costo).toFixed(2), "consegna_da_pagare": differenza.toFixed(2) }];
                } else {



                    differenza = parseFloat(comanda.totale_consegna) - parseFloat(testing4[0].consegna_costo);
                    var totaleee = differenza + parseFloat(testing4[0].consegna_costo);
                    alert("totale" + totaleee);
                    var update_query_tipo_consegna = 'update consegna_tempo SET consegna_ora="' + ora_consegna + '",consegna_costo_dopo_scontrino= "' + differenza + '"   where id="' + id_comanda + '"';
                    comanda.sock.send({ tipo: "aggiornamento", operatore: comanda.operatore, query: update_query_tipo_consegna, terminale: comanda.terminale, ip: comanda.ip_address });
                    alasql(update_query_tipo_consegna);
                    return [{ "consegna_gia_pagata": parseFloat(testing4[0].consegna_costo).toFixed(2), "consegna_da_pagare": differenza.toFixed(2) }];
                }


            } else {
                differenza = parseFloat(comanda.totale_consegna);
                var update_query_tipo_consegna = 'update consegna_tempo SET consegna_ora="' + ora_consegna + '",consegna_costo= "' + comanda.totale_consegna.toFixed(2) + '"   where id="' + id_comanda + '"';
                comanda.sock.send({ tipo: "aggiornamento", operatore: comanda.operatore, query: update_query_tipo_consegna, terminale: comanda.terminale, ip: comanda.ip_address });
                alasql(update_query_tipo_consegna);


                return [{ "consegna_gia_pagata": "0.00", "consegna_da_pagare": differenza.toFixed(2) }];

            }

        } else if (id_comanda !== false) {
            var query_tipo_consegna_controllo = 'select * from consegna_tempo where id="' + id_comanda + '"AND consegna_ora="' + ora_consegna + '"';
            var testing = alasql(query_tipo_consegna_controllo);

            var query_tipo_consegna = 'insert into consegna_tempo (id,consegna_ora,consegna_costo) VALUES ("' + id_comanda + '","' + ora_consegna + '","' + comanda.totale_consegna.toFixed(2) + '")'
            comanda.sock.send({ tipo: "aggiornamento", operatore: comanda.operatore, query: query_tipo_consegna, terminale: comanda.terminale, ip: comanda.ip_address });
            alasql(query_tipo_consegna);

            return [{ "consegna_gia_pagata": "0.00", "consegna_da_pagare": comanda.totale_consegna }];


        } else {


            return [{ "consegna_gia_pagata": "0.00", "consegna_da_pagare": differenza.toFixed(2) }];
        }

    }
}


function calcolo_differenza_consegna(totale_consegna) {

    let id_comanda = id_comanda_attuale_alasql();

    let queryVerificaCostiConsegnaPrecedenti = "select consegna_costo from consegna_tempo where id='" + id_comanda + "' limit 1;";

    let consegnePrecedenti = alasql(queryVerificaCostiConsegnaPrecedenti);


    let gia_pagato = 0;
    let da_pagare = 0;

    if (consegnePrecedenti.length > 0) {
        da_pagare = (parseFloat(totale_consegna) ) - parseFloat(consegnePrecedenti[0].consegna_costo);
        gia_pagato = parseFloat(consegnePrecedenti[0].consegna_costo);
    } else {
        da_pagare = parseFloat(totale_consegna) ;
        gia_pagato = 0;
    }

    if(da_pagare<0){
        da_pagare=0;
    }

    return [{ "consegna_gia_pagata": arrotondamento_pagamento(gia_pagato.toFixed(2)), "consegna_da_pagare": arrotondamento_pagamento(da_pagare.toFixed(2)) }];

}

function visualizza_conto(a, b) {

}

function visualizza_conto_grande(a, b) {
    /* setTimeout(function () {*/
    $('.tab_conto_grande').html(a);
    $('#intestazioni_conto_grande').html(b);
    /*}, 0);*/
}


function visualizza_conto_separato(a, b) {
    /* setTimeout(function () {*/
    $('#intestazioni_conto_separato_grande').html(a);
    $('.tab_conto_separato_grande').html(b);
    /*}, 0);*/
}

function visualizza_conto_separato_2(a, b) {
    /* setTimeout(function () {*/
    $('#intestazioni_conto_separato_2_grande').html(a);
    $('.tab_conto_separato_2_grande').html(b);
    /*}, 0);*/
}


function visualizza_ultime_battiture() {

}
