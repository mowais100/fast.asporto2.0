/****/

/* global epson, comanda, bootbox, REPARTO_view */

var alert_aperto_misuratore = false;

function IVA_model() {

    let misuratore_presente = false;

    /* Metodo Salva */

    this.salva_dato_singolo = async function (p, p_DECIMALE, i) {

        return new Promise(function (resolve, reject) {

            var xml = '<printerCommand><directIO  command="4005" data="' + aggZero(i, 2) + '' + aggZero(p[i], 2) + '' + aggZero(p_DECIMALE[i], 2) + '" /></printerCommand>';
            var epos = new epson.fiscalPrint();
            epos.send("http://" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=local_printer&timeout=10000", xml, 10000);
            epos.onreceive = function (result, tag_names_array, add_info, res_add) {
                resolve(true);
            };
            epos.onerror = function () {
                if (alert_aperto_misuratore === false) {
                    alert_aperto_misuratore = true;
                    bootbox.alert("Problema nella lettura dei reparti (IVA) dal misuratore RT.<br><br>NON LAVORARE SE DEVI STAMPARE SCONTRINI O FATTURE!<br><br>RIAVVIA IL MISURATORE, ATTENDI UN MINUTO, E POI RIAVVIA IL SOFTWARE.<br><br>Non preoccuparti, sono cose di routine.");
                    resolve(false);
                } else {
                    resolve(false);
                }
            };


        });
    };

    this.salva_in_misuratore = async function (p, p_DECIMALE) {

        if (comanda.lingua_stampa === "italiano") {
            let data_length = p.length;

            for (let i = 1; i < data_length; i++) {
                await this.salva_dato_singolo(p, p_DECIMALE, i);
            }

        }
    };

    /* Costruttore classe Model */

    var self = this;

    this.listaIVA = new listaIVA();

    function listaIVA() {
        /*IVA di default (non toccare mai)*/

        if (comanda.lingua_stampa === "italiano") {
            this.IVA_1 = "10";
            this.IVA_2 = "22";
            this.IVA_3 = "4";
            this.IVA_4 = "5";
            this.IVA_5 = "0";
            this.IVA_6 = "0";
            this.IVA_7 = "0";
            this.IVA_8 = "0";
            this.IVA_9 = "0";

            this.IVA_1_DECIMALE = "";
            this.IVA_2_DECIMALE = "";
            this.IVA_3_DECIMALE = "";
            this.IVA_4_DECIMALE = "";
            this.IVA_5_DECIMALE = "";
            this.IVA_6_DECIMALE = "";
            this.IVA_7_DECIMALE = "";
            this.IVA_8_DECIMALE = "";
            this.IVA_9_DECIMALE = "";
        } else if (comanda.lingua_stampa === "deutsch") {
            this.IVA_1 = "19";
            this.IVA_2 = "7";
            this.IVA_3 = "0";
            this.IVA_4 = "0";
            this.IVA_5 = "0";
            this.IVA_6 = "0";
            this.IVA_7 = "0";
            this.IVA_8 = "0";
            this.IVA_9 = "0";

            this.IVA_1_DECIMALE = "";
            this.IVA_2_DECIMALE = "";
            this.IVA_3_DECIMALE = "";
            this.IVA_4_DECIMALE = "";
            this.IVA_5_DECIMALE = "";
            this.IVA_6_DECIMALE = "";
            this.IVA_7_DECIMALE = "";
            this.IVA_8_DECIMALE = "";
            this.IVA_9_DECIMALE = "";
        }

    }


    function oggettoRispostaMisuratore(datoGrezzo) {

        this.rispostaGrezza = datoGrezzo.responseData;

        if (this.rispostaGrezza === undefined) {
            console.log("UNDEFINED");
        }

        this.numero = this.rispostaGrezza.substr(0, 2);
        this.iva = this.rispostaGrezza.substr(2, 2);
        this.iva_decimale = this.rispostaGrezza.substr(4, 2);
    }


    this.init = async function () {

        if (comanda.lingua_stampa === "italiano") {

            if (comanda.compatibile_xml !== true) {

                let r = alasql("select * from settaggi_profili");

                if (comanda.pizzeria_asporto === true) {

                    if (r[0].Field317 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field318 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field319 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field321 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field421 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field364 === "true") {
                        misuratore_presente = true;
                    }

                    if (r[0].Field503 === "true") {
                        misuratore_presente = true;
                    }

                    if (r[0].Field504 === "true") {
                        misuratore_presente = true;
                    }

                    if (r[0].Field200 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field322 === "true") {
                        misuratore_presente = true;
                    }

                } else {

                    if (comanda.mobile === true) {

                        if (r[0].Field430 === "true") {
                            misuratore_presente = true;
                        }
                        if (r[0].Field431 === "true") {
                            misuratore_presente = true;
                        }
                        if (r[0].Field432 === "true") {
                            misuratore_presente = true;
                        }
                        if (r[0].Field433 === "true") {
                            misuratore_presente = true;
                        }

                    }

                    if (r[0].Field197 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field198 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field199 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field200 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field329 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field201 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field202 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field364 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field365 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field366 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field367 === "true") {
                        misuratore_presente = true;
                    }
                    if (r[0].Field207 === "true") {
                        misuratore_presente = true;
                    }



                }
            }

            self.listaIVA["IVA_0"] = "ES";
            self.listaIVA["IVA_0_DECIMALE"] = "";

            for (let i = 1; i <= 9; i++) {
                await this.richiedi_dato(i);
            }

            self.listaIVA["IVA_10"] = "EE";
            self.listaIVA["IVA_10_DECIMALE"] = "";

            self.listaIVA["IVA_11"] = "NS";
            self.listaIVA["IVA_11_DECIMALE"] = "";

            self.listaIVA["IVA_12"] = "NI";
            self.listaIVA["IVA_12_DECIMALE"] = "";

            self.listaIVA["IVA_13"] = "RM";
            self.listaIVA["IVA_13_DECIMALE"] = "";

            self.listaIVA["IVA_14"] = "AL";
            self.listaIVA["IVA_14_DECIMALE"] = "";

            self.listaIVA["IVA_15"] = "V";
            self.listaIVA["IVA_15_DECIMALE"] = "";


        }
    };


    this.richiedi_dato = async function (i) {


        return new Promise(function (resolve, reject) {

            if (misuratore_presente === true) {

                var xml = '<printerCommand><directIO  command="4205" data="' + aggZero(i, 2) + '" /></printerCommand>';
                var epos = new epson.fiscalPrint();
                epos.send("http://" + comanda.indirizzo_cassa + "/cgi-bin/fpmate.cgi?devid=local_printer&timeout=10000", xml, 10000);
                epos.onreceive = function (result, tag_names_array, add_info, res_add) {
                    var ogg_risp = new oggettoRispostaMisuratore(add_info);
                    self.listaIVA["IVA_" + i] = ogg_risp.iva;
                    self.listaIVA["IVA_" + i + "_DECIMALE"] = ogg_risp.iva_decimale;
                    resolve(true);
                };

                epos.onerror = function () {

                    if (alert_aperto_misuratore === false) {
                        alert_aperto_misuratore = true;
                        bootbox.alert("Problema nella lettura dei reparti (IVA) dal misuratore RT.<br><br>NON LAVORARE SE DEVI STAMPARE SCONTRINI O FATTURE!<br><br>RIAVVIA IL MISURATORE, ATTENDI UN MINUTO, E POI RIAVVIA IL SOFTWARE.<br><br>Non preoccuparti, sono cose di routine.");
                        resolve(false);
                    } else {
                        resolve(false);
                    }

                };

            } else {

                resolve(false);

            }

        });


    };

    //this.init();

    /*indice iva
     substr(50,2)
     
     in base a indice iva
     trovi l aliquota
     */

    /* Fine costruttore */

}



function IVA_view() {

    for (let i = 0; i <= 15; i++) {
        this["IVA_" + i] = document.getElementById("IVA_" + i);
        this["IVA_" + i + "_DECIMALE"] = document.getElementById("IVA_" + i + "_DECIMALE");

        /* tabelle accessorie */
        this["REPARTO_IVA_" + i] = document.getElementById("REPARTO_IVA_" + i);
        this["REPARTO_IVA_" + i + "_DECIMALE"] = document.getElementById("REPARTO_IVA_" + i + "_DECIMALE");

    }

}



function IVA_controller() {


    this.init = function () {

        for (let c = 1; c <= 20; c++) {
            $(REPARTO_view["REPARTO_" + c]).html("");
        }

        for (let i = 0; i <= 15; i++) {
            IVA_view["IVA_" + i].value = IVA_model.listaIVA["IVA_" + i];
            IVA_view["IVA_" + i + "_DECIMALE"].value = IVA_model.listaIVA["IVA_" + i + "_DECIMALE"];

            /*tabelle accessorie*/
            IVA_view["REPARTO_IVA_" + i].innerHTML = IVA_model.listaIVA["IVA_" + i];

            //GUARDARE PERCHE IL CICLO VIENE RIPETUTO PIU VOLTE

            for (let c = 1; c <= 20; c++) {
                $(REPARTO_view["REPARTO_" + c]).append("<option value='" + aggZero(i, 2) + "'>" + i + " - " + IVA_model.listaIVA["IVA_" + i] + "%</option>");
            }
        }

    };

    this.salva = async function () {

        /* p significa parametro */

        let p = new Array();
        let p_DECIMALE = new Array();

        for (let i = 1; i <= 9; i++) {
            p[i] = IVA_view["IVA_" + i].value;
            p_DECIMALE[i] = IVA_view["IVA_" + i + "_DECIMALE"].value;
        }

        await IVA_model.salva_in_misuratore(p, p_DECIMALE);

        IVA_controller.aggiorna();
    };

    this.aggiorna = async function (callback) {
        await IVA_model.init();
        IVA_controller.init();
        if (typeof callback === "function") {
            callback(true);
        }
    };

    this.indice = function (numero) {

        //MANCA DECIMALE
        /*return IVA_model.listaIVA["IVA_" + numero];*/

    };

    this.tabella_IVA_semplice = function () {

        let dataset = alasql("select * from tabella_iva where lingua='" + comanda.lingua_stampa + "';");

        let tabella = '';

        dataset.forEach((obj) => {
            tabella += '<option value="' + obj.aliquota + '">' + obj.aliquota + '%</option>';
        });

        return tabella;

    };

}

/*Dichiaro le classi statiche*/
/* le variabili vengono inizializzate quando vedo gli ip delle stampanti su multilanguage.js */

var IVA_model;
var IVA_view;
var IVA_controller;