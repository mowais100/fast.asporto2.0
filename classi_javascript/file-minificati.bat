java -jar compiler.jar --js gestione_db.js --js_output_file gestione_db-min.js --create_source_map gestione_db-min.js.map --source_map_format=V3
java -jar compiler.jar --js funzioni_db_locale.js --js_output_file funzioni_db_locale-min.js --create_source_map funzioni_db_locale-min.js.map --source_map_format=V3
java -jar compiler.jar --js socket.js --js_output_file socket-min.js --create_source_map socket-min.js.map --source_map_format=V3
java -jar compiler.jar --js multilanguage.js --js_output_file multilanguage-min.js --create_source_map multilanguage-min.js.map --source_map_format=V3
java -jar compiler.jar --js cassa.js --js_output_file cassa-min.js --create_source_map cassa-min.js.map --source_map_format=V3
java -jar compiler.jar --js gestione_settaggi.js --js_output_file gestione_settaggi-min.js --create_source_map gestione_settaggi-min.js.map --source_map_format=V3
java -jar compiler.jar --js classi_pizzeria_asporto.js --js_output_file classi_pizzeria_asporto-min.js --create_source_map classi_pizzeria_asporto-min.js.map --source_map_format=V3
