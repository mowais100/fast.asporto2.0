/* global Infinity, comanda, bootbox */

async function adeguamento_dati_passati(colonna) {

    $('.loader2').show();
    let query_selezione_dato = "select id,descrizione," + colonna + " from prodotti where categoria!='XXX' and " + colonna + "  is not null and " + colonna + " !='' and " + colonna + " !='null' and " + colonna + " !='undefined';";

    let ris = alasql(query_selezione_dato);



    for (let v of ris) {

        let query_modifica = "update comanda set " + colonna + " ='" + v[colonna] + "' where cod_articolo='" + v['id'] + "'";
        let query_modifica_back_office = "";

        switch (colonna) {
            case "costo":
                query_modifica_back_office = "update back_office set costo ='" + v[colonna] + "',totale_costo=(" + v[colonna] + "*quantita) ,totale_ricavo=(prezzo_un*quantita)-(" + v[colonna] + "*quantita)  where cod_articolo='" + v['id'] + "'";
                break;

            default:
                query_modifica_back_office = "update back_office set " + colonna + " ='" + v[colonna] + "' where cod_articolo='" + v['id'] + "'";
        }


        await modifica_dati_passati(query_modifica, query_modifica_back_office);


    }

    bootbox.alert("Adeguamento Sqlite/Mysql/Back_Office Completato");

    $('.loader2').hide();

}

async function modifica_dati_passati(query_modifica, query_modifica_back_office, length) {

    var p1 = new Promise(function (resolve, reject) {

        alasql(query_modifica);

        comanda.sock.send({tipo: "aggiornamento", operatore: comanda.operatore, query: query_modifica, terminale: comanda.terminale, ip: comanda.ip_address});

        resolve(true);

    });

    var p2 = new Promise(function (resolve, reject) {
        comanda.sincro.query_incassi(query_modifica, Infinity, function () {

            resolve(true);

        });
    });

    var p3 = new Promise(function (resolve, reject) {
        comanda.sincro.query_incassi(query_modifica_back_office, Infinity, function () {

            resolve(true);

        });

    });

    return new Promise(resolve => {

        Promise.all([p1, p2, p3]).then(function () {
            resolve(true);
        });

    });
}

function autoAdeguamentoDB(ignora_numero_record, callback) {

    let url = '';

    if (comanda.compatibile_xml === true) {
        url = comanda.url_server + 'classi_php/autoAdeguamentoDBIbrido.php';
    } else {
        url = comanda.url_server + 'classi_php/autoAdeguamentoDB.php';
    }

    $.ajax({
        type: "POST",
        async: true,
        url: url,
        data: {
            ignora_numero_record: ignora_numero_record
        },

        beforeSend: function () {

        },
        success: function (risultato) {

            if (risultato === "true") {
                callback(true);
            } else if (risultato === "WARNING_NUMERO_RECORD") {
                callback(true);
                setTimeout(function () {
                    bootbox.confirm("Attenzione: per adeguare il database delle statistiche automaticamente all'avvio ci vorrebbe troppo tempo.\n\
                                <br><br>Vuoi adeguarlo manualmente in background, e ti daremo un avviso quando avra' finito? Nel frattempo potrai continuare a lavorare.\n\
                                <br><br>NB: Se stai già facendo l'adeguamento del database da un altro computer, NON FARLO DA QUESTO\n\
                                <br><br>NB2: Conviene fare sempre questa operazione dal server o dalla cassa meno utilizzata, per questioni di tempo",
                            function (risposta) {
                                if (risposta === true) {
                                    autoAdeguamentoDB(true, function () {
                                        bootbox.alert("Adeguamento Database Statistiche completato in background. Ora puoi utilizzare il backoffice e la stampa incassi");
                                    });
                                }
                            });
                }, 10000);
            } else {
                setTimeout(function () {
                    bootbox.alert("Errore nell'adeguamento automatico: " + risultato);
                }, 10000);
                callback(false);
            }

        }, error: function () {
            setTimeout(function () {
                bootbox.alert("Errore nell'adeguamento automatico: CHIAMATA ALLO SCRIPT ERRATA<br><br>Questo avviso si chiuderà da solo tra 10 secondi.");
            }, 10000);

            callback(false);
        }
    });



}