"use strict";

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/* global comanda */
function sblocca_socket() {
  if (comanda.socket_ciclizzati[0] !== undefined) {
    salva_log_sempreattivo(comanda.socket_ciclizzati[0].query);
    comanda.socket_ciclizzati.shift();
  }
}

function sblocca_db_centrale() {
  if (comanda.array_dbcentrale_mancanti[0] !== undefined) {
    salva_log_sempreattivo(comanda.array_dbcentrale_mancanti[0]);
    comanda.array_dbcentrale_mancanti.shift();
  }
}

var Sincro = function Sincro() {
  var dbtype = 'sqlite';
  var dbname = 'DATABASE_CLIENTE.sqlite';
  var dbnameSettaggi = 'DATABASE_SETTAGGI.sqlite';
  var db = new Object(); //MANCA LA GESTIONE ERRORI
  //SI RISCHIA DI NON ALLINEARE BENE LE TABELLE SE CI SONO ERRORI DI QUERY

  var ar;
  var query = '';

  this.return_db = function () {
    return db;
  };

  this.backup_corpi = function (callback) {
    var operatore = null;

    if (comanda.operatore !== undefined && comanda.operatore !== null && comanda.operatore.length > 0) {
      operatore = comanda.operatore;
    } else {
      operatore = prompt('CORTESEMENTE, INSERISCI UN IDENTIFICATIVO DEL TABLET');
    }

    if (operatore !== undefined && operatore !== null && operatore.length > 0) {
      var colonne = [];
      var righe = [];
      comanda.sincro.query('select * from comanda where terminale="' + comanda.terminale + '";', function (result) {
        result.forEach(function (obj) {
          for (var key in obj) {
            colonne.push('\"' + obj[key] + '\"');
          }

          righe.push(colonne.join());
          colonne = [];
        });
        var database_intero = righe.join('\r\n');
        comanda.sincro.backup_db_background(operatore + '_CORPI', database_intero, function (cb) {
          if (cb === true) {
            if (comanda.socket_ciclizzati.length > 0) {
              bootbox.alert("ERRORE (1). <br><br>NON HAI SINCRONIZZATO TUTTI I TUOI DATI CON GLI ALTRI COMPUTER. FAI OK E RITENTA DI CHIUDERE TRA 10 SECONDI. SE IL PROBLEMA PERSISTESSE CONTATTA L'ASSISTENZA.");

              if (comanda.socket_ciclizzati[0] !== undefined) {
                salva_log_sempreattivo(comanda.socket_ciclizzati[0].query);
              }

              callback(false);
            } else {
              comanda.sincro.query('select * from comanda;', function (result) {
                result.forEach(function (obj) {
                  for (var key in obj) {
                    colonne.push('\"' + obj[key] + '\"');
                  }

                  righe.push(colonne.join());
                  colonne = [];
                });
                var database_intero = righe.join('\r\n');
                comanda.sincro.backup_db_background(operatore + "_CORPI", database_intero, function (cb) {
                  if (cb === true) {
                    if (comanda.socket_ciclizzati.length > 0) {
                      bootbox.alert("ERRORE (2). <br><br>NON HAI SINCRONIZZATO TUTTI I TUOI DATI CON GLI ALTRI COMPUTER. FAI OK E RITENTA DI CHIUDERE TRA 10 SECONDI. SE IL PROBLEMA PERSISTESSE CONTATTA L'ASSISTENZA.");

                      if (comanda.socket_ciclizzati[0] !== undefined) {
                        salva_log_sempreattivo(comanda.socket_ciclizzati[0].query);
                      }

                      callback(false);
                    } else {
                      callback(true);
                    }
                  } else {
                    alert("ERRORE. IL BACKUP LOCALE (2) NON E' STATO SALVATO CORRETTAMENTE. RIPROVA.");
                    callback(false);
                  }
                });
              }); //callback(true);
            }
          } else {
            alert("ERRORE. IL BACKUP LOCALE (1) NON E' STATO SALVATO CORRETTAMENTE. RIPROVA.");
            callback(false);
          }
        });
      });
    } else {
      alert("ERRORE. DEVI INSERIRE ALMENO UN OPERATORE O UN IDENTIFICATIVO PER PROCEDERE. RIPROVA.");
      callback(false);
    }
  };

  this.backup_teste = function (callback) {
    var operatore = null;

    if (comanda.operatore !== undefined && comanda.operatore !== null && comanda.operatore.length > 0) {
      operatore = comanda.operatore;
    } else {
      operatore = prompt('CORTESEMENTE, INSERISCI UN IDENTIFICATIVO DEL TABLET');
    }

    if (operatore !== undefined && operatore !== null && operatore.length > 0) {
      var colonne = [];
      var righe = [];
      comanda.sincro.query('select * from record_teste;', function (result) {
        result.forEach(function (obj) {
          for (var key in obj) {
            colonne.push('\"' + obj[key] + '\"');
          }

          righe.push(colonne.join());
          colonne = [];
        });
        var database_intero = righe.join('\r\n');
        comanda.sincro.backup_db_background(operatore + '_TESTE', database_intero, function (cb) {
          if (cb === true) {
            if (comanda.socket_ciclizzati.length > 0) {
              bootbox.alert("ERRORE (3). <br><br>NON HAI SINCRONIZZATO TUTTI I TUOI DATI CON GLI ALTRI COMPUTER. FAI OK E RITENTA DI CHIUDERE TRA 10 SECONDI. SE IL PROBLEMA PERSISTESSE CONTATTA L'ASSISTENZA.");

              if (comanda.socket_ciclizzati[0] !== undefined) {
                salva_log_sempreattivo(comanda.socket_ciclizzati[0].query);
              }

              callback(false);
            } else {
              comanda.sincro.query('select * from record_teste;', function (result) {
                result.forEach(function (obj) {
                  for (var key in obj) {
                    colonne.push('\"' + obj[key] + '\"');
                  }

                  righe.push(colonne.join());
                  colonne = [];
                });
                var database_intero = righe.join('\r\n');
                comanda.sincro.backup_db_background(operatore + "_TESTE", database_intero, function (cb) {
                  if (cb === true) {
                    if (comanda.socket_ciclizzati.length > 0) {
                      bootbox.alert("ERRORE 4. <br><br>NON HAI SINCRONIZZATO TUTTI I TUOI DATI CON GLI ALTRI COMPUTER. FAI OK E RITENTA DI CHIUDERE TRA 10 SECONDI. SE IL PROBLEMA PERSISTESSE CONTATTA L'ASSISTENZA.");

                      if (comanda.socket_ciclizzati[0] !== undefined) {
                        salva_log_sempreattivo(comanda.socket_ciclizzati[0].query);
                      }

                      callback(false);
                    } else {
                      callback(true);
                    }
                  } else {
                    alert("ERRORE. IL BACKUP LOCALE (4) NON E' STATO SALVATO CORRETTAMENTE. RIPROVA.");
                    callback(false);
                  }
                });
              }); //callback(true);
            }
          } else {
            alert("ERRORE. IL BACKUP LOCALE (3) NON E' STATO SALVATO CORRETTAMENTE. RIPROVA.");
            callback(false);
          }
        });
      });
    } else {
      alert("ERRORE. DEVI INSERIRE ALMENO UN OPERATORE O UN IDENTIFICATIVO PER PROCEDERE. RIPROVA.");
      callback(false);
    }
  };

  this.backup_db_locale = function (callback) {
    var promessa_teste = new Promise(function (resolve, reject) {
      comanda.sincro.backup_teste(function () {
        resolve(true);
      });
    });
    var promessa_corpi = new Promise(function (resolve, reject) {
      comanda.sincro.backup_corpi(function () {
        resolve(true);
      });
    });
    Promise.all([promessa_teste, promessa_corpi]).then(function () {
      callback(true);
    });
  };

  this.backup_db_background = function (operatore, struttura, callBack) {
    var url_backup;

    if (location.protocol === 'http:' || location.protocol === 'https:') {
      url_backup = './classi_php/salva_file.php';
    } else {
      url_backup = comanda.url_server + 'classi_php/salva_file.php';
    }

    var request = $.ajax(_defineProperty({
      timeout: 10000,
      method: "POST",
      data: {
        android: comanda.AndroidPrinter,
        uuid: comanda.uuid
      },
      url: url_backup
    }, "data", {
      operatore: operatore,
      struttura: struttura
    }));
    request.done(function (data) {
      console.log("SALVAFILEXML DONE", struttura, CryptoJS.MD5(struttura).toString(), data);

      if (data === CryptoJS.MD5(struttura).toString()) {
        callBack(true);
      } else {
        console.log("SALVAFILEXML FAIL");
        callBack(false);
      }
    });
    request.fail(function (event) {
      console.log("SALVAFILEXML FAIL2");
      callBack(false);
    });
  };

  this.sincronizzazione_upload = function (tab, id_lic, callback) {
    var testo_query = new Array();

    switch (tab) {
      case "comanda":
        testo_query[0] = 'SELECT * FROM ' + tab + ' WHERE stato_record="CHIUSO" AND id LIKE "' + comanda.operatore.substr(0, 3) + '_%";';
        testo_query[1] = 'DELETE FROM comanda WHERE stato_record = "CHIUSO";';
        break;

      default:
        testo_query[0] = 'SELECT * FROM ' + tab + ';';
        testo_query[1] = 'select count(0);';
        break;
    }

    comanda.sincro.query(testo_query[0], function (righe) {
      ////console.log(righe);
      var cols = new Array();
      var rows = new Array();
      righe.forEach(function (riga) {
        ////console.log(riga);
        var key = new Array();
        var value = new Array();
        var i = 0;

        for (var chiave in riga) {
          key[i] = chiave; ////console.log(riga[chiave]);

          value[i] = "'" + escape(riga[chiave]) + "'";
          i++;
        }

        cols.push(key.join());
        rows.push(value.join());
      });
      $.ajax({
        type: "POST",
        dataType: "text",
        url: comanda.url_server + 'demo_api_html5/sincro_upload.php',
        data: {
          tbx: tab,
          colonne: JSON.stringify(cols[0]),
          righe: JSON.stringify(rows),
          type: dbtype,
          db_name: dbname,
          key: comanda.k1 + comanda.k2 + comanda.k3 + comanda.k4
        },
        success: function success(risposta) {
          if (risposta === "") {
            comanda.sincro.query(testo_query[1], function () {});
          } else {}
        }
      });
    });
  };

  this.sincronizzazione_download = function (callback) {
    var ciao = new Promise(function (resolve, reject) {
      /*
       * 09/03/2021 TOLTO IP E SOSTITUITO IN TUTTI I CASI, ANCHE MOBILE, L'ID DEL TERMINALE PER UNIFORMITA'
       * 
       * if (comanda.mobile === true) {
       document.addEventListener("deviceready", function () {
       getIPs(function (ip) {
       
       comanda.ip_address = ip;
       comanda.terminale = device.manufacturer + " " + device.model + '-' + ip;
       
       resolve(true);
       
       });
       }, false);
       resolve(true);
       } else if (comanda.ip_fisso_terminale === undefined || comanda.ip_fisso_terminale.length === 0 || comanda.ip_fisso_terminale === '' || comanda.ip_fisso_terminale === '0' || comanda.ip_fisso_terminale === 0)
       {
       getIPs(function (d) {
       
       comanda.ip_address = d;
       comanda.terminale = leggi_get_url('id') + '-' + d;
       
       resolve(true);
       
       });
       } else
       {
       comanda.ip_address = comanda.ip_fisso_terminale;
       comanda.terminale = leggi_get_url('id') + '-' + comanda.ip_fisso_terminale;
       resolve(true);
       }*/
      if (comanda.mobile === true) {
        if (comanda.terminale) {
          resolve(true);
        } else {
          var a = bootbox.alert("Avviso: dal 9 Marzo 2021 per uniformare i dati devi specificare un id di terminale nelle impostazioni del telefono. Per ora di default ho messo id 1. Clicca OK per continuare.", function () {
            comanda.terminale = "1";
            resolve(true);
          });
        }
      } else {
        var id_terminale = "";
        comanda.sincro.query_locale('SELECT * FROM settaggi WHERE nome="id_terminale";', function (risultato_settaggi_locali) {
          if (risultato_settaggi_locali.length > 0) {
            if (risultato_settaggi_locali[0].valore !== null && risultato_settaggi_locali[0].valore !== "") {
              id_terminale = risultato_settaggi_locali[0].valore;
            }
          }

          if (id_terminale) {
            comanda.terminale = id_terminale;
            resolve(true);
          } else {
            var _a = bootbox.alert("Avviso: dal 9 Marzo 2021 per uniformare i dati devi specificare un id di terminale nella tabella dei settaggi locale. Per ora di default ho messo id 1. Clicca OK per continuare.", function () {
              comanda.terminale = "1";
              resolve(true);
            });
          }
        });
      }
    });
    ciao.then(function () {
      if (true) {
        autoAdeguamentoDB(false, function () {
          sincronizzazione_alasql("id_occupati_forno", function () {
            sincronizzazione_alasql("terminali", function () {
              sincronizzazione_alasql("pony_express", function () {
                sincronizzazione_alasql("appuntamenti_calendario", function () {
                  sincronizzazione_alasql("cambi_listino", function () {
                    sincronizzazione_alasql("dati_servizio", function () {
                      sincronizzazione_alasql("settaggi_profili", function () {
                        scelta_profilo(function () {
                          sincronizzazione_alasql("gruppi_statistici", function () {
                            sincronizzazione_alasql("categorie", function () {
                              alasql("insert into categorie (id,descrizione,abilita_asporto,visu_tavoli,visu_bar,visu_asporto,visu_continuo,disabilita_pda,disabilita_server,consegna_abilitata,disabilita_visualizzazione,perc_iva,perc_iva_takeaway,reparto_servizi,reparto_beni) values ('V_666','SPECIFICHE','false','false','false','false','false','N','N','false','true','10','10','1','1');");
                              alasql("insert into categorie (id,descrizione,abilita_asporto,visu_tavoli,visu_bar,visu_asporto,visu_continuo,disabilita_pda,disabilita_server,consegna_abilitata,disabilita_visualizzazione,perc_iva,perc_iva_takeaway,reparto_servizi,reparto_beni) values ('666','COMPENSAZIONI','false','false','false','false','false','N','N','false','true','10','10','1','1');");
                              sincronizzazione_alasql("chiusure_fiscali", function () {
                                sincronizzazione_alasql("circuiti", function () {
                                  sincronizzazione_alasql("clienti", function () {
                                    sincronizzazione_alasql("socket_listeners", function () {
                                      /*QUESTO BLOCCO DEVE RESTARE COSI' COM E'*/
                                      comanda.loading_tab_comanda = true;
                                      comanda.sock.send({
                                        tipo: "notification",
                                        operatore: comanda.operatore,
                                        query: comanda.ip_address + ' inizio download comanda',
                                        terminale: comanda.terminale,
                                        ip: comanda.ip_address
                                      });
                                      sincronizzazione_alasql("comanda", function () {
                                        comanda.loading_tab_tavoli = true;
                                        alasql("CREATE INDEX rptci ON comanda(ntav_comanda,stato_record,posizione,categoria,id);");
                                        alasql("CREATE INDEX `QTAP_c` ON `comanda` (`QTAP` ASC)");
                                        alasql("CREATE INDEX `bis_c` ON `comanda` (`BIS` ASC)");
                                        alasql("CREATE INDEX `cont_var_c` ON `comanda` (`contiene_variante` ASC)");
                                        alasql("CREATE INDEX `desc_art_c` ON `comanda` (`desc_art` ASC)");
                                        alasql("CREATE INDEX `fiscalesospeso_c` ON `comanda` (`fiscale_sospeso` ASC)");
                                        alasql("CREATE INDEX `id_c` ON `comanda` (`id` ASC)");
                                        alasql("CREATE INDEX `n_conto_c` ON `comanda` (`numero_conto` ASC)");
                                        alasql("CREATE INDEX `ntav_c` ON `comanda` (`ntav_comanda` ASC)");
                                        alasql("CREATE INDEX `posizione_c` ON `comanda` (`posizione` ASC)");
                                        alasql("CREATE INDEX `st_record_c` ON `comanda` (`stato_record` ASC)");
                                        alasql("CREATE INDEX `stampata_sn_c` ON `comanda` (`stampata_sn` ASC)");
                                        sincronizzazione_alasql("tavoli", function () {
                                          sincronizzazione_alasql("tavoli_uniti", function () {
                                            sincronizzazione_alasql("centri_statistica", function () {
                                              sincronizzazione_alasql("progressivo_asporto", function () {
                                                sincronizzazione_alasql("progressivo_asporto_domicilio", function () {
                                                  sincronizzazione_alasql("elenco_progressivi_asporto_domicilio", function () {
                                                    sincronizzazione_alasql("elenco_codici_lotteria_comanda", function () {
                                                      //Dev'essere dopo tavoli e dopo progressivo asporto
                                                      Class_Servizio.servizio(function (callback_servizio) {
                                                        sincronizzazione_android_socket(function () {
                                                          /*FINE BLOCCO*/
                                                          sincronizzazione_alasql("cod_promo_2", function () {
                                                            sincronizzazione_alasql("impostazioni_fiscali", function () {
                                                              sincronizzazione_alasql("lingue", function () {
                                                                sincronizzazione_alasql("mov_fat", function () {
                                                                  sincronizzazione_alasql("nomi_portate", function () {
                                                                    sincronizzazione_alasql("nomi_stampanti", function () {
                                                                      sincronizzazione_alasql("operatori", function () {
                                                                        sincronizzazione_alasql("consegna_tempo", function () {
                                                                          sincronizzazione_alasql("prodotti", function () {
                                                                            alasql("CREATE INDEX icd ON prodotti(categoria,descrizione,id,ult_mod);");
                                                                            alasql("CREATE INDEX `id_p` ON `prodotti` (`id` ASC)");
                                                                            sincronizzazione_alasql("profilo_aziendale", function () {
                                                                              sincronizzazione_alasql("progressivi_fiscali", function () {
                                                                                sincronizzazione_alasql("record_teste", function () {
                                                                                  sincronizzazione_alasql("settaggi_ibrido", function () {
                                                                                    Class_Servizio.aggiorna_servizio(callback_servizio, function (cb2s) {
                                                                                      sincronizzazione_alasql("specifiche", function () {
                                                                                        sincronizzazione_alasql("tabella_iva", function () {
                                                                                          sincronizzazione_alasql("tabella_sconti_buoni", function () {
                                                                                            sincronizzazione_alasql("tagli_buoni", function () {
                                                                                              sincronizzazione_alasql("gestione_buoni_sconto", function () {
                                                                                                //QUERY CASSA PER SERVIZIO
                                                                                                //NON ELIMINARE ASSOLUTAMENTE
                                                                                                comanda.sincro.query_cassa();
                                                                                                callback(true);
                                                                                              });
                                                                                            });
                                                                                          });
                                                                                        });
                                                                                      });
                                                                                    });
                                                                                  });
                                                                                });
                                                                              });
                                                                            });
                                                                          });
                                                                        });
                                                                      });
                                                                    });
                                                                  });
                                                                });
                                                              });
                                                            });
                                                          });
                                                        });
                                                      });
                                                    });
                                                  });
                                                });
                                              });
                                            });
                                          });
                                        });
                                      }, comanda.data_servizio);
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      } else if (typeof sqlitePlugin !== "undefined") {
        db = sqlitePlugin.openDatabase({
          name: "memoria.db",
          location: 'default',
          createFromLocation: 1
        });
        comanda.sincro.db_2 = db;
        autoAdeguamentoDB(false, function () {
          sincronizzazione_android("id_occupati_forno", function () {
            sincronizzazione_android("terminali", function () {
              sincronizzazione_android("cambi_listino", function () {
                sincronizzazione_android("gruppi_statistici", function () {
                  sincronizzazione_android("categorie", function () {
                    sincronizzazione_android("chiusure_fiscali", function () {
                      sincronizzazione_android("circuiti", function () {
                        sincronizzazione_android("clienti", function () {
                          sincronizzazione_android("socket_listeners", function () {
                            /*QUESTO BLOCCO DEVE RESTARE COSI' COM E'*/
                            comanda.loading_tab_comanda = true;
                            comanda.sock.send({
                              tipo: "notification",
                              operatore: comanda.operatore,
                              query: comanda.ip_address + ' inizio download comanda',
                              terminale: comanda.terminale,
                              ip: comanda.ip_address
                            });
                            sincronizzazione_android("comanda", function () {
                              comanda.loading_tab_tavoli = true;
                              sincronizzazione_android("tavoli", function () {
                                sincronizzazione_android("tavoli_uniti", function () {
                                  sincronizzazione_android_socket(function () {
                                    /*FINE BLOCCO*/
                                    sincronizzazione_android("cod_promo_2", function () {
                                      sincronizzazione_android("dati_servizio", function () {
                                        sincronizzazione_android("impostazioni_fiscali", function () {
                                          sincronizzazione_android("lingue", function () {
                                            sincronizzazione_android("mov_fat", function () {
                                              sincronizzazione_android("nomi_portate", function () {
                                                sincronizzazione_android("nomi_stampanti", function () {
                                                  sincronizzazione_android("operatori", function () {
                                                    sincronizzazione_android("prodotti", function () {
                                                      sincronizzazione_android("profilo_aziendale", function () {
                                                        sincronizzazione_android("progressivi_fiscali", function () {
                                                          sincronizzazione_android("progressivo_asporto", function () {
                                                            sincronizzazione_android("progressivo_asporto_domicilio", function () {
                                                              sincronizzazione_android("elenco_progressivi_asporto_domicilio", function () {
                                                                sincronizzazione_android("elenco_codici_lotteria_comanda", function () {
                                                                  sincronizzazione_android("record_teste", function () {
                                                                    sincronizzazione_android("settaggi_ibrido", function () {
                                                                      sincronizzazione_android("settaggi_profili", function () {
                                                                        sincronizzazione_android("specifiche", function () {
                                                                          sincronizzazione_android("tabella_iva", function () {
                                                                            sincronizzazione_android("tabella_sconti_buoni", function () {
                                                                              sincronizzazione_android("tagli_buoni", function () {
                                                                                sincronizzazione_android("gestione_buoni_sconto", function () {
                                                                                  callback(true);
                                                                                });
                                                                              });
                                                                            });
                                                                          });
                                                                        });
                                                                      });
                                                                    });
                                                                  });
                                                                });
                                                              });
                                                            });
                                                          });
                                                        });
                                                      });
                                                    });
                                                  });
                                                });
                                              });
                                            });
                                          });
                                        });
                                      });
                                    });
                                  });
                                });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      } else {
        sincronizzazione_windows(function () {
          callback(true);
        });
      }
    });
  };

  function sincronizzazione_android(tab, callback) {
    $.ajax({
      beforeSend: function beforeSend() {},
      complete: function complete() {},
      success: function success(data) {
        ar = data; //console.log("JSON DATA", ar);

        if (ar && ar !== null && ar.length > 0 && ar[0] && ar[0] !== null) {
          var testo_query;
          testo_query = 'DROP TABLE IF EXISTS ' + tab + ';';
          db.transaction(function (tx, results) {
            tx.executeSql(testo_query);
            console.log("QUERY INCRIMINATA", testo_query);
            var colonne_create_array = ar[0].map(function (e) {
              return e + " TEXT DEFAULT '' ";
            });
            var colonne_create_string = colonne_create_array.join(',');
            var colonne = ar[0].join(',');

            if (tab === "lingue") {
              testo_query = 'CREATE TABLE IF NOT EXISTS ' + tab + ' (' + colonne_create_string + ');';
            } else {
              testo_query = 'CREATE TABLE ' + tab + ' (' + colonne_create_string + ');';
            }

            tx.executeSql(testo_query);
            ar.shift();
            ar.forEach(function (element, index, array) {
              var valori = Object.keys(element).map(function (key) {
                if (typeof element[key] === "string") {
                  return element[key].replace(/\'/, '');
                } else {
                  return element[key];
                }
              });
              valori = valori.join('","');
              valori = '"' + valori + '"';
              testo_query = 'INSERT INTO ' + tab + ' (' + colonne + ') VALUES (' + valori + ');';
              tx.executeSql(testo_query);
            });
            callback(true);
          });
        } else {
          callback(true);
        }

        $('.spia.status_server').css('background-color', 'green');
      },
      error: function error() {},
      type: "POST",
      dataType: "json",
      url: comanda.url_server + 'demo_api_html5/sqlite_json.lib.php',
      //TIMEOUT ALTRIMENTI DA PALMARE NON PARTE MAI
      //MENO DI 30 RISCHI PER CHI HA LA REPLICA
      timeout: 0,
      data: {
        tbx: tab,
        type: 'sqlite',
        db_name: 'DATABASE_CLIENTE.sqlite'
      }
    });
  }

  function sincronizzazione_alasql(tab, callback) {
    var eventuale_data_servizio = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "";
    var DBNameInterno = dbname;

    if (tab === "settaggi_profili" || tab === "profilo_aziendale") {
      DBNameInterno = dbnameSettaggi;
    }

    $.ajax({
      beforeSend: function beforeSend(jqXHR) {},
      complete: function complete() {},
      success: function success(data) {
        ar = data; //console.log("JSON DATA", ar);

        if (ar && ar !== null && ar.length > 0 && ar[0] && ar[0] !== null) {
          var testo_query;
          testo_query = 'DROP TABLE IF EXISTS ' + tab + ';';
          alasql(testo_query);
          console.log("QUERY INCRIMINATA", testo_query);
          var colonne_create_array = ar[0].map(function (e) {
            return e + " TEXT DEFAULT '' ";
          });
          var colonne_create_string = colonne_create_array.join(',');
          var colonne = ar[0].join(',');

          if (tab === "lingue") {
            testo_query = 'CREATE TABLE IF NOT EXISTS ' + tab + ' (' + colonne_create_string + ');';
          } else {
            testo_query = 'CREATE TABLE ' + tab + ' (' + colonne_create_string + ');';
          }

          alasql(testo_query);
          ar.shift();
          /*ar.forEach(
           function (element, index, array)
           {
           var valori = Object.keys(element).map(function (key) {
           if (typeof (element[key]) === "string") {
           return element[key].replace(/\'/, '');
           } else
           {
           return element[key];
           }
           });
           
           valori = valori.join('","');
           valori = '"' + valori + '"';
           
           testo_query = 'INSERT INTO ' + tab + ' (' + colonne + ') VALUES (' + valori + ');';
           
           alasql(testo_query);
           
           });*/

          alasql.tables[tab].data = ar;
          callback(true);
        } else {
          callback(true);
        }

        $('.spia.status_server').css('background-color', 'green');
      },
      error: function error() {},
      type: "POST",
      dataType: "json",
      url: comanda.url_server + 'demo_api_html5/sqlite_json.lib.php',
      //TIMEOUT ALTRIMENTI DA PALMARE NON PARTE MAI
      //MENO DI 30 E' TROPPO POCO SE HAI LA REPLICA E TANTI DATI TIPO BRO
      timeout: 0,
      data: {
        tbx: tab,
        type: 'sqlite',
        data_servizio: eventuale_data_servizio,
        db_name: DBNameInterno,
        key: comanda.k1 + comanda.k2 + comanda.k3 + comanda.k4
      }
    });
  }

  function sincronizzazione_android_socket(callback) {
    comanda.socket_starting = true;
    socket_init(once(function () {
      comanda.socket_starting = false;

      if (comanda.socket_sospesi_tavoli.length > 0) {
        var iter = 0;
        var cont = comanda.socket_sospesi_tavoli.length; //56

        comanda.socket_sospesi_tavoli.forEach(function (testo_query) {
          comanda.sincro.query(testo_query, function () {
            iter++;

            if (iter === cont) {
              comanda.loading_tab_tavoli = false;
            }
          });
        });
        comanda.loading_tab_tavoli = false;
        comanda.socket_sospesi_tavoli = [];
      } else {
        comanda.loading_tab_tavoli = false;
      }

      if (comanda.socket_sospesi.length > 0) {
        var iter = 0;
        var cont = comanda.socket_sospesi.length; //56

        console.log("ITER 2", iter, cont);
        comanda.socket_sospesi.forEach(function (testo_query) {
          comanda.sincro.query(testo_query, function () {
            iter++;

            if (iter === cont) {
              comanda.loading_tab_comanda = false;
            }
          });
        });
        comanda.loading_tab_comanda = false;
        comanda.socket_sospesi = [];
      } else {
        comanda.loading_tab_comanda = false;
      }

      callback(true);
    }));
  }

  function sincronizzazione_windows(callback) {
    $.ajax({
      type: "POST",
      async: true,
      url: 'http://localhost:' + comanda.porta_server_remoto + '/fast.intellinet/classi_php/trasferimentodb.php',
      data: {
        ip_server_centrale: comanda.url_server
      },
      dataType: "json",
      beforeSend: function beforeSend() {
        comanda.loading_tab_tavoli = true;
        comanda.loading_tab_comanda = true;
        comanda.sock.send({
          tipo: "notification",
          operatore: comanda.operatore,
          query: comanda.ip_address + ' inizio download comanda',
          terminale: comanda.terminale,
          ip: comanda.ip_address
        });
      },
      success: function success(risultato_json) {
        funzione_interna_success(risultato_json);
      },
      error: function error(errore) {
        funzione_interna_error(errore);
      }
    });

    var funzione_interna_success = function funzione_interna_success(risultato_json) {
      comanda.socket_starting = true;
      socket_init(once(function () {
        comanda.socket_starting = false;

        if (comanda.socket_sospesi_tavoli.length > 0) {
          var iter = 0;
          var cont = comanda.socket_sospesi_tavoli.length; //56

          comanda.socket_sospesi_tavoli.forEach(function (testo_query) {
            comanda.sincro.query(testo_query, function () {
              iter++;

              if (iter === cont) {
                comanda.loading_tab_tavoli = false;
              }
            });
          });
          comanda.loading_tab_tavoli = false;
          comanda.socket_sospesi_tavoli = [];
        } else {
          comanda.loading_tab_tavoli = false;
        }

        if (comanda.socket_sospesi.length > 0) {
          var iter = 0;
          var cont = comanda.socket_sospesi.length; //56

          console.log("ITER 2", iter, cont);
          comanda.socket_sospesi.forEach(function (testo_query) {
            comanda.sincro.query(testo_query, function () {
              iter++;

              if (iter === cont) {
                comanda.loading_tab_comanda = false;
              }
            });
          });
          comanda.loading_tab_comanda = false;
          comanda.socket_sospesi = [];
        } else {
          comanda.loading_tab_comanda = false;
        }

        if (risultato_json !== true) {
          alert("Ce un errore di connessione con il database centrale. \n\
Verifica che il server sia acceso e ri-connetti il dispositivo al Wifi.\n\
\n\
Se procederai con l'esecuzione del software, utilizzerai \n\
il DATABASE LOCALE NON SINCRONIZZATO, A TUO RISCHIO E PERICOLO.");
        }

        callback(true);
      }));
    };

    var funzione_interna_error = function funzione_interna_error(errore) {
      alert("Ce un errore di connessione con il database centrale. \n\
Verifica che il server sia acceso e ri-connetti il dispositivo al Wifi.\n\
\n\
Se procederai con l'esecuzione del software, utilizzerai \n\
il DATABASE LOCALE NON SINCRONIZZATO, A TUO RISCHIO E PERICOLO.");
      comanda.socket_starting = true;
      socket_init(once(function () {
        comanda.socket_starting = false;

        if (comanda.socket_sospesi_tavoli.length > 0) {
          var iter = 0;
          var cont = comanda.socket_sospesi_tavoli.length; //56

          comanda.socket_sospesi_tavoli.forEach(function (testo_query) {
            comanda.sincro.query(testo_query, function () {
              iter++;

              if (iter === cont) {
                comanda.loading_tab_tavoli = false;
              }
            });
          });
          comanda.loading_tab_tavoli = false;
          comanda.socket_sospesi_tavoli = [];
        } else {
          comanda.loading_tab_tavoli = false;
        }

        if (comanda.socket_sospesi.length > 0) {
          var iter = 0;
          var cont = comanda.socket_sospesi.length; //56

          console.log("ITER 2", iter, cont);
          comanda.socket_sospesi.forEach(function (testo_query) {
            comanda.sincro.query(testo_query, function () {
              iter++;

              if (iter === cont) {
                comanda.loading_tab_comanda = false;
              }
            });
          });
          comanda.loading_tab_comanda = false;
          comanda.socket_sospesi = [];
        } else {
          comanda.loading_tab_comanda = false;
        }

        callback(true);
      }));
    };
  }

  ;

  function comprimi_query(query_input) {
    if (typeof query_input === 'string') {
      var query_output = query_input.replace(/\n/gi, '')
      /*.replace(/\s\s+/g, ' ')*/
      ;
    } else {
      query_output = query_input;
    }

    return query_output;
  }

  this.query_cassa = function (query, timeout, callback) {
    console.trace("QUERY CASSA 1");
    var DBNameInterno = dbname;

    if (comanda.query_su_socket === false) {
      if (comanda.sincro_query_cassa_attivo !== true && comanda.array_dbcentrale_mancanti !== undefined && comanda.array_dbcentrale_mancanti[0] !== undefined && comanda.array_dbcentrale_mancanti[0] !== null || Date.now() / 1000 - comanda.last_time_query_cassa >= 5 || query !== undefined) {
        comanda.sincro_query_cassa_attivo = true;
        var testo_query = "";

        if (query !== undefined) {
          if (query.indexOf("settaggi_profili") !== -1 || query.indexOf("profilo_aziendale") !== -1) {
            DBNameInterno = dbnameSettaggi;
          }

          testo_query = query;
        } else {
          if (comanda.array_dbcentrale_mancanti[0] !== undefined && (comanda.array_dbcentrale_mancanti[0].indexOf("settaggi_profili") !== -1 || comanda.array_dbcentrale_mancanti[0].indexOf("profilo_aziendale") !== -1)) {
            DBNameInterno = dbnameSettaggi;
          }

          testo_query = comanda.array_dbcentrale_mancanti[0];
        }

        testo_query = comprimi_query(testo_query);
        console.log("DB CENTRALE SENDED", testo_query);
        var to = 10000;

        if (timeout !== undefined) {
          to = timeout;
        }

        $.ajax({
          type: "POST",
          async: true,
          url: comanda.url_server + 'classi_php/db_realtime_cassa.lib.php',
          data: {
            type: dbtype,
            db_name: DBNameInterno,
            query: testo_query,
            key: comanda.k1 + comanda.k2 + comanda.k3 + comanda.k4
          },
          dataType: "json",
          timeout: to,
          beforeSend: function beforeSend() {
            console.log("QUERY CASSA BEFORESEND", testo_query);
            comanda.last_time_query_cassa = Date.now() / 1000;
            comanda.sincro_query_cassa_attivo = true;
          },
          success: function success(dati) {
            console.log("QUERY CASSA DEBUG", dati);
            /*if (typeof (callback) === "function") {
             console.log("QUERY CASSA CALLBACK TRUE");
             
             callback(dati);
             }*/
            //Rimuove l'indice 0 per una lunghezza di indice, senza sostituire niente
            //CASISTICA IN CUI E' UNA QUERY CICLICIZZATA

            if (query === undefined && dati === true) {
              console.trace("QUERY CASSA 2");
              console.log("QUERY CASSA SUCCESS TRUE", testo_query,
              /*comanda.array_dbcentrale_mancanti[0], */
              comanda.array_dbcentrale_mancanti);
              comanda.array_dbcentrale_mancanti.shift();
              console.log("QUERY CASSA DELETED", comanda.array_dbcentrale_mancanti);
              comanda.last_time_query_cassa = Date.now() / 1000;
              $('.spia.db_centrale').css('background-color', 'green');
              comanda.sincro_query_cassa_attivo = false;
              comanda.sincro.query_cassa();

              if (typeof callback === "function") {
                console.log("QUERY CASSA CALLBACK TRUE");
                callback(dati);
              }
            } else if (dati === true) {
              console.trace("QUERY CASSA 3");
              console.log("QUERY CASSA (2) SUCCESS TRUE", testo_query, comanda.array_dbcentrale_mancanti[0], comanda.array_dbcentrale_mancanti);
              comanda.last_time_query_cassa = Date.now() / 1000;
              $('.spia.db_centrale').css('background-color', 'green');
              comanda.sincro_query_cassa_attivo = false;
              comanda.sincro.query_cassa();

              if (typeof callback === "function") {
                console.log("QUERY CASSA CALLBACK TRUE");
                callback(dati);
              }
            } else if (isJson(dati) == true && dati !== false) {
              console.trace("QUERY CASSA 4");
              console.log("QUERY CASSA CALLBACK TRUE 2", dati);
              comanda.last_time_query_cassa = Date.now() / 1000;
              $('.spia.db_centrale').css('background-color', 'green'); //console.trace();

              comanda.sincro_query_cassa_attivo = false;
              comanda.sincro.query_cassa();

              if (typeof callback === "function") {
                console.log("QUERY CASSA CALLBACK TRUE 2-F");
                callback(dati);
              }
            } else {
              console.trace("QUERY CASSA 5");

              if (typeof callback === "function") {
                callback(dati);
              }

              console.log("QUERY CASSA SUCCESS ERR", query);
              comanda.last_time_query_cassa = Date.now() / 1000;
              $('.spia.db_centrale').css('background-color', 'red');
              setTimeout(function () {
                //facendo così la condizione resta bloccata a true fino al prossimo giro
                //mettendo false fuori dal timeout ci sarebbero 3 secondi di gap con possibili overload
                comanda.sincro_query_cassa_attivo = false;
                comanda.sincro.query_cassa();
              }, 3000);
            }
          },
          error: function error(xhr, status, _error) {
            console.trace("QUERY CASSA ERR 1");

            if (typeof callback === "function") {
              console.log("QUERY CASSA CALLBACK FALSE");
              salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # DB ERROR # " + query.replace(/[\n\r]+/g, '') + " # " + new Error().stack.replace(/[\n\r]+/g, ''));
              callback(false);
            }

            console.log("QUERY CASSA ERROR", testo_query, comanda.array_dbcentrale_mancanti[0], xhr.responseText);
            comanda.last_time_query_cassa = Date.now() / 1000;
            $('.spia.db_centrale').css('background-color', 'red');
            setTimeout(function () {
              //facendo così la condizione resta bloccata a true fino al prossimo giro
              //mettendo false fuori dal timeout ci sarebbero 3 secondi di gap con possibili overload
              comanda.sincro_query_cassa_attivo = false;
              comanda.sincro.query_cassa();
            }, 3000);
          }
        });
      }
    }
  };

  this.query = function (string, callBack, comanda_su_oggetti) {
    if (string !== undefined) {
      if (true) {
        var risultato = alasql(string);
        callBack(risultato);
      } else if (typeof sqlitePlugin !== "undefined") {
        var risultato = new Array();
        db.transaction(function (tx, results) {
          tx.executeSql(string, [], function (tx, rs) {
            for (var i = 0; i < rs.rows.length; i++) {
              var row = rs.rows.item(i);
              risultato[i] = row;
            }

            callBack(risultato);
            /*db.close(function () {
             console.log('database is closed ok');
             }, function (error) {
             console.log('ERROR closing database');
             });*/
          }, errorHandler);

          function errorHandler(transaction, error) {
            console.log("DB ERROR : " + error.message + " -  " + string);
            salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # DB ERROR # " + string.replace(/[\n\r]+/g, '') + " # " + new Error().stack.replace(/[\n\r]+/g, ''));
          }
        });
      } else if (comanda_su_oggetti !== true && (string.toUpperCase().search(" FROM COMANDA ") !== -1 || string.toUpperCase().search("INSERT INTO COMANDA") !== -1 || string.toUpperCase().search("UPDATE COMANDA") !== -1 || string.toUpperCase().search("DELETE FROM COMANDA") !== -1 || string.toUpperCase().search("CREATE TABLE COMANDA") !== -1)) {
        if (string.search("(numero_conto='' OR numero_conto='undefined' OR numero_conto is null OR numero_conto='null' OR numero_conto='1' OR numero_conto=1)") !== -1 || string.search("select distinct numero_conto") !== -1) {} else {}

        alasql(comprimi_query(string), function (risultato) {
          callBack(risultato);
        }); //MOMENTANEAMENTE DISABILITATO

        /*$.ajax({
         type: "POST",
         async: true,
         url: 'http://localhost:' + comanda.porta_server_remoto + '/fast.intellinet/classi_php/query_locale.lib.php',
         data: {
         query: comprimi_query(string)
         },
         dataType: "json",
         beforeSend: function () {},
         success: function (risultato_json) {
         var risultato = new Array();
         
         for (var i = 0; i < risultato_json.length; i++) {
         var row = risultato_json[i];
         risultato[i] = row;
         }
         
         
         
         },
         error: function (errore) {
         console.log("ERRORE QUERY ASYNC: ", errore, comprimi_query(string));
         salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # DB ERROR # " + string.replace(/[\n\r]+/g, '') + " # " + new Error().stack.replace(/[\n\r]+/g, ''));
         }
         });*/
      } else {
        $.ajax({
          type: "POST",
          async: true,
          url: 'http://localhost:' + comanda.porta_server_remoto + '/fast.intellinet/classi_php/query_locale.lib.php',
          data: {
            query: comprimi_query(string)
          },
          dataType: "json",
          beforeSend: function beforeSend() {},
          success: function success(risultato_json) {
            var risultato = new Array();

            for (var i = 0; i < risultato_json.length; i++) {
              var row = risultato_json[i];
              risultato[i] = row;
            }

            callBack(risultato);
          },
          error: function error(errore) {
            console.log("ERRORE QUERY ASYNC: ", errore, comprimi_query(string));
            salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # DB ERROR # " + string.replace(/[\n\r]+/g, '') + " # " + new Error().stack.replace(/[\n\r]+/g, ''));
          }
        });
      }
    }
  };

  this.query_locale = function (string, callBack) {
    if (string !== undefined) {
      if (typeof sqlitePlugin !== "undefined") {
        //PROVVISORIO
        var risultato = new Array();
        window.MacAddress.getMacAddress(function (macAddress) {
          risultato.push({
            valore: macAddress
          });
          callBack(risultato);
        }, function (fail) {
          risultato.push({
            valore: "ND"
          });
          callBack(risultato);
        });
      } else {
        if (string.toUpperCase().search("COMANDA") !== -1) {
          if (string.search("(numero_conto='' OR numero_conto='undefined' OR numero_conto is null OR numero_conto='null' OR numero_conto='1' OR numero_conto=1)") !== -1 || string.search("select distinct numero_conto") !== -1) {} else {}
        }

        $.ajax({
          type: "POST",
          async: true,
          url: 'http://localhost:' + comanda.porta_server_remoto + '/fast.intellinet/classi_php/query_settaggi_locali.lib.php',
          data: {
            query: comprimi_query(string)
          },
          dataType: "json",
          beforeSend: function beforeSend() {},
          success: function success(risultato_json) {
            var risultato = new Array();

            for (var i = 0; i < risultato_json.length; i++) {
              var row = risultato_json[i];
              risultato[i] = row;
            }

            callBack(risultato);
          },
          error: function error(errore) {
            console.log("ERRORE QUERY LOCALE: ", errore, comprimi_query(string));
            salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # DB ERROR # " + string.replace(/[\n\r]+/g, '') + " # " + new Error().stack.replace(/[\n\r]+/g, ''));
          }
        });
      }
    }
  };

  this.query_locale_sincrona = function _callee(query) {
    return regeneratorRuntime.async(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            return _context.abrupt("return", new Promise(function (resolve, reject) {
              comanda.sincro.query_locale(query, function (res) {
                resolve(res);
              });
            }));

          case 1:
          case "end":
            return _context.stop();
        }
      }
    });
  };

  this.query_centrale = function (string, callBack) {
    if (string !== undefined) {
      if (typeof sqlitePlugin !== "undefined") {
        //PROVVISORIO
        var risultato = new Array();
        window.MacAddress.getMacAddress(function (macAddress) {
          risultato.push({
            valore: macAddress
          });
          callBack(risultato);
        }, function (fail) {
          risultato.push({
            valore: "ND"
          });
          callBack(risultato);
        });
      } else {
        if (string.toUpperCase().search("COMANDA") !== -1) {
          if (string.search("(numero_conto='' OR numero_conto='undefined' OR numero_conto is null OR numero_conto='null' OR numero_conto='1' OR numero_conto=1)") !== -1 || string.search("select distinct numero_conto") !== -1) {} else {}
        }

        $.ajax({
          type: "POST",
          async: true,
          url: comanda.url_server + 'classi_php/query_centrale.lib.php',
          data: {
            query: comprimi_query(string)
          },
          dataType: "json",
          beforeSend: function beforeSend() {},
          success: function success(risultato_json) {
            var risultato = new Array();

            for (var i = 0; i < risultato_json.length; i++) {
              var row = risultato_json[i];
              risultato[i] = row;
            }

            callBack(risultato);
          },
          error: function error(errore) {
            console.log("ERRORE QUERY LOCALE: ", errore, comprimi_query(string));
            salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # DB ERROR # " + string.replace(/[\n\r]+/g, '') + " # " + new Error().stack.replace(/[\n\r]+/g, ''));
          }
        });
      }
    }
  };

  var Array_Query_Sync = new Array();

  function funzione_query_sincrona() {
    try {
      if (Array_Query_Sync.length > 0) {
        var array_temp = Array_Query_Sync[0];
        Array_Query_Sync.shift();
        comanda.sincro.query(array_temp.string, function (r) {
          console.log("***SOCKET*** - QUERY ESEGUITA", array_temp.string);
          array_temp.callBack(r);

          if (Array_Query_Sync.length > 0) {
            //SI RIPETE AD OGNI CALLBACK SE CI SONO ALTRE QUERY
            console.log("***SOCKET*** - RICORSIONE - (" + Array_Query_Sync.length + ") QUERY IN ATTESA");
            funzione_query_sincrona();
          } else {
            //ALTRIMENTI ATTIVA UN TIMEOUT E RIPROVA TRA 3 SECONDI
            //console.log("***SOCKET*** - ATTESA QUERY 250MS");
            setTimeout(funzione_query_sincrona, 1000);
          }
        });
      } else {
        //console.log("***SOCKET*** ATTESA QUERY 250 MS");
        setTimeout(funzione_query_sincrona, 1000);
      }
    } catch (e) {
      salva_log_sempreattivo("FUNZIONE_QUERY_ASINCRONA " + new Date());

      if (e.message !== undefined) {
        salva_log_sempreattivo(e.message);
      }

      if (array_temp.string !== undefined) {
        salva_log_sempreattivo(array_temp.string);
      }

      funzione_query_sincrona();
    }
  }

  console.log("***SOCKET*** - PARTITO");
  funzione_query_sincrona();

  this.query_sync = function (string, callBack) {
    //Condizione modificata l'11 marzo 2021 perchè anche per l'asporto dev'esserci la sincronia. Prima era solo pizzeria_asporto !== true
    if (comanda.pizzeria_asporto === undefined || comanda.pizzeria_asporto === true || comanda.pizzeria_asporto === false || comanda.mobile === true) {
      if (string !== undefined && typeof string === "string" && string.indexOf("inizio download comanda") === -1) {
        Array_Query_Sync.push({
          string: string,
          callBack: callBack
        });
      }
    } else //TOLTO IL 29/08/2017 PER EVITARE IL FATTO CHE NEL CELLULARE FACCIA CASINO
      if (string !== undefined && typeof string === "string" && string.indexOf("inizio download comanda") === -1) {
        if (string.toUpperCase().search("COMANDA") !== -1) {
          if (string.search("(numero_conto='' OR numero_conto='undefined' OR numero_conto is null OR numero_conto='null' OR numero_conto='1' OR numero_conto=1)") !== -1 || string.search("select distinct numero_conto") !== -1) {} else {}
        }

        $.ajax({
          type: "POST",
          async: false,
          url: 'http://localhost:' + comanda.porta_server_remoto + '/fast.intellinet/classi_php/query_locale.lib.php',
          data: {
            query: comprimi_query(string)
          },
          dataType: "json",
          beforeSend: function beforeSend() {},
          success: function success(risultato_json) {
            var risultato = new Array();

            for (var i = 0; i < risultato_json.length; i++) {
              var row = risultato_json[i];
              risultato[i] = row;
            }

            callBack(risultato);
          },
          error: function error(errore) {
            console.log("ERRORE QUERY SYNC: ", errore, comprimi_query(string));
            salva_log(comanda.id_tablet + " # " + new Date().format("HH:MM:ss") + ":" + new Date().getMilliseconds() + " # DB ERROR # " + string.replace(/[\n\r]+/g, '') + " # " + new Error().stack.replace(/[\n\r]+/g, ''));
          }
        });
      }
  }; //E' una query mista tra MYSQL (PER CLIENTI GROSSI) E SQLITE


  this.query_incassi = function (string, timeout, callBack) {
    if (string !== undefined) {
      if (string.toUpperCase().search("COMANDA") !== -1) {
        if (string.search("(numero_conto='' OR numero_conto='undefined' OR numero_conto is null OR numero_conto='null' OR numero_conto='1' OR numero_conto=1)") !== -1 || string.search("select distinct numero_conto") !== -1) {} else {}
      }

      var db = "provaINTELLINET";

      if (comanda.server_replica_abilitato === true) {
        if (comanda.db_server_replica !== undefined && comanda.db_server_replica !== "") {
          db = comanda.db_server_replica;
        }
      }

      var url = comanda.url_server + 'classi_php/query_mysql.lib.php';

      if (comanda.url_server_mysql !== undefined && comanda.url_server_mysql !== "") {
        url = comanda.url_server_mysql + 'classi_php/query_mysql.lib.php';
      }

      $.ajax({
        type: "POST",
        async: true,
        url: url,
        data: {
          db: db,
          query: comprimi_query(string)
        },
        dataType: "json",
        beforeSend: function beforeSend() {},
        success: function success(risultato_json) {
          beep();
          var risultato = new Array();

          for (var i = 0; i < risultato_json.length; i++) {
            var row = risultato_json[i];
            risultato[i] = row;
          }

          callBack(risultato);
        },
        error: function error(errore) {
          bootbox.alert("Problema su query statistica: " + errore.responseText);
          console.log("ERRORE QUERY INCASSATI: ", errore, comprimi_query(string));
        }
      });
    }
  };

  this.upload_mysql_temp = function _callee2() {
    return regeneratorRuntime.async(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            return _context2.abrupt("return", new Promise(function (resolve) {
              $.ajax({
                type: "POST",
                async: true,
                data: {
                  secondi_attesa_replica_mysql: comanda.secondi_attesa_replica_mysql
                },
                url: comanda.url_server + 'classi_php/trasferimentosqlitemysql_temp.lib.php',
                dataType: "json",
                beforeSend: function beforeSend() {},
                success: function success(rar) {
                  if (rar === true) {
                    resolve(true);
                  } else {
                    resolve(false);
                  }
                },
                error: function error() {
                  resolve(false);
                }
              });
            }));

          case 1:
          case "end":
            return _context2.stop();
        }
      }
    });
  };

  this.query_incassi_async = function _callee3(string) {
    return regeneratorRuntime.async(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            return _context3.abrupt("return", new Promise(function (resolve) {
              if (string !== undefined) {
                if (string.toUpperCase().search("COMANDA") !== -1) {
                  if (string.search("(numero_conto='' OR numero_conto='undefined' OR numero_conto is null OR numero_conto='null' OR numero_conto='1' OR numero_conto=1)") !== -1 || string.search("select distinct numero_conto") !== -1) {} else {}
                }

                var db = "provaINTELLINET";

                if (comanda.server_replica_abilitato === true) {
                  if (comanda.db_server_replica !== undefined && comanda.db_server_replica !== "") {
                    db = comanda.db_server_replica;
                  }
                }

                var url = comanda.url_server + 'classi_php/query_mysql.lib.php';

                if (comanda.url_server_mysql !== undefined && comanda.url_server_mysql !== "") {
                  url = comanda.url_server_mysql + 'classi_php/query_mysql.lib.php';
                }

                $.ajax({
                  type: "POST",
                  async: true,
                  url: url,
                  data: {
                    db: db,
                    query: comprimi_query(string)
                  },
                  dataType: "json",
                  beforeSend: function beforeSend() {},
                  success: function success(risultato_json) {
                    beep();
                    var risultato = new Array();

                    for (var i = 0; i < risultato_json.length; i++) {
                      var row = risultato_json[i];
                      risultato[i] = row;
                    }

                    resolve(risultato);
                  },
                  error: function error(errore) {
                    bootbox.alert("Problema su query statistica: " + errore.responseText);
                    console.log("ERRORE QUERY INCASSATI: ", errore, comprimi_query(string));
                    resolve(false);
                  }
                });
              }
            }));

          case 1:
          case "end":
            return _context3.stop();
        }
      }
    });
  }; //E' una query mista tra MYSQL (PER CLIENTI GROSSI) E SQLITE


  this.query_statistiche = function (string, timeout, callBack, tipo) {
    if (string !== undefined) {
      if (string.toUpperCase().search("COMANDA") !== -1) {
        if (string.search("(numero_conto='' OR numero_conto='undefined' OR numero_conto is null OR numero_conto='null' OR numero_conto='1' OR numero_conto=1)") !== -1 || string.search("select distinct numero_conto") !== -1) {} else {}
      }

      var db = "provaINTELLINET";

      if (comanda.server_replica_abilitato === true) {
        if (comanda.db_server_replica !== undefined && comanda.db_server_replica !== "") {
          db = comanda.db_server_replica;
        }
      }

      var url = comanda.url_server + 'classi_php/query_statistiche.lib.php';

      if (comanda.url_server_mysql !== undefined && comanda.url_server_mysql !== "") {
        url = comanda.url_server_mysql + 'classi_php/query_statistiche.lib.php';
      }

      $.ajax({
        type: "POST",
        async: true,
        url: url,
        data: {
          db: db,
          tipo: tipo,
          query: comprimi_query(string)
        },
        dataType: "json",
        beforeSend: function beforeSend() {},
        success: function success(risultato_json) {
          beep();
          var risultato = new Array();

          for (var i = 0; i < risultato_json.length; i++) {
            var row = risultato_json[i];
            risultato[i] = row;
          }

          callBack(risultato);
        },
        error: function error(errore) {
          bootbox.alert("Problema su query statistica: " + errore.responseText);
          console.log("ERRORE QUERY INCASSATI: ", errore, comprimi_query(string));
        }
      });
    }
  };

  this.controllo_licenza = function (callBack) {
    /*return new Promise((resolve, reject) => {*/
    if (comanda.mobile === true) {
      callBack("MOBILE");
    } else {
      $.ajax({
        type: "POST",
        async: true,
        url: 'https://checchin.eu/controllo_licenza/scarica_licenza.php',
        data: {
          numero_licenza: comanda.numero_licenza_cliente
        },
        dataType: "json",
        beforeSend: function beforeSend() {},
        success: function success(risultato_json) {
          if (risultato_json.length > 0) {
            callBack(risultato_json[0].data_scadenza);
          } else {
            callBack("");
          }
        },
        error: function error() {
          callBack("");
        }
      });
    }
    /*});*/

  }; //E' una query mista tra MYSQL (PER CLIENTI GROSSI) E SQLITE


  this.query_movimenti = function (string, timeout, callBack) {
    if (string !== undefined) {
      if (string.toUpperCase().search("COMANDA") !== -1) {
        if (string.search("(numero_conto='' OR numero_conto='undefined' OR numero_conto is null OR numero_conto='null' OR numero_conto='1' OR numero_conto=1)") !== -1 || string.search("select distinct numero_conto") !== -1) {} else {}
      }

      $.ajax({
        type: "POST",
        async: true,
        url: comanda.url_server + 'classi_php/query_movimenti.lib.php',
        data: {
          query: comprimi_query(string)
        },
        dataType: "json",
        beforeSend: function beforeSend() {},
        success: function success(risultato_json) {
          beep();
          var risultato = new Array();

          for (var i = 0; i < risultato_json.length; i++) {
            var row = risultato_json[i];
            risultato[i] = row;
          }

          callBack(risultato);
        },
        error: function error(errore) {
          console.log("ERRORE QUERY MOVIMENTI: ", errore, comprimi_query(string));
        }
      });
    }
  };
};

function download_copia_centrale() {
  window.open(comanda.url_server + 'DATABASE_CLIENTE.sqlite');
}

function download_copia_locale() {
  comanda.sincro.backup_db_locale(function (cb_db_locale) {
    alert("Database Locale salvato correttamente nel percorso predefinito (C:/TAVOLI/FAST.INTELLINET/BACKUP_LOCALI");
  });
}