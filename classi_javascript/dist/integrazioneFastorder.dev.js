"use strict";

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global alasql, comanda, bootbox */
function FastorderApiClient() {
  this.export_products_fastorder = function () {
    var url_fastorder = $('#form_settaggi input[name="url_fastorder"]:visible').val();
    var categorie_come_intellinet = $('#form_settaggi input[name="categorie_come_intellinet"]:visible').is(':checked');

    if (categorie_come_intellinet === true) {
      this.export_categories_fastorder();
    }

    if (comanda.pizzeria_asporto === true) {
      this.export_settings_fastorder();
    }

    if (checkEmptyVariable(url_fastorder, "string") === true) {
      var datiTabellaGrezzi = JSON.parse(JSON.stringify(alasql.tables.prodotti.data));
      datiTabellaGrezzi.filter(function (v) {
        if (comanda.nome_categoria[v.categoria] !== undefined && v.categoria !== "XXX" && v.esportazione_fastorder_abilitata === "true") {
          return v;
        }
      }).forEach(function (v) {
        Object.keys(v).forEach(function (index) {
          if (v[index] === null || v[index] === "NaN" || v[index] === "undefined" || v[index] === "NULL" || v[index] === "null") {
            v[index] = "";
          }
        });

        if (categorie_come_intellinet === true) {
          v.cat_varianti = v.cat_varianti.split("/")[0];
        } else {
          if (comanda.nome_categoria[v.categoria].substr(0, 2) === "V.") {
            v.cat_varianti = "";
          } else {
            v.categoria = "999";
            v.cat_varianti = v.cat_varianti.split("/")[0];
          }
        }

        v.descrizione = v.descrizione_fastorder;
        v.ricetta = v.ricetta_fastorder;
      });
      var datiTabella = JSON.stringify(datiTabellaGrezzi);
      var modifica_prezzi_fastorder = $('#form_settaggi input[name="modifica_prezzi_fastorder"]:visible').is(':checked');
      $.ajax({
        dataType: 'json',
        type: 'POST',
        url: url_fastorder + "classi_php/importTable.php",
        timeout: 3000,
        data: {
          'modifica_prezzi_fastorder': modifica_prezzi_fastorder,
          'nome_tabella': 'prodotti',
          'dati_tabella': datiTabella
        },
        success: function success(dati) {
          /*se chiami l'api come json è true, altrimenti "true"*/
          if (dati === true) {
            bootbox.alert("Esportazione Completata");
          } else {
            bootbox.alert("Esportazione Errata - 1");
          }
        },
        error: function error() {
          bootbox.alert("Esportazione Errata - 2");
        }
      });
    }
  };

  this.export_categories_fastorder = function () {
    var url_fastorder = $('#form_settaggi input[name="url_fastorder"]:visible').val();

    if (checkEmptyVariable(url_fastorder, "string") === true) {
      var datiTabellaGrezzi = JSON.parse(JSON.stringify(alasql.tables.categorie.data));
      datiTabellaGrezzi.filter(function (v) {
        /*if (comanda.nome_categoria[v.categoria] !== undefined && v.categoria !== "XXX") {
         return v;
         }*/
      }).forEach(function (v) {
        Object.keys(v).forEach(function (index) {
          if (v[index] === null || v[index] === "NaN" || v[index] === "undefined" || v[index] === "NULL" || v[index] === "null") {
            v[index] = "";
          }
        });
        /*if (comanda.nome_categoria[v.categoria].substr(0, 2) === "V.") {
         v.cat_varianti = "";
         } else {
         v.categoria = "999";
         v.cat_varianti = v.cat_varianti.split("/")[0];
         }*/
      });
      var datiTabella = JSON.stringify(datiTabellaGrezzi);
      $.ajax({
        dataType: 'json',
        type: 'POST',
        url: url_fastorder + "classi_php/importTableCategorie.php",
        timeout: 3000,
        data: {
          'nome_tabella': 'categorie',
          'dati_tabella': datiTabella
        },
        success: function success(dati) {
          /*se chiami l'api come json è true, altrimenti "true"*/
          if (dati === true) {
            bootbox.alert("Esportazione Completata");
          } else {
            bootbox.alert("Esportazione Errata - 1");
          }
        },
        error: function error() {
          bootbox.alert("Esportazione Errata - 2");
        }
      });
    }
  };

  this.export_settings_fastorder = function () {
    var url_fastorder = $('#form_settaggi input[name="url_fastorder"]:visible').val();

    if (checkEmptyVariable(url_fastorder, "string") === true) {
      var datiTabellaGrezzi = JSON.parse(JSON.stringify(alasql.tables.impostazioni_fiscali.data));
      datiTabellaGrezzi.filter(function (v) {}).forEach(function (v) {
        Object.keys(v).forEach(function (index) {
          if (v[index] === null || v[index] === "NaN" || v[index] === "undefined" || v[index] === "NULL" || v[index] === "null") {
            v[index] = "";
          }
        });
      });
      var datiTabella = JSON.stringify(datiTabellaGrezzi);
      $.ajax({
        dataType: 'json',
        type: 'POST',
        url: url_fastorder + "classi_php/importTableSettaggiProfili.php",
        timeout: 3000,
        data: {
          'nome_tabella': 'impostazioni_fiscali',
          'dati_tabella': datiTabella
        },
        success: function success(dati) {
          /*se chiami l'api come json è true, altrimenti "true"*/
          if (dati === true) {
            bootbox.alert("Esportazione Completata");
          } else {
            bootbox.alert("Esportazione Errata - 1");
          }
        },
        error: function error() {
          bootbox.alert("Esportazione Errata - 2");
        }
      });
    }
  };

  this.api_request = function _callee(objJsonStr) {
    return regeneratorRuntime.async(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            return _context.abrupt("return", new Promise(function (resolve) {
              if (checkEmptyVariable(comanda.url_fastorder, "string") === true) {
                var objJsonB64 = btoa(unescape(encodeURIComponent(JSON.stringify(objJsonStr))));
                var dati_da_inviare = {
                  dati: objJsonB64
                };

                if (objJsonStr === undefined) {
                  dati_da_inviare = null;
                }

                $.post(comanda.url_fastorder + "classi_php/api_fastorder.php", dati_da_inviare, function (data, status) {
                  resolve(JSON.parse(data));
                });
              }
            }));

          case 1:
          case "end":
            return _context.stop();
        }
      }
    });
  };

  this.ricevi_ordini_inviati = function _callee2() {
    var oggetto_id_ordini_ricevuti, ordini_ricevuti, counter, id_cliente, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _loop, _iterator, _step, testo_query, key;

    return regeneratorRuntime.async(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            oggetto_id_ordini_ricevuti = new Object();
            _context2.next = 3;
            return regeneratorRuntime.awrap(fastorder.api_request({
              tipo: 'ricevi_ordini_inviati'
            }));

          case 3:
            ordini_ricevuti = _context2.sent;

            if (!(ordini_ricevuti !== null && ordini_ricevuti.length > 0)) {
              _context2.next = 37;
              break;
            }

            counter = 0;
            id_cliente = "";
            _iteratorNormalCompletion = true;
            _didIteratorError = false;
            _iteratorError = undefined;
            _context2.prev = 10;

            _loop = function _loop() {
              var ordine = _step.value;
              Object.keys(ordine).forEach(function (index) {
                if (ordine[index] === null || ordine[index] === "NaN" || ordine[index] === "undefined" || ordine[index] === "NULL" || ordine[index] === "null") {
                  ordine[index] = "";
                }
              });

              if (oggetto_id_ordini_ricevuti[ordine.id] === undefined) {
                oggetto_id_ordini_ricevuti[ordine.id] = ordine.id;
              }

              ordine.prezzo_vero = ordine.prezzo_un;

              if (comanda.pizzeria_asporto === true) {
                ordine.ora_consegna = ordine.ora_consegna.slice(-5);
                ordine.orario_preparazione = ordine.ora_consegna;

                if (comanda.orario_preparazione === "1") {
                  if (ordine.ora_consegna.length === 5) {
                    var a = new Date("01 Jan 2000 " + ordine.ora_consegna + ":00");

                    if (ordine.tipo_consegna === "DOMICILIO") {
                      a.setMinutes(a.getMinutes() + parseInt("-" + comanda.tempo_preparazione_default) + parseInt("-" + comanda.tempo_preparazione_default_domicilio));
                    } else {
                      a.setMinutes(a.getMinutes() + parseInt("-" + comanda.tempo_preparazione_default));
                    }

                    var b = aggZero(a.getHours(), 2) + ":" + aggZero(a.getMinutes(), 2);
                    ordine.orario_preparazione = b;
                  }
                }

                ordine.stato_record = "FORNO";

                if (ordine.ntav_comanda === "TAKE AWAY") {
                  ordine.ntav_comanda = "ASPORTO99999" + ordine.id;
                  ordine.cod_cliente = (parseInt(ordine.cod_cliente) + 3000000).toString();
                  var nome_cliente = alasql("select * from clienti where id=" + ordine.cod_cliente + ";");

                  if (nome_cliente.length > 0) {
                    ordine.nome_comanda = nome_cliente[0].nome + " " + nome_cliente[0].cognome;
                    ordine.rag_soc_cliente = ordine.cod_cliente;
                  } else {
                    ordine.nome_comanda = ordine.rag_soc_cliente.split(",")[0];
                  }
                } else {
                  ordine.cod_cliente = "";
                  ordine.ntav_comanda = "ASPORTO TAV " + ordine.ntav_comanda;
                  ordine.nome_comanda = ordine.ntav_comanda;

                  if (counter === 0) {
                    testo_query = "select id from clienti WHERE id<3000000 order by cast(id as int) desc limit 1;";
                    var risultato = alasql(testo_query);

                    if (risultato[0] !== undefined && risultato[0].id !== undefined) {
                      id_cliente = parseInt(risultato[0].id) + 1;
                    }

                    var query_inserimento_cliente_temp = "INSERT INTO clienti (id,ragione_sociale,nome,cognome,tipo_cliente,stato_sincro,formato_trasmissione,regime_fiscale) VALUES (" + id_cliente + ",'','" + ordine.ntav_comanda + "','','ap_privato','0','false','RF01');";
                    alasql(query_inserimento_cliente_temp);
                    comanda.sock.send({
                      tipo: "aggiornamento_tavolo",
                      operatore: comanda.operatore,
                      query: query_inserimento_cliente_temp,
                      terminale: comanda.terminale,
                      ip: comanda.ip_address
                    });
                  }

                  ordine.rag_soc_cliente = id_cliente;
                  ordine.tipo_consegna = "PIZZERIA";
                }

                ordine.progr_gg_uni = ordine.ntav_comanda;
                ordine.tipologia_fattorino = "0";
              } else {
                if (ordine.ntav_comanda === "TAKE AWAY") {
                  ordine.ntav_comanda = "ASPORTO99999" + ordine.id;
                  ordine.cod_cliente = (parseInt(ordine.cod_cliente) + 3000000).toString();

                  var _nome_cliente = alasql("select * from clienti where id=" + ordine.cod_cliente + ";");

                  if (_nome_cliente.length > 0) {
                    ordine.nome_comanda = _nome_cliente[0].nome + " " + _nome_cliente[0].cognome;
                    ordine.rag_soc_cliente = ordine.cod_cliente;
                  } else {
                    ordine.nome_comanda = ordine.rag_soc_cliente.split(",")[0];
                  }
                } else {
                  if (counter === 0) {
                    /*var testo_query = "select id from clienti WHERE id<3000000 order by cast(id as int) desc limit 1;";
                     let risultato = alasql(testo_query);
                     
                     
                     if (risultato[0] !== undefined && risultato[0].id !== undefined) {
                     id_cliente = parseInt(risultato[0].id) + 1;
                     }
                     
                     let query_inserimento_cliente_temp = "INSERT INTO clienti (id,ragione_sociale,nome,cognome,tipo_cliente,stato_sincro,formato_trasmissione,regime_fiscale) VALUES (" + id_cliente + ",'','" + ordine.ntav_comanda + "','','ap_privato','0','false','RF01');";
                     alasql(query_inserimento_cliente_temp);
                     comanda.sock.send({tipo: "aggiornamento_tavolo", operatore: comanda.operatore, query: query_inserimento_cliente_temp, terminale: comanda.terminale, ip: comanda.ip_address});
                     */
                    var query_tavolo_rosso = "UPDATE tavoli SET colore='green' WHERE numero='" + ordine.ntav_comanda + "';";
                    alasql(query_tavolo_rosso);
                    comanda.sock.send({
                      tipo: "aggiornamento_tavoli",
                      operatore: comanda.operatore,
                      query: query_tavolo_rosso,
                      terminale: comanda.terminale,
                      ip: comanda.ip_address
                    });
                    comanda.funzionidb.conto_attivo();
                    disegna_tavoli_salvati();
                  }
                }
                /*ordine.rag_soc_cliente = id_cliente;
                 ordine.tipo_consegna = "PIZZERIA";*/


                ordine.stato_record = "ATTIVO";
              }

              if (ordine.contiene_variante === "S") {
                ordine.contiene_variante = "1";
              }

              ordine.progr_gg_uni = ordine.ntav_comanda;
              ordine.tipologia_fattorino = "0";
              ordine.numero_servizio = comanda.nome_servizio;
              ordine.data_servizio = comanda.data_servizio;
              ordine.perc_iva = comanda.percentuale_iva_takeaway;
              ordine.reparto = comanda.reparto_beni_default;
              ordine.stampata_sn = "N";
              ordine.data_comanda = new Date().format("yyyy-mm-dd"); //BISOGNA METTERE LA DATA COMANDA IN FORMATO 2021-12-23 ad esempio

              var query_insert = "INSERT INTO comanda (" + Object.keys(ordine).join() + ") VALUES (" + Object.values(ordine).map(function (v) {
                return "'" + v + "'";
              }).join() + ");";
              alasql(query_insert);
              comanda.sock.send({
                tipo: "aggiornamento_tavolo",
                operatore: comanda.operatore,
                query: query_insert,
                terminale: comanda.terminale,
                ip: comanda.ip_address
              });
              counter++;
            };

            for (_iterator = ordini_ricevuti[Symbol.iterator](); !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
              _loop();
            }

            _context2.next = 19;
            break;

          case 15:
            _context2.prev = 15;
            _context2.t0 = _context2["catch"](10);
            _didIteratorError = true;
            _iteratorError = _context2.t0;

          case 19:
            _context2.prev = 19;
            _context2.prev = 20;

            if (!_iteratorNormalCompletion && _iterator["return"] != null) {
              _iterator["return"]();
            }

          case 22:
            _context2.prev = 22;

            if (!_didIteratorError) {
              _context2.next = 25;
              break;
            }

            throw _iteratorError;

          case 25:
            return _context2.finish(22);

          case 26:
            return _context2.finish(19);

          case 27:
            comanda.sincro.query_cassa();
            _context2.t1 = regeneratorRuntime.keys(oggetto_id_ordini_ricevuti);

          case 29:
            if ((_context2.t2 = _context2.t1()).done) {
              _context2.next = 35;
              break;
            }

            key = _context2.t2.value;
            _context2.next = 33;
            return regeneratorRuntime.awrap(fastorder.api_request({
              tipo: 'segna_ordine_stampato',
              id: key
            }));

          case 33:
            _context2.next = 29;
            break;

          case 35:
            righe_forno();
            comanda.sincro.query_cassa();

          case 37:
          case "end":
            return _context2.stop();
        }
      }
    }, null, null, [[10, 15, 19, 27], [20,, 22, 26]]);
  };

  this.ricevi_clienti_aggiornati = function _callee3() {
    var oggetto_clienti_ricevuti, ordini_ricevuti, _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _loop2, _iterator2, _step2, key;

    return regeneratorRuntime.async(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            oggetto_clienti_ricevuti = new Object();
            _context3.next = 3;
            return regeneratorRuntime.awrap(fastorder.api_request({
              tipo: 'ricevi_clienti_aggiornati'
            }));

          case 3:
            ordini_ricevuti = _context3.sent;

            if (!(ordini_ricevuti.length > 0)) {
              _context3.next = 33;
              break;
            }

            _iteratorNormalCompletion2 = true;
            _didIteratorError2 = false;
            _iteratorError2 = undefined;
            _context3.prev = 8;

            _loop2 = function _loop2() {
              var cliente = _step2.value;
              Object.keys(cliente).forEach(function (index) {
                if (cliente[index] === null || cliente[index] === "NaN" || cliente[index] === "undefined" || cliente[index] === "NULL" || cliente[index] === "null") {
                  cliente[index] = "";
                }

                if (index === "partita_iva" || index === "codice_fiscale" || index === "nome" || index === "cognome" || index === "ragione_sociale" || index === "indirizzo" || index === "citta" || index === "comune" || index === "numero" || index === "note") {
                  cliente[index] = cliente[index].toUpperCase();
                }
              });

              if (oggetto_clienti_ricevuti[cliente.id] === undefined) {
                oggetto_clienti_ricevuti[cliente.id] = cliente.id;
              }

              cliente.id = (parseInt(cliente.id) + 3000000).toString();
              /*cliente.stato_sincro = "1";*/

              var query_select = "SELECT * FROM clienti WHERE id=" + cliente.id + ";";
              var query_exec = "";
              var elenco_clienti_compatibili = alasql(query_select);

              if (elenco_clienti_compatibili.length === 0) {
                query_exec = "INSERT INTO clienti (" + Object.keys(cliente).join() + ") VALUES (" + Object.entries(cliente).map(function (v) {
                  if (v[0] === "id") {
                    return v[1];
                  } else {
                    if (v[1] === null || v[1] === "NaN" || v[1] === "undefined" || v[1] === "null" || v[1] === "NULL" || v[1] === "UNDEFINED") {
                      return "''";
                    } else {
                      return "'" + v[1] + "'";
                    }
                  }
                }).join() + ");";
                alasql(query_exec);
              } else {
                var id = cliente.id;
                delete cliente.id;
                query_exec = "UPDATE clienti SET " + Object.entries(cliente).map(function (v) {
                  return v[0] + "='" + v[1] + "'";
                }).join() + " WHERE id=" + id + ";";
                alasql(query_exec);
              }

              comanda.sock.send({
                tipo: "aggiornamento_tavolo",
                operatore: comanda.operatore,
                query: query_exec,
                terminale: comanda.terminale,
                ip: comanda.ip_address
              });
            };

            for (_iterator2 = ordini_ricevuti[Symbol.iterator](); !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
              _loop2();
            }

            _context3.next = 17;
            break;

          case 13:
            _context3.prev = 13;
            _context3.t0 = _context3["catch"](8);
            _didIteratorError2 = true;
            _iteratorError2 = _context3.t0;

          case 17:
            _context3.prev = 17;
            _context3.prev = 18;

            if (!_iteratorNormalCompletion2 && _iterator2["return"] != null) {
              _iterator2["return"]();
            }

          case 20:
            _context3.prev = 20;

            if (!_didIteratorError2) {
              _context3.next = 23;
              break;
            }

            throw _iteratorError2;

          case 23:
            return _context3.finish(20);

          case 24:
            return _context3.finish(17);

          case 25:
            _context3.t1 = regeneratorRuntime.keys(oggetto_clienti_ricevuti);

          case 26:
            if ((_context3.t2 = _context3.t1()).done) {
              _context3.next = 32;
              break;
            }

            key = _context3.t2.value;
            _context3.next = 30;
            return regeneratorRuntime.awrap(fastorder.api_request({
              tipo: 'segna_cliente_sincronizzato',
              id: key
            }));

          case 30:
            _context3.next = 26;
            break;

          case 32:
            /*righe_forno();*/
            comanda.sincro.query_cassa();

          case 33:
          case "end":
            return _context3.stop();
        }
      }
    }, null, null, [[8, 13, 17, 25], [18,, 20, 24]]);
  };

  this.intervallo = function () {
    fastorder.ricevi_clienti_aggiornati();
    fastorder.ricevi_ordini_inviati();
  };
  /* ogni 20 secondi si scarica i nuovi dati */


  setInterval(this.intervallo, 5000);

  this.invia_ordini_forno = function _callee4() {
    var testo_query, risultato, _iteratorNormalCompletion3, _didIteratorError3, _iteratorError3, _iterator3, _step3, dati_articolo;

    return regeneratorRuntime.async(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.next = 2;
            return regeneratorRuntime.awrap(fastorder.api_request({
              tipo: 'resetta_stato_forno_locale'
            }));

          case 2:
            /*desc_art,portata,quantita,spazio_forno,tipo_ricevuta,stampata_sn,ora_consegna,orario_preparazione,portata,id,stato_record,tipo_consegna*/
            testo_query = 'select *  from comanda where data_servizio="' + comanda.data_servizio + '" and (stato_record="FORNO" or stato_record="ATTIVO") and tipo_consegna!="NIENTE" and tipo_consegna!="";';
            risultato = alasql(testo_query);
            _iteratorNormalCompletion3 = true;
            _didIteratorError3 = false;
            _iteratorError3 = undefined;
            _context4.prev = 7;
            _iterator3 = risultato[Symbol.iterator]();

          case 9:
            if (_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done) {
              _context4.next = 19;
              break;
            }

            dati_articolo = _step3.value;
            dati_articolo.tipo = 'invia_articolo';
            dati_articolo.id = "SERVER_LOCALE";
            dati_articolo.stato_record = "STAMPATO";
            _context4.next = 16;
            return regeneratorRuntime.awrap(fastorder.api_request(dati_articolo));

          case 16:
            _iteratorNormalCompletion3 = true;
            _context4.next = 9;
            break;

          case 19:
            _context4.next = 25;
            break;

          case 21:
            _context4.prev = 21;
            _context4.t0 = _context4["catch"](7);
            _didIteratorError3 = true;
            _iteratorError3 = _context4.t0;

          case 25:
            _context4.prev = 25;
            _context4.prev = 26;

            if (!_iteratorNormalCompletion3 && _iterator3["return"] != null) {
              _iterator3["return"]();
            }

          case 28:
            _context4.prev = 28;

            if (!_didIteratorError3) {
              _context4.next = 31;
              break;
            }

            throw _iteratorError3;

          case 31:
            return _context4.finish(28);

          case 32:
            return _context4.finish(25);

          case 33:
          case "end":
            return _context4.stop();
        }
      }
    }, null, null, [[7, 21, 25, 33], [26,, 28, 32]]);
  };
}

var fastorder = new FastorderApiClient();