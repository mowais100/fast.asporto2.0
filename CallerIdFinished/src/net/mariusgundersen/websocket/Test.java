package net.mariusgundersen.websocket;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.mariusgundersen.websocket.WebSocketServer.Listener;

public class Test implements Listener {

    ArrayList<WebSocket> clienti = new ArrayList<>();

    public void sendCallerID(String numero_telefono) {
        System.out.println(numero_telefono + " inviato al socket 1.");
        for (int i = 0; i < clienti.size(); i++) {
            try {
                System.out.println(numero_telefono + " inviato al socket 2.");
                clienti.get(i).sendMessage(numero_telefono);

            } catch (IOException ex) {
                Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    @Override
    public void newConnection(WebSocket socket) {

        clienti.add(socket);

        socket.addEventListener(new WebSocket.Listener() {

            @Override
            public void messageReceived(String message, WebSocket source) {

            }

            @Override
            public void connectionClosed(WebSocket socket) {
                System.out.println("Connection with client closed");
            }
        });

    }

    @Override
    public void error(String message) {
        System.out.println("Error handshaking with client");
    }

    public static void main(String[] args) {

        final Test test = new Test();
        WebSocketServer server = new WebSocketServer(7778);
        server.addEventListener(test);
        server.startListening();

        try {
            (new CallerId(test)).connect("COM7");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
