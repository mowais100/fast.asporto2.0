package net.mariusgundersen.websocket;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class CallerId {

    SerialPort serialPort;
    Test test;

    public CallerId(Test test) {
        super();
        this.test = test;
    }

    void connect(String portName) throws Exception {
        CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(portName);
        if (portIdentifier.isCurrentlyOwned()) {
            System.out.println("Error: Port is currently in use");
        } else {
            CommPort commPort = portIdentifier.open(this.getClass().getName(), 6000);

            if (commPort instanceof SerialPort) {
                serialPort = (SerialPort) commPort;

                //QUESTI POSSONO ESSERE UTILI IN QUALCHE CASO
                /*System.out.println("BaudRate: " + serialPort.getBaudRate());
                System.out.println("DataBIts: " + serialPort.getDataBits());
                System.out.println("StopBits: " + serialPort.getStopBits());
                System.out.println("Parity: " + serialPort.getParity());
                System.out.println("FlowControl: " + serialPort.getFlowControlMode());
                serialPort.setSerialPortParams(4800, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_ODD);
                //serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_RTSCTS_IN);
                System.out.println("BaudRate: " + serialPort.getBaudRate());
                System.out.println("DataBIts: " + serialPort.getDataBits());
                System.out.println("StopBits: " + serialPort.getStopBits());
                System.out.println("Parity: " + serialPort.getParity());
                System.out.println("FlowControl: " + serialPort.getFlowControlMode());*/
                InputStream in = serialPort.getInputStream();
                OutputStream out = serialPort.getOutputStream();

                (new Thread(new SerialReader(in, test))).start();
                (new Thread(new SerialWriter(out, in))).start();

            } else {
                System.out.println("Error: Only serial ports are handled by this example.");
            }
        }
    }

    public static class SerialReader implements Runnable {

        InputStream in;
        Test test;

        public SerialReader(InputStream in, Test test) {
            this.in = in;
            this.test = test;
        }

        public void run() {
            byte[] buffer = new byte[1024];

            int len = -1;
            try {
                while ((len = this.in.read(buffer)) > -1) {
                    String[] buffer_string = new String(buffer, 0, len).trim().split("\r\n|\r|\n");

                    if (buffer_string.length > 2 && buffer_string[2].length() > 1) {
                        if (buffer_string[2].length() > 7) {
                            System.out.println(buffer_string[2].substring(7));
                            test.sendCallerID(buffer_string[2].substring(7));

                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static class SerialWriter implements Runnable {

        OutputStream out;
        InputStream in;

        public SerialWriter(OutputStream out, InputStream in) {
            this.out = out;
            this.in = in;
        }

        public void run() {
            try {

                this.out.write("ATZ".getBytes());
                this.out.write("\r\n".getBytes());

                this.out.write("AT+VCID=1".getBytes());
                this.out.write("\r\n".getBytes());

                this.out.write("AT+FCLASS=8".getBytes());
                this.out.write("\r\n".getBytes());

                byte mBytesIn1[] = new byte[1024];
                this.in.read(mBytesIn1);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
