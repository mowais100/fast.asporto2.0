<?php

$files = array_slice(scandir('./imgSlide/'), 2);

$ogni_tre = array_slice(scandir('./imgOgni3/'), 2);

$array_finale = [];

$lunghezza_array = count($files);



if ($lunghezza_array > 0 && count($ogni_tre) > 0) {

    for ($i = 1; $i <= $lunghezza_array; $i++) {
        if ($files[$i - 1] !== 'Thumbs.db') {
            array_push($array_finale, './imgSlide/' . $files[$i - 1]);
        }

        if ($i % 3 == 0) {

            for ($c = 0; $c < count($ogni_tre); $c++) {
                if ($ogni_tre[$c] !== 'Thumbs.db') {
                    array_push($array_finale, './imgOgni3/' . $ogni_tre[$c]);
                }
            }
        }
    }
} else {
    for ($i = 0; $i < count($files); $i++) {
        if ($files[$i] !== 'Thumbs.db') {
            array_push($array_finale, './imgSlide/' . $files[$i]);
        }
    }
}


echo json_encode($array_finale);
