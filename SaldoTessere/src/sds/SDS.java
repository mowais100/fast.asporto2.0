/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sds;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import java.awt.print.PrinterException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import com.sun.org.apache.xml.internal.utils.WrappedRuntimeException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Davide Cavallini Checchin Software S.r.l.
 */
public class SDS {

    static String numeroTessera = "";

    static class MyHandler implements HttpHandler {

        public static Map<String, String> getQueryMap(String query) {
            String[] params = query.split("&");
            Map<String, String> map = new HashMap<String, String>();
            for (String param : params) {
                String name = param.split("=")[0];
                String value = param.split("=")[1];
                map.put(name, value);
            }
            return map;
        }

        @Override
        public void handle(HttpExchange t) throws IOException, WrappedRuntimeException {

            t.getResponseHeaders().set("Access-Control-Allow-Origin", "*");

            t.getResponseHeaders().set("Content-Type", "text/xml; charset=utf-8");

            String query = t.getRequestURI().getQuery();

            if (query != null) {
                Map<String, String> map = getQueryMap(query);
                Set<String> keys = map.keySet();
                keys.stream().filter(key -> (key.compareTo("tessera") == 0)).forEachOrdered(key -> {
                    numeroTessera = map.get(key);
                });
            }

            String response = "false";

            String connectionUrl
                    = "jdbc:sqlserver://80.16.108.98:65432;"
                    + "database=fastcomanda;"
                    + "user=fastcomanda;"
                    + "password=FCcsl;"
                        + "encrypt=false;"
                + "trustServerCertificate=false;"
                + "loginTimeout=30;";

            ResultSet resultSet = null;

            try (Connection connection = DriverManager.getConnection(connectionUrl);
                    Statement statement = connection.createStatement();) {

                // Create and execute a SELECT SQL statement.
                String selectSql = "select TOP 1 PRG_FAT from CLIENTI";
                resultSet = statement.executeQuery(selectSql);

                // Print results from select statement
                while (resultSet.next()) {
                    response = resultSet.getString(1);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            t.sendResponseHeaders(200, response.length());
            try (OutputStream os = t.getResponseBody()) {
                os.write(response.getBytes());
            }
        }
    }

    public static void main(String[] args) throws PrinterException, IOException {

        HttpServer server = HttpServer.create(new InetSocketAddress(222), 0);

        server.createContext("/SALDO", new MyHandler());
        server.start();

    }

}
