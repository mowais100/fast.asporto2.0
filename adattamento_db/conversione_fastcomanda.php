<?php

ini_set('max_execution_time', 0);

set_time_limit(0);

$dbmysql = new mysqli("localhost", "root", "", "provaintellinet");

$dbmysql->set_charset("utf8");

$serverName = "DAVIDE-LENOVO\SQLEXPRESS"; //serverName\instanceName
$connectionInfo = array("Database" => "DGFE2016", "UID" => "SA", "PWD" => "Pwdcsl");
$conn = sqlsrv_connect($serverName, $connectionInfo);

if ($conn) {
    echo "Connessione Stabilita.<br />";
} else {
    echo "Connessione Non Possibile.<br />";
    die(mostra_errore(sqlsrv_errors()));
}



//CORPI
$sql = "SELECT * FROM transazioni where TC='C';";

/*
  TC >NIENTE
  DATA > data_comanda
  TAVOLO > NIENTE
  BIS: Codice della storicizzazione formato da una S seguita dal numero del terminale, ore, minuti, secondi
  PRG > prog_inser
  ART > NIENTE
  ART2 > cod_articolo
  VAR > questo va messo nel desc_art all'inizio dell'articolo
  DES > desc_art
  PRZ > prezzo_un
  COSTO > NIENTE
  QTA > quantita
  SN > stampata_sn
  CAT > categoria
  TOTALE > NIENTE
  SCONTO > NIENTE
  AUTORIZ > NIENTE
  INC > NIENTE
  CARTACRED > NIENTE
  BANCOMAT > NIENTE
  ASSEGNI > perc_iva
  NCARD1 > contiene_variante
  NCARD2 > NIENTE
  NCARD3 > NIENTE
  NCARD4 > NIENTE
  NCARD5 > ordinamento
  NSEGN > numero_conto
  NEXIT > NIENTE
  TS: Destinazione di stampa
  NOME > operatore
  ORA > ora_comanda
  LIB1 > numero_servizio
  LIB2 > NIENTE
  LIB3 > NIENTE
  LIB4 > NIENTE
  CLI > rag_soc_cliente
  QTAP > NIENTE
  NODO > nodo
  PORTATA > portata
  NUMP > nump_xml
  TAVOLOTXT > tavolo
  ULTPORT > ultima_portata
  LIB5 > dest_stampa_2
  AGG_GIAC > NIENTE
  PRZN > NIENTE
  DATAR > NIENTE
  NPRGF > progressivo_fiscale
 */

 $stmt = sqlsrv_query($conn, $sql);
  if ($stmt === false) {
  die(mostra_errore(sqlsrv_errors()));
  } 
/*
  DATA > data_comanda
  PRG > prog_inser
  ART2 > cod_articolo
  VAR > questo va messo nel desc_art all'inizio dell'articolo
  DES > desc_art
  PRZ > prezzo_un
  QTA > quantita
  SN > stampata_sn
  CAT > categoria
  ASSEGNI > perc_iva
  NCARD1 > contiene_variante
  NCARD5 > ordinamento
  NSEGN > numero_conto
  TS: Destinazione di stampa
  NOME > operatore
  ORA > ora_comanda
  LIB1 > numero_servizio
  CLI > rag_soc_cliente
  NODO > nodo
  PORTATA > portata
  NUMP > nump_xml
  TAVOLOTXT > tavolo
  ULTPORT > ultima_portata
  LIB5 > dest_stampa_2
  NPRGF > progressivo_fiscale
 */

while ($obj = sqlsrv_fetch_object($stmt)) {

  $var = "";
  if (!empty($obj->VAR)) {
  $var = $obj->VAR;
  }

  $id_comanda = identificativo_comanda($obj->NOME, $obj->DATA, $obj->ORA);

  $df = calcola_dati_fiscali_comanda($obj->PRZ, $obj->ASSEGNI);
  
  $giorno_settimana=giorno_settimana($obj->DATA);


  $query_corpo = "INSERT INTO comanda (giorno,posizione,tipo_ricevuta,fiscalizzata_sn,totale_scontato_ivato,imponibile_rep_1,imposta_rep_1,imponibile_rep_2,imposta_rep_2,imponibile_rep_3,imposta_rep_3,imponibile_rep_4,imposta_rep_4,"
  . "stato_record,id,data_comanda,prog_inser,cod_articolo,desc_art,prezzo_un,quantita,stampata_sn,categoria,perc_iva,contiene_variante,ordinamento,numero_conto,dest_stampa,"
  . "operatore,ora_comanda,numero_servizio,rag_soc_cliente,nodo,portata,nump_xml,ntav_comanda,ultima_portata,dest_stampa_2,progressivo_fiscale) VALUES ("
  . "'".$giorno_settimana."'" . ", " . "'CONTO'" . ", " .  "'SCONTRINO'" . ", " . "'S'" . ", " . "'" . $df["totale_scontato_ivato"] . "'" . ", " . "'" . $df["imponibile_1"] . "'" . ", " . "'" . $df["imposta_1"] . "'" . ", " . "'" . $df["imponibile_2"] . "'" . ", " . "'" . $df["imposta_2"] . "'"
  . ", " . "'" . $df["imponibile_3"] . "'" . ", " . "'" . $df["imposta_3"] . "'" . ", " . "'" . $df["imponibile_4"] . "'" . ", " . "'" . $df["imposta_4"] . "'" . ", " . "'CHIUSO'" . ", '" . $id_comanda . "'" . ", " . "'" . converti_data_stringa($obj->DATA, 'Y-m-d') . "'"
  . ", " . "'" . $obj->PRG . "'" . ", " . "'" . $obj->ART2 . "'" . ", " . "'" . $var . " " . $obj->DES . "'" . ", " . "'" . $obj->PRZ . "'" . ", "
  . "" . "'" . $obj->QTA . "'" . ", " . "'" . $obj->SN . "'" . ", " . "'" . $obj->CAT . "'" . ", " . "'" . $obj->ASSEGNI . "'" . ", " . "'" . $obj->NCARD1 . "'" . ", " . "'" . $obj->NCARD5 . "'" . ", " . "'" . $obj->NSEGN . "'" . ""
  . ", " . "'" . $obj->TS . "'" . ", " . "'" . $obj->NOME . "'" . ", " . "'" . $obj->ORA . "'" . ", " . "'" . $obj->LIB1 . "'" . ", " . "'" . $obj->CLI . "'" . ", " . "'" . $obj->NODO . "'" . ""
  . ", " . "'" . $obj->PORTATA . "'" . ", " . "'" . $obj->NUMP . "'" . ", " . "'" . $obj->TAVOLOTXT . "'" . ", " . "'" . $obj->ULTPORT . "'" . ", " . "'" . $obj->LIB5 . "'" . ", " . "'" . $obj->NPRGF . "'" . ");";

  esegui_query_mysql($query_corpo);
  } 

echo "FINE QUERY CORPO<br>";


//TESTE
$sql2 = "SELECT * FROM transazioni where TC='T';";

/*
  TC > niente
  data > data (dd mm YYYY)
  tavolo > NIENTE
  bis > NIENTE
  prg > NIENTE
  art > NIENTE
  art2 > ora inserimento comanda QUINDI NIENTE NON LO HO
  var > tipo tessera, 1 se è gutshein QUINDI NIENTE NON LO HO
  des > niente
  prz > resto
  costo > NIENTE
  qta > NIENTE
  sn > NIENTE
  cat > NIENTE
  totale > totale
  sconto > importo dello sconto QUINDI NIENTE NON CE L HO
  AUTORIZ > NIENTE
  INC > contanti
  CARTACRED > carte_credito
  BANCOMAT> bancomat
  ASSEGNI> assegno
  NCARD1 > NIENTE
  NCARD2 > NIENTE
  NCARD3 > NIENTE
  NCARD4 > NIENTE
  NCARD5 > NIENTE
  NSEGN > NIENTE
  NEXIT > NIENTE
  TS > NIENTE
  NOME > operatore
  ORA > ora
  LIB1 > NIENTE
  LIB2 > NIENTE
  LIB3 > NIENTE
  LIB4 > NIENTE
  CLI > NIENTE
  QTAP > NIENTE
  NODO > NIENTE
  PORTATA > NIENTE
  NUMP > NIENTE
  TAVOLOTXT > tavolo
  ULTPORT > NIENTE
  LIB5 > NIENTE
  AGG_GIAC > NIENTE
  PRZN > NIENTE
  DATAR > NIENTE
  NPRGF > numero_fiscale
 */

$stmt2 = sqlsrv_query($conn, $sql2);
if ($stmt2 === false) {
    die(mostra_errore(sqlsrv_errors()));
}
/*

  data > data (dd mm YYYY)
  art2 > ora inserimento comanda QUINDI NIENTE NON LO HO
  var > tipo tessera, 1 se è gutshein QUINDI NIENTE NON LO HO
  prz > resto
  totale > totale
  INC > contanti
  CARTACRED > carte_credito
  BANCOMAT> bancomat
  ASSEGNI> assegno
  NOME > operatore
  ORA > ora
  TAVOLOTXT > tavolo
  NPRGF > numero_fiscale
  LIB2 > scontrino fattura
 */
while ($obj2 = sqlsrv_fetch_object($stmt2)) {


    //$df2 = calcola_dati_fiscali_testa($obj2->TOTALE, $obj2->ASSEGNI);

    $id_comanda = identificativo_comanda($obj2->NOME, $obj2->DATA, $obj2->ORA);

    $query_testa = "INSERT INTO record_teste (progressivo_comanda,data_comanda,data_servizio,resto,totale,contanti,carte_credito,bancomat,assegno,operatore,ora_comanda,tavolo,numero_fiscale,tipologia) VALUES ("
            . " " . "'" . $id_comanda . "'" . ", " . "'" . converti_data_stringa($obj2->DATA, 'Y-m-d') . "'" . ", " . "'" . converti_data_stringa($obj2->DATA, 'Y-m-d') . "'" . ",  " . "'" . $obj2->PRZ . "'" . ", "
            . "" . "'" . $obj2->TOTALE . "'" . ", " . "'" . $obj2->INC . "'" . ", " . "'" . $obj2->CARTACRED . "'" . ", " . "'" . $obj2->BANCOMAT . "'" . ", " . "'" . $obj2->ASSEGNI . "'" . ","
            . " " . "'" . $obj2->NOME . "'" . ", " . "'" . $obj2->ORA . "'" . ", " . "'" . $obj2->TAVOLOTXT . "'" . ", " . "'" . $obj2->NPRGF . "'" . ", " . "'SCONTRINO'" . ");";

    esegui_query_mysql($query_testa);
}
echo "FINE QUERY TESTA<br>";




$dbmysql->close();

/* --------------------------------------------------- */
/* INIZIO FUNZIONI */

function calcola_dati_fiscali_comanda($prezzo_un, $perc_iva) {

    $divisore = $perc_iva / 100;

    $dividendo = $prezzo_un;

    $imposta = $divisore / $dividendo;

    $imponibile = $dividendo - $imposta;

    $a["imponibile_1"] = 0;
    $a["imposta_1"] = 0;
    $a["imponibile_2"] = 0;
    $a["imposta_2"] = 0;
    $a["imponibile_3"] = 0;
    $a["imposta_3"] = 0;
    $a["imponibile_4"] = 0;
    $a["imposta_4"] = 0;

    switch ($perc_iva) {
        case 10:
        case "10":
            $a["imponibile_1"] = $imponibile;
            $a["imposta_1"] = $imposta;
            break;
        case 22:
        case "22":
            $a["imponibile_2"] = $imponibile;
            $a["imposta_2"] = $imposta;
            break;
        case 4:
        case "4":
        case "04":
            $a["imponibile_3"] = $imponibile;
            $a["imposta_3"] = $imposta;
            break;
    }

    $a["totale_scontato_ivato"] = $dividendo;

    return $a;
}

function calcola_dati_fiscali_testa($prezzo_un, $perc_iva) {

    $divisore = $perc_iva / 100;

    $dividendo = $prezzo_un;

    $imposta = $divisore / $dividendo;

    $imponibile = $dividendo - $imposta;

    $a["imponibile_1"] = 0;
    $a["imposta_1"] = 0;
    $a["totale_1"] = 0;
    $a["imponibile_2"] = 0;
    $a["imposta_2"] = 0;
    $a["totale_2"] = 0;
    $a["imponibile_3"] = 0;
    $a["imposta_3"] = 0;
    $a["totale_3"] = 0;
    $a["imponibile_4"] = 0;
    $a["imposta_4"] = 0;
    $a["totale_4"] = 0;

    switch ($perc_iva) {
        case 10:
        case "10":
            $a["imponibile_1"] = $imponibile;
            $a["imposta_1"] = $imposta;
            $a["totale_1"] = $dividendo;
            break;
        case 22:
        case "22":
            $a["imponibile_2"] = $imponibile;
            $a["imposta_2"] = $imposta;
            $a["totale_2"] = $dividendo;
            break;
        case 4:
        case "4":
        case "04":
            $a["imponibile_3"] = $imponibile;
            $a["imposta_3"] = $imposta;
            $a["totale_3"] = $dividendo;
            break;
    }



    return $a;
}

function mostra_errore($error) {

    $a = "<pre>";
    $a .= var_export($error, true);
    $a .= "</pre>";

    return $a;
}

function converti_data_stringa($data, $formato_data) {

    return date_format($data, $formato_data);
}

function giorno_settimana($data) {
     
    $date=date_format($data, "Y/m/d");
    
    $weekday = date('N', strtotime($date));
    
    return $weekday;
    
}

function identificativo_comanda($nome, $data, $orario) {

    $operatore_temp = substr($nome, 0, 3);

    $operatore = '';

    for ($i = strlen($operatore_temp); $i < 3; $i++) {
        $operatore .= 'X';
    }
    $operatore .= $operatore_temp;

    $anno = converti_data_stringa($data, 'Y');

    $mese = converti_data_stringa($data, 'm');

    $giorno = converti_data_stringa($data, 'd');

    $ora = substr($orario, 0, 2);
    $minuti = substr($orario, 3, 2);
    $secondi = substr($orario, 6, 2);

    $millesimi = "000";

    return $operatore . "_" . $anno . $mese . $giorno . $ora . $minuti . $secondi . $millesimi;
}

function esegui_query_mysql($query) {

    global $dbmysql;

    $json = [];

    //echo $query . "<br>";

    $query1 = $dbmysql->query($query);

    //var_dump($query1);

    if (is_bool($query1) !== true) {
        while ($row = $query1->fetch_array(MYSQLI_ASSOC)) {

            array_push($json, $row);
        }
        return $json;
    } else {

        return $query1;
    }
}
