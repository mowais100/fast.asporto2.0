<?php

ini_set('max_execution_time', 0);

set_time_limit(0);

error_reporting(E_ERROR | E_PARSE);

/* RECORD TESTE */

$query = "ALTER TABLE record_teste ADD imponibile_rep_1 TEXT;";
esegui_query($query);

$query = "ALTER TABLE record_teste ADD imponibile_rep_2 TEXT;";
esegui_query($query);

$query = "ALTER TABLE record_teste ADD imponibile_rep_3 TEXT;";
esegui_query($query);

$query = "ALTER TABLE record_teste ADD imponibile_rep_4 TEXT;";
esegui_query($query);

$query = "ALTER TABLE record_teste ADD imponibile_rep_5 TEXT;";
esegui_query($query);

$query = "ALTER TABLE record_teste ADD percentuale_rep_1 TEXT;";
esegui_query($query);

$query = "ALTER TABLE record_teste ADD percentuale_rep_2 TEXT;";
esegui_query($query);

$query = "ALTER TABLE record_teste ADD percentuale_rep_3 TEXT;";
esegui_query($query);

$query = "ALTER TABLE record_teste ADD percentuale_rep_4 TEXT;";
esegui_query($query);

$query = "ALTER TABLE record_teste ADD percentuale_rep_5 TEXT;";
esegui_query($query);

$query = "ALTER TABLE record_teste_temp ADD imponibile_rep_1 TEXT;";
esegui_query($query);

$query = "ALTER TABLE record_teste_temp ADD imponibile_rep_2 TEXT;";
esegui_query($query);

$query = "ALTER TABLE record_teste_temp ADD imponibile_rep_3 TEXT;";
esegui_query($query);

$query = "ALTER TABLE record_teste_temp ADD imponibile_rep_4 TEXT;";
esegui_query($query);

$query = "ALTER TABLE record_teste_temp ADD imponibile_rep_5 TEXT;";
esegui_query($query);

$query = "ALTER TABLE record_teste_temp ADD percentuale_rep_1 TEXT;";
esegui_query($query);

$query = "ALTER TABLE record_teste_temp ADD percentuale_rep_2 TEXT;";
esegui_query($query);

$query = "ALTER TABLE record_teste_temp ADD percentuale_rep_3 TEXT;";
esegui_query($query);

$query = "ALTER TABLE record_teste_temp ADD percentuale_rep_4 TEXT;";
esegui_query($query);

$query = "ALTER TABLE record_teste_temp ADD percentuale_rep_5 TEXT;";
esegui_query($query);

/* RECORD COMANDA */

$query = "ALTER TABLE comanda ADD imponibile_rep_1 TEXT;";
esegui_query($query);

$query = "ALTER TABLE comanda ADD imponibile_rep_2 TEXT;";
esegui_query($query);

$query = "ALTER TABLE comanda ADD imponibile_rep_3 TEXT;";
esegui_query($query);

$query = "ALTER TABLE comanda ADD imponibile_rep_4 TEXT;";
esegui_query($query);

$query = "ALTER TABLE comanda ADD imponibile_rep_5 TEXT;";
esegui_query($query);

$query = "ALTER TABLE comanda ADD imposta_rep_1 TEXT;";
esegui_query($query);

$query = "ALTER TABLE comanda ADD imposta_rep_2 TEXT;";
esegui_query($query);

$query = "ALTER TABLE comanda ADD imposta_rep_3 TEXT;";
esegui_query($query);

$query = "ALTER TABLE comanda ADD imposta_rep_4 TEXT;";
esegui_query($query);

$query = "ALTER TABLE comanda ADD imposta_rep_5 TEXT;";
esegui_query($query);


$query = "ALTER TABLE comanda ADD totale_scontato_ivato TEXT;";
esegui_query($query);

$query = "ALTER TABLE comanda ADD prezzo_maxi TEXT;";
esegui_query($query);


$query = "ALTER TABLE comanda_temp ADD imponibile_rep_1 TEXT;";
esegui_query($query);

$query = "ALTER TABLE comanda_temp ADD imponibile_rep_2 TEXT;";
esegui_query($query);

$query = "ALTER TABLE comanda_temp ADD imponibile_rep_3 TEXT;";
esegui_query($query);

$query = "ALTER TABLE comanda_temp ADD imponibile_rep_4 TEXT;";
esegui_query($query);

$query = "ALTER TABLE comanda_temp ADD imponibile_rep_5 TEXT;";
esegui_query($query);

$query = "ALTER TABLE comanda_temp ADD imposta_rep_1 TEXT;";
esegui_query($query);

$query = "ALTER TABLE comanda_temp ADD imposta_rep_2 TEXT;";
esegui_query($query);

$query = "ALTER TABLE comanda_temp ADD imposta_rep_3 TEXT;";
esegui_query($query);

$query = "ALTER TABLE comanda_temp ADD imposta_rep_4 TEXT;";
esegui_query($query);

$query = "ALTER TABLE comanda_temp ADD imposta_rep_5 TEXT;";
esegui_query($query);



$query = "ALTER TABLE comanda_temp ADD totale_scontato_ivato  TEXT;";
esegui_query($query);

$query = "ALTER TABLE comanda_temp ADD prezzo_maxi TEXT;";
esegui_query($query);

/* RICALCOLO IMPONIBILE TESTE */
$query = "UPDATE record_teste SET imponibile_rep_1=ROUND((totale_rep_1-imposta_rep_1),2),imponibile_rep_2=ROUND((totale_rep_2-imposta_rep_2),2),imponibile_rep_3=ROUND((totale_rep_3-imposta_rep_3),2),imponibile_rep_4=ROUND((totale_rep_4-imposta_rep_4),2),imponibile_rep_5=ROUND((totale_rep_5-imposta_rep_5),2);";
esegui_query($query);

$query = "UPDATE record_teste SET percentuale_rep_1='10',percentuale_rep_2='22',percentuale_rep_3='4',percentuale_rep_4='0',percentuale_rep_5='0';";
esegui_query($query);



/* RICALCOLO SCONTO E IMPONIBILE PER RIGHE COMANDA */

$query = "UPDATE comanda SET imponibile_rep_1='0';";
esegui_query($query);

$query = "UPDATE comanda SET imponibile_rep_2='0';";
esegui_query($query);

$query = "UPDATE comanda SET imponibile_rep_3='0';";
esegui_query($query);

$query = "UPDATE comanda SET imponibile_rep_4='0';";
esegui_query($query);

$query = "UPDATE comanda SET imponibile_rep_5='0';";
esegui_query($query);


$query = "UPDATE comanda SET totale_scontato_ivato =ROUND(((prezzo_un*quantita)*sconto_perc/100),2)  where (sconto_perc!='' and sconto_perc!='0' and sconto_perc!='0.00);";
esegui_query($query);

$query = "UPDATE comanda SET totale_scontato_ivato =ROUND(((prezzo_un*quantita)),2)  where (sconto_perc='' or sconto_perc='0' or sconto_perc='0.00');";
esegui_query($query);


$query = "UPDATE comanda SET imponibile_rep_1=ROUND((totale_scontato_ivato/1.10),8)  where perc_iva='10' ;";
esegui_query($query);

$query = "UPDATE comanda SET imponibile_rep_2=ROUND((totale_scontato_ivato/1.22),8)  where perc_iva='22' ;";
esegui_query($query);

$query = "UPDATE comanda SET imponibile_rep_3=ROUND((totale_scontato_ivato/1.04),8)  where perc_iva='4' ;";
esegui_query($query);




/* RICALCOLO IMPOSTA COMANDA */

$query = "UPDATE comanda SET imposta_rep_1='0' ;";
esegui_query($query);

$query = "UPDATE comanda SET imposta_rep_2='0' ;";
esegui_query($query);

/* RICALCOLO IMPOSTA COMANDA */
$query = "UPDATE comanda SET imposta_rep_3='0' ;";
esegui_query($query);

$query = "UPDATE comanda SET imposta_rep_4='0' ;";
esegui_query($query);

/* RICALCOLO IMPOSTA COMANDA */
$query = "UPDATE comanda SET imposta_rep_5='0' ;";
esegui_query($query);


$query = "UPDATE comanda SET imposta_rep_1=ROUND((totale_scontato_ivato-imponibile_rep_1),8) where perc_iva='10';";
esegui_query($query);

/* RICALCOLO IMPOSTA COMANDA */
$query = "UPDATE comanda SET imposta_rep_2=ROUND((totale_scontato_ivato-imponibile_rep_2),8) where perc_iva='22';";
esegui_query($query);

/* RICALCOLO IMPOSTA COMANDA */
$query = "UPDATE comanda SET imposta_rep_3=ROUND((totale_scontato_ivato-imponibile_rep_3),8) where perc_iva='4';";
esegui_query($query);



function esegui_query($query) {

    $mysql_abilitato = true;

    $percorso_main = "C:/TAVOLI/fast.intellinet/";

    $websql = $percorso_main . 'DATABASE_CLIENTE.sqlite';

    try {

        $db = new SQLite3($websql);

        $db->busyTimeout(10000);

        if (strpos(strtolower($query), 'select') !== false) {
            echo "1.".$query."<br><br>";
            $results = $db->query($query);
        } else {
             echo "2.".$query."<br><br>";
            $results = $db->exec($query);
        }

        $json = [];
        $json2 = [];

        if (is_bool($results) !== true) {

            while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
                array_push($json, $row);
            }
        }

//PARTE MYSQL
        if ($mysql_abilitato === true) {

            $dbmysql = new mysqli("localhost", "root", "", "provaINTELLINET");


            $dbmysql->set_charset("utf8");

            $query1 = $dbmysql->query($query);

            if (is_bool($query1) !== true) {
                while ($row = $query1->fetch_array(MYSQLI_ASSOC)) {

                    array_push($json, $row);
                }
            }
        }

        //echo json_encode($json);
    } catch (Exception $ex) {

        //echo $ex;
    }


    $db->close();

    unset($db);
}
