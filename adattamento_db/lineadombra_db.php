<?php

ini_set('max_execution_time', 0);

set_time_limit(0);

error_reporting(E_ALL);

//DECIMALI

$query = "ALTER TABLE `comanda` CHANGE `prezzo_un` `prezzo_un` DECIMAL(8, 2) NOT NULL, CHANGE `netto` `netto` DECIMAL(8, 2) NOT NULL, CHANGE `prezzo_varianti_aggiuntive` `prezzo_varianti_aggiuntive` DECIMAL(8, 2) NOT NULL, CHANGE `prezzo_varianti_maxi` `prezzo_varianti_maxi` DECIMAL(8, 2) NOT NULL, CHANGE `prezzo_maxi_prima` `prezzo_maxi_prima` DECIMAL(8, 2) NOT NULL, CHANGE `prezzo_vero` `prezzo_vero` DECIMAL(8, 2) NOT NULL;";
esegui_query($query);

$query = "ALTER TABLE `comanda_temp` CHANGE `prezzo_un` `prezzo_un` DECIMAL(8, 2) NOT NULL, CHANGE `netto` `netto` DECIMAL(8, 2) NOT NULL, CHANGE `prezzo_varianti_aggiuntive` `prezzo_varianti_aggiuntive` DECIMAL(8, 2) NOT NULL, CHANGE `prezzo_varianti_maxi` `prezzo_varianti_maxi` DECIMAL(8, 2) NOT NULL, CHANGE `prezzo_maxi_prima` `prezzo_maxi_prima` DECIMAL(8, 2) NOT NULL, CHANGE `prezzo_vero` `prezzo_vero` DECIMAL(8, 2) NOT NULL;";
esegui_query($query);

//TROVARE PREZZO UNITARIO E FORSE TOTALE DI QUELLI DA 99.99
/* come faccio con quelli già riadeguati? */

//CONDIZIONI
//id tagliato >= 20190509
//netto  = 0.00
//prezzo_un = 99.99 
//(tipo_ricevuta LIKE 'scontrino' or tipo_ricevuta LIKE 'SCONTRINO' )
//
//imponibile_rep_3!=0
//
//ALLORA prezzo_un=imponibile_rep_3+imposta_rep_3
//
//SE INVECE
//imponibile_rep_4!=0
//
//ALLORA prezzo_un=imponibile_rep_4+imposta_rep_4


$condizione1="update comanda set prezzo_un=imponibile_rep_3+imposta_rep_3 where substr(id,5,8)>=20190509 and prezzo_un = 99.99 and netto  = 0.00 and (tipo_ricevuta LIKE 'scontrino' or tipo_ricevuta LIKE 'SCONTRINO' ) and imponibile_rep_3!=0;";
esegui_query($condizione1);

$condizione2="update comanda set prezzo_un=imponibile_rep_4+imposta_rep_4 where substr(id,5,8)>=20190509 and prezzo_un = 99.99 and netto  = 0.00 and (tipo_ricevuta LIKE 'scontrino' or tipo_ricevuta LIKE 'SCONTRINO' ) and imponibile_rep_4!=0;";
esegui_query($condizione2);



//STRUTTURA TABELLE NORMALI E _TEMP

$query = "ALTER TABLE `comanda` ADD `numero_paganti` TEXT NOT NULL AFTER `ora_comanda`, ADD `numero_servizio` TEXT NOT NULL AFTER `numero_paganti`;";
esegui_query($query);

$query = "ALTER TABLE `comanda_temp` ADD `numero_paganti` TEXT NOT NULL AFTER `ora_comanda`, ADD `numero_servizio` TEXT NOT NULL AFTER `numero_paganti`;";
esegui_query($query);

$query = "ALTER TABLE `record_teste` ADD `assegno` TEXT NOT NULL AFTER `totale_rep_5`, ADD `bonifico` TEXT NOT NULL AFTER `assegno`, ADD `ult_prog_inser` TEXT NOT NULL AFTER `bonifico`, ADD `numero_servizio` TEXT NOT NULL AFTER `ult_prog_inser`;";
esegui_query($query);

$query = "ALTER TABLE `record_teste_temp` ADD `assegno` TEXT NOT NULL AFTER `totale_rep_5`, ADD `bonifico` TEXT NOT NULL AFTER `assegno`, ADD `ult_prog_inser` TEXT NOT NULL AFTER `bonifico`, ADD `numero_servizio` TEXT NOT NULL AFTER `ult_prog_inser`;";
esegui_query($query);




//CANCELLARE CAMPI ERRATI DA DATA DA VERIFICARE
$query = "update comanda set numero_paganti='', numero_servizio='1', imponibile_rep_1='0', imponibile_rep_2='0', imponibile_rep_3='0', imponibile_rep_4='0', imponibile_rep_5='0', imposta_rep_1='0', imposta_rep_2='0', imposta_rep_3='0', imposta_rep_4='0', imposta_rep_5='0', totale_scontato_ivato='0';";
esegui_query($query);

//RIADEGUARE

/* RICALCOLO IMPONIBILE TESTE */
$query = "UPDATE record_teste SET imponibile_rep_1=ROUND((totale_rep_1-imposta_rep_1),2),imponibile_rep_2=ROUND((totale_rep_2-imposta_rep_2),2),imponibile_rep_3=ROUND((totale_rep_3-imposta_rep_3),2),imponibile_rep_4=ROUND((totale_rep_4-imposta_rep_4),2),imponibile_rep_5=ROUND((totale_rep_5-imposta_rep_5),2);";
esegui_query($query);

$query = "UPDATE record_teste SET percentuale_rep_1='10',percentuale_rep_2='22',percentuale_rep_3='4',percentuale_rep_4='0',percentuale_rep_5='0';";
esegui_query($query);



/* RICALCOLO SCONTO E IMPONIBILE PER RIGHE COMANDA */

$query = "UPDATE comanda SET imponibile_rep_1='0';";
esegui_query($query);

$query = "UPDATE comanda SET imponibile_rep_2='0';";
esegui_query($query);

$query = "UPDATE comanda SET imponibile_rep_3='0';";
esegui_query($query);

$query = "UPDATE comanda SET imponibile_rep_4='0';";
esegui_query($query);

$query = "UPDATE comanda SET imponibile_rep_5='0';";
esegui_query($query);


$query = "UPDATE comanda SET totale_scontato_ivato = ROUND(((prezzo_un*quantita)*sconto_perc/100),2)  where (sconto_perc!='' and sconto_perc!='0' and sconto_perc!='0.00);";
esegui_query($query);

$query = "UPDATE comanda SET totale_scontato_ivato =ROUND(((prezzo_un*quantita)),2)  where (sconto_perc='' or sconto_perc='0' or sconto_perc='0.00');";
esegui_query($query);


$query = "UPDATE comanda SET imponibile_rep_1=ROUND((totale_scontato_ivato/1.10),8)  where perc_iva='10' ;";
esegui_query($query);

$query = "UPDATE comanda SET imponibile_rep_2=ROUND((totale_scontato_ivato/1.22),8)  where perc_iva='22' ;";
esegui_query($query);

$query = "UPDATE comanda SET imponibile_rep_3=ROUND((totale_scontato_ivato/1.04),8)  where perc_iva='4' ;";
esegui_query($query);




/* RICALCOLO IMPOSTA COMANDA */

$query = "UPDATE comanda SET imposta_rep_1='0' ;";
esegui_query($query);

$query = "UPDATE comanda SET imposta_rep_2='0' ;";
esegui_query($query);

/* RICALCOLO IMPOSTA COMANDA */
$query = "UPDATE comanda SET imposta_rep_3='0' ;";
esegui_query($query);

$query = "UPDATE comanda SET imposta_rep_4='0' ;";
esegui_query($query);

/* RICALCOLO IMPOSTA COMANDA */
$query = "UPDATE comanda SET imposta_rep_5='0' ;";
esegui_query($query);


$query = "UPDATE comanda SET imposta_rep_1=ROUND((totale_scontato_ivato-imponibile_rep_1),8) where perc_iva='10';";
esegui_query($query);

/* RICALCOLO IMPOSTA COMANDA */
$query = "UPDATE comanda SET imposta_rep_2=ROUND((totale_scontato_ivato-imponibile_rep_2),8) where perc_iva='22';";
esegui_query($query);

/* RICALCOLO IMPOSTA COMANDA */
$query = "UPDATE comanda SET imposta_rep_3=ROUND((totale_scontato_ivato-imponibile_rep_3),8) where perc_iva='4';";
esegui_query($query);

echo "FINITO";

function esegui_query($query) {

    $json = [];

    $dbmysql = new mysqli("localhost", "root", "", "provaINTELLINET");


    $dbmysql->set_charset("utf8");

    $query1 = $dbmysql->query($query);

    if (is_bool($query1) !== true) {
        while ($row = $query1->fetch_array(MYSQLI_ASSOC)) {

            array_push($json, $row);
        }

        return $json;
    } else {
        return $query1;
    }
}