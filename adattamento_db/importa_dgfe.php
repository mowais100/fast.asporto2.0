<?php

ini_set('max_execution_time', 0);

set_time_limit(0);

error_reporting(E_ALL);

$dir_teste = "./esempi/DGFE/";

$array_files = scandir($dir_teste);

$inizio_lettura = false;

$prog_inser = "1";

$nodo = "1";


$oggetto_dgfe = [];



foreach ($array_files as $i => $v) {

    if ($v !== "." && $v !== "..") {

        $numero_matricola = substr(str_replace(".log", "", str_replace("DGFE_", "", $v)),-6);

        $nome_file = $dir_teste . $v;

        $fl = fopen($nome_file, "r");

        for ($x_pos = 0, $ln = 0, $output = array(); fseek($fl, $x_pos, SEEK_END) !== -1; $x_pos--) {

            $char = iconv("CP850", "UTF-8", fgetc($fl));

            if ($char === "\n") {

                $riga_esistente = false;

                // analyse completed line $output[$ln] if need be
                //ERAVAMO ARRIVATI QUA
                //echo $output[$ln];

                $pattern = "/(\d*)-(\d*)/";
                preg_match($pattern, $output[$ln], $result);


                //echo $output[$ln];

                if (isset($result[2]) && strlen($result[2]) == 4) {
                    $riga_esistente = true;

                    $progressivo_fiscale = number_format($result[2], 0);

                    $oggetto_dgfe[$numero_matricola][$progressivo_fiscale] = [];

                    /* echo "<b>n. " . $progressivo_fiscale . "</b>";
                      echo "<br>"; */
                } else if (strpos($output[$ln], "TOTALE COMPLESSIVO") !== FALSE) {
                    $riga_esistente = true;
                    $inizio_lettura = true;

                    $prog_inser = "1";
                    $nodo = "1";

                    $pattern = "/(\d*),(\d*)/";
                    preg_match($pattern, $output[$ln], $result);

                    /* echo "Totale " . str_replace(",", ".", str_replace(".", "", $result[0]));
                      echo "<br>";
                      echo " Inizio Lettura ";
                      echo "<br>"; */
                } else if (preg_match("/(\d*)-(\d*)-(\d*)/", $output[$ln], $result) === 1) {
                    $riga_esistente = true;

                    $data_intera = $result[0];

                    $giorno = substr($data_intera, 0, 2);
                    $mese = substr($data_intera, 3, 2);
                    $anno = substr($data_intera, 6, 4);

                    /* echo "Data " . $data_intera;
                      //echo " ". $giorno." / ".$mese." / ".$anno."<br>";
                      echo "<br>"; */


                    preg_match("/(\d*):(\d*)/", $output[$ln], $result);
                    $ora_intera = $result[0];

                    $ore = substr($ora_intera, 0, 2);
                    $minuti = substr($ora_intera, 3, 2);

                    /* echo "Ora " . $ora_intera . "<br>"; */

                    //echo "Ore ". $ore." Minuti " .  $minuti . "<br>";

                    /* $id_comanda="REC_".$anno.$mese.$giorno.$ore.$minuti."00000";

                      echo "Id Comanda: ";
                      echo "<b>";
                      echo $id_comanda;
                      echo "</b>";
                      echo "<br>"; */
                } else if (strpos($output[$ln], "DESCRIZIONE") !== FALSE) {
                    $riga_esistente = true;
                    $inizio_lettura = false;

                    /* echo "<br>";
                      echo "<b>Fine Lettura</b>";
                      echo "<br>";
                      echo "<br>"; */
                } else if ($inizio_lettura === TRUE) {



                    $pattern = "/\d*,\d*\%/";
                    preg_match($pattern, $output[$ln], $result, PREG_OFFSET_CAPTURE);

                    if (isset($result[0][1])) {

                        $riga_esistente = true;
                        //echo " ### " . $result[0][1] . ",".strlen($result[0][0]).",".($result[0][1]+strlen($result[0][0]))."";


                        $prezzo_totale_articolo = str_replace(",", ".", str_replace(".", "", substr($output[$ln], ($result[0][1] + strlen($result[0][0])))));
                        $netto = "";
                        $iva_articolo = str_replace(",", ".", str_replace(".", "", substr($output[$ln], $result[0][1], (strlen($result[0][0]) - 1))));
                        $descrizione_articolo = substr($output[$ln], 0, $result[0][1]);
                        $posizione = "CONTO";
                        $sconto_imp = "";

                        /* echo "<br>"; */

                        /* echo "Descrizione: " . $descrizione_articolo . "<br>";
                          echo "IVA: " . $iva_articolo . "<br>";
                          echo "Prezzo Tot. " . $prezzo_totale_articolo; */

                        if ($prezzo_totale_articolo < 0) {

                            /* echo "## SCONTO"; */

                            $netto = number_format($prezzo_totale_articolo * -1, 2);

                            $posizione = "SCONTO";

                            $sconto_imp = "F";
                        }

                        /* echo "<br>"; */

                        $nodo_finale = str_pad($nodo, 3, '0', STR_PAD_LEFT);

                        /* $query = "INSERT INTO comanda (prog_inser,nodo,desc_art,perc_iva,quantita,prezzo_un,netto,posizione,stato_record,sconto_imp,tipo_ricevuta,progressivo_fiscale,n_fiscale,fiscalizzata_sn,cod_articolo,numero_conto,tipo_record,data_fisc,ora_fisc,n_conto_parziale,operatore,nome_pc,tasto_segue, data_servizio,data_comanda,ora_comanda,numero_servizio) "
                          . "VALUES ('$prog_inser' , '$nodo_finale' , '$descrizione_articolo' , '$iva_articolo' , 1 , '$prezzo_totale_articolo' , '$netto' , '$posizione', 'CHIUSO' , '$sconto_imp' , 'SCONTRINO', '$progressivo_fiscale' , '$progressivo_fiscale' , 'S' , 'IDCSL_DES', '1' , 'CORPO' , '$giorno-$mese-$anno' , '$ore:$minuti' , '1' , 'DGFE' , 'SERVER' , '000001' , '$anno-$mese-$giorno' , '$anno-$mese-$giorno' , '$ore:$minuti:00','1');";

                          echo $query . "<br>"; */



                        /* prog_inser,nodo,desc_art,perc_iva,quantita,prezzo_un,netto,posizione,stato_record,sconto_imp,tipo_ricevuta,progressivo_fiscale,
                         * n_fiscale,fiscalizzata_sn,cod_articolo,numero_conto,tipo_record,data_fisc,ora_fisc,n_conto_parziale,operatore,nome_pc,tasto_segue, 
                         * data_servizio,data_comanda,ora_comanda,numero_servizio */




                        //'$prog_inser' , '$nodo_finale' , '$descrizione_articolo' , '$iva_articolo' , 1 , '$prezzo_totale_articolo' , '$netto' , '$posizione', 
                        //'CHIUSO' , '$sconto_imp' , 'SCONTRINO', '$progressivo_fiscale' , '$progressivo_fiscale' , 'S' , 'IDCSL_DES', '1' , 'CORPO' , 
                        //'$giorno-$mese-$anno' , '$ore:$minuti' , '1' , 'DGFE' , 'SERVER' , '000001' , '$anno-$mese-$giorno' , '$anno-$mese-$giorno' , 
                        //'$ore:$minuti:00','1'

                        $oggetto = [];

                        $oggetto["prog_inser"] = $prog_inser;
                        $oggetto["nodo"] = $nodo_finale;
                        $oggetto["desc_art"] = trim($descrizione_articolo);
                        $oggetto["perc_iva"] = $iva_articolo;
                        $oggetto["quantita"] = "1";
                        $oggetto["prezzo_un"] = trim($prezzo_totale_articolo);
                        $oggetto["netto"] = $netto;
                        $oggetto["posizione"] = $posizione;
                        $oggetto["stato_record"] = "CHIUSO";
                        $oggetto["sconto_imp"] = $sconto_imp;
                        $oggetto["tipo_ricevuta"] = "SCONTRINO";
                        $oggetto["progressivo_fiscale"] = $progressivo_fiscale;
                        $oggetto["n_fiscale"] = $progressivo_fiscale;
                        $oggetto["fiscalizzata_sn"] = "S";
                        $oggetto["cod_articolo"] = "IDCSL_DES";
                        $oggetto["numero_conto"] = "1  - RECORD TESTA";
                        $oggetto["tipo_record"] = "CORPO";
                        $oggetto["data_fisc"] = $giorno . "-" . $mese . "-" . $anno . "  - RECORD TESTA";
                        $oggetto["ora_fisc"] = $ore . ":" . $minuti . "  - RECORD TESTA";
                        $oggetto["n_conto_parziale"] = "1  - RECORD TESTA";
                        $oggetto["operatore"] = "DGFE  - RECORD TESTA";
                        $oggetto["nome_pc"] = "SERVER - RECORD TESTA";
                        $oggetto["tasto_segue"] = "000001";
                        $oggetto["data_servizio"] = $anno . "-" . $mese . "-" . $giorno . "  - RECORD TESTA";
                        $oggetto["data_comanda"] = $anno . "-" . $mese . "-" . $giorno . "  - RECORD TESTA";
                        $oggetto["ora_comanda"] = $ore . ":" . $minuti . "  - RECORD TESTA";
                        $oggetto["numero_servizio"] = "1" . "  - RECORD TESTA";


                        array_push($oggetto_dgfe[$numero_matricola][$progressivo_fiscale], $oggetto);


                        $prog_inser++;
                        $nodo++;
                    }
                }




                $ln++;

                continue;
            }

            $output[$ln] = $char . ((array_key_exists($ln, $output)) ? $output[$ln] : '');
        }

        fclose($fl);
    }
}
/*echo "<pre>";
echo var_export($oggetto_dgfe, true);
echo "</pre>";
*/
