<?php

/*
 * Come funziona:
 * 
 * Nella cartella esempi/teste metti tutti i backup locali delle teste che contengono quella giornata
 * Nella cartella esempi/corpi metti tutti i backup locali dei corpi di quella giornata
 * 
 * Nella cartella DGFE metti i log del misuratore di Solo Scontrini del giorno
 * 
 * Ti scarichi il suo database e lo metti nella cartella /esempi, e rinomini in DATABASE_CLIENTI_TEMP
 * 
 * Avvii questo programmino!
 * 
 * Esporti comanda e record_teste da TEMP
 * 
 * Cancelli i dati di quel giorno dal database vero
 * 
 * Importi le tabelle nuove
 */

ini_set('max_execution_time', 0);

set_time_limit(0);

error_reporting(E_ALL);

require("importa_dgfe.php");

/* var_export($oggetto_dgfe);

  exit; */

$data = filter_input(INPUT_GET, "data");

$check_array = array();

if (!$data) {
    echo "ERRORE. DEVI INSERIRE LA DATA NELLA VARIABILE GET 'data' NEL FORMATO yyyymmdd";
    exit;
}

echo "<pre>";

$query = "delete from record_teste;";
esegui_query_sqlite($query);

echo "CANCELLAZIONE RECORD_TESTE ESEGUITA<br><br>";

$query = "delete from comanda;";
esegui_query_sqlite($query);
echo "CANCELLAZIONE COMANDA ESEGUITA<br><br>";




//BLOCCO TESTE

$dir_teste = "./esempi/teste/";

$array_files = scandir($dir_teste);


foreach ($array_files as $i => $v) {

    if ($v !== "." && $v !== "..") {

        $nome_file = $dir_teste . $v;

        //echo "<br><br>TESTE ".$v."<br><br>";
        $csv = "";

        $fileHandle = fopen($nome_file, "r");

        while (($row = fgetcsv($fileHandle, 0, ",")) !== FALSE) {
            $linea = join(",", array_map('separatore_csv', $row)) . "<br>";

            if (strpos($linea, "_" . $data) !== FALSE) {

                $csv .= $linea;
            }
        }

        fclose($fileHandle);

        $websql_pdo = 'sqlite:./esempi/DATABASE_CLIENTE_TEMP.sqlite';
        $file_db = new PDO($websql_pdo);
        import_csv_to_sqlite($file_db, "record_teste", $csv, $options = array());
        $file_db = null;

        echo "IMPORTAZIONE record_teste ESEGUITA ($v)<br><br>";
    }
}


//BLOCCO CORPI

$dir_corpi = "./esempi/corpi/";

$array_files = scandir($dir_corpi);


foreach ($array_files as $i => $v) {

    if ($v !== "." && $v !== "..") {

        $nome_file = $dir_corpi . $v;

        //echo "<br><br>CORPI ".$v."<br><br>";

        $csv = "";

        $fileHandle = fopen($nome_file, "r");

        while (($row = fgetcsv($fileHandle, 0, ",")) !== FALSE) {


            $linea = join(",", array_map('separatore_csv', $row)) . "<br>";

            if (strpos($linea, "_" . $data) !== FALSE) {

                $csv .= $linea;
            }
        }


        //echo $csv;

        fclose($fileHandle);

        $websql_pdo = 'sqlite:./esempi/DATABASE_CLIENTE_TEMP.sqlite';
        $file_db = new PDO($websql_pdo);
        import_csv_to_sqlite($file_db, "comanda", $csv, $options = array());
        $file_db = null;


        echo "IMPORTAZIONE comanda ESEGUITA ($v)<br><br>";
    }
}


// FINE IMPORTAZIONE CSV SQLITE
$query = "select italiano,matricola from nomi_stampanti where fiscale='s' and italiano like 'SCONTRINO%';";
$dati_stampanti = esegui_query_sqlite($query);

$misuratori_riferimento = [];

foreach ($dati_stampanti as $i => $v) {

    echo substr($v["matricola"], -6) . "<br>";

    $misuratori_riferimento[str_replace("SCONTRINO", "", $v["italiano"])] = substr($v["matricola"], -6);
}

/* echo var_export($misuratori_riferimento,true)."<br>";




  echo "<br><br>";

  var_dump($oggetto_dgfe);
  exit; */

$query = "select progressivo_comanda,tavolo,nome_pc,operatore,parziale_comanda,numero_servizio,data_servizio,data_comanda,ora_comanda,data,ora,progressivo_comanda,numero_fiscale,misuratore_riferimento,totale from record_teste;";
$dati_teste = esegui_query_sqlite($query);

foreach ($dati_teste as $i => $v) {

    $query_comanda = "select sconto_perc,sum(prezzo_un*quantita) as totale from comanda where desc_art!='EXTRA' and stato_record NOT LIKE '%CANCELLATO%' and posizione='CONTO' and operatore_cancellazione = '' and progressivo_fiscale='" . $v["numero_fiscale"] . "' and misuratore_riferimento='" . $v["misuratore_riferimento"] . "';";
    $dati_comanda = esegui_query_sqlite($query_comanda);

    if (empty($dati_comanda[0]["sconto_perc"])) {
        $dati_comanda[0]["sconto_perc"] = "0.00";
    }

    $totale_comanda = $dati_comanda[0]["totale"] / 100 * (100 - $dati_comanda[0]["sconto_perc"]);

    $differenza = number_format($v["totale"], 2) - number_format($totale_comanda, 2);

    if ($differenza != 0) {

        echo "Scontrino Numero: " . $v["numero_fiscale"] . " - ID Comanda: " . $v["progressivo_comanda"] . " - Misuratore: " . $v["misuratore_riferimento"] . " - Totale Testa: " . $v["totale"] . " *** Totale Corpo: " . $totale_comanda;

        if (number_format($totale_comanda, 0) == 0) {
            echo " - VENDUTI MANCANTI. RECUPERARE DA SCONTRINO";



            echo "<pre>";
            $scontrino_dgfe = @$oggetto_dgfe[$misuratori_riferimento[$v["misuratore_riferimento"]]][$v["numero_fiscale"]];

            if (empty($scontrino_dgfe)) {
                echo "SCONTRINO NON TROVATO IN DGFE";
            } else {

                $totale_corpi = 0;
                $totale_corpi_senza_sconti = 0;
                $sconti = 0;

                foreach ($scontrino_dgfe as $k => $d) {

                    $totale_corpi += $d["prezzo_un"];
                    
                   

                    if ($d["prezzo_un"] < 0) {
                        $sconti += $d["prezzo_un"] * -1;
                    }else{
                         $totale_corpi_senza_sconti += $d["prezzo_un"];
                    }

                    $d["id"] = $v["progressivo_comanda"];
                    $d["ntav_comanda"] = $v["tavolo"];
                    $d["nome_pc"] = $v["nome_pc"];
                    $d["operatore"] = $v["operatore"];
                    $d["data_fisc"] = $v["data"];
                    $d["ora_fisc"] = $v["ora"];
                    $d["numero_conto"] = $v["parziale_comanda"];
                    $d["n_conto_parziale"] = $v["parziale_comanda"];
                    $d["numero_servizio"] = $v["numero_servizio"];
                    $d["data_servizio"] = $v["data_servizio"];
                    $d["data_comanda"] = $v["data_comanda"];
                    $d["ora_comanda"] = $v["ora_comanda"];
                    $d["misuratore_riferimento"] = $v["misuratore_riferimento"];

                    //SIAMO ARRIVATI QUI
                    //echo var_export($d, true)."<br>";

                    $keys = array_keys($d);

                    $values = array_values($d);

                    $chiavi = join(",", $keys);

                    $valori = join(",", array_map(function($value) {
                                return "'" . $value . "'";
                            }, $values));



                    $query_importati_misuratore = "INSERT INTO comanda ($chiavi) VALUES ($valori);";

                    $dati_comanda = esegui_query_sqlite($query_importati_misuratore);

                    echo $query_importati_misuratore . "<br>";

                    /* echo join(",",array_walk($keys, function(&$value, $key) { $value .= "'".$value."'"; } ))."<br>";

                      echo join(",",array_walk($values, function(&$value, $key) { $value .= "'".$value."'"; } ))."<br>"; */
                }

                if ($sconti > 0) {

                    //100:number_format($totale_corpi, 2)=x:$sconti

                    $sconto_percentuale = number_format((100 * $sconti) / $totale_corpi_senza_sconti,8);
                    
                    echo "<b>CALCOLO SCONTO PERCENTUALE 100 * ".$sconti." / ".$totale_corpi_senza_sconti."</b><br>";

                    $query_sconti = "UPDATE comanda SET sconto_perc='" . $sconto_percentuale . "' where id='" . $v["progressivo_comanda"] . "';";

                    esegui_query_sqlite($query_sconti);
                }

                if (number_format($v["totale"], 2) != number_format($totale_corpi, 2)) {

                    echo "<b>INCONGRUENZA TESTA / VENDUTI</b><br>";
                }
            }
            echo "</pre>";
        } else {
            echo " - INCONGRUENZA TESTA / VENDUTI";
        }

        echo "<br>";
    }
}


/*$query_finale = "update comanda set  stato_record='CHIUSO' where stato_record!='CHIUSO';";
esegui_query_sqlite($query_finale);

$query_finale2 = "update comanda set fiscalizzata_sn='S',fiscale_sospeso='' where stato_record NOT LIKE '%CANCELLATO%';";
esegui_query_sqlite($query_finale2);*/


echo "--FINE--<br>";

echo "</pre>";

//----------------------------------------------------------------------------//

function separatore_csv($n) {
    return '"' . $n . '"';
}

function esegui_query_sqlite($query) {

    $websql = './esempi/DATABASE_CLIENTE_TEMP.sqlite';

    try {

        $db = new SQLite3($websql);

        $db->busyTimeout(10000);

        if (strpos(strtolower($query), 'select') !== false) {
            //echo "1." . $query . "<br><br>";
            $results = $db->query($query);
        } else {
            //echo "2." . $query . "<br><br>";
            $results = $db->exec($query);
        }

        $json = [];


        if (is_bool($results) !== true) {

            while ($row = $results->fetchArray(SQLITE3_ASSOC)) {

                array_push($json, $row);
            }
        }

        return $json;
    } catch (Exception $ex) {

        //echo $ex;
    }

    $db->close();

    unset($db);
}

function import_csv_to_sqlite(&$pdo, $table, $csv, $options = array()) {

    global $check_array;

    extract($options);

    $csv_handle = fopen('php://memory', 'r+');
    fwrite($csv_handle, $csv);
    rewind($csv_handle);

    $delimiter = '<br>';

    $pdo->beginTransaction();

    while ($line = stream_get_line($csv_handle, 4096, $delimiter)) {
        // Parse the CSV line into an array
        $data = str_getcsv($line);

        $insert_values_str = join(",", array_map('separatore_csv', $data));

        $insert_sql = "INSERT INTO $table VALUES ($insert_values_str);";

        $query_tagliata = str_replace("CHIUSO", "", str_replace("RAPPORTOZ", "", substr($insert_sql, 0, strpos($insert_sql, "2019-12-24"))));

        // echo $query_tagliata."<br>";

        if (array_search($query_tagliata, $check_array) === FALSE) {

            /* echo $query_tagliata . "<br>"; */

            array_push($check_array, $query_tagliata);

            $pdo->query($insert_sql);

            /* echo $insert_sql . "<br><br>"; */
        } else {
            //echo "QUERY DOPPIA:<br>";
            //echo $insert_sql . "<br>";
        }
    }

    $pdo->commit();
}
