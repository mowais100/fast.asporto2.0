/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sds;

import java.awt.print.PrinterException;
import java.io.IOException;
import java.io.StringReader;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import static java.lang.Thread.sleep;
import java.net.*;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.WebSocket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import static sds.SDS.latch;
import static sds.SDS.clientEndPoint;
import static sds.SDS.parametro_url;

/**
 *
 * @author DavideLenovo
 */
public class SDS {

    static String parametro_url;
    static WebSocket clientEndPoint;
    static CountDownLatch latch;

    public static void main(String[] args) throws PrinterException, IOException, ParserConfigurationException, SAXException, URISyntaxException, InterruptedException {

        if (args.length != 1) {
            JOptionPane.showMessageDialog(null, "EXIT");
            System.exit(0);
        } else {
            parametro_url = args[0];
            JOptionPane.showMessageDialog(null, parametro_url);
        }

        Timer timer = new Timer();

        try {
            /*WebSocket da implementare*/
            latch = new CountDownLatch(1);
            clientEndPoint = HttpClient
                    .newHttpClient()
                    .newWebSocketBuilder()
                    .buildAsync(URI.create("ws://192.168.0.168:9000"), new Main.WebSocketClient(latch))
                    .join();
            //latch.await();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERRORE: SOCKET SPENTO. ESECUZIONE PROGRAMMA TERMINATA. RIAVVIARE MANUALMENTE");
            System.exit(0);
        }

        timer.schedule(new RichiestaPHP(), 0, 3000);

    }

}

class RichiestaPHP extends TimerTask {

    RichiestaPHP() {
        /*this.parametro_url = parametro_url;*/
    }

    @Override
    public void run() {

        try {

            URL url = new URL(parametro_url);

            System.out.println("TIMER");
            String urlParameters = "ConnectionType=GetRequest";
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Content-Length", Integer.toString(urlParameters.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");
            connection.setUseCaches(false);
            connection.setDoOutput(true);

            try ( //Invio dati di connessione al buffer in uscita (ovvero all'url)
                     DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
                wr.writeBytes(urlParameters);
            }

            //Ricezione risposta da URL
            InputStream is = connection.getInputStream();
            StringBuilder response;
            try ( BufferedReader rd = new BufferedReader(new InputStreamReader(is))) {
                response = new StringBuilder(); // or StringBuffer if Java version 5+
                String line;
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                    response.append('\r');
                }
            } // or StringBuffer if Java version 5+

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(new InputSource(new StringReader(response.toString())));

            //opzionale, ma raccomandato per normalizzare il parsing XML
            //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("TAVOLI");

            String data_convertita = "";
            String tavolo = "";
            String tavolotxt = "";
            String BIS_Corrente = "";

            /* Qua legge la prima riga e prende la data della cartella e il tavolo per vedere se il file esiste, o per crearlo */
            for (int temp = 0; temp < 1; temp++) {

                Node nNode = nList.item(temp);

                //System.out.println("\nCurrent Element :" + nNode.getNodeName());
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

                    BIS_Corrente = eElement.getElementsByTagName("BIS").item(0).getTextContent();

                    tavolo = eElement.getElementsByTagName("TAVOLO").item(0).getTextContent();

                    while (tavolo.charAt(0) == '0' && tavolo.length() > 1) {
                        tavolo = tavolo.substring(1);
                    }

                    tavolotxt = eElement.getElementsByTagName("TAVOLOTXT").item(0).getTextContent();

                    String data = eElement.getElementsByTagName("DATA").item(0).getTextContent();
                    data_convertita = data.substring(6, 10) + "-" + data.substring(3, 5) + "-" + data.substring(0, 2);

                    //esempio: 2021-02-12
                }
            }

            /*qui mi trovo ad avere BIS_Corrente,tavolo,tavolo,data e data_convertita
            qua vede se il file con quel tavolo e data esiste  */
            Date date = Calendar.getInstance().getTime();
            DateFormat dateFormat = new SimpleDateFormat("hhmmss");
            DateFormat oraAperturaTavolo = new SimpleDateFormat("hh:mm");
            DateFormat idComanda = new SimpleDateFormat("yyyyMMddhhmmss");
            DateFormat dataComandaServizio = new SimpleDateFormat("yyyy-MM-dd");

            System.out.println("QUERY 1");

            try ( Connection connectionSqlite = DriverManager.getConnection("jdbc:sqlite:C:/TAVOLI/fast.intellinet/DATABASE_CLIENTE.sqlite")) {
                connectionSqlite.setAutoCommit(true);

                ResultSet rs;
                String testo_query = "";

                testo_query = "select occupato from tavoli where numero='" + tavolo + "' limit 1;";

                /* ------------------------------------ */
                while (true) {
                    rs = connectionSqlite.createStatement().executeQuery(testo_query);

                    String tavolo_occupato = "";

                    if (rs.next()) {
                        if (rs.getString("occupato").equals("1")) {
                            tavolo_occupato = "1";
                        }
                    }

                    /*qua testa se il tavolo è occupato*/
                    if (tavolo_occupato.equals("1")) {
                        sleep(5000);
                    } else {
                        break;
                    }
                }

                /*------------------------------------------*/

 /* inizio  invio risposta al server */
 /* questo lo deve continuare a fare*/
                String response1 = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><PrintResponseInfo Version=\"1.00\"><response battery=\"0\" code=\"\" status=\"251658262\" success=\"true\" xmlns=\"http://www.epson-pos.com/schemas/2011/03/epos-print\"/></PrintResponseInfo>";

                String urlParameters1 = "ConnectionType=SetResponse&ResponseFile=" + URLEncoder.encode(response1, "UTF-8");

                HttpURLConnection connection1 = null;

                try {
                    //Create connection
                    URL url1 = new URL(parametro_url);

                    connection1 = (HttpURLConnection) url1.openConnection();
                    connection1.setRequestMethod("POST");

                    connection1.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    connection1.setRequestProperty("Content-Length", Integer.toString(urlParameters1.getBytes().length));

                    connection1.setUseCaches(false);
                    connection1.setDoOutput(true);

                    //Send request
                    DataOutputStream wr1 = new DataOutputStream(connection1.getOutputStream());
                    wr1.writeBytes(urlParameters1);
                    wr1.close();

                    //Get Response
                    InputStream is1 = connection1.getInputStream();
                    BufferedReader rd1 = new BufferedReader(new InputStreamReader(is1));
                    StringBuilder resp2 = new StringBuilder(); // or StringBuffer if Java version 5+
                    String line1;
                    while ((line1 = rd1.readLine()) != null) {
                        resp2.append(line1);
                        resp2.append('\r');
                    }
                    rd1.close();

                } catch (Exception e) {
                    e.printStackTrace();

                    //NIENTE
                } finally {
                    if (connection1 != null) {
                        connection1.disconnect();
                    }
                }

                /* fine invio risposta al server */
 /*file del tavolo*/
 /*file di stampa per SDS*/
                //File file1 = new File("C:\\TAVOLI\\DATI\\" + data_convertita + "\\SDS\\PDA21_" + strDate + "-" + BIS_Corrente + ".xml");
                String stato_record = "";

                testo_query = "select stato_record from comanda where ntav_comanda='" + tavolo + "' and stato_record='ATTIVO' limit 1;";

                rs = connectionSqlite.createStatement().executeQuery(testo_query);

                if (rs.next()) {
                    if (rs.getString("stato_record").equals("ATTIVO")) {
                        stato_record = "ATTIVO";
                    }
                } else {

                }

                String PRG = "000";

                String ULTIMO_NODO_UTILE = "";

                if (stato_record.equals("ATTIVO")) {

                    /*per vedere qual'era l'ultimo prg interno*/
                    testo_query = "select prog_inser from comanda where ntav_comanda='" + tavolo + "' and stato_record='ATTIVO' order by cast(prog_inser as int) desc limit 1;";
                    rs = connectionSqlite.createStatement().executeQuery(testo_query);
                    if (rs.next()) {
                        PRG = rs.getString("prog_inser");
                    } else {

                    }

                } else {
                }

                testo_query = "";

                /*inizializzo prima del ciclo cosi se sono varianti si tiene sti dati in memoria*/
                String reparto_servizi = "";
                String dest_st_1 = "";
                String portata = "";
                String perc_iva_base = "";

                for (int temp = 0; temp < nList.getLength(); temp++) {

                    Node nNode = nList.item(temp);

                    System.out.println("\nCurrent Element: " + nNode.getNodeName());

                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                        Node nodoCopiato = nNode.cloneNode(true);

                        Element eElement = (Element) nodoCopiato;

                        if ("T".equals(eElement.getElementsByTagName("TC").item(0).getTextContent())) {
                        } else {

                            String prog_inser = "";
                            String nodo = eElement.getElementsByTagName("NODO").item(0).getTextContent();

                            if (stato_record.equals("ATTIVO")) {

                                PRG = String.format("%03d", Integer.parseInt(PRG) + 1);

                                String tempNodo = "";

                                if (nodo.length() == 3) {

                                    ULTIMO_NODO_UTILE = PRG;

                                    tempNodo = ULTIMO_NODO_UTILE;

                                } else {

                                    tempNodo = ULTIMO_NODO_UTILE + " " + PRG;

                                }

                                prog_inser = PRG;
                                nodo = tempNodo;
                            } else {
                                prog_inser = eElement.getElementsByTagName("PRG").item(0).getTextContent();
                            }

                            String desc_art = eElement.getElementsByTagName("DES").item(0).getTextContent();
                            String prezzo_un = eElement.getElementsByTagName("PRZ").item(0).getTextContent();
                            String quantita = eElement.getElementsByTagName("QTA").item(0).getTextContent();
                            String art_variante = eElement.getElementsByTagName("ART2").item(0).getTextContent();
                            String autoriz = eElement.getElementsByTagName("AUTORIZ").item(0).getTextContent();
                            String categoria = "";

                            //-----------------------------------------------------------------------------------
                            String contiene_variante = "";
                            if (autoriz.equals("S")) {
                                contiene_variante = "1";
                            }

                            testo_query = "select * from prodotti where id='" + art_variante + "' limit 1";

                            rs = connectionSqlite.createStatement().executeQuery(testo_query);

                            if (rs.next()) {
                                reparto_servizi = rs.getString("reparto_servizi");
                                System.out.println("REPARTO_SERVIZI 1 " + reparto_servizi);

                                dest_st_1 = rs.getString("dest_st_1");
                                System.out.println("DEST_STAMPA 1 " + dest_st_1);

                                categoria = rs.getString("categoria");
                                System.out.println("CATEGORIA 1 " + categoria);

                                portata = rs.getString("portata");
                                System.out.println("PORTATA 1 " + portata);

                                perc_iva_base = rs.getString("perc_iva_base");
                                System.out.println("PERCENTUALE IVA BASE 1 " + perc_iva_base);
                            } else {

                            }

                            boolean result = true;

                            while (result) {

                                try {
                                    testo_query = "insert into comanda (contiene_variante,perc_iva,portata,categoria,dest_stampa,id,ntav_comanda,prog_inser,nodo,desc_art,prezzo_un,quantita,stato_record,posizione,tipo_record,terminale,numero_servizio,data_servizio,data_comanda,reparto) values ('" + contiene_variante + "','" + perc_iva_base + "','" + portata + "','" + categoria + "','" + dest_st_1 + "','FOR_" + idComanda.format(date) + "','" + tavolo + "','" + prog_inser + "','" + nodo + "','" + desc_art + "','" + prezzo_un + "','" + quantita + "','ATTIVO','CONTO','CORPO','SERVER FASTORDER','1','" + dataComandaServizio.format(date) + "','" + dataComandaServizio.format(date) + "','" + reparto_servizi + "');";

                                    System.out.println("QUERY :" + testo_query);

                                    connectionSqlite.createStatement().executeUpdate(testo_query);

                                    result = false;

                                    System.out.println("QUERY ESEGUITA");

                                } catch (SQLException e) {
                                    sleep(1000);
                                }

                            }

                            System.out.println("INVIO SOCKET 1");

                            clientEndPoint.sendText("{\"tipo\":\"aggiornamento_tavolo\",\"operatore\":\"FASTORDER\",\"query\":\"" + testo_query + "\",\"terminale\":\"null-X\",\"ip\":\"X\"}", true);
                            //latch.await();
                            sleep(200);

                            System.out.println("SOCKET INVIATO 1");
                        }
                    }
                    System.out.println("FINE IF CURRENT ELEMENT");
                }

                /*metto il tavolo rosso */
                testo_query = "update tavoli set colore='0',ora_apertura_tavolo='" + oraAperturaTavolo.format(date) + "' where numero='" + tavolo + "';";

                System.out.println("QUERY :" + testo_query);

                connectionSqlite.createStatement().execute(testo_query);

                System.out.println("QUERY ESEGUITA");

                clientEndPoint.sendText("{\"tipo\":\"tavolo_occupato\",\"operatore\":\"FASTORDER\",\"query\":\"" + testo_query + "\",\"terminale\":\"null-X\",\"ip\":\"X\"}", true);
                //latch.await();
                sleep(200);

                System.out.println("SOCKET INVIATO 1");

                clientEndPoint.sendText("{\"tipo\":\"stampa_comanda_fastorder\",\"operatore\":\"FASTORDER\",\"query\":\"" + tavolo + "\",\"terminale\":\"null-X\",\"ip\":\"X\"}", true);
                //latch.await();
                sleep(200);

                System.out.println("SOCKET INVIATO 1");

                System.out.println("FINE CICLO 1");

                /*planing e tavolo occupato */
                connectionSqlite.close();
            }

        } catch (IOException | ParserConfigurationException | DOMException | SAXException e) {
        } catch (SQLException ex) {
            Logger.getLogger(RichiestaPHP.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(RichiestaPHP.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
