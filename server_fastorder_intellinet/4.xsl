<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:x="http://www.epson-pos.com/schemas/2011/03/epos-print">
<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" />


  
  
  
  <xsl:template match="//x:text">
    
    
    
    <xsl:for-each select=".">
      
      <span><xsl:value-of select="."/></span>
      
    </xsl:for-each>
      
    
  </xsl:template>
  
 
</xsl:stylesheet>