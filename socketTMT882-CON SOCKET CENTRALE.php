﻿<?php
ini_set('memory_limit', '4M');

$array_ultimidiecimilainvii = array();

//error_reporting(E_ALL);

set_time_limit(0);

$IP = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : gethostbyname(gethostname());


$host = '192.168.0.215'; //$_SERVER['SERVER_ADDR']; //host
$port = 9000; //port
$null = NULL;
$read = NULL;
$except = NULL;

echo $host . '\r\n';

function is_connected() {

    $connected = @fsockopen("192.168.0.254", 80);
//website, port  (try 80 or 443)
//se è connesso e sono passati almeno 15 secondi dall'ultima riconnessione segna connesso
    if ($connected) {
        $is_conn = true; //action when connected
        fclose($connected);
    } else {
        $is_conn = false; //action in connection failure
    }
    return $is_conn;
}

function millitime() {
    $microtime = microtime();
    $comps = explode(' ', $microtime);

    // Note: Using a string here to prevent loss of precision
    // in case of "overflow" (PHP converts it to a double)
    return sprintf('%d%03d', $comps[1], $comps[0] * 1000);
}

//mittente è un socket aperto di chi ha fatto la richiesta
function send_message($msg, $terminale = 'nd', $mittente = 0, $ipdest = 0) {
    global $clients, $array_ultimidiecimilainvii;
    if (is_connected() === true) {

        if ($ipdest !== 0 && $mittente !== 0) {
            foreach ($clients as $changed_socket) {
                $ip = '';
                socket_getpeername($changed_socket, $ip);

                if ($ip === $ipdest) {
                    socket_write($changed_socket, $msg, strlen($msg));
                }
            }

            socket_write($mittente, $msg, strlen($msg));

            //@socket_write($ipdest, $msg, strlen($msg));
        } else if ($mittente !== 0) {

            socket_write($mittente, $msg, strlen($msg));
        } else {

            //condizione normale
            foreach ($clients as $changed_socket) {
                @socket_write($changed_socket, $msg, strlen($msg));
            }
            //serve a memorizzare gli ultimi 10mila invii ai socket su un array
            //BUG CONTINUA AD ACCUMULARE MEMORIA INVECE DI SOVRASCRIVERE I PRIMI INDICI
            //var_dump($msg);

            if (strpos($msg, '"bip"') === FALSE) {
                /* echo "ADD"; */
                //var_dump($msg);
                array_unshift($array_ultimidiecimilainvii, array($msg, millitime(), $terminale));
                $array_ultimidiecimilainvii = array_slice($array_ultimidiecimilainvii, 0, 1000);
            }
        }

        return true;
    } else {
        /* echo "F"; */
        return false;
    }
}

function invia_a_db_centrale($sql) {

    if ($tst_msg->tipo !== "stampa_comanda" && $tst_msg->query !== "X") {

        $prefix = "C:/TAVOLI/fast.intellinet/";

        $db_name = "DATABASE_CLIENTE.sqlite";

        $db2 = new SQLite3($prefix . $db_name);

        $result = false;

        $i = 0;
        while ($result !== true && $i < 10) {

            $ms = "0." . rand(6, 9);
            sleep($ms);
            if (strpos(strtolower($sql), 'select') !== false && strpos(strtolower($sql), 'delete') === false) {
                /* echo "query"; */
                $result = $db2->query($sql);
            } else {
                /* echo "exec"; */
                $result = $db2->exec($sql);
            }

            //var_dump($result);
            //QUI E' DIVERSO DA FALSE, PERCHE' UNA QUERY NON E' COME UN EXEC CHE RESTITUISCE TRUE
            //MA COMUNQUE NON RESTITUISCE FALSE, MA BENSI' UN OGGETTO, SE VA A BUON FINE
            if ($result !== false) {
                $risultato_booleano = "true";

                break;
            }

            $risultato_booleano = "false";
            //FALSE DOVREBBE ESSERE SEMPRE UN ERRORE, TEORICAMENTE (VEDI http://php.net/manual/en/sqlite3.query.php)
            //Returns an SQLite3Result object if the query returns results. Otherwise, returns TRUE if the query succeeded, FALSE on failure.

            $i++;
        }


        $db2->close();
    }
}

function sincronia_orario($socket) {

    $time_object = new stdClass();
    $time_object->type = 'time';
    $time_object->message = millitime();

    send_message(mask(json_encode($time_object)), $socket);
}

function invia_dati_post_time($time, $terminale, $socket) {
    global $array_ultimidiecimilainvii;

    $array_ultimidiecimilainvii_reverse = array_reverse($array_ultimidiecimilainvii);

    foreach ($array_ultimidiecimilainvii_reverse as $a) {

        //Se il tempo degli array inviati è maggiore dell'ultimo messaggio che ho ricevuto e si verificano altre conditions
        if ($a[1] > $time && $terminale !== $a[2] && $a[2] !== 'nd') {
            /* echo '\r\n';
              echo '\r\n';
              echo $a[0];
              echo $a[1];
              echo $a[2];
              echo '\r\n'; */

            send_message($a[0], $terminale, $socket);
        }
    }
}

//Unmask incoming framed message
function unmask($text) {
    $length = ord($text[1]) & 127;
    if ($length == 126) {
        $masks = substr($text, 4, 4);
        $data = substr($text, 8);
    } elseif ($length == 127) {
        $masks = substr($text, 10, 4);
        $data = substr($text, 14);
    } else {
        $masks = substr($text, 2, 4);
        $data = substr($text, 6);
    }
    $text = "";
    for ($i = 0; $i < strlen($data); ++$i) {
        $text .= $data[$i] ^ $masks[$i % 4];
    }
    return $text;
}

//Encode message for transfer to client.
function mask($text) {
    $b1 = 0x80 | (0x1 & 0x0f);
    $length = strlen($text);

    if ($length <= 125)
        $header = pack('CC', $b1, $length);
    elseif ($length > 125 && $length < 65536)
        $header = pack('CCn', $b1, 126, $length);
    elseif ($length >= 65536)
        $header = pack('CCNN', $b1, 127, $length);
    return $header . $text;
}

//handshake new client.
function perform_handshaking($receved_header, $client_conn, $host, $port) {
    if (is_connected() === true) {
        $headers = array();
        $lines = preg_split("/\r\n/", $receved_header);
        foreach ($lines as $line) {
            $line = chop($line);
            if (preg_match('/\A(\S+): (.*)\z/', $line, $matches)) {
                $headers[$matches[1]] = $matches[2];
            }
        }

//var_dump($headers);

        $secKey = $headers['Sec-WebSocket-Key'];

        $secAccept = base64_encode(pack('H*', sha1($secKey . '258EAFA5-E914-47DA-95CA-C5AB0DC85B11')));
//hand shaking header
        $upgrade = "HTTP/1.1 101 Web Socket Protocol Handshake\r\n" .
                "Upgrade: websocket\r\n" .
                "Connection: Upgrade\r\n" .
                "WebSocket-Origin: $host\r\n" .
                "WebSocket-Location: ws://$host:$port/socket_listener_1.php\r\n" .
                "Sec-WebSocket-Accept:$secAccept\r\n\r\n";
        socket_write($client_conn, $upgrade, strlen($upgrade));
        /* echo "Handshake"; */
    }
}

$ultimotimestamp = time();
//----------------------------------------------------------------------------------
//Così dovrebbe ripartire il socket in caso di crash
//Crea collegamento TCP/IP
$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

//Se non ci si può connettere all'indirizzo del socket, quindi il server è chiuso
//riparte tutto se il server non si connette
/* -E- */ while (@socket_connect($socket, $host, $port) === FALSE) {




//Parte un nuovo server
    echo "SOCKET SERVER STARTED\n\n";

//Porta riutilizabile (DEFAULT SI)
    socket_set_option($socket, SOL_SOCKET, SO_REUSEADDR, 1);
//Crea collegamento TCP/IP
    $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

//Lega il socket ad un indirizzo specifico
    if (socket_bind($socket, $host, $port) !== FALSE) {

//mette il socket server in ascolto
        socket_listen($socket);

//crea, e aggiunge il socket server alla lista
        $clients = array($socket);

//comincia un loop che non si ferma mai
        while (true) {
            //echo "W";
//la variabile changed prende la lista dei clienti (nel primo caso solo il server)
            $changed = $clients;


//La funzione socket_select() accetta un array di socket e si mette in attesa di una variazione di stato su questi. 
//Questa, derivando come background dai socket BSD, riconoscerà che questi array di risorse socket sono in realtà 
//dei set di descrittori di file. Saranno monitorati 3 set di socket.
            socket_select($changed, $read, $except, 1, 0);

//se in socket ci sono nuovi clients
            if (in_array($socket, $changed)) {

                $socket_new = socket_accept($socket); //accetta il nuovo socket (di default)

                $header = socket_read($socket_new, 2048); //legge l'header del nuovo client, al quale inviare i dati

                array_push($clients, $socket_new); //aggiunge alla lista dei clients il nuovo collegamento


                if (!empty($header)) { //Se l'header non è vuoto
                    perform_handshaking($header, $socket_new, $host, $port); //fa un nuovo handshake, ossia una sorta di codifica dei dati

                    socket_getpeername($socket_new, $ip); //prende l'ip dal socket connesso

                    $response = mask(json_encode(array('type' => 'system', 'message' => $ip . ' connected', 'orario' => millitime()))); //prepare json data

                    /* var_dump($response); */

                    send_message($response); //notify all users about new connection
                }



                $found_socket = array_search($socket, $changed); //cerca array socket dentro changed

                unset($changed[$found_socket]); //elimina l'indice trovato dall'array
            }




            if (time() - $ultimotimestamp >= 1) {
                $start_memory = memory_get_usage();

                $response = mask(json_encode(array('type' => 'bip', 'message' => "1", "orario" => millitime())));
                /* echo "\nVU1:" . (memory_get_usage() - $start_memory) . '\n\n'; */


                /* echo "\nMemory Allocated:" . memory_get_usage(true) . "\n";
                  echo "\nMemory Peak:" . memory_get_peak_usage(false) . "\n";
                  echo "\nMemory Peak Allocated:" . memory_get_peak_usage(true) . "\n"; */


                //echo "\nbip\n";

                $start_memory = memory_get_usage();
                send_message($response); //notify all users about new connection
                /* echo "\nVU2:" . (memory_get_usage() - $start_memory) . '\n\n'; */


                $ultimotimestamp = time();


                /* echo "\nGeneral Memory Usage:" . memory_get_usage(false) . "\n"; */
            }




            //var_dump($changed );
//ciclo dei socket connessi
            foreach ($changed as $changed_socket) {

                /* var_dump($clients); */
                //$ip2 = "000";
                //socket_getpeername($changed_socket, $ip_2);
                //echo "R" . $ip_2 . "-";
//scrive i dati del socket in un buffer di 2048 bytes
                /* -E- */ //while (socket_recv($changed_socket, $buf, 2048, 0) > 0) {
                while ($bytes = socket_recv($changed_socket, $buf, 2048, 0) > 1) {
//while (!false === ($buf = socket_read($changed_socket, 2048, PHP_BINARY_READ))) {
//var_dump($bytes);
//echo '\n\n';

                    $received_text = unmask($buf); //smaschera dati buffer

                    $tst_msg = json_decode($received_text); //decodifica in json 
//var_dump($tst_msg);


                    if ($tst_msg !== NULL) {

                        var_dump($tst_msg);

                        $tst_msg->orario = millitime();


                        if (!empty($tst_msg->ip_dest)) {

                            echo "\n\nA\n\n";

                            $response_text = mask(json_encode($tst_msg)); //lo rimaschera
                            /* Test del 03/04/2019 */

                            send_message($response_text, $tst_msg->terminale, $changed_socket, $tst_msg->ip_dest); //invia il dato a $changed_socket, che è una risorsa socket, e anche a chi l'ha inviato per la verifica
                            //echo $response_text . " to " . $tst_msg->ip_dest . "\n\n"; //Mostra il dato inviato a schermo
                        } else if ($tst_msg->tipo === "richiesta_aggiornamento") {

                            echo "\n\nB\n\n";

                            invia_dati_post_time($tst_msg->query, $tst_msg->terminale, $changed_socket);
                            $response_text = mask(json_encode($tst_msg));
                            //questo è solo per verifica
                            send_message($response_text, $tst_msg->terminale, $changed_socket);
                        } else if ($tst_msg->tipo === "time") {



                            sincronia_orario($changed_socket);
                        } else {

                            echo "\n\nC\n\n";

                            /* if (true) { */
                            /* echo "\n\n";
                              echo "\n\nSeguente4:";


                              var_dump($tst_msg->query);
                              echo "\n\n"; */


                            invia_a_db_centrale($tst_msg->query);

                            /* } */

                            $response_text = mask(json_encode($tst_msg));
                            send_message($response_text, $tst_msg->terminale);
                        }
                    }

                    break 2; //esce fino al ciclo while (di 2 livelli sopra)
                }

//il buffer legge il socket di prima, fino a 2048 bytes
                /* -E- */ //if (socket_recv($changed_socket, $buf, 2048, 0) === FALSE) { // se il $buf è false, quindi si presuppone che il client sia disconnesso

                if ($bytes = socket_recv($changed_socket, $buf, 2048, 0) === FALSE) {
                    $found_socket = array_search($changed_socket, $clients);
                    socket_getpeername($changed_socket, $ip);
                    /* echo "-ELIM" . $ip . "-"; */
                    unset($clients[$found_socket]); // rimuove il client dall'array dei clients
                }

//sleep(0.05);
            }
        }
        socket_close($socket); //in caso di errore del ciclo si chiude il socket in ascolto
    } else {
        echo "\nRTENGINE è stato riaperto troppo presto. Bisogna aspettare che la porta si liberi.\n";

        sleep(1);
        echo "....";
        sleep(1);
        echo "....";
        sleep(1);
        echo "....";
        sleep(1);
        echo "....";
        sleep(1);
        echo "....";
        sleep(1);
        echo "....";
        sleep(1);
        echo "....";
        sleep(1);
        echo "....";
        sleep(1);
        echo "....";
        sleep(1);
        echo "....";
        sleep(1);
        echo "....";
        sleep(1);
        echo "....";
        sleep(1);
        echo "....";
        sleep(1);
        echo "....";
        sleep(1);
    }
    socket_close($socket);
}
socket_close($socket);
