<?php
header('Access-Control-Allow-Origin: *');  

//SOLO ERRORI VERI NON WARNING ALTRIMENTI ALLA PRIMA DA ERRORE
error_reporting(E_ERROR);

function incrementa($search) {


    //DATA INTERA
    $data_intera = date("d M Y H:i:s");

    $debug = 'DATA INTERA:' . $data_intera;
    $debug .= '<br/><br/>';

    //ANNO CORRENTE DALLA CASSA
    $anno_corrente = date('Y');
    //$anno_corrente = "2016";

    $debug .= 'ANNO CORRENTE:' . $anno_corrente;
    $debug .= '<br/><br/>';

    //FILENAME (SEMPRE VALIDO
    $filename = 'p' . $anno_corrente . '.dll';

    $debug .= 'NOME FILE:' . $filename;
    $debug .= '<br/><br/>';


    //HANDLER DI LETTURA
    if (fopen($filename, 'r') !== false) {
        $fp = fopen($filename, 'r');
        $debug .= 'APERTURA FILE IN LETTURA';
        $debug .= '<br/><br/>';
        //FREAD FUNZIONA PER TUTTO IL FILE
        $contents = fread($fp, filesize($filename));
    } else {
        $fp = fopen($filename, 'w+');
        $debug .= 'CREAZIONE NUOVO FILE';

        $nuovo_file = ',FATTURA:0,DATA_FATTURA:0,ANNO_PROGRESSIVI:' . $anno_corrente . ',';

        $debug .= '<br/><br/>';
        //FREAD FUNZIONA PER TUTTO IL FILE
        $contents = $nuovo_file;
    }




    $debug .= 'CONTENUTO:' . $contents;
    $debug .= '<br/><br/>';

    //IMPOSTA IL BLOCCO SU FILE SE GIA' NON E' BLOCCATO
    if (!flock($fp, LOCK_EX | LOCK_NB)) {

        $debug .= 'OCCUPATO - RITENTATIVO IN CORSO...';
        $debug .= '<br/>';
        $debug .= '<br/>';


        sleep('0.' . rand(1, 5));
        incrementa($search);
    } else {
        $debug .= 'BLOCCATO';
        $debug .= '<br/>';
        $debug .= '<br/>';

        $fp = fopen($filename, 'w');

        $debug .= 'APERTURA FILE IN SCRITTURA';
        $debug .= '<br/><br/>';


        //LETTURA E CAMBIO PROGRESSIVO
        $pos_start = strpos($contents, $search);
        $pos_start+=strlen($search . ':');

        $pos_end = strpos($contents, ',', $pos_start);

        $length = $pos_end - $pos_start;

        $dato_finale = substr($contents, $pos_start, $length);

        $debug .= 'ESTRATTO:' . $dato_finale;
        $debug .= '<br/>';

        $progressivo_fiscale = $dato_finale + 1;

        //FINE ESTRAZIONE
        //INCREMENTAZIONE PROGRESSIVO DELLA RICHIESTA
        $debug .= 'INCREMENTAZIONE:' . $progressivo_fiscale;
        $debug .= '<br/>';
        $debug .= '<br/>';

        //IMPOSTA PROGRESSIVO
        $contents = str_replace(',' . $search . ':' . $dato_finale . ',', ',' . $search . ':' . $progressivo_fiscale . ',', $contents);

        //----------------------
        //LETTURA E CAMBIO DATA
        $pos_start = strpos($contents, 'DATA_' . $search);
        $pos_start+=strlen('DATA_' . $search . ':');

        $pos_end = strpos($contents, ',', $pos_start);

        $length = $pos_end - $pos_start;

        $dato_finale = substr($contents, $pos_start, $length);

        //IMPOSTA DATA
        $contents = str_replace(',DATA_' . $search . ':' . $dato_finale . ',', ',DATA_' . $search . ':' . $data_intera . ',', $contents);

        fwrite($fp, $contents);

        $debug .= 'FILE RISCRITTO';
        $debug .= '<br/>';
        $debug .= '<br/>';
        //FINE INCREMENTAZIONE
    }

    $debug .= 'SBLOCCATO';
    $debug .= '<br/>';
    $debug .= '<br/>';


    fclose($fp);

    $debug .= 'CHIUSO';
    $debug .= '<br/>';
    $debug .= '<br/>';

    //echo $debug;

    echo $progressivo_fiscale;

    //echo "FIN QUA CI SIAMO";
}

//MAIN
//----------------------------------------------------------------------------
//IMPOSTAZIONE RICHIESTA
/* if (!empty(filter_input(INPUT_GET, 'richiesta', FILTER_SANITIZE_STRING))) {
  $richiesta = filter_input(INPUT_GET, 'richiesta', FILTER_SANITIZE_STRING); */

if (!empty($_POST['richiesta'])) {
    $richiesta=$_POST['richiesta'];
} else {
    echo "ERRORE";
    exit(-1);
}

$array_risultati = incrementa($richiesta);

