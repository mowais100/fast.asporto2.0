/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sdsFastcomanda;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import java.awt.print.PrinterException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.InetSocketAddress;
import java.util.HashMap;
import javax.swing.JTextPane;
import java.util.Map;
import java.util.Set;
import com.sun.org.apache.xml.internal.utils.WrappedRuntimeException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.StringWriter;
import java.io.Writer;

import java.net.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.security.Timestamp;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import static jdk.nashorn.internal.objects.NativeRegExp.source;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import java.util.Date;
import java.util.List;
import javax.xml.transform.TransformerException;
import static sdsFastcomanda.SDSFastcomanda.data;
import static sdsFastcomanda.SDSFastcomanda.data_convertita;

import static sdsFastcomanda.SDSFastcomanda.parametro_url;
import static sdsFastcomanda.SDSFastcomanda.parametro_url_clienti;
import static sdsFastcomanda.SDSFastcomanda.parametro_url_domiciliazione;
import static sdsFastcomanda.SDSFastcomanda.servizio;
import static sdsFastcomanda.SDSFastcomanda.tavolo;

import static sdsFastcomanda.InterfacciamentoFastComanda.attivaTSE;
import static sdsFastcomanda.SDSFastcomanda.ivaDefault;

/**
 *
 * @author DavideLenovo
 */
public class SDSFastcomanda {

    static String data;
    static String data_convertita;
    static String printerNameDesired;
    static String parametro_url;
    static String parametro_url_clienti;
    static String parametro_url_domiciliazione;
    static String tavolo;
    static String servizio;
    static String ivaDefault;
    static String campo_iva_default_articolo;
    static String nomePc;
    static String tipo_profilo;

    public static class MyHandler {

        private static Document toXmlDocument(String str) throws ParserConfigurationException, SAXException, IOException {

            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document document = docBuilder.parse(new InputSource(new StringReader(str.trim().replaceFirst("^([\\W]+)<","<"))));

            return document;
        }

        public static void stampa(String nome_stampante, String pagina_xml) throws ParserConfigurationException, SAXException, IOException {

            String response = "";

            JTextPane jtp = new JTextPane();

            jtp.setContentType("text/html");

            printerNameDesired = "ERRORE";

            if (nome_stampante.isEmpty()) {
            } else {
                printerNameDesired = nome_stampante;
            }

            Document doc = toXmlDocument(pagina_xml);

            //QUA DEVE ESTRARRE OGNI EPOS-PRINT E SEPARARE OGNI STAMPA
            NodeList nodi_stampe_da_fare = doc.getElementsByTagName("epos-print");

            int numero_stampe = nodi_stampe_da_fare.getLength();

            for (int i = 0; i < numero_stampe; i++) {

                String printer_name = doc.getElementsByTagName("devid").item(i).getTextContent();

                Node file_stampa = nodi_stampe_da_fare.item(i);

                Stampa stampa = new Stampa(file_stampa, printer_name);

                stampa.printer_job();

            }

            //response = "<?xml version=\"1.0\" encoding=\"utf-8\"?><s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"><s:Body><response success=\"true\" code=\"\" status=\"251658262\" battery=\"0\" xmlns=\"http://www.epson-pos.com/schemas/2011/03/epos-print\"></response></s:Body></s:Envelope>";
            response = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><PrintResponseInfo Version=\"1.00\"><response battery=\"0\" code=\"\" status=\"251658262\" success=\"true\" xmlns=\"http://www.epson-pos.com/schemas/2011/03/epos-print\"/></PrintResponseInfo>";

            String urlParameters = "ConnectionType=SetResponse&ResponseFile=" + URLEncoder.encode(response, "UTF-8");

            HttpURLConnection connection = null;

            try {
                //Create connection
                URL url = new URL(parametro_url);

                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");

                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("Content-Length", Integer.toString(urlParameters.getBytes().length));

                connection.setUseCaches(false);
                connection.setDoOutput(true);

                //Send request
                DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                wr.writeBytes(urlParameters);
                wr.close();

                //Get Response  
                InputStream is = connection.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                StringBuilder resp2 = new StringBuilder(); // or StringBuffer if Java version 5+
                String line;
                while ((line = rd.readLine()) != null) {
                    resp2.append(line);
                    resp2.append('\r');
                }
                rd.close();

                String resp3 = resp2.toString();

            } catch (Exception e) {
                e.printStackTrace();

                //NIENTE
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
            }

        }
    }

    public static void main(String[] args) throws PrinterException, IOException, ParserConfigurationException, SAXException {

        parametro_url = "";
        parametro_url_clienti = "";
        parametro_url_domiciliazione = "";
        servizio = "_1";
        ivaDefault = "19";
        campo_iva_default_articolo = "IVA";
        tipo_profilo = "T";

        InetAddress addr = InetAddress.getLocalHost();
        String hostname = addr.getHostName();
        nomePc = hostname;
        //JOptionPane.showMessageDialog(null, "Nome PC: " + nomePc);


        /*DAVIDE-LENOVO*/
 /*SERVER01*/
        PrintStream warnings = new PrintStream(new FileOutputStream("warnings.txt"));
        PrintStream errors = new PrintStream(new FileOutputStream("errors.txt"));

        System.setOut(warnings);
        System.setErr(errors);
        if (args.length == 0) {
            JOptionPane.showMessageDialog(null, "EXIT");
            System.exit(0);
        } else if (args.length == 1) {
            parametro_url = args[0];
            JOptionPane.showMessageDialog(null, "Nome PC: " + nomePc + " \r\nTipo profilo: " + tipo_profilo + " \r\nUrl: " + parametro_url + " \r\nUrl clienti: " + parametro_url_clienti + " \r\nUrl nomi: " + parametro_url_domiciliazione + " \r\nServizio: " + servizio + " \r\nTSE attivo: " + attivaTSE + " \r\nAliquota IVA default: " + ivaDefault + " \r\nCampo IVA default: " + campo_iva_default_articolo);
        } else if (args.length == 2) {
            parametro_url = args[0];
            parametro_url_clienti = args[1];
            JOptionPane.showMessageDialog(null, "Nome PC: " + nomePc + " \r\nTipo profilo: " + tipo_profilo + " \r\nUrl: " + parametro_url + " \r\nUrl clienti: " + parametro_url_clienti + " \r\nUrl nomi: " + parametro_url_domiciliazione + " \r\nServizio: " + servizio + " \r\nTSE attivo: " + attivaTSE + " \r\nAliquota IVA default: " + ivaDefault + " \r\nCampo IVA default: " + campo_iva_default_articolo);
        } else if (args.length == 3) {
            parametro_url = args[0];
            parametro_url_clienti = args[1];
            parametro_url_domiciliazione = args[2];
            JOptionPane.showMessageDialog(null, "Nome PC: " + nomePc + " \r\nTipo profilo: " + tipo_profilo + " \r\nUrl: " + parametro_url + " \r\nUrl clienti: " + parametro_url_clienti + " \r\nUrl nomi: " + parametro_url_domiciliazione + " \r\nServizio: " + servizio + " \r\nTSE attivo: " + attivaTSE + " \r\nAliquota IVA default: " + ivaDefault + " \r\nCampo IVA default: " + campo_iva_default_articolo);
        } else if (args.length == 4) {
            parametro_url = args[0];
            parametro_url_clienti = args[1];
            parametro_url_domiciliazione = args[2];
            tipo_profilo = args[3];

            /* se è delivery o se è altro */
            if (tipo_profilo.equalsIgnoreCase("D") == true) {
                ivaDefault = "7";
                campo_iva_default_articolo = "REP";
            }

            JOptionPane.showMessageDialog(null, "Nome PC: " + nomePc + " \r\nTipo profilo: " + tipo_profilo + " \r\nUrl: " + parametro_url + " \r\nUrl clienti: " + parametro_url_clienti + " \r\nUrl nomi: " + parametro_url_domiciliazione + " \r\nServizio: " + servizio + " \r\nTSE attivo: " + attivaTSE + " \r\nAliquota IVA default: " + ivaDefault + " \r\nCampo IVA default: " + campo_iva_default_articolo);

        } else if (args.length == 5) {
            parametro_url = args[0];
            parametro_url_clienti = args[1];
            parametro_url_domiciliazione = args[2];
            tipo_profilo = args[3];
            servizio = "_" + args[4];

            /* se è delivery o se è altro */
            if (tipo_profilo.equalsIgnoreCase("D") == true) {
                ivaDefault = "7";
                campo_iva_default_articolo = "REP";
            }

            JOptionPane.showMessageDialog(null, "Nome PC: " + nomePc + " \r\nTipo profilo: " + tipo_profilo + " \r\nUrl: " + parametro_url + " \r\nUrl clienti: " + parametro_url_clienti + " \r\nUrl nomi: " + parametro_url_domiciliazione + " \r\nServizio: " + servizio + " \r\nTSE attivo: " + attivaTSE + " \r\nAliquota IVA default: " + ivaDefault + " \r\nCampo IVA default: " + campo_iva_default_articolo);

        }

        Timer timer = new Timer();
        timer.schedule(new RichiestaPHP(args), 0, 3000);

    }

}

class RichiestaPHP extends TimerTask {

    private final String[] args;

    RichiestaPHP(String[] args) {
        this.args = args;
    }

    @Override
    public void run() {

        String progressivoAsporto = "0";

        String urlParameters = "ConnectionType=GetRequest";

        HttpURLConnection connection = null;

        try {
            //Create connection
            URL url = new URL(parametro_url);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            connection.setRequestProperty("Content-Length",
                    Integer.toString(urlParameters.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoOutput(true);

            //Send request
            DataOutputStream wr = new DataOutputStream(
                    connection.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.close();

            //Get Response  
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
            String line;
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }

            /*if (!response.toString().isEmpty()) {
                //System.out.println(response);

                Calendar c1 = Calendar.getInstance();

                String fileName = "C:\\TAVOLI\\" + c1.getTimeInMillis() + ".xml";

                //System.out.println(fileName);
                BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
                writer.write(response.toString());
                writer.close();
            }*/

            rd.close();

            boolean is_fastcomanda_compatible = false;

            for (String arg : args) {
                if (arg.contains("xml_fastcomanda") == true) {
                    is_fastcomanda_compatible = true;
                }
            }

            if (is_fastcomanda_compatible == true) {
                if (!response.toString().isEmpty()) {
                    if (parametro_url_clienti.equalsIgnoreCase("")) {
                        /*niente*/
                        System.out.println("NO SINCRO CLIENTI");
                    } else {
                        System.out.println("SINCRO CLIENTI");
                        new SincroClienti();
                    }
                    System.out.println("FINE SINCRO CLIENTI");

                    try {

                        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

                        System.out.println("PARSE RESPONSE");
                       

                        Document doc = dBuilder.parse(new InputSource(new StringReader(response.toString().trim().replaceFirst("^([\\W]+)<","<"))));
                        System.out.println("FINE PARSE RESPONSE");

                        //optional, but recommended
                        //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
                        doc.getDocumentElement().normalize();

                        //System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
                        NodeList nList = doc.getElementsByTagName("TAVOLI");

                        //System.out.println("----------------------------");
                        data_convertita = "";
                        tavolo = "";
                        String tavolotxt = "";
                        String BIS_Corrente = "";

                        /* Qua legge la prima riga e prende la data della cartella e il tavolo per vedere se il file esiste, o per crearlo */
                        for (int temp = 0; temp < 1; temp++) {

                            Node nNode = nList.item(temp);

                            //System.out.println("\nCurrent Element :" + nNode.getNodeName());
                            if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                                Element eElement = (Element) nNode;

                                BIS_Corrente = eElement.getElementsByTagName("BIS").item(0).getTextContent();

                                tavolo = eElement.getElementsByTagName("TAVOLO").item(0).getTextContent();
                                tavolotxt = eElement.getElementsByTagName("TAVOLOTXT").item(0).getTextContent();

                                data = eElement.getElementsByTagName("DATA").item(0).getTextContent();
                                data_convertita = data.substring(6, 10) + "-" + data.substring(3, 5) + "-" + data.substring(0, 2) + servizio;

                                //System.out.println("Data: " + data_convertita);
                            }
                        }

                        //System.out.println("347");

                        /* qua vede se il file con quel tavolo e data esiste */
                        Date date = Calendar.getInstance().getTime();
                        DateFormat dateFormat = new SimpleDateFormat("hhmmss");
                        String strDate = dateFormat.format(date);

                        File f = new File("C:\\TAVOLI\\DATI\\" + data_convertita + "\\");
                        if (f.isDirectory()) {

                            //System.out.println("357");
                            File tavolo_occupato = new File("C:\\TAVOLI\\DATI\\" + data_convertita + "\\P" + tavolo + ".CO1");
                            if (tavolo_occupato.isFile()) {
                                JOptionPane.showMessageDialog(null, "fast.order sta tentanto di scaricare un ordine, ma il tavolo " + tavolo + "risulta occupato. Clicca OK quando sarai uscito o avrai sbloccato il tavolo.");
                            } else {

                                //System.out.println("364");

                                /* inizio  invio risposta al server */
                                String response1 = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><PrintResponseInfo Version=\"1.00\"><response battery=\"0\" code=\"\" status=\"251658262\" success=\"true\" xmlns=\"http://www.epson-pos.com/schemas/2011/03/epos-print\"/></PrintResponseInfo>";

                                String urlParameters1 = "ConnectionType=SetResponse&ResponseFile=" + URLEncoder.encode(response1, "UTF-8");

                                HttpURLConnection connection1 = null;

                                try {
                                    //Create connection
                                    URL url1 = new URL(parametro_url);

                                    connection1 = (HttpURLConnection) url1.openConnection();
                                    connection1.setRequestMethod("POST");

                                    connection1.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                                    connection1.setRequestProperty("Content-Length", Integer.toString(urlParameters1.getBytes().length));

                                    connection1.setUseCaches(false);
                                    connection1.setDoOutput(true);

                                    //Send request
                                    DataOutputStream wr1 = new DataOutputStream(connection1.getOutputStream());
                                    wr1.writeBytes(urlParameters1);
                                    wr1.close();

                                    //Get Response  
                                    InputStream is1 = connection1.getInputStream();
                                    BufferedReader rd1 = new BufferedReader(new InputStreamReader(is1));
                                    StringBuilder resp2 = new StringBuilder(); // or StringBuffer if Java version 5+
                                    String line1;
                                    while ((line1 = rd1.readLine()) != null) {
                                        resp2.append(line1);
                                        resp2.append('\r');
                                    }
                                    rd1.close();

                                    String resp3 = resp2.toString();
                                    //System.out.println(resp3);

                                } catch (Exception e) {
                                    e.printStackTrace();

                                    //NIENTE
                                } finally {
                                    if (connection1 != null) {
                                        connection1.disconnect();
                                    }
                                }

                                //System.out.println("415");
                                if (tavolotxt.contains("TAKE AWAY")) {
                                    String pattern = "yyyy-MM-dd";
                                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                                    String dateForm = simpleDateFormat.format(new Date());

                                    SQLDatabaseConnection SQLDb = new SQLDatabaseConnection();

                                    // "SELECT TOP 10 Title, FirstName, LastName from SalesLT.Customer";
                                    List<Map<String, Object>> rs = SQLDb.querySelect("SELECT PRG02=PRG02+1 FROM PROGRESSIVI WHERE DATA=CONVERT(datetime, '" + dateForm + "T00:00:00') AND CODICE='001'");

                                    for (Map<String, Object> map : rs) {
                                        for (Map.Entry<String, Object> entry : map.entrySet()) {
                                            String key = entry.getKey();
                                            Object value = entry.getValue();
                                            if (key.equalsIgnoreCase("PRG02")) {
                                                progressivoAsporto = value.toString();
                                                break;
                                            }
                                        }
                                    }

                                    tavolo = progressivoAsporto;
                                    tavolotxt = "TAKE AWAY/FO";

                                    //INSERT INTO SalesLT.Product (Name, ProductNumber, Color, StandardCost, ListPrice, SellStartDate) VALUES "+ "('NewBike', 'BikeNew', 'Blue', 50, 120, '2016-01-01')
                                    SQLDb.queryExecute("UPDATE PROGRESSIVI SET PRG02=(PRG02+1) WHERE DATA=CONVERT(datetime, '" + dateForm + "T00:00:00') and CODICE='001'");

                                }

                                //System.out.println("446");

                                /* fine invio risposta al server */
                                File file = new File("C:\\TAVOLI\\DATI\\" + data_convertita + "\\TAVOLO" + tavolo + ".xml");

                                File file1 = null;

                                if (BIS_Corrente.equalsIgnoreCase("") == true) {
                                    file1 = new File("C:\\TAVOLI\\DATI\\" + data_convertita + "\\SDS\\PDA21_" + strDate + ".xml");
                                } else {
                                    file1 = new File("C:\\TAVOLI\\DATI\\" + data_convertita + "\\SDS\\PDA21_" + strDate + "-" + BIS_Corrente + ".xml");
                                }

                                /*if (!file.isFile()) {
                                //se il file NON esiste lo crea per intero e fin qua non ci son problemi 
                                try (BufferedWriter writer = new BufferedWriter(new FileWriter(file1))) {
                                    writer.write(response.toString());
                                }

                            } else {*/
                                try {
                                    /* se il file esiste, partendo dall'ultimo prg del file che quindi va calcolato, vado ad aggiungere il resto */
                                    String PRG = "000";
                                    String ULTIMO_NODO_UTILE = "";

                                    System.out.println("FileName: " + file);

                                    String temp_PRG = "000";

                                    if (file.isFile()) {

                                        System.out.println("File Readed...");
                                    } else {
                                        try ( BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {

                                            System.out.println("FileWriting...");

                                            writer.write("<?xml version=\"1.0\" standalone=\"yes\"?>\n"
                                                    + "<dataroot xmlns=\"http://www.w3.org/2001/TAVOLI.xsd\"></dataroot>");
                                        }

                                    }

                                    Document doc2 = dBuilder.parse(file);

                                    Element element = doc2.getDocumentElement();

                                    doc2.getDocumentElement().normalize();

                                    //System.out.println("Root element :" + doc2.getDocumentElement().getNodeName());
                                    NodeList nList2 = doc2.getElementsByTagName("TAVOLI");

                                    //System.out.println("----------------------------");
                                    Node nNode2;
                                    for (int temp = 0; temp < nList2.getLength(); temp++) {

                                        nNode2 = nList2.item(temp);

                                        // System.out.println("\nCurrent Element :" + nNode.getNodeName());
                                        if (nNode2.getNodeType() == Node.ELEMENT_NODE) {

                                            Element eElement = (Element) nNode2;

                                            if ("T".equals(eElement.getElementsByTagName("TC").item(0).getTextContent()) && BIS_Corrente.equals(eElement.getElementsByTagName("BIS").item(0).getTextContent())) {

                                                element.removeChild(nNode2);

                                            } else {

                                                tavolo = eElement.getElementsByTagName("TAVOLO").item(0).getTextContent();
                                                tavolotxt = eElement.getElementsByTagName("TAVOLOTXT").item(0).getTextContent();

                                                data = eElement.getElementsByTagName("DATA").item(0).getTextContent();
                                                data_convertita = data.substring(6, 10) + "-" + data.substring(3, 5) + "-" + data.substring(0, 2) + servizio;

                                                if (file.isFile()) {
                                                    temp_PRG = eElement.getElementsByTagName("PRG").item(0).getTextContent();
                                                } else {
                                                    temp_PRG = "000";
                                                }

                                                //System.out.println("TEMPPRG: " + temp_PRG);
                                                //System.out.println("PRG: " + PRG);
                                                if (Integer.parseInt(temp_PRG) > Integer.parseInt(PRG)) {
                                                    PRG = temp_PRG;
                                                }
                                            }

                                        }
                                    }

                                    //System.out.println("519");
                                    if (parametro_url_domiciliazione.equalsIgnoreCase("")) {
                                        /*niente*/
                                        System.out.println("NO SINCRO DOMICILIO");
                                    } else if (tavolotxt.contains("TAKE AWAY")) {
                                        System.out.println("SINCRO DOMICILIO");
                                        new SincroDomicilio();
                                    } else {
                                        System.out.println("NO SINCRO DOMICILIO");
                                    }
                                    System.out.println("FINE SINCRO DOMICILIO");

                                    String categoriaPadre = "";
                                    String portataPadre = "";
                                    String destPadre = "";
                                    String ivaPadre = "";

                                    /* aggiungo elementi a xml */
                                    for (int temp = 0; temp < nList.getLength(); temp++) {

                                        Node nNode = nList.item(temp);

                                        //System.out.println("\nCurrent Element :" + nNode.getNodeName());
                                        if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                                            Node nodoCopiato = nNode.cloneNode(true);
                                            Element eElement = (Element) nodoCopiato;

                                            String nodo = eElement.getElementsByTagName("NODO").item(0).getTextContent();

                                            PRG = String.format("%03d", Integer.parseInt(PRG) + 1);

                                            eElement.getElementsByTagName("PRG").item(0).setTextContent(PRG);

                                            if ("T".equals(eElement.getElementsByTagName("TC").item(0).getTextContent())) {

                                            } else {

                                                String tempNodo;

                                                if (nodo.length() == 3) {

                                                    ULTIMO_NODO_UTILE = PRG;

                                                    tempNodo = ULTIMO_NODO_UTILE;

                                                } else {

                                                    tempNodo = ULTIMO_NODO_UTILE + " " + PRG;

                                                }

                                                eElement.getElementsByTagName("NODO").item(0).setTextContent(tempNodo);

                                            }

                                            /*------------------------*/
                                            //System.out.println("576");
                                            InterfacciamentoFastComanda interfacciamento = new InterfacciamentoFastComanda(servizio);

                                            //System.out.println("580");
                                            String codArticolo = eElement.getElementsByTagName("ART2").item(0).getTextContent();

                                            if (codArticolo.equalsIgnoreCase("IDCSL_DES")) {
                                                if ("T".equals(eElement.getElementsByTagName("TC").item(0).getTextContent())) {
                                                    categoriaPadre = "";
                                                    portataPadre = "";
                                                    destPadre = "";
                                                    ivaPadre = "";
                                                } else {
                                                    if (eElement.getElementsByTagName("VAR").item(0).getTextContent().equalsIgnoreCase("") == true) {
                                                        categoriaPadre = eElement.getElementsByTagName("CAT").item(0).getTextContent();
                                                        portataPadre = eElement.getElementsByTagName("PORTATA").item(0).getTextContent();
                                                        destPadre = eElement.getElementsByTagName("TS").item(0).getTextContent();
                                                        ivaPadre = ivaDefault;
                                                    } else {
                                                        ivaPadre = ivaDefault;
                                                    }
                                                }

                                                eElement.getElementsByTagName("ASSEGNI").item(0).setTextContent(ivaPadre);

                                                eElement.getElementsByTagName("TS").item(0).setTextContent(destPadre);

                                                eElement.getElementsByTagName("CAT").item(0).setTextContent(categoriaPadre);

                                                eElement.getElementsByTagName("PORTATA").item(0).setTextContent(portataPadre);
                                            } else {
                                                
                                                Prodotto prodotto = interfacciamento.leggiProdotto(codArticolo);
                                                
                                                if ("T".equals(eElement.getElementsByTagName("TC").item(0).getTextContent())) {
                                                    categoriaPadre = "";
                                                    portataPadre = "";
                                                    destPadre = prodotto.DEST;
                                                    ivaPadre = "";
                                                } else {
                                                    if (eElement.getElementsByTagName("VAR").item(0).getTextContent().equalsIgnoreCase("") == true) {
                                                        categoriaPadre = prodotto.CAT;
                                                        portataPadre = prodotto.PORTATA;
                                                        destPadre = prodotto.DEST;
                                                        ivaPadre = prodotto.IVA;
                                                    }
                                                }

                                                eElement.getElementsByTagName("ASSEGNI").item(0).setTextContent(ivaPadre);

                                                eElement.getElementsByTagName("TS").item(0).setTextContent(destPadre);

                                                eElement.getElementsByTagName("CAT").item(0).setTextContent(categoriaPadre);

                                                eElement.getElementsByTagName("PORTATA").item(0).setTextContent(portataPadre);
                                                
                                                /*eElement.getElementsByTagName("LIB1").item(0).setTextContent(prodotto.REP);*/
                                            }

                                            //System.out.println("613");
                                            if (tavolotxt.contains("TAKE AWAY")) {

                                                tavolo = progressivoAsporto;
                                                tavolotxt = "TAKE AWAY/FO";

                                            }
                                            eElement.getElementsByTagName("TAVOLOTXT").item(0).setTextContent(tavolotxt);
                                            eElement.getElementsByTagName("TAVOLO").item(0).setTextContent(tavolo);


                                            /* ----------------------- */
                                            nodoCopiato = (Node) eElement;

                                            Node node = doc2.adoptNode(nodoCopiato);

                                            element.appendChild(node);

                                        }

                                    }

                                    //System.out.println("636");
                                    try ( BufferedWriter writer = new BufferedWriter(new FileWriter(file1))) {

                                        try {
                                            StringWriter sw = new StringWriter();
                                            TransformerFactory tf = TransformerFactory.newInstance();
                                            Transformer transformer = tf.newTransformer();
                                            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
                                            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
                                            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                                            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

                                            transformer.transform(new DOMSource(doc2), new StreamResult(sw));

                                            writer.write(sw.toString());

                                        } catch (Exception ex) {
                                            throw new RuntimeException("Error converting to String", ex);
                                        }

                                    }

                                    //System.out.println("PRG: " + PRG);
                                } catch (IOException | DOMException | SAXException e) {
                                    Logger.getLogger(RichiestaPHP.class.getName()).log(Level.SEVERE, null, e);
                                } catch (Exception ex) {
                                    Logger.getLogger(RichiestaPHP.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                /*}*/

                                File directory = new File("C:\\TAVOLI\\DATI\\" + data_convertita + "\\PLANING\\");
                                File file2 = new File("C:\\TAVOLI\\DATI\\" + data_convertita + "\\PLANING\\P" + tavolo + ".8");

                                for (File f1 : directory.listFiles()) {
                                    if (f1.getName().startsWith("P" + tavolotxt)) {
                                        f1.delete();
                                    }
                                }

                                try ( BufferedWriter writer = new BufferedWriter(new FileWriter(file2))) {
                                    writer.write(" ");
                                }

                            }
                        } else {
                            JOptionPane.showMessageDialog(null, "fast.order sta tentanto di scaricare un ordine, ma il servizio " + servizio + " di fast.comanda è spento. Accendi fast.comanda sul servizio " + servizio + " e clicca OK.");
                        }

                    } catch (IOException | ParserConfigurationException | DOMException | SAXException e) {
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(RichiestaPHP.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } else {
                //STAMPA
                //LE VIRGOLETTE VUOTE SAREBBERO UN NOME STAMPANTE MA LO PRENDE DA ID
                SDSFastcomanda.MyHandler.stampa("", response.toString());
            }

        } catch (IOException | ParserConfigurationException | SAXException e) {
            //NIENTE

        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

}
