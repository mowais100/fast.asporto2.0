/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sdsFastcomanda;

/**
 *
 * @author Davide
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static sdsFastcomanda.SDSFastcomanda.nomePc;

public class SQLDatabaseConnection {

    String connectionUrl;
    Connection connection;

    // Connect to your database.
    // Replace server name, username, and password with your credentials
    SQLDatabaseConnection() throws ClassNotFoundException {

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException e) {
            //System.out.println(e.toString());
        }

        this.connectionUrl = "jdbc:sqlserver://" + nomePc + "\\SQLEXPRESS:65432;database=fastcomanda;user=SA;password=Pwdcsl;loginTimeout=30;encrypt=false;";

        try {
            this.connection = DriverManager.getConnection(this.connectionUrl);
        } catch (SQLException e) {
            //System.out.println(e.toString());
        }

    }

    public List<Map<String, Object>> querySelect(String query) {

        ResultSet resultSet = null;

        try ( Statement statement = this.connection.createStatement();) {

            // Create and execute a SELECT SQL statement.
            String selectSql = query;

            resultSet = statement.executeQuery(selectSql);

            // Print results from select statement
            /*while (resultSet.next()) {
                //System.out.println(resultSet.getString("PRG02"));
            }*/
            List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
            Map<String, Object> row = null;

            ResultSetMetaData metaData = resultSet.getMetaData();
            Integer columnCount = metaData.getColumnCount();

            while (resultSet.next()) {
                row = new HashMap<String, Object>();
                for (int i = 1; i <= columnCount; i++) {
                    row.put(metaData.getColumnName(i), resultSet.getObject(i));
                }
                resultList.add(row);
            }

            return resultList;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean queryExecute(String query) {
        ResultSet resultSet = null;

        String insertSql = query;

        try ( PreparedStatement prepsInsertProduct = connection.prepareStatement(insertSql, Statement.RETURN_GENERATED_KEYS);) {
            prepsInsertProduct.execute();
            // Retrieve the generated key from the insert.
            resultSet = prepsInsertProduct.getGeneratedKeys();

            return true;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}
