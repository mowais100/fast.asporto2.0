/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sdsFastcomanda;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import static sdsFastcomanda.SDSFastcomanda.parametro_url;
import static sdsFastcomanda.SDSFastcomanda.parametro_url_clienti;

/**
 *
 * @author Davide
 */
public class SincroClienti {

    StringBuilder response;
    String RAG_SOC;
    String VIA;
    String CAP;
    String CITTA;
    String PROV;
    String NAZIONE;
    String C_FISCALE;
    String P_IVA;
    String TEL1;
    String CEL1;
    String MAIL;
    String NOTE;
    String SC;

    List<Map<String, Object>> resultList;

    SincroClienti() throws IOException, ParserConfigurationException, SAXException {

        this.resultList = null;

        this.response = null;

        this.RAG_SOC = "";
        this.VIA = "";
        this.CAP = "";
        this.CITTA = "";
        this.PROV = "";
        this.NAZIONE = "";
        this.C_FISCALE = "";
        this.P_IVA = "";
        this.TEL1 = "";
        this.CEL1 = "";
        this.MAIL = "";
        this.NOTE = "";
        this.SC = "";

        run();
    }

    public void run() throws IOException, ParserConfigurationException, SAXException {
        this.richiestaHTTP();
        //System.out.println("SINCRO CLIENTI richiestaHTTP");
        this.estraiClientiXML();
        //System.out.println("SINCRO CLIENTI estraiClientiXML");
        this.aggiornaDatabaseClientiFastComanda();
        //System.out.println("SINCRO CLIENTI aggiornaDatabaseClientiFastComanda");
    }

    private boolean aggiornaDatabaseClientiFastComanda() {

        try {

            SQLDatabaseConnection SQLDb = new SQLDatabaseConnection();

            Iterator<Map<String, Object>> iteratore = this.resultList.iterator();
            while (iteratore.hasNext()) {
                Map<String, Object> map = iteratore.next();
                //System.out.println(map.get("RAG_SOC").toString());

                this.RAG_SOC = map.get("RAG_SOC").toString();
                this.VIA = map.get("VIA").toString();
                this.CAP = map.get("CAP").toString();
                this.CITTA = map.get("CITTA").toString();
                this.PROV = map.get("PROV").toString();
                this.NAZIONE = map.get("NAZIONE").toString();
                this.C_FISCALE = map.get("C_FISCALE").toString();
                this.P_IVA = map.get("P_IVA").toString();
                this.TEL1 = map.get("TEL1").toString();
                this.CEL1 = map.get("CEL1").toString();
                this.MAIL = map.get("MAIL").toString();
                this.NOTE = map.get("NOTE").toString();
                this.SC = map.get("SC").toString();

                String querySelect = "SELECT * FROM CLIENTI WHERE SC='" + this.SC + "';";
                List<Map<String, Object>> rs = SQLDb.querySelect(querySelect);

                if (rs.size() > 0) {

                    String queryUpdate = "UPDATE CLIENTI SET RAG_SOC='" + this.RAG_SOC + "',VIA='" + this.VIA + "',CAP='" + this.CAP + "',CITTA='" + this.CITTA + "',PROV='" + this.PROV + "',NAZIONE='" + this.NAZIONE + "',C_FISCALE='" + this.C_FISCALE + "',P_IVA='" + this.P_IVA + "',TEL1='" + this.TEL1 + "',CEL1='" + this.CEL1 + "',MAIL='" + this.MAIL + "',NOTE='" + this.NOTE + "' WHERE SC='" + this.SC + "';";
                    SQLDb.queryExecute(queryUpdate);

                } else {

                    String queryInsert = "INSERT INTO CLIENTI (RAG_SOC,SC,VIA,CAP,CITTA,PROV,NAZIONE,C_FISCALE,P_IVA,TEL1,CEL1,MAIL,NOTE) VALUES ('" + this.RAG_SOC + "','" + this.SC + "','" + this.VIA + "','" + this.CAP + "','" + this.CITTA + "','" + this.PROV + "','" + this.NAZIONE + "','" + this.C_FISCALE + "','" + this.P_IVA + "','" + this.TEL1 + "','" + this.CEL1 + "','" + this.MAIL + "','" + this.NOTE + "');";
                    SQLDb.queryExecute(queryInsert);

                }

            }

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SincroClienti.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return true;
        }

    }

    private String richiestaHTTP() throws MalformedURLException, IOException {

        HttpURLConnection connection;

        String urlParameters = "ConnectionType=GetRequest";

        String link = parametro_url_clienti;

        URL url = new URL(link);
        connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

        connection.setRequestProperty("Content-Length", Integer.toString(urlParameters.getBytes().length));
        connection.setRequestProperty("Content-Language", "en-US");

        connection.setUseCaches(false);
        connection.setDoOutput(true);

        try ( DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
            wr.writeBytes(urlParameters);
        }

        //Get Response  
        InputStream is = connection.getInputStream();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        this.response = new StringBuilder(); // or StringBuffer if Java version 5+
        String line;
        while ((line = rd.readLine()) != null) {
            this.response.append(line);
            this.response.append('\r');
        }

        return this.response.toString();

    }

    private List<Map<String, Object>> estraiClientiXML() throws ParserConfigurationException, SAXException, IOException {


        /* BLOCCO LETTURA XML */
        //Get Document Builder
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        if (!this.response.toString().trim().isEmpty()) {
            InputSource is = new InputSource(new StringReader(this.response.toString().trim().replaceFirst("^([\\W]+)<","<")));

            Document document = builder.parse(is);

            //Normalize the XML Structure; It's just too important !!
            document.getDocumentElement().normalize();

            //Here comes the root node
            Element root = document.getDocumentElement();
            //System.out.println(root.getNodeName());

            //Get all employees
            NodeList nList = document.getElementsByTagName("CLIENTE");
            //System.out.println("============================");

            this.resultList = new ArrayList<Map<String, Object>>();
            Map<String, Object> row;

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node node = nList.item(temp);
                //System.out.println("");    //Just a separator

                if (node.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) node;

                    row = new HashMap<String, Object>();

                    row.put("RAG_SOC", eElement.getElementsByTagName("RAG_SOC").item(0).getTextContent());
                    row.put("VIA", eElement.getElementsByTagName("VIA").item(0).getTextContent());
                    row.put("CAP", eElement.getElementsByTagName("CAP").item(0).getTextContent());
                    row.put("CITTA", eElement.getElementsByTagName("CITTA").item(0).getTextContent());
                    row.put("PROV", eElement.getElementsByTagName("PROV").item(0).getTextContent());
                    row.put("NAZIONE", eElement.getElementsByTagName("NAZIONE").item(0).getTextContent());
                    row.put("C_FISCALE", eElement.getElementsByTagName("C_FISCALE").item(0).getTextContent());
                    row.put("P_IVA", eElement.getElementsByTagName("P_IVA").item(0).getTextContent());
                    row.put("TEL1", eElement.getElementsByTagName("TEL1").item(0).getTextContent());
                    row.put("CEL1", eElement.getElementsByTagName("CEL1").item(0).getTextContent());
                    row.put("MAIL", eElement.getElementsByTagName("MAIL").item(0).getTextContent());
                    row.put("NOTE", eElement.getElementsByTagName("NOTE").item(0).getTextContent());
                    row.put("SC", eElement.getElementsByTagName("SC").item(0).getTextContent());

                    this.resultList.add(row);

                }
            }
        }
        return resultList;
    }
}
