/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sdsFastcomanda;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import static sdsFastcomanda.SDSFastcomanda.campo_iva_default_articolo;

/**
 *
 * @author Davide
 */
class Prodotto {

    private String codArticolo;
    private String cartellaBaseFastComanda;
    private String fileSettaggiXML;
    private String fileMenuXML;
    private String numeroServizio;

    protected String REP;
    protected String DEST;
    protected String CAT;
    protected String PORTATA;
    protected String IVA;

    protected InterfacciamentoFastComanda classe;


    /*private String desc_art;
    private String prezzo_un;
    private String quantita;
    private String art_variante;
    private String autoriz;
    private String categoria;

    private String contiene_variante;*/
    public Prodotto(InterfacciamentoFastComanda classe) throws ParserConfigurationException, SAXException, IOException {

        this.classe = classe;

        this.cartellaBaseFastComanda = classe.cartellaBaseFastComanda;

        this.codArticolo = classe.codArticolo;

        this.fileSettaggiXML = classe.fileSettaggiXML;
        this.fileMenuXML = classe.fileMenuXML;
        this.numeroServizio = classe.numeroServizio;

        this.REP = "";
        this.DEST = "";
        this.CAT = "";
        this.PORTATA = "";
        this.IVA = "";

        assegnaValoriProdotto();

    }

    private Prodotto assegnaValoriProdotto() throws ParserConfigurationException, SAXException, IOException {


        /* BLOCCO LETTURA XML */
        //Get Document Builder
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        String path = this.cartellaBaseFastComanda + this.fileMenuXML;
        //Build Document
        Document document = builder.parse(new File(path));

        //Normalize the XML Structure; It's just too important !!
        document.getDocumentElement().normalize();

        //Here comes the root node
        Element root = document.getDocumentElement();
        //System.out.println(root.getNodeName());

        //Get all employees
        NodeList nList = document.getElementsByTagName("BIBITE");
        //System.out.println("============================");

       
        for (int temp = 0; temp < nList.getLength(); temp++) {

            Node node = nList.item(temp);
            //System.out.println("");    //Just a separator

            if (node.getNodeType() == Node.ELEMENT_NODE) {

                //Print each employee's detail
                Element eElement = (Element) node;

                if (eElement.getElementsByTagName("ART").item(0) != null) {
                    String MenuART = eElement.getElementsByTagName("ART").item(0).getTextContent();

                    if (this.codArticolo.equalsIgnoreCase(MenuART)) {

                        String categoria_prodotto = eElement.getElementsByTagName("CAT").item(0).getTextContent();
                        
                        String portata_prodotto = eElement.getElementsByTagName("PORTATA").item(0).getTextContent();

                        Categoria categoria = new Categoria(this.classe, categoria_prodotto);

                        this.REP = "";/*eElement.getElementsByTagName("REP").item(0).getTextContent();*/
                        
                        this.DEST = eElement.getElementsByTagName("DEST").item(0).getTextContent();

                        this.CAT = categoria_prodotto;

                        this.PORTATA = categoria.PORTATA;
                        
                        if (!portata_prodotto.equalsIgnoreCase("")) {
                            this.PORTATA = portata_prodotto;
                        }

                        this.IVA = eElement.getElementsByTagName(campo_iva_default_articolo).item(0).getTextContent();
                    }
                }
            }
        }

        return this;
    }
}
