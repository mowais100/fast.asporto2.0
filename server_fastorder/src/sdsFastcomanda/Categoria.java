/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sdsFastcomanda;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Davide
 */
public class Categoria {

    protected String categoria;

    private String cartellaBaseFastComanda;
    private String fileSettaggiXML;
    private String fileMenuXML;
    private String numeroServizio;

    protected String REP;
    protected String DEST;
    protected String CAT;
    protected String PORTATA;
    protected String IVA;

    Categoria(InterfacciamentoFastComanda classe, String categoria) throws ParserConfigurationException, SAXException, IOException {
        this.categoria = categoria;

        this.cartellaBaseFastComanda = classe.cartellaBaseFastComanda;
        this.fileSettaggiXML = classe.fileSettaggiXML;
        this.fileMenuXML = classe.fileMenuXML;
        this.numeroServizio = classe.numeroServizio;

        //this.REP = "";
        ///this.DEST = "";
        //this.CAT = "";
        this.PORTATA = "";
        //this.IVA = "";

        assegnaValoriCategoria();
    }

    private Categoria assegnaValoriCategoria() throws ParserConfigurationException, SAXException, IOException {


        /* BLOCCO LETTURA XML */
        //Get Document Builder
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        String path = this.cartellaBaseFastComanda + this.fileMenuXML;
        //Build Document
        Document document = builder.parse(new File(path));

        //Normalize the XML Structure; It's just too important !!
        document.getDocumentElement().normalize();

        //Here comes the root node
        Element root = document.getDocumentElement();
        //System.out.println(root.getNodeName());

        //Get all employees
        NodeList nList = document.getElementsByTagName("CATEGORIE");
        //System.out.println("============================");

        for (int temp = 0; temp < nList.getLength(); temp++) {

            Node node = nList.item(temp);
            //System.out.println("");    //Just a separator

            if (node.getNodeType() == Node.ELEMENT_NODE) {

                //Print each employee's detail
                Element eElement = (Element) node;

                if (eElement.getElementsByTagName("CAT").item(0) != null) {
                    String MenuART = eElement.getElementsByTagName("CAT").item(0).getTextContent();

                    if (this.categoria.equalsIgnoreCase(MenuART)) {
                        /* this.REP = "";
                        this.DEST = eElement.getElementsByTagName("DEST").item(0).getTextContent();
                        this.CAT = eElement.getElementsByTagName("CAT").item(0).getTextContent();*/
                        this.PORTATA = eElement.getElementsByTagName("PORTATA").item(0).getTextContent();
                        /*  this.IVA = eElement.getElementsByTagName("IVA").item(0).getTextContent();*/
                    }
                }
            }
        }

        return this;
    }

}
