/*
 * Note: se l'ordine viene preso per giorni futuri comunque la comanda verrà salvata e stampata nel giorno di servizio nel quale viene preso l'ordine
 * e non sarà visualizzato sul giorno futuro ma solo su quello di oggi
 */
package sdsFastcomanda;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import static sdsFastcomanda.SDSFastcomanda.data;
import static sdsFastcomanda.SDSFastcomanda.parametro_url;
import static sdsFastcomanda.SDSFastcomanda.parametro_url_clienti;
import static sdsFastcomanda.SDSFastcomanda.parametro_url_domiciliazione;
import static sdsFastcomanda.SDSFastcomanda.servizio;
import static sdsFastcomanda.SDSFastcomanda.tavolo;

/**
 *
 * @author Davide
 */
public class SincroDomicilio {

    StringBuilder response;

    String REC;
    String TAV;
    String NOME;
    String RIF;
    String CODCLI;
    String CATEGORIA;

    List<Map<String, Object>> resultList;

    SincroDomicilio() throws IOException, ParserConfigurationException, SAXException, TransformerException {

        this.resultList = null;

        this.response = null;

        this.REC = "";
        this.TAV = "";
        this.NOME = "";
        this.RIF = "";
        this.CODCLI = "";
        this.CATEGORIA = "";

        run();
    }

    public void run() throws IOException, ParserConfigurationException, SAXException, TransformerException {
        this.richiestaHTTP();
        this.estraiNomiXML();
        this.aggiornaNomiXML();
    }

    private String richiestaHTTP() throws MalformedURLException, IOException {

        HttpURLConnection connection;

        String urlParameters = "ConnectionType=GetRequest";

        String link = parametro_url_domiciliazione;

        URL url = new URL(link);
        connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

        connection.setRequestProperty("Content-Length", Integer.toString(urlParameters.getBytes().length));
        connection.setRequestProperty("Content-Language", "en-US");

        connection.setUseCaches(false);
        connection.setDoOutput(true);

        try ( DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
            wr.writeBytes(urlParameters);
        }

        //Get Response  
        InputStream is = connection.getInputStream();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        this.response = new StringBuilder(); // or StringBuffer if Java version 5+
        String line;
        while ((line = rd.readLine()) != null) {
            this.response.append(line);
            this.response.append('\r');
        }

        return this.response.toString();

    }

    private List<Map<String, Object>> estraiNomiXML() throws ParserConfigurationException, SAXException, IOException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        InputSource is = new InputSource(new StringReader(this.response.toString().trim().replaceFirst("^([\\W]+)<","<")));

        Document document = builder.parse(is);

        //Normalize the XML Structure; It's just too important !!
        document.getDocumentElement().normalize();

        //Here comes the root node
        Element root = document.getDocumentElement();
        //System.out.println(root.getNodeName());

        //Get all employees
        NodeList nList = document.getElementsByTagName("NOMI");
        //System.out.println("============================");

        this.resultList = new ArrayList<Map<String, Object>>();
        Map<String, Object> row;

        for (int temp = 0; temp < nList.getLength(); temp++) {

            Node node = nList.item(temp);
            //System.out.println("");    //Just a separator

            if (node.getNodeType() == Node.ELEMENT_NODE) {

                Element eElement = (Element) node;

                row = new HashMap<String, Object>();

                String data_convertita = data.substring(0, 2) + "/" + data.substring(3, 5) + "/" + data.substring(6, 10);

                row.put("REC", data_convertita + servizio.replace("_", ""));
                row.put("TAV", tavolo);
                row.put("NOME", eElement.getElementsByTagName("NOME").item(0).getTextContent());
                row.put("RIF", eElement.getElementsByTagName("RIF").item(0).getTextContent());
                row.put("CODCLI", eElement.getElementsByTagName("CODCLI").item(0).getTextContent());
                row.put("CATEGORIA", eElement.getElementsByTagName("CATEGORIA").item(0).getTextContent());

                this.resultList.add(row);

            }
        }

        return resultList;
    }

    private boolean aggiornaNomiXML() throws TransformerConfigurationException, TransformerException, ParserConfigurationException, SAXException, IOException {

        String data_convertita = SDSFastcomanda.data_convertita;

        Iterator<Map<String, Object>> iteratore = this.resultList.iterator();
        while (iteratore.hasNext()) {
            Map<String, Object> map = iteratore.next();

            this.REC = map.get("REC").toString();
            this.TAV = map.get("TAV").toString();
            this.NOME = map.get("NOME").toString();
            this.RIF = map.get("RIF").toString();
            this.CODCLI = map.get("CODCLI").toString();
            this.CATEGORIA = map.get("CATEGORIA").toString();

            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse("C:\\TAVOLI\\DATI\\" + data_convertita + "\\NOMI.xml");
            Element root = document.getDocumentElement();

            Element newNomi = document.createElement("NOMI");

            Element REC = document.createElement("REC");
            REC.appendChild(document.createTextNode(this.REC));
            newNomi.appendChild(REC);

            Element TAV = document.createElement("TAV");
            TAV.appendChild(document.createTextNode(this.TAV));
            newNomi.appendChild(TAV);

            Element NOME = document.createElement("NOME");
            NOME.appendChild(document.createTextNode(this.NOME));
            newNomi.appendChild(NOME);

            Element RIF = document.createElement("RIF");
            RIF.appendChild(document.createTextNode(this.RIF));
            newNomi.appendChild(RIF);

            Element CODCLI = document.createElement("CODCLI");
            CODCLI.appendChild(document.createTextNode(this.CODCLI));
            newNomi.appendChild(CODCLI);

            Element CATEGORIA = document.createElement("CATEGORIA");
            CATEGORIA.appendChild(document.createTextNode(this.CATEGORIA));
            newNomi.appendChild(CATEGORIA);

            root.appendChild(newNomi);

            DOMSource source = new DOMSource(document);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            StreamResult result = new StreamResult("C:\\TAVOLI\\DATI\\" + data_convertita + "\\NOMI.xml");
            transformer.transform(source, result);

        }

        return true;

    }

}
