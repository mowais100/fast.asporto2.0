/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sdsFastcomanda;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import static java.awt.print.Printable.NO_SUCH_PAGE;
import static java.awt.print.Printable.PAGE_EXISTS;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.MediaSizeName;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import static java.awt.Image.SCALE_DEFAULT;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.io.File;
import java.io.IOException;
import java.util.EnumMap;
import java.util.Map;

/**
 *
 * @author Administrator
 */
public class Stampa implements Printable {

    String nomeStampante;
    Node global_document;
    Graphics global_grafica;
    PageFormat global_pf;
    int global_page;

    public Stampa(Node doc, String nS) {
        global_document = doc;
        nomeStampante = nS;
    }

    public Graphics2D conversione() throws WriterException {

        int sym_x = 0;
        int sym_y = 0;
        int y_area_1 = 0;

        Graphics2D g2d = (Graphics2D) global_grafica;
        FontMetrics fm;

        int width;
        int x = 3;
        double position = 0;
        int fontsize = 12;
        int fonttype = Font.PLAIN;
        boolean ul = false;
        String allineamento = "left";

        g2d.setPaint(Color.black);

        g2d.translate(global_pf.getImageableX(), global_pf.getImageableY());

        double larghezza_foglio = global_pf.getPaper().getWidth();

        //global_document.getDocumentElement().normalize();
        //NodeList nList = global_document.getElementsByTagName("epos-print");
        NodeList eposprint_child = global_document.getChildNodes();

        for (int temp = 0; temp < eposprint_child.getLength(); temp++) {

            Node nNode = eposprint_child.item(temp);

            if (nNode.getNodeName() != "#text") {

                //System.out.print("E " + nNode.getNodeName() + " E\n");
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

                    if (nNode.getNodeName() == "text") {
                        if (eElement.getTextContent().length() > 0) {
                            //html_risposta += "<span style=\"" + position + fontsize + em + ul + "\" >";

                            String[] array_linee = eElement.getTextContent().split("\n", -1);

                            g2d.setFont(new Font("Segoe Condensed", fonttype, fontsize));

                            int i = 0;
                            for (String element : array_linee) {

                                if (position > 0) {
                                    x = (int) Math.round(position);
                                }

                                fm = g2d.getFontMetrics(g2d.getFont());
                                width = fm.stringWidth(element);

                                if ("center".equals(allineamento)) {
                                    x = 210 / 2 - width / 2;
                                }

                                g2d.drawString(element, x, fontsize + 2);

                                if (ul == true) {
                                    g2d.drawLine(x, 17, x + width, 17);
                                }

                                x += width;

                                i++;
                                if (i < array_linee.length) {
                                    g2d.translate(0, fontsize + 2);
                                    x = 3;

                                }
                                position = 0;

                            }

                        } else {

                            if (eElement.getAttribute("x").length() > 0) {

                                int val_x = Integer.parseInt(eElement.getAttribute("x"));

                                if (val_x < 100) {
                                    position = val_x / 2;
                                } else {
                                    position = val_x / 3;
                                }

                            }

                            if ("2".equals(eElement.getAttribute("height")) && "2".equals(eElement.getAttribute("width"))) {
                                fontsize = 14;
                            } else if ("1".equals(eElement.getAttribute("height")) && "1".equals(eElement.getAttribute("width"))) {
                                fontsize = 12;
                            }

                            if (eElement.getAttribute("align").length() > 0) {

                                allineamento = eElement.getAttribute("align");

                                x = 3;

                            }

                            if (eElement.getAttribute("em").length() > 0) {
                                if ("true".equals(eElement.getAttribute("em"))) {
                                    fonttype = Font.BOLD;
                                } else {
                                    fonttype = Font.PLAIN;

                                }
                            }

                            if (eElement.getAttribute("ul").length() > 0) {
                                if ("true".equals(eElement.getAttribute("ul"))) {
                                    ul = true;

                                } else {
                                    ul = false;
                                }
                            }
                        }

                    } else if ("feed".equals(nNode.getNodeName())) {

                        int numero_linee = Integer.parseInt(eElement.getAttribute("line"));

                        for (int i = 0; i < numero_linee; i++) {
                            g2d.translate(0, fontsize + 2);
                            x = 3;
                            position = 0;
                        }

                    } else if ("page".equals(nNode.getNodeName())) {

                        NodeList page_child = nNode.getChildNodes();

                        for (int temp1 = 0; temp1 < page_child.getLength(); temp1++) {

                            Node nNode1 = page_child.item(temp1);

                            if (nNode1.getNodeType() == Node.ELEMENT_NODE) {

                                Element eElement1 = (Element) nNode1;

                                if (nNode1.getNodeName() == "text") {

                                    if (eElement1.getTextContent().length() > 0) {
                                        //html_risposta += "<span style=\"" + position + fontsize + em + ul + "\" >";

                                        String[] array_linee = eElement1.getTextContent().split("\n", -1);

                                        g2d.setFont(new Font("Segoe Condensed", fonttype, fontsize));

                                        int i = 0;
                                        for (String element : array_linee) {

                                            if (position > 0) {
                                                x = (int) Math.round(position);
                                            }

                                            fm = g2d.getFontMetrics(g2d.getFont());
                                            width = fm.stringWidth(element);

                                            if (sym_x == 1) {
                                                x = 10;
                                                y_area_1 = fontsize + 2;
                                            } else if (sym_x == 2) {
                                                x = 130;
                                            }

                                            g2d.drawString(element, x, fontsize + 2);

                                            if (ul == true) {
                                                g2d.drawLine(x, 17, x + width, 17);
                                            }

                                            x += width;

                                            i++;
                                            if (i < array_linee.length) {
                                                g2d.translate(0, fontsize + 2);
                                                x = 3;

                                            }
                                            position = 0;

                                        }

                                    } else {

                                        if (eElement1.getAttribute("x").length() > 0) {

                                            int val_x = Integer.parseInt(eElement1.getAttribute("x"));

                                            if (val_x < 100) {
                                                position = val_x / 2;
                                            } else {
                                                position = val_x / 3;
                                            }

                                        }

                                        if ("2".equals(eElement1.getAttribute("height")) && "2".equals(eElement1.getAttribute("width"))) {
                                            fontsize = 14;
                                        } else if ("1".equals(eElement1.getAttribute("height")) && "1".equals(eElement1.getAttribute("width"))) {
                                            fontsize = 12;
                                        }

                                        if (eElement1.getAttribute("align").length() > 0) {

                                            allineamento = eElement1.getAttribute("align");

                                            x = 3;

                                        }

                                        if (eElement1.getAttribute("em").length() > 0) {
                                            if ("true".equals(eElement1.getAttribute("em"))) {
                                                fonttype = Font.BOLD;
                                            } else {
                                                fonttype = Font.PLAIN;

                                            }
                                        }

                                        if (eElement1.getAttribute("ul").length() > 0) {
                                            if ("true".equals(eElement1.getAttribute("ul"))) {
                                                ul = true;

                                            } else {
                                                ul = false;
                                            }
                                        }
                                    }

                                } else if ("feed".equals(nNode1.getNodeName())) {

                                    int numero_linee = Integer.parseInt(eElement1.getAttribute("line"));

                                    for (int i = 0; i < numero_linee; i++) {
                                        g2d.translate(0, fontsize + 2);
                                        x = 3;
                                        position = 0;
                                    }

                                } else if ("area".equals(nNode1.getNodeName())) {

                                    sym_x++;
                                    sym_y++;

                                } else if ("symbol".equals(nNode1.getNodeName())) {

                                    if (sym_x == 1) {
                                        x = 10;
                                    } else if (sym_x == 2) {
                                        x = 130;
                                    }

                                    String myCodeText = eElement1.getTextContent();
                                    int size = 78;

                                    Map<EncodeHintType, Object> hintMap = new EnumMap<>(EncodeHintType.class);
                                    hintMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");

                                    // Now with zxing version 3.2.1 you could change border size (white border size to just 1)
                                    hintMap.put(EncodeHintType.MARGIN, 1);
                                    /* default = 4 */
                                    hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);

                                    QRCodeWriter qrCodeWriter = new QRCodeWriter();
                                    BitMatrix byteMatrix = qrCodeWriter.encode(myCodeText, BarcodeFormat.QR_CODE, size, size, hintMap);
                                    int CrunchifyWidth = byteMatrix.getWidth();
                                    BufferedImage image = new BufferedImage(CrunchifyWidth, CrunchifyWidth, BufferedImage.TYPE_INT_RGB);

                                    image.createGraphics();

                                    Graphics2D graphics = (Graphics2D) image.getGraphics();
                                    graphics.setColor(Color.WHITE);
                                    graphics.fillRect(0, 0, CrunchifyWidth, CrunchifyWidth);
                                    graphics.setColor(Color.BLACK);

                                    for (int i = 0; i < CrunchifyWidth; i++) {
                                        for (int j = 0; j < CrunchifyWidth; j++) {
                                            if (byteMatrix.get(i, j)) {
                                                graphics.fillRect(i, j, 1, 1);
                                            }
                                        }
                                    }
                                    Image a = image.getScaledInstance(size, size, SCALE_DEFAULT);

                                    g2d.drawImage(a, x, size * -1, null);
                                    //g2d.drawString("QR CODE", x, -42);

                                }

                            }

                        }

                    }
                }

            }
        }

        /*
        //CENTRATO
        g2d.setFont(new Font("Arial Narrow", Font.BOLD, 24));
        g2d.setPaint(Color.black);

        fm = g2d.getFontMetrics(g2d.getFont());
        width = fm.stringWidth("TITOLO");
        x = 220 / 2 - width / 2;
        g2d.drawString("TITOLO", x, 20);
        g2d.translate(0, 15);
        g2d.translate(0, 15);

        g2d.setFont(new Font("Arial Narrow", Font.PLAIN, 12));
        g2d.setPaint(Color.black);

        //SINISTRA
        fm = g2d.getFontMetrics(g2d.getFont());
        g2d.drawString("COMANDA", 3, 15);
       

        //DESTRA
         g2d.translate(0, 15);
        width = fm.stringWidth("€2.00");
        x = 200 - width;
        g2d.drawString("€2.00", x, 0);

        //SINISTRA
        fm = g2d.getFontMetrics(g2d.getFont());
        g2d.drawString("SUCCO ALBICOCCA", 3, 15);
        g2d.translate(0, 15);

        //DESTRA
        width = fm.stringWidth("€20.00");
        x = 200 - width;
        g2d.drawString("€20.00", x, 0);

        //SINISTRA       
        fm = g2d.getFontMetrics(g2d.getFont());
        g2d.drawString("SUCCO ACE", 3, 15);
        g2d.translate(0, 15);
        //DESTRA
        width = fm.stringWidth("€240.35");
        x = 200 - width;
        g2d.drawString("€240.35", x, 0);
         */
        return g2d;
    }

    @Override
    public int print(Graphics grafica, PageFormat pf, int page) throws PrinterException {

        if (page > 0) {
            /* We have only one page, and 'page' is zero-based */
            return NO_SUCH_PAGE;
        }

        global_grafica = grafica;
        global_pf = pf;
        global_page = page;

        try {
            Graphics2D g2d = conversione();
        } catch (WriterException ex) {
            Logger.getLogger(Stampa.class.getName()).log(Level.SEVERE, null, ex);
        }

        return PAGE_EXISTS;
    }

    public void printer_job() {
        PrinterJob job = PrinterJob.getPrinterJob();
        //PageFormat pf = job.defaultPage();

        PageFormat pf = job.defaultPage();

        Paper paper = new Paper();

        /*double margin = 9;
        paper.setImageableArea(margin, margin, paper.getWidth() - margin * 2, paper.getHeight() - margin * 2);
        
        pf.setPaper(paper);*/
        paper.setSize(pf.getWidth(), 100000); // 1/72 inch
        paper.setImageableArea(0, 0, paper.getWidth(), paper.getHeight()); // no margins
        PageFormat pageFormat = new PageFormat();
        pageFormat.setPaper(paper);

        job.setPrintable(this, pageFormat);

        PrintService[] service = PrinterJob.lookupPrintServices(); // list of printers
        DocPrintJob docPrintJob = null;

        int count = service.length;
        PrintService service_1 = null;

        for (int i = 0; i < count; i++) {
            if (service[i].getName().equalsIgnoreCase(nomeStampante)) {
                docPrintJob = service[i].createPrintJob();
                service_1 = service[i];
                i = count;
            }
        }

        PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
        aset.add(MediaSizeName.ISO_A4);

        Insets margins = new Insets(-10, 0, 0, 0);

        try {

            job.setPrintService(service_1);
        } catch (PrinterException ex) {
            Logger.getLogger(SDSFastcomanda.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            job.print();
        } catch (PrinterException ex) {
            Logger.getLogger(SDSFastcomanda.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
