/*  
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sdsFastcomanda;

/* Import dom parser packages (XML) */
import static com.sun.tools.javac.tree.TreeInfo.name;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import java.io.*;
import javax.xml.XMLConstants;
import javax.xml.transform.dom.DOMSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.xml.sax.SAXException;

/**
 *
 * @author Davide
 */
public class InterfacciamentoFastComanda {

    /* dichiarazione variabili */
    protected String codArticolo;

    protected String cartellaBaseFastComanda;

    protected String fileSettaggiXML;
    protected String fileMenuXML;
    protected String numeroServizio;

    public static boolean attivaTSE = true;
    private Prodotto prodotto = null;

    /* costruttore */
    InterfacciamentoFastComanda(String numeroServizio) throws ParserConfigurationException, SAXException, IOException {
        this.cartellaBaseFastComanda = "C://TAVOLI/MAGA//";
        this.fileSettaggiXML = "";
        this.fileMenuXML = "";
        this.numeroServizio = numeroServizio;
        // this.attivaTSE = true;

        //this.leggiProdotto();
    }

    private String leggiFileSettaggi() {

        this.fileSettaggiXML = "SETTAGGI.XML";

        if (attivaTSE == true) {

            this.fileSettaggiXML = "SETTAGGITSE.XML";

        }

        return this.fileSettaggiXML;

    }

    private String leggiFileMenuXML() throws ParserConfigurationException, SAXException, IOException {

        this.fileMenuXML = "";

        /* BLOCCO LETTURA XML */
        //Get Document Builder
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        String path = this.cartellaBaseFastComanda + this.fileSettaggiXML;
        //Build Document
        Document document = builder.parse(new File(path));

        //Normalize the XML Structure; It's just too important !!
        document.getDocumentElement().normalize();

        //Here comes the root node
        Element root = document.getDocumentElement();
        //System.out.println(root.getNodeName());

        //Get all employees
        NodeList nList = document.getElementsByTagName("SETTAGGI");
        //System.out.println("============================");

        String REC = "";
        String SALA = "";
        String SERVIZIO = "";

        for (int temp = 0; temp < nList.getLength(); temp++) {

            Node node = nList.item(temp);
            //System.out.println("");    //Just a separator

            if (node.getNodeType() == Node.ELEMENT_NODE) {

                //Print each employee's detail
                Element eElement = (Element) node;

                //System.out.println("REC : " + eElement.getElementsByTagName("REC").item(0).getTextContent());
                //System.out.println("SALA : " + eElement.getAttribute("SALA"));
                REC = eElement.getElementsByTagName("REC").item(0).getTextContent();
                SALA = eElement.getElementsByTagName("SALA").item(0).getTextContent();
                SERVIZIO = eElement.getElementsByTagName("SERVIZIO").item(0).getTextContent();

                String numeroServizioSenzaUnderscore = this.numeroServizio.replace("_", "");

                if (!REC.contains("_") && SERVIZIO.equalsIgnoreCase(numeroServizioSenzaUnderscore)) {

                    this.fileMenuXML = SALA;

                    break;

                }
            }
        }

        return this.fileMenuXML;

    }

    Prodotto leggiProdotto(String codArticolo) throws ParserConfigurationException, SAXException, IOException {

        this.leggiFileSettaggi();

        this.leggiFileMenuXML();

        this.codArticolo = codArticolo;

        return new Prodotto(this);

    }
}
