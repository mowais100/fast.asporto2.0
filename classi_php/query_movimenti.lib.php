<?php

ini_set('max_execution_time', 300);
set_time_limit(300);

error_reporting(E_ALL);

$percorso_main="C:/TAVOLI/fast.intellinet/";

$mysql_abilitato = true;

$websql = $percorso_main.'DATABASE_CLIENTE.sqlite';
//$websql='/storage/emulated/0/www/fast.intellinet/DATABASE_LOCALE.sqlite';

$query = $_POST['query'];

try {

    $db = new SQLite3($websql);

    $db->busyTimeout(10000);

    if (strpos(strtolower($query), 'select') !== false) {
        $results = $db->query($query);
    } else {
        $results = $db->exec($query);
    }

    $json = [];
    $json1 = [];
    $json2 = [];

    if (is_bool($results) !== true) {

        while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
            array_push($json1, $row);
        }
    }

//PARTE MYSQL
    if ($mysql_abilitato === true) {

        $dbmysql = new mysqli("localhost", "root", "", "provaINTELLINET");
        
        $dbmysql->set_charset("utf8");


        $query1 = $dbmysql->query($query);

        if (is_bool($query1) !== true) {
            while ($row = $query1->fetch_array(MYSQLI_ASSOC)) {
                array_push($json2, $row);
            }
        }
               
        
        if (isset($json2[0])&&array_key_exists('prezzo_tot',$json2[0])&&!array_key_exists('giorno',$json2[0])&&!array_key_exists('mese',$json2[0])) {
            foreach ($json2 as $i1 => $v1) {
                
                foreach ($json2[$i1] as $i2 => $v2) {                    
                    array_push($json,["prezzo_tot"=>number_format($json2[$i1][$i2]+$json1[$i1][$i2],2)]);
                    }
            }
        } else {

            //QUESTA VA BENISSIMO COME CASISTICA DEI PRIMI 2 GRAFICI DEI MOVIMENTI
            //IN QUANTO L'INDICE NON E' PREZZO TOT
            $json = array_merge($json1, $json2);
        }
    } else {
        $json = $json1;
    }

    echo json_encode($json);
} catch (Exception $ex) {

    echo $ex;
}

$db->close();
unset($db);
