<?php

//CONSIGLIO:
//CERCA CAMPI PER NOME TABELLA TRA APICI

ini_set('max_execution_time', 0);
ini_set("memory_limit", "1G");

set_time_limit(0);

error_reporting(E_ALL);

$ignora_numero_record = filter_input(INPUT_POST, "ignora_numero_record", FILTER_SANITIZE_STRING);

$nomeDBMysql = "provaINTELLINET";

$mysqli = new mysqli("localhost", "root", "", "$nomeDBMysql");

if ($mysqli->connect_errno) {
    echo "MYSQL_CONNECTION_ERROR";
    exit;
}

$db = new PDO("sqlite:C:/TAVOLI/fast.intellinet/DATABASE_CLIENTE.sqlite");
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

function eseguiQueryMysql($query) {
    global $mysqli;

    $result = $mysqli->query($query);

    if ($result === true) {
        return true;
    } else if ($result === false) {
        $tipo_errore = $mysqli->error;

        if (strpos($tipo_errore, "already exists") !== FALSE || strpos($tipo_errore, "Duplicate column name") !== FALSE) {
            return true;
        } else {
            echo $tipo_errore;
            return false;
        }
    } else {
        //ritorna solo 1 risultato
        if ($result->num_rows === 1) {
            return $row = $result->fetch_array(MYSQLI_ASSOC);
        } else {
            return 1;
        }
    }
}

function eseguiQuerySqlite($query) {
    global $db;

    $result = false;
    $eccezioni = false;

    try {
        $result = $db->query($query);
    } catch (Exception $e) {


        $tipo_errore = $e->getMessage();
        if (strpos($tipo_errore, "already exists") !== FALSE || strpos($tipo_errore, "duplicate column name") !== FALSE) {
            $eccezioni = true;
        }
    }

    if ($result === false) {
        if ($eccezioni === false) {
            echo $tipo_errore;
            return false;
        } else {
            return true;
        }
    } else {
        return true;
    }
}

$campiSqlite = array(
    /* tabella inutilizzata al momento */
    'mov_fat' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'INTEGER',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '1',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'tipo_movimento',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'sez_iva',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        3 =>
        array(
            'cid' => '3',
            'name' => 'n_prog_reg',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        4 =>
        array(
            'cid' => '4',
            'name' => 'n_fiscale',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        5 =>
        array(
            'cid' => '5',
            'name' => 'data_registrazione',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        6 =>
        array(
            'cid' => '6',
            'name' => 'ora_registrazione',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        7 =>
        array(
            'cid' => '7',
            'name' => 'servizi',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        8 =>
        array(
            'cid' => '8',
            'name' => 'nome_terminale',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        9 =>
        array(
            'cid' => '9',
            'name' => 'n_terminale',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        10 =>
        array(
            'cid' => '10',
            'name' => 'codice_centro',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        11 =>
        array(
            'cid' => '11',
            'name' => 'n_tavolo_comanda',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        12 =>
        array(
            'cid' => '12',
            'name' => 'n_tavolo_attrib',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        13 =>
        array(
            'cid' => '13',
            'name' => 'n_tavolo_bis',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        14 =>
        array(
            'cid' => '14',
            'name' => 'cod_cliente',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        15 =>
        array(
            'cid' => '15',
            'name' => 'rag_soc_cliente',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        16 =>
        array(
            'cid' => '16',
            'name' => 'tot_documento',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        17 =>
        array(
            'cid' => '17',
            'name' => 'tot_buoni_pasto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        18 =>
        array(
            'cid' => '18',
            'name' => 'tot_sconto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        19 =>
        array(
            'cid' => '19',
            'name' => 'prepagato_sn',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        20 =>
        array(
            'cid' => '20',
            'name' => 'tot_prepagato',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        21 =>
        array(
            'cid' => '21',
            'name' => 'tipo_prepagato',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        22 =>
        array(
            'cid' => '22',
            'name' => 'tot_corr_pagato',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        23 =>
        array(
            'cid' => '23',
            'name' => 'tot_corr_non_pagato',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        24 =>
        array(
            'cid' => '24',
            'name' => 'tot_bar',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        25 =>
        array(
            'cid' => '25',
            'name' => 'tot_take_away',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        26 =>
        array(
            'cid' => '26',
            'name' => 'imp_1',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        27 =>
        array(
            'cid' => '27',
            'name' => 'alq_1',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        28 =>
        array(
            'cid' => '28',
            'name' => 'imposta_1',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        29 =>
        array(
            'cid' => '29',
            'name' => 'imposta_2',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        30 =>
        array(
            'cid' => '30',
            'name' => 'imposta_3',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        31 =>
        array(
            'cid' => '31',
            'name' => 'imposta_4',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        32 =>
        array(
            'cid' => '32',
            'name' => 'imposta_5',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        33 =>
        array(
            'cid' => '33',
            'name' => 'fat_sc_non_pagato',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        34 =>
        array(
            'cid' => '34',
            'name' => 'data_documento_fat',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        35 =>
        array(
            'cid' => '35',
            'name' => 'n_doc_fat',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        36 =>
        array(
            'cid' => '36',
            'name' => 'data_pagamento',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        37 =>
        array(
            'cid' => '37',
            'name' => 'Field38',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        38 =>
        array(
            'cid' => '38',
            'name' => 'Field39',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),

    /* serve per creare i bottoni di sconto quando premo il tasto SCONTO/BUONO 
    nella schermata pre-pagamento. Si gestisce da Gestione->Sconti */

    'tabella_sconti_buoni' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'INTEGER',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '1',
        ),
        1 =>
        /* in realtà è il valore dello sconto. Deve essere un numero */
        array(
            'cid' => '1',
            'name' => 'percentuale',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        /* nome dello sconto, ad esempio "Interno" */
        array(
            'cid' => '2',
            'name' => 'descrizione',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        3 =>
        /* Tipo di sconto. SCONTO è percentuale, P è imponibile */
        array(
            'cid' => '3',
            'name' => 'tipo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
    'lingue' =>
    /* tabella delle traduzioni con le corrispondenze tra italiano, inglese e tedesco */
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'INTEGER',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '1',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'italiano',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'english',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        3 =>
        array(
            'cid' => '3',
            'name' => 'deutsch',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
    'id_motivazione' =>
    /* per salvare id di righa per cancalare in tedesqo  */
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'id_mot',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        
    ),
    'storni' =>
    /* per salvare motivazioni cancelare a tedesqo  */
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'data',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'datar',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        3 =>
        array(
            'cid' => '3',
            'name' => 'ora',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        4 =>
        array(
            'cid' => '4',
            'name' => 'nterm',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        5 =>
        array(
            'cid' => '5',
            'name' => 'tavolotxt',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        6 =>
        array(
            'cid' => '6',
            'name' => 'tavolo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        7 =>
        array(
            'cid' => '7',
            'name' => 'BIS',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        8 =>
        array(
            'cid' => '8',
            'name' => 'tipop',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        9 =>
        array(
            'cid' => '9',
            'name' => 'prg',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        10 =>
        array(
            'cid' => '10',
            'name' => 'art',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        11 =>
        array(
            'cid' => '11',
            'name' => 'des',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        12 =>
        array(
            'cid' => '12',
            'name' => 'prz_money',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        13 =>
        array(
            'cid' => '13',
            'name' => 'qta_money',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        14 =>
        array(
            'cid' => '14',
            'name' => 'totale_money',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        15 =>
        array(
            'cid' => '15',
            'name' => 'nome',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        16 =>
        array(
            'cid' => '16',
            'name' => 'motivazione',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        17 =>
        array(
            'cid' => '17',
            'name' => 'TID',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        
    ),


    'dati_servizio' =>
    /* la data del servizio attuale e l'ora finale del servizio, che si imposta dai settaggi principali */
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'data',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'ora_finale_servizio',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
    'id_occupati_forno' =>
    /* gli id degli ordini che sono occupati nel forno, 
    con relativo id del terminale occupante e operatore */
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'id_terminale',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'id_comanda',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        3 =>
        array(
            'cid' => '3',
            'name' => 'operatore',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
    'socket_listeners' =>
    /* gli indirizzi ip dei "socket server" */
    array(
        0 =>
        /* di solito va messo da 0 a 2, e devono essere tutti diversi */
        /* Massimo per ora si possono mettere 3 socket, che poi verranno
        gestiti da socket_listener.js */
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        1 =>
        /* indirizzo ip */
        array(
            'cid' => '1',
            'name' => 'ip',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        /* solitamente corrisponde all'id */
        array(
            'cid' => '2',
            'name' => 'priorita',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        3 =>
        /* è sempre TERMINALE */
        array(
            'cid' => '3',
            'name' => 'tipo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        4 =>
        /* sono i millisecondi di delay tra una richiesta al socket e la successiva
        di solito è 25, ma se è un server molto lento si può alzare il numero
        anche fino a 100ms */
        array(
            'cid' => '4',
            'name' => 'delay',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
    'specifiche' =>
    /* Si gestisce da Gestione -> Specifiche. Serve a creare i bottoni che appaiono
    sul popup di scelta delle specifiche, all'interno della schermata dell'ordine */
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        1 =>
        /* Testo del bottone */
        array(
            'cid' => '1',
            'name' => 'descrizione',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        /* Prezzo della specifica, può essere es. -1.00, 1.00, -10%, 10% */
        array(
            'cid' => '2',
            'name' => 'prezzo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        3 =>
        /* il colore del tasto */
        array(
            'cid' => '3',
            'name' => 'colore',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        4 =>
        /* quanto spazio occupa questa specifica in forno, ad esempio se è MAXI
        può occupare 2 pizze. E' un numero. */
        array(
            'cid' => '4',
            'name' => 'spazio_forno',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        5 =>
        /* L'ordine della specifica nella visualizzazione da cellulare */
        array(
            'cid' => '5',
            'name' => 'ordine_telefono',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
    'tagli_buoni' =>
    /* sarebbero i prezzi che hanno i buoni pasto, per tipo,
    es. Ticket Restaurant ha i tagli da 3.60, 5.16 ecc
    E' un numero con 2 decimali */
    array(
        0 =>
        /* id autoincrement del taglio del buono */
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'INTEGER',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '1',
        ),
        1 =>
        /* id del circuito del buono pasto (tab. circuiti) */
        array(
            'cid' => '1',
            'name' => 'id_buono',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        /* importo del taglio del buono (numero 2 decimali) */
        array(
            'cid' => '2',
            'name' => 'importo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
    'progressivo_asporto' =>
    /* E' un progressivo che aumenta man mano che fai comande non intestate ad un nome 
    e serve per chiamare il cliente che deve ritirare il cibo con un numero.
    E' solo 1 riga fissa nella tabella. Id è sempre 1. Numero è il progressivo */
    array(
        0 =>
        /* id è sempre 1 */
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'INTEGER',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '1',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'numero',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
    'cont_prog_gg_uni' =>
    /* non utilizzata. Era stata fatta per la sincronia degli asporti
    ma poi abbiamo cambiato sistema */
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'INTEGER',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '1',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'data',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'numero',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
    'tabella_iva' =>
    /* tabella con le ive esistenti in germania e italia, che poi appariranno nelle tendine 
    della gestione prodotti, categorie e modifiche articolo nell'ordine */
    array(
        0 =>
        /* autoincrement */
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'INTEGER',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '1',
        ),
        1 =>
        /* percentuale di iva, o intero o con 2 decimali dopo il punto */
        array(
            'cid' => '1',
            'name' => 'aliquota',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        /* ad esempio può essere "ristorazione", "mensa" o "corrente" */
        array(
            'cid' => '2',
            'name' => 'descrizione',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        3 =>
        /* la lingua di chi usa quella percentuale iva. es. "italiano","english","deutsch" */
        array(
            'cid' => '3',
            'name' => 'lingua',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => 'italiano',
            'pk' => '0',
        ),
    ),
    'settaggi_ibrido' =>
    /* settaggi che si applicano SOLO alla versione ibrida */
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        1 =>
        /* Comanda + Conto abilitata (vedi fast.comanda) */
        array(
            'cid' => '1',
            'name' => 'comandapconto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        /* Storicizzazione automatica abilitata (vedi fast.comanda) */
        array(
            'cid' => '2',
            'name' => 'storicizzazioneauto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        3 =>
        /* Incasso Automatico Abilitato (vedi fast.comanda) */
        array(
            'cid' => '3',
            'name' => 'incassoauto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        4 =>
        /* Modalità Sagra 
        Quando entri nel tavolo ti chiede il nome del cliente */
        array(
            'cid' => '4',
            'name' => 'modalitasagra',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        5 =>
        /* Nome del file XML del Menu */
        array(
            'cid' => '5',
            'name' => 'menu',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        6 =>
        /* credo che non sia usato */
        array(
            'cid' => '6',
            'name' => 'controllo_utente',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        7 =>
        /* cancellazione articolo abilitata */
        array(
            'cid' => '7',
            'name' => 'cancellazione_articolo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        8 =>
        /* password per cancellazione articolo */
        /* se è valorizzato */
        array(
            'cid' => '8',
            'name' => 'pwd_canc_articolo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        9 =>
        /* prodotto libero abilitato (S/N) */
        array(
            'cid' => '9',
            'name' => 'p_libero_abilitato',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        10 =>
        /* tasto scontrino abilitato */
        array(
            'cid' => '10',
            'name' => 'tasto_scontrino',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        11 =>
        /* tasto fattura abilitato */
        array(
            'cid' => '11',
            'name' => 'tasto_fattura',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        12 =>
        /* tasto conto abilitato */
        array(
            'cid' => '12',
            'name' => 'tasto_conto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        13 =>
        /* tasto fastconto abilitato. fastconto su tablet o telefono si intende */
        array(
            'cid' => '13',
            'name' => 'tasto_fastconto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        14 =>
        /* tasto conto separato */
        array(
            'cid' => '14',
            'name' => 'tasto_contosep',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        15 =>
        /* tasto incasso abilitato */
        array(
            'cid' => '15',
            'name' => 'tasto_incasso',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        16 =>
        /* tasto poco cotto */
        array(
            'cid' => '16',
            'name' => 'tasto_pococotto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        17 =>
        /* tasto fine cottura */
        array(
            'cid' => '17',
            'name' => 'tasto_finecottura',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        18 =>
        /* vuol dire che il tavolo preso da un cameriere
        può essere continuato SOLO da quel cameriere, ma non da altri */
        array(
            'cid' => '18',
            'name' => 'blocco_tavolo_cameriere',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        19 =>
        /* tasto sconto */
        array(
            'cid' => '19',
            'name' => 'tasto_sconto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        20 =>
        /* praticamente serve per riunire le varianti uguali in un unico record
        tipo se hai 2 limoni ti mette la riga 2 limoni, anzichè mettere limoni per 2 volte
        quindi usando 2 righe (però splittabili) */
        array(
            'cid' => '20',
            'name' => 'varianti_riunite',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        21 =>
        /* modifica del prodotto battuto abilitata */
        array(
            'cid' => '21',
            'name' => 'modifica_prodotto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        22 =>
        array(
            'cid' => '22',
            'name' => 'modifica_prodotto_descrizione',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        23 =>
        array(
            'cid' => '23',
            'name' => 'modifica_prodotto_quantita',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        24 =>
        array(
            'cid' => '24',
            'name' => 'modifica_prodotto_prezzo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        25 =>
        array(
            'cid' => '25',
            'name' => 'modifica_prodotto_destinazione_stampa',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        26 =>
        array(
            'cid' => '26',
            'name' => 'modifica_prodotto_portata',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        27 =>
        array(
            'cid' => '27',
            'name' => 'modifica_prodotto_clona',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        28 =>
        array(
            'cid' => '28',
            'name' => 'modifica_prodotto_servito',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        29 =>
        array(
            'cid' => '29',
            'name' => 'filtro_lettera_bloccato',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        30 =>
        array(
            'cid' => '30',
            'name' => 'tasto_comanda_conto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        31 =>
        array(
            'cid' => '31',
            'name' => 'stampante_fastingresso',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        32 =>
        array(
            'cid' => '32',
            'name' => 'tastiera_scomparsa_auto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        33 =>
        array(
            'cid' => '33',
            'name' => 'cerca_prime_lettere',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        34 =>
        array(
            'cid' => '34',
            'name' => 'tasto_gusti_gelato',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        35 =>
        array(
            'cid' => '35',
            'name' => 'tasto_gutshein',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        36 =>
        array(
            'cid' => '36',
            'name' => 'avviso_incasso',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        37 =>
        array(
            'cid' => '37',
            'name' => 'sconto_non_eliminabile',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        38 =>
        array(
            'cid' => '38',
            'name' => 'avviso_quittung',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        39 =>
        array(
            'cid' => '39',
            'name' => 'avviso_rechnung',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        40 =>
        array(
            'cid' => '40',
            'name' => 'slide_qta_articolo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        41 =>
        array(
            'cid' => '41',
            'name' => 'listino_palmari',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        42 =>
        array(
            'cid' => '42',
            'name' => 'ricerca_su_variante',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        43 =>
        array(
            'cid' => '43',
            'name' => 'ultime_battiture_grandi',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        44 =>
        array(
            'cid' => '44',
            'name' => 'layout_destri',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        45 =>
        array(
            'cid' => '45',
            'name' => 'categorie_fullscreen',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        46 =>
        array(
            'cid' => '46',
            'name' => 'tasto_incasso_manuale',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        47 =>
        array(
            'cid' => '47',
            'name' => 'varianti_su_tastiera',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        48 =>
        array(
            'cid' => '48',
            'name' => 'variante_libera',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        49 =>
        array(
            'cid' => '49',
            'name' => 'mantieni_lettera',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        50 =>
        array(
            'cid' => '50',
            'name' => 'prevenzione_tocco_accidentale_comanda',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        51 =>
        array(
            'cid' => '51',
            'name' => 'tasto_carte_rechnung',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        52 =>
        array(
            'cid' => '52',
            'name' => 'chiusura_tastiera_dopo_prima_lettera',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        53 =>
        array(
            'cid' => '53',
            'name' => 'tutte_le_varianti',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        54 =>
        array(
            'cid' => '54',
            'name' => 'tasto_tavolo_esce',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => '""',
            'pk' => '0',
        ),
        55 =>
        array(
            'cid' => '55',
            'name' => 'azzera_prezzo_veloce',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => '""',
            'pk' => '0',
        ),
        56 =>
        array(
            'cid' => '56',
            'name' => 'modifica_portata_veloce',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => '""',
            'pk' => '0',
        ),
        57 =>
        array(
            'cid' => '57',
            'name' => 'modifica_prodotto_raddoppiaprezzo',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => '""',
            'pk' => '0',
        ),
        58 =>
        array(
            'cid' => '58',
            'name' => 'tasto_comanda_conto_su_comanda',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => '""',
            'pk' => '0',
        ),
    ),
    'cambi_listino' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'giorno',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'mese',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        3 =>
        array(
            'cid' => '3',
            'name' => 'anno',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        4 =>
        array(
            'cid' => '4',
            'name' => 'id_prodotto',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        5 =>
        array(
            'cid' => '5',
            'name' => 'prezzo_1',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        6 =>
        array(
            'cid' => '6',
            'name' => 'prezzo_2',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        7 =>
        array(
            'cid' => '7',
            'name' => 'prezzo_3',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        8 =>
        array(
            'cid' => '8',
            'name' => 'prezzo_4',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        9 =>
        array(
            'cid' => '9',
            'name' => 'descrizione',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        10 =>
        array(
            'cid' => '10',
            'name' => 'categoria',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        11 =>
        array(
            'cid' => '11',
            'name' => 'cod_articolo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        12 =>
        array(
            'cid' => '12',
            'name' => 'pagina',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        13 =>
        array(
            'cid' => '13',
            'name' => 'posizione',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        14 =>
        array(
            'cid' => '14',
            'name' => 'ordinamento',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        15 =>
        array(
            'cid' => '15',
            'name' => 'cat_varianti',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        16 =>
        array(
            'cid' => '16',
            'name' => 'cat_ricorrente',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        17 =>
        array(
            'cid' => '17',
            'name' => 'gruppo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        18 =>
        array(
            'cid' => '18',
            'name' => 'perc_iva_base',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        19 =>
        array(
            'cid' => '19',
            'name' => 'perc_iva_takeaway',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        20 =>
        array(
            'cid' => '20',
            'name' => 'dest_st_1',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        21 =>
        array(
            'cid' => '21',
            'name' => 'dest_st_2',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        22 =>
        array(
            'cid' => '22',
            'name' => 'portata',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        23 =>
        array(
            'cid' => '23',
            'name' => 'ultima_portata',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        24 =>
        array(
            'cid' => '24',
            'name' => 'giacenza_flash',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        25 =>
        array(
            'cid' => '25',
            'name' => 'giacenza_reale',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        26 =>
        array(
            'cid' => '26',
            'name' => 'giacenza',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        27 =>
        array(
            'cid' => '27',
            'name' => 'costo_un',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        28 =>
        array(
            'cid' => '28',
            'name' => 'colore_tasto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        29 =>
        array(
            'cid' => '29',
            'name' => 'coef_trasf',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        30 =>
        array(
            'cid' => '30',
            'name' => 'peso_ums',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        31 =>
        array(
            'cid' => '31',
            'name' => 'desc_lunga',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        32 =>
        array(
            'cid' => '32',
            'name' => 'immagine',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        33 =>
        array(
            'cid' => '33',
            'name' => 'perc_servizio',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        34 =>
        array(
            'cid' => '34',
            'name' => 'ult_mod',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        35 =>
        array(
            'cid' => '35',
            'name' => 'ricetta',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        36 =>
        array(
            'cid' => '36',
            'name' => 'colore_tasto_vecchio',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        37 =>
        array(
            'cid' => '37',
            'name' => 'ricetta_fastorder',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        38 =>
        array(
            'cid' => '38',
            'name' => 'posizione_2',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        39 =>
        array(
            'cid' => '39',
            'name' => 'posizione_3',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        40 =>
        array(
            'cid' => '40',
            'name' => 'posizione_4',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        41 =>
        array(
            'cid' => '41',
            'name' => 'posizione_1',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        42 =>
        array(
            'cid' => '42',
            'name' => 'abilita_riepilogo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        43 =>
        array(
            'cid' => '43',
            'name' => 'cod_promo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        44 =>
        array(
            'cid' => '44',
            'name' => 'qta_fissa',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        45 =>
        array(
            'cid' => '45',
            'name' => 'spazio_forno',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        46 =>
        array(
            'cid' => '46',
            'name' => 'listino_bar',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        47 =>
        array(
            'cid' => '47',
            'name' => 'listino_asporto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        48 =>
        array(
            'cid' => '48',
            'name' => 'listino_continuo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        49 =>
        array(
            'cid' => '49',
            'name' => 'listino_tavolo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        50 =>
        array(
            'cid' => '50',
            'name' => 'prezzo_varianti_aggiuntive',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        51 =>
        array(
            'cid' => '51',
            'name' => 'prezzo_maxi',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        52 =>
        array(
            'cid' => '52',
            'name' => 'prezzo_varianti_maxi',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        53 =>
        array(
            'cid' => '53',
            'name' => 'prezzo_maxi_prima',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        54 =>
        array(
            'cid' => '54',
            'name' => 'automodifica_prezzo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        55 =>
        array(
            'cid' => '55',
            'name' => 'prezzo_variante_meno',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        56 =>
        array(
            'cid' => '56',
            'name' => 'prezzo_fattorino1_norm',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        57 =>
        array(
            'cid' => '57',
            'name' => 'prezzo_fattorino2_norm',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        58 =>
        array(
            'cid' => '58',
            'name' => 'prezzo_fattorino3_norm',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        59 =>
        array(
            'cid' => '59',
            'name' => 'prezzo_fattorino1_maxi',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        60 =>
        array(
            'cid' => '60',
            'name' => 'prezzo_fattorino2_maxi',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        61 =>
        array(
            'cid' => '61',
            'name' => 'prezzo_fattorino3_maxi',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        62 =>
        array(
            'cid' => '62',
            'name' => 'reparto_servizi',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        63 =>
        array(
            'cid' => '63',
            'name' => 'reparto_beni',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
    'appuntamenti_calendario' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'da_data',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'da_ora',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        3 =>
        array(
            'cid' => '3',
            'name' => 'da_minuti',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        4 =>
        array(
            'cid' => '4',
            'name' => 'a_data',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        5 =>
        array(
            'cid' => '5',
            'name' => 'a_ora',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        6 =>
        array(
            'cid' => '6',
            'name' => 'a_minuti',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        7 =>
        array(
            'cid' => '7',
            'name' => 'nome',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        8 =>
        array(
            'cid' => '8',
            'name' => 'cognome',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        9 =>
        array(
            'cid' => '9',
            'name' => 'telefono',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        10 =>
        array(
            'cid' => '10',
            'name' => 'tipologia',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        11 =>
        array(
            'cid' => '11',
            'name' => 'qta_persone',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        12 =>
        array(
            'cid' => '12',
            'name' => 'testo',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        13 =>
        array(
            'cid' => '13',
            'name' => 'candeline',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        14 =>
        array(
            'cid' => '14',
            'name' => 'fontana',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        15 =>
        array(
            'cid' => '15',
            'name' => 'candeline_anni',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        16 =>
        array(
            'cid' => '16',
            'name' => 'avviso',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        17 =>
        array(
            'cid' => '17',
            'name' => 'selezione_gruppo_ordinazione',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        18 =>
        array(
            'cid' => '18',
            'name' => 'ordinazione_pertinenza',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        19 =>
        array(
            'cid' => '19',
            'name' => 'titolo_personale',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        20 =>
        array(
            'cid' => '20',
            'name' => 'ordine_evaso',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        21 =>
        array(
            'cid' => '21',
            'name' => 'avviso_stampato',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
    ),
    'gruppi_calendario' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'gruppo1',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'gruppo2',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        3 =>
        array(
            'cid' => '3',
            'name' => 'gruppo3',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        4 =>
        array(
            'cid' => '4',
            'name' => 'gruppo4',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
    ),
    'consegna_tempo' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'consegna_ora',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'consegna_costo',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
    ),
    'cod_promo_2' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'INTEGER',
            'notnull' => '1',
            'dflt_value' => NULL,
            'pk' => '1',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'id_articolo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'qta_soglia',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        3 =>
        array(
            'cid' => '3',
            'name' => 'tipo_sconto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        4 =>
        array(
            'cid' => '4',
            'name' => 'valore_sconto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        5 =>
        array(
            'cid' => '5',
            'name' => 'da_ora',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        6 =>
        array(
            'cid' => '6',
            'name' => 'a_ora',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        7 =>
        array(
            'cid' => '7',
            'name' => 'giorni_settimana',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
    'progressivi_fiscali' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'INTEGER',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '1',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'FATTURA',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'DATA_FATTURA',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        3 =>
        array(
            'cid' => '3',
            'name' => 'ANNO_PROGRESSIVI',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        4 =>
        array(
            'cid' => '4',
            'name' => 'QUITTUNG',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        5 =>
        array(
            'cid' => '5',
            'name' => 'RECHNUNG',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        6 =>
        array(
            'cid' => '6',
            'name' => 'ZBERICHT',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
    ),
    'chiusure_fiscali' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'INTEGER',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '1',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'data_servizio',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'data_emissione',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        3 =>
        array(
            'cid' => '3',
            'name' => 'ora_emissione',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        4 =>
        array(
            'cid' => '4',
            'name' => 'lingua_stampa',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        5 =>
        array(
            'cid' => '5',
            'name' => 'protocollo',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        6 =>
        array(
            'cid' => '6',
            'name' => 'progressivo_riga',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        7 =>
        array(
            'cid' => '7',
            'name' => 'operatore',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        8 =>
        array(
            'cid' => '8',
            'name' => 'tipo_record',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        9 =>
        array(
            'cid' => '9',
            'name' => 'descrizione',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        10 =>
        array(
            'cid' => '10',
            'name' => 'quantita',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        11 =>
        array(
            'cid' => '11',
            'name' => 'importo',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        12 =>
        array(
            'cid' => '12',
            'name' => 'nome_locale',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        13 =>
        array(
            'cid' => '13',
            'name' => 'fiscale_sn',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        14 =>
        array(
            'cid' => '14',
            'name' => 'nome_pc',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        15 =>
        array(
            'cid' => '15',
            'name' => 'valore_fiscale',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
    ),
    'impostazioni_fiscali' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'INTEGER',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '1',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'lingua_stampa',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'aliquota_default',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        3 =>
        array(
            'cid' => '3',
            'name' => 'aliquota_takeaway',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        4 =>
        array(
            'cid' => '4',
            'name' => 'valore_coperto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        5 =>
        array(
            'cid' => '5',
            'name' => 'coperti_obbligatorio',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        6 =>
        array(
            'cid' => '6',
            'name' => 'righe_zero',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        7 =>
        array(
            'cid' => '7',
            'name' => 'max_caratteri',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        8 =>
        array(
            'cid' => '8',
            'name' => 'categoria_partenza',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        9 =>
        array(
            'cid' => '9',
            'name' => 'valore_servizio',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        10 =>
        array(
            'cid' => '10',
            'name' => 'visualizzazione_coperti',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        11 =>
        array(
            'cid' => '11',
            'name' => 'fastorder',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        12 =>
        array(
            'cid' => '12',
            'name' => 'grandezza_font_articoli',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        13 =>
        array(
            'cid' => '13',
            'name' => 'grandezza_font_categorie',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        14 =>
        array(
            'cid' => '14',
            'name' => 'grandezza_font_tavoli',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        15 =>
        array(
            'cid' => '15',
            'name' => 'touch_articolo_conto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        16 =>
        array(
            'cid' => '16',
            'name' => 'sconto_su_promo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        17 =>
        array(
            'cid' => '17',
            'name' => 'articolo_a_peso',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        18 =>
        array(
            'cid' => '18',
            'name' => 'prezzo_x_um',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        19 =>
        array(
            'cid' => '19',
            'name' => 'varianti_riunite',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        20 =>
        array(
            'cid' => '20',
            'name' => 'ricerca_alfabetica',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        21 =>
        array(
            'cid' => '21',
            'name' => 'ricerca_tastiera',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        22 =>
        array(
            'cid' => '22',
            'name' => 'filtro_ricerca_generico',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        23 =>
        array(
            'cid' => '23',
            'name' => 'listino_predefinito',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        24 =>
        array(
            'cid' => '24',
            'name' => 'categoria_tavoli',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        25 =>
        array(
            'cid' => '25',
            'name' => 'listino_tavoli',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        26 =>
        array(
            'cid' => '26',
            'name' => 'categoria_bar',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        27 =>
        array(
            'cid' => '27',
            'name' => 'listino_bar',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        28 =>
        array(
            'cid' => '28',
            'name' => 'categoria_asporto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        29 =>
        array(
            'cid' => '29',
            'name' => 'listino_asporto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        30 =>
        array(
            'cid' => '30',
            'name' => 'categoria_continuo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        31 =>
        array(
            'cid' => '31',
            'name' => 'listino_continuo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        32 =>
        array(
            'cid' => '32',
            'name' => 'multiquantita',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        33 =>
        array(
            'cid' => '33',
            'name' => 'categoria_partenza_mobile',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        34 =>
        array(
            'cid' => '34',
            'name' => 'cerca_prime_lettere',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        35 =>
        array(
            'cid' => '35',
            'name' => 'consegna_a_pizza',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        36 =>
        array(
            'cid' => '36',
            'name' => 'consegna_mezzo_metro',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        37 =>
        array(
            'cid' => '37',
            'name' => 'consegna_metro',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        38 =>
        array(
            'cid' => '38',
            'name' => 'consegna_su_totale',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        39 =>
        array(
            'cid' => '39',
            'name' => 'una_pizza',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        40 =>
        array(
            'cid' => '40',
            'name' => 'piu_di_una_pizza',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        41 =>
        array(
            'cid' => '41',
            'name' => 'iva_estera_abilitata',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        42 =>
        array(
            'cid' => '42',
            'name' => 'multiprofilo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        43 =>
        array(
            'cid' => '43',
            'name' => 'tasto_visu_varianti',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        44 =>
        array(
            'cid' => '44',
            'name' => 'clona_diretto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        45 =>
        array(
            'cid' => '45',
            'name' => 'pcx',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        46 =>
        array(
            'cid' => '46',
            'name' => 'pcy',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        47 =>
        array(
            'cid' => '47',
            'name' => 'simx',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        48 =>
        array(
            'cid' => '48',
            'name' => 'simy',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        49 =>
        array(
            'cid' => '49',
            'name' => 'pdax',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        50 =>
        array(
            'cid' => '50',
            'name' => 'pday',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        51 =>
        array(
            'cid' => '51',
            'name' => 'consegna_maxi',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        52 =>
        array(
            'cid' => '52',
            'name' => 'da_fascia_1',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        53 =>
        array(
            'cid' => '53',
            'name' => 'a_fascia_1',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        54 =>
        array(
            'cid' => '54',
            'name' => 'da_fascia_2',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        55 =>
        array(
            'cid' => '55',
            'name' => 'a_fascia_2',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        56 =>
        array(
            'cid' => '56',
            'name' => 'fissa_da_fascia_1',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        57 =>
        array(
            'cid' => '57',
            'name' => 'fissa_a_fascia_1',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        58 =>
        array(
            'cid' => '58',
            'name' => 'fissa_una_pizza',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        59 =>
        array(
            'cid' => '59',
            'name' => 'fissa_da_fascia_2',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        60 =>
        array(
            'cid' => '60',
            'name' => 'fissa_a_fascia_2',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        61 =>
        array(
            'cid' => '61',
            'name' => 'fissa_piu_di_una_pizza',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        62 =>
        array(
            'cid' => '62',
            'name' => 'abilita_fattorino1',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        63 =>
        array(
            'cid' => '63',
            'name' => 'abilita_fattorino2',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        64 =>
        array(
            'cid' => '64',
            'name' => 'abilita_fattorino3',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        65 =>
        array(
            'cid' => '65',
            'name' => 'nome_fattorino1',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        66 =>
        array(
            'cid' => '66',
            'name' => 'nome_fattorino2',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        67 =>
        array(
            'cid' => '67',
            'name' => 'nome_fattorino3',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        68 =>
        array(
            'cid' => '68',
            'name' => 'orario_preparazione',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        69 =>
        array(
            'cid' => '69',
            'name' => 'tempo_preparazione_default',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        70 =>
        array(
            'cid' => '70',
            'name' => 'reparto_servizi_default',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        71 =>
        array(
            'cid' => '71',
            'name' => 'reparto_beni_default',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        72 =>
        array(
            'cid' => '72',
            'name' => 'tempo_preparazione_default_domicilio',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        73 =>
        array(
            'cid' => '73',
            'name' => 'aliquota_consegna',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        74 =>
        array(
            'cid' => '74',
            'name' => 'reparto_consegna',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        75 =>
        array(
            'cid' => '75',
            'name' => 'reparto_ritiro',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'0\'',
            'pk' => '0',
        ),
        76 =>
        array(
            'cid' => '76',
            'name' => 'reparto_domicilio',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'0\'',
            'pk' => '0',
        ),
        77 =>
        array(
            'cid' => '77',
            'name' => 'reparto_mangiaqui',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'0\'',
            'pk' => '0',
        ),
        78 =>
        array(
            'cid' => '78',
            'name' => 'tastiera_ricerca_partenza_numerica',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'0\'',
            'pk' => '0',
        ),
        79 =>
        array(
            'cid' => '79',
            'name' => 'tasto_visu_varianti_menopiu',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '""',
            'pk' => '0',
        ),
        80 =>
        array(
            'cid' => '80',
            'name' => 'tasto_visu_varianti',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '""',
            'pk' => '0',
        ),
        81 =>
        array(
            'cid' => '81',
            'name' => 'tasto_visu_varianti_piumeno',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '""',
            'pk' => '0',
        ),
        82 =>
        array(
            'cid' => '82',
            'name' => 'avviso_coperti_comanda',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '""',
            'pk' => '0',
        ),
        83 =>
        array(
            'cid' => '83',
            'name' => 'time_gap_consegna',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '""',
            'pk' => '0',
        ),
        84 =>
        array(
            'cid' => '84',
            'name' => 'salva_numero_associa',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '""',
            'pk' => '0',
        )
    ),
    'operatori' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'INTEGER',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '1',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'nome',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'password',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        3 =>
        array(
            'cid' => '3',
            'name' => 'permesso',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        4 =>
        array(
            'cid' => '4',
            'name' => 'p_statistiche',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        5 =>
        array(
            'cid' => '5',
            'name' => 'p_gestioni',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        6 =>
        array(
            'cid' => '6',
            'name' => 'p_layout',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        7 =>
        array(
            'cid' => '7',
            'name' => 'p_backoffice',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        8 =>
        array(
            'cid' => '8',
            'name' => 'p_chiusura',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        9 =>
        array(
            'cid' => '9',
            'name' => 'p_mappa_tavoli',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        10 =>
        array(
            'cid' => '10',
            'name' => 'p_gestione_clienti',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        11 =>
        array(
            'cid' => '11',
            'name' => 'p_mappa_tavoli_phone',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        12 =>
        array(
            'cid' => '12',
            'name' => 'p_verifiche',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
    'gestione_buoni_sconto' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'INTEGER',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '1',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'codice_buono',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'data_emissione',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        3 =>
        array(
            'cid' => '3',
            'name' => 'percentuale_buono',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        4 =>
        array(
            'cid' => '4',
            'name' => 'data_scadenza',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        5 =>
        array(
            'cid' => '5',
            'name' => 'data_utilizzo',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        6 =>
        array(
            'cid' => '6',
            'name' => 'id_comanda',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        7 =>
        array(
            'cid' => '7',
            'name' => 'n_scontrino_incassato',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        8 =>
        array(
            'cid' => '8',
            'name' => 'n_term_incassato',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        9 =>
        array(
            'cid' => '9',
            'name' => 'data_emissione_ascii',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        10 =>
        array(
            'cid' => '10',
            'name' => 'data_utilizzo_ascii',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        11 =>
        array(
            'cid' => '11',
            'name' => 'nome_pc_incasso',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        12 =>
        array(
            'cid' => '12',
            'name' => 'giorno_incasso',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        13 =>
        array(
            'cid' => '13',
            'name' => 'nome_pc_emissione',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        14 =>
        array(
            'cid' => '14',
            'name' => 'giorno_emissione',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        15 =>
        array(
            'cid' => '15',
            'name' => 'valore_euro',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        16 =>
        array(
            'cid' => '16',
            'name' => 'tipo_sconto',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
    ),
    'profilo_aziendale' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'INTEGER',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '1',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'ragione_sociale',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'indirizzo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        3 =>
        array(
            'cid' => '3',
            'name' => 'citta',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        4 =>
        array(
            'cid' => '4',
            'name' => 'provincia',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        5 =>
        array(
            'cid' => '5',
            'name' => 'telefono',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        6 =>
        array(
            'cid' => '6',
            'name' => 'email',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        7 =>
        array(
            'cid' => '7',
            'name' => 'codice_fiscale',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        8 =>
        array(
            'cid' => '8',
            'name' => 'partita_iva',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        9 =>
        array(
            'cid' => '9',
            'name' => 'cod_cliente',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        10 =>
        array(
            'cid' => '10',
            'name' => 'tipo_cliente',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        11 =>
        array(
            'cid' => '11',
            'name' => 'cap',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        12 =>
        array(
            'cid' => '12',
            'name' => 'paese',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        13 =>
        array(
            'cid' => '13',
            'name' => 'fatt_fine_mese_sn',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        14 =>
        array(
            'cid' => '14',
            'name' => 'cat_cliente',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        15 =>
        array(
            'cid' => '15',
            'name' => 'cellulare',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        16 =>
        array(
            'cid' => '16',
            'name' => 'fax',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        17 =>
        array(
            'cid' => '17',
            'name' => 'contatto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        18 =>
        array(
            'cid' => '18',
            'name' => 'banca',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        19 =>
        array(
            'cid' => '19',
            'name' => 'abi',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        20 =>
        array(
            'cid' => '20',
            'name' => 'cab',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        21 =>
        array(
            'cid' => '21',
            'name' => 'cin',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        22 =>
        array(
            'cid' => '22',
            'name' => 'cc',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        23 =>
        array(
            'cid' => '23',
            'name' => 'iban',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        24 =>
        array(
            'cid' => '24',
            'name' => 'bic_swift',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        25 =>
        array(
            'cid' => '25',
            'name' => 'listino',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        26 =>
        array(
            'cid' => '26',
            'name' => 'esenzione',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        27 =>
        array(
            'cid' => '27',
            'name' => 'aliquota_iva_agev',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        28 =>
        array(
            'cid' => '28',
            'name' => 'cod_pag_desc',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        29 =>
        array(
            'cid' => '29',
            'name' => 'sconto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        30 =>
        array(
            'cid' => '30',
            'name' => 'cos_mnem_circ',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        31 =>
        array(
            'cid' => '31',
            'name' => 'cod_rif_cli_circ',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        32 =>
        array(
            'cid' => '32',
            'name' => 'foto_cliente',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        33 =>
        array(
            'cid' => '33',
            'name' => 'note',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        34 =>
        array(
            'cid' => '34',
            'name' => 'numero',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        35 =>
        array(
            'cid' => '35',
            'name' => 'attivazione_tessere',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        36 =>
        array(
            'cid' => '36',
            'name' => 'data_attivazione',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        37 =>
        array(
            'cid' => '37',
            'name' => 'tipo_tessere',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        38 =>
        array(
            'cid' => '38',
            'name' => 'validita_tessere',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        39 =>
        array(
            'cid' => '39',
            'name' => 'monouso_giornaliero',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        40 =>
        array(
            'cid' => '40',
            'name' => 'uso_privato_aziendale',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        41 =>
        array(
            'cid' => '41',
            'name' => 'valore_tessera',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        42 =>
        array(
            'cid' => '42',
            'name' => 'nome_locale',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        43 =>
        array(
            'cid' => '43',
            'name' => 'licenza',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        44 =>
        array(
            'cid' => '44',
            'name' => 'comune',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        45 =>
        array(
            'cid' => '45',
            'name' => 'id_nazione',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        46 =>
        array(
            'cid' => '46',
            'name' => 'nazione',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        47 =>
        array(
            'cid' => '47',
            'name' => 'numero_licenza',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        48 =>
        array(
            'cid' => '48',
            'name' => 'centro',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '"000"',
            'pk' => '0',
        ),
        49 =>
        array(
            'cid' => '49',
            'name' => 'tentativo_scaricamento_licenza',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '"0"',
            'pk' => '0',
        ),
        50 =>
        array(
            'cid' => '50',
            'name' => 'data_scadenza_licenza',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '""',
            'pk' => '0',
        ),
    ),
    'settaggi_profili' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'INTEGER',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '1',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'commento',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'Field3',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        3 =>
        array(
            'cid' => '3',
            'name' => 'Field4',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        4 =>
        array(
            'cid' => '4',
            'name' => 'Field5',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        5 =>
        array(
            'cid' => '5',
            'name' => 'Field6',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        6 =>
        array(
            'cid' => '6',
            'name' => 'Field7',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        7 =>
        array(
            'cid' => '7',
            'name' => 'Field8',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        8 =>
        array(
            'cid' => '8',
            'name' => 'Field9',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        9 =>
        array(
            'cid' => '9',
            'name' => 'Field10',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        10 =>
        array(
            'cid' => '10',
            'name' => 'Field11',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        11 =>
        array(
            'cid' => '11',
            'name' => 'Field12',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        12 =>
        array(
            'cid' => '12',
            'name' => 'Field13',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        13 =>
        array(
            'cid' => '13',
            'name' => 'Field14',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        14 =>
        array(
            'cid' => '14',
            'name' => 'Field15',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        15 =>
        array(
            'cid' => '15',
            'name' => 'Field16',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        16 =>
        array(
            'cid' => '16',
            'name' => 'Field17',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        17 =>
        array(
            'cid' => '17',
            'name' => 'Field18',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        18 =>
        array(
            'cid' => '18',
            'name' => 'Field19',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        19 =>
        array(
            'cid' => '19',
            'name' => 'Field20',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        20 =>
        array(
            'cid' => '20',
            'name' => 'Field21',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        21 =>
        array(
            'cid' => '21',
            'name' => 'Field22',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        22 =>
        array(
            'cid' => '22',
            'name' => 'Field23',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        23 =>
        array(
            'cid' => '23',
            'name' => 'Field24',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        24 =>
        array(
            'cid' => '24',
            'name' => 'Field25',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        25 =>
        array(
            'cid' => '25',
            'name' => 'Field26',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        26 =>
        array(
            'cid' => '26',
            'name' => 'Field27',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        27 =>
        array(
            'cid' => '27',
            'name' => 'Field28',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        28 =>
        array(
            'cid' => '28',
            'name' => 'Field29',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        29 =>
        array(
            'cid' => '29',
            'name' => 'Field30',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        30 =>
        array(
            'cid' => '30',
            'name' => 'Field31',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        31 =>
        array(
            'cid' => '31',
            'name' => 'Field32',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        32 =>
        array(
            'cid' => '32',
            'name' => 'Field33',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        33 =>
        array(
            'cid' => '33',
            'name' => 'Field34',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        34 =>
        array(
            'cid' => '34',
            'name' => 'Field35',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        35 =>
        array(
            'cid' => '35',
            'name' => 'Field36',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        36 =>
        array(
            'cid' => '36',
            'name' => 'Field37',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        37 =>
        array(
            'cid' => '37',
            'name' => 'Field38',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        38 =>
        array(
            'cid' => '38',
            'name' => 'Field39',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        39 =>
        array(
            'cid' => '39',
            'name' => 'Field40',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        40 =>
        array(
            'cid' => '40',
            'name' => 'Field41',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        41 =>
        array(
            'cid' => '41',
            'name' => 'Field42',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        42 =>
        array(
            'cid' => '42',
            'name' => 'Field43',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        43 =>
        array(
            'cid' => '43',
            'name' => 'Field44',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        44 =>
        array(
            'cid' => '44',
            'name' => 'Field45',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        45 =>
        array(
            'cid' => '45',
            'name' => 'Field46',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        46 =>
        array(
            'cid' => '46',
            'name' => 'Field47',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        47 =>
        array(
            'cid' => '47',
            'name' => 'Field48',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        48 =>
        array(
            'cid' => '48',
            'name' => 'Field49',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        49 =>
        array(
            'cid' => '49',
            'name' => 'Field50',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        50 =>
        array(
            'cid' => '50',
            'name' => 'Field51',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        51 =>
        array(
            'cid' => '51',
            'name' => 'Field52',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        52 =>
        array(
            'cid' => '52',
            'name' => 'Field53',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        53 =>
        array(
            'cid' => '53',
            'name' => 'Field54',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        54 =>
        array(
            'cid' => '54',
            'name' => 'Field55',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        55 =>
        array(
            'cid' => '55',
            'name' => 'Field56',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        56 =>
        array(
            'cid' => '56',
            'name' => 'Field57',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        57 =>
        array(
            'cid' => '57',
            'name' => 'Field58',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        58 =>
        array(
            'cid' => '58',
            'name' => 'Field59',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        59 =>
        array(
            'cid' => '59',
            'name' => 'Field60',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        60 =>
        array(
            'cid' => '60',
            'name' => 'Field61',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        61 =>
        array(
            'cid' => '61',
            'name' => 'Field62',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        62 =>
        array(
            'cid' => '62',
            'name' => 'Field63',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        63 =>
        array(
            'cid' => '63',
            'name' => 'Field64',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        64 =>
        array(
            'cid' => '64',
            'name' => 'Field65',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        65 =>
        array(
            'cid' => '65',
            'name' => 'Field66',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        66 =>
        array(
            'cid' => '66',
            'name' => 'Field67',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        67 =>
        array(
            'cid' => '67',
            'name' => 'Field68',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        68 =>
        array(
            'cid' => '68',
            'name' => 'Field69',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        69 =>
        array(
            'cid' => '69',
            'name' => 'Field70',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        70 =>
        array(
            'cid' => '70',
            'name' => 'Field71',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        71 =>
        array(
            'cid' => '71',
            'name' => 'Field72',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        72 =>
        array(
            'cid' => '72',
            'name' => 'Field73',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        73 =>
        array(
            'cid' => '73',
            'name' => 'Field74',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        74 =>
        array(
            'cid' => '74',
            'name' => 'Field75',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        75 =>
        array(
            'cid' => '75',
            'name' => 'Field76',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        76 =>
        array(
            'cid' => '76',
            'name' => 'Field77',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        77 =>
        array(
            'cid' => '77',
            'name' => 'Field78',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        78 =>
        array(
            'cid' => '78',
            'name' => 'Field79',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        79 =>
        array(
            'cid' => '79',
            'name' => 'Field80',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        80 =>
        array(
            'cid' => '80',
            'name' => 'Field81',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        81 =>
        array(
            'cid' => '81',
            'name' => 'Field82',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        82 =>
        array(
            'cid' => '82',
            'name' => 'Field83',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        83 =>
        array(
            'cid' => '83',
            'name' => 'Field84',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        84 =>
        array(
            'cid' => '84',
            'name' => 'Field85',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        85 =>
        array(
            'cid' => '85',
            'name' => 'Field86',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        86 =>
        array(
            'cid' => '86',
            'name' => 'Field87',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        87 =>
        array(
            'cid' => '87',
            'name' => 'Field88',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        88 =>
        array(
            'cid' => '88',
            'name' => 'Field89',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        89 =>
        array(
            'cid' => '89',
            'name' => 'Field90',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        90 =>
        array(
            'cid' => '90',
            'name' => 'Field91',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        91 =>
        array(
            'cid' => '91',
            'name' => 'Field92',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        92 =>
        array(
            'cid' => '92',
            'name' => 'Field93',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        93 =>
        array(
            'cid' => '93',
            'name' => 'Field94',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        94 =>
        array(
            'cid' => '94',
            'name' => 'Field95',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        95 =>
        array(
            'cid' => '95',
            'name' => 'Field96',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        96 =>
        array(
            'cid' => '96',
            'name' => 'Field97',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        97 =>
        array(
            'cid' => '97',
            'name' => 'Field98',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        98 =>
        array(
            'cid' => '98',
            'name' => 'Field99',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        99 =>
        array(
            'cid' => '99',
            'name' => 'Field100',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        100 =>
        array(
            'cid' => '100',
            'name' => 'Field101',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        101 =>
        array(
            'cid' => '101',
            'name' => 'Field102',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        102 =>
        array(
            'cid' => '102',
            'name' => 'Field103',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        103 =>
        array(
            'cid' => '103',
            'name' => 'Field104',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        104 =>
        array(
            'cid' => '104',
            'name' => 'Field105',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        105 =>
        array(
            'cid' => '105',
            'name' => 'Field106',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        106 =>
        array(
            'cid' => '106',
            'name' => 'Field107',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        107 =>
        array(
            'cid' => '107',
            'name' => 'Field108',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        108 =>
        array(
            'cid' => '108',
            'name' => 'Field109',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        109 =>
        array(
            'cid' => '109',
            'name' => 'Field110',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        110 =>
        array(
            'cid' => '110',
            'name' => 'Field111',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        111 =>
        array(
            'cid' => '111',
            'name' => 'Field112',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        112 =>
        array(
            'cid' => '112',
            'name' => 'Field113',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        113 =>
        array(
            'cid' => '113',
            'name' => 'Field114',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        114 =>
        array(
            'cid' => '114',
            'name' => 'Field115',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        115 =>
        array(
            'cid' => '115',
            'name' => 'Field116',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        116 =>
        array(
            'cid' => '116',
            'name' => 'Field117',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        117 =>
        array(
            'cid' => '117',
            'name' => 'Field118',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        118 =>
        array(
            'cid' => '118',
            'name' => 'Field119',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        119 =>
        array(
            'cid' => '119',
            'name' => 'Field120',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        120 =>
        array(
            'cid' => '120',
            'name' => 'Field121',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        121 =>
        array(
            'cid' => '121',
            'name' => 'Field122',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        122 =>
        array(
            'cid' => '122',
            'name' => 'Field123',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        123 =>
        array(
            'cid' => '123',
            'name' => 'Field124',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        124 =>
        array(
            'cid' => '124',
            'name' => 'Field125',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        125 =>
        array(
            'cid' => '125',
            'name' => 'Field126',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        126 =>
        array(
            'cid' => '126',
            'name' => 'Field127',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        127 =>
        array(
            'cid' => '127',
            'name' => 'Field128',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        128 =>
        array(
            'cid' => '128',
            'name' => 'Field129',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        129 =>
        array(
            'cid' => '129',
            'name' => 'Field130',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        130 =>
        array(
            'cid' => '130',
            'name' => 'Field131',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        131 =>
        array(
            'cid' => '131',
            'name' => 'Field132',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        132 =>
        array(
            'cid' => '132',
            'name' => 'Field133',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        133 =>
        array(
            'cid' => '133',
            'name' => 'Field134',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        134 =>
        array(
            'cid' => '134',
            'name' => 'Field135',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        135 =>
        array(
            'cid' => '135',
            'name' => 'Field136',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        136 =>
        array(
            'cid' => '136',
            'name' => 'Field137',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        137 =>
        array(
            'cid' => '137',
            'name' => 'Field138',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        138 =>
        array(
            'cid' => '138',
            'name' => 'Field139',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        139 =>
        array(
            'cid' => '139',
            'name' => 'Field140',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        140 =>
        array(
            'cid' => '140',
            'name' => 'Field141',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        141 =>
        array(
            'cid' => '141',
            'name' => 'Field142',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        142 =>
        array(
            'cid' => '142',
            'name' => 'Field143',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        143 =>
        array(
            'cid' => '143',
            'name' => 'Field144',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        144 =>
        array(
            'cid' => '144',
            'name' => 'Field145',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        145 =>
        array(
            'cid' => '145',
            'name' => 'Field146',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        146 =>
        array(
            'cid' => '146',
            'name' => 'Field147',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        147 =>
        array(
            'cid' => '147',
            'name' => 'Field148',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        148 =>
        array(
            'cid' => '148',
            'name' => 'Field149',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        149 =>
        array(
            'cid' => '149',
            'name' => 'Field150',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        150 =>
        array(
            'cid' => '150',
            'name' => 'Field151',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        151 =>
        array(
            'cid' => '151',
            'name' => 'Field152',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        152 =>
        array(
            'cid' => '152',
            'name' => 'Field153',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        153 =>
        array(
            'cid' => '153',
            'name' => 'Field154',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        154 =>
        array(
            'cid' => '154',
            'name' => 'Field155',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        155 =>
        array(
            'cid' => '155',
            'name' => 'Field156',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        156 =>
        array(
            'cid' => '156',
            'name' => 'Field157',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        157 =>
        array(
            'cid' => '157',
            'name' => 'Field158',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        158 =>
        array(
            'cid' => '158',
            'name' => 'Field159',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        159 =>
        array(
            'cid' => '159',
            'name' => 'Field160',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        160 =>
        array(
            'cid' => '160',
            'name' => 'Field161',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        161 =>
        array(
            'cid' => '161',
            'name' => 'Field162',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        162 =>
        array(
            'cid' => '162',
            'name' => 'Field163',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        163 =>
        array(
            'cid' => '163',
            'name' => 'Field164',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        164 =>
        array(
            'cid' => '164',
            'name' => 'Field165',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        165 =>
        array(
            'cid' => '165',
            'name' => 'Field166',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        166 =>
        array(
            'cid' => '166',
            'name' => 'Field167',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        167 =>
        array(
            'cid' => '167',
            'name' => 'Field168',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        168 =>
        array(
            'cid' => '168',
            'name' => 'Field169',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        169 =>
        array(
            'cid' => '169',
            'name' => 'Field170',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        170 =>
        array(
            'cid' => '170',
            'name' => 'Field171',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        171 =>
        array(
            'cid' => '171',
            'name' => 'Field172',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        172 =>
        array(
            'cid' => '172',
            'name' => 'Field173',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        173 =>
        array(
            'cid' => '173',
            'name' => 'Field174',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        174 =>
        array(
            'cid' => '174',
            'name' => 'Field175',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        175 =>
        array(
            'cid' => '175',
            'name' => 'Field176',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        176 =>
        array(
            'cid' => '176',
            'name' => 'Field177',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        177 =>
        array(
            'cid' => '177',
            'name' => 'Field178',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        178 =>
        array(
            'cid' => '178',
            'name' => 'Field179',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        179 =>
        array(
            'cid' => '179',
            'name' => 'Field180',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        180 =>
        array(
            'cid' => '180',
            'name' => 'Field181',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        181 =>
        array(
            'cid' => '181',
            'name' => 'Field182',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        182 =>
        array(
            'cid' => '182',
            'name' => 'Field183',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        183 =>
        array(
            'cid' => '183',
            'name' => 'Field184',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        184 =>
        array(
            'cid' => '184',
            'name' => 'Field185',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        185 =>
        array(
            'cid' => '185',
            'name' => 'Field186',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        186 =>
        array(
            'cid' => '186',
            'name' => 'Field187',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        187 =>
        array(
            'cid' => '187',
            'name' => 'Field188',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        188 =>
        array(
            'cid' => '188',
            'name' => 'Field189',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        189 =>
        array(
            'cid' => '189',
            'name' => 'Field190',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        190 =>
        array(
            'cid' => '190',
            'name' => 'Field191',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        191 =>
        array(
            'cid' => '191',
            'name' => 'Field192',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        192 =>
        array(
            'cid' => '192',
            'name' => 'Field193',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        193 =>
        array(
            'cid' => '193',
            'name' => 'Field194',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        194 =>
        array(
            'cid' => '194',
            'name' => 'Field195',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        195 =>
        array(
            'cid' => '195',
            'name' => 'Field196',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        196 =>
        array(
            'cid' => '196',
            'name' => 'Field197',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        197 =>
        array(
            'cid' => '197',
            'name' => 'Field198',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        198 =>
        array(
            'cid' => '198',
            'name' => 'Field199',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        199 =>
        array(
            'cid' => '199',
            'name' => 'Field200',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        200 =>
        array(
            'cid' => '200',
            'name' => 'Field201',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        201 =>
        array(
            'cid' => '201',
            'name' => 'Field202',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        202 =>
        array(
            'cid' => '202',
            'name' => 'Field203',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        203 =>
        array(
            'cid' => '203',
            'name' => 'Field204',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        204 =>
        array(
            'cid' => '204',
            'name' => 'Field205',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        205 =>
        array(
            'cid' => '205',
            'name' => 'Field206',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        206 =>
        array(
            'cid' => '206',
            'name' => 'Field207',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        207 =>
        array(
            'cid' => '207',
            'name' => 'Field208',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        208 =>
        array(
            'cid' => '208',
            'name' => 'Field209',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        209 =>
        array(
            'cid' => '209',
            'name' => 'Field210',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        210 =>
        array(
            'cid' => '210',
            'name' => 'Field211',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        211 =>
        array(
            'cid' => '211',
            'name' => 'Field212',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        212 =>
        array(
            'cid' => '212',
            'name' => 'Field213',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        213 =>
        array(
            'cid' => '213',
            'name' => 'Field214',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        214 =>
        array(
            'cid' => '214',
            'name' => 'Field215',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        215 =>
        array(
            'cid' => '215',
            'name' => 'Field216',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        216 =>
        array(
            'cid' => '216',
            'name' => 'Field217',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        217 =>
        array(
            'cid' => '217',
            'name' => 'Field218',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        218 =>
        array(
            'cid' => '218',
            'name' => 'Field219',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        219 =>
        array(
            'cid' => '219',
            'name' => 'Field220',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        220 =>
        array(
            'cid' => '220',
            'name' => 'Field221',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        221 =>
        array(
            'cid' => '221',
            'name' => 'Field222',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        222 =>
        array(
            'cid' => '222',
            'name' => 'Field223',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        223 =>
        array(
            'cid' => '223',
            'name' => 'Field224',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        224 =>
        array(
            'cid' => '224',
            'name' => 'Field225',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        225 =>
        array(
            'cid' => '225',
            'name' => 'Field226',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        226 =>
        array(
            'cid' => '226',
            'name' => 'Field227',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        227 =>
        array(
            'cid' => '227',
            'name' => 'Field228',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        228 =>
        array(
            'cid' => '228',
            'name' => 'Field229',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        229 =>
        array(
            'cid' => '229',
            'name' => 'Field230',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        230 =>
        array(
            'cid' => '230',
            'name' => 'Field231',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        231 =>
        array(
            'cid' => '231',
            'name' => 'Field232',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        232 =>
        array(
            'cid' => '232',
            'name' => 'Field233',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        233 =>
        array(
            'cid' => '233',
            'name' => 'Field234',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        234 =>
        array(
            'cid' => '234',
            'name' => 'Field235',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        235 =>
        array(
            'cid' => '235',
            'name' => 'Field236',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        236 =>
        array(
            'cid' => '236',
            'name' => 'Field237',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        237 =>
        array(
            'cid' => '237',
            'name' => 'Field238',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        238 =>
        array(
            'cid' => '238',
            'name' => 'Field239',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        239 =>
        array(
            'cid' => '239',
            'name' => 'Field240',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        240 =>
        array(
            'cid' => '240',
            'name' => 'Field241',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        241 =>
        array(
            'cid' => '241',
            'name' => 'Field242',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        242 =>
        array(
            'cid' => '242',
            'name' => 'Field243',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        243 =>
        array(
            'cid' => '243',
            'name' => 'Field244',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        244 =>
        array(
            'cid' => '244',
            'name' => 'Field245',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        245 =>
        array(
            'cid' => '245',
            'name' => 'Field246',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        246 =>
        array(
            'cid' => '246',
            'name' => 'Field247',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        247 =>
        array(
            'cid' => '247',
            'name' => 'Field248',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        248 =>
        array(
            'cid' => '248',
            'name' => 'Field249',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        249 =>
        array(
            'cid' => '249',
            'name' => 'Field250',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        250 =>
        array(
            'cid' => '250',
            'name' => 'Field251',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        251 =>
        array(
            'cid' => '251',
            'name' => 'Field252',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        252 =>
        array(
            'cid' => '252',
            'name' => 'Field253',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        253 =>
        array(
            'cid' => '253',
            'name' => 'Field254',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        254 =>
        array(
            'cid' => '254',
            'name' => 'Field255',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        255 =>
        array(
            'cid' => '255',
            'name' => 'Field256',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        256 =>
        array(
            'cid' => '256',
            'name' => 'Field257',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        257 =>
        array(
            'cid' => '257',
            'name' => 'Field258',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        258 =>
        array(
            'cid' => '258',
            'name' => 'Field259',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        259 =>
        array(
            'cid' => '259',
            'name' => 'Field260',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        260 =>
        array(
            'cid' => '260',
            'name' => 'Field261',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        261 =>
        array(
            'cid' => '261',
            'name' => 'Field262',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        262 =>
        array(
            'cid' => '262',
            'name' => 'Field263',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        263 =>
        array(
            'cid' => '263',
            'name' => 'Field264',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        264 =>
        array(
            'cid' => '264',
            'name' => 'Field265',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        265 =>
        array(
            'cid' => '265',
            'name' => 'Field266',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        266 =>
        array(
            'cid' => '266',
            'name' => 'Field267',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        267 =>
        array(
            'cid' => '267',
            'name' => 'Field268',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        268 =>
        array(
            'cid' => '268',
            'name' => 'Field269',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        269 =>
        array(
            'cid' => '269',
            'name' => 'Field270',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        270 =>
        array(
            'cid' => '270',
            'name' => 'Field271',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        271 =>
        array(
            'cid' => '271',
            'name' => 'Field272',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        272 =>
        array(
            'cid' => '272',
            'name' => 'Field273',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        273 =>
        array(
            'cid' => '273',
            'name' => 'Field274',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        274 =>
        array(
            'cid' => '274',
            'name' => 'Field275',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        275 =>
        array(
            'cid' => '275',
            'name' => 'Field276',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        276 =>
        array(
            'cid' => '276',
            'name' => 'Field277',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        277 =>
        array(
            'cid' => '277',
            'name' => 'Field278',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        278 =>
        array(
            'cid' => '278',
            'name' => 'Field279',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        279 =>
        array(
            'cid' => '279',
            'name' => 'Field280',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        280 =>
        array(
            'cid' => '280',
            'name' => 'Field281',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        281 =>
        array(
            'cid' => '281',
            'name' => 'Field282',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        282 =>
        array(
            'cid' => '282',
            'name' => 'Field283',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        283 =>
        array(
            'cid' => '283',
            'name' => 'Field284',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        284 =>
        array(
            'cid' => '284',
            'name' => 'Field285',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        285 =>
        array(
            'cid' => '285',
            'name' => 'Field286',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        286 =>
        array(
            'cid' => '286',
            'name' => 'Field287',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        287 =>
        array(
            'cid' => '287',
            'name' => 'Field288',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        288 =>
        array(
            'cid' => '288',
            'name' => 'Field289',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        289 =>
        array(
            'cid' => '289',
            'name' => 'Field290',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        290 =>
        array(
            'cid' => '290',
            'name' => 'Field291',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        291 =>
        array(
            'cid' => '291',
            'name' => 'Field292',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        292 =>
        array(
            'cid' => '292',
            'name' => 'Field293',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        293 =>
        array(
            'cid' => '293',
            'name' => 'Field294',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        294 =>
        array(
            'cid' => '294',
            'name' => 'Field295',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        295 =>
        array(
            'cid' => '295',
            'name' => 'Field296',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        296 =>
        array(
            'cid' => '296',
            'name' => 'Field297',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        297 =>
        array(
            'cid' => '297',
            'name' => 'Field298',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        298 =>
        array(
            'cid' => '298',
            'name' => 'Field299',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        299 =>
        array(
            'cid' => '299',
            'name' => 'Field300',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        300 =>
        array(
            'cid' => '300',
            'name' => 'Field301',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        301 =>
        array(
            'cid' => '301',
            'name' => 'Field302',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        302 =>
        array(
            'cid' => '302',
            'name' => 'Field303',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        303 =>
        array(
            'cid' => '303',
            'name' => 'Field304',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        304 =>
        array(
            'cid' => '304',
            'name' => 'Field305',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        305 =>
        array(
            'cid' => '305',
            'name' => 'Field306',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        306 =>
        array(
            'cid' => '306',
            'name' => 'Field307',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        307 =>
        array(
            'cid' => '307',
            'name' => 'Field308',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        308 =>
        array(
            'cid' => '308',
            'name' => 'Field309',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        309 =>
        array(
            'cid' => '309',
            'name' => 'Field310',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        310 =>
        array(
            'cid' => '310',
            'name' => 'Field311',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        311 =>
        array(
            'cid' => '311',
            'name' => 'Field312',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        312 =>
        array(
            'cid' => '312',
            'name' => 'Field313',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        313 =>
        array(
            'cid' => '313',
            'name' => 'Field314',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        314 =>
        array(
            'cid' => '314',
            'name' => 'Field315',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        315 =>
        array(
            'cid' => '315',
            'name' => 'Field316',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        316 =>
        array(
            'cid' => '316',
            'name' => 'Field317',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        317 =>
        array(
            'cid' => '317',
            'name' => 'Field318',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        318 =>
        array(
            'cid' => '318',
            'name' => 'Field319',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        319 =>
        array(
            'cid' => '319',
            'name' => 'Field320',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        320 =>
        array(
            'cid' => '320',
            'name' => 'Field321',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        321 =>
        array(
            'cid' => '321',
            'name' => 'Field322',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        322 =>
        array(
            'cid' => '322',
            'name' => 'Field323',
            'type' => 'INTEGER',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        323 =>
        array(
            'cid' => '323',
            'name' => 'Field324',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        324 =>
        array(
            'cid' => '324',
            'name' => 'Field325',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        325 =>
        array(
            'cid' => '325',
            'name' => 'Field326',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        326 =>
        array(
            'cid' => '326',
            'name' => 'Field327',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        327 =>
        array(
            'cid' => '327',
            'name' => 'Field328',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        328 =>
        array(
            'cid' => '328',
            'name' => 'Field329',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        329 =>
        array(
            'cid' => '329',
            'name' => 'Field330',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        330 =>
        array(
            'cid' => '330',
            'name' => 'Field331',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        331 =>
        array(
            'cid' => '331',
            'name' => 'Field332',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        332 =>
        array(
            'cid' => '332',
            'name' => 'Field333',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        333 =>
        array(
            'cid' => '333',
            'name' => 'Field334',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        334 =>
        array(
            'cid' => '334',
            'name' => 'Field335',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        335 =>
        array(
            'cid' => '335',
            'name' => 'Field336',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        336 =>
        array(
            'cid' => '336',
            'name' => 'Field337',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        337 =>
        array(
            'cid' => '337',
            'name' => 'Field338',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        338 =>
        array(
            'cid' => '338',
            'name' => 'Field339',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        339 =>
        array(
            'cid' => '339',
            'name' => 'Field340',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        340 =>
        array(
            'cid' => '340',
            'name' => 'Field341',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        341 =>
        array(
            'cid' => '341',
            'name' => 'Field342',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        342 =>
        array(
            'cid' => '342',
            'name' => 'Field343',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        343 =>
        array(
            'cid' => '343',
            'name' => 'Field344',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        344 =>
        array(
            'cid' => '344',
            'name' => 'Field345',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        345 =>
        array(
            'cid' => '345',
            'name' => 'Field346',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        346 =>
        array(
            'cid' => '346',
            'name' => 'Field347',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        347 =>
        array(
            'cid' => '347',
            'name' => 'Field348',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        348 =>
        array(
            'cid' => '348',
            'name' => 'Field349',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        349 =>
        array(
            'cid' => '349',
            'name' => 'Field350',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        350 =>
        array(
            'cid' => '350',
            'name' => 'nome_profilo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        351 =>
        array(
            'cid' => '351',
            'name' => 'Field352',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        352 =>
        array(
            'cid' => '352',
            'name' => 'Field353',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        353 =>
        array(
            'cid' => '353',
            'name' => 'Field354',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        354 =>
        array(
            'cid' => '354',
            'name' => 'Field355',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        355 =>
        array(
            'cid' => '355',
            'name' => 'Field356',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        356 =>
        array(
            'cid' => '356',
            'name' => 'Field357',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        357 =>
        array(
            'cid' => '357',
            'name' => 'Field358',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        358 =>
        array(
            'cid' => '358',
            'name' => 'Field359',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        359 =>
        array(
            'cid' => '359',
            'name' => 'Field360',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        360 =>
        array(
            'cid' => '360',
            'name' => 'Field361',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        361 =>
        array(
            'cid' => '361',
            'name' => 'Field351',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        362 =>
        array(
            'cid' => '362',
            'name' => 'Field363',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        363 =>
        array(
            'cid' => '363',
            'name' => 'Field364',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        364 =>
        array(
            'cid' => '364',
            'name' => 'Field365',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        365 =>
        array(
            'cid' => '365',
            'name' => 'Field366',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        366 =>
        array(
            'cid' => '366',
            'name' => 'Field367',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        367 =>
        array(
            'cid' => '367',
            'name' => 'Field368',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        368 =>
        array(
            'cid' => '368',
            'name' => 'Field369',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        369 =>
        array(
            'cid' => '369',
            'name' => 'Field370',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        370 =>
        array(
            'cid' => '370',
            'name' => 'Field371',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        371 =>
        array(
            'cid' => '371',
            'name' => 'Field372',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        372 =>
        array(
            'cid' => '372',
            'name' => 'Field373',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        373 =>
        array(
            'cid' => '373',
            'name' => 'Field374',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        374 =>
        array(
            'cid' => '374',
            'name' => 'Field375',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        375 =>
        array(
            'cid' => '375',
            'name' => 'Field376',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        376 =>
        array(
            'cid' => '376',
            'name' => 'Field377',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        377 =>
        array(
            'cid' => '377',
            'name' => 'Field378',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        378 =>
        array(
            'cid' => '378',
            'name' => 'Field379',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        379 =>
        array(
            'cid' => '379',
            'name' => 'Field380',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        380 =>
        array(
            'cid' => '380',
            'name' => 'Field381',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        381 =>
        array(
            'cid' => '381',
            'name' => 'Field382',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        382 =>
        array(
            'cid' => '382',
            'name' => 'Field383',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        383 =>
        array(
            'cid' => '383',
            'name' => 'Field384',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        384 =>
        array(
            'cid' => '384',
            'name' => 'Field385',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        385 =>
        array(
            'cid' => '385',
            'name' => 'Field386',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        386 =>
        array(
            'cid' => '386',
            'name' => 'Field387',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        387 =>
        array(
            'cid' => '387',
            'name' => 'Field388',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        388 =>
        array(
            'cid' => '388',
            'name' => 'Field389',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        389 =>
        array(
            'cid' => '389',
            'name' => 'Field390',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        390 =>
        array(
            'cid' => '390',
            'name' => 'Field391',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        391 =>
        array(
            'cid' => '391',
            'name' => 'Field392',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        392 =>
        array(
            'cid' => '392',
            'name' => 'Field393',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        393 =>
        array(
            'cid' => '393',
            'name' => 'Field394',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        394 =>
        array(
            'cid' => '394',
            'name' => 'Field395',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        395 =>
        array(
            'cid' => '395',
            'name' => 'Field396',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        396 =>
        array(
            'cid' => '396',
            'name' => 'Field397',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        397 =>
        array(
            'cid' => '397',
            'name' => 'Field398',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        398 =>
        array(
            'cid' => '398',
            'name' => 'Field399',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        399 =>
        array(
            'cid' => '399',
            'name' => 'Field400',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        400 =>
        array(
            'cid' => '400',
            'name' => 'Field401',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        401 =>
        array(
            'cid' => '401',
            'name' => 'Field402',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        402 =>
        array(
            'cid' => '402',
            'name' => 'Field403',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        403 =>
        array(
            'cid' => '403',
            'name' => 'Field404',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        404 =>
        array(
            'cid' => '404',
            'name' => 'Field405',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        405 =>
        array(
            'cid' => '405',
            'name' => 'Field406',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        406 =>
        array(
            'cid' => '406',
            'name' => 'Field407',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        407 =>
        array(
            'cid' => '407',
            'name' => 'Field408',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        408 =>
        array(
            'cid' => '408',
            'name' => 'Field409',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        409 =>
        array(
            'cid' => '409',
            'name' => 'Field410',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        410 =>
        array(
            'cid' => '410',
            'name' => 'piantina_partenza',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        411 =>
        array(
            'cid' => '411',
            'name' => 'Field412',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        412 =>
        array(
            'cid' => '412',
            'name' => 'Field413',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        413 =>
        array(
            'cid' => '413',
            'name' => 'Field414',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        414 =>
        array(
            'cid' => '414',
            'name' => 'Field415',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        415 =>
        array(
            'cid' => '415',
            'name' => 'Field416',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        416 =>
        array(
            'cid' => '416',
            'name' => 'Field417',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        417 =>
        array(
            'cid' => '417',
            'name' => 'Field418',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        418 =>
        array(
            'cid' => '418',
            'name' => 'Field419',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        419 =>
        array(
            'cid' => '419',
            'name' => 'Field420',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        420 =>
        array(
            'cid' => '420',
            'name' => 'Field421',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        421 =>
        array(
            'cid' => '421',
            'name' => 'Field422',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        422 =>
        array(
            'cid' => '422',
            'name' => 'Field423',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        423 =>
        array(
            'cid' => '423',
            'name' => 'Field424',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        424 =>
        array(
            'cid' => '424',
            'name' => 'Field425',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        425 =>
        array(
            'cid' => '425',
            'name' => 'Field426',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        426 =>
        array(
            'cid' => '426',
            'name' => 'Field427',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        427 =>
        array(
            'cid' => '427',
            'name' => 'Field428',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        428 =>
        array(
            'cid' => '428',
            'name' => 'Field429',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        429 =>
        array(
            'cid' => '429',
            'name' => 'Field430',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        430 =>
        array(
            'cid' => '430',
            'name' => 'Field431',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        431 =>
        array(
            'cid' => '431',
            'name' => 'Field362',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        432 =>
        array(
            'cid' => '432',
            'name' => 'Field432',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        433 =>
        array(
            'cid' => '433',
            'name' => 'Field433',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        434 =>
        array(
            'cid' => '434',
            'name' => 'Field434',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        435 =>
        array(
            'cid' => '435',
            'name' => 'Field435',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        436 =>
        array(
            'cid' => '436',
            'name' => 'Field436',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        437 =>
        array(
            'cid' => '437',
            'name' => 'Field437',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        438 =>
        array(
            'cid' => '438',
            'name' => 'Field438',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        439 =>
        array(
            'cid' => '439',
            'name' => 'Field439',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        440 =>
        array(
            'cid' => '440',
            'name' => 'Field440',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        441 =>
        array(
            'cid' => '441',
            'name' => 'Field441',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        442 =>
        array(
            'cid' => '442',
            'name' => 'Field442',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        443 =>
        array(
            'cid' => '443',
            'name' => 'Field443',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        444 =>
        array(
            'cid' => '444',
            'name' => 'Field444',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        445 =>
        array(
            'cid' => '445',
            'name' => 'Field445',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        446 =>
        array(
            'cid' => '446',
            'name' => 'Field446',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        447 =>
        array(
            'cid' => '447',
            'name' => 'Field447',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        448 =>
        array(
            'cid' => '448',
            'name' => 'Field448',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        449 =>
        array(
            'cid' => '449',
            'name' => 'Field449',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        450 =>
        array(
            'cid' => '450',
            'name' => 'Field450',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        451 =>
        array(
            'cid' => '451',
            'name' => 'Field451',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        452 =>
        array(
            'cid' => '452',
            'name' => 'Field452',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        453 =>
        array(
            'cid' => '453',
            'name' => 'Field453',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        454 =>
        array(
            'cid' => '454',
            'name' => 'Field454',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        455 =>
        array(
            'cid' => '455',
            'name' => 'Field455',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        456 =>
        array(
            'cid' => '456',
            'name' => 'Field456',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        457 =>
        array(
            'cid' => '457',
            'name' => 'Field457',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        458 =>
        array(
            'cid' => '458',
            'name' => 'Field458',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        459 =>
        array(
            'cid' => '459',
            'name' => 'Field459',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        460 =>
        array(
            'cid' => '460',
            'name' => 'Field460',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        461 =>
        array(
            'cid' => '461',
            'name' => 'Field461',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        462 =>
        array(
            'cid' => '462',
            'name' => 'Field462',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        463 =>
        array(
            'cid' => '463',
            'name' => 'Field463',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        464 =>
        array(
            'cid' => '464',
            'name' => 'Field464',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        465 =>
        array(
            'cid' => '465',
            'name' => 'Field465',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        466 =>
        array(
            'cid' => '466',
            'name' => 'Field466',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        467 =>
        array(
            'cid' => '467',
            'name' => 'Field467',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        468 =>
        array(
            'cid' => '468',
            'name' => 'Field468',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        469 =>
        array(
            'cid' => '469',
            'name' => 'Field469',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        470 =>
        array(
            'cid' => '470',
            'name' => 'Field470',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        471 =>
        array(
            'cid' => '471',
            'name' => 'Field471',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        472 =>
        array(
            'cid' => '472',
            'name' => 'Field472',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        473 =>
        array(
            'cid' => '473',
            'name' => 'Field473',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        474 =>
        array(
            'cid' => '474',
            'name' => 'Field474',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        475 =>
        array(
            'cid' => '475',
            'name' => 'Field475',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        476 =>
        array(
            'cid' => '476',
            'name' => 'Field476',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        477 =>
        array(
            'cid' => '477',
            'name' => 'Field477',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        478 =>
        array(
            'cid' => '478',
            'name' => 'Field478',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        479 =>
        array(
            'cid' => '479',
            'name' => 'Field479',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        480 =>
        array(
            'cid' => '480',
            'name' => 'Field480',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        481 =>
        array(
            'cid' => '481',
            'name' => 'Field481',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        482 =>
        array(
            'cid' => '482',
            'name' => 'Field482',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        483 =>
        array(
            'cid' => '483',
            'name' => 'Field483',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        484 =>
        array(
            'cid' => '484',
            'name' => 'Field484',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        485 =>
        array(
            'cid' => '485',
            'name' => 'Field485',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        486 =>
        array(
            'cid' => '486',
            'name' => 'Field486',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        487 =>
        array(
            'cid' => '487',
            'name' => 'Field487',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        488 =>
        array(
            'cid' => '488',
            'name' => 'Field488',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        489 =>
        array(
            'cid' => '489',
            'name' => 'Field489',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        490 =>
        array(
            'cid' => '490',
            'name' => 'Field490',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        491 =>
        array(
            'cid' => '491',
            'name' => 'Field491',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        492 =>
        array(
            'cid' => '492',
            'name' => 'Field492',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        493 =>
        array(
            'cid' => '493',
            'name' => 'Field493',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        494 =>
        array(
            'cid' => '494',
            'name' => 'Field494',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        495 =>
        array(
            'cid' => '495',
            'name' => 'Field495',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        496 =>
        array(
            'cid' => '496',
            'name' => 'Field496',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        497 =>
        array(
            'cid' => '497',
            'name' => 'Field497',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        498 =>
        array(
            'cid' => '498',
            'name' => 'Field498',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        499 =>
        array(
            'cid' => '499',
            'name' => 'Field499',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        500 =>
        array(
            'cid' => '500',
            'name' => 'Field500',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        501 =>
        array(
            'cid' => '501',
            'name' => 'Field501',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        502 =>
        array(
            'cid' => '502',
            'name' => 'Field502',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        503 =>
        array(
            'cid' => '503',
            'name' => 'Field503',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        504 =>
        array(
            'cid' => '504',
            'name' => 'Field504',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        505 =>
        array(
            'cid' => '505',
            'name' => 'Field505',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        506 =>
        array(
            'cid' => '506',
            'name' => 'Field506',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        507 =>
        array(
            'cid' => '507',
            'name' => 'Field507',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        508 =>
        array(
            'cid' => '508',
            'name' => 'Field508',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        509 =>
        array(
            'cid' => '509',
            'name' => 'Field509',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        510 =>
        array(
            'cid' => '510',
            'name' => 'Field510',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        511 =>
        array(
            'cid' => '511',
            'name' => 'Field511',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        512 =>
        array(
            'cid' => '512',
            'name' => 'Field512',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        513 =>
        array(
            'cid' => '513',
            'name' => 'Field513',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        514 =>
        array(
            'cid' => '514',
            'name' => 'Field514',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        515 =>
        array(
            'cid' => '515',
            'name' => 'Field515',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        516 =>
        array(
            'cid' => '516',
            'name' => 'Field516',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        517 =>
        array(
            'cid' => '517',
            'name' => 'Field517',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        518 =>
        array(
            'cid' => '518',
            'name' => 'Field518',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        519 =>
        array(
            'cid' => '519',
            'name' => 'Field519',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        520 =>
        array(
            'cid' => '520',
            'name' => 'Field520',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        521 =>
        array(
            'cid' => '521',
            'name' => 'Field521',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        522 =>
        array(
            'cid' => '522',
            'name' => 'Field522',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        523 =>
        array(
            'cid' => '523',
            'name' => 'Field523',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        524 =>
        array(
            'cid' => '524',
            'name' => 'Field524',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        525 =>
        array(
            'cid' => '525',
            'name' => 'Field525',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        526 =>
        array(
            'cid' => '526',
            'name' => 'Field526',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        527 =>
        array(
            'cid' => '527',
            'name' => 'Field527',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        528 =>
        array(
            'cid' => '528',
            'name' => 'Field528',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        529 =>
        array(
            'cid' => '529',
            'name' => 'Field529',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        530 =>
        array(
            'cid' => '530',
            'name' => 'Field530',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        531 =>
        array(
            'cid' => '531',
            'name' => 'Field531',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        532 =>
        array(
            'cid' => '532',
            'name' => 'Field532',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        533 =>
        array(
            'cid' => '533',
            'name' => 'Field533',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        534 =>
        array(
            'cid' => '534',
            'name' => 'Field534',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        535 =>
        array(
            'cid' => '535',
            'name' => 'Field535',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        536 =>
        array(
            'cid' => '536',
            'name' => 'Field536',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        537 =>
        array(
            'cid' => '537',
            'name' => 'Field537',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        538 =>
        array(
            'cid' => '538',
            'name' => 'Field538',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        539 =>
        array(
            'cid' => '539',
            'name' => 'Field539',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        540 =>
        array(
            'cid' => '540',
            'name' => 'Field540',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        541 =>
        array(
            'cid' => '541',
            'name' => 'Field541',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        542 =>
        array(
            'cid' => '542',
            'name' => 'Field542',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        543 =>
        array(
            'cid' => '543',
            'name' => 'Field543',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        544 =>
        array(
            'cid' => '544',
            'name' => 'Field544',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        545 =>
        array(
            'cid' => '545',
            'name' => 'Field545',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        546 =>
        array(
            'cid' => '546',
            'name' => 'Field546',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        547 =>
        array(
            'cid' => '547',
            'name' => 'Field547',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        548 =>
        array(
            'cid' => '548',
            'name' => 'Field548',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        549 =>
        array(
            'cid' => '549',
            'name' => 'Field549',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        550 =>
        array(
            'cid' => '550',
            'name' => 'Field550',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
    'circuiti' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'INTEGER',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '1',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'ragione_sociale',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'indirizzo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        3 =>
        array(
            'cid' => '3',
            'name' => 'cap',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        4 =>
        array(
            'cid' => '4',
            'name' => 'citta',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        5 =>
        array(
            'cid' => '5',
            'name' => 'provincia',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        6 =>
        array(
            'cid' => '6',
            'name' => 'paese',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        7 =>
        array(
            'cid' => '7',
            'name' => 'telefono',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        8 =>
        array(
            'cid' => '8',
            'name' => 'codice_fiscale',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        9 =>
        array(
            'cid' => '9',
            'name' => 'partita_iva',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        10 =>
        array(
            'cid' => '10',
            'name' => 'email',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        11 =>
        array(
            'cid' => '11',
            'name' => 'cellulare',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        12 =>
        array(
            'cid' => '12',
            'name' => 'numero',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        13 =>
        array(
            'cid' => '13',
            'name' => 'fax',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        14 =>
        array(
            'cid' => '14',
            'name' => 'ordinamento',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        15 =>
        array(
            'cid' => '15',
            'name' => 'adeguamento_perc',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        16 =>
        array(
            'cid' => '16',
            'name' => 'partita_iva_estera',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        17 =>
        array(
            'cid' => '17',
            'name' => 'formato_trasmissione',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        18 =>
        array(
            'cid' => '18',
            'name' => 'codice_destinatario',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        19 =>
        array(
            'cid' => '19',
            'name' => 'pec',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        20 =>
        array(
            'cid' => '20',
            'name' => 'regime_fiscale',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        21 =>
        array(
            'cid' => '21',
            'name' => 'iva_non_pagati',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        22 =>
        array(
            'cid' => '22',
            'name' => 'riepilogo_scontrini',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
    'settaggi_profili2' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'commento',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'Field3',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        3 =>
        array(
            'cid' => '3',
            'name' => 'Field4',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        4 =>
        array(
            'cid' => '4',
            'name' => 'Field5',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        5 =>
        array(
            'cid' => '5',
            'name' => 'Field6',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        6 =>
        array(
            'cid' => '6',
            'name' => 'Field7',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        7 =>
        array(
            'cid' => '7',
            'name' => 'Field8',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        8 =>
        array(
            'cid' => '8',
            'name' => 'Field9',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        9 =>
        array(
            'cid' => '9',
            'name' => 'Field10',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        10 =>
        array(
            'cid' => '10',
            'name' => 'Field11',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        11 =>
        array(
            'cid' => '11',
            'name' => 'Field12',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        12 =>
        array(
            'cid' => '12',
            'name' => 'Field13',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        13 =>
        array(
            'cid' => '13',
            'name' => 'Field14',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        14 =>
        array(
            'cid' => '14',
            'name' => 'Field15',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        15 =>
        array(
            'cid' => '15',
            'name' => 'Field16',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        16 =>
        array(
            'cid' => '16',
            'name' => 'Field17',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        17 =>
        array(
            'cid' => '17',
            'name' => 'Field18',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        18 =>
        array(
            'cid' => '18',
            'name' => 'Field19',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        19 =>
        array(
            'cid' => '19',
            'name' => 'Field20',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        20 =>
        array(
            'cid' => '20',
            'name' => 'Field21',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        21 =>
        array(
            'cid' => '21',
            'name' => 'Field22',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        22 =>
        array(
            'cid' => '22',
            'name' => 'Field23',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        23 =>
        array(
            'cid' => '23',
            'name' => 'Field24',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        24 =>
        array(
            'cid' => '24',
            'name' => 'Field25',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        25 =>
        array(
            'cid' => '25',
            'name' => 'Field26',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        26 =>
        array(
            'cid' => '26',
            'name' => 'Field27',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        27 =>
        array(
            'cid' => '27',
            'name' => 'Field28',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        28 =>
        array(
            'cid' => '28',
            'name' => 'Field29',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        29 =>
        array(
            'cid' => '29',
            'name' => 'Field30',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        30 =>
        array(
            'cid' => '30',
            'name' => 'Field31',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        31 =>
        array(
            'cid' => '31',
            'name' => 'Field32',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        32 =>
        array(
            'cid' => '32',
            'name' => 'Field33',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        33 =>
        array(
            'cid' => '33',
            'name' => 'Field34',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        34 =>
        array(
            'cid' => '34',
            'name' => 'Field35',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        35 =>
        array(
            'cid' => '35',
            'name' => 'Field36',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        36 =>
        array(
            'cid' => '36',
            'name' => 'Field37',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        37 =>
        array(
            'cid' => '37',
            'name' => 'Field38',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        38 =>
        array(
            'cid' => '38',
            'name' => 'Field39',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        39 =>
        array(
            'cid' => '39',
            'name' => 'Field40',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        40 =>
        array(
            'cid' => '40',
            'name' => 'Field41',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        41 =>
        array(
            'cid' => '41',
            'name' => 'Field42',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        42 =>
        array(
            'cid' => '42',
            'name' => 'Field43',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        43 =>
        array(
            'cid' => '43',
            'name' => 'Field44',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        44 =>
        array(
            'cid' => '44',
            'name' => 'Field45',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        45 =>
        array(
            'cid' => '45',
            'name' => 'Field46',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        46 =>
        array(
            'cid' => '46',
            'name' => 'Field47',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        47 =>
        array(
            'cid' => '47',
            'name' => 'Field48',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        48 =>
        array(
            'cid' => '48',
            'name' => 'Field49',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        49 =>
        array(
            'cid' => '49',
            'name' => 'Field50',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        50 =>
        array(
            'cid' => '50',
            'name' => 'Field51',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        51 =>
        array(
            'cid' => '51',
            'name' => 'Field52',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        52 =>
        array(
            'cid' => '52',
            'name' => 'Field53',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        53 =>
        array(
            'cid' => '53',
            'name' => 'Field54',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        54 =>
        array(
            'cid' => '54',
            'name' => 'Field55',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        55 =>
        array(
            'cid' => '55',
            'name' => 'Field56',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        56 =>
        array(
            'cid' => '56',
            'name' => 'Field57',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        57 =>
        array(
            'cid' => '57',
            'name' => 'Field58',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        58 =>
        array(
            'cid' => '58',
            'name' => 'Field59',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        59 =>
        array(
            'cid' => '59',
            'name' => 'Field60',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        60 =>
        array(
            'cid' => '60',
            'name' => 'Field61',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        61 =>
        array(
            'cid' => '61',
            'name' => 'Field62',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        62 =>
        array(
            'cid' => '62',
            'name' => 'Field63',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        63 =>
        array(
            'cid' => '63',
            'name' => 'Field64',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        64 =>
        array(
            'cid' => '64',
            'name' => 'Field65',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        65 =>
        array(
            'cid' => '65',
            'name' => 'Field66',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        66 =>
        array(
            'cid' => '66',
            'name' => 'Field67',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        67 =>
        array(
            'cid' => '67',
            'name' => 'Field68',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        68 =>
        array(
            'cid' => '68',
            'name' => 'Field69',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        69 =>
        array(
            'cid' => '69',
            'name' => 'Field70',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        70 =>
        array(
            'cid' => '70',
            'name' => 'Field71',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        71 =>
        array(
            'cid' => '71',
            'name' => 'Field72',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        72 =>
        array(
            'cid' => '72',
            'name' => 'Field73',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        73 =>
        array(
            'cid' => '73',
            'name' => 'Field74',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        74 =>
        array(
            'cid' => '74',
            'name' => 'Field75',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        75 =>
        array(
            'cid' => '75',
            'name' => 'Field76',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        76 =>
        array(
            'cid' => '76',
            'name' => 'Field77',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        77 =>
        array(
            'cid' => '77',
            'name' => 'Field78',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        78 =>
        array(
            'cid' => '78',
            'name' => 'Field79',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        79 =>
        array(
            'cid' => '79',
            'name' => 'Field80',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        80 =>
        array(
            'cid' => '80',
            'name' => 'Field81',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        81 =>
        array(
            'cid' => '81',
            'name' => 'Field82',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        82 =>
        array(
            'cid' => '82',
            'name' => 'Field83',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        83 =>
        array(
            'cid' => '83',
            'name' => 'Field84',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        84 =>
        array(
            'cid' => '84',
            'name' => 'Field85',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        85 =>
        array(
            'cid' => '85',
            'name' => 'Field86',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        86 =>
        array(
            'cid' => '86',
            'name' => 'Field87',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        87 =>
        array(
            'cid' => '87',
            'name' => 'Field88',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        88 =>
        array(
            'cid' => '88',
            'name' => 'Field89',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        89 =>
        array(
            'cid' => '89',
            'name' => 'Field90',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        90 =>
        array(
            'cid' => '90',
            'name' => 'Field91',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        91 =>
        array(
            'cid' => '91',
            'name' => 'Field92',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        92 =>
        array(
            'cid' => '92',
            'name' => 'Field93',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        93 =>
        array(
            'cid' => '93',
            'name' => 'Field94',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        94 =>
        array(
            'cid' => '94',
            'name' => 'Field95',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        95 =>
        array(
            'cid' => '95',
            'name' => 'Field96',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        96 =>
        array(
            'cid' => '96',
            'name' => 'Field97',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        97 =>
        array(
            'cid' => '97',
            'name' => 'Field98',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        98 =>
        array(
            'cid' => '98',
            'name' => 'Field99',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        99 =>
        array(
            'cid' => '99',
            'name' => 'Field100',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        100 =>
        array(
            'cid' => '100',
            'name' => 'Field101',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        101 =>
        array(
            'cid' => '101',
            'name' => 'Field102',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        102 =>
        array(
            'cid' => '102',
            'name' => 'Field103',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        103 =>
        array(
            'cid' => '103',
            'name' => 'Field104',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        104 =>
        array(
            'cid' => '104',
            'name' => 'Field105',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        105 =>
        array(
            'cid' => '105',
            'name' => 'Field106',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        106 =>
        array(
            'cid' => '106',
            'name' => 'Field107',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        107 =>
        array(
            'cid' => '107',
            'name' => 'Field108',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        108 =>
        array(
            'cid' => '108',
            'name' => 'Field109',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        109 =>
        array(
            'cid' => '109',
            'name' => 'Field110',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        110 =>
        array(
            'cid' => '110',
            'name' => 'Field111',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        111 =>
        array(
            'cid' => '111',
            'name' => 'Field112',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        112 =>
        array(
            'cid' => '112',
            'name' => 'Field113',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        113 =>
        array(
            'cid' => '113',
            'name' => 'Field114',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        114 =>
        array(
            'cid' => '114',
            'name' => 'Field115',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        115 =>
        array(
            'cid' => '115',
            'name' => 'Field116',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        116 =>
        array(
            'cid' => '116',
            'name' => 'Field117',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        117 =>
        array(
            'cid' => '117',
            'name' => 'Field118',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        118 =>
        array(
            'cid' => '118',
            'name' => 'Field119',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        119 =>
        array(
            'cid' => '119',
            'name' => 'Field120',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        120 =>
        array(
            'cid' => '120',
            'name' => 'Field121',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        121 =>
        array(
            'cid' => '121',
            'name' => 'Field122',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        122 =>
        array(
            'cid' => '122',
            'name' => 'Field123',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        123 =>
        array(
            'cid' => '123',
            'name' => 'Field124',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        124 =>
        array(
            'cid' => '124',
            'name' => 'Field125',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        125 =>
        array(
            'cid' => '125',
            'name' => 'Field126',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        126 =>
        array(
            'cid' => '126',
            'name' => 'Field127',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        127 =>
        array(
            'cid' => '127',
            'name' => 'Field128',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        128 =>
        array(
            'cid' => '128',
            'name' => 'Field129',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        129 =>
        array(
            'cid' => '129',
            'name' => 'Field130',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        130 =>
        array(
            'cid' => '130',
            'name' => 'Field131',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        131 =>
        array(
            'cid' => '131',
            'name' => 'Field132',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        132 =>
        array(
            'cid' => '132',
            'name' => 'Field133',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        133 =>
        array(
            'cid' => '133',
            'name' => 'Field134',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        134 =>
        array(
            'cid' => '134',
            'name' => 'Field135',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        135 =>
        array(
            'cid' => '135',
            'name' => 'Field136',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        136 =>
        array(
            'cid' => '136',
            'name' => 'Field137',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        137 =>
        array(
            'cid' => '137',
            'name' => 'Field138',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        138 =>
        array(
            'cid' => '138',
            'name' => 'Field139',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        139 =>
        array(
            'cid' => '139',
            'name' => 'Field140',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        140 =>
        array(
            'cid' => '140',
            'name' => 'Field141',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        141 =>
        array(
            'cid' => '141',
            'name' => 'Field142',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        142 =>
        array(
            'cid' => '142',
            'name' => 'Field143',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        143 =>
        array(
            'cid' => '143',
            'name' => 'Field144',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        144 =>
        array(
            'cid' => '144',
            'name' => 'Field145',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        145 =>
        array(
            'cid' => '145',
            'name' => 'Field146',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        146 =>
        array(
            'cid' => '146',
            'name' => 'Field147',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        147 =>
        array(
            'cid' => '147',
            'name' => 'Field148',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        148 =>
        array(
            'cid' => '148',
            'name' => 'Field149',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        149 =>
        array(
            'cid' => '149',
            'name' => 'Field150',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        150 =>
        array(
            'cid' => '150',
            'name' => 'Field151',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        151 =>
        array(
            'cid' => '151',
            'name' => 'Field152',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        152 =>
        array(
            'cid' => '152',
            'name' => 'Field153',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        153 =>
        array(
            'cid' => '153',
            'name' => 'Field154',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        154 =>
        array(
            'cid' => '154',
            'name' => 'Field155',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        155 =>
        array(
            'cid' => '155',
            'name' => 'Field156',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        156 =>
        array(
            'cid' => '156',
            'name' => 'Field157',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        157 =>
        array(
            'cid' => '157',
            'name' => 'Field158',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        158 =>
        array(
            'cid' => '158',
            'name' => 'Field159',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        159 =>
        array(
            'cid' => '159',
            'name' => 'Field160',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        160 =>
        array(
            'cid' => '160',
            'name' => 'Field161',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        161 =>
        array(
            'cid' => '161',
            'name' => 'Field162',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        162 =>
        array(
            'cid' => '162',
            'name' => 'Field163',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        163 =>
        array(
            'cid' => '163',
            'name' => 'Field164',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        164 =>
        array(
            'cid' => '164',
            'name' => 'Field165',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        165 =>
        array(
            'cid' => '165',
            'name' => 'Field166',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        166 =>
        array(
            'cid' => '166',
            'name' => 'Field167',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        167 =>
        array(
            'cid' => '167',
            'name' => 'Field168',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        168 =>
        array(
            'cid' => '168',
            'name' => 'Field169',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        169 =>
        array(
            'cid' => '169',
            'name' => 'Field170',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        170 =>
        array(
            'cid' => '170',
            'name' => 'Field171',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        171 =>
        array(
            'cid' => '171',
            'name' => 'Field172',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        172 =>
        array(
            'cid' => '172',
            'name' => 'Field173',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        173 =>
        array(
            'cid' => '173',
            'name' => 'Field174',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        174 =>
        array(
            'cid' => '174',
            'name' => 'Field175',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        175 =>
        array(
            'cid' => '175',
            'name' => 'Field176',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        176 =>
        array(
            'cid' => '176',
            'name' => 'Field177',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        177 =>
        array(
            'cid' => '177',
            'name' => 'Field178',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        178 =>
        array(
            'cid' => '178',
            'name' => 'Field179',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        179 =>
        array(
            'cid' => '179',
            'name' => 'Field180',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        180 =>
        array(
            'cid' => '180',
            'name' => 'Field181',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        181 =>
        array(
            'cid' => '181',
            'name' => 'Field182',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        182 =>
        array(
            'cid' => '182',
            'name' => 'Field183',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        183 =>
        array(
            'cid' => '183',
            'name' => 'Field184',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        184 =>
        array(
            'cid' => '184',
            'name' => 'Field185',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        185 =>
        array(
            'cid' => '185',
            'name' => 'Field186',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        186 =>
        array(
            'cid' => '186',
            'name' => 'Field187',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        187 =>
        array(
            'cid' => '187',
            'name' => 'Field188',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        188 =>
        array(
            'cid' => '188',
            'name' => 'Field189',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        189 =>
        array(
            'cid' => '189',
            'name' => 'Field190',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        190 =>
        array(
            'cid' => '190',
            'name' => 'Field191',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        191 =>
        array(
            'cid' => '191',
            'name' => 'Field192',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        192 =>
        array(
            'cid' => '192',
            'name' => 'Field193',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        193 =>
        array(
            'cid' => '193',
            'name' => 'Field194',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        194 =>
        array(
            'cid' => '194',
            'name' => 'Field195',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        195 =>
        array(
            'cid' => '195',
            'name' => 'Field196',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        196 =>
        array(
            'cid' => '196',
            'name' => 'Field197',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        197 =>
        array(
            'cid' => '197',
            'name' => 'Field198',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        198 =>
        array(
            'cid' => '198',
            'name' => 'Field199',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        199 =>
        array(
            'cid' => '199',
            'name' => 'Field200',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        200 =>
        array(
            'cid' => '200',
            'name' => 'Field201',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        201 =>
        array(
            'cid' => '201',
            'name' => 'Field202',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        202 =>
        array(
            'cid' => '202',
            'name' => 'Field203',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        203 =>
        array(
            'cid' => '203',
            'name' => 'Field204',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        204 =>
        array(
            'cid' => '204',
            'name' => 'Field205',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        205 =>
        array(
            'cid' => '205',
            'name' => 'Field206',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        206 =>
        array(
            'cid' => '206',
            'name' => 'Field207',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        207 =>
        array(
            'cid' => '207',
            'name' => 'Field208',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        208 =>
        array(
            'cid' => '208',
            'name' => 'Field209',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        209 =>
        array(
            'cid' => '209',
            'name' => 'Field210',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        210 =>
        array(
            'cid' => '210',
            'name' => 'Field211',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        211 =>
        array(
            'cid' => '211',
            'name' => 'Field212',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        212 =>
        array(
            'cid' => '212',
            'name' => 'Field213',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        213 =>
        array(
            'cid' => '213',
            'name' => 'Field214',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        214 =>
        array(
            'cid' => '214',
            'name' => 'Field215',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        215 =>
        array(
            'cid' => '215',
            'name' => 'Field216',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        216 =>
        array(
            'cid' => '216',
            'name' => 'Field217',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        217 =>
        array(
            'cid' => '217',
            'name' => 'Field218',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        218 =>
        array(
            'cid' => '218',
            'name' => 'Field219',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        219 =>
        array(
            'cid' => '219',
            'name' => 'Field220',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        220 =>
        array(
            'cid' => '220',
            'name' => 'Field221',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        221 =>
        array(
            'cid' => '221',
            'name' => 'Field222',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        222 =>
        array(
            'cid' => '222',
            'name' => 'Field223',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        223 =>
        array(
            'cid' => '223',
            'name' => 'Field224',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        224 =>
        array(
            'cid' => '224',
            'name' => 'Field225',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        225 =>
        array(
            'cid' => '225',
            'name' => 'Field226',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        226 =>
        array(
            'cid' => '226',
            'name' => 'Field227',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        227 =>
        array(
            'cid' => '227',
            'name' => 'Field228',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        228 =>
        array(
            'cid' => '228',
            'name' => 'Field229',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        229 =>
        array(
            'cid' => '229',
            'name' => 'Field230',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        230 =>
        array(
            'cid' => '230',
            'name' => 'Field231',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        231 =>
        array(
            'cid' => '231',
            'name' => 'Field232',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        232 =>
        array(
            'cid' => '232',
            'name' => 'Field233',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        233 =>
        array(
            'cid' => '233',
            'name' => 'Field234',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        234 =>
        array(
            'cid' => '234',
            'name' => 'Field235',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        235 =>
        array(
            'cid' => '235',
            'name' => 'Field236',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        236 =>
        array(
            'cid' => '236',
            'name' => 'Field237',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        237 =>
        array(
            'cid' => '237',
            'name' => 'Field238',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        238 =>
        array(
            'cid' => '238',
            'name' => 'Field239',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        239 =>
        array(
            'cid' => '239',
            'name' => 'Field240',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        240 =>
        array(
            'cid' => '240',
            'name' => 'Field241',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        241 =>
        array(
            'cid' => '241',
            'name' => 'Field242',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        242 =>
        array(
            'cid' => '242',
            'name' => 'Field243',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        243 =>
        array(
            'cid' => '243',
            'name' => 'Field244',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        244 =>
        array(
            'cid' => '244',
            'name' => 'Field245',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        245 =>
        array(
            'cid' => '245',
            'name' => 'nome_profilo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        246 =>
        array(
            'cid' => '246',
            'name' => 'Field246',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        247 =>
        array(
            'cid' => '247',
            'name' => 'Field247',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        248 =>
        array(
            'cid' => '248',
            'name' => 'Field248',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        249 =>
        array(
            'cid' => '249',
            'name' => 'Field249',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        250 =>
        array(
            'cid' => '250',
            'name' => 'Field250',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        251 =>
        array(
            'cid' => '251',
            'name' => 'Field251',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        252 =>
        array(
            'cid' => '252',
            'name' => 'Field252',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        253 =>
        array(
            'cid' => '253',
            'name' => 'Field253',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        254 =>
        array(
            'cid' => '254',
            'name' => 'Field254',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        255 =>
        array(
            'cid' => '255',
            'name' => 'Field255',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        256 =>
        array(
            'cid' => '256',
            'name' => 'Field256',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        257 =>
        array(
            'cid' => '257',
            'name' => 'Field257',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        258 =>
        array(
            'cid' => '258',
            'name' => 'Field258',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        259 =>
        array(
            'cid' => '259',
            'name' => 'Field259',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        260 =>
        array(
            'cid' => '260',
            'name' => 'Field260',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        261 =>
        array(
            'cid' => '261',
            'name' => 'Field261',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        262 =>
        array(
            'cid' => '262',
            'name' => 'Field262',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        263 =>
        array(
            'cid' => '263',
            'name' => 'Field263',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        264 =>
        array(
            'cid' => '264',
            'name' => 'Field264',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        265 =>
        array(
            'cid' => '265',
            'name' => 'Field265',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        266 =>
        array(
            'cid' => '266',
            'name' => 'Field266',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        267 =>
        array(
            'cid' => '267',
            'name' => 'Field267',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        268 =>
        array(
            'cid' => '268',
            'name' => 'Field268',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        269 =>
        array(
            'cid' => '269',
            'name' => 'Field269',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        270 =>
        array(
            'cid' => '270',
            'name' => 'Field270',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        271 =>
        array(
            'cid' => '271',
            'name' => 'Field271',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        272 =>
        array(
            'cid' => '272',
            'name' => 'Field272',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        273 =>
        array(
            'cid' => '273',
            'name' => 'Field273',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        274 =>
        array(
            'cid' => '274',
            'name' => 'Field274',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        275 =>
        array(
            'cid' => '275',
            'name' => 'Field275',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        276 =>
        array(
            'cid' => '276',
            'name' => 'Field276',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        277 =>
        array(
            'cid' => '277',
            'name' => 'Field277',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        278 =>
        array(
            'cid' => '278',
            'name' => 'Field278',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        279 =>
        array(
            'cid' => '279',
            'name' => 'Field279',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        280 =>
        array(
            'cid' => '280',
            'name' => 'Field280',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        281 =>
        array(
            'cid' => '281',
            'name' => 'Field281',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        282 =>
        array(
            'cid' => '282',
            'name' => 'Field282',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        283 =>
        array(
            'cid' => '283',
            'name' => 'Field283',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        284 =>
        array(
            'cid' => '284',
            'name' => 'Field284',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        285 =>
        array(
            'cid' => '285',
            'name' => 'Field285',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        286 =>
        array(
            'cid' => '286',
            'name' => 'Field286',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        287 =>
        array(
            'cid' => '287',
            'name' => 'Field287',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        288 =>
        array(
            'cid' => '288',
            'name' => 'Field288',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        289 =>
        array(
            'cid' => '289',
            'name' => 'Field289',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        290 =>
        array(
            'cid' => '290',
            'name' => 'Field290',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        291 =>
        array(
            'cid' => '291',
            'name' => 'Field291',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        292 =>
        array(
            'cid' => '292',
            'name' => 'Field292',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        293 =>
        array(
            'cid' => '293',
            'name' => 'Field293',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        294 =>
        array(
            'cid' => '294',
            'name' => 'Field294',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        295 =>
        array(
            'cid' => '295',
            'name' => 'Field295',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        296 =>
        array(
            'cid' => '296',
            'name' => 'Field296',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        297 =>
        array(
            'cid' => '297',
            'name' => 'Field297',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        298 =>
        array(
            'cid' => '298',
            'name' => 'Field298',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        299 =>
        array(
            'cid' => '299',
            'name' => 'Field299',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        300 =>
        array(
            'cid' => '300',
            'name' => 'Field300',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        301 =>
        array(
            'cid' => '301',
            'name' => 'Field301',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        302 =>
        array(
            'cid' => '302',
            'name' => 'Field302',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        303 =>
        array(
            'cid' => '303',
            'name' => 'Field303',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        304 =>
        array(
            'cid' => '304',
            'name' => 'Field304',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        305 =>
        array(
            'cid' => '305',
            'name' => 'Field305',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        306 =>
        array(
            'cid' => '306',
            'name' => 'Field306',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        307 =>
        array(
            'cid' => '307',
            'name' => 'Field307',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        308 =>
        array(
            'cid' => '308',
            'name' => 'Field308',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        309 =>
        array(
            'cid' => '309',
            'name' => 'Field309',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        310 =>
        array(
            'cid' => '310',
            'name' => 'Field310',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        311 =>
        array(
            'cid' => '311',
            'name' => 'Field311',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        312 =>
        array(
            'cid' => '312',
            'name' => 'Field312',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        313 =>
        array(
            'cid' => '313',
            'name' => 'Field313',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        314 =>
        array(
            'cid' => '314',
            'name' => 'Field314',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        315 =>
        array(
            'cid' => '315',
            'name' => 'Field315',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        316 =>
        array(
            'cid' => '316',
            'name' => 'Field316',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        317 =>
        array(
            'cid' => '317',
            'name' => 'Field317',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        318 =>
        array(
            'cid' => '318',
            'name' => 'Field318',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        319 =>
        array(
            'cid' => '319',
            'name' => 'Field319',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        320 =>
        array(
            'cid' => '320',
            'name' => 'Field320',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        321 =>
        array(
            'cid' => '321',
            'name' => 'Field321',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        322 =>
        array(
            'cid' => '322',
            'name' => 'Field322',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        323 =>
        array(
            'cid' => '323',
            'name' => 'Field323',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        324 =>
        array(
            'cid' => '324',
            'name' => 'Field324',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        325 =>
        array(
            'cid' => '325',
            'name' => 'Field325',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        326 =>
        array(
            'cid' => '326',
            'name' => 'Field326',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        327 =>
        array(
            'cid' => '327',
            'name' => 'Field327',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        328 =>
        array(
            'cid' => '328',
            'name' => 'Field328',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        329 =>
        array(
            'cid' => '329',
            'name' => 'Field329',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        330 =>
        array(
            'cid' => '330',
            'name' => 'Field330',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        331 =>
        array(
            'cid' => '331',
            'name' => 'Field331',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        332 =>
        array(
            'cid' => '332',
            'name' => 'Field332',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        333 =>
        array(
            'cid' => '333',
            'name' => 'Field333',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        334 =>
        array(
            'cid' => '334',
            'name' => 'Field334',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        335 =>
        array(
            'cid' => '335',
            'name' => 'Field335',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        336 =>
        array(
            'cid' => '336',
            'name' => 'Field336',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        337 =>
        array(
            'cid' => '337',
            'name' => 'Field337',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        338 =>
        array(
            'cid' => '338',
            'name' => 'Field338',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        339 =>
        array(
            'cid' => '339',
            'name' => 'Field339',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        340 =>
        array(
            'cid' => '340',
            'name' => 'Field340',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        341 =>
        array(
            'cid' => '341',
            'name' => 'Field341',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        342 =>
        array(
            'cid' => '342',
            'name' => 'Field342',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        343 =>
        array(
            'cid' => '343',
            'name' => 'Field343',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        344 =>
        array(
            'cid' => '344',
            'name' => 'Field344',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        345 =>
        array(
            'cid' => '345',
            'name' => 'Field345',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        346 =>
        array(
            'cid' => '346',
            'name' => 'Field346',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        347 =>
        array(
            'cid' => '347',
            'name' => 'Field347',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        348 =>
        array(
            'cid' => '348',
            'name' => 'Field348',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        349 =>
        array(
            'cid' => '349',
            'name' => 'Field349',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        350 =>
        array(
            'cid' => '350',
            'name' => 'Field350',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        351 =>
        array(
            'cid' => '351',
            'name' => 'Field352',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        352 =>
        array(
            'cid' => '352',
            'name' => 'Field353',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        353 =>
        array(
            'cid' => '353',
            'name' => 'Field354',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        354 =>
        array(
            'cid' => '354',
            'name' => 'Field355',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        355 =>
        array(
            'cid' => '355',
            'name' => 'Field356',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        356 =>
        array(
            'cid' => '356',
            'name' => 'Field357',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        357 =>
        array(
            'cid' => '357',
            'name' => 'Field358',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        358 =>
        array(
            'cid' => '358',
            'name' => 'Field359',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        359 =>
        array(
            'cid' => '359',
            'name' => 'Field360',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        360 =>
        array(
            'cid' => '360',
            'name' => 'Field361',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        361 =>
        array(
            'cid' => '361',
            'name' => 'Field351',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        362 =>
        array(
            'cid' => '362',
            'name' => 'Field363',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        363 =>
        array(
            'cid' => '363',
            'name' => 'Field364',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        364 =>
        array(
            'cid' => '364',
            'name' => 'Field365',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        365 =>
        array(
            'cid' => '365',
            'name' => 'Field366',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        366 =>
        array(
            'cid' => '366',
            'name' => 'Field367',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        367 =>
        array(
            'cid' => '367',
            'name' => 'Field368',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        368 =>
        array(
            'cid' => '368',
            'name' => 'Field369',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        369 =>
        array(
            'cid' => '369',
            'name' => 'Field370',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        370 =>
        array(
            'cid' => '370',
            'name' => 'Field371',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        371 =>
        array(
            'cid' => '371',
            'name' => 'Field372',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        372 =>
        array(
            'cid' => '372',
            'name' => 'Field373',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        373 =>
        array(
            'cid' => '373',
            'name' => 'Field374',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        374 =>
        array(
            'cid' => '374',
            'name' => 'Field375',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        375 =>
        array(
            'cid' => '375',
            'name' => 'Field376',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        376 =>
        array(
            'cid' => '376',
            'name' => 'Field377',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        377 =>
        array(
            'cid' => '377',
            'name' => 'Field378',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        378 =>
        array(
            'cid' => '378',
            'name' => 'Field379',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        379 =>
        array(
            'cid' => '379',
            'name' => 'Field380',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        380 =>
        array(
            'cid' => '380',
            'name' => 'Field381',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        381 =>
        array(
            'cid' => '381',
            'name' => 'Field382',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        382 =>
        array(
            'cid' => '382',
            'name' => 'Field383',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        383 =>
        array(
            'cid' => '383',
            'name' => 'Field384',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        384 =>
        array(
            'cid' => '384',
            'name' => 'Field385',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        385 =>
        array(
            'cid' => '385',
            'name' => 'Field386',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        386 =>
        array(
            'cid' => '386',
            'name' => 'Field387',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        387 =>
        array(
            'cid' => '387',
            'name' => 'Field388',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        388 =>
        array(
            'cid' => '388',
            'name' => 'Field389',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        389 =>
        array(
            'cid' => '389',
            'name' => 'Field390',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        390 =>
        array(
            'cid' => '390',
            'name' => 'Field391',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        391 =>
        array(
            'cid' => '391',
            'name' => 'Field392',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        392 =>
        array(
            'cid' => '392',
            'name' => 'Field393',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        393 =>
        array(
            'cid' => '393',
            'name' => 'Field394',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        394 =>
        array(
            'cid' => '394',
            'name' => 'Field395',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        395 =>
        array(
            'cid' => '395',
            'name' => 'Field396',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        396 =>
        array(
            'cid' => '396',
            'name' => 'Field397',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        397 =>
        array(
            'cid' => '397',
            'name' => 'Field398',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        398 =>
        array(
            'cid' => '398',
            'name' => 'Field399',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        399 =>
        array(
            'cid' => '399',
            'name' => 'Field400',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        400 =>
        array(
            'cid' => '400',
            'name' => 'Field401',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        401 =>
        array(
            'cid' => '401',
            'name' => 'Field402',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        402 =>
        array(
            'cid' => '402',
            'name' => 'Field403',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        403 =>
        array(
            'cid' => '403',
            'name' => 'Field404',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        404 =>
        array(
            'cid' => '404',
            'name' => 'Field405',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        405 =>
        array(
            'cid' => '405',
            'name' => 'Field406',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        406 =>
        array(
            'cid' => '406',
            'name' => 'Field407',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        407 =>
        array(
            'cid' => '407',
            'name' => 'Field408',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        408 =>
        array(
            'cid' => '408',
            'name' => 'Field409',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        409 =>
        array(
            'cid' => '409',
            'name' => 'Field410',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
    'settaggi_profili3' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'nome_profilo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'commento',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        3 =>
        array(
            'cid' => '3',
            'name' => 'Field3',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        4 =>
        array(
            'cid' => '4',
            'name' => 'Field4',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        5 =>
        array(
            'cid' => '5',
            'name' => 'Field5',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        6 =>
        array(
            'cid' => '6',
            'name' => 'Field6',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        7 =>
        array(
            'cid' => '7',
            'name' => 'Field7',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        8 =>
        array(
            'cid' => '8',
            'name' => 'Field8',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        9 =>
        array(
            'cid' => '9',
            'name' => 'Field9',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        10 =>
        array(
            'cid' => '10',
            'name' => 'Field10',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        11 =>
        array(
            'cid' => '11',
            'name' => 'Field11',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        12 =>
        array(
            'cid' => '12',
            'name' => 'Field12',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        13 =>
        array(
            'cid' => '13',
            'name' => 'Field13',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        14 =>
        array(
            'cid' => '14',
            'name' => 'Field14',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        15 =>
        array(
            'cid' => '15',
            'name' => 'Field15',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        16 =>
        array(
            'cid' => '16',
            'name' => 'Field16',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        17 =>
        array(
            'cid' => '17',
            'name' => 'Field17',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        18 =>
        array(
            'cid' => '18',
            'name' => 'Field18',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        19 =>
        array(
            'cid' => '19',
            'name' => 'Field19',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        20 =>
        array(
            'cid' => '20',
            'name' => 'Field20',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        21 =>
        array(
            'cid' => '21',
            'name' => 'Field21',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        22 =>
        array(
            'cid' => '22',
            'name' => 'Field22',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        23 =>
        array(
            'cid' => '23',
            'name' => 'Field23',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        24 =>
        array(
            'cid' => '24',
            'name' => 'Field24',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        25 =>
        array(
            'cid' => '25',
            'name' => 'Field25',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        26 =>
        array(
            'cid' => '26',
            'name' => 'Field26',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        27 =>
        array(
            'cid' => '27',
            'name' => 'Field27',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        28 =>
        array(
            'cid' => '28',
            'name' => 'Field28',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        29 =>
        array(
            'cid' => '29',
            'name' => 'Field29',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        30 =>
        array(
            'cid' => '30',
            'name' => 'Field30',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        31 =>
        array(
            'cid' => '31',
            'name' => 'Field31',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        32 =>
        array(
            'cid' => '32',
            'name' => 'Field32',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        33 =>
        array(
            'cid' => '33',
            'name' => 'Field33',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        34 =>
        array(
            'cid' => '34',
            'name' => 'Field34',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        35 =>
        array(
            'cid' => '35',
            'name' => 'Field35',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        36 =>
        array(
            'cid' => '36',
            'name' => 'Field36',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        37 =>
        array(
            'cid' => '37',
            'name' => 'Field37',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        38 =>
        array(
            'cid' => '38',
            'name' => 'Field38',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        39 =>
        array(
            'cid' => '39',
            'name' => 'Field39',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        40 =>
        array(
            'cid' => '40',
            'name' => 'Field40',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        41 =>
        array(
            'cid' => '41',
            'name' => 'Field41',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        42 =>
        array(
            'cid' => '42',
            'name' => 'Field42',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        43 =>
        array(
            'cid' => '43',
            'name' => 'Field43',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        44 =>
        array(
            'cid' => '44',
            'name' => 'Field44',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        45 =>
        array(
            'cid' => '45',
            'name' => 'Field45',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        46 =>
        array(
            'cid' => '46',
            'name' => 'Field46',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        47 =>
        array(
            'cid' => '47',
            'name' => 'Field47',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        48 =>
        array(
            'cid' => '48',
            'name' => 'Field48',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        49 =>
        array(
            'cid' => '49',
            'name' => 'Field49',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        50 =>
        array(
            'cid' => '50',
            'name' => 'Field50',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        51 =>
        array(
            'cid' => '51',
            'name' => 'Field51',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        52 =>
        array(
            'cid' => '52',
            'name' => 'Field52',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        53 =>
        array(
            'cid' => '53',
            'name' => 'Field53',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        54 =>
        array(
            'cid' => '54',
            'name' => 'Field54',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        55 =>
        array(
            'cid' => '55',
            'name' => 'Field55',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        56 =>
        array(
            'cid' => '56',
            'name' => 'Field56',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        57 =>
        array(
            'cid' => '57',
            'name' => 'Field57',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        58 =>
        array(
            'cid' => '58',
            'name' => 'Field58',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        59 =>
        array(
            'cid' => '59',
            'name' => 'Field59',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        60 =>
        array(
            'cid' => '60',
            'name' => 'Field60',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        61 =>
        array(
            'cid' => '61',
            'name' => 'Field61',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        62 =>
        array(
            'cid' => '62',
            'name' => 'Field62',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        63 =>
        array(
            'cid' => '63',
            'name' => 'Field63',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        64 =>
        array(
            'cid' => '64',
            'name' => 'Field64',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        65 =>
        array(
            'cid' => '65',
            'name' => 'Field65',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        66 =>
        array(
            'cid' => '66',
            'name' => 'Field66',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        67 =>
        array(
            'cid' => '67',
            'name' => 'Field67',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        68 =>
        array(
            'cid' => '68',
            'name' => 'Field68',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        69 =>
        array(
            'cid' => '69',
            'name' => 'Field69',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        70 =>
        array(
            'cid' => '70',
            'name' => 'Field70',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        71 =>
        array(
            'cid' => '71',
            'name' => 'Field71',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        72 =>
        array(
            'cid' => '72',
            'name' => 'Field72',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        73 =>
        array(
            'cid' => '73',
            'name' => 'Field73',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        74 =>
        array(
            'cid' => '74',
            'name' => 'Field74',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        75 =>
        array(
            'cid' => '75',
            'name' => 'Field75',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        76 =>
        array(
            'cid' => '76',
            'name' => 'Field76',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        77 =>
        array(
            'cid' => '77',
            'name' => 'Field77',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        78 =>
        array(
            'cid' => '78',
            'name' => 'Field78',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        79 =>
        array(
            'cid' => '79',
            'name' => 'Field79',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        80 =>
        array(
            'cid' => '80',
            'name' => 'Field80',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        81 =>
        array(
            'cid' => '81',
            'name' => 'Field81',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        82 =>
        array(
            'cid' => '82',
            'name' => 'Field82',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        83 =>
        array(
            'cid' => '83',
            'name' => 'Field83',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        84 =>
        array(
            'cid' => '84',
            'name' => 'Field84',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        85 =>
        array(
            'cid' => '85',
            'name' => 'Field85',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        86 =>
        array(
            'cid' => '86',
            'name' => 'Field86',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        87 =>
        array(
            'cid' => '87',
            'name' => 'Field87',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        88 =>
        array(
            'cid' => '88',
            'name' => 'Field88',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        89 =>
        array(
            'cid' => '89',
            'name' => 'Field89',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        90 =>
        array(
            'cid' => '90',
            'name' => 'Field90',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        91 =>
        array(
            'cid' => '91',
            'name' => 'Field91',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        92 =>
        array(
            'cid' => '92',
            'name' => 'Field92',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        93 =>
        array(
            'cid' => '93',
            'name' => 'Field93',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        94 =>
        array(
            'cid' => '94',
            'name' => 'Field94',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        95 =>
        array(
            'cid' => '95',
            'name' => 'Field95',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        96 =>
        array(
            'cid' => '96',
            'name' => 'Field96',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        97 =>
        array(
            'cid' => '97',
            'name' => 'Field97',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        98 =>
        array(
            'cid' => '98',
            'name' => 'Field98',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        99 =>
        array(
            'cid' => '99',
            'name' => 'Field99',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        100 =>
        array(
            'cid' => '100',
            'name' => 'Field100',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        101 =>
        array(
            'cid' => '101',
            'name' => 'Field101',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        102 =>
        array(
            'cid' => '102',
            'name' => 'Field102',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        103 =>
        array(
            'cid' => '103',
            'name' => 'Field103',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        104 =>
        array(
            'cid' => '104',
            'name' => 'Field104',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        105 =>
        array(
            'cid' => '105',
            'name' => 'Field105',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        106 =>
        array(
            'cid' => '106',
            'name' => 'Field106',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        107 =>
        array(
            'cid' => '107',
            'name' => 'Field107',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        108 =>
        array(
            'cid' => '108',
            'name' => 'Field108',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        109 =>
        array(
            'cid' => '109',
            'name' => 'Field109',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        110 =>
        array(
            'cid' => '110',
            'name' => 'Field110',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        111 =>
        array(
            'cid' => '111',
            'name' => 'Field111',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        112 =>
        array(
            'cid' => '112',
            'name' => 'Field112',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        113 =>
        array(
            'cid' => '113',
            'name' => 'Field113',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        114 =>
        array(
            'cid' => '114',
            'name' => 'Field114',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        115 =>
        array(
            'cid' => '115',
            'name' => 'Field115',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        116 =>
        array(
            'cid' => '116',
            'name' => 'Field116',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        117 =>
        array(
            'cid' => '117',
            'name' => 'Field117',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        118 =>
        array(
            'cid' => '118',
            'name' => 'Field118',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        119 =>
        array(
            'cid' => '119',
            'name' => 'Field119',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        120 =>
        array(
            'cid' => '120',
            'name' => 'Field120',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        121 =>
        array(
            'cid' => '121',
            'name' => 'Field121',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        122 =>
        array(
            'cid' => '122',
            'name' => 'Field122',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        123 =>
        array(
            'cid' => '123',
            'name' => 'Field123',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        124 =>
        array(
            'cid' => '124',
            'name' => 'Field124',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        125 =>
        array(
            'cid' => '125',
            'name' => 'Field125',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        126 =>
        array(
            'cid' => '126',
            'name' => 'Field126',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        127 =>
        array(
            'cid' => '127',
            'name' => 'Field127',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        128 =>
        array(
            'cid' => '128',
            'name' => 'Field128',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        129 =>
        array(
            'cid' => '129',
            'name' => 'Field129',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        130 =>
        array(
            'cid' => '130',
            'name' => 'Field130',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        131 =>
        array(
            'cid' => '131',
            'name' => 'Field131',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        132 =>
        array(
            'cid' => '132',
            'name' => 'Field132',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        133 =>
        array(
            'cid' => '133',
            'name' => 'Field133',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        134 =>
        array(
            'cid' => '134',
            'name' => 'Field134',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        135 =>
        array(
            'cid' => '135',
            'name' => 'Field135',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        136 =>
        array(
            'cid' => '136',
            'name' => 'Field136',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        137 =>
        array(
            'cid' => '137',
            'name' => 'Field137',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        138 =>
        array(
            'cid' => '138',
            'name' => 'Field138',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        139 =>
        array(
            'cid' => '139',
            'name' => 'Field139',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        140 =>
        array(
            'cid' => '140',
            'name' => 'Field140',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        141 =>
        array(
            'cid' => '141',
            'name' => 'Field141',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        142 =>
        array(
            'cid' => '142',
            'name' => 'Field142',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        143 =>
        array(
            'cid' => '143',
            'name' => 'Field143',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        144 =>
        array(
            'cid' => '144',
            'name' => 'Field144',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        145 =>
        array(
            'cid' => '145',
            'name' => 'Field145',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        146 =>
        array(
            'cid' => '146',
            'name' => 'Field146',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        147 =>
        array(
            'cid' => '147',
            'name' => 'Field147',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        148 =>
        array(
            'cid' => '148',
            'name' => 'Field148',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        149 =>
        array(
            'cid' => '149',
            'name' => 'Field149',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        150 =>
        array(
            'cid' => '150',
            'name' => 'Field150',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        151 =>
        array(
            'cid' => '151',
            'name' => 'Field151',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        152 =>
        array(
            'cid' => '152',
            'name' => 'Field152',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        153 =>
        array(
            'cid' => '153',
            'name' => 'Field153',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        154 =>
        array(
            'cid' => '154',
            'name' => 'Field154',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        155 =>
        array(
            'cid' => '155',
            'name' => 'Field155',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        156 =>
        array(
            'cid' => '156',
            'name' => 'Field156',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        157 =>
        array(
            'cid' => '157',
            'name' => 'Field157',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        158 =>
        array(
            'cid' => '158',
            'name' => 'Field158',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        159 =>
        array(
            'cid' => '159',
            'name' => 'Field159',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        160 =>
        array(
            'cid' => '160',
            'name' => 'Field160',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        161 =>
        array(
            'cid' => '161',
            'name' => 'Field161',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        162 =>
        array(
            'cid' => '162',
            'name' => 'Field162',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        163 =>
        array(
            'cid' => '163',
            'name' => 'Field163',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        164 =>
        array(
            'cid' => '164',
            'name' => 'Field164',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        165 =>
        array(
            'cid' => '165',
            'name' => 'Field165',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        166 =>
        array(
            'cid' => '166',
            'name' => 'Field166',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        167 =>
        array(
            'cid' => '167',
            'name' => 'Field167',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        168 =>
        array(
            'cid' => '168',
            'name' => 'Field168',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        169 =>
        array(
            'cid' => '169',
            'name' => 'Field169',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        170 =>
        array(
            'cid' => '170',
            'name' => 'Field170',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        171 =>
        array(
            'cid' => '171',
            'name' => 'Field171',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        172 =>
        array(
            'cid' => '172',
            'name' => 'Field172',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        173 =>
        array(
            'cid' => '173',
            'name' => 'Field173',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        174 =>
        array(
            'cid' => '174',
            'name' => 'Field174',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        175 =>
        array(
            'cid' => '175',
            'name' => 'Field175',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        176 =>
        array(
            'cid' => '176',
            'name' => 'Field176',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        177 =>
        array(
            'cid' => '177',
            'name' => 'Field177',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        178 =>
        array(
            'cid' => '178',
            'name' => 'Field178',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        179 =>
        array(
            'cid' => '179',
            'name' => 'Field179',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        180 =>
        array(
            'cid' => '180',
            'name' => 'Field180',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        181 =>
        array(
            'cid' => '181',
            'name' => 'Field181',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        182 =>
        array(
            'cid' => '182',
            'name' => 'Field182',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        183 =>
        array(
            'cid' => '183',
            'name' => 'Field183',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        184 =>
        array(
            'cid' => '184',
            'name' => 'Field184',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        185 =>
        array(
            'cid' => '185',
            'name' => 'Field185',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        186 =>
        array(
            'cid' => '186',
            'name' => 'Field186',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        187 =>
        array(
            'cid' => '187',
            'name' => 'Field187',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        188 =>
        array(
            'cid' => '188',
            'name' => 'Field188',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        189 =>
        array(
            'cid' => '189',
            'name' => 'Field189',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        190 =>
        array(
            'cid' => '190',
            'name' => 'Field190',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        191 =>
        array(
            'cid' => '191',
            'name' => 'Field191',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        192 =>
        array(
            'cid' => '192',
            'name' => 'Field192',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        193 =>
        array(
            'cid' => '193',
            'name' => 'Field193',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        194 =>
        array(
            'cid' => '194',
            'name' => 'Field194',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        195 =>
        array(
            'cid' => '195',
            'name' => 'Field195',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        196 =>
        array(
            'cid' => '196',
            'name' => 'Field196',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        197 =>
        array(
            'cid' => '197',
            'name' => 'Field197',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        198 =>
        array(
            'cid' => '198',
            'name' => 'Field198',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        199 =>
        array(
            'cid' => '199',
            'name' => 'Field199',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        200 =>
        array(
            'cid' => '200',
            'name' => 'Field200',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        201 =>
        array(
            'cid' => '201',
            'name' => 'Field201',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        202 =>
        array(
            'cid' => '202',
            'name' => 'Field202',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        203 =>
        array(
            'cid' => '203',
            'name' => 'Field203',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        204 =>
        array(
            'cid' => '204',
            'name' => 'Field204',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        205 =>
        array(
            'cid' => '205',
            'name' => 'Field205',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        206 =>
        array(
            'cid' => '206',
            'name' => 'Field206',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        207 =>
        array(
            'cid' => '207',
            'name' => 'Field207',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        208 =>
        array(
            'cid' => '208',
            'name' => 'Field208',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        209 =>
        array(
            'cid' => '209',
            'name' => 'Field209',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        210 =>
        array(
            'cid' => '210',
            'name' => 'Field210',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        211 =>
        array(
            'cid' => '211',
            'name' => 'Field211',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        212 =>
        array(
            'cid' => '212',
            'name' => 'Field212',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        213 =>
        array(
            'cid' => '213',
            'name' => 'Field213',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        214 =>
        array(
            'cid' => '214',
            'name' => 'Field214',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        215 =>
        array(
            'cid' => '215',
            'name' => 'Field215',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        216 =>
        array(
            'cid' => '216',
            'name' => 'Field216',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        217 =>
        array(
            'cid' => '217',
            'name' => 'Field217',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        218 =>
        array(
            'cid' => '218',
            'name' => 'Field218',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        219 =>
        array(
            'cid' => '219',
            'name' => 'Field219',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        220 =>
        array(
            'cid' => '220',
            'name' => 'Field220',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        221 =>
        array(
            'cid' => '221',
            'name' => 'Field221',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        222 =>
        array(
            'cid' => '222',
            'name' => 'Field222',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        223 =>
        array(
            'cid' => '223',
            'name' => 'Field223',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        224 =>
        array(
            'cid' => '224',
            'name' => 'Field224',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        225 =>
        array(
            'cid' => '225',
            'name' => 'Field225',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        226 =>
        array(
            'cid' => '226',
            'name' => 'Field226',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        227 =>
        array(
            'cid' => '227',
            'name' => 'Field227',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        228 =>
        array(
            'cid' => '228',
            'name' => 'Field228',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        229 =>
        array(
            'cid' => '229',
            'name' => 'Field229',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        230 =>
        array(
            'cid' => '230',
            'name' => 'Field230',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        231 =>
        array(
            'cid' => '231',
            'name' => 'Field231',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        232 =>
        array(
            'cid' => '232',
            'name' => 'Field232',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        233 =>
        array(
            'cid' => '233',
            'name' => 'Field233',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        234 =>
        array(
            'cid' => '234',
            'name' => 'Field234',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        235 =>
        array(
            'cid' => '235',
            'name' => 'Field235',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        236 =>
        array(
            'cid' => '236',
            'name' => 'Field236',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        237 =>
        array(
            'cid' => '237',
            'name' => 'Field237',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        238 =>
        array(
            'cid' => '238',
            'name' => 'Field238',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        239 =>
        array(
            'cid' => '239',
            'name' => 'Field239',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        240 =>
        array(
            'cid' => '240',
            'name' => 'Field240',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        241 =>
        array(
            'cid' => '241',
            'name' => 'Field241',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        242 =>
        array(
            'cid' => '242',
            'name' => 'Field242',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        243 =>
        array(
            'cid' => '243',
            'name' => 'Field243',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        244 =>
        array(
            'cid' => '244',
            'name' => 'Field244',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        245 =>
        array(
            'cid' => '245',
            'name' => 'Field245',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        246 =>
        array(
            'cid' => '246',
            'name' => 'Field246',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        247 =>
        array(
            'cid' => '247',
            'name' => 'Field247',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        248 =>
        array(
            'cid' => '248',
            'name' => 'Field248',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        249 =>
        array(
            'cid' => '249',
            'name' => 'Field249',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        250 =>
        array(
            'cid' => '250',
            'name' => 'Field250',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        251 =>
        array(
            'cid' => '251',
            'name' => 'Field251',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        252 =>
        array(
            'cid' => '252',
            'name' => 'Field252',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        253 =>
        array(
            'cid' => '253',
            'name' => 'Field253',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        254 =>
        array(
            'cid' => '254',
            'name' => 'Field254',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        255 =>
        array(
            'cid' => '255',
            'name' => 'Field255',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        256 =>
        array(
            'cid' => '256',
            'name' => 'Field256',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        257 =>
        array(
            'cid' => '257',
            'name' => 'Field257',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        258 =>
        array(
            'cid' => '258',
            'name' => 'Field258',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        259 =>
        array(
            'cid' => '259',
            'name' => 'Field259',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        260 =>
        array(
            'cid' => '260',
            'name' => 'Field260',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        261 =>
        array(
            'cid' => '261',
            'name' => 'Field261',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        262 =>
        array(
            'cid' => '262',
            'name' => 'Field262',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        263 =>
        array(
            'cid' => '263',
            'name' => 'Field263',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        264 =>
        array(
            'cid' => '264',
            'name' => 'Field264',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        265 =>
        array(
            'cid' => '265',
            'name' => 'Field265',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        266 =>
        array(
            'cid' => '266',
            'name' => 'Field266',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        267 =>
        array(
            'cid' => '267',
            'name' => 'Field267',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        268 =>
        array(
            'cid' => '268',
            'name' => 'Field268',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        269 =>
        array(
            'cid' => '269',
            'name' => 'Field269',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        270 =>
        array(
            'cid' => '270',
            'name' => 'Field270',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        271 =>
        array(
            'cid' => '271',
            'name' => 'Field271',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        272 =>
        array(
            'cid' => '272',
            'name' => 'Field272',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        273 =>
        array(
            'cid' => '273',
            'name' => 'Field273',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        274 =>
        array(
            'cid' => '274',
            'name' => 'Field274',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        275 =>
        array(
            'cid' => '275',
            'name' => 'Field275',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        276 =>
        array(
            'cid' => '276',
            'name' => 'Field276',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        277 =>
        array(
            'cid' => '277',
            'name' => 'Field277',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        278 =>
        array(
            'cid' => '278',
            'name' => 'Field278',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        279 =>
        array(
            'cid' => '279',
            'name' => 'Field279',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        280 =>
        array(
            'cid' => '280',
            'name' => 'Field280',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        281 =>
        array(
            'cid' => '281',
            'name' => 'Field281',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        282 =>
        array(
            'cid' => '282',
            'name' => 'Field282',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        283 =>
        array(
            'cid' => '283',
            'name' => 'Field283',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        284 =>
        array(
            'cid' => '284',
            'name' => 'Field284',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        285 =>
        array(
            'cid' => '285',
            'name' => 'Field285',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        286 =>
        array(
            'cid' => '286',
            'name' => 'Field286',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        287 =>
        array(
            'cid' => '287',
            'name' => 'Field287',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        288 =>
        array(
            'cid' => '288',
            'name' => 'Field288',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        289 =>
        array(
            'cid' => '289',
            'name' => 'Field289',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        290 =>
        array(
            'cid' => '290',
            'name' => 'Field290',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        291 =>
        array(
            'cid' => '291',
            'name' => 'Field291',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        292 =>
        array(
            'cid' => '292',
            'name' => 'Field292',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        293 =>
        array(
            'cid' => '293',
            'name' => 'Field293',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        294 =>
        array(
            'cid' => '294',
            'name' => 'Field294',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        295 =>
        array(
            'cid' => '295',
            'name' => 'Field295',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        296 =>
        array(
            'cid' => '296',
            'name' => 'Field296',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        297 =>
        array(
            'cid' => '297',
            'name' => 'Field297',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        298 =>
        array(
            'cid' => '298',
            'name' => 'Field298',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        299 =>
        array(
            'cid' => '299',
            'name' => 'Field299',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        300 =>
        array(
            'cid' => '300',
            'name' => 'Field300',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        301 =>
        array(
            'cid' => '301',
            'name' => 'Field301',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        302 =>
        array(
            'cid' => '302',
            'name' => 'Field302',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        303 =>
        array(
            'cid' => '303',
            'name' => 'Field303',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        304 =>
        array(
            'cid' => '304',
            'name' => 'Field304',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        305 =>
        array(
            'cid' => '305',
            'name' => 'Field305',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        306 =>
        array(
            'cid' => '306',
            'name' => 'Field306',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        307 =>
        array(
            'cid' => '307',
            'name' => 'Field307',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        308 =>
        array(
            'cid' => '308',
            'name' => 'Field308',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        309 =>
        array(
            'cid' => '309',
            'name' => 'Field309',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        310 =>
        array(
            'cid' => '310',
            'name' => 'Field310',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        311 =>
        array(
            'cid' => '311',
            'name' => 'Field311',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        312 =>
        array(
            'cid' => '312',
            'name' => 'Field312',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        313 =>
        array(
            'cid' => '313',
            'name' => 'Field313',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        314 =>
        array(
            'cid' => '314',
            'name' => 'Field314',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        315 =>
        array(
            'cid' => '315',
            'name' => 'Field315',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        316 =>
        array(
            'cid' => '316',
            'name' => 'Field316',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        317 =>
        array(
            'cid' => '317',
            'name' => 'Field317',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        318 =>
        array(
            'cid' => '318',
            'name' => 'Field318',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        319 =>
        array(
            'cid' => '319',
            'name' => 'Field319',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        320 =>
        array(
            'cid' => '320',
            'name' => 'Field320',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        321 =>
        array(
            'cid' => '321',
            'name' => 'Field321',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        322 =>
        array(
            'cid' => '322',
            'name' => 'Field322',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        323 =>
        array(
            'cid' => '323',
            'name' => 'Field323',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        324 =>
        array(
            'cid' => '324',
            'name' => 'Field324',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        325 =>
        array(
            'cid' => '325',
            'name' => 'Field325',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        326 =>
        array(
            'cid' => '326',
            'name' => 'Field326',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        327 =>
        array(
            'cid' => '327',
            'name' => 'Field327',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        328 =>
        array(
            'cid' => '328',
            'name' => 'Field328',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        329 =>
        array(
            'cid' => '329',
            'name' => 'Field329',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        330 =>
        array(
            'cid' => '330',
            'name' => 'Field330',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        331 =>
        array(
            'cid' => '331',
            'name' => 'Field331',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        332 =>
        array(
            'cid' => '332',
            'name' => 'Field332',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        333 =>
        array(
            'cid' => '333',
            'name' => 'Field333',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        334 =>
        array(
            'cid' => '334',
            'name' => 'Field334',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        335 =>
        array(
            'cid' => '335',
            'name' => 'Field335',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        336 =>
        array(
            'cid' => '336',
            'name' => 'Field336',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        337 =>
        array(
            'cid' => '337',
            'name' => 'Field337',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        338 =>
        array(
            'cid' => '338',
            'name' => 'Field338',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        339 =>
        array(
            'cid' => '339',
            'name' => 'Field339',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        340 =>
        array(
            'cid' => '340',
            'name' => 'Field340',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        341 =>
        array(
            'cid' => '341',
            'name' => 'Field341',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        342 =>
        array(
            'cid' => '342',
            'name' => 'Field342',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        343 =>
        array(
            'cid' => '343',
            'name' => 'Field343',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        344 =>
        array(
            'cid' => '344',
            'name' => 'Field344',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        345 =>
        array(
            'cid' => '345',
            'name' => 'Field345',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        346 =>
        array(
            'cid' => '346',
            'name' => 'Field346',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        347 =>
        array(
            'cid' => '347',
            'name' => 'Field347',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        348 =>
        array(
            'cid' => '348',
            'name' => 'Field348',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        349 =>
        array(
            'cid' => '349',
            'name' => 'Field349',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        350 =>
        array(
            'cid' => '350',
            'name' => 'Field350',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        351 =>
        array(
            'cid' => '351',
            'name' => 'Field352',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        352 =>
        array(
            'cid' => '352',
            'name' => 'Field353',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        353 =>
        array(
            'cid' => '353',
            'name' => 'Field354',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        354 =>
        array(
            'cid' => '354',
            'name' => 'Field355',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        355 =>
        array(
            'cid' => '355',
            'name' => 'Field356',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        356 =>
        array(
            'cid' => '356',
            'name' => 'Field357',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        357 =>
        array(
            'cid' => '357',
            'name' => 'Field358',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        358 =>
        array(
            'cid' => '358',
            'name' => 'Field359',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        359 =>
        array(
            'cid' => '359',
            'name' => 'Field360',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        360 =>
        array(
            'cid' => '360',
            'name' => 'Field361',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        361 =>
        array(
            'cid' => '361',
            'name' => 'Field351',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        362 =>
        array(
            'cid' => '362',
            'name' => 'Field363',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        363 =>
        array(
            'cid' => '363',
            'name' => 'Field364',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        364 =>
        array(
            'cid' => '364',
            'name' => 'Field365',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        365 =>
        array(
            'cid' => '365',
            'name' => 'Field366',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        366 =>
        array(
            'cid' => '366',
            'name' => 'Field367',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        367 =>
        array(
            'cid' => '367',
            'name' => 'Field368',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        368 =>
        array(
            'cid' => '368',
            'name' => 'Field369',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        369 =>
        array(
            'cid' => '369',
            'name' => 'Field370',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        370 =>
        array(
            'cid' => '370',
            'name' => 'Field371',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        371 =>
        array(
            'cid' => '371',
            'name' => 'Field372',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        372 =>
        array(
            'cid' => '372',
            'name' => 'Field373',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        373 =>
        array(
            'cid' => '373',
            'name' => 'Field374',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        374 =>
        array(
            'cid' => '374',
            'name' => 'Field375',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        375 =>
        array(
            'cid' => '375',
            'name' => 'Field376',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        376 =>
        array(
            'cid' => '376',
            'name' => 'Field377',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        377 =>
        array(
            'cid' => '377',
            'name' => 'Field378',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        378 =>
        array(
            'cid' => '378',
            'name' => 'Field379',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        379 =>
        array(
            'cid' => '379',
            'name' => 'Field380',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        380 =>
        array(
            'cid' => '380',
            'name' => 'Field381',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        381 =>
        array(
            'cid' => '381',
            'name' => 'Field382',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        382 =>
        array(
            'cid' => '382',
            'name' => 'Field383',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        383 =>
        array(
            'cid' => '383',
            'name' => 'Field384',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        384 =>
        array(
            'cid' => '384',
            'name' => 'Field385',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        385 =>
        array(
            'cid' => '385',
            'name' => 'Field386',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        386 =>
        array(
            'cid' => '386',
            'name' => 'Field387',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        387 =>
        array(
            'cid' => '387',
            'name' => 'Field388',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        388 =>
        array(
            'cid' => '388',
            'name' => 'Field389',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        389 =>
        array(
            'cid' => '389',
            'name' => 'Field390',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        390 =>
        array(
            'cid' => '390',
            'name' => 'Field391',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        391 =>
        array(
            'cid' => '391',
            'name' => 'Field392',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        392 =>
        array(
            'cid' => '392',
            'name' => 'Field393',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        393 =>
        array(
            'cid' => '393',
            'name' => 'Field394',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        394 =>
        array(
            'cid' => '394',
            'name' => 'Field395',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        395 =>
        array(
            'cid' => '395',
            'name' => 'Field396',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        396 =>
        array(
            'cid' => '396',
            'name' => 'Field397',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        397 =>
        array(
            'cid' => '397',
            'name' => 'Field398',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        398 =>
        array(
            'cid' => '398',
            'name' => 'Field399',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        399 =>
        array(
            'cid' => '399',
            'name' => 'Field400',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        400 =>
        array(
            'cid' => '400',
            'name' => 'Field401',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        401 =>
        array(
            'cid' => '401',
            'name' => 'Field402',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        402 =>
        array(
            'cid' => '402',
            'name' => 'Field403',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        403 =>
        array(
            'cid' => '403',
            'name' => 'Field404',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        404 =>
        array(
            'cid' => '404',
            'name' => 'Field405',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        405 =>
        array(
            'cid' => '405',
            'name' => 'Field406',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        406 =>
        array(
            'cid' => '406',
            'name' => 'Field407',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        407 =>
        array(
            'cid' => '407',
            'name' => 'Field408',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        408 =>
        array(
            'cid' => '408',
            'name' => 'Field409',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        409 =>
        array(
            'cid' => '409',
            'name' => 'Field410',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
    'gruppi_statistici' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'nome',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
    'terminali' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'ip',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'nome',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
    'centri_statistica' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'nome_centro',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'nome_db',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
    'tavoli_uniti' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'fila_tavoli',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
    'categorie' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'ordinamento',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'codice',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        3 =>
        array(
            'cid' => '3',
            'name' => 'descrizione',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        4 =>
        array(
            'cid' => '4',
            'name' => 'alias',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        5 =>
        array(
            'cid' => '5',
            'name' => 'portata',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        6 =>
        array(
            'cid' => '6',
            'name' => 'escl_server',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        7 =>
        array(
            'cid' => '7',
            'name' => 'escl_pda',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        8 =>
        array(
            'cid' => '8',
            'name' => 'escl_tablet',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        9 =>
        array(
            'cid' => '9',
            'name' => 'escl_iphone',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        10 =>
        array(
            'cid' => '10',
            'name' => 'var_aperte',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        11 =>
        array(
            'cid' => '11',
            'name' => 'perc_iva',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        12 =>
        array(
            'cid' => '12',
            'name' => 'cat_var',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        13 =>
        array(
            'cid' => '13',
            'name' => 'dest_stampa',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        14 =>
        array(
            'cid' => '14',
            'name' => 'gruppo',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        15 =>
        array(
            'cid' => '15',
            'name' => 'img_tasto',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        16 =>
        array(
            'cid' => '16',
            'name' => 'categoria_partenza',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        17 =>
        array(
            'cid' => '17',
            'name' => 'abilita_asporto',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        18 =>
        array(
            'cid' => '18',
            'name' => 'numero_preferiti',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        19 =>
        array(
            'cid' => '19',
            'name' => 'disabilita_visualizzazione',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        20 =>
        array(
            'cid' => '20',
            'name' => 'visu_tavoli',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        21 =>
        array(
            'cid' => '21',
            'name' => 'visu_bar',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        22 =>
        array(
            'cid' => '22',
            'name' => 'visu_asporto',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        23 =>
        array(
            'cid' => '23',
            'name' => 'visu_continuo',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        24 =>
        array(
            'cid' => '24',
            'name' => 'dest_stampa_2',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        25 =>
        array(
            'cid' => '25',
            'name' => 'disabilita_pda',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        26 =>
        array(
            'cid' => '26',
            'name' => 'disabilita_server',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        27 =>
        array(
            'cid' => '27',
            'name' => 'consegna_abilitata',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        28 =>
        array(
            'cid' => '28',
            'name' => 'perc_iva_takeaway',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        29 =>
        array(
            'cid' => '29',
            'name' => 'reparto_servizi',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        30 =>
        array(
            'cid' => '30',
            'name' => 'reparto_beni',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
    'nomi_portate' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'numero',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'italiano',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        3 =>
        array(
            'cid' => '3',
            'name' => 'english',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        4 =>
        array(
            'cid' => '4',
            'name' => 'deutsch',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        5 =>
        array(
            'cid' => '5',
            'name' => 'ord_fastorder',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        6 =>
        array(
            'cid' => '6',
            'name' => 'consegna_abilitata',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        7 =>
        array(
            'cid' => '7',
            'name' => 'ordinamento_stampa',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => '""',
            'pk' => '0',
        ),
        8 =>
        array(
            'cid' => '8',
            'name' => 'visu_articoli_concomitanze',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => '"true"',
            'pk' => '0',
        ),
        9 =>
        array(
            'cid' => '9',
            'name' => 'dimensione_font_concomitanze',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => '"D"',
            'pk' => '0',
        ),
        10 =>
        array(
            'cid' => '10',
            'name' => 'attributi_font_concomitanze',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => '"N"',
            'pk' => '0',
        ),
        11 =>
        array(
            'cid' => '11',
            'name' => 'concomitanze',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => '"N"',
            'pk' => '0',
        )
    ),
    'nomi_stampanti' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'numero',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'italiano',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        3 =>
        array(
            'cid' => '3',
            'name' => 'english',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        4 =>
        array(
            'cid' => '4',
            'name' => 'deutsch',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        5 =>
        array(
            'cid' => '5',
            'name' => 'ip',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        6 =>
        array(
            'cid' => '6',
            'name' => 'dimensione',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        7 =>
        array(
            'cid' => '7',
            'name' => 'nome',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        8 =>
        array(
            'cid' => '8',
            'name' => 'intelligent',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        9 =>
        array(
            'cid' => '9',
            'name' => 'fiscale',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        10 =>
        array(
            'cid' => '10',
            'name' => 'devid_nf',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        11 =>
        array(
            'cid' => '11',
            'name' => 'devicetype',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        12 =>
        array(
            'cid' => '12',
            'name' => 'devicemodel',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        13 =>
        array(
            'cid' => '13',
            'name' => 'ip_alternativo',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        14 =>
        array(
            'cid' => '14',
            'name' => 'matricola',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        15 =>
        array(
            'cid' => '15',
            'name' => 'doppia',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        16 =>
        array(
            'cid' => '16',
            'name' => 'sds_controllo',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
    'tavoli' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'classe',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'tavolotxt',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        3 =>
        array(
            'cid' => '3',
            'name' => 'numero',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        4 =>
        array(
            'cid' => '4',
            'name' => 'abilitato',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        5 =>
        array(
            'cid' => '5',
            'name' => 'altezza',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        6 =>
        array(
            'cid' => '6',
            'name' => 'larghezza',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        7 =>
        array(
            'cid' => '7',
            'name' => 'pos_x',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        8 =>
        array(
            'cid' => '8',
            'name' => 'pos_y',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        9 =>
        array(
            'cid' => '9',
            'name' => 'colore',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        10 =>
        array(
            'cid' => '10',
            'name' => 'posti',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        11 =>
        array(
            'cid' => '11',
            'name' => 'ora_apertura_tavolo',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        12 =>
        array(
            'cid' => '12',
            'name' => 'ora_ultima_comanda',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        13 =>
        array(
            'cid' => '13',
            'name' => 'occupato',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        14 =>
        array(
            'cid' => '14',
            'name' => 'comanda_attiva',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        15 =>
        array(
            'cid' => '15',
            'name' => 'nome_sala',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        16 =>
        array(
            'cid' => '16',
            'name' => 'pos_x_mobile',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        17 =>
        array(
            'cid' => '17',
            'name' => 'pos_y_mobile',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        18 =>
        array(
            'cid' => '18',
            'name' => 'non_mostrare_prima_cifra',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        19 =>
        array(
            'cid' => '19',
            'name' => 'disabilita_cellulare',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        20 =>
        array(
            'cid' => '20',
            'name' => 'estensione',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        21 =>
        array(
            'cid' => '21',
            'name' => 'disabilita_cellulari',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        22 =>
        array(
            'cid' => '22',
            'name' => 'n_st_conto',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        23 =>
        array(
            'cid' => '23',
            'name' => 'id_cliente_salvato',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        24 =>
        array(
            'cid' => '24',
            'name' => 'nome_sala_smartphone',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        25 =>
        array(
            'cid' => '25',
            'name' => 'font_mobile',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        26 =>
        array(
            'cid' => '26',
            'name' => 'collegamento',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
    'comanda' =>
    
    array(
        /* primary key */
        0 =>
        array(
            'cid' => '0',
            'name' => 'prim_key',
            'type' => 'INTEGER',
            'notnull' => '0',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        /* order id - id comanda */
        1 =>
        array(
            'cid' => '1',
            'name' => 'id',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        2 =>
        /* delivery time - ora di consegna */
        array(
            'cid' => '2',
            'name' => 'ora_consegna',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        3 =>
        /* delivery type - tipo di consegna (DOMICILIO, PIZZERIA, RITIRO, 
        NIENTE - cioè devi ancora impostarlo-, vuoto- cioè devi ancora impostarlo-) */
        array(
            'cid' => '3',
            'name' => 'tipo_consegna',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        4 =>
        /* delivery progressive number - progressivo di asporto di RITIRO */
        array(
            'cid' => '4',
            'name' => 'numeretto_asp',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        5 =>
        /* delivery price divided by articles - prezzo di consegna suddiviso per artiolo */
        array(
            'cid' => '5',
            'name' => 'QTA',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        6 =>
        /* on hybrid version is the addition symbol - simbolo variante su ibrido */
        array(
            'cid' => '6',
            'name' => 'VAR',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        7 =>
        /* on hybrid DEVO SCRIVERLO */
        array(
            'cid' => '7',
            'name' => 'QTAP',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        8 =>
        /* hybrid category id */
        array(
            'cid' => '8',
            'name' => 'CAT',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        9 =>
        /* hybrid product cost */
        array(
            'cid' => '9',
            'name' => 'PRZ',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        10 =>
        /* hybrid DEVO SCRIVERLO */
        array(
            'cid' => '10',
            'name' => 'LIB2',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        11 =>
        array(
            'cid' => '11',
            'name' => 'PRG',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        12 =>
        array(
            'cid' => '12',
            'name' => 'LIB5',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        13 =>
        array(
            'cid' => '13',
            'name' => 'NSEGN',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        14 =>
        array(
            'cid' => '14',
            'name' => 'AUTORIZ',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        15 =>
        array(
            'cid' => '15',
            'name' => 'LIB1',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        16 =>
        array(
            'cid' => '16',
            'name' => 'LIB3',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        17 =>
        array(
            'cid' => '17',
            'name' => 'LIB4',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        18 =>
        array(
            'cid' => '18',
            'name' => 'NCARD5',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        19 =>
        /* seats number - numero di coperti */
        array(
            'cid' => '19',
            'name' => 'nump_xml',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        20 =>
        /* usually SUBITO or empty value */
        array(
            'cid' => '20',
            'name' => 'jolly',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        21 =>
        /* receipt type (SCONTRINO, FATTURA, CONTO, INCASSO) */
        array(
            'cid' => '21',
            'name' => 'tipo_ricevuta',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        22 =>
        /* fiscal receipt (SCONTRINO or FATTURA) progressive number */
        array(
            'cid' => '22',
            'name' => 'progressivo_fiscale',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        23 =>
        /* DEVO SCRIVERLO */
        array(
            'cid' => '23',
            'name' => 'ordinamento',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        24 =>
        /* if it is S you have to take it as a last course (e.g. coffee) */
        array(
            'cid' => '24',
            'name' => 'ultima_portata',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        25 =>
        /* product code */
        array(
            'cid' => '25',
            'name' => 'cod_articolo',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        26 =>
        /* partial account number, if you split the accounts */
        array(
            'cid' => '26',
            'name' => 'numero_conto',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        27 =>
        /* it's 1 if has sub-additions, or empty */
        array(
            'cid' => '27',
            'name' => 'contiene_variante',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        28 =>
        /* printer destination id */
        array(
            'cid' => '28',
            'name' => 'dest_stampa',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        29 =>
        /* product additions category id
        before slash is the normal additions category
        after slash is the automatic additions category
        ND is not defined
        */
        array(
            'cid' => '29',
            'name' => 'cat_variante',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        30 =>
        /* it's the record type, always CORPO */
        array(
            'cid' => '30',
            'name' => 'tipo_record',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        31 =>
        /* the state of the product (ATTIVO means visible now or ACTIVE, 
        FORNO means has been placed in OVEN, CANCELLATO is deleted, ECC...)*/
        array(
            'cid' => '31',
            'name' => 'stato_record',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        32 =>
        /* unused */
        array(
            'cid' => '32',
            'name' => 'planning_colori',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        33 =>
        /* receipt fiscal date */
        array(
            'cid' => '33',
            'name' => 'data_fisc',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        34 =>
        /* receipt fiscal time */
        array(
            'cid' => '34',
            'name' => 'ora_fisc',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        35 =>
        /* table from which the order was taken */
        array(
            'cid' => '35',
            'name' => 'ntav_comanda',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        36 =>
        /* unused */
        array(
            'cid' => '36',
            'name' => 'ntav_attrib',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        37 =>
        /* name of the splitted table */
        array(
            'cid' => '37',
            'name' => 'tab_bis',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        38 =>
        /* progressive number of inserted row */
        array(
            'cid' => '38',
            'name' => 'prog_inser',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        39 =>
        array(
            'cid' => '39',
            'name' => 'art_primario',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        40 =>
        array(
            'cid' => '40',
            'name' => 'art_variante',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        41 =>
        array(
            'cid' => '41',
            'name' => 'tipo_variante',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        42 =>
        /* product description */
        array(
            'cid' => '42',
            'name' => 'desc_art',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        43 =>
        /* product quantity */
        array(
            'cid' => '43',
            'name' => 'quantita',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        44 =>
        /* unit price */
        array(
            'cid' => '44',
            'name' => 'prezzo_un',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        45 =>
        /* discount percentage */
        array(
            'cid' => '45',
            'name' => 'sconto_perc',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        46 =>
        /* discount price */
        array(
            'cid' => '46',
            'name' => 'sconto_imp',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        47 =>
        array(
            'cid' => '47',
            'name' => 'netto',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        48 =>
        array(
            'cid' => '48',
            'name' => 'imp_tot_netto',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        49 =>
        array(
            'cid' => '49',
            'name' => 'perc_iva',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        50 =>
        array(
            'cid' => '50',
            'name' => 'costo',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        51 =>
        array(
            'cid' => '51',
            'name' => 'autor_sconto',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        52 =>
        array(
            'cid' => '52',
            'name' => 'categoria',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        53 =>
        array(
            'cid' => '53',
            'name' => 'gruppo',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        54 =>
        array(
            'cid' => '54',
            'name' => 'incassato',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        55 =>
        array(
            'cid' => '55',
            'name' => 'residuo',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        56 =>
        array(
            'cid' => '56',
            'name' => 'tipo_incasso',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        57 =>
        array(
            'cid' => '57',
            'name' => 'contanti',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        58 =>
        array(
            'cid' => '58',
            'name' => 'carta_credito',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        59 =>
        array(
            'cid' => '59',
            'name' => 'bancomat',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        60 =>
        array(
            'cid' => '60',
            'name' => 'assegni',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        61 =>
        array(
            'cid' => '61',
            'name' => 'tessera_prepagata',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        62 =>
        array(
            'cid' => '62',
            'name' => 'numero_tessera_prepag',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        63 =>
        array(
            'cid' => '63',
            'name' => 'stampata_sn',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        64 =>
        array(
            'cid' => '64',
            'name' => 'data_stampa',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        65 =>
        array(
            'cid' => '65',
            'name' => 'ora_stampa',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        66 =>
        array(
            'cid' => '66',
            'name' => 'fiscalizzata_sn',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        67 =>
        array(
            'cid' => '67',
            'name' => 'data_',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        68 =>
        array(
            'cid' => '68',
            'name' => 'ora_',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        69 =>
        array(
            'cid' => '69',
            'name' => 'n_fiscale',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        70 =>
        array(
            'cid' => '70',
            'name' => 'n_conto_parziale',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        71 =>
        array(
            'cid' => '71',
            'name' => 'nodo',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        72 =>
        array(
            'cid' => '72',
            'name' => 'portata',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        73 =>
        array(
            'cid' => '73',
            'name' => 'ult_portata',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        74 =>
        array(
            'cid' => '74',
            'name' => 'centro',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        75 =>
        array(
            'cid' => '75',
            'name' => 'terminale',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        76 =>
        array(
            'cid' => '76',
            'name' => 'operatore',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        77 =>
        array(
            'cid' => '77',
            'name' => 'cod_cliente',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        78 =>
        array(
            'cid' => '78',
            'name' => 'rag_soc_cliente',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        79 =>
        array(
            'cid' => '79',
            'name' => 'coef_trasf',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        80 =>
        array(
            'cid' => '80',
            'name' => 'peso_x_um',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        81 =>
        array(
            'cid' => '81',
            'name' => 'peso_ums',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        82 =>
        array(
            'cid' => '82',
            'name' => 'peso_tot',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        83 =>
        array(
            'cid' => '83',
            'name' => 'qnt_prod',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        84 =>
        array(
            'cid' => '84',
            'name' => 'nome_comanda',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        85 =>
        array(
            'cid' => '85',
            'name' => 'posizione',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        86 =>
        array(
            'cid' => '86',
            'name' => 'fiscale_sospeso',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        87 =>
        array(
            'cid' => '87',
            'name' => 'BIS',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        88 =>
        array(
            'cid' => '88',
            'name' => 'nome_pc',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        89 =>
        array(
            'cid' => '89',
            'name' => 'giorno',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        90 =>
        array(
            'cid' => '90',
            'name' => 'cod_promo',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        91 =>
        array(
            'cid' => '91',
            'name' => 'qta_fissa',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        92 =>
        array(
            'cid' => '92',
            'name' => 'spazio_forno',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        93 =>
        array(
            'cid' => '93',
            'name' => 'tipo_impasto',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        94 =>
        array(
            'cid' => '94',
            'name' => 'tasto_segue',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        95 =>
        array(
            'cid' => '95',
            'name' => 'NCARD2',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        96 =>
        array(
            'cid' => '96',
            'name' => 'NCARD3',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        97 =>
        array(
            'cid' => '97',
            'name' => 'NCARD4',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        98 =>
        array(
            'cid' => '98',
            'name' => 'NEXIT',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        99 =>
        array(
            'cid' => '99',
            'name' => 'droppay',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        100 =>
        array(
            'cid' => '100',
            'name' => 'dest_stampa_2',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        101 =>
        array(
            'cid' => '101',
            'name' => 'id_pony',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        102 =>
        array(
            'cid' => '102',
            'name' => 'prezzo_varianti_aggiuntive',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        103 =>
        array(
            'cid' => '103',
            'name' => 'prezzo_varianti_maxi',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        104 =>
        array(
            'cid' => '104',
            'name' => 'prezzo_maxi_prima',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        105 =>
        array(
            'cid' => '105',
            'name' => 'prezzo_vero',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        106 =>
        array(
            'cid' => '106',
            'name' => 'riepilogo',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        107 =>
        array(
            'cid' => '107',
            'name' => 'data_servizio',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        108 =>
        array(
            'cid' => '108',
            'name' => 'data_comanda',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        109 =>
        array(
            'cid' => '109',
            'name' => 'ora_comanda',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        110 =>
        array(
            'cid' => '110',
            'name' => 'numero_paganti',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        111 =>
        array(
            'cid' => '111',
            'name' => 'numero_servizio',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        112 =>
        array(
            'cid' => '112',
            'name' => 'imponibile_rep_1',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        113 =>
        array(
            'cid' => '113',
            'name' => 'imponibile_rep_2',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        114 =>
        array(
            'cid' => '114',
            'name' => 'imponibile_rep_3',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        115 =>
        array(
            'cid' => '115',
            'name' => 'imponibile_rep_4',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        116 =>
        array(
            'cid' => '116',
            'name' => 'imponibile_rep_5',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        117 =>
        array(
            'cid' => '117',
            'name' => 'imposta_rep_1',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        118 =>
        array(
            'cid' => '118',
            'name' => 'imposta_rep_2',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        119 =>
        array(
            'cid' => '119',
            'name' => 'imposta_rep_3',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        120 =>
        array(
            'cid' => '120',
            'name' => 'imposta_rep_4',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        121 =>
        array(
            'cid' => '121',
            'name' => 'imposta_rep_5',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        122 =>
        array(
            'cid' => '122',
            'name' => 'totale_scontato_ivato',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        123 =>
        array(
            'cid' => '123',
            'name' => 'prezzo_maxi',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        124 =>
        array(
            'cid' => '124',
            'name' => 'misuratore_riferimento',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        125 =>
        array(
            'cid' => '125',
            'name' => 'operatore_cancellazione',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        126 =>
        array(
            'cid' => '126',
            'name' => 'TID',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        127 =>
        array(
            'cid' => '127',
            'name' => 'tipologia_fattorino',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        128 =>
        array(
            'cid' => '128',
            'name' => 'AGG_GIAC',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        129 =>
        array(
            'cid' => '129',
            'name' => 'orario_preparazione',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        130 =>
        array(
            'cid' => '130',
            'name' => 'reparto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        131 =>
        array(
            'cid' => '131',
            'name' => 'importo_totale_fiscale',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        132 =>
        array(
            'cid' => '132',
            'name' => 'progr_gg_uni',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        133 =>
        array(
            'cid' => '133',
            'name' => 'start_data_tid',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        134 =>
        array(
            'cid' => '134',
            'name' => 'record_cancellato',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        134 =>
        array(
            'cid' => '135',
            'name' => 'consegna_gap',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
    'pony_express' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'INTEGER',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '1',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'nome',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
    'record_teste' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'prim_key',
            'type' => 'INTEGER',
            'notnull' => '0',
            'dflt_value' => '\'\'',
            'pk' => '0',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'id',
            'type' => 'INTEGER',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '1',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'progressivo_comanda',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        3 =>
        array(
            'cid' => '3',
            'name' => 'parziale_comanda',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        4 =>
        array(
            'cid' => '4',
            'name' => 'tipologia',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        5 =>
        array(
            'cid' => '5',
            'name' => 'contanti',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        6 =>
        array(
            'cid' => '6',
            'name' => 'bancomat',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        7 =>
        array(
            'cid' => '7',
            'name' => 'carte_credito',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        8 =>
        array(
            'cid' => '8',
            'name' => 'totale',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        9 =>
        array(
            'cid' => '9',
            'name' => 'operatore',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        10 =>
        array(
            'cid' => '10',
            'name' => 'tavolo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        11 =>
        array(
            'cid' => '11',
            'name' => 'data',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        12 =>
        array(
            'cid' => '12',
            'name' => 'ora',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        13 =>
        array(
            'cid' => '13',
            'name' => 'locale',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        14 =>
        array(
            'cid' => '14',
            'name' => 'numero_fiscale',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        15 =>
        array(
            'cid' => '15',
            'name' => 'bool_fatturato',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        16 =>
        array(
            'cid' => '16',
            'name' => 'data_fatturazione',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        17 =>
        array(
            'cid' => '17',
            'name' => 'numero_fattura',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        18 =>
        array(
            'cid' => '18',
            'name' => 'non_pagato',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        19 =>
        array(
            'cid' => '19',
            'name' => 'id_cliente',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        20 =>
        array(
            'cid' => '20',
            'name' => 'iva_non_pagato',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        21 =>
        array(
            'cid' => '21',
            'name' => 'nome_buono',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        22 =>
        array(
            'cid' => '22',
            'name' => 'buoni_pasto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        23 =>
        array(
            'cid' => '23',
            'name' => 'numero_buoni_pasto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        24 =>
        array(
            'cid' => '24',
            'name' => 'buoni_acquisto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        25 =>
        array(
            'cid' => '25',
            'name' => 'buoni_acquisto_emessi',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        26 =>
        array(
            'cid' => '26',
            'name' => 'nome_buono_2',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        27 =>
        array(
            'cid' => '27',
            'name' => 'buoni_pasto_2',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        28 =>
        array(
            'cid' => '28',
            'name' => 'numero_buoni_pasto_2',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        29 =>
        array(
            'cid' => '29',
            'name' => 'nome_buono_3',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        30 =>
        array(
            'cid' => '30',
            'name' => 'buoni_pasto_3',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        31 =>
        array(
            'cid' => '31',
            'name' => 'numero_buoni_pasto_3',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        32 =>
        array(
            'cid' => '32',
            'name' => 'nome_buono_4',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        33 =>
        array(
            'cid' => '33',
            'name' => 'buoni_pasto_4',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        34 =>
        array(
            'cid' => '34',
            'name' => 'numero_buoni_pasto_4',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        35 =>
        array(
            'cid' => '35',
            'name' => 'nome_buono_5',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        36 =>
        array(
            'cid' => '36',
            'name' => 'buoni_pasto_5',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        37 =>
        array(
            'cid' => '37',
            'name' => 'numero_buoni_pasto_5',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        38 =>
        array(
            'cid' => '38',
            'name' => 'resto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        39 =>
        array(
            'cid' => '39',
            'name' => 'nome_pc',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        40 =>
        array(
            'cid' => '40',
            'name' => 'giorno',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        41 =>
        array(
            'cid' => '41',
            'name' => 'droppay',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        42 =>
        array(
            'cid' => '42',
            'name' => 'data_comanda',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        43 =>
        array(
            'cid' => '43',
            'name' => 'ora_comanda',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        44 =>
        array(
            'cid' => '44',
            'name' => 'data_servizio',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        45 =>
        array(
            'cid' => '45',
            'name' => 'imposta_rep_1',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        46 =>
        array(
            'cid' => '46',
            'name' => 'imposta_rep_2',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        47 =>
        array(
            'cid' => '47',
            'name' => 'imposta_rep_3',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        48 =>
        array(
            'cid' => '48',
            'name' => 'imposta_rep_4',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        49 =>
        array(
            'cid' => '49',
            'name' => 'imposta_rep_5',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        50 =>
        array(
            'cid' => '50',
            'name' => 'totale_rep_1',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        51 =>
        array(
            'cid' => '51',
            'name' => 'totale_rep_2',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        52 =>
        array(
            'cid' => '52',
            'name' => 'totale_rep_3',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        53 =>
        array(
            'cid' => '53',
            'name' => 'totale_rep_4',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        54 =>
        array(
            'cid' => '54',
            'name' => 'totale_rep_5',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        55 =>
        array(
            'cid' => '55',
            'name' => 'assegno',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        56 =>
        array(
            'cid' => '56',
            'name' => 'bonifico',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        57 =>
        array(
            'cid' => '57',
            'name' => 'ult_prog_inser',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        58 =>
        array(
            'cid' => '58',
            'name' => 'numero_servizio',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        59 =>
        array(
            'cid' => '59',
            'name' => 'imponibile_rep_1',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        60 =>
        array(
            'cid' => '60',
            'name' => 'imponibile_rep_2',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        61 =>
        array(
            'cid' => '61',
            'name' => 'imponibile_rep_3',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        62 =>
        array(
            'cid' => '62',
            'name' => 'imponibile_rep_4',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        63 =>
        array(
            'cid' => '63',
            'name' => 'imponibile_rep_5',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        64 =>
        array(
            'cid' => '64',
            'name' => 'percentuale_rep_1',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        65 =>
        array(
            'cid' => '65',
            'name' => 'percentuale_rep_2',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        66 =>
        array(
            'cid' => '66',
            'name' => 'percentuale_rep_3',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        67 =>
        array(
            'cid' => '67',
            'name' => 'percentuale_rep_4',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        68 =>
        array(
            'cid' => '68',
            'name' => 'percentuale_rep_5',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        69 =>
        array(
            'cid' => '69',
            'name' => 'carte_credito2',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        70 =>
        array(
            'cid' => '70',
            'name' => 'carte_credito3',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        71 =>
        array(
            'cid' => '71',
            'name' => 'misuratore_riferimento',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        72 =>
        array(
            'cid' => '72',
            'name' => 'zrep',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        73 =>
        array(
            'cid' => '73',
            'name' => 'TID',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        74 =>
        array(
            'cid' => '74',
            'name' => 'indice_rep_1',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        75 =>
        array(
            'cid' => '75',
            'name' => 'indice_rep_2',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        76 =>
        array(
            'cid' => '76',
            'name' => 'indice_rep_3',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        77 =>
        array(
            'cid' => '77',
            'name' => 'indice_rep_4',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        78 =>
        array(
            'cid' => '78',
            'name' => 'indice_rep_5',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        79 =>
        array(
            'cid' => '79',
            'name' => 'non_riscosso_servizi',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        80 =>
        array(
            'cid' => '80',
            'name' => 'sconto_a_pagare',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        81 =>
        array(
            'cid' => '81',
            'name' => 'arrotondamento',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        82 =>
        array(
            'cid' => '82',
            'name' => 'progr_gg_uni',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
    'progressivo_asporto_domicilio' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'numero',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
    'elenco_progressivi_asporto_domicilio' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'numero',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'progressivo_comanda',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
    'prodotti' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'descrizione',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'prezzo_1',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        3 =>
        array(
            'cid' => '3',
            'name' => 'categoria',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        4 =>
        array(
            'cid' => '4',
            'name' => 'cod_articolo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        5 =>
        array(
            'cid' => '5',
            'name' => 'pagina',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        6 =>
        array(
            'cid' => '6',
            'name' => 'posizione',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        7 =>
        array(
            'cid' => '7',
            'name' => 'ordinamento',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        8 =>
        array(
            'cid' => '8',
            'name' => 'cat_varianti',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        9 =>
        array(
            'cid' => '9',
            'name' => 'cat_ricorrente',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        10 =>
        array(
            'cid' => '10',
            'name' => 'gruppo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        11 =>
        array(
            'cid' => '11',
            'name' => 'perc_iva_base',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        12 =>
        array(
            'cid' => '12',
            'name' => 'perc_iva_takeaway',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        13 =>
        array(
            'cid' => '13',
            'name' => 'dest_st_1',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        14 =>
        array(
            'cid' => '14',
            'name' => 'dest_st_2',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        15 =>
        array(
            'cid' => '15',
            'name' => 'portata',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        16 =>
        array(
            'cid' => '16',
            'name' => 'ultima_portata',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        17 =>
        array(
            'cid' => '17',
            'name' => 'prezzo_2',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        18 =>
        array(
            'cid' => '18',
            'name' => 'prezzo_3',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        19 =>
        array(
            'cid' => '19',
            'name' => 'prezzo_4',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        20 =>
        array(
            'cid' => '20',
            'name' => 'giacenza_flash',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        21 =>
        array(
            'cid' => '21',
            'name' => 'giacenza_reale',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        22 =>
        array(
            'cid' => '22',
            'name' => 'giacenza',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        23 =>
        array(
            'cid' => '23',
            'name' => 'costo_un',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        24 =>
        array(
            'cid' => '24',
            'name' => 'colore_tasto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        25 =>
        array(
            'cid' => '25',
            'name' => 'coef_trasf',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        26 =>
        array(
            'cid' => '26',
            'name' => 'peso_ums',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        27 =>
        array(
            'cid' => '27',
            'name' => 'desc_lunga',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        28 =>
        array(
            'cid' => '28',
            'name' => 'immagine',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '""',
            'pk' => '0',
        ),
        29 =>
        array(
            'cid' => '29',
            'name' => 'perc_servizio',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        30 =>
        array(
            'cid' => '30',
            'name' => 'ult_mod',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        31 =>
        array(
            'cid' => '31',
            'name' => 'ricetta',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        32 =>
        array(
            'cid' => '32',
            'name' => 'colore_tasto_vecchio',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        33 =>
        array(
            'cid' => '33',
            'name' => 'ricetta_fastorder',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => '""',
            'pk' => '0',
        ),
        34 =>
        array(
            'cid' => '34',
            'name' => 'posizione_2',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        35 =>
        array(
            'cid' => '35',
            'name' => 'posizione_3',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        36 =>
        array(
            'cid' => '36',
            'name' => 'posizione_4',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        37 =>
        array(
            'cid' => '37',
            'name' => 'posizione_1',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        38 =>
        array(
            'cid' => '38',
            'name' => 'abilita_riepilogo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        39 =>
        array(
            'cid' => '39',
            'name' => 'cod_promo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        40 =>
        array(
            'cid' => '40',
            'name' => 'qta_fissa',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        41 =>
        array(
            'cid' => '41',
            'name' => 'spazio_forno',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        42 =>
        array(
            'cid' => '42',
            'name' => 'listino_bar',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        43 =>
        array(
            'cid' => '43',
            'name' => 'listino_asporto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        44 =>
        array(
            'cid' => '44',
            'name' => 'listino_continuo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        45 =>
        array(
            'cid' => '45',
            'name' => 'listino_tavolo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        46 =>
        array(
            'cid' => '46',
            'name' => 'prezzo_varianti_aggiuntive',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        47 =>
        array(
            'cid' => '47',
            'name' => 'prezzo_maxi',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        48 =>
        array(
            'cid' => '48',
            'name' => 'prezzo_varianti_maxi',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        49 =>
        array(
            'cid' => '49',
            'name' => 'prezzo_maxi_prima',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        50 =>
        array(
            'cid' => '50',
            'name' => 'automodifica_prezzo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        51 =>
        array(
            'cid' => '51',
            'name' => 'prezzo_variante_meno',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        52 =>
        array(
            'cid' => '52',
            'name' => 'prezzo_fattorino1_norm',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        53 =>
        array(
            'cid' => '53',
            'name' => 'prezzo_fattorino2_norm',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        54 =>
        array(
            'cid' => '54',
            'name' => 'prezzo_fattorino3_norm',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        55 =>
        array(
            'cid' => '55',
            'name' => 'prezzo_fattorino1_maxi',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        56 =>
        array(
            'cid' => '56',
            'name' => 'prezzo_fattorino2_maxi',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        57 =>
        array(
            'cid' => '57',
            'name' => 'prezzo_fattorino3_maxi',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        58 =>
        array(
            'cid' => '58',
            'name' => 'costo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        59 =>
        array(
            'cid' => '59',
            'name' => 'reparto_servizi',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        60 =>
        array(
            'cid' => '60',
            'name' => 'reparto_beni',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        61 =>
        array(
            'cid' => '61',
            'name' => 'descrizione_fastorder',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => '""',
            'pk' => '0',
        ),
        62 =>
        array(
            'cid' => '62',
            'name' => 'esportazione_fastorder_abilitata',
            'type' => '',
            'notnull' => '0',
            'dflt_value' => '"true"',
            'pk' => '0',
        ),
    ),
    'clienti' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'INTEGER',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '1',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'ragione_sociale',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'indirizzo',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        3 =>
        array(
            'cid' => '3',
            'name' => 'citta',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        4 =>
        array(
            'cid' => '4',
            'name' => 'provincia',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        5 =>
        array(
            'cid' => '5',
            'name' => 'telefono',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        6 =>
        array(
            'cid' => '6',
            'name' => 'email',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        7 =>
        array(
            'cid' => '7',
            'name' => 'codice_fiscale',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        8 =>
        array(
            'cid' => '8',
            'name' => 'partita_iva',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        9 =>
        array(
            'cid' => '9',
            'name' => 'cod_cliente',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        10 =>
        array(
            'cid' => '10',
            'name' => 'tipo_cliente',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        11 =>
        array(
            'cid' => '11',
            'name' => 'cap',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        12 =>
        array(
            'cid' => '12',
            'name' => 'paese',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        13 =>
        array(
            'cid' => '13',
            'name' => 'fatt_fine_mese_sn',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        14 =>
        array(
            'cid' => '14',
            'name' => 'cat_cliente',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        15 =>
        array(
            'cid' => '15',
            'name' => 'cellulare',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        16 =>
        array(
            'cid' => '16',
            'name' => 'fax',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        17 =>
        array(
            'cid' => '17',
            'name' => 'contatto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        18 =>
        array(
            'cid' => '18',
            'name' => 'banca',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        19 =>
        array(
            'cid' => '19',
            'name' => 'abi',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        20 =>
        array(
            'cid' => '20',
            'name' => 'cab',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        21 =>
        array(
            'cid' => '21',
            'name' => 'cin',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        22 =>
        array(
            'cid' => '22',
            'name' => 'cc',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        23 =>
        array(
            'cid' => '23',
            'name' => 'iban',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        24 =>
        array(
            'cid' => '24',
            'name' => 'bic_swift',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        25 =>
        array(
            'cid' => '25',
            'name' => 'listino',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        26 =>
        array(
            'cid' => '26',
            'name' => 'esenzione',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        27 =>
        array(
            'cid' => '27',
            'name' => 'aliquota_iva_agev',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        28 =>
        array(
            'cid' => '28',
            'name' => 'cod_pag_desc',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        29 =>
        array(
            'cid' => '29',
            'name' => 'sconto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        30 =>
        array(
            'cid' => '30',
            'name' => 'cos_mnem_circ',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        31 =>
        array(
            'cid' => '31',
            'name' => 'cod_rif_cli_circ',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        32 =>
        array(
            'cid' => '32',
            'name' => 'foto_cliente',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        33 =>
        array(
            'cid' => '33',
            'name' => 'note',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        34 =>
        array(
            'cid' => '34',
            'name' => 'numero',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        35 =>
        array(
            'cid' => '35',
            'name' => 'attivazione_tessere',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        36 =>
        array(
            'cid' => '36',
            'name' => 'data_attivazione',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        37 =>
        array(
            'cid' => '37',
            'name' => 'tipo_tessere',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        38 =>
        array(
            'cid' => '38',
            'name' => 'validita_tessere',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        39 =>
        array(
            'cid' => '39',
            'name' => 'monouso_giornaliero',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        40 =>
        array(
            'cid' => '40',
            'name' => 'uso_privato_aziendale',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        41 =>
        array(
            'cid' => '41',
            'name' => 'valore_tessera',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        42 =>
        array(
            'cid' => '42',
            'name' => 'iva_non_pagati',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        43 =>
        array(
            'cid' => '43',
            'name' => 'data_nascita',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        44 =>
        array(
            'cid' => '44',
            'name' => 'nome',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        45 =>
        array(
            'cid' => '45',
            'name' => 'cognome',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        46 =>
        array(
            'cid' => '46',
            'name' => 'telefono_3',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        47 =>
        array(
            'cid' => '47',
            'name' => 'telefono_4',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        48 =>
        array(
            'cid' => '48',
            'name' => 'telefono_5',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        49 =>
        array(
            'cid' => '49',
            'name' => 'telefono_6',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        50 =>
        array(
            'cid' => '50',
            'name' => 'partita_iva_estera',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        51 =>
        array(
            'cid' => '51',
            'name' => 'formato_trasmissione',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        52 =>
        array(
            'cid' => '52',
            'name' => 'codice_destinatario',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        53 =>
        array(
            'cid' => '53',
            'name' => 'regime_fiscale',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        54 =>
        array(
            'cid' => '54',
            'name' => 'pec',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        55 =>
        array(
            'cid' => '55',
            'name' => 'comune',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        56 =>
        array(
            'cid' => '56',
            'name' => 'id_nazione',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        57 =>
        array(
            'cid' => '57',
            'name' => 'nazione',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        58 =>
        array(
            'cid' => '58',
            'name' => 'esente_fattura_elettronica',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        59 =>
        array(
            'cid' => '59',
            'name' => 'split_payment',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        60 =>
        array(
            'cid' => '60',
            'name' => 'percentuale_sconto_su_totale',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        61 =>
        array(
            'cid' => '61',
            'name' => 'codice_lotteria',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        62 =>
        array(
            'cid' => '62',
            'name' => 'password',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        63 =>
        array(
            'cid' => '63',
            'name' => 'flag_codice_confermato',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ), 64 =>
        array(
            'cid' => '64',
            'name' => 'privacy',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ), 65 =>
        array(
            'cid' => '65',
            'name' => 'maggiorenne',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ), 66 =>
        array(
            'cid' => '66',
            'name' => 'newsletter',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ), 67 =>
        array(
            'cid' => '67',
            'name' => 'richiedi_fattura',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ), 68 =>
        array(
            'cid' => '68',
            'name' => 'codice_tessera',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ), 69 =>
        array(
            'cid' => '69',
            'name' => 'stato_sincro',
            'type' => 'TEXT',
            'notnull' => '1',
            'dflt_value' => '0',
            'pk' => '0',
        ),
    ),
    'elenco_codici_lotteria_comanda' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'INTEGER',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'codice_lotteria',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'progressivo_comanda',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
    'prezzi_impasti_maxi' =>
    array(
        0 =>
        array(
            'cid' => '0',
            'name' => 'id',
            'type' => 'INTEGER',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        1 =>
        array(
            'cid' => '1',
            'name' => 'kumat',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        2 =>
        array(
            'cid' => '2',
            'name' => 'farro',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        3 =>
        array(
            'cid' => '3',
            'name' => 'soja',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        4 =>
        array(
            'cid' => '4',
            'name' => 'integrale',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        5 =>
        array(
            'cid' => '5',
            'name' => 'pasta_madre',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        6 =>
        array(
            'cid' => '6',
            'name' => 'ch_cerali',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        7 =>
        array(
            'cid' => '7',
            'name' => 'set_cerali',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        8 =>
        array(
            'cid' => '8',
            'name' => 'senza_glutine',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        9 =>
        array(
            'cid' => '9',
            'name' => 'carbone',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        10 =>
        array(
            'cid' => '10',
            'name' => 'napoli',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        11 =>
        array(
            'cid' => '11',
            'name' => 'doppia_pasta',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        12 =>
        array(
            'cid' => '12',
            'name' => 'batutta',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        13 =>
        array(
            'cid' => '13',
            'name' => 'batutta_farcita',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        14 =>
        array(
            'cid' => '14',
            'name' => 'enkir',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        15 =>
        array(
            'cid' => '15',
            'name' => 'canapa',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        16 =>
        array(
            'cid' => '16',
            'name' => 'grano_arso',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        17 =>
        array(
            'cid' => '17',
            'name' => 'manitoba',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        18 =>
        array(
            'cid' => '18',
            'name' => 'patate',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        19 =>
        array(
            'cid' => '19',
            'name' => 'pinsa_romana',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        20 =>
        array(
            'cid' => '20',
            'name' => 'fagioli',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        21 =>
        array(
            'cid' => '21',
            'name' => 'buratto',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        22 =>
        array(
            'cid' => '22',
            'name' => 'testo_pasta_pers_1',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        23 =>
        array(
            'cid' => '23',
            'name' => 'testo_pasta_pers_2',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        24 =>
        array(
            'cid' => '24',
            'name' => 'testo_pasta_pers_3',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        25 =>
        array(
            'cid' => '25',
            'name' => 'testo_pasta_pers_4',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        26 =>
        array(
            'cid' => '26',
            'name' => 'testo_pasta_pers_5',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
        27 =>
        array(
            'cid' => '27',
            'name' => 'testo_pasta_pers_6',
            'type' => 'TEXT',
            'notnull' => '0',
            'dflt_value' => NULL,
            'pk' => '0',
        ),
    ),
);

$campiMysql = array(
    'back_office' =>
    array(
        0 =>
        array(
            'COLUMN_NAME' => 'prim_key',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'int(11)',
            'COLUMN_KEY' => 'PRI',
            'EXTRA' => 'auto_increment',
            'IS_NULLABLE' => 'NO',
        ),
        1 =>
        array(
            'COLUMN_NAME' => 'id',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'varchar(21)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        2 =>
        array(
            'COLUMN_NAME' => 'data_comanda',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'date',
            'COLUMN_KEY' => 'MUL',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        3 =>
        array(
            'COLUMN_NAME' => 'ora_comanda',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'time',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        4 =>
        array(
            'COLUMN_NAME' => 'giorno',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'varchar(1)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        5 =>
        array(
            'COLUMN_NAME' => 'cod_articolo',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'varchar(7)',
            'COLUMN_KEY' => 'MUL',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        6 =>
        array(
            'COLUMN_NAME' => 'desc_art',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'varchar(40)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        7 =>
        array(
            'COLUMN_NAME' => 'quantita',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'int(3)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        8 =>
        array(
            'COLUMN_NAME' => 'prezzo_un',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'decimal(12,8)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        9 =>
        array(
            'COLUMN_NAME' => 'prezzo_vero',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'decimal(12,8)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        10 =>
        array(
            'COLUMN_NAME' => 'sconto_imp',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'varchar(6)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        11 =>
        array(
            'COLUMN_NAME' => 'sconto_perc',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'varchar(6)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        12 =>
        array(
            'COLUMN_NAME' => 'netto',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'decimal(12,8)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        13 =>
        array(
            'COLUMN_NAME' => 'tipo_consegna',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'varchar(12)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        14 =>
        array(
            'COLUMN_NAME' => 'QTA',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'varchar(6)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        15 =>
        array(
            'COLUMN_NAME' => 'perc_iva',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'decimal(8,2)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        16 =>
        array(
            'COLUMN_NAME' => 'totale',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'decimal(9,2)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        17 =>
        array(
            'COLUMN_NAME' => 'categoria',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'varchar(3)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        18 =>
        array(
            'COLUMN_NAME' => 'portata',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'varchar(3)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        19 =>
        array(
            'COLUMN_NAME' => 'dest_stampa',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'varchar(3)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        20 =>
        array(
            'COLUMN_NAME' => 'operatore',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'varchar(20)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        21 =>
        array(
            'COLUMN_NAME' => 'ntav_comanda',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'varchar(20)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        22 =>
        array(
            'COLUMN_NAME' => 'tipologia_fattorino',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'varchar(2)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        23 =>
        array(
            'COLUMN_NAME' => 'gruppo',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'varchar(2)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        24 =>
        array(
            'COLUMN_NAME' => 'fiscalizzata_sn',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'varchar(1)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        25 =>
        array(
            'COLUMN_NAME' => 'tipo_ricevuta',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'varchar(24)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        26 =>
        array(
            'COLUMN_NAME' => 'n_fiscale',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'int(4)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        27 =>
        array(
            'COLUMN_NAME' => 'cod_promo',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'int(3)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        28 =>
        array(
            'COLUMN_NAME' => 'posizione',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'varchar(6)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        29 =>
        array(
            'COLUMN_NAME' => 'costo',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'decimal(12,8)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        30 =>
        array(
            'COLUMN_NAME' => 'totale_costo',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'decimal(12,8)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        31 =>
        array(
            'COLUMN_NAME' => 'totale_ricavo',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'decimal(12,8)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        32 =>
        array(
            'COLUMN_NAME' => 'importo_totale_fiscale',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'decimal(12,8)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        33 =>
        array(
            'COLUMN_NAME' => 'tipo_impasto',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'varchar(24)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
    ),
    'comanda' =>
    array(
        0 =>
        array(
            'COLUMN_NAME' => 'prim_key',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'int(11)',
            'COLUMN_KEY' => 'PRI',
            'EXTRA' => 'auto_increment',
            'IS_NULLABLE' => 'NO',
        ),
        1 =>
        array(
            'COLUMN_NAME' => 'id',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'varchar(21)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        2 =>
        array(
            'COLUMN_NAME' => 'ora_consegna',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        3 =>
        array(
            'COLUMN_NAME' => 'tipo_consegna',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        4 =>
        array(
            'COLUMN_NAME' => 'numeretto_asp',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        5 =>
        array(
            'COLUMN_NAME' => 'QTA',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        6 =>
        array(
            'COLUMN_NAME' => 'VAR',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        7 =>
        array(
            'COLUMN_NAME' => 'QTAP',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        8 =>
        array(
            'COLUMN_NAME' => 'CAT',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        9 =>
        array(
            'COLUMN_NAME' => 'PRZ',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        10 =>
        array(
            'COLUMN_NAME' => 'LIB2',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        11 =>
        array(
            'COLUMN_NAME' => 'PRG',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        12 =>
        array(
            'COLUMN_NAME' => 'LIB5',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        13 =>
        array(
            'COLUMN_NAME' => 'NSEGN',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        14 =>
        array(
            'COLUMN_NAME' => 'AUTORIZ',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        15 =>
        array(
            'COLUMN_NAME' => 'LIB1',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        16 =>
        array(
            'COLUMN_NAME' => 'LIB3',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        17 =>
        array(
            'COLUMN_NAME' => 'LIB4',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        18 =>
        array(
            'COLUMN_NAME' => 'NCARD5',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        19 =>
        array(
            'COLUMN_NAME' => 'nump_xml',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        20 =>
        array(
            'COLUMN_NAME' => 'jolly',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        21 =>
        array(
            'COLUMN_NAME' => 'tipo_ricevuta',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'varchar(24)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        22 =>
        array(
            'COLUMN_NAME' => 'progressivo_fiscale',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        23 =>
        array(
            'COLUMN_NAME' => 'ordinamento',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        24 =>
        array(
            'COLUMN_NAME' => 'ultima_portata',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        25 =>
        array(
            'COLUMN_NAME' => 'cod_articolo',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        26 =>
        array(
            'COLUMN_NAME' => 'numero_conto',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        27 =>
        array(
            'COLUMN_NAME' => 'contiene_variante',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        28 =>
        array(
            'COLUMN_NAME' => 'dest_stampa',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        29 =>
        array(
            'COLUMN_NAME' => 'cat_variante',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        30 =>
        array(
            'COLUMN_NAME' => 'tipo_record',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        31 =>
        array(
            'COLUMN_NAME' => 'stato_record',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'varchar(55)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        32 =>
        array(
            'COLUMN_NAME' => 'planning_colori',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        33 =>
        array(
            'COLUMN_NAME' => 'data_fisc',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        34 =>
        array(
            'COLUMN_NAME' => 'ora_fisc',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        35 =>
        array(
            'COLUMN_NAME' => 'ntav_comanda',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        36 =>
        array(
            'COLUMN_NAME' => 'ntav_attrib',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        37 =>
        array(
            'COLUMN_NAME' => 'tab_bis',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        38 =>
        array(
            'COLUMN_NAME' => 'prog_inser',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        39 =>
        array(
            'COLUMN_NAME' => 'art_primario',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        40 =>
        array(
            'COLUMN_NAME' => 'art_variante',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        41 =>
        array(
            'COLUMN_NAME' => 'tipo_variante',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        42 =>
        array(
            'COLUMN_NAME' => 'desc_art',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        43 =>
        array(
            'COLUMN_NAME' => 'quantita',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'int(3)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        44 =>
        array(
            'COLUMN_NAME' => 'prezzo_un',
            'COLUMN_DEFAULT' => '0.00000000',
            'COLUMN_TYPE' => 'decimal(12,8)',
            'COLUMN_KEY' => 'MUL',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        45 =>
        array(
            'COLUMN_NAME' => 'sconto_perc',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        46 =>
        array(
            'COLUMN_NAME' => 'sconto_imp',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        47 =>
        array(
            'COLUMN_NAME' => 'netto',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'decimal(8,2)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        48 =>
        array(
            'COLUMN_NAME' => 'imp_tot_netto',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        49 =>
        array(
            'COLUMN_NAME' => 'perc_iva',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        50 =>
        array(
            'COLUMN_NAME' => 'costo',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        51 =>
        array(
            'COLUMN_NAME' => 'autor_sconto',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        52 =>
        array(
            'COLUMN_NAME' => 'categoria',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        53 =>
        array(
            'COLUMN_NAME' => 'gruppo',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        54 =>
        array(
            'COLUMN_NAME' => 'incassato',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        55 =>
        array(
            'COLUMN_NAME' => 'residuo',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        56 =>
        array(
            'COLUMN_NAME' => 'tipo_incasso',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        57 =>
        array(
            'COLUMN_NAME' => 'contanti',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        58 =>
        array(
            'COLUMN_NAME' => 'carta_credito',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        59 =>
        array(
            'COLUMN_NAME' => 'bancomat',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        60 =>
        array(
            'COLUMN_NAME' => 'assegni',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        61 =>
        array(
            'COLUMN_NAME' => 'tessera_prepagata',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        62 =>
        array(
            'COLUMN_NAME' => 'numero_tessera_prepag',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        63 =>
        array(
            'COLUMN_NAME' => 'stampata_sn',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        64 =>
        array(
            'COLUMN_NAME' => 'data_stampa',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        65 =>
        array(
            'COLUMN_NAME' => 'ora_stampa',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        66 =>
        array(
            'COLUMN_NAME' => 'fiscalizzata_sn',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'varchar(1)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        67 =>
        array(
            'COLUMN_NAME' => 'data_',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        68 =>
        array(
            'COLUMN_NAME' => 'ora_',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        69 =>
        array(
            'COLUMN_NAME' => 'n_fiscale',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        70 =>
        array(
            'COLUMN_NAME' => 'n_conto_parziale',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        71 =>
        array(
            'COLUMN_NAME' => 'nodo',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        72 =>
        array(
            'COLUMN_NAME' => 'portata',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'varchar(9)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        73 =>
        array(
            'COLUMN_NAME' => 'ult_portata',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        74 =>
        array(
            'COLUMN_NAME' => 'centro',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        75 =>
        array(
            'COLUMN_NAME' => 'terminale',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        76 =>
        array(
            'COLUMN_NAME' => 'operatore',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        77 =>
        array(
            'COLUMN_NAME' => 'cod_cliente',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        78 =>
        array(
            'COLUMN_NAME' => 'rag_soc_cliente',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        79 =>
        array(
            'COLUMN_NAME' => 'coef_trasf',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        80 =>
        array(
            'COLUMN_NAME' => 'peso_x_um',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        81 =>
        array(
            'COLUMN_NAME' => 'peso_ums',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        82 =>
        array(
            'COLUMN_NAME' => 'peso_tot',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        83 =>
        array(
            'COLUMN_NAME' => 'qnt_prod',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        84 =>
        array(
            'COLUMN_NAME' => 'nome_comanda',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        85 =>
        array(
            'COLUMN_NAME' => 'posizione',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'varchar(7)',
            'COLUMN_KEY' => 'MUL',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        86 =>
        array(
            'COLUMN_NAME' => 'fiscale_sospeso',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        87 =>
        array(
            'COLUMN_NAME' => 'BIS',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        88 =>
        array(
            'COLUMN_NAME' => 'nome_pc',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        89 =>
        array(
            'COLUMN_NAME' => 'giorno',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        90 =>
        array(
            'COLUMN_NAME' => 'cod_promo',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'int(3)',
            'COLUMN_KEY' => 'MUL',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        91 =>
        array(
            'COLUMN_NAME' => 'qta_fissa',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        92 =>
        array(
            'COLUMN_NAME' => 'spazio_forno',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        93 =>
        array(
            'COLUMN_NAME' => 'tipo_impasto',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        94 =>
        array(
            'COLUMN_NAME' => 'tasto_segue',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        95 =>
        array(
            'COLUMN_NAME' => 'NCARD2',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        96 =>
        array(
            'COLUMN_NAME' => 'NCARD3',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        97 =>
        array(
            'COLUMN_NAME' => 'NCARD4',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        98 =>
        array(
            'COLUMN_NAME' => 'NEXIT',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        99 =>
        array(
            'COLUMN_NAME' => 'droppay',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        100 =>
        array(
            'COLUMN_NAME' => 'dest_stampa_2',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        101 =>
        array(
            'COLUMN_NAME' => 'id_pony',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        102 =>
        array(
            'COLUMN_NAME' => 'prezzo_varianti_aggiuntive',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'decimal(8,2)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        103 =>
        array(
            'COLUMN_NAME' => 'prezzo_varianti_maxi',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'decimal(8,2)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        104 =>
        array(
            'COLUMN_NAME' => 'prezzo_maxi_prima',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'decimal(8,2)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        105 =>
        array(
            'COLUMN_NAME' => 'prezzo_vero',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'decimal(8,2)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        106 =>
        array(
            'COLUMN_NAME' => 'riepilogo',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        107 =>
        array(
            'COLUMN_NAME' => 'data_servizio',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'date',
            'COLUMN_KEY' => 'MUL',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        108 =>
        array(
            'COLUMN_NAME' => 'data_comanda',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'date',
            'COLUMN_KEY' => 'MUL',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        109 =>
        array(
            'COLUMN_NAME' => 'ora_comanda',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'time',
            'COLUMN_KEY' => 'MUL',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        110 =>
        array(
            'COLUMN_NAME' => 'numero_paganti',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        111 =>
        array(
            'COLUMN_NAME' => 'numero_servizio',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        112 =>
        array(
            'COLUMN_NAME' => 'imponibile_rep_1',
            'COLUMN_DEFAULT' => '0.00000000',
            'COLUMN_TYPE' => 'decimal(8,8)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        113 =>
        array(
            'COLUMN_NAME' => 'imponibile_rep_2',
            'COLUMN_DEFAULT' => '0.00000000',
            'COLUMN_TYPE' => 'decimal(8,8)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        114 =>
        array(
            'COLUMN_NAME' => 'imponibile_rep_3',
            'COLUMN_DEFAULT' => '0.00000000',
            'COLUMN_TYPE' => 'decimal(8,8)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        115 =>
        array(
            'COLUMN_NAME' => 'imponibile_rep_4',
            'COLUMN_DEFAULT' => '0.00000000',
            'COLUMN_TYPE' => 'decimal(8,8)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        116 =>
        array(
            'COLUMN_NAME' => 'imponibile_rep_5',
            'COLUMN_DEFAULT' => '0.00000000',
            'COLUMN_TYPE' => 'decimal(8,8)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        117 =>
        array(
            'COLUMN_NAME' => 'imposta_rep_1',
            'COLUMN_DEFAULT' => '0.00000000',
            'COLUMN_TYPE' => 'decimal(8,8)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        118 =>
        array(
            'COLUMN_NAME' => 'imposta_rep_2',
            'COLUMN_DEFAULT' => '0.00000000',
            'COLUMN_TYPE' => 'decimal(8,8)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        119 =>
        array(
            'COLUMN_NAME' => 'imposta_rep_3',
            'COLUMN_DEFAULT' => '0.00000000',
            'COLUMN_TYPE' => 'decimal(8,8)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        120 =>
        array(
            'COLUMN_NAME' => 'imposta_rep_4',
            'COLUMN_DEFAULT' => '0.00000000',
            'COLUMN_TYPE' => 'decimal(8,8)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        121 =>
        array(
            'COLUMN_NAME' => 'imposta_rep_5',
            'COLUMN_DEFAULT' => '0.00',
            'COLUMN_TYPE' => 'decimal(8,2)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        122 =>
        array(
            'COLUMN_NAME' => 'totale_scontato_ivato',
            'COLUMN_DEFAULT' => '0.00000000',
            'COLUMN_TYPE' => 'decimal(8,8)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        123 =>
        array(
            'COLUMN_NAME' => 'prezzo_maxi',
            'COLUMN_DEFAULT' => '0.00000000',
            'COLUMN_TYPE' => 'decimal(8,8)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        124 =>
        array(
            'COLUMN_NAME' => 'misuratore_riferimento',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        125 =>
        array(
            'COLUMN_NAME' => 'operatore_cancellazione',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        126 =>
        array(
            'COLUMN_NAME' => 'TID',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        127 =>
        array(
            'COLUMN_NAME' => 'tipologia_fattorino',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        128 =>
        array(
            'COLUMN_NAME' => 'AGG_GIAC',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        129 =>
        array(
            'COLUMN_NAME' => 'orario_preparazione',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        130 =>
        array(
            'COLUMN_NAME' => 'reparto',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        131 =>
        array(
            'COLUMN_NAME' => 'importo_totale_fiscale',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        132 =>
        array(
            'COLUMN_NAME' => 'progr_gg_uni',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        133 =>
        array(
            'COLUMN_NAME' => 'tipo_impasto',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'varchar(24)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
    ),
    'comanda_temp' =>
    array(
        0 =>
        array(
            'COLUMN_NAME' => 'prim_key',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'int(11)',
            'COLUMN_KEY' => 'PRI',
            'EXTRA' => 'auto_increment',
            'IS_NULLABLE' => 'NO',
        ),
        1 =>
        array(
            'COLUMN_NAME' => 'id',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'varchar(21)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        2 =>
        array(
            'COLUMN_NAME' => 'ora_consegna',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        3 =>
        array(
            'COLUMN_NAME' => 'tipo_consegna',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        4 =>
        array(
            'COLUMN_NAME' => 'numeretto_asp',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        5 =>
        array(
            'COLUMN_NAME' => 'QTA',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        6 =>
        array(
            'COLUMN_NAME' => 'VAR',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        7 =>
        array(
            'COLUMN_NAME' => 'QTAP',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        8 =>
        array(
            'COLUMN_NAME' => 'CAT',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        9 =>
        array(
            'COLUMN_NAME' => 'PRZ',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        10 =>
        array(
            'COLUMN_NAME' => 'LIB2',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        11 =>
        array(
            'COLUMN_NAME' => 'PRG',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        12 =>
        array(
            'COLUMN_NAME' => 'LIB5',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        13 =>
        array(
            'COLUMN_NAME' => 'NSEGN',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        14 =>
        array(
            'COLUMN_NAME' => 'AUTORIZ',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        15 =>
        array(
            'COLUMN_NAME' => 'LIB1',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        16 =>
        array(
            'COLUMN_NAME' => 'LIB3',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        17 =>
        array(
            'COLUMN_NAME' => 'LIB4',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        18 =>
        array(
            'COLUMN_NAME' => 'NCARD5',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        19 =>
        array(
            'COLUMN_NAME' => 'nump_xml',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        20 =>
        array(
            'COLUMN_NAME' => 'jolly',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        21 =>
        array(
            'COLUMN_NAME' => 'tipo_ricevuta',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'varchar(24)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        22 =>
        array(
            'COLUMN_NAME' => 'progressivo_fiscale',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        23 =>
        array(
            'COLUMN_NAME' => 'ordinamento',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        24 =>
        array(
            'COLUMN_NAME' => 'ultima_portata',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        25 =>
        array(
            'COLUMN_NAME' => 'cod_articolo',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        26 =>
        array(
            'COLUMN_NAME' => 'numero_conto',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        27 =>
        array(
            'COLUMN_NAME' => 'contiene_variante',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        28 =>
        array(
            'COLUMN_NAME' => 'dest_stampa',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        29 =>
        array(
            'COLUMN_NAME' => 'cat_variante',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        30 =>
        array(
            'COLUMN_NAME' => 'tipo_record',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        31 =>
        array(
            'COLUMN_NAME' => 'stato_record',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'varchar(55)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        32 =>
        array(
            'COLUMN_NAME' => 'planning_colori',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        33 =>
        array(
            'COLUMN_NAME' => 'data_fisc',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        34 =>
        array(
            'COLUMN_NAME' => 'ora_fisc',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        35 =>
        array(
            'COLUMN_NAME' => 'ntav_comanda',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        36 =>
        array(
            'COLUMN_NAME' => 'ntav_attrib',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        37 =>
        array(
            'COLUMN_NAME' => 'tab_bis',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        38 =>
        array(
            'COLUMN_NAME' => 'prog_inser',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        39 =>
        array(
            'COLUMN_NAME' => 'art_primario',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        40 =>
        array(
            'COLUMN_NAME' => 'art_variante',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        41 =>
        array(
            'COLUMN_NAME' => 'tipo_variante',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        42 =>
        array(
            'COLUMN_NAME' => 'desc_art',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        43 =>
        array(
            'COLUMN_NAME' => 'quantita',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'int(3)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        44 =>
        array(
            'COLUMN_NAME' => 'prezzo_un',
            'COLUMN_DEFAULT' => '0.00000000',
            'COLUMN_TYPE' => 'decimal(12,8)',
            'COLUMN_KEY' => 'MUL',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        45 =>
        array(
            'COLUMN_NAME' => 'sconto_perc',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        46 =>
        array(
            'COLUMN_NAME' => 'sconto_imp',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        47 =>
        array(
            'COLUMN_NAME' => 'netto',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'decimal(8,2)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        48 =>
        array(
            'COLUMN_NAME' => 'imp_tot_netto',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        49 =>
        array(
            'COLUMN_NAME' => 'perc_iva',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        50 =>
        array(
            'COLUMN_NAME' => 'costo',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        51 =>
        array(
            'COLUMN_NAME' => 'autor_sconto',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        52 =>
        array(
            'COLUMN_NAME' => 'categoria',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        53 =>
        array(
            'COLUMN_NAME' => 'gruppo',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        54 =>
        array(
            'COLUMN_NAME' => 'incassato',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        55 =>
        array(
            'COLUMN_NAME' => 'residuo',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        56 =>
        array(
            'COLUMN_NAME' => 'tipo_incasso',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        57 =>
        array(
            'COLUMN_NAME' => 'contanti',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        58 =>
        array(
            'COLUMN_NAME' => 'carta_credito',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        59 =>
        array(
            'COLUMN_NAME' => 'bancomat',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        60 =>
        array(
            'COLUMN_NAME' => 'assegni',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        61 =>
        array(
            'COLUMN_NAME' => 'tessera_prepagata',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        62 =>
        array(
            'COLUMN_NAME' => 'numero_tessera_prepag',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        63 =>
        array(
            'COLUMN_NAME' => 'stampata_sn',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        64 =>
        array(
            'COLUMN_NAME' => 'data_stampa',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        65 =>
        array(
            'COLUMN_NAME' => 'ora_stampa',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        66 =>
        array(
            'COLUMN_NAME' => 'fiscalizzata_sn',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'varchar(1)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        67 =>
        array(
            'COLUMN_NAME' => 'data_',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        68 =>
        array(
            'COLUMN_NAME' => 'ora_',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        69 =>
        array(
            'COLUMN_NAME' => 'n_fiscale',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        70 =>
        array(
            'COLUMN_NAME' => 'n_conto_parziale',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        71 =>
        array(
            'COLUMN_NAME' => 'nodo',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        72 =>
        array(
            'COLUMN_NAME' => 'portata',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'varchar(9)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        73 =>
        array(
            'COLUMN_NAME' => 'ult_portata',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        74 =>
        array(
            'COLUMN_NAME' => 'centro',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        75 =>
        array(
            'COLUMN_NAME' => 'terminale',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        76 =>
        array(
            'COLUMN_NAME' => 'operatore',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        77 =>
        array(
            'COLUMN_NAME' => 'cod_cliente',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        78 =>
        array(
            'COLUMN_NAME' => 'rag_soc_cliente',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        79 =>
        array(
            'COLUMN_NAME' => 'coef_trasf',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        80 =>
        array(
            'COLUMN_NAME' => 'peso_x_um',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        81 =>
        array(
            'COLUMN_NAME' => 'peso_ums',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        82 =>
        array(
            'COLUMN_NAME' => 'peso_tot',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        83 =>
        array(
            'COLUMN_NAME' => 'qnt_prod',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        84 =>
        array(
            'COLUMN_NAME' => 'nome_comanda',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        85 =>
        array(
            'COLUMN_NAME' => 'posizione',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'varchar(7)',
            'COLUMN_KEY' => 'MUL',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        86 =>
        array(
            'COLUMN_NAME' => 'fiscale_sospeso',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        87 =>
        array(
            'COLUMN_NAME' => 'BIS',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        88 =>
        array(
            'COLUMN_NAME' => 'nome_pc',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        89 =>
        array(
            'COLUMN_NAME' => 'giorno',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        90 =>
        array(
            'COLUMN_NAME' => 'cod_promo',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'int(3)',
            'COLUMN_KEY' => 'MUL',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        91 =>
        array(
            'COLUMN_NAME' => 'qta_fissa',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        92 =>
        array(
            'COLUMN_NAME' => 'spazio_forno',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        93 =>
        array(
            'COLUMN_NAME' => 'tipo_impasto',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        94 =>
        array(
            'COLUMN_NAME' => 'tasto_segue',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        95 =>
        array(
            'COLUMN_NAME' => 'NCARD2',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        96 =>
        array(
            'COLUMN_NAME' => 'NCARD3',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        97 =>
        array(
            'COLUMN_NAME' => 'NCARD4',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        98 =>
        array(
            'COLUMN_NAME' => 'NEXIT',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        99 =>
        array(
            'COLUMN_NAME' => 'droppay',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        100 =>
        array(
            'COLUMN_NAME' => 'dest_stampa_2',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        101 =>
        array(
            'COLUMN_NAME' => 'id_pony',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        102 =>
        array(
            'COLUMN_NAME' => 'prezzo_varianti_aggiuntive',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'decimal(8,2)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        103 =>
        array(
            'COLUMN_NAME' => 'prezzo_varianti_maxi',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'decimal(8,2)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        104 =>
        array(
            'COLUMN_NAME' => 'prezzo_maxi_prima',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'decimal(8,2)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        105 =>
        array(
            'COLUMN_NAME' => 'prezzo_vero',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'decimal(8,2)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        106 =>
        array(
            'COLUMN_NAME' => 'riepilogo',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        107 =>
        array(
            'COLUMN_NAME' => 'data_servizio',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'date',
            'COLUMN_KEY' => 'MUL',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        108 =>
        array(
            'COLUMN_NAME' => 'data_comanda',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'date',
            'COLUMN_KEY' => 'MUL',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        109 =>
        array(
            'COLUMN_NAME' => 'ora_comanda',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'time',
            'COLUMN_KEY' => 'MUL',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        110 =>
        array(
            'COLUMN_NAME' => 'numero_paganti',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        111 =>
        array(
            'COLUMN_NAME' => 'numero_servizio',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        112 =>
        array(
            'COLUMN_NAME' => 'imponibile_rep_1',
            'COLUMN_DEFAULT' => '0.00000000',
            'COLUMN_TYPE' => 'decimal(8,8)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        113 =>
        array(
            'COLUMN_NAME' => 'imponibile_rep_2',
            'COLUMN_DEFAULT' => '0.00000000',
            'COLUMN_TYPE' => 'decimal(8,8)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        114 =>
        array(
            'COLUMN_NAME' => 'imponibile_rep_3',
            'COLUMN_DEFAULT' => '0.00000000',
            'COLUMN_TYPE' => 'decimal(8,8)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        115 =>
        array(
            'COLUMN_NAME' => 'imponibile_rep_4',
            'COLUMN_DEFAULT' => '0.00000000',
            'COLUMN_TYPE' => 'decimal(8,8)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        116 =>
        array(
            'COLUMN_NAME' => 'imponibile_rep_5',
            'COLUMN_DEFAULT' => '0.00000000',
            'COLUMN_TYPE' => 'decimal(8,8)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        117 =>
        array(
            'COLUMN_NAME' => 'imposta_rep_1',
            'COLUMN_DEFAULT' => '0.00000000',
            'COLUMN_TYPE' => 'decimal(8,8)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        118 =>
        array(
            'COLUMN_NAME' => 'imposta_rep_2',
            'COLUMN_DEFAULT' => '0.00000000',
            'COLUMN_TYPE' => 'decimal(8,8)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        119 =>
        array(
            'COLUMN_NAME' => 'imposta_rep_3',
            'COLUMN_DEFAULT' => '0.00000000',
            'COLUMN_TYPE' => 'decimal(8,8)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        120 =>
        array(
            'COLUMN_NAME' => 'imposta_rep_4',
            'COLUMN_DEFAULT' => '0.00000000',
            'COLUMN_TYPE' => 'decimal(8,8)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        121 =>
        array(
            'COLUMN_NAME' => 'imposta_rep_5',
            'COLUMN_DEFAULT' => '0.00',
            'COLUMN_TYPE' => 'decimal(8,2)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        122 =>
        array(
            'COLUMN_NAME' => 'totale_scontato_ivato',
            'COLUMN_DEFAULT' => '0.00000000',
            'COLUMN_TYPE' => 'decimal(8,8)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        123 =>
        array(
            'COLUMN_NAME' => 'prezzo_maxi',
            'COLUMN_DEFAULT' => '0.00000000',
            'COLUMN_TYPE' => 'decimal(8,8)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        124 =>
        array(
            'COLUMN_NAME' => 'misuratore_riferimento',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        125 =>
        array(
            'COLUMN_NAME' => 'operatore_cancellazione',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        126 =>
        array(
            'COLUMN_NAME' => 'TID',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        127 =>
        array(
            'COLUMN_NAME' => 'tipologia_fattorino',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        128 =>
        array(
            'COLUMN_NAME' => 'AGG_GIAC',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        129 =>
        array(
            'COLUMN_NAME' => 'orario_preparazione',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        130 =>
        array(
            'COLUMN_NAME' => 'reparto',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        131 =>
        array(
            'COLUMN_NAME' => 'importo_totale_fiscale',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        132 =>
        array(
            'COLUMN_NAME' => 'progr_gg_uni',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        133 =>
        array(
            'COLUMN_NAME' => 'tipo_impasto',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'varchar(24)',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
    ),
    'record_teste' =>
    array(
        0 =>
        array(
            'COLUMN_NAME' => 'prim_key',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'int(11)',
            'COLUMN_KEY' => 'PRI',
            'EXTRA' => 'auto_increment',
            'IS_NULLABLE' => 'NO',
        ),
        1 =>
        array(
            'COLUMN_NAME' => 'id',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        2 =>
        array(
            'COLUMN_NAME' => 'progressivo_comanda',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        3 =>
        array(
            'COLUMN_NAME' => 'parziale_comanda',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        4 =>
        array(
            'COLUMN_NAME' => 'tipologia',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        5 =>
        array(
            'COLUMN_NAME' => 'contanti',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        6 =>
        array(
            'COLUMN_NAME' => 'bancomat',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        7 =>
        array(
            'COLUMN_NAME' => 'carte_credito',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        8 =>
        array(
            'COLUMN_NAME' => 'totale',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        9 =>
        array(
            'COLUMN_NAME' => 'operatore',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        10 =>
        array(
            'COLUMN_NAME' => 'tavolo',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        11 =>
        array(
            'COLUMN_NAME' => 'data',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => 'MUL',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        12 =>
        array(
            'COLUMN_NAME' => 'ora',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        13 =>
        array(
            'COLUMN_NAME' => 'locale',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        14 =>
        array(
            'COLUMN_NAME' => 'numero_fiscale',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        15 =>
        array(
            'COLUMN_NAME' => 'bool_fatturato',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        16 =>
        array(
            'COLUMN_NAME' => 'data_fatturazione',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        17 =>
        array(
            'COLUMN_NAME' => 'numero_fattura',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        18 =>
        array(
            'COLUMN_NAME' => 'non_pagato',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        19 =>
        array(
            'COLUMN_NAME' => 'id_cliente',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        20 =>
        array(
            'COLUMN_NAME' => 'iva_non_pagato',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        21 =>
        array(
            'COLUMN_NAME' => 'nome_buono',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        22 =>
        array(
            'COLUMN_NAME' => 'buoni_pasto',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        23 =>
        array(
            'COLUMN_NAME' => 'numero_buoni_pasto',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        24 =>
        array(
            'COLUMN_NAME' => 'buoni_acquisto',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        25 =>
        array(
            'COLUMN_NAME' => 'buoni_acquisto_emessi',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        26 =>
        array(
            'COLUMN_NAME' => 'nome_buono_2',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        27 =>
        array(
            'COLUMN_NAME' => 'buoni_pasto_2',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        28 =>
        array(
            'COLUMN_NAME' => 'numero_buoni_pasto_2',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        29 =>
        array(
            'COLUMN_NAME' => 'nome_buono_3',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        30 =>
        array(
            'COLUMN_NAME' => 'buoni_pasto_3',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        31 =>
        array(
            'COLUMN_NAME' => 'numero_buoni_pasto_3',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        32 =>
        array(
            'COLUMN_NAME' => 'nome_buono_4',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        33 =>
        array(
            'COLUMN_NAME' => 'buoni_pasto_4',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        34 =>
        array(
            'COLUMN_NAME' => 'numero_buoni_pasto_4',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        35 =>
        array(
            'COLUMN_NAME' => 'nome_buono_5',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        36 =>
        array(
            'COLUMN_NAME' => 'buoni_pasto_5',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        37 =>
        array(
            'COLUMN_NAME' => 'numero_buoni_pasto_5',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        38 =>
        array(
            'COLUMN_NAME' => 'resto',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        39 =>
        array(
            'COLUMN_NAME' => 'nome_pc',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        40 =>
        array(
            'COLUMN_NAME' => 'giorno',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        41 =>
        array(
            'COLUMN_NAME' => 'droppay',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        42 =>
        array(
            'COLUMN_NAME' => 'data_comanda',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'date',
            'COLUMN_KEY' => 'MUL',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        43 =>
        array(
            'COLUMN_NAME' => 'ora_comanda',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'time',
            'COLUMN_KEY' => 'MUL',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        44 =>
        array(
            'COLUMN_NAME' => 'data_servizio',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'date',
            'COLUMN_KEY' => 'MUL',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        45 =>
        array(
            'COLUMN_NAME' => 'imposta_rep_1',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        46 =>
        array(
            'COLUMN_NAME' => 'imposta_rep_2',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        47 =>
        array(
            'COLUMN_NAME' => 'imposta_rep_3',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        48 =>
        array(
            'COLUMN_NAME' => 'imposta_rep_4',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        49 =>
        array(
            'COLUMN_NAME' => 'imposta_rep_5',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        50 =>
        array(
            'COLUMN_NAME' => 'totale_rep_1',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        51 =>
        array(
            'COLUMN_NAME' => 'totale_rep_2',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        52 =>
        array(
            'COLUMN_NAME' => 'totale_rep_3',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        53 =>
        array(
            'COLUMN_NAME' => 'totale_rep_4',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        54 =>
        array(
            'COLUMN_NAME' => 'totale_rep_5',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        55 =>
        array(
            'COLUMN_NAME' => 'assegno',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        56 =>
        array(
            'COLUMN_NAME' => 'bonifico',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        57 =>
        array(
            'COLUMN_NAME' => 'ult_prog_inser',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        58 =>
        array(
            'COLUMN_NAME' => 'numero_servizio',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        59 =>
        array(
            'COLUMN_NAME' => 'imponibile_rep_1',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        60 =>
        array(
            'COLUMN_NAME' => 'imponibile_rep_2',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        61 =>
        array(
            'COLUMN_NAME' => 'imponibile_rep_3',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        62 =>
        array(
            'COLUMN_NAME' => 'imponibile_rep_4',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        63 =>
        array(
            'COLUMN_NAME' => 'imponibile_rep_5',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        64 =>
        array(
            'COLUMN_NAME' => 'percentuale_rep_1',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        65 =>
        array(
            'COLUMN_NAME' => 'percentuale_rep_2',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        66 =>
        array(
            'COLUMN_NAME' => 'percentuale_rep_3',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        67 =>
        array(
            'COLUMN_NAME' => 'percentuale_rep_4',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        68 =>
        array(
            'COLUMN_NAME' => 'percentuale_rep_5',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        69 =>
        array(
            'COLUMN_NAME' => 'carte_credito2',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        70 =>
        array(
            'COLUMN_NAME' => 'carte_credito3',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        71 =>
        array(
            'COLUMN_NAME' => 'misuratore_riferimento',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        72 =>
        array(
            'COLUMN_NAME' => 'zrep',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        73 =>
        array(
            'COLUMN_NAME' => 'TID',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        74 =>
        array(
            'COLUMN_NAME' => 'indice_rep_1',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        75 =>
        array(
            'COLUMN_NAME' => 'indice_rep_2',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        76 =>
        array(
            'COLUMN_NAME' => 'indice_rep_3',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        77 =>
        array(
            'COLUMN_NAME' => 'indice_rep_4',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        78 =>
        array(
            'COLUMN_NAME' => 'indice_rep_5',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        79 =>
        array(
            'COLUMN_NAME' => 'non_riscosso_servizi',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        80 =>
        array(
            'COLUMN_NAME' => 'sconto_a_pagare',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        81 =>
        array(
            'COLUMN_NAME' => 'arrotondamento',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        82 =>
        array(
            'COLUMN_NAME' => 'progr_gg_uni',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
    ),
    'record_teste_temp' =>
    array(
        0 =>
        array(
            'COLUMN_NAME' => 'prim_key',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'int(11)',
            'COLUMN_KEY' => 'PRI',
            'EXTRA' => 'auto_increment',
            'IS_NULLABLE' => 'NO',
        ),
        1 =>
        array(
            'COLUMN_NAME' => 'id',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        2 =>
        array(
            'COLUMN_NAME' => 'progressivo_comanda',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        3 =>
        array(
            'COLUMN_NAME' => 'parziale_comanda',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        4 =>
        array(
            'COLUMN_NAME' => 'tipologia',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        5 =>
        array(
            'COLUMN_NAME' => 'contanti',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        6 =>
        array(
            'COLUMN_NAME' => 'bancomat',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        7 =>
        array(
            'COLUMN_NAME' => 'carte_credito',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        8 =>
        array(
            'COLUMN_NAME' => 'totale',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        9 =>
        array(
            'COLUMN_NAME' => 'operatore',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        10 =>
        array(
            'COLUMN_NAME' => 'tavolo',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        11 =>
        array(
            'COLUMN_NAME' => 'data',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => 'MUL',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        12 =>
        array(
            'COLUMN_NAME' => 'ora',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        13 =>
        array(
            'COLUMN_NAME' => 'locale',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        14 =>
        array(
            'COLUMN_NAME' => 'numero_fiscale',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        15 =>
        array(
            'COLUMN_NAME' => 'bool_fatturato',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        16 =>
        array(
            'COLUMN_NAME' => 'data_fatturazione',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        17 =>
        array(
            'COLUMN_NAME' => 'numero_fattura',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        18 =>
        array(
            'COLUMN_NAME' => 'non_pagato',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        19 =>
        array(
            'COLUMN_NAME' => 'id_cliente',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        20 =>
        array(
            'COLUMN_NAME' => 'iva_non_pagato',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        21 =>
        array(
            'COLUMN_NAME' => 'nome_buono',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        22 =>
        array(
            'COLUMN_NAME' => 'buoni_pasto',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        23 =>
        array(
            'COLUMN_NAME' => 'numero_buoni_pasto',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        24 =>
        array(
            'COLUMN_NAME' => 'buoni_acquisto',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        25 =>
        array(
            'COLUMN_NAME' => 'buoni_acquisto_emessi',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        26 =>
        array(
            'COLUMN_NAME' => 'nome_buono_2',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        27 =>
        array(
            'COLUMN_NAME' => 'buoni_pasto_2',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        28 =>
        array(
            'COLUMN_NAME' => 'numero_buoni_pasto_2',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        29 =>
        array(
            'COLUMN_NAME' => 'nome_buono_3',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        30 =>
        array(
            'COLUMN_NAME' => 'buoni_pasto_3',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        31 =>
        array(
            'COLUMN_NAME' => 'numero_buoni_pasto_3',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        32 =>
        array(
            'COLUMN_NAME' => 'nome_buono_4',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        33 =>
        array(
            'COLUMN_NAME' => 'buoni_pasto_4',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        34 =>
        array(
            'COLUMN_NAME' => 'numero_buoni_pasto_4',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        35 =>
        array(
            'COLUMN_NAME' => 'nome_buono_5',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        36 =>
        array(
            'COLUMN_NAME' => 'buoni_pasto_5',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        37 =>
        array(
            'COLUMN_NAME' => 'numero_buoni_pasto_5',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        38 =>
        array(
            'COLUMN_NAME' => 'resto',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        39 =>
        array(
            'COLUMN_NAME' => 'nome_pc',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        40 =>
        array(
            'COLUMN_NAME' => 'giorno',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        41 =>
        array(
            'COLUMN_NAME' => 'droppay',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        42 =>
        array(
            'COLUMN_NAME' => 'data_comanda',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'date',
            'COLUMN_KEY' => 'MUL',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        43 =>
        array(
            'COLUMN_NAME' => 'ora_comanda',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'time',
            'COLUMN_KEY' => 'MUL',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        44 =>
        array(
            'COLUMN_NAME' => 'data_servizio',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'date',
            'COLUMN_KEY' => 'MUL',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        45 =>
        array(
            'COLUMN_NAME' => 'imposta_rep_1',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        46 =>
        array(
            'COLUMN_NAME' => 'imposta_rep_2',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        47 =>
        array(
            'COLUMN_NAME' => 'imposta_rep_3',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        48 =>
        array(
            'COLUMN_NAME' => 'imposta_rep_4',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        49 =>
        array(
            'COLUMN_NAME' => 'imposta_rep_5',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        50 =>
        array(
            'COLUMN_NAME' => 'totale_rep_1',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        51 =>
        array(
            'COLUMN_NAME' => 'totale_rep_2',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        52 =>
        array(
            'COLUMN_NAME' => 'totale_rep_3',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        53 =>
        array(
            'COLUMN_NAME' => 'totale_rep_4',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        54 =>
        array(
            'COLUMN_NAME' => 'totale_rep_5',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        55 =>
        array(
            'COLUMN_NAME' => 'assegno',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        56 =>
        array(
            'COLUMN_NAME' => 'bonifico',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        57 =>
        array(
            'COLUMN_NAME' => 'ult_prog_inser',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        58 =>
        array(
            'COLUMN_NAME' => 'numero_servizio',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        59 =>
        array(
            'COLUMN_NAME' => 'imponibile_rep_1',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        60 =>
        array(
            'COLUMN_NAME' => 'imponibile_rep_2',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        61 =>
        array(
            'COLUMN_NAME' => 'imponibile_rep_3',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        62 =>
        array(
            'COLUMN_NAME' => 'imponibile_rep_4',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        63 =>
        array(
            'COLUMN_NAME' => 'imponibile_rep_5',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        64 =>
        array(
            'COLUMN_NAME' => 'percentuale_rep_1',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        65 =>
        array(
            'COLUMN_NAME' => 'percentuale_rep_2',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        66 =>
        array(
            'COLUMN_NAME' => 'percentuale_rep_3',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        67 =>
        array(
            'COLUMN_NAME' => 'percentuale_rep_4',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        68 =>
        array(
            'COLUMN_NAME' => 'percentuale_rep_5',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        69 =>
        array(
            'COLUMN_NAME' => 'carte_credito2',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        70 =>
        array(
            'COLUMN_NAME' => 'carte_credito3',
            'COLUMN_DEFAULT' => 'NULL',
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'YES',
        ),
        71 =>
        array(
            'COLUMN_NAME' => 'misuratore_riferimento',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        72 =>
        array(
            'COLUMN_NAME' => 'zrep',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        73 =>
        array(
            'COLUMN_NAME' => 'TID',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        74 =>
        array(
            'COLUMN_NAME' => 'indice_rep_1',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        75 =>
        array(
            'COLUMN_NAME' => 'indice_rep_2',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        76 =>
        array(
            'COLUMN_NAME' => 'indice_rep_3',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        77 =>
        array(
            'COLUMN_NAME' => 'indice_rep_4',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        78 =>
        array(
            'COLUMN_NAME' => 'indice_rep_5',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        79 =>
        array(
            'COLUMN_NAME' => 'non_riscosso_servizi',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        80 =>
        array(
            'COLUMN_NAME' => 'sconto_a_pagare',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        81 =>
        array(
            'COLUMN_NAME' => 'arrotondamento',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
        82 =>
        array(
            'COLUMN_NAME' => 'progr_gg_uni',
            'COLUMN_DEFAULT' => NULL,
            'COLUMN_TYPE' => 'text',
            'COLUMN_KEY' => '',
            'EXTRA' => '',
            'IS_NULLABLE' => 'NO',
        ),
    ),
);

/* echo "<br>";
  echo "<br>";
  echo "<br>";
  echo "<h1>SQLITE</h1>";
  echo "<br>";
  echo "<br>";
  echo "<br>";
 */

foreach ($campiSqlite as $nome => $colonne) {

    $query = "CREATE TABLE $nome (";

    foreach ($colonne as $indice => $colonna) {


        //array(6) { ["cid"]=> string(1) "0" ["name"]=> string(2) "id" ["type"]=> string(7) "INTEGER" ["notnull"]=> string(1) "0" ["dflt_value"]=> NULL ["pk"]=> string(1) "1" }
        //CREATE TABLE `provaintellinet`.`prova2` ( `id` INT NOT NULL AUTO_INCREMENT , PRIMARY KEY (`id`)) ENGINE = InnoDB;


        $query .= $colonna['name'] . " ";

        $query .= $colonna["type"] . " ";

        if ($colonna["notnull"] === "1") {
            $query .= "NOT NULL" . " ";
        }

        if (!is_null($colonna["dflt_value"])) {
            $query .= "DEFAULT " . $colonna["dflt_value"] . " ";
        }

        if ($colonna["pk"] === "1") {
            $query .= " PRIMARY KEY AUTOINCREMENT" . " ";
        }

        if ($indice < count($colonne) - 1) {
            $query .= ", ";
        }
    }

    $query .= ");";

    /* echo $query . "<br><br>"; */
    $risultato = eseguiQuerySqlite($query);
    if ($risultato === false) {
        echo "SQLITE_QUERY_ERROR";

        exit;
    }
}

foreach ($campiSqlite as $nome => $colonne) {



    foreach ($colonne as $indice => $colonna) {


        //array(6) { ["cid"]=> string(1) "0" ["name"]=> string(2) "id" ["type"]=> string(7) "INTEGER" ["notnull"]=> string(1) "0" ["dflt_value"]=> NULL ["pk"]=> string(1) "1" }
        //ALTER TABLE `prova` ADD `campo_testo` TEXT NOT NULL DEFAULT '\'\'' AFTER `id`;

        $query = "ALTER TABLE `" . $nome . "` ADD `" . $colonna['name'] . "` " . $colonna["type"] . " ";

        if ($colonna["notnull"] === "1") {
            $query .= "NOT NULL" . " ";
        }

        if (!is_null($colonna["dflt_value"])) {
            $query .= "DEFAULT " . $colonna["dflt_value"] . " ";
        }

        if ($indice < count($colonne) - 1) {
            $query .= "; ";
        }

        /* echo $query . "<br><br>"; */
        $risultato = eseguiQuerySqlite($query);
        if ($risultato === false) {
            echo "SQLITE_QUERY_ERROR";
            exit;
        }
    }
}

/* echo "<br>";
  echo "<br>";
  echo "<br>";
  echo "<h1>MYSQL</h1>";
  echo "<br>";
  echo "<br>";
  echo "<br>";
 */


foreach ($campiMysql as $nome => $colonne) {

    //string(11) "back_office" 
    $query = "CREATE TABLE $nome (";

    foreach ($colonne as $indice => $colonna) {

        //array(6) { ["COLUMN_NAME"]=> string(8) "prim_key" ["COLUMN_DEFAULT"]=> NULL ["COLUMN_TYPE"]=> string(7) "int(11)" ["COLUMN_KEY"]=> string(3) "PRI" ["EXTRA"]=> string(14) "auto_increment" ["IS_NULLABLE"]=> string(2) "NO" }
        //CREATE TABLE `provaintellinet`.`prova2` ( `id` INT NOT NULL AUTO_INCREMENT , PRIMARY KEY (`id`)) ENGINE = InnoDB;


        $query .= $colonna['COLUMN_NAME'] . " ";

        $query .= $colonna["COLUMN_TYPE"] . " ";

        if ($colonna["IS_NULLABLE"] === "YES") {
            $query .= "NOT NULL" . " ";
        }

        if ($colonna["EXTRA"] === "auto_increment") {
            $query .= "AUTO_INCREMENT" . " ";
        }


        if ($colonna["COLUMN_DEFAULT"] !== NULL && $colonna["COLUMN_DEFAULT"] !== "NULL") {
            $query .= "DEFAULT " . $colonna["COLUMN_DEFAULT"] . " ";
        }

        if ($colonna["COLUMN_KEY"] === "PRI") {
            $query .= " , PRIMARY KEY (`" . $colonna['COLUMN_NAME'] . "`)" . " ";
        }

        if ($indice < count($colonne) - 1) {
            $query .= ", ";
        }
    }

    $query .= ") ENGINE = InnoDB;";

    /* echo $query . "<br><br>"; */

    $risultato = eseguiQueryMysql($query);
    if ($risultato === false) {
        echo "MYSQL_QUERY_ERROR";
        exit;
    }
}

foreach ($campiMysql as $nome => $colonne) {


    foreach ($colonne as $indice => $colonna) {

        //prima devi vedere se c'è già la colonna nel db
        $query = "select COLUMN_NAME from information_schema.columns where table_schema='$nomeDBMysql' and table_name = '" . $nome . "' and COLUMN_NAME='" . $colonna['COLUMN_NAME'] . "';";

        $colonna_esistente = eseguiQueryMysql($query);

        //vuol dire che non esiste quella colonna
        if ($colonna_esistente === 1) {


            //poi fai il test

            $query = "SELECT COUNT(*) AS numero_record FROM $nome ;";

            $numero_record = eseguiQueryMysql($query);

            if ($ignora_numero_record !== "true" && $numero_record["numero_record"] >= 1000000) {

                echo "WARNING_NUMERO_RECORD";
                exit;
            }


            //poi eventualmente adegui
            //ALTER TABLE `prova` ADD `campo_testo` TEXT NOT NULL DEFAULT '\'\'';

            if ($colonna['COLUMN_NAME'] === "prim_key") {
                $query = "ALTER TABLE " . $nome . " ADD `prim_key` INT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`prim_key`);";

                $risultato = eseguiQueryMysql($query);
                if ($risultato === false) {
                    echo "MYSQL_QUERY_ERROR";
                    exit;
                }
            } else {
                $query = "ALTER TABLE `" . $nome . "` ADD `" . $colonna['COLUMN_NAME'] . "` " . $colonna["COLUMN_TYPE"] . " ";

                if ($colonna["IS_NULLABLE"] === "YES") {
                    $query .= "NOT NULL" . " ";
                }

                if ($colonna["COLUMN_DEFAULT"] !== NULL && $colonna["COLUMN_DEFAULT"] !== "NULL") {
                    $query .= "DEFAULT " . $colonna["COLUMN_DEFAULT"] . " ";
                }

                if ($indice < count($colonne) - 1) {
                    $query .= "; ";
                }

                /* echo $query . "<br><br>"; */

                $risultato = eseguiQueryMysql($query);
                if ($risultato === false) {
                    echo "MYSQL_QUERY_ERROR";
                    exit;
                }
            }
        }
    }
}

/* blocco accessorio 08/02/2021 */
$query = "ALTER TABLE `comanda_temp` CHANGE `quantita` `quantita` INT(5) NOT NULL, CHANGE `prezzo_un` `prezzo_un` DECIMAL(14,8) NOT NULL DEFAULT '0.00000000', CHANGE `netto` `netto` DECIMAL(14,8) NOT NULL, CHANGE `prezzo_varianti_aggiuntive` `prezzo_varianti_aggiuntive` DECIMAL(14,8) NOT NULL, CHANGE `prezzo_varianti_maxi` `prezzo_varianti_maxi` DECIMAL(14,8) NOT NULL, CHANGE `prezzo_maxi_prima` `prezzo_maxi_prima` DECIMAL(14,8) NOT NULL, CHANGE `prezzo_vero` `prezzo_vero` DECIMAL(14,8) NOT NULL, CHANGE `imponibile_rep_1` `imponibile_rep_1` DECIMAL(14,8) NOT NULL DEFAULT '0.00000000', CHANGE `imponibile_rep_2` `imponibile_rep_2` DECIMAL(14,8) NOT NULL DEFAULT '0.00000000', CHANGE `imponibile_rep_3` `imponibile_rep_3` DECIMAL(14,8) NOT NULL DEFAULT '0.00000000', CHANGE `imponibile_rep_4` `imponibile_rep_4` DECIMAL(14,8) NOT NULL DEFAULT '0.00000000', CHANGE `imponibile_rep_5` `imponibile_rep_5` DECIMAL(14,8) NOT NULL DEFAULT '0.00000000', CHANGE `imposta_rep_1` `imposta_rep_1` DECIMAL(14,8) NOT NULL DEFAULT '0.00000000', CHANGE `imposta_rep_2` `imposta_rep_2` DECIMAL(14,8) NOT NULL DEFAULT '0.00000000', CHANGE `imposta_rep_3` `imposta_rep_3` DECIMAL(14,8) NOT NULL DEFAULT '0.00000000', CHANGE `imposta_rep_4` `imposta_rep_4` DECIMAL(14,8) NOT NULL DEFAULT '0.00000000', CHANGE `imposta_rep_5` `imposta_rep_5` DECIMAL(14,8) NOT NULL DEFAULT '0.00000000', CHANGE `totale_scontato_ivato` `totale_scontato_ivato` DECIMAL(14,8) NOT NULL DEFAULT '0.00000000', CHANGE `prezzo_maxi` `prezzo_maxi` DECIMAL(14,8) NOT NULL DEFAULT '0.00000000';";
$risultato = eseguiQueryMysql($query);
if ($risultato === false) {
    echo "MYSQL_QUERY_ERROR";
    exit;
}

$query = "ALTER TABLE `comanda` CHANGE `quantita` `quantita` INT(5) NOT NULL, CHANGE `prezzo_un` `prezzo_un` DECIMAL(14,8) NOT NULL DEFAULT '0.00000000', CHANGE `netto` `netto` DECIMAL(14,8) NOT NULL, CHANGE `prezzo_varianti_aggiuntive` `prezzo_varianti_aggiuntive` DECIMAL(14,8) NOT NULL, CHANGE `prezzo_varianti_maxi` `prezzo_varianti_maxi` DECIMAL(14,8) NOT NULL, CHANGE `prezzo_maxi_prima` `prezzo_maxi_prima` DECIMAL(14,8) NOT NULL, CHANGE `prezzo_vero` `prezzo_vero` DECIMAL(14,8) NOT NULL, CHANGE `imponibile_rep_1` `imponibile_rep_1` DECIMAL(14,8) NOT NULL DEFAULT '0.00000000', CHANGE `imponibile_rep_2` `imponibile_rep_2` DECIMAL(14,8) NOT NULL DEFAULT '0.00000000', CHANGE `imponibile_rep_3` `imponibile_rep_3` DECIMAL(14,8) NOT NULL DEFAULT '0.00000000', CHANGE `imponibile_rep_4` `imponibile_rep_4` DECIMAL(14,8) NOT NULL DEFAULT '0.00000000', CHANGE `imponibile_rep_5` `imponibile_rep_5` DECIMAL(14,8) NOT NULL DEFAULT '0.00000000', CHANGE `imposta_rep_1` `imposta_rep_1` DECIMAL(14,8) NOT NULL DEFAULT '0.00000000', CHANGE `imposta_rep_2` `imposta_rep_2` DECIMAL(14,8) NOT NULL DEFAULT '0.00000000', CHANGE `imposta_rep_3` `imposta_rep_3` DECIMAL(14,8) NOT NULL DEFAULT '0.00000000', CHANGE `imposta_rep_4` `imposta_rep_4` DECIMAL(14,8) NOT NULL DEFAULT '0.00000000', CHANGE `imposta_rep_5` `imposta_rep_5` DECIMAL(14,8) NOT NULL DEFAULT '0.00000000', CHANGE `totale_scontato_ivato` `totale_scontato_ivato` DECIMAL(14,8) NOT NULL DEFAULT '0.00000000', CHANGE `prezzo_maxi` `prezzo_maxi` DECIMAL(14,8) NOT NULL DEFAULT '0.00000000';";
$risultato = eseguiQueryMysql($query);
if ($risultato === false) {
    echo "MYSQL_QUERY_ERROR";
    exit;
}

echo "true";
