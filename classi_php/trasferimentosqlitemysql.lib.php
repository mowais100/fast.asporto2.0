<?php

ini_set('max_execution_time', 300);
set_time_limit(300);

error_reporting(E_ALL);

$data = date("Ymd");

$fileName = "blocco_esportazione_$data.csv";

if (file_exists($fileName)) {
    echo "true";
} else {
    /* file che blocca l'esportazione dopo la prima */

    $file_content = "B";
    file_put_contents($fileName, $file_content);

    $percorso_main = "C:/TAVOLI/fast.intellinet/";

    $mysqli = new mysqli("localhost", "root", "", "provaINTELLINET");

    //LETTURA COMANDA

    $db = new PDO("sqlite:../DATABASE_CLIENTE.sqlite");

    $queryVERIFICAcomanda1 = $db->query("select sum(prog_inser) as somma from comanda where substr(id,5,8)='" . ($data - 1) . "';");
    $Vc1 = $queryVERIFICAcomanda1->fetch(PDO::FETCH_ASSOC);
    $Vc1_r = $Vc1["somma"];

    $query1 = $db->query("select * from comanda where substr(id,5,8)<'$data';");

    $file_content = "";

    while ($row = $query1->fetch(PDO::FETCH_ASSOC)) {

        $arr_len = count($row);
        $i = 1;

        foreach ($row as $key => $value) {
            $file_content .= '"' . $value . '"';
            if ($i < $arr_len) {
                $file_content .= ',';
            }
            $i++;
        }

        $file_content .= "\n";
    }

    $fileName = "file_temporaneo" . time() . ".csv";

    file_put_contents($fileName, $file_content);

    //IMPORTAZIONE COMANDA MYSQL
    $query = "SET sql_mode = '';";

    $mysqli->query($query);

    $query = "LOAD DATA INFILE 'C:/TAVOLI/fast.intellinet/classi_php/$fileName' INTO TABLE comanda FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\n';";

    $mysqli->query($query);

    $queryVERIFICAcomanda2 = $mysqli->query("select sum(prog_inser) as somma from comanda where substr(id,5,8)='" . ($data - 1) . "';");
    $Vc2 = $queryVERIFICAcomanda2->fetch_array(MYSQLI_ASSOC);
    $Vc2_r = $Vc2["somma"];

    //LETTURA TESTE
    $queryVERIFICAtesta1 = $db->query("select sum(numero_fiscale) as somma from record_teste where substr(progressivo_comanda,5,8)='" . ($data - 1) . "';");
    $Vt1 = $queryVERIFICAtesta1->fetch(PDO::FETCH_ASSOC);
    $Vt1_r = $Vt1["somma"];

    $query1 = $db->query("select * from record_teste where substr(progressivo_comanda,5,8)<'$data';");

    $file_content = "";

    while ($row = $query1->fetch(PDO::FETCH_ASSOC)) {

        $arr_len = count($row);
        $i = 1;

        foreach ($row as $key => $value) {
            $file_content .= '"' . $value . '"';
            if ($i < $arr_len) {
                $file_content .= ',';
            }
            $i++;
        }

        $file_content .= "\n";
    }

    $fileName = "file_teste" . time() . ".csv";

    file_put_contents($fileName, $file_content);

    //IMPORTAZIONE TESTE MYSQL
    $query = "SET sql_mode = '';";

    $mysqli->query($query);

    $query = "LOAD DATA INFILE '{$percorso_main}classi_php/$fileName' INTO TABLE record_teste FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\n';";

    $mysqli->query($query);

    $queryVERIFICAtesta2 = $mysqli->query("select sum(numero_fiscale) as somma from record_teste where substr(progressivo_comanda,5,8)='" . ($data - 1) . "';");
    $Vt2 = $queryVERIFICAtesta2->fetch_array(MYSQLI_ASSOC);
    $Vt2_r = $Vt2["somma"];

    /* Somma corpi e teste sqlite e mysql */

    $file_content = "Sqlite\n\nSomma corpi: " . $Vc1_r . "\n\nSomma teste: " . $Vt1_r . "\n\n\nMySql\n\nSomma corpi: " . $Vc2_r . "\n\nSomma teste: " . $Vt2_r;

    $fileName = "file_somma" . time() . ".txt";

    file_put_contents($fileName, $file_content);

    /* CANCELLA IL DB CENTRALE ALLA FINE */

    $db->query("delete from comanda where substr(id,5,8)<'$data';");

    $db->query("delete from record_teste where substr(progressivo_comanda,5,8)<'$data';");

    //LETTURA ULTIMO ID BACKOFFICE
    //al posto di unsigned va int se è mariadb
    $queryUltimoIdBackOffice = $mysqli->query("select substr(id,5,8) as ultimo_id from back_office order by cast(substr(id,5,8) as UNSIGNED) desc limit 1;");

    if ($queryUltimoIdBackOffice->num_rows > 0) {
        $VUltimoIdBackOffice = $queryUltimoIdBackOffice->fetch_array(MYSQLI_ASSOC);
        $UltimoIdBackOffice = $VUltimoIdBackOffice["ultimo_id"];
    } else {
        $UltimoIdBackOffice = "0";
    }

    $query = "INSERT LOW_PRIORITY INTO back_office 

                (id,tipologia_fattorino,gruppo,giorno,fiscalizzata_sn,tipo_ricevuta,n_fiscale,operatore,ntav_comanda,quantita,desc_art,sconto_imp,sconto_perc,perc_iva,categoria,
                dest_stampa,portata, netto,prezzo_un,prezzo_vero,QTA,tipo_consegna,cod_articolo,data_comanda,ora_comanda,cod_promo,posizione,totale,costo,totale_costo,totale_ricavo,importo_totale_fiscale)

                SELECT id,tipologia_fattorino,gruppo,giorno,fiscalizzata_sn,tipo_ricevuta,n_fiscale,operatore,ntav_comanda,sum(quantita),desc_art,sconto_imp,sconto_perc,perc_iva,categoria,
                dest_stampa,portata, netto, prezzo_un,prezzo_vero,QTA,tipo_consegna,cod_articolo,data_comanda,ora_comanda,cod_promo,posizione,(prezzo_un*sum(quantita)) as totale,costo,(costo*sum(quantita)) 
                as totale_costo,(prezzo_un*sum(quantita))-(costo*sum(quantita))  as totale_ricavo,importo_totale_fiscale*sum(quantita) as importo_totale_fiscale

                FROM comanda WHERE substr(id,5,8)>'$UltimoIdBackOffice' and QTA='00000' and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and (fiscalizzata_sn='S' OR tipo_ricevuta='conto' OR tipo_ricevuta='STORICIZZAZIONE DI PROVA') and tipo_ricevuta!='' group by id asc,desc_art asc,prezzo_un asc,sconto_perc asc;";

    $mysqli->query($query);

    $query = "INSERT LOW_PRIORITY INTO back_office 

                (id,tipologia_fattorino,gruppo,giorno,fiscalizzata_sn,tipo_ricevuta,n_fiscale,operatore,ntav_comanda,quantita,desc_art,sconto_imp,sconto_perc,perc_iva,categoria,
                dest_stampa,portata, netto,prezzo_un,prezzo_vero,QTA,tipo_consegna,cod_articolo,data_comanda,ora_comanda,cod_promo,posizione,totale,costo,totale_costo,totale_ricavo,importo_totale_fiscale)

                SELECT id,tipologia_fattorino,gruppo,giorno,fiscalizzata_sn,tipo_ricevuta,n_fiscale,operatore,ntav_comanda,sum(quantita),desc_art,sconto_imp,sconto_perc,perc_iva,categoria,
                dest_stampa,portata, netto, prezzo_un,prezzo_vero,QTA,tipo_consegna,cod_articolo,data_comanda,ora_comanda,cod_promo,posizione,(prezzo_un*sum(quantita)) as totale,costo,(costo*sum(quantita)) 
                as totale_costo,(prezzo_un*sum(quantita))-(costo*sum(quantita))  as totale_ricavo,importo_totale_fiscale*sum(quantita) as importo_totale_fiscale
                
                FROM comanda WHERE substr(id,5,8)>'$UltimoIdBackOffice' and  QTA!='00000' and (tipo_ricevuta='scontrino' or tipo_ricevuta='fattura' or stato_record='CHIUSO' or stato_record='RAPPORTOZ') and (fiscalizzata_sn='S' OR tipo_ricevuta='conto' OR tipo_ricevuta='STORICIZZAZIONE DI PROVA') and tipo_ricevuta!='' group by id asc,desc_art asc,prezzo_un asc,sconto_perc asc;";

    $mysqli->query($query);

    if ($Vc1_r == $Vc2_r && $Vt1_r == $Vt2_r) {
        echo "true";
    } else {
        echo "false";
    }
}
