<?php

$mysqli = new mysqli("localhost", "root", "", "provaINTELLINET");

$db = new PDO("sqlite:../DATABASE_CLIENTE.sqlite");

$oggettoTabellaSqlite = [];

$resTables = $db->query("SELECT * FROM sqlite_master WHERE type='table';");

while ($table = $resTables->fetch(PDO::FETCH_ASSOC)) {

    if ($table['tbl_name'] !== "sqlite_sequence") {

        $oggettoTabellaSqlite[$table['tbl_name']] = [];

        $resColumns = $db->query("PRAGMA table_info(" . $table['tbl_name'] . ");");

        while ($column = $resColumns->fetch(PDO::FETCH_ASSOC)) {

            $oggettoTabellaSqlite[$table['tbl_name']][] = $column;
        }
    }
}

echo "<pre>";
//echo var_export($oggettoTabellaSqlite);
echo "</pre>";

echo "<br>";
echo "<br>";
echo "<br>";


$oggettoTabellaMysql = [];

$resTablesMySql = $mysqli->query("SELECT table_name FROM information_schema.tables WHERE table_schema='provaINTELLINET';");

while ($table = $resTablesMySql->fetch_array(MYSQLI_ASSOC)) {

    $resColumnsMySql = $mysqli->query("select COLUMN_NAME,COLUMN_DEFAULT,COLUMN_TYPE,COLUMN_KEY,EXTRA,IS_NULLABLE from information_schema.columns where table_schema='provaINTELLINET' and table_name = '" . $table['table_name'] . "';");

    $oggettoTabellaMysql[$table['table_name']] = [];

    while ($column = $resColumnsMySql->fetch_array(MYSQLI_ASSOC)) {

        $oggettoTabellaMysql[$table['table_name']][] = $column;
    }
}

echo "<pre>";
echo var_export($oggettoTabellaMysql);
echo "</pre>";

//es creazione tabella
//CREATE TABLE `provaintellinet`.`prova2` ( `id` INT NOT NULL AUTO_INCREMENT , PRIMARY KEY (`id`)) ENGINE = InnoDB;
//ALTER TABLE `prova` ADD `campo_testo` TEXT NOT NULL DEFAULT '\'\'' AFTER `id`;

