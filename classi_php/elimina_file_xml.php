<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);

$ora_servizio = $_POST['ora_servizio'];
$directory_dati = $_POST['directory_dati'];

if (date('H') < $ora_servizio) {
    $data = date('Y-m-d', strtotime("now-1 day"));
} else {
    $data = date("Y-m-d");
}

$folder_prefix = "C:/TAVOLI/" . $directory_dati . "/" . $data;

$nome = $_POST['nome'];
$cancellazione_ampia = $_POST['cancellazione_ampia'];

$file = $nome;

//var_dump($cancellazione_ampia);
//NB: I booleani passati da Js a Php diventano stringhe
if ($cancellazione_ampia !== "true") {
    //echo "Removing " . $folder_prefix . $file;
    if (file_exists($folder_prefix . $file)) {
        unlink($folder_prefix . $file);
    }
} else {
    //echo "Removing " . $folder_prefix . $file . "*";
    array_map('unlink', glob($folder_prefix . $file . "*"));
}

echo "1";
