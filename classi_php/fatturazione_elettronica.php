<?php

/* Per fare andare la fattura elettronica abilitare extension=php_pdo_odbc.dll in php.ini */

error_reporting(E_ALL);

/* Strutture dati */

class Testa {

    public $IDTESTA; /*  */
    public $C1111; /* Id Paese + */
    public $C1112; /* Id Codice + */
    public $C112; /* Progressivo Invio + */
    public $C113; /* Formato Trasmissione + */
    public $C114; /* Codice Destinatario + */
    public $C116; /* PEC Destinatario + */
    public $C12111; /* Id Paese + */
    public $C12112; /* Id Codice + */
    public $C1212; /* Codice Fiscale + */
    public $C12131; /* Denominazione + */
    public $C1218; /* Regime Fiscale + */
    public $C1221; /* Indirizzo + */
    public $C1223; /* CAP + */
    public $C1224; /* Comune + */
    public $C1225; /* Provincia + */
    public $C1226; /* Nazione + */
    public $C14111; /* Id Paese + */
    public $C14112; /* Id Codice + */
    public $C1412; /* Codice Fiscale + */
    public $C14131; /* Denominazione + */
    public $C1421; /* Indirizzo + */
    public $C1423; /* CAP + */
    public $C1424; /* Comune + */
    public $C1425; /* Provincia + */
    public $C1426; /* Nazione + */
    public $C15111; /* Id Paese */
    public $C15112; /* Id Codice */
    public $C15131; /* Denominazione */
    public $C2111; /* Tipo Documento + */
    public $C2112; /* Divisa + */
    public $C2113; /* Data + */
    public $C2114; /* Numero + */
    public $C21151; /* Tipo Ritenuta */
    public $C21152; /* Importo Ritenuta */
    public $C21153; /* Aliquota Ritenuta */
    public $C21154; /* Causale Pagamento */
    public $C21181; /* Tipo */
    public $C21182; /* Percentuale */
    public $C21183; /* Importo */
    public $C2119; /* Importo Totale Documento + */
    public $C21111; /* Causale */
    public $SPED; /*  */
    public $DATAS; /*  */
    public $INDIRIZZO; /*  */

}

class Corpo {

    public $IDTESTA; /* Id Testa */
    public $C2211; /* Numero Linea + */
    public $C2212; /* Tipo Cessione Prestazione */
    public $C22131; /* Codice Tipo + */
    public $C22132; /* Codice Valore + */
    public $C2214; /* Descrizione + */
    public $C2215; /* Quantità + */
    public $C2216; /* Unità Misura + */
    public $C2219; /* Prezzo Unitario + */
    public $C221101; /* Tipo (Sconto-Magg-iorazione) */
    public $C221103; /* Importo Sconto */
    public $C22111; /* Prezzo Totale + */
    public $C22112; /* Aliquota IVA + */

}

class Totali {

    public $IDTESTA; /* Id Testa */
    public $C2221; /* Aliquota IVA + */
    public $C2222; /* Natura */
    public $C2225; /* Imponibile Importo + */
    public $C2226; /* Imposta + */
    public $C2227; /* Esigibilità IVA */

}

class Pagamento {

    public $IDTESTA; /* Id Testa */
    public $C241; /* Condizioni Pagamento */
    public $C2422; /* Modalità Pagamento */
    public $C2426; /* Importo Pagamento */

}

/* Dichiarazione dati POST */

/* Dichiarazione Array */

$testa = $_POST['testa'][0];
$corpo = $_POST['corpo'];

$totali = $_POST['totali'];
$pagamento = $_POST['pagamento'];

$xPI = "";
$xNDoc = "";

$obj0 = new DOMDocument('1.0', 'UTF-8');

$objLiv0 = $obj0->createElement("p:FatturaElettronica");

$objLiv0->SetAttribute("versione", "FPR12");
$objLiv0->SetAttribute("xmlns:ds", "http://www.w3.org/2000/09/xmldsig#");
$objLiv0->SetAttribute("xmlns:p", "http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fatture/v1.2");
$objLiv0->SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
$objLiv0->SetAttribute("xsi:schemaLocation", "http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fatture/v1.2 http://www.fatturapa.gov.it/export/fatturazione/sdi/fatturapa/v1.2/Schema_del_file_xml_FatturaPA_versione_1.2.xsd");

$objLiv0 = $obj0->appendChild($objLiv0);

$value = $testa;

$objLiv1 = $obj0->createElement("FatturaElettronicaHeader");
$objLiv0->appendChild($objLiv1);
$objLiv1->setAttribute("xmlns", "");

$objLiv11 = $obj0->createElement("DatiTrasmissione");
$objLiv1->appendChild($objLiv11);

$objLiv111 = $obj0->createElement("IdTrasmittente");
$objLiv11->appendChild($objLiv111);
$objV = $obj0->createElement("IdPaese");
$objLiv111->appendChild($objV);
$objV->textContent = strtoupper($value["C1111"]);
$objV = $obj0->createElement("IdCodice");
$objLiv111->appendChild($objV);
$objV->textContent = $value["C1112"];

$objLiv111 = $obj0->createElement("ProgressivoInvio");
$objLiv11->appendChild($objLiv111);
$objLiv111->textContent = sprintf('%05d', $value["C2114"]);
$objLiv111 = $obj0->createElement("FormatoTrasmissione");
$objLiv11->appendChild($objLiv111);
$objLiv111->textContent = $value["C113"];
$objLiv111 = $obj0->createElement("CodiceDestinatario");
$objLiv11->appendChild($objLiv111);
$objLiv111->textContent = $value["C114"];

if (strlen($value["C116"]) > 0) {
    $objLiv111 = $obj0->createElement("PECDestinatario");
    $objLiv11->appendChild($objLiv111);
    $objLiv111->textContent = $value["C116"];
}



/* --- 1.2 --- */
$objLiv10 = $obj0->createElement("CedentePrestatore");
$objLiv1->appendChild($objLiv10);
/* --- 1.2.1 --- */
$objLiv11 = $obj0->createElement("DatiAnagrafici");
$objLiv10->appendChild($objLiv11);
$objLiv111 = $obj0->createElement("IdFiscaleIVA");
$objLiv11->appendChild($objLiv111);
$objV = $obj0->createElement("IdPaese");
$objLiv111->appendChild($objV);
$objV->textContent = strtoupper($value["C12111"]);

$objV = $obj0->createElement("IdCodice");
$objLiv111->appendChild($objV);
$objV->textContent = $value["C12112"];

$objLiv111 = $obj0->createElement("Anagrafica");
$objLiv11->appendChild($objLiv111);
$objV = $obj0->createElement("Denominazione");
$objLiv111->appendChild($objV);
$objV->textContent = $value["C12131"];

if (strlen($value["C1212"]) > 0) {
    $objLiv111 = $obj0->createElement("CodiceFiscale");
    $objLiv11->appendChild($objLiv111);
    $objLiv111->textContent = $value["C1212"];
}

$objLiv111 = $obj0->createElement("RegimeFiscale");
$objLiv11->appendChild($objLiv111);
$objLiv111->textContent = $value["C1218"];
/* --- 1.2.2 --- */
$objLiv11 = $obj0->createElement("Sede");
$objLiv10->appendChild($objLiv11);
$objLiv111 = $obj0->createElement("Indirizzo");
$objLiv11->appendChild($objLiv111);
$objLiv111->textContent = $value["C1221"];
$objLiv111 = $obj0->createElement("CAP");
$objLiv11->appendChild($objLiv111);
$objLiv111->textContent = $value["C1223"];

$objLiv111 = $obj0->createElement("Comune");
$objLiv11->appendChild($objLiv111);
$objLiv111->textContent = $value["C1224"];

if (strlen($value["C1225"]) > 0) {
    $objLiv111 = $obj0->createElement("Provincia");
    $objLiv11->appendChild($objLiv111);
    $objLiv111->textContent = strtoupper($value["C1225"]);
}

$objLiv111 = $obj0->createElement("Nazione");
$objLiv11->appendChild($objLiv111);
$objLiv111->textContent = strtoupper($value["C1226"]);

/* --- 1.4 --- */
$objLiv10 = $obj0->createElement("CessionarioCommittente");
$objLiv1->appendChild($objLiv10);
/* ' --- 1.4.1 --- */
$objLiv11 = $obj0->createElement("DatiAnagrafici");
$objLiv10->appendChild($objLiv11);
$objLiv111 = $obj0->createElement("IdFiscaleIVA");
$objLiv11->appendChild($objLiv111);
$objV = $obj0->createElement("IdPaese");
$objLiv111->appendChild($objV);
$objV->textContent = strtoupper($value["C14111"]);

if (strlen($value["C14112"]) > 2) {
    $objV = $obj0->createElement("IdCodice");
    $objLiv111->appendChild($objV);
    $objV->textContent = $value["C14112"];
}


if (strlen($value["C1412"]) > 0) {
    $objV = $obj0->createElement("CodiceFiscale");
    $objLiv11->appendChild($objV);
    $objV->textContent = $value["C1412"];
}

$objLiv111 = $obj0->createElement("Anagrafica");
$objLiv11->appendChild($objLiv111);

$objV = $obj0->createElement("Denominazione");
$objLiv111->appendChild($objV);
$objV->textContent = $value["C14131"];
/* --- 1.4.2 --- */
$objLiv11 = $obj0->createElement("Sede");
$objLiv10->appendChild($objLiv11);
$objLiv111 = $obj0->createElement("Indirizzo");
$objLiv11->appendChild($objLiv111);
$objLiv111->textContent = $value["C1421"];
$objLiv111 = $obj0->createElement("CAP");
$objLiv11->appendChild($objLiv111);
$objLiv111->textContent = $value["C1423"];

$objLiv111 = $obj0->createElement("Comune");
$objLiv11->appendChild($objLiv111);
$objLiv111->textContent = $value["C1424"];

if (strlen($value["C1425"]) > 0) {
    $objLiv111 = $obj0->createElement("Provincia");
    $objLiv11->appendChild($objLiv111);
    $objLiv111->textContent = strtoupper($value["C1425"]);
}

$objLiv111 = $obj0->createElement("Nazione");
$objLiv11->appendChild($objLiv111);
$objLiv111->textContent = strtoupper($value["C1426"]);

/* --- 2 --- */
$objLiv1 = $obj0->createElement("FatturaElettronicaBody");
$objLiv0->appendChild($objLiv1);
/* --- 2.1 --- */
$objLiv11 = $obj0->createElement("DatiGenerali");
$objLiv1->appendChild($objLiv11);
/* --- 2.1.1 --- */
$objLiv111 = $obj0->createElement("DatiGeneraliDocumento");
$objLiv11->appendChild($objLiv111);
$objV = $obj0->createElement("TipoDocumento");
$objLiv111->appendChild($objV);
$objV->textContent = $value["C2111"];
$objV = $obj0->createElement("Divisa");
$objLiv111->appendChild($objV);
$objV->textContent = $value["C2112"];
$objV = $obj0->createElement("Data");
$objLiv111->appendChild($objV);
$objV->textContent = $value["C2113"];
$xDDoc = $value["C2113"];
$objV = $obj0->createElement("Numero");
$objLiv111->appendChild($objV);
$objV->textContent = $_POST['sezionale'] . "/001/" . sprintf('%06d', $value["C2114"]);
$xNDoc = strtoupper($value["C2114"]);

if ($value["C21181"] == "SC") {
    $objSC = $obj0->createElement("ScontoMaggiorazione");
    $objSC1 = $objLiv111->appendChild($objSC);
    
    $objSC11 = $obj0->createElement("Tipo");
    $objSC1->appendChild($objSC11);
    $objSC11->textContent = $value["C21181"];

    if (!empty($value["C21182"])) {
        $objSC12 = $obj0->createElement("Percentuale");
        $objSC1->appendChild($objSC12);
        $objSC12->textContent = $value["C21182"];
    }

    if (!empty($value["C21183"])) {
        $objSC13 = $obj0->createElement("Importo");
        $objSC1->appendChild($objSC13);
        $objSC13->textContent = $value["C21183"];
    }
}






$objV = $obj0->createElement("ImportoTotaleDocumento");
$objLiv111->appendChild($objV);
$objV->textContent = $value["C2119"];
$objV = $obj0->createElement("Causale");
$objLiv111->appendChild($objV);
$objV->textContent = $value["C21111"];

/* --- 2.1.6 --- */
if ($value["C2162"] != "" && $value["C2163"] != "") {

    $objLiv111 = $obj0->createElement("DatiFattureCollegate");
    $objLiv11->appendChild($objLiv111);
    $objV = $obj0->createElement("IdDocumento");
    $objLiv111->appendChild($objV);
    $objV->textContent = $value["C2162"];
    $objV = $obj0->createElement("Data");
    $objLiv111->appendChild($objV);
    $objV->textContent = $value["C2163"];
}



$myC = $corpo;

if ($myC != false) {
    $objLiv11 = $obj0->createElement("DatiBeniServizi");
    $objLiv1->appendChild($objLiv11);

    foreach ($myC as $key1 => $value1) {


        $objLiv111 = $obj0->createElement("DettaglioLinee");
        $objLiv11->appendChild($objLiv111);
        $objV = $obj0->createElement("NumeroLinea");
        $objLiv111->appendChild($objV);
        $objV->textContent = $value1["C2211"];
        $objV = $obj0->createElement("Descrizione");
        $objLiv111->appendChild($objV);
        $objV->textContent = $value1["C2214"];
        $objV = $obj0->createElement("Quantita");
        $objLiv111->appendChild($objV);
        $objV->textContent = $value1["C2215"];
        $objV = $obj0->createElement("UnitaMisura");
        $objLiv111->appendChild($objV);
        $objV->textContent = $value1["C2216"];
        $objV = $obj0->createElement("PrezzoUnitario");
        $objLiv111->appendChild($objV);
        $objV->textContent = $value1["C2219"];

        /* if ($value["C21181"] == "SC") {

          $objV = $obj0->createElement("ScontoMaggiorazione");
          $objSCL = $objLiv111->appendChild($objV);

          $objSCL1 = $obj0->createElement("Tipo");
          $objSCL->appendChild($objSCL1);
          $objSCL1->textContent = "SC";

          $objSCL2 = $obj0->createElement("Percentuale");
          $objSCL->appendChild($objSCL2);
          $objSCL2->textContent = $value["C21182"];

          $objSCL3 = $obj0->createElement("Importo");
          $objSCL->appendChild($objSCL3);
          $sconto_riga = number_format((float) ($value1["C2219"] / 100 * $value["C21182"]), 2, '.', '');
          $objSCL3->textContent = $sconto_riga;
          } */


        $objV = $obj0->createElement("PrezzoTotale");
        $objLiv111->appendChild($objV);
        $objV->textContent = $value1["C22111"];
        $objV = $obj0->createElement("AliquotaIVA");
        $objLiv111->appendChild($objV);
        $objV->textContent = $value1["C22112"];
    }
}


$myT = $totali;

If ($myT != false) {
    foreach ($myT as $key2 => $value2) {

        /* --- 2.2.2 --- */
        $objLiv111 = $obj0->createElement("DatiRiepilogo");
        $objLiv11->appendChild($objLiv111);
        $objV = $obj0->createElement("AliquotaIVA");
        $objLiv111->appendChild($objV);
        $objV->textContent = $value2["C2221"];

        if (strlen($value2["C2222"]) > 0) {
            $objV = $obj0->createElement("Natura");
            $objLiv111->appendChild($objV);
            $objV->textContent = $value2["C2222"];
        }

        $objV = $obj0->createElement("ImponibileImporto");
        $objLiv111->appendChild($objV);
        $objV->textContent = $value2["C2225"];
        $objV = $obj0->createElement("Imposta");
        $objLiv111->appendChild($objV);
        $objV->textContent = $value2["C2226"];
        $objV = $obj0->createElement("EsigibilitaIVA");
        $objLiv111->appendChild($objV);
        $objV->textContent = $value2["C2227"];
    }
}



$myP = $pagamento;

If ($myP != false) {
    $objLiv11 = $obj0->createElement("DatiPagamento");

    $objLiv1->appendChild($objLiv11);

    foreach ($myP as $key3 => $value3) {

        /* --- 2.2.2 --- */
        $objLiv111 = $obj0->createElement("CondizioniPagamento");
        $objLiv11->appendChild($objLiv111);
        $objLiv111->textContent = $value3["C241"];
        $objLiv1111 = $obj0->createElement("DettaglioPagamento");
        $objLiv11->appendChild($objLiv1111);
        $objV = $obj0->createElement("ModalitaPagamento");
        $objLiv1111->appendChild($objV);
        $objV->textContent = $value3["C2422"];
        $objV = $obj0->createElement("ImportoPagamento");
        $objLiv1111->appendChild($objV);
        $objV->textContent = $value3["C2426"];
    }
}


if ($value["C14111"] === "IT") {
    if (strlen($value["C14112"]) === 11) {
        //P.IVA
        $xPI = strtoupper($value["C14112"]);
    } else {
        //CF
        $xPI = strtoupper($value["C1412"]);
    }
} else {
    if (strlen($value["C14112"]) > 2) {
        //P.IVA o Ragione Sociale
        $xPI = substr(strtoupper($value["C14112"]), 0, 28);
    } else {
        //CF
        $xPI = strtoupper($value["C1412"]);
    }
}




//Nazione + Xpi che sarebbe partita iva o codice fiscale o rag.soc
$xPI = strtoupper($value["C14111"] . $xPI);

$nomefile = "C:/TAVOLI/ESP/FattureElettroniche/" . $xPI . "_" . sprintf('%05d', $xNDoc) . ".XML";

$obj0->save($nomefile);

echo "true";
