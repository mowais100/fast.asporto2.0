<?php

//NON SI VEDONO ERRORI
error_reporting(0);

$db = new SQLite3('../DATABASE_CLIENTE.sqlite');

$folder_number = $_GET['folder_number'];

$array_queries = array();
$ora_servizio = $_POST['ora_servizio'];

if (date('H') < $ora_servizio) {
    $data = date('Y-m-d', strtotime("now-1 day"));
} else {
    $data = date("Y-m-d");
}

//PREFISSO CARTELLA
$folder_prefix = "C:/TAVOLI/DATI/" . $data . "_" . $folder_number . "/";

//SCANSIONE CARTELLAPLANING
$output = scandir($folder_prefix);

$array_tavoli = array();

foreach ($output as $key => $value) {

    if (substr($value, -4) === '.XML' && substr($value, 6, 3) !== '990' && substr($value, 6, 3) !== '991' && substr($value, 0, 6) === 'TAVOLO') {

        $file = file_get_contents($folder_prefix . 'TAVOLO' . substr($value, 6, 3) . '.XML');

        if (strrpos($file, ">C<") !== FALSE) {

            array_push($array_tavoli, substr($value, 6, 3));
            
        }
    }
}

echo json_encode($array_tavoli);
