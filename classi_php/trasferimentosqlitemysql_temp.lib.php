<?php

$percorso_main = "C:/TAVOLI/fast.intellinet/";

$data = date("Ymd");

//ESPORTAZIONE


@$url_db_replica = json_decode($_POST["url_db_replica"]);
@$secondi_attesa_replica_mysql = $_POST["secondi_attesa_replica_mysql"];
@$download_local_db = $_POST["download_local_db"];

if (is_array($url_db_replica) && count($url_db_replica) > 0) {

    if ($download_local_db === "true") {
        foreach ($url_db_replica as $i => $url) {

            $mysqli = new mysqli("localhost", "root", "", $url->nome_db);

            $mysqli->query("delete from comanda_temp;");

            $mysqli->query("delete from record_teste_temp;");

            /* COMPRESSIONE E SCARICAMENTO DEL FILE REMOTO */

            $percorso_db_replicati = $_SERVER['DOCUMENT_ROOT'] . '/replica/';

            $source = $url->url_db_sqlite . 'DATABASE_CLIENTE.zip';

            $destination1 = $percorso_db_replicati . 'DATABASE_CLIENTE.zip';
            $destination1_2 = $percorso_db_replicati . 'DATABASE_CLIENTE.sqlite';

            $xml = file_get_contents($url->url_db_sqlite . "classi_php/compressore.php");

            copy($source, $destination1);

            $zip = new ZipArchive;

            if ($zip->open($destination1) === TRUE) {

                $zip->extractTo($percorso_db_replicati);

                $zip->close();
            }







            /**/



            $db = new PDO("sqlite:" . $destination1_2);

            $query1 = $db->query("select * from comanda;");

            $file_content = "";

            while ($row = $query1->fetch(PDO::FETCH_ASSOC)) {

                $arr_len = count($row);
                $i = 1;

                foreach ($row as $key => $value) {
                    $file_content .= '"' . $value . '"';
                    if ($i < $arr_len) {
                        $file_content .= ',';
                    }
                    $i++;
                }

                $file_content .= "\n";
            }

            $fileName = "TEMP_file_temporaneo" . time() . ".csv";

            file_put_contents($fileName, $file_content);

            $query = "SET sql_mode = '';";

            $mysqli->query($query);

//IMPORTAZIONE
            $query = "LOAD DATA INFILE '{$percorso_main}classi_php/$fileName' INTO TABLE comanda_temp FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\n'";

            $mysqli->query($query);

            $query1 = $db->query("select * from record_teste;");

            $file_content = "";

            while ($row = $query1->fetch(PDO::FETCH_ASSOC)) {

                $arr_len = count($row);
                $i = 1;

                foreach ($row as $key => $value) {
                    $file_content .= '"' . $value . '"';
                    if ($i < $arr_len) {
                        $file_content .= ',';
                    }
                    $i++;
                }

                $file_content .= "\n";
            }

            $fileName = "TEMP_file_teste" . time() . ".csv";

            file_put_contents($fileName, $file_content);

            $query = "SET sql_mode = '';";

            $mysqli->query($query);

//IMPORTAZIONE
            $query = "LOAD DATA INFILE '{$percorso_main}classi_php/$fileName' INTO TABLE record_teste_temp FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\n'";

            $mysqli->query($query);
        }

        if (is_numeric($secondi_attesa_replica_mysql)) {
            sleep($secondi_attesa_replica_mysql);
        }
    }
} else {

    $mysqli = new mysqli("localhost", "root", "", "provaINTELLINET");

    $mysqli->query("delete from comanda_temp;");

    $mysqli->query("delete from record_teste_temp;");

    $db = new PDO("sqlite:../DATABASE_CLIENTE.sqlite");

    $query1 = $db->query("select * from comanda;");

    $file_content = "";

    while ($row = $query1->fetch(PDO::FETCH_ASSOC)) {

        $arr_len = count($row);
        $i = 1;

        foreach ($row as $key => $value) {
            $file_content .= '"' . $value . '"';
            if ($i < $arr_len) {
                $file_content .= ',';
            }
            $i++;
        }

        $file_content .= "\n";
    }

    $fileName = "TEMP_file_temporaneo" . time() . ".csv";

    file_put_contents($fileName, $file_content);

    $query = "SET sql_mode = '';";

    $mysqli->query($query);

//IMPORTAZIONE
    $query = "LOAD DATA INFILE '{$percorso_main}classi_php/$fileName' INTO TABLE comanda_temp FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\n'";

    $mysqli->query($query);

    $query1 = $db->query("select * from record_teste;");

    $file_content = "";

    while ($row = $query1->fetch(PDO::FETCH_ASSOC)) {

        $arr_len = count($row);
        $i = 1;

        foreach ($row as $key => $value) {
            $file_content .= '"' . $value . '"';
            if ($i < $arr_len) {
                $file_content .= ',';
            }
            $i++;
        }

        $file_content .= "\n";
    }

    $fileName = "TEMP_file_teste" . time() . ".csv";

    file_put_contents($fileName, $file_content);

    $query = "SET sql_mode = '';";

    $mysqli->query($query);

//IMPORTAZIONE
    $query = "LOAD DATA INFILE '{$percorso_main}classi_php/$fileName' INTO TABLE record_teste_temp FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\n'";

    $mysqli->query($query);

    if (is_numeric($secondi_attesa_replica_mysql)) {
        sleep($secondi_attesa_replica_mysql);
    }
}

echo "true";
