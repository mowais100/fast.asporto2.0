<?php

error_reporting(0);

$key = 'bRuD5WYw5wd0rdHR9yLlM6wt2vteuiniQBqE70nAuhU=';

function my_encrypt($data, $key)
{
    $encryption_key = base64_decode($key);
    $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
    $encrypted = openssl_encrypt($data, 'aes-256-cbc', $encryption_key, 0, $iv);
    return base64_encode($encrypted . '::' . $iv);
}

function my_decrypt($data, $key)
{
    $encryption_key = base64_decode($key);
    list($encrypted_data, $iv) = explode('::', base64_decode($data), 2);
    return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
}



for ($i = 0; $i < 100; $i++) {
    /* cripta */
    if (file_exists('../DATABASE_SETTAGGI.sqlite')) {
        $code = file_get_contents('../DATABASE_SETTAGGI.sqlite'); //Get the code to be encypted.
    } else {
        echo "database settaggi non esiste. non posso leggerlo " . $i;
        break;
        exit;
    }

    if (empty($code)) {
        echo "codice vuoto " . $i;
        break;
        exit;
    }
    try {
        $db = new SQLite3('../DATABASE_SETTAGGI.sqlite');
        $db->close();
    } catch (Exception $e) {
        echo "database settaggi non apribile " . $i;
        break;
        exit;
    }

    $encrypted_code = my_encrypt($code, $key); //Encrypt the code.

    //if (!file_exists('../DATABASE_SETTAGGI_ENCRYPTED.sqlite')) {
    file_put_contents('../DATABASE_SETTAGGI_ENCRYPTED.sqlite', $encrypted_code);

    if (file_exists('../DATABASE_SETTAGGI.sqlite')) {
        unlink('../DATABASE_SETTAGGI.sqlite');
    } else {
        echo "database settaggi non esiste quindi non cancellabile " . $i;
    }
    /*} else {
        SOVRASCRITTURA FILE 
    }
*/



    /*decripta*/
    if (file_exists('../DATABASE_SETTAGGI_ENCRYPTED.sqlite')) {
        $encrypted_code = file_get_contents('../DATABASE_SETTAGGI_ENCRYPTED.sqlite'); //Get the encrypted code.
    } else {
        echo "file settaggi encrypted non esiste " . $i;
        break;
        exit;
    }

    if (empty($encrypted_code)) {
        echo "false 3";
        break;
        exit;
    }
    try {
        $db = new SQLite3('../DATABASE_SETTAGGI_ENCRYPTED.sqlite');
        $db->close();
    } catch (Exception $e) {
        echo "database settaggi encrypted non apribile " . $i;
        break;
        exit;
    }
    $decrypted_code = my_decrypt($encrypted_code, $key); //Decrypt the encrypted code.
    if (!file_exists('../DATABASE_SETTAGGI.sqlite')) {
        file_put_contents('../DATABASE_SETTAGGI.sqlite', $decrypted_code); //Save the decrypted code somewhere.    
    } else {
        echo "file settaggi gia esistente " . $i;
        break;
        exit;
    }
}

echo '<br><br>FATTO <br><br>';
//echo $decrypted_code;
