<?php

$data = date('d-m-y');

$sql = "SELECT * FROM comanda WHERE data_stampa='" . $data . "' AND stato_record='CHIUSO' AND posizione='CONTO';";

define("db_type", "../DATABASE_CLIENTE.sqlite");

$db2 = null;

if (db_type === 'mysql') {
    $db2 = new mysqli(host, user, pass, db);
} else {
    $db2 = new SQLite3(db_type);
    #var_dump($db);
}

$result = false;

$array_risultato_json = array();

while ($result === false) {
    $ms = "0." . rand(2, 6);
    sleep($ms);
    #echo "QUERY";
    $result = $db2->query($sql);

    if (db_type === 'mysql') {
        while ($row = $result->fetch_assoc()) {
            array_push($array_risultato_json, $row);
        }
    } else {
        while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
            array_push($array_risultato_json, $row);
        }
    }
}


echo json_encode($array_risultato_json);

$db2->close();

include 'dgfe_transfer.php';

?>