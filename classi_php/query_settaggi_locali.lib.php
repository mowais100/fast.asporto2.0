<?php

$percorso_main="C:/TAVOLI/fast.intellinet/";

$websql = '../settaggi_locali/settaggi.sqlite';
//$websql='/storage/emulated/0/www/fast.intellinet/settaggi_locali/settaggi.sqlite';

$query = $_POST['query'];

try {

    $db = new SQLite3($websql);

    $db->busyTimeout(10000);

    if (strpos(strtolower($query), 'select') !== false) {
        $results = $db->query($query);
    } else {
        $results = $db->exec($query);
    }

    $json = [];

    if (is_bool($results) !== true) {

        while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
            array_push($json, $row);
        }
    } else {
        $json = $results;
    }

    echo json_encode($json);
    
} catch (Exception $ex) {

    echo $ex;
}

$db->close();
unset($db);
