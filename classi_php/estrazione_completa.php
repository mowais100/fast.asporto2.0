<?php

ini_set('memory_limit', -1);

ini_set('display_errors', 1);

set_time_limit(0);

//DICHIARAZIONE ARRAYS

$array_riga = array();
$array_colonna = array();

//OGGETTO DATABASE

$db = new SQLite3('../DATABASE_CLIENTE.sqlite');


//COLONNE

$sql_colonne = "PRAGMA table_info(comanda);";

$colonne = $db->query($sql_colonne);

while ($colonna = $colonne->fetchArray(SQLITE3_ASSOC)) {
    array_push($array_colonna, $colonna['name']);
}

//RIGHE

$sql_righe = "SELECT * FROM comanda;";

$righe = $db->query($sql_righe);

$i = 0;

while ($riga = $righe->fetchArray(SQLITE3_ASSOC)) {

    //CREAZIONE INDICE ARRAY   
    $array_riga[$i] = array();

    foreach ($riga as $key => $value) {
        array_push($array_riga[$i], $value);
    }

    $i++;
}


//OUTPUT TUTTO COMMENTABILE A VIDEO
//OUTPUT COLONNE INTABELLATE IN FORMATO TABLET HEADER

echo "<table border='1' cellspacing='0'>";



echo "<tr class='header'>";

echo "<td class='empty'></td>";


foreach ($array_colonna as $key => $value) {

    echo "<th class='$key'>$value</th>";
}

echo "</tr>";





//OUTPUT RIGHE INTABELLATE
$tot_indici = count($array_riga);

for ($i = 0; $i < $tot_indici; $i++) {

    //INIZIO RIGA CON CLASSE CORRISPONDENTE ALLA KEY
    echo "<tr class='$i'>";

    echo "<td class='rowid'>".($i+1)."</td>";

    foreach ($array_riga[$i] as $key => $value) {

        //COLONNA CON CLASSE CORRISPONDENTE A KEY
        echo "<td class='$key'>$value</td>";
    }

    //FINE RIGA
    echo "</tr>";
}


echo "</table>";
?>