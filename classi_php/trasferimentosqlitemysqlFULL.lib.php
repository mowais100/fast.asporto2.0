<?php

ini_set('max_execution_time', 300);
set_time_limit(300);

error_reporting(E_ALL);

$data = date("Ymd");

$fileName = "blocco_esportazione_$data.csv";

if (file_exists($fileName)) {
    echo "true";
} else {
    /* file che blocca l'esportazione dopo la prima */

    $file_content = "B";
    file_put_contents($fileName, $file_content);

    $percorso_main = "C:/TAVOLI/fast.intellinet/";

    $mysqli = new mysqli("localhost", "root", "", "provaINTELLINET");


//LETTURA COMANDA

    $db = new PDO("sqlite:../DATABASE_CLIENTE.sqlite");



    $queryVERIFICAcomanda1 = $db->query("select sum(prog_inser) as somma from comanda ;");
    $Vc1 = $queryVERIFICAcomanda1->fetch(PDO::FETCH_ASSOC);
    $Vc1_r = $Vc1["somma"];

    $query1 = $db->query("select * from comanda ;");

    $file_content = "";

    while ($row = $query1->fetch(PDO::FETCH_ASSOC)) {

        $arr_len = count($row);
        $i = 1;

        foreach ($row as $key => $value) {
            $file_content .= '"' . $value . '"';
            if ($i < $arr_len) {
                $file_content .= ',';
            }
            $i++;
        }

        $file_content .= "\n";
    }

    $fileName = "file_temporaneo" . time() . ".csv";

    file_put_contents($fileName, $file_content);


//IMPORTAZIONE COMANDA MYSQL
    $query = <<<eof
    LOAD DATA INFILE 'C:/TAVOLI/fast.intellinet/classi_php/$fileName'
     INTO TABLE comanda
     FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
     LINES TERMINATED BY '\n'
eof;

    $mysqli->query($query);

    $queryVERIFICAcomanda2 = $mysqli->query("select sum(prog_inser) as somma from comanda ;");
    $Vc2 = $queryVERIFICAcomanda2->fetch_array(MYSQLI_ASSOC);
    $Vc2_r = $Vc2["somma"];



//LETTURA TESTE
    $queryVERIFICAtesta1 = $db->query("select sum(numero_fiscale) as somma from record_teste ;");
    $Vt1 = $queryVERIFICAtesta1->fetch(PDO::FETCH_ASSOC);
    $Vt1_r = $Vt1["somma"];




    $query1 = $db->query("select * from record_teste ;");

    $file_content = "";

    while ($row = $query1->fetch(PDO::FETCH_ASSOC)) {

        $arr_len = count($row);
        $i = 1;

        foreach ($row as $key => $value) {
            $file_content .= '"' . $value . '"';
            if ($i < $arr_len) {
                $file_content .= ',';
            }
            $i++;
        }

        $file_content .= "\n";
    }

    $fileName = "file_teste" . time() . ".csv";

    file_put_contents($fileName, $file_content);


//IMPORTAZIONE TESTE MYSQL
    $query = <<<eof
    LOAD DATA INFILE '{$percorso_main}classi_php/$fileName'
     INTO TABLE record_teste
     FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
     LINES TERMINATED BY '\n'
eof;

    $mysqli->query($query);

    $queryVERIFICAtesta2 = $mysqli->query("select sum(numero_fiscale) as somma from record_teste ;");
    $Vt2 = $queryVERIFICAtesta2->fetch_array(MYSQLI_ASSOC);
    $Vt2_r = $Vt2["somma"];


    /* Somma corpi e teste sqlite e mysql */

    $file_content = "Sqlite\n\nSomma corpi: " . $Vc1_r . "\n\nSomma teste: " . $Vt1_r . "\n\n\nMySql\n\nSomma corpi: " . $Vc2_r . "\n\nSomma teste: " . $Vt2_r;

    $fileName = "file_somma" . time() . ".txt";

    file_put_contents($fileName, $file_content);

    /* CANCELLA IL DB CENTRALE ALLA FINE */

    $db->query("delete from comanda ;");

    $db->query("delete from record_teste ;");


    if ($Vc1_r == $Vc2_r && $Vt1_r == $Vt2_r) {
        echo "true";
    } else {
        echo "false";
    }
}