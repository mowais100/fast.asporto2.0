<?php

class LogTouchTester
{

    public string $csv_file;

    private function __construct()
    {

        /* @var mixed $csv_file */
        $this->csv_file = filter_input(INPUT_POST, 'csv_file', FILTER_SANITIZE_STRING);

        /* Variabile di test */
        /* $csv_file = "C:/CLIENTI/Imperial/2021-08-05/LOG_TOUCH_2021-08-04_SEP_CANCELLETTO.csv"; */
    }


    public static function get_csv_to_json()
    {

        $global_variables = new static();

        /* @var array $csv_log_array */
        $csv_log_array = [];

        /* $this->dbg("<pre>");*/

        /* @var resource|false $file_handler */
        if (($file_handler = fopen($global_variables->csv_file, "r")) !== FALSE) {

            /* @var array|false $line */
            while (($line = fgetcsv($file_handler, 0, '#', '"')) !== FALSE) {

                /* @var int $indexes_number */
                $indexes_number = count($line);

                /* @var string $event */
                $event = "";

                if (!empty($line[2])) {
                    $event = trim($line[2]);
                }

                if ($event === "touchend" ||$event === "click") {

                    /* @var string $touch_element */
                    $touch_element = "";

                    /* @var int $i */
                    for ($i = 3; $i < $indexes_number; $i++) {
                        //$this->dbg(" # ");

                        if (strpos($line[$i], "TENTATIVO ") === false) {
                            $touch_element .= $line[$i];
                        }
                    }

                    $touch_element = trim($touch_element);

                    if (!empty($touch_element)) {

                        /*$this->dbg($event);

                            $this->dbg(" # ");

                            $this->dbg($touch_element); */

                        array_push($csv_log_array, ["event" => $event, "element" => $touch_element]);
                    }

                    /* $this->dbg("<br>"); */
                }
            }

            fclose($file_handler);
        }

        /* $this->dbg(""</pre>");*/

        print_r(json_encode($csv_log_array));

        /*$this->dbg(""FINE FILE");*/
    }
}

LogTouchTester::get_csv_to_json();
