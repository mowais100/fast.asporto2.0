<?php

$ip_client = $_SERVER['REMOTE_ADDR'];

$ip_client="192.168.0.151";

$arp = "arp -a " . $ip_client;

exec($arp, $output);

$output_string = join($output);

$regex = "/([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})/";

$macaddress = preg_match($regex, $output_string, $matches_out);

if ($ip_client === $_SERVER['SERVER_ADDR']) {
    echo "SERVER";
} else if ($macaddress && $matches_out && $matches_out[0]) {
    echo $matches_out[0];
} else {
    echo "ND";
}