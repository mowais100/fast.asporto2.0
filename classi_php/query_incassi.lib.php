<?php

ini_set('max_execution_time', 300);
set_time_limit(300);

error_reporting(E_ALL);

$mysql_abilitato = true;


$percorso_main = "C:/TAVOLI/fast.intellinet/";


$websql = $percorso_main . 'DATABASE_CLIENTE.sqlite';
//$websql='/storage/emulated/0/www/fast.intellinet/DATABASE_LOCALE.sqlite';

$query = $_POST['query'];

try {

    $db = new SQLite3($websql);

    $db->busyTimeout(10000);

    if (strpos(strtolower($query), 'select') !== false) {
        $results = $db->query($query);
    } else {
        $results = $db->exec($query);
    }

    $json = [];
    $json2 = [];

    if (is_bool($results) !== true) {

        while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
            array_push($json, $row);
        }
    }

//PARTE MYSQL
    if ($mysql_abilitato === true) {

        $dbmysql = new mysqli("localhost", "root", "", "provaINTELLINET");


        $dbmysql->set_charset("utf8");

        $query1 = $dbmysql->query($query);

        if (is_bool($query1) !== true) {
            while ($row = $query1->fetch_array(MYSQLI_ASSOC)) {

                array_push($json, $row);
            }
        }
    }

    echo json_encode($json);
} catch (Exception $ex) {

    echo $ex;
}

$db->close();

unset($db);
