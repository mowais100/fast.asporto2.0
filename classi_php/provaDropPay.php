<?php

error_reporting(E_ALL);

$data = array(
    "grant_type" => "authorization_code",
    "code" => "e51ac7e761ef452b84fa6a87aeff570a",
    "client_id" => "7bda978fd1124a5ba49416991abc162a",
    "client_secret" => "fc71a0885822481a988529c565243cb3",
    "scope" => "app"
    );

$data_string = json_encode($data);

$ch = curl_init('https://api.drop-pay.io/oa2/v1/ac/token');
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Content-Length: ' . strlen($data_string))
);

$result = curl_exec($ch);

echo "<pre>$result</pre>";