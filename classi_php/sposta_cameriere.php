<?php

try {
    $tavolo = filter_input(INPUT_POST, "tavolo", FILTER_SANITIZE_STRING);
    $operatore = filter_input(INPUT_POST, "operatore", FILTER_SANITIZE_STRING);
    $directory_dati = filter_input(INPUT_POST, "directory_dati", FILTER_SANITIZE_STRING);
    $ora_servizio = filter_input(INPUT_POST, "ora_servizio", FILTER_SANITIZE_STRING);
    $servizio = filter_input(INPUT_POST, "servizio", FILTER_SANITIZE_STRING);

    //CONVERTIRE IL TAVOLO IN NOME FILE SIA XML CHE .CO1
    //TAVOLO000.XML (id)
    //Tav0.CO1 (id)


    if (date('H') < $ora_servizio) {
        $data = date('Y-m-d', strtotime("now-1 day"));
    } else {
        $data = date("Y-m-d");
    }

    $folder_prefix = "C:/TAVOLI/" . $directory_dati . "/" . $data . "_" . $servizio;

    $nome_file = "TAVOLO" . str_pad($tavolo, 3, '0', STR_PAD_LEFT) . ".XML";
    $file_tavolo_occupato = "Tav" . $tavolo . ".CO1";
    $file_path = $folder_prefix . "/" . $nome_file;
    
    $orario_log=date("h:i:s");

    //BLOCCO TAVOLO
    bloccaggio_tavolo(true);

    //LETTURA E SCRITTURA XML TAVOLO
    $xml = simplexml_load_file($file_path);

    $i = 0;
    foreach ($xml as $article) {
        if ($article->DES == "RECORD TESTA") {
            
            break;
        }
        $i++;
    }

    $xml->TAVOLI[$i]->NOME = $operatore;
//var_dump($xml);
    
    $xml->asXML($file_path);
} catch (Exception $e) {

    //SBLOCCO TAVOLO
    bloccaggio_tavolo(false);

    salva_log($orario_log."[PHP] Errore: " . $e);
    echo "false";
    return false;
}

//SBLOCCO TAVOLO
bloccaggio_tavolo(false);

salva_log($orario_log."[PHP] Cambio Cameriere Tavolo: " . $tavolo . " a " . $operatore);
echo "true";

// --- FUNZIONI ---
function salva_log($stringa) {

    global $folder_prefix;

    $my_file = $folder_prefix . "/LOGERRSS.TXT";
    $handle = fopen($my_file, 'a');
    $data = $stringa . '\r\n';
    fwrite($handle, $data);
    fclose($handle);
}

function bloccaggio_tavolo($bool) {

    global $file_tavolo_occupato, $folder_prefix;

    $my_file = $folder_prefix . "/COND/" . $file_tavolo_occupato;

    if ($bool == true) {
        file_put_contents($my_file, " ");
    } else {

        unlink($my_file);
    }
}
