<?php

ini_set('memory_limit', '-1');

//LASCIANDO GLI ERRORI ABILITATI SI RISOLVONO ALCUNI ERRORI IN CASSA, 
//IN CUI DAREBBE FALSE MA IN REALTA' E' BLOCCATO O SPENTO IL DB CENTRALE
//PERO' C'E' IL PROBLEMA CHE RIPETENDO 10 VOLTE LA QUERY PER TESTARE GLI ERRORI
//SE LA PRIMA VOLTA E' LOCKED E POI LA QUERY VIENE EFFETTUATA LA GESTIONEDB
//LO VEDE COMUNQUE COME ERRORE E RIPETE LA QUERY UN ALTRA VOLTA, CREANDO UN DOPPIO RECORD
error_reporting(0);

$sql = $_POST['query'];

//MYSQL
@$host = $_REQUEST['db_host'];
@$user = $_REQUEST['db_user'];
@$pass = $_REQUEST['db_pass'];

@$key = $_REQUEST['key'];

$db_name = $_REQUEST['db_name'];
$db_type = $_REQUEST['type'];

$prefix = '../';

$db2 = null;


if ($db_type === 'mysql') {
    $db2 = new mysqli($host, $user, $pass, $db_name);
} else {
    if ($db_name === "DATABASE_SETTAGGI.sqlite") {
        if (file_exists($prefix . 'DATABASE_SETTAGGI_ENCRYPTED.sqlite')) {
            $encrypted_code = file_get_contents($prefix . 'DATABASE_SETTAGGI_ENCRYPTED.sqlite'); //Get the encrypted code.
        } else {
            echo "false";
            exit;
        }
        if (empty($encrypted_code)) {
            echo "false";
            exit;
        }
        try {
            $db = new SQLite3('../DATABASE_SETTAGGI_ENCRYPTED.sqlite');
            $db->close();
        } catch (Exception $e) {
            echo "false";
            exit;
        }
        $decrypted_code = my_decrypt($encrypted_code, $key); //Decrypt the encrypted code.

        if (!file_exists($prefix . 'DATABASE_SETTAGGI.sqlite')) {
            file_put_contents($prefix . 'DATABASE_SETTAGGI.sqlite', $decrypted_code); //Save the decrypted code somewhere.    
        } else {
            echo "false";
            exit;
        }
    }
    $db2 = new SQLite3($prefix . $db_name);
}

$result = false;

$i = 0;
while ($result !== true && $i < 10) {

    $ms = "0." . rand(6, 9);
    sleep($ms);
    if (strpos(strtolower($sql), 'select') !== false && strpos(strtolower($sql), 'delete') === false) {
        $result = $db2->query($sql);
    } else {
        $result = $db2->exec($sql);
    }

    //var_dump($result);
    //QUI E' DIVERSO DA FALSE, PERCHE' UNA QUERY NON E' COME UN EXEC CHE RESTITUISCE TRUE
    //MA COMUNQUE NON RESTITUISCE FALSE, MA BENSI' UN OGGETTO, SE VA A BUON FINE
    if ($result !== false) {
        $risultato_booleano = "true";

        break;
    }

    $risultato_booleano = "false";
    //FALSE DOVREBBE ESSERE SEMPRE UN ERRORE, TEORICAMENTE (VEDI http://php.net/manual/en/sqlite3.query.php)
    //Returns an SQLite3Result object if the query returns results. Otherwise, returns TRUE if the query succeeded, FALSE on failure.

    $i++;
}

if (strpos(strtolower($sql), 'select') === false || strpos(strtolower($sql), 'delete') !== false) {
    $db2->close();
} else {
    if ($db_type === 'mysql') {
        for ($i = 1; $riga = $result->fetch_assoc(); $i++) {
            $prodotti[$i] = $riga;
        }
    } else {
        for ($i = 1; $riga = $result->fetchArray(SQLITE3_ASSOC); $i++) {
            $prodotti[$i] = $riga;
        }
    }
}
//qui chiude in ogni caso
$db2->close();

if ($db_name === "DATABASE_SETTAGGI.sqlite") {
    if (file_exists($prefix . 'DATABASE_SETTAGGI.sqlite')) {
        $code = file_get_contents($prefix . 'DATABASE_SETTAGGI.sqlite'); //Get the code to be encypted.
    } else {
        echo "false";
        exit;
    }
    if (empty($code)) {
        echo "false";
        exit;
    }
    try {
        $db = new SQLite3('../DATABASE_SETTAGGI.sqlite');
        $db->close();
    } catch (Exception $e) {
        echo "false";
        exit;
    }
    $encrypted_code = my_encrypt($code, $key); //Encrypt the code.    

    file_put_contents($prefix . 'DATABASE_SETTAGGI_ENCRYPTED.sqlite', $encrypted_code); //Save the ecnypted code somewhere.
    unlink($prefix . 'DATABASE_SETTAGGI.sqlite');
}
//var_dump($prodotti);

if (@json_encode($prodotti) !== null && @json_encode($prodotti) !== 'null') {
    echo json_encode($prodotti);
} else {
    echo $risultato_booleano;
}

function my_encrypt($data, $key)
{
    $encryption_key = base64_decode($key);
    $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
    $encrypted = openssl_encrypt($data, 'aes-256-cbc', $encryption_key, 0, $iv);
    return base64_encode($encrypted . '::' . $iv);
}

function my_decrypt($data, $key)
{
    $encryption_key = base64_decode($key);
    list($encrypted_data, $iv) = explode('::', base64_decode($data), 2);
    return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
}
