<?php
error_reporting(0);

try {
    $data = explode("\n", file_get_contents("/proc/meminfo"));
    $meminfo = array();
    foreach ($data as $line) {
        list($key, $val) = explode(":", $line);
        $meminfo[$key] = trim($val);
    }
    print_r(var_export($meminfo,true));
} catch (Exception $e) {
     echo 'Caught exception: ',  $e->getMessage();
}

echo '<br/><br/>';

try {
    exec('wmic memorychip get capacity', $totalMemory);
    print_r(var_export($totalMemory,true));
} catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage();
}
?>