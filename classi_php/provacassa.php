<?php

ini_set('memory_limit', '-1');

//LASCIANDO GLI ERRORI ABILITATI SI RISOLVONO ALCUNI ERRORI IN CASSA, 
//IN CUI DAREBBE FALSE MA IN REALTA' E' BLOCCATO O SPENTO IL DB CENTRALE
//PERO' C'E' IL PROBLEMA CHE RIPETENDO 10 VOLTE LA QUERY PER TESTARE GLI ERRORI
//SE LA PRIMA VOLTA E' LOCKED E POI LA QUERY VIENE EFFETTUATA LA GESTIONEDB
//LO VEDE COMUNQUE COME ERRORE E RIPETE LA QUERY UN ALTRA VOLTA, CREANDO UN DOPPIO RECORD
error_reporting(E_ALL);
ini_set('display_errors', 1);

$sql = "DELETE FROM comanda WHERE ntav_comanda='CONTINUO_1' AND stato_record='ATTIVO' AND (contiene_variante is null OR contiene_variante='') AND desc_art='BIBITA BOTTIGLIA' and portata='B' and cast(prog_inser as int)>=(select prog_inser from comanda where ntav_comanda='CONTINUO_1' AND stato_record='ATTIVO' AND (contiene_variante is null OR contiene_variante='') AND desc_art='BIBITA BOTTIGLIA' and portata='B' order by cast(prog_inser as int) desc limit 0,0); ";

//MYSQL
@$host = "localhost";
@$user = "root";
@$pass = "";

$db_name = "DATABASE_CLIENTE.sqlite";
$db_type = "sqlite";

$prefix = '../';

$db2 = null;

if ($db_type === 'mysql') {
    $db2 = new mysqli($host, $user, $pass, $db_name);
} else {
    $db2 = new SQLite3($prefix . $db_name);
}

$result = false;


$i = 0;
while ($result !== true && $i < 10) {

    $ms = "0." . rand(6, 9);
    sleep($ms);
    if (strpos(strtolower($sql), 'select') !== false&&strpos(strtolower($sql), 'delete') === false) {
        echo "A";
        $result = $db2->query($sql);
    } else {
        echo "B";
        $result = $db2->exec($sql);
    }

    //var_dump($result);

    //QUI E' DIVERSO DA FALSE, PERCHE' UNA QUERY NON E' COME UN EXEC CHE RESTITUISCE TRUE
    //MA COMUNQUE NON RESTITUISCE FALSE, MA BENSI' UN OGGETTO, SE VA A BUON FINE
    if ($result !== false) {
        echo "C";
        $risultato_booleano = "true";
        
        break;
    }

    $risultato_booleano = "false";
    //FALSE DOVREBBE ESSERE SEMPRE UN ERRORE, TEORICAMENTE (VEDI http://php.net/manual/en/sqlite3.query.php)
    //Returns an SQLite3Result object if the query returns results. Otherwise, returns TRUE if the query succeeded, FALSE on failure.
    
    $i++;
}

if (strpos(strtolower($sql), 'select') === false||strpos(strtolower($sql), 'delete') !== false) {
    echo "D";
    $db2->close();
} else {
    echo "E";
    if ($db_type === 'mysql') {
        for ($i = 1; $riga = $result->fetch_assoc(); $i++) {
            $prodotti[$i] = $riga;
        }
    } else {
        for ($i = 1; $riga = $result->fetchArray(SQLITE3_ASSOC); $i++) {
            $prodotti[$i] = $riga;
        }
    }
}
//qui chiude in ogni caso
$db2->close();

//var_dump($prodotti);

if (@json_encode($prodotti) !== null && @json_encode($prodotti) !== 'null') {
    echo json_encode($prodotti);
} else {
    echo $risultato_booleano;
}
