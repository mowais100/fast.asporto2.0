<?php

$percorso_main="C:/TAVOLI/fast.intellinet/";
$websql = $percorso_main.'DATABASE_LOCALE.sqlite';
//$websql='/storage/emulated/0/www/fast.intellinet/DATABASE_LOCALE.sqlite';

$query = $_POST['query'];

try {

    $db = new SQLite3($websql);

    $db->busyTimeout(10000);

    //Bug query exec mod_articolo (diminuzione quantita) risolto con queste 2 condizioni assieme
    if (strpos(strtolower($query), 'select') !== false&&strpos(strtolower($query), 'delete') === false) {
        $results = $db->query($query);
    } else {
        $results = $db->exec($query);
    }

    $json = [];

    if (is_bool($results) !== true) {

        while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
            array_push($json, $row);
        }
    } else {
        $json = $results;
    }

    echo json_encode($json);
    
} catch (Exception $ex) {

    echo $ex;
}

$db->close();
unset($db);
