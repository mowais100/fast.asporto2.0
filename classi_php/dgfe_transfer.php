<?php

//PRIMA PARTE TRASFERIMENTO DATI PULITI A DGFE

$websql = 'C:/TAVOLI/fast.intellinet_NUOVA/provaINTELLINET_RISTO.sqlite';
$query = "ATTACH DATABASE 'C:/TAVOLI/fast.intellinet_NUOVA/prova_DGFE/DGFE.sqlite' AS new; INSERT INTO new.comanda SELECT * FROM comanda WHERE substr(stato_record,1,10)!='CANCELLATO';";

try {

    $db = new SQLite3($websql);

    $db->busyTimeout(10000);


    $results = $db->exec($query);


    $json = [];

    if (is_bool($results) !== true) {

        while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
            array_push($json, $row);
        }
    } else {
        $json = $results;
    }
} catch (Exception $ex) {
    
}

$db->close();
unset($db);

//SECONDA PARTE TRASFERIMENTO DATI SUPERFLUI A DATISUPERFLUI

$websql = 'C:/TAVOLI/fast.intellinet_NUOVA/provaINTELLINET_RISTO.sqlite';
$query = "ATTACH DATABASE 'C:/TAVOLI/fast.intellinet_NUOVA/prova_DGFE/DatiSuperflui.sqlite' AS new; INSERT INTO new.comanda SELECT * FROM comanda WHERE substr(stato_record,1,10)='CANCELLATO';";

try {

    $db = new SQLite3($websql);

    $db->busyTimeout(10000);


    $results = $db->exec($query);


    $json = [];

    if (is_bool($results) !== true) {

        while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
            array_push($json, $row);
        }
    } else {
        $json = $results;
    }
} catch (Exception $ex) {
    
}

$db->close();
unset($db);

//TERZA PARTE CANCELLAZIONE COMANDE DA DB CENTRALE

$websql = 'C:/TAVOLI/fast.intellinet_NUOVA/provaINTELLINET_RISTO.sqlite';
$query = "DELETE FROM comanda;";

try {

    $db = new SQLite3($websql);

    $db->busyTimeout(10000);


    $results = $db->exec($query);


    $json = [];

    if (is_bool($results) !== true) {

        while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
            array_push($json, $row);
        }
    } else {
        $json = $results;
    }
} catch (Exception $ex) {
    
}

$db->close();
unset($db);

echo "true";
