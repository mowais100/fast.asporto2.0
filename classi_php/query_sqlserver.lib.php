<?php

ini_set('max_execution_time', 0);

set_time_limit(0);

$SET88 = $_POST["SET88"];

$SET89 = $_POST["SET89"];

$SET90 = $_POST["SET90"];

$sql = $_POST["query"];

/*var_dump($SET88);
var_dump($SET89);
var_dump($SET90);
var_dump($sql);*/

$serverName = $SET88; //serverName\instanceName
$connectionInfo = array("Database" => $SET89, "UID" => $SET89, "PWD" => $SET90);
$conn = sqlsrv_connect($serverName, $connectionInfo);

if ($conn) {
    /*echo "Connessione Stabilita.<br />";*/
} else {
    /*echo "Connessione Non Possibile.<br />";*/
    die(mostra_errore(sqlsrv_errors()));
}

try {


    $stmt = sqlsrv_query($conn, $sql);

    $json = [];

    if ($stmt === false) {
        die(mostra_errore(sqlsrv_errors()));
    }

    if (is_bool($stmt) !== true) {

        while ($row = sqlsrv_fetch_object($stmt)) {
            array_push($json, $row);
        }

        echo json_encode($json);
    } else {
        echo $results;
    }
} catch (Exception $ex) {
    
}

function mostra_errore($error) {

    $a = "<pre>";
    $a .= var_export($error, true);
    $a .= "</pre>";

    return $a;
}
