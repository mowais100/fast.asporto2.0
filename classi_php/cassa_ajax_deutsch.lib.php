<?php

session_set_cookie_params(0, "/", "", false, false);
session_start();

ini_set('memory_limit', -1);

ini_set('display_errors', 1);

error_reporting(1);

set_time_limit(0);



require_once ( 'cassa.lib.php' );

//var_dump($_SESSION);
//SERVE ALTRIMENTI NON E' LA STESSA COSA, ANCHE SE DA LA NOTICE

ini_set('max_execution_time', 300);

$host = $_REQUEST['db_host'];
$user = $_REQUEST['db_user'];
$psas = $_REQUEST['db_pass'];

$prefix = '../';


$db_name = $_REQUEST['db_name'];
$db_name = "DATABASE_CLIENTE.sqlite";

$db_type = $_REQUEST['type'];
$db_type = 'sqlite';

$funzione = $_POST['action'];

if (!function_exists("array_column")) {

    function array_column($array, $column_name) {

        return array_map(function($element) use($column_name) {
            return $element[$column_name];
        }, $array);
    }

}

//FUNZIONI PER CLONARE LE CLASSI JS IN AJAX
if (($funzione === "costruttore_ajax" || $funzione === "elenco_movimenti") && !empty($_POST['dati_costruttore'])) {

    $dati_costruttore = $_POST['dati_costruttore'];

    $_SESSION['cassa_ajax_args'] = array($dati_costruttore['nome_dispositivo'], $dati_costruttore['nome_operatore'], $dati_costruttore['password_md5'],
        $dati_costruttore['id_categoria'], $dati_costruttore['numero_pagina'], $dati_costruttore['numero_tavolo'], $dati_costruttore['nome_parcheggio']);

    if ($funzione === "costruttore_ajax") {
        echo $dati_costruttore['id_categoria'];
    }
}


if (!empty($_SESSION['cassa_ajax_args'])) {
    $cassa_ajax = new cassa_ajax($_SESSION['cassa_ajax_args'][0], $_SESSION['cassa_ajax_args'][1], $_SESSION['cassa_ajax_args'][2], $_SESSION['cassa_ajax_args'][3], $_SESSION['cassa_ajax_args'][4], $_SESSION['cassa_ajax_args'][5], $_SESSION['cassa_ajax_args'][6]);
}

//var_dump($cassa_ajax);

switch ($funzione) {

    case "elenco_movimenti":
        $cassa_ajax->elenco_movimenti(@$_POST['da_data'], @$_POST['a_data'], @$_POST['bandiera']);
        break;

    case "tabella_impostazioni_fiscali":
        $cassa_ajax->tabella_impostazioni_fiscali();
        break;

    case "aggiungi_prodotto":
        $cassa_ajax->aggiungi_prodotto($_POST['form'], $_POST['categoria']);
        break;

    case "salva_mod_tab":
        $cassa_ajax->salva_mod_tab($_POST['quale'], $_POST['form'], $_POST['id']);
        break;

    case "aggiungi_categoria":
        $cassa_ajax->aggiungi_categoria($_POST['form']);
        break;

    case "select_elenco_clienti":
        $cassa_ajax->select_elenco_clienti();
        break;

    case "aggiungi_cliente":
        $cassa_ajax->aggiungi_cliente($_POST['form']);
        break;

    case "aggiungi_sconto":
        $cassa_ajax->aggiungi_sconto($_POST['form']);
        break;

    case "elenco_comande_aperte":
        $cassa_ajax->elenco_comande_aperte();
        break;

    case "gestione_sconti":
        $cassa_ajax->tabella_sconti();
        break;

    case "gestione_prodotti":
        $cassa_ajax->tabella_prodotti();
        break;

    case "gestione_categorie":
        $cassa_ajax->tabella_categorie();
        break;

    case "gestione_clienti":
        $cassa_ajax->tabella_clienti();
        break;

    case "elimina_da_tabella":
        $cassa_ajax->elimina_da_tabella($_POST['tabella'], $_POST['id']);
        break;

    case "cancella_conto_intero":
        $cassa_ajax->cancella_conto_intero();
        break;

    case "elimina_articolo":
        $cassa_ajax->elimina_articolo($_POST['id']);
        break;

    case "modifica_articolo":
        $cassa_ajax->modifica_articolo($_POST['posizione_articolo']);
        break;

    case "salva_articolo":
        $cassa_ajax->salva_articolo($_POST['form']);
        break;

    case "elenco_prodotti":
#var_dump($cassa_ajax);
        $cassa_ajax->elenco_prodotti();
        break;

    case "riempi_form_fattura":
        $cassa_ajax->riempi_form_fattura($_POST['id']);
        break;

    case "riapri_comanda":
        $cassa_ajax->riapri_comanda($_POST['id']);
        break;

    case "login":

        $cassa_ajax = new cassa_ajax(null, $_POST['nome'], $_POST['password'], null, null, null, null);
        $cassa_ajax->verifica_operatore();
        break;

    default:
        break;
}

class cassa_ajax {

//COSTRUTTORE E DISTRUTTORE
    public function __construct($nome_dispositivo, $nome_operatore, $password_md5, $id_categoria, $numero_pagina, $numero_tavolo, $nome_parcheggio) {

        global $prefix, $db_name, $host, $user, $pass, $db_type;


        if ($db_type === 'mysql') {
            $db = new mysqli($host, $user, $pass, $db_name);
        } else {
            $db = new SQLite3($prefix . $db_name);
        }

//var_dump($prefix . $db_name);
//var_dump($db);


        $this->db = $db;

//VARIABILI STATICHE
        $this->nome_dispositivo = $nome_dispositivo;
        $this->numero_pagina = $numero_pagina;
        $this->numero_tavolo = $numero_tavolo;
        $this->password_md5 = $password_md5;
        $this->nome_operatore = $nome_operatore;
        $this->id_categoria = $id_categoria;
        $this->nome_parcheggio = $nome_parcheggio;

//VARIABILI DA CUI ESTRARRE TUTTI I DATI
        $this->array_operatore = $this->dati_operatore();
        $this->array_categoria = $this->dati_categoria();
    }

    public function __destruct() {
        $this->db->close();
    }

//METODI PRIVATI PER DEFINIRE BENE LA CLASSE
    private function dati_operatore() {
        $sql = "SELECT * FROM operatori WHERE nome='" . $this->nome_operatore . "' LIMIT 1;";

        $result = false;
        while ($result === false) {
            $ms = "0." . rand(2, 6);
            sleep($ms);
            $result = $this->db->query($sql);
        }

        if (!empty($result)) {
            return $result->fetchArray(SQLITE3_ASSOC);
        } else {
            return false;
        }
    }

    private function dati_categoria() {
        $sql = "SELECT * FROM categorie WHERE id='" . $this->id_categoria . "' LIMIT 1;";

        $result = $this->db->query($sql);

        if (!empty($result)) {
            return $result->fetchArray(SQLITE3_ASSOC);
        } else {
            return false;
        }
    }

    public function salva_mod_tab($quale, $form, $id) {

        if ($quale === "prodotti" || $quale === "tabella_sconti_buoni" || $quale === "categorie" || $quale === "clienti" || $quale === "impostazioni_fiscali") {
            $params = array();
            parse_str($form, $params);
            $params = $params['tab_' . $quale][$id];

#var_dump($params);

            $istruzioni = '';
            $campi = '';
            $valori = '';

            $td = 0;
            foreach ($params as $key => $value) {
                if ($td === 0) {
                    $prefix = "";
                } else {
                    $prefix = ",";
                }

                $value = addslashes($value);

                $istruzioni.=$prefix . substr($key, 1, -1) . "='$value'";

                $td++;
            }

            $query = "UPDATE $quale SET $istruzioni WHERE id='" . $id . "';";

            if ($this->db->query($query) !== FALSE) {
                echo $query;
            }
        }
    }

//METODI PER LA GESTIONE DELLA COMANDA
    public function aggiungi_prodotto($form) {
        $params = array();
        parse_str($form, $params);

        $query = "select * from prodotti where categoria='" . $this->id_categoria . "' and posizione is not null order by pagina,posizione desc limit 1";
        $risultato = $this->db->query($query);

        $risultato = $risultato->fetchArray(SQLITE3_ASSOC);

        $cat_varianti = $risultato['cat_varianti'];

        $ultima_posizione = $risultato['posizione'];

        $ultima_riga = substr($ultima_posizione, 0, 1);
        $ultima_colonna = substr($ultima_posizione, 2, 1);
        $ultima_pagina = $risultato['pagina'];

        if ($ultima_colonna === '7' && $ultima_riga === '7') {

            $pagina = $ultima_pagina + 1;
            $riga = '1';
            $colonna = '1';
        } else if ($ultima_colonna === '7' && $ultima_riga !== '7') {

            $pagina = $ultima_pagina;
            $riga = $ultima_riga + 1;
            $colonna = '1';
        } else {

            $pagina = $ultima_pagina;
            $riga = $ultima_riga;
            $colonna = $ultima_colonna + 1;
        }

        $posizione = $riga . '-' . $colonna;

        if (empty($params['modifica_id'])) {
            $query = "INSERT INTO prodotti(pagina,posizione,descrizione,prezzo_1,categoria,cat_varianti) VALUES (\"" . $pagina . "\",\"" . $posizione . "\",\"" . $params['descrizione'] . "\",\"" . $params['prezzo_1'] . "\",\"" . $this->id_categoria . "\",\"$cat_varianti\");";
        } elseif (!empty($params['modifica_id'])) {
            $query = "UPDATE prodotti SET descrizione='" . $params['descrizione'] . "',prezzo_1='" . $params['prezzo_1'] . "',categoria='" . $this->id_categoria . "' WHERE id='" . $params['modifica_id'] . "'";
        }

        $this->db->query($query);
    }

    public function aggiungi_categoria($form) {

        $params = array();
        parse_str($form, $params);


        $query = "INSERT INTO categorie(descrizione,perc_iva,dest_stampa) VALUES (\"" . $params['descrizione'] . "\",\"" . $params['prezzo_1'] . "\",\"" . $this->id_categoria . "\");";

        echo "<pre>" . $query . "</pre>";

        $this->db->query($query);
    }

    public function aggiungi_cliente($form) {

        $params = array();
        parse_str($form, $params);


        $query = "INSERT INTO clienti ( ragione_sociale , indirizzo , citta , provincia , telefono , email , codice_fiscale , partita_iva  ) VALUES (\"" . $params['ragione_sociale'] . "\",\"" . $params['indirizzo'] . "\",\"" . $params['citta'] . "\",\"" . $params['provincia'] . "\",\"" . $params['telefono'] . "\",\"" . $params['email'] . "\",\"" . $params['codice_fiscale'] . "\",\"" . $params['partita_iva'] . "\");";

        echo "<pre>" . $query . "</pre>";

        $this->db->query($query);
    }

    public function aggiungi_sconto($form) {

        $params = array();
        parse_str($form, $params);


        $query = "INSERT INTO tabella_sconti_buoni ( percentuale, descrizione, tipo ) VALUES (\"" . $params['percentuale'] . "\",\"" . $params['descrizione'] . "\",\"" . $params['tipo'] . "\");";

        echo "<pre>" . $query . "</pre>";

        $this->db->query($query);
    }

    public function tabella_prodotti() {

        $query = "SELECT * FROM prodotti WHERE categoria='" . $this->id_categoria . "' ORDER BY descrizione ASC;";

        $risultato = $this->db->query($query);
        $num_col = $risultato->numColumns();

        echo "<h1>lang_66</h1><br/><br/>";

        echo "<form id=\"tab_prodotti\">";
        echo "<table id='gestione_prodotti' class='table table-responsive table-striped table-bordered sortable' >";

//RIGA DI INTESTAZIONE
        echo "<tr>";
        echo "<th>Elimina</th>";

        for ($i = 0; $i < $num_col; $i++) {
            $prodotto = $risultato->columnName($i);
            if ($prodotto != "id") {
                echo "<th style=\"min-width:90px;\">$prodotto</th>";
            }
        }
        echo "</tr>";

//TABELLA
        $i = 0;
        while ($prodotto = $risultato->fetchArray(SQLITE3_ASSOC)) {
            echo "<tr>";
            echo "<td onclick='elimina_da_tabella(\"prodotti\"," . $prodotto['id'] . ");' style='text-align:center;font-size:25px;color:red;cursor:pointer;'>X</td>";

            foreach ($prodotto as $key => $value) {
                if ($key != "id") {
                    $class = '';


                    echo "<td style=\"min-width:" . (strlen($value) * 10) . "px;\"><input style=\"border:1px solid grey; background-color:transparent;color:snow;\" onchange=\"salva_mod_tab('prodotti'," . $prodotto['id'] . ");\" name=\"tab_prodotti[" . $prodotto['id'] . "]['$key']\" type=\"text\" value=\"$value\" class=\"form-control $class\"></td>";
                }
            }
            echo "</tr>";
            $i++;
        }
        echo "</table>";
        echo "</form>";
        echo '<script type="text/javascript" src="./classi_javascript/sorttable.js"></script>';
    }

    public function tabella_impostazioni_fiscali() {

        $query = "SELECT * FROM impostazioni_fiscali ORDER BY id ASC LIMIT 1;";

        $risultato = $this->db->query($query);
        $num_col = $risultato->numColumns();

        echo "<h1>lang_29</h1><br/><br/>";

        echo "<table id='impostazioni_fiscali' class='table table-responsive table-striped table-bordered sortable' >";

//RIGA DI INTESTAZIONE
        echo "<tr>";
        for ($i = 0; $i < $num_col; $i++) {
            $prodotto = $risultato->columnName($i);
            if ($prodotto != "id") {
                echo "<th style=\"min-width:90px;\">$prodotto</th>";
            }
        }
        echo "</tr>";

//TABELLA
        while ($prodotto = $risultato->fetchArray(SQLITE3_ASSOC)) {
            echo "<tr>";

            foreach ($prodotto as $key => $value) {
                if ($key != "id") {
                    $class = '';

                    echo "<td style=\"min-width:" . (strlen($value) * 10) . "px;\"><input style=\"border:1px solid grey; background-color:transparent;color:snow;\" onchange=\"salva_mod_tab('impostazioni_fiscali'," . $prodotto['id'] . ");\" name=\"tab_impostazioni_fiscali[" . $prodotto['id'] . "]['$key']\" type=\"text\" value=\"$value\" class=\"form-control $class\"></td>";
                }
            }
            echo "</tr>";
        }
        echo "</table>";
        echo '<script type="text/javascript" src="./classi_javascript/sorttable.js"></script>';
    }

    public function tabella_categorie() {

        $query = "SELECT * FROM categorie ORDER BY ordinamento ASC;";

        $risultato = $this->db->query($query);
        $num_col = $risultato->numColumns();

        echo "<h1>lang_63</h1><br/><br/>";

        echo "<table id='gestione_categorie' class='table table-responsive table-striped table-bordered sortable' >";

//RIGA DI INTESTAZIONE
        echo "<tr>";
        echo "<th>Elimina</th>";

        for ($i = 0; $i < $num_col; $i++) {
            $prodotto = $risultato->columnName($i);
            if ($prodotto != "id") {
                $px = (strlen($prodotto) * 50) . "px";
                echo "<th style=\"min-width:90px;width:$px;\">$prodotto</th>";
            }
        }
        echo "</tr>";

//TABELLA
        while ($prodotto = $risultato->fetchArray(SQLITE3_ASSOC)) {
            echo "<tr>";
            echo "<td onclick='elimina_da_tabella(\"categorie\"," . $prodotto['id'] . ");' style='text-align:center;font-size:25px;color:red;cursor:pointer;'>X</td>";

            foreach ($prodotto as $key => $value) {
                if ($key != "id") {
                    $class = '';

                    echo "<td ><input style=\"border:1px solid grey; background-color:transparent;color:snow;\" onchange=\"salva_mod_tab('categorie'," . $prodotto['id'] . ");\" name=\"tab_categorie[" . $prodotto['id'] . "]['$key']\" type=\"text\" value=\"$value\" class=\"form-control $class\"></td>";
                }
            }
            echo "</tr>";
        }
        echo "</table>";
        echo '<script type="text/javascript" src="./classi_javascript/sorttable.js"></script>';
    }

    public function tabella_sconti() {

        $query = "SELECT * FROM tabella_sconti_buoni ORDER BY percentuale ASC;";

        $risultato = $this->db->query($query);
        $num_col = $risultato->numColumns();

        echo "<h1>lang_67</h1><br/><br/>";

        echo "<form id=\"tab_sconti\">";
        echo "<table id='gestione_sconti' class='table table-responsive table-striped table-bordered sortable' >";

//RIGA DI INTESTAZIONE
        echo "<tr>";
        echo "<th>Elimina</th>";

        for ($i = 0; $i < $num_col; $i++) {
            $prodotto = $risultato->columnName($i);
            if ($prodotto != "id") {
                echo "<th style=\"min-width:90px;\">$prodotto</th>";
            }
        }
        echo "</tr>";

//TABELLA
        $i = 0;
        while ($prodotto = $risultato->fetchArray(SQLITE3_ASSOC)) {
            echo "<tr>";
            echo "<td onclick='elimina_da_tabella(\"tabella_sconti_buoni\"," . $prodotto['id'] . ");' style='text-align:center;font-size:25px;color:red;cursor:pointer;'>X</td>";

            foreach ($prodotto as $key => $value) {
                if ($key != "id") {
                    echo "<td style=\"min-width:" . (strlen($value) * 10) . "px;\"><input style=\"border:1px solid grey; background-color:transparent;color:snow;\" onchange=\"this.value = this.value.toLowerCase();salva_mod_tab('tabella_sconti_buoni'," . $prodotto['id'] . ");\" name=\"tab_tabella_sconti_buoni[" . $prodotto['id'] . "]['$key']\" type=\"text\" value=\"$value\" class=\"form-control\"></td>";
                }
            }
            echo "</tr>";
            $i++;
        }
        echo "</table>";
        echo "</form>";
        echo '<script type="text/javascript" src="./classi_javascript/sorttable.js"></script>';
    }

    public function tabella_clienti() {

        $query = "SELECT * FROM clienti ORDER BY ragione_sociale ASC;";


        $risultato = $this->db->query($query);

        $num_col = $risultato->numColumns();

        echo "<h1>lang_64</h1><br/><br/>";

        echo "<table id='gestione_categorie' class='table table-responsive table-striped table-bordered sortable' >";

//RIGA DI INTESTAZIONE
        echo "<tr>";
        echo "<th>Elimina</th>";

        for ($i = 0; $i < $num_col; $i++) {
            $prodotto = $risultato->columnName($i);
            if ($prodotto != "id") {
                echo "<th style=\"min-width:90px;\">$prodotto</th>";
            }
        }

        echo "</tr>";

//TABELLA
        while ($prodotto = $risultato->fetchArray(SQLITE3_ASSOC)) {
            echo "<tr>";
            echo "<td onclick='elimina_da_tabella(\"clienti\"," . $prodotto['id'] . ");' style='text-align:center;font-size:25px;color:red;cursor:pointer;'>X</td>";

            foreach ($prodotto as $key => $value) {
                if ($key != "id") {
                    echo "<td style=\"min-width:" . (strlen($value) * 10) . "px;\"><input style=\"border:1px solid grey; background-color:transparent;color:snow;\" onchange=\"salva_mod_tab('clienti'," . $prodotto['id'] . ");\" name=\"tab_clienti[" . $prodotto['id'] . "]['$key']\" type=\"text\" value=\"$value\" class=\"form-control\"></td>";
                }
            }

            echo "</tr>";
        }
        echo "</table>";
        echo '<script type="text/javascript" src="./classi_javascript/sorttable.js"></script>';
    }

    //Flag serve in caso di opzioni particolari facoltative
    public function elenco_movimenti($da_data = null, $a_data = null, $flag) {

        $data_scritta = date('d/m/Y');

        echo "<div class='col-md-12 text-center'>";
        echo "<button class=\"btn btn-success\" onclick=\"selezione_operatore('MOVIMENTI');\" style=\"width:130px;\">Bargeld</button>";

        echo "<button class=\"btn btn-info\" onclick=\"stampa_totali_giornata('dettagliovenduti');\" style=\"width:130px;\">Produkte</button>";
        echo "</div>";


        echo "<div class='col-md-12 text-center'>";


        echo "<h2>$data_scritta</h2>";

        echo "</div>";

        echo "<div class=\"col-xs-2\"><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>"
        . "<select name=\"giorno_1\" class=\"form-control\"><option value=\"Giorno\" selected>Tag</option><option value=\"01\">01</option><option value=\"02\">02</option><option value=\"03\">03</option><option value=\"04\">04</option><option value=\"05\">05</option><option value=\"06\">06</option><option value=\"07\">07</option><option value=\"08\">08</option><option value=\"09\">09</option><option value=\"10\">10</option><option value=\"11\">11</option><option value=\"12\">12</option><option value=\"13\">13</option><option value=\"14\">14</option><option value=\"15\">15</option><option value=\"16\">16</option><option value=\"17\">17</option><option value=\"18\">18</option><option value=\"19\">19</option><option value=\"20\">20</option><option value=\"21\">21</option><option value=\"22\">22</option><option value=\"23\">23</option><option value=\"24\">24</option><option value=\"25\">25</option><option value=\"26\">26</option><option value=\"27\">27</option><option value=\"28\">28</option><option value=\"29\">29</option><option value=\"30\">30</option><option value=\"31\">31</option></select><br>"
        . "<select name=\"mese_1\" class=\"form-control\"><option value=\"Mese\" selected>Monat</option><option value=\"01\">Januar</option><option value=\"02\">Februar</option><option value=\"03\">März</option><option value=\"04\">April</option><option value=\"05\">Mai</option><option value=\"06\">Juni</option><option value=\"07\">Juli</option><option value=\"08\">August</option><option value=\"09\">September</option><option value=\"10\">Oktober</option><option value=\"11\">November</option><option value=\"12\">Dezember</option></select><br>"
        . "<select name=\"anno_1\" class=\"form-control\"><option value=\"Anno\" selected>Jahr</option><option value=\"16\">2016</option><option value=\"17\">2017</option><option value=\"18\">2018</option><option value=\"19\">2019</option><option value=\"20\">2020</option></select><br>"
        . "<div class=\"col-xs-12 text-center\"><button class=\"btn btn-success\" onclick=\"confronta_periodo(1);\" style=\"font-size: 16px;\">Senden</button></div>"
        . "</div>";

        echo "<div class=\"col-xs-8\">";

        echo "<div class=\"col-xs-12 text-center\"  style=\"color:#00bc8c\"><h4>CORRENTE:</h4></div>";
        echo "<div class=\"col-xs-6\" style=\"padding-bottom:20px;padding-top:20px;border:1px solid grey\">Jahr: <strong>" . date("Y") . "</strong>"
        . "<canvas id=\"canvas\"></canvas>"
        . "<script>
                    

        window.chartColors = {
            red: 'rgb(255, 99, 132)',
            orange: 'rgb(255, 159, 64)',
            yellow: 'rgb(255, 205, 86)',
            green: 'rgb(0, 188, 140)',
            blue: 'rgb(54, 162, 235)',
            purple: 'rgb(153, 102, 255)',
            grey: 'rgb(231,233,237)'
        };

        window.randomScalingFactor = function() {
            return Math.floor(Math.random()*(500000-0+1)+0);
        };

        Chart.defaults.global.defaultFontColor='#fff';

        var config = {
            type: 'line',
            data: {
                labels:  [\"Januar\", \"Februar\", \"März\", \"April\", \"Mai\", \"Juni\", \"Juli\", \"August\", 
                
                \"September\", \"Oktober\", \"November\", \"Dezember\"],
                datasets: [{
                    label: \"BARGELD\",
                    backgroundColor: window.chartColors.green,
                    borderColor: window.chartColors.green,
                    data: [";

        for ($i = 1; $i <= 12; $i++) {
            $query = "select sum(replace(valore_fiscale,',','.')) as prezzo_tot from chiusure_fiscali where substr(data_emissione,4,2)='" . sprintf('%02d', $i) . "';";

            $risultato = $this->db->query($query);

            while ($totale = $risultato->fetchArray(SQLITE3_ASSOC)) {
                if ($totale['prezzo_tot'] > 0) {
                    echo $totale['prezzo_tot'];
                } else {
                    echo 0;
                }
                if ($i < 12) {
                    echo ",";
                }
            }
        }
        echo "],
                    fill: false,
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:''
                },
                legend: {
        display: false
    },
                 tooltips: {
        callbacks: {
           label: function(tooltipItem) {
                  return tooltipItem.yLabel;
           }
        }
    },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Monate'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Euro'
                        }
                    }]
                }
            }
        };

      
            var ctx = document.getElementById(\"canvas\").getContext(\"2d\");
            window.myLine = new Chart(ctx, config);
        
            window.myLine.update();
       

       
    </script>"
        . ""
        . ""
        . "</div>";


        echo "<div class=\"col-xs-6\" style=\"padding-bottom:20px;padding-top:20px;border:1px solid grey\">Monat: " . date("F") . ""
        . "<canvas id=\"canvas2\"></canvas>"
        . "<script>
                    


window.chartColors = {
	red: 'rgb(255, 99, 132)',
	orange: 'rgb(255, 159, 64)',
	yellow: 'rgb(255, 205, 86)',
	green: 'rgb(0, 188, 140)',
	blue: 'rgb(54, 162, 235)',
	purple: 'rgb(153, 102, 255)',
	grey: 'rgb(231,233,237)'
};

window.randomScalingFactor = function() {
	return Math.floor(Math.random()*(10000-0+1)+0);
};

Chart.defaults.global.defaultFontColor='#fff';

        var config = {
            type: 'line',
            data: {
                labels:  [\"1\", \"2\", \"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"10\",\"11\",\"12\",\"13\",\"14\",\"15\",\"16\",\"17\",\"18\",\"19\",\"20\",\"21\",\"22\",\"23\",\"24\",\"25\",\"26\",\"27\",\"28\",\"29\",\"30\",\"31\" ],
                datasets: [{
                    label: \"BARGELD\",
                    backgroundColor: window.chartColors.green,
                    borderColor: window.chartColors.green,
                    data: [";

        for ($i = 1; $i <= 31; $i++) {
            $query = "select sum(replace(valore_fiscale,',','.')) as prezzo_tot from chiusure_fiscali where substr(data_emissione,4,2)='" . sprintf('%02d', date('m')) . "' and substr(data_emissione,1,2)='" . sprintf('%02d', $i) . "';";

            $risultato = $this->db->query($query);

            while ($totale = $risultato->fetchArray(SQLITE3_ASSOC)) {
                if ($totale['prezzo_tot'] > 0) {
                    echo $totale['prezzo_tot'];
                } else {
                    echo 0;
                }
                if ($i < 31) {
                    echo ",";
                }
            }
        }
        echo "],
                    fill: false,
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:''
                },
                legend: {
        display: false
    },
                 tooltips: {
        callbacks: {
           label: function(tooltipItem) {
                  return tooltipItem.yLabel;
           }
        }
    },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Tage'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Euro'
                        }
                    }]
                }
            }
        };

      
            var ctx = document.getElementById(\"canvas2\").getContext(\"2d\");
            window.myLine = new Chart(ctx, config);
        
            window.myLine.update();
       

       
    </script>"
        . ""
        . ""
        . "</div>";


        echo "<div class=\"col-xs-12 text-center\" style=\"color:#00bc8c\"><h4>CONFRONTA:</h4></div>";
        echo "<div class=\"col-xs-6\" style=\"padding-bottom:20px;padding-top:20px;border:1px solid grey\"><span id=\"grafico_selezione_periodo_1\"></span>"
        . "<canvas id=\"canvas3\"></canvas>"
        . "<script>
                    


window.chartColors = {
	red: 'rgb(255, 99, 132)',
	orange: 'rgb(255, 159, 64)',
	yellow: 'rgb(255, 205, 86)',
	green: 'rgb(0, 188, 140)',
	blue: 'rgb(54, 162, 235)',
	purple: 'rgb(153, 102, 255)',
	grey: 'rgb(231,233,237)'
};

Chart.defaults.global.defaultFontColor='#fff';

        var config = {
            type: 'bar',
            data: {
                labels:  [],
                datasets: [{
                    label: \"...\",
                    backgroundColor: window.chartColors.green,
                    borderColor: window.chartColors.green,
                    data: [0],
                    fill: false,
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:''
                },
                legend: {
        display: false
    },
                 tooltips: {
        callbacks: {
           label: function(tooltipItem) {
                  return tooltipItem.yLabel;
           }
        }
    },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: '...'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Euro'
                        }
                    }]
                }
            }
        };

      
            var ctx = document.getElementById(\"canvas3\").getContext(\"2d\");
           window.myLine_confronto1 = new Chart(ctx, config);
        

       

       
    </script>"
        . ""
        . ""
        . "</div>";




         echo "<div class=\"col-xs-6\" style=\"padding-bottom:20px;padding-top:20px;border:1px solid grey\"><span id=\"grafico_selezione_periodo_2\"></span>"
        . "<canvas id=\"canvas4\"></canvas>"
        . "<script>
                    


window.chartColors = {
	red: 'rgb(255, 99, 132)',
	orange: 'rgb(255, 159, 64)',
	yellow: 'rgb(255, 205, 86)',
	green: 'rgb(0, 188, 140)',
	blue: 'rgb(54, 162, 235)',
	purple: 'rgb(153, 102, 255)',
	grey: 'rgb(231,233,237)'
};

Chart.defaults.global.defaultFontColor='#fff';

        var config = {
            type: 'bar',
            data: {
                labels:  [],
                datasets: [{
                    label: \"...\",
                    backgroundColor: window.chartColors.green,
                    borderColor: window.chartColors.green,
                    data: [],
                    fill: false,
                }]
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:''
                },
                legend: {
        display: false
    },
                 tooltips: {
        callbacks: {
           label: function(tooltipItem) {
                  return tooltipItem.yLabel;
           }
        }
    },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: '...'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Euro'
                        }
                    }]
                }
            }
        };

      
            var ctx = document.getElementById(\"canvas4\").getContext(\"2d\");
            window.myLine_confronto2 = new Chart(ctx, config);
        

           
            </script>"
        . "</div>";


        echo "</div>";

        echo "<div class=\"col-xs-2\"><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>"
        . "<select name=\"giorno_2\" class=\"form-control\"><option value=\"Giorno\" selected>Tag</option><option value=\"01\">01</option><option value=\"02\">02</option><option value=\"03\">03</option><option value=\"04\">04</option><option value=\"05\">05</option><option value=\"06\">06</option><option value=\"07\">07</option><option value=\"08\">08</option><option value=\"09\">09</option><option value=\"10\">10</option><option value=\"11\">11</option><option value=\"12\">12</option><option value=\"13\">13</option><option value=\"14\">14</option><option value=\"15\">15</option><option value=\"16\">16</option><option value=\"17\">17</option><option value=\"18\">18</option><option value=\"19\">19</option><option value=\"20\">20</option><option value=\"21\">21</option><option value=\"22\">22</option><option value=\"23\">23</option><option value=\"24\">24</option><option value=\"25\">25</option><option value=\"26\">26</option><option value=\"27\">27</option><option value=\"28\">28</option><option value=\"29\">29</option><option value=\"30\">30</option><option value=\"31\">31</option></select><br>"
        . "<select name=\"mese_2\" class=\"form-control\"><option value=\"Mese\" selected>Monat</option><option value=\"01\">Januar</option><option value=\"02\">Februar</option><option value=\"03\">März</option><option value=\"04\">April</option><option value=\"05\">Mai</option><option value=\"06\">Juni</option><option value=\"07\">Juli</option><option value=\"08\">August</option><option value=\"09\">September</option><option value=\"10\">Oktober</option><option value=\"11\">November</option><option value=\"12\">Dezember</option></select><br>"
        . "<select name=\"anno_2\" class=\"form-control\"><option value=\"Anno\" selected>Jahr</option><option value=\"16\">2016</option><option value=\"17\">2017</option><option value=\"18\">2018</option><option value=\"19\">2019</option><option value=\"20\">2020</option></select><br>"
        . "<div class=\"col-xs-12 text-center\"><button class=\"btn btn-success\" onclick=\"confronta_periodo(2);\" style=\"font-size: 16px;\">Senden</button></div>"
        . "</div>";
        
        echo "<script>
        
        var data=new Date();
        data.setDate(data.getDate() - 1);
        var giorno=aggZero(data.getDate().toString(),2);
        var mese=aggZero((data.getMonth()+1).toString(),2);
        var anno=data.getFullYear().toString().slice(2);
        
        console.log('GIORNO',giorno);
        

        $('[name=\"giorno_1\"] option:containsExact('+giorno+')').attr('selected',true);
        $('[name=\"mese_1\"] option[value='+mese+']').attr('selected',true);
        $('[name=\"anno_1\"] option[value='+anno+']').attr('selected',true);
        
            confronta_periodo(1);
            

        var data=new Date();
        var giorno=aggZero(data.getDate().toString(),2);
        var mese=aggZero((data.getMonth()+1).toString(),2);
        var anno=data.getFullYear().toString().slice(2);
        
 console.log('GIORNO',giorno);

        $('[name=\"giorno_2\"] option:containsExact('+giorno+')').attr('selected',true);
        $('[name=\"mese_2\"] option[value='+mese+']').attr('selected',true);
        $('[name=\"anno_2\"] option[value='+anno+']').attr('selected',true);
                
            confronta_periodo(2);
                
        </script>";
    }

    public function salva_articolo($form) {

        $params = array();
        parse_str($form, $params);

        $query = "UPDATE prodotti SET posizione=null, pagina=null WHERE categoria='" . $this->id_categoria . "' AND pagina='" . $this->numero_pagina . "' AND posizione='" . $params["posizione"] . "';";
        @$this->db->query($query);


        $query = "UPDATE prodotti SET posizione='" . $params["posizione"] . "',pagina='" . $this->numero_pagina . "',colore_tasto='" . $params["colore_tasto"] . "' WHERE id='" . $params["id_articolo"] . "';";

        $this->db->query($query);
    }

    public function elimina_da_tabella($tabella, $id) {
        switch ($tabella) {
            case "clienti":
            case "categorie":
            case "prodotti":
            case "tabella_sconti_buoni":
                $query_base = "DELETE FROM $tabella WHERE id='$id';";
                break;

            default:
                break;
        }

        $risultato = $this->db->query($query_base);
    }

    public function modifica_articolo($posizione) {


        $query_base = "SELECT * FROM prodotti WHERE categoria='" . $this->id_categoria . "' AND posizione='$posizione' ORDER BY descrizione ASC;";

        $risultato = $this->db->query($query_base);

        $prodotto = $risultato->fetchArray(SQLITE3_ASSOC);


        if (!empty($prodotto['descrizione'])) {
            echo "$('#modifica_articolo select[name=\"id_articolo\"]').append($('<option>', {
      value: '" . $prodotto['id'] . "',
      text: '" . addslashes($prodotto['descrizione']) . "'
      }));";
        }

        $query_base = "SELECT * FROM prodotti WHERE categoria='" . $this->id_categoria . "' AND (posizione is null OR posizione='') ORDER BY descrizione ASC;";

        $risultato = $this->db->query($query_base);

        while ($prodotto = $risultato->fetchArray(SQLITE3_ASSOC)) {

            echo "$('#modifica_articolo select[name=\"id_articolo\"]').append($('<option>', {
      value: '" . $prodotto['id'] . "',
      text: '" . addslashes($prodotto['descrizione']) . "'
      }));";
        }

        $query_base = "SELECT * FROM prodotti WHERE categoria='" . $this->id_categoria . "' AND posizione!='' ORDER BY descrizione ASC;";

        $risultato = $this->db->query($query_base);

        while ($prodotto = $risultato->fetchArray(SQLITE3_ASSOC)) {

            echo "$('#modifica_articolo select[name=\"id_articolo\"]').append($('<option>', {
      value: '" . $prodotto['id'] . "',
      text: '-ND- " . addslashes($prodotto['descrizione']) . "'
      }));";
        }
    }

//Lasciare così le variabili (è un altra funzione)
    public function verifica_operatore() {

        if ($this->password_md5 !== "0") {
            $password = trim($this->password_md5);

            $sql = "SELECT * FROM operatori WHERE upper(nome)='" . strtoupper($this->nome_operatore) . "' AND password='" . $password . "';";

            $result = false;
            while ($result === false) {
                $ms = "0." . rand(2, 6);
                sleep($ms);
                $result = $this->db->query($sql);
            }


            $operatore = $result->fetchArray(SQLITE3_ASSOC);

            if (!empty($operatore)) {
                echo 'true';
                return true;
            }
        }

        echo 'false';
        return false;
    }

    function select_elenco_clienti() {

//I tipi possono essere: lista | select
//$mod è per cambiare nell'oggetto la categoria. è tipo booleano

        $results = $this->db->query('SELECT * FROM clienti ORDER BY ragione_sociale ASC;');

        $opzione = "<option></option>";
        echo "$('#fattura_elenco_clienti').append('$opzione');";

        while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
            $opzione = "<option value=\"" . $row['id'] . "\">" . $row['ragione_sociale'] . "</option>";
            echo "$('#fattura_elenco_clienti').append('$opzione');";
        }
    }

    function riempi_form_fattura($id) {

//I tipi possono essere: lista | select
//$mod è per cambiare nell'oggetto la categoria. è tipo booleano

        $sql = "SELECT * FROM clienti WHERE id='$id' LIMIT 1;";
        $results = $this->db->query($sql);

        $row = $results->fetchArray(SQLITE3_ASSOC);

        foreach ($row as $key => $value) {
            echo "$('#fattura form [name=\"" . $key . "\"]').val('" . $value . "');";
        }
    }

}
