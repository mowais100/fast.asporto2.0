/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sds;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import java.awt.print.PrinterException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.InetSocketAddress;
import java.util.HashMap;
import javax.swing.JTextPane;
import java.util.Map;
import java.util.Set;
import com.sun.org.apache.xml.internal.utils.WrappedRuntimeException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 *
 * @author DavideLenovo
 */

/*
$.ajax({
            type: "POST",
            url: 'http://localhost:8000/BAR3',
			data:'<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><epos-print xmlns="http://www.epson-pos.com/schemas/2011/03/epos-print"><text font="font_a"/><text align="center"/><text>&#10;&#10;</text><text width="2" height="2"/><text>n° 66&#10;</text><text font="font_a"/><feed line="1"/><text>BAR&#10;</text><feed line="1"/><text align="left"/><text width="1" height="1"/><feed line="1"/><text reverse="false" ul="false" em="true" color="color_1"/><text reverse="false" ul="false" em="true" color="color_1"/><text>Operatore: COMANDA&#10;</text><text reverse="false" ul="false" em="true" color="color_1"/><text>21/06/17 </text><text reverse="false" ul="false" em="true" color="color_1"/><text>12:31</text><text>&#10;</text><text>&#10;</text><text reverse="false" ul="false" em="true" color="color_1"/><text>Coperti: 1&#10;</text><text reverse="false" ul="false" em="false" color="color_1"/><text width="1" height="1"/><feed line="1"/><text align="center"/><text>----------------- BIBITE -----------------&#10;</text><text reverse="false" ul="false" em="false" color="color_1"/><text align="left"/><text width="2" height="2"/><text>1</text><text x="50"/><text x="50"/><text>SPREMUTA POMPELMO</text><text>&#10;</text><text width="1" height="1"/><text reverse="false" ul="false" em="false" color="color_1"/><text width="2" height="2"/><text>1</text><text x="50"/><text x="50"/><text>THE LIMONE MEDIO</text><text>&#10;</text><text width="1" height="1"/><text reverse="false" ul="false" em="false" color="color_1"/><text align="center"/><text>-----------------------------------------&#10;</text><text align="left"/><text reverse="false" ul="false" em="true" color="color_1"/><feed line="1"/><text>2</text><text x="50"/><text>articoli </text><text>&#10;&#10;</text><text align="left"/><feed line="1"/><text reverse="false" ul="false" em="true" color="color_1"/><text align="center"/><text reverse="false" ul="false" em="false" color="color_1"/><text align="center"/><text reverse="false" ul="false" em="false" color="color_1"/><text align="center"/><feed line="2"/><cut type="feed"/></epos-print></s:Body></s:Envelope>'
        });
 */
public class SDS {

    static String printerNameDesired;

    static class MyHandler implements HttpHandler {

        public static Map<String, String> getQueryMap(String query) {
            String[] params = query.split("&");
            Map<String, String> map = new HashMap<String, String>();
            for (String param : params) {
                String name = param.split("=")[0];
                String value = param.split("=")[1];
                map.put(name, value);
            }
            return map;
        }

        @Override
        public void handle(HttpExchange t) throws IOException, WrappedRuntimeException {

            String response = null;

            t.getResponseHeaders().set("Access-Control-Allow-Origin", "*");
            t.getResponseHeaders().set("Access-Control-Allow-Credentials", "true");
            t.getResponseHeaders().set("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
            t.getResponseHeaders().set("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,If-Modified-Since,SOAPAction");

            t.getResponseHeaders().set("Content-Type", "text/xml; charset=utf-8");

            JTextPane jtp = new JTextPane();

            jtp.setContentType("text/html");

            InputStreamReader isr = new InputStreamReader(t.getRequestBody(), "utf-8");
            BufferedReader br = new BufferedReader(isr);

// From now on, the right way of moving from bytes to utf-8 characters:
            int b;
            StringBuilder buf = new StringBuilder(512);
            while ((b = br.read()) != -1) {
                buf.append((char) b);
            }

            br.close();
            isr.close();

            String query = t.getRequestURI().getQuery();

            printerNameDesired = "ERRORE";

            if (query != null) {
                Map<String, String> map = getQueryMap(query);
                Set<String> keys = map.keySet();
                for (String key : keys) {
                    if (key.compareTo("stampante") == 0) {
                        printerNameDesired = map.get(key);
                    }
                }
            }

            String strResult = null;
            try {

                try {

                    DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                    InputSource is = new InputSource();
                    is.setCharacterStream(new StringReader(buf.toString()));

                    Document doc = db.parse(is);

                    Stampa stampa = new Stampa(doc);
                    
                    

                    stampa.printer_job();

                } catch (WrappedRuntimeException e) {
                    response = "<?xml version=\"1.0\" encoding=\"utf-8\"?><s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"><s:Body><response success=\"false\" code=\"" + e + "\" status=\"251658300\" battery=\"0\" xmlns=\"http://www.epson-pos.com/schemas/2011/03/epos-print\"></response></s:Body></s:Envelope>";
                }

                //strResult = writer.toString();
            } catch (Exception e) {
                e.printStackTrace();
                response = "<?xml version=\"1.0\" encoding=\"utf-8\"?><s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"><s:Body><response success=\"false\" code=\"" + e + "\" status=\"251658300\" battery=\"0\" xmlns=\"http://www.epson-pos.com/schemas/2011/03/epos-print\"></response></s:Body></s:Envelope>";

            }

            //Era result
            //jtp.setText(strResult);
            /* PrintService[] service = PrinterJob.lookupPrintServices(); // list of printers
            DocPrintJob docPrintJob = null;

            int count = service.length;

            for (int i = 0; i < count; i++) {
                if (service[i].getName().equalsIgnoreCase(printerNameDesired)) {
                    docPrintJob = service[i].createPrintJob();
                    i = count;
                }
            }*/
 /*try {

                PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
                aset.add(MediaSizeName.ISO_A4);

                Insets margins = new Insets(-10, 0, 0, 0);
                jtp.setMargin(margins);


                 
                jtp.print(null, null, false, docPrintJob.getPrintService(), aset, false);

            } catch (NullPointerException e) {

                response = "<?xml version=\"1.0\" encoding=\"utf-8\"?><s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"><s:Body><response success=\"false\" code=\"" + e + "\" status=\"251658300\" battery=\"0\" xmlns=\"http://www.epson-pos.com/schemas/2011/03/epos-print\"></response></s:Body></s:Envelope>";

            } catch (PrinterException ex) {
                Logger.getLogger(SDS.class.getName()).log(Level.SEVERE, null, ex);
                response = "<?xml version=\"1.0\" encoding=\"utf-8\"?><s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"><s:Body><response success=\"false\" code=\"" + ex + "\" status=\"251658300\" battery=\"0\" xmlns=\"http://www.epson-pos.com/schemas/2011/03/epos-print\"></response></s:Body></s:Envelope>";

            }*/
            response = "<?xml version=\"1.0\" encoding=\"utf-8\"?><s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"><s:Body><response success=\"true\" code=\"\" status=\"251658262\" battery=\"0\" xmlns=\"http://www.epson-pos.com/schemas/2011/03/epos-print\"></response></s:Body></s:Envelope>";

            t.sendResponseHeaders(200, response.length());
            try (OutputStream os = t.getResponseBody()) {
                os.write(response.getBytes());
            }
        }
    }

    public static String conversione(Document doc) {
        try {

            String html_risposta = "";

            //optional, but recommended
            //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
            doc.getDocumentElement().normalize();

            //System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
            NodeList nList = doc.getElementsByTagName("epos-print");

            NodeList eposprint_child = nList.item(0).getChildNodes();

            // System.out.println("----------------------------");
            String position = "";
            String fontsize = "font-size:12pt;";
            String align = "";
            String em = "";
            String ul = "";
            int cursor = 0;

            boolean div_aperto = false;

            html_risposta += "<html>\n";
            html_risposta += "<head>\n";
            html_risposta += "<title>Stampa SDS</title>\n";
            html_risposta += "<meta charset=\"UTF-8\">\n";
            //html_risposta += "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">";
            html_risposta += "</head>\n";
            html_risposta += "<body>\n";
            html_risposta += "<div style=\"font-family: 'Arial Narrow' ; line-height: 15px;\">\n";

            for (int temp = 0; temp < eposprint_child.getLength(); temp++) {

                Node nNode = eposprint_child.item(temp);

                if (nNode.getNodeName() != "#text") {

                    //System.out.print("E " + nNode.getNodeName() + " E\n");
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                        Element eElement = (Element) nNode;

                        if (nNode.getNodeName() == "text") {
                            if (eElement.getTextContent().length() > 0) {
                                html_risposta += "<span style=\"" + position + fontsize + em + ul + "\" >";

                                String[] array_linee = eElement.getTextContent().split("\n", -1);

                                int i = 0;
                                for (String element : array_linee) {
                                    html_risposta += element;
                                    cursor += element.length();

                                    i++;
                                    if (i < array_linee.length) {
                                        html_risposta += "<br/>";
                                        cursor = 0;
                                    }

                                }

                                html_risposta += "</span>\n";
                                position = "";
                            } else {

                                if (eElement.getAttribute("x").length() > 0) {

                                    int val_x = Integer.parseInt(eElement.getAttribute("x"));

                                    int val_cursor = cursor;

                                    for (; val_cursor < val_x; val_cursor += 18) {

                                        html_risposta += "&nbsp;";
                                        cursor += 18;
                                    }

                                }

                                //System.out.print("Position : " + eElement.getAttribute("x"));
                                if ("2".equals(eElement.getAttribute("height")) && "2".equals(eElement.getAttribute("width"))) {
                                    fontsize = "font-size:18pt;";
                                } else if ("1".equals(eElement.getAttribute("height")) && "1".equals(eElement.getAttribute("width"))) {
                                    fontsize = "font-size:12pt;";
                                }

                                if (eElement.getAttribute("align").length() > 0) {

                                    if (div_aperto == true) {
                                        html_risposta += "</div>\n";
                                        div_aperto = false;
                                    }

                                    html_risposta += "<div style=\"text-align:" + eElement.getAttribute("align") + ";\">\n";
                                    div_aperto = true;

                                }

                                //System.out.print("Align : " + eElement.getAttribute("align"));
                                if (eElement.getAttribute("em").length() > 0) {
                                    if ("true".equals(eElement.getAttribute("em"))) {
                                        em = "font-weight:bold;";
                                    } else {
                                        em = "font-weight:normal;";
                                    }
                                }
                                //System.out.print("Strong : " + eElement.getAttribute("em"));

                                if (eElement.getAttribute("ul").length() > 0) {
                                    if ("true".equals(eElement.getAttribute("ul"))) {
                                        ul = "text-decoration:underline;";
                                    } else {
                                        ul = "text-decoration:none;";
                                    }
                                }
                                //System.out.print("Underline : " + eElement.getAttribute("ul"));
                            }

                        } else if ("feed".equals(nNode.getNodeName())) {

                            int numero_linee = Integer.parseInt(eElement.getAttribute("line"));

                            for (int i = 0; i < numero_linee; i++) {
                                html_risposta += "<br/>";
                                cursor = 0;
                            }

                        }
                    }

                }
            }
            if (div_aperto == true) {
                html_risposta += "</div>\n";
                div_aperto = false;
            }
            html_risposta += "</div>\n";
            html_risposta += "</body>\n</html>";

            System.out.println(html_risposta);

            return html_risposta;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void main(String[] args) throws PrinterException, IOException {

        HttpServer server = HttpServer.create(new InetSocketAddress(8000), 0);

        server.createContext("/SPOOLER", new MyHandler());
        server.start();

    }

}
