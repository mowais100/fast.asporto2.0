/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sds;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import static java.awt.print.Printable.NO_SUCH_PAGE;
import static java.awt.print.Printable.PAGE_EXISTS;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.MediaSizeName;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Administrator
 */
public class Stampa implements Printable {

    Document global_document;
    Graphics global_grafica;
    PageFormat global_pf;
    int global_page;

    public Stampa(Document doc) {
        global_document = doc;
    }

    public Graphics2D conversione() {

        Graphics2D g2d = (Graphics2D) global_grafica;
        FontMetrics fm;

        int width;
        int x = 3;
        double position = 0;
        int fontsize = 12;
        int fonttype = Font.PLAIN;
        boolean ul = false;
        String allineamento = "left";

        g2d.setPaint(Color.black);

        g2d.translate(global_pf.getImageableX(), global_pf.getImageableY());

        double larghezza_foglio = global_pf.getPaper().getWidth();

        global_document.getDocumentElement().normalize();

        NodeList nList = global_document.getElementsByTagName("epos-print");

        NodeList eposprint_child = nList.item(0).getChildNodes();

        for (int temp = 0; temp < eposprint_child.getLength(); temp++) {

            Node nNode = eposprint_child.item(temp);

            if (nNode.getNodeName() != "#text") {

                //System.out.print("E " + nNode.getNodeName() + " E\n");
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;

                    if (nNode.getNodeName() == "text") {
                        if (eElement.getTextContent().length() > 0) {
                            //html_risposta += "<span style=\"" + position + fontsize + em + ul + "\" >";

                            String[] array_linee = eElement.getTextContent().split("\n", -1);

                            g2d.setFont(new Font("Arial Narrow", fonttype, fontsize));

                            int i = 0;
                            for (String element : array_linee) {

                                if (position > 0) {
                                    x = (int) Math.round(position);
                                }

                                fm = g2d.getFontMetrics(g2d.getFont());
                                width = fm.stringWidth(element);
                                
                                if ("center".equals(allineamento)) {                                  
                                    x = 210 / 2 - width / 2;
                                }

                                g2d.drawString(element, x, fontsize+2);                                

                                if (ul == true) {
                                    g2d.drawLine(x, 17, x + width, 17);
                                }

                                x += width;

                                i++;
                                if (i < array_linee.length) {
                                    g2d.translate(0, fontsize+2);
                                    x = 3;

                                }
                                position = 0;

                            }

                        } else {

                            if (eElement.getAttribute("x").length() > 0) {

                                int val_x = Integer.parseInt(eElement.getAttribute("x"));

                                if (val_x < 100) {
                                    position = val_x / 2;
                                } else {
                                    position = val_x / 3;
                                }

                            }

                            if ("2".equals(eElement.getAttribute("height")) && "2".equals(eElement.getAttribute("width"))) {
                                fontsize = 18;
                            } else if ("1".equals(eElement.getAttribute("height")) && "1".equals(eElement.getAttribute("width"))) {
                                fontsize = 12;
                            }

                            if (eElement.getAttribute("align").length() > 0) {

                                allineamento = eElement.getAttribute("align");
                                
                                x=3;

                            }

                            if (eElement.getAttribute("em").length() > 0) {
                                if ("true".equals(eElement.getAttribute("em"))) {
                                    fonttype = Font.BOLD;
                                } else {
                                    fonttype = Font.PLAIN;

                                }
                            }

                            if (eElement.getAttribute("ul").length() > 0) {
                                if ("true".equals(eElement.getAttribute("ul"))) {
                                    ul = true;

                                } else {
                                    ul = false;
                                }
                            }
                        }

                    } else if ("feed".equals(nNode.getNodeName())) {

                        int numero_linee = Integer.parseInt(eElement.getAttribute("line"));

                        for (int i = 0; i < numero_linee; i++) {
                            g2d.translate(0, fontsize+2);
                            x = 3;
                            position = 0;
                        }

                    }
                }

            }
        }

        /*
        //CENTRATO
        g2d.setFont(new Font("Arial Narrow", Font.BOLD, 24));
        g2d.setPaint(Color.black);

        fm = g2d.getFontMetrics(g2d.getFont());
        width = fm.stringWidth("TITOLO");
        x = 220 / 2 - width / 2;
        g2d.drawString("TITOLO", x, 20);
        g2d.translate(0, 15);
        g2d.translate(0, 15);

        g2d.setFont(new Font("Arial Narrow", Font.PLAIN, 12));
        g2d.setPaint(Color.black);

        //SINISTRA
        fm = g2d.getFontMetrics(g2d.getFont());
        g2d.drawString("COMANDA", 3, 15);
       

        //DESTRA
         g2d.translate(0, 15);
        width = fm.stringWidth("€2.00");
        x = 200 - width;
        g2d.drawString("€2.00", x, 0);

        //SINISTRA
        fm = g2d.getFontMetrics(g2d.getFont());
        g2d.drawString("SUCCO ALBICOCCA", 3, 15);
        g2d.translate(0, 15);

        //DESTRA
        width = fm.stringWidth("€20.00");
        x = 200 - width;
        g2d.drawString("€20.00", x, 0);

        //SINISTRA       
        fm = g2d.getFontMetrics(g2d.getFont());
        g2d.drawString("SUCCO ACE", 3, 15);
        g2d.translate(0, 15);
        //DESTRA
        width = fm.stringWidth("€240.35");
        x = 200 - width;
        g2d.drawString("€240.35", x, 0);
         */
        return g2d;
    }

    @Override
    public int print(Graphics grafica, PageFormat pf, int page) throws PrinterException {

        if (page > 0) {
            /* We have only one page, and 'page' is zero-based */
            return NO_SUCH_PAGE;
        }

        global_grafica = grafica;
        global_pf = pf;
        global_page = page;

        Graphics2D g2d = conversione();

        return PAGE_EXISTS;
    }

    public void printer_job() {
        PrinterJob job = PrinterJob.getPrinterJob();
        PageFormat pf = job.defaultPage();
        Paper paper = new Paper();

        double margin = 9;
        paper.setImageableArea(margin, margin, paper.getWidth() - margin * 2, paper.getHeight() - margin * 2);
        pf.setPaper(paper);

        job.setPrintable(this, pf);

        PrintService[] service = PrinterJob.lookupPrintServices(); // list of printers
        DocPrintJob docPrintJob = null;

        int count = service.length;
        PrintService service_1 = null;

        for (int i = 0; i < count; i++) {
            if (service[i].getName().equalsIgnoreCase("BAR")) {
                docPrintJob = service[i].createPrintJob();
                service_1 = service[i];
                i = count;
            }
        }

        PrintRequestAttributeSet aset = new HashPrintRequestAttributeSet();
        aset.add(MediaSizeName.ISO_A4);

        Insets margins = new Insets(-10, 0, 0, 0);

        try {

            job.setPrintService(service_1);
        } catch (PrinterException ex) {
            Logger.getLogger(SDS.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            job.print();
        } catch (PrinterException ex) {
            Logger.getLogger(SDS.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
