/* global comanda, classe_ping */

function test_server_centrale_ON(tab, callback) {

    $.ajax({
        url: comanda.url_server + 'demo_api_html5/sqlite_json.lib.php',
        type: "POST",
        timeout: 5000,
        success: function () {
            callback(true);
        },
        error: function () {

            if (comanda.syncerror !== true || tab === "comanda") {
                comanda.syncerror = true;
                alert("Errore!\n\nTABELLA: " + tab + "(1)\n\nEffettua questi controlli:\n\n\- Che la stampante, misuratore fiscale o server che contiene il database centrale sia acceso da almeno 2 minuti (per sicurezza).\n\n- Di essere connesso alla rete WiFi corretta.\n\n- Che il misuratore fiscale abbia fatto il TEST\n\nSe non effettuerai queste operazioni, utilizzeremo i DATI PRECEDENTI, e probabilmente potresti avere dei problemi.\n\nNB In alcuni casi potrebbe solamente trattarsi di una lentezza nella risposta del server.");
            }

            callback(false);
        }
    });
}

function test_pagina_cgi(indirizzo, callback) {

    console.log("TEST PAGINA CGI", indirizzo);

    //Vede in che posizione è il ? della richiesta GET
    var indice_get = indirizzo.indexOf('?');

    //taglia al punto di domanda l'indirizzo
    var pagina_grezza = indirizzo.slice(0, indice_get).trim();

    //Genera la richiesta con il callback
    //"http://192.168.0.140/cgi-bin/epos/service.cgi"

    $.ajax({
        url: pagina_grezza,
        type: "POST",
        timeout: 4000,
        success: function () {
            callback(true);
        },
        error: function () {
            callback(false);
        }

    });

    /* var risposta_xml;
     
     var address = indirizzo.trim();
     
     var builder = new epson.ePOSBuilder();
     
     var request = builder.toString();
     
     var soap = '<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body><epos-print xmlns="http://www.epson-pos.com/schemas/2011/03/epos-print"></epos-print></s:Body></s:Envelope>';
     
     var xhr = new XMLHttpRequest();
     
     xhr.open('POST', address, true);
     //Settaggio headers
     xhr.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
     xhr.setRequestHeader('If-Modified-Since', 'Thu, 01 Jan 1970 00:00:00 GMT');
     xhr.setRequestHeader('SOAPAction', '""');
     //DEV'ESSERE UGUALE AL TIMEOUT SI STAMPA ALTRIMENTI SI BLOCCA PRIMA E RISTAMPA
     xhr.timeout = 30000;
     
     xhr.send(soap);
     
     xhr.onreadystatechange = function () {
     if (xhr.readyState === 4 && xhr.status === 200) {
     risposta_xml = xhr.responseXML;
     //console.log(xhr);
     //Prendo i dati per capire dalla risposta se ci sono errori, e quali sono
     var dati_importanti_risposta = risposta_xml.getElementsByTagName("response")[0];
     //Variabile che dice se è andato tutto a buon fine
     var buon_fine = dati_importanti_risposta.attributes.success.nodeValue;
     //Variabile che dice se qual'è il codice d'errore, se la variabile buon_fine è false
     var codice_errore = dati_importanti_risposta.attributes.code.nodeValue;
     
     if(buon_fine==="true"){
     
     callback(true);
     
     }
     
     }else
     {
     callback(false);
     }       
     };*/

}









function pong(url, callback) {


    if (comanda.server_cloud !== true) {
        var a = document.createElement('a');
        a.href = url;
        var host = a.hostname;
        var port = a.port;

        let arr = comanda.nome_stampante.filter(function (innerArr) {
            if (innerArr.ip !== undefined && innerArr.ip !== null && typeof innerArr.ip === "string" && innerArr.ip.trim() !== "") {
                return !this[innerArr.ip] && (this[innerArr.ip] = true);
            }
        }, {}).map(v => {

            if (v.fiscale === "n") {
                return {ip: "http://" + v.ip + "/cgi-bin/epos/service.cgi", type: "OPTIONS"};
            } else
            {
                return {ip: "http://" + v.ip + "/cgi-bin/fpmate.cgi?devid=local_printer&timeout=2000", type: "OPTIONS"};
            }

        });

        var callback_done = false;

        var promises = [];

        arr.push({ip: comanda.indirizzo_access_point, type: "GET"});
        arr.push({ip: comanda.url_server + "classi_php/cwd.php", type: "GET"});

        for (let v of arr) {
            /* $.ajax returns a promise*/
            promises.push($.ajax({

                url: v.ip,
                type: v.type,
                timeout: 1000

            }).then(function (data) {
                callback_done = true;
                return true;
            },
                    function (err) {
                        console.log("ERRORE PING", v.ip, err.status, err.statusText);
                        // convert failure into success so $.when() doesn't stop
                        return $.Deferred().resolve('err');
                    }
            ));
        }

        $.when.apply(null, promises).done(function () {
            callback(callback_done);
        });

    } else
    {
        callback(true);
    }
}

