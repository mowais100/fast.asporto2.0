function salva_utente()
{
    var scadenza = new Date();
    var adesso = new Date();
    var md5 = +new Date();
    md5 = CryptoJS.MD5(md5.toString()).toString();
    scadenza.setTime(adesso.getTime() + (24 * 60 * 60000));
    document.cookie = "UTENTE" + '=' + md5 + '; expires=' + scadenza.toGMTString() + '; path=/';

    return md5;
}





var fine_conto_attivo = function (callBACCO) {

    var obj2 = alasql("select tavolotxt from tavoli where numero='" + comanda.tavolo.replace(/^0+/, '') + "' limit 1;");
    if (obj2 !== undefined && obj2[0] !== undefined && obj2[0].tavolotxt !== undefined) {
        $('#numero_tavolo').html(obj2[0].tavolotxt);
    }


    $('#intestazioni_conto_separato_grande').show();
    //console.log(indice, array_numero_righe, indice === array_numero_righe);
    $('#intestazioni_conto_separato_2_grande').show();
    //console.log("TOTALE REINSERITO");

    calcola_totale();

    if (comanda.mobile === true || comanda.pizzeria_asporto === true) {
        ultime_battiture();
    }

    if (typeof (callBACCO) === "function") {
        console.log("CALLBACK CONTO ATTIVO");
        callBACCO(true);
    }

};

comanda.funzionidb.conto_attivo = function (callBACCO) {

    //CANCELLA RIGHE DAL CONTO ATTIVO
    $('.ultime_battiture tr,#intestazioni_conto_separato_grande tr,#intestazioni_conto_separato_2_grande tr,#conto tr,#intestazioni_conto tr,.tab_conto_grande tr,#intestazioni_conto_grande tr,.tab_conto_separato_grande tr,.tab_conto_separato_2_grande tr').remove();
    //CREATE TABLE COMANDA
    //Crea tabella comanda

    if (comanda.tavolo !== 0) {

        //LA QUERY DEV'ESSERE MODIFICATA IN MODO DI RAGGRUPPARE I null, 'null', 1 in uno e 2 in un altro
        //var query_numero_conto = "select distinct numero_conto FROM comanda WHERE stato_record='ATTIVO' AND ntav_comanda='" + comanda.tavolo + "' ORDER BY numero_conto ASC;";

        var p1 = new Promise(function (resolve, reject) {
            var query_numero_conto = "select distinct numero_conto from comanda where  (numero_conto=\"null\" OR numero_conto IS NULL OR numero_conto=\"1\" OR numero_conto=\"\") and ntav_comanda=\"" + comanda.tavolo + "\" and stato_record=\"ATTIVO\" group by ntav_comanda\n\
                                            union all\n\
                                        select distinct numero_conto from comanda where  numero_conto=\"2\" and ntav_comanda=\"" + comanda.tavolo + "\" and stato_record=\"ATTIVO\" group by ntav_comanda;";
            console.log("conto_attivo", query_numero_conto);
            comanda.totale_conto = new Object();
            //AL CONTO
            comanda.sincro.query(query_numero_conto, function (num_conto) {
                resolve(num_conto);
            });
        });


        p1.then(function (num_conto) {
            //SE C'E' SOLO UN CONTO CANCELLA L'ALTRO CHE NON SERVE
            if (comanda.mobile === true && num_conto.length === 1 && (num_conto[0].numero_conto === '1'))
            {
                $('#div_conto_separato_2').hide();
            } else
            {
                $('#div_conto_separato_2').show();
            }

            var array_numero_righe = num_conto.length - 1;
            for (var indice in num_conto) {

                var db_num_conto = num_conto[indice];


                if (db_num_conto.numero_conto !== undefined) {
                    var numero_conto;
                    var nc_query;
                    switch (db_num_conto.numero_conto) {

                        case '2':
                        case 2:
                            nc_query = " (numero_conto='2' OR numero_conto=2) ";
                            numero_conto = "2";
                            break;

                        case '':
                        case 'undefined':
                        case null:
                        case 'null':
                        case '1':
                        case 1:
                        default:
                            nc_query = " (numero_conto='' OR numero_conto='undefined' OR numero_conto is null OR numero_conto='null' OR numero_conto='1' OR numero_conto=1) ";
                            numero_conto = "1";
                            break;
                    }

                    //BLOCCO
                    var p2 = new Promise(function (resolve, reject) {

                        var testo_query = "select  substr(desc_art,1,3)||'M'|| substr(nodo,1,3) as ord,ora_,rag_soc_cliente,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,(select ord_fastorder from nomi_portate where numero=portata) as portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita from comanda as c1 where contiene_variante='1' and ntav_comanda='" + comanda.tavolo + "'  and posizione='CONTO'  and (trim(QTAP)='0' or trim(QTAP) is null or trim(QTAP)='') and stato_record='ATTIVO' and  " + nc_query + "  \n\
                                        union all\n\
                                        select  substr(desc_art,1,3)||'M'||substr(nodo,1,3) as ord,ora_,rag_soc_cliente,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,(select ord_fastorder from nomi_portate where numero=portata) as portata,categoria,prog_inser,nodo,desc_art,prezzo_un,sum(quantita) as quantita from comanda as c1 where (contiene_variante !='1' or contiene_variante='null' or contiene_variante is null) and length(nodo)=3 and ntav_comanda='" + comanda.tavolo + "' and posizione='CONTO' and (trim(QTAP)='0' or trim(QTAP) is null or trim(QTAP)='')  and stato_record='ATTIVO'  and " + nc_query + "  group by portata,desc_art,nome_comanda\n\
                                        union all		\n\
                                        select  (select substr(desc_art,1,3) from comanda where nodo=substr(c1.nodo,1,3)  and (trim(QTAP)='0' or trim(QTAP) is null or trim(QTAP)='') and stato_record='ATTIVO'  and ntav_comanda='" + comanda.tavolo + "')||'M' ||substr(nodo,1,3) as ord,ora_,rag_soc_cliente,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,(select ord_fastorder from nomi_portate where numero=portata) as portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita from comanda as c1 where length(nodo)=7 and substr(desc_art,1,1)='-' and ntav_comanda='" + comanda.tavolo + "' and posizione='CONTO' and (trim(QTAP)='0' or trim(QTAP) is null or trim(QTAP)='')  and stato_record='ATTIVO' and " + nc_query + " \n\
                                        union all\n\
                                        select (select substr(desc_art,1,3) from comanda where nodo=substr(c1.nodo,1,3)  and (trim(QTAP)='0' or trim(QTAP) is null or trim(QTAP)='') and stato_record='ATTIVO'  and ntav_comanda='" + comanda.tavolo + "')||'M'||substr(nodo,1,3) as ord,ora_,rag_soc_cliente,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,(select ord_fastorder from nomi_portate where numero=portata) as portata,categoria,prog_inser,nodo,desc_art,prezzo_un,quantita from comanda as c1 where length(nodo)=7 and substr(desc_art,1,1)!='-' and ntav_comanda='" + comanda.tavolo + "' and posizione='CONTO' and (trim(QTAP)='0' or trim(QTAP) is null or trim(QTAP)='') and stato_record='ATTIVO' and " + nc_query + "  \n\
                                        order by nome_comanda DESC, ord ASC\n\
                                        ;";

                        console.log("QUERY CONTO ATTIVO", testo_query);

                        comanda.sincro.query(testo_query, function (d1) {

                            console.log("conto_attivo", d1);
                            if (d1 !== undefined && d1[0] !== undefined && d1[0].rag_soc_cliente !== undefined && d1[0].rag_soc_cliente !== null && d1[0].rag_soc_cliente.length > 0)
                            {
                                comanda.ultimo_id_cliente = d1[0].rag_soc_cliente;
                                var ora = $('.' + comanda.ultimo_id_asporto + ' td:first-child').html();
                                $('[name="ora_consegna"]').val(ora);
                            }

                            if (d1 !== undefined && d1[0] !== undefined && d1[0].ora_ !== undefined && d1[0].ora_ !== null && d1[0].ora_.length > 0 && d1[0].ora_.indexOf(':') !== -1) {
                                if (comanda.tavolo === "BANCO" || comanda.tavolo === "ASPORTO" || comanda.tavolo === "TAKE AWAY") {

                                    comanda.ora_consegna = d1[0].ora_.split(':')[0];
                                    comanda.minuti_consegna = d1[0].ora_.split(':')[1];
                                }
                            }

                            if (d1 !== undefined && d1[0] !== undefined && d1[0].numero_conto && d1[0].numero_conto === '2') {
                                $('#conto,.ultime_battiture').append('<tr class="non-contare-riga" id="righe_conto_' + d1[0].numero_conto + '"><td colspan="4"><center><strong> - - - CONTO ' + d1[0].numero_conto + ' - - - </strong></center></td></tr>');
                                $('.tab_conto_grande').append('<tr class="non-contare-riga"><td colspan="4"><center><strong> - - - CONTO ' + d1[0].numero_conto + ' - - - </strong></center></td></tr>');
                            }

                            var elemento_comanda = new Object();
                            //SERVE A DARE UN ORDINE MIGLIORE ALLE VARIANTI
                            //E' NECESSARISSIMO
                            var ord = 0;
                            d1.forEach(function (attivo) {

                                console.log("prodotto", attivo);
                                if (comanda.totale_conto[comanda.conto] === undefined) {
                                    comanda.totale_conto[comanda.conto] = new Object();
                                }

                                if (comanda.totale_conto[comanda.conto][attivo.perc_iva] === undefined) {
                                    comanda.totale_conto[comanda.conto][attivo.perc_iva] = new Object();
                                    comanda.totale_conto[comanda.conto][attivo.perc_iva].iva = 0;
                                    comanda.totale_conto[comanda.conto][attivo.perc_iva].netto = 0;
                                    comanda.totale_conto[comanda.conto][attivo.perc_iva].mwst = 0;
                                    comanda.totale_conto[comanda.conto][attivo.perc_iva].gesamt = 0;
                                }

                                comanda.totale_conto[comanda.conto][attivo.perc_iva].iva = parseInt(attivo.perc_iva);
                                comanda.totale_conto[comanda.conto][attivo.perc_iva].netto += parseFloat(((attivo.prezzo_un / 100 * (100 - parseInt(attivo.perc_iva)))) * parseInt(attivo.quantita));
                                comanda.totale_conto[comanda.conto][attivo.perc_iva].gesamt += parseFloat((attivo.prezzo_un * attivo.quantita));
                                comanda.totale_conto[comanda.conto][attivo.perc_iva].mwst = parseFloat((comanda.totale_conto[comanda.conto][attivo.perc_iva].gesamt / 100 * attivo.perc_iva));
                                console.log(attivo);
                                if (elemento_comanda[attivo.portata] === undefined)
                                {

                                    elemento_comanda[attivo.portata] = new Object();
                                }



                                //ORDINANDO PER ORD RESTANO IN ORDINE ALFABETICO GLI ARTICOLI PER OGNI PORTATA
                                if (elemento_comanda[attivo.portata][attivo.ord + "_" + ord] === undefined) {
                                    //Diventa come (che equivale a dire uguale) attivo
                                    elemento_comanda[attivo.portata][attivo.ord + "_" + ord] = new Object();
                                    elemento_comanda[attivo.portata][attivo.ord + "_" + ord] = attivo;
                                }
                                ord++;
                            });
                            console.log("ELEMENTO COMANDA", elemento_comanda);
                            for (var portata in elemento_comanda) {
                                console.log("conto_attivo - analisi - portata", portata);
                                //CONDIZIONI PER MOSTRARE GLI ARTICOLI, VARIANTI, STAMPATI, SERVITI, ECCETERA...


                                for (var indice_articolo in elemento_comanda[portata]) {

                                    //ATTIVO RIPRENDE I SUOI ATTRIBUTI
                                    var attivo = elemento_comanda[portata][indice_articolo];
                                    console.log("conto_attivo - analisi - art", attivo.desc_art, attivo.quantita);
                                    console.log("funzionidb conto_attivo", attivo);




                                    //PER IL PC CHE HA GIA' STAMPATO LA COMANDA

                                    console.log("CA ARTICOLO PRINC", attivo.desc_art, attivo.desc_art[0]);
                                    //SE L'ARTICOLO E' UNA VARIANTE
                                    if (attivo.desc_art[0] === "+" || attivo.desc_art[0] === "-" || attivo.desc_art[0] === "(" || attivo.desc_art[0] === "*" || attivo.desc_art[0] === "x" || attivo.desc_art[0] === "=") {


                                        $('#conto,.ultime_battiture').append('<tr style="height:0;" class="non-contare-riga"></tr>');
                                        $('.tab_conto_grande').append('<tr style="height:0;" class="non-contare-riga"></tr>');
                                        if (attivo.numero_conto === '1' || attivo.numero_conto === '' || attivo.numero_conto === 'null' || attivo.numero_conto === null)
                                        {
                                            $('.tab_conto_separato_grande').append('<tr style="height:0;" class="non-contare-riga"></tr>');
                                            $('.tab_conto_separato_grande').append('<tr class="comanda' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" ><td>' + attivo.quantita + '</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td>&euro;' + attivo.prezzo_un + '</td></tr>');
                                        } else
                                        {
                                            $('.tab_conto_separato_2_grande').append('<tr style="height:0;" class="non-contare-riga"></tr>');
                                            $('.tab_conto_separato_2_grande').append('<tr class="comanda' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" ><td>' + attivo.quantita + '</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td>&euro;' + attivo.prezzo_un + '</td></tr>');
                                        }
                                        console.log("CA ARTICOLO VARIANTE", attivo.desc_art[0]);
                                        $('#conto').append('<tr class="comanda ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" ><td id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td id="art_' + attivo.prog_inser + '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td id="art_' + attivo.prog_inser + '">&euro;' + attivo.prezzo_un + '</td></tr>');
                                        $('.tab_conto_grande').append('<tr class="comanda ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" ><td id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td id="art_' + attivo.prog_inser + '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td id="art_' + attivo.prog_inser + '">&euro;' + attivo.prezzo_un + '</td></tr>');
                                    }
                                    //ALTRIMENTI LI STAMPA IN MODO NORMALE
                                    else
                                    {
                                        console.log("CA ARTICOLO NON VARIANTE", attivo.desc_art);
                                        $('#conto').append('<tr class="comanda ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" ><td>' + attivo.quantita + '</td><td id="art_' + attivo.prog_inser + '">' + attivo.desc_art + '</td><td id="art_' + attivo.prog_inser + '">&euro;' + attivo.prezzo_un + '</td></tr>');
                                        $('.tab_conto_grande').append('<tr class="comanda ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" ><td id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td id="art_' + attivo.prog_inser + '">' + attivo.desc_art + '</td><td id="art_' + attivo.prog_inser + '">&euro;' + attivo.prezzo_un + '</td></tr>');
                                        if (attivo.numero_conto === '1' || attivo.numero_conto === '' || attivo.numero_conto === 'null' || attivo.numero_conto === null)
                                        {
                                            $('.tab_conto_separato_grande').append('<tr  class="comanda' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '"><td>' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td>&euro;' + attivo.prezzo_un + '</td></tr>');
                                        } else {
                                            $('.tab_conto_separato_2_grande').append('<tr  class="comanda' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '"><td>' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td>&euro;' + attivo.prezzo_un + '</td></tr>');
                                        }
                                    }



                                }
                                //FINE CICLO FOR IN 1
                            }
                            //FINE CICLO FOR IN 2
                            resolve(true);
                        });
                    });
                    //FINE BLOCCO


                    //BLOCCO
                    var p3 = new Promise(function (resolve, reject) {
                        testo_query = "SELECT numero_conto,nodo,prog_inser,sum(quantita) as quantita,desc_art,prezzo_un FROM comanda WHERE " + nc_query + " AND stato_record='ATTIVO' AND posizione='SCONTO' AND ntav_comanda='" + comanda.tavolo + "' GROUP BY numero_conto,prezzo_un ORDER BY nodo ASC;";
                        comanda.sincro.query(testo_query, function (d2) {

                            d2.forEach(function (attivo) {
                                $('#intestazioni_conto').prepend('<tr id="art_' + attivo.prog_inser + '"><td>' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td>' + attivo.prezzo_un + '</td></tr>');
                                $('#intestazioni_conto_grande').prepend('<tr id="art_' + attivo.prog_inser + '"><td style="width:10%;">' + attivo.quantita + '</td><td style="width:55%;">' + attivo.desc_art + '</td><td  style="width:25%;">' + attivo.prezzo_un + '</td></tr>');
                                if (attivo.numero_conto === '1' || attivo.numero_conto === '' || attivo.numero_conto === 'null' || attivo.numero_conto === null) {
                                    $('#intestazioni_conto_separato_grande').prepend('<tr id="art_' + attivo.prog_inser + '"><td onclick="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:10%;"  >' + attivo.quantita + '</td><td onclick="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:55%;" >' + attivo.desc_art + '</td><td onclick="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:25%;" >' + attivo.prezzo_un + '</td></tr>');
                                } else
                                {
                                    $('#intestazioni_conto_separato_2_grande').prepend('<tr id="art_' + attivo.prog_inser + '"><td onclick="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:10%;"  >' + attivo.quantita + '</td><td onclick="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:55%;" >' + attivo.desc_art + '</td><td onclick="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:25%;" >' + attivo.prezzo_un + '</td></tr>');
                                }
                            });

                            resolve(true);

                        });
                    });
                    //FINE BLOCCO


                    //BLOCCO
                    var p4 = new Promise(function (resolve, reject) {
                        testo_query = "SELECT numero_conto,nodo,prog_inser,sum(quantita) as quantita,desc_art,prezzo_un FROM comanda WHERE " + nc_query + " AND stato_record='ATTIVO' AND posizione='COPERTO' AND ntav_comanda='" + comanda.tavolo + "' GROUP BY numero_conto ORDER BY nodo ASC;";
                        console.log(testo_query);
                        comanda.sincro.query(testo_query, function (d3) {
                            d3.forEach(function (attivo) {
                                $('#intestazioni_conto').prepend('<tr id="art_' + attivo.prog_inser + '"><td id="numero_coperti_effettivi">' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td>' + attivo.prezzo_un + '</td></tr>');
                                $('#intestazioni_conto_grande').prepend('<tr id="art_' + attivo.prog_inser + '"><td  style="width:10%;" id="numero_coperti_effettivi">' + attivo.quantita + '</td><td  style="width:55%;">' + attivo.desc_art + '</td><td  style="width:25%;">' + attivo.prezzo_un + '</td></tr>');
                                if (attivo.numero_conto === '1' || attivo.numero_conto === '' || attivo.numero_conto === 'null' || attivo.numero_conto === null) {
                                    $('#intestazioni_conto_separato_grande').prepend('<tr id="art_' + attivo.prog_inser + '"><td onclick="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:10%;" id="numero_coperti_effettivi" >' + attivo.quantita + '</td><td onclick="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:55%;" >' + attivo.desc_art + '</td><td onclick="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:25%;" >' + attivo.prezzo_un + '</td></tr>');
                                } else
                                {
                                    $('#intestazioni_conto_separato_2_grande').prepend('<tr id="art_' + attivo.prog_inser + '"><td onclick="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:10%;" id="numero_coperti_effettivi" >' + attivo.quantita + '</td><td onclick="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:55%;" >' + attivo.desc_art + '</td><td onclick="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:25%;" >' + attivo.prezzo_un + '</td></tr>');
                                }
                            });

                            resolve(true);
                        });
                    });
                    //FINE BLOCCO

                }
            }
            //FINE FOREACH
            Promise.all([p2, p3, p4]).then(function () {
                fine_conto_attivo(callBACCO);
            });

        });

    }

};

function once(fn) {

    var run = true;

    var timeout = setTimeout(function () {
        if (run) {
            //fn(false);
            run = false;
            alert("ATTENZIONE! RTEngine bloccati a causa di Problemi di Rete. Non puoi continuare a lavorare in queste condizioni.\n\nAssicurati che la rete funzioni bene, e riavvia il programma");
            $('#popup_attesa_socket').modal('hide');
            $('#messaggi_caricamento').hide();
            $('#loading').hide();
            $('#game-over').modal('show');
            window.close();
        }
    }, 20000);

    return function () {
        if (run) {
            fn.apply(null, arguments);
            //resolve(true);
            clearTimeout(timeout);
            run = false;
        }
    };
}

function invia_ordinazione_da_pagare() {
    var testo_query = "update tavoli set occupato='0',colore='green' where numero='" + comanda.tavolo + "';";

    comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
    comanda.sincro.query(testo_query, function () {

        var testo_query = "update comanda set nome_comanda='" + comanda.parcheggio + "',operatore='INVIATO' where ntav_comanda='" + comanda.tavolo + "' and stato_record='ATTIVO' and operatore='" + comanda.operatore + "';";

        comanda.sock.send({tipo: "aggiornamento_tavoli", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
        comanda.sincro.query(testo_query, function () {

            $('#fullscreen').hide();
            $('#totale_conto').hide();
            $('#tab_sx').hide();
            $('#tab_dx').hide();
            $('#elenco_prodotti').hide();
            $('#ordine_inviato').show();

            $('#prezzo_a_testa').html('€ ' + (parseFloat(comanda.totale_fastorder) / $('#numero_persone').val()).toFixed(2));
        });
    });
}

function elenco_categorie_main() {
    $('#tab_sx > div> a').show();
    $('#tab_sx').css('height', '');
    $('#tab_sx').css('overflow', '');
    $('#elenco_prodotti').hide();
}

function avviso_varianti_fastorder() {
    console.log("evento", event);
    $(event.target).css('opacity', '0.1');
    $(event.target).parent().parent().find('.avviso_fastorder').html('<h4>Vuoi aggiungere qualcosa?</h4><h5 style="font-weight:bold" onclick="risposta_avviso_varianti_fastorder(true)">SI</h5><h5 style="font-weight:bold" onclick="risposta_avviso_varianti_fastorder(false)">NO</h5>');
}

function risposta_avviso_varianti_fastorder(risposta) {

    if (risposta === true) {
        mod_categoria(comanda.ultima_cat_variante, '+');
    }

    console.log("evento", event);

    $(event.target).parent().parent().find('.immagine_fastorder img').css('opacity', '');
    $(event.target).parent().parent().find('.avviso_fastorder').html('');

    event.preventDefault();
    event.stopPropagation();

}

function nome_comanda() {
    comanda.parcheggio = $('#nome_utente_input').val();
    $('#nome_utente').hide();

}



function carrello_fastorder() {
    if (comanda.paginata !== "carrello") {
        comanda.paginata = "carrello";

        $('#tab_sx').hide();
        $('#tab_sx').css('height', 'auto');
        $('#tab_sx').css('overflow', 'hidden');

        $('#elenco_prodotti').hide();

        $('#tab_dx').show();
    } else
    {
        comanda.paginata = "categorie";
        $('#elenco_prodotti').hide();
        $('#tab_dx').hide();

        $('#tab_sx').show();
        $('#tab_sx > div> a').show();
        $('#tab_sx').css('height', '');
        $('#tab_sx').css('overflow', '');
    }
}

function calcola_totale(spooler) {

    var totale = 0.0;
    var quantita = 0;
    var totale_senza_coperti = 0.0;
    var percentuale_sconto = 0;
    //QUESTO CONTA TUTTI QUELLI CON IL VALORE IN EURO E NON PERCENTUALE COMPRESI I COPERTI
    $('.tab_conto').find('tr[id^="art_"]:not(:contains("%"))').each(function () {
        if ($(this).find('td:nth-child(4)').html().replace(',', '.').slice(1).match(/[^€]/g) !== null)
        {
            if ($(this).find('td:nth-child(1)').is('#numero_coperti_effettivi') !== true) {
                var a = parseFloat($(this).find('td:nth-child(4)').html().replace(',', '.').match(/[^€]/g).join(''));
                //Prezzo x quantità se è un prezzo
                totale_senza_coperti += a * $(this).find('td:nth-child(1)').html();
            }
            var a = parseFloat($(this).find('td:nth-child(4)').html().replace(',', '.').match(/[^€]/g).join(''));
            //Prezzo x quantità se è un prezzo
            totale += a * $(this).find('td:nth-child(1)').html();
            quantita += parseInt($(this).find('td:nth-child(1)').html());
        }
    });

    if (parseInt(comanda.percentuale_servizio) > 0 && spooler !== 'spooler') {

        var totale_servizio = totale_senza_coperti / 100 * comanda.percentuale_servizio;
        totale += totale_servizio;

        $('#conto').prepend('<tr id="art_servizio"><td style="color:transparent;">1</td><td>SERVIZIO ' + comanda.percentuale_servizio + ' %</td><td>€' + parseFloat(totale_servizio).toFixed(2) + '</td><td></td></tr>');
        $('.tab_conto_grande').prepend('<tr id="art_servizio"><td style="color:transparent;">1</td><td>SERVIZIO ' + comanda.percentuale_servizio + ' %</td><td>€' + parseFloat(totale_servizio).toFixed(2) + '</td><td></td></tr>');
    }


    //totale - percentuale se il valore è in percentuale (però va calcolato alla fine)
    $('.tab_conto #intestazioni_conto').find('tr[id^="art_"]:contains("%")').each(function () {
        percentuale_sconto += parseFloat($(this).find('td:nth-child(4)').html().replace(',', '.').slice(0, -1));
    });

    var differenza = (totale / 100 * percentuale_sconto) * -1; //così diventa sempre positivo

    totale = totale - differenza;
    //MANCA IL CONTEGGIO SERVIZI SUI CONTI SEPARATI

    $('#totale_conto tr td:nth-child(3)').html('€ ' + totale.toFixed(2));
    $('#totale_conto_grande tr:nth-child(1) td:nth-child(3)').html('€ ' + totale.toFixed(2));

    //$('#totale_conto_grande').show();
    $('#totale_conto tr td:nth-child(3)').attr('id', "totale_scontrino");

    //QUESTO SERVE PER L'ORDINAZIONE AUTOMATICA
    $('#totale_conto>.totale_scontrino').html("€ " + totale.toFixed(2));
    $('#quantita_carrello').html(quantita);
    comanda.totale_fastorder = totale.toFixed(2);

    var totale = 0.0;
    var totale_senza_coperti = 0.0;
    var percentuale_sconto = 0;
    $('.tab_conto_separato_grande,#intestazioni_conto_separato_grande').find('tr[id^="art_"]:not(:contains("%"))').each(function () {
        if ($(this).find('td:nth-child(1)').is('#numero_coperti_effettivi') !== true) {
            var a = parseFloat($(this).find('td:nth-child(4)').html().replace(',', '.').match(/[^€]/g).join(''));
            //Prezzo x quantità se è un prezzo
            totale_senza_coperti += a * $(this).find('td:nth-child(1)').html();
        }

        var a = parseFloat($(this).find('td:nth-child(4)').html().replace(',', '.').match(/[^€]/g).join(''));
        //Prezzo x quantità se è un prezzo
        totale += a * $(this).find('td:nth-child(1)').html();
    });

    if (parseInt(comanda.percentuale_servizio) > 0 && spooler !== 'spooler') {

        var totale_servizio = totale_senza_coperti / 100 * comanda.percentuale_servizio;
        totale += totale_servizio;
        $('.tab_conto_separato_grande').prepend('<tr id="art_servizio"><td style="color:transparent;">1</td><td>SERVIZIO ' + comanda.percentuale_servizio + ' %</td><td>€' + parseFloat(totale_servizio).toFixed(2) + '</td><td></td></tr>');
    }


    //totale - percentuale se il valore è in percentuale (però va calcolato alla fine)
    $('#intestazioni_conto_separato_grande').find('tr[id^="art_"]:contains("%")').each(function () {
        percentuale_sconto += parseFloat($(this).find('td:nth-child(4)').html().replace(',', '.').slice(0, -1));
    });
    var differenza = (totale / 100 * percentuale_sconto) * -1; //così diventa sempre positivo
    var differenza_senza_coperti = (totale_senza_coperti / 100 * percentuale_sconto) * -1;
    totale = totale - differenza;
    $('#totale_conto_separato_grande tr:nth-child(1) td:nth-child(3)').html('€ ' + totale.toFixed(2));
    var totale = 0.0;
    var totale_senza_coperti = 0.0;
    var percentuale_sconto = 0;
    $('.tab_conto_separato_2_grande,#intestazioni_conto_separato_2_grande').find('tr[id^="art_"]:not(:contains("%"))').each(function () {
        if ($(this).find('td:nth-child(4)').html().match(/[^€]/g) !== null)
        {
            if ($(this).find('td:nth-child(1)').is('#numero_coperti_effettivi') !== true) {
                var a = parseFloat($(this).find('td:nth-child(4)').html().replace(',', '.').match(/[^€]/g).join(''));
                //Prezzo x quantità se è un prezzo
                totale_senza_coperti += a * $(this).find('td:nth-child(1)').html();
            }
            var a = parseFloat($(this).find('td:nth-child(4)').html().replace(',', '.').match(/[^€]/g).join(''));
            //Prezzo x quantità se è un prezzo
            totale += a * $(this).find('td:nth-child(1)').html();
        }
    });

    if (parseInt(comanda.percentuale_servizio) > 0 && spooler !== 'spooler') {

        var totale_servizio = totale_senza_coperti / 100 * comanda.percentuale_servizio;
        totale += totale_servizio;
        $('.tab_conto_separato_2_grande').prepend('<tr id="art_servizio"><td style="color:transparent;">1</td><td>SERVIZIO ' + comanda.percentuale_servizio + ' %</td><td>€' + parseFloat(totale_servizio).toFixed(2) + '</td><td></td></tr>');
    }

    //totale - percentuale se il valore è in percentuale (però va calcolato alla fine)
    $('#intestazioni_conto_separato_2_grande').find('tr[id^="art_"]:contains("%")').each(function () {
        percentuale_sconto += parseFloat($(this).find('td:nth-child(4)').html().replace(',', '.').slice(0, -1));
    });

    var differenza = (totale / 100 * percentuale_sconto) * -1; //così diventa sempre positivo
    var differenza_senza_coperti = (totale_senza_coperti / 100 * percentuale_sconto) * -1;
    totale = totale - differenza;
    $('#totale_conto_separato_2_grande tr:nth-child(1) td:nth-child(3)').html('€ ' + totale.toFixed(2));
}

function categoria_cliccata() {
    if (comanda.paginata === "prodotti") {
        comanda.paginata = "categorie";
        elenco_categorie_main();
    } else {
        comanda.paginata = "prodotti";
        $('#tab_sx > div > a').not(event.target).hide();
        $('#tab_sx').css('height', 'auto');
        $('#tab_sx').css('overflow', 'hidden');
        $(event.target).css('background-color', 'rgb(70, 69, 69)');
        $('#elenco_prodotti').show();
    }
}

comanda.xml = new Object();
comanda.xml.salva_file_xml = function (a, b, c, cb) {
    if (typeof (cb) === "function")
        cb(true);
};


$(document).on('change', '#numero_persone', function () {
    $('#prezzo_a_testa').html('€ ' + (parseFloat(comanda.totale_fastorder) / $('#numero_persone').val()).toFixed(2));
});

var click_chiusura = 0;
var timeout_chiusura;
$(document).on('click', '#lista_tavoli_aperti', function () {
    click_chiusura++;
    clearTimeout(timeout_chiusura);

    if (click_chiusura === 5) {
        disegna_tavoli_aperti_fastconto();
        $('#pagina_konto').hide();
        $('#pagina_selezione_tavoli_aperti').show();
    }

    timeout_chiusura = setTimeout(function () {
        click_chiusura = 0;
    }, 2000);

});

//PER ORA E' SOLO PER SIMULAZIONE
function apri_konto(tavolo) {

    comanda.tavolo = tavolo;

    lettura_tavolo_xml(comanda.tavolo, function () {

        comanda.funzionidb.conto_attivo();

        $('#pagina_selezione_tavoli_aperti').hide();
        $('#pagina_konto').show();

    });

}

function disegna_tavoli_aperti_fastconto() {

    comanda.xml.elimina_file_xml("_" + comanda.folder_number + "/COND/Tav" + comanda.tavolo.replace(/^0+/, '') + ".CO1", false, function () {

        $.getJSON("../classi_php/disegna_tavoli_aperti_ibrido.php", {inizio: true, folder_number: comanda.folder_number}, function (res) {
            $('.tavolo_aperto').remove();
            res.forEach(function (obj) {

                comanda.sincro.query("select tavolotxt,numero from tavoli where numero='" + obj.replace(/^0+/, '') + "' limit 1;", function (obj2) {

                    console.log("TAVOLOTXT", obj2[0].tavolotxt);
                    $('#pagina_selezione_tavoli_aperti').append('<div class="tavolo_aperto col-md-2  col-xs-2 text-center" style="border:2px solid #222222;;overflow: hidden;font-family:Droid Serif;font-size:4vh;background-color:grey;color:black;margin-top: 6px;" onclick="apri_konto(\'' + obj + '\');">' + obj2[0].tavolotxt + '</div>');

                });

            });

        });
    });
}

$(document).ready(
        function () {

            $('body').addClass('loading');
            var date = new Date();
            var month = aggZero(date.getUTCMonth() + 1, 2); //months from 1-12
            var day = aggZero(date.getUTCDate(), 2);
            var year = aggZero(date.getUTCFullYear(), 2);
            var newdate = year + "-" + month + "-" + day;
            console.log("BLOCCO1");
            verifica_esistenza_file("/MAGA/MULTILINGUA.ldb?_=" + newdate, function (risultato) {
                if (risultato !== true) {
                    alert("FAST.COMANDA E' SPENTO O LA RETE NON E' RAGGIUNGIBILE!");
                    $('body').removeClass('loading');
                    $('body').addClass('game-over');
                } else {
                    seleziona_servizio(function () {
                        leggi_cartelle_maga_dati(function () {
                            comanda.sincro.sincronizzazione_alasql('settaggi_ibrido', 1, function () {
                                var testo_query = 'SELECT * FROM settaggi_ibrido WHERE 1 limit 1;';
                                comanda.sincro.query(testo_query, function (risultato) {

                                    comanda.sincro.sincronizzazione_alasql('socket_listeners', 1, function () {

                                        comanda.sincro.sincronizzazione_alasql('nomi_portate', 1, function () {

                                            corrispondenze_portate(comanda.lingua);
                                            comanda.sincro.sincronizzazione_alasql('lingue', 1, function () {

                                                comanda.sincro.sincronizzazione_alasql('clienti', 1, function () {

                                                    comanda.sincro.sincronizzazione_alasql('operatori', 1, function () {

                                                        comanda.sincro.sincronizzazione_alasql('categorie', 1, function () {

                                                            comanda.sincro.query("CREATE TABLE IF NOT EXISTS comanda ( `id` TEXT  DEFAULT '', `ora_consegna` TEXT  DEFAULT '', `tipo_consegna` TEXT  DEFAULT '', `numeretto_asp` TEXT  DEFAULT '', `QTA` TEXT  DEFAULT '', `VAR` TEXT  DEFAULT '', `QTAP` TEXT  DEFAULT '', `CAT` TEXT  DEFAULT '', `PRZ` TEXT  DEFAULT '', `LIB2` TEXT  DEFAULT '', `PRG` TEXT  DEFAULT '', `LIB5` TEXT  DEFAULT '', `NSEGN` TEXT  DEFAULT '', `AUTORIZ` TEXT  DEFAULT '', `LIB1` TEXT  DEFAULT '', `LIB3` TEXT  DEFAULT '', `LIB4` TEXT  DEFAULT '', `NCARD5` TEXT  DEFAULT '', `nump_xml` TEXT  DEFAULT '', `jolly` TEXT  DEFAULT '', `tipo_ricevuta` TEXT  DEFAULT '', `progressivo_fiscale` TEXT  DEFAULT '', `ordinamento` TEXT  DEFAULT '', `ultima_portata` TEXT  DEFAULT '', `cod_articolo` TEXT  DEFAULT '', `numero_conto` TEXT  DEFAULT '', `contiene_variante` TEXT  DEFAULT '', `dest_stampa` TEXT  DEFAULT '', `cat_variante` TEXT  DEFAULT '', `tipo_record` TEXT  DEFAULT '', `stato_record` TEXT  DEFAULT '', `planning_colori` TEXT  DEFAULT '', `data_fisc` TEXT  DEFAULT '', `ora_fisc` TEXT  DEFAULT '', `ntav_comanda` TEXT  DEFAULT '', `ntav_attrib` TEXT  DEFAULT '', `tab_bis` TEXT  DEFAULT '', `prog_inser` TEXT  DEFAULT '', `art_primario` TEXT  DEFAULT '', `art_variante` TEXT  DEFAULT '', `tipo_variante` TEXT  DEFAULT '', `desc_art` TEXT  DEFAULT '', `quantita` TEXT  DEFAULT '', `prezzo_un` TEXT  DEFAULT '', `sconto_perc` TEXT  DEFAULT '', `sconto_imp` TEXT  DEFAULT '', `netto` TEXT  DEFAULT '', `imp_tot_netto` TEXT  DEFAULT '', `perc_iva` TEXT  DEFAULT '', `costo` TEXT  DEFAULT '', `autor_sconto` TEXT  DEFAULT '', `categoria` TEXT  DEFAULT '', `gruppo` TEXT  DEFAULT '', `incassato` TEXT  DEFAULT '', `residuo` TEXT  DEFAULT '', `tipo_incasso` TEXT  DEFAULT '', `contanti` TEXT  DEFAULT '', `carta_credito` TEXT  DEFAULT '', `bancomat` TEXT  DEFAULT '', `assegni` TEXT  DEFAULT '', `tessera_prepagata` TEXT  DEFAULT '', `numero_tessera_prepag` TEXT  DEFAULT '', `stampata_sn` TEXT  DEFAULT '', `data_stampa` TEXT  DEFAULT '', `ora_stampa` TEXT  DEFAULT '', `fiscalizzata_sn` TEXT  DEFAULT '', `data_` TEXT  DEFAULT '', `ora_` TEXT  DEFAULT '', `n_fiscale` TEXT  DEFAULT '', `n_conto_parziale` TEXT  DEFAULT '', `nodo` TEXT  DEFAULT '', `portata` TEXT  DEFAULT '', `ult_portata` TEXT  DEFAULT '', `centro` TEXT  DEFAULT '', `terminale` TEXT  DEFAULT '', `operatore` TEXT  DEFAULT '', `cod_cliente` TEXT  DEFAULT '', `rag_soc_cliente` TEXT  DEFAULT '', `coef_trasf` TEXT  DEFAULT '', `peso_x_um` TEXT  DEFAULT '', `peso_ums` TEXT  DEFAULT '', `peso_tot` TEXT  DEFAULT '', `qnt_prod` TEXT  DEFAULT '', `nome_comanda` TEXT  DEFAULT '', `posizione` TEXT  DEFAULT '', `fiscale_sospeso` TEXT  DEFAULT '' , `BIS` TEXT  DEFAULT '',`nome_pc` TEXT  DEFAULT '', `giorno` TEXT NOT NULL DEFAULT '',`cod_promo` TEXT NOT NULL DEFAULT '',`qta_fissa` TEXT NOT NULL DEFAULT '',`spazio_forno` TEXT NOT NULL DEFAULT '',`tipo_impasto` TEXT NOT NULL DEFAULT '',`tasto_segue` TEXT NOT NULL DEFAULT '', `NCARD2` TEXT NOT NULL DEFAULT '', `NCARD3` TEXT NOT NULL DEFAULT '', `NCARD4` TEXT NOT NULL DEFAULT '', `NEXIT` TEXT NOT NULL DEFAULT '',`droppay` TEXT NOT NULL DEFAULT '',`dest_stampa_2` TEXT NOT NULL DEFAULT '',`id_pony` TEXT NOT NULL DEFAULT '',`prezzo_varianti_aggiuntive` TEXT NOT NULL DEFAULT '',`prezzo_varianti_maxi` TEXT NOT NULL DEFAULT '',`prezzo_maxi_prima` TEXT NOT NULL DEFAULT '' );", function () {

                                                                //comanda.sincro.query("CREATE TABLE IF NOT EXISTS 'comanda' ( `id` TEXT  DEFAULT '', `ora_consegna` TEXT  DEFAULT '', `tipo_consegna` TEXT  DEFAULT '', `numeretto_asp` TEXT  DEFAULT '', `QTA` TEXT  DEFAULT '', `VAR` TEXT  DEFAULT '', `QTAP` TEXT  DEFAULT '', `CAT` TEXT  DEFAULT '', `PRZ` TEXT  DEFAULT '', `LIB2` TEXT  DEFAULT '', `PRG` TEXT  DEFAULT '', `LIB5` TEXT  DEFAULT '', `NSEGN` TEXT  DEFAULT '', `AUTORIZ` TEXT  DEFAULT '', `LIB1` TEXT  DEFAULT '', `LIB3` TEXT  DEFAULT '', `LIB4` TEXT  DEFAULT '', `NCARD5` TEXT  DEFAULT '', `nump_xml` TEXT  DEFAULT '', `jolly` TEXT  DEFAULT '', `tipo_ricevuta` TEXT  DEFAULT '', `progressivo_fiscale` TEXT  DEFAULT '', `ordinamento` TEXT  DEFAULT '', `ultima_portata` TEXT  DEFAULT '', `cod_articolo` TEXT  DEFAULT '', `numero_conto` TEXT  DEFAULT '', `contiene_variante` TEXT  DEFAULT '', `dest_stampa` TEXT  DEFAULT '', `cat_variante` TEXT  DEFAULT '', `tipo_record` TEXT  DEFAULT '', `stato_record` TEXT  DEFAULT '', `planning_colori` TEXT  DEFAULT '', `data_fisc` TEXT  DEFAULT '', `ora_fisc` TEXT  DEFAULT '', `ntav_comanda` TEXT  DEFAULT '', `ntav_attrib` TEXT  DEFAULT '', `tab_bis` TEXT  DEFAULT '', `prog_inser` TEXT  DEFAULT '', `art_primario` TEXT  DEFAULT '', `art_variante` TEXT  DEFAULT '', `tipo_variante` TEXT  DEFAULT '', `desc_art` TEXT  DEFAULT '', `quantita` TEXT  DEFAULT '', `prezzo_un` TEXT  DEFAULT '', `sconto_perc` TEXT  DEFAULT '', `sconto_imp` TEXT  DEFAULT '', `netto` TEXT  DEFAULT '', `imp_tot_netto` TEXT  DEFAULT '', `perc_iva` TEXT  DEFAULT '', `costo` TEXT  DEFAULT '', `autor_sconto` TEXT  DEFAULT '', `categoria` TEXT  DEFAULT '', `gruppo` TEXT  DEFAULT '', `incassato` TEXT  DEFAULT '', `residuo` TEXT  DEFAULT '', `tipo_incasso` TEXT  DEFAULT '', `contanti` TEXT  DEFAULT '', `carta_credito` TEXT  DEFAULT '', `bancomat` TEXT  DEFAULT '', `assegni` TEXT  DEFAULT '', `tessera_prepagata` TEXT  DEFAULT '', `numero_tessera_prepag` TEXT  DEFAULT '', `stampata_sn` TEXT  DEFAULT '', `data_stampa` TEXT  DEFAULT '', `ora_stampa` TEXT  DEFAULT '', `fiscalizzata_sn` TEXT  DEFAULT '', `data_` TEXT  DEFAULT '', `ora_` TEXT  DEFAULT '', `n_fiscale` TEXT  DEFAULT '', `n_conto_parziale` TEXT  DEFAULT '', `nodo` TEXT  DEFAULT '', `portata` TEXT  DEFAULT '', `ult_portata` TEXT  DEFAULT '', `centro` TEXT  DEFAULT '', `terminale` TEXT  DEFAULT '', `operatore` TEXT  DEFAULT '', `cod_cliente` TEXT  DEFAULT '', `rag_soc_cliente` TEXT  DEFAULT '', `coef_trasf` TEXT  DEFAULT '', `peso_x_um` TEXT  DEFAULT '', `peso_ums` TEXT  DEFAULT '', `peso_tot` TEXT  DEFAULT '', `qnt_prod` TEXT  DEFAULT '', `nome_comanda` TEXT  DEFAULT '', `posizione` TEXT  DEFAULT '', `fiscale_sospeso` TEXT  DEFAULT '' );", function () {

                                                                comanda.sincro.sincronizzazione_alasql('tavoli', 1, function () {

                                                                    comanda.sincro.sincronizzazione_alasql('impostazioni_fiscali', 1, function () {

                                                                        leggi_coperti(function () {

                                                                            var testo_query = 'CREATE TABLE IF NOT EXISTS tavoli (id,classe,numero,abilitato,altezza,larghezza,pos_x,pos_y,colore,posti,ora_apertura_tavolo,ora_ultima_comanda);';
                                                                            comanda.sincro.query(testo_query, function () {

                                                                                var testo_query = 'SELECT categoria_partenza,lingua_stampa,valore_coperto,coperti_obbligatorio FROM impostazioni_fiscali WHERE 1 limit 1;';
                                                                                comanda.sincro.query(testo_query, function (risultato) {

                                                                                    comanda.lingua_stampa = risultato[0].lingua_stampa;
                                                                                    lng(comanda.lingua);
                                                                                    comanda.valore_coperto = risultato[0].valore_coperto;
                                                                                    comanda.coperti_obbligatorio = risultato[0].coperti_obbligatorio;
                                                                                    if (comanda.coperti_necessari !== true)
                                                                                    {
                                                                                        $('.tasti_coperti_indietro_cel').remove();
                                                                                    }

                                                                                    lngstm(comanda.lingua_stampa, function () {

                                                                                    });
                                                                                    disegna_tavoli_aperti_fastconto();
                                                                                    $('body').removeClass('loading');
                                                                                });
                                                                            });
                                                                        });
                                                                    });
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                }
            });
        });
/*
 $(document).ready(function () {
 
 var md5 = getCookie('UTENTE');
 
 if (md5 === null) {
 md5 = salva_utente();
 }
 comanda.operatore = md5;
 comanda.paginata = "categorie";
 comanda.terminale = "TAVOLO" + comanda.tavolo;
 comanda.ip_address = comanda.tavolo;
 
 lng(comanda.lingua);
 lngstm(comanda.lingua_stampa, function () {});
 
 
 socket_init(function () {
 var testo_query = "update tavoli set occupato='1' where numero='" + comanda.tavolo + "';";
 
 comanda.sincro.query(testo_query, function () {
 
 comanda.sock.send({tipo: "tavolo_occupato", operatore: comanda.operatore, query: testo_query, terminale: comanda.terminale, ip: comanda.ip_address});
 
 comanda.funzionidb.elenco_categorie("list", "1", "0", "#tab_sx");
 
 corrispondenze_stampanti(comanda.lingua_stampa);
 corrispondenze_portate(comanda.lingua_stampa);
 
 comanda.funzionidb.elenco_prodotti(function (cb_elenco_prodotti) {});
 
 comanda.funzionidb.conto_attivo(function () {});
 
 });
 });
 
 });
 */

comanda.sock.waitForSocketConnection = function (socket, callback) {

    if (websocket !== undefined && websocket[0] !== undefined && websocket[0].readyState === 1) {
        if (callback !== null) {
            callback(0);
        }
        return;
    } else if (websocket !== undefined && websocket[1] !== undefined && websocket[1].readyState === 1) {
        if (callback !== null) {
            callback(1);
        }
        return;
    } else if (websocket !== undefined && websocket[2] !== undefined && websocket[2].readyState === 1) {
        if (callback !== null) {
            callback(2);
        }
        return;
    } else
    {
        if (callback !== null) {
            callback(false);
        }
        return;
    }
};
//FA PARTE DELLA FUNZIONE SOTTO
var esperimento_output;
var inizio_elenco_prodotti = false;
comanda.funzionidb.elenco_prodotti = function (callBack) {

    if (inizio_elenco_prodotti === false) {
        inizio_elenco_prodotti = true;
        console.log("INIZIO CALLBACK PRODOTTI");
        esperimento_output = new Object();
        //QUERY SETTAGGIO COLORI TABLET
        var query_colori_tablet = "select Field116 from settaggi_profili limit 1;";
        comanda.sincro.query(query_colori_tablet, function (colori_tablet) {

            //Può essere "true" o "false"
            var colori_tablet_uguali_pc = colori_tablet[0].Field116;
            //QUERY DI SELEZIONE CATEGORIE
            var query_categorie = "select * from categorie;";
            var prodotti = new Array();
            //VARIABILE DI OUTPUT

            comanda.sincro.query(query_categorie, function (categoria) {

                var count_cat_number = categoria.length;
                var cat_n = 1;
                categoria.forEach(function (cat, i1) {

                    //RIEMPIO I DATI DELLE CATEGORIE
                    if (prodotti[cat.id] === undefined) {
                        prodotti[cat.id] = new Object();
                    }

                    //QUERY DI SELEZIONE PRODOTTI
                    //IN QUESTO CASO LA POSIZIONE HA SOLO UN NUMERO E NON DELLE COORDINATE
                    var query_prodotti = '';
                    query_prodotti = 'select ricetta_fastorder, immagine, id, ordinamento, cat_varianti,colore_tasto,descrizione,prezzo_1, categoria, pagina, posizione, perc_iva_base, perc_iva_takeaway  from prodotti WHERE descrizione!="" AND prezzo_1 !="" AND categoria="' + cat.id + '" ORDER BY cast(ordinamento as int) ASC, descrizione ASC;';
                    console.log("PROD QUERY", query_prodotti);
                    //FUNZIONE DELLA QUERY
                    comanda.sincro.query(query_prodotti, function (prodotto) {
                        var i = 0;
                        var pagina = 0;
                        //PER OGNI PRODOTTO VADO A CREARE UNA SERIE DI ARRAY CON VARI DATI
                        prodotto.forEach(function (prod, i2) {

                            //CALCOLA LA PAGINA IN BASE AL NUMERO DI PRODOTTI
                            //Al 50esimo articolo cambia pagina e non 49esimo!

                            //ATTENZIONE A QUESTA CONDIZIONE PUO' ESSERE PERICOLOSA!
                            var max_prodotti = 49;
                            if (cat.descrizione.substr(0, 2) === 'V.')
                            {
                                max_prodotti = 48;
                            }

                            if (i % 42 === 0 && comanda.pizzeria_asporto === true) {
                                pagina++;
                            } else
                            {
                                pagina = 1;
                            }

                            if (prodotti[cat.id][pagina] === undefined) {
                                prodotti[cat.id][pagina] = new Object();
                            }

                            if (prodotti[cat.id][pagina][i] === undefined) {
                                prodotti[cat.id][pagina][i] = new Object();
                            }

                            //DATI PRODOTTO
                            prodotti[cat.id][pagina][i].id = prod.id;
                            prodotti[cat.id][pagina][i].cat_varianti = prod.cat_varianti;
                            prodotti[cat.id][pagina][i].colore_tasto = prod.colore_tasto;
                            prodotti[cat.id][pagina][i].descrizione = prod.descrizione;
                            prodotti[cat.id][pagina][i].prezzo_1 = prod.prezzo_1;
                            prodotti[cat.id][pagina][i].categoria = prod.categoria;
                            prodotti[cat.id][pagina][i].pagina = prod.pagina;
                            prodotti[cat.id][pagina][i].posizione = prod.posizione;
                            prodotti[cat.id][pagina][i].perc_iva_base = prod.perc_iva_base;
                            prodotti[cat.id][pagina][i].perc_iva_takeaway = prod.perc_iva_takeaway;
                            prodotti[cat.id][pagina][i].immagine = prod.immagine;
                            prodotti[cat.id][pagina][i].ricetta_fastorder = prod.ricetta_fastorder;
                            i++;
                        });
                        //PRENDO LE PAGINE NEI PRODOTTI
                        for (var p in prodotti[cat.id]) {
                            if (esperimento_output[cat.id] === undefined)
                            {
                                esperimento_output[cat.id] = new Object();
                            }

                            if (esperimento_output[cat.id][p] === undefined)
                            {
                                esperimento_output[cat.id][p] = '';
                            }

                            esperimento_output[cat.id][p] += "<div class='pag_" + p + " cat_" + cat.id + "' >";
                            esperimento_output[cat.id][p] += "<div class='bs-docs-section btn_COMANDA'>";
                            esperimento_output[cat.id][p] += "<div class='bs-glyphicons'>";
                            esperimento_output[cat.id][p] += "<ul class='bs-glyphicons-list elenco_prodotti_draggable'>";
                            var i = 0;
                            if (cat.descrizione.substr(0, 2) === 'V.' && i === 0) {
                                i++;
                                esperimento_output[cat.id][p] += '<li style="height: 106px !important;background-color:lightblue; color:aliceblue; font-weight: bold; font-size:3em;" value="VARIANTELIBERA" class="btn_variante_libera btn_comanda prodotto_esistente"  onClick="popup_variante_libera();">+</li>';
                            }

                            //PRENDO I PRODOTTI
                            for (var art in prodotti[cat.id][p]) {

                                var articolo = prodotti[cat.id][p][art];
                                var colore = '';
                                console.log("COLORE TASTI" + articolo.colore_tasto);
                                console.log("COLORE TASTI", cat.descrizione.substr(0, 2) === 'V.', comanda.mobile === false && comanda.pizzeria_asporto === false && articolo.colore_tasto !== undefined && articolo.colore_tasto !== null && articolo.colore_tasto.length > 0, comanda.mobile === true && articolo.colore_tasto !== undefined && articolo.colore_tasto !== null && articolo.colore_tasto.length > 0, comanda.pizzeria_asporto === true);
                                //linen
                                var colore_tasto = "linen";
                                colore = ' style="background-color:' + colore_tasto + ';" ';
                                //DIFFERENZA TRA ARTICOLO CON VARIANTE AUTOMATICA E SENZA

                                console.log("ARTICOLO VARIANTE AUTO", articolo, articolo.cat_varianti !== undefined && articolo.cat_varianti !== "undefined" && articolo.cat_varianti !== "undefined/undefined" && articolo.cat_varianti !== null && articolo.cat_varianti.indexOf("/") !== -1 && articolo.cat_varianti.split('/')[1] !== "ND");
                                if (articolo.cat_varianti !== undefined && articolo.cat_varianti !== "undefined" && articolo.cat_varianti !== "undefined/undefined" && articolo.cat_varianti !== null && articolo.cat_varianti.indexOf("/") !== -1 && articolo.cat_varianti.split('/')[1] !== "ND") {
                                    esperimento_output[cat.id][p] += '<li ' + colore + ' value="' + articolo.id + '" class="btn_comanda prodotto_esistente"  onClick="$(\'body\').css(\'pointer-events\', \'none\');setTimeout(function(){aggiungi_articolo(\'' + articolo.id + '\',\'' + articolo.descrizione.replace('\'', "\\'") + '\',\'&euro;' + articolo.prezzo_1 + '\',null,$(\'#quantita_articolo\').val(),null,\'' + articolo.cat_varianti.match(/\w+/g)[0] + '\',null,null,\'' + articolo.perc_iva_base + '\',\'' + articolo.perc_iva_takeaway + '\');if(comanda.dispositivo=== \'TAVOLO SELEZIONATO\'){setTimeout(function(){comanda.ultima_cat_variante=\'' + articolo.cat_varianti + '\';mod_categoria(\'' + articolo.cat_varianti + '\', \'=\',\'\',true);},250);}},100);">';
                                } else
                                {
                                    //SE LA VARIANTE E' NORMALE
                                    esperimento_output[cat.id][p] += '<li ' + colore + ' value="' + articolo.id + '" class="btn_comanda prodotto_esistente"  onClick="$(\'body\').css(\'pointer-events\', \'none\');setTimeout(function(){aggiungi_articolo(\'' + articolo.id + '\',\'' + articolo.descrizione.replace('\'', "\\'") + '\',\'&euro;' + articolo.prezzo_1 + '\',null,\'1\',null,\'' + articolo.cat_varianti + '\',null,null,\'' + articolo.perc_iva_base + '\',\'' + articolo.perc_iva_takeaway + '\');},100);">';
                                }

                                var ricetta_fastorder = '';
                                if (articolo.ricetta_fastorder !== undefined && articolo.ricetta_fastorder !== null) {
                                    ricetta_fastorder = '<br/>' + articolo.ricetta_fastorder;
                                }

                                if (articolo.immagine !== undefined && articolo.immagine !== null && articolo.immagine !== '') {
                                    esperimento_output[cat.id][p] += '<div style="text-align:center;padding:5px;"><div class="avviso_fastorder" style="z-index:1000; position: absolute;text-align: center;padding:5px;width: 95%;"></div><div class="immagine_fastorder" onclick="avviso_varianti_fastorder();"><img style="width:95%;height:auto;" src="./immagini/' + articolo.immagine + '" /></div></strong></div>';
                                    esperimento_output[cat.id][p] += '<div class="glyphicon-class"><strong>' + articolo.descrizione + '</strong> € ' + articolo.prezzo_1 + ricetta_fastorder + '</div>';
                                } else if (articolo.ricetta_fastorder !== undefined && articolo.ricetta_fastorder !== null) {
                                    esperimento_output[cat.id][p] += '<div class="glyphicon-class"><strong>' + articolo.descrizione + '</strong> € ' + articolo.prezzo_1 + ricetta_fastorder + '</div>';
                                } else
                                {
                                    esperimento_output[cat.id][p] += '<div class="glyphicon-class"><strong>' + articolo.descrizione + '</strong><br/>€ ' + articolo.prezzo_1 + ricetta_fastorder + '</div>';
                                }
                                esperimento_output[cat.id][p] += '</li>';
                                i++;
                                console.log("ELENCO PRODOTTI DATI: ", i, pagina);
                            }

                            esperimento_output[cat.id][p] += "</ul>";
                            esperimento_output[cat.id][p] += "</div>";
                            esperimento_output[cat.id][p] += "</div>";
                            esperimento_output[cat.id][p] += "</div>";
                            console.log("ELENCO PRODOTTI DATI " + p + " - NUMERO ARTICOLI: ", prodotti[cat.id][p]);
                        }

                        console.log(cat_n, count_cat_number);
                        if (cat_n === count_cat_number) {
                            console.log("OGGETTO_PRODOTTI", prodotti);
                            inizio_elenco_prodotti = false;
                            callBack(true);
                        }

                        cat_n++;
                    });
                });
            });
        });
    }
};

var RAM = new Object();


var progressivo_ultimo_conto = 0;
comanda.funzionidb.conto_attivo = function (callBACCO, salta_colore) {
    var time1 = 0;/*, time2 = 0, time3 = 0, time4 = 0, time5 = 0, time6 = 0, time7 = 0, time8 = 0, time9 = 0, time10 = 0
     , time11 = 0, time12 = 0, time13 = 0, time14 = 0, time15 = 0, time16 = 0, time17 = 0, time18 = 0, time19 = 0;*/

    time1 = new Date().getTime();
    console.log("TIME_CO1", 0);



    comanda.consegna_presente = false;
    comanda.quantita_pizze_totali = 0;
    comanda.totale_consegna = "0.00";
    comanda.stampata_s = false;
    $('.tasto_numero_tavolo').css('pointer-events', '');
    $('.split_successivo').css('pointer-events', '');
    comanda.disabilita_tasto_bis = false;
    comanda.disabilita_tasto_tavolo = false;

    progressivo_ultimo_conto++;
    var progressivo_ultimo_conto_interno = progressivo_ultimo_conto;
    var inizio_conto = new Date().getTime();
    var numero_conti_attivi = 1;
    console.log("INIZIO CONTO", inizio_conto);
    //CANCELLA RIGHE DAL CONTO ATTIVO
    //CREATE TABLE COMANDA
    //$('.ultime_battiture tr,#intestazioni_conto_separato_grande tr,#intestazioni_conto_separato_2_grande tr,#conto tr,#intestazioni_conto tr,.tab_conto_grande tr,#intestazioni_conto_grande tr,.tab_conto_separato_grande tr,.tab_conto_separato_2_grande tr').remove();

    var ultima_due_gusti = false, ultima_maxi = false, numero_nodo_variante_piu = 0, consegna_presente = false, calcolo_consegna_una_pizza = 0, calcolo_consegna_piu_di_una_pizza = 0, calcolo_consegna_su_totale = 0, calcolo_consegna_mezzo_metro = 0, calcolo_consegna_un_metro = 0, calcolo_consegna_maxi = 0, calcolo_consegna_a_pizza = 0, riga_calcolo_consegna = '', ultime_battiture = '', intestazioni_conto_separato_grande = '', intestazioni_conto_separato_2_grande = '', intestazioni_conto_separato_2_grande_split = '', conto = '', intestazioni_conto = '', tab_conto_grande = '', intestazioni_conto_grande = '', intestazioni_conto_split = '', tab_conto_separato_grande = '', tab_conto_separato_2_grande = '', tab_conto_separato_2_grande_split = '';
    /*comanda.consegna_a_pizza = risultato[0].consegna_a_pizza;
     comanda.consegna_mezzo_metro = risultato[0].consegna_mezzo_metro;
     comanda.consegna_metro = risultato[0].consegna_metro;
     comanda.consegna_su_totale = risultato[0].consegna_su_totale;*/

    if (comanda.pizzeria_asporto === true && (comanda.una_pizza.length > 0 || comanda.piu_di_una_pizza.length > 0 || comanda.consegna_a_pizza.length > 0 || comanda.consegna_mezzo_metro.length > 0 || comanda.consegna_metro.length > 0 || comanda.consegna_maxi.length > 0 || comanda.consegna_su_totale.length > 0)) {
        comanda.consegna_presente = true;
    }


//Crea tabella comanda

    if (comanda.tavolo !== 0) {





        /* PARTE NUOVA */


        comanda.totale_conto = new Object();
        //BLOCCO
        var p2 = new Promise(function (resolve, reject) {

            var esclusione_fiscale_sospeso = '';
            //if (typeof (comanda.tavolo) === 'string' && (comanda.tavolo === "BAR" || comanda.tavolo.indexOf('CONTINUO') !== -1)) {
            esclusione_fiscale_sospeso = ' and fiscale_sospeso!="STAMPAFISCALESOSPESO" ';
            //}



            var groupby = "group by cod_promo,fiscalizzata_sn,portata,tipo_impasto,desc_art,prog_inser,nome_comanda,prezzo_un";
            if (comanda.tasto_contosep === "S" || comanda.multiquantita === "N") {
                groupby = "group by cod_promo,fiscalizzata_sn,portata,tipo_impasto,desc_art,nome_comanda,prezzo_un";
            }

            var testo_query = "select BIS,cod_promo,fiscalizzata_sn,ora_,rag_soc_cliente,perc_iva,nome_comanda,stampata_sn,numero_conto,dest_stampa,portata,categoria,prog_inser,nodo,desc_art,prezzo_un,prezzo_varianti_aggiuntive,prezzo_varianti_maxi,prezzo_maxi_prima,quantita,contiene_variante,tipo_impasto from comanda where desc_art!='RECORD TESTA' and  BIS='" + lettera_conto_split() + "' and ntav_comanda='" + comanda.tavolo + "' and posizione='CONTO'   and (trim(QTAP)='0' or trim(QTAP) is null or trim(QTAP)='') and stato_record='ATTIVO'    " + esclusione_fiscale_sospeso + ";";
            var d1 = alasql(testo_query);

            /*time2 = new Date().getTime();
             console.log("TIME2", time2 - time1);*/
            /*and  (numero_conto='' OR numero_conto='undefined' OR numero_conto is null OR numero_conto='null' OR numero_conto='1' OR numero_conto=1)*/
            /*cod_promo,fiscalizzata_sn,portata,tipo_impasto,desc_art,prog_inser,nome_comanda,prezzo_un*/
            /*contiene_variante,tipo_impasto mancano*/
            alasql("DELETE FROM oggetto_conto");
            alasql("DELETE FROM oggetto_conto_2");
            d1.forEach(function (attivo, i) {

                var ord = "";

                if (comanda.pizzeria_asporto !== true) {
                    if (attivo.stampata_sn === 'S') {
                        ord = "A";
                    } else {
                        ord = "B";
                    }
                }

                if (attivo.contiene_variante === '1')
                {
                    ord += attivo.desc_art.substr(0, 3) + 'M' + attivo.nodo.substr(0, 3);
                } else if (attivo.nodo.length === 7)
                {
                    var query_interna = "select substr(desc_art,1,3) as desc_art_princ from comanda where desc_art!='RECORD TESTA' and  BIS='" + lettera_conto_split() + "' and  nodo='" + attivo.nodo.substr(0, 3) + "'  and (trim(QTAP)='0' or trim(QTAP) is null or trim(QTAP)='') and stato_record='ATTIVO'  and ntav_comanda='" + comanda.tavolo + "' limit 1;";
                    var rqi = alasql(query_interna);
                    if (attivo.desc_art.substr(0, 1) === '-') {
                        //SBAGLIATO (RIVEDERE QUERY C1) 
                        ord += rqi[0].desc_art_princ + 'M' + attivo.nodo;
                    } else if (attivo.desc_art.substr(0, 1) !== '-') {
                        ord += rqi[0].desc_art_princ + 'M' + attivo.nodo;
                    }
                } else if (attivo.contiene_variante !== '1')
                {
                    ord += attivo.desc_art.substr(0, 3) + 'M' + attivo.nodo.substr(0, 3);
                }

                if (attivo.numero_conto === "2") {
                    numero_conti_attivi = 2;
                    alasql.tables.oggetto_conto_2.data.push(new riga_conto_ricostruito(ord, attivo.cod_promo, attivo.fiscalizzata_sn, attivo.ora_, attivo.rag_soc_cliente, attivo.perc_iva, attivo.nome_comanda, attivo.stampata_sn, attivo.numero_conto, attivo.dest_stampa, attivo.portata, attivo.categoria, attivo.prog_inser, attivo.nodo, attivo.desc_art, attivo.prezzo_un, attivo.prezzo_varianti_aggiuntive, attivo.prezzo_varianti_maxi, attivo.prezzo_maxi_prima, attivo.quantita, attivo.contiene_variante, attivo.tipo_impasto));
                } else {
                    alasql.tables.oggetto_conto.data.push(new riga_conto_ricostruito(ord, attivo.cod_promo, attivo.fiscalizzata_sn, attivo.ora_, attivo.rag_soc_cliente, attivo.perc_iva, attivo.nome_comanda, attivo.stampata_sn, attivo.numero_conto, attivo.dest_stampa, attivo.portata, attivo.categoria, attivo.prog_inser, attivo.nodo, attivo.desc_art, attivo.prezzo_un, attivo.prezzo_varianti_aggiuntive, attivo.prezzo_varianti_maxi, attivo.prezzo_maxi_prima, attivo.quantita, attivo.contiene_variante, attivo.tipo_impasto));
                }
            });

            /*time3 = new Date().getTime();
             console.log("TIME3", time3 - time2);*/
//RAGGRUPPAMENTO DELLA QUANTITA SE*/
            /*var groupby = "group by cod_promo,fiscalizzata_sn,portata,tipo_impasto,desc_art,prog_inser,nome_comanda,prezzo_un";
             if (comanda.tasto_contosep === "S" || comanda.multiquantita === "N") {
             groupby = "group by cod_promo,fiscalizzata_sn,portata,tipo_impasto,desc_art,nome_comanda,prezzo_un";
             }
             
             var group = alasql("select * from oggetto_conto where contiene_variante!='1' " + groupby);
             alasql.tables.oggetto_conto.data = group;*/

            var groupby = 1;
            if (comanda.tasto_contosep === "S" || comanda.multiquantita === "N") {
                groupby = 2;
            }


            var raggruppamento = new Object();
            alasql("select * from oggetto_conto where contiene_variante!='1' and length(nodo)=3;").forEach(function (e) {

                if (e.stampata_sn !== "S") {
                    e.stampata_sn = "N";
                }

                if (groupby === 1) {

                    if (raggruppamento[e.cod_promo] === undefined) {
                        raggruppamento[e.cod_promo] = new Object();
                    }

                    if (raggruppamento[e.cod_promo][e.fiscalizzata_sn] === undefined) {
                        raggruppamento[e.cod_promo][e.fiscalizzata_sn] = new Object();
                    }

                    if (raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn] === undefined) {
                        raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn] = new Object();
                    }

                    if (raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata] === undefined) {
                        raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata] = new Object();
                    }

                    if (raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto] === undefined) {
                        raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto] = new Object();
                    }

                    if (raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art] === undefined) {
                        raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art] = new Object();
                    }

                    if (raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.prog_inser] === undefined) {
                        raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.prog_inser] = new Object();
                    }

                    if (raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.prog_inser][e.nome_comanda] === undefined) {
                        raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.prog_inser][e.nome_comanda] = new Object();
                    }


                    if (raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.prog_inser][e.nome_comanda][e.prezzo_un] === undefined) {
                        e.quantita = parseInt(e.quantita);
                        raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.prog_inser][e.nome_comanda][e.prezzo_un] = e;
                    } else {
                        raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.prog_inser][e.nome_comanda][e.prezzo_un].quantita += parseInt(e.quantita);
                    }


                } else if (groupby === 2) {
                    if (raggruppamento[e.cod_promo] === undefined) {
                        raggruppamento[e.cod_promo] = new Object();
                    }

                    if (raggruppamento[e.cod_promo][e.fiscalizzata_sn] === undefined) {
                        raggruppamento[e.cod_promo][e.fiscalizzata_sn] = new Object();
                    }

                    if (raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn] === undefined) {
                        raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn] = new Object();
                    }

                    if (raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata] === undefined) {
                        raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata] = new Object();
                    }

                    if (raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto] === undefined) {
                        raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto] = new Object();
                    }

                    if (raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art] === undefined) {
                        raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art] = new Object();
                    }



                    if (raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.nome_comanda] === undefined) {
                        raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.nome_comanda] = new Object();
                    }


                    if (raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.nome_comanda][e.prezzo_un] === undefined) {
                        e.quantita = parseInt(e.quantita);
                        raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.nome_comanda][e.prezzo_un] = e;
                    } else {
                        raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.nome_comanda][e.prezzo_un].quantita += parseInt(e.quantita);
                    }
                }
            });

            /*time4 = new Date().getTime();
             console.log("TIME4", time4 - time3);*/

            alasql("delete from oggetto_conto where contiene_variante!='1' and length(nodo)=3;");

            /*time5 = new Date().getTime();
             console.log("TIME5", time5 - time4);*/

            if (groupby === 1) {
                for (var cod_promo in raggruppamento) {
                    for (var fiscalizzata_sn in raggruppamento[cod_promo]) {
                        for (var stampata_sn in raggruppamento[cod_promo][fiscalizzata_sn]) {
                            for (var portata in raggruppamento[cod_promo][fiscalizzata_sn][stampata_sn]) {
                                for (var tipo_impasto in raggruppamento[cod_promo][fiscalizzata_sn][stampata_sn][portata]) {
                                    for (var desc_art in raggruppamento[cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto]) {
                                        for (var prog_inser in raggruppamento[cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto][desc_art]) {
                                            for (var nome_comanda in raggruppamento[cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto][desc_art][prog_inser]) {
                                                for (var prezzo_un in raggruppamento[cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto][desc_art][prog_inser][nome_comanda]) {

                                                    alasql.tables.oggetto_conto.data.push(raggruppamento[cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto][desc_art][prog_inser][nome_comanda][prezzo_un]);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else if (groupby === 2) {
                for (var cod_promo in raggruppamento) {
                    for (var fiscalizzata_sn in raggruppamento[cod_promo]) {
                        for (var stampata_sn in raggruppamento[cod_promo][fiscalizzata_sn]) {
                            for (var portata in raggruppamento[cod_promo][fiscalizzata_sn][stampata_sn]) {
                                for (var tipo_impasto in raggruppamento[cod_promo][fiscalizzata_sn][stampata_sn][portata]) {
                                    for (var desc_art in raggruppamento[cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto]) {
                                        for (var nome_comanda in raggruppamento[cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto][desc_art]) {

                                            for (var prezzo_un in raggruppamento[cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto][desc_art][nome_comanda]) {


                                                alasql.tables.oggetto_conto.data.push(raggruppamento[cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto][desc_art][nome_comanda][prezzo_un]);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            /*time6 = new Date().getTime();
             console.log("TIME6", time6 - time5);*/

            raggruppamento = {};
            delete raggruppamento;

            /*time7 = new Date().getTime();
             console.log("TIME7", time7 - time6);*/

            if (numero_conti_attivi === 2) {
                var raggruppamento = new Object();
                alasql("select * from oggetto_conto_2 where contiene_variante!='1' and length(nodo)=3;").forEach(function (e) {
                    if (e.stampata_sn !== "S") {
                        e.stampata_sn = "N";
                    }
                    if (groupby === 1) {

                        if (raggruppamento[e.cod_promo] === undefined) {
                            raggruppamento[e.cod_promo] = new Object();
                        }

                        if (raggruppamento[e.cod_promo][e.fiscalizzata_sn] === undefined) {
                            raggruppamento[e.cod_promo][e.fiscalizzata_sn] = new Object();
                        }

                        if (raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn] === undefined) {
                            raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn] = new Object();
                        }


                        if (raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata] === undefined) {
                            raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata] = new Object();
                        }

                        if (raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto] === undefined) {
                            raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto] = new Object();
                        }

                        if (raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art] === undefined) {
                            raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art] = new Object();
                        }

                        if (raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.prog_inser] === undefined) {
                            raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.prog_inser] = new Object();
                        }

                        if (raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.prog_inser][e.nome_comanda] === undefined) {
                            raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.prog_inser][e.nome_comanda] = new Object();
                        }


                        if (raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.prog_inser][e.nome_comanda][e.prezzo_un] === undefined) {
                            e.quantita = parseInt(e.quantita);
                            raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.prog_inser][e.nome_comanda][e.prezzo_un] = e;
                        } else {
                            raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.prog_inser][e.nome_comanda][e.prezzo_un].quantita += parseInt(e.quantita);
                        }


                    } else if (groupby === 2) {
                        if (raggruppamento[e.cod_promo] === undefined) {
                            raggruppamento[e.cod_promo] = new Object();
                        }

                        if (raggruppamento[e.cod_promo][e.fiscalizzata_sn] === undefined) {
                            raggruppamento[e.cod_promo][e.fiscalizzata_sn] = new Object();
                        }

                        if (raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn] === undefined) {
                            raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn] = new Object();
                        }

                        if (raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata] === undefined) {
                            raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata] = new Object();
                        }

                        if (raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto] === undefined) {
                            raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto] = new Object();
                        }

                        if (raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art] === undefined) {
                            raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art] = new Object();
                        }



                        if (raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.nome_comanda] === undefined) {
                            raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.nome_comanda] = new Object();
                        }


                        if (raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.nome_comanda][e.prezzo_un] === undefined) {
                            e.quantita = parseInt(e.quantita);
                            raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.nome_comanda][e.prezzo_un] = e;
                        } else {
                            raggruppamento[e.cod_promo][e.fiscalizzata_sn][e.stampata_sn][e.portata][e.tipo_impasto][e.desc_art][e.nome_comanda][e.prezzo_un].quantita += parseInt(e.quantita);
                        }
                    }
                });


                alasql("delete from oggetto_conto_2 where contiene_variante!='1' and length(nodo)=3;")

                if (groupby === 1) {
                    for (var cod_promo in raggruppamento) {
                        for (var fiscalizzata_sn in raggruppamento[cod_promo]) {
                            for (var stampata_sn in raggruppamento[cod_promo][fiscalizzata_sn]) {
                                for (var portata in raggruppamento[cod_promo][fiscalizzata_sn][stampata_sn]) {
                                    for (var tipo_impasto in raggruppamento[cod_promo][fiscalizzata_sn][stampata_sn][portata]) {
                                        for (var desc_art in raggruppamento[cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto]) {
                                            for (var prog_inser in raggruppamento[cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto][desc_art]) {
                                                for (var nome_comanda in raggruppamento[cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto][desc_art][prog_inser]) {
                                                    for (var prezzo_un in raggruppamento[cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto][desc_art][prog_inser][nome_comanda]) {

                                                        alasql.tables.oggetto_conto_2.data.push(raggruppamento[cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto][desc_art][prog_inser][nome_comanda][prezzo_un]);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else if (groupby === 2) {
                    for (var cod_promo in raggruppamento) {
                        for (var fiscalizzata_sn in raggruppamento[cod_promo]) {
                            for (var stampata_sn in raggruppamento[cod_promo][fiscalizzata_sn]) {
                                for (var portata in raggruppamento[cod_promo][fiscalizzata_sn][stampata_sn]) {
                                    for (var tipo_impasto in raggruppamento[cod_promo][fiscalizzata_sn][stampata_sn][portata]) {
                                        for (var desc_art in raggruppamento[cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto]) {
                                            for (var nome_comanda in raggruppamento[cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto][desc_art]) {

                                                for (var prezzo_un in raggruppamento[cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto][desc_art][nome_comanda]) {


                                                    alasql.tables.oggetto_conto_2.data.push(raggruppamento[cod_promo][fiscalizzata_sn][stampata_sn][portata][tipo_impasto][desc_art][nome_comanda][prezzo_un]);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                raggruppamento = {};
                delete raggruppamento;
            }

            /*time8 = new Date().getTime();
             console.log("TIME8", time8 - time7);*/

            var d1 = alasql("select * from oggetto_conto order by nome_comanda DESC, ord ASC;");

            /*time9 = new Date().getTime();
             console.log("TIME9", time9 - time8);*/

            alasql.tables.oggetto_conto.data = d1;
            //console.log(d1);

            /*time10 = new Date().getTime();
             console.log("TIME10", time10 - time9);*/

            var elemento_comanda = new Object();
            //SERVE A DARE UN ORDINE MIGLIORE ALLE VARIANTI
            //E' NECESSARISSIMO
            var ord = 0;
            d1.forEach(function (attivo, i) {

                /*FINE PARTE NUOVA*/

                /*RAGGRUPPAMENTO VECCHIO PER PORTATA*/



                attivo.desc_art = attivo.desc_art.substr(0, 21);
                console.log("prodotto", attivo);
                if (comanda.totale_conto[comanda.conto] === undefined) {
                    comanda.totale_conto[comanda.conto] = new Object();
                }

                if (comanda.totale_conto[comanda.conto][attivo.perc_iva] === undefined) {
                    comanda.totale_conto[comanda.conto][attivo.perc_iva] = new Object();
                    comanda.totale_conto[comanda.conto][attivo.perc_iva].iva = 0;
                    comanda.totale_conto[comanda.conto][attivo.perc_iva].netto = 0;
                    comanda.totale_conto[comanda.conto][attivo.perc_iva].mwst = 0;
                    comanda.totale_conto[comanda.conto][attivo.perc_iva].gesamt = 0;
                }

                comanda.totale_conto[comanda.conto][attivo.perc_iva].iva = parseInt(attivo.perc_iva);
                comanda.totale_conto[comanda.conto][attivo.perc_iva].netto += parseFloat(((attivo.prezzo_un / 100 * (100 - parseInt(attivo.perc_iva)))) * parseInt(attivo.quantita));
                comanda.totale_conto[comanda.conto][attivo.perc_iva].gesamt += parseFloat((attivo.prezzo_un * attivo.quantita));
                comanda.totale_conto[comanda.conto][attivo.perc_iva].mwst = parseFloat((comanda.totale_conto[comanda.conto][attivo.perc_iva].gesamt / 100 * attivo.perc_iva));
                console.log(attivo);
                if (elemento_comanda[attivo.portata] === undefined)
                {
                    elemento_comanda[attivo.portata] = new Object();
                }



                //ORDINANDO PER ORD RESTANO IN ORDINE ALFABETICO GLI ARTICOLI PER OGNI PORTATA
                if (elemento_comanda[attivo.portata][attivo.ord + "_" + ord] === undefined) {
                    //Diventa come (che equivale a dire uguale) attivo
                    elemento_comanda[attivo.portata][attivo.ord + "_" + ord] = new Object();
                    elemento_comanda[attivo.portata][attivo.ord + "_" + ord] = attivo;
                }
                ord++;

            });

            /*time11 = new Date().getTime();
             console.log("TIME11", time11 - time10);*/



            for (var portata in elemento_comanda) {
                console.log("conto_attivo - analisi - portata", portata);
                //CONDIZIONI PER MOSTRARE GLI ARTICOLI, VARIANTI, STAMPATI, SERVITI, ECCETERA...

                if (comanda.nome_portata[portata] === undefined) {
                    comanda.nome_portata[portata] = "";
                }

                conto += '<tr class="comanda non-contare-riga portata text-center" style="border:1px solid black;text-shadow: -1px 0 black, 0 1px black, 2px 0 black, 0 -1px black;cursor:pointer;background-color:grey;font-weight:bold;"><td colspan="5">' + comanda.nome_portata[portata] + '</td></tr>';
                if (comanda.mobile !== true) {
                    tab_conto_grande += '<tr class="comanda non-contare-riga portata text-center" style="border:1px solid black;text-shadow: -1px 0 black, 0 1px black, 2px 0 black, 0 -1px black;cursor:pointer;background-color:grey;font-weight:bold;"><td colspan="6">' + comanda.nome_portata[portata] + '</td></tr>';
                }

                for (var indice_articolo in elemento_comanda[portata]) {



                    //ATTIVO RIPRENDE I SUOI ATTRIBUTI
                    var attivo = elemento_comanda[portata][indice_articolo];
//IVA DI DEFAULT 10 REPARTO 2
                    //10% Ã¯Â¿Â½ il reparto 2 (FPWizard)
                    var reparto = "rep_1";
                    var iva_esposta = "10";
                    if (typeof (attivo.perc_iva) === "string") {
                        switch (attivo.perc_iva.trim())
                        {
                            case "22":
                                //22% Ã¯Â¿Â½ il reparto 1 (FPWizard)
                                reparto = "rep_2";
                                break;
                            case "4":
                                //4% Ã¯Â¿Â½ il reparto 3 (FPWizard)
                                reparto = "rep_3";
                                break;
                            default:
                        }

                        iva_esposta = attivo.perc_iva;
                        if (comanda.iva_esposta !== 'true') {
                            iva_esposta = "";
                        }
                    }

                    var fiscalizzata_sn = '';
                    if (attivo.fiscalizzata_sn !== undefined && attivo.fiscalizzata_sn !== null && attivo.fiscalizzata_sn === 'S') {
                        fiscalizzata_sn = 'fiscalizzata';
                    }

                    var cod_promo = '';
                    if (attivo.cod_promo !== undefined && attivo.cod_promo !== null && attivo.cod_promo !== '') {
                        cod_promo = 'cod_promo_' + attivo.cod_promo;
                    }

                    var prezzo_un = attivo.prezzo_un;
                    if (cod_promo === 'cod_promo_V_1') {
                        prezzo_un = '0.00';
                    }


                    if (comanda.consegna_presente === true) {
                        if (comanda.categorie_con_consegna_abilitata.indexOf(attivo.categoria) !== -1 && attivo.desc_art[0] !== "+" && attivo.desc_art[0] !== "-" && attivo.desc_art[0] !== "(" && attivo.desc_art[0] !== "*" && attivo.desc_art[0] !== "x" && attivo.desc_art[0] !== "=") {
                            if (comanda.consegna_su_totale.length > 0) {
                                calcolo_consegna_su_totale += parseFloat(prezzo_un) * parseInt(attivo.quantita);
                                comanda.quantita_pizze_totali++;
                            }

                            if (attivo.desc_art.indexOf("MEZZO METRO") !== -1)
                            {
                                calcolo_consegna_mezzo_metro += parseInt(attivo.quantita);
                            } else if (attivo.desc_art.indexOf("UN METRO") !== -1)
                            {
                                calcolo_consegna_un_metro += parseInt(attivo.quantita);
                            } else if (attivo.desc_art.indexOf("MAXI") !== -1)
                            {
                                calcolo_consegna_maxi += parseInt(attivo.quantita);
                            } else if (comanda.consegna_a_pizza.length > 0 && attivo.desc_art.indexOf("+") === -1 && attivo.desc_art.indexOf("-") === -1 && attivo.desc_art.indexOf("=") === -1)
                            {
                                calcolo_consegna_a_pizza += parseInt(attivo.quantita);
                            }

                            if ((comanda.una_pizza.length > 0 || comanda.piu_di_una_pizza.length > 0) && attivo.desc_art.indexOf("+") === -1 && attivo.desc_art.indexOf("-") === -1 && attivo.desc_art.indexOf("=") === -1)
                            {
                                calcolo_consegna_una_pizza += parseInt(attivo.quantita);
                                calcolo_consegna_piu_di_una_pizza += parseInt(attivo.quantita);
                            }
                        }
                    }

                    console.log("conto_attivo - analisi - art", attivo.desc_art, attivo.quantita);
                    console.log("funzionidb conto_attivo", attivo);
                    if (attivo.stampata_sn === "S") {
                        comanda.stampata_s = true;
                    } else
                    {
                        comanda.disabilita_tasto_bis = true;
                        if (attivo.BIS !== lettera_conto_split()) {
                            comanda.disabilita_tasto_tavolo = true;
                        }
                    }
                    if (comanda.pizzeria_asporto !== true && (attivo.stampata_sn === "S")) {



                        //E' UNA REGOLA CHE NON VALE PER MATTIA131
                        if (comanda.mobile === true && comanda.licenza !== "mattia131") {

                            if (attivo.desc_art[0] === "+" || attivo.desc_art[0] === "-" || attivo.desc_art[0] === "(" || attivo.desc_art[0] === "*" || attivo.desc_art[0] === "x" || attivo.desc_art[0] === "=") {

                                conto += '<tr style="height:0;" class="comanda non-contare-riga"></tr>';
                                ultime_battiture += '<tr style="height:0;" class="comanda non-contare-riga"></tr>';
                                tab_conto_grande += '<tr style="height:0;" class="comanda non-contare-riga"></tr>';
                                tab_conto_separato_grande += '<tr style="height:0;" class="comanda non-contare-riga"></tr>';
                                tab_conto_separato_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda ' + comanda.tavolo + ' ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td style="color:transparent;" ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td><td></td></tr>';
                                conto += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td    ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="color:transparent;background-color:black;" id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro;' + prezzo_un + '</td><td></td></tr>';
                                tab_conto_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda ' + comanda.tavolo + ' ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td   ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="color:transparent;background-color:black;" id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro; ' + prezzo_un + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro; ' + parseFloat(parseFloat(prezzo_un) * parseInt(attivo.quantita)).toFixed(2) + '</td><td></td></tr>';
                            } else
                            {
                                conto += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda ' + comanda.tavolo + ' ' + comanda.nome_portata[portata] + '" ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '" ><td>' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td>&euro;' + prezzo_un + '</td><td></td></tr>';
                                tab_conto_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda ' + comanda.tavolo + ' ' + comanda.nome_portata[portata] + '"  ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '"><td>' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td>&euro; ' + prezzo_un + '</td><td>&euro; ' + parseFloat(parseFloat(prezzo_un) * parseInt(attivo.quantita)).toFixed(2) + '</td><td></td></tr>';
                                tab_conto_separato_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '"  style="background-color:black;" id="art_' + attivo.prog_inser + '" ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')"><td>' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td>&euro;' + prezzo_un + '</td><td></td></tr>';
                            }
                        } else
                        {
                            if (comanda.cancellazione_articolo === 'S' || (comanda.tavolo === "BANCO" || comanda.tavolo === "BAR" || comanda.tavolo === "ASPORTO" || comanda.tavolo === "TAKE AWAY")) {
                                var x = '';
                                var eliminart = '';
                            } else
                            {
                                var x = '';
                                var eliminart = '';
                            }

                            //PER IL PC CHE HA GIA' STAMPATO LA COMANDA

                            console.log("CA ARTICOLO PRINC", attivo.desc_art, attivo.desc_art[0]);
                            //SE L'ARTICOLO E' UNA VARIANTE
                            if (attivo.desc_art[0] === "+" || attivo.desc_art[0] === "-" || attivo.desc_art[0] === "(" || attivo.desc_art[0] === "*" || attivo.desc_art[0] === "x" || attivo.desc_art[0] === "=") {

                                conto += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                ultime_battiture += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                tab_conto_grande += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                tab_conto_separato_grande += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                tab_conto_separato_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td style="color:transparent;" ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td></tr>';
                                console.log("CA ARTICOLO VARIANTE", attivo.desc_art[0]);
                                conto += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="color:transparent;background-color:black;" id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro;' + prezzo_un + '</td></tr>';
                                tab_conto_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="color:transparent;background-color:black;" id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro; ' + prezzo_un + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro;' + parseFloat(parseFloat(prezzo_un) * parseInt(attivo.quantita)).toFixed(2) + '</td></tr>';
                            }
                            //ALTRIMENTI LI STAMPA IN MODO NORMALE
                            else
                            {
                                console.log("CA ARTICOLO NON VARIANTE", attivo.desc_art);
                                conto += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td style="background-color:black;" ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')"  id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro;' + prezzo_un + '</td><td ' + eliminart + ' style="color:red;background-color:black;cursor:pointer;">' + x + '</td></tr>';
                                tab_conto_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td  ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro; ' + prezzo_un + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro; ' + parseFloat(parseFloat(prezzo_un) * parseInt(attivo.quantita)).toFixed(2) + '</td></tr>';
                                tab_conto_separato_grande += '<tr style="background-color:black;" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '"><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td></tr>';
                            }
                        }

                    }

                    //ALTRIMENTI
                    else
                    {


                        console.log("CA ARTICOLO PRINC", attivo.desc_art, attivo.desc_art[0]);
                        //SE L'ARTICOLO E' UNA VARIANTE
                        if (attivo.desc_art[0] === "+" || attivo.desc_art[0] === "-" || attivo.desc_art[0] === "(" || attivo.desc_art[0] === "*" || attivo.desc_art[0] === "x" || attivo.desc_art[0] === "=") {

                            if (attivo.desc_art[0] === "+") {
                                numero_nodo_variante_piu++;
                            }





                            if (ultima_maxi === true && attivo.desc_art[0] !== "-") {

                                if (numero_nodo_variante_piu > 1 && attivo.prezzo_varianti_maxi !== undefined && attivo.prezzo_varianti_maxi !== null && attivo.prezzo_varianti_maxi !== "undefined" && attivo.prezzo_varianti_maxi !== "null" && attivo.prezzo_varianti_maxi !== "" && !isNaN(parseFloat(attivo.prezzo_varianti_maxi))) {
                                    prezzo_un = attivo.prezzo_varianti_maxi;
                                } else if (attivo.prezzo_maxi_prima !== undefined && attivo.prezzo_maxi_prima !== null && attivo.prezzo_maxi_prima !== "undefined" && attivo.prezzo_maxi_prima !== "null" && attivo.prezzo_maxi_prima !== "" && !isNaN(parseFloat(attivo.prezzo_maxi_prima))) {
                                    prezzo_un = attivo.prezzo_maxi_prima;
                                }
                            } else {
                                if (numero_nodo_variante_piu > 1 && attivo.prezzo_varianti_aggiuntive !== undefined && attivo.prezzo_varianti_aggiuntive !== null && attivo.prezzo_varianti_aggiuntive !== "undefined" && attivo.prezzo_varianti_aggiuntive !== "null" && attivo.prezzo_varianti_aggiuntive !== "" && !isNaN(parseFloat(attivo.prezzo_varianti_aggiuntive))) {
                                    prezzo_un = attivo.prezzo_varianti_aggiuntive;
                                }
                            }

                            if (ultima_due_gusti === true && attivo.desc_art[0] === "=") {
                                prezzo_un = "0.00";
                            }



                            conto += '<tr style="height:0;" class="non-contare-riga"></tr>';
                            ultime_battiture += '<tr style="height:0;" class="non-contare-riga"></tr>';
                            tab_conto_grande += '<tr style="height:0;" class="non-contare-riga"></tr>';
                            tab_conto_separato_grande += '<tr style="height:0;" class="non-contare-riga"></tr>';
                            tab_conto_separato_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td style="color:transparent;" ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td></tr>';
                            console.log("CA ARTICOLO VARIANTE", attivo.desc_art[0]);
                            conto += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  style="color:transparent;" ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td></tr>';
                            ultime_battiture += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  style="color:transparent;" ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td></tr>';
                            tab_conto_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  style="color:transparent;" ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro; ' + prezzo_un + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro; ' + parseFloat(parseFloat(prezzo_un) * parseInt(attivo.quantita)).toFixed(2) + '</td></tr>';
                            //MODIFICA 07/06/2018
                            /*if (comanda.compatibile_xml !== true) {
                             //BASSA PRIORITA PER NON INTACCARE PRESTAZIONI
                             var query_prezzo_vero = "update comanda set prezzo_vero='" + prezzo_un + "' where  desc_art NOT LIKE '= 1/2 %' AND stato_record='ATTIVO' AND prog_inser='" + attivo.prog_inser + "';";
                             
                             comanda.sock.send({tipo: "set_prezzo_vero", operatore: comanda.operatore, query: query_prezzo_vero, terminale: comanda.terminale, ip: comanda.ip_address});
                             
                             comanda.sincro.query(query_prezzo_vero, function () {});
                             }*/

                        }
                        //ALTRIMENTI LI STAMPA IN MODO NORMALE
                        else
                        {

                            if (attivo.desc_art.indexOf("MAXI") !== -1) {
                                ultima_maxi = true;
                            } else
                            {
                                ultima_maxi = false;
                            }

                            if (attivo.desc_art.indexOf("DUE GUSTI") !== -1) {
                                ultima_due_gusti = true;
                                var nodo_art_princ = attivo.nodo.substr(0, 3);
                                var prezzo_due_gusti = "0.00";
                                var contatore_duegusti = 0;
                                for (var articolo in elemento_comanda[portata]) {
                                    var art = elemento_comanda[portata][articolo];
                                    var nodo_var = art.nodo.substr(0, 3);
                                    if (nodo_art_princ === nodo_var && art.desc_art[0] === "=") {

                                        contatore_duegusti++;
                                        if (parseFloat(art.prezzo_un) > parseFloat(prezzo_due_gusti)) {
                                            prezzo_due_gusti = art.prezzo_un;
                                        }
                                    }
                                }
                                prezzo_un = prezzo_due_gusti;
                                var prezzo_diviso = arrotonda_prezzo(parseFloat(prezzo_due_gusti) / parseInt(contatore_duegusti));
                                //MODIFICA 07/06/2018
                                //IN QUESTO CASO SUDDIVIDE L'INCASSO PER OGNI GUSTO IN BASE AL NODO
                                //SOLO NEL NODO PIU LUNGO DI TRE RISPETTO A NODO_ART_PRINC
                                //CIOE' AI GUSTI
                                //IL CONTATORE DICE QUANTI GUSTI SONO
                                /*if (comanda.compatibile_xml !== true) {
                                 //BASSA PRIORITA PER NON INTACCARE PRESTAZIONI
                                 var query_prezzo_vero = "update comanda set prezzo_vero='" + prezzo_diviso + "' where stato_record='ATTIVO' AND nodo LIKE '" + nodo_art_princ + " %' AND  nodo!='" + nodo_art_princ + "' and desc_art LIKE '= 1/2 %';";
                                 
                                 comanda.sock.send({tipo: "set_prezzo_vero", operatore: comanda.operatore, query: query_prezzo_vero, terminale: comanda.terminale, ip: comanda.ip_address});
                                 
                                 comanda.sincro.query(query_prezzo_vero, function () {});
                                 
                                 }*/

                            } else
                            {
                                ultima_due_gusti = false;
                                //MODIFICA 07/06/2018
                                /*if (comanda.compatibile_xml !== true) {
                                 //BASSA PRIORITA PER NON INTACCARE PRESTAZIONI
                                 var query_prezzo_vero = "update comanda set prezzo_vero='" + prezzo_un + "' where desc_art NOT LIKE '= 1/2 %' AND stato_record='ATTIVO' AND prog_inser='" + attivo.prog_inser + "';";
                                 
                                 comanda.sock.send({tipo: "set_prezzo_vero", operatore: comanda.operatore, query: query_prezzo_vero, terminale: comanda.terminale, ip: comanda.ip_address});
                                 
                                 comanda.sincro.query(query_prezzo_vero, function () {});
                                 }*/

                            }

                            numero_nodo_variante_piu = 0;
                            console.log("CA ARTICOLO NON VARIANTE", attivo.desc_art);
                            conto += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td></tr>';
                            ultime_battiture += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td></tr>';
                            tab_conto_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro; ' + prezzo_un + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro; ' + parseFloat(parseFloat(prezzo_un) * parseInt(attivo.quantita)).toFixed(2) + '</td></tr>';
                            tab_conto_separato_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td></tr>';
                        }



                    }
                    //FINE CONDIZIONI
                }
                //FINE CICLO FOR IN 1 --ARTICOLI
            }
            //FINE CICLO FOR IN 2

            /*time12 = new Date().getTime();
             console.log("TIME12", time12 - time11);*/


            if (numero_conti_attivi === 2) {
                var d1 = alasql("select * from oggetto_conto_2 order by nome_comanda DESC, ord ASC;");
                alasql.tables.oggetto_conto_2.data = d1;
                //console.log(d1);

                var elemento_comanda = new Object();
                //SERVE A DARE UN ORDINE MIGLIORE ALLE VARIANTI
                //E' NECESSARISSIMO
                var ord = 0;
                d1.forEach(function (attivo, i) {

                    /*FINE PARTE NUOVA*/

                    /*RAGGRUPPAMENTO VECCHIO PER PORTATA*/



                    attivo.desc_art = attivo.desc_art.substr(0, 21);
                    console.log("prodotto", attivo);
                    if (comanda.totale_conto[comanda.conto] === undefined) {
                        comanda.totale_conto[comanda.conto] = new Object();
                    }

                    if (comanda.totale_conto[comanda.conto][attivo.perc_iva] === undefined) {
                        comanda.totale_conto[comanda.conto][attivo.perc_iva] = new Object();
                        comanda.totale_conto[comanda.conto][attivo.perc_iva].iva = 0;
                        comanda.totale_conto[comanda.conto][attivo.perc_iva].netto = 0;
                        comanda.totale_conto[comanda.conto][attivo.perc_iva].mwst = 0;
                        comanda.totale_conto[comanda.conto][attivo.perc_iva].gesamt = 0;
                    }

                    comanda.totale_conto[comanda.conto][attivo.perc_iva].iva = parseInt(attivo.perc_iva);
                    comanda.totale_conto[comanda.conto][attivo.perc_iva].netto += parseFloat(((attivo.prezzo_un / 100 * (100 - parseInt(attivo.perc_iva)))) * parseInt(attivo.quantita));
                    comanda.totale_conto[comanda.conto][attivo.perc_iva].gesamt += parseFloat((attivo.prezzo_un * attivo.quantita));
                    comanda.totale_conto[comanda.conto][attivo.perc_iva].mwst = parseFloat((comanda.totale_conto[comanda.conto][attivo.perc_iva].gesamt / 100 * attivo.perc_iva));
                    console.log(attivo);
                    if (elemento_comanda[attivo.portata] === undefined)
                    {
                        elemento_comanda[attivo.portata] = new Object();
                    }



                    //ORDINANDO PER ORD RESTANO IN ORDINE ALFABETICO GLI ARTICOLI PER OGNI PORTATA
                    if (elemento_comanda[attivo.portata][attivo.ord + "_" + ord] === undefined) {
                        //Diventa come (che equivale a dire uguale) attivo
                        elemento_comanda[attivo.portata][attivo.ord + "_" + ord] = new Object();
                        elemento_comanda[attivo.portata][attivo.ord + "_" + ord] = attivo;
                    }
                    ord++;

                });

                conto += '<tr class="non-contare-riga" id="righe_conto_2"><td colspan="5"><center><strong> - - - CONTO 2 - - - </strong></center></td></tr>';
                ultime_battiture += '<tr class="non-contare-riga" id="righe_conto_2"><td colspan="5"><center><strong> - - - CONTO 2 - - - </strong></center></td></tr>';
                tab_conto_grande += '<tr class="non-contare-riga"><td colspan="6"><center><strong> - - - CONTO 2 - - - </strong></center></td></tr>';


                for (var portata in elemento_comanda) {
                    console.log("conto_attivo - analisi - portata", portata);
                    //CONDIZIONI PER MOSTRARE GLI ARTICOLI, VARIANTI, STAMPATI, SERVITI, ECCETERA...

                    if (comanda.nome_portata[portata] === undefined) {
                        comanda.nome_portata[portata] = "";
                    }

                    conto += '<tr class="comanda non-contare-riga portata text-center" style="border:1px solid black;text-shadow: -1px 0 black, 0 1px black, 2px 0 black, 0 -1px black;cursor:pointer;background-color:grey;font-weight:bold;"><td colspan="5">' + comanda.nome_portata[portata] + '</td></tr>';
                    if (comanda.mobile !== true) {
                        tab_conto_grande += '<tr class="comanda non-contare-riga portata text-center" style="border:1px solid black;text-shadow: -1px 0 black, 0 1px black, 2px 0 black, 0 -1px black;cursor:pointer;background-color:grey;font-weight:bold;"><td colspan="6">' + comanda.nome_portata[portata] + '</td></tr>';
                    }

                    for (var indice_articolo in elemento_comanda[portata]) {



                        //ATTIVO RIPRENDE I SUOI ATTRIBUTI
                        var attivo = elemento_comanda[portata][indice_articolo];
//IVA DI DEFAULT 10 REPARTO 2
                        //10% Ã¯Â¿Â½ il reparto 2 (FPWizard)
                        var reparto = "rep_1";
                        var iva_esposta = "10";
                        if (typeof (attivo.perc_iva) === "string") {
                            switch (attivo.perc_iva.trim())
                            {
                                case "22":
                                    //22% Ã¯Â¿Â½ il reparto 1 (FPWizard)
                                    reparto = "rep_2";
                                    break;
                                case "4":
                                    //4% Ã¯Â¿Â½ il reparto 3 (FPWizard)
                                    reparto = "rep_3";
                                    break;
                                default:
                            }

                            iva_esposta = attivo.perc_iva;
                            if (comanda.iva_esposta !== 'true') {
                                iva_esposta = "";
                            }
                        }

                        var fiscalizzata_sn = '';
                        if (attivo.fiscalizzata_sn !== undefined && attivo.fiscalizzata_sn !== null && attivo.fiscalizzata_sn === 'S') {
                            fiscalizzata_sn = 'fiscalizzata';
                        }

                        var cod_promo = '';
                        if (attivo.cod_promo !== undefined && attivo.cod_promo !== null && attivo.cod_promo !== '') {
                            cod_promo = 'cod_promo_' + attivo.cod_promo;
                        }

                        var prezzo_un = attivo.prezzo_un;
                        if (cod_promo === 'cod_promo_V_1') {
                            prezzo_un = '0.00';
                        }


                        if (comanda.consegna_presente === true) {
                            if (comanda.categorie_con_consegna_abilitata.indexOf(attivo.categoria) !== -1 && attivo.desc_art[0] !== "+" && attivo.desc_art[0] !== "-" && attivo.desc_art[0] !== "(" && attivo.desc_art[0] !== "*" && attivo.desc_art[0] !== "x" && attivo.desc_art[0] !== "=") {
                                if (comanda.consegna_su_totale.length > 0) {
                                    calcolo_consegna_su_totale += parseFloat(prezzo_un) * parseInt(attivo.quantita);
                                    comanda.quantita_pizze_totali++;
                                }

                                if (attivo.desc_art.indexOf("MEZZO METRO") !== -1)
                                {
                                    calcolo_consegna_mezzo_metro += parseInt(attivo.quantita);
                                } else if (attivo.desc_art.indexOf("UN METRO") !== -1)
                                {
                                    calcolo_consegna_un_metro += parseInt(attivo.quantita);
                                } else if (attivo.desc_art.indexOf("MAXI") !== -1)
                                {
                                    calcolo_consegna_maxi += parseInt(attivo.quantita);
                                } else if (comanda.consegna_a_pizza.length > 0 && attivo.desc_art.indexOf("+") === -1 && attivo.desc_art.indexOf("-") === -1 && attivo.desc_art.indexOf("=") === -1)
                                {
                                    calcolo_consegna_a_pizza += parseInt(attivo.quantita);
                                }

                                if ((comanda.una_pizza.length > 0 || comanda.piu_di_una_pizza.length > 0) && attivo.desc_art.indexOf("+") === -1 && attivo.desc_art.indexOf("-") === -1 && attivo.desc_art.indexOf("=") === -1)
                                {
                                    calcolo_consegna_una_pizza += parseInt(attivo.quantita);
                                    calcolo_consegna_piu_di_una_pizza += parseInt(attivo.quantita);
                                }
                            }
                        }

                        console.log("conto_attivo - analisi - art", attivo.desc_art, attivo.quantita);
                        console.log("funzionidb conto_attivo", attivo);
                        if (attivo.stampata_sn === "S") {
                            comanda.stampata_s = true;
                        } else
                        {
                            comanda.disabilita_tasto_bis = true;
                            if (attivo.BIS !== lettera_conto_split()) {
                                comanda.disabilita_tasto_tavolo = true;
                            }
                        }
                        if (comanda.pizzeria_asporto !== true && (attivo.stampata_sn === "S")) {



                            //E' UNA REGOLA CHE NON VALE PER MATTIA131
                            if (comanda.mobile === true && comanda.licenza !== "mattia131") {

                                if (attivo.desc_art[0] === "+" || attivo.desc_art[0] === "-" || attivo.desc_art[0] === "(" || attivo.desc_art[0] === "*" || attivo.desc_art[0] === "x" || attivo.desc_art[0] === "=") {

                                    conto += '<tr style="height:0;" class="comanda non-contare-riga"></tr>';
                                    ultime_battiture += '<tr style="height:0;" class="comanda non-contare-riga"></tr>';
                                    tab_conto_grande += '<tr style="height:0;" class="comanda non-contare-riga"></tr>';
                                    tab_conto_separato_2_grande += '<tr style="height:0;" class="comanda non-contare-riga"></tr>';
                                    tab_conto_separato_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda ' + comanda.tavolo + ' ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td style="color:transparent;" ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td><td></td></tr>';
                                    conto += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td    ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="color:transparent;background-color:black;" id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro;' + prezzo_un + '</td><td></td></tr>';
                                    tab_conto_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda ' + comanda.tavolo + ' ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td   ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="color:transparent;background-color:black;" id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro; ' + prezzo_un + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro; ' + parseFloat(parseFloat(prezzo_un) * parseInt(attivo.quantita)).toFixed(2) + '</td><td></td></tr>';
                                } else
                                {
                                    conto += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda ' + comanda.tavolo + ' ' + comanda.nome_portata[portata] + '" ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '" ><td>' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td>&euro;' + prezzo_un + '</td><td></td></tr>';
                                    tab_conto_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda ' + comanda.tavolo + ' ' + comanda.nome_portata[portata] + '"  ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '"><td>' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td>&euro; ' + prezzo_un + '</td><td>&euro; ' + parseFloat(parseFloat(prezzo_un) * parseInt(attivo.quantita)).toFixed(2) + '</td><td></td></tr>';
                                    tab_conto_separato_2_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '"  style="background-color:black;" id="art_' + attivo.prog_inser + '" ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')"><td>' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td>&euro;' + prezzo_un + '</td><td></td></tr>';
                                }
                            } else
                            {
                                if (comanda.cancellazione_articolo === 'S' || (comanda.tavolo === "BANCO" || comanda.tavolo === "BAR" || comanda.tavolo === "ASPORTO" || comanda.tavolo === "TAKE AWAY")) {
                                    var x = '';
                                    var eliminart = '';
                                } else
                                {
                                    var x = '';
                                    var eliminart = '';
                                }

                                //PER IL PC CHE HA GIA' STAMPATO LA COMANDA

                                console.log("CA ARTICOLO PRINC", attivo.desc_art, attivo.desc_art[0]);
                                //SE L'ARTICOLO E' UNA VARIANTE
                                if (attivo.desc_art[0] === "+" || attivo.desc_art[0] === "-" || attivo.desc_art[0] === "(" || attivo.desc_art[0] === "*" || attivo.desc_art[0] === "x" || attivo.desc_art[0] === "=") {

                                    conto += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                    ultime_battiture += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                    tab_conto_grande += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                    tab_conto_separato_2_grande += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                    tab_conto_separato_2_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td style="color:transparent;" ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td></tr>';
                                    console.log("CA ARTICOLO VARIANTE", attivo.desc_art[0]);
                                    conto += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="color:transparent;background-color:black;" id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro;' + prezzo_un + '</td></tr>';
                                    tab_conto_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="color:transparent;background-color:black;" id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro; ' + prezzo_un + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro;' + parseFloat(parseFloat(prezzo_un) * parseInt(attivo.quantita)).toFixed(2) + '</td></tr>';
                                }
                                //ALTRIMENTI LI STAMPA IN MODO NORMALE
                                else
                                {
                                    console.log("CA ARTICOLO NON VARIANTE", attivo.desc_art);
                                    conto += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td style="background-color:black;" ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')"  id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro;' + prezzo_un + '</td><td ' + eliminart + ' style="color:red;background-color:black;cursor:pointer;">' + x + '</td></tr>';
                                    tab_conto_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda ' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '" style="background-color:black;"><td  ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" id="art_' + attivo.prog_inser + '">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro; ' + prezzo_un + '</td><td ' + comanda.evento + '="popup_sposta_articolo(\'' + attivo.nodo.substr(0, 3) + '\')" style="background-color:black;" id="art_' + attivo.prog_inser + '">&euro; ' + parseFloat(parseFloat(prezzo_un) * parseInt(attivo.quantita)).toFixed(2) + '</td></tr>';
                                    tab_conto_separato_2_grande += '<tr style="background-color:black;" class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + ' comanda' + comanda.tavolo + '  ' + comanda.nome_portata[portata] + '" id="art_' + attivo.prog_inser + '"><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.desc_art + '</td><td style="background-color:black;">' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td></tr>';
                                }
                            }

                        }

                        //ALTRIMENTI
                        else
                        {


                            console.log("CA ARTICOLO PRINC", attivo.desc_art, attivo.desc_art[0]);
                            //SE L'ARTICOLO E' UNA VARIANTE
                            if (attivo.desc_art[0] === "+" || attivo.desc_art[0] === "-" || attivo.desc_art[0] === "(" || attivo.desc_art[0] === "*" || attivo.desc_art[0] === "x" || attivo.desc_art[0] === "=") {

                                if (attivo.desc_art[0] === "+") {
                                    numero_nodo_variante_piu++;
                                }





                                if (ultima_maxi === true && attivo.desc_art[0] !== "-") {

                                    if (numero_nodo_variante_piu > 1 && attivo.prezzo_varianti_maxi !== undefined && attivo.prezzo_varianti_maxi !== null && attivo.prezzo_varianti_maxi !== "undefined" && attivo.prezzo_varianti_maxi !== "null" && attivo.prezzo_varianti_maxi !== "" && !isNaN(parseFloat(attivo.prezzo_varianti_maxi))) {
                                        prezzo_un = attivo.prezzo_varianti_maxi;
                                    } else if (attivo.prezzo_maxi_prima !== undefined && attivo.prezzo_maxi_prima !== null && attivo.prezzo_maxi_prima !== "undefined" && attivo.prezzo_maxi_prima !== "null" && attivo.prezzo_maxi_prima !== "" && !isNaN(parseFloat(attivo.prezzo_maxi_prima))) {
                                        prezzo_un = attivo.prezzo_maxi_prima;
                                    }
                                } else {
                                    if (numero_nodo_variante_piu > 1 && attivo.prezzo_varianti_aggiuntive !== undefined && attivo.prezzo_varianti_aggiuntive !== null && attivo.prezzo_varianti_aggiuntive !== "undefined" && attivo.prezzo_varianti_aggiuntive !== "null" && attivo.prezzo_varianti_aggiuntive !== "" && !isNaN(parseFloat(attivo.prezzo_varianti_aggiuntive))) {
                                        prezzo_un = attivo.prezzo_varianti_aggiuntive;
                                    }
                                }

                                if (ultima_due_gusti === true && attivo.desc_art[0] === "=") {
                                    prezzo_un = "0.00";
                                }



                                conto += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                ultime_battiture += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                tab_conto_grande += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                tab_conto_2_separato_grande += '<tr style="height:0;" class="non-contare-riga"></tr>';
                                tab_conto_2_separato_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td style="color:transparent;" ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td></tr>';
                                console.log("CA ARTICOLO VARIANTE", attivo.desc_art[0]);
                                conto += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  style="color:transparent;" ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td></tr>';
                                ultime_battiture += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  style="color:transparent;" ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td></tr>';
                                tab_conto_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  style="color:transparent;" ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro; ' + prezzo_un + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro; ' + parseFloat(parseFloat(prezzo_un) * parseInt(attivo.quantita)).toFixed(2) + '</td></tr>';
                                //MODIFICA 07/06/2018
                                /*if (comanda.compatibile_xml !== true) {
                                 //BASSA PRIORITA PER NON INTACCARE PRESTAZIONI
                                 var query_prezzo_vero = "update comanda set prezzo_vero='" + prezzo_un + "' where  desc_art NOT LIKE '= 1/2 %' AND stato_record='ATTIVO' AND prog_inser='" + attivo.prog_inser + "';";
                                 
                                 comanda.sock.send({tipo: "set_prezzo_vero", operatore: comanda.operatore, query: query_prezzo_vero, terminale: comanda.terminale, ip: comanda.ip_address});
                                 
                                 comanda.sincro.query(query_prezzo_vero, function () {});
                                 }*/

                            }
                            //ALTRIMENTI LI STAMPA IN MODO NORMALE
                            else
                            {

                                if (attivo.desc_art.indexOf("MAXI") !== -1) {
                                    ultima_maxi = true;
                                } else
                                {
                                    ultima_maxi = false;
                                }

                                if (attivo.desc_art.indexOf("DUE GUSTI") !== -1) {
                                    ultima_due_gusti = true;
                                    var nodo_art_princ = attivo.nodo.substr(0, 3);
                                    var prezzo_due_gusti = "0.00";
                                    var contatore_duegusti = 0;
                                    for (var articolo in elemento_comanda[portata]) {
                                        var art = elemento_comanda[portata][articolo];
                                        var nodo_var = art.nodo.substr(0, 3);
                                        if (nodo_art_princ === nodo_var && art.desc_art[0] === "=") {

                                            contatore_duegusti++;
                                            if (parseFloat(art.prezzo_un) > parseFloat(prezzo_due_gusti)) {
                                                prezzo_due_gusti = art.prezzo_un;
                                            }
                                        }
                                    }
                                    prezzo_un = prezzo_due_gusti;
                                    var prezzo_diviso = arrotonda_prezzo(parseFloat(prezzo_due_gusti) / parseInt(contatore_duegusti));
                                    //MODIFICA 07/06/2018
                                    //IN QUESTO CASO SUDDIVIDE L'INCASSO PER OGNI GUSTO IN BASE AL NODO
                                    //SOLO NEL NODO PIU LUNGO DI TRE RISPETTO A NODO_ART_PRINC
                                    //CIOE' AI GUSTI
                                    //IL CONTATORE DICE QUANTI GUSTI SONO
                                    /*if (comanda.compatibile_xml !== true) {
                                     //BASSA PRIORITA PER NON INTACCARE PRESTAZIONI
                                     var query_prezzo_vero = "update comanda set prezzo_vero='" + prezzo_diviso + "' where stato_record='ATTIVO' AND nodo LIKE '" + nodo_art_princ + " %' AND  nodo!='" + nodo_art_princ + "' and desc_art LIKE '= 1/2 %';";
                                     
                                     comanda.sock.send({tipo: "set_prezzo_vero", operatore: comanda.operatore, query: query_prezzo_vero, terminale: comanda.terminale, ip: comanda.ip_address});
                                     
                                     comanda.sincro.query(query_prezzo_vero, function () {});
                                     
                                     }*/

                                } else
                                {
                                    ultima_due_gusti = false;
                                    //MODIFICA 07/06/2018
                                    /*if (comanda.compatibile_xml !== true) {
                                     //BASSA PRIORITA PER NON INTACCARE PRESTAZIONI
                                     var query_prezzo_vero = "update comanda set prezzo_vero='" + prezzo_un + "' where desc_art NOT LIKE '= 1/2 %' AND stato_record='ATTIVO' AND prog_inser='" + attivo.prog_inser + "';";
                                     
                                     comanda.sock.send({tipo: "set_prezzo_vero", operatore: comanda.operatore, query: query_prezzo_vero, terminale: comanda.terminale, ip: comanda.ip_address});
                                     
                                     comanda.sincro.query(query_prezzo_vero, function () {});
                                     }*/

                                }

                                numero_nodo_variante_piu = 0;
                                console.log("CA ARTICOLO NON VARIANTE", attivo.desc_art);
                                conto += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td></tr>';
                                ultime_battiture += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td></tr>';
                                tab_conto_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td  ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro; ' + prezzo_un + '</td><td ' + comanda.evento + '="popup_mod_art(\'' + attivo.prog_inser + '\',\'' + attivo.nodo.substr(0, 3) + '\')">&euro; ' + parseFloat(parseFloat(prezzo_un) * parseInt(attivo.quantita)).toFixed(2) + '</td></tr>';
                                tab_conto_separato_2_grande += '<tr class="' + reparto + ' ' + fiscalizzata_sn + cod_promo + '" id="art_' + attivo.prog_inser + '"><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')">&euro;' + prezzo_un + '</td></tr>';
                            }



                        }
                        //FINE CONDIZIONI
                    }
                    //FINE CICLO FOR IN 1 --ARTICOLI
                }
            }

            /*time13 = new Date().getTime();
             console.log("TIME13", time13 - time12);*/

            console.log("conto_attivo", d1);
            if (d1 !== undefined && d1[0] !== undefined && d1[0].rag_soc_cliente !== undefined && d1[0].rag_soc_cliente !== null && d1[0].rag_soc_cliente.length > 0)
            {
                comanda.ultimo_id_cliente = d1[0].rag_soc_cliente;
                comanda.blocca_progressivo = true;
                var testo_query = "select ora_consegna,tipo_consegna,jolly from comanda where desc_art!='RECORD TESTA' and  BIS='" + lettera_conto_split() + "' and  id='" + comanda.ultimo_id_asporto + "' order by ora_consegna desc, tipo_consegna desc, jolly desc limit 1;";
                comanda.sincro.query(testo_query, function (dati_cliente) {

                    if (dati_cliente !== undefined && dati_cliente[0] !== undefined && dati_cliente[0].ora_consegna !== undefined && dati_cliente[0].tipo_consegna !== undefined)
                    {

                        var ora = dati_cliente[0].ora_consegna;
                        $('[name="ora_consegna"]').val(ora);
                        var tipo_consegna = dati_cliente[0].tipo_consegna;
                        if (tipo_consegna === undefined) {
                            //NIENTE
                        } else if (tipo_consegna === 'RITIRO')
                        {
                            comanda.tipo_consegna = "RITIRO";
                            $('#tipo_consegna_ritiro').attr('src', './img/CasaVerde100x100.png');
                        } else if (tipo_consegna === 'DOMICILIO') {
                            comanda.tipo_consegna = "DOMICILIO";
                            $('#tipo_consegna_domicilio').attr('src', './img/ScooterVerde100x100.png');
                        }
                    }

                    if (dati_cliente !== undefined && dati_cliente[0] !== undefined && dati_cliente[0].jolly !== undefined && dati_cliente[0].jolly === "SUBITO") {
                        comanda.esecuzione_comanda_immediata = true;
                        var data_js = new Date();
                        var ora_adesso = addZero(data_js.getHours(), 2) + ':' + addZero(data_js.getMinutes(), 2);
                        $('#popup_scelta_cliente input[name="ora_consegna"]:visible').val(ora_adesso);
                        comanda.tipo_consegna = 'RITIRO';
                        $('#tipo_consegna_ritiro').attr('src', './img/CasaVerde100x100.png');
                        $('#tipo_consegna_domicilio').attr('src', './img/ScooterBianco100x100.png');
                        $('#switch_esecuzione_comanda_immediata').css('background-color', 'grey');
                    } else
                    {
                        comanda.esecuzione_comanda_immediata = false;
                        $('#switch_esecuzione_comanda_immediata').css('background-color', '');
                    }
                    //FACENDO COSI DOVREI RISOLVERE UN PROBLEMA DELLA PIZZERIA ASPORTO
                    //CIOE' CHE RIAPRENDO LE RIGHE FORNO, QUANDO FACCIO CONTO ATTIVO
                    //NON RIPRENDE I DATI DEL CLIENTE
                    /*time14 = new Date().getTime();
                     console.log("TIME14", time14 - time13);*/
                    resolve(true);
                });

            } else
            {
                /*time15 = new Date().getTime();
                 console.log("TIME15", time15 - time14);*/
                resolve(true);
            }

        });

        var p3 = new Promise(function (resolve, reject) {
            var esclusione_fiscale_sospeso = '';
            //if (typeof (comanda.tavolo) === 'string' && (comanda.tavolo === "BAR" || comanda.tavolo.indexOf('CONTINUO') !== -1)) {
            esclusione_fiscale_sospeso = ' and fiscale_sospeso!="STAMPAFISCALESOSPESO" ';
            //}

            var rd2 = alasql("SELECT perc_iva,numero_conto,nodo,prog_inser,quantita,desc_art,prezzo_un FROM comanda WHERE desc_art!='RECORD TESTA' and  BIS='" + lettera_conto_split() + "'   AND stato_record='ATTIVO' AND posizione='SCONTO' AND ntav_comanda='" + comanda.tavolo + "' " + esclusione_fiscale_sospeso + " ORDER BY nodo ASC;");
            //GROUP BY numero_conto,prezzo_un ;

            var raggruppamento = new Object();

            rd2.forEach(function (e) {
                if (raggruppamento[e.numero_conto] === undefined) {
                    raggruppamento[e.numero_conto] = new Object();
                }
                if (raggruppamento[e.numero_conto][e.prezzo_un] === undefined) {
                    e.quantita = parseInt(e.quantita);
                    raggruppamento[e.numero_conto][e.prezzo_un] = e;
                } else {
                    raggruppamento[e.numero_conto][e.prezzo_un].quantita += parseInt(e.quantita);
                }
            });

            var sconto_conto_uno = new Array();
            var sconto_conto_due = new Array();

            for (var numero_conto in raggruppamento) {
                for (var prezzo_un in raggruppamento[numero_conto]) {
                    if (numero_conti_attivi === 2 && raggruppamento[numero_conto][prezzo_un].prezzo_un.indexOf("%") !== -1) {
                        sconto_conto_uno.push(raggruppamento[numero_conto][prezzo_un]);
                        sconto_conto_due.push(raggruppamento[numero_conto][prezzo_un]);
                    } else {
                        if (numero_conto === '2') {
                            sconto_conto_due.push(raggruppamento[numero_conto][prezzo_un]);
                        } else {
                            sconto_conto_uno.push(raggruppamento[numero_conto][prezzo_un]);
                        }
                    }
                }
            }

            raggruppamento = {};
            delete raggruppamento;

            sconto_conto_uno.forEach(function (attivo) {
                //IVA DI DEFAULT 10 REPARTO 2
                //10% ï¿½ il reparto 2 (FPWizard)
                var reparto = "rep_1";
                var iva_esposta = "10";

                if (typeof (attivo.perc_iva) === "string") {
                    switch (attivo.perc_iva.trim())
                    {
                        case "22":
                            //22% ï¿½ il reparto 1 (FPWizard)
                            reparto = "rep_2";
                            break;
                        case "4":
                            //4% ï¿½ il reparto 3 (FPWizard)
                            reparto = "rep_3";
                            break;
                        default:
                    }
                    iva_esposta = attivo.perc_iva;
                    if (comanda.iva_esposta !== 'true') {
                        iva_esposta = "";
                    }
                }

                var settaggio_sconto_non_eliminabile = '';
                if (comanda.compatibile_xml === true && comanda.sconto_non_eliminabile === "S") {
                    settaggio_sconto_non_eliminabile = '<td style="width:10%;color:red;cursor:pointer;"></td>';
                }

                intestazioni_conto = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td>' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td>' + attivo.prezzo_un + '</td>' + settaggio_sconto_non_eliminabile + '</tr>' + intestazioni_conto;
                intestazioni_conto_grande = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td style="width:10%;">' + attivo.quantita + '</td><td style="width:55%;">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td  style="width:25%;">' + attivo.prezzo_un + '</td><td  style="width:25%;">' + parseFloat(parseFloat(attivo.prezzo_un) * parseInt(attivo.quantita)).toFixed(2) + '</td>' + settaggio_sconto_non_eliminabile + '</tr>' + intestazioni_conto_grande;
                intestazioni_conto_split = '<tr  style="font-weight:bold;"   class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td>' + attivo.quantita + '</td><td>SCONTO</td><td>' + iva_esposta + '</td><td>' + attivo.prezzo_un + '</td><td></td><td></td></tr>' + intestazioni_conto_split;


                intestazioni_conto_separato_grande = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:10%;"  >' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:55%;" >' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:25%;" >' + attivo.prezzo_un + '</td>' + settaggio_sconto_non_eliminabile + '</tr>' + intestazioni_conto_separato_grande;

            });


            sconto_conto_due.forEach(function (attivo) {
                //IVA DI DEFAULT 10 REPARTO 2
                //10% ï¿½ il reparto 2 (FPWizard)
                var reparto = "rep_1";
                var iva_esposta = "10";

                if (typeof (attivo.perc_iva) === "string") {
                    switch (attivo.perc_iva.trim())
                    {
                        case "22":
                            //22% ï¿½ il reparto 1 (FPWizard)
                            reparto = "rep_2";
                            break;
                        case "4":
                            //4% ï¿½ il reparto 3 (FPWizard)
                            reparto = "rep_3";
                            break;
                        default:
                    }
                    iva_esposta = attivo.perc_iva;
                    if (comanda.iva_esposta !== 'true') {
                        iva_esposta = "";
                    }
                }

                var settaggio_sconto_non_eliminabile = '';
                if (comanda.compatibile_xml === true && comanda.sconto_non_eliminabile === "S") {
                    settaggio_sconto_non_eliminabile = '<td style="width:10%;color:red;cursor:pointer;"></td>';
                }

                intestazioni_conto = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td>' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td>' + attivo.prezzo_un + '</td>' + settaggio_sconto_non_eliminabile + '</tr>' + intestazioni_conto;
                intestazioni_conto_grande = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td style="width:10%;">' + attivo.quantita + '</td><td style="width:55%;">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td  style="width:25%;">' + attivo.prezzo_un + '</td><td  style="width:25%;">' + parseFloat(parseFloat(attivo.prezzo_un) * parseInt(attivo.quantita)).toFixed(2) + '</td>' + settaggio_sconto_non_eliminabile + '</tr>' + intestazioni_conto_grande;
                intestazioni_conto_split = '<tr  style="font-weight:bold;"   class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td>' + attivo.quantita + '</td><td>SCONTO</td><td>' + iva_esposta + '</td><td>' + attivo.prezzo_un + '</td><td></td><td></td></tr>' + intestazioni_conto_split;


                intestazioni_conto_separato_2_grande = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:10%;"  >' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:55%;" >' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:25%;" >' + attivo.prezzo_un + '</td>' + settaggio_sconto_non_eliminabile + '</tr>' + intestazioni_conto_separato_2_grande;

            });

            /*time16 = new Date().getTime();
             console.log("TIME16", time16 - time15);*/
            resolve(true);


        });
        var p4 = new Promise(function (resolve, reject) {
            var dr3 = alasql("SELECT perc_iva,numero_conto,nodo,prog_inser,quantita,desc_art,prezzo_un FROM comanda WHERE desc_art!='RECORD TESTA' and  BIS='" + lettera_conto_split() + "' and   stato_record='ATTIVO' AND posizione='COPERTO' AND ntav_comanda='" + comanda.tavolo + "' ORDER BY nodo ASC;");
            //GROUP BY numero_conto ;

            var raggruppamento = new Object();

            dr3.forEach(function (e) {
                if (raggruppamento[e.numero_conto] === undefined) {
                    //raggruppamento[e.numero_conto] = new Object();
                    e.quantita = parseInt(e.quantita);
                    raggruppamento[e.numero_conto] = e;
                } else {
                    raggruppamento[e.numero_conto].quantita += parseInt(e.quantita);
                }
                /*if (raggruppamento[e.numero_conto][e.prezzo_un] === undefined) {
                 e.quantita = parseInt(e.quantita);
                 raggruppamento[e.numero_conto][e.prezzo_un] = e;
                 } else {
                 raggruppamento[e.numero_conto][e.prezzo_un].quantita += parseInt(e.quantita);
                 }*/
            });

            var coperti_conto_uno = new Array();
            var coperti_conto_due = new Array();

            for (var numero_conto in raggruppamento) {
                if (numero_conto === '2') {
                    coperti_conto_due.push(raggruppamento[numero_conto]);
                } else
                {
                    coperti_conto_uno.push(raggruppamento[numero_conto]);
                }
            }

            raggruppamento = {};
            delete raggruppamento;

            coperti_conto_uno.forEach(function (attivo) {

                //IVA DI DEFAULT 10 REPARTO 2
                //10% ï¿½ il reparto 2 (FPWizard)
                var reparto = "rep_1";
                var iva_esposta = "10";

                if (typeof (attivo.perc_iva) === "string") {
                    switch (attivo.perc_iva.trim())
                    {
                        case "22":
                            //22% ï¿½ il reparto 1 (FPWizard)
                            reparto = "rep_2";
                            break;
                        case "4":
                            //4% ï¿½ il reparto 3 (FPWizard)
                            reparto = "rep_3";
                            break;
                        default:
                    }
                    iva_esposta = attivo.perc_iva;
                    if (comanda.iva_esposta !== 'true') {
                        iva_esposta = "";
                    }
                }


                intestazioni_conto = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td id="numero_coperti_effettivi">' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td>' + attivo.prezzo_un + '</td></tr>' + intestazioni_conto;
                intestazioni_conto_grande = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td  style="width:10%;" id="numero_coperti_effettivi">' + attivo.quantita + '</td><td  style="width:55%;">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td  style="width:25%;">' + attivo.prezzo_un + '</td><td  style="width:25%;">' + parseFloat(parseFloat(attivo.prezzo_un) * parseInt(attivo.quantita)).toFixed(2) + '</td></tr>' + intestazioni_conto_grande;
                intestazioni_conto_split = '<tr  style="font-weight:bold;"  class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td id="numero_coperti_effettivi">' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td>' + attivo.prezzo_un + '</td><td></td><td></td></tr>' + intestazioni_conto_split;


                //CONTO SEPARATO
                intestazioni_conto_separato_grande = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:10%;" id="numero_coperti_effettivi" >' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:55%;" >' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:25%;" >' + attivo.prezzo_un + '</td></tr>' + intestazioni_conto_separato_grande;

            });


            coperti_conto_due.forEach(function (attivo) {

                //IVA DI DEFAULT 10 REPARTO 2
                //10% ï¿½ il reparto 2 (FPWizard)
                var reparto = "rep_1";
                var iva_esposta = "10";

                if (typeof (attivo.perc_iva) === "string") {
                    switch (attivo.perc_iva.trim())
                    {
                        case "22":
                            //22% ï¿½ il reparto 1 (FPWizard)
                            reparto = "rep_2";
                            break;
                        case "4":
                            //4% ï¿½ il reparto 3 (FPWizard)
                            reparto = "rep_3";
                            break;
                        default:
                    }
                    iva_esposta = attivo.perc_iva;
                    if (comanda.iva_esposta !== 'true') {
                        iva_esposta = "";
                    }
                }


                intestazioni_conto = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td id="numero_coperti_effettivi">' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td>' + attivo.prezzo_un + '</td></tr>' + intestazioni_conto;
                intestazioni_conto_grande = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td  style="width:10%;" id="numero_coperti_effettivi">' + attivo.quantita + '</td><td  style="width:55%;">' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td  style="width:25%;">' + attivo.prezzo_un + '</td><td  style="width:25%;">' + parseFloat(parseFloat(attivo.prezzo_un) * parseInt(attivo.quantita)).toFixed(2) + '</td></tr>' + intestazioni_conto_grande;
                intestazioni_conto_split = '<tr  style="font-weight:bold;"  class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td id="numero_coperti_effettivi">' + attivo.quantita + '</td><td>' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td>' + attivo.prezzo_un + '</td><td></td><td></td></tr>' + intestazioni_conto_split;


                //CONTO SEPARATO
                intestazioni_conto_separato_2_grande = '<tr class="' + reparto + '" id="art_' + attivo.prog_inser + '"><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:10%;" id="numero_coperti_effettivi" >' + attivo.quantita + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:55%;" >' + attivo.desc_art + '</td><td>' + iva_esposta + '</td><td ' + comanda.evento + '="sposta_conto(\'' + attivo.nodo.substr(0, 3) + '\')" style="width:25%;" >' + attivo.prezzo_un + '</td></tr>' + intestazioni_conto_separato_2_grande;

            });

            /*time17 = new Date().getTime();
             console.log("TIME17", time17 - time16);*/

            resolve(true);

        });
        Promise.all([p2, p3, p4]).then(function () {



            if (progressivo_ultimo_conto_interno === progressivo_ultimo_conto) {

                if (comanda.split_abilitato === true) {
                    if (comanda.disabilita_tasto_bis === true) {
                        $('.split_successivo').css('pointer-events', 'none');
                        if (comanda.disabilita_tasto_tavolo === true) {
                            $('.tasto_numero_tavolo').css('pointer-events', 'none');
                        }
                    }
                }



                var totale_calcolo_consegna = 0;
                comanda.consegna_totale = 0;
                comanda.consegna_unitario_mezzo_metro = 0;
                comanda.consegna_unitario_metro = 0;
                comanda.consegna_unitario_a_pizza = 0;
                comanda.consegna_una_pizza = 0;
                comanda.consegna_piu_di_una_pizza = 0;
                comanda.consegna_unitario_maxi = 0;

                if (comanda.consegna_presente === true) {
                    if (calcolo_consegna_su_totale > 0 && comanda.consegna_su_totale.length > 0) {
                        if (comanda.consegna_su_totale.indexOf("%") !== -1) {
                            var add = parseFloat(calcolo_consegna_su_totale) / 100 * parseFloat(comanda.consegna_su_totale.replace("%", ""))
                            totale_calcolo_consegna += add;
                            comanda.consegna_totale += add;
                        } else
                        {
                            var add = parseFloat(comanda.consegna_su_totale);
                            totale_calcolo_consegna += add;
                            comanda.consegna_totale += add;
                        }
                    }
                    if (calcolo_consegna_maxi > 0 && comanda.consegna_maxi.length > 0)
                    {
                        var add = parseInt(calcolo_consegna_maxi) * parseFloat(comanda.consegna_maxi);
                        totale_calcolo_consegna += add;
                        comanda.consegna_unitario_maxi = comanda.consegna_maxi;
                    }
                    if (calcolo_consegna_mezzo_metro > 0 && comanda.consegna_mezzo_metro.length > 0)
                    {
                        var add = parseInt(calcolo_consegna_mezzo_metro) * parseFloat(comanda.consegna_mezzo_metro);
                        totale_calcolo_consegna += add;
                        comanda.consegna_unitario_mezzo_metro = comanda.consegna_mezzo_metro;
                    }
                    if (calcolo_consegna_un_metro > 0 && comanda.consegna_metro.length > 0)
                    {
                        var add = parseInt(calcolo_consegna_un_metro) * parseFloat(comanda.consegna_metro);
                        totale_calcolo_consegna += add;
                        comanda.consegna_unitario_metro = comanda.consegna_metro;
                    }
                    if (calcolo_consegna_a_pizza > 0 && comanda.consegna_a_pizza.length > 0)
                    {
                        var add = parseInt(calcolo_consegna_a_pizza) * parseFloat(comanda.consegna_a_pizza);
                        totale_calcolo_consegna += add;
                        comanda.consegna_unitario_a_pizza = comanda.consegna_a_pizza;
                    }

                    if (calcolo_consegna_una_pizza === 1 && comanda.una_pizza.length > 0)
                    {
                        var add = parseInt(calcolo_consegna_una_pizza) * parseFloat(comanda.una_pizza);
                        totale_calcolo_consegna += add;
                        comanda.consegna_una_pizza = comanda.una_pizza;
                    } else if (calcolo_consegna_piu_di_una_pizza > 1 && comanda.piu_di_una_pizza.length > 0)
                    {
                        var add = parseInt(calcolo_consegna_piu_di_una_pizza) * parseFloat(comanda.piu_di_una_pizza);
                        totale_calcolo_consegna += add;
                        comanda.consegna_piu_di_una_pizza = comanda.piu_di_una_pizza;
                    }

                    totale_calcolo_consegna = arrotonda_prezzo(parseFloat(totale_calcolo_consegna));
                    riga_calcolo_consegna = "";
                    if (comanda.tipo_consegna === "DOMICILIO" && parseFloat(totale_calcolo_consegna) > 0) {
                        comanda.totale_consegna = totale_calcolo_consegna;
                        riga_calcolo_consegna = "<tr  class='rep_2' id='art_999'><td style='color:transparent;'>1</td><td>CONSEGNA</td><td></td><td>&euro;" + totale_calcolo_consegna + "</td><td>&euro; " + totale_calcolo_consegna + "</td><td></td></tr>";
                    }
                }

                /*time18 = new Date().getTime();
                 console.log("TIME18", time18 - time17);*/

                $('.ultime_battiture').html(ultime_battiture);
                $('#intestazioni_conto_separato_grande').html(intestazioni_conto_separato_grande);
                $('#intestazioni_conto_separato_2_grande').html(intestazioni_conto_separato_2_grande);
                $('#conto').html(conto + riga_calcolo_consegna);
                $('#campo_ordinazione .anteprima').html(conto);
                console.log("CAMPO_ORDINAZIONI CONTO_1", conto);
                console.log("FINE CONTO", inizio_conto, new Date().getTime(), conto);
                $('#intestazioni_conto').html(intestazioni_conto);
                $('.tab_conto_grande').html(tab_conto_grande + riga_calcolo_consegna);
                $('#intestazioni_conto_grande').html(intestazioni_conto_grande);
                //socket_fast_split.send(JSON.stringify({intestazione: intestazioni_conto_split, conto: tab_conto_grande}));
                //$(popup_fast_split.document.getElementById("fS_prodotti_conto_grande")).html(tab_conto_grande);
                //$(popup_fast_split.document.getElementById("fS_intestazioni_conto_grande")).html(intestazioni_conto_grande);

                $('.tab_conto_separato_grande').html(tab_conto_separato_grande);
                $('.tab_conto_separato_2_grande').html(tab_conto_separato_2_grande);

                /*if (numero_conti_attivi === 2) {
                 socket_fast_split.send(JSON.stringify({intestazione: intestazioni_conto_separato_2_grande_split, conto: tab_conto_separato_2_grande_split}));
                 }*/

                time19 = new Date().getTime();
                console.log("TIME_CO2", time19 - time1);

                comanda.sincro.query_cassa();
                fine_conto_attivo(callBACCO, salta_colore);
            }

        });
    }
};

var oggetto_conto = new Array();
function riga_conto(prezzo_maxi_prima, prezzo_varianti_maxi, prezzo_varianti_aggiuntive, tasto_segue, spazio_forno, cod_promo, QTAP, giorno, nome_pc, stampata_sn, terminale, perc_iva, id, cod_articolo, ordinamento, peso_ums, ultima_portata, numero_conto, nodo, cat_variante, dest_stampa, dest_stampa_2, portata, desc_art, quantita, prezzo_un, categoria, ntav_comanda, operatore, tipo_record, stato_record, prog_inser, posizione, BIS) {
    this.prezzo_maxi_prima = prezzo_maxi_prima;
    this.prezzo_varianti_maxi = prezzo_varianti_maxi;
    this.prezzo_varianti_aggiuntive = prezzo_varianti_aggiuntive;
    this.tasto_segue = tasto_segue;
    this.spazio_forno = spazio_forno;
    this.cod_promo = cod_promo;
    this.QTAP = QTAP;
    this.giorno = giorno;
    this.nome_pc = nome_pc;
    this.stampata_sn = stampata_sn;
    this.terminale = terminale;
    this.perc_iva = perc_iva;
    this.id = id;
    this.cod_articolo = cod_articolo;
    this.ordinamento = ordinamento;
    this.peso_ums = peso_ums;
    this.ultima_portata = ultima_portata;
    this.numero_conto = numero_conto;
    this.nodo = nodo;
    this.cat_variante = cat_variante;
    this.dest_stampa = dest_stampa;
    this.dest_stampa_2 = dest_stampa_2;
    this.portata = portata;
    this.desc_art = desc_art;
    this.quantita = quantita;
    this.prezzo_un = prezzo_un;
    this.categoria = categoria;
    this.ntav_comanda = ntav_comanda;
    this.operatore = operatore;
    this.tipo_record = tipo_record;
    this.stato_record = stato_record;
    this.prog_inser = prog_inser;
    this.posizione = posizione;
    this.BIS = BIS;
}

function riga_conto_ricostruito(ord, cod_promo, fiscalizzata_sn, ora_, rag_soc_cliente, perc_iva, nome_comanda, stampata_sn, numero_conto, dest_stampa, portata, categoria, prog_inser, nodo, desc_art, prezzo_un, prezzo_varianti_aggiuntive, prezzo_varianti_maxi, prezzo_maxi_prima, quantita, contiene_variante, tipo_impasto) {
    this.ord = ord;
    this.cod_promo = cod_promo;
    this.fiscalizzata_sn = fiscalizzata_sn;
    this.ora_ = ora_;
    this.rag_soc_cliente = rag_soc_cliente;
    this.perc_iva = perc_iva;
    this.nome_comanda = nome_comanda;
    this.stampata_sn = stampata_sn;
    this.numero_conto = numero_conto;
    this.dest_stampa = dest_stampa;
    this.portata = portata;
    this.categoria = categoria;
    this.prog_inser = prog_inser;
    this.nodo = nodo;
    this.desc_art = desc_art;
    this.prezzo_un = prezzo_un;
    this.prezzo_varianti_aggiuntive = prezzo_varianti_aggiuntive;
    this.prezzo_varianti_maxi = prezzo_varianti_maxi;
    this.prezzo_maxi_prima = prezzo_maxi_prima;
    this.quantita = quantita;
    this.contiene_variante = contiene_variante;
    this.tipo_impasto = tipo_impasto;
}
