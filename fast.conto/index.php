﻿<!DOCTYPE html>
<!-- DOCUMENTAZIONE: https://bootswatch.com/darkly/ -->
<html>
    <head>
        <title>fast.intellinet</title>

        <!-- TAG BASE -->
        <meta charset="UTF-8">
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="mobile-web-app-capable" content="yes">

        <!--ICONA-->
        <link rel="icon" href="../img/fastintellinet.ico">

        <!-- CSS DI BOOTSTRAP -->
        <link href="../bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../bootstrap/css/jquery-ui.min.css" rel="stylesheet">

        <!-- Temi e aggiunte Bootstrap -->
        <link href="../bootstrap/css/darkly.min.css" rel="stylesheet">
        <link href="../bootstrap/css/docs.min.css" rel="stylesheet">
        <link href="../bootstrap/css/sticky-footer-navbar.css" rel="stylesheet">
        <link href="../bootstrap/css/stile_fastintelligent.css" rel="stylesheet">

        <!-- Classe loading per il caricamento della webapp-->
        <link href="../bootstrap/css/loading.css" rel="stylesheet">

        <script type="text/javascript" src="screenfull.js"></script>

        <!-- jQuery -->
        <script type="text/javascript" src="../jQuery/jquery-1.11.3.min.js"></script>
        <script type="text/javascript" src="../jQuery/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="../jQuery/jquery-ui.js"></script>

        <!-- Plugins jQuery -->
        <script type="text/javascript" src="../jQuery/jquery.ui.touch-punch.min.js"></script>
        <script type="text/javascript" src="../jQuery/jquery.mobile.custom.min.js"></script>
        <script type="text/javascript" src="../jQuery/fabric.js"></script>

        <!-- PrintArea -->
        <script type="text/javascript" src="../classi_javascript/jquery.PrintArea.js" ></script>

        <!-- Bootstrap Js -->
        <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js" ></script>

        <!-- Classi per la stampa -->
        <script type="text/javascript" src="../classi_javascript/epos-print-4.1.0.js"></script>
        <script type="text/javascript" src="../classi_javascript/fiscalprint.js"></script>

        <script src="../classi_javascript/operazioni_xml.js" type="text/javascript"></script>
        <script type="text/javascript" src="../classi_javascript/spooler.js"></script>

        <!-- Gestione del database locale -->
        <script type="text/javascript" src="../classi_javascript/gestione_db_xml.js"></script>
        <script type="text/javascript" src="../classi_javascript/funzioni_db_locale.js"></script>

        <!-- Riconnessione al websocket -->
        <script type="text/javascript" src="../classe_riconnessione_websocket/reconnecting-websocket.js"></script>

        <!-- Configurazione iniziale -->
        <script type="text/javascript" src="../classi_javascript/socket.js"></script>

        <script type="text/javascript" src="configuration_cliente.js"></script>

        <script type="text/javascript">
            comanda.fastconto = true;
            var websocket = new Array();
        </script>

        <!-- Gestione multilanguage -->
        <script type="text/javascript" src="../classi_javascript/multilanguage.js"></script>



        <!--Raccoglitore informazioni-->
        <script type="text/javascript" src="../classi_javascript/raccoglitore_informazioni.js"></script>

        <!-- Script principali per la gestione locale -->
        <script type="text/javascript" src="../classi_javascript/cassa.js"></script>

        <script type="text/javascript" src="bootstrap_cliente.js"></script>
        <script src="../classi_javascript/servizio.js" type="text/javascript"></script>

        <script src="../classi_javascript/importazione_accurata.js" type="text/javascript"></script>
        <script type="text/javascript" src="../classi_javascript/differenze_xml.js"></script>

        <!-- Gestione sicurezza MD5 e date -->
        <script type="text/javascript" src="../classi_javascript/md5.js"></script>
        <script type="text/javascript" src="../classi_javascript/dateformat.js"></script>

        <!-- Tastiera virtuale -->
        <link href="../tastiera_virtuale/css/keyboard.css" rel="stylesheet">
        <link href="../tastiera_virtuale/css/keyboard-previewkeyset.css" rel="stylesheet">
        <script src="../tastiera_virtuale/js/jquery.keyboard.js"></script>
        <script src="../tastiera_virtuale/js/jquery.keyboard.extension-all.js"></script>

        <!--PING-->
        <script src="../ping_js/ping.js" type="text/javascript"></script>

        <script src="../classi_javascript/incassi_giornalieri.js" type="text/javascript"></script>

        <script src="../MultiDatesPicker/jquery-ui.multidatespicker.js"></script>

        <script type="text/javascript" src="../classi_javascript/event_register.js"></script>
        <script type="text/javascript" src="../chart/dist/Chart.bundle.js"></script>

        <script type="text/javascript" src="../classi_javascript/ricerca_per_lettera.js"></script>

        <script type="text/javascript" src="../classi_javascript/split_conto.js"></script>

        <script src="../classi_javascript/alasql@0.4.js" type="text/javascript"></script>

        <style>
            body,h1,h2{
                font-family: "Century Gothic", CenturyGothic, AppleGothic, sans-serif;
            }

            .bs-glyphicons li{                
                min-width:100% !important;
                font-size: 3vh;
                height: 10vh;
                color:black;
                border-radius:10px;
            }

            .bs-glyphicons li:hover {
                color:initial;
            }

            .bs-glyphicons li:active {
                background-color:#3498db !important;
                color:black;
            }

            .btn{
                border-radius:10px;
            }

            .ultime_battiture tr>td{
                text-align:left;
                font-size:3.2vh;
                padding:2px !important;
            }

            #tab_sx > div{
                font-size:3vh !important;
                margin-bottom:0 !important;

            }

            #tab_sx > div >a{
                border-radius:10px;
            }

            #conto_grande th, #conto_grande td{
                font-size:3vh;
            }

            #tab_dx tr th, #tab_dx tr td{
                font-size:3vh;                
                padding: 5px;
                padding-top: 0;
                padding-bottom:10px;           
            }

            #tab_dx tr:not(.portata) th, #tab_dx tr:not(.portata) td{
                text-align: left;}


            .btn-default,.btn-default:focus,btn-default:active,btn-default:hover,btn-default:visited{
                background-color:#464545;
                outline:0 !important;
                outline-color:#464545;
                border:5px solid #464545;
                border:0;
            }

            .bs-glyphicons li:active{
                background-color:#e74c3c !important;
            }

            .modal{
                z-index:2000;
            }

            .modal-body{
                overflow:auto;
            }

            h4.modal-title{
                font-size:4vh;
            }

            @font-face {
                font-family: ARBlanca;
                src: url(ARBLANCA.TTF);
            }

            .table {
                margin-bottom:0;
            }

            .tab_conto tr>td:nth-child(4){
                text-align:right !important;
            }

        </style>

        <script>
            comanda.tavolo = '<?php echo @$_GET['tavolo']; ?>';

            comanda.dispositivo = 'TAVOLO SELEZIONATO';
            comanda.operatore = 'FAST.CONTO';
            comanda.folder_number = '1';
        </script>

        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway" />
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Jaldi" />
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Droid+Serif" />
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Lora" />
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=PT+Serif" />

    </head>

    <body style="width:100%;height:100vh;">

        <div class="modal" id="popup_scelta_servizio"  data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" style="width:100%;margin:0;">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">SCELTA SERVIZIO</h4>
                    </div>
                    <div class="modal-body" >
                        <table class="table table-striped table-hover" style="font-weight:bold;font-size:45px;">

                        </table>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>

        <div id="pagina_selezione_tavoli_aperti">

            <div class="col-xs-12 nopadding text-center" id="lista_tavoli_aperti"><img src="logobianco.png" style="width: 50%;margin-top: 3px;"></div>

            <div class="col-md-12  col-xs-12 text-center" style="text-shadow: 2px 0 0 rgb(210, 35, 39), -2px 0 0 rgb(210, 35, 39), 0 2px 0 rgb(210, 35, 39), 0 -2px 0 rgb(210, 35, 39), 2px 2px rgb(210, 35, 39), -2px -2px 0 rgb(210, 35, 39), 2px -2px 0 rgb(210, 35, 39), -2px 2px 0 rgb(210, 35, 39);overflow: auto;font-family:Droid Serif;font-size:4vh;border:2px solid rgb(210, 35, 39);background-color: rgb(231, 76, 60);margin-top: 6px;">TAVOLI APERTI<span onclick="disegna_tavoli_aperti_fastconto();" style="width:60px;height:auto;float:right;"><img src="../img/refresh.png" /></span></div>

            <!--<div class="col-md-2  col-xs-2 text-center" style="border:2px solid #222222;;overflow: auto;font-family:Droid Serif;font-size:4vh;background-color:grey;color:black;margin-top: 6px;" onclick="apri_konto();">1</div>-->

        </div>



        <div id="pagina_konto" style="display:none;">

            <div class="col-xs-12 nopadding text-center" id="lista_tavoli_aperti"><img src="logobianco.png" style="width: 50%;margin-top: 3px;"></div>

            <div class="col-md-12  col-xs-12 text-center" style="text-shadow: 2px 0 0 rgb(210, 35, 39), -2px 0 0 rgb(210, 35, 39), 0 2px 0 rgb(210, 35, 39), 0 -2px 0 rgb(210, 35, 39), 2px 2px rgb(210, 35, 39), -2px -2px 0 rgb(210, 35, 39), 2px -2px 0 rgb(210, 35, 39), -2px 2px 0 rgb(210, 35, 39);overflow: auto;font-family:Droid Serif;font-size:4vh;border:2px solid rgb(210, 35, 39);background-color: rgb(231, 76, 60);margin-top: 6px;">KONTO n° <span id="numero_tavolo"></span></div>


            <div id="tab_dx" class="col-xs-12 text-center nopadding" style="overflow: auto;height:67vh;font-family:PT Serif;">

                <div style="overflow: auto;">

                    <table class="tab_conto table bg-info">
                        <thead class="bg-info" id="intestazioni_conto">
                        </thead>
                    </table>

                    <table class="tab_conto table table-striped table-hover">
                        <tbody id="conto" >
                        </tbody>
                    </table>

                </div>

            </div>

            <div class="col-md-12  col-xs-12 nopadding" style="overflow: auto;border:2px solid rgb(210, 35, 39);">
                <table class="tab_conto table bg-info">
                    <thead  class="bg-danger" id="totale_conto" >
                        <tr>
                            <td style="text-shadow: 2px 0 0 rgb(210, 35, 39), -2px 0 0 rgb(210, 35, 39), 0 2px 0 rgb(210, 35, 39), 0 -2px 0 rgb(210, 35, 39), 2px 2px rgb(210, 35, 39), -2px -2px 0 rgb(210, 35, 39), 2px -2px 0 rgb(210, 35, 39), -2px 2px 0 rgb(210, 35, 39);font-weight:normal;font-size: 3vh;font-family:Droid Serif;">Total</td>
                            <td style="width: 40%;"></td>

                            <td class="totale_scontrino" style="width:30%;text-shadow: 2px 0 0 rgb(210, 35, 39), -2px 0 0 rgb(210, 35, 39), 0 2px 0 rgb(210, 35, 39), 0 -2px 0 rgb(210, 35, 39), 2px 2px rgb(210, 35, 39), -2px -2px 0 rgb(210, 35, 39), 2px -2px 0 rgb(210, 35, 39), -2px 2px 0 rgb(210, 35, 39);font-weight:bold;font-size: 3vh;"></td>

                        </tr>
                    </thead>
                </table>

            </div>

            <div class="col-md-12  col-xs-12 text-center" style="overflow: auto;"><span style="font-size: 3.5vh;font-family:ARBlanca;">Danke und auf Wiedersehen</span><br><span style="color:grey;font-size: 2.2vh;font-family:Times New Roman;">by Checchin Software</span></div>

        </div>

    </body>

</html>
