<?php

ini_set('display_errors', 'On');

error_reporting(0);

set_time_limit(0);


$host = $_REQUEST['db_host'];
$user = $_REQUEST['db_user'];
$pass = $_REQUEST['db_pass'];

$db_name = $_REQUEST['db_name'];
$db_type = $_REQUEST['type'];

$prefix = '../../';

if ($db_type === 'mysql') {
    $db = new mysqli($host, $user, $pass, $db_name);
} else {
    $db = new SQLite3($prefix . $db_name);
}

//lcz corrisponderebbe a $_GET['lcz']
if (function_exists('filter_input')) {
    $id_licenza = filter_input(INPUT_POST, 'lcz', FILTER_SANITIZE_NUMBER_INT);
    $tabella = filter_input(INPUT_POST, 'tbx', FILTER_SANITIZE_STRING);
} else {
    $id_licenza = $_POST['lcz'];
    $tabella = $_POST['tbx'];
}

if ($db_type !== 'mysql') {

    $tablesquery = $db->query("PRAGMA table_info($tabella);");

    for ($i = 0; $table = $tablesquery->fetchArray(SQLITE3_ASSOC); $i++) {
        $colonna[$i] = $table['name'];
    }

    $prodotti[0] = $colonna;
}

if ($db_type === 'mysql') {
    $query = "SELECT * FROM fastcassa_$tabella WHERE id_licenza=$id_licenza ORDER BY id ASC;";
} else if ($tabella === "comanda") {
    //ESEMPIO DATA 19-05-16
    
    $data = date("Ymd");
    //BISOGNA AVERE ANCHE I NON ATTIVI PER SICUREZZA IN CASO DI RIALLINEAMENTO
    $query = "SELECT * FROM comanda WHERE substr(id,5,8)='$data' ORDER BY id ASC;";
} else {
    $query = "SELECT * FROM $tabella ORDER BY id ASC;";
}

$risultato = $db->query($query);


if ($db_type === 'mysql') {
    for ($i = 1; $riga = $risultato->fetch_assoc(); $i++) {
        $prodotti[$i] = $riga;
    }
} else {
    for ($i = 1; $riga = $risultato->fetchArray(SQLITE3_ASSOC); $i++) {
        $prodotti[$i] = $riga;
    }
}

$db->close();


echo json_encode($prodotti);
