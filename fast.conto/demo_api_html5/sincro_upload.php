<?php

//Imposto un timeout
set_time_limit(20);

//Solo errori fatali
error_reporting(0);

header('Access-Control-Allow-Origin: *');

$host = $_REQUEST['db_host'];
$user = $_REQUEST['db_user'];
$psas = $_REQUEST['db_pass'];

$db_name = $_REQUEST['db_name'];
$db_type = $_REQUEST['type'];

$prefix = '../';

main();

function main() {

    if ($db_type === 'mysql') {
        $db = new mysqli($host, $user, $pass, $db_name);
    } else {
        $db = new SQLite3($prefix.$db_name);
    }

//lcz corrisponderebbe a $_GET['lcz']
    if (function_exists('filter_input')) {
        $id_licenza = filter_input(INPUT_POST, 'lcz', FILTER_SANITIZE_NUMBER_INT);
        $tabella = filter_input(INPUT_POST, 'tbx', FILTER_SANITIZE_STRING);
        $colonne = json_decode($_POST['colonne']);
        $righe = json_decode($_POST['righe']);
    } else {
        $id_licenza = $_POST['lcz'];
        $tabella = $_POST['tbx'];
        $colonne = json_decode($_POST['colonne']);
        $righe = json_decode($_POST['righe']);
    }

    $num = count($righe);

    //var_dump($colonne[0]);
    //var_dump($righe[1]);

    function query_delete($db, $query) {

        if ($db->query($query) === FALSE) {

            $milly = rand(2, 6) / 10;

            sleep($milly);

            query_delete($db,$query);

            //echo '\n tentativo cancellazione ' . $query;
        }
    }

    function query_insert($db, $query) {
        
        if ($db->query($query) === FALSE) {

            $milly = rand(2, 6) / 10;

            sleep($milly);

            query_insert($db,$query);

            //echo '\n tentativo insert ' . $query;
        }
        else
        {
            echo $query;
        }
    }

    if ($tabella !== "comanda") {



        $query = "delete from $tabella;";

        query_delete($db, $query);
    }

    //echo '-'. $query;


    for ($i = 0; $i < $num; $i++) {

        //var_dump($righe[i]);

        $righe[$i] = str_replace("&#39;", "'", $righe[$i]);
        $righe[$i] = str_replace("%3A", ":", $righe[$i]);
        $righe[$i] = str_replace("%20", " ", $righe[$i]);
        $righe[$i] = str_replace("%E8", "è", $righe[$i]);


        //var_dump($colonne[$i]);
        //var_dump($righe[$i]);

        $query = "\nINSERT INTO " . $tabella . " (" . $colonne . ") VALUES (" . $righe[$i] . ");";

//var_dump($colonne);
//var_dump($righe);
        query_insert($db, $query);


        //echo  $query;
    }

    $db->close();
}
