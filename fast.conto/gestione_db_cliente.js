var Sincro = function () {


    var dbtype = 'sqlite';
    var dbname = 'provaINTELLINET.sqlite';

    //MANCA LA GESTIONE ERRORI
    //SI RISCHIA DI NON ALLINEARE BENE LE TABELLE SE CI SONO ERRORI DI QUERY

    var ar;
    var query = '';

    this.return_db = function ( ) {
        return db;
    };

    this.backup_db_locale = function (callback) {

        var operatore = null;

        if (comanda.operatore !== undefined && comanda.operatore !== null && comanda.operatore.length > 0)
        {
            operatore = comanda.operatore;
        } else
        {
            operatore = prompt('CORTESEMENTE, INSERISCI UN IDENTIFICATIVO DEL TABLET');
        }

        if (operatore !== undefined && operatore !== null && operatore.length > 0) {

            var colonne = [];
            var righe = [];

            comanda.sincro.query('select * from comanda where terminale="' + comanda.terminale + '";', function (result) {

                result.forEach(function (obj) {

                    for (var key in obj) {

                        colonne.push('\"' + obj[key] + '\"');

                    }

                    righe.push(colonne.join());

                    colonne = [];

                });

                var database_intero = righe.join('\r\n');

                comanda.sincro.backup_db_background(operatore, database_intero, function (cb) {
                    if (cb === true) {

                        if (comanda.socket_ciclizzati.length > 0) {
                            alert("ERRORE (1). NON HAI SINCRONIZZATO TUTTI I TUOI DATI CON GLI ALTRI COMPUTER. RITENTA DI CHIUDERE TRA 10 SECONDI.");

                            callback(false);
                        } else
                        {
                            comanda.sincro.query('select * from comanda;', function (result) {

                                result.forEach(function (obj) {

                                    for (var key in obj) {

                                        colonne.push('\"' + obj[key] + '\"');

                                    }

                                    righe.push(colonne.join());

                                    colonne = [];

                                });

                                var database_intero = righe.join('\r\n');

                                comanda.sincro.backup_db_background(operatore, database_intero, function (cb) {
                                    if (cb === true) {

                                        if (comanda.socket_ciclizzati.length > 0) {
                                            alert("ERRORE (2). NON HAI SINCRONIZZATO TUTTI I TUOI DATI CON GLI ALTRI COMPUTER. RITENTA DI CHIUDERE TRA 10 SECONDI.");

                                            callback(false);
                                        } else
                                        {
                                            callback(true);
                                        }

                                    } else
                                    {
                                        alert("ERRORE. IL BACKUP LOCALE (2) NON E' STATO SALVATO CORRETTAMENTE. RIPROVA.");
                                        callback(false);
                                    }
                                });

                            });
                            //callback(true);
                        }

                    } else
                    {
                        alert("ERRORE. IL BACKUP LOCALE (1) NON E' STATO SALVATO CORRETTAMENTE. RIPROVA.");
                        callback(false);
                    }
                });

            });



        } else
        {
            alert("ERRORE. DEVI INSERIRE ALMENO UN OPERATORE O UN IDENTIFICATIVO PER PROCEDERE. RIPROVA.");
            callback(false);
        }
    };

    this.backup_db_background = function (operatore, struttura, callBack) {

        var url_backup;

        if (location.protocol === 'http:' || location.protocol === 'https:') {
            url_backup = './classi_php/salva_file.php';
        } else
        {
            url_backup = comanda.url_server + 'classi_php/salva_file.php';
        }

        var request = $.ajax({
            timeout: 10000,
            method: "POST",
            url: url_backup,
            data: {operatore: operatore, struttura: struttura}
        });

        request.done(function (data) {

            console.log("SALVAFILEXML DONE", struttura, CryptoJS.MD5(struttura).toString(), data);

            if (data === CryptoJS.MD5(struttura).toString()) {
                callBack(true);
            } else
            {
                console.log("SALVAFILEXML FAIL");

                callBack(false);
            }
        });

        request.fail(function (event) {
            console.log("SALVAFILEXML FAIL");

            callBack(false);
        });
    };

    this.sincronizzazione_upload = function (tab, id_lic, callback) {
        var testo_query = new Array();

        switch (tab) {
            case "comanda":
                testo_query[0] = 'SELECT * FROM ' + tab + ' WHERE stato_record="CHIUSO" AND id LIKE "' + comanda.operatore.substr(0, 3) + '_%";';
                testo_query[1] = 'DELETE FROM comanda WHERE stato_record = "CHIUSO";';
                break;

            default:
                testo_query[0] = 'SELECT * FROM ' + tab + ';';
                testo_query[1] = 'select count(0);';
                break;

        }

        comanda.sincro.query(testo_query[0], function (righe) {
            ////console.log(righe);

            var cols = new Array();
            var rows = new Array();


            righe.forEach(function (riga) {
                ////console.log(riga);

                var key = new Array();
                var value = new Array();

                var i = 0;
                for (var chiave in riga) {
                    key[i] = chiave;
                    ////console.log(riga[chiave]);
                    value[i] = "'" + escape(riga[chiave]) + "'";

                    i++;
                }

                cols.push(key.join());
                rows.push(value.join());

            });


            $.ajax({
                type: "POST",
                dataType: "text",
                url: comanda.url_server + 'demo_api_html5/sincro_upload.php',
                data: {tbx: tab, colonne: JSON.stringify(cols[0]), righe: JSON.stringify(rows), type: dbtype,
                    db_name: dbname},
                success: function (risposta) {
                    if (risposta === "")
                    {
                        comanda.sincro.query(testo_query[1], function () {
                        });
                    } else {
                    }
                }
            });

        });
    };



    this.sincronizzazione_download = function (callback) {

        var ciao = new Promise(function (resolve, reject) {
            if (comanda.ip_fisso_terminale === undefined || comanda.ip_fisso_terminale.length === 0 || comanda.ip_fisso_terminale === '' || comanda.ip_fisso_terminale === '0' || comanda.ip_fisso_terminale === 0)
            {
                getIPs(function (d) {

                    comanda.ip_address = d;
                    comanda.terminale = leggi_get_url('id') + '-' + d;

                    resolve(true);

                });
            } else
            {
                resolve(true);
            }
        });



        ciao.then(function () {

            comanda.socket_starting = true;
            socket_init(once(function () {
                comanda.socket_starting = false;

                $.ajax({
                    type: "POST",
                    async: true,
                    url: 'http://localhost:' + comanda.porta_server_remoto + '/fast.intellinet_NUOVA/classi_php/trasferimentodb.php',
                    data: {
                        ip_server_centrale: comanda.url_server
                    },
                    dataType: "json",
                    beforeSend: function () {


                        comanda.loading_tab_tavoli = true;
                        comanda.loading_tab_comanda = true;
                        comanda.sock.send({tipo: "notification", operatore: comanda.operatore, query: comanda.ip_address + ' inizio download comanda', terminale: comanda.terminale, ip: comanda.ip_address});


                    },
                    success: function (risultato_json) {
                        if (comanda.socket_sospesi_tavoli.length > 0)
                        {
                            var iter = 0;

                            var cont = comanda.socket_sospesi_tavoli.length; //56

                            comanda.socket_sospesi_tavoli.forEach(function (testo_query) {

                                comanda.sincro.query(testo_query, function () {

                                    iter++;

                                    if (iter === cont) {
                                        comanda.loading_tab_tavoli = false;
                                    }
                                });
                            });

                            comanda.loading_tab_tavoli = false;
                            comanda.socket_sospesi_tavoli = [];
                        } else
                        {
                            comanda.loading_tab_tavoli = false;
                        }

                        if (comanda.socket_sospesi.length > 0)
                        {
                            var iter = 0;

                            var cont = comanda.socket_sospesi.length; //56

                            console.log("ITER 2", iter, cont);

                            comanda.socket_sospesi.forEach(function (testo_query) {

                                comanda.sincro.query(testo_query, function () {
                                    iter++;

                                    if (iter === cont) {
                                        comanda.loading_tab_comanda = false;
                                    }

                                });

                            });

                            comanda.loading_tab_comanda = false;
                            comanda.socket_sospesi = [];
                        } else
                        {
                            comanda.loading_tab_comanda = false;
                        }
                        if (risultato_json !== true) {
                            alert("Ce un errore di connessione con il database centrale. \n\
Verifica che il server sia acceso e ri-connetti il dispositivo al Wifi.\n\
\n\
Se procederai con l'esecuzione del software, utilizzerai \n\
il DATABASE LOCALE NON SINCRONIZZATO, A TUO RISCHIO E PERICOLO.");
                        }
                        callback(true);
                    },
                    error: function (errore) {
                        alert("Ce un errore di connessione con il database centrale. \n\
Verifica che il server sia acceso e ri-connetti il dispositivo al Wifi.\n\
\n\
Se procederai con l'esecuzione del software, utilizzerai \n\
il DATABASE LOCALE NON SINCRONIZZATO, A TUO RISCHIO E PERICOLO.");
                        callback(true);
                    }
                });
            }));
        });
    };

    function comprimi_query(query_input) {

        if (typeof query_input === 'string') {

            var query_output = query_input.replace(/\n/gi, '').replace(/\s\s+/g, ' ');

        } else
        {
            query_output = query_input;
        }

        return query_output;
    }

    this.query_cassa = function (query, timeout, callback) {

        if ((comanda.sincro_query_cassa_attivo !== true && comanda.array_dbcentrale_mancanti !== undefined && comanda.array_dbcentrale_mancanti[0] !== undefined && comanda.array_dbcentrale_mancanti[0] !== null) || (Date.now() / 1000 - comanda.last_time_query_cassa >= 5) || query !== undefined) {

            comanda.sincro_query_cassa_attivo = true;


            var testo_query;

            if (query !== undefined) {
                testo_query = query;
            } else
            {
                testo_query = comanda.array_dbcentrale_mancanti[0];
            }

            testo_query = comprimi_query(testo_query);


            console.log("DB CENTRALE SENDED", testo_query);

            var to = 10000;
            if (timeout !== undefined)
            {
                to = timeout;
            }

            $.ajax({
                type: "POST",
                async: true,
                url: comanda.url_server + 'classi_php/db_realtime_cassa.lib.php',
                data: {
                    type: dbtype,
                    db_name: dbname,
                    query: testo_query
                },
                dataType: "json",
                timeout: to,
                beforeSend: function () {

                    console.log("QUERY CASSA BEFORESEND", testo_query);

                    comanda.last_time_query_cassa = Date.now() / 1000;
                    comanda.sincro_query_cassa_attivo = true;

                },
                success: function (dati) {



                    console.log("QUERY CASSA DEBUG", dati);

                    /*if (typeof (callback) === "function") {
                     console.log("QUERY CASSA CALLBACK TRUE");
                     
                     callback(dati);
                     }*/

                    //Rimuove l'indice 0 per una lunghezza di indice, senza sostituire niente
                    //CASISTICA IN CUI E' UNA QUERY CICLICIZZATA
                    if (query === undefined && dati === true) {


                        console.log("QUERY CASSA SUCCESS TRUE", testo_query, comanda.array_dbcentrale_mancanti[0], comanda.array_dbcentrale_mancanti);

                        comanda.array_dbcentrale_mancanti.shift();

                        console.log("QUERY CASSA DELETED", comanda.array_dbcentrale_mancanti);


                        comanda.last_time_query_cassa = Date.now() / 1000;

                        $('.spia.db_centrale').css('background-color', 'green');

                        comanda.sincro_query_cassa_attivo = false;

                        comanda.sincro.query_cassa();

                        if (typeof (callback) === "function") {
                            console.log("QUERY CASSA CALLBACK TRUE");

                            callback(dati);
                        }


                    }
                    //CASISTICA IN CUI E' UNA QUERY IN CASSA DIRETTA
                    else if (dati === true) {



                        console.log("QUERY CASSA (2) SUCCESS TRUE", testo_query, comanda.array_dbcentrale_mancanti[0], comanda.array_dbcentrale_mancanti);


                        comanda.last_time_query_cassa = Date.now() / 1000;

                        $('.spia.db_centrale').css('background-color', 'green');

                        comanda.sincro_query_cassa_attivo = false;

                        comanda.sincro.query_cassa();

                        if (typeof (callback) === "function") {
                            console.log("QUERY CASSA CALLBACK TRUE");

                            callback(dati);
                        }


                    } else if (isJson(dati) == true) {



                        comanda.last_time_query_cassa = Date.now() / 1000;

                        $('.spia.db_centrale').css('background-color', 'green');

                        console.trace();



                        comanda.sincro_query_cassa_attivo = false;

                        comanda.sincro.query_cassa();

                        if (typeof (callback) === "function") {
                            callback(dati);
                        }

                    } else {



                        if (typeof (callback) === "function") {
                            callback(dati);
                        }

                        console.log("QUERY CASSA SUCCESS ERR", query);

                        comanda.last_time_query_cassa = Date.now() / 1000;


                        $('.spia.db_centrale').css('background-color', 'red');

                        setTimeout(function () {
                            //facendo così la condizione resta bloccata a true fino al prossimo giro
                            //mettendo false fuori dal timeout ci sarebbero 3 secondi di gap con possibili overload
                            comanda.sincro_query_cassa_attivo = false;
                            comanda.sincro.query_cassa();
                        }, 3000);


                    }

                },
                error: function (xhr, status, error) {

                    if (typeof (callback) === "function") {
                        console.log("QUERY CASSA CALLBACK FALSE");

                        callback(false);
                    }

                    console.log("QUERY CASSA ERROR", testo_query, comanda.array_dbcentrale_mancanti[0], xhr.responseText);

                    comanda.last_time_query_cassa = Date.now() / 1000;


                    $('.spia.db_centrale').css('background-color', 'red');


                    setTimeout(function () {
                        //facendo così la condizione resta bloccata a true fino al prossimo giro
                        //mettendo false fuori dal timeout ci sarebbero 3 secondi di gap con possibili overload
                        comanda.sincro_query_cassa_attivo = false;
                        comanda.sincro.query_cassa();
                    }, 3000);
                }

            });
        }

    };

    this.query = function (string, callBack) {

        if (string !== undefined) {


            if (string.toUpperCase().search("COMANDA") !== -1)
            {
                if (string.search("(numero_conto='' OR numero_conto='undefined' OR numero_conto is null OR numero_conto='null' OR numero_conto='1' OR numero_conto=1)") !== -1||string.search("select distinct numero_conto") !== -1)
                {
                } else {
                }
            }

            $.ajax({
                type: "POST",
                async: true,
                url: comanda.url_server+'ordinazione_cliente/query.lib.php',
                data: {
                    query: comprimi_query(string)
                },
                dataType: "json",
                beforeSend: function () {},
                success: function (risultato_json) {
                    var risultato = new Array();

                    for (var i = 0; i < risultato_json.length; i++) {
                        var row = risultato_json[i];
                        risultato[i] = row;
                    }


                    callBack(risultato);
                },
                error: function (errore) {
                    console.log("ERRORE QUERY ASYNC: ", errore, comprimi_query(string));
                }
            });
        }
    }
    ;

    this.query_sync = function (string, callBack) {

        if (string !== undefined) {

             if (string.toUpperCase().search("COMANDA") !== -1)
            {
                if (string.search("(numero_conto='' OR numero_conto='undefined' OR numero_conto is null OR numero_conto='null' OR numero_conto='1' OR numero_conto=1)") !== -1||string.search("select distinct numero_conto") !== -1)
                {
                } else {
                }
            }




            $.ajax({
                type: "POST",
                async: false,
                url: 'http://localhost:' + comanda.porta_server_remoto + '/fast.intellinet_NUOVA/classi_php/query_locale.lib.php',
                data: {
                    query: comprimi_query(string)
                },
                dataType: "json",
                beforeSend: function () {},
                success: function (risultato_json) {
                    var risultato = new Array();

                    for (var i = 0; i < risultato_json.length; i++) {
                        var row = risultato_json[i];
                        risultato[i] = row;
                    }

                    callBack(risultato);
                },
                error: function (errore) {
                    console.log("ERRORE QUERY SYNC: ", errore, comprimi_query(string));
                }
            });
        }
    };
};



function download_copia_centrale() {
    window.open(comanda.url_server + 'provaINTELLINET.sqlite');
}

function download_copia_locale() {
    comanda.sincro.backup_db_locale(function (cb_db_locale) {
        alert("Database Locale salvato correttamente nel percorso predefinito (C:/TAVOLI/FAST.INTELLINET_NUOVA/BACKUP_LOCALI");
    });
}