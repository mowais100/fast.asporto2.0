function leggi_get_url(name, url) {
    if (!url)
        url = location.href;
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(url);
    return results === null ? null : results[1];
}

function getIPs(callback) {
    window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;   //compatibility for firefox and chrome
    var pc = new RTCPeerConnection({iceServers: []}), noop = function () {};
    pc.createDataChannel("");    //create a bogus data channel
    pc.createOffer(pc.setLocalDescription.bind(pc), noop);    // create offer and set local description
    pc.onicecandidate = function (ice) {  //listen for candidate events
        if (!ice || !ice.candidate || !ice.candidate.candidate)
            return;
        var myIP = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/.exec(ice.candidate.candidate)[1];
        console.log('my IP: ', myIP);
        callback(myIP);
        pc.onicecandidate = noop;
    };
}

function Comanda() {
this.evento="onclick";
    this.eventino="click";
    this.ip_fisso_terminale = '';

    this.licenza = "mattia131";

    this.esecuzione_comanda_immediata = false;

    this.loading_tab_comanda = false;

    this.socket_sospesi = new Array();
    this.socket_sospesi_tavoli = new Array();

    //Errore quando si spengono tutti i socket
    this.rt_engine_error = false;

    //TEST PER IL NUOVO SPOOLER
    this.id_stampa = new Array();

    this.terminale = leggi_get_url('id');

    //ELIMINATO 02/11/2016
    //if (this.ip_fisso_terminale === undefined || this.ip_fisso_terminale.length === 0 || this.ip_fisso_terminale === '' || this.ip_fisso_terminale === '0' || this.ip_fisso_terminale === 0)
    //{
    //    getIPs(function (d) {
    //
    //        comanda.ip_address = d;
    //        comanda.terminale = leggi_get_url('id') + '-' + d;
    //
    //    });
    //}

    //CAMBIARE DOPO

    //ANDROID CANTIERE 8080
    //WINDOWS DEFAULT 80
    this.porta_server_remoto = '80';

    this.url_server = 'http://192.168.0.168:80/fast.intellinet/';

    this.indirizzo_access_point = 'http://192.168.0.254';

    this.intelligent_non_fiscale = "192.168.0.140";

    this.tipo_consegna = "NIENTE";

    this.prezzo_tasti_sotto = '0.00';

    this.specifica_tasti_sotto = '';

    //PREZZO NON MODIFICABILE IN MODIFICA ARTICOLO
    this.prezzo_non_modificabile = true;

    this.socket_ciclizzati_temp = new Array();

    this.socket_ciclizzati = new Array();

    this.partenza_automatica_socket_dispositivo = true;

    this.tentativo_fallito = new Array();

    this.server_cloud = false;

    this.ora_ultimo_spooler = Math.floor(Date.now() / 1000);

    this.disable_logs = false;

    this.percentuale_iva_default = 22;

    this.bandierina = "0";

    //DISATTIVA CONSOLE.LOG E CONSOLE.TRACE
    if (this.disable_logs === true) {
        console.log = function () {
        };
        console.trace = function () {
        };
    }

    //Qui si trovano tutti i settaggi locali

    //QUESTI PARAMETRI SONO SOLO PER KONTO QUITTUNG RESHNUNG SCONTRINO FATTURA
    //PER LE COMANDE VEDERE NOMI_STAMPANTI NEL DATABASE

    //INDIRIZZO IP DEL MISURATORE FISCALE INTELLINET
    //this.indirizzo_cassa = "192.168.0.2";

    //INDIRIZZO IP DELLA STAMPANTE NON FISCALE INTELLINET
    //this.intelligent_non_fiscale = "192.168.0.60";    

    //this.intelligent_non_fiscale = "192.168.0.140";

    //INDIRIZZO IP DELLA STAMPANTE NON FISCALE NON INTELLINET
    //this.indirizzo_non_fiscale = "192.168.0.51"; //STAMPANTE COMANDA N.F.


    //NON SERVE SCEGLIERE SE IL QUITTUNG E' O NO INTELLINET PERCHE' LO E' SEMPRE

    //STAMPA COMANDA INTELLINET O NO? (S/N)
    //SE STAMPO IN TEDESCO AD ESEMPIO PER ORA E' SI    

    //this.opz_comanda_intellinet = "N";

    //PREFISSO PER LA STAMPA FISSA
    //this.carattere_stampa_piccola = String.fromCharCode(18);//Ascii 27+71+1

    //SE LA STAMPANTE PER SCONTRINI E' PICCOLA SI SETTA SU S
    //E I CARATTERI DI QUITTUNG, RESHNUNG E CONTO VENGONO RIDOTTI DI 10

    this.nsegn = 0;

    this.sincro_query_cassa_attivo = false;


    this.stampante_piccola = "N";
    this.stampante_piccola_conto = "N";

    //VAR DI SISTEMA - NON TOCCARE
    this.stampato = true;

    //FINE DEI COMANDI PER LE STAMPE ESCLUSE LE COMANDE


    //DISPOSITIVO INIZIALE DI DEFAULT
    this.dispositivo = "COMANDA";

    /*
     * LINGUE DISPONIBILI:
     * italiano
     * english
     * deutsch
     */

    //Lingua del display locale
    this.lingua = "italiano";

    //Lingua di stampa predefinita (in caso di errori NON TOCCARE)
    this.lingua_stampa = "deutsch";

    //Operatore predefinito
    this.operatore = 0;

    //Password predefinita
    this.password = 0;

    //Quanto apro il tavolo apre subito la categoria predefinita (FACOLTATIVO VALE SOLO SU PALMARE)
    this.apri_subito_categoria_predefinita = true;



    //Pagina degli articoli predefinita (numero pagina)
    this.pagina = 1;

    //Tavolo predefinito (numero tavolo)
    this.tavolo = 0;

    //Debug abilitato
    this.debug = 0;

    //Variabili del conto separato o no (NE MANCANO ALCUNE FISCALI)
    //this.intestazioni_conto=false;
    this.conto = '#conto';
    this.intestazione_conto = '#intestazioni_conto';
    this.totale_scontrino = '#totale_scontrino';

    //La finestra dei coperti sopra i totali è necessaria?
    this.coperti_necessari = true;

    //Funzione per reimpostare le variabili di default dopo il conto separato
    this.reimposta_variabili_conto_separato = function () {
        this.conto = '#conto';
        this.intestazione_conto = '#intestazioni_conto';
        this.totale_scontrino = '#totale_scontrino';
    };

    //Settaggio per scegliere se tornare a bar o a tavoli
    this.torna_bar = true;

    //Settaggi di sistema di default (non toccare se non sei un programmatore esperto)

    this.spooler_aperto = false;
    this.spooler_risposte_catturate = 0;

    this.syncerror = false;
    this.compatibile_xml = false;
    this.parcheggio_torna_tavoli === false;
    this.tasto_indietro_parcheggio = false;
    this.popup_modifica_articolo = false;
    this.loading = true;
    this.popup_modifica_articolo = false;
    this.join = false;
    this.ajax_attivo = 0;
    this.parcheggio = 0;
    this.ora_consegna = '';
    this.minuti_consegna = '';
    this.ultima_cat_variante = 0; //V_Bibite
    this.ultima_opz_variante = ''; // + o -
    //this.ultima_categoria_principale = 1;
    this.sblocca_tavolo_cliccato = false;
    this.permesso = 1;
    this.drag = false;

    //Metodi di sistema (non toccare se non sei un programmatore esperto)
    this.sincro = new Sincro();
    this.funzionidb = new FunzioniDB();
    this.sock = new Funzioni_Socket();

    //Memorizzazione delle sincronie mancanti
    //Array di oggetti
    //L'array è numerata e contiene per ogni numero l'oggetto sock
    if (this.array_dbcentrale_mancanti === undefined) {
        this.array_dbcentrale_mancanti = new Array();
    }

    //Questa è solo per i tavoli. Serve ad inviare un tavolo quando è stato completato nell'array vero
    if (this.array_dbcentrale_mancanti_temp === undefined) {
        this.array_dbcentrale_mancanti_temp = new Array();
    }

    //Funzione Ajax di base (non toccare se non sei un programmatore esperto)
    this.costruttore_ajax = function (async) {

        //la sincronia predefinita è SINCRONA
        if (async === undefined)
        {
            async = false;
        }

        ////console.log(async);

        $.ajax({
            type: "POST",
            async: async,
            timeout: 10000,
            url: comanda.url_server + 'classi_php/cassa_ajax_'+comanda.lingua_stampa+'.lib.php',
            data: {
                action: "costruttore_ajax",
                //passo dati_costruttore come array
                dati_costruttore: {
                    nome_dispositivo: comanda.dispositivo,
                    nome_operatore: comanda.operatore,
                    password_md5: comanda.password,
                    id_categoria: comanda.categoria,
                    numero_pagina: comanda.pagina,
                    numero_tavolo: comanda.tavolo,
                    nome_parcheggio: comanda.parcheggio
                }
            }
        });
    };
}




//OGGETTO PER CREARE LA COMANDA CON TUTTE LE SUE PROPRIETA'
var comanda = new Comanda();